﻿using ScoreCard.Data;
using ScoreCard.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Data;

namespace ScoreCard.Controllers
{
    public class HomeNewController : Controller
    {
        DataModel data = new DataModel();
        int location_id;
        // GET: HomeNew
        public ActionResult Index(int? id)
        {
            //changed by Bhupesh 01-04-2019
            // Here LocationID is now called as Account ID...
            Session["LocationID"] = Convert.ToInt32(id);
            location_id = Convert.ToInt32(Session["LocationID"]);

            return View();
        }
        public ActionResult PracticeProfile(string FromDate, string Todate)
        {
            List<PracticeProfile> practiceProfiles = new List<PracticeProfile>();
            // if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["OfficeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
            {
                practiceProfiles = data.GetPracticeProfile(FromDate, Todate, Convert.ToInt32(Session["LocationID"]));
                if (practiceProfiles == null || practiceProfiles.Count == 0)
                {
                    PracticeProfile profile;
                    for (int i = 1; i <= 12; i++)
                    {
                        profile = new PracticeProfile();
                        profile.HYGIENE_OPERATORIES = 0;
                        profile.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                        //  profile.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                        profile.HYGIENE_OPERATORIES = 0;
                        profile.RESTORATIVE_OPERATORIES = 0;
                        profile.TOTAL_DOCTORS = 0;
                        profile.TOTAL_OPERATORIES = 0;
                        profile.Marketing_Costs = 0;
                        profile.Month = i;
                        profile.MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i);
                        practiceProfiles.Add(profile);
                    }

                }
                return View(practiceProfiles);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }
        public ActionResult SmartNumbers(string FromDate, string Todate, string submit)
        {
            try
            {
                // if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["OfficeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {
                    SmartNumbersList smartNumbers = new SmartNumbersList();
                    
                    smartNumbers = data.GetSmartNumbers(FromDate, Todate, Convert.ToInt32(Session["LocationID"]));
                    SmartNumbers smartNumbersData;
                    for (int i = 1; i <= 12; i++)
                    {
                        if (smartNumbers.SmartNumbersData.Where(x => x.Month == i).Count() == 0)
                        {
                            smartNumbersData = new SmartNumbers();
                            smartNumbersData.COLLECTIBLE_PRODUCTION = 0;
                            smartNumbersData.COLLECTIONS = 0;
                            smartNumbersData.HYGEINE_PRODUCTION = 0;
                            smartNumbersData.TOTAL_OFFICE_HOURS = 0;
                            smartNumbersData.RESTORATIVE_PRODUCTION = 0;
                            smartNumbersData.PRODUCTION = 0;
                            smartNumbersData.NEW_PATIENTS = 0;
                            smartNumbersData.Month = i;
                            smartNumbersData.MARKETING_COSTS = 0;
                            smartNumbersData.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                            // smartNumbersData.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                            smartNumbers.SmartNumbersData.Add(smartNumbersData);
                        }
                    }
                    return View(smartNumbers.SmartNumbersData);
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - NovemberGoals", ex.Message, ex.StackTrace);
                return View();
            }
        }
        public ActionResult CriticalNumbers(string FromDate, string Todate, string submit)
        {
            try
            {
                //if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["OfficeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {
                    CriticalNumbersList criticalNumbers = new CriticalNumbersList();
                    
                    criticalNumbers = data.GetCriticalNumbers(FromDate, Todate, Convert.ToInt32(Session["LocationID"]));
                    CriticalNumbers criticalNumbersData;
                    for (int i = 1; i <= 12; i++)
                    {
                        if (criticalNumbers.CriticalNumbersData.Where(x => x.Month == i).Count() == 0)
                        {
                            criticalNumbersData = new CriticalNumbers();
                            criticalNumbersData.CollectibleProduction_HygeineVisit = 0;
                            criticalNumbersData.Collection_Ratio = 0;
                            criticalNumbersData.HygeineProduction_Operatory = 0;
                            criticalNumbersData.MarketingCosts_NewPatient = 0;
                            criticalNumbersData.Marketing_ROI = 0;
                            criticalNumbersData.Production_Hour = 0;
                            criticalNumbersData.Production_NonDoc = 0;
                            criticalNumbersData.Month = i;
                            criticalNumbersData.Production_NonProdEmployee = 0;
                            criticalNumbersData.Production_Operatory = 0;
                            criticalNumbersData.RestorativeProduction_Operatory = 0;
                            criticalNumbersData.TotalProduction_HygeineVisit = 0;
                            criticalNumbersData.TotalProduction_NewPatient = 0;
                            criticalNumbersData.TotalRestorativeProduction_HygeineVisit = 0;
                            criticalNumbersData.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                            // smartNumbersData.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                            criticalNumbers.CriticalNumbersData.Add(criticalNumbersData);
                        }
                    }
                    return View(criticalNumbers.CriticalNumbersData);
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - NovemberGoals", ex.Message, ex.StackTrace);
                return View();
            }
        }
    }
}