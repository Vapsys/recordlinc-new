﻿using ScoreCard.Data;
using ScoreCard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ScoreCard.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {           
            return View();
        }

        public bool CheckLogin(string UserName, string Password)
        {
            try
            {
                ErrorLog.InsertErrorLog("Login - CheckLogin", "Entered", UserName + " " +Password);
                UserDetails us = new UserDetails();
                DataModel model = new DataModel();
                us = model.CheckLogin(UserName, Password);
                if (us.ID != 0)
                {
                    ErrorLog.InsertErrorLog("Login - CheckLogin", "Username", us.FirstName + " " + us.LastName + " " +us.OFFICE_ID+ " " +us.LOCATION_ID);
                    Session["UserName"] = us.FirstName + " " + us.LastName;
                    Session["OfficeID"] = us.OFFICE_ID;
                    Session["LocationID"] = us.LOCATION_ID;
                    Session["UserID"] = us.ID;
                    ErrorLog.InsertErrorLog("Login - CheckLogin", "Username", Session["UserName"].ToString() + " " + Session["OfficeID"].ToString() + " " + Session["LocationID"].ToString() + " " + Session["UserID"].ToString());
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("LoginController - CheckLogin", ex.Message, ex.StackTrace);
                return false;
            }

        }

        [HttpPost]
        public ActionResult Index(UserDetails us)
        {
            try {
                if (ModelState.IsValid)
                {
                    bool isValid = CheckLogin(us.UserName, us.Password);
                    if (isValid)
                    {
                        return RedirectToAction("Index", "Home",new { id = Session["LocationID"] });
                    }
                    else
                    {
                        ViewBag.Message = "Login credentials are incorrect. Kindly try again.";
                        return View(us);
                    }
                }
                else
                {
                    ViewBag.Message = "Login credentials are incorrect. Kindly try again.";
                    return View(us);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("LoginController - Index", ex.Message, ex.StackTrace);
                return View(us);
            }

        }
    }
}