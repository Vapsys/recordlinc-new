﻿using ScoreCard.Data;
using ScoreCard.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ScoreCard.Controllers
{
    public class ReportNewController : Controller
    {
        DataModelNew data = new DataModelNew();

        public ActionResult Appointment_Reports()        {            DataTable dt = data.GetAppointmentSummaryAll(Convert.ToInt32(Session["LocationID"]));            return View(dt);        }        [HttpPost]        public ActionResult Appointment_Reports(string FromDate, string ToDate, string submit, string GridHtml)        {            DataTable dt = new DataTable();            switch (submit)
            {
                case "Excel":
                    {
                        var gv = new GridView();
                        gv.DataSource = data.GetAppointmentSummary(FromDate, ToDate, Convert.ToInt32(Session["LocationID"]));
                        gv.DataBind();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment; filename=DemoExcel.xls");
                        Response.ContentType = "application/ms-excel";
                        Response.Charset = "";
                        StringWriter objStringWriter = new StringWriter();
                        HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
                        gv.RenderControl(objHtmlTextWriter);
                        Response.Output.Write(objStringWriter.ToString());
                        Response.Flush();
                        Response.End();
                        dt = data.GetAppointmentSummary(FromDate, ToDate, Convert.ToInt32(Session["LocationID"]));
                        return View(dt);
                    }
                case "SearchByDate":
                    {
                        dt = data.GetAppointmentSummary(FromDate, ToDate, Convert.ToInt32(Session["LocationID"]));
                        return View(dt);
                    }
            }            return View(dt);        }
        public ActionResult Operatory_Appointment_List()
        {
            DataTable dt = data.GetOperatoryAppointmentSummaryAll(Convert.ToInt32(Session["LocationID"]));
            return View(dt);
        }
        [HttpPost]
        public ActionResult Operatory_Appointment_List(string FromDate, string ToDate, string submit)
        {
            DataTable dt = new DataTable();            switch (submit)
            {
                case "Excel":
                    {
                        var gv = new GridView();
                        gv.DataSource = data.GetOperatoryAppointmentSummary(FromDate, ToDate, Convert.ToInt32(Session["LocationID"]));
                        gv.DataBind();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment; filename=DemoExcel.xls");
                        Response.ContentType = "application/ms-excel";
                        Response.Charset = "";
                        StringWriter objStringWriter = new StringWriter();
                        HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
                        gv.RenderControl(objHtmlTextWriter);
                        Response.Output.Write(objStringWriter.ToString());
                        Response.Flush();
                        Response.End();
                        dt = data.GetOperatoryAppointmentSummary(FromDate, ToDate, Convert.ToInt32(Session["LocationID"]));
                        return View(dt);
                    }
                case "SearchByDate":
                    {
                        dt = data.GetOperatoryAppointmentSummary(FromDate, ToDate, Convert.ToInt32(Session["LocationID"]));
                        return View(dt);
                    }
            }            return View(dt);
        }

        public ActionResult New_Patient_Summary()        {            DataTable dt = data.GetNewPatientSummaryAll(Convert.ToInt32(Session["LocationID"]));            return View(dt);        }        [HttpPost]        public ActionResult New_Patient_Summary(string FromDate, string ToDate, string submit)        {            DataTable dt = new DataTable();            switch (submit)
            {
                case "Excel":
                    {
                        var gv = new GridView();
                        gv.DataSource = data.GetNewPatientSummary(FromDate, ToDate, Convert.ToInt32(Session["LocationID"]));
                        gv.DataBind();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment; filename=DemoExcel.xls");
                        Response.ContentType = "application/ms-excel";
                        Response.Charset = "";
                        StringWriter objStringWriter = new StringWriter();
                        HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
                        gv.RenderControl(objHtmlTextWriter);
                        Response.Output.Write(objStringWriter.ToString());
                        Response.Flush();
                        Response.End();
                        dt = data.GetNewPatientSummary(FromDate, ToDate, Convert.ToInt32(Session["LocationID"]));
                        return View(dt);
                    }
                case "SearchByDate":
                    {
                        dt = data.GetNewPatientSummary(FromDate, ToDate, Convert.ToInt32(Session["LocationID"]));
                        return View(dt);
                    }
            }            return View(dt);        }

        public ActionResult Daily_Sheet_Reports()
        {
            DataTable dt = data.GetDaily_Sheet_ReportsAll(Convert.ToInt32(Session["LocationID"]));            return View(dt);
        }
        [HttpPost]        public ActionResult Daily_Sheet_Reports(string FromDate, string ToDate, string submit)        {            DataTable dt = new DataTable();            var gv = new GridView();
            gv.DataSource = data.GetDaily_Sheet_ReportsAll(Convert.ToInt32(Session["LocationID"]));
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=DemoExcel.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter objStringWriter = new StringWriter();
            HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
            gv.RenderControl(objHtmlTextWriter);
            Response.Output.Write(objStringWriter.ToString());
            Response.Flush();
            Response.End();
            dt = data.GetDaily_Sheet_ReportsAll(Convert.ToInt32(Session["LocationID"]));
            return View(dt);        }

        public ActionResult Schedule_Reports()        {            DataTable dt = data.GetScheduleSummaryAll(Convert.ToInt32(Session["LocationID"]));            return View(dt);        }        [HttpPost]        public ActionResult Schedule_Reports(string FromDate, string ToDate, string submit)        {            DataTable dt = new DataTable();            switch (submit)
            {
                case "Excel":
                    {
                        var gv = new GridView();
                        gv.DataSource = data.GetScheduleSummary(FromDate, ToDate, Convert.ToInt32(Session["LocationID"]));
                        gv.DataBind();
                        Response.ClearContent();
                        Response.Buffer = true;
                        Response.AddHeader("content-disposition", "attachment; filename=DemoExcel.xls");
                        Response.ContentType = "application/ms-excel";
                        Response.Charset = "";
                        StringWriter objStringWriter = new StringWriter();
                        HtmlTextWriter objHtmlTextWriter = new HtmlTextWriter(objStringWriter);
                        gv.RenderControl(objHtmlTextWriter);
                        Response.Output.Write(objStringWriter.ToString());
                        Response.Flush();
                        Response.End();
                        dt = data.GetScheduleSummary(FromDate, ToDate, Convert.ToInt32(Session["LocationID"]));
                        return View(dt);
                    }
                case "SearchByDate":
                    {
                        dt = data.GetScheduleSummary(FromDate, ToDate, Convert.ToInt32(Session["LocationID"]));
                        return View(dt);
                    }
            }            return View(dt);
        }

        public ActionResult SampleDemo()
        {
            return View();
        }
    }
}