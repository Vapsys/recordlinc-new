﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ScoreCard.Data;
using ScoreCard.Models;
using System.Configuration;
using System.Text;

namespace ScoreCard.Controllers
{
    public class ReportToolController : Controller
    {
        DataModel data = new DataModel();

        public ActionResult IndividualPracticeStatistics(int? year)        {            try            {
                // if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["officeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {                    IndividualSummaryList individualSummary = new IndividualSummaryList();                    int yearR = 0;                    if (year == 0 || year == null)                    {                        year = DateTime.Now.Year;                    }                    yearR = (int)year;                    individualSummary = data.GetIndividualSummary(yearR, Convert.ToInt32(Session["LocationID"]));                    ViewBag.Year = yearR;                    IndividualSummary individualSummarydata;                    for (int i = 1; i <= 12; i++)                    {                        if (individualSummary.IndividualSummaryData.Where(x => x.Month == i).Count() == 0)                        {                            individualSummarydata = new IndividualSummary();                            individualSummarydata.GrossProduction = 0;                            individualSummarydata.OrthoProduction = 0;                            individualSummarydata.Adjustments = 0;                            individualSummarydata.AdjustmentPer = 0;                            individualSummarydata.NetProduction = 0;                            individualSummarydata.NetProductionPerDay = 0;                            individualSummarydata.Collections = 0;                            individualSummarydata.RefundsNSF = 0;                            individualSummarydata.NetCollections = 0;                            individualSummarydata.OrthoProduction = 0;                            individualSummarydata.CollectionPer = 0;                            individualSummarydata.TotalAR = 0;                            individualSummarydata.ARRatio = 0;                            individualSummarydata.OrthoAR = 0;                            individualSummarydata.SimplePayAR = 0;                            individualSummarydata.ARMinusOrthoSimplepay = 0;                            individualSummarydata.ARRatioMinusOrthoSimplepay = 0;                            individualSummarydata.NewPts = 0;                            individualSummarydata.PtReferrals = 0;                            individualSummarydata.PerPtReferrals = 0;                            individualSummarydata.DeactivatedPts = 0;                            individualSummarydata.TotalPtCount = 0;                            individualSummarydata.TotalPtsSeen = 0;                            individualSummarydata.NetProductionPerPt = 0;                            individualSummarydata.ActualContCareVisits = 0;                            individualSummarydata.ContCareEfficiency = 0;                            individualSummarydata.Supplies = 0;                            individualSummarydata.SuppliesExpenseofGrossProduction = 0;                            individualSummarydata.LabExpense = 0;                            individualSummarydata.LabExpenseofGrossProduction = 0;                            individualSummarydata.Month = i;                            individualSummarydata.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                            // individualSummarydata.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                            individualSummary.IndividualSummaryData.Add(individualSummarydata);                        }                    }                    return View(individualSummary.IndividualSummaryData);                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }

            catch (Exception ex)            {                ErrorLog.InsertErrorLog("Report - IndividualPracticeStatistics", ex.Message, ex.StackTrace);                return View();            }        }

        public ActionResult HygeneMonthlySummary(int? year)
        {
            try
            {
                // if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["OfficeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {
                    HygeneSummaryList hygeneSummaries = new HygeneSummaryList();
                    int yearR = 0;
                    if (year == 0 || year == null)
                    {
                        year = DateTime.Now.Year;
                    }
                    yearR = (int)year;
                    hygeneSummaries = data.GetHygeneSummary(yearR, Convert.ToInt32(Session["LocationID"]));
                    ViewBag.Year = yearR;
                    HygeneSummary hygeneSummarydata;
                    for (int i = 1; i <= 12; i++)
                    {
                        if (hygeneSummaries.HygeneSummaryData.Where(x => x.Month == i).Count() == 0)
                        {
                            hygeneSummarydata = new HygeneSummary();

                            hygeneSummarydata.GENERAL_PRODUCTION = 0;
                            hygeneSummarydata.NET_PRODUCTION = 0;
                            hygeneSummarydata.ORTHO_PRODUCTION = 0;
                            hygeneSummarydata.NET_ORTHO_PRODUCTION = 0;
                            hygeneSummarydata.PTS_SEEN = 0;
                            hygeneSummarydata.GROSS_PRODUCTION_PER_PTS_SEEN = 0;
                            hygeneSummarydata.GROSS_PROVIDER_PRODUCTION_PER_DAY = 0;
                            hygeneSummarydata.GROSS_PRODUCTION = 0;
                            hygeneSummarydata.NET_PRODUCTION_NEW = 0;
                            hygeneSummarydata.PTS_SEEN_NEW = 0;
                            hygeneSummarydata.GROSS_PRODUCTION_PER_PTS_SEEN_NEW = 0;
                            hygeneSummarydata.ARESTIN = 0;
                            hygeneSummarydata.SRP_PERIO_MAINT = 0;
                            hygeneSummarydata.ARESTIN_PERIO_PTS_SEEN = 0;
                            hygeneSummarydata.Month = i;

                            hygeneSummarydata.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                            //hygeneSummarydata.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                            hygeneSummaries.HygeneSummaryData.Add(hygeneSummarydata);

                        }
                    }
                    return View(hygeneSummaries.HygeneSummaryData);
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Report - HygeneMonthlySummary", ex.Message, ex.StackTrace);
                return View();
            }


        }

        public ActionResult StatsAllThree(int? year)
        {
            try            {
                // if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["officeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {                    StatsAllSummary_List statsSummary_List = new StatsAllSummary_List();                    int yearR = 0;                    if (year == 0 || year == null)                    {                        year = DateTime.Now.Year;                    }                    yearR = (int)year;
                    statsSummary_List = data.GetStatsSummary(yearR, Convert.ToInt32(Session["LocationID"]));
                    ViewBag.Year = yearR;                    Stats_All_Summary statsSummary;                    for (int i = 1; i <= 12; i++)                    {                        if (statsSummary_List.stats_all_summarys_.Where(x => x.MONTH == i).Count() == 0)                        {                            statsSummary = new Stats_All_Summary();                            statsSummary.GROSS_PRODUCTION = 0;                            statsSummary.ORTHO_PRODUCTION = 0;                            statsSummary.ADJUSTMENTS = 0;                            statsSummary.NET_PRODUCTION = 0;
                            statsSummary.NET_PRODUCTION_PER_DAY = 0;
                            statsSummary.NET_PRODUCTION_PER_PT_DAY = 0;
                            statsSummary.COLLECTIONS = 0;
                            statsSummary.REFUNDS_NSF = 0;
                            statsSummary.SALES_TAX = 0;
                            statsSummary.NET_COLLECTIONS = 0;
                            statsSummary.ORTHO_COLLECTION = 0;
                            statsSummary.COLLECTION_PERC = 0;
                            statsSummary.TotalAR = 0;
                            statsSummary.TOTAL_TAX_PLANNED = 0;
                            statsSummary.OPPORTUNITIES = 0;
                            statsSummary.TOTAL_TAX_PLANNED_PER_OPPORUNITY = 0;
                            statsSummary.GOSS_HYG_PRODUCTION = 0;
                            statsSummary.HYG_ADJUSTMENTS = 0;
                            statsSummary.NET_HYG_PRODUCTION = 0;
                            statsSummary.PER_NET_PRODUCTION_NET_HYG = 0;
                            statsSummary.GOSS_PERIO_PRODUCTION = 0;
                            statsSummary.PER_GOSS_HYG_PRODUCTION_PERIO = 0;
                            statsSummary.NEW_PTS = 0;
                            statsSummary.NEW_PTS_AMT_TAX = 0;
                            statsSummary.NEW_PTS_AMT_COMPLETE_MONTHS = 0;
                            statsSummary.AVG_TAX_PER_NEWPTS = 0;
                            statsSummary.COMPELETE_RATIO = 0;
                            statsSummary.GENERAL_PRODUCTION = 0;
                            statsSummary.ORHO_ADJUSTMENTS = 0;
                            statsSummary.NET_ORTHO = 0;
                            statsSummary.NEW_PTS_SEEN = 0;
                            statsSummary.NET_ORTHO_PTS_SEEN = 0;
                            statsSummary.ORTHO_PTS_SEEN = 0;
                            statsSummary.AVG_GOSS_PRODUCTION_PER_PTS = 0;
                            statsSummary.AVG_GOSS_PRODUCTION_PER_DAY = 0;
                            statsSummary.CROWNS = 0;
                            statsSummary.BUILDUPS = 0;
                            statsSummary.PARTIALS = 0;
                            statsSummary.PREC_ATTACHMENTS = 0;
                            statsSummary.IMPLANTS_PLACED = 0;
                            statsSummary.EXT = 0;
                            statsSummary.ORTHO_STARTS = 0;
                            statsSummary.MONTH = i;                            statsSummary.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                            //statsSummary.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                            statsSummary_List.stats_all_summarys_.Add(statsSummary);                        }                    }                    return View(statsSummary_List.stats_all_summarys_);                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }

            catch (Exception ex)            {                ErrorLog.InsertErrorLog("Report - StatsAllThree", ex.Message, ex.StackTrace);                return View();            }
        }

        [HttpPost]
        public JsonResult AddData(int displayID, int pageId, int locationID)
        {
            try
            {
                var _string = data.UpdateDisplayMode(displayID, pageId, Convert.ToInt32(Session["LocationID"]));
                return Json("", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - UpdateFormStatus POST", ex.Message, ex.StackTrace);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public FileResult Export(string GridHtml)
        {
            return File(Encoding.ASCII.GetBytes(GridHtml), "application/vnd.ms-excel", "Report.xls");
        }
    }
}