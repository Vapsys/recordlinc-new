﻿using ScoreCard.Data;
using ScoreCard.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Data;

namespace ScoreCard.Controllers
{
    public class HomeController : Controller
    {
        DataModel data = new DataModel();
        int location_id;
        public ActionResult Index(int? id)
        {
            //changed by Bhupesh 01-04-2019
            // Here LocationID is now called as Account ID...
            Session["LocationID"] = Convert.ToInt32(id);
            location_id = Convert.ToInt32(Session["LocationID"]);

            return View();
        }
        
        
        [HttpPost]
        public JsonResult NewProvider(FormCollection frms)
        {
            var FName = frms["txtFName"];
            var LName = frms["txtLName"];
            var TypeOfProvider = frms[2];
            var DentrixProviderID = frms[3];
            var _result = data.AddNewProvider(FName, LName, TypeOfProvider, DentrixProviderID, Session["LocationID"].ToString());
            return Json("Data Saved Successfully.",JsonRequestBehavior.AllowGet);
        }

        public ActionResult Reports(int? id)        {            Session["LocationID"] = Convert.ToInt32(id);            return View();        }

        public ActionResult PracticeProfile(int? year)
        {
            List<PracticeProfile> practiceProfiles = new List<PracticeProfile>();
            // if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["OfficeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
            if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
            {
                int yearR = 0;
                if (year == 0 || year == null)
                {
                    year = DateTime.Now.Year;

                }
                yearR = (int)year;
            //    practiceProfiles = data.GetPracticeProfile(yearR, Convert.ToInt32(Session["LocationID"]));
                if (practiceProfiles == null || practiceProfiles.Count == 0)
                {
                    PracticeProfile profile;
                    for (int i = 1; i <= 12; i++)
                    {
                        profile = new PracticeProfile();
                        profile.HYGIENE_OPERATORIES = 0;
                        profile.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                        profile.Year = yearR;
                        //  profile.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                        profile.HYGIENE_OPERATORIES = 0;
                        profile.RESTORATIVE_OPERATORIES = 0;
                        profile.TOTAL_DOCTORS = 0;
                        profile.TOTAL_OPERATORIES = 0;
                        profile.Marketing_Costs = 0;
                        profile.Month = i;
                        profile.MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetAbbreviatedMonthName(i);
                        practiceProfiles.Add(profile);
                    }

                }
                return View(practiceProfiles);
            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        public ActionResult SmartNumbers(int? year)
        {
            try
            {
                // if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["OfficeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {
                    SmartNumbersList smartNumbers = new SmartNumbersList();
                    int yearR = 0;
                    if (year == 0 || year == null)
                    {
                        year = DateTime.Now.Year;

                    }
                    yearR = (int)year;
                  //  smartNumbers = data.GetSmartNumbers(yearR, Convert.ToInt32(Session["LocationID"]));
                    ViewBag.Year = yearR;
                    SmartNumbers smartNumbersData;
                    for (int i = 1; i <= 12; i++)
                    {
                        if (smartNumbers.SmartNumbersData.Where(x => x.Month == i).Count() == 0)
                        {
                            smartNumbersData = new SmartNumbers();
                            smartNumbersData.COLLECTIBLE_PRODUCTION = 0;
                            smartNumbersData.COLLECTIONS = 0;
                            smartNumbersData.HYGEINE_PRODUCTION = 0;
                            smartNumbersData.TOTAL_OFFICE_HOURS = 0;
                            smartNumbersData.RESTORATIVE_PRODUCTION = 0;
                            smartNumbersData.PRODUCTION = 0;
                            smartNumbersData.NEW_PATIENTS = 0;
                            smartNumbersData.Month = i;
                            smartNumbersData.MARKETING_COSTS = 0;
                            smartNumbersData.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                            // smartNumbersData.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                            smartNumbers.SmartNumbersData.Add(smartNumbersData);
                        }
                    }
                    return View(smartNumbers.SmartNumbersData);
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - NovemberGoals", ex.Message, ex.StackTrace);
                return View();
            }
        }
        public ActionResult CriticalNumbers(int? year)
        {
            try
            {
                //if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["OfficeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {
                    CriticalNumbersList criticalNumbers = new CriticalNumbersList();
                    int yearR = 0;
                    if (year == 0 || year == null)
                    {
                        year = DateTime.Now.Year;

                    }
                    yearR = (int)year;
                  //  criticalNumbers = data.GetCriticalNumbers(yearR, Convert.ToInt32(Session["LocationID"]));
                    ViewBag.Year = yearR;
                    CriticalNumbers criticalNumbersData;
                    for (int i = 1; i <= 12; i++)
                    {
                        if (criticalNumbers.CriticalNumbersData.Where(x => x.Month == i).Count() == 0)
                        {
                            criticalNumbersData = new CriticalNumbers();
                            criticalNumbersData.CollectibleProduction_HygeineVisit = 0;
                            criticalNumbersData.Collection_Ratio = 0;
                            criticalNumbersData.HygeineProduction_Operatory = 0;
                            criticalNumbersData.MarketingCosts_NewPatient = 0;
                            criticalNumbersData.Marketing_ROI = 0;
                            criticalNumbersData.Production_Hour = 0;
                            criticalNumbersData.Production_NonDoc = 0;
                            criticalNumbersData.Month = i;
                            criticalNumbersData.Production_NonProdEmployee = 0;
                            criticalNumbersData.Production_Operatory = 0;
                            criticalNumbersData.RestorativeProduction_Operatory = 0;
                            criticalNumbersData.TotalProduction_HygeineVisit = 0;
                            criticalNumbersData.TotalProduction_NewPatient = 0;
                            criticalNumbersData.TotalRestorativeProduction_HygeineVisit = 0;
                            criticalNumbersData.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                            // smartNumbersData.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                            criticalNumbers.CriticalNumbersData.Add(criticalNumbersData);
                        }
                    }
                    return View(criticalNumbers.CriticalNumbersData);
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - NovemberGoals", ex.Message, ex.StackTrace);
                return View();
            }
        }
        public ActionResult OfficeHours()
        {
            try
            {
                 ViewBag.ProviderList = data.GetProviderName(Convert.ToInt32(Session["LocationID"]));
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {
                    OfficeHours officeHours = new OfficeHours();
                    officeHours = data.GetOfficeHours(DateTime.Now.ToString("MM/dd/yyyy"), Convert.ToInt32(Session["LocationID"]));
                    officeHours.OFFICE_ENDHOUR = officeHours.OFFICE_ENDHOUR == null ? "" : officeHours.OFFICE_ENDHOUR;
                    officeHours.OFFICE_STARTHOUR = officeHours.OFFICE_STARTHOUR == null ? "" : officeHours.OFFICE_STARTHOUR;
                    return View(officeHours);
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - OfficeHours", ex.Message, ex.StackTrace);
                return View();
            }
        }

        public ActionResult JanGoals(int? year)
        {
            try
            {
                if(!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {
                    List<FishGoals> resultfishGoals = new List<FishGoals>();
                    List<FishGoals> fishGoals = new List<FishGoals>();
                    int yearR = 0;
                    if (year == 0 || year == null)
                    {
                        year = DateTime.Now.Year;

                    }
                    yearR = (int)year;
                    resultfishGoals = data.GetFishGoals(1, yearR, Convert.ToInt32(Session["LocationID"]));
                    fishGoals = resultfishGoals;
                    ViewBag.Year = yearR;
                    FishGoals smartNumbersData;
                    if (resultfishGoals.Count != 0)
                    {
                        foreach (string id in resultfishGoals.Select(x => x.PROVIDER_DENTRIXID).Distinct().ToList())
                        {
                            for (int i = 1; i <= 31; i++)
                            {
                                if (resultfishGoals.Where(x => x.REFDATE.Date == new DateTime(yearR, 1, i).Date && x.PROVIDER_DENTRIXID == id).Count() == 0)
                                {
                                    smartNumbersData = new FishGoals();
                                    smartNumbersData.PROVIDER_DENTRIXID = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().PROVIDER_DENTRIXID;
                                    smartNumbersData.ACTUAL_PRODUCTION = 0;
                                    smartNumbersData.FIRSTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().FIRSTNAME;
                                    smartNumbersData.GOAL_PRODUCTION = 0;
                                    smartNumbersData.HOURS_FOR_THEDAY = 0;
                                    smartNumbersData.HYGEINE_EXAMS = 0;
                                    smartNumbersData.LASTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().LASTNAME;
                                    smartNumbersData.NP_APPOINTMENTS = 0;
                                    smartNumbersData.Month = i;
                                    smartNumbersData.PERHOUR_PRODUCTION = 0;
                                    smartNumbersData.REFDATE = new DateTime(yearR, 1, i);
                                    smartNumbersData.ROUTINE_HYEGEINE_VISIT = 0;
                                    smartNumbersData.SCHEDULED_PRODUCTION = 0;
                                    smartNumbersData.SRP = 0;
                                    smartNumbersData.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                                    // smartNumbersData.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                                    fishGoals.Add(smartNumbersData);
                                }
                            }
                        }
                    }
                    return View(fishGoals);
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - JanFishGoals", ex.Message, ex.StackTrace);
                return View();
            }
        }
        public ActionResult FebGoals(int? year)
        {
            try
            {
                //  if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["OfficeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {
                    List<FishGoals> resultfishGoals = new List<FishGoals>();
                    List<FishGoals> fishGoals = new List<FishGoals>();
                    int yearR = 0;
                    if (year == 0 || year == null)
                    {
                        year = DateTime.Now.Year;

                    }
                    yearR = (int)year;
                    resultfishGoals = data.GetFishGoals(2, yearR, Convert.ToInt32(Session["LocationID"]));
                    fishGoals = resultfishGoals;
                    ViewBag.Year = yearR;
                    FishGoals smartNumbersData;
                    int j = yearR % 4 == 0 ? 29 : 28;
                    if (resultfishGoals.Count != 0)
                    {
                        foreach (string id in resultfishGoals.Select(x => x.PROVIDER_DENTRIXID).Distinct().ToList())
                        {
                            for (int i = 1; i <= j; i++)
                            {
                                if (resultfishGoals.Where(x => x.REFDATE.Date == new DateTime(yearR, 2, i).Date && x.PROVIDER_DENTRIXID == id).Count() == 0)
                                {
                                    smartNumbersData = new FishGoals();
                                    smartNumbersData.PROVIDER_DENTRIXID = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().PROVIDER_DENTRIXID;
                                    smartNumbersData.ACTUAL_PRODUCTION = 0;
                                    smartNumbersData.FIRSTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().FIRSTNAME;
                                    smartNumbersData.GOAL_PRODUCTION = 0;
                                    smartNumbersData.HOURS_FOR_THEDAY = 0;
                                    smartNumbersData.HYGEINE_EXAMS = 0;
                                    smartNumbersData.LASTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().LASTNAME;
                                    smartNumbersData.NP_APPOINTMENTS = 0;
                                    smartNumbersData.Month = i;
                                    smartNumbersData.PERHOUR_PRODUCTION = 0;
                                    smartNumbersData.REFDATE = new DateTime(yearR, 2, i);
                                    smartNumbersData.ROUTINE_HYEGEINE_VISIT = 0;
                                    smartNumbersData.SCHEDULED_PRODUCTION = 0;
                                    smartNumbersData.SRP = 0;
                                    smartNumbersData.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                                    // smartNumbersData.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                                    fishGoals.Add(smartNumbersData);
                                }
                            }
                        }
                    }
                    return View(fishGoals);
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - FebGoals", ex.Message, ex.StackTrace);
                return View();
            }
        }
        public ActionResult MarchGoals(int? year)
        {
            try
            {
                //  if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["OfficeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {
                    List<FishGoals> resultfishGoals = new List<FishGoals>();
                    List<FishGoals> fishGoals = new List<FishGoals>();
                    int yearR = 0;
                    if (year == 0 || year == null)
                    {
                        year = DateTime.Now.Year;

                    }
                    yearR = (int)year;
                    resultfishGoals = data.GetFishGoals(3, yearR, Convert.ToInt32(Session["LocationID"]));

                    fishGoals = resultfishGoals;
                    ViewBag.Year = yearR;
                    FishGoals smartNumbersData;
                    if (resultfishGoals.Count != 0)
                    {
                        foreach (string id in resultfishGoals.Select(x => x.PROVIDER_DENTRIXID).Distinct().ToList())
                        {
                            for (int i = 1; i <= 31; i++)
                            {
                                if (resultfishGoals.Where(x => x.REFDATE.Date == new DateTime(yearR, 3, i).Date && x.PROVIDER_DENTRIXID == id).Count() == 0)
                                {
                                    smartNumbersData = new FishGoals();
                                    smartNumbersData.PROVIDER_DENTRIXID = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().PROVIDER_DENTRIXID;
                                    smartNumbersData.ACTUAL_PRODUCTION = 0;
                                    smartNumbersData.FIRSTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().FIRSTNAME;
                                    smartNumbersData.GOAL_PRODUCTION = 0;
                                    smartNumbersData.HOURS_FOR_THEDAY = 0;
                                    smartNumbersData.HYGEINE_EXAMS = 0;
                                    smartNumbersData.LASTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().LASTNAME;
                                    smartNumbersData.NP_APPOINTMENTS = 0;
                                    smartNumbersData.Month = i;
                                    smartNumbersData.PERHOUR_PRODUCTION = 0;
                                    smartNumbersData.REFDATE = new DateTime(yearR, 3, i);
                                    smartNumbersData.ROUTINE_HYEGEINE_VISIT = 0;
                                    smartNumbersData.SCHEDULED_PRODUCTION = 0;
                                    smartNumbersData.SRP = 0;
                                    smartNumbersData.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                                    //  smartNumbersData.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                                    fishGoals.Add(smartNumbersData);
                                }
                            }
                        }
                    }
                    return View(fishGoals);
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - MarchGoals", ex.Message, ex.StackTrace);
                return View();
            }
        }
        public ActionResult AprilGoals(int? year)
        {
            try
            {
                // if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["OfficeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {
                    List<FishGoals> resultfishGoals = new List<FishGoals>();
                    List<FishGoals> fishGoals = new List<FishGoals>();
                    int yearR = 0;
                    if (year == 0 || year == null)
                    {
                        year = DateTime.Now.Year;

                    }
                    yearR = (int)year;
                    resultfishGoals = data.GetFishGoals(4, yearR, Convert.ToInt32(Session["LocationID"]));
                    fishGoals = resultfishGoals;
                    ViewBag.Year = yearR;
                    FishGoals smartNumbersData;
                    if (resultfishGoals.Count != 0)
                    {
                        foreach (string id in resultfishGoals.Select(x => x.PROVIDER_DENTRIXID).Distinct().ToList())
                        {
                            for (int i = 1; i <= 30; i++)
                            {
                                if (resultfishGoals.Where(x => x.REFDATE.Date == new DateTime(yearR, 4, i).Date && x.PROVIDER_DENTRIXID == id).Count() == 0)
                                {
                                    smartNumbersData = new FishGoals();
                                    smartNumbersData.PROVIDER_DENTRIXID = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().PROVIDER_DENTRIXID;
                                    smartNumbersData.ACTUAL_PRODUCTION = 0;
                                    smartNumbersData.FIRSTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().FIRSTNAME;
                                    smartNumbersData.GOAL_PRODUCTION = 0;
                                    smartNumbersData.HOURS_FOR_THEDAY = 0;
                                    smartNumbersData.HYGEINE_EXAMS = 0;
                                    smartNumbersData.LASTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().LASTNAME;
                                    smartNumbersData.NP_APPOINTMENTS = 0;
                                    smartNumbersData.Month = i;
                                    smartNumbersData.PERHOUR_PRODUCTION = 0;
                                    smartNumbersData.REFDATE = new DateTime(yearR, 4, i);
                                    smartNumbersData.ROUTINE_HYEGEINE_VISIT = 0;
                                    smartNumbersData.SCHEDULED_PRODUCTION = 0;
                                    smartNumbersData.SRP = 0;
                                    smartNumbersData.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                                    // smartNumbersData.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                                    fishGoals.Add(smartNumbersData);
                                }
                            }
                        }
                    }
                    return View(fishGoals);
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - AprilGoals", ex.Message, ex.StackTrace);
                return View();
            }
        }
        public ActionResult MayGoals(int? year)
        {
            try
            {
                // if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["OfficeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {
                    List<FishGoals> resultfishGoals = new List<FishGoals>();
                    List<FishGoals> fishGoals = new List<FishGoals>();
                    int yearR = 0;
                    if (year == 0 || year == null)
                    {
                        year = DateTime.Now.Year;

                    }
                    yearR = (int)year;
                    resultfishGoals = data.GetFishGoals(5, yearR, Convert.ToInt32(Session["LocationID"]));
                    fishGoals = resultfishGoals;
                    ViewBag.Year = yearR;
                    FishGoals smartNumbersData;
                    if (resultfishGoals.Count != 0)
                    {
                        foreach (string id in resultfishGoals.Select(x => x.PROVIDER_DENTRIXID).Distinct().ToList())
                        {
                            for (int i = 1; i <= 31; i++)
                            {
                                if (resultfishGoals.Where(x => x.REFDATE.Date == new DateTime(yearR, 5, i).Date && x.PROVIDER_DENTRIXID == id).Count() == 0)
                                {
                                    smartNumbersData = new FishGoals();
                                    smartNumbersData.PROVIDER_DENTRIXID = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().PROVIDER_DENTRIXID;
                                    smartNumbersData.ACTUAL_PRODUCTION = 0;
                                    smartNumbersData.FIRSTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().FIRSTNAME;
                                    smartNumbersData.GOAL_PRODUCTION = 0;
                                    smartNumbersData.HOURS_FOR_THEDAY = 0;
                                    smartNumbersData.HYGEINE_EXAMS = 0;
                                    smartNumbersData.LASTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().LASTNAME;
                                    smartNumbersData.NP_APPOINTMENTS = 0;
                                    smartNumbersData.Month = i;
                                    smartNumbersData.PERHOUR_PRODUCTION = 0;
                                    smartNumbersData.REFDATE = new DateTime(yearR, 5, i);
                                    smartNumbersData.ROUTINE_HYEGEINE_VISIT = 0;
                                    smartNumbersData.SCHEDULED_PRODUCTION = 0;
                                    smartNumbersData.SRP = 0;
                                    smartNumbersData.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                                    //smartNumbersData.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                                    fishGoals.Add(smartNumbersData);
                                }
                            }
                        }
                    }
                    return View(fishGoals);
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - MayGoals", ex.Message, ex.StackTrace);
                return View();
            }
        }
        public ActionResult JuneGoals(int? year)
        {
            try
            {
                //if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["OfficeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {
                    List<FishGoals> resultfishGoals = new List<FishGoals>();
                    List<FishGoals> fishGoals = new List<FishGoals>();
                    int yearR = 0;
                    if (year == 0 || year == null)
                    {
                        year = DateTime.Now.Year;

                    }
                    yearR = (int)year;
                    resultfishGoals = data.GetFishGoals(6, yearR, Convert.ToInt32(Session["LocationID"]));
                    fishGoals = resultfishGoals;
                    ViewBag.Year = yearR;
                    FishGoals smartNumbersData;
                    if (resultfishGoals.Count != 0)
                    {
                        foreach (string id in resultfishGoals.Select(x => x.PROVIDER_DENTRIXID).Distinct().ToList())
                        {
                            for (int i = 1; i <= 30; i++)
                            {
                                if (resultfishGoals.Where(x => x.REFDATE.Date == new DateTime(yearR, 6, i).Date && x.PROVIDER_DENTRIXID == id).Count() == 0)
                                {
                                    smartNumbersData = new FishGoals();
                                    smartNumbersData.PROVIDER_DENTRIXID = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().PROVIDER_DENTRIXID;
                                    smartNumbersData.ACTUAL_PRODUCTION = 0;
                                    smartNumbersData.FIRSTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().FIRSTNAME;
                                    smartNumbersData.GOAL_PRODUCTION = 0;
                                    smartNumbersData.HOURS_FOR_THEDAY = 0;
                                    smartNumbersData.HYGEINE_EXAMS = 0;
                                    smartNumbersData.LASTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().LASTNAME;
                                    smartNumbersData.NP_APPOINTMENTS = 0;
                                    smartNumbersData.Month = i;
                                    smartNumbersData.PERHOUR_PRODUCTION = 0;
                                    smartNumbersData.REFDATE = new DateTime(yearR, 6, i);
                                    smartNumbersData.ROUTINE_HYEGEINE_VISIT = 0;
                                    smartNumbersData.SCHEDULED_PRODUCTION = 0;
                                    smartNumbersData.SRP = 0;
                                    smartNumbersData.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                                    //smartNumbersData.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                                    fishGoals.Add(smartNumbersData);
                                }
                            }
                        }
                    }
                    return View(fishGoals);
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - JuneGoals", ex.Message, ex.StackTrace);
                return View();
            }
        }
        public ActionResult JulyGoals(int? year)
        {
            try
            {
                // if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["OfficeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {
                    List<FishGoals> resultfishGoals = new List<FishGoals>();
                    List<FishGoals> fishGoals = new List<FishGoals>();
                    int yearR = 0;
                    if (year == 0 || year == null)
                    {
                        year = DateTime.Now.Year;

                    }
                    yearR = (int)year;
                    resultfishGoals = data.GetFishGoals(7, yearR, Convert.ToInt32(Session["LocationID"]));
                    fishGoals = resultfishGoals;
                    ViewBag.Year = yearR;
                    FishGoals smartNumbersData;
                    if (resultfishGoals.Count != 0)
                    {
                        foreach (string id in resultfishGoals.Select(x => x.PROVIDER_DENTRIXID).Distinct().ToList())
                        {
                            for (int i = 1; i <= 31; i++)
                            {
                                if (resultfishGoals.Where(x => x.REFDATE.Date == new DateTime(yearR, 7, i).Date && x.PROVIDER_DENTRIXID == id).Count() == 0)
                                {
                                    smartNumbersData = new FishGoals();
                                    smartNumbersData.PROVIDER_DENTRIXID = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().PROVIDER_DENTRIXID;
                                    smartNumbersData.ACTUAL_PRODUCTION = 0;
                                    smartNumbersData.FIRSTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().FIRSTNAME;
                                    smartNumbersData.GOAL_PRODUCTION = 0;
                                    smartNumbersData.HOURS_FOR_THEDAY = 0;
                                    smartNumbersData.HYGEINE_EXAMS = 0;
                                    smartNumbersData.LASTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().LASTNAME;
                                    smartNumbersData.NP_APPOINTMENTS = 0;
                                    smartNumbersData.Month = i;
                                    smartNumbersData.PERHOUR_PRODUCTION = 0;
                                    smartNumbersData.REFDATE = new DateTime(yearR, 7, i);
                                    smartNumbersData.ROUTINE_HYEGEINE_VISIT = 0;
                                    smartNumbersData.SCHEDULED_PRODUCTION = 0;
                                    smartNumbersData.SRP = 0;
                                    smartNumbersData.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                                    //smartNumbersData.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                                    fishGoals.Add(smartNumbersData);
                                }
                            }
                        }
                    }
                    return View(fishGoals);
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - JulyGoals", ex.Message, ex.StackTrace);
                return View();
            }
        }
        public ActionResult AugustGoals(int? year)
        {
            try
            {
                // if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["OfficeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {
                    List<FishGoals> resultfishGoals = new List<FishGoals>();
                    List<FishGoals> fishGoals = new List<FishGoals>();
                    int yearR = 0;
                    if (year == 0 || year == null)
                    {
                        year = DateTime.Now.Year;

                    }
                    yearR = (int)year;
                    resultfishGoals = data.GetFishGoals(8, yearR, Convert.ToInt32(Session["LocationID"]));
                    fishGoals = resultfishGoals;
                    ViewBag.Year = yearR;
                    FishGoals smartNumbersData;
                    if (resultfishGoals.Count != 0)
                    {
                        foreach (string id in resultfishGoals.Select(x => x.PROVIDER_DENTRIXID).Distinct().ToList())
                        {
                            for (int i = 1; i <= 31; i++)
                            {
                                if (resultfishGoals.Where(x => x.REFDATE.Date == new DateTime(yearR, 8, i).Date && x.PROVIDER_DENTRIXID == id).Count() == 0)
                                {
                                    smartNumbersData = new FishGoals();
                                    smartNumbersData.PROVIDER_DENTRIXID = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().PROVIDER_DENTRIXID;
                                    smartNumbersData.ACTUAL_PRODUCTION = 0;
                                    smartNumbersData.FIRSTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().FIRSTNAME;
                                    smartNumbersData.GOAL_PRODUCTION = 0;
                                    smartNumbersData.HOURS_FOR_THEDAY = 0;
                                    smartNumbersData.HYGEINE_EXAMS = 0;
                                    smartNumbersData.LASTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().LASTNAME;
                                    smartNumbersData.NP_APPOINTMENTS = 0;
                                    smartNumbersData.Month = i;
                                    smartNumbersData.PERHOUR_PRODUCTION = 0;
                                    smartNumbersData.REFDATE = new DateTime(yearR, 8, i);
                                    smartNumbersData.ROUTINE_HYEGEINE_VISIT = 0;
                                    smartNumbersData.SCHEDULED_PRODUCTION = 0;
                                    smartNumbersData.SRP = 0;
                                    smartNumbersData.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                                    //  smartNumbersData.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                                    fishGoals.Add(smartNumbersData);
                                }
                            }
                        }
                    }
                    return View(fishGoals);
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - AugustGoals", ex.Message, ex.StackTrace);
                return View();
            }
        }
        public ActionResult SeptemberGoals(int? year)
        {
            try
            {
                // if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["OfficeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {
                    List<FishGoals> resultfishGoals = new List<FishGoals>();
                    List<FishGoals> fishGoals = new List<FishGoals>();
                    int yearR = 0;
                    if (year == 0 || year == null)
                    {
                        year = DateTime.Now.Year;

                    }
                    yearR = (int)year;
                    resultfishGoals = data.GetFishGoals(9, yearR, Convert.ToInt32(Session["LocationID"]));
                    fishGoals = resultfishGoals;
                    ViewBag.Year = yearR;
                    FishGoals smartNumbersData;
                    if (resultfishGoals.Count != 0)
                    {
                        foreach (string id in resultfishGoals.Select(x => x.PROVIDER_DENTRIXID).Distinct().ToList())
                        {
                            for (int i = 1; i <= 30; i++)
                            {
                                if (resultfishGoals.Where(x => x.REFDATE.Date == new DateTime(yearR, 9, i).Date && x.PROVIDER_DENTRIXID == id).Count() == 0)
                                {
                                    smartNumbersData = new FishGoals();
                                    smartNumbersData.PROVIDER_DENTRIXID = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().PROVIDER_DENTRIXID;
                                    smartNumbersData.ACTUAL_PRODUCTION = 0;
                                    smartNumbersData.FIRSTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().FIRSTNAME;
                                    smartNumbersData.GOAL_PRODUCTION = 0;
                                    smartNumbersData.HOURS_FOR_THEDAY = 0;
                                    smartNumbersData.HYGEINE_EXAMS = 0;
                                    smartNumbersData.LASTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().LASTNAME;
                                    smartNumbersData.NP_APPOINTMENTS = 0;
                                    smartNumbersData.Month = i;
                                    smartNumbersData.PERHOUR_PRODUCTION = 0;
                                    smartNumbersData.REFDATE = new DateTime(yearR, 9, i);
                                    smartNumbersData.ROUTINE_HYEGEINE_VISIT = 0;
                                    smartNumbersData.SCHEDULED_PRODUCTION = 0;
                                    smartNumbersData.SRP = 0;
                                    smartNumbersData.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                                    //smartNumbersData.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                                    fishGoals.Add(smartNumbersData);
                                }
                            }
                        }
                    }
                    return View(fishGoals);
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - SeptemberGoals", ex.Message, ex.StackTrace);
                return View();
            }
        }
        public ActionResult OctoberGoals(int? year)
        {
            try
            {
                //  if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["OfficeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {
                    List<FishGoals> resultfishGoals = new List<FishGoals>();
                    List<FishGoals> fishGoals = new List<FishGoals>();
                    int yearR = 0;
                    if (year == 0 || year == null)
                    {
                        year = DateTime.Now.Year;

                    }
                    yearR = (int)year;
                    resultfishGoals = data.GetFishGoals(10, yearR, Convert.ToInt32(Session["LocationID"]));
                    fishGoals = resultfishGoals;
                    ViewBag.Year = yearR;
                    FishGoals smartNumbersData;
                    if (resultfishGoals.Count != 0)
                    {
                        foreach (string id in resultfishGoals.Select(x => x.PROVIDER_DENTRIXID).Distinct().ToList())
                        {
                            for (int i = 1; i <= 31; i++)
                            {
                                if (resultfishGoals.Where(x => x.REFDATE.Date == new DateTime(yearR, 10, i).Date && x.PROVIDER_DENTRIXID == id).Count() == 0)
                                {
                                    smartNumbersData = new FishGoals();
                                    smartNumbersData.PROVIDER_DENTRIXID = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().PROVIDER_DENTRIXID;
                                    smartNumbersData.ACTUAL_PRODUCTION = 0;
                                    smartNumbersData.FIRSTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().FIRSTNAME;
                                    smartNumbersData.GOAL_PRODUCTION = 0;
                                    smartNumbersData.HOURS_FOR_THEDAY = 0;
                                    smartNumbersData.HYGEINE_EXAMS = 0;
                                    smartNumbersData.LASTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().LASTNAME;
                                    smartNumbersData.NP_APPOINTMENTS = 0;
                                    smartNumbersData.Month = i;
                                    smartNumbersData.PERHOUR_PRODUCTION = 0;
                                    smartNumbersData.REFDATE = new DateTime(yearR, 10, i);
                                    smartNumbersData.ROUTINE_HYEGEINE_VISIT = 0;
                                    smartNumbersData.SCHEDULED_PRODUCTION = 0;
                                    smartNumbersData.SRP = 0;
                                    smartNumbersData.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                                    // smartNumbersData.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                                    fishGoals.Add(smartNumbersData);
                                }
                            }
                        }
                    }
                    return View(fishGoals);
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - OctoberGoals", ex.Message, ex.StackTrace);
                return View();
            }
        }
        public ActionResult NovemberGoals(int? year)
        {
            try
            {
                // if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["OfficeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {
                    List<FishGoals> resultfishGoals = new List<FishGoals>();
                    List<FishGoals> fishGoals = new List<FishGoals>();
                    int yearR = 0;
                    if (year == 0 || year == null)
                    {
                        year = DateTime.Now.Year;

                    }
                    yearR = (int)year;
                    resultfishGoals = data.GetFishGoals(11, yearR, Convert.ToInt32(Session["LocationID"]));
                    fishGoals = resultfishGoals;
                    ViewBag.Year = yearR;
                    FishGoals smartNumbersData;
                    if (resultfishGoals.Count != 0)
                    {
                        foreach (string id in resultfishGoals.Select(x => x.PROVIDER_DENTRIXID).Distinct().ToList())
                        {
                            for (int i = 1; i <= 30; i++)
                            {
                                if (resultfishGoals.Where(x => x.REFDATE.Date == new DateTime(yearR, 11, i).Date && x.PROVIDER_DENTRIXID == id).Count() == 0)
                                {
                                    smartNumbersData = new FishGoals();
                                    smartNumbersData.PROVIDER_DENTRIXID = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().PROVIDER_DENTRIXID;
                                    smartNumbersData.ACTUAL_PRODUCTION = 0;
                                    smartNumbersData.FIRSTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().FIRSTNAME;
                                    smartNumbersData.GOAL_PRODUCTION = 0;
                                    smartNumbersData.HOURS_FOR_THEDAY = 0;
                                    smartNumbersData.HYGEINE_EXAMS = 0;
                                    smartNumbersData.LASTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().LASTNAME;
                                    smartNumbersData.NP_APPOINTMENTS = 0;
                                    smartNumbersData.Month = i;
                                    smartNumbersData.PERHOUR_PRODUCTION = 0;
                                    smartNumbersData.REFDATE = new DateTime(yearR, 11, i);
                                    smartNumbersData.ROUTINE_HYEGEINE_VISIT = 0;
                                    smartNumbersData.SCHEDULED_PRODUCTION = 0;
                                    smartNumbersData.SRP = 0;
                                    smartNumbersData.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                                    // smartNumbersData.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                                    fishGoals.Add(smartNumbersData);
                                }
                            }
                        }
                    }
                    return View(fishGoals);
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - NovemberGoals", ex.Message, ex.StackTrace);
                return View();
            }
        }
        public ActionResult DecemberGoals(int? year)
        {
            try
            {
                // if (!string.IsNullOrEmpty(Convert.ToString(Session["UserName"])) && !string.IsNullOrEmpty(Convert.ToString(Session["OfficeID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])) && !string.IsNullOrEmpty(Convert.ToString(Session["UserID"])))
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LocationID"])))
                {
                    List<FishGoals> resultfishGoals = new List<FishGoals>();
                    List<FishGoals> fishGoals = new List<FishGoals>();
                    int yearR = 0;
                    if (year == 0 || year == null)
                    {
                        year = DateTime.Now.Year;

                    }
                    yearR = (int)year;
                    resultfishGoals = data.GetFishGoals(12, yearR, Convert.ToInt32(Session["LocationID"]));
                    fishGoals = resultfishGoals;
                    ViewBag.Year = yearR;
                    FishGoals smartNumbersData;
                    if (resultfishGoals.Count != 0)
                    {
                        foreach (string id in resultfishGoals.Select(x => x.PROVIDER_DENTRIXID).Distinct().ToList())
                        {
                            for (int i = 1; i <= 31; i++)
                            {
                                if (resultfishGoals.Where(x => x.REFDATE.Date == new DateTime(yearR, 12, i).Date && x.PROVIDER_DENTRIXID == id).Count() == 0)
                                {
                                    smartNumbersData = new FishGoals();
                                    smartNumbersData.PROVIDER_DENTRIXID = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().PROVIDER_DENTRIXID;
                                    smartNumbersData.ACTUAL_PRODUCTION = 0;
                                    smartNumbersData.FIRSTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().FIRSTNAME;
                                    smartNumbersData.GOAL_PRODUCTION = 0;
                                    smartNumbersData.HOURS_FOR_THEDAY = 0;
                                    smartNumbersData.HYGEINE_EXAMS = 0;
                                    smartNumbersData.LASTNAME = resultfishGoals.Where(x => x.PROVIDER_DENTRIXID == id).FirstOrDefault().LASTNAME;
                                    smartNumbersData.NP_APPOINTMENTS = 0;
                                    smartNumbersData.Month = i;
                                    smartNumbersData.PERHOUR_PRODUCTION = 0;
                                    smartNumbersData.REFDATE = new DateTime(yearR, 12, i);
                                    smartNumbersData.ROUTINE_HYEGEINE_VISIT = 0;
                                    smartNumbersData.SCHEDULED_PRODUCTION = 0;
                                    smartNumbersData.SRP = 0;
                                    smartNumbersData.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                                    // smartNumbersData.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                                    fishGoals.Add(smartNumbersData);
                                }
                            }
                        }
                    }
                    return View(fishGoals);
                }
                else
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - DecemberGoals", ex.Message, ex.StackTrace);
                return View();
            }
        }
        [HttpPost]
        //Changes By Bhupesh
        public JsonResult ScheduleOfficeHours(string ScheduleStartDay, string ScheduleEndDay, string StartTime, string EndTime)
        {
            try
            {
                //DateTime startDay = Convert.ToDateTime(ScheduleStartDay);
                //DateTime endDay = Convert.ToDateTime(ScheduleEndDay);
                DateTime startDay = DateTime.ParseExact(ScheduleStartDay, "MM/dd/yyyy", null);                DateTime endDay = DateTime.ParseExact(ScheduleEndDay, "MM/dd/yyyy", null);
                TimeSpan ts = DateTime.Parse(StartTime).TimeOfDay;
                TimeSpan te = DateTime.Parse(EndTime).TimeOfDay;

                DateTime startTime;
                DateTime endTime;
                string result = string.Empty;
                bool returnResult = false;

                while (startDay <= endDay)
                {

                    startTime = startDay.Add(ts);
                    endTime = startDay.Add(te);
                    returnResult = data.SubmitOfficeHours(startDay, startTime, endTime, Convert.ToInt32(Session["LocationID"]));
                    if (!returnResult)
                    {
                        result += "Could not save: " + startDay + " Hours.";
                        result += Environment.NewLine;
                    }
                    startDay = startDay.AddDays(1);
                }

                if (string.IsNullOrEmpty(result))
                {
                    return Json("Successfully Saved!", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - OfficeHours", ex.Message, ex.StackTrace);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ProviderOfficeHours(string ScheduleStartDay, string ScheduleEndDay, string StartTime, string EndTime, string MemberID, string ProdPerHour)
        {
            try
            {
                //DateTime startDay = Convert.ToDateTime(ScheduleStartDay);
                //DateTime endDay = Convert.ToDateTime(ScheduleEndDay);
                DateTime startDay = DateTime.ParseExact(ScheduleStartDay, "MM/dd/yyyy", null);                DateTime endDay = DateTime.ParseExact(ScheduleEndDay, "MM/dd/yyyy", null);
                TimeSpan ts = DateTime.Parse(StartTime).TimeOfDay;
                TimeSpan te = DateTime.Parse(EndTime).TimeOfDay;

                DateTime startTime;
                DateTime endTime;
                string result = string.Empty;
                bool returnResult = false;
                var ss = Session["LocationID"].ToString();

                while (startDay <= endDay)
                {

                    startTime = startDay.Add(ts);
                    endTime = startDay.Add(te);
                    returnResult = data.SubmitProviderOfficeHours(startDay, startTime, endTime, MemberID, Convert.ToInt32(ProdPerHour),Convert.ToInt32(ss));
                    if (!returnResult)
                    {
                        result += "Could not save: " + startDay + " Hours.";
                        result += Environment.NewLine;
                    }
                    startDay = startDay.AddDays(1);
                }

                if (string.IsNullOrEmpty(result))
                {
                    return Json("Successfully Saved!", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(result, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - ProviderOfficeHours", ex.Message, ex.StackTrace);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult PracticeProfile(List<PracticeProfile> practiceProfiles)
        {
            try
            {
                bool result = false;

                if (ModelState.IsValid)
                {
                    foreach (PracticeProfile profile in practiceProfiles)
                    {
                        profile.LOCATION_ID = Convert.ToInt32(Session["LocationID"]);
                        // profile.OFFICE_ID = Convert.ToInt32(Session["OfficeID"]);
                        result = data.SubmitPracticeProfile(profile);
                    }
                    if (!result)
                    {
                        ModelState.AddModelError("Error", "Could not save some data.");
                        return Json("Could not save some data.", JsonRequestBehavior.AllowGet);

                    }
                    else
                    {
                        return Json("Data Saved Successfully.", JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json("Could not save some data.", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - PracticeProfile POST", ex.Message, ex.StackTrace);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public FileResult Export(string GridHtml)
        {
            return File(Encoding.ASCII.GetBytes(GridHtml), "application/vnd.ms-excel", "Report.xls");
        }

        public JsonResult UpdateFormStatus(int displayID, int pageId, int locationID)
        {
            try
            {
                locationID = Convert.ToInt32(Session["LocationID"]);
                // Fetching the Data From DB
                string _listDisplayId = data.UpdateFormsStatus(displayID, pageId, Convert.ToInt32(Session["LocationID"]));
                return Json(_listDisplayId, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("Home - UpdateFormStatus POST", ex.Message, ex.StackTrace);
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

    }
}

