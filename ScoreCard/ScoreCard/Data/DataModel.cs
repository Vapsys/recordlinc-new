﻿using DataAccessLayer.Common;
using ScoreCard.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ScoreCard.Data
{
    public class DataModel
    {
        public string connectionString = ConfigurationManager.ConnectionStrings["ReportConnectionString"].ConnectionString;
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        public List<FishGoals> GetFishGoals(int month, int year,int locationID)
        {
            try
            {
                List<FishGoals> fishGoals = new List<FishGoals>();
                FishGoals goals;

                using (SqlConnection myConnection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("GetFishGoals", myConnection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@Month", month));
                        cmd.Parameters.Add(new SqlParameter("@YEAR", year));
                        //cmd.Parameters.Add(new SqlParameter("@OfficeID", officeID));
                        cmd.Parameters.Add(new SqlParameter("@LocationID", locationID));
                        myConnection.Open();

                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                goals = new FishGoals();
                                goals.PROVIDER_DENTRIXID = Convert.ToString(dr["PROVIDER_DENTRIXID"]);
                                goals.LOCATION_ID = dr["ACCOUNT_ID"] == DBNull.Value ? 0 :  dr["ACCOUNT_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ACCOUNT_ID"]); 
                                //goals.OFFICE_ID = dr["OFFICE_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OFFICE_ID"]); 
                                goals.FIRSTNAME = Convert.ToString(dr["FIRSTNAME"]);
                                goals.LASTNAME = Convert.ToString(dr["LASTNAME"]);
                                goals.HOURS_FOR_THEDAY =dr["HOURS_FOR_THEDAY"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HOURS_FOR_THEDAY"]);
                                goals.GOAL_PRODUCTION = dr["GOAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GOAL_PRODUCTION"]);
                                goals.SCHEDULED_PRODUCTION = dr["SCHEDULED_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SCHEDULED_PRODUCTION"]);
                                goals.ACTUAL_PRODUCTION = dr["ACTUAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ACTUAL_PRODUCTION"]);
                                goals.HYGEINE_EXAMS = dr["HYGEINE_EXAMS"] == DBNull.Value ? 0 : dr["HYGEINE_EXAMS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HYGEINE_EXAMS"]);
                                goals.NP_APPOINTMENTS = dr["NP_APPOINTMENTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NP_APPOINTMENTS"]);
                                goals.ROUTINE_HYEGEINE_VISIT = dr["ROUTINE_HYEGEINE_VISIT"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ROUTINE_HYEGEINE_VISIT"]);
                                goals.SRP = dr["SRP"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SRP"]);
                                goals.REFDATE = Convert.ToDateTime(dr["REFDATE"]);
                                goals.TYPEOF_PROVIDER = Convert.ToString(dr["TYPEOF_PROVIDER"]);
                                goals.PERHOUR_PRODUCTION = dr["PERHOUR_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["PERHOUR_PRODUCTION"]); 
                                fishGoals.Add(goals);
                            }
                        }
                    }
                }
                return fishGoals;
            }
            catch(Exception ex)
            {
                ErrorLog.InsertErrorLog("DataModel - GetFishGoals", ex.Message, ex.StackTrace);
                return null;
            }
        }

        public SmartNumbersList GetSmartNumbers(string FromDate, string Todate, int locationID)
        {
            try
            {
                List<FishGoals> fishGoals = new List<FishGoals>();
                List<SmartNumbers> smartNumbers = new List<SmartNumbers>();
                SmartNumbersList smartNumbersList = new SmartNumbersList();
                FishGoals goals;
                SmartNumbers smart;

                using (SqlConnection myConnection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("GetSmartNumbers", myConnection))
                    {
                        if (myConnection.State == ConnectionState.Open)
                        {
                            myConnection.Close();
                        }
                        myConnection.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@FromDate", FromDate));
                        cmd.Parameters.Add(new SqlParameter("@ToDate", Todate));                        cmd.Parameters.Add(new SqlParameter("@LocationID", locationID));                        cmd.Parameters.Add(new SqlParameter("@Type", "DateWise"));

                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                smart = new SmartNumbers();
                                smart.ACCOUNT_ID =  dr["ACCOUNT_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ACCOUNT_ID"]); 
                               // smart.OFFICE_ID = dr["OFFICE_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OFFICE_ID"]); 
                                smart.TOTAL_OFFICE_HOURS = dr["TOTAL_OFFICE_HOURS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["TOTAL_OFFICE_HOURS"]); 
                                smart.HYGEINE_VISITS = dr["HYGEINE_VISITS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HYGEINE_VISITS"]); 
                                smart.NEW_PATIENTS = dr["NEW_PATIENTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NEW_PATIENTS"]); 
                                smart.MARKETING_COSTS = dr["MARKETING_COSTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["MARKETING_COSTS"]); 
                                smart.COLLECTIONS = dr["COLLECTIONS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["COLLECTIONS"]); 
                                smart.PRODUCTION = dr["PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["PRODUCTION"]);
                                smart.COLLECTIBLE_PRODUCTION = dr["COLLECTIBLE_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["COLLECTIBLE_PRODUCTION"]); 
                                smart.HYGEINE_PRODUCTION = dr["HYGEINE_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HYGEINE_PRODUCTION"]); 
                                smart.RESTORATIVE_PRODUCTION = dr["RESTORATIVE_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["RESTORATIVE_PRODUCTION"]);
                                smart.Month = dr["Month"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Month"]); 
                                smart.MonthName = Convert.ToString(dr["MonthName"]);
                                smartNumbers.Add(smart);
                            }

                            dr.NextResult();

                            while (dr.Read())
                            {
                                goals = new FishGoals();
                                goals.PROVIDER_DENTRIXID = Convert.ToString(dr["PROVIDER_DENTRIXID"]);
                               // goals.ACCOUNT_ID = dr["PROVIDER_DENTRIXID"] == DBNull.Value ? 0 :  dr["PROVIDER_DENTRIXID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["PROVIDER_DENTRIXID"]);  
                             //   goals.OFFICE_ID = dr["OFFICE_ID"] == DBNull.Value ? 0 : dr["OFFICE_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OFFICE_ID"]);  
                                goals.FIRSTNAME = Convert.ToString(dr["FIRSTNAME"]);
                                goals.LASTNAME = Convert.ToString(dr["LASTNAME"]);
                                goals.HOURS_FOR_THEDAY = dr["HOURS_FOR_THEDAY"] == DBNull.Value ? 0 :dr["HOURS_FOR_THEDAY"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HOURS_FOR_THEDAY"]);
                                goals.GOAL_PRODUCTION = dr["GOAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GOAL_PRODUCTION"]); 
                                goals.SCHEDULED_PRODUCTION = dr["SCHEDULED_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SCHEDULED_PRODUCTION"]); 
                                goals.ACTUAL_PRODUCTION = dr["ACTUAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ACTUAL_PRODUCTION"]);
                                goals.HYGEINE_EXAMS = dr["HYGEINE_EXAMS"] == DBNull.Value ? 0 :  dr["HYGEINE_EXAMS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HYGEINE_EXAMS"]);  
                                goals.NP_APPOINTMENTS = dr["NP_APPOINTMENTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NP_APPOINTMENTS"]); 
                                goals.ROUTINE_HYEGEINE_VISIT = Convert.ToInt32(dr["ROUTINE_HYEGEINE_VISIT"]);
                                goals.SRP = dr["SRP"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SRP"]); 
                                goals.Month = dr["Month"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Month"]); 
                                goals.MonthName = Convert.ToString(dr["MonthName"]);
                                goals.TYPEOF_PROVIDER = Convert.ToString(dr["TYPEOF_PROVIDER"]);
                                fishGoals.Add(goals);
                            }
                        }
                    }
                }
                smartNumbersList.FishGoalsData = fishGoals;
                smartNumbersList.SmartNumbersData = smartNumbers;
                return smartNumbersList;
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("DataModel - GetSmartNumbers", ex.Message, ex.StackTrace);
                return null;
            }
        }

        public CriticalNumbersList GetCriticalNumbers(string FromDate, string Todate, int locationID)
        {
            try
            {
                List<IndividualGoals> fishGoals = new List<IndividualGoals>();
                List<CriticalNumbers> criticalNumbers = new List<CriticalNumbers>();
                CriticalNumbersList criticalNumbersList = new CriticalNumbersList();
                IndividualGoals goals;
                CriticalNumbers critical;

                using (SqlConnection myConnection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("GetCriticalNumbersReport", myConnection))
                    {
                        if (myConnection.State == ConnectionState.Open)
                        {
                            myConnection.Close();
                        }
                        myConnection.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@FromDate", FromDate));
                        cmd.Parameters.Add(new SqlParameter("@ToDate", Todate));                        cmd.Parameters.Add(new SqlParameter("@LocationID", locationID));                        cmd.Parameters.Add(new SqlParameter("@Type", "DateWise"));

                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                critical = new CriticalNumbers();
                                critical.ACCOUNT_ID =  dr["ACCOUNT_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ACCOUNT_ID"]); 
                               // critical.OFFICE_ID = dr["OFFICE_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OFFICE_ID"]); 
                                critical.Collection_Ratio = dr["Collection_Ratio"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Collection_Ratio"]);
                                critical.TotalProduction_NewPatient = dr["TotalProduction_NewPatient"] == DBNull.Value ? 0 : Convert.ToInt32(dr["TotalProduction_NewPatient"]);
                                critical.CollectibleProduction_HygeineVisit = dr["CollectibleProduction_HygeineVisit"] == DBNull.Value ? 0 : Convert.ToInt32(dr["CollectibleProduction_HygeineVisit"]);
                                critical.TotalRestorativeProduction_HygeineVisit = dr["TotalRestorativeProduction_HygeineVisit"] == DBNull.Value ? 0 : Convert.ToInt32(dr["TotalRestorativeProduction_HygeineVisit"]);
                                critical.MarketingCosts_NewPatient = dr["MarketingCosts_NewPatient"] == DBNull.Value ? 0 : Convert.ToInt32(dr["MarketingCosts_NewPatient"]);
                                critical.Marketing_ROI = dr["Marketing_ROI"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Marketing_ROI"]);
                                critical.Production_Hour = dr["Production_Hour"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Production_Hour"]);
                                critical.Production_Operatory = dr["Production_Operatory"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Production_Operatory"]);
                                critical.RestorativeProduction_Operatory = dr["RestorativeProduction_Operatory"] == DBNull.Value ? 0 : Convert.ToInt32(dr["RestorativeProduction_Operatory"]);
                                critical.HygeineProduction_Operatory = dr["HygeineProduction_Operatory"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HygeineProduction_Operatory"]);
                                critical.Production_NonProdEmployee = dr["Production_NonProdEmployee"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Production_NonProdEmployee"]);
                                critical.Production_NonDoc = dr["Production_NonDoc"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Production_NonDoc"]);
                                critical.Month = dr["Month"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Month"]);
                                critical.MonthName = Convert.ToString(dr["MonthName"]);
                                criticalNumbers.Add(critical);
                            }

                            dr.NextResult();

                            while (dr.Read())
                            {
                                goals = new IndividualGoals();
                                goals.PROVIDER_DENTRIXID = Convert.ToString(dr["PROVIDER_DENTRIXID"]);
                           //     goals.LOCATION_ID =  dr["LOCATION_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["LOCATION_ID"]); 
                               // goals.OFFICE_ID = dr["OFFICE_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OFFICE_ID"]); 
                                goals.FIRSTNAME = Convert.ToString(dr["FIRSTNAME"]);
                                goals.LASTNAME = Convert.ToString(dr["LASTNAME"]);
                                goals.Production_Hour = dr["Production_Hour"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Production_Hour"]);
                                goals.Production_HygieneExam = dr["Production_HygieneExam"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Production_HygieneExam"]);
                                goals.ActualToGoal = dr["ActualToGoal"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ActualToGoal"]);
                                goals.HygienistProduction_Hour = dr["HygienistProduction_Hour"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HygienistProduction_Hour"]);
                                goals.HYGEINE_EXAM_Hour = dr["HYGEINE_EXAM_Hour"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HYGEINE_EXAM_Hour"]);
                                goals.Production_NewPatient = dr["Production_NewPatient"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Production_NewPatient"]);
                                goals.Month = dr["Month"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Month"]);
                                goals.MonthName = Convert.ToString(dr["MonthName"]);
                                goals.TYPEOF_PROVIDER = Convert.ToString(dr["TYPEOF_PROVIDER"]);
                                fishGoals.Add(goals);
                            }
                        }
                    }
                }
                criticalNumbersList.IndividualGoalsData = fishGoals;
                criticalNumbersList.CriticalNumbersData = criticalNumbers;
                return criticalNumbersList;
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("DataModel - GetCriticalNumbers", ex.Message, ex.StackTrace);
                return null;
            }
        }

        public List<PracticeProfile> GetPracticeProfile(string FromDate, string Todate,  int locationID)
        {
            try
            {
                List<PracticeProfile> practiceProfiles = new List<PracticeProfile>();
                PracticeProfile goals;

                using (SqlConnection myConnection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("GetPracticeProfile", myConnection))
                    {
                        if(myConnection.State == ConnectionState.Open)
                        {
                            myConnection.Close();
                        }
                        myConnection.Open();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@FromDate", FromDate));
                        cmd.Parameters.Add(new SqlParameter("@ToDate", Todate));                        cmd.Parameters.Add(new SqlParameter("@LocationID", locationID));                        cmd.Parameters.Add(new SqlParameter("@Type", "DateWise"));

                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                goals = new PracticeProfile();
                                goals.ACCOUNT_ID =  dr["ACCOUNT_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ACCOUNT_ID"]); 
                              //  goals.OFFICE_ID = dr["OFFICE_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OFFICE_ID"]); 
                                goals.TOTAL_OPERATORIES = dr["TOTAL_OPERATORIES"] == DBNull.Value ? 0 : Convert.ToInt32(dr["TOTAL_OPERATORIES"]);
                                goals.RESTORATIVE_OPERATORIES = dr["RESTORATIVE_OPERATORIES"] == DBNull.Value ? 0 : Convert.ToInt32(dr["RESTORATIVE_OPERATORIES"]);
                                goals.HYGIENE_OPERATORIES = dr["HYGIENE_OPERATORIES"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HYGIENE_OPERATORIES"]);
                                goals.TOTAL_DOCTORS = dr["TOTAL_DOCTORS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["TOTAL_DOCTORS"]);
                                goals.Marketing_Costs = dr["Marketing_Costs"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Marketing_Costs"]);
                                goals.Month = dr["Month"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Month"]);
                                goals.MonthName = Convert.ToString(dr["MonthName"]);
                                goals.Year = Convert.ToInt32(dr["YEAR"]);
                                practiceProfiles.Add(goals);
                            }
                        }
                    }
                }
                return practiceProfiles;
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("DataModel - GetFishGoals", ex.Message, ex.StackTrace);
                return null;
            }
        }

        public bool SubmitOfficeHours(DateTime ScheduleDay, DateTime StartTime, DateTime EndTime, int LocationID)
        {
            try
            {
                string result = string.Empty;

                using (SqlConnection myConnection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("FillOfficeHours", myConnection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@ScheduleDay", ScheduleDay));
                        cmd.Parameters.Add(new SqlParameter("@StartHour", StartTime));
                        cmd.Parameters.Add(new SqlParameter("@EndHour", EndTime));
                        cmd.Parameters.Add(new SqlParameter("@ACCOUNT_ID", LocationID));
                        //cmd.Parameters.Add(new SqlParameter("@OfficeID", OfficeID));
                        //cmd.Parameters.Add(new SqlParameter("@UserID", UserID));
                        myConnection.Open();
                        int value = Convert.ToInt32(cmd.ExecuteScalar());
                        if (value > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("DataModel - SubmitOfficeHours", ex.Message, ex.StackTrace);
                return false;
            }
        }

        public bool SubmitProviderOfficeHours(DateTime ScheduleDay, DateTime StartTime, DateTime EndTime, string MemberID, int ProdPerHour,int location_id)
        {
            try
            {
                string result = string.Empty;

                using (SqlConnection myConnection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("FillProviderOfficeHours", myConnection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@ScheduleDay", ScheduleDay));
                        cmd.Parameters.Add(new SqlParameter("@StartHour", StartTime));
                        cmd.Parameters.Add(new SqlParameter("@EndHour", EndTime));
                        // cmd.Parameters.Add(new SqlParameter("@UserID", UserID));
                        cmd.Parameters.Add(new SqlParameter("@MemberName", MemberID));
                        cmd.Parameters.Add(new SqlParameter("@ProdPerHour", ProdPerHour));
                        cmd.Parameters.Add(new SqlParameter("@ACCOUNT_ID", location_id));
                        myConnection.Open();
                        int value = Convert.ToInt32(cmd.ExecuteScalar());
                        if (value > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("DataModel - SubmitProviderOfficeHours", ex.Message, ex.StackTrace);
                return false;
            }
        }

        public bool SubmitPracticeProfile(PracticeProfile profile)
        {
            try
            {
                string result = string.Empty;

                using (SqlConnection myConnection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SavePracticeProfile", myConnection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@TOTAL_OPERATORIES", profile.TOTAL_OPERATORIES));
                        cmd.Parameters.Add(new SqlParameter("@RESTORATIVE_OPERATORIES", profile.RESTORATIVE_OPERATORIES));
                        cmd.Parameters.Add(new SqlParameter("@HYGIENE_OPERATORIES", profile.HYGIENE_OPERATORIES));
                        cmd.Parameters.Add(new SqlParameter("@TOTAL_DOCTORS", profile.TOTAL_DOCTORS));
                        cmd.Parameters.Add(new SqlParameter("@MARKETING_COSTS", profile.Marketing_Costs));
                        cmd.Parameters.Add(new SqlParameter("@MONTH", profile.Month));
                        cmd.Parameters.Add(new SqlParameter("@YEAR", profile.Year));
                        cmd.Parameters.Add(new SqlParameter("@LocationID", profile.LOCATION_ID));
                    //    cmd.Parameters.Add(new SqlParameter("@OfficeID", profile.OFFICE_ID));
                    //    cmd.Parameters.Add(new SqlParameter("@UserID", UserID));
                        myConnection.Open();
                        int value = Convert.ToInt32(cmd.ExecuteScalar());
                        if (value > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("DataModel - SubmitOfficeHours", ex.Message, ex.StackTrace);
                return false;
            }
        }

        public OfficeHours GetOfficeHours(string date, int locationID)
        {
            try
            {
                OfficeHours profile = new OfficeHours();

                using (SqlConnection myConnection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SELECT [OFFICE_STARTHOUR],[OFFICE_ENDHOUR] FROM [Reports_RecordLinc].[dbo].[OfficeHours] where [SCHEDULE_DATE] = @Date AND ACCOUNT_ID = @ACCOUNT_ID ", myConnection))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Add(new SqlParameter("@Date", date));
                       //  cmd.Parameters.Add(new SqlParameter("@OfficeID", officeID));
                        cmd.Parameters.Add(new SqlParameter("@ACCOUNT_ID", locationID));
                        myConnection.Open();

                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                profile.OFFICE_STARTHOUR = string.Format("{0:hh:mm:ss tt}", Convert.ToDateTime(dr["OFFICE_STARTHOUR"]));
                                profile.OFFICE_ENDHOUR = string.Format("{0:hh:mm:ss tt}", Convert.ToDateTime(dr["OFFICE_ENDHOUR"]));
                            }
                        }
                    }
                }
                return profile;
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("DataModel - GetOfficeHours", ex.Message, ex.StackTrace);
                return null;
            }
        }

        public UserDetails CheckLogin(string Username, string Password)
        {
            try
            {
                string decryptedText = ObjTripleDESCryptoHelper.encryptText(Password);                
                UserDetails userDetails = new UserDetails();

                using (SqlConnection myConnection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("SELECT [ID],[FirstName],[LastName],officeid,locationid  FROM [Reports_RecordLinc].[dbo].[UserLogin] where LoginUserName = '" + Username + "' and Password = '"+Password+"'", myConnection))
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.Add(new SqlParameter("@UserName", Username));
                        cmd.Parameters.Add(new SqlParameter("@Password", decryptedText));
                        myConnection.Open();

                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {                                
                                userDetails.ID = Convert.ToInt32(dr["ID"]);
                                userDetails.FirstName = Convert.ToString(dr["FirstName"]);
                                userDetails.LastName = Convert.ToString(dr["LastName"]);
                               // userDetails.OFFICE_ID = Convert.ToInt32(dr["officeid"]);
                                userDetails.LOCATION_ID = Convert.ToInt32(dr["locationid"]);
                            }
                        }
                    }
                }
                return userDetails;
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("DataModel - CheckLogin", ex.Message, ex.StackTrace);
                return null;
            }
        }
        
        public IndividualSummaryList GetIndividualSummary(int year, int locationID)        {            try            {                List<IndividualSummary> individualSummary = new List<IndividualSummary>();                IndividualSummaryList individualSummaryList = new IndividualSummaryList();                IndividualSummary individual;                FishGoals goals;                List<FishGoals> fishGoals = new List<FishGoals>();                using (SqlConnection myconnection = new SqlConnection(connectionString))                {                    using (SqlCommand cmd = new SqlCommand("GetIndividualSummaryReport", myconnection))                    {                        cmd.CommandType = CommandType.StoredProcedure;                        cmd.Parameters.Add(new SqlParameter("@YEAR", year));                       // cmd.Parameters.Add(new SqlParameter("@OfficeID", officeID));                        cmd.Parameters.Add(new SqlParameter("@LocationID", locationID));                        cmd.Parameters.Add(new SqlParameter("@Type", "List"));                        myconnection.Open();                        using (SqlDataReader dr = cmd.ExecuteReader())                        {                            while (dr.Read())                            {                                individual = new IndividualSummary();                                individual.LOCATION_ID = dr["ACCOUNT_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ACCOUNT_ID"]);                             //   individual.OFFICE_ID = dr["OFFICE_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OFFICE_ID"]);                                individual.GrossProduction = dr["GrossProduction"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GrossProduction"]);                                individual.OrthoProduction = dr["OrthoProduction"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OrthoProduction"]);                                individual.Adjustments = dr["Adjustments"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Adjustments"]);                                individual.AdjustmentPer = dr["AdjustmentPer"] == DBNull.Value ? 0 : Convert.ToInt32(dr["AdjustmentPer"]);                                individual.NetProduction = dr["NetProduction"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NetProduction"]);                                individual.NetProductionPerDay = dr["NetProductionPerDay"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NetProductionPerDay"]);                                individual.Collections = dr["Collections"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Collections"]);                                individual.NetCollections = dr["NetCollections"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NetCollections"]);                                individual.OrthoCollection = dr["OrthoCollection"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OrthoCollection"]);                                individual.CollectionPer = dr["CollectionPer"] == DBNull.Value ? 0 : Convert.ToInt32(dr["CollectionPer"]);                                individual.TotalAR = dr["TotalAR"] == DBNull.Value ? 0 : Convert.ToInt32(dr["TotalAR"]);                                individual.ARRatio = dr["ARRatio"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ARRatio"]);                                individual.OrthoAR = dr["OrthoAR"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OrthoAR"]);                                individual.SimplePayAR = dr["SimplePayAR"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SimplePayAR"]);                                individual.ARMinusOrthoSimplepay = dr["ARMinusOrthoSimplepay"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ARMinusOrthoSimplepay"]);                                individual.ARRatioMinusOrthoSimplepay = dr["ARRatioMinusOrthoSimplepay"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ARRatioMinusOrthoSimplepay"]);                                individual.NewPts = dr["NewPts"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NewPts"]);                                individual.PtReferrals = dr["PtReferrals"] == DBNull.Value ? 0 : Convert.ToInt32(dr["PtReferrals"]);                                individual.PerPtReferrals = dr["PerPtReferrals"] == DBNull.Value ? 0 : Convert.ToInt32(dr["PerPtReferrals"]);                                individual.DeactivatedPts = dr["DeactivatedPts"] == DBNull.Value ? 0 : Convert.ToInt32(dr["DeactivatedPts"]);                                individual.TotalPtCount = dr["TotalPtCount"] == DBNull.Value ? 0 : Convert.ToInt32(dr["TotalPtCount"]);                                individual.TotalPtsSeen = dr["TotalPtsSeen"] == DBNull.Value ? 0 : Convert.ToInt32(dr["TotalPtsSeen"]);                                individual.NetProductionPerPt = dr["NetProductionPerPt"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NetProductionPerPt"]);                                individual.ActualContCareVisits = dr["ActualContCareVisits"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ActualContCareVisits"]);                                individual.ContCareEfficiency = dr["ContCareEfficiency"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ContCareEfficiency"]);                                individual.Supplies = dr["Supplies"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Supplies"]);                                individual.SuppliesExpenseofGrossProduction = dr["SuppliesExpenseofGrossProduction"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SuppliesExpenseofGrossProduction"]);                                individual.LabExpense = dr["LabExpense"] == DBNull.Value ? 0 : Convert.ToInt32(dr["LabExpense"]);                                individual.LabExpenseofGrossProduction = dr["LabExpenseofGrossProduction"] == DBNull.Value ? 0 : Convert.ToInt32(dr["LabExpenseofGrossProduction"]);                                individual.Month = dr["Month"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Month"]);                                individual.MonthName = Convert.ToString(dr["MonthName"]);                                individualSummary.Add(individual);                            }                            dr.NextResult();                            //while (dr.Read())                            //{                            //    goals = new FishGoals();                            //    goals.PROVIDER_DENTRIXID = Convert.ToString(dr["PROVIDER_DENTRIXID"]);                            //    goals.LOCATION_ID = dr["LOCATION_ID"] == DBNull.Value ? 0 : dr["LOCATION_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["LOCATION_ID"]);
                            //    //    goals.OFFICE_ID = dr["OFFICE_ID"] == DBNull.Value ? 0 : dr["OFFICE_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OFFICE_ID"]);
                            //    goals.FIRSTNAME = Convert.ToString(dr["FIRSTNAME"]);                            //    goals.LASTNAME = Convert.ToString(dr["LASTNAME"]);                            //    goals.HOURS_FOR_THEDAY = dr["HOURS_FOR_THEDAY"] == DBNull.Value ? 0 : dr["HOURS_FOR_THEDAY"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HOURS_FOR_THEDAY"]);                            //    goals.GOAL_PRODUCTION = dr["GOAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GOAL_PRODUCTION"]);                            //    goals.SCHEDULED_PRODUCTION = dr["SCHEDULED_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SCHEDULED_PRODUCTION"]);                            //    goals.ACTUAL_PRODUCTION = dr["ACTUAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ACTUAL_PRODUCTION"]);                            //    goals.HYGEINE_EXAMS = dr["HYGEINE_EXAMS"] == DBNull.Value ? 0 : dr["HYGEINE_EXAMS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HYGEINE_EXAMS"]);                            //    goals.NP_APPOINTMENTS = dr["NP_APPOINTMENTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NP_APPOINTMENTS"]);                            //    goals.ROUTINE_HYEGEINE_VISIT = Convert.ToInt32(dr["ROUTINE_HYEGEINE_VISIT"]);                            //    goals.SRP = dr["SRP"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SRP"]);                            //    goals.Month = dr["Month"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Month"]);                            //    goals.MonthName = Convert.ToString(dr["MonthName"]);                            //    goals.TYPEOF_PROVIDER = Convert.ToString(dr["TYPEOF_PROVIDER"]);                            //    fishGoals.Add(goals);                            //}                        }                    }                }                individualSummaryList.IndividualSummaryData = individualSummary;                // individualSummaryList.FishGoalsData = fishGoals;                return individualSummaryList;            }            catch (Exception ex)
            {                ErrorLog.InsertErrorLog("DataModel - GetIndividualSummary", ex.Message, ex.StackTrace);                return null;            }        }
        public IndividualSummaryList GetIndividualSummaryDate(string FromDate, string Todate, int locationID)        {            try            {                List<IndividualSummary> individualSummary = new List<IndividualSummary>();                IndividualSummaryList individualSummaryList = new IndividualSummaryList();                IndividualSummary individual;                FishGoals goals;                List<FishGoals> fishGoals = new List<FishGoals>();                using (SqlConnection myconnection = new SqlConnection(connectionString))                {                    using (SqlCommand cmd = new SqlCommand("GetIndividualSummaryReport", myconnection))                    {                        cmd.CommandType = CommandType.StoredProcedure;                        cmd.Parameters.Add(new SqlParameter("@FromDate", FromDate));                        cmd.Parameters.Add(new SqlParameter("@ToDate", Todate));
                        // cmd.Parameters.Add(new SqlParameter("@OfficeID", officeID));
                        cmd.Parameters.Add(new SqlParameter("@LocationID", locationID));                        cmd.Parameters.Add(new SqlParameter("@Type", "DateWise"));                        if (myconnection.State == ConnectionState.Closed)                        {                            myconnection.Open();                        }
                        //myconnection.Open();
                        using (SqlDataReader dr = cmd.ExecuteReader())                        {                            while (dr.Read())                            {                                individual = new IndividualSummary();                                individual.LOCATION_ID = dr["ACCOUNT_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ACCOUNT_ID"]);
                                //   individual.OFFICE_ID = dr["OFFICE_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OFFICE_ID"]);
                                individual.GrossProduction = dr["GrossProduction"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GrossProduction"]);                                individual.OrthoProduction = dr["OrthoProduction"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OrthoProduction"]);                                individual.Adjustments = dr["Adjustments"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Adjustments"]);                                individual.AdjustmentPer = dr["AdjustmentPer"] == DBNull.Value ? 0 : Convert.ToInt32(dr["AdjustmentPer"]);                                individual.NetProduction = dr["NetProduction"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NetProduction"]);                                individual.NetProductionPerDay = dr["NetProductionPerDay"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NetProductionPerDay"]);                                individual.Collections = dr["Collections"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Collections"]);                                individual.NetCollections = dr["NetCollections"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NetCollections"]);                                individual.OrthoCollection = dr["OrthoCollection"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OrthoCollection"]);                                individual.CollectionPer = dr["CollectionPer"] == DBNull.Value ? 0 : Convert.ToInt32(dr["CollectionPer"]);                                individual.TotalAR = dr["TotalAR"] == DBNull.Value ? 0 : Convert.ToInt32(dr["TotalAR"]);                                individual.ARRatio = dr["ARRatio"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ARRatio"]);                                individual.OrthoAR = dr["OrthoAR"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OrthoAR"]);                                individual.SimplePayAR = dr["SimplePayAR"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SimplePayAR"]);                                individual.ARMinusOrthoSimplepay = dr["ARMinusOrthoSimplepay"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ARMinusOrthoSimplepay"]);                                individual.ARRatioMinusOrthoSimplepay = dr["ARRatioMinusOrthoSimplepay"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ARRatioMinusOrthoSimplepay"]);                                individual.NewPts = dr["NewPts"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NewPts"]);                                individual.PtReferrals = dr["PtReferrals"] == DBNull.Value ? 0 : Convert.ToInt32(dr["PtReferrals"]);                                individual.PerPtReferrals = dr["PerPtReferrals"] == DBNull.Value ? 0 : Convert.ToInt32(dr["PerPtReferrals"]);                                individual.DeactivatedPts = dr["DeactivatedPts"] == DBNull.Value ? 0 : Convert.ToInt32(dr["DeactivatedPts"]);                                individual.TotalPtCount = dr["TotalPtCount"] == DBNull.Value ? 0 : Convert.ToInt32(dr["TotalPtCount"]);                                individual.TotalPtsSeen = dr["TotalPtsSeen"] == DBNull.Value ? 0 : Convert.ToInt32(dr["TotalPtsSeen"]);                                individual.NetProductionPerPt = dr["NetProductionPerPt"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NetProductionPerPt"]);                                individual.ActualContCareVisits = dr["ActualContCareVisits"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ActualContCareVisits"]);                                individual.ContCareEfficiency = dr["ContCareEfficiency"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ContCareEfficiency"]);                                individual.Supplies = dr["Supplies"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Supplies"]);                                individual.SuppliesExpenseofGrossProduction = dr["SuppliesExpenseofGrossProduction"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SuppliesExpenseofGrossProduction"]);                                individual.LabExpense = dr["LabExpense"] == DBNull.Value ? 0 : Convert.ToInt32(dr["LabExpense"]);                                individual.LabExpenseofGrossProduction = dr["LabExpenseofGrossProduction"] == DBNull.Value ? 0 : Convert.ToInt32(dr["LabExpenseofGrossProduction"]);                                individual.Month = dr["Month"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Month"]);                                individual.MonthName = Convert.ToString(dr["MonthName"]);                                individualSummary.Add(individual);                            }                            dr.NextResult();
                            //while (dr.Read())
                            //{
                            //    goals = new FishGoals();
                            //    goals.PROVIDER_DENTRIXID = Convert.ToString(dr["PROVIDER_DENTRIXID"]);
                            //    goals.LOCATION_ID = dr["LOCATION_ID"] == DBNull.Value ? 0 : dr["LOCATION_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["LOCATION_ID"]);
                            //    //    goals.OFFICE_ID = dr["OFFICE_ID"] == DBNull.Value ? 0 : dr["OFFICE_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OFFICE_ID"]);
                            //    goals.FIRSTNAME = Convert.ToString(dr["FIRSTNAME"]);
                            //    goals.LASTNAME = Convert.ToString(dr["LASTNAME"]);
                            //    goals.HOURS_FOR_THEDAY = dr["HOURS_FOR_THEDAY"] == DBNull.Value ? 0 : dr["HOURS_FOR_THEDAY"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HOURS_FOR_THEDAY"]);
                            //    goals.GOAL_PRODUCTION = dr["GOAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GOAL_PRODUCTION"]);
                            //    goals.SCHEDULED_PRODUCTION = dr["SCHEDULED_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SCHEDULED_PRODUCTION"]);
                            //    goals.ACTUAL_PRODUCTION = dr["ACTUAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ACTUAL_PRODUCTION"]);
                            //    goals.HYGEINE_EXAMS = dr["HYGEINE_EXAMS"] == DBNull.Value ? 0 : dr["HYGEINE_EXAMS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HYGEINE_EXAMS"]);
                            //    goals.NP_APPOINTMENTS = dr["NP_APPOINTMENTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NP_APPOINTMENTS"]);
                            //    goals.ROUTINE_HYEGEINE_VISIT = Convert.ToInt32(dr["ROUTINE_HYEGEINE_VISIT"]);
                            //    goals.SRP = dr["SRP"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SRP"]);
                            //    goals.Month = dr["Month"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Month"]);
                            //    goals.MonthName = Convert.ToString(dr["MonthName"]);
                            //    goals.TYPEOF_PROVIDER = Convert.ToString(dr["TYPEOF_PROVIDER"]);
                            //    fishGoals.Add(goals);
                            //}
                        }                    }                }                individualSummaryList.IndividualSummaryData = individualSummary;
                // individualSummaryList.FishGoalsData = fishGoals;
                return individualSummaryList;            }            catch (Exception ex)            {                ErrorLog.InsertErrorLog("DataModel - GetIndividualSummaryDate", ex.Message, ex.StackTrace);                return null;            }        }

        public StatsAllSummary_List GetStatsSummary(int year,int locationID)        {            try            {                List<Stats_All_Summary> statsAllSummary = new List<Stats_All_Summary>();                StatsAllSummary_List statsAllSummaryList = new StatsAllSummary_List();                Stats_All_Summary stats;                FishGoals goals;                List<FishGoals> fishGoals = new List<FishGoals>();                using (SqlConnection myconnection = new SqlConnection(connectionString))                {                    using (SqlCommand cmd = new SqlCommand("GetStatsSummaryReport", myconnection))                    {                        cmd.CommandType = CommandType.StoredProcedure;                        cmd.Parameters.Add(new SqlParameter("@Type", "List"));                        cmd.Parameters.Add(new SqlParameter("@YEAR", year));                        cmd.Parameters.Add(new SqlParameter("@LocationID", locationID));                        myconnection.Open();                        using (SqlDataReader dr = cmd.ExecuteReader())                        {                            while (dr.Read())                            {                                stats = new Stats_All_Summary();                                stats.LOCATION_ID = dr["ACCOUNT_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ACCOUNT_ID"]);                               // stats.OFFICE_ID = dr["OFFICE_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OFFICE_ID"]);                                stats.GROSS_PRODUCTION = dr["GROSS_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GROSS_PRODUCTION"]);                                stats.ORTHO_COLLECTION = dr["ORTHO_COLLECTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ORTHO_COLLECTION"]);                                stats.ADJUSTMENTS = dr["ADJUSTMENTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ADJUSTMENTS"]);                                stats.NET_PRODUCTION = dr["NET_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_PRODUCTION"]);                                stats.NET_PRODUCTION_PER_DAY = dr["NET_PRODUCTION_PER_DAY"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_PRODUCTION_PER_DAY"]);                                stats.NET_PRODUCTION_PER_PT_DAY = dr["NET_PRODUCTION_PER_PT_DAY"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_PRODUCTION_PER_PT_DAY"]);                                stats.COLLECTIONS = dr["COLLECTIONS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["COLLECTIONS"]);                                stats.REFUNDS_NSF = dr["REFUNDS_NSF"] == DBNull.Value ? 0 : Convert.ToInt32(dr["REFUNDS_NSF"]);                                stats.SALES_TAX = dr["SALES_TAX"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SALES_TAX"]);                                stats.NET_COLLECTIONS = dr["NET_COLLECTIONS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_COLLECTIONS"]);                                stats.ORTHO_COLLECTION = dr["ORTHO_COLLECTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ORTHO_COLLECTION"]);                                stats.COLLECTION_PERC = dr["COLLECTION_PERC"] == DBNull.Value ? 0 : Convert.ToInt32(dr["COLLECTION_PERC"]);                                stats.TotalAR = dr["TotalAR"] == DBNull.Value ? 0 : Convert.ToInt32(dr["TotalAR"]);                                stats.TOTAL_TAX_PLANNED = dr["TOTAL_TAX_PLANNED"] == DBNull.Value ? 0 : Convert.ToInt32(dr["TOTAL_TAX_PLANNED"]);                                stats.OPPORTUNITIES = dr["OPPORTUNITIES"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OPPORTUNITIES"]);                                stats.TOTAL_TAX_PLANNED_PER_OPPORUNITY = dr["TOTAL_TAX_PLANNED_PER_OPPORUNITY"] == DBNull.Value ? 0 : Convert.ToInt32(dr["TOTAL_TAX_PLANNED_PER_OPPORUNITY"]);                                stats.GOSS_HYG_PRODUCTION = dr["GOSS_HYG_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GOSS_HYG_PRODUCTION"]);                                stats.HYG_ADJUSTMENTS = dr["HYG_ADJUSTMENTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HYG_ADJUSTMENTS"]);                                stats.NET_HYG_PRODUCTION = dr["NET_HYG_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_HYG_PRODUCTION"]);                                stats.PER_NET_PRODUCTION_NET_HYG = dr["PER_NET_PRODUCTION_NET_HYG"] == DBNull.Value ? 0 : Convert.ToInt32(dr["PER_NET_PRODUCTION_NET_HYG"]);                                stats.GOSS_PERIO_PRODUCTION = dr["GOSS_PERIO_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GOSS_PERIO_PRODUCTION"]);                                stats.PER_GOSS_HYG_PRODUCTION_PERIO = dr["PER_GOSS_HYG_PRODUCTION_PERIO"] == DBNull.Value ? 0 : Convert.ToInt32(dr["PER_GOSS_HYG_PRODUCTION_PERIO"]);                                stats.NEW_PTS = dr["NEW_PTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NEW_PTS"]);                                stats.NEW_PTS_AMT_TAX = dr["NEW_PTS_AMT_TAX"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NEW_PTS_AMT_TAX"]);                                stats.NEW_PTS_AMT_COMPLETE_MONTHS = dr["NEW_PTS_AMT_COMPLETE_MONTHS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NEW_PTS_AMT_COMPLETE_MONTHS"]);                                stats.AVG_TAX_PER_NEWPTS = dr["AVG_TAX_PER_NEWPTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["AVG_TAX_PER_NEWPTS"]);                                stats.COMPELETE_RATIO = dr["COMPELETE_RATIO"] == DBNull.Value ? 0 : Convert.ToInt32(dr["COMPELETE_RATIO"]);                                stats.GENERAL_PRODUCTION = dr["GENERAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GENERAL_PRODUCTION"]);                                stats.ORHO_ADJUSTMENTS = dr["ORHO_ADJUSTMENTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ORHO_ADJUSTMENTS"]);                                stats.NET_ORTHO = dr["NET_ORTHO"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_ORTHO"]);                                stats.NEW_PTS_SEEN = dr["NEW_PTS_SEEN"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NEW_PTS_SEEN"]);                                stats.NET_ORTHO_PTS_SEEN = dr["NET_ORTHO_PTS_SEEN"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_ORTHO_PTS_SEEN"]);                                stats.ORTHO_PTS_SEEN = dr["ORTHO_PTS_SEEN"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ORTHO_PTS_SEEN"]);                                stats.AVG_GOSS_PRODUCTION_PER_PTS = dr["AVG_GOSS_PRODUCTION_PER_PTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["AVG_GOSS_PRODUCTION_PER_PTS"]);                                stats.AVG_GOSS_PRODUCTION_PER_DAY = dr["AVG_GOSS_PRODUCTION_PER_DAY"] == DBNull.Value ? 0 : Convert.ToInt32(dr["AVG_GOSS_PRODUCTION_PER_DAY"]);                                stats.CROWNS = dr["CROWNS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["CROWNS"]);                                stats.BUILDUPS = dr["BUILDUPS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["BUILDUPS"]);                                stats.PARTIALS = dr["PARTIALS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["PARTIALS"]);                                stats.PREC_ATTACHMENTS = dr["PREC_ATTACHMENTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["PREC_ATTACHMENTS"]);                                stats.IMPLANTS_PLACED = dr["IMPLANTS_PLACED"] == DBNull.Value ? 0 : Convert.ToInt32(dr["IMPLANTS_PLACED"]);                                stats.EXT = dr["EXT"] == DBNull.Value ? 0 : Convert.ToInt32(dr["EXT"]);                                stats.ORTHO_STARTS = dr["ORTHO_STARTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ORTHO_STARTS"]);                                stats.MONTH = dr["MONTH"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Month"]);                                stats.MONTHNAME = Convert.ToString(dr["MONTHNAME"]);                                statsAllSummary.Add(stats);                            }                            dr.NextResult();                            while (dr.Read())                            {                                goals = new FishGoals();                                goals.PROVIDER_DENTRIXID = Convert.ToString(dr["PROVIDER_DENTRIXID"]);                                goals.LOCATION_ID = dr["LOCATION_ID"] == DBNull.Value ? 0 : dr["LOCATION_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["LOCATION_ID"]);                               // goals.OFFICE_ID = dr["OFFICE_ID"] == DBNull.Value ? 0 : dr["OFFICE_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OFFICE_ID"]);                                goals.FIRSTNAME = Convert.ToString(dr["FIRSTNAME"]);                                goals.LASTNAME = Convert.ToString(dr["LASTNAME"]);                                goals.HOURS_FOR_THEDAY = dr["HOURS_FOR_THEDAY"] == DBNull.Value ? 0 : dr["HOURS_FOR_THEDAY"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HOURS_FOR_THEDAY"]);                                goals.GOAL_PRODUCTION = dr["GOAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GOAL_PRODUCTION"]);                                goals.SCHEDULED_PRODUCTION = dr["SCHEDULED_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SCHEDULED_PRODUCTION"]);                                goals.ACTUAL_PRODUCTION = dr["ACTUAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ACTUAL_PRODUCTION"]);                                goals.HYGEINE_EXAMS = dr["HYGEINE_EXAMS"] == DBNull.Value ? 0 : dr["HYGEINE_EXAMS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HYGEINE_EXAMS"]);                                goals.NP_APPOINTMENTS = dr["NP_APPOINTMENTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NP_APPOINTMENTS"]);                                goals.ROUTINE_HYEGEINE_VISIT = Convert.ToInt32(dr["ROUTINE_HYEGEINE_VISIT"]);                                goals.SRP = dr["SRP"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SRP"]);                                goals.Month = dr["Month"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Month"]);                                goals.MonthName = Convert.ToString(dr["MonthName"]);                                goals.TYPEOF_PROVIDER = Convert.ToString(dr["TYPEOF_PROVIDER"]);                                fishGoals.Add(goals);                            }                        }                    }                }                statsAllSummaryList.stats_all_summarys_ = statsAllSummary;                statsAllSummaryList.FishGoalsData = fishGoals;                return statsAllSummaryList;            }            catch (Exception ex)            {                ErrorLog.InsertErrorLog("DataModel - GetStatsSummary", ex.Message, ex.StackTrace);                return null;            }        }

        public StatsAllSummary_List GetStatsSummaryDate(string FromDate, string Todate, int locationID)        {            try            {                List<Stats_All_Summary> statsAllSummary = new List<Stats_All_Summary>();                StatsAllSummary_List statsAllSummaryList = new StatsAllSummary_List();                Stats_All_Summary stats;                FishGoals goals;                List<FishGoals> fishGoals = new List<FishGoals>();                using (SqlConnection myconnection = new SqlConnection(connectionString))                {                    if(myconnection.State == ConnectionState.Open)
                    {
                        myconnection.Close();
                    }                    myconnection.Open();                    using (SqlCommand cmd = new SqlCommand("GetStatsSummaryReport", myconnection))                    {                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@FromDate", FromDate));
                        cmd.Parameters.Add(new SqlParameter("@ToDate", Todate));                        cmd.Parameters.Add(new SqlParameter("@LocationID", locationID));                        cmd.Parameters.Add(new SqlParameter("@Type", "DateWise"));                        using (SqlDataReader dr = cmd.ExecuteReader())                        {                            while (dr.Read())                            {                                stats = new Stats_All_Summary();                                stats.LOCATION_ID = dr["ACCOUNT_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ACCOUNT_ID"]);
                                // stats.OFFICE_ID = dr["OFFICE_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OFFICE_ID"]);
                                stats.GROSS_PRODUCTION = dr["GROSS_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GROSS_PRODUCTION"]);                                stats.ORTHO_COLLECTION = dr["ORTHO_COLLECTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ORTHO_COLLECTION"]);                                stats.ADJUSTMENTS = dr["ADJUSTMENTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ADJUSTMENTS"]);                                stats.NET_PRODUCTION = dr["NET_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_PRODUCTION"]);                                stats.NET_PRODUCTION_PER_DAY = dr["NET_PRODUCTION_PER_DAY"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_PRODUCTION_PER_DAY"]);                                stats.NET_PRODUCTION_PER_PT_DAY = dr["NET_PRODUCTION_PER_PT_DAY"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_PRODUCTION_PER_PT_DAY"]);                                stats.COLLECTIONS = dr["COLLECTIONS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["COLLECTIONS"]);                                stats.REFUNDS_NSF = dr["REFUNDS_NSF"] == DBNull.Value ? 0 : Convert.ToInt32(dr["REFUNDS_NSF"]);                                stats.SALES_TAX = dr["SALES_TAX"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SALES_TAX"]);                                stats.NET_COLLECTIONS = dr["NET_COLLECTIONS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_COLLECTIONS"]);                                stats.ORTHO_COLLECTION = dr["ORTHO_COLLECTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ORTHO_COLLECTION"]);                                stats.COLLECTION_PERC = dr["COLLECTION_PERC"] == DBNull.Value ? 0 : Convert.ToInt32(dr["COLLECTION_PERC"]);                                stats.TotalAR = dr["TotalAR"] == DBNull.Value ? 0 : Convert.ToInt32(dr["TotalAR"]);                                stats.TOTAL_TAX_PLANNED = dr["TOTAL_TAX_PLANNED"] == DBNull.Value ? 0 : Convert.ToInt32(dr["TOTAL_TAX_PLANNED"]);                                stats.OPPORTUNITIES = dr["OPPORTUNITIES"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OPPORTUNITIES"]);                                stats.TOTAL_TAX_PLANNED_PER_OPPORUNITY = dr["TOTAL_TAX_PLANNED_PER_OPPORUNITY"] == DBNull.Value ? 0 : Convert.ToInt32(dr["TOTAL_TAX_PLANNED_PER_OPPORUNITY"]);                                stats.GOSS_HYG_PRODUCTION = dr["GOSS_HYG_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GOSS_HYG_PRODUCTION"]);                                stats.HYG_ADJUSTMENTS = dr["HYG_ADJUSTMENTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HYG_ADJUSTMENTS"]);                                stats.NET_HYG_PRODUCTION = dr["NET_HYG_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_HYG_PRODUCTION"]);                                stats.PER_NET_PRODUCTION_NET_HYG = dr["PER_NET_PRODUCTION_NET_HYG"] == DBNull.Value ? 0 : Convert.ToInt32(dr["PER_NET_PRODUCTION_NET_HYG"]);                                stats.GOSS_PERIO_PRODUCTION = dr["GOSS_PERIO_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GOSS_PERIO_PRODUCTION"]);                                stats.PER_GOSS_HYG_PRODUCTION_PERIO = dr["PER_GOSS_HYG_PRODUCTION_PERIO"] == DBNull.Value ? 0 : Convert.ToInt32(dr["PER_GOSS_HYG_PRODUCTION_PERIO"]);                                stats.NEW_PTS = dr["NEW_PTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NEW_PTS"]);                                stats.NEW_PTS_AMT_TAX = dr["NEW_PTS_AMT_TAX"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NEW_PTS_AMT_TAX"]);                                stats.NEW_PTS_AMT_COMPLETE_MONTHS = dr["NEW_PTS_AMT_COMPLETE_MONTHS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NEW_PTS_AMT_COMPLETE_MONTHS"]);                                stats.AVG_TAX_PER_NEWPTS = dr["AVG_TAX_PER_NEWPTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["AVG_TAX_PER_NEWPTS"]);                                stats.COMPELETE_RATIO = dr["COMPELETE_RATIO"] == DBNull.Value ? 0 : Convert.ToInt32(dr["COMPELETE_RATIO"]);                                stats.GENERAL_PRODUCTION = dr["GENERAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GENERAL_PRODUCTION"]);                                stats.ORHO_ADJUSTMENTS = dr["ORHO_ADJUSTMENTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ORHO_ADJUSTMENTS"]);                                stats.NET_ORTHO = dr["NET_ORTHO"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_ORTHO"]);                                stats.NEW_PTS_SEEN = dr["NEW_PTS_SEEN"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NEW_PTS_SEEN"]);                                stats.NET_ORTHO_PTS_SEEN = dr["NET_ORTHO_PTS_SEEN"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_ORTHO_PTS_SEEN"]);                                stats.ORTHO_PTS_SEEN = dr["ORTHO_PTS_SEEN"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ORTHO_PTS_SEEN"]);                                stats.AVG_GOSS_PRODUCTION_PER_PTS = dr["AVG_GOSS_PRODUCTION_PER_PTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["AVG_GOSS_PRODUCTION_PER_PTS"]);                                stats.AVG_GOSS_PRODUCTION_PER_DAY = dr["AVG_GOSS_PRODUCTION_PER_DAY"] == DBNull.Value ? 0 : Convert.ToInt32(dr["AVG_GOSS_PRODUCTION_PER_DAY"]);                                stats.CROWNS = dr["CROWNS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["CROWNS"]);                                stats.BUILDUPS = dr["BUILDUPS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["BUILDUPS"]);                                stats.PARTIALS = dr["PARTIALS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["PARTIALS"]);                                stats.PREC_ATTACHMENTS = dr["PREC_ATTACHMENTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["PREC_ATTACHMENTS"]);                                stats.IMPLANTS_PLACED = dr["IMPLANTS_PLACED"] == DBNull.Value ? 0 : Convert.ToInt32(dr["IMPLANTS_PLACED"]);                                stats.EXT = dr["EXT"] == DBNull.Value ? 0 : Convert.ToInt32(dr["EXT"]);                                stats.ORTHO_STARTS = dr["ORTHO_STARTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ORTHO_STARTS"]);                                stats.MONTH = dr["MONTH"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Month"]);                                stats.MONTHNAME = Convert.ToString(dr["MONTHNAME"]);                                statsAllSummary.Add(stats);                            }                            dr.NextResult();                            while (dr.Read())                            {                                goals = new FishGoals();                                goals.PROVIDER_DENTRIXID = Convert.ToString(dr["PROVIDER_DENTRIXID"]);                                goals.LOCATION_ID = dr["LOCATION_ID"] == DBNull.Value ? 0 : dr["LOCATION_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["LOCATION_ID"]);
                                // goals.OFFICE_ID = dr["OFFICE_ID"] == DBNull.Value ? 0 : dr["OFFICE_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OFFICE_ID"]);
                                goals.FIRSTNAME = Convert.ToString(dr["FIRSTNAME"]);                                goals.LASTNAME = Convert.ToString(dr["LASTNAME"]);                                goals.HOURS_FOR_THEDAY = dr["HOURS_FOR_THEDAY"] == DBNull.Value ? 0 : dr["HOURS_FOR_THEDAY"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HOURS_FOR_THEDAY"]);                                goals.GOAL_PRODUCTION = dr["GOAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GOAL_PRODUCTION"]);                                goals.SCHEDULED_PRODUCTION = dr["SCHEDULED_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SCHEDULED_PRODUCTION"]);                                goals.ACTUAL_PRODUCTION = dr["ACTUAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ACTUAL_PRODUCTION"]);                                goals.HYGEINE_EXAMS = dr["HYGEINE_EXAMS"] == DBNull.Value ? 0 : dr["HYGEINE_EXAMS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HYGEINE_EXAMS"]);                                goals.NP_APPOINTMENTS = dr["NP_APPOINTMENTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NP_APPOINTMENTS"]);                                goals.ROUTINE_HYEGEINE_VISIT = Convert.ToInt32(dr["ROUTINE_HYEGEINE_VISIT"]);                                goals.SRP = dr["SRP"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SRP"]);                                goals.Month = dr["Month"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Month"]);                                goals.MonthName = Convert.ToString(dr["MonthName"]);                                goals.TYPEOF_PROVIDER = Convert.ToString(dr["TYPEOF_PROVIDER"]);                                fishGoals.Add(goals);                            }                        }                    }                }                statsAllSummaryList.stats_all_summarys_ = statsAllSummary;                statsAllSummaryList.FishGoalsData = fishGoals;                return statsAllSummaryList;            }            catch (Exception ex)            {                ErrorLog.InsertErrorLog("DataModel - GetStatsSummary", ex.Message, ex.StackTrace);                return null;            }        }


        public HygeneSummaryList GetHygeneSummary(int year,int locationID)
        {
            try
            {
                List<HygeneSummary> hygeneSummaries = new List<HygeneSummary>();
                HygeneSummaryList hygeneSummaryList = new HygeneSummaryList();
                HygeneSummary hygene;
                FishGoals goals;                List<FishGoals> fishGoals = new List<FishGoals>();

                using (SqlConnection myconnection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("GetHygeneReport", myconnection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("@Type", "List"));
                        cmd.Parameters.Add(new SqlParameter("@YEAR", year));
                        cmd.Parameters.Add(new SqlParameter("@LocationID", locationID));
                        myconnection.Open();

                        using (SqlDataReader dr = cmd.ExecuteReader())
                        {
                            while (dr.Read())
                            {
                                hygene = new HygeneSummary();
                                hygene.LOCATION_ID = dr["ACCOUNT_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ACCOUNT_ID"]);
                               // hygene.OFFICE_ID = dr["OFFICE_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OFFICE_ID"]);
                                hygene.GENERAL_PRODUCTION = dr["GENERAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GENERAL_PRODUCTION"]);
                                hygene.NET_PRODUCTION = dr["NET_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_PRODUCTION"]);
                                hygene.ORTHO_PRODUCTION = dr["ORTHO_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ORTHO_PRODUCTION"]);
                                hygene.NET_ORTHO_PRODUCTION = dr["NET_ORTHO_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_ORTHO_PRODUCTION"]);
                                hygene.PTS_SEEN = dr["PTS_SEEN"] == DBNull.Value ? 0 : Convert.ToInt32(dr["PTS_SEEN"]);
                                hygene.GROSS_PRODUCTION_PER_PTS_SEEN = dr["GROSS_PRODUCTION_PER_PTS_SEEN"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GROSS_PRODUCTION_PER_PTS_SEEN"]);
                                hygene.GROSS_PROVIDER_PRODUCTION_PER_DAY = dr["GROSS_PROVIDER_PRODUCTION_PER_DAY"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GROSS_PROVIDER_PRODUCTION_PER_DAY"]);
                                hygene.GROSS_PRODUCTION = dr["GROSS_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GROSS_PRODUCTION"]);
                                hygene.NET_PRODUCTION_NEW = dr["NET_PRODUCTION_NEW"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_PRODUCTION_NEW"]);

                                hygene.PTS_SEEN_NEW = dr["PTS_SEEN_NEW"] == DBNull.Value ? 0 : Convert.ToInt32(dr["PTS_SEEN_NEW"]);
                                hygene.GROSS_PRODUCTION_PER_PTS_SEEN_NEW = dr["GROSS_PRODUCTION_PER_PTS_SEEN_NEW"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GROSS_PRODUCTION_PER_PTS_SEEN_NEW"]);
                                hygene.ARESTIN = dr["ARESTIN"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ARESTIN"]);
                                hygene.SRP_PERIO_MAINT = dr["SRP_PERIO_MAINT"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SRP_PERIO_MAINT"]);
                                hygene.ARESTIN_PERIO_PTS_SEEN = dr["ARESTIN_PERIO_PTS_SEEN"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ARESTIN_PERIO_PTS_SEEN"]);

                                hygene.MonthName = Convert.ToString(dr["MonthName"]);
                                hygene.Month = dr["Month"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Month"]);

                                hygeneSummaries.Add(hygene);
                            }

                            dr.NextResult();
                            //while (dr.Read())                            //{                            //    goals = new FishGoals();                            //    goals.PROVIDER_DENTRIXID = Convert.ToString(dr["PROVIDER_DENTRIXID"]);                            //    goals.LOCATION_ID = dr["LOCATION_ID"] == DBNull.Value ? 0 : dr["LOCATION_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["LOCATION_ID"]);
                            //    //    goals.OFFICE_ID = dr["OFFICE_ID"] == DBNull.Value ? 0 : dr["OFFICE_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OFFICE_ID"]);
                            //    goals.FIRSTNAME = Convert.ToString(dr["FIRSTNAME"]);                            //    goals.LASTNAME = Convert.ToString(dr["LASTNAME"]);                            //    goals.HOURS_FOR_THEDAY = dr["HOURS_FOR_THEDAY"] == DBNull.Value ? 0 : dr["HOURS_FOR_THEDAY"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HOURS_FOR_THEDAY"]);                            //    goals.GOAL_PRODUCTION = dr["GOAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GOAL_PRODUCTION"]);                            //    goals.SCHEDULED_PRODUCTION = dr["SCHEDULED_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SCHEDULED_PRODUCTION"]);                            //    goals.ACTUAL_PRODUCTION = dr["ACTUAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ACTUAL_PRODUCTION"]);                            //    goals.HYGEINE_EXAMS = dr["HYGEINE_EXAMS"] == DBNull.Value ? 0 : dr["HYGEINE_EXAMS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HYGEINE_EXAMS"]);                            //    goals.NP_APPOINTMENTS = dr["NP_APPOINTMENTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NP_APPOINTMENTS"]);                            //    goals.ROUTINE_HYEGEINE_VISIT = Convert.ToInt32(dr["ROUTINE_HYEGEINE_VISIT"]);                            //    goals.SRP = dr["SRP"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SRP"]);                            //    goals.Month = dr["Month"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Month"]);                            //    goals.MonthName = Convert.ToString(dr["MonthName"]);                            //    goals.TYPEOF_PROVIDER = Convert.ToString(dr["TYPEOF_PROVIDER"]);                            //    fishGoals.Add(goals);                            //}
                        }
                    }
                }
                hygeneSummaryList.HygeneSummaryData = hygeneSummaries;
                return hygeneSummaryList;
            }
            catch (Exception ex)
            {
                ErrorLog.InsertErrorLog("DataModel - GetHygeneReport", ex.Message, ex.StackTrace);
                return null;
            }
        }


        public HygeneSummaryList GetHygeneSummaryDate(string FromDate, string Todate, int locationID)        {            try            {                List<HygeneSummary> hygeneSummaries = new List<HygeneSummary>();                HygeneSummaryList hygeneSummaryList = new HygeneSummaryList();                HygeneSummary hygene;                FishGoals goals;                List<FishGoals> fishGoals = new List<FishGoals>();                using (SqlConnection myconnection = new SqlConnection(connectionString))                {                    using (SqlCommand cmd = new SqlCommand("GetHygeneReport", myconnection))                    {                        cmd.CommandType = CommandType.StoredProcedure;                        cmd.Parameters.Add(new SqlParameter("@FromDate", FromDate));
                        cmd.Parameters.Add(new SqlParameter("@ToDate", Todate));                        cmd.Parameters.Add(new SqlParameter("@LocationID", locationID));                        cmd.Parameters.Add(new SqlParameter("@Type", "DateWise"));                        myconnection.Open();                        using (SqlDataReader dr = cmd.ExecuteReader())                        {                            while (dr.Read())                            {                                hygene = new HygeneSummary();                                hygene.LOCATION_ID = dr["ACCOUNT_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ACCOUNT_ID"]);
                                // hygene.OFFICE_ID = dr["OFFICE_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OFFICE_ID"]);
                                hygene.GENERAL_PRODUCTION = dr["GENERAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GENERAL_PRODUCTION"]);                                hygene.NET_PRODUCTION = dr["NET_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_PRODUCTION"]);                                hygene.ORTHO_PRODUCTION = dr["ORTHO_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ORTHO_PRODUCTION"]);                                hygene.NET_ORTHO_PRODUCTION = dr["NET_ORTHO_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_ORTHO_PRODUCTION"]);                                hygene.PTS_SEEN = dr["PTS_SEEN"] == DBNull.Value ? 0 : Convert.ToInt32(dr["PTS_SEEN"]);                                hygene.GROSS_PRODUCTION_PER_PTS_SEEN = dr["GROSS_PRODUCTION_PER_PTS_SEEN"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GROSS_PRODUCTION_PER_PTS_SEEN"]);                                hygene.GROSS_PROVIDER_PRODUCTION_PER_DAY = dr["GROSS_PROVIDER_PRODUCTION_PER_DAY"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GROSS_PROVIDER_PRODUCTION_PER_DAY"]);                                hygene.GROSS_PRODUCTION = dr["GROSS_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GROSS_PRODUCTION"]);                                hygene.NET_PRODUCTION_NEW = dr["NET_PRODUCTION_NEW"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NET_PRODUCTION_NEW"]);                                hygene.PTS_SEEN_NEW = dr["PTS_SEEN_NEW"] == DBNull.Value ? 0 : Convert.ToInt32(dr["PTS_SEEN_NEW"]);                                hygene.GROSS_PRODUCTION_PER_PTS_SEEN_NEW = dr["GROSS_PRODUCTION_PER_PTS_SEEN_NEW"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GROSS_PRODUCTION_PER_PTS_SEEN_NEW"]);                                hygene.ARESTIN = dr["ARESTIN"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ARESTIN"]);                                hygene.SRP_PERIO_MAINT = dr["SRP_PERIO_MAINT"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SRP_PERIO_MAINT"]);                                hygene.ARESTIN_PERIO_PTS_SEEN = dr["ARESTIN_PERIO_PTS_SEEN"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ARESTIN_PERIO_PTS_SEEN"]);                                hygene.MonthName = Convert.ToString(dr["MonthName"]);                                hygene.Month = dr["Month"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Month"]);                                hygeneSummaries.Add(hygene);                            }                            dr.NextResult();
                            //while (dr.Read())
                            //{
                            //    goals = new FishGoals();
                            //    goals.PROVIDER_DENTRIXID = Convert.ToString(dr["PROVIDER_DENTRIXID"]);
                            //    goals.LOCATION_ID = dr["LOCATION_ID"] == DBNull.Value ? 0 : dr["LOCATION_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["LOCATION_ID"]);
                            //    //    goals.OFFICE_ID = dr["OFFICE_ID"] == DBNull.Value ? 0 : dr["OFFICE_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["OFFICE_ID"]);
                            //    goals.FIRSTNAME = Convert.ToString(dr["FIRSTNAME"]);
                            //    goals.LASTNAME = Convert.ToString(dr["LASTNAME"]);
                            //    goals.HOURS_FOR_THEDAY = dr["HOURS_FOR_THEDAY"] == DBNull.Value ? 0 : dr["HOURS_FOR_THEDAY"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HOURS_FOR_THEDAY"]);
                            //    goals.GOAL_PRODUCTION = dr["GOAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["GOAL_PRODUCTION"]);
                            //    goals.SCHEDULED_PRODUCTION = dr["SCHEDULED_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SCHEDULED_PRODUCTION"]);
                            //    goals.ACTUAL_PRODUCTION = dr["ACTUAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ACTUAL_PRODUCTION"]);
                            //    goals.HYGEINE_EXAMS = dr["HYGEINE_EXAMS"] == DBNull.Value ? 0 : dr["HYGEINE_EXAMS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HYGEINE_EXAMS"]);
                            //    goals.NP_APPOINTMENTS = dr["NP_APPOINTMENTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NP_APPOINTMENTS"]);
                            //    goals.ROUTINE_HYEGEINE_VISIT = Convert.ToInt32(dr["ROUTINE_HYEGEINE_VISIT"]);
                            //    goals.SRP = dr["SRP"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SRP"]);
                            //    goals.Month = dr["Month"] == DBNull.Value ? 0 : Convert.ToInt32(dr["Month"]);
                            //    goals.MonthName = Convert.ToString(dr["MonthName"]);
                            //    goals.TYPEOF_PROVIDER = Convert.ToString(dr["TYPEOF_PROVIDER"]);
                            //    fishGoals.Add(goals);
                            //}
                        }                    }                }                hygeneSummaryList.HygeneSummaryData = hygeneSummaries;                return hygeneSummaryList;            }            catch (Exception ex)            {                ErrorLog.InsertErrorLog("DataModel - GetHygeneReport", ex.Message, ex.StackTrace);                return null;            }        }

        // Changes By Bhupesh
        public string UpdateDisplayMode(int DisId , int PageId , int LocId)
        {
            string _todayDate = DateTime.Now.ToShortDateString();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                if (con.State == System.Data.ConnectionState.Open)
                    con.Close();
                SqlCommand cmd = new SqlCommand("UPDATE SHOW_HIDE_FORMS SET DISPLAY = '" + DisId + "',Modified_Date='" + _todayDate + "' WHERE PAGES_ID = '" + PageId + "' AND ACCOUNT_ID = '" + LocId+ "'", con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            return "Success";
        }

        // Changes By Bhupesh
        public string UpdateFormsStatus(int DisId, int PageId, int LocId)
        {
            string _todayDate = DateTime.Now.ToShortDateString();
            string _listDisplayId = string.Empty;
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                if (con.State == System.Data.ConnectionState.Open)
                    con.Close();
                int[] _display_id = new int[10];
                
                for (int i = 1; i <= 9; i++)
                {
                    SqlCommand cmds = new SqlCommand("select display from SHOW_HIDE_FORMS where ACCOUNT_ID = '" + LocId + "' and PAGES_ID = '" + Convert.ToInt32(i) + "'", con);
                    con.Open();

                    SqlDataReader dr = cmds.ExecuteReader();
                    if (dr.Read())
                    {
                        _display_id[i] = Convert.ToInt16(dr["display"]);
                        _listDisplayId += _display_id[i] + ",";
                    }
                    con.Close();
                }
            }
            return _listDisplayId;
        }

        // Changes By Bhupesh
        public string AddNewProvider(string F_Name, string L_Name, string TypeProvider, string DentrixProvider, string LocationId)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                try
                {
                    if (con.State == ConnectionState.Open)
                        con.Close();
                    con.Open();
                    SqlCommand cmd = new SqlCommand("New_Provider");
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    
                    cmd.Parameters.AddWithValue("@TYPE", "INSERT");
                    cmd.Parameters.AddWithValue("@MEMBER_FNAME", F_Name);
                    cmd.Parameters.AddWithValue("@MEMBER_LNAME", L_Name);
                    cmd.Parameters.AddWithValue("@TYPEOF_PROVIDER", TypeProvider);
                    cmd.Parameters.AddWithValue("@DENTRIX_PROVIDERID", DentrixProvider);
                    cmd.Parameters.AddWithValue("@LOCATION_ID", LocationId);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
                catch (Exception ex)
                {
                }
                return "1";
            }
        }

        // Changes By Bhupesh
        //public static List<Member_Details> GetProviderList(int LocationId)
        //{
        //    DataSet ds = new DataSet();
        //    using (SqlConnection con = new SqlConnection(connectionString))
        //    {
        //        DataTable dt = new DataTable();
        //        try
        //        {
        //            if (con.State == ConnectionState.Open)
        //                con.Close();
        //            con.Open();
        //            // SqlCommand cmd = new SqlCommand(" SELECT MEMBER_FNAME+' '+ MEMBER_LNAME AS FULL_NAME FROM Member_Details WHERE  LOCATION_ID = '"+ LocationId + "'",con);
        //            SqlCommand cmd = new SqlCommand("New_Provider");
        //            cmd.Connection = con;
        //            cmd.CommandType = CommandType.StoredProcedure;
        //            cmd.Parameters.AddWithValue("@TYPE", "GET_PROVIDER_LIST");
        //            cmd.Parameters.AddWithValue("@LOCATION_ID", LocationId);

        //            SqlDataAdapter da = new SqlDataAdapter(cmd);
        //            var stare = da.Fill(ds);
        //            con.Close();
        //            // return ds.Tables[0];
        //        }

        //        catch (Exception ex)
        //        {
        //        }
        //        return ds.Tables[0];
        //    }

        //}
        public List<Member_Details> GetProviderName(int LocationId)
        {
            List<Member_Details> listName = new List<Member_Details>();
            DataTable dt = GetProviderList(LocationId);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    listName.Add(new Member_Details()
                    {
                        FULL_NAME = Convert.ToString(row[0]),
                    });
                }
            }
            return listName;
        }



        public DataTable GetProviderList(int LocationId)
        {
            DataSet ds = new DataSet();
            int i;
            using (SqlConnection con = new SqlConnection(connectionString))
            { 
                try
                {
                    if (con.State == ConnectionState.Open)
                        con.Close();
                    con.Open();
                    SqlCommand cmd = new SqlCommand("New_Provider");
                    cmd.Connection = con;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@TYPE", "GET_PROVIDER_LIST");
                    cmd.Parameters.AddWithValue("@LOCATION_ID", LocationId);

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    var stare = da.Fill(ds);
                    con.Close();
                    return ds.Tables[0];
                }
                catch (Exception ex)
                {
                }
            return ds.Tables[0];
            }
        }
    }
}