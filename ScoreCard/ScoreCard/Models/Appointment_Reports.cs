﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScoreCard.Models
{
    public class Appointment_Reports
    {
        public string Appointment_Date { get; set; }
        public string Provider_Id { get; set; }
        public string Provider_First_Name { get; set; }
        public string FRONT_DESK_ID { get; set; }
        public string FRONT_DESK_FIRST_NAME { get; set; }
        public string DENTAL_ASSISTANT_ID { get; set; }
        public string DENTAL_ASSISTANT_FIRST_NAME { get; set; }
        public string TREATEMENT_PLANNER_ID { get; set; }
        public string TREATEMENT_PLANNER_FIRST_NAME { get; set; }
        public string OFFICE_MANAGER_ID { get; set; }
        public string OFFICE_MANAGER_FIRST_NAME { get; set; }
        public string REGIONAL_MANAGER_ID { get; set; }
        public string REGIONAL_MANAGER_FIRST_NAME { get; set; }
        public string Patient_Id { get; set; }
        public string Patient_Name { get; set; }
        public string Appointment_Reason { get; set; }
        public string START_HOUR { get; set; }
        public string START_MINUTE { get; set; }
        public string SCHEDULE_PRODUCTION_AMOUNT { get; set; }
        public string ACTUAL_STATUS_DESCRIPTION { get; set; }
        public string ACTUAL_PRODUCTION_AMOUNT { get; set; }
        public string Production_Type { get; set; }
        public string Next_Appointment_Date { get; set; }
        public string Treatment_Presented { get; set; }
        public string Treatment_Accepted { get; set; }
        public string Collection { get; set; }
    }
}