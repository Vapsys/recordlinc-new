﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScoreCard.Models
{
    public class FishGoals
    {
        public int PROVIDER_ID { get; set; }
        public string PROVIDER_DENTRIXID { get; set; }
        public int LOCATION_ID { get; set; }
        public int ACCOUNT_ID { get; set; }
        //  public int OFFICE_ID { get; set; }
        //  public int ORG_ID { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public int PERHOUR_PRODUCTION { get; set; }
        public int HOURS_FOR_THEDAY { get; set; }
        public int GOAL_PRODUCTION { get; set; }
        public int SCHEDULED_PRODUCTION { get; set; }
        public int ACTUAL_PRODUCTION { get; set; }
        public int HYGEINE_EXAMS { get; set; }
        public int NP_APPOINTMENTS { get; set; }
        public int GROUPING_ID { get; set; }
        public int ROUTINE_HYEGEINE_VISIT { get; set; }
        public int SRP { get; set; }
        public DateTime REFDATE { get; set; }
        public int Month { get; set; }
        public string MonthName { get; set; }
        public string TYPEOF_PROVIDER { get; set; }
        public DateTime CREATEDDATE { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime MODIFIEDATE { get; set; }
        public string MODIFIEDBY { get; set; }
    }
}