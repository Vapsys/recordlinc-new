﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScoreCard.Models
{
    public class CriticalNumbers
    {
        public int ID { get; set; }
        public int LOCATION_ID { get; set; }
        public int ACCOUNT_ID { get; set; }
        //public int OFFICE_ID { get; set; }
        //public int ORG_ID { get; set; }
        public int Collection_Ratio { get; set; }
        public int TotalProduction_NewPatient { get; set; }
        public int TotalProduction_HygeineVisit { get; set; }
        public int CollectibleProduction_HygeineVisit { get; set; }
        public int TotalRestorativeProduction_HygeineVisit { get; set; }
        public int MarketingCosts_NewPatient { get; set; }
        public int Marketing_ROI { get; set; }
        public int Production_Hour { get; set; }
        public int Production_Operatory { get; set; }
        public int RestorativeProduction_Operatory { get; set; }
        public int HygeineProduction_Operatory { get; set; }
        public int Production_NonProdEmployee { get; set; }
        public int Production_NonDoc { get; set; }
        public DateTime REFDATE { get; set; }
        public int Month { get; set; }
        public string MonthName { get; set; }
        public DateTime CREATEDDATE { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime MODIFIEDATE { get; set; }
        public string MODIFIEDBY { get; set; }
    }


    public class IndividualGoals
    {
        public int PROVIDER_ID { get; set; }
        public string PROVIDER_DENTRIXID { get; set; }
        public int LOCATION_ID { get; set; }
        public int OFFICE_ID { get; set; }
        public int ORG_ID { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public int Production_Hour { get; set; }
        public int Production_HygieneExam { get; set; }
        public int ActualToGoal { get; set; }
        public int HygienistProduction_Hour { get; set; }
        public int HYGEINE_EXAM_Hour { get; set; }
        public int Production_NewPatient { get; set; }
        public DateTime REFDATE { get; set; }
        public int Month { get; set; }
        public string MonthName { get; set; }
        public string TYPEOF_PROVIDER { get; set; }
        public DateTime CREATEDDATE { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime MODIFIEDATE { get; set; }
        public string MODIFIEDBY { get; set; }
    }

    public class CriticalNumbersList
    {
        public List<CriticalNumbers> CriticalNumbersData { get; set; }
        public List<IndividualGoals> IndividualGoalsData { get; set; }
    }
}