﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScoreCard.Models
{
    public class Stats_All_Summary
    {
        public int ID { get; set; }
        public int LOCATION_ID { get; set; }
        public int OFFICE_ID { get; set; }
        public int ORG_ID { get; set; }
        public float GROSS_PRODUCTION { get; set; }
        public float ORTHO_PRODUCTION { get; set; }
        public float ADJUSTMENTS { get; set; }
        public float NET_PRODUCTION { get; set; }
        public float NET_PRODUCTION_PER_DAY { get; set; }
        public float NET_PRODUCTION_PER_PT_DAY { get; set; }
        public float COLLECTIONS { get; set; }
        public float REFUNDS_NSF { get; set; }
        public float SALES_TAX { get; set; }
        public float NET_COLLECTIONS { get; set; }
        public float ORTHO_COLLECTION { get; set; }
        public float COLLECTION_PERC { get; set; }
        public float TotalAR { get; set; }
        public float TOTAL_TAX_PLANNED { get; set; }
        public float OPPORTUNITIES { get; set; }
        public float TOTAL_TAX_PLANNED_PER_OPPORUNITY { get; set; }
        public float GOSS_HYG_PRODUCTION { get; set; }
        public float HYG_ADJUSTMENTS { get; set; }
        public float NET_HYG_PRODUCTION { get; set; }
        public float PER_NET_PRODUCTION_NET_HYG { get; set; }
        public float GOSS_PERIO_PRODUCTION { get; set; }
        public float PER_GOSS_HYG_PRODUCTION_PERIO { get; set; }
        public float NEW_PTS { get; set; }
        public float NEW_PTS_AMT_TAX { get; set; }
        public float NEW_PTS_AMT_COMPLETE_MONTHS { get; set; }
        public float AVG_TAX_PER_NEWPTS { get; set; }
        public float COMPELETE_RATIO { get; set; }
        public float GENERAL_PRODUCTION { get; set; }
        public float ORHO_ADJUSTMENTS { get; set; }
        public float NET_ORTHO { get; set; }
        public float NEW_PTS_SEEN { get; set; }
        public float NET_ORTHO_PTS_SEEN { get; set; }
        public float ORTHO_PTS_SEEN { get; set; }
        public float AVG_GOSS_PRODUCTION_PER_PTS { get; set; }
        public float AVG_GOSS_PRODUCTION_PER_DAY { get; set; }
        public float CROWNS { get; set; }
        public float BUILDUPS { get; set; }
        public float PARTIALS { get; set; }
        public float PREC_ATTACHMENTS { get; set; }
        public float IMPLANTS_PLACED { get; set; }
        public float EXT { get; set; }
        public float ORTHO_STARTS { get; set; }
        public DateTime REFDATE { get; set; }
        public int MONTH { get; set; }
        public string MONTHNAME { get; set; }

    }

    public class StatsAllSummary_List
    {
        public List<Stats_All_Summary> stats_all_summarys_ { get; set; }
        public List<FishGoals> FishGoalsData { get; set; }
    }
}