﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScoreCard.Models
{
    public class OfficeHours
    {
        public int ID { get; set; }
        public int LOCATION_ID { get; set; }
        public int OFFICE_ID { get; set; }
        public string OFFICE_STARTHOUR { get; set; }
        public string OFFICE_ENDHOUR { get; set; }
        public DateTime SCHEDULE_DATE { get; set; }
        public DateTime CREATEDDATE { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime MODIFIEDATE { get; set; }
        public string MODIFIEDBY { get; set; }
    }

    public class Showhideforms
    {
        public int ID { get; set; }
        public int LOCATION_ID { get; set; }
        public int PAGES_ID { get; set; }
        public int DISPLAY { get; set; }
        public DateTime CREATED_DATE { get; set; }
        public string MODIFIED_DATE { get; set; }

    }
}