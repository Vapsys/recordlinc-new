﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScoreCard.Models
{
    public class IndividualSummary
    {
        public int ID { get; set; }
        public int LOCATION_ID { get; set; }
        public double GrossProduction { get; set; }
        public double OrthoProduction { get; set; }
        public double Adjustments { get; set; }
        public double AdjustmentPer { get; set; }
        public double NetProduction { get; set; }
        public double NetProductionPerDay { get; set; }
        public double Collections { get; set; }
        public double RefundsNSF { get; set; }
        public double NetCollections { get; set; }
        public double OrthoCollection { get; set; }
        public double CollectionPer { get; set; }
        public double TotalAR { get; set; }
        public double ARRatio { get; set; }
        public double OrthoAR { get; set; }
        public double SimplePayAR { get; set; }
        public double ARMinusOrthoSimplepay { get; set; }
        public double ARRatioMinusOrthoSimplepay { get; set; }
        public double NewPts { get; set; }
        public double PtReferrals { get; set; }
        public double PerPtReferrals { get; set; }
        public double DeactivatedPts { get; set; }
        public double TotalPtCount { get; set; }
        public double TotalPtsSeen { get; set; }
        public double NetProductionPerPt { get; set; }
        public double ActualContCareVisits { get; set; }
        public double ContCareEfficiency { get; set; }
        public double Supplies { get; set; }
        public double SuppliesExpenseofGrossProduction { get; set; }

        public int Month { get; set; }
        public string MonthName { get; set; }

        public double LabExpense { get; set; }
        public double LabExpenseofGrossProduction { get; set; }
        public DateTime REFDATE { get; set; }
      
    }
    public class IndividualSummaryList
    {
        public List<IndividualSummary> IndividualSummaryData { get; set; }
        public List<FishGoals> FishGoalsData { get; set; }
    }
}