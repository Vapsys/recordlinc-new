﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScoreCard.Models
{
    public class Member_Details
    {
        public int MEMBER_ID { get; set; }
        public string MEMBER_FNAME { get; set; }
        public string MEMBER_LNAME { get; set; }
        public string FULL_NAME { get; set; }
        
        public string DENTRIX_PROVIDERID { get; set; }
        public string LOCATION_ID { get; set; }
        public string TYPEOF_PROVIDER { get; set; }
        public string PERHOUR_PRODUCTION { get; set; }
        public string CREATEDDATE { get; set; }
        public string CREATEDBY { get; set; }
        public string MODIFIEDDATE { get; set; }
        public string MODIFIEDBY { get; set; }
    }
}