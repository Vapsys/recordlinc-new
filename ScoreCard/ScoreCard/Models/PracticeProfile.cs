﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ScoreCard.Models
{
    public class PracticeProfile
    {
        public int ID { get; set; }
        public int LOCATION_ID { get; set; }
        public int ACCOUNT_ID { get; set; }
        // public int OFFICE_ID { get; set; }
        public int TOTAL_OPERATORIES { get; set; }
        public int RESTORATIVE_OPERATORIES { get; set; }
        public int HYGIENE_OPERATORIES { get; set; }
        public int TOTAL_DOCTORS { get; set; }
        public int Marketing_Costs { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string MonthName { get; set; }
        public DateTime CREATEDDATE { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime MODIFIEDATE { get; set; }
        public string MODIFIEDBY { get; set; }
    }
}