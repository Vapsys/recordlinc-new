﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using BO.Models;
using BO.ViewModel;
using static BO.Enums.Common;
using DataAccessLayer.Common;

namespace DataAccessLayer
{
    public class ReferralFormDAL
    {
        clsHelper clshelper = new clsHelper();
        clsCommon ObjCommon = new clsCommon();
        /// <summary>
        /// Get the Speciality details for referal setting.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public DataTable GetDentistSpeciality(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                dt = clshelper.DataTable("GetDentistSpeciality", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("ReferralFormDAL - GetDentistSpeciality", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Get the dentist service detail from specility.
        /// </summary>
        /// <param name="UserId,SpecialityId"></param>
        /// <returns></returns>
        public DataTable GetDentistServices(int UserId, int SpecialityId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@SpecialityId", SqlDbType.Int);
                strParameter[1].Value = SpecialityId;
                dt = clshelper.DataTable("GetDentistServices", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("ReferralFormDAL - GetDentistServices", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Get the dentist Information details like Name,Email,Phone,IProvider.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public DataTable GetDentistreferral(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                dt = clshelper.DataTable("GetDentistReferral", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("ReferralFormDAL - GetDentistreferral", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// save RefferalForm Settings,Speciality,Services For Doctor.
        /// </summary>
        /// <param name="UserId,rfsobj,spcltyObj,servobj,subfieldObj"></param>
        /// <returns></returns>
        public static bool AddRefferalFormSetting(int UserId, List<ReferralFormSetting> rfsobj, List<ReferralFormSetting> spcltyObj, List<ReferralFormServices> servobj, List<ReferralFormSubField> subfieldObj)
        {
            try
            {
                //bool? p_email = false;
                //bool? birthdate = false;
                //foreach (var item in rfsobj)
                //{
                //    if (item.Name == "DateofBirth")
                //    {
                //        birthdate = item.IsEnable;
                //    }
                //    else if (item.Name == "PatientEmail")
                //    {
                //        p_email = item.IsEnable;
                //    }
                //}
                bool res = true;
                foreach (var item in rfsobj)
                {
                    DataTable dt = new DataTable();
                    SqlParameter[] strParameter = new SqlParameter[4];
                    strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                    strParameter[0].Value = UserId;
                    strParameter[1] = new SqlParameter("@Name", SqlDbType.NVarChar);
                    strParameter[1].Value = item.Name;
                    strParameter[2] = new SqlParameter("@IsEnable", SqlDbType.Bit);
                    strParameter[2].Value = item.IsEnable;
                    strParameter[3] = new SqlParameter("@OrderBy", SqlDbType.Int);
                    strParameter[3].Value = item.OrderBy;
                    res = new clsHelper().ExecuteNonQuery("InsertDentistReferral", strParameter);
                }
                foreach (var item in spcltyObj)
                {
                    DataTable dt = new DataTable();
                    SqlParameter[] strParameter = new SqlParameter[4];
                    strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                    strParameter[0].Value = UserId;
                    strParameter[1] = new SqlParameter("@Id", SqlDbType.Int);
                    strParameter[1].Value = item.SpecialityId;
                    strParameter[2] = new SqlParameter("@IsEnable", SqlDbType.Bit);
                    strParameter[2].Value = item.IsEnable;
                    strParameter[3] = new SqlParameter("@OrderBy", SqlDbType.Int);
                    strParameter[3].Value = item.OrderBy;
                    res = new clsHelper().ExecuteNonQuery("InsertDentistSpeciality", strParameter);
                }
                foreach (var item in servobj)
                {
                    DataTable dt = new DataTable();
                    SqlParameter[] strParameter = new SqlParameter[5];
                    strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                    strParameter[0].Value = UserId;
                    strParameter[1] = new SqlParameter("@Id", SqlDbType.Int);
                    strParameter[1].Value = item.FieldId;
                    strParameter[2] = new SqlParameter("@IsEnable", SqlDbType.Bit);
                    strParameter[2].Value = item.IsEnable;
                    strParameter[3] = new SqlParameter("@OrderBy", SqlDbType.Int);
                    strParameter[3].Value = item.OrderBy;
                    strParameter[4] = new SqlParameter("@SpecialityId", SqlDbType.Int);
                    strParameter[4].Value = item.SpecialityId;
                    res = new clsHelper().ExecuteNonQuery("InsertDentistServices", strParameter);
                }
                foreach (var item in subfieldObj)
                {
                    DataTable dt = new DataTable();
                    SqlParameter[] strParameter = new SqlParameter[6];
                    strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                    strParameter[0].Value = UserId;
                    strParameter[1] = new SqlParameter("@ServiceId", SqlDbType.Int);
                    strParameter[1].Value = item.SubFieldId;
                    strParameter[2] = new SqlParameter("@IsEnable", SqlDbType.Bit);
                    strParameter[2].Value = item.IsEnable;
                    strParameter[3] = new SqlParameter("@OrderBy", SqlDbType.Int);
                    strParameter[3].Value = item.OrderBy;
                    strParameter[4] = new SqlParameter("@SpecialityId", SqlDbType.Int);
                    strParameter[4].Value = item.SpecialityId;
                    strParameter[5] = new SqlParameter("@ParentFieldId", SqlDbType.Int);
                    strParameter[5].Value = item.ParentFieldId;
                    res = new clsHelper().ExecuteNonQuery("InsertDentistSubFieldServices", strParameter);
                }
                return res;

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormDAL--AddRefferalFormSetting", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// save RefferalForm Settings,Speciality,Services For Patient.
        /// </summary>
        /// <param name="UserId,smobj,fmObj,subfieldObj"></param>
        /// <returns></returns>
        public static bool AddDentistPatientreferralFormSetting(int UserId, List<PatientSectionMapping> smobj, List<PatientFieldMapping> fmObj, List<PatientSubFieldMapping> subfieldObj)
        {
            try
            {
                bool res = true;
                foreach (var item in smobj)
                {
                    DataTable dt = new DataTable();
                    SqlParameter[] strParameter = new SqlParameter[4];
                    strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                    strParameter[0].Value = UserId;
                    strParameter[1] = new SqlParameter("@SectionId", SqlDbType.Int);
                    strParameter[1].Value = item.SectionId;
                    strParameter[2] = new SqlParameter("@OrderBy", SqlDbType.Int);
                    strParameter[2].Value = item.OrderBy;
                    strParameter[3] = new SqlParameter("@IsEnable", SqlDbType.Bit);
                    strParameter[3].Value = item.IsEnable;
                    res = new clsHelper().ExecuteNonQuery("USP_InsertDentistPatientSectionMapping", strParameter);
                }
                foreach (var item in fmObj)
                {
                    DataTable dt = new DataTable();
                    SqlParameter[] strParameter = new SqlParameter[5];
                    strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                    strParameter[0].Value = UserId;
                    strParameter[1] = new SqlParameter("@SectionId", SqlDbType.Int);
                    strParameter[1].Value = item.SectionId;
                    strParameter[2] = new SqlParameter("@OrderBy", SqlDbType.Int);
                    strParameter[2].Value = item.OrderBy;
                    strParameter[3] = new SqlParameter("@IsEnable", SqlDbType.Bit);
                    strParameter[3].Value = item.IsEnable;
                    strParameter[4] = new SqlParameter("@FieldId", SqlDbType.Int);
                    strParameter[4].Value = item.FieldId;
                    res = new clsHelper().ExecuteNonQuery("USP_InsertPatientDentistFieldMapping", strParameter);
                }
                foreach (var item in subfieldObj)
                {
                    DataTable dt = new DataTable();
                    SqlParameter[] strParameter = new SqlParameter[6];
                    strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                    strParameter[0].Value = UserId;
                    strParameter[1] = new SqlParameter("@SectionId", SqlDbType.Int);
                    strParameter[1].Value = item.SectionId;
                    strParameter[2] = new SqlParameter("@OrderBy", SqlDbType.Int);
                    strParameter[2].Value = item.OrderBy;
                    strParameter[3] = new SqlParameter("@IsEnable", SqlDbType.Bit);
                    strParameter[3].Value = item.IsEnable;
                    strParameter[4] = new SqlParameter("@FieldId", SqlDbType.Int);
                    strParameter[4].Value = item.SubFieldId;
                    strParameter[5] = new SqlParameter("@ParentFieldId", SqlDbType.Int);
                    strParameter[5].Value = item.ParentFieldId;
                    res = new clsHelper().ExecuteNonQuery("USP_InsertPatientDentistSubFieldMapping", strParameter);
                }
                return res;

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormDAL--AddDentistPatientreferralFormSetting", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Get Referal setting in that Section details For Patient.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        //public DataTable GetDentistPatientSectionDetails(int UserId)
        //{
        //    try
        //    {
        //        DataTable dt = new DataTable();
        //        SqlParameter[] strParameter = new SqlParameter[1];
        //        strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
        //        strParameter[0].Value = UserId;
        //        dt = clshelper.DataTable("USP_GetDentistPatientSectionDetails", strParameter);
        //        return dt;
        //    }
        //    catch (Exception Ex)
        //    {
        //        ObjCommon.InsertErrorLog("ReferralFormDAL - GetDentistPatientSectionDetails", Ex.Message, Ex.StackTrace);
        //        throw;
        //    }
        //}

        public DataSet GetDentistPatientSectionDetails(int UserId)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                ds = clshelper.GetDatasetData("USP_GetDentistPatientDetails", strParameter);
                return ds;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("ReferralFormDAL - GetDentistPatientSectionDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Get Referal setting in that Services details For Patient.
        /// </summary>
        /// <param name="UserId,SectionId"></param>
        /// <returns></returns>
        public DataTable GetDentistPatientFieldDetails(int UserId, int SectionId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@SectionId", SqlDbType.Int);
                strParameter[1].Value = SectionId;
                dt = clshelper.DataTable("USP_GetDentistPatientFieldDetails", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("ReferralFormDAL - GetDentistPatientFieldDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Get Referal setting in that Child field details For Dentist from Parentfield in service.
        /// </summary>
        /// <param name="UserId,SpecialityId,ParentFieldId"></param>
        /// <returns></returns>
        public DataTable GetServicesSubFieldDetails(int UserId, int SpecialityId, int ParentFieldId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@SpecialityId", SqlDbType.Int);
                strParameter[1].Value = SpecialityId;
                strParameter[2] = new SqlParameter("@ParentFieldId", SqlDbType.Int);
                strParameter[2].Value = ParentFieldId;
                dt = clshelper.DataTable("USP_GetServicesSubFieldDetails", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("ReferralFormDAL - GetDentistPatientSubFieldDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }


        /// <summary>
        /// Get Referal setting in that Child field details For Patient from Parentfield in service.
        /// </summary>
        /// <param name="UserId,SectionId,ParentFieldId"></param>
        /// <returns></returns>
        public DataTable GetDentistPatientSubFieldDetails(int UserId, int SectionId, int ParentFieldId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@SectionId", SqlDbType.Int);
                strParameter[1].Value = SectionId;
                strParameter[2] = new SqlParameter("@ParentFieldId", SqlDbType.Int);
                strParameter[2].Value = ParentFieldId;
                dt = clshelper.DataTable("USP_GetDentistPatientSubFieldDetails", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("ReferralFormDAL - GetDentistPatientSubFieldDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Get Details for Patient seeting field for dentist sectrion referal setting.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public DataTable GetDentistPatientFieldDetailsForAllSpeciality(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@ColleagueId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                dt = clshelper.DataTable("Usp_GetReferralvisibleDetail", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("ReferralFormDAL - GetDentistPatientFieldDetailsForAllSpeciality", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Get Details for Patient seeting field for dentist sectrion referal setting.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public DataTable GetPatientFieldDetailsForAll(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@ColleagueId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                dt = clshelper.DataTable("Usp_GetPatientFormField", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("ReferralFormDAL - GetPatientFieldDetailsForAll", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Get Patient referal Form Setting Detail
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public DataTable GetSpecialityForPatientReferralFormSetting(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                dt = clshelper.DataTable("Usp_GetPatientReferralvisibleDetail", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("ReferralFormDAL - GetSpecialityForPatientReferralFormSetting", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Get Patient Section Field Details for referal setting in recordlinc.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public DataTable GetPatientReferalVisibleSection(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                dt = clshelper.DataTable("Usp_GetPatientReferralSectionVisibility", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("ReferralFormDAL - GetPatientReferalVisibleSection", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static DataTable GetTeamMemberDetailsOfDoctor(int ParentUserId, int PageIndex, int PageSize, BO.Enums.Common.PMSProviderFilterType ProviderType)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@ParentUserId", SqlDbType.Int);
                strParameter[0].Value = ParentUserId;
                strParameter[1] = new SqlParameter("@ProvTypefilter", SqlDbType.Int);
                strParameter[1].Value = ProviderType;

                dt = new clsHelper().DataTable("USP_GetTeamMemberListForOCR_New", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormDAL - GetTeamMemberDetailsOfDoctor", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        //public static DataTable GetTeamMemberDetailsOfDoctor(int ParentUserId, int PageIndex, int PageSize, BO.Enums.Common.PMSProviderFilterType ProviderType)
        //{
        //    try
        //    {
        //        DataTable dt = new DataTable();
        //        SqlParameter[] strParameter = new SqlParameter[4];
        //        strParameter[0] = new SqlParameter("@ParentUserId", SqlDbType.Int);
        //        strParameter[0].Value = ParentUserId;
        //        strParameter[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
        //        strParameter[1].Value = PageIndex;
        //        strParameter[2] = new SqlParameter("@PageSize", SqlDbType.Int);
        //        strParameter[2].Value = PageSize;
        //        strParameter[3] = new SqlParameter("@ProvTypefilter", SqlDbType.Int);
        //        strParameter[3].Value = ProviderType;

        //        dt = new clsHelper().DataTable("USP_GetTeamMemberListForOCR", strParameter);
        //        return dt;
        //    }
        //    catch (Exception Ex)
        //    {
        //        new clsCommon().InsertErrorLog("ReferralFormDAL - GetTeamMemberDetailsOfDoctor", Ex.Message, Ex.StackTrace);
        //        throw;
        //    }
        //}

        /// <summary>
        /// Save Sequence for Patient Referral Form Settings 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool SaveManageSequence(FieldSequence data)
        {
            SqlParameter[] param = new SqlParameter[7];
            param[0] = new SqlParameter("@KeyId", SqlDbType.Int);
            param[0].Value = data.KeyId;
            param[1] = new SqlParameter("@ParentKeyId", SqlDbType.Int);
            param[1].Value = data.ParentKeyId;
            param[2] = new SqlParameter("@SpecialityId", SqlDbType.Int);
            param[2].Value = data.SpecialityId;
            param[3] = new SqlParameter("@ServicesId", SqlDbType.Int);
            param[3].Value = data.ServicesId;
            param[4] = new SqlParameter("@NewSequence", SqlDbType.Int);
            param[4].Value = data.NewSequence;
            param[5] = new SqlParameter("@UserId", SqlDbType.Int);
            param[5].Value = data.UserId;
            param[6] = new SqlParameter("@TableName", SqlDbType.NVarChar);
            param[6].Value = data.TableName;

            return clshelper.ExecuteNonQuery("SaveManageSequence", param); ;
        }

        /// <summary>
        /// Save Sequence for Patient Referral Form Settings 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool SavePatientManageSequence(FieldSequence data)
        {
            SqlParameter[] param = new SqlParameter[6];
            param[0] = new SqlParameter("@KeyId", SqlDbType.Int);
            param[0].Value = data.KeyId;
            param[1] = new SqlParameter("@NewSequence", SqlDbType.Int);
            param[1].Value = data.NewSequence;
            param[2] = new SqlParameter("@ParentKeyId", SqlDbType.Int);
            param[2].Value = data.ParentKeyId;
            param[3] = new SqlParameter("@ServicesId", SqlDbType.Int); // ServicesId and FieldId both are same
            param[3].Value = data.ServicesId;
            param[4] = new SqlParameter("@SpecialityId", SqlDbType.Int); // SpecialityId and SectionId both are same
            param[4].Value = data.SpecialityId;
            param[5] = new SqlParameter("@UserId", SqlDbType.Int);
            param[5].Value = data.UserId;


            return clshelper.ExecuteNonQuery("SavePatientManageSequence", param); ;
        }
    }
}
