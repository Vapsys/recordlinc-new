﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Models;
using DataAccessLayer.Common;
using System.Web;

namespace DataAccessLayer.PlanTreatment
{
    public class PlanTreatmentDAL
    {
        public static clsHelper clshelper = new clsHelper();
        public static clsCommon objcommon = new clsCommon();
        public static DataTable GetAllCaseDefalutStepsList()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = clshelper.DataTable("GetAllCaseDefaultSteps", null);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("PlanTreatmentDAL--GetAllCaseDefaultSteps", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static int InsertPlanDetails(Cases _objCases)
        {           
            if (_objCases.TreatmentAccepted == false)
            {
                _objCases.TreatmentAcceptedDate = null;
            }
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[9];                
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = _objCases.PatientId;
                strParameter[1] = new SqlParameter("@Status", SqlDbType.Int);
                strParameter[1].Value = _objCases.Status;
                strParameter[2] = new SqlParameter("@TreatmentAccepted", SqlDbType.Bit);
                strParameter[2].Value = _objCases.TreatmentAccepted;
                strParameter[3] = new SqlParameter("@TreatmentAcceptedDate", SqlDbType.DateTime);
                strParameter[3].Value = _objCases.TreatmentAcceptedDate;
                strParameter[4] = new SqlParameter("@CreatedBy", SqlDbType.NVarChar);
                strParameter[4].Value = Convert.ToString(HttpContext.Current.Session["UserId"]);
                strParameter[5] = new SqlParameter("@CaseName", SqlDbType.NVarChar);
                strParameter[5].Value = _objCases.CaseName;
                strParameter[6] = new SqlParameter("@oCaseId", SqlDbType.Int);
                strParameter[6].Direction = ParameterDirection.Output;
                strParameter[7] = new SqlParameter("@CaseId", SqlDbType.Int);
                strParameter[7].Value = _objCases.CaseId;
                strParameter[8] = new SqlParameter("@CaseStepList", SqlDbType.Structured);
                strParameter[8].Value = clsCommon.ToDataTable(_objCases._caseStepList);                
                clshelper.DataTable("SP_InsertPlanDetails", strParameter);

                int result = Convert.ToInt32(strParameter[6].Value);
                return result;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("PlanTreatmentDAL--SP_InsertPlanDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool InsertUpdatePlanStepDetails(CaseSteps _objCaseSteps)
        {
            int result = 0;
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[11];
                strParameter[0] = new SqlParameter("@CaseStepId", SqlDbType.Int);
                strParameter[0].Value = _objCaseSteps.CaseStepId;
                strParameter[1] = new SqlParameter("@CaseId", SqlDbType.Int);
                strParameter[1].Value = _objCaseSteps.CaseId;
                strParameter[2] = new SqlParameter("@StepOrder", SqlDbType.NVarChar);
                strParameter[2].Value = _objCaseSteps.StepOrder;
                strParameter[3] = new SqlParameter("@Description", SqlDbType.NVarChar);
                strParameter[3].Value = _objCaseSteps.Description;
                strParameter[4] = new SqlParameter("@StartDate", SqlDbType.NVarChar);
                strParameter[4].Value = _objCaseSteps.StartDate;
                strParameter[5] = new SqlParameter("@EndDate", SqlDbType.NVarChar);
                strParameter[5].Value = _objCaseSteps.EndDate;
                strParameter[6] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[6].Value = _objCaseSteps.UserId;
                strParameter[7] = new SqlParameter("@Status", SqlDbType.Bit);
                strParameter[7].Value = _objCaseSteps.Status;
                strParameter[8] = new SqlParameter("@CreatedBy", SqlDbType.NVarChar);
                strParameter[8].Value = Convert.ToString(HttpContext.Current.Session["UserId"]);
                strParameter[9] = new SqlParameter("@ModifiedDate", SqlDbType.DateTime);
                strParameter[9].Value = _objCaseSteps.ModifiedAt;
                strParameter[10] = new SqlParameter("@ModifiedBy", SqlDbType.Int);
                strParameter[10].Value = _objCaseSteps.ModifiedBy;                

                result = Convert.ToInt32(clshelper.ExecuteNonQuery("SP_InsertUpdatePlanStepDetails", strParameter));

                return result > 0;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("PlanTreatmentDAL--SP_InsertUpdatePlanStepDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool CheckPatientPlanByPatientId(int _patientId)
        {
            int result = 0;
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = _patientId;               
                strParameter[1] = new SqlParameter("@CreatedBy", SqlDbType.NVarChar);
                strParameter[1].Value = Convert.ToString(HttpContext.Current.Session["UserId"]);

                result = Convert.ToInt32(clshelper.ExecuteScalar("SP_CheckPatientPlanByPatientId", strParameter));

                return result > 0;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("PlanTreatmentDAL--SP_CheckPatientPlanByPatientId", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public static DataSet GetPlanDetailsByCaseId(int _caseId, int _patientId)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = _patientId;
                strParameter[1] = new SqlParameter("@CaseId", SqlDbType.Int);
                strParameter[1].Value = _caseId;
                ds = clshelper.GetDatasetData("GetPlanDetailsByCaseId", strParameter);
                return ds;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("PlanTreatmentDAL--GetPlanDetailsByCaseId", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public static DataTable GetCaseReport(int PatientId, int UserId, string SortColumn, string SortDirection, int PageIndex, int PageSize, string SearchText, string IsTreatmentAccepted, string PatientName, string DoctorName, DateTime? StartDate, DateTime? EndDate,string PlanStatus)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[13];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = PatientId;
                strParameter[1] = new SqlParameter("@SortColumn", SqlDbType.NVarChar);
                strParameter[1].Value = SortColumn;
                strParameter[2] = new SqlParameter("@SortDirection", SqlDbType.NVarChar);
                strParameter[2].Value = SortDirection;
                strParameter[3] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[3].Value = PageIndex;
                strParameter[4] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[4].Value = PageSize;
                strParameter[5] = new SqlParameter("@searchtext", SqlDbType.NVarChar);
                strParameter[5].Value = SearchText;
                strParameter[6] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[6].Value = UserId;
                strParameter[7] = new SqlParameter("@IsTreatmentAccepted", SqlDbType.NVarChar);
                strParameter[7].Value = IsTreatmentAccepted;
                strParameter[8] = new SqlParameter("@PatientName", SqlDbType.NVarChar);
                strParameter[8].Value = PatientName;
                strParameter[9] = new SqlParameter("@StartDate", SqlDbType.DateTime);
                strParameter[9].Value = StartDate;
                strParameter[10] = new SqlParameter("@EndDate", SqlDbType.DateTime);
                strParameter[10].Value = EndDate;
                strParameter[11] = new SqlParameter("@DoctorName", SqlDbType.NVarChar);
                strParameter[11].Value = DoctorName;
                strParameter[12] = new SqlParameter("@PlanStatus", SqlDbType.NVarChar);
                strParameter[12].Value = PlanStatus;

                dt = clshelper.DataTable("Usp_GetCaseReport", strParameter);

                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("PlanTreatmentDAL--Usp_GetCaseReport", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static DataSet GetCaseByPatientId(int _patientId, string UserId)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = Convert.ToInt32(_patientId);
                strParameter[1] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[1].Value = Convert.ToInt32(UserId);
                ds = clshelper.GetDatasetData("GetCaseByPatientId", strParameter);
                return ds;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("PlanTreatmentDAL--GetCaseByPatientId", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public static bool DeletePlanStepbyId(int CasestepId,int UserId)
        {
            bool result = false;
            try
            {
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@CasestepId", SqlDbType.Int);
                strParameter[0].Value = CasestepId;
                strParameter[1] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[1].Value = UserId;
                return result = clshelper.ExecuteNonQuery("Usp_DeleteCaseStepById", strParameter);
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("PlanTreatmentDAL--GetPlanDetailsByCaseId", ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
