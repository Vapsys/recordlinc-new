﻿using System;
using System.Data;
using System.Data.SqlClient;
using DataAccessLayer.Common;
using BO.Models;
using BO.ViewModel;
using System.Collections.Generic;
using System.Linq;

namespace DataAccessLayer.ProfileData
{
    public class clsProfileData
    {
        clsHelper clshelper;
        clsCommon objcommon = new clsCommon();
        public clsProfileData()
        {
            clshelper = new clsHelper();
        }
        public DataTable getGallaryById(int UserId, int GallaryId)
        {  
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@GallaryId", SqlDbType.Int);
                strParameter[1].Value = GallaryId;
                dt = clshelper.DataTable("sp_GetGallary", strParameter);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool InsertUpdateGallery(int gallaryId, int userId, string MediaUrl, int MediaType)
        {

            
            try
            {
                
                SqlParameter[] sqlpara = new SqlParameter[5];
                sqlpara[0] = new SqlParameter("@MediaUrl", SqlDbType.NVarChar);
                sqlpara[0].Value = MediaUrl;
                sqlpara[1] = new SqlParameter("@MediaType", SqlDbType.Int);
                sqlpara[1].Value = MediaType;
                sqlpara[2] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlpara[2].Value = userId;
                sqlpara[3] = new SqlParameter("@CreatedBy", SqlDbType.Int);
                sqlpara[3].Value = userId;
                sqlpara[4] = new SqlParameter("@GallaryId", SqlDbType.Int);
                sqlpara[4].Value = gallaryId;

                return clshelper.ExecuteNonQuery("USP_InsertUpdateGallary", sqlpara);

                

            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool RemoveGallaryItem(int UserId, int GallaryId)
        {
            try
            {

                SqlParameter[] sqlpara = new SqlParameter[2];
                sqlpara[0] = new SqlParameter("@UserId", SqlDbType.NVarChar);
                sqlpara[0].Value = UserId;
                sqlpara[1] = new SqlParameter("@GallaryId", SqlDbType.Int);
                sqlpara[1].Value = GallaryId;
               

                return clshelper.ExecuteNonQuery("sp_DeleteGallary", sqlpara);



            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Member_Insurance

        public DataTable GetInsuranceDetail()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] addParameter = new SqlParameter[2];
                addParameter[0] = new SqlParameter("@PageIndex", SqlDbType.Int);
                addParameter[0].Value = 1;
                addParameter[1] = new SqlParameter("@PageSize", SqlDbType.Int);
                addParameter[1].Value = 100000;               
                dt = clshelper.DataTable("USP_GetAllInsuranceDetail", addParameter);
                return dt;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool InsertInsuranceMember(Insurance insurance)
        {
            bool result = false;
            try
            {
                
                SqlParameter[] sqlpara = new SqlParameter[4];
                sqlpara[0] = new SqlParameter("@Id", SqlDbType.Int);
                sqlpara[0].Value = insurance.Member_InsuranceId;
                sqlpara[1] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlpara[1].Value = insurance.UserId;
                sqlpara[2] = new SqlParameter("@InsuranceId", SqlDbType.Int);
                sqlpara[2].Value = insurance.InsuranceId;
                sqlpara[3] = new SqlParameter("@LocationId", SqlDbType.Int);
                sqlpara[3].Value = insurance.LocationId;
                result = clshelper.ExecuteNonQuery("USP_InsertUpdateMember_Insurance", sqlpara);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public bool RemoveInsuranceMember(int UserId,int LocationId = 0)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[2];
                sqlpara[0] = new SqlParameter("@UserId", SqlDbType.NVarChar);
                sqlpara[0].Value = UserId;
                sqlpara[1] = new SqlParameter("@LocationId", SqlDbType.Int);
                sqlpara[1].Value = LocationId;
                return clshelper.ExecuteNonQuery("USP_RemoveMember_Insurance", sqlpara);
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("clsProfileData--RemoveInsuranceMember", Ex.Message, Ex.StackTrace);

                throw;
            }
        } 

        public DataTable GetMemberInsuranceByUserId(int UserId,int LocationId = 0)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] addParameter = new SqlParameter[2];
                addParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addParameter[0].Value = UserId;
                addParameter[1] = new SqlParameter("@LocationId", SqlDbType.Int);
                addParameter[1].Value = LocationId;
                dt = clshelper.DataTable("USP_GetMember_InsuranceByUserId", addParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("clsProfileData--GetMemberInsuranceByUserId", Ex.Message, Ex.StackTrace);

                throw;
            }
        }

        public DataTable GetAllProviderListByUserId(int UserId,int LocationId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] addParameter = new SqlParameter[2];
                addParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addParameter[0].Value = UserId;
                addParameter[1] = new SqlParameter("@LocationId", SqlDbType.Int);
                addParameter[1].Value = LocationId;
                dt = clshelper.DataTable("USP_GetAllProviderListByUserId", addParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("clsProfileData--GetAllProviderListByUserId", Ex.Message, Ex.StackTrace);

                throw;
            }
        }

        public bool RemoveInsuranceProvider(int UserId, int InsuranceId)
        {
            try
            {
                
                SqlParameter[] addParameter = new SqlParameter[2];
                addParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addParameter[0].Value = UserId;
                addParameter[1] = new SqlParameter("@InsuranceId", SqlDbType.Int);
                addParameter[1].Value = InsuranceId;
                return clshelper.ExecuteNonQuery("USP_DeleteInsuranceProvider", addParameter);
            }
            catch (Exception Ex)
            {

                objcommon.InsertErrorLog("clsProfileData--RemoveInsuranceProvider", Ex.Message, Ex.StackTrace);
                throw;

            }
        }


        public DataTable GetMemberInsuranceByInsuranceId(int InsuranceId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] addParameter = new SqlParameter[1];
                addParameter[0] = new SqlParameter("@InsuranceId", SqlDbType.Int);
                addParameter[0].Value = InsuranceId;
                dt = clshelper.DataTable("USP_GetMember_InsuranceByInsuranceId", addParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("clsProfileData--GetMemberInsuranceByInsuranceId", Ex.Message, Ex.StackTrace);

                throw;
            }
        }

        public bool RemoveInsuranceMemberById(int Id,int userid)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[2];
                sqlpara[0] = new SqlParameter("@Id", SqlDbType.NVarChar);
                sqlpara[0].Value = Id;
                sqlpara[1] = new SqlParameter("@UserId", SqlDbType.NVarChar);
                sqlpara[1].Value = userid;
                return clshelper.ExecuteNonQuery("USP_RemoveInsurance_MemberById", sqlpara);
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("clsProfileData--RemoveInsuranceMemberById", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        #endregion
        #region PublicProfileSections
        public DataTable GetPublicProfileSectionDetials(int Userid)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[1];
                sqlpara[0] = new SqlParameter("@UserId", SqlDbType.NVarChar);
                sqlpara[0].Value = Userid;
                dt = clshelper.DataTable("Usp_GetPublicProfileSectionDetials", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("clsProfileData--GetPublicProfileSectionDetials", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        #endregion
        #region getVCardDetials of Doctor
        public DataTable GetVcardDetialsofDoctor(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[1];
                sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlpara[0].Value = UserId;
                dt = clshelper.DataTable("Usp_GetVcardDetialsOfDoctor", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("clsProfileData--GetVcardDetialsofDoctor", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        #endregion
        #region Insurance calculator cost estimation 
        public DataSet GetTotalCostEstimation(CalculateInsuranceModel Model,int UserId)
        {
            try
            {
                DataSet dt = new DataSet();
                SqlParameter[] sqlpara = new SqlParameter[8];
                sqlpara[0] = new SqlParameter("@ProcedureId", SqlDbType.NVarChar);
                sqlpara[0].Value = string.Join(",", Model.DentalProcedureId.Select(n => n.ToString()).ToArray());
                sqlpara[1] = new SqlParameter("@InsuranceId", SqlDbType.Int);
                sqlpara[1].Value = Model.InsuranceCompanyId;
                sqlpara[2] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlpara[2].Value = UserId;
                sqlpara[3] = new SqlParameter("@IsInsurace", SqlDbType.Bit);
                sqlpara[3].Value = Model.isInsurance;
                sqlpara[4] = new SqlParameter("@EstimateViaCall", SqlDbType.NVarChar);
                sqlpara[4].Value = Model.EstimateViaCall;
                sqlpara[5] = new SqlParameter("@EstimateViaEmail", SqlDbType.NVarChar);
                sqlpara[5].Value = Model.EstimateViaEmail;
                sqlpara[6] = new SqlParameter("@EstimateViaText", SqlDbType.NVarChar);
                sqlpara[6].Value = Model.EstimateViaText;
                sqlpara[7] = new SqlParameter("@FeeesType", SqlDbType.Int);
                sqlpara[7].Value = Model.FeesType;
                dt = clshelper.GetDatasetData("Usp_GetTotalAmountForInsuranceCalculator", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("clsProfileData--GetTotalCostEstimation", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        #endregion

        /// <summary>
        /// Get Member Public Profile Settings base on User Id
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns>Member Public Profile Settings</returns>
        public DataTable GetMemberPublicProfileSettings(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] addParameter = new SqlParameter[1];
                addParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addParameter[0].Value = UserId;
                dt = clshelper.DataTable("SP_GetPublicProfileSettings", addParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("clsProfileData--GetMemberPublicProfileSettings", Ex.Message, Ex.StackTrace);

                throw;
            }
        }

        /// <summary>
        /// update public profile url base on userid
        /// </summary>
        /// <returns>bool value</returns>
        public bool UpdatePublicProfileURL(ProfileSection profileSection)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[2];
                sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlpara[0].Value = profileSection.UserId;
                sqlpara[1] = new SqlParameter("@PublicProfileUrl", SqlDbType.NVarChar);
                sqlpara[1].Value = profileSection.PublicProfileUrl;
                return clshelper.ExecuteNonQuery("SP_UpdatePublicProfileUrl", sqlpara);
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("clsProfileData--UpdatePublicProfileURL", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
