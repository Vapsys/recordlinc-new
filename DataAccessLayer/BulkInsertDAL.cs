﻿using DataAccessLayer.Common;
using System;
using System.Configuration;

namespace DataAccessLayer
{
    public static class BulkInsertDAL
    {
        public static clsHelper ObjHelper = new clsHelper(clsCommon.SafeValue<string>(ConfigurationManager.ConnectionStrings["INTEGRATIONCONNECTION"].ConnectionString));

        public static bool BulkInsertProcedures()
        {
            ObjHelper.ExecuteNonQuery("Usp_InsertPatient_DentrixProcedures_merge", null, 15 * 1000);
            return true;
        }

        public static bool BulkInsertPatientAdjustments()
        {
            ObjHelper.ExecuteNonQuery("Usp_InsertUpdatePatientAdjustmentDetails_merge", null, 15 * 1000);
            return true;
        }

        public static bool BulkInsertPatientDentrixInsurance()
        {
            ObjHelper.ExecuteNonQuery("Usp_InsertPatient_DentrixInsuranceDetails_merge", null, 15 * 1000);
            return true;
        }

        public static bool BulkInsertPatientDentrixStandardPayment()
        {
            ObjHelper.ExecuteNonQuery("Usp_InsertPatient_DentrixStandardPayment_merge", null, 15 * 1000);
            return true;
        }

        public static bool BulkInsertPatientDentrixFinanceCharges()
        {
            ObjHelper.ExecuteNonQuery("Usp_InsertPatient_DentrixFinanceCharges_merge", null, 15 * 1000);
            return true;
        }

        public static bool BulkInsertPatientDentrixAccountAging()
        {
            ObjHelper.ExecuteNonQuery("Usp_InsertPatient_DentrixAccountAging_merge", null, 15 * 1000);
            return true;
        }
        public static bool BulkInsertPatientAppointment()
        {
            ObjHelper.ExecuteNonQuery("Usp_InsertPatient_DentrixAppointment_merge", null, 15 * 1000);
            return true;
        }
        public static bool InsuranceData()
        {
            ObjHelper.ExecuteNonQuery("Usp_AddInsuranceData_merge", null, 15 * 1000);
            return true;
        }
    }
}
