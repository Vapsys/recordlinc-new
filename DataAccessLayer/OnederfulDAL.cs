﻿using DataAccessLayer.Common;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DataAccessLayer
{
    public class OnederfulDAL
    {
        public static bool InsertPatientInsuranceDetails(int PatientId, string InsuranceJSON,int UserId,int AccountId,bool IsError = false)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                param[0].Value = PatientId;
                param[1] = new SqlParameter("@JSON", SqlDbType.NVarChar);
                param[1].Value = InsuranceJSON;
                param[2] = new SqlParameter("@AccountId", SqlDbType.NVarChar);
                param[2].Value = AccountId;
                param[3] = new SqlParameter("@UserId", SqlDbType.NVarChar);
                param[3].Value = UserId;
                param[4] = new SqlParameter("@IsError", SqlDbType.Bit);
                param[4].Value = IsError;
                bool Result = new clsHelper().ExecuteNonQuery("Usp_InsertOnederfulPatInsInfo", param);
                return Result;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("OnederfulBLL-CallOnederfulAPI", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
