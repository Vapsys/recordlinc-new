﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Common;
using System.Data;
using System.Data.SqlClient;
using BO.ViewModel;
using BO.Models;

namespace DataAccessLayer.AppointmentData
{
    public class clsAppointment
    {
        clsCommon objCommon = new clsCommon();
        clsHelper objHelper = new clsHelper();

        public void InsertAppointmentWeekSchedule(int AppointmentWeekScheduleId, int AppointmentWorkingTimeAllocationId, int DoctorId, int Day, bool IsOffDay, TimeSpan? MorningFromTime, TimeSpan? MorningToTime, TimeSpan? EveningFromTime, TimeSpan? EveningToTime, DateTime CreatedOn)
        {
            bool bl = false;
            SqlParameter[] sqlpara = new SqlParameter[10];
            sqlpara[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
            sqlpara[0].Value = DoctorId;
            sqlpara[1] = new SqlParameter("@Day", SqlDbType.Int);
            sqlpara[1].Value = Day;
            sqlpara[2] = new SqlParameter("@IsOffDay", SqlDbType.Text);
            sqlpara[2].Value = IsOffDay ? "Y" : "N";
            sqlpara[3] = new SqlParameter("@MorningFromTime", SqlDbType.Time);
            sqlpara[3].Value = MorningFromTime;
            sqlpara[4] = new SqlParameter("@MorningToTime", SqlDbType.Time);
            sqlpara[4].Value = MorningToTime;
            sqlpara[5] = new SqlParameter("@EveningFromTime", SqlDbType.Time);
            sqlpara[5].Value = EveningFromTime;
            sqlpara[6] = new SqlParameter("@EveningToTime", SqlDbType.Time);
            sqlpara[6].Value = EveningToTime;
            sqlpara[7] = new SqlParameter("@CreatedOn", SqlDbType.DateTime);
            sqlpara[7].Value = CreatedOn;
            sqlpara[8] = new SqlParameter("@AppointmentWeekScheduleId", SqlDbType.Int);
            sqlpara[8].Value = AppointmentWeekScheduleId;
            sqlpara[9] = new SqlParameter("@AppointmentWorkingTimeAllocationId", SqlDbType.Int);
            sqlpara[9].Value = AppointmentWorkingTimeAllocationId;

            bl = objHelper.ExecuteNonQuery("Usp_InsertUpdateWeekSchedule", sqlpara);
        }

        public bool UpdateAppointmentWeekSchedule(int AppointmentWeekScheduleId, int AppointmentWorkingTimeAllocationId, bool IsOffDay, TimeSpan? MorningFromTime, TimeSpan? MorningToTime, TimeSpan? EveningFromTime, TimeSpan? EveningToTime, int DoctorId, DateTime CreatedOn, int Day)
        {
            bool UpdateStatus = false;
            SqlParameter[] sqlpara = new SqlParameter[10];
            sqlpara[0] = new SqlParameter("@AppointmentWeekScheduleId", SqlDbType.Int);
            sqlpara[0].Value = AppointmentWeekScheduleId;
            sqlpara[1] = new SqlParameter("@AppointmentWorkingTimeAllocationId", SqlDbType.Int);
            sqlpara[1].Value = AppointmentWorkingTimeAllocationId;
            sqlpara[2] = new SqlParameter("@IsOffDay", SqlDbType.Text);
            sqlpara[2].Value = IsOffDay ? "Y" : "N";
            sqlpara[3] = new SqlParameter("@MorningFromTime", SqlDbType.Time);
            sqlpara[3].Value = MorningFromTime;
            sqlpara[4] = new SqlParameter("@MorningToTime", SqlDbType.Time);
            sqlpara[4].Value = MorningToTime;
            sqlpara[5] = new SqlParameter("@EveningFromTime", SqlDbType.Time);
            sqlpara[5].Value = EveningFromTime;
            sqlpara[6] = new SqlParameter("@EveningToTime", SqlDbType.Time);
            sqlpara[6].Value = EveningToTime;
            sqlpara[7] = new SqlParameter("@DoctorId", SqlDbType.Int);
            sqlpara[7].Value = DoctorId;
            sqlpara[8] = new SqlParameter("@Day", SqlDbType.Int);
            sqlpara[8].Value = Day;
            sqlpara[9] = new SqlParameter("@CreatedOn", SqlDbType.DateTime);
            sqlpara[9].Value = CreatedOn;

            UpdateStatus = objHelper.ExecuteNonQuery("Usp_InsertUpdateWeekSchedule", sqlpara);
            return UpdateStatus;
        }

        public DataTable GetDoctorAppointmentWeekScheduleDetails(int DoctorId)
        {
            try
            {
                DataTable ds = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[1];
                sqlpara[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                sqlpara[0].Value = DoctorId;
                ds = objHelper.DataTable("Usp_GetDoctorAppointmentWeekScheduleDetails", sqlpara);

                return ds;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public DataTable GetDoctorSpecialDaySchedule(int DoctorId, DateTime Date)
        {
            try
            {
                DataTable ds = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[2];
                sqlpara[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                sqlpara[0].Value = DoctorId;
                sqlpara[1] = new SqlParameter("@Date", SqlDbType.Date);
                sqlpara[1].Value = Date;
                ds = objHelper.DataTable("Usp_GetSpecialDaySchedule", sqlpara);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetDoctorSpecialDayScheduleDate(int DoctorId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[1];
                sqlpara[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                sqlpara[0].Value = DoctorId;
                dt = objHelper.DataTable("Usp_GetDoctorSpecialDayScheduleDates", sqlpara);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void InsertSpecialDaySchedule(int AppointmentSpecialDayScheduleId, int DoctorId, DateTime Date, bool IsOffDay, int AppointmentWorkingTimeAllocationId, TimeSpan? MorningFromTime, TimeSpan? MorningToTime)
        {
            bool bl = false;
            SqlParameter[] sqlpara = new SqlParameter[7];
            sqlpara[0] = new SqlParameter("@AppointmentSpecialDayScheduleId", SqlDbType.Int);
            sqlpara[0].Value = AppointmentSpecialDayScheduleId;
            sqlpara[1] = new SqlParameter("@DoctorId", SqlDbType.Int);
            sqlpara[1].Value = DoctorId;
            sqlpara[2] = new SqlParameter("@Date", SqlDbType.Date);
            sqlpara[2].Value = Date;
            sqlpara[3] = new SqlParameter("@IsOffDay", SqlDbType.Text);
            sqlpara[3].Value = IsOffDay ? "Y" : "N";
            sqlpara[4] = new SqlParameter("@AppointmentWorkingTimeAllocationId", SqlDbType.Int);
            sqlpara[4].Value = AppointmentWorkingTimeAllocationId;
            sqlpara[5] = new SqlParameter("@MorningFromTime", SqlDbType.Time);
            sqlpara[5].Value = MorningFromTime;
            sqlpara[6] = new SqlParameter("@MorningToTime", SqlDbType.Time);
            sqlpara[6].Value = MorningToTime;

            bl = objHelper.ExecuteNonQuery("Usp_InsertUpdateSpecialDaySchedule", sqlpara);
        }
        public bool UpdateSpecialDaySchedule(int AppointmentSpecialDayScheduleId, int DoctorId, DateTime Date, bool IsOffDay, int AppointmentWorkingTimeAllocationId, TimeSpan? MorningFromTime, TimeSpan? MorningToTime)
        {
            bool bl = false;
            SqlParameter[] sqlpara = new SqlParameter[7];
            sqlpara[0] = new SqlParameter("@AppointmentSpecialDayScheduleId", SqlDbType.Int);
            sqlpara[0].Value = AppointmentSpecialDayScheduleId;
            sqlpara[1] = new SqlParameter("@DoctorId", SqlDbType.Int);
            sqlpara[1].Value = DoctorId;
            sqlpara[2] = new SqlParameter("@Date", SqlDbType.Date);
            sqlpara[2].Value = Date;
            sqlpara[3] = new SqlParameter("@IsOffDay", SqlDbType.Text);
            sqlpara[3].Value = IsOffDay ? "Y" : "N";
            sqlpara[4] = new SqlParameter("@AppointmentWorkingTimeAllocationId", SqlDbType.Int);
            sqlpara[4].Value = AppointmentWorkingTimeAllocationId;
            sqlpara[5] = new SqlParameter("@MorningFromTime", SqlDbType.Time);
            sqlpara[5].Value = MorningFromTime;
            sqlpara[6] = new SqlParameter("@MorningToTime", SqlDbType.Time);
            sqlpara[6].Value = MorningToTime;

            bl = objHelper.ExecuteNonQuery("Usp_InsertUpdateSpecialDaySchedule", sqlpara);
            return bl;
        }
        public DataTable GetDoctorAppointmentData(int DoctorId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[1];
                sqlpara[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                sqlpara[0].Value = DoctorId;

                dt = objHelper.DataTable("Usp_GetAppointmentDashboardNotification", sqlpara);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public DataTable GetPatientAppointmentView(int PatientId, int ShortColumn, int ShortDirection, DateTime? Fromdate = null, DateTime? Todate = null)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[5];
                sqlpara[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                sqlpara[0].Value = PatientId;
                sqlpara[1] = new SqlParameter("@ShortColumn", SqlDbType.Int);
                sqlpara[1].Value = ShortColumn;
                sqlpara[2] = new SqlParameter("@ShortDirection", SqlDbType.Int);
                sqlpara[2].Value = ShortDirection;
                sqlpara[3] = new SqlParameter("@Fromdate", SqlDbType.DateTime);
                sqlpara[3].Value = Fromdate;
                sqlpara[4] = new SqlParameter("@ToDate", SqlDbType.DateTime);
                sqlpara[4].Value = Todate;
                dt = objHelper.DataTable("Usp_GetPatientAppointmentList", sqlpara);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetTreatingDoctorListForBookAppointmentByPatientId(int PatientId, int DoctorId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[2];
                sqlpara[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                sqlpara[0].Value = PatientId;
                sqlpara[1] = new SqlParameter("@DoctorId", SqlDbType.Int);
                sqlpara[1].Value = DoctorId;
                dt = objHelper.DataTable("Usp_GetTreatingDoctorListForBookAppointment_MyDentalFile", sqlpara);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public DataTable GetDoctorServicesByDoctorId(int DoctorId, int AppointmentServiceId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[2];
                sqlpara[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                sqlpara[0].Value = DoctorId;
                sqlpara[1] = new SqlParameter("@AppointmentServiceId", SqlDbType.Int);
                sqlpara[1].Value = AppointmentServiceId;
                dt = objHelper.DataTable("Usp_GetAppointmentServicesByDoctorId_MyDentalFile", sqlpara);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public DataTable GetOprationTheatorForCalendarByLocation(int LocationId)
        {
            return objHelper.DataTable("USP_GetAppointmentDataForCalanderByLocation", new SqlParameter[] {
                new SqlParameter() { ParameterName = "@LocationId", Value = LocationId },
                });
        }
        public DataTable GetDoctorDetails(int DoctorId)
        {
            return objHelper.DataTable("USP_GetDoctorDetails", new SqlParameter[] {
                new SqlParameter() { ParameterName = "@DoctorId", Value = DoctorId },
            });
        }
        public DataTable CancleAppointmentByPatient(int AppointmentId, int CancelledBy, int CancelledByUserType, string CancelNote, int AppointmentStatusId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[5];
                sqlpara[0] = new SqlParameter("@AppointmentId", SqlDbType.Int);
                sqlpara[0].Value = AppointmentId;
                sqlpara[1] = new SqlParameter("@CancelledBy", SqlDbType.Int);
                sqlpara[1].Value = CancelledBy;
                sqlpara[2] = new SqlParameter("@CancelledByUserType", SqlDbType.Int);
                sqlpara[2].Value = CancelledByUserType;
                sqlpara[3] = new SqlParameter("@CancelNote", SqlDbType.Text);
                sqlpara[3].Value = CancelNote;
                sqlpara[4] = new SqlParameter("@AppointmentStatusId", SqlDbType.Int);
                sqlpara[4].Value = AppointmentStatusId;
                dt = objHelper.DataTable("Usp_CancleAppintmentFormPatient_MydentalFile", sqlpara);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int SubmitAppointmentBookingData(int DoctorId, int PatientId, DateTime AppointmentDate, TimeSpan AppointmentTime, double AppointmentLength, int ServiceId, string Note, int CreatedBy, int CreatedByUserType, int ApptResourceId, int UpdatedBy, int AppointmentId, int InsuranceId)
        {
            return Convert.ToInt32(objHelper.DataTable("USP_CreateAppointmentByDoctor", new SqlParameter[] {
                new SqlParameter() {ParameterName ="@DoctorId",Value= DoctorId},
                new SqlParameter() {ParameterName ="@PatientId",Value= PatientId},
                new SqlParameter() {ParameterName ="@AppointmentDate",Value= AppointmentDate},
                new SqlParameter() {ParameterName ="@AppointmentTime",Value= AppointmentTime},
                new SqlParameter() {ParameterName ="@AppointmentLength",Value= AppointmentLength},
                new SqlParameter() {ParameterName ="@ServiceTypeId",Value=ServiceId },
                new SqlParameter() {ParameterName ="@Note",Value= Note},
                new SqlParameter() {ParameterName = "@CreatedBy",Value=CreatedBy},
                new SqlParameter() {ParameterName = "@CreatedByUserType",Value=CreatedByUserType},
                new SqlParameter() {ParameterName = "@ApptResourceId",Value=ApptResourceId},
                new SqlParameter() {ParameterName = "@UpdatedBy",Value=UpdatedBy},
                new SqlParameter() {ParameterName = "@AppointmentId",Value=AppointmentId},
                new SqlParameter() {ParameterName = "@InsuranceId",Value=InsuranceId}
            }).Rows[0]["AppointmentId"]);
        }
        //public DataSet GetCreateAppointmentTimeSlotViewData(int DoctorId)
        //{
        //    return objHelper.GetDatasetData("USP_GetCreateAppointmentDataForTimeAllocation", new SqlParameter[] { new SqlParameter() { ParameterName = "@DoctorId", Value = DoctorId } });
        //}
        public DataSet GetCreateAppointmentTimeSlotViewData(int DoctorId, int TheatreID, int ApptointmentId)
        {
            return objHelper.GetDatasetData("USP_GetCreateAppointmentDataForTimeAllocation", new SqlParameter[]
            { new SqlParameter() {ParameterName = "@DoctorId", Value = DoctorId },
              new SqlParameter() {ParameterName ="@TheatreID",Value= TheatreID},
              new SqlParameter() {ParameterName ="@ApptointmentId",Value= ApptointmentId}
            });
        }
        public DataTable CheckAvalibleDoctorByDateTimeTheatreDoctor(int DoctorId, TimeSpan AppointmentFromTime, TimeSpan AppointmentToTime, DateTime AppintmentDate, int ApptointmentId)
        {
            return objHelper.DataTable("CheckAvalibleDoctorByDateTimeTheatreDoctor", new SqlParameter[] {
                new SqlParameter() {ParameterName ="@DoctorId",Value= DoctorId},
                new SqlParameter() {ParameterName ="@AppointmentFromTime",Value= AppointmentFromTime},
                 new SqlParameter() {ParameterName ="@AppointmentToTime",Value= AppointmentToTime},
                new SqlParameter() {ParameterName ="@AppintmentDate",Value= AppintmentDate},
                  new SqlParameter() {ParameterName ="@ApptointmentId",Value= ApptointmentId},
                });
        }
        public DataTable CheckAvalibleDoctorByDateTimeTheatrePatientId(int PatientId, TimeSpan AppointmentFromTime, TimeSpan AppointmentToTime, DateTime AppintmentDate, int ApptointmentId)
        {
            return objHelper.DataTable("CheckAvalibleDoctorByDateTimeTheatrePatientId", new SqlParameter[] {
                new SqlParameter() {ParameterName ="@PatientId",Value= PatientId},
                new SqlParameter() {ParameterName ="@AppointmentFromTime",Value= AppointmentFromTime},
                 new SqlParameter() {ParameterName ="@AppointmentToTime",Value= AppointmentToTime},
                new SqlParameter() {ParameterName ="@AppintmentDate",Value= AppintmentDate},
                new SqlParameter() {ParameterName ="@ApptointmentId",Value= ApptointmentId},

                });
        }
        public DataTable GetEmailAllFieldForCreateAppointmentByAppointmentId(int AppointmentId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[1];
                sqlpara[0] = new SqlParameter("@AppointmentId", SqlDbType.Int);
                sqlpara[0].Value = AppointmentId;
                dt = objHelper.DataTable("Usp_EmailNotificationforCreateAppointment", sqlpara);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public DataTable GetInsuredAppointmentListDLL(int UserId, int SortColumn, int SortDirection, int PageIndex, int PageSize, string SearchText)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[6];
                sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlpara[0].Value = UserId;
                sqlpara[1] = new SqlParameter("@SortColumn", SqlDbType.Int);
                sqlpara[1].Value = SortColumn;
                sqlpara[2] = new SqlParameter("@SortDirection", SqlDbType.Int);
                sqlpara[2].Value = SortDirection;
                sqlpara[3] = new SqlParameter("@PageIndex", SqlDbType.Int);
                sqlpara[3].Value = PageIndex;
                sqlpara[4] = new SqlParameter("@PageSize", SqlDbType.Int);
                sqlpara[4].Value = PageSize;
                sqlpara[5] = new SqlParameter("@searchtext", SqlDbType.NVarChar);
                sqlpara[5].Value = SearchText;
                dt = objHelper.DataTable("Usp_verificationOfBenefitsReport", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("clsAppointmentData--GetInsuredAppointmentListDLL", Ex.Message, Ex.StackTrace);
                throw;
            }
        }


        /// <summary>
        /// List of Operatory (OperatoryList)
        /// </summary>
        /// <param name="DentrixConnectorID"></param>
        /// <param name="LocationId"></param>
        /// <returns>List of Opration Room</returns>
        public DataTable OperatoryList(int LocationId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[1];
                sqlpara[0] = new SqlParameter("@LocationId", SqlDbType.Int);
                sqlpara[0].Value = LocationId;
                dt = objHelper.DataTable("Usp_OperatoryList", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("clsAppointmentData--OperatoryList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// List of doctor services
        /// </summary>
        /// <param name="DentrixConnectorID"></param>
        /// <returns>services</returns>
        public DataTable DoctorServices(int LocationId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[1];
                sqlpara[0] = new SqlParameter("@LocationId", SqlDbType.Int);
                sqlpara[0].Value = LocationId;
                dt = objHelper.DataTable("Usp_DoctorServices", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("clsAppointmentData--DoctorServices", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Doctor time slot for appointment book
        /// </summary>
        /// <param name="DentrixConnectorID"></param>
        /// <returns>List of available time slot</returns>
        public DataTable DoctorTimeSlot(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[1];
                sqlpara[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                sqlpara[0].Value = UserId;
                dt = objHelper.DataTable("Usp_GetDoctorAppointmentWeekScheduleDetails", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("clsAppointmentData--DoctorTimeSloat", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get List of available appointment listing
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>List of available appointment</returns>
        public DataTable AppointmentListing(int UserId, DentrixAppointment obj)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[7];
                sqlpara[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                sqlpara[0].Value = UserId;
                //sqlpara[1] = new SqlParameter("@StartDate", SqlDbType.DateTime);
                //sqlpara[1].Value = obj.StartDate;
                //sqlpara[2] = new SqlParameter("@EndDate", SqlDbType.DateTime);
                //sqlpara[2].Value = obj.EndDate;
                sqlpara[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                sqlpara[1].Value = obj.PageIndex;
                sqlpara[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                sqlpara[2].Value = obj.PageSize;
                sqlpara[3] = new SqlParameter("@FilterByDentistId", SqlDbType.Int);
                sqlpara[3].Value = obj.DentistId;
                sqlpara[4] = new SqlParameter("@FilterByStatusId", SqlDbType.Int);
                sqlpara[4].Value = obj.StatusId;
                sqlpara[5] = new SqlParameter("@FilterByLocationId", SqlDbType.Int);
                sqlpara[5].Value = obj.LocationId;
                sqlpara[6] = new SqlParameter("@FilterByPatientName", SqlDbType.NVarChar);
                sqlpara[6].Value = obj.PatientName;
                dt = objHelper.DataTable("Usp_GetDentrixAppointmentListing", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("clsAppointment--AppointmentListing", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// get all patient base on doctor
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public DataTable PatientListing(int UserId, PostPatient obj)
        {
            try
            {
                return objHelper.DataTable("USP_GetPatients", new SqlParameter[] {
                new SqlParameter() {ParameterName ="@DoctorId",Value= UserId},
                new SqlParameter() {ParameterName ="@PageIndex",Value= obj.PageIndex},
                 new SqlParameter() {ParameterName ="@PageSize",Value= obj.PageSize},
                new SqlParameter() {ParameterName ="@SearchText",Value= obj.SearchText},
                });
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("clsAppointment--PatientListing", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get team member list
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="obj"></param>
        /// <returns>dataset</returns>
        public DataSet TeamMembersListing(int UserId, PostTeamMember obj)
        {
            try
            {
                return objHelper.GetDatasetData("USP_GetTeamMember", new SqlParameter[] {
                new SqlParameter() {ParameterName ="@UserId",Value= UserId},
                new SqlParameter() {ParameterName ="@PageIndex",Value= obj.PageIndex},
                 new SqlParameter() {ParameterName ="@PageSize",Value= obj.PageSize},
                new SqlParameter() {ParameterName ="@SearchText",Value= obj.SearchText},
                new SqlParameter() {ParameterName ="@IncludeUser",Value= obj.IncludeUser},
                });
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("clsAppointment--TeamMembersListing", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Book appointment 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>Inserted appointmentid</returns>
        public int AppointmentInsert(int UserId, BookAppointment obj)
        {
            try
            {
                return Convert.ToInt32(objHelper.DataTable("USP_BookAppointmentByDoctor", new SqlParameter[] {
                new SqlParameter() {ParameterName ="@AppointmentId",Value= obj.Id},
                new SqlParameter() {ParameterName ="@DoctorId",Value= obj.DoctorId},
                new SqlParameter() {ParameterName ="@ProviderId",Value= obj.ProviderId},
                new SqlParameter() {ParameterName ="@AppointmentDate",Value= obj.Date},
                new SqlParameter() {ParameterName ="@AppointmentTime",Value= obj.Time},
                new SqlParameter() {ParameterName ="@DurationInMinutes",Value= obj.DurationInMinutes},
                new SqlParameter() {ParameterName ="@ServiceId",Value=obj.ServiceId },
                new SqlParameter() {ParameterName ="@ResourceId",Value=obj.ServiceId },
                new SqlParameter() {ParameterName ="@OperatoryId",Value=obj.ServiceId },
                new SqlParameter() {ParameterName ="@Note",Value= obj.Note},
                new SqlParameter() {ParameterName ="@DentrixConnectorID",Value= obj.DentrixConnectorID},
                new SqlParameter() {ParameterName ="@InsuranceId",Value= obj.InsuranceId},
                new SqlParameter() {ParameterName ="@PatientId",Value= obj.PatientId},
                new SqlParameter() {ParameterName ="@CreatedBy",Value= UserId}
             }).Rows[0]["AppointmentId"]);
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("clsAppointment--AppointmentInsert", Ex.Message, Ex.StackTrace);
                return 0;
            }
        }

        /// <summary>
        /// Get write back appointment list
        /// </summary>
        /// <param name="DoctorId"></param>
        /// <param name="DentrixConnectorKey"></param>
        /// <returns></returns>
        public static DataTable GetWriteBackAPpointmentList(int DoctorId,string DentrixConnectorKey)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[2];
                sqlpara[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                sqlpara[0].Value = DoctorId;
                sqlpara[1] = new SqlParameter("@DentrixConnectorId", SqlDbType.NVarChar);
                sqlpara[1].Value = DentrixConnectorKey;
                dt = new clsHelper().DataTable("Usp_GetWriteBackTempAppointmentList", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsAppointment--GetWriteBackAPpointmentList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Appointment writeback for PMS
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns>booked appointment list</returns>
        public DataTable AppointmentWriteback(WritebackAppointment Obj)
        {
            try
            {
                return objHelper.DataTable("USP_AppointmentWriteback", new SqlParameter[] {
                new SqlParameter() {ParameterName ="@PMSConnectorId",Value= Obj.PMSConnectorId},
                new SqlParameter() {ParameterName ="@PageIndex",Value= Obj.PageIndex},
                 new SqlParameter() {ParameterName ="@PageSize",Value= Obj.FetchRecordCount},
                new SqlParameter() {ParameterName ="@StartDate",Value= Obj.Startdate},
                new SqlParameter() {ParameterName ="@EndDate",Value= Obj.Enddate},
                });
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("clsAppointment--PatientListing", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Appointment writeback response
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public int PMSAppointmentWritebackResponse(string PMSConnectorId,DMPSAppointmentResponsedData Obj)
        {
            try
            {
                return Convert.ToInt32(objHelper.DataTable("USP_AppointmentWritebackFieldUpdate", new SqlParameter[] {
                new SqlParameter() {ParameterName ="@PMSConnectorId",Value= PMSConnectorId},
                new SqlParameter() {ParameterName ="@AppointmentId",Value= Obj.AppointmentId},
                new SqlParameter() {ParameterName ="@RLAppointmentId",Value= Obj.RLAppointmentId},
                 new SqlParameter() {ParameterName ="@RLPendingAppointmentId",Value= Obj.RLPendingAppointmentId}
                 }).Rows[0]["PMSId"]);
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("clsAppointment--PMSAppointmentWritebackResponse", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetSchedules(FilterSchedule filterSchedule)
        {
            try
            {
                return objHelper.DataTable("Usp_GetDentrixSchedule", new SqlParameter[] {
                new SqlParameter() {ParameterName ="@ScheduleType",Value= (int)filterSchedule.scheduleType},
                new SqlParameter() {ParameterName ="@DentrixConnectorKey",Value= filterSchedule.DentrixConnectorId}});
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("clsAppointment--GetSchedules", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
