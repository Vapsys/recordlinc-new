﻿using System;
using System.Data;
using System.Data.SqlClient;
using BO.ViewModel;
using DataAccessLayer.Common;
namespace DataAccessLayer
{
    public class DentistRatingDAL
    {
        clsHelper clshelper;
        const string OPPORTUNITY_TYPE = "Dentist";
        public DentistRatingDAL()
        {
            clshelper = new clsHelper();
        }
        public DataTable getDentistRatings(int UserId, int RatingId, int status, int page, int PageSize=10, int SortDirection = 2, int SortColumn = 1)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[8];
                strParameter[0] = new SqlParameter("@OpportunityType", SqlDbType.NVarChar);
                strParameter[0].Value = OPPORTUNITY_TYPE;
                strParameter[1] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[1].Value = UserId;
                strParameter[2] = new SqlParameter("@RatingId", SqlDbType.Int);
                strParameter[2].Value = RatingId;
                strParameter[3] = new SqlParameter("@Status", SqlDbType.Int);
                strParameter[3].Value = status;
                strParameter[4] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[4].Value = PageSize;
                strParameter[5] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[5].Value = SortColumn;
                strParameter[6] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[6].Value = SortDirection;
                strParameter[7] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[7].Value = page;


                dt = clshelper.DataTable("SP_GetRating", strParameter);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool InsertRatings(DentistRatingModel model)
        {
            int result = 0;
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[6];
                strParameter[0] = new SqlParameter("@OpportunityType", SqlDbType.NVarChar);
                strParameter[0].Value = OPPORTUNITY_TYPE;
                strParameter[1] = new SqlParameter("@RatingValue", SqlDbType.Decimal);
                strParameter[1].Value = model.ratings.RatingValue;
                strParameter[2] = new SqlParameter("@SenderName", SqlDbType.NVarChar);
                strParameter[2].Value = model.ratings.SenderName;
                strParameter[3] = new SqlParameter("@SenderEmail", SqlDbType.NVarChar);
                strParameter[3].Value = model.ratings.SenderEmail;
                strParameter[4] = new SqlParameter("@RatingMessage", SqlDbType.NVarChar);
                strParameter[4].Value = model.ratings.RatingMessage;
                strParameter[5] = new SqlParameter("@ReceiverId", SqlDbType.Int);
                strParameter[5].Value = model.ratings.ReceiverId;

                result = Convert.ToInt32(clshelper.ExecuteScalar("SP_InsertRating", strParameter));

                return result > 0;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int GetDentistRatingsCount(int userid, int status)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[4];
                strParameter[0] = new SqlParameter("@OpportunityType", SqlDbType.NVarChar);
                strParameter[0].Value = OPPORTUNITY_TYPE;
                strParameter[1] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[1].Value = userid;
                strParameter[2] = new SqlParameter("@RatingId", SqlDbType.Int);
                strParameter[2].Value = 0;
                strParameter[3] = new SqlParameter("@Status", SqlDbType.Int);
                strParameter[3].Value = status;
                return Convert.ToInt32(clshelper.ExecuteScalar("SP_CountRating", strParameter));
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool ApproveDeleteRating(int RatingId,int UserId,int Status)
        {
            int result = 0;
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@RatingId", SqlDbType.NVarChar);
                strParameter[0].Value = RatingId;
                strParameter[1] = new SqlParameter("@UserId", SqlDbType.Decimal);
                strParameter[1].Value = UserId;
                strParameter[2] = new SqlParameter("@Status", SqlDbType.NVarChar);
                strParameter[2].Value = Status;
                
                result = Convert.ToInt32(clshelper.ExecuteScalar("USP_ApproveDeleteRating", strParameter));

                return result > 0;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
