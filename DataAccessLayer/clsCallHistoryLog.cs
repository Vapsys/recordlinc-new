﻿using BO.Models;
using DataAccessLayer.Common;
using System.Data;
using System.Data.SqlClient;

namespace DataAccessLayer
{
    public class clsCallHistoryLog
    {

        public static bool InsertCostEstimatorCallLog(CallHistoryModel model)
        {
            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@Content", SqlDbType.NVarChar);
            param[0].Value = model.Content;
            param[1] = new SqlParameter("@PhoneNumber", SqlDbType.NVarChar);
            param[1].Value = model.PhoneNumber;
            param[2] = new SqlParameter("@STATUS", SqlDbType.Bit);
            param[2].Value = model.STATUS;
            param[3] = new SqlParameter("@APIMessage", SqlDbType.NVarChar);
            param[3].Value = model.APIMessage;
            param[4] = new SqlParameter("@APIStatus", SqlDbType.Int);
            param[4].Value = model.APIStatus;
            param[5] = new SqlParameter("@APICode", SqlDbType.Int);
            param[5].Value = model.APICode;
            param[6] = new SqlParameter("@APIMoreInfo", SqlDbType.NVarChar);
            param[6].Value = model.APIMoreInfo;
            param[7] = new SqlParameter("@APISId", SqlDbType.NVarChar);
            param[7].Value = model.APISId;
            param[8] = new SqlParameter("@CallType", SqlDbType.Int);
            param[8].Value = model.CallType;
            param[9] = new SqlParameter("@UserId", SqlDbType.Int);
            param[9].Value = model.UserId;

            return new clsHelper().ExecuteNonQuery("USP_InsertCallHistory", param);
        }

    }
}
