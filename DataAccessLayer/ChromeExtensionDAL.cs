﻿using BO.Models;
using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class ChromeExtensionDAL
    {
        static clsHelper objhelper = new clsHelper();
        clsCommon objcommon = new clsCommon();
        /// <summary>
        /// Get Un-Read Referral Count for Chrome Extension
        /// </summary>
        /// <param name="Filter">Filter Message Object</param>
        /// <returns></returns>
        public static DataTable GetUnreadReferralCount(FilterMessageConversation Filter)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[] {
                new SqlParameter() {ParameterName = "@MessageId",Value= Filter.MessageId },
                new SqlParameter() {ParameterName = "@UserId",Value= Filter.UserId },
                new SqlParameter() {ParameterName = "@PageSize",Value= Filter.PageSize },
                new SqlParameter() {ParameterName = "@PageIndex",Value= Filter.PageIndex},
                new SqlParameter() {ParameterName = "@SortColumn",Value= Filter.SortColumn },
                new SqlParameter() {ParameterName = "@SortDirection",Value= Filter.SortDirection },
                new SqlParameter() {ParameterName = "@DoctorName",Value= Filter.DoctorName },
                new SqlParameter() {ParameterName = "@FromDate",Value= Filter.FromDate},
                new SqlParameter() {ParameterName = "@Todate",Value= Filter.Todate},
                new SqlParameter() {ParameterName = "@Disposition",Value= Filter.Disposition },
                new SqlParameter() {ParameterName = "@Message",Value= Filter.Message },
                new SqlParameter() {ParameterName = "@MessageStatus",Value= Filter.MessageStatus },
                new SqlParameter() {ParameterName = "@PatietName",Value= Filter.PatientName },
                new SqlParameter() {ParameterName = "@MessageFrom",Value= Filter.MessageFrom }
                };
                var dt = objhelper.DataTable("USP_GetMessageConversation", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ChromeExtensionDAL---GetUnReadReferralCount", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
