﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Models;
using DataAccessLayer.Common;
using System.Data.SqlClient;
using System.Data;

namespace DataAccessLayer.ZifyPay
{
    public class ZiftPayDAL
    {
        public static clsHelper clshelper = new clsHelper();
        public static clsCommon objcommon = new clsCommon();

        /// <summary>
        /// This method insert transaction details on database based on patient transaction.
        /// </summary>
        /// <param name="objResponse"></param>
        /// <param name="patientId"></param>
        /// <param name="IsSaveTransaction"></param>
        /// <param name="MasterID"></param>
        /// <param name="GatewayID"></param>
        /// <returns></returns>
        public static bool AddTransaction(ZPTransactionResponse objResponse, string patientId, bool IsSaveTransaction, int MasterID, int GatewayID)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[25];
                strParameter[0] = new SqlParameter("@accountAccessory", SqlDbType.NVarChar);
                strParameter[0].Value = objResponse.accountAccessory;
                strParameter[1] = new SqlParameter("@accountId", SqlDbType.Int);
                strParameter[1].Value = Convert.ToInt32(objResponse.accountId);
                strParameter[2] = new SqlParameter("@accountNumberMasked", SqlDbType.NVarChar);
                strParameter[2].Value = objResponse.accountNumberMasked;
                strParameter[3] = new SqlParameter("@accountType", SqlDbType.NVarChar);
                strParameter[3].Value = objResponse.accountType;
                strParameter[4] = new SqlParameter("@approvalCode", SqlDbType.NVarChar);
                strParameter[4].Value = objResponse.approvalCode;                
                strParameter[5] = new SqlParameter("@PPId", SqlDbType.Int);
                strParameter[5].Direction = ParameterDirection.Output;
                strParameter[6] = new SqlParameter("@balance", SqlDbType.VarChar);
                strParameter[6].Value = decimal.Parse((objResponse.balance == null || objResponse.balance == String.Empty) ? "0" : objResponse.balance.ToString());
                strParameter[7] = new SqlParameter("@currencyCode", SqlDbType.NVarChar);
                strParameter[7].Value = objResponse.currencyCode;
                strParameter[8] = new SqlParameter("@cycleCode", SqlDbType.NVarChar);
                strParameter[8].Value = objResponse.cycleCode;
                strParameter[9] = new SqlParameter("@extendedAccountType", SqlDbType.NVarChar);
                strParameter[9].Value = objResponse.extendedAccountType;
                strParameter[10] = new SqlParameter("@feeAmount", SqlDbType.VarChar);
                strParameter[10].Value = decimal.Parse((objResponse.feeAmount == null || objResponse.feeAmount == String.Empty) ? "0" : objResponse.feeAmount.ToString());
                strParameter[11] = new SqlParameter("@holderName", SqlDbType.NVarChar);
                strParameter[11].Value = objResponse.holderName;
                strParameter[12] = new SqlParameter("@originalAmount", SqlDbType.Decimal);
                strParameter[12].Value = objResponse.originalAmount;
                strParameter[13] = new SqlParameter("@requestId", SqlDbType.Int);
                strParameter[13].Value = Convert.ToInt32(objResponse.requestId);
                strParameter[14] = new SqlParameter("@responseCode", SqlDbType.NVarChar);
                strParameter[14].Value = objResponse.responseCode;
                strParameter[15] = new SqlParameter("@responseMessage", SqlDbType.NVarChar);
                strParameter[15].Value = objResponse.responseMessage;
                strParameter[16] = new SqlParameter("@responseType", SqlDbType.NVarChar);
                strParameter[16].Value = objResponse.responseType;                
                strParameter[17] = new SqlParameter("@transactionDate", SqlDbType.DateTime);
                strParameter[17].Value = DateTime.ParseExact(objResponse.transactionDate, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                strParameter[18] = new SqlParameter("@transactionId", SqlDbType.NVarChar);
                strParameter[18].Value = objResponse.transactionId;
                strParameter[19] = new SqlParameter("@warningCode", SqlDbType.NVarChar);
                strParameter[19].Value = objResponse.warningCode;
                strParameter[20] = new SqlParameter("@patientId", SqlDbType.Int);
                strParameter[20].Value = Convert.ToInt32(patientId);
                strParameter[21] = new SqlParameter("@paymentGatewayType", SqlDbType.Int);
                strParameter[21].Value = GatewayID;
                strParameter[22] = new SqlParameter("@IsSaveTransaction", SqlDbType.Bit);
                strParameter[22].Value = IsSaveTransaction;
                strParameter[23] = new SqlParameter("@MasterID", SqlDbType.Int);
                strParameter[23].Value = MasterID;
                strParameter[24] = new SqlParameter("@requestParam", SqlDbType.NText);
                strParameter[24].Value = objResponse.requestParam;
                clshelper.DataTable("USP_InsertPatientPayment", strParameter);

                int result = Convert.ToInt32(strParameter[5].Value);
                return result > 0 ? true : false;                
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("ZifyPayDAL--AddTransaction", ex.Message, ex.StackTrace);
                throw;
            }            
        }

        /// <summary>
        /// This method add patient card details(Patient Profile Details) on database.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="zpaccountid"></param>
        /// <returns></returns>
        public static int AddPatientCardDetails(PatientPaymentRequest obj, int zpaccountid)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[8];
                strParameter[0] = new SqlParameter("@Amount", SqlDbType.VarChar);
                strParameter[0].Value = obj.Amount;               
                strParameter[1] = new SqlParameter("@GatewayId", SqlDbType.Int);
                strParameter[1].Value = obj.GatewayId;
                strParameter[2] = new SqlParameter("@Patientid", SqlDbType.BigInt);
                strParameter[2].Value = obj.Patientid;
                strParameter[3] = new SqlParameter("@PaymentDate", SqlDbType.Date);
                strParameter[3].Value = obj.PaymentDate;
                strParameter[4] = new SqlParameter("@Status", SqlDbType.Bit);
                strParameter[4].Value = obj.Status;
                strParameter[5] = new SqlParameter("@PRId", SqlDbType.Int);
                strParameter[5].Direction = ParameterDirection.Output;
                strParameter[6] = new SqlParameter("@zpaccountid", SqlDbType.Int);
                strParameter[6].Value = zpaccountid;
                strParameter[7] = new SqlParameter("@PPTId", SqlDbType.Int);
                strParameter[7].Value = obj.PPTId;

                clshelper.DataTable("USP_InsertPatientCardDetails", strParameter);

                int result = Convert.ToInt32(strParameter[5].Value);
                return result;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("ZifyPayDAL--AddPatientCardDetails", ex.Message, ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Getting Ziftpay details form database.
        /// </summary>
        /// <returns></returns>
        public static DataTable GetZiftDetails()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = clshelper.DataTable("USP_GetZiftPayDetails", null);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("ZiftPayDAL--USP_GetZiftPayDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This method insert a merchant details on recordlinc database.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="doctorId"></param>
        /// <param name="gatewayId"></param>
        /// <returns></returns>
        public static int AddDoctorMerchant(MerchantResponse obj, int doctorId, int gatewayId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[12];
                strParameter[0] = new SqlParameter("@responseType", SqlDbType.NVarChar);
                strParameter[0].Value = obj.responseType;
                strParameter[1] = new SqlParameter("@applicationCode", SqlDbType.NVarChar);
                strParameter[1].Value = obj.applicationCode;
                strParameter[2] = new SqlParameter("@applicationId", SqlDbType.NVarChar);
                strParameter[2].Value = obj.applicationId;
                strParameter[3] = new SqlParameter("@merchantId", SqlDbType.NVarChar);
                strParameter[3].Value = obj.merchantId;
                strParameter[4] = new SqlParameter("@directDebitDescriptor", SqlDbType.NVarChar);
                strParameter[4].Value = obj.directDebitDescriptor;
                strParameter[5] = new SqlParameter("@DMId", SqlDbType.Int);
                strParameter[5].Direction = ParameterDirection.Output;
                strParameter[6] = new SqlParameter("@doctorId", SqlDbType.Int);
                strParameter[6].Value = doctorId;
                strParameter[7] = new SqlParameter("@responseCode", SqlDbType.NVarChar);
                strParameter[7].Value = obj.responseCode;
                strParameter[8] = new SqlParameter("@responseMessage", SqlDbType.NVarChar);
                strParameter[8].Value = obj.responseMessage;
                strParameter[9] = new SqlParameter("@paymentgatewayid", SqlDbType.Int);
                strParameter[9].Value = gatewayId;                
                strParameter[10] = new SqlParameter("@IsCreated", SqlDbType.Bit);
                strParameter[10].Value = obj.responseMessage.ToLower().ToString() == "approved" ? true : false;
                strParameter[11] = new SqlParameter("@accountId", SqlDbType.NVarChar);
                strParameter[11].Value = obj.accountId;
                clshelper.DataTable("USP_InsertDoctorMerchantDetails", strParameter);

                int result = Convert.ToInt32(strParameter[5].Value);
                return result;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("ZifyPayDAL--AddPatientCardDetails", ex.Message, ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This method getting Zift pay account-id using recordlinc userid.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public DataTable GetAccountZiftPayDetailsByUserId(int UserId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] para = new SqlParameter[1];
                para[0] = new SqlParameter("@UserId", SqlDbType.Int);
                para[0].Value = UserId;

                dt = clshelper.DataTable("USP_AccountZiftPayDetails_By_UserId", para);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("GetAccountZiftPayDetailsByUserId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This method getting Zift pay details form database using Zift pay account-id.
        /// </summary>
        /// <param name="ZPAccountId"></param>
        /// <returns></returns>
        public DataTable GetAccountZiftPayDetailsByZPAccountId(int ZPAccountId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] para = new SqlParameter[1];
                para[0] = new SqlParameter("@ZPAccountId", SqlDbType.Int);
                para[0].Value = ZPAccountId;

                dt = clshelper.DataTable("USP_AccountZiftPayDetails_By_ZPAccountId", para);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("GetAccountZiftPayDetailsByZPAccountId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This method insert a Split payment details while make a transaction.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static bool AddPatientSplitPaymentDetail(PatientSplitPayment obj)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[5];
                strParameter[0] = new SqlParameter("@PPMId", SqlDbType.Int);
                strParameter[0].Value = obj.PPMId;
                strParameter[1] = new SqlParameter("@split_accountId", SqlDbType.Int);
                strParameter[1].Value = obj.Split_AccountId;
                strParameter[2] = new SqlParameter("@split_TransactionId", SqlDbType.NVarChar);
                strParameter[2].Value = obj.Split_TransactionId;
                strParameter[3] = new SqlParameter("@split_Amount", SqlDbType.Decimal);
                strParameter[3].Value = obj.Split_Amount;
                strParameter[4] = new SqlParameter("@PSPId", SqlDbType.Int);
                strParameter[4].Value = ParameterDirection.Output;
                clshelper.DataTable("USP_PatientSplitPaymentDetail", strParameter);

                int result = Convert.ToInt32(strParameter[4].Value);
                return result > 0 ? true : false;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("ZifyPayDAL--AddPatientSplitPaymentDetail", ex.Message, ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Insert and update the patient token detail
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static int AddPatientPaymentToken(PatientPaymentToken obj)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[11];
                strParameter[0] = new SqlParameter("@Id", SqlDbType.Int);
                strParameter[0].Value = DBNull.Value;
                strParameter[1] = new SqlParameter("@PaymentGatewayId", SqlDbType.Int);
                strParameter[1].Value = obj.PaymentGatewayId;
                strParameter[2] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[2].Value = obj.PatientId;
                strParameter[3] = new SqlParameter("@Token", SqlDbType.NVarChar);
                strParameter[3].Value = obj.Token;
                strParameter[4] = new SqlParameter("@SourceIP", SqlDbType.NVarChar);
                strParameter[4].Value = obj.SourceIP;
                strParameter[5] = new SqlParameter("@CreatedBy", SqlDbType.Int);
                strParameter[5].Value = obj.CreatedBy;
                strParameter[6] = new SqlParameter("@IsActive", SqlDbType.Bit);
                strParameter[6].Value = obj.IsActive;
                strParameter[7] = new SqlParameter("@PTId", SqlDbType.Int);
                strParameter[7].Direction = ParameterDirection.Output;
                strParameter[8] = new SqlParameter("@TokenResponse", SqlDbType.NVarChar);
                strParameter[8].Value = obj.TokenResponse;
                strParameter[9] = new SqlParameter("@AccountNumberMasked", SqlDbType.NVarChar);
                strParameter[9].Value = obj.AccountNumberMasked;
                strParameter[10] = new SqlParameter("@AccountAccessory", SqlDbType.NVarChar);
                strParameter[10].Value = obj.AccountAccessory;
                clshelper.DataTable("USP_InsertUpdatePatientPaymentToken", strParameter);

                int result = Convert.ToInt32(strParameter[7].Value);
                return result;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("ZifyPayDAL--AddPatientPaymentToken", ex.Message, ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Token by patientid from patientpaymenttoken
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public DataTable GetPatientPaymentTokenByPatientId(int PatientId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] para = new SqlParameter[1];
                para[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                para[0].Value = PatientId;

                dt = clshelper.DataTable("USP_GetPatientPaymentTokenByPatientId", para);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("ZifyPayDAL--GetPatientPaymentTokenByPatientId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Remove patient payment card detail
        /// </summary>
        /// <param name="PPTId"></param>
        /// <returns></returns>
        public static bool RemovePatientPaymentToken(int PPTId)
        {
            try
            {
                bool UpdateStatus = false;
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@PPTId", SqlDbType.Int);
                strParameter[0].Value = PPTId;
                UpdateStatus = clshelper.ExecuteNonQuery("USP_RemovePatientPaymentToken", strParameter);
                return UpdateStatus;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("ZifyPayDAL--RemovePatientPaymentToken", ex.Message, ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This method get patient payment details of the Patient.
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public static DataTable GetPatientPaymentDetails(int PatientId)
        {
            try
            {
                clsHelper ObjHelper = new clsHelper();
                SqlParameter[] sp = new SqlParameter[1];
                sp[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                sp[0].Value = PatientId;

                DataTable dt = ObjHelper.DataTable("USP_GetPatientZiftPayPaymentDetails", sp);
                return dt;
            }
            catch (Exception ex)
            {
                new clsCommon().InsertErrorLog("ZifyPayDAL--RemovePatientPaymentToken", ex.Message, ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This function check doctor registered with zift pay or not.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static int CheckDoctorRegisteredWithZiftpay(int UserId)
        {
            try
            {
                int Result = -1;
                clsHelper ObjHelper = new clsHelper();
                SqlParameter[] sp = new SqlParameter[1];
                sp[0] = new SqlParameter("@UserId", SqlDbType.Int);
                sp[0].Value = UserId;
                DataTable dt = ObjHelper.DataTable("USP_CheckZiftPayUserExists_By_UserId", sp);
                if (dt != null && dt.Rows.Count > 0)
                {
                    Result = Convert.ToInt32(dt.Rows[0][0]);
                }
                return Result;
            }
            catch (Exception ex)
            {
                new clsCommon().InsertErrorLog("ZifyPayDAL--RemovePatientPaymentToken", ex.Message, ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Used for getting records of dentist which is received payment from patients.
        /// </summary>
        /// <param name="FilterObj"></param>
        /// <returns></returns>
        public static DataTable GetDentistPaymentListOfZiftPay(ZiftPayFilter FilterObj)
        {
            try
            {
                clsHelper ObjHelper = new clsHelper();
                SqlParameter[] sp = new SqlParameter[3];
                sp[0] = new SqlParameter("@UserId", FilterObj.UserId);
                sp[0].Value = FilterObj.UserId;
                sp[1] = new SqlParameter("@SortColumn", FilterObj.SortColumn);
                sp[1].Value = FilterObj.SortColumn;
                sp[2] = new SqlParameter("@SortDirection", FilterObj.SortDirection);
                sp[2].Value = FilterObj.SortDirection;
                DataTable dt = ObjHelper.DataTable("USP_GetZiftPayregisterDoctorbyId", sp);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ZiftPayDAL--GetDentistPaymentListOfZiftPay", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This method Get Merchant details from database.
        /// </summary>
        /// <param name="AccountId"></param>
        /// <param name="AccountName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public static DataTable GetMerchantDetails(int AccountId, string AccountName, DateTime? FromDate, DateTime? ToDate)
        {
            try
            {
                clsHelper ObjHelper = new clsHelper();
                SqlParameter[] sp = new SqlParameter[4];
                sp[0] = new SqlParameter("@AccountId", SqlDbType.Int);
                sp[0].Value = AccountId;
                sp[1] = new SqlParameter("@AccountName", SqlDbType.NVarChar);
                sp[1].Value = AccountName;
                sp[2] = new SqlParameter("@FromDate", SqlDbType.DateTime);
                sp[2].Value = FromDate;
                sp[3] = new SqlParameter("@ToDate", SqlDbType.DateTime);
                sp[3].Value = ToDate;
                DataTable dt = ObjHelper.DataTable("Usp_GetZiftPayMerchantDetail", sp);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("GetMerchantDetails---GetMerchantDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static DataTable GetMerchantListOfTreatingDoctor(int PatientId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@PatientId",Value=PatientId}
                };
                DataTable dt = new clsHelper().DataTable("USP_GetMerchantDoctorDetails", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("GetMerchantDetails---GetMerchantDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

    }
}
