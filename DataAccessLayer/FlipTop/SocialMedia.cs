﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using BO.Models;
using BO.ViewModel;
using Newtonsoft.Json;
using RestSharp;

namespace DataAccessLayer.FlipTop
{
    public class SocialMedia
    {
        SqlConnection theConnection;

        public SocialMedia()
        {
        }

        /// <summary>
        /// Retrieve a person using email address and type
        /// </summary>
        /// <param name="email"></param>
        /// <returns>Person</returns>
        public Person LookUpPerson(string email)
        {
            string api = string.Empty; //"7zh58tyvsrauyv6p8sptky2c";
            string xml = string.Empty;
            string url = string.Empty;

            string fullcontactapi = string.Empty;
            string fullcontacturl = string.Empty;
            string fullcontactxml = string.Empty;

            Person per = new Person();

            api = ConfigurationManager.AppSettings["fullcontactAPI"];
            url = ConfigurationManager.AppSettings["fullcontactUrl"] + email + "&api_key=" + api + "&format=xml";


            try
            {
                WebRequest request = WebRequest.Create(url);

                using (var response = (HttpWebResponse)request.GetResponse())
                {

                    if (response.StatusCode == HttpStatusCode.Accepted || response.StatusCode == HttpStatusCode.OK) //200 Accepted
                    {
                        using (var sr = new StreamReader(response.GetResponseStream()))
                        {
                            //build person object to return
                            per = BuildPerson(sr);
                            per.StatusCode = "OK";
                        }
                    }
                    //update the statuscode
                    per.StatusDescription = response.StatusCode.ToString();
                }
            }
            catch (WebException ex)
            {
                per.StatusCode = ex.Message;
            }

            return per;
        }

        private Person BuildPerson(StreamReader xml)
        {
            Person person = new Person();

            //build query
            var pers = from p in XDocument.Load(xml).Elements("person")
                       select p;
            //execute query
            foreach (var per in pers)
            {

                person.FullName = per.Element("name").Value;
                person.FirstName = CapitalizeFirstCharacter(per.Element("first_name").Value);
                person.LastName = CapitalizeFirstCharacter(per.Element("last_name").Value);
                person.Age = per.Element("age").Value;
                person.Gender = per.Element("gender").Value;
                person.Location = per.Element("location").Value;
                person.Company = CapitalizeFirstCharacter(per.Element("company").Value);
                person.Title = per.Element("title").Value;
                person.Email = per.Element("email").Value;

                //get the memberships
                if (per.Element("memberships").Element("facebook") != null)
                {
                    person.FacebookURL = per.Element("memberships").Element("facebook").Value;
                }



                if (per.Element("memberships").Element("linkedin") != null)
                {
                    person.LinkedInURL = per.Element("memberships").Element("linkedin").Value;
                }
            }

            LookUpPersonfullcontact(person.Email);

            return person;
        }



        #region Full Contact API

        #region Old
        //public Person LookUpPersonfullcontact(string email)
        //{
        //    string fullcontactapi = string.Empty;
        //    string fullcontacturl = string.Empty;
        //    string fullcontactxml = string.Empty;
        //    Person per = new Person();
        //    fullcontactapi = ConfigurationManager.AppSettings["fullcontactAPI"];
        //    fullcontacturl = ConfigurationManager.AppSettings["fullcontactUrl"] + email + "&apiKey=" + fullcontactapi;
        //    try
        //    {

        //        WebRequest request = WebRequest.Create(fullcontacturl);
        //        request.Method = "POST";
        //        request.ContentType = "text/xml";
        //        System.Net.ServicePointManager.Expect100Continue = false;
        //        SecurityProtocolType origSecurityProtocol = System.Net.ServicePointManager.SecurityProtocol;
        //        System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

        //        using (var response = (HttpWebResponse)request.GetResponse())
        //        {

        //            if (response.StatusCode == HttpStatusCode.Accepted || response.StatusCode == HttpStatusCode.OK) //200 Accepted
        //            {
        //                using (var sr = new StreamReader(response.GetResponseStream()))
        //                {
        //                    //build person object to return

        //                    per = BuildPersonfullcontact(sr);
        //                    per.StatusCode = "OK";
        //                }
        //            }
        //            //update the statuscode
        //            per.StatusDescription = response.StatusCode.ToString();
        //        }


        //    }
        //    catch (WebException ex)
        //    {
        //        per.StatusCode = ex.Message;
        //    }

        //    return per;
        //}
        #endregion

        public Person LookUpPersonfullcontact(string email)
        {
            string fullcontacturlV3 = string.Empty;
            string fullcontactAPIKeyV3 = string.Empty;

            Person per = new Person();
            fullcontacturlV3 = ConfigurationManager.AppSettings["fullcontactUrlV3"];
            fullcontactAPIKeyV3 = ConfigurationManager.AppSettings["fullcontactAPIKeyV3"];
            try
            {
                var client = new RestClient(fullcontacturlV3);
                var request = new RestRequest(Method.POST);
                request.AddHeader("Authorization", "Bearer " + fullcontactAPIKeyV3 + "");
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("undefined", "{\"email\":\"" + email + "\"}", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    FullContact Obj = JsonConvert.DeserializeObject<FullContact>(response.Content);
                    per = BuildPersonFullContactObject(Obj);
                    per.StatusCode = "OK";
                }
                else
                {
                    per.StatusCode = response.StatusCode.ToString();
                }
            }
            catch (WebException ex)
            {
                per.StatusCode = ex.Message;
            }
            return per;
        }

        private Person BuildPersonFullContactObject(FullContact Obj)
        {
            Person person = new Person();
            if (Obj != null)
            {
                if (Obj.details != null && Obj.details.name != null)
                {
                    person.FirstName = Obj.details.name.given;
                    person.LastName = Obj.details.name.family;
                }
                person.ImageUrl = Obj.avatar;
                person.Title = Obj.title;
                person.Description = Obj.bio;
                person.LinkedInURL = Obj.linkedin;
                person.FacebookURL = Obj.facebook;
                person.TwitterURL = Obj.twitter;
                person.Website = Obj.website;
            }
            return person;
        }
        #endregion



        public Person LookUpPersonClearBit(string email)
        {


            string clearbitapi = string.Empty;
            string clearbiturl = string.Empty;
            string clearbitxml = string.Empty;

            Person per = new Person();

            clearbiturl = ConfigurationManager.AppSettings["ClearBitUrl"] + email;
            try
            {
                WebRequest request = WebRequest.Create(clearbiturl);
                request.Headers.Add("Authorization", "Bearer " + ConfigurationManager.AppSettings["ClearBitKey"]);
                request.ContentType = "application/json; charset=utf-8";
                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.Accepted || response.StatusCode == HttpStatusCode.OK) //200 Accepted
                    {
                        using (var sr = new StreamReader(response.GetResponseStream()))
                        {
                            JObject o = JObject.Parse(sr.ReadToEnd());
                            per = BuildPersonClearbit(o);
                            per.StatusCode = "OK";
                        }
                    }
                    //update the statuscode
                    per.StatusDescription = response.StatusCode.ToString();
                }
            }
            catch (WebException ex)
            {
                per.StatusCode = ex.Message;
            }

            return per;
        }
        private Person BuildPersonfullcontact(StreamReader xml)
        {
            Person person = new Person();
            XDocument doc = XDocument.Load(xml);
            //build query
            var pers = from p in doc.Elements("person")
                       select p;
            //execute query
            foreach (var per in pers)
            {
                if (string.IsNullOrEmpty(person.ImageUrl))
                {
                    if (per.Element("photos") != null && per.Element("photos").Element("photo") != null && per.Element("photos").Element("photo").Element("isPrimary") != null)
                    {
                        if (per.Element("photos").Element("photo").Element("isPrimary").Value == "true")
                        {
                            person.ImageUrl = per.Element("photos").Element("photo").Element("url").Value;
                        }
                    }
                }

                if (per.Element("contactInfo") != null)
                {
                    if (string.IsNullOrEmpty(person.FullName))
                    {
                        if (per.Element("contactInfo") != null && per.Element("contactInfo").Element("fullName") != null)
                        {
                            if (per.Element("contactInfo").Element("fullName") != null)
                            {
                                person.FullName = per.Element("contactInfo").Element("fullName").Value;
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(person.FirstName))
                    {
                        if (per.Element("contactInfo") != null && per.Element("contactInfo").Element("givenName") != null)
                        {


                            if (per.Element("contactInfo").Element("givenName") != null)
                            {
                                person.FirstName = per.Element("contactInfo").Element("givenName").Value;
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(person.LastName))
                    {
                        if (per.Element("contactInfo") != null && per.Element("contactInfo").Element("familyName") != null)
                        {

                            if (per.Element("contactInfo").Element("familyName") != null)
                            {
                                person.LastName = per.Element("contactInfo").Element("familyName").Value;
                            }
                        }
                    }


                }
                if (per.Element("socialProfiles") != null)
                {
                    var items = from g in doc.Root.Descendants("socialProfiles").Elements("socialProfile")
                                select new
                                {
                                    name = (string)g.Element("typeName"),
                                    id = (string)g.Element("url") ?? "none",
                                    bio = (string)g.Element("bio") ?? "",
                                };
                    foreach (var dataElement in items.ToList())
                    {

                        if (string.IsNullOrEmpty(person.FacebookURL))
                        {
                            if (dataElement.name == "Facebook")
                            {
                                person.FacebookURL = dataElement.id.ToString();
                            }
                        }
                        if (string.IsNullOrEmpty(person.TwitterURL))
                        {
                            if (dataElement.name == "Twitter")
                            {
                                person.TwitterURL = dataElement.id.ToString();
                            }
                        }
                        if (string.IsNullOrEmpty(person.LinkedInURL))
                        {
                            if (dataElement.name == "LinkedIn")
                            {
                                person.LinkedInURL = dataElement.id.ToString();
                                person.Description = dataElement.bio.ToString();
                            }
                        }


                    }
                }
            }


            return person;
        }
        private Person BuildPersonClearbit(JObject o)
        {
            Person person = new Person();
            if (!string.IsNullOrEmpty(Convert.ToString(o["person"])))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(o["person"]["avatar"])))
                {
                    person.ImageUrl = Convert.ToString(o["person"]["avatar"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(o["person"]["name"])))
                {

                    if (!string.IsNullOrEmpty(Convert.ToString(o["person"]["name"]["fullName"])))
                    {
                        person.FullName = Convert.ToString(o["person"]["name"]["fullName"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(o["person"]["name"]["givenName"])))
                    {
                        person.FirstName = Convert.ToString(o["person"]["name"]["givenName"]);
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(o["person"]["name"]["familyName"])))
                    {
                        person.LastName = Convert.ToString(o["person"]["name"]["familyName"]);
                    }

                }
                if (!string.IsNullOrEmpty(Convert.ToString(o["person"]["bio"])))
                {
                    person.Description = Convert.ToString(o["person"]["bio"]);

                }

                if (!string.IsNullOrEmpty(Convert.ToString(o["person"]["facebook"]["handle"])))
                {
                    person.FacebookURL = "https://www.facebook.com/" + Convert.ToString(o["person"]["facebook"]["handle"]);

                }
                if (!string.IsNullOrEmpty(Convert.ToString(o["person"]["twitter"]["handle"])))
                {
                    person.TwitterURL = "https://twitter.com/" + Convert.ToString(o["person"]["twitter"]["handle"]);

                }
                if (!string.IsNullOrEmpty(Convert.ToString(o["person"]["linkedin"]["handle"])))
                {
                    person.LinkedInURL = "https://www.linkedin.com/" + Convert.ToString(o["person"]["linkedin"]["handle"]);

                }
            }
            else if (!string.IsNullOrEmpty(Convert.ToString(o["company"])))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(o["company"]["legalName"])))
                {
                    person.FullName = Convert.ToString(o["company"]["legalName"]);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(o["company"]["description"])))
                {
                    person.Description = Convert.ToString(o["company"]["description"]);

                }

                if (!string.IsNullOrEmpty(Convert.ToString(o["company"]["facebook"]["handle"])))
                {
                    person.FacebookURL = "https://www.facebook.com/" + Convert.ToString(o["company"]["facebook"]["handle"]);

                }
                if (!string.IsNullOrEmpty(Convert.ToString(o["company"]["twitter"]["handle"])))
                {
                    person.TwitterURL = "https://twitter.com/" + Convert.ToString(o["company"]["twitter"]["handle"]);

                }
                if (!string.IsNullOrEmpty(Convert.ToString(o["company"]["linkedin"]["handle"])))
                {
                    person.LinkedInURL = "https://www.linkedin.com/" + Convert.ToString(o["company"]["linkedin"]["handle"]);

                }
            }
            return person;
        }

        public bool UpdateRecordLincMember(Person person)
        {
            try
            {
                if (connect())
                {
                    SqlCommand theCommand = new SqlCommand("UpdateMemberSocialMediaInfo", theConnection);
                    theCommand.CommandType = CommandType.StoredProcedure;

                    theCommand.Parameters.Add("@EmailAddress", SqlDbType.VarChar, 100).Value = person.Email.ToString();
                    theCommand.Parameters.Add("@FacebookUrl", SqlDbType.VarChar, 250).Value = person.FacebookURL.ToString();
                    theCommand.Parameters.Add("@TwitterUrl", SqlDbType.VarChar, 250).Value = person.TwitterURL.ToString();
                    theCommand.Parameters.Add("@LinkedinUrl", SqlDbType.VarChar, 250).Value = person.LinkedInURL.ToString();
                    theCommand.Parameters.Add("@FirstName", SqlDbType.VarChar, 100).Value = person.FirstName.ToString();
                    theCommand.Parameters.Add("@LastName", SqlDbType.VarChar, 100).Value = person.LastName.ToString();
                    theCommand.Parameters.Add("@Title", SqlDbType.VarChar, 200).Value = person.Title.ToString();
                    theCommand.Parameters.Add("@Company", SqlDbType.VarChar, 200).Value = person.Company.ToString();

                    theCommand.ExecuteNonQuery();
                }
            }
            catch
            {
                //doesn't matter if we were able to save it, the user will type it in the page anyway
            }

            return true;
        }

        protected string CapitalizeFirstCharacter(string data)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(data))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(data[0]) + data.Substring(1);
        }

        /// <summary>
        /// Connects the database
        /// </summary>
        /// <returns>If the connection could be established or not</returns>
        private bool connect()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["CONNECTIONSTRING"].ConnectionString;
            theConnection = new SqlConnection(connectionString);
            try
            {
                theConnection.Open();
                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }

    //public enum StatusCode
    //{
    //    NotAvailable = 202, //202-Data not available. At least one profile from the request is still being processed.
    //    BadRequest = 400, //400-Bad request. The request is invalid or has incorrect syntax.
    //    DeveloperInactive = 403, //403-Developer Inactive. There is a problem with your api key, please contact: api@fliptop.com
    //    PageNotFound = 404, //404-Page not found. Request were processed successfully, and no results were found
    //    InternalServerError = 500 //500-Internal Server error(rare). There was an unexpected server error. If you encounter this repeatedly, please email api@fliptop.com
    //}
}
