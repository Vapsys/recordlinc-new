﻿using DataAccessLayer.Common;
using System;
using System.Data;
using System.Data.SqlClient;
using BO.Models;
using BO.ViewModel;
using BO.Enums;

namespace DataAccessLayer.SmileBrand
{
    public class clsSmileBrand
    {
        /// <summary>
        /// This Method insert a Token on database for smile brand.
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="Token"></param>
        /// <returns></returns>
        public static DataTable GenerateTokenBasedonRequest(int UserId, string Token)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@UserId",Value = UserId},
                    new SqlParameter{ParameterName="@TOKEN",Value = Token}
                };
                return new clsHelper().DataTable("Usp_GenerateToken", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--GenerateTokenBasedonRequest", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This Method check token is valid or not.
        /// </summary>
        /// <param name="Token"></param>
        /// <returns></returns>
        public static DataTable CheckTokenIsValid(string Token)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@Token",Value=Token},
                    new SqlParameter{ParameterName="@oOutput",Direction=ParameterDirection.Output,SqlDbType=SqlDbType.Int}
                };
                DataTable dt = new clsHelper().DataTable("Usp_CheckTokenIsValid", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--CheckTokenIsValid", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable GetPatientProcedures(int RequestId, int Count, int PageIndex, int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@RequestId",Value=RequestId},
                    new SqlParameter{ParameterName="@Count",Value=Count},
                    new SqlParameter{ParameterName="@PageIndex",Value=PageIndex},
                    new SqlParameter{ParameterName="@UserId",Value=UserId}
                };
                DataTable dt = new clsHelper().DataTable("Usp_GetPatientProcedure", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--GetPatientProcedures", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This Method used for getting Location list of dentist
        /// </summary>
        /// <param name="AccountId"></param>
        /// <returns></returns>
        public static DataTable GetLocationList(int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@UserId",Value=UserId}
                };
                DataTable dt = new clsHelper().DataTable("Usp_GetLocationDetailsOfDoctor", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--GetLocationList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// This method will insert an record for Request.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public static int InsertDPMS_SyncDetails(DPMS_SyncDetails Obj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@DentrixConnectorKey",Value = Obj.DentrixConnectorId},
                    new SqlParameter{ParameterName="@RequestType",Value = Obj.RequestType},
                    new SqlParameter{ParameterName="@GUID",Value=Obj.GUID},
                    new SqlParameter{ParameterName="@RequestTime",Value=Obj.RequestTime},
                    new SqlParameter{ParameterName="@RequestId",Direction=ParameterDirection.Output,SqlDbType=SqlDbType.Int},
                    new SqlParameter{ParameterName="@IsFromSBI",Value=Obj.IsFromSBI}
                };
                new clsHelper().ExecuteNonQuery("Usp_InsertDPMS_SyncDetails", sqlpara);
                int Result = Convert.ToInt32(sqlpara[4].Value);
                return Result;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--InsertDPMS_SyncDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// This Method InsertProcessQueue for Procedure SBI Integration
        /// </summary>
        /// <param name="Obj"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static bool InsertDMPSProcedureProcessQueue(PatientProcedure<BO.Enums.Common.PatientProcedureDateFilter> Obj, int UserId, int RequestId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter {ParameterName="@RequestId",Value=RequestId},
                    new SqlParameter {ParameterName="@UserId",Value=UserId},
                    new SqlParameter {ParameterName="@SendAll",Value=0},
                    new SqlParameter {ParameterName="@FromDate",Value=Obj.FromDate},
                    new SqlParameter {ParameterName="@ToDate",Value=Obj.ToDate},
                    new SqlParameter {ParameterName="@PatientId",Value=Obj.PatientId},
                    new SqlParameter {ParameterName="@LocationId",Value=Obj.LocationId},
                    new SqlParameter {ParameterName="@OrderBy",Value=Obj.OrderBy},
                    new SqlParameter {ParameterName="@DateFilterBy",Value=Obj.DateFilterBy}
                };
                bool dt = new clsHelper().ExecuteNonQuery("Usp_InsertDPMSProcessQueueForProcess", sqlpara, 5 * 60);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--InsertDMPSProcedureProcessQueue", Ex.Message, Ex.StackTrace);
                throw;
            }
        }


        /// <summary>
        /// This Method InsertProcessQueue for Patients SBI Integration
        /// </summary>
        /// <param name="Obj"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static bool InsertDMPSPatientProcessQueue(PatientFilter Obj, int UserId, int RequestId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter { ParameterName = "@RequestId", Value=RequestId },
                    new SqlParameter { ParameterName = "@DoctorId", Value=UserId },
                    new SqlParameter { ParameterName = "@FirstName", Value=Obj.FirstName },
                    new SqlParameter { ParameterName = "@LastName", Value=Obj.Lastname },
                    new SqlParameter { ParameterName = "@Email", Value=Obj.Email },
                    new SqlParameter { ParameterName = "@Phone", Value=Obj.Phone },
                    // Missing "@ZipCode
                    new SqlParameter { ParameterName = "@SortColumn", Value=Obj.SortColumn},
                    new SqlParameter { ParameterName = "@SortDirection", Value=Obj.SortDirection},
                    new SqlParameter { ParameterName = "@LocationId", Value=Obj.LocationId},
                    new SqlParameter { ParameterName = "@FromDate", Value=Obj.FromDate},
                    new SqlParameter { ParameterName = "@ToDate", Value=Obj.ToDate},
                };
                bool dt = new clsHelper().ExecuteNonQuery("Usp_InsertProcessQueueForPatients", sqlpara, 5 * 60);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--InsertDMPSPatientProcessQueue", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This Method used for patient payments for SmileBrand Write back.
        /// </summary>
        /// <param name="GUID"></param>
        /// <returns></returns>
        public static Request GetPMSSyncDetailsByGUID(string GUID)
        {
            try
            {
                Request Obj = new Request();
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@GUID",Value=GUID}
                };
                DataTable dt = new clsHelper().DataTable("USP_GetPMSSyncDetailsByGUID", sqlpara);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        if (Convert.ToString(item["RequestType"]) == "FinanceChanges_SBI")
                        {
                            Obj.FinanceRequestId = Convert.ToInt32(item["Id"]);
                            Obj.FinanceRequestKey = GUID;
                        }
                        else if (Convert.ToString(item["RequestType"]) == "InsurancePayment_SBI")
                        {
                            Obj.InsuranceRequestId = Convert.ToInt32(item["Id"]);
                            Obj.InsuranceRequestKey = GUID;
                        }
                        else if (Convert.ToString(item["RequestType"]) == "StandardPayment_SBI")
                        {
                            Obj.StandardRequestId = Convert.ToInt32(item["Id"]);
                            Obj.StandardRequestKey = GUID;
                        }
                        else if (Convert.ToString(item["RequestType"]) == "AdjustmentPayment_SBI")
                        {
                            Obj.AdjustmentRequestId = Convert.ToInt32(item["Id"]);
                            Obj.StandardRequestKey = GUID;
                        }
                    }
                }
                return Obj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--GetPMSSyncDetailsByGUID", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool InsertDPMSProcessQueueForPayment(int Finance, int Standard, int Insurance, int Adjustment, PatientPayments Obj, int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter { ParameterName = "@Type", Value = Obj.Type },
                    new SqlParameter { ParameterName = "@PatientId", Value = Obj.PatientId },
                    new SqlParameter { ParameterName = "@FromDate", Value = Obj.FromDate },
                    new SqlParameter { ParameterName = "@ToDate", Value = Obj.ToDate },
                    new SqlParameter { ParameterName = "@Order", Value = Obj.Order },
                    new SqlParameter { ParameterName = "@UserId", Value = UserId },
                    new SqlParameter { ParameterName = "@FinanceRequestId", Value = Finance },
                    new SqlParameter { ParameterName = "@StandardRequestId", Value = Standard },
                    new SqlParameter { ParameterName = "@InsuranceRequestId", Value = Insurance },
                    new SqlParameter { ParameterName = "@AdjustmentRequestId", Value = Adjustment },
                    new SqlParameter { ParameterName = "@LocationId", Value = Obj.LocationId },
                    new SqlParameter { ParameterName = "@DateFilterBy", Value = Obj.DateFilterBy }
                };
                bool dt = new clsHelper().ExecuteNonQuery("Usp_InsertProcessQueueForPayments", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--InsertDPMSProcessQueueForPayment", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable GetFinanceChanges(PatientPayments Obj, int FinanceRequestId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
               {
                    new SqlParameter { ParameterName = "@FinanceRequestId", Value = FinanceRequestId },
                    new SqlParameter { ParameterName = "@PageIndex", Value=Obj.PageIndex},
                    new SqlParameter { ParameterName = "@Count", Value=Obj.FetchRecordCount}
               };
                DataTable dt = new clsHelper().DataTable("Usp_GetFinanceChanrgesDetails", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--GetFinanceChanges", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable GetInsuranceDetails(PatientPayments Obj, int InsuranceRequestId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
               {
                    new SqlParameter { ParameterName = "@InsuranceRequestId", Value = InsuranceRequestId },
                    new SqlParameter { ParameterName = "@PageIndex", Value=Obj.PageIndex},
                    new SqlParameter { ParameterName = "@Count", Value=Obj.FetchRecordCount}
               };
                DataTable dt = new clsHelper().DataTable("Usp_GetInsuranceDetails", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--GetInsuranceDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable GetAdjustmentDetails(PatientPayments Obj, int AdjustmentRequestId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
               {

                    new SqlParameter { ParameterName = "@AdjustRequestId", Value = AdjustmentRequestId },
                    new SqlParameter { ParameterName = "@PageIndex", Value=Obj.PageIndex},
                    new SqlParameter { ParameterName = "@Count", Value=Obj.FetchRecordCount}
               };
                DataTable dt = new clsHelper().DataTable("Usp_GetAdjustmentDetails", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--GetAdjustmentDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable GetStandardDetails(PatientPayments Obj, int StandardRequestId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
               {

                    new SqlParameter { ParameterName = "@StandardRequestId", Value = StandardRequestId },
                    new SqlParameter { ParameterName = "@PageIndex", Value=Obj.PageIndex},
                    new SqlParameter { ParameterName = "@Count", Value=Obj.FetchRecordCount}
               };
                DataTable dt = new clsHelper().DataTable("Usp_GetStandardDetails", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--GetStandardDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int GetCountByRequestId(int RequestId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
               {
                    new SqlParameter { ParameterName = "@RequestId", Value = RequestId}
               };
                return Convert.ToInt32(new clsHelper().ExecuteScalar("USP_GetCountOfRequestId", sqlpara));
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--GetCountByRequestId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int GetCountofPatientProcedure(PatientProcedure<BO.Enums.Common.PatientProcedureDateFilter> Obj, int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@UserId",Value=UserId},
                    new SqlParameter{ParameterName="@SendAll",Value=0},
                    new SqlParameter{ParameterName="@FromDate",Value=Obj.FromDate},
                    new SqlParameter{ParameterName="@ToDate",Value=Obj.ToDate},
                    new SqlParameter{ParameterName="@PatientId",Value=Obj.PatientId},
                    new SqlParameter{ParameterName="@LocationId",Value=Obj.LocationId},
                    new SqlParameter{ParameterName="@OrderBy",Value=Obj.OrderBy},
                    new SqlParameter{ParameterName="@DateFilterBy",Value=Obj.DateFilterBy}

                };
                string dt = new clsHelper().ExecuteScalar("Usp_GetCountOfPatientProcedure", sqlpara);
                return (!string.IsNullOrWhiteSpace(dt)) ? Convert.ToInt32(dt) : 0;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--GetCountByRequestId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable GetCountofPatientPayments(PatientPayments Obj, int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter { ParameterName = "@Type", Value = Obj.Type },
                    new SqlParameter { ParameterName = "@PatientId", Value = Obj.PatientId },
                    new SqlParameter { ParameterName = "@FromDate", Value = Obj.FromDate },
                    new SqlParameter { ParameterName = "@ToDate", Value = Obj.ToDate },
                    new SqlParameter { ParameterName = "@Order", Value = Obj.Order },
                    new SqlParameter { ParameterName = "@UserId", Value = UserId },
                    new SqlParameter { ParameterName = "@LocationId", Value = Obj.LocationId },
                    new SqlParameter { ParameterName = "@DateFilterBy", Value = Obj.DateFilterBy }
                };
                DataTable dt = new clsHelper().DataTable("Usp_GetCountOfPatientPayments", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--GetCountofPatientPayments", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static int GetCountofPatientAppointment(PatientProcedure<BO.Enums.Common.PatientAppointmentDateFilter> Obj, int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@UserId",Value=UserId},
                    new SqlParameter{ParameterName="@FromDate",Value=Obj.FromDate},
                    new SqlParameter{ParameterName="@ToDate",Value=Obj.ToDate},
                    new SqlParameter{ParameterName="@PatientId",Value=Obj.PatientId},
                    new SqlParameter{ParameterName="@LocationId",Value=Obj.LocationId},
                    new SqlParameter{ParameterName="@DateFilterBy",Value=Obj.DateFilterBy}

                };
                string dt = new clsHelper().ExecuteScalar("Usp_GetCountOfPatientAppointment", sqlpara);
                return (!string.IsNullOrWhiteSpace(dt)) ? Convert.ToInt32(dt) : 0;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--GetCountofPatientAppointment", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static DataTable GetPatientAppointments(int RequestId, int Count, int PageIndex)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@RequestId",Value=RequestId},
                    new SqlParameter{ParameterName="@Count",Value=Count},
                    new SqlParameter{ParameterName="@PageIndex",Value=PageIndex}
                };
                DataTable dt = new clsHelper().DataTable("Usp_GetPatientAppointments", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--GetPatientAppointments", Ex.Message, Ex.StackTrace);
                throw;
            };
        }

        /// <summary>
        /// Used to create process queue for further retrieval 
        /// </summary>
        /// <param name="Obj">Contains request data basis on which we have to filter records</param>
        /// <param name="UserId">For which dentist required data.</param>
        /// <param name="ApptRequestId">Request Id of appointment</param>
        /// <returns></returns>
        public static bool InsertDMPSAppointmentProcessQueue(PatientProcedure<BO.Enums.Common.PatientAppointmentDateFilter> Obj, int? UserId, int ApptRequestId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter {ParameterName="@RequestId",Value=ApptRequestId},
                    new SqlParameter {ParameterName="@UserId",Value=UserId},
                    new SqlParameter {ParameterName="@FromDate",Value=Obj.FromDate},
                    new SqlParameter {ParameterName="@ToDate",Value=Obj.ToDate},
                    new SqlParameter {ParameterName="@PatientId",Value=Obj.PatientId},
                    new SqlParameter {ParameterName="@LocationId",Value=Obj.LocationId},
                    new SqlParameter {ParameterName="@OrderBy",Value=Obj.OrderBy},
                    new SqlParameter {ParameterName="@DateFilterBy",Value=Obj.DateFilterBy}
                };
                bool dt = new clsHelper().ExecuteNonQuery("Usp_InsertDMPSAppointmentProcessQueue", sqlpara, 5 * 60);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--InsertDMPSAppointmentProcessQueue", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This Method is used to get count of patients SBI Integration
        /// </summary>
        /// <param name="Obj"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static int GetCountofPatients(PatientFilter Obj, int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter { ParameterName = "@DoctorId", Value=UserId },
                    new SqlParameter { ParameterName = "@FirstName", Value=Obj.FirstName },
                    new SqlParameter { ParameterName = "@LastName", Value=Obj.Lastname },
                    new SqlParameter { ParameterName = "@Email", Value=Obj.Email },
                    new SqlParameter { ParameterName = "@Phone", Value=Obj.Phone },
                    // Missing "@ZipCode
                    new SqlParameter { ParameterName = "@SortColumn", Value=Obj.SortColumn},
                    new SqlParameter { ParameterName = "@SortDirection", Value=Obj.SortDirection},
                    new SqlParameter { ParameterName = "@LocationId", Value=Obj.LocationId},
                    new SqlParameter { ParameterName = "@FromDate", Value=Obj.FromDate},
                    new SqlParameter { ParameterName = "@ToDate", Value=Obj.ToDate},
                };
                string dt = new clsHelper().ExecuteScalar("Usp_GetCountOfPatients", sqlpara);
                return (!string.IsNullOrWhiteSpace(dt)) ? Convert.ToInt32(dt) : 0;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--InsertDMPSPatientProcessQueue", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable GetPatientDetails(int PatientRequestId, int PageIndex, int PageCount)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
               {

                    new SqlParameter { ParameterName = "@PatientRequestId", Value = PatientRequestId },
                    new SqlParameter { ParameterName = "@PageIndex", Value=PageIndex},
                    new SqlParameter { ParameterName = "@Count", Value=PageCount}
               };
                DataTable dt = new clsHelper().DataTable("Usp_GetPatientDetails", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsSmileBrand--GetStandardDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
