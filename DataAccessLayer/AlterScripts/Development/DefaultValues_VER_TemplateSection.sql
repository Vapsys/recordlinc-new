-- Added Defult values into the  VER_TemplateSection

INSERT INTO VER_TemplateSection (SectionName,TemplateID,[order],ParentTemplateID,CreatedDate,CreatedBy)
VALUES ('payer',1,1,null,GETDATE(),60)

INSERT INTO VER_TemplateSection (SectionName,TemplateID,[order],ParentTemplateID,CreatedDate,CreatedBy)
VALUES ('provider',1,2,null,GETDATE(),60)

INSERT INTO VER_TemplateSection (SectionName,TemplateID,[order],ParentTemplateID,CreatedDate,CreatedBy)
VALUES ('subscriber',1,3,null,GETDATE(),60)

INSERT INTO VER_TemplateSection (SectionName,TemplateID,[order],ParentTemplateID,CreatedDate,CreatedBy)
VALUES ('activeCoverage',1,4,null,GETDATE(),60)

INSERT INTO VER_TemplateSection (SectionName,TemplateID,[order],ParentTemplateID,CreatedDate,CreatedBy)
VALUES ('coInsurance',1,5,null,GETDATE(),60)

INSERT INTO VER_TemplateSection (SectionName,TemplateID,[order],ParentTemplateID,CreatedDate,CreatedBy)
VALUES ('deductible',1,6,null,GETDATE(),60)

INSERT INTO VER_TemplateSection (SectionName,TemplateID,[order],ParentTemplateID,CreatedDate,CreatedBy)
VALUES ('limitations',1,7,null,GETDATE(),60)

INSERT INTO VER_TemplateSection (SectionName,TemplateID,[order],ParentTemplateID,CreatedDate,CreatedBy)
VALUES ('maximums',1,8,null,GETDATE(),60)