IF NOT EXISTS (
  SELECT *
  FROM sysobjects
  WHERE NAME = 'VER_TemplateSection'
   AND xtype = 'U'
  )
BEGIN
CREATE TABLE [dbo].[VER_TemplateSection](
	[SectionID] [int] IDENTITY(1,1) NOT NULL,
	[TemplateID] [int] NULL,
	[SectionName] [nvarchar](500) NOT NULL,
	[Order] [int] NOT NULL,
	[ParentTemplateID] [int] NULL,
	[SectionDisplayName] [nvarchar](500) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_VER_TemplateSection] PRIMARY KEY CLUSTERED 
(
	[SectionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO


