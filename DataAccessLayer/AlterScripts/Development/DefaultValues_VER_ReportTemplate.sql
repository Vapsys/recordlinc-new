-- Add Defult values into the Verident report Template Tables

INSERT INTO VER_ReportTemplate (TemplateName,Description,CreatedDate,CreatedBy) 
VALUES ('General Dentist','View provider infomation, freqencies, waiting periods and patient history',GETDATE(),60)

INSERT INTO VER_ReportTemplate (TemplateName,Description,CreatedDate,CreatedBy) 
VALUES ('Endodontist','View provider infomation, freqencies, waiting periods and patient history',GETDATE(),60)

INSERT INTO VER_ReportTemplate (TemplateName,Description,CreatedDate,CreatedBy) 
VALUES ('Orthodontist','View provider infomation, freqencies, waiting periods and patient history',GETDATE(),60)

INSERT INTO VER_ReportTemplate (TemplateName,Description,CreatedDate,CreatedBy) 
VALUES ('Prosthodontist','View provider infomation, freqencies, waiting periods and patient history',GETDATE(),60)
