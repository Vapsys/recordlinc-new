﻿@echo off

cls
echo **** Deployment Script ****
set /p myS=Please enter the SQL Server Name:
set /p myD=SQL Database:
set /p myU=SQL User Name:
set /p myP=SQL Password:


cls
echo **** Deployment Script ****
set /p choice=Confirm deployment to %myD% on %myS% (y/n)?

if '%choice%'=='y' goto begin
goto end


:begin
if exist _Deploy.txt del _Deploy.txt

echo Beginning execution for Usp_AddPatientLabel.sql >> _Deploy.txt 2>&1
sqlcmd -d %myD% -S %myS% -U %myU% -P %myP% -I -i "Usp_AddPatientLabel.sql" >> _Deploy.txt 2>&1

echo Beginning execution for Usp_RemovePatientLabelAssociation.sql >> _Deploy.txt 2>&1
sqlcmd -d %myD% -S %myS% -U %myU% -P %myP% -I -i "Usp_RemovePatientLabelAssociation.sql" >> _Deploy.txt 2>&1

echo **** Deployment Completed **** 2>&1
@notepad _Deploy.txt 

:end
@echo on