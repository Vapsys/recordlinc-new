IF NOT EXISTS (
  SELECT *
  FROM sysobjects
  WHERE NAME = 'VER_AccountTemplate'
   AND xtype = 'U'
  )
BEGIN
CREATE TABLE [dbo].[VER_AccountTemplate](
	[AccountId] [int] NOT NULL,
	[TemplateId] [int] NOT NULL
) ON [PRIMARY]
END
GO

ALTER TABLE [dbo].[VER_AccountTemplate]  WITH CHECK ADD  CONSTRAINT [FK_VER_AccountTemplate_VER_ReportTemplate] FOREIGN KEY([TemplateId])
REFERENCES [dbo].[VER_ReportTemplate] ([TemplateID])
GO

ALTER TABLE [dbo].[VER_AccountTemplate] CHECK CONSTRAINT [FK_VER_AccountTemplate_VER_ReportTemplate]
GO


