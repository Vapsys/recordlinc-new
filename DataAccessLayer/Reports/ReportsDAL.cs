﻿using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Reports
{
   public class ReportsDAL
    {
        clsHelper clshelper = new clsHelper();
        clsCommon ObjCommon = new clsCommon();
        /// <summary>
        ///  Get the Provider Production Goals
        /// </summary>
        /// <param name="DentrixProviderIDs"></param>
        /// <param name="ProcedureCodes"></param>
        /// <param name="HygeineCodes"></param>
        /// <param name="RestoritiveCodes"></param>
        /// <returns></returns>
        public DataTable GetProviderProductionGoals(string DentrixProviderIDs, string ProcedureCodes, string HygeineCodes, string RestoritiveCodes, string npCodes, int bulk)        {            try            {                DataTable dt = new DataTable();                SqlParameter[] strParameter = new SqlParameter[5];
                                strParameter[0] = new SqlParameter("@ProcedureCodes", SqlDbType.NVarChar);                strParameter[0].Value = ProcedureCodes;                strParameter[1] = new SqlParameter("@HygeineCodes", SqlDbType.NVarChar);                strParameter[1].Value = HygeineCodes;                strParameter[2] = new SqlParameter("@RestoritiveCodes", SqlDbType.NVarChar);                strParameter[2].Value = RestoritiveCodes;                strParameter[3] = new SqlParameter("@NPCodes", SqlDbType.NVarChar);                strParameter[3].Value = npCodes;                strParameter[4] = new SqlParameter("@Bulk", SqlDbType.Int);                strParameter[4].Value = bulk;
                dt = clshelper.DataTable("GetProviderProductionGoals_", strParameter);                return dt;            }            catch (Exception Ex)            {                ObjCommon.InsertErrorLog("ReportsDAL - GetProviderProductionGoals", Ex.Message, Ex.StackTrace);                throw;            }        }
        public DataTable Get_Member_Details()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];

                strParameter[0] = new SqlParameter("@ParentUserId", SqlDbType.NVarChar);                strParameter[0].Value = 69;
                strParameter[1] = new SqlParameter("@Status", SqlDbType.NVarChar);
                strParameter[1].Value = 1;

                dt = clshelper.DataTable("Get_Member_Details", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("ReportsDAL - Get_Member_Details", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        ///  Get the Smart Numbers
        /// </summary>
        /// <param name="DentrixProviderIDs"></param>
        /// <returns></returns>
        /// 
        public DataTable GetCriticalNumbers(string HygeineCodes, string RestoritiveCodes, string provider_dentrix, int bulk)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[5];

                strParameter[0] = new SqlParameter("@HygeineCodes", SqlDbType.NVarChar);
                strParameter[0].Value = HygeineCodes;
                strParameter[1] = new SqlParameter("@RestoritiveCodes", SqlDbType.NVarChar);
                strParameter[1].Value = RestoritiveCodes;
                strParameter[2] = new SqlParameter("@PROVIDER_DENTRIXID", SqlDbType.NVarChar);
                strParameter[2].Value = provider_dentrix;
                strParameter[3] = new SqlParameter("@BULK", SqlDbType.Int);
                strParameter[3].Value = bulk;
                strParameter[4] = new SqlParameter("@ACCOUNT_ID", SqlDbType.VarChar);
                strParameter[4].Value = null;

                dt = clshelper.DataTable("GetCriticalNumbers", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("ReportsDAL - GetCriticalNumbers", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public DataTable GetSmartNumbers(string HygeineCodes, string RestoritiveCodes , String provider_dentrix, int bulk)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[5];

                strParameter[0] = new SqlParameter("@HygeineCodes", SqlDbType.NVarChar);
                strParameter[0].Value = HygeineCodes;
                strParameter[1] = new SqlParameter("@RestoritiveCodes", SqlDbType.NVarChar);
                strParameter[1].Value = RestoritiveCodes;
                strParameter[2] = new SqlParameter("@PROVIDER_DENTRIXID", SqlDbType.NVarChar);
                strParameter[2].Value = provider_dentrix;
                strParameter[3]= new SqlParameter("@BULK", SqlDbType.Int);
                strParameter[3].Value = bulk;
                strParameter[4] = new SqlParameter("@Account_Id", SqlDbType.Int);
                strParameter[4].Value = null;

                dt = clshelper.DataTable("GetSmartNumbers", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("ReportsDAL - GetSmartNumbers", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        #region --  Changes By Bhupesh On Dated 30-04-2019
        public DataTable GetLocations_List_RecordLinc()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];

                strParameter[0] = new SqlParameter("@Status", SqlDbType.NVarChar);                strParameter[0].Value = 1;
                strParameter[1] = new SqlParameter("@type", SqlDbType.NVarChar);                strParameter[1].Value = "LocationDetails";

                dt = clshelper.DataTable("GetLocation_List_RecordLinc", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("ReportsDAL - GetLocation_List_RecordLinc", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable Get_Stats_Reports(int bulk)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];

                strParameter[0] = new SqlParameter("@ACCOUNT_ID", SqlDbType.NVarChar);                strParameter[0].Value = 0;
                strParameter[1] = new SqlParameter("@BULK", SqlDbType.Int);                strParameter[1].Value = bulk;

                dt = clshelper.DataTable("GetLocation_List_RecordLinc", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("ReportsDAL - GetLocation_List_RecordLinc", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable Get_Individual_Location_Amount(int bulk)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];

                strParameter[0] = new SqlParameter("@ACCOUNT_ID", SqlDbType.NVarChar);                strParameter[0].Value = 0;
                strParameter[1] = new SqlParameter("@BULK", SqlDbType.Int);                strParameter[1].Value = bulk;

                dt = clshelper.DataTable("GetLocation_List_RecordLinc", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("ReportsDAL - GetLocation_List_RecordLinc", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable Get_Hygiene_Location_Amount(int bulk)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];

                strParameter[0] = new SqlParameter("@ACCOUNT_ID", SqlDbType.NVarChar);                strParameter[0].Value = 0;
                strParameter[1] = new SqlParameter("@BULK", SqlDbType.Int);                strParameter[1].Value = bulk;

                dt = clshelper.DataTable("GetLocation_List_RecordLinc", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("ReportsDAL - GetLocation_List_RecordLinc", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable Get_List_RecordLinc()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];

                strParameter[0] = new SqlParameter("@status", SqlDbType.NVarChar);                strParameter[0].Value = 1;
                //strParameter[1] = new SqlParameter("@type", SqlDbType.NVarChar);                //strParameter[1].Value = "LocationAndAmount";

                dt = clshelper.DataTable("Get_List_RecordLinc", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("ReportsDAL - Get_List_RecordLinc", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable Get_AccountID_List_RecordLinc()        {            try            {                DataTable dt = new DataTable();                SqlParameter[] strParameter = new SqlParameter[1];                strParameter[0] = new SqlParameter("@status", SqlDbType.NVarChar);                strParameter[0].Value = 1;
                //strParameter[1] = new SqlParameter("@type", SqlDbType.NVarChar);
                //strParameter[1].Value = "LocationAndAmount";

                dt = clshelper.DataTable("Get_AccountID_RecordLinc", strParameter);                return dt;            }            catch (Exception Ex)            {                ObjCommon.InsertErrorLog("ReportsDAL - Get_AccountID_RecordLinc", Ex.Message, Ex.StackTrace);                throw;            }        }
        #endregion
    }
}
