﻿using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Models;

namespace DataAccessLayer
{
    public class clsLandingDAL
    {
        clsHelper clshelper;
        public clsLandingDAL()
        {
            clshelper = new clsHelper();
        }

        public bool InsertLandingHistory(LandingHistory landinghistory)
        {
            bool result = false;
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[3];
                sqlpara[0] = new SqlParameter("@Id", SqlDbType.Int);
                sqlpara[0].Value = landinghistory.LanHistoryId;
                sqlpara[1] = new SqlParameter("@Email", SqlDbType.NVarChar);
                sqlpara[1].Value = landinghistory.Email;
                sqlpara[2] = new SqlParameter("@Page", SqlDbType.NVarChar);
                sqlpara[2].Value = landinghistory.LandingPageName;                
                result = clshelper.ExecuteNonQuery("USP_InsertLandingHistory", sqlpara);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public DataTable GetLandingHistoryDetails(int PageIndex,int PageSize,int SortColumn,int SortDirection,string SearchText)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] addParameter = new SqlParameter[5];
                addParameter[0] = new SqlParameter("@PageIndex", SqlDbType.Int);
                addParameter[0].Value = PageIndex;
                addParameter[1] = new SqlParameter("@PageSize", SqlDbType.Int);
                addParameter[1].Value = PageSize;
                addParameter[2] = new SqlParameter("@SortColumn", SqlDbType.Int);
                addParameter[2].Value = SortColumn;
                addParameter[3] = new SqlParameter("@SortDirection", SqlDbType.Int);
                addParameter[3].Value = SortDirection;
                addParameter[4] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
                addParameter[4].Value = SearchText;
                dt = clshelper.DataTable("USP_GetAllLandingHistoryDetail", addParameter);
                return dt;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
