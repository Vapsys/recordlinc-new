﻿using DataAccessLayer.ColleaguesData;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using BO.Models;
using System.Data.SqlTypes;
using Newtonsoft.Json;

namespace DataAccessLayer
{
    public class Login
    {
        public string username { get; set; }
        public string password { get; set; }
    }
    public class AuthenticationPR
    {
        public const string UserName = "recordlinc_dev";
        public const string Password = "{gb5h+(po,o04oly$/g42K";
        public const string PatientRewardApi = "https://dev.myrewardstore.com/";
        private string _LogfilePath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["LogFilePath"]); 
        private string _LogFileName = "RecordlincLog";
        private Object thisLock = new Object();
        private bool _IsLoggingEnabled = true;
        public bool IsLoggingEnabled
        {
            get { return _IsLoggingEnabled; }
            set
            {
                if (value != _IsLoggingEnabled)        //  Only display if it changes.
                    WriteLog("Logging is being set to " + value.ToString(), true);
                _IsLoggingEnabled = value;
            }
        }

        public static async Task<string> GetTokenFromPatientRewardAsync()
        {
            string Token = string.Empty;
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(PatientRewardApi);
            //client.DefaultRequestHeaders.Add("Accept", "application/json");
            //client.DefaultRequestHeaders.Add("Content-Type", "application/json");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var content = new FormUrlEncodedContent(new[]
            {
                new KeyValuePair<string, string>("username", UserName),
                new KeyValuePair<string, string>("password", Password),
            });
            //HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "dentalApi/v1/authentication/token");
            //request.Content = new StringContent("{\"username\":\"recordlinc_dev\",\"password\":{gb5h+(po,o04oly$/g42K}",
            //                        Encoding.UTF8,
            //                        "application/json");
            
            //await client.SendAsync(request).ContinueWith(responseTask =>
            //{
            //    if (responseTask.Result.IsSuccessStatusCode)
            //    {
            //        Token = responseTask.Result.Content.ReadAsStringAsync().Result.ToString();
            //        string[] split = Token.Split('"');
            //        Token = Convert.ToString(split[5]);
            //    }
            
            //}).ConfigureAwait(false);
            try
            {
                new AuthenticationPR().WriteLog("Calling the S1p for getting access token:- "+ content.ReadAsStringAsync().Result, true);
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                var responses = await client.PostAsync("dentalApi/v1/authentication/token", content).ConfigureAwait(false);
                new AuthenticationPR().WriteLog("Called the S1p for access token:- " + responses.Content.ReadAsStringAsync().Result, true);
                if (responses.IsSuccessStatusCode)
                {
                    Token = responses.Content.ReadAsStringAsync().Result;
                    string[] split = Token.Split('"');
                    Token = Convert.ToString(split[5]);
                    return Token;
                }
                else
                {
                    new AuthenticationPR().WriteLog("Not Success for getting access token:- "+ responses.Content.ReadAsStringAsync().Result, true);
                    return Token;
                }
            }
            catch (OperationCanceledException oce)
            {
                new Common.clsCommon().InsertErrorLog("AuthenticationPR---OperationCanceledException---GetTokenFromPatientReward", oce.Message, oce.StackTrace);
                throw;
            }
            catch (Exception Ex)
            {
                string Inner = string.Empty;
                if (Ex.InnerException != null)
                {
                    Inner = "[Inner Message] : "+Ex.InnerException.Message + "--- [StackTrace] : "+Ex.InnerException.StackTrace;
                }
                new AuthenticationPR().WriteLog("Calling the S1p for getting access token Error:- " + Inner, true);
                new Common.clsCommon().InsertErrorLog("AuthenticationPR---GetTokenFromPatientReward", Inner + Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static  HttpResponseMessage CallApi(string URL, FormUrlEncodedContent obj, string Token)
        {
            int LogId = 0;
            var response = new HttpResponseMessage();
            try
            {
                if (string.IsNullOrWhiteSpace(Token))
                {
                    //Token = clsColleaguesData.GetS1pTokenFromDatabase();
                    Task<String> t = GetTokenFromPatientRewardAsync();
                    Token = t.Result;

                }
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(PatientRewardApi);
                //client.DefaultRequestHeaders.TryAddWithoutValidation("ContentType", "application/json");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                var content = obj;
                new AuthenticationPR().WriteLog("BEFORE Calling the S1p for Insert values :- " + content.ReadAsStringAsync().Result, true);
                //Ankit Here 06-22-2018
                //PRCSP-307 related changes as per Bruce comment on it we need to insert an error log in it.
                LogId = InsertAPILogsOfS1p(content, null, null, 0, PatientRewardApi+URL, Token);
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                response = client.PostAsync(URL, content).Result;
                //Ankit Here 06-22-2018
                //PRCSP-307 related changes as per Bruce comment on it we need to insert an error log in it.
                InsertAPILogsOfS1p(null, response, null, LogId, PatientRewardApi+URL, Token);
                new AuthenticationPR().WriteLog("AFTER called the S1p for Insert values Success:- " + response.Content.ReadAsStringAsync().Result, true);
                return response;
            }
            catch (Exception Ex)
            {
                //Ankit Here 06-22-2018
                //PRCSP-307 related changes as per Bruce comment on it we need to insert an error log in it.
                InsertAPILogsOfS1p(null, response, Ex, LogId, PatientRewardApi+URL, Token);
                string Inner = string.Empty;
                if (Ex.InnerException != null)
                {
                    Inner = "[Inner Message] : " + Ex.InnerException.Message + "--- [StackTrace] : " + Ex.InnerException.StackTrace;
                }
                new AuthenticationPR().WriteLog("Calling the S1p for getting access token Error:- " + Inner, true);
                new Common.clsCommon().InsertErrorLog("AuthenticationPR---CALL__API", Inner + Ex.Message, Ex.StackTrace);
                throw;
            }
            
        }

        public static HttpResponseMessage CallApiMultiPartFormData(string URL, FormUrlEncodedContent obj, string Token)
        {
            int LogId = 0;
            try
            {
                if (string.IsNullOrWhiteSpace(Token))
                {
                    Token = clsColleaguesData.GetS1pTokenFromDatabase();
                }
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(PatientRewardApi);
                client.DefaultRequestHeaders.TryAddWithoutValidation("ContentType", "application/json");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", Token);
                var content = obj;
                new AuthenticationPR().WriteLog("BEFORE Calling the S1p for Insert values :- " + content.ReadAsStringAsync().Result, true);
                //Ankit Here 06-22-2018
                //PRCSP-307 related changes as per Bruce comment on it we need to insert an error log in it.
                LogId = InsertAPILogsOfS1p(content, null, null, 0, PatientRewardApi + URL, Token);
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
                var response = client.PostAsync(URL, content).Result;
                //Ankit Here 06-22-2018
                //PRCSP-307 related changes as per Bruce comment on it we need to insert an error log in it.
                InsertAPILogsOfS1p(null, response, null, LogId, PatientRewardApi + URL, Token);
                new AuthenticationPR().WriteLog("AFTER called the S1p for Insert values Success:- " + response.Content.ReadAsStringAsync().Result, true);
                return response;
            }
            catch (Exception Ex)
            {
                //Ankit Here 06-22-2018
                //PRCSP-307 related changes as per Bruce comment on it we need to insert an error log in it.
                InsertAPILogsOfS1p(null, null, Ex, LogId, PatientRewardApi + URL, Token);
                string Inner = string.Empty;
                if (Ex.InnerException != null)
                {
                    Inner = "[Inner Message] : " + Ex.InnerException.Message + "--- [StackTrace] : " + Ex.InnerException.StackTrace;
                }
                new AuthenticationPR().WriteLog("Calling the S1p for getting access token Error:- " + Inner, true);
                new Common.clsCommon().InsertErrorLog("AuthenticationPR---CALL__API", Inner + Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        public static string GetToken()
        {
            string Token = string.Empty;
            var Obj = new Login();
            Obj.username = UserName;
            Obj.password = Password;
            var request = (HttpWebRequest)WebRequest.Create(PatientRewardApi + "dentalApi/v1/authentication/token");
            //var postData = "username=recordlinc_dev";
            //postData += "&password={gb5h+(po,o04oly$/g42K";
            //var data = Encoding.UTF8.GetBytes(Obj);

            request.Method = "POST";
            request.ContentType = "application/json; charset=UTF-8";
            request.Accept = "application/json";
            //request.ContentLength = data.Length;

            using (var streamWriter = new StreamWriter(request.GetRequestStream()))
            {
                string json = new JavaScriptSerializer().Serialize(new
                {
                    username = "recordlinc_dev",
                    password = @"{gb5h+(po,o04oly$/g42K"
                });
                //string json = "{\"username\":\"recordlinc_dev\"," +
                //  "\"password\":\"{gb5h+(po,o04oly$/g42K\"}";

                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
                //streamWriter.Write(data, 0, data.Length);
            }
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            var response = (HttpWebResponse)request.GetResponse();
            if(response.StatusCode == HttpStatusCode.OK)
            {
                Token = response.ContentEncoding.ToString();// ReadAsStringAsync().Result;
                string[] split = Token.Split('"');
                Token = Convert.ToString(split[5]);
                return Token;
            }
            
            // var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            return Token;
        }
        public void WriteLog(string prmData, bool prmForce)
        {
            string fullFileName = ".."+ _LogfilePath + "\\" + _LogFileName + ".log";
            try
            {
                if (_IsLoggingEnabled == true || prmForce == true)
                {
                    string Path = HttpContext.Current.Server.MapPath(_LogfilePath+ _LogFileName+".log");
                    lock (thisLock)
                    {
                        // If the log file is way too large, rename it and start over. 

                        if (File.Exists(Path))
                        {
                            FileInfo f = new FileInfo(Path);
                            if (f.Length > 5000000L)        // 5 MB
                            {
                                // Rename the current file.
                                string newFileName = _LogfilePath + " Ending " + BuildTimeWithoutSlashes() + ".oldlog";
                                File.Move(Path, newFileName);
                            }
                        }

                        using (StreamWriter sw = new StreamWriter(Path, true))
                        {
                            sw.WriteLine(DateTime.Now.ToString() + " " + prmData);
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errMsg = "Logger Failed: Check to see if someone is holding the log file open. " + prmData + " " +
                        ex.GetBaseException().Message + " " +
                        ex.StackTrace;

            }
        }
        // Can't put slashes in file names.
        private string BuildTimeWithoutSlashes()
        {
            string x = DateTime.Now.Year.ToString() + " " +
                        AddLeadingZero(DateTime.Now.Month.ToString()) + " " +
                        AddLeadingZero(DateTime.Now.Day.ToString()) + " " +
                        AddLeadingZero(DateTime.Now.Hour.ToString()) +
                        AddLeadingZero(DateTime.Now.Minute.ToString()) +
                        AddLeadingZero(DateTime.Now.Second.ToString());
            return x;
        }
        // Need leading zeroes to sort properly in Windows Explorer.
        private string AddLeadingZero(string prmNumber)
        {
            return prmNumber.Length == 1 ? "0" + prmNumber : prmNumber;
        }

        /// <summary>
        /// Used for insert an API logs while calling the S1p API.
        /// </summary>
        /// <param name="Content"></param>
        /// <param name="response"></param>
        /// <param name="Ex"></param>
        /// <param name="LogId"></param>
        /// <param name="URL"></param>
        /// <param name="Token"></param>
        /// <returns></returns>
        public static int InsertAPILogsOfS1p(HttpContent Content = null, HttpResponseMessage response = null,Exception Ex = null,int LogId = 0,string URL=null,string Token=null)
        {
            int LogIds = 0;
            try
            {
               
                S1p_LogHistory Obj = new S1p_LogHistory();
                if(Content != null)
                {
                    Obj.LogId = LogId;
                    Obj.RequestHeaders = Content.Headers.ToString();
                    Obj.RequestContentBody = Content.ReadAsStringAsync().Result;
                    Obj.RequestMethod = "POST";
                    Obj.RequestTime = DateTime.Now;
                    Obj.RequestUri = URL;
                    Obj.CreatedDate = DateTime.Now;
                    Obj.Token = Token;
                }
                if(response != null)
                {
                    Obj.LogId = LogId;
                    Obj.ResponseContentBody = response.Content.ReadAsStringAsync().Result;
                    Obj.ResponseHeaders = response.Headers.ToString();
                    Obj.StatusCode = (int)response.StatusCode;
                    Obj.ResponseTime = DateTime.Now;
                    Obj.IsSuccess = ((int)response.StatusCode == 200) ? 1 : 0;
                }
                if(Ex != null)
                {
                    Obj.ErrorMessage = Ex.Message;
                    Obj.ErrorStackTrace = Ex.StackTrace;
                    if(Ex.InnerException != null)
                    {
                        Obj.ErrorInnerException = Ex.InnerException.Message;
                        Obj.ErrorInnerStackTrace = Ex.InnerException.StackTrace;
                    }
                }
                Obj.RequestTime = (Obj.RequestTime.ToShortDateString() == DateTime.MinValue.ToShortDateString()) ? Convert.ToDateTime(SqlDateTime.MinValue.ToString()) : Obj.RequestTime;
                Obj.ResponseTime = (Obj.ResponseTime.ToShortDateString() == DateTime.MinValue.ToShortDateString()) ? Convert.ToDateTime(SqlDateTime.MinValue.ToString()) : Obj.ResponseTime;
                Obj.CreatedDate = (Obj.CreatedDate.ToShortDateString() == DateTime.MinValue.ToShortDateString()) ? Convert.ToDateTime(SqlDateTime.MinValue.ToString()) : Obj.CreatedDate;
                LogIds = clsColleaguesData.InsertUpdateS1pAPICallLogHistory(Obj);
            }
            catch (Exception Esx)
            {
                new Common.clsCommon().InsertErrorLog("AuthenticationPR---InsertAPILogsOfS1p", Esx.Message, Esx.StackTrace);
            }
            return LogIds;
        }
    }
}
