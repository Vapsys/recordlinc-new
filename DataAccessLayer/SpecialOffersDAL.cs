﻿using BO.Models;
using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class SpecialOffersDAL
    {
        clsHelper clshelper = new clsHelper();

        public bool InsertUpdateSpecialOffers(SpecialOffers specialoffers)
        {
            int result;
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[7];
                sqlpara[0] = new SqlParameter("@OfferId", SqlDbType.Int);
                sqlpara[0].Value = specialoffers.OfferId;
                sqlpara[1] = new SqlParameter("@Title", SqlDbType.VarChar);
                sqlpara[1].Value = specialoffers.Title;
                sqlpara[2] = new SqlParameter("@Description", SqlDbType.VarChar);
                sqlpara[2].Value = specialoffers.Description;
                sqlpara[3] = new SqlParameter("@Code", SqlDbType.VarChar);
                sqlpara[3].Value = specialoffers.Code;
                sqlpara[4] = new SqlParameter("@SortOrder", SqlDbType.Int);
                sqlpara[4].Value = specialoffers.SortOrder;
                sqlpara[5] = new SqlParameter("@Status", SqlDbType.Bit);
                sqlpara[5].Value = specialoffers.Status;
                sqlpara[6] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlpara[6].Value = specialoffers.UserId;
                result = Convert.ToInt32(clshelper.ExecuteScalar("USP_InsertUpdateSpecialOffers", sqlpara));
                return result > 0;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetSpecialOfferById(int OfferId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@OfferId", SqlDbType.Int);
                strParameter[0].Value = OfferId;
                dt = clshelper.DataTable("USP_GetSpecialOfferById", strParameter);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool RemoveSpecialOffer(int OfferId)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@OfferId", SqlDbType.Int);
                strParameter[0].Value = OfferId;
                int result = Convert.ToInt32(clshelper.ExecuteScalar("USP_DeleteSpecialOffer", strParameter));
                return result > 0;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetSpecialOfferByUserId(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                dt = clshelper.DataTable("USP_GetSpecialOfferByUserId", strParameter);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int CheckSpecialOfferById(SpecialOffers specialoffers)
        {
            SqlParameter[] sqlpara = new SqlParameter[5];
            sqlpara[0] = new SqlParameter("@Title", SqlDbType.VarChar);
            sqlpara[0].Value = specialoffers.Title;
            sqlpara[1] = new SqlParameter("@Code", SqlDbType.VarChar);
            sqlpara[1].Value = specialoffers.Code;
            sqlpara[2] = new SqlParameter("@SortOrder", SqlDbType.VarChar);
            sqlpara[2].Value = specialoffers.SortOrder;
            sqlpara[3] = new SqlParameter("@UserId", SqlDbType.VarChar);
            sqlpara[3].Value = specialoffers.UserId;
            sqlpara[4] = new SqlParameter("@OfferId", SqlDbType.VarChar);
            sqlpara[4].Value = specialoffers.OfferId;

            int result;
            result = Convert.ToInt32(clshelper.ExecuteScalar("USP_CheckSpecialOfferById", sqlpara));
            return result;
        }

    }
}
