﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Common;
using System.Data;
using System.Data.SqlClient;
using BO.ViewModel;
using BO.Models;

namespace DataAccessLayer.Admin
{
    public class Membership_FeatureDLL
    {
        clsHelper objhelper = new clsHelper();
        clsCommon objcommon = new clsCommon();

        public DataTable GetMembershipDetails(int ID)
        {
            try
            {
                DataTable Ds = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[] {
                     new SqlParameter() {ParameterName = "@MemberShipId",Value= ID }
                };
                Ds = objhelper.DataTable("Usp_GetMembershipDetails", sqlpara);
                return Ds;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("Membership_FeatureDLL--GetMembershipAndFeathersDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetFeaturesDetails(int ID)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[] {
                    new SqlParameter() {ParameterName = "@FeaturesId",Value=ID }
                };
                dt = objhelper.DataTable("Usp_GetFeathersDeatils", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("Membership_FeatureDLL--GetFeaturesDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool InsertUpdateMemberShipFeatures(int MFID, int FeaturesId, int MembershipId, bool cansee, string MaxCount, int CreatedBy)
        {
            bool result = false;
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                     new SqlParameter() {ParameterName = "@MembershipFeathersId",Value= MFID },
                     new SqlParameter() {ParameterName = "@MembershipId",Value= MembershipId },
                     new SqlParameter() {ParameterName = "@FeatherId",Value= FeaturesId },
                     new SqlParameter() {ParameterName = "@cansee",Value= cansee },
                     new SqlParameter() {ParameterName = "@MaxCount",Value= MaxCount },
                     new SqlParameter() {ParameterName = "@CreatedBy",Value= CreatedBy },
               };
                result = objhelper.ExecuteNonQuery("Usp_InsertUpdateMembershipFeatures", sqlpara);
                return result;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("Membership_FeatureDLL--InsertUpdateMemberShipFeatures", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool DeleteExistMembershipdata(int MembershipId)
        {
            try
            {
                bool result = false;
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                     new SqlParameter() {ParameterName = "@MembershipId",Value= MembershipId },
                };
                result = objhelper.ExecuteNonQuery("Usp_DeleteCurrentMembership", sqlpara);
                return result;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("Membership_FeatureDLL--DeleteExistMembershipdata", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetMemberShipANDFeaturesDetails()
        {
            DataTable Dt = new DataTable();
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[] { };
                Dt = objhelper.DataTable("Usp_GetMembershipFeaturesDetais", sqlpara);
                return Dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("Membership_FeatureDLL--GetMemberShipANDFeaturesDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool DeleteMembershipFeaturesDelete(int Id)
        {
            bool Result = false;
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                     new SqlParameter() {ParameterName = "@MembershipFeaturesId",Value= Id },
                };
                return Result = objhelper.ExecuteNonQuery("Usp_DeleteMembershipFeaturesById", sqlpara);
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("Membership_FeatureDLL--DeleteMembershipFeaturesDelete", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetMembershipFeatures(int Id)
        {
            DataTable Result = new DataTable();
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                     new SqlParameter() {ParameterName = "@Id",Value= Id },
                };
                return Result = objhelper.DataTable("Usp_GetMemberShipFeatureesDetails", sqlpara);
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("Membership_FeatureDLL--Usp_GetMemberShipFeatureesDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool UpdateMemberMembership(int UserId, int MembershipId)
        {
            try
            {
                bool Result = false;
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                     new SqlParameter() {ParameterName = "@UserId",Value= UserId },
                     new SqlParameter() {ParameterName = "@MembershipId",Value= MembershipId },
                };
                return Result = objhelper.ExecuteNonQuery("Usp_UpdateMemberMembership", sqlpara);
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("Membership_FeatureDLL--UpdateMemberMembership", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool InsertMembershipHistoryofUser(Membership_History ObjMemHist)
        {
            try
            {
                bool Result = false;
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                     new SqlParameter() {ParameterName = "@UserId",Value= ObjMemHist.UserId },
                     new SqlParameter() {ParameterName = "@OldValue",Value= ObjMemHist.OldValues},
                     new SqlParameter() {ParameterName = "@NewValue",Value= ObjMemHist.NewValues },
                     new SqlParameter() {ParameterName = "@MembershipName",Value= ObjMemHist.MembershipName },
                     new SqlParameter() {ParameterName = "@CreatedBy",Value= ObjMemHist.CreatedBy },
                };
                return Result = objhelper.ExecuteNonQuery("Usp_InsertMembershipHistoryOfUser", sqlpara);
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("Membership_FeatureDLL--InsertMembershipHistoryofUser", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetMemberShipHistoryOfUser(int UserId)
        {
            try
            {
                DataTable Result = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                     new SqlParameter() {ParameterName = "@UserId",Value= UserId },
                };
                return Result = objhelper.DataTable("Usp_GetMemberShipHistoryOfUser", sqlpara);
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("Membership_FeatureDLL--UpdateMemberMembership", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetMembershipvaluebyPrice(int MounthlyPrice)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                     new SqlParameter() {ParameterName = "@MonthlyPrice",Value= MounthlyPrice },
                };
                return dt = objhelper.DataTable("Usp_GetMembershiplistonPrice", sqlpara);
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("Membership_FeatureDLL--GetMembershipvaluebyPrice", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetUpgreadeMembership(int MembershipID)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                     new SqlParameter() {ParameterName = "@MembershipId",Value= MembershipID },
                };
                return dt = objhelper.DataTable("GetMembershipUpgreadAccount", sqlpara);
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("Membership_FeatureDLL--GetUpgreadeMembership", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
