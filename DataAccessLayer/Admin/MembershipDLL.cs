﻿using BO.Models;
using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Admin
{
    public class MembershipDLL
    {
        clsHelper clshelper = new clsHelper();
        clsCommon objcommon = new clsCommon();
        public bool InsertUpdate(Membership obj)
        {
            bool result;
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[11];
                sqlpara[0] = new SqlParameter("@Membership_Id", SqlDbType.Int);
                sqlpara[0].Value = obj.Membership_Id;
                sqlpara[1] = new SqlParameter("@Title", SqlDbType.NVarChar);
                sqlpara[1].Value = obj.Title;
                sqlpara[2] = new SqlParameter("@Description", SqlDbType.NVarChar);
                sqlpara[2].Value = obj.Description;
                sqlpara[3] = new SqlParameter("@SortOrder", SqlDbType.Int);
                sqlpara[3].Value = obj.SortOrder;
                sqlpara[4] = new SqlParameter("@Status", SqlDbType.Int);
                sqlpara[4].Value = obj.Status;
                sqlpara[5] = new SqlParameter("@MonthlyPrice", SqlDbType.Decimal);
                sqlpara[5].Value = obj.MonthlyPrice;
                sqlpara[6] = new SqlParameter("@AnnualPrice", SqlDbType.Decimal);
                sqlpara[6].Value = obj.AnnualPrice;
                sqlpara[7] = new SqlParameter("@QuaterlyPrice", SqlDbType.Decimal);
                sqlpara[7].Value = obj.QuaterlyPrice;
                sqlpara[8] = new SqlParameter("@HalfYearlyPrice", SqlDbType.Decimal);
                sqlpara[8].Value = obj.HalfYearlyPrice;
                sqlpara[9] = new SqlParameter("@CreatedBy", SqlDbType.Int);
                sqlpara[9].Value = obj.CreatedBy;
                sqlpara[10] = new SqlParameter("@ModifiedBy", SqlDbType.Int);
                sqlpara[10].Value = obj.ModifiedBy;
                result = clshelper.ExecuteNonQuery("USP_InsertUpdateMembership", sqlpara);
                return result;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("MMembershipDLL--InsertUpdate", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public DataTable GetById(int Id)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Membership_Id", SqlDbType.Int);
                strParameter[0].Value = Id;
                dt = clshelper.DataTable("USP_GetMembershipById", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("MMembershipDLL--GetById", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public bool RemoveById(int Id)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Membership_Id", SqlDbType.Int);
                strParameter[0].Value = Id;
                int result = Convert.ToInt32(clshelper.ExecuteScalar("USP_DeleteMembership", strParameter));
                return result > 0;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("MMembershipDLL--RemoveById", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetAllMembershipList(int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] addParameter = new SqlParameter[5];
                addParameter[0] = new SqlParameter("@PageIndex", SqlDbType.Int);
                addParameter[0].Value = PageIndex;
                addParameter[1] = new SqlParameter("@PageSize", SqlDbType.Int);
                addParameter[1].Value = PageSize;
                addParameter[2] = new SqlParameter("@SortColumn", SqlDbType.Int);
                addParameter[2].Value = SortColumn;
                addParameter[3] = new SqlParameter("@SortDirection", SqlDbType.Int);
                addParameter[3].Value = SortDirection;
                addParameter[4] = new SqlParameter("@Searchtext", SqlDbType.NVarChar);
                addParameter[4].Value = Searchtext;
                dt = clshelper.DataTable("USP_GetAllMembershipDetail", addParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("MMembershipDLL--GetAllMembershipList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetMemberDetialsByMembership(int UserId,int MembershipId, int SortDirection, int SortColumn,int PageSize,int PageIndex,string SerachText= null)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[7];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@MemberShipId", SqlDbType.Int);
                strParameter[1].Value = MembershipId;
                strParameter[2] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
                strParameter[2].Value = SerachText;
                strParameter[3] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[3].Value = SortDirection;
                strParameter[4] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[4].Value = SortColumn;
                strParameter[5] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[5].Value = PageSize;
                strParameter[6] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[6].Value = PageIndex;
                dt = clshelper.DataTable("GetMemberbyMembershipdetails", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("MMembershipDLL--GetMemberbyMembershipdetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        
        /* Devansh: Not needed as of now.
        public DataTable GetMemberDetialsByMembershipForFree(int UserId, int MembershipId, int SortDirection, int SortColumn, int PageSize, int PageIndex, string SerachText = null)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[7];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@MemberShipId", SqlDbType.Int);
                strParameter[1].Value = MembershipId;
                strParameter[2] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
                strParameter[2].Value = SerachText;
                strParameter[3] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[3].Value = SortDirection;
                strParameter[4] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[4].Value = SortColumn;
                strParameter[5] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[5].Value = PageSize;
                strParameter[6] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[6].Value = PageIndex;
                dt = clshelper.DataTable("GetMemberbyMembershipdetailsForFree", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("MMembershipDLL--GetMemberbyMembershipdetailsForFree", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        */
        
        public DataTable GetAllMemberListByMembership(int UserId, int MembershipId, int SortDirection, int SortColumn, int PageSize, int PageIndex, string SerachText = null)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[7];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@MemberShipId", SqlDbType.Int);
                strParameter[1].Value = MembershipId;
                strParameter[2] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
                strParameter[2].Value = SerachText;
                strParameter[3] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[3].Value = SortDirection;
                strParameter[4] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[4].Value = SortColumn;
                strParameter[5] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[5].Value = PageSize;
                strParameter[6] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[6].Value = PageIndex;
                dt = clshelper.DataTable("GetAllMemberListByMembership", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("MMembershipDLL--GetAllMemberListByMembership", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public int GetMemberCountByMembershipId(int MembershipId)
        {
            int Count = 0;
            try
            {
               DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@MembershipId", SqlDbType.Int);
                strParameter[0].Value = MembershipId;
                dt = clshelper.DataTable("Usp_GetMemberCountByMembershipId", strParameter);
                if(dt!=null && dt.Rows.Count >0){
                    Count = Convert.ToInt32(dt.Rows[0]["CTN"]);
                }
                return Count;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("MembershipDLL--", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
