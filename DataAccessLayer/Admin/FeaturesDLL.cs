﻿using BO.Models;
using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.Admin
{
   public class FeaturesDLL
    {
        clsHelper clshelper = new clsHelper();
        clsCommon objcommon = new clsCommon();
        public bool InsertUpdate(Features obj)
        {
            bool result;
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[6];
                sqlpara[0] = new SqlParameter("@Feature_Id", SqlDbType.Int);
                sqlpara[0].Value = obj.Feature_Id;
                sqlpara[1] = new SqlParameter("@Title", SqlDbType.NVarChar);
                sqlpara[1].Value = obj.Title;
                sqlpara[2] = new SqlParameter("@Status", SqlDbType.Int);
                sqlpara[2].Value = obj.Status;
                sqlpara[3] = new SqlParameter("@SortOrder", SqlDbType.Int);
                sqlpara[3].Value = obj.SortOrder;
                sqlpara[4] = new SqlParameter("@CreatedBy", SqlDbType.Int);
                sqlpara[4].Value = obj.CreatedBy;
                sqlpara[5] = new SqlParameter("@ModifiedBy", SqlDbType.Int);
                sqlpara[5].Value = obj.ModifiedBy;

                result = clshelper.ExecuteNonQuery("USP_InsertUpdateFeature", sqlpara);
                return result;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("FeaturesDLL--InsertUpdate", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public DataTable GetById(int Id)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Feature_Id", SqlDbType.Int);
                strParameter[0].Value = Id;
                dt = clshelper.DataTable("USP_GetFeatureById", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("FeaturesDLL--GetById", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public bool RemoveById(int Id)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Feature_Id", SqlDbType.Int);
                strParameter[0].Value = Id;
                int result = Convert.ToInt32(clshelper.ExecuteScalar("USP_DeleteFeature", strParameter));
                return result > 0;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("FeaturesDLL--RemoveById", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetAll(int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] addParameter = new SqlParameter[5];
                addParameter[0] = new SqlParameter("@PageIndex", SqlDbType.Int);
                addParameter[0].Value = PageIndex;
                addParameter[1] = new SqlParameter("@PageSize", SqlDbType.Int);
                addParameter[1].Value = PageSize;
                addParameter[2] = new SqlParameter("@SortColumn", SqlDbType.Int);
                addParameter[2].Value = SortColumn;
                addParameter[3] = new SqlParameter("@SortDirection", SqlDbType.Int);
                addParameter[3].Value = SortDirection;
                addParameter[4] = new SqlParameter("@Searchtext", SqlDbType.NVarChar);
                addParameter[4].Value = Searchtext;
                dt = clshelper.DataTable("USP_GetAllFeatureDetail", addParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("FeaturesDLL--GetAll", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

    }
}
