﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.Common;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.IO;
using System.Web.UI.WebControls;
using System.Drawing;
using BO.ViewModel;
using BO.Models;
using DataAccessLayer.ColleaguesData;
using System.Data.SqlTypes;
using BO.Enums;
namespace DataAccessLayer
{
   public class VeridentDAL
    {
        #region Global Veriable
        public static clsHelper ObjHelper = new clsHelper();
        public static clsCommon ObjCommon = new clsCommon();
        #endregion

        public DataTable GetPatientsWithApp(BO.ViewModel.PatientFilterForVeri FilterObj)
        {
            try
            {
                DataTable dt = new DataTable();                
                SqlParameter[] strParameter = new SqlParameter[8];
                strParameter[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                strParameter[0].Value = FilterObj.UserId;
                strParameter[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[1].Value = FilterObj.PageIndex;
                strParameter[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[2].Value = FilterObj.PageSize;
                strParameter[3] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
                strParameter[3].Value = FilterObj.SearchText;
                strParameter[4] = new SqlParameter("@SortColumn", SqlDbType.NVarChar);
                strParameter[4].Value = FilterObj.SortColumn;
                strParameter[5] = new SqlParameter("@SortDirection", SqlDbType.NVarChar);
                strParameter[5].Value = FilterObj.SortDirection;
                strParameter[6] = new SqlParameter("@IsInsVerify", SqlDbType.Int);
                strParameter[6].Value = FilterObj.IsInsVerify;
                strParameter[7] = new SqlParameter("@LabelId", SqlDbType.NVarChar);
                strParameter[7].Value = string.Join(",", FilterObj.LabelId.ToArray()); 

                if (FilterObj.PatientWithApp == true)
                {
                    dt = ObjHelper.DataTable("Usp_GetAppointmentListForVerident", strParameter);
                }else
                {
                    dt = ObjHelper.DataTable("Usp_GetPatientListForVerident", strParameter);
                }
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentDAL -GetPatientsWithApp", Ex.Message, Ex.StackTrace);
                throw;
            }
        }


        /// <summary>
        /// Get Patient details for verident
        /// </summary>
        /// <param name="PatientID"></param>
        /// <returns></returns>
        public DataTable GetPatientDetails(int PatientID)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                dt = ObjHelper.DataTable("USP_GetPatientDetailsForVeri", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentDAL -GetPatientDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }


        /// <summary>
        /// Check Email Exists
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        public DataTable CheckEmailExists(string Email)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Email", SqlDbType.VarChar);
                strParameter[0].Value = Email;
                dt = ObjHelper.DataTable("USP_CheckDoctorEmailExists", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentDAL -CheckEmailExists", Ex.Message, Ex.StackTrace);
                throw;
            }
        }



        /// <summary>
        /// Add edit lable for verident
        /// </summary>
        /// <param name="objLable"></param>
        /// <returns></returns>
        public int AddEditLabel(Labels objLabel,int UserId, int AccountId)
        {
            try
            {
                int LabelId = 0;               
                SqlParameter[] sqlParam = new SqlParameter[5];
                sqlParam[0] = new SqlParameter("@LabelId", SqlDbType.Int);
                sqlParam[0].Value = objLabel.Id;
                sqlParam[1] = new SqlParameter("@LabelName", SqlDbType.VarChar);
                sqlParam[1].Value = objLabel.Name;
                sqlParam[2] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlParam[2].Value = UserId;
                sqlParam[3] = new SqlParameter("@AccountId", SqlDbType.Int);
                sqlParam[3].Value = AccountId;
                sqlParam[4] = new SqlParameter("@ILabelId", SqlDbType.Int);
                sqlParam[4].Direction = ParameterDirection.Output;
                ObjHelper.ExecuteNonQuery("USP_AddEditLabel", sqlParam);
                LabelId = Convert.ToInt32(sqlParam[4].Value);               
                return LabelId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentDAL -AddEditLabel", Ex.Message, Ex.StackTrace);
                throw;
            }
        }


        /// <summary>
        /// Delete label 
        /// </summary>
        /// <param name="LabelId"></param>
        /// <param name="UserId"></param>
        /// <param name="AccountId"></param>
        /// <returns></returns>
        public int DeleteLabel(int LabelId, int UserId,int AccountId)
        {
            try
            {
                 int labelStatus = 0;
                SqlParameter[] sqlParam = new SqlParameter[4];
                sqlParam[0] = new SqlParameter("@LabelId", SqlDbType.Int);
                sqlParam[0].Value = LabelId;
                sqlParam[1] = new SqlParameter("@UserId", SqlDbType.VarChar);
                sqlParam[1].Value = UserId;
                sqlParam[2] = new SqlParameter("@AccountId", SqlDbType.VarChar);
                sqlParam[2].Value = AccountId;
                sqlParam[3] = new SqlParameter("@reStatus", SqlDbType.Int);
                sqlParam[3].Direction = ParameterDirection.Output;
                ObjHelper.ExecuteNonQuery("USP_DeleteLabel", sqlParam);
                labelStatus = Convert.ToInt32(sqlParam[3].Value);
                return labelStatus;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentDAL -DeleteLabel", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Get label list
        /// </summary>
        /// <param name="LabelId"></param>
        /// <param name="AccountId"></param>
        /// <returns></returns>
        public DataTable GetLabels(int LabelId, int AccountId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlParam = new SqlParameter[2];
                sqlParam[0] = new SqlParameter("@LabelId", SqlDbType.Int);
                sqlParam[0].Value = LabelId;                
                sqlParam[1] = new SqlParameter("@AccountId", SqlDbType.VarChar);
                sqlParam[1].Value = AccountId;
                return ObjHelper.DataTable("USP_GetLabels", sqlParam);                
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentDAL -GetLabels", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool AddPatientLabel(PatientLabelAssociation patientLabel,int UserId,int Accountid)
        {
            try
            {
                foreach (var item in patientLabel.PatientId)
                {
                    SqlParameter[] sqlParam = new SqlParameter[4];
                    sqlParam[0] = new SqlParameter("@LabelId", SqlDbType.Int);
                    sqlParam[0].Value = patientLabel.LabelId;
                    sqlParam[1] = new SqlParameter("@UserId", SqlDbType.Int);
                    sqlParam[1].Value = UserId;
                    sqlParam[2] = new SqlParameter("@AccountId", SqlDbType.Int);
                    sqlParam[2].Value = Accountid;
                    sqlParam[3] = new SqlParameter("@PatientId", SqlDbType.Int);
                    sqlParam[3].Value = item;
                    ObjHelper.ExecuteNonQuery("Usp_AddPatientLabel", sqlParam);
                }
                return true;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentDAL--AddPatientLabel", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool RemovePatientLabel(PatientLabelAssociation patientLabel,int UserId,int AccountId)
        {
            try
            {
                foreach (var item in patientLabel.PatientId)
                {
                    SqlParameter[] sqlParam = new SqlParameter[4];
                    sqlParam[0] = new SqlParameter("@LabelId", SqlDbType.Int);
                    sqlParam[0].Value = patientLabel.LabelId;
                    sqlParam[1] = new SqlParameter("@UserId", SqlDbType.Int);
                    sqlParam[1].Value = UserId;
                    sqlParam[2] = new SqlParameter("@AccountId", SqlDbType.Int);
                    sqlParam[2].Value = AccountId;
                    sqlParam[3] = new SqlParameter("@PatientId", SqlDbType.Int);
                    sqlParam[3].Value = item;
                    ObjHelper.ExecuteNonQuery("Usp_RemovePatientLabelAssociation", sqlParam);
                }
                return true;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentDAL--RemovePatientLabel", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public DataSet GetUserLoginDetails(VeridentLogin objVeridentLogin)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlParam = new SqlParameter[2];
                sqlParam[0] = new SqlParameter("@Username", SqlDbType.VarChar);
                sqlParam[0].Value = objVeridentLogin.Username;
                sqlParam[1] = new SqlParameter("@Password", SqlDbType.VarChar);
                sqlParam[1].Value = objVeridentLogin.Password;                
                return ObjHelper.GetDatasetData("USP_GetUserDetailsForVerident", sqlParam);                             
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentDAL--GetUserLoginDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public int AddEditPatientVeri(PatientDetailsForVeri objPatientDetailsForVeri,int UserId, string Registration)
        {
            int nPatientId = 0;

            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlParam = new SqlParameter[14];
                sqlParam[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                sqlParam[0].Value = objPatientDetailsForVeri.PatientId;
                sqlParam[1] = new SqlParameter("@FirstName", SqlDbType.VarChar);
                sqlParam[1].Value = objPatientDetailsForVeri.FirstName;
                sqlParam[2] = new SqlParameter("@LastName", SqlDbType.VarChar);
                sqlParam[2].Value = objPatientDetailsForVeri.LastName;                
                sqlParam[3] = new SqlParameter("@Email", SqlDbType.VarChar);
                sqlParam[3].Value = objPatientDetailsForVeri.Email;
                sqlParam[4] = new SqlParameter("@DateOfBirth", SqlDbType.VarChar);
                sqlParam[4].Value = string.IsNullOrEmpty(objPatientDetailsForVeri.DateOfBirth) ? null : objPatientDetailsForVeri.DateOfBirth;
                sqlParam[5] = new SqlParameter("@Phone", SqlDbType.VarChar);
                sqlParam[5].Value = objPatientDetailsForVeri.Phone;
                sqlParam[6] = new SqlParameter("@MemberId", SqlDbType.VarChar);
                sqlParam[6].Value = objPatientDetailsForVeri.MemberId;
                sqlParam[7] = new SqlParameter("@InsuranceCompanyName", SqlDbType.VarChar);
                sqlParam[7].Value = objPatientDetailsForVeri.InsuranceCompanyName;
                sqlParam[8] = new SqlParameter("@NameofInsured", SqlDbType.VarChar);
                sqlParam[8].Value = objPatientDetailsForVeri.NameofInsured;
                sqlParam[9] = new SqlParameter("@InsuranceCompanyPhone", SqlDbType.VarChar);
                sqlParam[9].Value = objPatientDetailsForVeri.InsuranceCompanyPhone;
                sqlParam[10] = new SqlParameter("@GroupNumber", SqlDbType.VarChar);
                sqlParam[10].Value = objPatientDetailsForVeri.GroupNumber;
                sqlParam[11] = new SqlParameter("@Registration", SqlDbType.VarChar);
                sqlParam[11].Value = Registration;
                sqlParam[12] = new SqlParameter("@OwnerId", SqlDbType.VarChar);
                sqlParam[12].Value = UserId;
                sqlParam[13] = new SqlParameter("@nPatientId", SqlDbType.Int);
                sqlParam[13].Direction = ParameterDirection.Output;
                ObjHelper.ExecuteNonQuery("USP_AddEditPatientForVerident", sqlParam);
                nPatientId = Convert.ToInt32(sqlParam[13].Value);
                return nPatientId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentDAL -AddEditPatientVeri", Ex.Message, Ex.StackTrace);
                throw;
            }            
        }



        public bool  ResetPasswordOfDoctor(UserAccountPro objResetPassword)
        {
            try
            {
                bool rStatus = false;
                SqlParameter[] sqlParam = new SqlParameter[2];
                sqlParam[0] = new SqlParameter("@UserName", SqlDbType.VarChar);
                sqlParam[0].Value = objResetPassword.Email;
                sqlParam[1] = new SqlParameter("@Password", SqlDbType.VarChar);
                sqlParam[1].Value = objResetPassword.Password;
                rStatus = ObjHelper.ExecuteNonQuery("USP_ResetUserPassword", sqlParam);
                return rStatus;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentDAL--UpdatePasswordOfDoctor", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        //public DataTable GetPatientRegistarionDetails(int PatientId)
        //{            
        //    try
        //    {                
        //        SqlParameter[] sqlParam = new SqlParameter[1];
        //        sqlParam[0] = new SqlParameter("@PatientId", SqlDbType.VarChar);
        //        sqlParam[0].Value = PatientId;                
        //        return ObjHelper.DataTable("DentalRegistration_veri", sqlParam);
        //    }
        //    catch (Exception Ex)
        //    {
        //        new clsCommon().InsertErrorLog("VeridentDAL -GetPatientRegistarionDetails", Ex.Message, Ex.StackTrace);
        //        throw;
        //    }            
        //}
    }
}
