﻿using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer
{
    public class clsPlanPricingDAL
    {
        //GetPlanPricingDetail
        clsHelper clshelper;
        clsCommon objcommon = new clsCommon();
        public clsPlanPricingDAL()
        {
            clshelper = new clsHelper();
        }

        #region Plan Pricing        
        public DataTable GetPlanPricingDetail()
        {
            try
            {
                DataTable dt = new DataTable();                
                dt = clshelper.DataTable("sp_getMembershipPlanFeatures");
                return dt;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion
    }
}
