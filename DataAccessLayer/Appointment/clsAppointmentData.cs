﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using DataAccessLayer.Common;
namespace DataAccessLayer.Appointment
{
    public class clsAppointmentData
    {
        clsHelper objhelper = new clsHelper();
        clsCommon objcommon = new clsCommon();

        #region DoctorService methods
        public DataTable GetAppointmentServices(int DoctorId, int ServiceId)
        {
            SqlParameter[] sqlpara = new SqlParameter[] {
                new SqlParameter() {ParameterName = "@DoctorId",Value= DoctorId },
                new SqlParameter() {ParameterName = "@AppServiceId",Value= ServiceId }
            };
            var dt = objhelper.DataTable("Usp_GetDoctorAppointmentServices", sqlpara);
            return dt;
        }
        public bool InsertAppointmentServices(int AppointmentServiceId, int DoctorId, string ServiceType, TimeSpan TimeDuration, string Price)
        {
            bool UpdateStatus = false;
            SqlParameter[] sqlpara = new SqlParameter[5];
            sqlpara[0] = new SqlParameter("@AppointmentServiceId", SqlDbType.Int);
            sqlpara[0].Value = AppointmentServiceId;
            sqlpara[1] = new SqlParameter("@DoctorId", SqlDbType.Int);
            sqlpara[1].Value = DoctorId;
            sqlpara[2] = new SqlParameter("@ServiceType", SqlDbType.Text);
            sqlpara[2].Value = ServiceType;
            sqlpara[3] = new SqlParameter("@TimeDuration", SqlDbType.Time);
            sqlpara[3].Value = TimeDuration;
            sqlpara[4] = new SqlParameter("@Price", SqlDbType.Float);
            sqlpara[4].Value = Price;


            UpdateStatus = objhelper.ExecuteNonQuery("Usp_InsertUpdateAppointmentServices", sqlpara);

            return UpdateStatus;

        }
        public bool CheckServiceName(string ServiceName, int ServiceId, int DoctorId)
        {
            return objhelper.DataTable("USP_CheckServiceName", new SqlParameter[] {
                new SqlParameter() { ParameterName = "@DoctorId", Value = DoctorId },
                 new SqlParameter() { ParameterName = "@ServiceId", Value = ServiceId },
                new SqlParameter() { ParameterName = "@ServiceName", Value = ServiceName },
            }).Rows.Count > 0 ? true : false;
        }
        public int DeleteAppointmentService(int appointmentServiceId, int DoctorId)
        {
            SqlParameter[] sqlpara = new SqlParameter[] {
                new SqlParameter() {ParameterName = "@AppointmentServiceId", Value = appointmentServiceId },
                  new SqlParameter() { ParameterName = "@DoctorId", Value = DoctorId }
            };
            return Convert.ToInt32(objhelper.ExecuteScalar("USP_DeleteAppointmentSerivce", sqlpara));
        }
        #endregion

        #region Doctor Week Schedule methods
        public void InsertAppointmentWeekSchedule(int AppointmentWeekScheduleId, int AppointmentWorkingTimeAllocationId, int DoctorId, int Day, bool IsOffDay, TimeSpan? MorningFromTime, TimeSpan? MorningToTime, TimeSpan? EveningFromTime, TimeSpan? EveningToTime, DateTime CreatedOn)
        {
            bool bl = false;
            SqlParameter[] sqlpara = new SqlParameter[10];
            sqlpara[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
            sqlpara[0].Value = DoctorId;
            sqlpara[1] = new SqlParameter("@Day", SqlDbType.Int);
            sqlpara[1].Value = Day;
            sqlpara[2] = new SqlParameter("@IsOffDay", SqlDbType.Text);
            sqlpara[2].Value = IsOffDay ? "N" : "Y";
            sqlpara[3] = new SqlParameter("@MorningFromTime", SqlDbType.Time);
            sqlpara[3].Value = MorningFromTime;
            sqlpara[4] = new SqlParameter("@MorningToTime", SqlDbType.Time);
            sqlpara[4].Value = MorningToTime;
            sqlpara[5] = new SqlParameter("@EveningFromTime", SqlDbType.Time);
            sqlpara[5].Value = EveningFromTime;
            sqlpara[6] = new SqlParameter("@EveningToTime", SqlDbType.Time);
            sqlpara[6].Value = EveningToTime;
            sqlpara[7] = new SqlParameter("@CreatedOn", SqlDbType.DateTime);
            sqlpara[7].Value = CreatedOn;
            sqlpara[8] = new SqlParameter("@AppointmentWeekScheduleId", SqlDbType.Int);
            sqlpara[8].Value = AppointmentWeekScheduleId;
            sqlpara[9] = new SqlParameter("@AppointmentWorkingTimeAllocationId", SqlDbType.Int);
            sqlpara[9].Value = AppointmentWorkingTimeAllocationId;

            bl = objhelper.ExecuteNonQuery("Usp_InsertUpdateWeekSchedule", sqlpara);
        }

        public bool UpdateAppointmentWeekSchedule(int AppointmentWeekScheduleId, int AppointmentWorkingTimeAllocationId, bool IsOffDay, TimeSpan? MorningFromTime, TimeSpan? MorningToTime, TimeSpan? EveningFromTime, TimeSpan? EveningToTime, int DoctorId, DateTime CreatedOn, int Day)
        {
            bool UpdateStatus = false;
            SqlParameter[] sqlpara = new SqlParameter[10];
            sqlpara[0] = new SqlParameter("@AppointmentWeekScheduleId", SqlDbType.Int);
            sqlpara[0].Value = AppointmentWeekScheduleId;
            sqlpara[1] = new SqlParameter("@AppointmentWorkingTimeAllocationId", SqlDbType.Int);
            sqlpara[1].Value = AppointmentWorkingTimeAllocationId;
            sqlpara[2] = new SqlParameter("@IsOffDay", SqlDbType.Text);
            sqlpara[2].Value = IsOffDay ? "N" : "Y";
            sqlpara[3] = new SqlParameter("@MorningFromTime", SqlDbType.Time);
            sqlpara[3].Value = MorningFromTime;
            sqlpara[4] = new SqlParameter("@MorningToTime", SqlDbType.Time);
            sqlpara[4].Value = MorningToTime;
            sqlpara[5] = new SqlParameter("@EveningFromTime", SqlDbType.Time);
            sqlpara[5].Value = EveningFromTime;
            sqlpara[6] = new SqlParameter("@EveningToTime", SqlDbType.Time);
            sqlpara[6].Value = EveningToTime;
            sqlpara[7] = new SqlParameter("@DoctorId", SqlDbType.Int);
            sqlpara[7].Value = DoctorId;
            sqlpara[8] = new SqlParameter("@Day", SqlDbType.Int);
            sqlpara[8].Value = Day;
            sqlpara[9] = new SqlParameter("@CreatedOn", SqlDbType.DateTime);
            sqlpara[9].Value = CreatedOn;

            UpdateStatus = objhelper.ExecuteNonQuery("Usp_InsertUpdateWeekSchedule", sqlpara);
            return UpdateStatus;
        }

        public DataTable GetDoctorAppointmentWeekScheduleDetails(int DoctorId)
        {
            try
            {
                DataTable ds = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[1];
                sqlpara[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                sqlpara[0].Value = DoctorId;
                ds = objhelper.DataTable("Usp_GetDoctorAppointmentWeekScheduleDetails", sqlpara);

                return ds;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Create Appointment Methods
        public DataTable GetPatientList(int DoctorId, int PatientId, int locationId = 0, string searchText = null)
        {
            if (string.IsNullOrEmpty(searchText))
            {
                searchText = null;
            }
            return objhelper.DataTable("USP_GetPatientListForAppointment", new SqlParameter[] {
                new SqlParameter() { ParameterName = "@DoctorId", Value = DoctorId },
                new SqlParameter() { ParameterName = "@PatientId", Value = PatientId },
                new SqlParameter() { ParameterName = "@locationId", Value = locationId },
                new SqlParameter() { ParameterName = "@searchText", Value = searchText }
            });
        }
        public DataSet GetCreateAppointmentTimeSlotViewData(int DoctorId, int TheatreID, int ApptointmentId)
        {
            return objhelper.GetDatasetData("USP_GetCreateAppointmentDataForTimeAllocation", new SqlParameter[]
            { new SqlParameter() {ParameterName = "@DoctorId", Value = DoctorId },
              new SqlParameter() {ParameterName ="@TheatreID",Value= TheatreID},
               new SqlParameter() {ParameterName ="@ApptointmentId",Value= ApptointmentId}

            });
        }
        public DataTable GetWeekScheduleData(int LocationId)
        {
            return objhelper.DataTable("SP_GetWeekSchduleByLocation", new SqlParameter[]
            {
                new SqlParameter() {ParameterName = "@LocationId", Value = LocationId }

            });
        }

        public int SubmitAppointmentBookingData(int DoctorId, int PatientId, DateTime AppointmentDate, TimeSpan AppointmentTime, double AppointmentLength, string ServiceId, string Note, int CreatedBy, int CreatedByUserType, int ApptResourceId, int UpdatedBy, int AppointmentId, int InsuranceId)
        {
            return Convert.ToInt32(objhelper.DataTable("USP_CreateAppointmentByDoctor", new SqlParameter[] {
                new SqlParameter() {ParameterName ="@DoctorId",Value= DoctorId},
                new SqlParameter() {ParameterName ="@PatientId",Value= PatientId},
                new SqlParameter() {ParameterName ="@AppointmentDate",Value= AppointmentDate},
                new SqlParameter() {ParameterName ="@AppointmentTime",Value= AppointmentTime},
                new SqlParameter() {ParameterName ="@AppointmentLength",Value= AppointmentLength},
                new SqlParameter() {ParameterName ="@ServiceTypeId",Value=ServiceId },
                new SqlParameter() {ParameterName ="@Note",Value= Note},
                new SqlParameter() {ParameterName = "@CreatedBy",Value=CreatedBy},
                new SqlParameter() {ParameterName = "@CreatedByUserType",Value=CreatedByUserType},
                new SqlParameter() {ParameterName = "@ApptResourceId",Value=ApptResourceId},
                new SqlParameter() {ParameterName = "@UpdatedBy",Value=UpdatedBy},
                new SqlParameter() {ParameterName = "@AppointmentId",Value=AppointmentId},
                new SqlParameter() {ParameterName = "@InsuranceId",Value=InsuranceId}
            }).Rows[0]["AppointmentId"]);
        }
        public DataTable CheckAvalibleDoctorByDateTimeTheatreDoctor(int DoctorId, TimeSpan AppointmentFromTime, TimeSpan AppointmentToTime, DateTime AppintmentDate, int ApptointmentId)
        {
            return objhelper.DataTable("CheckAvalibleDoctorByDateTimeTheatreDoctor", new SqlParameter[] {
                new SqlParameter() {ParameterName ="@DoctorId",Value= DoctorId},
                new SqlParameter() {ParameterName ="@AppointmentFromTime",Value= AppointmentFromTime},
                 new SqlParameter() {ParameterName ="@AppointmentToTime",Value= AppointmentToTime},
                new SqlParameter() {ParameterName ="@AppintmentDate",Value= AppintmentDate},
                new SqlParameter() {ParameterName ="@ApptointmentId",Value= ApptointmentId},

                });
        }
        public DataTable CheckAvalibleDoctorByDateTimeTheatrePatientId(int PatientId, TimeSpan AppointmentFromTime, TimeSpan AppointmentToTime, DateTime AppintmentDate, int ApptointmentId)
        {
            return objhelper.DataTable("CheckAvalibleDoctorByDateTimeTheatrePatientId", new SqlParameter[] {
                new SqlParameter() {ParameterName ="@PatientId",Value= PatientId},
                new SqlParameter() {ParameterName ="@AppointmentFromTime",Value= AppointmentFromTime},
                 new SqlParameter() {ParameterName ="@AppointmentToTime",Value= AppointmentToTime},
                new SqlParameter() {ParameterName ="@AppintmentDate",Value= AppintmentDate},
                new SqlParameter() {ParameterName ="@ApptointmentId",Value= ApptointmentId},

                });
        }


        
        public DataTable ViewDetailsForBookedAppointment(int AppointmentId)
        {
            return objhelper.DataTable("USP_AppointmentDetailsView", new SqlParameter[] {
                new SqlParameter() { ParameterName = "@AppointmentId", Value = AppointmentId },
            });
        }
        public DataTable GetSingleAppointmentData(int AppointmentId, int ParentUserId)
        {
            return objhelper.DataTable("GetSingleAppointmentData", new SqlParameter[] {
                new SqlParameter() { ParameterName = "@AppointmentId", Value = AppointmentId },
                new SqlParameter() { ParameterName = "@ParentUserId", Value = ParentUserId },
            });
        }

        public void CancelAppointment(int AppointmentId, int CancelledBy, int CancelledByUserType, string CancelNote, int AppointmentStatusId)
        {
            objhelper.DataTable("Usp_CancleAppintmentFormPatient_MydentalFile", new SqlParameter[] {
                new SqlParameter() { ParameterName = "@AppointmentId", Value = AppointmentId },
                new SqlParameter() {ParameterName ="@CancelledBy",Value=CancelledBy },
                new SqlParameter() {ParameterName ="@CancelledByUserType",Value=CancelledByUserType },
                new SqlParameter() {ParameterName ="@CancelNote",Value=CancelNote },
                new SqlParameter() {ParameterName ="@AppointmentStatusId",Value=AppointmentStatusId },
                 });
        }
        public void ChangeAppointmentStatus(int AppointmentId, int AppointmentStatusId)
        {
            objhelper.DataTable("SP_UpdateAppointmentStatus", new SqlParameter[] {
                new SqlParameter() { ParameterName = "@AppointmentId", Value = AppointmentId },
                new SqlParameter() {ParameterName ="@AppointmentStatusId",Value=AppointmentStatusId },
         });
        }
        public DataTable GetBookedAppointmentForCalendar(int DoctorId, DateTime FromDate, DateTime ToDate, int LocationId)
        {
            return objhelper.DataTable("USP_GetAppointmentDataForCalander", new SqlParameter[] {
                new SqlParameter() { ParameterName = "@DoctorId", Value = DoctorId },
                new SqlParameter() { ParameterName = "@FromDate",Value = FromDate },
                new SqlParameter() { ParameterName = "@ToDate", Value = ToDate },
                new SqlParameter() { ParameterName = "@LocationId", Value = LocationId }

            });
        }
        public DataTable GetOprationTheatorForCalendarByLocation(int LocationId)
        {
            return objhelper.DataTable("USP_GetAppointmentDataForCalanderByLocation", new SqlParameter[] {
                new SqlParameter() { ParameterName = "@LocationId", Value = LocationId },
                });
        }
        public DataTable GetDoctorDetails(int DoctorId)
        {
            return objhelper.DataTable("USP_GetDoctorDetails", new SqlParameter[] {
                new SqlParameter() { ParameterName = "@DoctorId", Value = DoctorId },
            });
        }


        public DataTable CheckPatientExists(string Email)
        {
            return objhelper.DataTable("CheckPatientExists", new SqlParameter[] {
                new SqlParameter() { ParameterName = "@PatientEmail", Value = Email },
            });
        }
        public DataTable GetEmailAllFieldForCreateAppointmentByAppointmentId(int AppointmentId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[1];
                sqlpara[0] = new SqlParameter("@AppointmentId", SqlDbType.Int);
                sqlpara[0].Value = AppointmentId;
                dt = objhelper.DataTable("Usp_EmailNotificationforCreateAppointment", sqlpara);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public DataTable GetDoctorAppointmentData(int DoctorId, int SortColumn, int SortDirection, int PageIndex, int PageSize)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[5];
                sqlpara[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                sqlpara[0].Value = DoctorId;
                sqlpara[1] = new SqlParameter("@SortColumn", SqlDbType.Int);
                sqlpara[1].Value = SortColumn;
                sqlpara[2] = new SqlParameter("@SortDirection", SqlDbType.Int);
                sqlpara[2].Value = SortDirection;
                sqlpara[3] = new SqlParameter("@PageIndex", SqlDbType.Int);
                sqlpara[3].Value = PageIndex;
                sqlpara[4] = new SqlParameter("@PageSize", SqlDbType.Int);
                sqlpara[4].Value = PageSize;

                dt = objhelper.DataTable("Usp_GetAppointmentDashboardNotification", sqlpara);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public DataTable GetTop3PatientAppointment(int PatientId, int AppointmentType)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[2];
                sqlpara[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                sqlpara[0].Value = PatientId;
                sqlpara[1] = new SqlParameter("@AppointmentType", SqlDbType.Int);
                sqlpara[1].Value = AppointmentType;
                dt = objhelper.DataTable("SP_GetTop3PatientAppointment", sqlpara);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public DataTable GetPateintDetailsByPatientId(long PatientId,int? DoctorId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = PatientId;
                strParameter[1] = new SqlParameter("@Doctorid", SqlDbType.Int);
                strParameter[1].Value = DoctorId;
                dt = objhelper.DataTable("USP_GetPateintDetailsByPatientId",strParameter);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region Doctor Operation Theatre methods
        public DataTable GetAppointmentRosocurces(int DoctorId, int locationId, int resourceId = 0)
        {
            SqlParameter[] sqlpara = new SqlParameter[] {
                new SqlParameter() {ParameterName = "@DoctorId",Value= DoctorId },
                new SqlParameter() {ParameterName = "@locationId",Value= locationId },
                new SqlParameter() {ParameterName = "@resourceId",Value= resourceId }
            };
            var dt = objhelper.DataTable("Usp_GetAppointmentRosources", sqlpara);
            return dt;
        }
        public DataTable GetLocationForAppointment(int DoctorId)
        {
            SqlParameter[] sqlpara = new SqlParameter[] {
                new SqlParameter() {ParameterName = "@UserId",Value= DoctorId }
            };
            var dt = objhelper.DataTable("Usp_GetLocationOfDoctorbyUserId", sqlpara);
            return dt;
        }

        public DataTable GetInuranceNamesOfDoctorForAppointment(int DoctorId)
        {
            SqlParameter[] sqlpara = new SqlParameter[] {
                new SqlParameter() {ParameterName = "@UserId",Value= DoctorId }
            };
            var dt = objhelper.DataTable("usp_GetInuranceNamesOfDoctor", sqlpara);
            return dt;
        }
        public DataTable GetAppointmentStaus()
        {
            SqlParameter[] sqlpara = new SqlParameter[] { };
            var dt = objhelper.DataTable("SP_GetAppointmentStatus", sqlpara);
            return dt;
        }
        public DataTable GetDentistForAppointment(int DoctorId, int LocationId)
        {
            SqlParameter[] sqlpara = new SqlParameter[] {
                new SqlParameter() {ParameterName = "@UserId",Value= DoctorId },
                  new SqlParameter() {ParameterName = "@LocationId",Value= LocationId }
            };
            var dt = objhelper.DataTable("USP_GetDentistListByDentistandLocation", sqlpara);
            return dt;
        }
        public bool InsertAppointmentResources(
               int LocationId,
               string ResourceName,
               string EventTextColor,
               string EventBackgroundColor,
               string EventBorderColor,
               string EventColor,
               int UserId,
               int ApptResourceId
            )
        {
            bool UpdateStatus = false;
            SqlParameter[] sqlpara = new SqlParameter[8];
            sqlpara[0] = new SqlParameter("@LocationId", SqlDbType.Int);
            sqlpara[0].Value = LocationId;
            sqlpara[1] = new SqlParameter("@ResourceName", SqlDbType.Text);
            sqlpara[1].Value = ResourceName;
            sqlpara[2] = new SqlParameter("@EventTextColor", SqlDbType.Text);
            sqlpara[2].Value = EventTextColor;
            sqlpara[3] = new SqlParameter("@EventBackgroundColor", SqlDbType.Text);
            sqlpara[3].Value = EventBackgroundColor;
            sqlpara[4] = new SqlParameter("@EventBorderColor", SqlDbType.Text);
            sqlpara[4].Value = EventBorderColor;
            sqlpara[5] = new SqlParameter("@EventColor", SqlDbType.Text);
            sqlpara[5].Value = EventColor;
            sqlpara[6] = new SqlParameter("@UserId", SqlDbType.Int);
            sqlpara[6].Value = UserId;
            sqlpara[7] = new SqlParameter("@ApptResourceId", SqlDbType.Int);
            sqlpara[7].Value = ApptResourceId;


            UpdateStatus = objhelper.ExecuteNonQuery("Usp_InsertUpdateAppointmentResources", sqlpara);

            return UpdateStatus;

        }

        public int InsertAPIAppointmentResources(BO.Models.Operatory obj)
        {
            bool UpdateStatus = false;
            SqlParameter[] sqlpara = new SqlParameter[7];
            sqlpara[0] = new SqlParameter("@OperatoryId", SqlDbType.Text);
            sqlpara[0].Value = obj.OperatoryId;
            sqlpara[1] = new SqlParameter("@IdNumber", SqlDbType.Int);
            sqlpara[1].Value = obj.DentrixId;
            sqlpara[2] = new SqlParameter("@Title", SqlDbType.Text);
            sqlpara[2].Value = obj.Title;
            sqlpara[3] = new SqlParameter("@DentrixConnetcorkey", SqlDbType.VarChar);
            sqlpara[3].Value = obj.DentrixConnectorId;
            sqlpara[4] = new SqlParameter("@AppoinmentResourceId", SqlDbType.Int);
            sqlpara[4].Direction = ParameterDirection.Output;
            sqlpara[5] = new SqlParameter("@Identifier", SqlDbType.Int);
            sqlpara[5].Value = obj.Identifier;
            sqlpara[6] = new SqlParameter("@AutoModifiedTimeStamp", SqlDbType.VarChar);
            sqlpara[6].Value = obj.automodifiedtimestamp;
            UpdateStatus = objhelper.ExecuteNonQuery("Usp_InsertAPIUpdateAppointmentResources", sqlpara);
            int RlAppointmentId = Convert.ToInt32(sqlpara[4].Value);
            return RlAppointmentId;

        }

        public DataTable GetAPIDentrixSyncDetail(string DentrixConnectorID)
        {
            DataTable Dt = new DataTable();
            SqlParameter[] sqlpara = new SqlParameter[1];
            sqlpara[0] = new SqlParameter("@DentrixConnectorID", SqlDbType.NVarChar);
            sqlpara[0].Value = DentrixConnectorID;
            Dt = objhelper.DataTable("Usp_GetAPIDentrixSyncDetail", sqlpara);
            return Dt;
        }


        public bool DeleteAPIAppointmentResource(BO.Models.RemoveOperatory robj)
        {
            bool DeleteStatus = false;
            SqlParameter[] sqlpara = new SqlParameter[2];
            sqlpara[0] = new SqlParameter("@IdNumber", SqlDbType.Int);
            sqlpara[0].Value = robj.DentrixId;
            sqlpara[1] = new SqlParameter("@DentrixConnectorKey", SqlDbType.VarChar);
            sqlpara[1].Value = robj.DentrixConnecoterKey;
            DeleteStatus = objhelper.ExecuteNonQuery("Usp_DeleteAPIAppointmentResources", sqlpara);
            return DeleteStatus;
        }


        public bool CheckResourceName(string ResourceName, int LocationId, int ApptResourceId)
        {
            return objhelper.DataTable("USP_CheckResourceName", new SqlParameter[] {
                new SqlParameter() { ParameterName = "@ApptResourceId", Value = ApptResourceId },
                 new SqlParameter() { ParameterName = "@LocationId", Value = LocationId },
                new SqlParameter() { ParameterName = "@ResourceName", Value = ResourceName },
            }).Rows.Count > 0 ? true : false;
        }
        public int DeleteOperationTheatre(int locationId, int resourceId, int DoctorId)
        {
            SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter() { ParameterName = "@locationId", Value = locationId },
                new SqlParameter() { ParameterName = "@resourceId", Value = resourceId },
                new SqlParameter() { ParameterName = "@DoctorId", Value = DoctorId }
            };
            int id= Convert.ToInt32(objhelper.ExecuteScalar("USP_DeleteAppointmentResource", sqlpara));
            return id;
        }
        public DataTable GetSingleAppointmentData(int AppointmentId)
        {
            SqlParameter[] sqlpara = new SqlParameter[] {
                new SqlParameter() {ParameterName = "@AppointmentId",Value= AppointmentId },
               };
            var dt = objhelper.DataTable("SP_GetSingleAppointmentData", sqlpara);
            return dt;
        }
        public void UpdateAppointmentBookingData(DateTime AppointmentDate, TimeSpan AppointmentTime, int ApptResourceId, int AppointmentId)
        {
            objhelper.DataTable("USP_UpdateAppointmentByDoctor", new SqlParameter[] {
                new SqlParameter() {ParameterName ="@AppointmentDate",Value= AppointmentDate},
                new SqlParameter() {ParameterName ="@AppointmentTime",Value= AppointmentTime},
                new SqlParameter() {ParameterName = "@ApptResourceId",Value=ApptResourceId},
                new SqlParameter() {ParameterName = "@AppointmentId",Value=AppointmentId},
          });
        }

        public string GetTimeZoneOfDoctor(int doctorId)
        {
            SqlParameter[] sqlpara = new SqlParameter[1];
            sqlpara[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
            sqlpara[0].Value = doctorId;
            
            return objhelper.ExecuteScalar("USP_GetTimezoneOfDoctor", sqlpara);
        }
        #endregion
    }
}
