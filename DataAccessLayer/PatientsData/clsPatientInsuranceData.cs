﻿using BO.Models;
using BO.ViewModel;
using DataAccessLayer.Common;
using DataAccessLayer.PatientsData.Schema;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.PatientsData
{
    public class clsPatientInsuranceData
    {
        public static clsHelper ObjHelper = new clsHelper();
        public static clsCommon ObjCommon = new clsCommon();
        private SqlConnection objCon = new SqlConnection();
        public static string IntegrationConnectionString = ConfigurationManager.ConnectionStrings["INTEGRATIONCONNECTION"].ConnectionString;


        public int SaveCarrier(BO.Models.InsuranceCarrier model)
        {

            var _db = new PatientContext(ConfigurationManager.ConnectionStrings["CONNECTIONSTRING"].ConnectionString);
            var dentrixConnector = _db.DentrixConnector.AsNoTracking().Where(q => q.AccountKey == model.dentrixConnectorID).FirstOrDefault();
            model.PMSConnectorId = dentrixConnector.DentrixConnectorId;
            
            // sprocs for saving data //
            ///////////////////////////
            //save carrier info 
            var carrierId = SaveCarrierInfo(model);
            //save carrier addresses 
            SaveCarrierAddresses(model, carrierId);
            //save carrier phonenumbers 
            SaveCarrierPhoneNumbers(model, carrierId);
            //save carrier emails 
            SaveCarrierEmails(model, carrierId);

            return carrierId;
        }


        public int SaveInsuredRecord(BO.Models.InsuredRecord model)
        {
            var _db = new PatientContext(ConfigurationManager.ConnectionStrings["CONNECTIONSTRING"].ConnectionString);
            var dentrixConnector = _db.DentrixConnector.AsNoTracking().Where(q => q.AccountKey == model.dentrixConnectorID).FirstOrDefault();
            model.PMSConnectorId = dentrixConnector.DentrixConnectorId;
            // sprocs for saving data //
            ///////////////////////////           
            //save insured record 
            var insuredRecordId = SaveInsuredRecordInfo(model);
            //save insured members 
            SaveInsuredMember(model, insuredRecordId);

            return insuredRecordId;
        }

        /// <summary>
        /// This Method will save insured records from outside systems.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string SaveJsonInsuredRecord(DataTable JsonString)
        {
            clsHelper IntObjHelper = new clsHelper(IntegrationConnectionString);
            SqlParameter[] SqlParam = new SqlParameter[1];

            SqlParam[0] = new SqlParameter("@JsonString", SqlDbType.Structured);
            SqlParam[0].Value = JsonString;
            //string isSuccess = ObjHelper.ExecuteScalar("USP_NewInsertInsuredRecord", SqlParam);
            string isSuccess = IntObjHelper.ExecuteScalar("USP_NewInsertInsuredRecord", SqlParam);
            return isSuccess;
        }

        /// <summary>
        /// This Method will save insurance carrier data from outside systems.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public string SaveJsonCarrierInfo(DataTable JsonString)
        {
            clsHelper IntObjHelper = new clsHelper(IntegrationConnectionString);
            SqlParameter[] SqlParam = new SqlParameter[1];

            SqlParam[0] = new SqlParameter("@JsonString", SqlDbType.Structured);
            SqlParam[0].Value = JsonString;
            //string isSuccess = ObjHelper.ExecuteScalar("USP_NewInsertInsuranceCarrier", SqlParam);
            string isSuccess = IntObjHelper.ExecuteScalar("USP_NewInsertInsuranceCarrier", SqlParam);
            return isSuccess;
        }

        #region Carrier
        private int SaveCarrierInfo(BO.Models.InsuranceCarrier model)
        {
            if (model == null)
            {
                model = new BO.Models.InsuranceCarrier();
            }
            try
            {
                SqlParameter[] SqlParam = new SqlParameter[16];

                SqlParam[0] = new SqlParameter("@CarrierId", SqlDbType.Int);
                SqlParam[0].Value = model.CarrierID;
                SqlParam[1] = new SqlParameter("@InsuranceType", SqlDbType.Int);
                SqlParam[1].Value = model.InsuranceType;
                SqlParam[2] = new SqlParameter("@Name", SqlDbType.VarChar);
                SqlParam[2].Value = model.Name;
                SqlParam[3] = new SqlParameter("@GroupName", SqlDbType.VarChar);
                SqlParam[3].Value = model.GroupName;
                SqlParam[4] = new SqlParameter("@GroupNumber", SqlDbType.VarChar);
                SqlParam[4].Value = model.GroupNumber;
                SqlParam[5] = new SqlParameter("@CoverageTableId", SqlDbType.Int);
                SqlParam[5].Value = model.CoverageTableId;
                SqlParam[6] = new SqlParameter("@RenewalMonth", SqlDbType.Int);
                SqlParam[6].Value = model.RenewalMonth;
                SqlParam[7] = new SqlParameter("@FeeScheduleId", SqlDbType.Int);
                SqlParam[7].Value = model.FeeScheduleId;
                SqlParam[8] = new SqlParameter("@PayorId", SqlDbType.VarChar);
                SqlParam[8].Value = model.PayorId;
                SqlParam[9] = new SqlParameter("@Person", SqlDbType.Decimal);
                SqlParam[9].Value = model.Person;
                SqlParam[10] = new SqlParameter("@Family", SqlDbType.Decimal);
                SqlParam[10].Value = model.Family;
                SqlParam[11] = new SqlParameter("@DentrixId", SqlDbType.Int);
                SqlParam[11].Value = model.DentrixId;
                SqlParam[12] = new SqlParameter("@PracticeID", SqlDbType.Int);
                SqlParam[12].Value = model.PracticeID;
                SqlParam[13] = new SqlParameter("@ConnectorID", SqlDbType.VarChar);
                SqlParam[13].Value = model.ConnectorID;
                SqlParam[14] = new SqlParameter("@pmsConnectorID", SqlDbType.Int);
                SqlParam[14].Value = model.PMSConnectorId;

                SqlParam[15] = new SqlParameter("@DateCreated", SqlDbType.DateTime);
                SqlParam[15].Value = DateTime.Now;
                var carrierId = int.Parse(ObjHelper.ExecuteScalar("USP_InsertInsuranceCarrier", SqlParam));

                //save deductible info
                if (model.DeductibleInfo != null)
                {
                    if (model.DeductibleInfo.Family != null)
                    {
                        SaveCarrierDeductible(model.DeductibleInfo.Family, "Family", carrierId);
                    }
                    if (model.DeductibleInfo.Lifetime != null)
                    {
                        SaveCarrierDeductible(model.DeductibleInfo.Lifetime, "Lifetime", carrierId);
                    }
                    if (model.DeductibleInfo.Person != null)
                    {
                        SaveCarrierDeductible(model.DeductibleInfo.Person, "Person", carrierId);
                    }
                }

                if (model.MaxBenfits != null)
                {
                    SaveCarrierMaxBenefits(model.MaxBenfits, carrierId);
                }


                return carrierId;
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("clsPatientInsuranceData--USP_InsertPatientInsuranceStaging", ex.Message, ex.StackTrace);
                throw;
            }
        }

        private void SaveCarrierDeductible(BO.Models.Deductible model, string type, int carrierId)
        {

            try
            {
                SqlParameter[] SqlParam = new SqlParameter[5];

                SqlParam[0] = new SqlParameter("@CarrierId", SqlDbType.Int);
                SqlParam[0].Value = carrierId;
                SqlParam[1] = new SqlParameter("@Type", SqlDbType.VarChar);
                SqlParam[1].Value = type;
                SqlParam[2] = new SqlParameter("@Standard", SqlDbType.Int);
                SqlParam[2].Value = model.Standard;
                SqlParam[3] = new SqlParameter("@Preventive", SqlDbType.Int);
                SqlParam[3].Value = model.Preventive;
                SqlParam[4] = new SqlParameter("@Other", SqlDbType.Int);
                SqlParam[4].Value = model.Other;
                ObjHelper.ExecuteNonQuery("USP_InsertInsuranceCarrierDeductible", SqlParam);

            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("clsPatientInsuranceData--USP_InsertInsuranceCarrierDeductible", ex.Message, ex.StackTrace);
                throw;
            }
        }

        private void SaveCarrierMaxBenefits(MaxBenefits model, int carrierId)
        {

            try
            {
                SqlParameter[] SqlParam = new SqlParameter[4];

                SqlParam[0] = new SqlParameter("@CarrierId", SqlDbType.Int);
                SqlParam[0].Value = carrierId;
                SqlParam[1] = new SqlParameter("@Person", SqlDbType.Int);
                SqlParam[1].Value = model.Person;
                SqlParam[2] = new SqlParameter("@Family", SqlDbType.Int);
                SqlParam[2].Value = model.Family;
                SqlParam[3] = new SqlParameter("@Lifetime", SqlDbType.Int);
                SqlParam[3].Value = model.Lifetime;
                ObjHelper.ExecuteNonQuery("USP_InsertInsuranceCarrierMaxBenefits", SqlParam);

            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("clsPatientInsuranceData--USP_InsertInsuranceCarrierMaxBenefits", ex.Message, ex.StackTrace);
                throw;
            }
        }

        private void SaveCarrierAddresses(BO.Models.InsuranceCarrier model, int carrierId)
        {
            model.Addresses.ForEach(address =>
            {
                try
                {
                    SqlParameter[] SqlParam = new SqlParameter[6];

                    SqlParam[0] = new SqlParameter("@CarrierId", SqlDbType.Int);
                    SqlParam[0].Value = carrierId;
                    SqlParam[1] = new SqlParameter("@Street1", SqlDbType.VarChar);
                    SqlParam[1].Value = address.Street1;
                    SqlParam[2] = new SqlParameter("@City", SqlDbType.VarChar);
                    SqlParam[2].Value = address.City;
                    SqlParam[3] = new SqlParameter("@State", SqlDbType.VarChar);
                    SqlParam[3].Value = address.State;
                    SqlParam[4] = new SqlParameter("@Zipcode", SqlDbType.VarChar);
                    SqlParam[4].Value = address.Zipcode;
                    SqlParam[5] = new SqlParameter("@DentrixId", SqlDbType.Int);
                    SqlParam[5].Value = address.DentrixId;

                    ObjHelper.ExecuteNonQuery("USP_InsertInsuranceCarrierAddress", SqlParam);

                }
                catch (Exception ex)
                {
                    ObjCommon.InsertErrorLog("clsPatientInsuranceData--USP_InsertInsuranceCarrierAddress", ex.Message, ex.StackTrace);
                    throw;
                }
            });
        }

        private void SaveCarrierPhoneNumbers(BO.Models.InsuranceCarrier model, int carrierId)
        {
            model.PhoneNumbers.ForEach(phone =>
            {
                try
                {
                    SqlParameter[] SqlParam = new SqlParameter[3];

                    SqlParam[0] = new SqlParameter("@CarrierId", SqlDbType.Int);
                    SqlParam[0].Value = carrierId;
                    SqlParam[1] = new SqlParameter("@PhoneType", SqlDbType.VarChar);
                    SqlParam[1].Value = phone.PhoneType;
                    SqlParam[2] = new SqlParameter("@PhoneNumber", SqlDbType.VarChar);
                    SqlParam[2].Value = phone.PhoneNumber;


                    ObjHelper.ExecuteNonQuery("USP_InsertInsuranceCarrierPhoneNumber", SqlParam);

                }
                catch (Exception ex)
                {
                    ObjCommon.InsertErrorLog("clsPatientInsuranceData--USP_InsertInsuranceCarrierAddress", ex.Message, ex.StackTrace);
                    throw;
                }
            });
        }

        private void SaveCarrierEmails(BO.Models.InsuranceCarrier model, int carrierId)
        {
            model.Emails.ForEach(email =>
            {
                try
                {
                    SqlParameter[] SqlParam = new SqlParameter[2];

                    SqlParam[0] = new SqlParameter("@CarrierId", SqlDbType.Int);
                    SqlParam[0].Value = carrierId;
                    SqlParam[1] = new SqlParameter("@Email", SqlDbType.VarChar);
                    SqlParam[1].Value = email;

                    ObjHelper.ExecuteNonQuery("USP_InsertInsuranceCarrierEmail", SqlParam);

                }
                catch (Exception ex)
                {
                    ObjCommon.InsertErrorLog("clsPatientInsuranceData--USP_InsertInsuranceCarrierEmail", ex.Message, ex.StackTrace);
                    throw;
                }
            });
        }
        #endregion

        #region InsuredRecord

        private int SaveInsuredRecordInfo(BO.Models.InsuredRecord model)
        {
            try
            {
                SqlParameter[] SqlParam = new SqlParameter[11];

                SqlParam[0] = new SqlParameter("@AutoModifiedDateTime", SqlDbType.DateTime2);
                SqlParam[0].Value = model.automodifiedtimestamp;
                SqlParam[1] = new SqlParameter("@InsuredId", SqlDbType.Int);
                SqlParam[1].Value = model.InsuredId;
                SqlParam[2] = new SqlParameter("@CarrierId", SqlDbType.Int);
                SqlParam[2].Value = model.CarrierId;
                SqlParam[3] = new SqlParameter("@InsuredPartyId", SqlDbType.Int);
                SqlParam[3].Value = model.InsuredPartyId;
                SqlParam[4] = new SqlParameter("@SubscriberId", SqlDbType.Int);
                SqlParam[4].Value = model.SubscriberID;
                SqlParam[5] = new SqlParameter("@BenefitsUsed", SqlDbType.Decimal);
                SqlParam[5].Value = model.BenefitsUsed;
                SqlParam[6] = new SqlParameter("@DentrixId", SqlDbType.Int);
                SqlParam[6].Value = model.DentrixId;
                SqlParam[7] = new SqlParameter("@PracticeID", SqlDbType.Int);
                SqlParam[7].Value = model.PracticeID;
                SqlParam[8] = new SqlParameter("@ConnectorID", SqlDbType.VarChar);
                SqlParam[8].Value = model.ConnectorID;
                SqlParam[9] = new SqlParameter("@pmsConnectorID", SqlDbType.Int);
                SqlParam[9].Value = model.PMSConnectorId;
                SqlParam[10] = new SqlParameter("@DateCreated", SqlDbType.DateTime);
                SqlParam[10].Value = DateTime.Now;
                SqlParam[10] = new SqlParameter("@GroupName", SqlDbType.VarChar);
                SqlParam[10].Value = model.GroupName;
                SqlParam[10] = new SqlParameter("@GroupNumber", SqlDbType.VarChar);
                SqlParam[10].Value = model.GroupNumber;

                var recordId = int.Parse(ObjHelper.ExecuteScalar("USP_InsertInsuredRecord", SqlParam));


                //save deductible in use
                if (model.DeductibleUsed != null && model.DeductibleUsed.Family != null)
                {
                    SaveInsuredRecordDeductibleInUse(model.DeductibleUsed.Family, "Family", recordId);
                }
                if (model.DeductibleUsed != null && model.DeductibleUsed.Lifetime != null)
                {
                    SaveInsuredRecordDeductibleInUse(model.DeductibleUsed.Lifetime, "Lifetime", recordId);
                }
                if (model.DeductibleUsed != null && model.DeductibleUsed.Person != null)
                {
                    SaveInsuredRecordDeductibleInUse(model.DeductibleUsed.Person, "Person", recordId);
                }

                return recordId;
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("clsPatientInsuranceData--USP_InsertInsuredRecord", ex.Message, ex.StackTrace);
                throw;
            }
        }

        private void SaveInsuredRecordDeductibleInUse(BO.Models.Deductible model, string type, int insuredRecordId)
        {
            try
            {
                SqlParameter[] SqlParam = new SqlParameter[5];

                SqlParam[0] = new SqlParameter("@InsuredRecordId", SqlDbType.Int);
                SqlParam[0].Value = insuredRecordId;
                SqlParam[1] = new SqlParameter("@Type", SqlDbType.VarChar);
                SqlParam[1].Value = type;
                SqlParam[2] = new SqlParameter("@Standard", SqlDbType.Int);
                SqlParam[2].Value = model.Standard;
                SqlParam[3] = new SqlParameter("@Preventive", SqlDbType.Int);
                SqlParam[3].Value = model.Preventive;
                SqlParam[4] = new SqlParameter("@Other", SqlDbType.Int);
                SqlParam[4].Value = model.Other;
                ObjHelper.ExecuteNonQuery("USP_InsertInsuredRecordDeductibleInUse", SqlParam);

            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("clsPatientInsuranceData--USP_InsertInsuredRecordDeductibleInUse", ex.Message, ex.StackTrace);
                throw;
            }
        }

        private void SaveInsuredMember(BO.Models.InsuredRecord model, int insuredRecordId)
        {
            if (model.InsuredMembers != null && model.InsuredMembers.Count() > 0)
            {
                foreach (var member in model.InsuredMembers)
                {
                    try
                    {
                        //call sproc
                        SqlParameter[] SqlParam = new SqlParameter[17];

                        SqlParam[0] = new SqlParameter("@AutoModifiedDateTime", SqlDbType.DateTime2);
                        SqlParam[0].Value = member.automodifiedtimestamp;
                        SqlParam[1] = new SqlParameter("@InsuredRecordId", SqlDbType.Int);
                        SqlParam[1].Value = insuredRecordId;
                        SqlParam[2] = new SqlParameter("@PatientId", SqlDbType.Int);
                        SqlParam[2].Value = member.PatientId;
                        SqlParam[3] = new SqlParameter("@FirstName", SqlDbType.VarChar);
                        SqlParam[3].Value = member.FirstName;
                        SqlParam[4] = new SqlParameter("@LastName", SqlDbType.VarChar);
                        SqlParam[4].Value = member.LastName;
                        SqlParam[5] = new SqlParameter("@MiddleName", SqlDbType.VarChar);
                        SqlParam[5].Value = member.MiddleName;
                        SqlParam[6] = new SqlParameter("@Gender", SqlDbType.Int);
                        SqlParam[6].Value = member.Gender;
                        SqlParam[7] = new SqlParameter("@DOB", SqlDbType.DateTime2);
                        SqlParam[7].Value = member.DOB;
                        SqlParam[8] = new SqlParameter("@SSN", SqlDbType.VarChar);
                        SqlParam[8].Value = member.SSN;
                        SqlParam[9] = new SqlParameter("@InsuranceType", SqlDbType.Int);
                        SqlParam[9].Value = member.InsuranceType;
                        SqlParam[10] = new SqlParameter("@RelationToInsured", SqlDbType.Int);
                        SqlParam[10].Value = member.RelationToInsured;
                        SqlParam[11] = new SqlParameter("@PracticeID", SqlDbType.Int);
                        SqlParam[11].Value = member.PracticeID;
                        SqlParam[12] = new SqlParameter("@BirthDate", SqlDbType.DateTime2);
                        SqlParam[12].Value = member.BirthDate;
                        SqlParam[13] = new SqlParameter("@BenefitsUsed", SqlDbType.Decimal);
                        SqlParam[13].Value = member.BenefitsUsed;
                        SqlParam[14] = new SqlParameter("@InsurancePosition", SqlDbType.Int);
                        SqlParam[14].Value = member.InsurancePosition;

                        SqlParam[15] = new SqlParameter("@DateCreated", SqlDbType.DateTime);
                        SqlParam[15].Value = DateTime.Now;
                        SqlParam[16] = new SqlParameter("@pmsConnectorId", SqlDbType.Int);
                        SqlParam[16].Value = model.PMSConnectorId;

                        var memberId = int.Parse(ObjHelper.ExecuteScalar("USP_InsertInsuredMember", SqlParam));

                        //save deductible info
                        if (memberId != 0)
                        {
                            if (member.DeductibleInfo != null && member.DeductibleInfo.Family != null)
                            {
                                SaveMemberDeductibleInfo(member.DeductibleInfo.Family, "Family", memberId);
                            }
                            if (member.DeductibleInfo != null && member.DeductibleInfo.Lifetime != null)
                            {
                                SaveMemberDeductibleInfo(member.DeductibleInfo.Lifetime, "Lifetime", memberId);
                            }
                            if (member.DeductibleInfo != null && member.DeductibleInfo.Person != null)
                            {
                                SaveMemberDeductibleInfo(member.DeductibleInfo.Person, "Person", memberId);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        ObjCommon.InsertErrorLog("clsPatientInsuranceData--USP_InsertInsuredMember", ex.Message, ex.StackTrace);
                        throw;
                    }
                }
            }
            if (model.SubscriberDetail != null)
            {
                try
                {
                    //call sproc
                    SqlParameter[] SqlParam = new SqlParameter[16];

                    SqlParam[0] = new SqlParameter("@AutoModifiedDateTime", SqlDbType.DateTime2);
                    SqlParam[0].Value = model.SubscriberDetail.automodifiedtimestamp;
                    SqlParam[1] = new SqlParameter("@InsuredRecordId", SqlDbType.Int);
                    SqlParam[1].Value = insuredRecordId;
                    SqlParam[2] = new SqlParameter("@PatientId", SqlDbType.Int);
                    SqlParam[2].Value = model.SubscriberDetail.PatientId;
                    SqlParam[3] = new SqlParameter("@FirstName", SqlDbType.VarChar);
                    SqlParam[3].Value = model.SubscriberDetail.FirstName;
                    SqlParam[4] = new SqlParameter("@LastName", SqlDbType.VarChar);
                    SqlParam[4].Value = model.SubscriberDetail.LastName;
                    SqlParam[5] = new SqlParameter("@MiddleName", SqlDbType.VarChar);
                    SqlParam[5].Value = model.SubscriberDetail.MiddleName;
                    SqlParam[6] = new SqlParameter("@Gender", SqlDbType.Int);
                    SqlParam[6].Value = model.SubscriberDetail.Gender;
                    SqlParam[7] = new SqlParameter("@DOB", SqlDbType.DateTime2);
                    SqlParam[7].Value = model.SubscriberDetail.DOB;
                    SqlParam[8] = new SqlParameter("@SSN", SqlDbType.VarChar);
                    SqlParam[8].Value = model.SubscriberDetail.SSN;
                    SqlParam[9] = new SqlParameter("@InsuranceType", SqlDbType.Int);
                    SqlParam[9].Value = model.SubscriberDetail.InsuranceType;
                    SqlParam[10] = new SqlParameter("@RelationToInsured", SqlDbType.Int);
                    SqlParam[10].Value = model.SubscriberDetail.RelationToInsured;
                    SqlParam[11] = new SqlParameter("@PracticeID", SqlDbType.Int);
                    SqlParam[11].Value = model.SubscriberDetail.PracticeID;
                    SqlParam[12] = new SqlParameter("@BirthDate", SqlDbType.DateTime2);
                    SqlParam[12].Value = model.SubscriberDetail.BirthDate;
                    SqlParam[13] = new SqlParameter("@BenefitsUsed", SqlDbType.Decimal);
                    SqlParam[13].Value = model.SubscriberDetail.BenefitsUsed;
                    SqlParam[14] = new SqlParameter("@InsurancePosition", SqlDbType.Int);
                    SqlParam[14].Value = model.SubscriberDetail.InsurancePosition;

                    SqlParam[15] = new SqlParameter("@DateCreated", SqlDbType.DateTime);
                    SqlParam[15].Value = DateTime.Now;

                    var memberId = int.Parse(ObjHelper.ExecuteScalar("USP_InsertInsuredSubscriberDetail", SqlParam));


                    //save deductible info
                    if (memberId != 0)
                    {
                        if (model.SubscriberDetail.DeductibleInfo != null && model.SubscriberDetail.DeductibleInfo.Family != null)
                        {
                            SaveSubscriberDeductibleInfo(model.SubscriberDetail.DeductibleInfo.Family, "Family", memberId);
                        }
                        if (model.SubscriberDetail.DeductibleInfo != null && model.SubscriberDetail.DeductibleInfo.Lifetime != null)
                        {
                            SaveSubscriberDeductibleInfo(model.SubscriberDetail.DeductibleInfo.Lifetime, "Lifetime", memberId);
                        }
                        if (model.SubscriberDetail.DeductibleInfo != null && model.SubscriberDetail.DeductibleInfo.Person != null)
                        {
                            SaveSubscriberDeductibleInfo(model.SubscriberDetail.DeductibleInfo.Person, "Person", memberId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ObjCommon.InsertErrorLog("clsPatientInsuranceData--USP_InsertInsuredSubscriberDetail", ex.Message, ex.StackTrace);
                    throw;
                }
            }

        }

        private void SaveMemberDeductibleInfo(BO.Models.Deductible model, string type, int memberId)
        {
            try
            {
                SqlParameter[] SqlParam = new SqlParameter[5];

                SqlParam[0] = new SqlParameter("@MemberId", SqlDbType.Int);
                SqlParam[0].Value = memberId;
                SqlParam[1] = new SqlParameter("@Type", SqlDbType.VarChar);
                SqlParam[1].Value = type;
                SqlParam[2] = new SqlParameter("@Standard", SqlDbType.Int);
                SqlParam[2].Value = model.Standard;
                SqlParam[3] = new SqlParameter("@Preventive", SqlDbType.Int);
                SqlParam[3].Value = model.Preventive;
                SqlParam[4] = new SqlParameter("@Other", SqlDbType.Int);
                SqlParam[4].Value = model.Other;
                ObjHelper.ExecuteNonQuery("USP_InsertInsuredMemberDeductibleInfo", SqlParam);

            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("clsPatientInsuranceData--USP_InsertInsuredMemberDeductibleInfo", ex.Message, ex.StackTrace);
                throw;
            }
        }

        private void SaveSubscriberDeductibleInfo(BO.Models.Deductible model, string type, int memberId)
        {
            try
            {
                SqlParameter[] SqlParam = new SqlParameter[5];

                SqlParam[0] = new SqlParameter("@MemberId", SqlDbType.Int);
                SqlParam[0].Value = memberId;
                SqlParam[1] = new SqlParameter("@Type", SqlDbType.VarChar);
                SqlParam[1].Value = type;
                SqlParam[2] = new SqlParameter("@Standard", SqlDbType.Int);
                SqlParam[2].Value = model.Standard;
                SqlParam[3] = new SqlParameter("@Preventive", SqlDbType.Int);
                SqlParam[3].Value = model.Preventive;
                SqlParam[4] = new SqlParameter("@Other", SqlDbType.Int);
                SqlParam[4].Value = model.Other;
                ObjHelper.ExecuteNonQuery("USP_InsertInsuredSubscriberDetailDeductibleInfo", SqlParam);

            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("clsPatientInsuranceData--USP_InsertInsuredSubscriberDetailDeductibleInfo", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public List<Schema.InsuredRecordSchema> GetInsuranceRecordList(PatientInsuranceFilter filter)
        {
            var _db = new PatientContext(ConfigurationManager.ConnectionStrings["CONNECTIONSTRING"].ConnectionString);
            var query = _db.InsuredRecord.AsNoTracking().AsQueryable();

            if (filter.SubscriberId.Count() > 0)
            {
                query = query.Where(q => filter.SubscriberId.Contains(q.SubscriberId));
            }

            if (filter.CarrierId.Count() > 0)
            {
                query = query.Where(q => filter.CarrierId.Contains(q.CarrierId));
            }

            if (filter.StartDate != null)
            {
                query = query.Where(q => q.CreatedDate >= filter.StartDate);
            }

            if (filter.EndDate != null)
            {
                query = query.Where(q => q.CreatedDate <= filter.EndDate);
            }

            if (filter.ConnectorId.Count() > 0)
            {
                query = query.Where(q => filter.ConnectorId.Contains(q.ConnectorID));
            }
          
            if (filter.Page == null)
            {
                filter.Page = 0;
            }

            if (filter.PageSize == null || filter.PageSize == 0)
            {
                filter.Page = 1000;
            }


            return query.Skip((int)filter.Page * (int)filter.PageSize).Take((int)filter.PageSize).ToList();
        }
        #endregion

    }
}
