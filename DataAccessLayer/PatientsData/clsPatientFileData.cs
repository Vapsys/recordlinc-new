﻿using BO.Models;
using Box.V2.Config;
using Box.V2.JWTAuth;
using Box.V2.Models;
using DataAccessLayer.PatientsData.Schema;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;

namespace DataAccessLayer.PatientsData
{
    public class clsPatientFileData 
    {
        private readonly PatientContext _db;
        private readonly BoxSettings _settings;
        private readonly BoxConfig _boxConfig;
        private readonly BoxJWTAuth _boxJWT;
        //private clsPatientFileData _boxFileHandler;
        public clsPatientFileData(PatientContext db)
        {
            _db = db;
            _settings = new BoxSettings();
            _boxConfig = new BoxConfig(_settings.ClientId, _settings.ClientSecret, _settings.EnterpriseID, _settings.PrivateKey, _settings.Passphrase, _settings.PublicKeyID);
            _boxJWT = new BoxJWTAuth(_boxConfig);

        }

        public string[] _imageFormats = new string[] {"jpg", "png", "img", "ico", "gif", "svg", "bmp", "tif", "tiff", "ppm", "pgm", "pbm"};
        public string CreateBoxFolder()
        {
            var userToken = _boxJWT.UserToken(_settings.UserId);
            var client = _boxJWT.UserClient(userToken, _settings.UserId);
            var result = client.FoldersManager.CreateAsync(new BoxFolderRequest()
            {
                Name = Guid.NewGuid().ToString(),
                Parent = new BoxRequestEntity() { Id = _settings.FolderId }
            });
            return result.Result.Id;
        }

        public void CreateMatrix(FileModel model)
        {
            if (!string.IsNullOrEmpty(model.FolderId))
            {
                _db.PatientFile.Add(new PatientFile
                {
                    FolderId = model.FolderId,
                    RecordId = model.RecordId,
                    PatientId = model.PatientId,
                    FileId = model.FileId,
                    FileName = model.FileName,
                    CreatedDate = DateTime.Now
                });
                _db.SaveChanges();
            }
        }

        public string UploadFile(FileModel model)
        {
            try
            {
                //save to box
                var fileNameArray = model.FileName.Split('.');
                if (fileNameArray.Length == 2)
                {
                    var newName = fileNameArray[0] + "_" + RandomString(4) + "." + fileNameArray[1];
                    model.FileName = newName;
                }


                var userToken = _boxJWT.UserToken(_settings.UserId);
                var client = _boxJWT.UserClient(userToken, _settings.UserId);
                BoxFileRequest req = new BoxFileRequest()
                {
                    Name = model.FileName,
                    Parent = new BoxRequestEntity() { Id = model.FolderId }
                };

                var newFile = client.FilesManager.UploadAsync(req, model.FileStream, timeout: new TimeSpan(1, 0, 0)).Result;
                return newFile.Id;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }
        public string UploadFileForConversation(FileModel model)
        {
            try
            {
                //save to box
                var fileNameArray = model.FileName.Split('.');
                if (fileNameArray.Length == 2)
                {
                    var newName = fileNameArray[0] + "_" + RandomString(4) + "." + fileNameArray[1];
                    model.FileName = newName;
                }


                var userToken = _boxJWT.UserToken(_settings.UserId);
                var client = _boxJWT.UserClient(userToken, _settings.UserId);
                BoxFileRequest req = new BoxFileRequest()
                {
                    Name = model.FileName,
                    Parent = new BoxRequestEntity() { Id = model.FolderId }
                };

                var newFile = client.FilesManager.UploadAsync(req, model.FileStream, timeout: new TimeSpan(1, 0, 0)).Result;
                model.FileId = newFile.Id;
                //clsPatientFileData _boxFileHandler = new clsPatientFileData(new PatientContext(ConfigurationManager.ConnectionStrings["CONNECTIONSTRING"].ConnectionString));
                //_boxFileHandler.CreateMatrix(model);
                return newFile.Id;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }
        public FileListModel Files(int recordId, string folderId, int patientId)
        {
            var model = new FileListModel();
            model.Files = new List<FileModel>();

            var userToken = _boxJWT.UserToken(_settings.UserId);
            var client = _boxJWT.UserClient(userToken, _settings.UserId);
            List<PatientFile> matrices = null;
            //the conditional statements below gets folders from box based on which screen the widget is on. patient history or referral/conversation history
            //get all folders for a recordid if recordid is populated (there should only be one)
            if (recordId != 0)
            {
                matrices = _db.PatientFile.AsNoTracking().Where(q => q.RecordId == recordId).ToList();
            }

            //else get all folders for a patient if patient id is populated (there can be more than one)
            else if (patientId != 0)
            {
                matrices = _db.PatientFile.AsNoTracking().OrderByDescending(q => q.RecordId)
                    .Where(q => q.PatientId == patientId).ToList();
            }

            if (recordId + patientId != 0)
            {
                if (matrices != null)
                {
                    matrices.ForEach(matrix =>
                    {
                        try
                        {
                            model.FolderId = matrix.FolderId;
                            var result = client.FoldersManager.GetFolderItemsAsync(matrix.FolderId, limit: 9999).Result;
                            result.Entries.ForEach(q =>
                            {
                                BoxFile file =  client.FilesManager.GetInformationAsync(id: q.Id).Result;
                                //get file extension
                                var extension = "txt";
                                string base64 = string.Empty;
                                var nameArray = q.Name.Split('.');
                                try
                                {
                                    extension = nameArray[1];
                                }
                                catch { }
                                //generate thumbnail if less than 10mb
                                if(_imageFormats.Contains(extension))
                                {
                                    var stream = client.FilesManager.DownloadStreamAsync(id: q.Id).Result;
                                    var thumbBytes = MakeThumbnail(StreamToArray(stream), 600, 600);
                                     base64 = Convert.ToBase64String(thumbBytes);
                                }


                                model.Files.Add(new FileModel
                                {
                                    RecordId = recordId,
                                    FolderId = matrix.FolderId,
                                    FileName = q.Name,
                                    FileId = q.Id,
                                    FileExtension = extension,
                                    Image = base64,
                                    PatientId = (matrix.PatientId == null) ? 0 : (int)matrix.PatientId
                                });
                            });
                        }
                        catch
                        {
                            
                        }
                    });

                    if (string.IsNullOrEmpty(model.FolderId))
                    {
                        model.FolderId = CreateBoxFolder();
                        var newMatrixRecord = new FileModel();
                        newMatrixRecord.FolderId = model.FolderId;
                        newMatrixRecord.RecordId = recordId;
                        newMatrixRecord.PatientId = patientId;
                        CreateMatrix(newMatrixRecord);
                    }

                }
                else
                {
                    if (string.IsNullOrEmpty(folderId))
                    {
                        model.FolderId = CreateBoxFolder();
                        var newMatrixRecord = new FileModel();
                        newMatrixRecord.FolderId = model.FolderId;
                        newMatrixRecord.RecordId = recordId;
                        newMatrixRecord.PatientId = patientId;
                        CreateMatrix(newMatrixRecord);
                    }
                    else
                    {
                        model.FolderId = folderId;
                        var newMatrixRecord = new FileModel();
                        newMatrixRecord.FolderId = model.FolderId;
                        newMatrixRecord.RecordId = recordId;
                        newMatrixRecord.PatientId = patientId;
                        CreateMatrix(newMatrixRecord);
                        var result = client.FoldersManager.GetFolderItemsAsync(folderId, limit: 9999).Result;
                        result.Entries.ForEach(q =>
                        {
                            BoxFile file = client.FilesManager.GetInformationAsync(id: q.Id).Result;
                            //get file extension
                            var extension = "txt";
                            string base64 = string.Empty;
                            var nameArray = q.Name.Split('.');
                            try
                            {
                                extension = nameArray[1];
                            }
                            catch { }
                            //generate thumbnail if less than 10mb
                            if (_imageFormats.Contains(extension))
                            {
                                var stream = client.FilesManager.DownloadStreamAsync(id: q.Id).Result;
                                var thumbBytes = MakeThumbnail(StreamToArray(stream), 600, 600);
                                base64 = Convert.ToBase64String(thumbBytes);
                            }

                            model.Files.Add(new FileModel
                            {
                                RecordId = recordId,
                                FolderId = folderId,
                                FileName = q.Name,
                                FileExtension = extension,
                                Image = base64,
                                FileId = q.Id
                            });
                        });
                    }

                }
            }
            else
            {
                if (string.IsNullOrEmpty(folderId))
                {
                    model.FolderId = CreateBoxFolder();
                }
                else
                {
                    model.FolderId = folderId;
                    var result = client.FoldersManager.GetFolderItemsAsync(folderId, limit: 9999).Result;
                    result.Entries.ForEach(q =>
                    {
                        BoxFile file = client.FilesManager.GetInformationAsync(id: q.Id).Result;
                        //get file extension
                        var extension = "txt";
                        string base64 = string.Empty;
                        var nameArray = q.Name.Split('.');
                        try
                        {
                            extension = nameArray[1];
                        }
                        catch { }
                        //generate thumbnail if less than 10mb
                        if (_imageFormats.Contains(extension))
                        {
                            var stream = client.FilesManager.DownloadStreamAsync(id: q.Id).Result;
                            var thumbBytes = MakeThumbnail(StreamToArray(stream), 600, 600);
                            base64 = Convert.ToBase64String(thumbBytes);
                        }

                        model.Files.Add(new FileModel
                        {
                            RecordId = recordId,
                            FolderId = folderId,
                            FileName = q.Name,
                            FileExtension = extension,
                            Image = base64,
                            FileId = q.Id
                        });
                    });
                }
            }

           
            return model;


            //TODO: test dev and test thumbnails
            //populate file stream if less than 10mb for a thumbnail
            //files.ForEach(file =>
            //{
            //    if(file.FileSize <= 10)
            //    {
            //        var userToken = _boxJWT.UserToken(_settings.UserId);
            //        var client = _boxJWT.UserClient(userToken, _settings.UserId);
            //        BoxFile bFile = client.FilesManager.GetInformationAsync(id: file.FileId).Result;
            //        file.FileStream = client.FilesManager.DownloadStreamAsync(id: file.FileId).Result;                
            //    }
            //});

        }

        public void Delete(string fileId)
        {  //delete from box
            var userToken = _boxJWT.UserToken(_settings.UserId);
            var client = _boxJWT.UserClient(userToken, _settings.UserId);
            var result = client.FilesManager.DeleteAsync(id: fileId).Result;
        }

        public FileModel Download(string fileId)
        {
            var userToken = _boxJWT.UserToken(_settings.UserId);
            var client = _boxJWT.UserClient(userToken, _settings.UserId);
            BoxFile file = client.FilesManager.GetInformationAsync(id: fileId).Result;
            Stream fileContents = client.FilesManager.DownloadStreamAsync(id: fileId).Result;




            return new FileModel
            {
                FileId = fileId,
                FileName = file.Name,
                FileStream = fileContents
            };


        }

        public byte[] MakeThumbnail(byte[] myImage, int thumbWidth, int thumbHeight)
        {
            using (MemoryStream ms = new MemoryStream(myImage))
            {
                int imgWidth = 0;
                int imgHeight = 0;
                Image img = Image.FromStream(ms);
                if (img.Width >= img.Height)
                {
                    if (img.Width > 15)
                    {
                        imgWidth = 15;

                        if (img.Height > 15)
                        {
                            imgHeight = 15 * img.Height / img.Width;
                        }
                        else
                        {
                            imgHeight = img.Height;
                        }
                    }
                    else
                    {
                        imgWidth = img.Width;
                        imgHeight = img.Height;
                    }
                }
                else
                {
                    if (img.Height > 15)
                    {
                        imgHeight = 15;
                        if (img.Width > 15)
                        {
                            imgWidth = 15 * img.Width / img.Height;
                        }
                        else
                        {
                            imgWidth = img.Width;
                        }
                    }
                    else
                    {
                        imgHeight = img.Height;
                        imgWidth = img.Width;
                    }

                }
                using (Image thumbnail = Image.FromStream(new MemoryStream(myImage)).GetThumbnailImage(imgWidth, imgHeight, null, new IntPtr()))
                {
                    thumbnail.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                    return ms.ToArray();
                }
            }
        }

        public static byte[] StreamToArray(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
