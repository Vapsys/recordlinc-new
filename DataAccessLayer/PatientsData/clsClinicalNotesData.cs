﻿using BO.Models;
using BO.ViewModel;
using DataAccessLayer.Common;
using DataAccessLayer.PatientsData.Schema;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.PatientsData
{
    public class clsClinicalNotesData
    {
        public static clsHelper ObjHelper = new clsHelper();
        public static clsCommon ObjCommon = new clsCommon();
        private SqlConnection objCon = new SqlConnection();

        public int Save(BO.Models.ClinicalNote model)
        {
            try
            {
                SqlParameter[] SqlParam = new SqlParameter[13];

                SqlParam[0] = new SqlParameter("@Identifier", SqlDbType.VarChar);
                SqlParam[0].Value = model.Identifier;
                SqlParam[1] = new SqlParameter("@NoteId", SqlDbType.Int);
                SqlParam[1].Value = model.NoteId;
                SqlParam[2] = new SqlParameter("@NoteType", SqlDbType.Int);
                SqlParam[2].Value = model.NoteType;
                SqlParam[3] = new SqlParameter("@ProviderId", SqlDbType.VarChar);
                SqlParam[3].Value = model.ProviderId;
                SqlParam[4] = new SqlParameter("@PatientId", SqlDbType.Int);
                SqlParam[4].Value = model.PatientId;
                SqlParam[5] = new SqlParameter("@CreatedDate", SqlDbType.DateTime2);
                SqlParam[5].Value = model.CreatedDate;
                SqlParam[6] = new SqlParameter("@NoteDate", SqlDbType.DateTime2);
                SqlParam[6].Value = model.NoteDate;
                SqlParam[7] = new SqlParameter("@LockedDate", SqlDbType.DateTime2);
                SqlParam[7].Value = model.LockedDate;
                SqlParam[8] = new SqlParameter("@Text", SqlDbType.VarChar);
                SqlParam[8].Value = model.Text;
                SqlParam[9] = new SqlParameter("@automodifiedtimestamp", SqlDbType.DateTime2);
                SqlParam[9].Value = model.automodifiedtimestamp;
                SqlParam[10] = new SqlParameter("@DentrixId", SqlDbType.Int);
                SqlParam[10].Value = model.DentrixId;
                SqlParam[11] = new SqlParameter("@ConnectorID", SqlDbType.VarChar);
                SqlParam[11].Value = model.ConnectorID;
                SqlParam[12] = new SqlParameter("@dentrixConnectorID", SqlDbType.VarChar);
                SqlParam[12].Value = model.dentrixConnectorID;

                var noteId = int.Parse(ObjHelper.ExecuteScalar("USP_InsertClinicalNotes", SqlParam));
                return noteId;
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("clsClinicalNotesData--USP_InsertClinicalNotes", ex.Message, ex.StackTrace);
                throw;
            }

        }

        public bool Delete(BO.Models.ClinicalNote model)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@Identifier",Value=model.Identifier},
                    new SqlParameter{ParameterName="@dentrixConnectorID",Value=model.dentrixConnectorID},
                    new SqlParameter{ParameterName="@DentrixId",Value=model.DentrixId}
                };

                new clsHelper().ExecuteNonQuery("Usp_DeleteClinicalNote", sqlpara);
                return true;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsCLinicalNoteData--Delete", Ex.Message, Ex.StackTrace);
                return false;
            }
        }

        public List<BO.Models.ClinicalNote> List(ClinicalNoteFilter filter)
        {
            var _db = new PatientContext(ConfigurationManager.ConnectionStrings["CONNECTIONSTRING"].ConnectionString);
            var query = _db.ClinicalNote.AsNoTracking().AsQueryable();

            if (filter.PatientIds != null && filter.PatientIds.Count() > 0)
            {
                query = query.Where(q => filter.PatientIds.Contains(q.PatientId));
            }

            if (filter.StartDate != null)
            {
                query = query.Where(q => q.NoteDate >= filter.StartDate);
            }

            if (filter.EndDate != null)
            {
                query = query.Where(q => q.NoteDate <= filter.EndDate);
            }

            //check that at least one filter exists
            if (filter != null && (filter.StartDate != null || filter.EndDate != null || (filter.PatientIds != null && filter.PatientIds.Count() > 0)))
            {
                return query.Select(q => new BO.Models.ClinicalNote
                {
                    automodifiedtimestamp = q.automodifiedtimestamp,
                    ConnectorID = q.ConnectorID,
                    CreatedDate = q.CreatedDate,
                    dentrixConnectorID = q.dentrixConnectorID,
                    DentrixId = q.DentrixId,
                    Identifier = q.Identifier,
                    IsDeleted = (q.IsDeleted == null) ? false : (bool)q.IsDeleted,
                    LockedDate = q.LockedDate,
                    NoteDate = q.NoteDate,
                    NoteId = q.NoteId,
                    NoteType = q.NoteType,
                    PatientId = q.PatientId,
                    ProviderId = q.ProviderId,
                    Text = q.Text
                }).ToList();
            }
            else
            {
                return new List<BO.Models.ClinicalNote>();
            }
        }
    }
}