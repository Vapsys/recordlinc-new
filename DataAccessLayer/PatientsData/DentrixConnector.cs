﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.PatientsData
{
    public class DentrixConnector
    {
        [Key]
        public int DentrixConnectorId { get; set; }
        public int AccountId { get; set; }
        public  string AccountKey { get; set; }
        public int? LocationId { get; set; }
        public bool IsDeleted { get; set; }
        public int IntegrationId { get; set; }
        public DateTime? DeletedDate { get; set; }
        public bool IsGroupBy { get; set; }
    }
}
