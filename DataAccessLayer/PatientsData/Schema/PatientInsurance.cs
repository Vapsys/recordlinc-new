﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.PatientsData.Schema
{
    [Table("InsuranceCarrier")]
    public class InsuranceCarrierSchema
    {
        [Key]
        public int InsuranceId { get; set; }
        public int? CarrierId { get; set; }
        public int? InsuranceType { get; set; }
        public string Name { get; set; }
        public string GroupName { get; set; }
        public string GroupNumber { get; set; }
        public int? CoverageTableId { get; set; }
        public int? RenewalMonth { get; set; }
        public int? FeeScheduleId { get; set; }
        public string PayorId { get; set; }
        public InsuranceCarrierMaxBenefits MaxBenefits { get; set; }
        public decimal Person { get; set; }
        public decimal Family { get; set; }
        public InsuranceCarrierDeductible InsuranceCarrierDeductible { get; set; }
        public List<InsuranceCarrierAddress> Addresses { get; set; }
        public List<InsuranceCarrierPhoneNumber> PhoneNumbers { get; set; }
        public List<string> Emails { get; set; }
        public int? DentrixId { get; set; }
        public int? PracticeId { get; set; }
        public string ConnectorID { get; set; }
        public string DentrixConnectorID { get; set; }
        public DateTime DateCreated { get; set; }
    }

    public class InsuranceCarrierAddress
    {
        [Key]
        public int Id { get; set; }
        public int InsuranceCarrierId { get; set; }
        public int? DentrixId { get; set; }
        public string Street1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
    }

    public class InsuranceCarrierDeductible
    {
        [Key]
        public int Id { get; set; }
        public int InsuranceCarrierId { get; set; }
        public string Type { get; set; }
        public int? Standard { get; set; }
        public int? Preventive { get; set; }
        public int? Other { get; set; }
    }

    public class InsuranceCarrierEmail
    {
        [Key]
        public int Id { get; set; }
        public int InsuranceCarrierId { get; set; }
        public string Email { get; set; }
    }

    public class InsuranceCarrierMaxBenefits
    {
        [Key]
        public int Id { get; set; }
        public int InsuranceCarrierId { get; set; }
        public int? Person { get; set; }
        public int? Family { get; set; }
        public int? Lifetime { get; set; }
    }

    public class InsuranceCarrierPhoneNumber
    {
        [Key]
        public int Id { get; set; }
        public int InsuranceCarrierId { get; set; }
        public string Type { get; set; }
        public int? PhoneNumber { get; set; }
    }
    [Table("InsuredMember")]
    public class InsuredMemberSchema
    {
        [Key]
        public int Id { get; set; }
        public int InsuredRecordId { get; set; }
        public DateTime? AutoModifiedDateTime { get; set; }
        public int? PatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public int? Gender { get; set; }
        public DateTime? DOB { get; set; }
        public DateTime? BirthDate { get; set; }
        public string SSN { get; set; }
        public decimal? BenefitsUsed { get; set; }
        public int? InsuranceType { get; set; }
        public int? InsurancePosition { get; set; }
        public int? RelationToInsured { get; set; }
        public int? PracticeID { get; set; }
        public InsuredMemberDeductible InsuredMemberDeductible { get; set; }
        public DateTime DateCreated { get; set; }
    }

    public class InsuredMemberDeductible
    {
        [Key]
        public int Id { get; set; }
        public int InsuredMemberId { get; set; }
        public string Type { get; set; }
        public int? Standard { get; set; }
        public int? Preventive { get; set; }
        public int? Other { get; set; }
    }
    [Table("InsuredRecord")]
    public class InsuredRecordSchema
    {
        [Key]
        public int InsuredRecordId { get; set; }
        public DateTime? AutoModifiedDateTime { get; set; }
        public int? InsuredId { get; set; }
        public int? CarrierId { get; set; }
        public int? InsuredPartyId { get; set; }
        public int? SubscriberId { get; set; }
        public string GroupName { get; set; }
        public string GroupNumber { get; set; }
        public decimal? BenefitsUsed { get; set; }
        public int? DentrixId { get; set; }
        public int? PracticeId { get; set; }
        public string ConnectorID { get; set; }
        public string DentrixConnectorID { get; set; }
        public DateTime CreatedDate { get; set; }
        [ForeignKey("CarrierId")]
        public InsuranceCarrierSchema InsuranceCarrier { get; set; }
        [ForeignKey("InsuredRecordId")]
        public InsuredRecordDeductibleInUse InsuredRecordDeductibleInUse { get; set; }
        [ForeignKey("InsuredRecordId")]
        public InsuredMemberSchema SubscriberDetail { get; set; }
        [ForeignKey("InsuredRecordId")]
        public List<InsuredMemberSchema> InsuredMembers { get; set; }
    }

    public class InsuredRecordDeductibleInUse
    {
        [Key]
        public int Id { get; set; }
        public int InsuranceCarrierId { get; set; }
        public string Type { get; set; }
        public int? Standard { get; set; }
        public int? Preventive { get; set; }
        public int? Other { get; set; }
    }
    public class InsuredSubscriberDetail
    {
        [Key]
        public int Id { get; set; }
        public int InsuredRecordId { get; set; }
        public DateTime? AutoModifiedDateTime { get; set; }
        public int? PatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public int? Gender { get; set; }
        public DateTime? DOB { get; set; }
        public DateTime? BirthDate { get; set; }
        public string SSN { get; set; }
        public decimal? BenefitsUsed { get; set; }
        public int? InsuranceType { get; set; }
        public int? InsurancePosition { get; set; }
        public int? RelationToInsured { get; set; }
        public int? PracticeID { get; set; }
        public InsuredSubscriberDetailDeductible InsuredSubscriberDetailDeductible { get; set; }
        public DateTime DateCreated { get; set; }
    }
    public class InsuredSubscriberDetailDeductible
    {
        [Key]
        public int Id { get; set; }
        public int InsuredMemberId { get; set; }
        public string Type { get; set; }
        public int? Standard { get; set; }
        public int? Preventive { get; set; }
        public int? Other { get; set; }
    }

    public class DeductibleInfo
    {
        public Deductible Person { get; set; }
        public Deductible Family { get; set; }
        public Deductible Lifetime { get; set; }
    }

    public class Deductible
    {
        public int? Standard { get; set; }
        public int? Preventive { get; set; }
        public int? Other { get; set; }
    }
    
    
}
