﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.PatientsData.Schema
{

    public class PatientFile
    {       
        public int RecordId { get; set; }       
        public string FolderId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? PatientId { get; set; }
        public string FileId { get; set; }
        public string FileName { get; set; }
    }
}
