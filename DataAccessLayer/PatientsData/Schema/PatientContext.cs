﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.PatientsData.Schema
{
    public class PatientContext : DbContext
    {
        //table for BOX/large uploads
        public DbSet<PatientFile> PatientFile { get; set; }
        //clinical notes table
        public DbSet<ClinicalNote> ClinicalNote { get; set; }

        //insurance carrier tables
        public DbSet<InsuranceCarrierSchema> InsuranceCarrier { get; set; }
        public DbSet<InsuranceCarrierAddress> InsuranceCarrierAddress { get; set; }
        public DbSet<InsuranceCarrierDeductible> InsuranceCarrierDeductible { get; set; }
        public DbSet<InsuranceCarrierEmail> InsuranceCarrierEmail { get; set; }
        public DbSet<InsuranceCarrierMaxBenefits> InsuranceCarrierMaxBenefits { get; set; }
        public DbSet<InsuranceCarrierPhoneNumber> InsuranceCarrierPhoneNumber { get; set; }
        public DbSet<InsuredMemberSchema> InsuredMember { get; set; }
        public DbSet<InsuredMemberDeductible> InsuredMemberDeductible { get; set; }
        public DbSet<InsuredRecordSchema> InsuredRecord { get; set; }
        public DbSet<InsuredRecordDeductibleInUse> InsuredRecordDeductibleInUse { get; set; }
        public DbSet<InsuredSubscriberDetail> InsuredSubscriberDetail { get; set; }
        public DbSet<InsuredSubscriberDetailDeductible> InsuredSubscriberDetailDeductible { get; set; }
        public DbSet<DentrixConnector> DentrixConnector { get; set; }
        static PatientContext()
        {
            Database.SetInitializer<PatientContext>(null);
        }

        public PatientContext(string connectionString)
           : base(connectionString)
        {
            this.Database.CommandTimeout = 600;
            this.Configuration.ProxyCreationEnabled = false;
            this.Configuration.LazyLoadingEnabled = false;
            this.Configuration.AutoDetectChangesEnabled = true;
            this.Configuration.ValidateOnSaveEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.PluralizingTableNameConvention>();
            modelBuilder.Entity<PatientFile>().HasKey(p => new { p.FolderId });
        }
    }
}
