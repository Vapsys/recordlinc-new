using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataAccessLayer.Common;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Web;
using System.IO;
using System.Web.UI.WebControls;
using System.Drawing;
using BO.ViewModel;
using BO.Models;
using DataAccessLayer.ColleaguesData;
using System.Data.SqlTypes;
using BO.Enums;

namespace DataAccessLayer.PatientsData
{
    public class clsPatientsData
    {
        public static clsHelper ObjHelper = new clsHelper();
        public static clsCommon ObjCommon = new clsCommon();
        private SqlConnection objCon = new SqlConnection();
        private clsPatientFileData _boxFileHandler;
        public string BoxFolderId;
        public static string IntegrationConnectionString = ConfigurationManager.ConnectionStrings["INTEGRATIONCONNECTION"].ConnectionString;



        #region Method for get details of Social Media for patient
        public DataTable GetSocialMediaDetailsForPatient(int UserId, int StartIndex, int EndSize, string SearchText, int SortColumn, int SortDirection)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[6];
                strParameter[0] = new SqlParameter("@userid", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@index", SqlDbType.Int);
                strParameter[1].Value = StartIndex;
                strParameter[2] = new SqlParameter("@size", SqlDbType.Int);
                strParameter[2].Value = EndSize;
                strParameter[3] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
                strParameter[3].Value = SearchText;

                strParameter[4] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[4].Value = SortColumn;
                strParameter[5] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[5].Value = SortDirection;


                dt = ObjHelper.DataTable("USP_GetPatientsSocialMediaDetails", strParameter);


                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Method for get details of Patient Referral History
        public DataTable GetPatientReferralHistoryDetails(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirection, string SearchText)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[6];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[1].Value = PageIndex;
                strParameter[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[2].Value = PageSize;
                strParameter[3] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[3].Value = SortColumn;
                strParameter[4] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[4].Value = SortDirection;
                strParameter[5] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
                strParameter[5].Value = SearchText;

                dt = ObjHelper.DataTable("USP_GetPatientReferralHistoryDetails", strParameter);


                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Method For Get Patient details of doctor
        #region Get 5 Rendom Patient For Doctor For BirdEye API 
        public DataTable GetPatientDetialsForDoctor(int DoctorId)
        {
            SqlParameter[] StrParam = new SqlParameter[] { new SqlParameter("@DoctorId", DoctorId) };

            return ObjHelper.DataTable("GetRendomFivePatientsOFDoctor", StrParam);
        }
        #endregion

        public DataTable GetPatientDetailsOfDoctor(int UserId, int PageIndex, int PageSize, string SearchText, int SortColumn, int SortDirection, string NewSearchText, int FilterBy, int Location = 0)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[9];
                strParameter[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[1].Value = PageIndex;
                strParameter[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[2].Value = PageSize;
                strParameter[3] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
                strParameter[3].Value = SearchText;

                strParameter[4] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[4].Value = SortColumn;

                strParameter[5] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[5].Value = SortDirection;
                strParameter[6] = new SqlParameter("@NewSearchText", SqlDbType.NVarChar);
                strParameter[6].Value = NewSearchText;
                strParameter[7] = new SqlParameter("@FilterBy", SqlDbType.Int);
                strParameter[7].Value = FilterBy;
                strParameter[8] = new SqlParameter("@LocationId", SqlDbType.NVarChar);
                strParameter[8].Value = Convert.ToString(Location);

                dt = ObjHelper.DataTable("USP_GetPatientDetailsOfDoctor", strParameter);


                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        public DataTable NewGetPatientDetailsOfDoctor(BO.Models.PatientFilter FilterObj)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[10];
                strParameter[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                strParameter[0].Value = FilterObj.UserId;
                strParameter[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[1].Value = FilterObj.PageIndex;
                strParameter[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[2].Value = FilterObj.PageSize;
                strParameter[3] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
                strParameter[3].Value = FilterObj.SearchText;

                strParameter[4] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[4].Value = FilterObj.SortColumn;

                strParameter[5] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[5].Value = FilterObj.SortDirection;
                strParameter[6] = new SqlParameter("@NewSearchText", SqlDbType.NVarChar);
                strParameter[6].Value = FilterObj.NewSearchtext;
                strParameter[7] = new SqlParameter("@FilterBy", SqlDbType.Int);
                strParameter[7].Value = FilterObj.FilterBy;
                strParameter[8] = new SqlParameter("@LocationId", SqlDbType.NVarChar);
                strParameter[8].Value = Convert.ToString(FilterObj.LocationBy);
                strParameter[9] = new SqlParameter("@ExcludeFamilyMembers", SqlDbType.Bit);
                strParameter[9].Value = Convert.ToBoolean(FilterObj.ExcludeFamilyMembers);

                dt = ObjHelper.DataTable("USP_GetPatientDetailsOfDoctor", strParameter);


                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }

      

        public bool PatientHasInsuranceData(int intPatientId)
        {

            string PatientCount = string.Empty;
            bool ItPatientHasInsuranceData = false;
            try
            {
                SqlParameter[] sqlparam = new SqlParameter[]
            {
                new SqlParameter{ParameterName="@PatientId",Value=intPatientId}
            };

                PatientCount = ObjHelper.ExecuteScalar("PatientHasInsuranceData", sqlparam);

                if (!string.IsNullOrWhiteSpace(PatientCount) && PatientCount != "0")
                {
                    ItPatientHasInsuranceData = true;
                }
                return ItPatientHasInsuranceData;
            }
            catch (Exception EX)
            {
                ObjCommon.InsertErrorLog("", EX.Message, EX.StackTrace);
                throw;
            }

        }

        public DataTable GetPatientCountByDoctorId(int DoctorId, string SearchText = null, string NewSearchText = null, int FilterBy = 0, int Location = 0)
        {
            try
            {
                DataTable CountofPatient = new DataTable();
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[5];
                strParameter[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                strParameter[0].Value = DoctorId;
                strParameter[1] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
                strParameter[1].Value = SearchText;
                strParameter[2] = new SqlParameter("@NewSearchText", SqlDbType.NVarChar);
                strParameter[2].Value = NewSearchText;
                strParameter[3] = new SqlParameter("@FilterBy", SqlDbType.Int);
                strParameter[3].Value = FilterBy;
                strParameter[4] = new SqlParameter("@LocationId", SqlDbType.NVarChar);
                strParameter[4].Value = Convert.ToString(Location);

                CountofPatient = ObjHelper.DataTable("USP_CountOfPatientbyDoctorId", strParameter);
                return CountofPatient;
            }
            catch (Exception EX)
            {
                ObjCommon.InsertErrorLog("", EX.Message, EX.StackTrace);
                throw;
            }
        }

        //New patient Count
        public DataTable NewGetPatientCountByDoctorId(int DoctorId, string SearchText = null, string NewSearchText = null, int FilterBy = 0, int Location = 0)
        {
            try
            {
                DataTable CountofPatient = new DataTable();
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[5];
                strParameter[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                strParameter[0].Value = DoctorId;
                strParameter[1] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
                strParameter[1].Value = SearchText;
                strParameter[2] = new SqlParameter("@NewSearchText", SqlDbType.NVarChar);
                strParameter[2].Value = NewSearchText;
                strParameter[3] = new SqlParameter("@FilterBy", SqlDbType.Int);
                strParameter[3].Value = FilterBy;
                strParameter[4] = new SqlParameter("@LocationId", SqlDbType.NVarChar);
                strParameter[4].Value = Convert.ToString(Location);

                CountofPatient = ObjHelper.DataTable("USP_CountOfPatientbyDoctorId", strParameter);
                return CountofPatient;
            }
            catch (Exception EX)
            {
                ObjCommon.InsertErrorLog("", EX.Message, EX.StackTrace);
                throw;
            }
        }

        #region Method For Get Details of Patient By PatientId
        public DataSet GetPateintDetailsByPatientId(int PatientId, int? DoctorId = null)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = PatientId;
                strParameter[1] = new SqlParameter("@Doctorid", SqlDbType.Int);
                strParameter[1].Value = DoctorId;
                ds = ObjHelper.GetDatasetData("USP_GetPateintDetailsByPatientId", strParameter);

                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Method For Check Pateint Unique EmailId
        public bool CheckPatientEmail(string PatientEmail)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@PatientEmail", SqlDbType.NVarChar);
                strParameter[0].Value = PatientEmail;

                dt = ObjHelper.DataTable("USP_CheckPatientEmail", strParameter);
                if (dt != null && dt.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion

        #region Method for get patient password based on Emailid
        public DataTable GetPatientPassword(string PatientEmail)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@PatientEmail", SqlDbType.VarChar);
                strParameter[0].Value = PatientEmail;

                dtTable = ObjHelper.DataTable("Dental_GetPatientPassword", strParameter);
                return dtTable;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable Dental_GetPatientPasswordTemp(string PatientEmail)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@PatientEmail", SqlDbType.VarChar);
                strParameter[0].Value = PatientEmail;

                dtTable = ObjHelper.DataTable("Dental_GetPatientPasswordTemp", strParameter);
                return dtTable;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Method For Check Unique AssignedPateintId
        public DataTable CheckAssignedPatientId(string AssignedPatientId, int OwnerId, int PatientId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@AssignedPatientId", SqlDbType.NVarChar);
                if (string.IsNullOrWhiteSpace(AssignedPatientId))
                    strParameter[0].Value = DBNull.Value;
                else
                    strParameter[0].Value = AssignedPatientId;
                strParameter[1] = new SqlParameter("@OwnerId", SqlDbType.Int);
                strParameter[1].Value = OwnerId;
                strParameter[2] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[2].Value = PatientId;


                dt = ObjHelper.DataTable("USP_CheckAssignedPatientId", strParameter);



                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public DataTable GetPatientMessageofDoctor(int MessageId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[1];
                DataTable DT = new DataTable();
                sqlpara[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                sqlpara[0].Value = MessageId;

                DT = ObjHelper.DataTable("Usp_GetPatientMessageForDoctorbyMessageId", sqlpara);
                return DT;
            }
            catch (Exception)
            {

                throw;
            }
        }

        #region Method Reagarding patient Insert and Update
        public int PatientInsertAndUpdate(int PatientId, string AssignedPatientId, string FirstName,
                                           string MiddelName, string LastName, DateTime? DateOfBirth,
                                           int Gender, string ProfileImage, string Phone, string Email,
                                           string Address, string City, string State, string Zip, string Country,
                                           int OwnerId, int PatientStatus, string Address2, string Password, string Notes, int? ReferredBy, string LocationName, string FirstExamDate = null)
        {
            bool UpdateStatus = false;
            try
            {
                string strDateOfBirth = DateOfBirth.HasValue ? Convert.ToDateTime(DateOfBirth).ToString("MM/dd/yyyy") : null;
                SqlParameter[] strParameter = new SqlParameter[24];

                strParameter[0] = new SqlParameter("@pPatientId", SqlDbType.Int);
                strParameter[0].Value = PatientId;
                strParameter[1] = new SqlParameter("@pAssignedPatientId", SqlDbType.NVarChar);
                strParameter[1].Value = AssignedPatientId;
                strParameter[2] = new SqlParameter("@pFirstName", SqlDbType.NVarChar);
                strParameter[2].Value = FirstName;
                strParameter[3] = new SqlParameter("@pMiddelName", SqlDbType.NVarChar);
                strParameter[3].Value = MiddelName;
                strParameter[4] = new SqlParameter("@pLastName", SqlDbType.NVarChar);
                strParameter[4].Value = LastName;
                strParameter[5] = new SqlParameter("@pDateOfBirth", SqlDbType.NVarChar);
                strParameter[5].Value = strDateOfBirth;
                strParameter[6] = new SqlParameter("@pGender", SqlDbType.Int);
                strParameter[6].Value = Gender;
                strParameter[7] = new SqlParameter("@pProfileImage", SqlDbType.NVarChar);
                strParameter[7].Value = ProfileImage;
                strParameter[8] = new SqlParameter("@pPhone", SqlDbType.NVarChar);
                strParameter[8].Value = Phone;
                strParameter[9] = new SqlParameter("@pEmail", SqlDbType.NVarChar);
                strParameter[9].Value = Email;
                strParameter[10] = new SqlParameter("@pAddress", SqlDbType.NVarChar);
                strParameter[10].Value = Address;
                strParameter[11] = new SqlParameter("@pCity", SqlDbType.NVarChar);
                strParameter[11].Value = City;
                strParameter[12] = new SqlParameter("@pState", SqlDbType.NVarChar);
                strParameter[12].Value = State;
                strParameter[13] = new SqlParameter("@pZip", SqlDbType.NVarChar);
                strParameter[13].Value = Zip;
                strParameter[14] = new SqlParameter("@pCountry", SqlDbType.NVarChar);
                strParameter[14].Value = Country;
                strParameter[15] = new SqlParameter("@pOwnerId", SqlDbType.Int);
                strParameter[15].Value = OwnerId;
                strParameter[16] = new SqlParameter("@pPatientStatus", SqlDbType.Int);
                strParameter[16].Value = PatientStatus;
                strParameter[17] = new SqlParameter("@pAddress2", SqlDbType.NVarChar);
                strParameter[17].Value = Address2;
                strParameter[18] = new SqlParameter("@pPassword", SqlDbType.VarChar);
                strParameter[18].Value = Password;
                strParameter[19] = new SqlParameter("@pNotes", SqlDbType.NVarChar);
                strParameter[19].Value = Notes;
                strParameter[20] = new SqlParameter("@pReferredBy", SqlDbType.NVarChar);
                strParameter[20].Value = ReferredBy;
                strParameter[21] = new SqlParameter("@Location", SqlDbType.NVarChar);
                strParameter[21].Value = LocationName;
                strParameter[22] = new SqlParameter("@oNewPatientId", SqlDbType.Int);
                strParameter[22].Direction = ParameterDirection.Output;
                strParameter[23] = new SqlParameter("@pFirstExamDate", SqlDbType.NVarChar);
                strParameter[23].Value = FirstExamDate;

                UpdateStatus = ObjHelper.ExecuteNonQuery("USP_PatientInsertAndUpdate", strParameter);

                int newPatientId = Convert.ToInt32(strParameter[22].Value);
                if (ReferredBy != null && ReferredBy > 0)
                {
                    DataTable User = new DataTable();
                    clsColleaguesData ObjColleaguesData = new clsColleaguesData();
                    User = ObjColleaguesData.GetDoctorDetailsbyId(Convert.ToInt32(OwnerId));
                    string UserName = string.Empty; string Colleaguesname = string.Empty;
                    if (User != null && User.Rows.Count > 0)
                    {
                        UserName = User.Rows[0]["FirstName"] + " " + User.Rows[0]["LastName"];
                    }
                    DataTable Colleague = new DataTable();
                    Colleague = ObjColleaguesData.GetDoctorDetailsbyId(Convert.ToInt32(ReferredBy));
                    if (Colleague != null && Colleague.Rows.Count > 0)
                    {
                        Colleaguesname = Colleague.Rows[0]["FirstName"] + " " + Colleague.Rows[0]["LastName"];
                    }
                    string XMLString = "";
                    if (newPatientId > 0)
                    {
                        if (Notes != null && Notes != "")
                        {
                            ObjColleaguesData.InsertReferral(Colleaguesname, UserName, "Referral Card", "Refer Patient", 2, Convert.ToInt32(Convert.ToString(ReferredBy)), PatientId, "", "", 0, Convert.ToInt32(OwnerId), Notes, "", "", "", "", "", LocationName, XMLString);
                        }
                        else
                        {
                            ObjColleaguesData.InsertReferral(Colleaguesname, UserName, "Referral Card", "Refer Patient", 2, Convert.ToInt32(Convert.ToString(ReferredBy)), PatientId, "", "", 0, Convert.ToInt32(OwnerId), "", "", "", "", "", "", LocationName, XMLString);
                        }
                    }
                    else
                    {
                        if (Notes != null && Notes != "")
                        {
                            ObjColleaguesData.InsertReferral(Colleaguesname, UserName, "Referral Card", "Refer Patient", 2, Convert.ToInt32(Convert.ToString(ReferredBy)), newPatientId, "", "", 0, Convert.ToInt32(OwnerId), Notes, "", "", "", "", "", LocationName, XMLString);
                        }
                        else
                        {
                            ObjColleaguesData.InsertReferral(Colleaguesname, UserName, "Referral Card", "Refer Patient", 2, Convert.ToInt32(Convert.ToString(ReferredBy)), newPatientId, "", "", 0, Convert.ToInt32(OwnerId), "", "", "", "", "", "", LocationName, XMLString);
                        }

                    }


                }
                if (UpdateStatus)
                {
                    if (newPatientId != 0)
                    {
                        if (!string.IsNullOrWhiteSpace(Email))
                        {
                            clsTemplate clstemp = new clsTemplate();
                            clstemp.PatientAddedNotification(OwnerId, newPatientId, Email, FirstName, LastName, Phone, Address, "", City, State, Country, Zip);
                            if (!string.IsNullOrWhiteSpace(Password))
                            {
                                clstemp.PatientSignUpEMail(Email, Password, OwnerId);
                            }
                        }


                        int RecordId = 0;
                        int DoctorId = OwnerId;

                        int MontageId = 0;
                        int Monatgetype = 0;
                        int imageId = 0;
                        int UploadImageId = 0;

                        int ownerId = DoctorId;
                        int ownertype = 0; int images;
                        DataTable dtimageupload = ObjCommon.Get_UploadedImagesByDoctor(DoctorId, clsCommon.GetUniquenumberForAttachments());
                        DataTable dtfileupload = ObjCommon.Get_UploadedFilesByDoctor(DoctorId, clsCommon.GetUniquenumberForAttachments());
                        DataTable dtPatientImages = GetPatientImages(newPatientId, null);

                        if (dtPatientImages.Rows.Count != 0)
                        {
                            MontageId = Convert.ToInt32(dtPatientImages.Rows[0]["MontageId"]);

                        }


                        if (MontageId == 0)
                        {

                            string MontageName = newPatientId + "Montage";
                            int montageid = Convert.ToInt32(AddMontage(MontageName, Monatgetype, newPatientId, ownerId, ownertype));

                            for (images = 0; images < dtimageupload.Rows.Count; images++)
                            {
                                string pathfull = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings.Get("ImageBank").ToString().Replace("../", ""));
                                pathfull = pathfull.Replace("\\Patients\\", "\\");
                                string fullpath = pathfull;
                                string filename = Convert.ToString(dtimageupload.Rows[images]["DocumentName"]).Replace(ConfigurationManager.AppSettings.Get("ImageBank"), "");
                                int Imagetype;

                                Imagetype = 0;

                                int imageid = AddImagetoPatient(newPatientId, 1, filename, 1000, 1000, 0, Imagetype, fullpath, dtimageupload.Rows[images]["DocumentName"].ToString(), ownerId, ownertype, 1, "Doctor");
                                if (imageid > 0)
                                {

                                    bool resmonimg = AddImagetoMontage(imageid, Imagetype, montageid);
                                }


                            }

                            for (int dentaldoc = 0; dentaldoc < dtfileupload.Rows.Count; dentaldoc++)
                            {
                                RecordId = UploadPatientDocument(montageid, dtfileupload.Rows[dentaldoc]["DocumentName"].ToString(), "Doctor", 0, 0);
                            }

                        }

                        else if (MontageId != 0)
                        {
                            for (int MontageImage = 0; MontageImage < dtimageupload.Rows.Count; MontageImage++)
                            {

                                if (UploadImageId == 0)
                                {

                                    string pathfull = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings.Get("ImageBank").ToString().Replace("../", ""));
                                    pathfull = pathfull.Replace("\\Patients\\", "\\");
                                    string fullpath = pathfull;
                                    string imagename = Convert.ToString(dtimageupload.Rows[MontageImage]["DocumentName"]).Replace(ConfigurationManager.AppSettings.Get("ImageBank"), "");
                                    int Imagetype;

                                    Imagetype = 0;

                                    imageId = AddImagetoPatient(newPatientId, 1, imagename, 1000, 1000, 0, Imagetype, fullpath, dtimageupload.Rows[MontageImage]["DocumentName"].ToString(), ownerId, ownertype, 1, "Doctor");
                                    if (imageId > 0)
                                    {
                                        bool resmonimg = AddImagetoMontage(imageId, Imagetype, MontageId);

                                    }

                                }


                            }

                            for (int dentaldoc = 0; dentaldoc < dtfileupload.Rows.Count; dentaldoc++)
                            {
                                RecordId = UploadPatientDocument(MontageId, dtfileupload.Rows[dentaldoc]["DocumentName"].ToString(), "Doctor", 0, 0);
                            }


                        }


                        string Sessionid = clsCommon.GetUniquenumberForAttachments();

                        DataTable dtTempFiles = ObjCommon.GetRecordsFromTempTablesForDoctor(ownerId, Sessionid);


                        if (dtTempFiles != null && dtTempFiles.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtTempFiles.Rows.Count; i++)
                            {
                                string DocName = Convert.ToString(dtTempFiles.Rows[i]["DocumentName"]);


                                string Extension = Path.GetExtension(DocName);
                                DocName = Convert.ToString(dtTempFiles.Rows[i]["DocumentName"]);
                                DocName = DocName.Replace("../TempUploads/", "");

                                string sourceFile = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings.Get("TempUploadsFolderPath")) + DocName;

                                sourceFile = sourceFile.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");
                                string tempPath = System.Configuration.ConfigurationManager.AppSettings["ImageBank"];
                                string destinationFile = HttpContext.Current.Server.MapPath(tempPath);
                                destinationFile = destinationFile.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");
                                string DestinationFilePath = destinationFile;
                                destinationFile = destinationFile + DocName;

                                if (System.IO.File.Exists(sourceFile))
                                {
                                    System.IO.File.Copy(sourceFile, destinationFile);


                                    if (Extension.ToLower() == ".jpg" || Extension.ToLower() == ".jpeg" || Extension.ToLower() == ".gif" || Extension.ToLower() == ".png" || Extension.ToLower() == ".bmp" || Extension.ToLower() == ".psd" || Extension.ToLower() == ".pspimage" || Extension.ToLower() == ".thm" || Extension.ToLower() == ".tif")
                                    {
                                        string ThubimagePath = HttpContext.Current.Server.MapPath("~/" + System.Configuration.ConfigurationManager.AppSettings["FolderPathThumb"]);

                                        ObjCommon.GenerateThumbnailImage(DestinationFilePath, DocName, ThubimagePath);
                                    }

                                }

                            }





                            // Delete from temp folder

                            for (int i = 0; i < dtTempFiles.Rows.Count; i++)
                            {
                                string DocName = Convert.ToString(dtTempFiles.Rows[i]["DocumentName"]);


                                string Extension = Path.GetExtension(DocName);
                                DocName = Convert.ToString(dtTempFiles.Rows[i]["DocumentName"]);
                                DocName = DocName.Replace("../TempUploads/", "");

                                string sourceFile = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings.Get("TempUploadsFolderPath")) + DocName;

                                sourceFile = sourceFile.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");
                                string tempPath = System.Configuration.ConfigurationManager.AppSettings["ImageBank"];
                                string destinationFile = HttpContext.Current.Server.MapPath(tempPath);
                                destinationFile = destinationFile.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");
                                string DestinationFilePath = destinationFile;
                                destinationFile = destinationFile + DocName;

                                if (System.IO.File.Exists(sourceFile))
                                {
                                    System.IO.File.Delete(sourceFile);
                                }
                            }




                        }

                        ObjCommon.Temp_RemoveAllByUserId(ownerId, Sessionid);

                    }
                }
                return newPatientId;

            }
            catch (Exception Ex)
            {
                ObjCommon.Temp_RemoveAllByUserId(OwnerId, clsCommon.GetUniquenumberForAttachments());
                return 0;
            }



        }









        public int PatientInsert(int PatientId, string AssignedPatientId, string FirstName,
                                        string MiddelName, string LastName, string DateOfBirth,
                                        int Gender, string ProfileImage, string Phone, string Email,
                                        string Address, string City, string State, string Zip, string Country,
                                        int OwnerId, int PatientStatus, string Address2, string Password, string Notes, object DentrixConnectorId = null, object Mobile = null)
        {
            int newPatientId = 0;
            DentrixConnectorId = DentrixConnectorId ?? DBNull.Value;
            Mobile = Mobile ?? DBNull.Value;
            //try
            //{
            bool UpdateStatus = false;

            SqlParameter[] strParameter = new SqlParameter[23];

            strParameter[0] = new SqlParameter("@pPatientId", SqlDbType.Int);
            strParameter[0].Value = PatientId;
            strParameter[1] = new SqlParameter("@pAssignedPatientId", SqlDbType.NVarChar);
            strParameter[1].Value = AssignedPatientId;
            strParameter[2] = new SqlParameter("@pFirstName", SqlDbType.NVarChar);
            strParameter[2].Value = FirstName;
            strParameter[3] = new SqlParameter("@pMiddelName", SqlDbType.NVarChar);
            strParameter[3].Value = MiddelName;
            strParameter[4] = new SqlParameter("@pLastName", SqlDbType.NVarChar);
            strParameter[4].Value = LastName;
            strParameter[5] = new SqlParameter("@pDateOfBirth", SqlDbType.NVarChar);
            strParameter[5].Value = DateOfBirth;
            strParameter[6] = new SqlParameter("@pGender", SqlDbType.Int);
            strParameter[6].Value = Gender;
            strParameter[7] = new SqlParameter("@pProfileImage", SqlDbType.NVarChar);
            strParameter[7].Value = ProfileImage;
            strParameter[8] = new SqlParameter("@pPhone", SqlDbType.NVarChar);
            strParameter[8].Value = Phone;
            strParameter[9] = new SqlParameter("@pEmail", SqlDbType.NVarChar);
            strParameter[9].Value = Email;
            strParameter[10] = new SqlParameter("@pAddress", SqlDbType.NVarChar);
            strParameter[10].Value = Address;
            strParameter[11] = new SqlParameter("@pCity", SqlDbType.NVarChar);
            strParameter[11].Value = City;
            strParameter[12] = new SqlParameter("@pState", SqlDbType.NVarChar);
            strParameter[12].Value = State;
            strParameter[13] = new SqlParameter("@pZip", SqlDbType.NVarChar);
            strParameter[13].Value = Zip;
            strParameter[14] = new SqlParameter("@pCountry", SqlDbType.NVarChar);
            strParameter[14].Value = Country;
            strParameter[15] = new SqlParameter("@pOwnerId", SqlDbType.Int);
            strParameter[15].Value = OwnerId;
            strParameter[16] = new SqlParameter("@pPatientStatus", SqlDbType.Int);
            strParameter[16].Value = PatientStatus;
            strParameter[17] = new SqlParameter("@pAddress2", SqlDbType.NVarChar);
            strParameter[17].Value = Address2;
            strParameter[18] = new SqlParameter("@pPassword", SqlDbType.VarChar);
            strParameter[18].Value = Password;
            strParameter[19] = new SqlParameter("@pNotes", SqlDbType.NVarChar);
            strParameter[19].Value = Notes;
            strParameter[20] = new SqlParameter("@oNewPatientId", SqlDbType.Int);
            strParameter[20].Direction = ParameterDirection.Output;
            strParameter[21] = new SqlParameter("@DentrixConnectorId", SqlDbType.Int);
            strParameter[21].Value = DentrixConnectorId;
            strParameter[22] = new SqlParameter("@Mobile", SqlDbType.NVarChar);
            strParameter[22].Value = Mobile;

            UpdateStatus = ObjHelper.ExecuteNonQuery("USP_PatientInsertAndUpdate", strParameter);

            newPatientId = Convert.ToInt32(strParameter[20].Value);

            return newPatientId;

            //}
            //catch (Exception )
            //{
            //    return newPatientId;
            //}
        }



        public int NewPatientInsert(MappedColumn mappedcolumn, int OwnerId, int newPatientId, int PatientId, string AssignedPatientId, string ProfileImage, int PatientStatus, string Address2, string Notes = null)
        {


            int newPatientID = 0;
            object DentrixConnectorId = null;
            object Mobile = null;
            DentrixConnectorId = DentrixConnectorId ?? DBNull.Value;
            Mobile = Mobile ?? DBNull.Value;
            //try
            //{
            bool UpdateStatus = false;

            SqlParameter[] strParameter = new SqlParameter[23];

            strParameter[0] = new SqlParameter("@pPatientId", SqlDbType.Int);
            strParameter[0].Value = PatientId;
            strParameter[1] = new SqlParameter("@pAssignedPatientId", SqlDbType.NVarChar);
            strParameter[1].Value = AssignedPatientId;
            strParameter[2] = new SqlParameter("@pFirstName", SqlDbType.NVarChar);
            strParameter[2].Value = mappedcolumn.FirstName;
            strParameter[3] = new SqlParameter("@pMiddelName", SqlDbType.NVarChar);
            strParameter[3].Value = mappedcolumn.MiddleName;
            strParameter[4] = new SqlParameter("@pLastName", SqlDbType.NVarChar);
            strParameter[4].Value = mappedcolumn.LastName;
            strParameter[5] = new SqlParameter("@pDateOfBirth", SqlDbType.NVarChar);
            strParameter[5].Value = mappedcolumn.DateOfBirth;
            strParameter[6] = new SqlParameter("@pGender", SqlDbType.Int);
            strParameter[6].Value = mappedcolumn.Gender;
            strParameter[7] = new SqlParameter("@pProfileImage", SqlDbType.NVarChar);
            strParameter[7].Value = ProfileImage;
            strParameter[8] = new SqlParameter("@pPhone", SqlDbType.NVarChar);
            strParameter[8].Value = mappedcolumn.Phone;
            strParameter[9] = new SqlParameter("@pEmail", SqlDbType.NVarChar);
            strParameter[9].Value = mappedcolumn.Email;
            strParameter[10] = new SqlParameter("@pAddress", SqlDbType.NVarChar);
            strParameter[10].Value = mappedcolumn.Address;
            strParameter[11] = new SqlParameter("@pCity", SqlDbType.NVarChar);
            strParameter[11].Value = mappedcolumn.City;
            strParameter[12] = new SqlParameter("@pState", SqlDbType.NVarChar);
            strParameter[12].Value = mappedcolumn.State;
            strParameter[13] = new SqlParameter("@pZip", SqlDbType.NVarChar);
            strParameter[13].Value = mappedcolumn.Zip;
            strParameter[14] = new SqlParameter("@pCountry", SqlDbType.NVarChar);
            strParameter[14].Value = mappedcolumn.Country;
            strParameter[15] = new SqlParameter("@pOwnerId", SqlDbType.Int);
            strParameter[15].Value = OwnerId;
            strParameter[16] = new SqlParameter("@pPatientStatus", SqlDbType.Int);
            strParameter[16].Value = PatientStatus;
            strParameter[17] = new SqlParameter("@pAddress2", SqlDbType.NVarChar);
            strParameter[17].Value = Address2;
            strParameter[18] = new SqlParameter("@pPassword", SqlDbType.VarChar);
            strParameter[18].Value = mappedcolumn.Password;
            strParameter[19] = new SqlParameter("@pNotes", SqlDbType.NVarChar);
            strParameter[19].Value = Notes;
            strParameter[20] = new SqlParameter("@oNewPatientId", SqlDbType.Int);
            strParameter[20].Direction = ParameterDirection.Output;
            strParameter[21] = new SqlParameter("@DentrixConnectorId", SqlDbType.Int);
            strParameter[21].Value = DentrixConnectorId;
            strParameter[22] = new SqlParameter("@Mobile", SqlDbType.NVarChar);
            strParameter[22].Value = Mobile;

            UpdateStatus = ObjHelper.ExecuteNonQuery("USP_PatientInsertAndUpdate", strParameter);
            newPatientID = Convert.ToInt32(strParameter[20].Value);

            clsTemplate clstemp = new clsTemplate();
            if (!string.IsNullOrWhiteSpace(mappedcolumn.Email))
            {
                clstemp.PatientAddedNotification(OwnerId, newPatientID, mappedcolumn.Email, mappedcolumn.FirstName, mappedcolumn.LastName, mappedcolumn.Phone, "", "", mappedcolumn.City, mappedcolumn.State, mappedcolumn.Country, "");
                if (!string.IsNullOrWhiteSpace(mappedcolumn.Password))
                {
                    clstemp.PatientSignUpEMail(mappedcolumn.Email, mappedcolumn.Password, OwnerId);
                }
            }
            return newPatientID;
        }
        //Patient Insert Update New Method
        public int PatientInsertAndUpdateNew(Patients newPatientObj)
        {
            int newPatientId = 0;
            bool status = false;
            try
            {
                SqlParameter[] SqlParam = new SqlParameter[20];
                SqlParam[0] = new SqlParameter("pPatientId", SqlDbType.Int);
                SqlParam[0].Value = newPatientObj.PatientId;
                SqlParam[1] = new SqlParameter("pAssignedPatientId", SqlDbType.Int);
                SqlParam[1].Value = Convert.ToInt32(newPatientObj.AssignedPatientId);
                SqlParam[2] = new SqlParameter("pFirstname", SqlDbType.NVarChar);
                SqlParam[2].Value = newPatientObj.FirstName;
                SqlParam[3] = new SqlParameter("pLastName", SqlDbType.NVarChar);
                SqlParam[3].Value = newPatientObj.LastName;
                SqlParam[4] = new SqlParameter("pPhone", SqlDbType.NVarChar);
                SqlParam[4].Value = newPatientObj.Phone;
                SqlParam[5] = new SqlParameter("pEmail", SqlDbType.NVarChar);
                SqlParam[5].Value = newPatientObj.Email;
                SqlParam[6] = new SqlParameter("pGender", SqlDbType.NVarChar);
                SqlParam[6].Value = newPatientObj.Gender;
                SqlParam[7] = new SqlParameter("pReferredBy", SqlDbType.NVarChar);
                SqlParam[7].Value = newPatientObj.ReferredBy;
                SqlParam[8] = new SqlParameter("Location", SqlDbType.NVarChar);
                SqlParam[8].Value = newPatientObj.LocationId;
                SqlParam[9] = new SqlParameter("pAddress", SqlDbType.NVarChar);
                SqlParam[9].Value = newPatientObj.ExactAddress;
                SqlParam[10] = new SqlParameter("pCity", SqlDbType.NVarChar);
                SqlParam[10].Value = newPatientObj.City;
                SqlParam[11] = new SqlParameter("pState", SqlDbType.NVarChar);
                SqlParam[11].Value = newPatientObj.State;
                SqlParam[12] = new SqlParameter("pZip", SqlDbType.NVarChar);
                SqlParam[12].Value = newPatientObj.ZipCode;
                SqlParam[13] = new SqlParameter("pDateOfBirth", SqlDbType.NVarChar);
                SqlParam[13].Value = string.IsNullOrEmpty(newPatientObj.DateOfBirth) ? null : newPatientObj.DateOfBirth;
                SqlParam[14] = new SqlParameter("pFirstExamDate", SqlDbType.NVarChar);
                SqlParam[14].Value = newPatientObj.FirstExamDate;
                SqlParam[15] = new SqlParameter("pNotes", SqlDbType.NVarChar);
                SqlParam[15].Value = newPatientObj.Notes;
                SqlParam[16] = new SqlParameter("pOwnerId", SqlDbType.Int);
                SqlParam[16].Value = newPatientObj.OwnerId;
                SqlParam[17] = new SqlParameter("pPatientStatus", SqlDbType.Int);
                SqlParam[17].Value = newPatientObj.PatientStatus;
                SqlParam[18] = new SqlParameter("@pPassword", SqlDbType.VarChar);
                SqlParam[18].Value = newPatientObj.Password;
                SqlParam[19] = new SqlParameter("@oNewPatientId", SqlDbType.Int);
                SqlParam[19].Direction = ParameterDirection.Output;

                status = ObjHelper.ExecuteNonQuery("USP_PatientInsertAndUpdate", SqlParam);
                newPatientId = Convert.ToInt32(SqlParam[19].Value);
                //SUP-68 Changes
                if (newPatientObj.PatientId == 0)
                {
                    if (!string.IsNullOrWhiteSpace(newPatientObj.Email))
                    {
                        clsTemplate clstemp = new clsTemplate();
                        clstemp.PatientAddedNotification(newPatientObj.OwnerId, newPatientId, newPatientObj.Email, newPatientObj.FirstName, newPatientObj.LastName, newPatientObj.Phone, newPatientObj.ExactAddress, "", newPatientObj.City, newPatientObj.State, newPatientObj.Country, newPatientObj.ZipCode);
                        if (!string.IsNullOrWhiteSpace(newPatientObj.Password))
                        {
                            clstemp.PatientSignUpEMail(newPatientObj.Email, newPatientObj.Password, newPatientObj.OwnerId);
                        }
                    }
                }

                if (newPatientId > 0)
                {
                    newPatientObj.PatientId = newPatientId;
                }
                if (newPatientObj.ReferredBy != null && newPatientObj.ReferredBy != "")
                {
                    DataTable User = new DataTable();
                    clsColleaguesData ObjColleaguesData = new clsColleaguesData();
                    User = ObjColleaguesData.GetDoctorDetailsbyId(Convert.ToInt32(newPatientObj.OwnerId));
                    string UserName = string.Empty; string Colleaguesname = string.Empty;
                    if (User != null && User.Rows.Count > 0)
                    {
                        UserName = User.Rows[0]["FirstName"] + " " + User.Rows[0]["LastName"];
                    }
                    DataTable Colleague = new DataTable();
                    Colleague = ObjColleaguesData.GetDoctorDetailsbyId(Convert.ToInt32(newPatientObj.ReferredBy));
                    if (Colleague != null && Colleague.Rows.Count > 0)
                    {
                        Colleaguesname = Colleague.Rows[0]["FirstName"] + " " + Colleague.Rows[0]["LastName"];
                    }
                    string XMLString = "";
                    if (newPatientId > 0)
                    {
                        if (newPatientObj.Notes != null && newPatientObj.Notes != "")
                        {
                            ObjColleaguesData.InsertReferral(Colleaguesname, UserName, "Referral Card", "Refer Patient", 2, Convert.ToInt32(Convert.ToString(newPatientObj.ReferredBy)), newPatientObj.PatientId, "", "", 0, Convert.ToInt32(newPatientObj.OwnerId), newPatientObj.Notes, "", "", "", "", "", newPatientObj.LocationName, XMLString);
                        }
                        else
                        {
                            ObjColleaguesData.InsertReferral(Colleaguesname, UserName, "Referral Card", "Refer Patient", 2, Convert.ToInt32(Convert.ToString(newPatientObj.ReferredBy)), newPatientObj.PatientId, "", "", 0, Convert.ToInt32(newPatientObj.OwnerId), "", "", "", "", "", "", newPatientObj.LocationName, XMLString);
                        }
                    }
                    else
                    {
                        if (newPatientObj.Notes != null && newPatientObj.Notes != "")
                        {
                            ObjColleaguesData.InsertReferral(Colleaguesname, UserName, "Referral Card", "Refer Patient", 2, Convert.ToInt32(Convert.ToString(newPatientObj.ReferredBy)), newPatientId, "", "", 0, Convert.ToInt32(newPatientObj.OwnerId), newPatientObj.Notes, "", "", "", "", "", newPatientObj.LocationName, XMLString);
                        }
                        else
                        {
                            ObjColleaguesData.InsertReferral(Colleaguesname, UserName, "Referral Card", "Refer Patient", 2, Convert.ToInt32(Convert.ToString(newPatientObj.ReferredBy)), newPatientId, "", "", 0, Convert.ToInt32(newPatientObj.OwnerId), "", "", "", "", "", "", newPatientObj.LocationName, XMLString);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("clsPatientsData--USP_PatientInsertAndUpdate", ex.Message, ex.StackTrace);
                throw;
            }

            return newPatientObj.PatientId;
        }

        #endregion

        #region Method For get details of Pateint's All Notes
        public DataTable GetPatientNotesDetails(int PatientId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@pPatientId", SqlDbType.Int);
                strParameter[0].Value = PatientId;

                dt = ObjHelper.DataTable("USP_GetAllPatientNotes", strParameter);

                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        public DataTable GetPatientNoteByNoteId(int NoteId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] Sqlpara = new SqlParameter[1];
                Sqlpara[0] = new SqlParameter("@NoteId", SqlDbType.Int);
                Sqlpara[0].Value = NoteId;

                dt = ObjHelper.DataTable("Usp_GetPatientNotebyNoteId", Sqlpara);
                return dt;
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("", ex.Message, ex.StackTrace);
                throw;
            }
        }

        #region Method For Remove Patient
        public bool RemovePatient(int PatientId, int UserId)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                addphone[0].Value = PatientId;
                addphone[1] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[1].Value = UserId;

                UpdateStatus = ObjHelper.ExecuteNonQuery("USP_RemovePatient", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public bool RemovePatientNew(int PatientId, int UserId)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                addphone[0].Value = PatientId;
                addphone[1] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[1].Value = UserId;

                UpdateStatus = ObjHelper.ExecuteNonQuery("USP_RemovePatientNew", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }

        }

        //New Patinet Remove Method from Patien BLL Pankaj
        public bool RemovePatientnew(int PatientId, int UserId)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                addphone[0].Value = PatientId;
                addphone[1] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[1].Value = UserId;

                UpdateStatus = ObjHelper.ExecuteNonQuery("USP_RemovePatientNew", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }

        }




        #endregion

        #region Method Regarding Pateint's Note Insert
        public bool AddPatientNote(int PatientId, int UserId, string Note)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[3];
                addphone[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                addphone[0].Value = PatientId;
                addphone[1] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[1].Value = UserId;
                addphone[2] = new SqlParameter("@Note", SqlDbType.NVarChar);
                addphone[2].Value = Note;

                UpdateStatus = ObjHelper.ExecuteNonQuery("USP_InsertPatientNote", addphone);

                return UpdateStatus;
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("", ex.Message, ex.StackTrace);
                throw;
            }
        }
        #endregion

        #region Method Regarding Pateint's Note Remove
        public bool RemovePatientNote(int NoteId)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[1];
                addphone[0] = new SqlParameter("@NoteId", SqlDbType.Int);
                addphone[0].Value = NoteId;


                UpdateStatus = ObjHelper.ExecuteNonQuery("USP_RemovePatientNote", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Method for Get Details Patient Referral HistoryDetails By PatientId
        public DataTable GetPatientReferralHistoryDetailsByPatientId(int UserId, int PatientId, int PageIndex, int PageSize)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[4];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[1].Value = PatientId;
                strParameter[2] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[2].Value = PageIndex;
                strParameter[3] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[3].Value = PageSize;
                dt = ObjHelper.DataTable("USP_GetPatientReferralHistoryDetailsByPatientId", strParameter);

                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get all Colleagues TeamMember and Patient Treating doctorlist
        public DataTable GetColleagueTeamMemberandTreatingDoctorList(int PatientId, int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[1].Value = PatientId;
                dt = ObjHelper.DataTable("Usp_GetTeamMemberColleaguesandTreatingDoctorofPatient", strParameter);

                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetSelectedTeamMemberForPatientHistory(int PatientId, int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[1].Value = PatientId;
                dt = ObjHelper.DataTable("Usp_GetSelectedTeamMemberColleaguesandTreatingDoctorofPatient", strParameter);

                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        #endregion

        #region Method for Get All  Messages Of  Doctor sent by Patient
        public DataTable GetPatientMessagesForDoctor(int UserId, int PageIndex, int PageSize)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[1].Value = PageIndex;
                strParameter[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[2].Value = PageSize;
                dt = ObjHelper.DataTable("USP_GetPatientMessagesForDoctor", strParameter);

                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region Get All Patient Note For Doctor by DoctorId
        public DataTable GetPatientNotesforDoctor(int UserId, int ShortColumn, int ShortDirection, int PageIndex, int PageSize)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[5];
                sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlpara[0].Value = UserId;
                sqlpara[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                sqlpara[1].Value = PageIndex;
                sqlpara[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                sqlpara[2].Value = PageSize;
                sqlpara[3] = new SqlParameter("@ShortColumn", SqlDbType.Int);
                sqlpara[3].Value = ShortColumn;
                sqlpara[4] = new SqlParameter("@ShortDirection", SqlDbType.Int);
                sqlpara[4].Value = ShortDirection;
                dt = ObjHelper.DataTable("Usp_GetPatientNotesForDoctor", sqlpara);
                return dt;
            }
            catch (Exception)
            {


                throw;
            }
        }
        #endregion

        public DataTable GetAppointmentReportofdoctor(int UserId, string SortColumn, string SortDirection, int PageIndex, int PageSize, string SearchText, string Name = null, int Age = 0, string DoctorName = null, string Alert = null, DateTime? LastHygiene = null, string HygieneType = null, DateTime? NextAppointmentTime = null, DateTime? LastAppointmentTime = null, DateTime? AppointmentDateFrom = null, DateTime? AppointmentDateTo = null)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[16];
                sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlpara[0].Value = UserId;
                sqlpara[1] = new SqlParameter("@SortColumn", SqlDbType.Text);
                sqlpara[1].Value = SortColumn;
                sqlpara[2] = new SqlParameter("@SortDirection", SqlDbType.Text);
                sqlpara[2].Value = SortDirection;
                sqlpara[3] = new SqlParameter("@PageIndex", SqlDbType.Int);
                sqlpara[3].Value = PageIndex;
                sqlpara[4] = new SqlParameter("@PageSize", SqlDbType.Int);
                sqlpara[4].Value = PageSize;
                sqlpara[5] = new SqlParameter("@SearchText", SqlDbType.Text);
                sqlpara[5].Value = SearchText;
                sqlpara[6] = new SqlParameter("@Name", SqlDbType.Text);
                sqlpara[6].Value = Name;
                sqlpara[7] = new SqlParameter("@Age", SqlDbType.Int);
                sqlpara[7].Value = Age;
                sqlpara[8] = new SqlParameter("@DoctorName", SqlDbType.Text);
                sqlpara[8].Value = DoctorName;
                sqlpara[9] = new SqlParameter("@Alert", SqlDbType.Text);
                sqlpara[9].Value = Alert;
                sqlpara[10] = new SqlParameter("@LastHygiene", SqlDbType.DateTime);
                sqlpara[10].Value = LastHygiene;
                sqlpara[11] = new SqlParameter("@HygieneType", SqlDbType.Text);
                sqlpara[11].Value = HygieneType;
                sqlpara[12] = new SqlParameter("@NextAppointmentTime", SqlDbType.DateTime);
                sqlpara[12].Value = NextAppointmentTime;
                sqlpara[13] = new SqlParameter("@LastAppointmentTime", SqlDbType.DateTime);
                sqlpara[13].Value = LastAppointmentTime;
                sqlpara[14] = new SqlParameter("@AppointmentDateFrom", SqlDbType.DateTime);
                sqlpara[14].Value = AppointmentDateFrom;
                sqlpara[15] = new SqlParameter("@AppointmentDateTo", SqlDbType.DateTime);
                sqlpara[15].Value = AppointmentDateTo;

                dt = ObjHelper.DataTable("Usp_GetAppointmentReportOfDoctor", sqlpara);
                return dt;
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public DataTable GetAppointmentReportofdoctorForOneClick(int UserId, string SortColumn, string SortDirection, int PageIndex, int PageSize, string FirstName, string LastName , string DOB, string Email, string Phone)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[10];
                sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlpara[0].Value = UserId;
                sqlpara[1] = new SqlParameter("@SortColumn", SqlDbType.Text);
                sqlpara[1].Value = SortColumn;
                sqlpara[2] = new SqlParameter("@SortDirection", SqlDbType.Text);
                sqlpara[2].Value = SortDirection;
                sqlpara[3] = new SqlParameter("@PageIndex", SqlDbType.Int);
                sqlpara[3].Value = PageIndex;
                sqlpara[4] = new SqlParameter("@PageSize", SqlDbType.Int);
                sqlpara[4].Value = PageSize;
                sqlpara[5] = new SqlParameter("@FirstName", SqlDbType.Text);
                sqlpara[5].Value = FirstName;
                sqlpara[6] = new SqlParameter("@LastName", SqlDbType.Text);
                sqlpara[6].Value = LastName;
                sqlpara[7] = new SqlParameter("@DOB", SqlDbType.Text);
                sqlpara[7].Value = DOB;
                sqlpara[8] = new SqlParameter("@Email", SqlDbType.Text);
                sqlpara[8].Value = Email;
                sqlpara[9] = new SqlParameter("@Phone", SqlDbType.Text);
                sqlpara[9].Value = Phone;

                //dt = ObjHelper.DataTable("Usp_GetAppointmentForOneClickReferral", sqlpara);
                dt = ObjHelper.DataTable("Usp_GetAppointmentForOneClickReferral_New", sqlpara);
                return dt;
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("", ex.Message, ex.StackTrace);
                throw;
            }
        }


        public DataTable PatientSentAndReceivedList(int UserId, int Flag, int SortColumn, int SortDirection, string SearchText, int PageIndex, int PageSize, string NewSearchText, int FilterBy, string ColleagueId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[10];
                sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlpara[0].Value = UserId;
                sqlpara[1] = new SqlParameter("@Flag", SqlDbType.Int);
                sqlpara[1].Value = Flag;
                sqlpara[2] = new SqlParameter("@SortColumn", SqlDbType.Int);
                sqlpara[2].Value = SortColumn;
                sqlpara[3] = new SqlParameter("@SortDirection", SqlDbType.Int);
                sqlpara[3].Value = SortDirection;
                sqlpara[4] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
                sqlpara[4].Value = SearchText;
                sqlpara[5] = new SqlParameter("@PageIndex", SqlDbType.Int);
                sqlpara[5].Value = PageIndex;
                sqlpara[6] = new SqlParameter("@PageSize", SqlDbType.Int);
                sqlpara[6].Value = PageSize;
                sqlpara[7] = new SqlParameter("@FilterBy", SqlDbType.Int);
                sqlpara[7].Value = FilterBy;
                sqlpara[8] = new SqlParameter("@NewSearchText", SqlDbType.NVarChar);
                sqlpara[8].Value = NewSearchText;
                sqlpara[9] = new SqlParameter("@RefUserId", SqlDbType.NVarChar);
                sqlpara[9].Value = ColleagueId;

                dt = ObjHelper.DataTable("Usp_GetSentAndReceivedPatientList", sqlpara);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #region Method for Remove Message sent by patient from doctor
        //
        public bool RemovePatientMessageByDoctor(int PatientmessageId)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[1];
                addphone[0] = new SqlParameter("@PatientmessageId", SqlDbType.Int);
                addphone[0].Value = PatientmessageId;


                UpdateStatus = ObjHelper.ExecuteNonQuery("USP_RemovePatientMessageByDoctor", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }

        }
        #endregion

        #region Send Message to Patient
        public int Insert_PatientMessages(string FromField, string ToField, string Subject, string Body, int DoctorId, int PatientId, bool IsSave, bool IsDoctor, bool IsPatient)
        {
            try
            {
                bool bval;
                int oNewId = 0;
                SqlParameter[] Officeinfo = new SqlParameter[10];
                Officeinfo[0] = new SqlParameter();
                Officeinfo[0] = new SqlParameter("@FromField", SqlDbType.VarChar);
                Officeinfo[0].Value = FromField;
                Officeinfo[1] = new SqlParameter("@ToField", SqlDbType.VarChar);
                Officeinfo[1].Value = ToField;
                Officeinfo[2] = new SqlParameter("@Subject", SqlDbType.VarChar);
                Officeinfo[2].Value = Subject;
                Officeinfo[3] = new SqlParameter("@Body", SqlDbType.Text);
                Officeinfo[3].Value = Body;
                Officeinfo[4] = new SqlParameter("@DoctorId", SqlDbType.Int);
                Officeinfo[4].Value = DoctorId;
                Officeinfo[5] = new SqlParameter("@PatientId", SqlDbType.Int);
                Officeinfo[5].Value = PatientId;
                Officeinfo[6] = new SqlParameter("@IsSave", SqlDbType.Bit);
                Officeinfo[6].Value = IsSave;
                Officeinfo[7] = new SqlParameter("@IsDoctor", SqlDbType.Bit);
                Officeinfo[7].Value = IsDoctor;
                Officeinfo[8] = new SqlParameter("@IsPatient", SqlDbType.Bit);
                Officeinfo[8].Value = IsPatient;
                Officeinfo[9] = new SqlParameter("@Newpatientmessageid", SqlDbType.Int);
                Officeinfo[9].Direction = ParameterDirection.Output;
                bval = ObjHelper.ExecuteNonQuery("USP_InsertPatientMessages", Officeinfo);
                oNewId = Convert.ToInt32(Officeinfo[9].Value);
                return oNewId;
            }
            catch (Exception)
            {
                throw;
            }

        }
        #endregion

        #region Get PatientMessages Sent By Doctor To Patient by Id

        public DataTable GetPatientMessagesSentByDoctorToPatientId(int UserId, int PageIndex, int PageSize, int PatientId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] GetColleagueParams = new SqlParameter[4];


                GetColleagueParams[0] = new SqlParameter("@UserId", SqlDbType.Int);
                GetColleagueParams[0].Value = UserId;
                GetColleagueParams[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                GetColleagueParams[1].Value = PageIndex;

                GetColleagueParams[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                GetColleagueParams[2].Value = PageSize;

                GetColleagueParams[3] = new SqlParameter("@PatientId", SqlDbType.Int);
                GetColleagueParams[3].Value = PatientId;

                dt = ObjHelper.DataTable("USP_GetPatientMessagesSentByDoctorToPatientId", GetColleagueParams);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }

        }
        #endregion

        #region Insert Attachment For Patient in Message
        public bool InsertMessageAttachemntsForPatient(int MessageId, string AttachemntName)
        {
            bool result = false;
            try
            {

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                strParameter[0].Value = MessageId;
                strParameter[1] = new SqlParameter("@AttachemntName", SqlDbType.VarChar);
                strParameter[1].Value = AttachemntName;

                result = ObjHelper.ExecuteNonQuery("USP_InsertMessageAttachemnts", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }
        #endregion

        #region Method for Update ImageType Id
        public bool UpdateImageTypeId(int ImageId, int ImageTypeId)
        {
            bool res;
            try
            {
                SqlParameter[] updatepwd = new SqlParameter[2];
                updatepwd[0] = new SqlParameter("@ImageId", SqlDbType.Int);
                updatepwd[0].Value = ImageId;
                updatepwd[1] = new SqlParameter("@ImageTypeId", SqlDbType.Int);
                updatepwd[1].Value = ImageTypeId;

                res = ObjHelper.ExecuteNonQuery("USP_UpdateImageTypeId", updatepwd);
            }
            catch (Exception)
            {
                throw;
            }
            return res;
        }
        #endregion

        #region Patient's Images GetAll,Delete,Get by Id and Update by id

        public DataTable GetPatientImages(int patientid, int? OwnerId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] GetColleagueParams = new SqlParameter[2];


                GetColleagueParams[0] = new SqlParameter("@pPatientId", SqlDbType.Int);
                GetColleagueParams[0].Value = patientid;

                GetColleagueParams[1] = new SqlParameter("@pOwnerId", SqlDbType.Int);
                GetColleagueParams[1].Value = OwnerId;

                dt = ObjHelper.DataTable("getImageRecordsOfPatient", GetColleagueParams);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }

        }
        public bool RemovePatientImage(int ImageId, int MontageId)
        {
            bool res;
            try
            {
                SqlParameter[] updatepwd = new SqlParameter[1];
                updatepwd[0] = new SqlParameter("@ImageId", SqlDbType.Int);
                updatepwd[0].Value = ImageId;
                //updatepwd[1] = new SqlParameter("@MontageId", SqlDbType.Int);
                //updatepwd[1].Value = 0;

                res = ObjHelper.ExecuteNonQuery("USP_RemovePatientImage", updatepwd);
            }
            catch (Exception)
            {
                throw;
            }
            return res;
        }
        public DataTable GetDetailByImageId(int ImageId)
        {

            SqlParameter[] getreferoptions = new SqlParameter[1];
            getreferoptions[0] = new SqlParameter("@ImageId", SqlDbType.Int);
            getreferoptions[0].Value = ImageId;



            DataTable gr = ObjHelper.DataTable("GetImageById", getreferoptions);
            return gr;
        }

        public DataTable GetDetailByDocumentId(int DocumentId)
        {

            SqlParameter[] getreferoptions = new SqlParameter[1];
            getreferoptions[0] = new SqlParameter("@DocumentId", SqlDbType.Int);
            getreferoptions[0].Value = DocumentId;



            DataTable gr = ObjHelper.DataTable("GetDocumentByDocumentId", getreferoptions);
            return gr;
        }


        public bool UpdateDescriptionofImage(int ImageId, DateTime CreationDate, DateTime LastModifiedDate, string Description)
        {
            bool ResultTrue;
            try
            {
                SqlParameter[] strParameter = new SqlParameter[5];
                strParameter[0] = new SqlParameter("@ImageId", SqlDbType.Int);
                strParameter[0].Value = ImageId;
                strParameter[1] = new SqlParameter("@CreationDate", SqlDbType.DateTime);
                strParameter[1].Value = CreationDate;
                strParameter[2] = new SqlParameter("@LastModifiedDate", SqlDbType.DateTime);
                strParameter[2].Value = LastModifiedDate;
                strParameter[3] = new SqlParameter("@Description", SqlDbType.NVarChar);
                strParameter[3].Value = Description;
                strParameter[4] = new SqlParameter("@IsAPI", SqlDbType.Int);
                strParameter[4].Value = 0;

                ResultTrue = ObjHelper.ExecuteNonQuery("UpdateDescriptionofImage", strParameter);
                return ResultTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion

        #region Patient's Documents GetAll,Delete

        public DataTable GetPatientDocumentsAll(int PatientId, int? OwnerId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] getPatientDocs = new SqlParameter[2];
                getPatientDocs[0] = new SqlParameter("@pPatientId", SqlDbType.Int);
                getPatientDocs[0].Value = PatientId;
                getPatientDocs[1] = new SqlParameter("@OwnerID", SqlDbType.Int);
                getPatientDocs[1].Value = OwnerId;
                dt = ObjHelper.DataTable("GetPatientDocs", getPatientDocs);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool RemovePatientDocumentById(int MontageId)
        {
            bool res;
            try
            {
                SqlParameter[] updatepwd = new SqlParameter[1];
                updatepwd[0] = new SqlParameter("@MontageId", SqlDbType.Int);
                updatepwd[0].Value = MontageId;


                res = ObjHelper.ExecuteNonQuery("USP_RemovePatientDocumentById", updatepwd);
            }
            catch (Exception)
            {
                throw;
            }
            return res;
        }


        public DataTable GetDescriptionofDocumentById(int PatientId, int MontageId)
        {

            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@pPatientId", SqlDbType.Int);
                strParameter[0].Value = PatientId;
                strParameter[1] = new SqlParameter("@pMontageId", SqlDbType.Int);
                strParameter[1].Value = MontageId;

                dt = ObjHelper.DataTable("GetDescriptionofDocumentById", strParameter);
                return dt;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateDescriptionofDocument(int DocMontageId, DateTime CreationDate, DateTime LastModifiedDate, string Description)
        {
            bool ResultTrue;
            try
            {
                SqlParameter[] strParameter = new SqlParameter[5];
                strParameter[0] = new SqlParameter("@MontageId", SqlDbType.Int);
                strParameter[0].Value = DocMontageId;
                strParameter[1] = new SqlParameter("@CreationDate", SqlDbType.DateTime);
                strParameter[1].Value = CreationDate;
                strParameter[2] = new SqlParameter("@LastModifiedDate", SqlDbType.DateTime);
                strParameter[2].Value = LastModifiedDate;
                strParameter[3] = new SqlParameter("@Description", SqlDbType.NVarChar);
                strParameter[3].Value = Description;
                strParameter[4] = new SqlParameter("@IsAPI", SqlDbType.Int);
                strParameter[4].Value = 0;

                ResultTrue = ObjHelper.ExecuteNonQuery("UpdateDescriptionofDocument", strParameter);
                return ResultTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Patient's Images and Documents Direct upload
        public int AddMontage(string MontageName, int MonatageType, int PatientId, int OwnerId, int OwnerType)
        {
            try
            {
                bool result = false;
                int x = 0;
                SqlParameter[] addmontage = new SqlParameter[6];
                addmontage[0] = new SqlParameter();
                addmontage[0].ParameterName = "@pMontageName";
                addmontage[0].Value = MontageName;
                addmontage[1] = new SqlParameter();
                addmontage[1].ParameterName = "@pMontageType";
                addmontage[1].Value = MonatageType;
                addmontage[2] = new SqlParameter();
                addmontage[2].ParameterName = "@pPatientId";
                addmontage[2].Value = PatientId;
                addmontage[3] = new SqlParameter();
                addmontage[3].ParameterName = "@pOwnerId";
                addmontage[3].Value = OwnerId;
                addmontage[4] = new SqlParameter();
                addmontage[4].ParameterName = "@pOwnerType";
                addmontage[4].Value = OwnerType;
                addmontage[5] = new SqlParameter();
                addmontage[5].ParameterName = "@oMontageId";
                addmontage[5].Direction = ParameterDirection.Output;
                addmontage[5].Value = 1;
                result = ObjHelper.ExecuteNonQuery("addMontageNew", addmontage);

                x = Convert.ToInt32(addmontage[5].Value);
                return x;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int AddImagetoPatient(int PatientId, int TimePoint, string Name, int Height, int Width, int ImageFormat, int ImageType, string ImageFullPath, string RelativePath, int OwnerId, int OwnerType, int ImageStatus, string Uploadby)
        {
            try
            {
                int ImageId = 0;
                SqlParameter[] addImagetoPatientParameters = new SqlParameter[14];

                addImagetoPatientParameters[0] = new SqlParameter("@pPatientId", SqlDbType.Int);

                addImagetoPatientParameters[0].Value = PatientId;

                addImagetoPatientParameters[1] = new SqlParameter("@pTimepoint", SqlDbType.Int);

                addImagetoPatientParameters[1].Value = TimePoint;

                addImagetoPatientParameters[2] = new SqlParameter("@pName", SqlDbType.VarChar);

                addImagetoPatientParameters[2].Value = Name;

                addImagetoPatientParameters[3] = new SqlParameter("@pHeight", SqlDbType.Int);

                addImagetoPatientParameters[3].Value = Height;

                addImagetoPatientParameters[4] = new SqlParameter("@pWidth", SqlDbType.Int);

                addImagetoPatientParameters[4].Value = Width;

                addImagetoPatientParameters[5] = new SqlParameter("@pImageFormat", SqlDbType.Int);

                addImagetoPatientParameters[5].Value = ImageFormat;

                addImagetoPatientParameters[6] = new SqlParameter("@pImageType", SqlDbType.Int);

                addImagetoPatientParameters[6].Value = ImageType;

                addImagetoPatientParameters[7] = new SqlParameter("@pFullPathToImage", SqlDbType.VarChar);

                addImagetoPatientParameters[7].Value = ImageFullPath;

                addImagetoPatientParameters[8] = new SqlParameter("@pRelativePathToImage", SqlDbType.VarChar);

                addImagetoPatientParameters[8].Value = RelativePath;

                addImagetoPatientParameters[9] = new SqlParameter("@pOwnerId", SqlDbType.Int);

                addImagetoPatientParameters[9].Value = OwnerId;

                addImagetoPatientParameters[10] = new SqlParameter("@pOwnerType", SqlDbType.Int);

                addImagetoPatientParameters[10].Value = OwnerType;

                addImagetoPatientParameters[11] = new SqlParameter("@pImageStatus", SqlDbType.Int);

                addImagetoPatientParameters[11].Value = ImageStatus;

                addImagetoPatientParameters[12] = new SqlParameter("@oNewImageId", SqlDbType.Int);

                addImagetoPatientParameters[12].Direction = ParameterDirection.Output;


                addImagetoPatientParameters[13] = new SqlParameter("@pUploadby", SqlDbType.VarChar);

                addImagetoPatientParameters[13].Value = Uploadby;

                ObjHelper.ExecuteNonQuery("AddImageToPatientNew", addImagetoPatientParameters);

                ImageId = Convert.ToInt32(addImagetoPatientParameters[12].Value);

                return ImageId;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool AddImagetoMontage(int ImagId, int PositioninMontage, int MontageId)
        {
            try
            {
                bool result = false;
                SqlParameter[] addmontageImage = new SqlParameter[3];
                addmontageImage[0] = new SqlParameter();
                addmontageImage[0].ParameterName = "@pMontageId";
                addmontageImage[0].Value = MontageId;
                addmontageImage[1] = new SqlParameter();
                addmontageImage[1].ParameterName = "@pImageId";
                addmontageImage[1].Value = ImagId;
                addmontageImage[2] = new SqlParameter();
                addmontageImage[2].ParameterName = "@pPosition";
                addmontageImage[2].Value = PositioninMontage;
                result = ObjHelper.ExecuteNonQuery("addMontageImageNew", addmontageImage);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public int UploadPatientDocument(int MontageId, string DocumentName, string uploadby, int? PatientId, int? OwnerId)
        {
            bool res;
            try
            {
                int DocId = 0;
                SqlParameter[] updatepwd = new SqlParameter[6];
                updatepwd[0] = new SqlParameter("@MontageId", SqlDbType.Int);
                updatepwd[0].Value = 0;
                updatepwd[1] = new SqlParameter("@DocumentName", SqlDbType.VarChar);
                updatepwd[1].Value = DocumentName;
                updatepwd[2] = new SqlParameter("@uploadby", SqlDbType.VarChar);
                updatepwd[2].Value = uploadby;
                updatepwd[3] = new SqlParameter("@oNewDocId", SqlDbType.Int);
                updatepwd[3].Direction = ParameterDirection.Output;
                updatepwd[4] = new SqlParameter("@patientid", SqlDbType.Int);
                updatepwd[4].Value = PatientId;
                updatepwd[5] = new SqlParameter("@ownerid", SqlDbType.Int);
                updatepwd[5].Value = OwnerId;


                res = ObjHelper.ExecuteNonQuery("USP_UploadPatientDocument", updatepwd);
                DocId = Convert.ToInt32(updatepwd[3].Value);
                return DocId;
            }
            catch (Exception EX)
            {
                throw;
            }

        }

        #endregion

        #region PatientLoginform
        public DataTable PatientLoginform(string PatientEmail, string PatientPassword)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientEmail", SqlDbType.VarChar);
                strParameter[0].Value = PatientEmail;

                strParameter[1] = new SqlParameter("@PatientPassword", SqlDbType.VarChar);
                strParameter[1].Value = PatientPassword;

                dtTable = ObjHelper.DataTable("Dental_PatientLogin", strParameter);
                return dtTable;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region CheckTempTable
        public DataTable CheckTempTable(string email)
        {
            try
            {
                SqlParameter[] Getuploadfiles = new SqlParameter[1];
                Getuploadfiles[0] = new SqlParameter("@email", SqlDbType.VarChar);
                Getuploadfiles[0].Value = email;
                return ObjHelper.DataTable("CheckEmailInPatientTemp", Getuploadfiles);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Code For Patient Forms details insert in database


        public DataTable GetPatientsDetails(int PatientId)
        {
            try
            {
                SqlParameter[] getcolleaguepatientsparams = new SqlParameter[1];
                getcolleaguepatientsparams[0] = new SqlParameter();
                getcolleaguepatientsparams[0].ParameterName = "@PatientId ";
                getcolleaguepatientsparams[0].Value = PatientId;
                DataTable PatientsDt = ObjHelper.DataTable("getPatientProfiles", getcolleaguepatientsparams);
                return PatientsDt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool InsertPatientForms(int PatientID, string Operation, int UserId, int FormId)
        {
            bool result = false;
            try
            {

                SqlParameter[] strParameter = new SqlParameter[4];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;
                strParameter[2] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[2].Value = UserId;
                strParameter[3] = new SqlParameter("@FormId", SqlDbType.Int);
                strParameter[3].Value = FormId;

                result = ObjHelper.ExecuteNonQuery("Dental_PatientForms", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public DataTable GetRegistration(int PatientID, string Operation)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientID", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;

                dtTable = ObjHelper.DataTable("Dental_Registration", strParameter);
                return dtTable;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetInsuranceCoverage(int PatientID, string Operation)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@PatientID", SqlDbType.Int);
                strParameter[0].Value = PatientID;


                dtTable = ObjHelper.DataTable("DentalRegistration", strParameter);
                return dtTable;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool UpdateRegistrationForm(int PatientID, string Operation, string DateofBirth, string Gender, string Status, string Address, string city, string state, string zip, string phone, string mobile, string fax, string Registration)
        {
            bool result = false;
            try
            {
                SqlParameter[] strParameter = new SqlParameter[13];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;
                strParameter[2] = new SqlParameter("@DateofBirth", SqlDbType.NVarChar);
                strParameter[2].Value = (!string.IsNullOrWhiteSpace(DateofBirth)) ? (object)DateofBirth : DBNull.Value;
                strParameter[3] = new SqlParameter("@Gender", SqlDbType.VarChar);
                strParameter[3].Value = Gender;
                strParameter[4] = new SqlParameter("@Status", SqlDbType.VarChar);
                strParameter[4].Value = Status;
                strParameter[5] = new SqlParameter("@Address", SqlDbType.VarChar);
                strParameter[5].Value = Address;
                strParameter[6] = new SqlParameter("@city", SqlDbType.VarChar);
                strParameter[6].Value = city;
                strParameter[7] = new SqlParameter("@state", SqlDbType.VarChar);
                strParameter[7].Value = state;
                strParameter[8] = new SqlParameter("@zip", SqlDbType.VarChar);
                strParameter[8].Value = zip;
                strParameter[9] = new SqlParameter("@phone", SqlDbType.VarChar);
                strParameter[9].Value = phone;
                strParameter[10] = new SqlParameter("@mobile", SqlDbType.VarChar);
                strParameter[10].Value = mobile;
                strParameter[11] = new SqlParameter("@fax", SqlDbType.VarChar);
                strParameter[11].Value = fax;
                strParameter[12] = new SqlParameter("@Registration", SqlDbType.VarChar);
                strParameter[12].Value = Registration;

                result = ObjHelper.ExecuteNonQuery("Dental_Registration_Form", strParameter);
                return result;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clspatientdata--UpdateRegistrationForm", Ex.Message, Ex.StackTrace);
                return result;
            }
        }




        public DataTable GetMedicalHistory(int PatientID, string Operation)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientID", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;

                dtTable = ObjHelper.DataTable("Dental_MedicalHistory", strParameter);
                return dtTable;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool UpdateMedicalHistory(int PatientID, string Operation, string MedicalHistory)
        {
            bool result = false;
            try
            {
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;
                strParameter[2] = new SqlParameter("@MedicalHistory", SqlDbType.VarChar);
                strParameter[2].Value = MedicalHistory;
                result = ObjHelper.ExecuteNonQuery("Dental_Registration_Form", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public DataTable GetDentalHistory(int PatientID, string Operation)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientID", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;

                dtTable = ObjHelper.DataTable("Dental_DentalHistory", strParameter);
                return dtTable;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool UpdateDentalHistory(int PatientID, string Operation, string DentalHistory)
        {
            bool result = false;
            try
            {
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;
                strParameter[2] = new SqlParameter("@DentalHistory", SqlDbType.VarChar);
                strParameter[2].Value = DentalHistory;
                result = ObjHelper.ExecuteNonQuery("Dental_Registration_Form", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }
        #endregion

        #region Method For Remove Patient Profile Image
        public bool RemovePatientProfileImage(int PatientId)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[1];
                addphone[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                addphone[0].Value = PatientId;

                UpdateStatus = ObjHelper.ExecuteNonQuery("USP_RemovePatientProfileImage", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }

        }
        #endregion

        #region  GetMontage Id On Patient and owner

        public DataTable GetMontageID(int PatientId, int OwnerId)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = PatientId;
                strParameter[1] = new SqlParameter("@OwnerId", SqlDbType.Int);
                strParameter[1].Value = OwnerId;
                DataTable MontageIDDt = ObjHelper.DataTable("GetMontageIdOnPatientandowner", strParameter);

                return MontageIDDt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
        public DataTable GetFamilyDetails()
        {

            try
            {
                SqlParameter[] param = new SqlParameter[0];
                DataTable dtFamilyDetails = ObjHelper.DataTable("SP_GetFamilyMemberDetails", param);
                return dtFamilyDetails;
            }
            catch (Exception)
            {
                throw;
            }

        }
        public DataTable GetFamilyHistoryDetails(int PatientId)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                param[0].Value = PatientId;
                DataTable dtFamilyDetails = ObjHelper.DataTable("SP_FamilyHistory", param);
                return dtFamilyDetails;
            }
            catch (Exception)
            {
                throw;
            }

        }
        public bool InsertFamilyRelationDetails(int FromPatientId, int ToPatientId, int ReleativeId)
        {
            bool Status = false;
            try
            {

                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@FromPatientId", SqlDbType.Int);
                strParameter[0].Value = FromPatientId;
                strParameter[1] = new SqlParameter("@ToPatientId", SqlDbType.NVarChar);
                strParameter[1].Value = ToPatientId;
                strParameter[2] = new SqlParameter("@ReleativeId", SqlDbType.NVarChar);
                strParameter[2].Value = ReleativeId;
                return Status = ObjHelper.ExecuteNonQuery("SP_InsertFamilyDetails", strParameter);
            }
            catch (Exception)
            {
                return Status;
            }
        }
        public bool DeleteRelation(int FromPatientId, int ToPatientId)
        {
            bool Status = false;
            try
            {

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@FromPatientId", SqlDbType.Int);
                strParameter[0].Value = FromPatientId;
                strParameter[1] = new SqlParameter("@ToPatientId", SqlDbType.NVarChar);
                strParameter[1].Value = ToPatientId;
                return Status = ObjHelper.ExecuteNonQuery("SP_DeleteReleation", strParameter);
            }
            catch (Exception)
            {
                return Status;
            }
        }
        public int CheckReleationIsExist(int FromPatientId, int ToPatientId)
        {
            int Count = 0;
            try
            {

                SqlParameter[] param = new SqlParameter[2];
                param[0] = new SqlParameter("@FromPatientId", SqlDbType.Int);
                param[0].Value = FromPatientId;
                param[1] = new SqlParameter("@ToPatientId", SqlDbType.NVarChar);
                param[1].Value = ToPatientId;
                DataTable dtReleationDetails = ObjHelper.DataTable("SP_CheckReleationIsExist", param);
                return Count = dtReleationDetails.Rows.Count;
            }
            catch (Exception)
            {
                return Count;
            }
        }


        #region Method Reagarding Notification_Setting Insert and Update
        public void NotificationSettingInsertAndUpdate(int Userid, int Patientid, int Accountid, bool Voice, bool Text, bool Email)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[6];
                strParameter[0] = new SqlParameter("@Userid", SqlDbType.Int);
                strParameter[0].Value = Userid;
                strParameter[1] = new SqlParameter("@Patientid", SqlDbType.Int);
                strParameter[1].Value = Patientid;
                strParameter[2] = new SqlParameter("@Accountid", SqlDbType.Int);
                strParameter[2].Value = Accountid;
                strParameter[3] = new SqlParameter("@Voice", SqlDbType.Bit);
                strParameter[3].Value = Voice;
                strParameter[4] = new SqlParameter("@Text", SqlDbType.Bit);
                strParameter[4].Value = Text;
                strParameter[5] = new SqlParameter("@Email", SqlDbType.Bit);
                strParameter[5].Value = Email;

                ObjHelper.ExecuteNonQuery("USP_InsertUpdateNotificationSetting", strParameter);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void NotificationSettingInsertAndUpdateForAll(int Userid, bool Voice, bool Text, bool Email)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[4];
                strParameter[0] = new SqlParameter("@Userid", SqlDbType.Int);
                strParameter[0].Value = Userid;
                strParameter[1] = new SqlParameter("@Voice", SqlDbType.Bit);
                strParameter[1].Value = Voice;
                strParameter[2] = new SqlParameter("@Text", SqlDbType.Bit);
                strParameter[2].Value = Text;
                strParameter[3] = new SqlParameter("@Email", SqlDbType.Bit);
                strParameter[3].Value = Email;

                ObjHelper.ExecuteNonQuery("USP_InsertUpdateNotificationSettingForAll", strParameter);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetAllPatientDetailsOfDoctor(int UserId, string SearchText, int SortColumn, int SortDirection, string NewSearchText, int FilterBy)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[6];
                strParameter[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
                strParameter[1].Value = SearchText;
                strParameter[2] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[2].Value = SortColumn;
                strParameter[3] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[3].Value = SortDirection;
                strParameter[4] = new SqlParameter("@NewSearchText", SqlDbType.NVarChar);
                strParameter[4].Value = NewSearchText;
                strParameter[5] = new SqlParameter("@FilterBy", SqlDbType.Int);
                strParameter[5].Value = FilterBy;

                dt = ObjHelper.DataTable("USP_GetALLPatientDetailsOfDoctor", strParameter);


                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }


        #endregion

        public int InsertPatientFromPublicProfile(MDLPatientSignUp model)
        {
            try
            {
                int NewPatientId = 0;
                bool Result = false;
                SqlParameter[] param = new SqlParameter[8];
                param[0] = new SqlParameter("@OwnerId", SqlDbType.Int);
                param[0].Value = model.PublicProfileUserId;
                param[1] = new SqlParameter("@FirstName", SqlDbType.NVarChar);
                param[1].Value = model.FirstName;
                param[2] = new SqlParameter("@LastName", SqlDbType.NVarChar);
                param[2].Value = model.LastName;
                param[3] = new SqlParameter("@Email", SqlDbType.NVarChar);
                param[3].Value = model.Email;
                param[4] = new SqlParameter("@Phone", SqlDbType.NVarChar);
                param[4].Value = model.Phone;
                param[5] = new SqlParameter("@OfferId", SqlDbType.NVarChar);
                param[5].Value = model.OfferID;
                param[6] = new SqlParameter("@Password", SqlDbType.NVarChar);
                param[6].Value = model.Password;
                param[7] = new SqlParameter("@NewPatientId", SqlDbType.Int);
                param[7].Direction = ParameterDirection.Output;

                Result = ObjHelper.ExecuteNonQuery("USP_PatientInsertAndUpdate_New", param);
                NewPatientId = Convert.ToInt32(param[7].Value);
                return NewPatientId;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clspatientdata--InsertPatientFromPublicProfile", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool InsertPatientContactInfoFromPublicProfile(PatientContactInfo ObjPatientContactInfo)
        {
            try
            {
                bool Result = false;
                SqlParameter[] param = new SqlParameter[9];
                param[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                param[0].Value = ObjPatientContactInfo.PatientId;
                param[1] = new SqlParameter("@Address", SqlDbType.NVarChar);
                param[1].Value = ObjPatientContactInfo.Address;
                param[2] = new SqlParameter("@City", SqlDbType.NVarChar);
                param[2].Value = ObjPatientContactInfo.City;
                param[3] = new SqlParameter("@State", SqlDbType.NVarChar);
                param[3].Value = ObjPatientContactInfo.State;
                param[4] = new SqlParameter("@ZipCode", SqlDbType.NVarChar);
                param[4].Value = ObjPatientContactInfo.ZipCode;
                param[5] = new SqlParameter("@DOB", SqlDbType.Date);
                param[5].Value = ObjPatientContactInfo.BOD;
                param[6] = new SqlParameter("@Gender", SqlDbType.Bit);
                param[6].Value = ObjPatientContactInfo.Gender;
                param[7] = new SqlParameter("@EMGContactNo", SqlDbType.NVarChar);
                param[7].Value = ObjPatientContactInfo.EMGContactNo;
                param[8] = new SqlParameter("@Email", SqlDbType.NVarChar);
                param[8].Value = ObjPatientContactInfo.Email;
                return Result = ObjHelper.ExecuteNonQuery("InsertPatientContactInfo_New", param);
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clspatientdata--InsertPatientContactInfo_New", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetPatientListForAutoComplete(int UserId, string Searchtext, string PatientId = null)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
                strParameter[1].Value = Searchtext;
                strParameter[2] = new SqlParameter("@PatientId", SqlDbType.NVarChar);
                strParameter[2].Value = PatientId;
                dt = ObjHelper.DataTable("Usp_AutoComplete_GetPatientList", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsPatientssData--Usp_AutoComplete_GetPatientList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public int InsertUpdateDraftMessage(int MessageId, string Body, string DraftPatientId, int CreatedByUserId)
        {
            try
            {
                bool bval;
                int oNewId = 0;
                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                param[0].Value = MessageId;
                param[1] = new SqlParameter("@Body", SqlDbType.NVarChar);
                param[1].Value = Body;
                param[2] = new SqlParameter("@DraftPatientId", SqlDbType.NVarChar);
                param[2].Value = DraftPatientId;
                param[3] = new SqlParameter("@CreatedByUserId", SqlDbType.Int);
                param[3].Value = CreatedByUserId;
                param[4] = new SqlParameter("@Newmessageid", SqlDbType.Int);
                param[4].Direction = ParameterDirection.Output;

                bval = ObjHelper.ExecuteNonQuery("SP_InsertPatientDraftMessage", param);
                oNewId = Convert.ToInt32(param[4].Value);
                return oNewId;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData - InsertUpdateDraftMessage", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        public int InsertUpdatePatientMessages(string FromField, string ToField, string Subject, string Body, int DoctorId, int PatientId, bool IsSave, bool IsDoctor, bool IsPatient, int MessageId = 0)
        {
            try
            {
                bool bval;
                int oNewId = 0;
                SqlParameter[] Officeinfo = new SqlParameter[11];
                Officeinfo[0] = new SqlParameter();
                Officeinfo[0] = new SqlParameter("@FromField", SqlDbType.VarChar);
                Officeinfo[0].Value = FromField;
                Officeinfo[1] = new SqlParameter("@ToField", SqlDbType.VarChar);
                Officeinfo[1].Value = ToField;
                Officeinfo[2] = new SqlParameter("@Subject", SqlDbType.VarChar);
                Officeinfo[2].Value = Subject;
                Officeinfo[3] = new SqlParameter("@Body", SqlDbType.Text);
                Officeinfo[3].Value = Body;
                Officeinfo[4] = new SqlParameter("@DoctorId", SqlDbType.Int);
                Officeinfo[4].Value = DoctorId;
                Officeinfo[5] = new SqlParameter("@PatientId", SqlDbType.Int);
                Officeinfo[5].Value = PatientId;
                Officeinfo[6] = new SqlParameter("@IsSave", SqlDbType.Bit);
                Officeinfo[6].Value = IsSave;
                Officeinfo[7] = new SqlParameter("@IsDoctor", SqlDbType.Bit);
                Officeinfo[7].Value = IsDoctor;
                Officeinfo[8] = new SqlParameter("@IsPatient", SqlDbType.Bit);
                Officeinfo[8].Value = IsPatient;
                Officeinfo[9] = new SqlParameter("@MessageId", SqlDbType.Int);
                Officeinfo[9].Value = MessageId;
                Officeinfo[10] = new SqlParameter("@Newpatientmessageid", SqlDbType.Int);
                Officeinfo[10].Direction = ParameterDirection.Output;
                bval = ObjHelper.ExecuteNonQuery("USP_NewInsertUpdatePatientMessages", Officeinfo);
                oNewId = Convert.ToInt32(Officeinfo[10].Value);
                return oNewId;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public DataTable GetPatientBasedOnAssignedPatientId(string AssignedPatientId, int DoctorId)
        {
            DataTable dt = new DataTable();
            SqlParameter[] strParameter = new SqlParameter[2];
            strParameter[0] = new SqlParameter("@AssignedPatientId", SqlDbType.NVarChar);
            strParameter[0].Value = AssignedPatientId;
            strParameter[1] = new SqlParameter("@DoctorId", SqlDbType.Int);
            strParameter[1].Value = DoctorId;

            return ObjHelper.DataTable("USP_CheckPatientAssignedPatientId", strParameter);
        }
        public DataTable GetPatientCostEstimation(int UserId)
        {
            DataTable dt = new DataTable();
            SqlParameter[] strParameter = new SqlParameter[1];
            strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
            strParameter[0].Value = UserId;
            return ObjHelper.DataTable("Usp_GetPatientEstimationReport", strParameter);
        }

        public static bool UpdatePatientDetails(PatientEditableVM obj)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[15];
                strParameter[0] = new SqlParameter("@AssignedPatientId", SqlDbType.VarChar);
                strParameter[0].Value = obj.AssignedPatientId;
                strParameter[1] = new SqlParameter("@DobId", SqlDbType.DateTime);
                strParameter[1].Value = obj.DobId;
                strParameter[2] = new SqlParameter("@EmailId", SqlDbType.NVarChar);
                strParameter[2].Value = obj.EmailId;
                strParameter[3] = new SqlParameter("@GenderId", SqlDbType.Int);
                strParameter[3].Value = obj.GenderId;
                strParameter[4] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[4].Value = obj.PatientId;
                strParameter[5] = new SqlParameter("@PhoneId", SqlDbType.VarChar);
                strParameter[5].Value = obj.PhoneId;
                strParameter[6] = new SqlParameter("@AddressId", SqlDbType.VarChar);
                strParameter[6].Value = obj.AddressId;
                strParameter[7] = new SqlParameter("@SecondryEmail", SqlDbType.VarChar);
                strParameter[7].Value = obj.SecondryEmail;
                strParameter[8] = new SqlParameter("@AddressInfoEmail", SqlDbType.VarChar);
                strParameter[8].Value = obj.AddressInfoEmail;
                strParameter[9] = new SqlParameter("@Mobile", SqlDbType.VarChar);
                strParameter[9].Value = obj.Mobile;
                strParameter[10] = new SqlParameter("@Workphone", SqlDbType.VarChar);
                strParameter[10].Value = obj.Workphone;
                strParameter[11] = new SqlParameter("@Fax", SqlDbType.VarChar);
                strParameter[11].Value = obj.Fax;
                strParameter[12] = new SqlParameter("@City", SqlDbType.VarChar);
                strParameter[12].Value = obj.City;
                strParameter[13] = new SqlParameter("@Zip", SqlDbType.VarChar);
                strParameter[13].Value = obj.ZipCode;
                strParameter[14] = new SqlParameter("@State", SqlDbType.VarChar);
                strParameter[14].Value = obj.State;

                ObjHelper.DataTable("SP_UpdatePatientDetailsFromPH", strParameter);
                return true;
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("clsPatientData - UpdatePatientDetails", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public static DataTable CheckAssignPatientId(int AssignedPatientId, int PatientId, int UserId)
        {
            DataTable dt = new DataTable();
            SqlParameter[] param = new SqlParameter[3];
            param[0] = new SqlParameter("@AssignedPatientId", SqlDbType.Int);
            param[0].Value = AssignedPatientId;
            param[1] = new SqlParameter("@OwnerId", SqlDbType.Int);
            param[1].Value = UserId;
            param[2] = new SqlParameter("@PatientId", SqlDbType.Int);
            param[2].Value = PatientId;
            dt = ObjHelper.DataTable("USP_CheckAssignedPatientId", param);
            return dt;
        }


        public DataTable GetPatientRewardsReport(int UserId, int PageIndex, int PageSize, string SearchText, int SortColumn, int SortDirection, string NewSearchText, int FilterBy, int Location = 0)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[9];
                strParameter[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[1].Value = PageIndex;
                strParameter[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[2].Value = PageSize;
                strParameter[3] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
                strParameter[3].Value = SearchText;
                strParameter[4] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[4].Value = SortColumn;
                strParameter[5] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[5].Value = SortDirection;
                strParameter[6] = new SqlParameter("@NewSearchText", SqlDbType.NVarChar);
                strParameter[6].Value = NewSearchText;
                strParameter[7] = new SqlParameter("@FilterBy", SqlDbType.Int);
                strParameter[7].Value = FilterBy;
                strParameter[8] = new SqlParameter("@LocationId", SqlDbType.NVarChar);
                strParameter[8].Value = Convert.ToString(Location);

                dt = ObjHelper.DataTable("USP_GetPatientRewardsReport", strParameter);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// This Method Insert Patient On Recordlinc database.
        /// </summary>
        /// <param name="newPatientObj"></param>
        /// <param name="Userid"></param>
        /// <returns></returns>
        public static IDictionary<int, int> PatientInsertUpdateForDentrix(DentrixPatient newPatientObj)
        {
            try
            {
                var response = new Dictionary<int, int>();
                string emailAddress = string.Empty;
                string work = string.Empty;
                if (newPatientObj.Emails != null)
                {
                    if (newPatientObj.Emails.Count > 0)
                    {
                        emailAddress = newPatientObj.Emails.FirstOrDefault();
                    }
                }
                if (newPatientObj.PhoneNumbers != null)
                {
                    foreach (var item in newPatientObj.PhoneNumbers)
                    {
                        if (item.PhoneType == 2)//Work
                        {
                            work = item.PhoneNumber;
                        }
                    }
                }
                bool UpdateStatus = false;
                string PatientId = string.Empty;
                SqlParameter[] SqlParam = new SqlParameter[13];
                SqlParam[0] = new SqlParameter("@inPMSPatientID", SqlDbType.Int);
                SqlParam[0].Value = Convert.ToInt32(newPatientObj.patid);
                SqlParam[1] = new SqlParameter("@inFirstName", SqlDbType.NVarChar);
                SqlParam[1].Value = newPatientObj.FirstName;
                SqlParam[2] = new SqlParameter("@inLastName", SqlDbType.NVarChar);
                SqlParam[2].Value = newPatientObj.LastName;
                SqlParam[3] = new SqlParameter("@inWorkPhone", SqlDbType.NVarChar);
                SqlParam[3].Value = work.Trim();
                SqlParam[4] = new SqlParameter("@inEmail", SqlDbType.NVarChar);
                SqlParam[4].Value = emailAddress;
                SqlParam[5] = new SqlParameter("@inGender", SqlDbType.Int);
                SqlParam[5].Value = newPatientObj.gender;
                SqlParam[6] = new SqlParameter("@inDOB", SqlDbType.Date);
                SqlParam[6].Value = newPatientObj.birthdate;
                SqlParam[7] = new SqlParameter("@inProvId", SqlDbType.NVarChar);
                SqlParam[7].Value = newPatientObj.primprovid.Trim();
                SqlParam[8] = new SqlParameter("@inPassword", SqlDbType.VarChar);
                SqlParam[8].Value = newPatientObj.Password;
                SqlParam[9] = new SqlParameter("@oNewPatientId", SqlDbType.Int);
                SqlParam[9].Direction = ParameterDirection.Output;
                SqlParam[10] = new SqlParameter("@inDentrixConnectorKey", SqlDbType.VarChar);
                SqlParam[10].Value = newPatientObj.dentrixConnectorID;
                SqlParam[11] = new SqlParameter("@inMiddelName", SqlDbType.NVarChar);
                SqlParam[11].Value = (!string.IsNullOrWhiteSpace(newPatientObj.MiddleName)) ? newPatientObj.MiddleName.Trim() : string.Empty;
                SqlParam[12] = new SqlParameter("@inModifiedon", SqlDbType.DateTime);
                SqlParam[12].Value = newPatientObj.automodifiedtimestamp;
                UpdateStatus = ObjHelper.ExecuteNonQuery("Usp_InsertPatientFromDentrix", SqlParam);
                PatientId = Convert.ToString(SqlParam[9].Value);
                int PatientIds;
                int AddressId = 0;
                if (Int32.TryParse(PatientId, out PatientIds))
                {
                    PatientIds = Convert.ToInt32(PatientId);
                }
                if (PatientIds > 0 && PatientIds != 2)
                {
                    AddressId = AddPatientAddressDetails(newPatientObj, PatientIds);
                }
                response.Add(PatientIds, AddressId);
                return response;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("PatientInsertUpdateForDentrix", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool InsertupdateDentrixPatientDetails(DentrixPatient Obj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName = "@PatientId",Value=Obj.RL_Id},
                    new SqlParameter{ParameterName = "@patguid",Value=Obj.patguid},
                    new SqlParameter{ParameterName = "@guarid",Value=Obj.guarid},
                    new SqlParameter{ParameterName = "@isguar",Value=Obj.isguar},
                    new SqlParameter{ParameterName = "@isinssubscriber",Value=Obj.isinssubscriber},
                    new SqlParameter{ParameterName = "@statusstring",Value=Obj.statusstring},
                    new SqlParameter{ParameterName = "@secprovider",Value=Obj.secprovid},
                    new SqlParameter{ParameterName = "@firstvisitdate",Value=Obj.FirstVisitDate},
                    new SqlParameter{ParameterName = "@lastvisitdate",Value=Obj.LastVisitDate},
                    new SqlParameter{ParameterName = "@automodifiedtimestamp",Value=Obj.automodifiedtimestamp},
                     new SqlParameter{ParameterName = "@BillingType",Value=Obj.BillingType}
                };
                return new clsHelper().ExecuteNonQuery("Usp_InsertUpdateDentrixPatientDetails", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--InsertupdateDentrixPatientDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int InsertUpdatePatient_DentrixAdjustmentDetails(Patient_DentrixAdjustments Obj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName = "@Patid",Value=Obj.patid},
                    new SqlParameter{ParameterName = "@guarid",Value=Obj.guarid},
                    new SqlParameter{ParameterName = "@procdate",Value=Obj.procdate},
                    new SqlParameter{ParameterName = "@claimid",Value=Obj.claimid},
                    new SqlParameter{ParameterName = "@AdjustType",Value=Obj.adjusttype},
                    new SqlParameter{ParameterName = "@AdjustTypeString",Value=Obj.adjusttypestring},
                    new SqlParameter{ParameterName = "@Adjustdefid",Value=Obj.adjustdefid},
                    new SqlParameter{ParameterName = "@Adjustdescript",Value=Obj.adjustdescript},
                    new SqlParameter{ParameterName = "@amount",Value=Obj.amount},
                    new SqlParameter{ParameterName = "@history",Value=Obj.history},
                    new SqlParameter{ParameterName = "@AppliedToPayAgreement",Value=Obj.appliedtopayagreement},
                    new SqlParameter{ParameterName = "@DentrixConnecotrKey",Value=Obj.dentrixConnectorID},
                    new SqlParameter{ParameterName = "@automodifiedtimestamp",Value=Obj.automodifiedtimestamp},
                    new SqlParameter{ParameterName = "@inProvId",Value=Obj.provid},
                    new SqlParameter{ParameterName = "@adjustid",Value=Obj.adjustid},
                    new SqlParameter{ParameterName = "@oOutPut",Direction = ParameterDirection.Output,SqlDbType=SqlDbType.Int}
                };
                new clsHelper().ExecuteNonQuery("Usp_InsertUpdatePatientAdjustmentDetails", sqlpara);
                int AdjustmentId = Convert.ToInt32(sqlpara[15].Value);
                return AdjustmentId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--InsertUpdatePatient_DentrixAdjustmentDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int InsertUpdatePatient_DentrixInsurancePaymentDetails(Patient_DentrixInsurancePayment Obj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName ="@Patid",Value=Obj.patid},
                    new SqlParameter{ParameterName ="@Guarid",Value=Obj.guarid},
                    new SqlParameter{ParameterName ="@Procdate",Value=Obj.procdate},
                    new SqlParameter{ParameterName ="@Claimid",Value=Obj.claimid},
                    new SqlParameter{ParameterName ="@CreateDate",Value=Obj.createdate},
                    new SqlParameter{ParameterName ="@InsurancePaytype",Value=Obj.inspaytype},
                    new SqlParameter{ParameterName ="@InsurancePayDescription",Value=Obj.inspaydescript},
                    new SqlParameter{ParameterName ="@Amount",Value=Obj.amount},
                    new SqlParameter{ParameterName ="@history",Value=Obj.history},
                    new SqlParameter{ParameterName ="@DentrixConnectorKey",Value=Obj.dentrixConnectorID},
                    new SqlParameter{ParameterName = "@automodifiedtimestamp",Value=Obj.automodifiedtimestamp},
                    new SqlParameter{ParameterName ="@paymentId",Value=Obj.paymentId},
                    new SqlParameter{ParameterName = "@inProvId",Value=Obj.provid},
                    new SqlParameter{ParameterName = "@oOutPut",Direction=ParameterDirection.Output,SqlDbType=SqlDbType.Int},
                };
                new clsHelper().ExecuteNonQuery("Usp_InsertPatient_DentrixInsuranceDetails", sqlpara);
                int PaymentId = Convert.ToInt32(sqlpara[13].Value);
                return PaymentId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--InsertUpdatePatient_DentrixAdjustmentDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int InsertUpdatePatient_DentrixStandarsPayment(Patient_DentrixStandardPayment Obj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName = "@patId",Value=Obj.patid},
                    new SqlParameter{ParameterName = "@guarid",Value=Obj.guarid},
                    new SqlParameter{ParameterName = "@Procdate",Value=Obj.procdate},
                    new SqlParameter{ParameterName = "@Paymentdefid",Value=Obj.paymentdefid},
                    new SqlParameter{ParameterName = "@Description",Value=Obj.paymentdesc},
                    new SqlParameter{ParameterName = "@Amount",Value=Obj.amount},
                    new SqlParameter{ParameterName = "@History",Value=Obj.history},
                    new SqlParameter{ParameterName = "@AppliedToPayAgreeement",Value=Obj.appliedtopayagreement},
                    new SqlParameter{ParameterName = "@DentrixConnectorKey",Value=Obj.dentrixConnectorID},
                    new SqlParameter{ParameterName = "@automodifiedtimestamp",Value=Obj.automodifiedtimestamp},
                    new SqlParameter{ParameterName = "@paymentId",Value=Obj.paymentId},
                    new SqlParameter{ParameterName = "@inProvId",Value=Obj.provid},
                    new SqlParameter{ParameterName = "@oOutPut",Direction=ParameterDirection.Output,SqlDbType=SqlDbType.Int},
                };
                new clsHelper().ExecuteNonQuery("Usp_InsertPatient_DentrixStandardPayment", sqlpara);
                int PaymentId = Convert.ToInt32(sqlpara[12].Value);
                return PaymentId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--InsertUpdatePatient_DentrixStandarsPayment", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int InsertUpdatePatient_DentrixFinanceCharges(Patient_DentrixFinanceCharges Obj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName = "@Patid",Value=Obj.patid},
                    new SqlParameter{ParameterName = "@guarid",Value=Obj.guarid},
                    new SqlParameter{ParameterName = "@procdate",Value=Obj.procdate},
                    new SqlParameter{ParameterName = "@chargetype",Value=Obj.chargetype},
                    new SqlParameter{ParameterName = "@chargetypestring",Value=Obj.chargetypestring},
                    new SqlParameter{ParameterName = "@amount",Value=Obj.amount},
                    new SqlParameter{ParameterName = "@History",Value=Obj.history},
                    new SqlParameter{ParameterName = "@DentrixConnecotrKey",Value=Obj.dentrixConnectorID},
                    new SqlParameter{ParameterName = "@automodifiedtimestamp",Value=Obj.automodifiedtimestamp},
                     new SqlParameter{ParameterName = "@DtxChargesId",Value=Obj.chargeId},
                    new SqlParameter{ParameterName = "@inProvId",Value=Obj.provid},
                    new SqlParameter{ParameterName = "@oOutPut",Direction=ParameterDirection.Output,SqlDbType=SqlDbType.Int},
                };
                new clsHelper().ExecuteNonQuery("Usp_InsertPatient_DentrixFinanceCharges", sqlpara);
                int PaymentId = Convert.ToInt32(sqlpara[11].Value);
                return PaymentId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--Usp_InsertPatient_DentrixFinanceCharges", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int InsertIntoPatient_DentrixProcedure(Patient_DentrixProcedure Obj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[37];

                sqlpara[0] = new SqlParameter("@Patid", SqlDbType.Int);
                sqlpara[0].Value = Convert.ToInt32(Obj.patid);

                sqlpara[1] = new SqlParameter("@Guarid", SqlDbType.Int);
                sqlpara[1].Value = Convert.ToInt32(Obj.guarid);

                sqlpara[2] = new SqlParameter("@Procid", SqlDbType.Int);
                sqlpara[2].Value = Convert.ToInt32(Obj.procid);

                sqlpara[3] = new SqlParameter("@Procdate", SqlDbType.Date);
                sqlpara[3].Value = Obj.procdate;

                sqlpara[4] = new SqlParameter("@History", SqlDbType.Bit);
                sqlpara[4].Value = Convert.ToBoolean(Obj.history);

                sqlpara[5] = new SqlParameter("@TxCaseId", SqlDbType.Int);
                sqlpara[5].Value = Convert.ToInt32(Obj.txcaseid);

                sqlpara[6] = new SqlParameter("@ChartStatus", SqlDbType.Int);
                sqlpara[6].Value = Convert.ToInt32(Obj.chartstatus);

                sqlpara[7] = new SqlParameter("@ChartStatusString", SqlDbType.NVarChar);
                sqlpara[7].Value = (!string.IsNullOrWhiteSpace(Convert.ToString(Obj.chartstatusstring))) ? Convert.ToString(Obj.chartstatusstring).Trim() : string.Empty;

                sqlpara[8] = new SqlParameter("@PreAuthid", SqlDbType.Int);
                sqlpara[8].Value = Convert.ToInt32(Obj.preauthid);

                sqlpara[9] = new SqlParameter("@AmountPreAuth", SqlDbType.Decimal);
                sqlpara[9].Value = Convert.ToDecimal(Obj.amountpreauth);

                sqlpara[10] = new SqlParameter("@AuthStatus", SqlDbType.Int);
                sqlpara[10].Value = Convert.ToInt32(Obj.authstatus);

                sqlpara[11] = new SqlParameter("@AuthStatusString", SqlDbType.NVarChar);
                sqlpara[11].Value = (!string.IsNullOrWhiteSpace(Convert.ToString(Obj.authstatusstring))) ? Convert.ToString(Obj.authstatusstring).Trim() : string.Empty;

                sqlpara[12] = new SqlParameter("@AmountSecPreAuth", SqlDbType.Decimal);
                sqlpara[12].Value = Convert.ToDecimal(Obj.amountsecpreauth);

                sqlpara[13] = new SqlParameter("@SecAuthStatus", SqlDbType.Int);
                sqlpara[13].Value = Convert.ToInt32(Obj.secauthstatus);

                sqlpara[14] = new SqlParameter("@SecAuthStatusString", SqlDbType.NVarChar);
                sqlpara[14].Value = (!string.IsNullOrWhiteSpace(Convert.ToString(Obj.secauthstatusstring))) ? Convert.ToString(Obj.secauthstatusstring).Trim() : string.Empty;

                sqlpara[15] = new SqlParameter("@Amount", SqlDbType.Decimal);
                sqlpara[15].Value = Convert.ToDecimal(Obj.amount);

                sqlpara[16] = new SqlParameter("@AmountPrimInsPaid", SqlDbType.Decimal);
                sqlpara[16].Value = Convert.ToDecimal(Obj.amountpriminspaid);

                sqlpara[17] = new SqlParameter("@AmountSecInsPaid", SqlDbType.Decimal);
                sqlpara[17].Value = Convert.ToDecimal(Obj.amountsecinspaid);

                sqlpara[18] = new SqlParameter("@ClaimId", SqlDbType.Int);
                sqlpara[18].Value = Convert.ToInt32(Obj.claimid);

                sqlpara[19] = new SqlParameter("@ProcCodeId", SqlDbType.Int);
                sqlpara[19].Value = Convert.ToInt32(Obj.proccodeid);

                sqlpara[20] = new SqlParameter("@ADACode", SqlDbType.NVarChar);
                sqlpara[20].Value = (!string.IsNullOrWhiteSpace(Convert.ToString(Obj.adacode))) ? Convert.ToString(Obj.adacode).Trim() : string.Empty;

                sqlpara[21] = new SqlParameter("@MedProctype", SqlDbType.Bit);
                sqlpara[21].Value = Convert.ToBoolean(Obj.medproctype);

                sqlpara[22] = new SqlParameter("@DoNotBillInsurance", SqlDbType.Bit);
                sqlpara[22].Value = Convert.ToBoolean(Obj.donotbillinsurance);

                sqlpara[23] = new SqlParameter("@ToothRangeStart", SqlDbType.Int);
                sqlpara[23].Value = Convert.ToInt32(Obj.toothrangestart);

                sqlpara[24] = new SqlParameter("@ToothRangeEnd", SqlDbType.Int);
                sqlpara[24].Value = Convert.ToInt32(Obj.toothrangeend);

                sqlpara[25] = new SqlParameter("@SurfaceString", SqlDbType.NVarChar);
                sqlpara[25].Value = (!string.IsNullOrWhiteSpace(Convert.ToString(Obj.surfacestring))) ? Convert.ToString(Obj.surfacestring).Trim() : string.Empty;

                sqlpara[26] = new SqlParameter("@SurfaceBytes", SqlDbType.NVarChar);
                sqlpara[26].Value = (!string.IsNullOrWhiteSpace(Convert.ToString(Obj.surfacebytes))) ? Convert.ToString(Obj.surfacebytes).Trim() : string.Empty;

                sqlpara[27] = new SqlParameter("@TxPlannedDate", SqlDbType.Date);
                sqlpara[27].Value = Obj.txplanneddate;

                sqlpara[28] = new SqlParameter("@RefId", SqlDbType.Int);
                sqlpara[28].Value = Convert.ToInt32(Obj.refid);

                sqlpara[29] = new SqlParameter("@RefType", SqlDbType.Int);
                sqlpara[29].Value = Convert.ToInt32(Obj.reftype);

                sqlpara[30] = new SqlParameter("@DentrixConnectorKey", SqlDbType.NVarChar);
                sqlpara[30].Value = Convert.ToString(Obj.dentrixConnectorID);

                sqlpara[31] = new SqlParameter("@automodifiedtimestamp", SqlDbType.DateTime);
                sqlpara[31].Value = Obj.automodifiedtimestamp;

                sqlpara[32] = new SqlParameter("@inProvId", SqlDbType.NVarChar);
                sqlpara[32].Value = (!string.IsNullOrWhiteSpace(Convert.ToString(Obj.provid))) ? Convert.ToString(Obj.provid).Trim() : string.Empty;

                sqlpara[33] = new SqlParameter("@oOutput", SqlDbType.Int);
                sqlpara[33].Direction = ParameterDirection.Output;

                sqlpara[34] = new SqlParameter("@startdate", SqlDbType.DateTime);
                sqlpara[34].Value = Obj.startdate;

                sqlpara[35] = new SqlParameter("@completiondate", SqlDbType.DateTime);
                sqlpara[35].Value = Obj.completiondate;

                sqlpara[36] = new SqlParameter("@adadescription", SqlDbType.NVarChar);
                sqlpara[36].Value = Obj.adadescription;
                new clsHelper().ExecuteNonQuery("Usp_InsertPatient_DentrixProcedures", sqlpara);
                int REtruId = Convert.ToInt32(sqlpara[33].Value);
                return REtruId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--InsertIntoPatient_DentrixProcedure", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool UpdatePatientProfileImg(int PatientId, string ProfileImage)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addPatientParameters = new SqlParameter[2];

                addPatientParameters[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                //addPatientParameters[0].ParameterName = "@PatientId";
                addPatientParameters[0].Value = PatientId;

                addPatientParameters[1] = new SqlParameter("@ProfileImage", SqlDbType.VarChar);
                // addPatientParameters[1].ParameterName = "@ProfileImage";
                addPatientParameters[1].Value = ProfileImage;

                UpdateStatus = new clsHelper().ExecuteNonQuery("UpdatePatientProfileImg", addPatientParameters);

                return UpdateStatus;

            }
            catch (Exception)
            {
                throw;
            }
        }
        public static DataTable UpdatePatientDentails(Reward_PatientUpdate Obj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[16];

                sqlpara[0] = new SqlParameter("@FirstName", SqlDbType.NVarChar);
                sqlpara[0].Value = Convert.ToString(Obj.FirstName);

                sqlpara[1] = new SqlParameter("@LastName", SqlDbType.NVarChar);
                sqlpara[1].Value = Convert.ToString(Obj.LastName);

                sqlpara[2] = new SqlParameter("@Gender", SqlDbType.Bit);
                sqlpara[2].Value = Convert.ToInt32(Obj.Gender);

                sqlpara[3] = new SqlParameter("@DateOfBirth", SqlDbType.Date);
                sqlpara[3].Value = Obj.DateOfBirth;

                sqlpara[4] = new SqlParameter("@Address", SqlDbType.NVarChar);
                sqlpara[4].Value = Convert.ToString(Obj.Address);

                sqlpara[5] = new SqlParameter("@City", SqlDbType.NVarChar);
                sqlpara[5].Value = Convert.ToString(Obj.City);

                sqlpara[6] = new SqlParameter("@State", SqlDbType.NVarChar);
                sqlpara[6].Value = Convert.ToString(Obj.State);

                sqlpara[7] = new SqlParameter("@WorkPhone", SqlDbType.NVarChar);
                sqlpara[7].Value = Convert.ToString(Obj.WorkPhone);

                sqlpara[8] = new SqlParameter("@Email", SqlDbType.NVarChar);
                sqlpara[8].Value = Convert.ToString(Obj.Email);

                sqlpara[9] = new SqlParameter("@Zipcode", SqlDbType.NVarChar);
                sqlpara[9].Value = Convert.ToString(Obj.Zipcode);

                sqlpara[10] = new SqlParameter("@Password", SqlDbType.VarChar);
                sqlpara[10].Value = Convert.ToString(Obj.Password);

                sqlpara[11] = new SqlParameter("@solutiononeId", SqlDbType.Int);
                sqlpara[11].Direction = ParameterDirection.Output;

                sqlpara[12] = new SqlParameter("@RewardPartnerId", SqlDbType.Int);
                sqlpara[12].Value = Convert.ToInt32(Obj.RewardPartnerId);

                sqlpara[13] = new SqlParameter("@HomePhone", SqlDbType.NVarChar);
                sqlpara[13].Value = Convert.ToString(Obj.HomePhone);

                sqlpara[14] = new SqlParameter("@MobilePhone", SqlDbType.NVarChar);
                sqlpara[14].Value = Convert.ToString(Obj.MobilePhone);

                sqlpara[15] = new SqlParameter("@IsSendPushNotification", SqlDbType.Bit);
                sqlpara[15].Value = Convert.ToString(Obj.IsSendPushNotification);
                string _storedProcName = ObjCommon.GetStoredProcName((int)Obj.sourcetype, "Usp_UpdatePatientDetails", "Usp_Sup_UpdatePatientDetails");
                DataTable dt = new clsHelper().DataTable(_storedProcName, sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--UpdatePatientDentails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool AddUserForRewardPlatForm(Reward_AddDentist Obj)
        {
            try
            {
                bool IsInserted = false;
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ ParameterName="@OldSolutionOneId",Value=Obj.OldSolutionOneId},
                    new SqlParameter{ ParameterName="@OldAccountId",Value=Obj.OldAccountId},
                    new SqlParameter{ ParameterName="@NewSolutionOneId",Value=Obj.NewSolutionOneId},
                    new SqlParameter{ ParameterName="@NewAccountId",Value=Obj.NewAccountId},
                    new SqlParameter{ ParameterName="@OldRLPatientId",Value=Obj.OldRLPatientId},
                    new SqlParameter{ ParameterName="@NewRLPatientId",Value=Obj.NewRLPatientId}
                };
                //string _storedProcName = ObjCommon.GetStoredProcName((int)Obj.sourcetype, "Usp_AddDentistForRewardPlatForm", "Usp_AddDentistForRewardPlatForm");
                IsInserted = ObjHelper.ExecuteNonQuery("Usp_AddDentistForRewardPlatForm", sqlpara);
                return IsInserted;

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--AddUserForRewardPlatForm", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable GetDentistListForRewardUser(Reward_DentistList Obj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ ParameterName="@RewardPartnerId",Value=Obj.RewardPartnerId}
                };
                return new clsHelper().DataTable("Usp_GetDentistListForReward", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--GetDentistListForRewardUser", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int CreateRewardsForPatientSolutionOne(CreateRewards obj, int source, int type)
        {
            try
            {
                bool IsInserted = false;
                int PatientRewardId = 0;
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter {ParameterName = "@RewardDate", Value= Convert.ToDateTime(obj.RewardDate)},
                    new SqlParameter {ParameterName = "@RewardPartnerId", Value=obj.RewardPartnerId},
                    new SqlParameter {ParameterName = "@RewardTypeCode", Value=obj.RewardTypeCode},
                    new SqlParameter {ParameterName = "@PatientRewardId", Direction=ParameterDirection.Output,SqlDbType=SqlDbType.Int},
                    new SqlParameter {ParameterName = "@Source", Value=source},
                    new SqlParameter {ParameterName = "@RewardType", Value=type},
                };
                string _storedProcName = ObjCommon.GetStoredProcName((int)obj.sourcetype, "Usp_CreateRewardsForPatientSolutionOne", "Usp_Sup_CreateReward");
                IsInserted = ObjHelper.ExecuteNonQuery(_storedProcName, sqlpara);
                return PatientRewardId = Convert.ToInt32(sqlpara[3].Value);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--CreateRewardsForPatientSolutionOne", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool CheckRewardtype(CreateRewards obj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter {ParameterName = "@RewardTypeCode", Value=obj.RewardTypeCode},
                };
                var retVal = ObjHelper.ExecuteScalar("Usp_CheckRewardType", sqlpara);
                if (retVal != "0")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--CheckRewardtype", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        public static DataTable GetRewardDetailsByRewardId(int RewardId, int SourceType)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@RewardId", SqlDbType.Int);
                strParameter[0].Value = RewardId;
                string _storedProcName = ObjCommon.GetStoredProcName(SourceType, "USP_GetRewardDetailsByRewardId", "Usp_Sup_GetRewardDetailsById");
                dt = ObjHelper.DataTable(_storedProcName, strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--GetRewardDetailsByRewardId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataSet GetRewardsList(Reward_RewardsCall Obj)
        {
            try
            {
                string _storedProcName = string.Empty;
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@RewardPartnerId",Value=Obj.RewardPartnerId},
                    new SqlParameter{ParameterName="@AccountId",Value=Obj.AccountId},
                    new SqlParameter{ParameterName="@Pageindex",Value=Obj.Pageindex},
                    new SqlParameter{ParameterName="@PageSize",Value=Obj.PageSize},
                    new SqlParameter{ParameterName="@Startdate",Value=Obj.Startdate},
                    new SqlParameter{ParameterName="@Enddate",Value=Obj.Enddate},
                    new SqlParameter{ParameterName="@RewardType",Value=Obj.RewardType}
                };
                switch (Obj.sourcetype)
                {
                    case APIFilter.FilterFlag.IsRewardPlatform:
                        _storedProcName = "Usp_GetRewardsDetials";
                        break;

                    case APIFilter.FilterFlag.IsSuperBucks:
                        _storedProcName = "Usp_SUP_GetPatientRewardsDetails";
                        break;
                }
                return new clsHelper().GetDatasetData(_storedProcName, sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--GetRewardsList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int GetPatientIdFromRewardPartnerId(Reward_UploadProfileImage Obj)
        {
            try
            {
                int PatientId = 0;
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@RewardPartnerId",Value=Obj.RewardPartnerId},
                };
                string _storedProcName = ObjCommon.GetStoredProcName((int)Obj.sourcetype, "USP_GetPatientIdOFRewardPartnerId", "USP_SUP_GetPatientIdOfSuperDentist");
                DataTable dt = new clsHelper().DataTable(_storedProcName, sqlpara);
                if (dt != null && dt.Rows.Count > 0)
                {
                    PatientId = Convert.ToInt32(dt.Rows[0]["PatientId"]);
                }
                return PatientId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--GetPatientIdFromRewardPartnerId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable CheckDentrixConnectorExistOrNot(string DentrixConnectorKey)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@DentrixConnectorKey",Value=DentrixConnectorKey}
                };
                DataTable dt = new clsHelper().DataTable("Usp_CheckDentrixConnectorKey", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--CheckDentrixConnectorExistOrNot", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int GetRewardAmountBalance(RewardBalanceJson obj)
        {
            try
            {
                int TotalRewards = 0;
                string _storedProcName = ObjCommon.GetStoredProcName((int)obj.sourcetype, "USP_GetPatientRewardBalance", "USP_SUP_GetPatientRewardBalance");
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@RewardPartnerId",Value=obj.RewardPartnerId},
                    new SqlParameter{ParameterName="@RewardType",Value=Convert.ToInt32(obj.RewardType)},
                };
                switch (obj.sourcetype)
                {
                    case APIFilter.FilterFlag.IsRewardPlatform:
                        _storedProcName = "USP_GetPatientRewardBalance";
                        break;

                    case APIFilter.FilterFlag.IsSuperBucks:
                        _storedProcName = "USP_SUP_GetPatientRewardBalance";
                        break;
                }
                DataTable dt = new clsHelper().DataTable(_storedProcName, sqlpara);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (string.IsNullOrEmpty(dt.Rows[0]["RewardBalance"].ToString()))
                    {
                        TotalRewards = 0;
                    }
                    else
                    {
                        TotalRewards = Convert.ToInt32(dt.Rows[0]["RewardBalance"]);
                    }

                }
                return TotalRewards;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--GetRewardAmountBalance", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int InsertPatientReferral(PatientReferral Obj)
        {
            try
            {
                string _storedProcName = ObjCommon.GetStoredProcName((int)Obj.sourcetype, "Usp_InsertTempPatientForReferral", "Usp_SUP_InsertTempPatientForReferral");
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName = "@RewardPertnerId",Value=Obj.RewardPartnerId},
                    new SqlParameter{ParameterName = "@FirstName",Value=Obj.FirstName},
                    new SqlParameter{ParameterName = "@LastName",Value=Obj.LastName},
                    new SqlParameter{ParameterName = "@Email",Value=Obj.Email},
                    new SqlParameter{ParameterName = "@Phone",Value=Obj.phone},
                    new SqlParameter{ParameterName = "@DentistId",Value=Obj.DentistId},
                    new SqlParameter{ParameterName = "@AccountId",Value=Obj.AccountId},
                    new SqlParameter{ParameterName = "@oOUTPUT",Direction =ParameterDirection.Output,SqlDbType=SqlDbType.Int},
                    new SqlParameter{ParameterName = "@Source",Value=Obj.Source},
                };
                bool result = new clsHelper().ExecuteNonQuery(_storedProcName, sqlpara);
                return Convert.ToInt32(sqlpara[7].Value);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--InsertPatientReferral", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool RecordlincPMSAssociation(PatientAssociation Obj)
        {
            try
            {
                string _storedProcName = ObjCommon.GetStoredProcName((int)Obj.sourcetype, "Usp_InsertRecordlincPMSAssociation", "Usp_SUP_InsertPatientAssociation");
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@RewardPartnerId",Value=Obj.RewardPartnerId},
                    new SqlParameter{ParameterName="@PatientId",Value=Obj.PatientId},
                    new SqlParameter{ParameterName="@AccountId",Value=Obj.AccountId},
                    new SqlParameter{ParameterName = "@oOutPut",Direction =ParameterDirection.Output,SqlDbType=SqlDbType.Int},
                };
                switch (Obj.sourcetype)
                {
                    case APIFilter.FilterFlag.IsRewardPlatform:
                        _storedProcName = "Usp_InsertRecordlincPMSAssociation";
                        break;

                    case APIFilter.FilterFlag.IsSuperBucks:
                        _storedProcName = "Usp_SUP_InsertPatientAssociation";
                        break;
                }
                return new clsHelper().ExecuteNonQuery(_storedProcName, sqlpara);

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--RecordlincPMSAssociation", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable CheckRewardPartnerIdassociation(PatientAssociation Obj)
        {
            try
            {
                string _storedProcName = ObjCommon.GetStoredProcName((int)Obj.sourcetype, "Usp_CheckAssociatePatientId", "Usp_SUP_CheckAssociatePatientId");
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@RewardPartnerId",Value=Obj.RewardPartnerId},
                    new SqlParameter{ParameterName="@PatientId",Value=Obj.PatientId},
                    new SqlParameter{ParameterName="@AccountId",Value=Obj.AccountId},
                };
                DataTable dt = new clsHelper().DataTable(_storedProcName, sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--CheckRewardPartnerIdassociation", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool DeletePatientProcedure(RemoveProcedure Obj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@ProcId",Value=Obj.DentrixId},
                    new SqlParameter{ParameterName="@DentrixConnectorId",Value=Obj.DentrixConnectorId},
                    new SqlParameter{ParameterName="@Ooutput",Direction=ParameterDirection.Output,SqlDbType=SqlDbType.Int}
                };
                new clsHelper().ExecuteNonQuery("Usp_DeleteDentrixProcedureOfPatient", sqlpara);
                int Ooutput = Convert.ToInt32(sqlpara[2].Value);
                return (Ooutput == 0) ? true : false;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--DeletePatientProcedure", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool checkPatientPartnerID(CreateRewards obj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter {ParameterName = "@RewardPartnerId", Value=obj.RewardPartnerId},
                    new SqlParameter {ParameterName = "@RewardTypeCode", Value=obj.RewardTypeCode},
                };
                string _storedProcName = ObjCommon.GetStoredProcName((int)obj.sourcetype, "Usp_checkPatientPartnerID", "Usp_Sup_CheckPatientAlreadyRegister");
                var retVal = ObjHelper.ExecuteScalar(_storedProcName, sqlpara);
                if (Convert.ToInt32(retVal) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--checkPatientPartnerID", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        public static bool PatientAssociationwithRewardPlatform(int PatientId, int sourcetype)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@PatientId",Value=PatientId},
                    new SqlParameter{ParameterName="@Ooutput",Direction=ParameterDirection.Output,SqlDbType = SqlDbType.Bit}
                };
                string _storedprocName = ObjCommon.GetStoredProcName(sourcetype, "Usp_CheckPatientHasRewardPartnerId", "Usp_CheckPatientHasSupPatientId");
                new clsHelper().ExecuteNonQuery(_storedprocName, sqlpara);

                return Convert.ToBoolean(sqlpara[1].Value);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--checkPatientPartnerID", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetPateintBySolutionOneId(PatientSolOne Obj)
        {
            DataTable Dt = new DataTable();
            SqlParameter[] sqlpara = new SqlParameter[1];
            sqlpara[0] = new SqlParameter("@RewardsPlatformID", SqlDbType.Int);
            sqlpara[0].Value = Obj.RewardsPlatformID;
            string _storedProcName = ObjCommon.GetStoredProcName((int)Obj.sourcetype, "Usp_GetPateintBySolutionOneId", "Usp_Sup_GetPateintBySupId");
            Dt = ObjHelper.DataTable(_storedProcName, sqlpara);
            return Dt;
        }
        public static int InsertUpdatePatientAppointment(Appointments objApp)
        {
            try
            {
                int PatientInfo = 1;
                string fax = string.Empty; string AppstartTime = string.Empty;
                string home = string.Empty;
                string mobile = string.Empty;
                string work = string.Empty;
                string street1 = string.Empty;
                string street2 = string.Empty;
                string city = string.Empty;
                string state = string.Empty;
                string zipcode = string.Empty;
                string email = string.Empty;
                string Fname = string.Empty;
                string Lname = string.Empty;
                if (objApp.NewPatientInfo != null)
                {
                    if (!string.IsNullOrWhiteSpace(objApp.NewPatientInfo.PatientName))
                    {
                        // if patient name has only one word that time  error occur array out of bound.
                        var name = objApp.NewPatientInfo.PatientName.Split(',');
                        Fname = name[1];
                        Lname = name[0];
                    }
                    if (objApp.NewPatientInfo.Addresses != null)
                    {
                        foreach (var items in objApp.NewPatientInfo.Addresses)
                        {
                            street1 = items.Street1;
                            street2 = items.Street2;
                            city = items.City;
                            state = items.State;
                            zipcode = items.ZipCode;
                        }
                    }
                    if (objApp.NewPatientInfo.EmailAdresses != null)
                    {
                        foreach (var itememail in objApp.NewPatientInfo.EmailAdresses)
                        {
                            email = itememail;
                        }
                    }
                    if (objApp.NewPatientInfo.phonenumbers != null)
                    {
                        foreach (var item in objApp.NewPatientInfo.phonenumbers)
                        {
                            if (item.PhoneType == 0)//Home
                            {
                                home = item.PhoneNumber;
                            }
                            if (item.PhoneType == 1)//Mobile
                            {
                                mobile = item.PhoneNumber;
                            }
                            if (item.PhoneType == 2)//Work
                            {
                                work = item.PhoneNumber;
                            }
                            if (item.PhoneType == 3)//Fax
                            {
                                fax = item.PhoneNumber;
                            }
                        }
                    }
                }
                else
                {
                    PatientInfo = 0;
                }
                if (Convert.ToDateTime(objApp.automodifiedtimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    objApp.automodifiedtimestamp = null;
                }
                if (objApp.AppointmentStart != null)
                {
                    AppstartTime = objApp.AppointmentStart.ToLongTimeString();
                }

                if (Convert.ToDateTime(objApp.AppointmentDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    DateTime sqlMinDateAsNetDateTime = (DateTime)SqlDateTime.MinValue;
                    objApp.AppointmentDate = sqlMinDateAsNetDateTime.AddYears(1);
                }
                if (Convert.ToDateTime(objApp.BrokenDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    objApp.BrokenDate = null;
                }

                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@InPatientId",Value=objApp.PatientId},
                    new SqlParameter{ParameterName="@DentrixAppointmentId",Value=objApp.AppointmentId},
                    new SqlParameter{ParameterName="@Appointemntdate",Value=objApp.AppointmentDate},
                    new SqlParameter{ParameterName="@AppointmentNote",Value=objApp.AppointmentReason},
                    new SqlParameter{ParameterName="@ModificationDate",Value=DateTime.Now},
                    new SqlParameter{ParameterName="@AppointmentTime",Value=AppstartTime},
                    new SqlParameter{ParameterName="@AppointmentLength",Value=objApp.AppointmentLength},
                    new SqlParameter{ParameterName="@AutoModifiedTimeStamp",Value=objApp.automodifiedtimestamp},
                    new SqlParameter{ParameterName="@DentrixConnectorKey",Value=objApp.dentrixConnectorID},
                    new SqlParameter{ParameterName="@ProviderId",Value=objApp.ProviderId},
                    new SqlParameter{ParameterName="@Operatory",Value=objApp.OperatoryId},
                    new SqlParameter{ParameterName="@Amount",Value=objApp.Amount},
                    new SqlParameter{ParameterName="@ServiceType",Value=null},
                    new SqlParameter{ParameterName="@PatientFName",Value=Fname},
                    new SqlParameter{ParameterName="@PatientLName",Value=Lname},
                    new SqlParameter{ParameterName="@Street1",Value=street1 },
                    new SqlParameter{ParameterName="@Street2",Value=street2 },
                    new SqlParameter{ParameterName="@City",Value=city},
                    new SqlParameter{ParameterName="@State",Value=state},
                    new SqlParameter{ParameterName="@ZipCode",Value=zipcode},
                    new SqlParameter{ParameterName="@Phone",Value=home},
                    new SqlParameter{ParameterName="@Fax",Value=fax},
                    new SqlParameter{ParameterName="@Mobile",Value=mobile},
                    new SqlParameter{ParameterName="@Work",Value=work},
                    new SqlParameter{ParameterName="@Email",Value=email},
                    new SqlParameter{ParameterName="@Output",Direction=ParameterDirection.Output,SqlDbType = SqlDbType.Int},
                    new SqlParameter{ParameterName="@StaffId",Value=objApp.StaffId},
                    new SqlParameter{ParameterName="@AmountOverriden",Value=objApp.AmountOverriden},
                    new SqlParameter{ParameterName="@BrokenDate",Value=objApp.BrokenDate},
                    new SqlParameter{ParameterName="@IsPatientInfo",Value=PatientInfo}
                };
                new clsHelper().ExecuteNonQuery("Usp_DentrixAppointmentInsertAndUpdate", sqlpara);
                return Convert.ToInt32(sqlpara[25].Value);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--Usp_DentrixAppointmentInsertAndUpdate", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool InsertDataAppointMentMapping(int AppId, List<int> obj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@AppointmentId",Value=AppId},
                };
                new clsHelper().ExecuteNonQuery("Usp_DentrixDeleteAppointmentMappingProcedure", sqlpara);

                foreach (var item in obj)
                {
                    SqlParameter[] sqlparas = new SqlParameter[]
                    {
                        new SqlParameter{ParameterName="@ProcID",Value=item},
                        new SqlParameter{ParameterName="@AppointmentId",Value=AppId},
                    };
                    new clsHelper().ExecuteNonQuery("Usp_DentrixAppointmentMappingProcedure", sqlparas);
                }
                return true;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--Usp_DentrixAppointmentInsertAndUpdate", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool DeletePatientAppointment(RemoveAppointment Obj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@AppointmentId",Value=Obj.DentrixId},
                    new SqlParameter{ParameterName="@DentrixConnectorId",Value=Obj.DentrixConnectorId},
                    new SqlParameter{ParameterName="@Ooutput",Direction=ParameterDirection.Output,SqlDbType = SqlDbType.Int}
                };
                new clsHelper().ExecuteNonQuery("Usp_DeletePatientAppointmentOfDentrix", sqlpara);
                return (Convert.ToInt32(sqlpara[2].Value) == 0) ? false : true;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--DeletePatientAppointment", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int AddPatientAddressDetails(DentrixPatient Obj, int PatientId)
        {
            try
            {
                string SecondaryEmail = string.Empty;
                string home = string.Empty;
                string mobile = string.Empty;
                if (Obj.PhoneNumbers != null)
                {
                    foreach (var item in Obj.PhoneNumbers)
                    {
                        if (item.PhoneType == 0)//Home
                        {
                            home = item.PhoneNumber;
                        }
                        if (item.PhoneType == 1)//Mobile
                        {
                            mobile = item.PhoneNumber;
                        }
                    }
                }
                if (Obj.Emails != null)
                {
                    if (Obj.Emails.Count > 0)
                    {
                        SecondaryEmail = Obj.Emails.Last();
                    }
                }
                if (Obj.Addresses != null)
                {
                    foreach (var item in Obj.Addresses)
                    {
                        if (!string.IsNullOrWhiteSpace(item.DentrixId))
                        {
                            SqlParameter[] sqlpara = new SqlParameter[]
                            {
                                new SqlParameter{ParameterName="@Street1",Value=(!string.IsNullOrWhiteSpace(item.Street1)) ? item.Street1.Trim() : string.Empty},
                                new SqlParameter{ParameterName="@Street2",Value=(!string.IsNullOrWhiteSpace(item.Street2)) ? item.Street2.Trim() : string.Empty},
                                new SqlParameter{ParameterName="@City",Value=(!string.IsNullOrWhiteSpace(item.City)) ? item.City.Trim() : string.Empty},
                                new SqlParameter{ParameterName="@State",Value=(!string.IsNullOrWhiteSpace(item.State)) ? item.State.Trim() : string.Empty},
                                new SqlParameter{ParameterName="@ZipCode",Value=(!string.IsNullOrWhiteSpace(item.ZipCode)) ? item.ZipCode.Trim() : string.Empty},
                                new SqlParameter {ParameterName="@Mobile",Value=(!string.IsNullOrWhiteSpace(mobile)) ? mobile.Trim() : string.Empty},
                                new SqlParameter {ParameterName="@HomePhone",Value=(!string.IsNullOrWhiteSpace(home)) ? home.Trim() : string.Empty},
                                new SqlParameter {ParameterName="@Email",Value=(!string.IsNullOrWhiteSpace(SecondaryEmail)) ? SecondaryEmail.Trim() : string.Empty},
                                new SqlParameter{ParameterName="@PatientId",Value=PatientId},
                                new SqlParameter{ParameterName="@IdNumber",Value=(!string.IsNullOrWhiteSpace(item.DentrixId)) ? item.DentrixId.Trim() : string.Empty}
                            };
                            bool result = new clsHelper().ExecuteNonQuery("Usp_AddPatientContactDetails", sqlpara);
                            return (result) ? 0 : 1;
                        }
                        else
                        {
                            return 1;
                        }
                    }
                }
                else
                {
                    return 0;
                }
                return 0;
            }
            catch (Exception Ex)
            {

                new clsCommon().InsertErrorLog("clsPatientData--AddPatientAddressDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int DeletePatientFromDentrix(RemovePatient Obj)
        {
            try
            {
                int ooutput = 0;
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@PatId",Value=Obj.DentrixId},
                    new SqlParameter{ParameterName="@DentrixConnectorId",Value=Obj.dentrixConnectorId},
                    new SqlParameter{ParameterName="@Ooutput",Direction = ParameterDirection.Output,SqlDbType=SqlDbType.Int}
                };
                new clsHelper().ExecuteNonQuery("Usp_DeletePatientFromDentrix", sqlpara);
                return ooutput = Convert.ToInt32(sqlpara[2].Value);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--AddPatientAddressDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int InsertAppointment_DentrixDetails(Appointments Obj)
        {
            try
            {
                string txproc = string.Empty; string adacode = string.Empty;
                if (Convert.ToDateTime(Obj.automodifiedtimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    Obj.automodifiedtimestamp = null;
                }
                if (Convert.ToDateTime(Obj.AppointmentDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    DateTime sqlMinDateAsNetDateTime = (DateTime)SqlDateTime.MinValue;
                    Obj.AppointmentDate = sqlMinDateAsNetDateTime.AddYears(1);
                }
                if (Convert.ToDateTime(Obj.BrokenDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    Obj.BrokenDate = null;
                }
                if (Obj.TxProcs != null && Obj.TxProcs.Count > 0)
                {
                    txproc = string.Join(",", Obj.TxProcs.ToArray());
                }
                if (Obj.AdaCodes != null && Obj.AdaCodes.Count > 0)
                {
                    adacode = string.Join(",", Obj.AdaCodes.ToArray());
                }
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@DentrixApptmentId",Value=Obj.AppointmentId},
                    new SqlParameter{ParameterName="@AppointmentDate",Value=Obj.AppointmentDate},
                    new SqlParameter{ParameterName="@AppointmentLenght",Value=Obj.AppointmentLength},
                    new SqlParameter{ParameterName="@OperatoryId",Value=Obj.OperatoryId},
                    new SqlParameter{ParameterName="@ProviderId",Value=Obj.ProviderId},
                    new SqlParameter{ParameterName="@StaffId",Value=Obj.StaffId},
                    new SqlParameter{ParameterName="@AmountOverriden",Value=Obj.AmountOverriden},
                    new SqlParameter{ParameterName="@Amount",Value=Obj.Amount},
                    new SqlParameter{ParameterName="@PatientId",Value=Obj.PatientId},
                    new SqlParameter{ParameterName="@AppointmentReason",Value=Obj.AppointmentReason},
                    new SqlParameter{ParameterName="@AdaCodes",Value=adacode},
                    new SqlParameter{ParameterName="@TxProcs",Value=txproc},
                    new SqlParameter{ParameterName="@Identifier",Value=Obj.DentrixId},
                    new SqlParameter{ParameterName="@automodifiedtimestamp",Value=Obj.automodifiedtimestamp},
                    new SqlParameter{ParameterName="@dentrixConnectorID",Value=Obj.dentrixConnectorID},
                    new SqlParameter{ParameterName="@HistoryId",Direction=ParameterDirection.Output,SqlDbType=SqlDbType.Int}
                };
                new clsHelper().ExecuteNonQuery("Usp_InsertDentrixAppointment", sqlpara);
                return Convert.ToInt32(sqlpara[15].Value);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--InsertAppointment_DentrixDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool DeleteDentrixAppointmentDetails(RemoveAppointment Obj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@ApptId",Value=Obj.DentrixId},
                    new SqlParameter{ParameterName="@DentrixConnectorId",Value=Obj.DentrixConnectorId}
                };
                return new clsHelper().ExecuteNonQuery("Usp_DeleteDentrixAppointmentDetails", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--DeletePatientAppointment", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Ankit here 
        /// Update date : 08-08-2018 
        /// Reason : added one more filed name IsFromSBI for when we receive a record form SBI side then we doesn't need to update values 
        /// of Patient table thats why I have added this condition on it.
        /// </summary>
        /// <param name="Obj"></param>
        /// <param name="RequestId"></param>
        /// <param name="IsFromSBI"></param>
        /// <returns></returns>
        public static bool DPMSResponseUpdate(List<ResponsedData> Obj, int RequestId, bool IsFromSBI = false)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@RequestId",Value=RequestId},
                    new SqlParameter{ParameterName="@ProcessQueue",Value=clsCommon.ToDataTable(Obj)},
                    new SqlParameter{ParameterName="@IsFromSBI",Value=IsFromSBI}
                };
                return new clsHelper().ExecuteNonQuery("USP_PatientProcessQueueUpdate", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--DPMSResponseUpdate", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataSet GetSupPatientRewardsList(Reward_PatientRewardCall Obj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@SupPatientId",Value=Obj.SupPatientId},
                    new SqlParameter{ParameterName="@AccountID",Value=Obj.AccountId},
                    new SqlParameter{ParameterName="@Pageindex",Value=Obj.Pageindex},
                    new SqlParameter{ParameterName="@PageSize",Value=Obj.PageSize},
                    new SqlParameter{ParameterName="@Startdate",Value=Obj.Startdate},
                    new SqlParameter{ParameterName="@Enddate",Value=Obj.Enddate},
                    new SqlParameter{ParameterName="@RewardType",Value=Obj.RewardType}
                };
                return new clsHelper().GetDatasetData("Usp_SUP_GetPatientRewardsDetails", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--GetSupPatientRewardsList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable SUP_GetPatientDetailsOfDoctor(BO.Models.PatientFilter FilterObj)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[11];
                strParameter[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                strParameter[0].Value = FilterObj.UserId;
                strParameter[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[1].Value = (FilterObj.PageIndex) == 0 ? 1 : FilterObj.PageIndex;
                strParameter[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[2].Value = (FilterObj.PageSize) == 0 ? 20 : FilterObj.PageSize;
                strParameter[3] = new SqlParameter("@FirstName", SqlDbType.NVarChar);
                strParameter[3].Value = FilterObj.FirstName;
                strParameter[4] = new SqlParameter("@LastName", SqlDbType.NVarChar);
                strParameter[4].Value = FilterObj.Lastname;
                strParameter[5] = new SqlParameter("@Email", SqlDbType.NVarChar);
                strParameter[5].Value = FilterObj.Email;
                strParameter[6] = new SqlParameter("@Phone", SqlDbType.NVarChar);
                strParameter[6].Value = FilterObj.Phone;
                strParameter[7] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[7].Value = FilterObj.SortColumn;
                strParameter[8] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[8].Value = FilterObj.SortDirection;
                strParameter[9] = new SqlParameter("@ExcludeFamilyMembers", SqlDbType.Bit);
                strParameter[9].Value = Convert.ToBoolean(FilterObj.ExcludeFamilyMembers);
                strParameter[10] = new SqlParameter("@DateofBirth", SqlDbType.DateTime);
                strParameter[10].Value = FilterObj.DateOfBirth;

                dt = ObjHelper.DataTable("Usp_Sup_PatientDetailsOfDoctor", strParameter);

                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public DataTable GetPatientList(PatientFilter FilterObj)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[12];
                strParameter[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                strParameter[0].Value = FilterObj.UserId;
                strParameter[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[1].Value = (FilterObj.PageIndex) == 0 ? 1 : FilterObj.PageIndex;
                strParameter[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[2].Value = (FilterObj.PageSize) == 0 ? 1500 : FilterObj.PageSize;
                strParameter[3] = new SqlParameter("@FirstName", SqlDbType.NVarChar);
                strParameter[3].Value = FilterObj.FirstName;
                strParameter[4] = new SqlParameter("@LastName", SqlDbType.NVarChar);
                strParameter[4].Value = FilterObj.Lastname;
                strParameter[5] = new SqlParameter("@Email", SqlDbType.NVarChar);
                strParameter[5].Value = FilterObj.Email;
                strParameter[6] = new SqlParameter("@Phone", SqlDbType.NVarChar);
                strParameter[6].Value = FilterObj.Phone;
                strParameter[7] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[7].Value = FilterObj.SortColumn;
                strParameter[8] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[8].Value = FilterObj.SortDirection;
                strParameter[9] = new SqlParameter("@LocationId", SqlDbType.Int);
                strParameter[9].Value = FilterObj.LocationId;
                strParameter[10] = new SqlParameter("@FromDate", SqlDbType.DateTime);
                strParameter[10].Value = FilterObj.FromDate;
                strParameter[11] = new SqlParameter("@ToDate", SqlDbType.DateTime);
                strParameter[11].Value = FilterObj.ToDate;

                dt = ObjHelper.DataTable("USP_PatientList", strParameter);


                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static DataTable checkPatientExists(int RewardPartnerId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                   new SqlParameter{ParameterName="@RewardId",Value=RewardPartnerId}
                };
                DataTable dt = new clsHelper().DataTable("SUP_GetRewardTypeOfId", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--checkPatientExists", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable GetDentistsListOfAccount(int AccountId, int LocationId = 0)
        {
            SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter{ParameterName="@AccountId",Value=AccountId},
                new SqlParameter{ParameterName="@LocationId",Value=LocationId}
            };
            DataTable dt = new clsHelper().DataTable("Usp_GetDentistsIdFromAccount", sqlpara);
            return dt;
        }
        public static DataTable checkAcountLinked(int AccountId, int UserId = 0)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                   new SqlParameter{ParameterName="@AccountId",Value=AccountId},
                   new SqlParameter{ParameterName="@UserId",Value=UserId},
                };
                DataTable dt = new clsHelper().DataTable("GetLinkedAccountDetails", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--checkPatientExists", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        //Method For One click Referral
        public DataTable GetMedicalHistoryForOneClickreferral(int PatientID, string Operation)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientID", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;

                dtTable = ObjHelper.DataTable("Dental_MedicalHistoryForOneClickRefferal", strParameter);
                return dtTable;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetDentalHistoryForOneClickreferral(int PatientID, string Operation)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientID", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;

                dtTable = ObjHelper.DataTable("Get_DentalHistory", strParameter);
                return dtTable;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int AddPatientFromOneClickRefferal(PatinentInfo model, int ReferBy, string Phone, int ValLocation)
        {
            try
            {
                int NewPatientId = 0;
                bool Result = false;
                SqlParameter[] param = new SqlParameter[10];
                param[0] = new SqlParameter("@OwnerId", SqlDbType.Int);
                param[0].Value = model.UserId;
                param[1] = new SqlParameter("@FirstName", SqlDbType.NVarChar);
                param[1].Value = model.FirstName;
                param[2] = new SqlParameter("@LastName", SqlDbType.NVarChar);
                param[2].Value = model.LastName;
                param[3] = new SqlParameter("@Email", SqlDbType.NVarChar);
                param[3].Value = model.Email;
                param[4] = new SqlParameter("@Phone", SqlDbType.NVarChar);
                param[4].Value = Phone;
                param[5] = new SqlParameter("@Password", SqlDbType.NVarChar);
                param[5].Value = model.Password;
                param[6] = new SqlParameter("@PatientId", SqlDbType.Int);
                param[6].Value = model.PatientId;
                param[7] = new SqlParameter("@ReferBy", SqlDbType.Int);
                param[7].Value = ReferBy;
                param[8] = new SqlParameter("@LocationId", SqlDbType.Int);
                param[8].Value = ValLocation;
                param[9] = new SqlParameter("@NewPatientId", SqlDbType.Int);
                param[9].Direction = ParameterDirection.Output;

                Result = ObjHelper.ExecuteNonQuery("USP_PatientInsertAndUpdate_New", param);
                NewPatientId = Convert.ToInt32(param[9].Value);
                return NewPatientId;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clspatientdata--AddPatientFromOneClickRefferal", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool AddPatientContactInfoFromOneclick(PatientContactInfo _ConeactInfo, string Phone, string Email)
        {
            try
            {
                bool Result = false;
                SqlParameter[] param = new SqlParameter[9];
                param[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                param[0].Value = _ConeactInfo.PatientId;
                param[1] = new SqlParameter("@Address", SqlDbType.NVarChar);
                param[1].Value = _ConeactInfo.Address;
                param[2] = new SqlParameter("@City", SqlDbType.NVarChar);
                param[2].Value = _ConeactInfo.City;
                param[3] = new SqlParameter("@State", SqlDbType.NVarChar);
                param[3].Value = _ConeactInfo.State;
                param[4] = new SqlParameter("@ZipCode", SqlDbType.NVarChar);
                param[4].Value = _ConeactInfo.ZipCode;
                param[5] = new SqlParameter("@DOB", SqlDbType.Date);
                param[5].Value = _ConeactInfo.BOD;
                param[6] = new SqlParameter("@Gender", SqlDbType.Bit);
                param[6].Value = _ConeactInfo.Gender;
                param[7] = new SqlParameter("@EMGContactNo", SqlDbType.NVarChar);
                param[7].Value = Phone;
                param[8] = new SqlParameter("@Email", SqlDbType.NVarChar);
                param[8].Value = Email;

                return Result = ObjHelper.ExecuteNonQuery("InsertPatientContactInfo_New", param);
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clspatientdata--AddPatientContactInfoFromOneclick", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static string GetDoctorNameFormId(int UserId)
        {
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@UserId", SqlDbType.Int);
            param[0].Value = UserId;
            string DoctorName = ObjHelper.ExecuteScalar("USP_GetDoctorNameFromId", param);
            return DoctorName;
        }
        //This creates referral header/message header. box matrix handler added here.
        public int InsertReferral(string Subject, string Body, int MessageType, int ReceiverId, int PatientId, string RegardOption, string RequestingOption, int ReferStatus, int SenderId, string XMLstring, bool IsOneClickRefferal, int LocationId, string Comment, string imageid, string documentid, int ItHasInsuranceData = 0)
        {

            SqlParameter[] param = new SqlParameter[16];
            param[0] = new SqlParameter("@Subject", SqlDbType.NVarChar);
            param[0].Value = Subject;
            param[1] = new SqlParameter("@Body", SqlDbType.NVarChar);
            param[1].Value = Body;
            param[2] = new SqlParameter("@MessageType", SqlDbType.Int);
            param[2].Value = MessageType;
            param[3] = new SqlParameter("@SenderId", SqlDbType.Int);
            param[3].Value = SenderId;
            param[4] = new SqlParameter("@PatientId", SqlDbType.Int);
            param[4].Value = PatientId;
            param[5] = new SqlParameter("@pRegardOption", SqlDbType.NVarChar);
            param[5].Value = RegardOption;
            param[6] = new SqlParameter("@pRequestingOption", SqlDbType.NVarChar);
            param[6].Value = RequestingOption;
            param[7] = new SqlParameter("@pReferralStatus", SqlDbType.Int);
            param[7].Value = ReferStatus;
            param[8] = new SqlParameter("@RefferedUserId", SqlDbType.Int);
            param[8].Value = ReceiverId;
            param[9] = new SqlParameter("@XMLString", SqlDbType.Xml);
            param[9].Value = XMLstring;
            param[10] = new SqlParameter("@IsOneClickRefferal", SqlDbType.Bit);
            param[10].Value = IsOneClickRefferal;
            param[11] = new SqlParameter("@oReferralCardId", SqlDbType.Int);
            param[11].Direction = ParameterDirection.Output;
            param[12] = new SqlParameter("@oMessageId", SqlDbType.Int);
            param[12].Direction = ParameterDirection.Output;
            param[13] = new SqlParameter("@LocationId", SqlDbType.Int);
            param[13].Value = LocationId;
            param[14] = new SqlParameter("@pComments", SqlDbType.NVarChar);
            param[14].Value = Comment;
            param[15] = new SqlParameter("@ItHasInsuranceData", SqlDbType.Int);
            param[15].Value = ItHasInsuranceData;

            bool status = ObjHelper.ExecuteNonQuery("USP_InsertReferralFromOneClickReferral", param);
            int[] referralids = new int[2];
            int referidnew = Convert.ToInt32(param[11].Value);
            referralids[0] = referidnew;
            referralids[1] = Convert.ToInt32(param[12].Value);

            //link message referral id with box folder id
            if (!string.IsNullOrEmpty(BoxFolderId))
            {
                _boxFileHandler = new clsPatientFileData(new Schema.PatientContext(ConfigurationManager.ConnectionStrings["CONNECTIONSTRING"].ConnectionString));
                _boxFileHandler.CreateMatrix(new FileModel()
                {
                    FolderId = BoxFolderId,
                    RecordId = referralids[1],
                    PatientId = PatientId
                });
            }

            if (referidnew > 0 && !string.IsNullOrEmpty(imageid))
            {
                string[] arryimage = imageid.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < arryimage.Count(); i++)
                {
                    new clsColleaguesData().InsertReferral_Image_Refer(referidnew, Convert.ToInt32(arryimage[i]));
                }
            }

            if (referidnew > 0 && !string.IsNullOrEmpty(documentid))
            {
                string[] arrydocument = documentid.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < arrydocument.Count(); i++)
                {
                    new clsColleaguesData().InsertReferral_Document_Refer(referidnew, Convert.ToInt32(arrydocument[i]));
                }
            }
            new clsColleaguesData().AddPatientToReferredDoctor(ReceiverId, PatientId);
            new clsColleaguesData().AddPatientToReferredDoctor(SenderId, PatientId);
            int intResult = 0;
            intResult = Convert.ToInt32(referralids[1]);
            return intResult;
        }


        public DataTable GetOneClickReferralHistoryById(ReferralFilter FilterObj, int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@UserId",Value=UserId},
                    new SqlParameter{ParameterName="@PageIndex",Value=FilterObj.PageIndex},
                    new SqlParameter{ParameterName="@PageSize",Value=FilterObj.PageSize},
                    new SqlParameter{ParameterName="@SortColumn",Value=FilterObj.SortColumn},
                    new SqlParameter{ParameterName="@SortDirection",Value=FilterObj.SortDirection},
                    new SqlParameter{ParameterName="@DoctorName",Value=FilterObj.DoctorName},
                    new SqlParameter{ParameterName="@FromDate",Value=FilterObj.FromDate},
                    new SqlParameter{ParameterName="@Todate",Value=FilterObj.ToDate},
                    new SqlParameter{ParameterName="@Disposition",Value=FilterObj.Disposition},
                    new SqlParameter{ParameterName="@Message",Value=FilterObj.Message},
                    new SqlParameter{ParameterName="@MessageStatus",Value=FilterObj.MessageStatus},
                    new SqlParameter{ParameterName="@PatietName",Value=FilterObj.PatientName}
                };
                DataTable dt = new clsHelper().DataTable("USP_GetOneClickReferralHistoryByUserId", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--GetOneClickReferralHistoryById", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetOneClickReferralHistory(ReferralFilter FilterObj, int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@UserId",Value=UserId},
                    new SqlParameter{ParameterName="@PageIndex",Value=FilterObj.PageIndex},
                    new SqlParameter{ParameterName="@PageSize",Value=FilterObj.PageSize},
                    new SqlParameter{ParameterName="@SortColumn",Value=FilterObj.SortColumn},
                    new SqlParameter{ParameterName="@SortDirection",Value=FilterObj.SortDirection},
                    new SqlParameter{ParameterName="@DoctorName",Value=FilterObj.DoctorName},
                    new SqlParameter{ParameterName="@FromDate",Value=FilterObj.FromDate},
                    new SqlParameter{ParameterName="@Todate",Value=FilterObj.ToDate},
                    new SqlParameter{ParameterName="@Disposition",Value=FilterObj.Disposition},
                    new SqlParameter{ParameterName="@Message",Value=FilterObj.Message},
                    new SqlParameter{ParameterName="@MessageStatus",Value=FilterObj.MessageStatus},
                    new SqlParameter{ParameterName="@PatietName",Value=FilterObj.PatientName},
                    new SqlParameter{ParameterName="@MessageFrom",Value=FilterObj.ReferralFrom},
                };
                DataTable dt = new clsHelper().DataTable("USP_GetOneClickReferralHistoryByUserId_NEW", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--GetOneClickReferralHistory", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetDispositionlist()
        {
            try
            {
                DataTable dt = new clsHelper().DataTable("USP_GetDispositionlist");
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--GetDispositionlist", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
       
        public static int CheckPlanExistsOfUser(int RewardPartnerId, int PlanId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@RewardPartnerId",Value=RewardPartnerId},
                    new SqlParameter{ParameterName="@PlanID",Value=PlanId},
                    new SqlParameter{ParameterName="@Ooutput",Direction=ParameterDirection.Output,SqlDbType=SqlDbType.Int}
                };
                new clsHelper().ExecuteNonQuery("Usp_Sup_CheckAccountHasPlan", sqlpara);
                int Returnval = Convert.ToInt32(sqlpara[2].Value);
                return Returnval;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--CheckPlanExistsOfUser", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public int UpdateDispositionStatus(int Disposition, int MessageId, int UserId, string Note = null, bool VisibleToPatient = false, bool VisibleToDentist = true, DateTime? ScheduledDate = null)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@Disposition",Value=Disposition},
                    new SqlParameter{ParameterName="@MessageId",Value=MessageId},
                    new SqlParameter{ParameterName="@UserId",Value=UserId},
                    new SqlParameter{ParameterName="@Note",Value=Note},
                    new SqlParameter{ParameterName="@VisibleToPatient",Value=VisibleToPatient},
                    new SqlParameter{ParameterName="@VisibleToDoctor",Value=VisibleToDentist},
                    new SqlParameter{ParameterName="@ScheduledDate",Value=ScheduledDate},
                    new SqlParameter{ParameterName="@NewMessageId",Direction=ParameterDirection.Output,SqlDbType=SqlDbType.Int}
                };
                bool result = new clsHelper().ExecuteNonQuery("USP_UpdateDispositionInMessage", sqlpara);
                int NewMessageId = Convert.ToInt32(sqlpara[7].Value);
                return NewMessageId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--UpdateDispositionStatus", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetSearchByFilter(string DoctorName, DateTime? FromDate, DateTime? ToDate, int? Disposition, int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                new SqlParameter{ParameterName="@DoctorName",Value=DoctorName},
                  new SqlParameter{ParameterName="@FromDate",Value=FromDate},
                   new SqlParameter{ParameterName="@ToDate",Value=ToDate},
                   new SqlParameter{ParameterName="@Disposition",Value=Disposition},
                   new SqlParameter{ParameterName="@UserId",Value=UserId}
                };
                DataTable dt = new clsHelper().DataTable("USP_GetOneClickReferralHistoryByFilter", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--GetSearchByFilter", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataSet InsertPatient_DentrixAccountAgingDetails(List<Patient_DentrixAccountAgingModel> lstPatient_DentrixAccountAging)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@PatientDentrixAccountAgingList", SqlDbType.Structured);                strParameter[0].Value = clsCommon.ToDataTable(lstPatient_DentrixAccountAging);                ds = new clsHelper().GetDatasetData("USP_InsertPatientDentrixAccountAging", strParameter);                return ds;            }            catch (Exception)            {                throw;            }        }

        //Inser bulk in temp 
        public static DataSet InsertTempPatient_DentrixAccountAgingDetails(List<Patient_DentrixAccountAgingModel> lstPatient_DentrixAccountAging)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Temp_Patient_DentrixAccountAgingList", SqlDbType.Structured);
                strParameter[0].Value = clsCommon.ToDataTable(lstPatient_DentrixAccountAging);
                ds = ObjHelper.GetDatasetData("USP_InsertTemp_Patient_DentrixAccountAging", strParameter);

                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static DataTable GetAgingRecords(int PatientId, int UserId = 0)
        {
            SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter{ParameterName="@PatientId",Value=PatientId},
                new SqlParameter{ParameterName="@UserId",Value=UserId}
            };
            DataTable dt = new clsHelper().DataTable("USP_GetAgingRecords", sqlpara);
            return dt;
        }
        public static DataSet GetPatientPayments(string Type, int PatientId, DateTime? FromDate, DateTime? ToDate, string Order, int Finance, int Standard, int Adjustment, int Insurance, int PageIndex, int Count, int UserId = 0)
        {
            DataSet ds = new DataSet();

            SqlParameter[] strParameter = new SqlParameter[12];
            strParameter[0] = new SqlParameter("@Type", SqlDbType.VarChar);
            strParameter[0].Value = Type;
            strParameter[1] = new SqlParameter("@PatientId", SqlDbType.Int);
            strParameter[1].Value = PatientId;
            strParameter[2] = new SqlParameter("@FromDate", SqlDbType.DateTime);
            strParameter[2].Value = FromDate;
            strParameter[3] = new SqlParameter("@ToDate", SqlDbType.DateTime);
            strParameter[3].Value = ToDate;
            strParameter[4] = new SqlParameter("@Order", SqlDbType.VarChar);
            strParameter[4].Value = Order;
            strParameter[5] = new SqlParameter("@UserId", SqlDbType.Int);
            strParameter[5].Value = UserId;
            strParameter[6] = new SqlParameter("@FinanceRequestId", SqlDbType.Int);
            strParameter[6].Value = Finance;
            strParameter[7] = new SqlParameter("@StandardRequestId", SqlDbType.Int);
            strParameter[7].Value = Standard;
            strParameter[8] = new SqlParameter("@InsuranceRequestId", SqlDbType.Int);
            strParameter[8].Value = Insurance;
            strParameter[9] = new SqlParameter("@AdjustmentRequestId", SqlDbType.Int);
            strParameter[9].Value = Adjustment;
            strParameter[10] = new SqlParameter("@PageIndex", SqlDbType.Int);
            strParameter[10].Value = PageIndex;
            strParameter[11] = new SqlParameter("@Count", SqlDbType.Int);
            strParameter[11].Value = Count;

            ds = ObjHelper.GetDatasetData("USP_GetPatientPayments", strParameter);

            return ds;
        }
        public static int EditPatientReward(EditSupPatientReward model, int UserId, int AccountId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@RewardId",Value=model.RewardId},
                    new SqlParameter{ParameterName="@RewardType",Value=model.RewardType},
                    new SqlParameter{ParameterName="@Amount",Value=model.Amount},
                    new SqlParameter{ParameterName="@ProcedureCodeId",Value=model.ProcedureCodeId},
                    new SqlParameter{ParameterName="@UserId",Value=UserId},
                    new SqlParameter{ParameterName="@AccountId",Value=AccountId},
                };
                string result = new clsHelper().ExecuteScalar("Usp_Sup_EditPatientReward", sqlpara);
                return (!string.IsNullOrWhiteSpace(result)) ? Convert.ToInt32(result) : 0;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--EditPatientReward", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int AddPatientReward(AddPatientReward model, int UserId, int AccountId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@SupPatientId",Value=model.SupPatientId},
                    new SqlParameter{ParameterName="@RewardType",Value=model.RewardType},
                    new SqlParameter{ParameterName="@Amount",Value=model.Amount},
                    new SqlParameter{ParameterName="@ProcedureCodeId",Value=model.ProcedureCodeId},
                    new SqlParameter{ParameterName="@UserId",Value=UserId},
                    new SqlParameter{ParameterName="@AccountId",Value=AccountId},
                };
                string result = new clsHelper().ExecuteScalar("Usp_Sup_AddPatientReward", sqlpara);
                return (!string.IsNullOrWhiteSpace(result)) ? Convert.ToInt32(result) : 0;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--AddPatientReward", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int DeletePatientReward(int RewardId, int AccountId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@RewardId",Value=RewardId},
                    new SqlParameter{ParameterName="@AccountId",Value=AccountId}
                };
                string result = new clsHelper().ExecuteScalar("Usp_Sup_DeletePatientReward", sqlpara);
                return (!string.IsNullOrWhiteSpace(result)) ? Convert.ToInt32(result) : 0;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--DeletePatientReward", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static BulkResponse BulkInsertOfPatientProcedure(DataTable Dt)
        {
            try
            {
                BulkResponse bulk = new BulkResponse();
                bulk.ReceivedRecords = Dt.Rows.Count;
                bulk.TotalInsertedRecord = new clsHelper(IntegrationConnectionString).BulkInsert(Dt, "Temp_Patient_DentrixProcedure");
                bulk.TotalNotInsertedRecord = Dt.Rows.Count - bulk.TotalInsertedRecord;
                return bulk;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--BulkInsertOfPatientProcedure", Ex.Message, Ex.StackTrace);
                throw;
            }
        }


        public static BulkResponse BulkInsertOfPatient(DataTable Dt)
        {
            try
            {
                BulkResponse bulk = new BulkResponse();
                bulk.ReceivedRecords = Dt.Rows.Count;
                bulk.TotalInsertedRecord = new clsHelper().BulkInsert(Dt, "Temp_Patient");
                bulk.TotalNotInsertedRecord = Dt.Rows.Count - bulk.TotalInsertedRecord;
                return bulk;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--BulkInsertOfPatient", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static BulkResponse BulkInsertOfPatientDentrixAccountAging(DataTable Dt)
        {
            try
            {
                BulkResponse bulk = new BulkResponse();
                bulk.ReceivedRecords = Dt.Rows.Count;
                bulk.TotalInsertedRecord = new clsHelper(IntegrationConnectionString).BulkInsert(Dt, "Temp_Patient_DentrixAccountAging");
                bulk.TotalNotInsertedRecord = Dt.Rows.Count - bulk.TotalInsertedRecord;
                return bulk;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--BulkInsertOfPatientDentrixAccountAging", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static BulkResponse BulkInsertOfAppointments_DentrixDetails(DataTable Dt)
        {
            try
            {
                BulkResponse bulk = new BulkResponse();
                bulk.ReceivedRecords = Dt.Rows.Count;
                bulk.TotalInsertedRecord = new clsHelper(IntegrationConnectionString).BulkInsert(Dt, "Temp_Appointments_DentrixDetails");
                bulk.TotalNotInsertedRecord = Dt.Rows.Count - bulk.TotalInsertedRecord;
                return bulk;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--BulkInsertOfAppointments_DentrixDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Bulk insert Provider Schedule Data from the Dentrix
        /// </summary>
        /// <param name="Dt"></param>
        /// <returns></returns>
        public static BulkResponse BulkInsertProviderSchedule(DataTable Dt)
        {
            try
            {
                BulkResponse bulk = new BulkResponse();
                bulk.ReceivedRecords = Dt.Rows.Count;
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@DataTable", SqlDbType.Structured);
                strParameter[0].Value = Dt;
                new clsHelper().ExecuteNonQuery("Usp_InsertProviderSchedule", strParameter, 5 * 1000);
                bulk.TotalNotInsertedRecord = Dt.Rows.Count - bulk.TotalInsertedRecord;
                return bulk;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--BulkInsertProviderSchedule", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static BulkResponse BulkInsertOperatorySchedule(DataTable Dt)
        {
            try
            {
                BulkResponse bulk = new BulkResponse();
                bulk.ReceivedRecords = Dt.Rows.Count;
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@DataTable", SqlDbType.Structured);
                strParameter[0].Value = Dt;
                new clsHelper().ExecuteNonQuery("Usp_InsertOperatorySchedule", strParameter, 5 * 1000);
                bulk.TotalNotInsertedRecord = Dt.Rows.Count - bulk.TotalInsertedRecord;
                return bulk;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--BulkInsertOperatorySchedule", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static BulkResponse BulkInsertPracticeSchedule(DataTable Dt)
        {
            try
            {
                BulkResponse bulk = new BulkResponse();
                bulk.ReceivedRecords = Dt.Rows.Count;
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@DataTable", SqlDbType.Structured);
                strParameter[0].Value = Dt;
                new clsHelper().ExecuteNonQuery("Usp_InsertPracticeSchedule", strParameter, 5 * 1000);
                bulk.TotalNotInsertedRecord = Dt.Rows.Count - bulk.TotalInsertedRecord;
                return bulk;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--BulkInsertPracticeSchedule", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static RewardResponse GetNotInsertedRecordsListofProcedure(DataTable Dt)
        {
            RewardResponse retVal = new RewardResponse();
            try
            {
                DataSet dts = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@DataTable", SqlDbType.Structured);
                strParameter[0].Value = Dt;
                dts = new clsHelper(IntegrationConnectionString).GetDatasetData("Usp_GetNotInsertedRowOnTempProcedure", strParameter, 5 * 1000);
                retVal.ReceivedRecords = Dt.Rows.Count;
                if (dts.Tables.Count > 0)
                {
                    retVal.TotalInsertedRecord = dts.Tables[0].Rows.Count;

                    if (dts.Tables[0].Rows.Count > 0)
                    {
                        retVal.InsertedList = (from p in dts.Tables[0].AsEnumerable()
                                               select new InsertedRecord
                                               {
                                                   DentrixId = Convert.ToInt32(p["procid"]),
                                                   RLId = 0,
                                                   ProviderId = 0
                                               }
                                            ).ToList();
                    }
                    else
                    {
                        retVal.InsertedList = null;
                    }


                    if (retVal.InsertedList != null)
                    {
                        List<int> obj_insertedid = dts.Tables[0].AsEnumerable().Select(x => x.Field<int>("procid"))
                                                       .ToList();

                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  where !(obj_insertedid.Contains(Convert.ToInt32(p["procid"])))
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["procid"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                      ).ToList();
                    }
                    else
                    {
                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["procid"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                     ).ToList();
                    }


                }
                else
                {
                    retVal.TotalInsertedRecord = 0;
                    retVal.TotalNotInsertedRecord = 0;
                }


                return retVal;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--Usp_GetNotInsertedRowOnTempProcedure", Ex.Message, Ex.StackTrace);
                throw;
            }

        }



        public static RewardResponse GetNotInsertedRecordsListofPatients(DataTable Dt)
        {
            RewardResponse retVal = new RewardResponse();
            try
            {
                DataSet dts = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@DataTable", SqlDbType.Structured);
                strParameter[0].Value = Dt;
                dts = new clsHelper().GetDatasetData("Usp_GetNotInsertedRowOnTempPatients", strParameter, 5 * 1000);
                retVal.ReceivedRecords = Dt.Rows.Count;
                if (dts.Tables.Count > 0)
                {
                    retVal.TotalInsertedRecord = dts.Tables[0].Rows.Count;

                    if (dts.Tables[0].Rows.Count > 0)
                    {
                        retVal.InsertedList = (from p in dts.Tables[0].AsEnumerable()
                                               select new InsertedRecord
                                               {
                                                   DentrixId = Convert.ToInt32(p["patid"]),
                                                   RLId = 0,
                                                   ProviderId = 0
                                               }
                                            ).ToList();
                    }
                    else
                    {
                        retVal.InsertedList = null;
                    }


                    if (retVal.InsertedList != null)
                    {
                        List<int> obj_insertedid = dts.Tables[0].AsEnumerable().Select(x => x.Field<int>("patid"))
                                                       .ToList();

                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  where !(obj_insertedid.Contains(Convert.ToInt32(p["patid"])))
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["patid"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                      ).ToList();
                    }
                    else
                    {
                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["patid"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                     ).ToList();
                    }


                }
                else
                {
                    retVal.TotalInsertedRecord = 0;
                    retVal.TotalNotInsertedRecord = 0;
                }


                return retVal;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--GetNotInsertedRecordsListofPatients", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        public static RewardResponse GetNotInsertedRecordsListofAccountAging(DataTable Dt)
        {
            RewardResponse retVal = new RewardResponse();
            try
            {
                DataSet dts = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@DataTable", SqlDbType.Structured);
                strParameter[0].Value = Dt;
                dts = new clsHelper(IntegrationConnectionString).GetDatasetData("Usp_GetNotInsertedRowOnTempAccountAging", strParameter, 5 * 1000);
                retVal.ReceivedRecords = Dt.Rows.Count;
                if (dts.Tables.Count > 0)
                {
                    retVal.TotalInsertedRecord = dts.Tables[0].Rows.Count;

                    if (dts.Tables[0].Rows.Count > 0)
                    {
                        retVal.InsertedList = (from p in dts.Tables[0].AsEnumerable()
                                               select new InsertedRecord
                                               {
                                                   DentrixId = Convert.ToInt32(p["GuarId"]),
                                                   RLId = 0,
                                                   ProviderId = 0
                                               }
                                            ).ToList();
                    }
                    else
                    {
                        retVal.InsertedList = null;
                    }


                    if (retVal.InsertedList != null)
                    {
                        List<int> obj_insertedid = dts.Tables[0].AsEnumerable().Select(x => x.Field<int>("GuarId"))
                                                       .ToList();

                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  where !(obj_insertedid.Contains(Convert.ToInt32(p["GuarId"])))
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["GuarId"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                      ).ToList();
                    }
                    else
                    {
                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["GuarId"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                     ).ToList();
                    }
                }
                else
                {
                    retVal.TotalInsertedRecord = 0;
                    retVal.TotalNotInsertedRecord = 0;
                }
                return retVal;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--GetNotInsertedRecordsListofAccountAging", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        public static RewardResponse GetNotInsertedRecordsListofAppointmentsDentrixDetails(DataTable Dt)
        {
            RewardResponse retVal = new RewardResponse();
            try
            {
                DataSet dts = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@DataTable", SqlDbType.Structured);
                strParameter[0].Value = Dt;
                dts = new clsHelper(IntegrationConnectionString).GetDatasetData("Usp_GetNotInsertedRowOnTempAppointmentsDentrixDetails", strParameter, 5 * 1000);
                retVal.ReceivedRecords = Dt.Rows.Count;
                if (dts.Tables.Count > 0)
                {
                    retVal.TotalInsertedRecord = dts.Tables[0].Rows.Count;

                    if (dts.Tables[0].Rows.Count > 0)
                    {
                        retVal.InsertedList = (from p in dts.Tables[0].AsEnumerable()
                                               select new InsertedRecord
                                               {
                                                   DentrixId = Convert.ToInt32(p["AppointmentId"]),
                                                   RLId = 0,
                                                   ProviderId = 0
                                               }
                                            ).ToList();
                    }
                    else
                    {
                        retVal.InsertedList = null;
                    }


                    if (retVal.InsertedList != null)
                    {
                        List<int> obj_insertedid = dts.Tables[0].AsEnumerable().Select(x => x.Field<int>("AppointmentId"))
                                                       .ToList();

                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  where !(obj_insertedid.Contains(Convert.ToInt32(p["AppointmentId"])))
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["AppointmentId"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                      ).ToList();
                    }
                    else
                    {
                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["AppointmentId"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                     ).ToList();
                    }
                }
                else
                {
                    retVal.TotalInsertedRecord = 0;
                    retVal.TotalNotInsertedRecord = 0;
                }
                return retVal;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--Usp_GetNotInsertedRowOnTempProcedure", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        public static RewardResponse GetNotInsertedRecordsOfProviderSchedule(DataTable Dt)
        {
            RewardResponse retVal = new RewardResponse();
            try
            {
                DataSet dts = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@DataTable", SqlDbType.Structured);
                strParameter[0].Value = Dt;
                dts = new clsHelper().GetDatasetData("Usp_GetNotInsertedRowOnTempProviderSchedule", strParameter, 5 * 1000);
                retVal.ReceivedRecords = Dt.Rows.Count;
                if (dts.Tables.Count > 0)
                {
                    retVal.TotalInsertedRecord = dts.Tables[0].Rows.Count;
                    if (dts.Tables[0].Rows.Count > 0)
                    {
                        retVal.InsertedList = (from p in dts.Tables[0].AsEnumerable()
                                               select new InsertedRecord
                                               {
                                                   DentrixId = Convert.ToInt32(p["id"]),
                                                   RLId = 0,
                                                   ProviderId = 0
                                               }
                                            ).ToList();
                    }
                    else
                    {
                        retVal.InsertedList = null;
                    }
                    if (retVal.InsertedList != null)
                    {
                        List<int> obj_insertedid = dts.Tables[0].AsEnumerable().Select(x => x.Field<int>("id"))
                                                       .ToList();
                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  where !(obj_insertedid.Contains(Convert.ToInt32(p["id"])))
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["id"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                      ).ToList();
                    }
                    else
                    {
                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["id"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                     ).ToList();
                    }
                }
                else
                {
                    retVal.TotalInsertedRecord = 0;
                    retVal.TotalNotInsertedRecord = 0;
                }
                return retVal;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--GetNotInsertedRecordsOfProviderSchedule", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static RewardResponse GetNotInsertedRecordsOfOperatorySchedule(DataTable Dt)
        {
            RewardResponse retVal = new RewardResponse();
            try
            {
                DataSet dts = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@DataTable", SqlDbType.Structured);
                strParameter[0].Value = Dt;
                dts = new clsHelper().GetDatasetData("Usp_GetNotInsertedRowOnTempOperatorySchedule", strParameter, 5 * 1000);
                retVal.ReceivedRecords = Dt.Rows.Count;
                if (dts.Tables.Count > 0)
                {
                    retVal.TotalInsertedRecord = dts.Tables[0].Rows.Count;
                    if (dts.Tables[0].Rows.Count > 0)
                    {
                        retVal.InsertedList = (from p in dts.Tables[0].AsEnumerable()
                                               select new InsertedRecord
                                               {
                                                   DentrixId = Convert.ToInt32(p["id"]),
                                                   RLId = 0,
                                                   ProviderId = 0
                                               }
                                            ).ToList();
                    }
                    else
                    {
                        retVal.InsertedList = null;
                    }
                    if (retVal.InsertedList != null)
                    {
                        List<int> obj_insertedid = dts.Tables[0].AsEnumerable().Select(x => x.Field<int>("id"))
                                                       .ToList();
                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  where !(obj_insertedid.Contains(Convert.ToInt32(p["id"])))
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["id"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                      ).ToList();
                    }
                    else
                    {
                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["id"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                     ).ToList();
                    }
                }
                else
                {
                    retVal.TotalInsertedRecord = 0;
                    retVal.TotalNotInsertedRecord = 0;
                }
                return retVal;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--GetNotInsertedRecordsOfProviderSchedule", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static RewardResponse GetNotInsertedRecordsOfPracticeSchedule(DataTable Dt)
        {
            RewardResponse retVal = new RewardResponse();
            try
            {
                DataSet dts = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@DataTable", SqlDbType.Structured);
                strParameter[0].Value = Dt;
                dts = new clsHelper().GetDatasetData("Usp_GetNotInsertedRowOnTempPracticeSchedule", strParameter, 5 * 1000);
                retVal.ReceivedRecords = Dt.Rows.Count;
                if (dts.Tables.Count > 0)
                {
                    retVal.TotalInsertedRecord = dts.Tables[0].Rows.Count;
                    if (dts.Tables[0].Rows.Count > 0)
                    {
                        retVal.InsertedList = (from p in dts.Tables[0].AsEnumerable()
                                               select new InsertedRecord
                                               {
                                                   DentrixId = Convert.ToInt32(p["id"]),
                                                   RLId = 0,
                                                   ProviderId = 0
                                               }
                                            ).ToList();
                    }
                    else
                    {
                        retVal.InsertedList = null;
                    }
                    if (retVal.InsertedList != null)
                    {
                        List<int> obj_insertedid = dts.Tables[0].AsEnumerable().Select(x => x.Field<int>("id"))
                                                       .ToList();
                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  where !(obj_insertedid.Contains(Convert.ToInt32(p["id"])))
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["id"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                      ).ToList();
                    }
                    else
                    {
                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["id"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                     ).ToList();
                    }
                }
                else
                {
                    retVal.TotalInsertedRecord = 0;
                    retVal.TotalNotInsertedRecord = 0;
                }
                return retVal;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--GetNotInsertedRecordsOfOperatorySchedule", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static BulkResponse BulkInsertOfPatientAdjustment(DataTable Dt)
        {
            try
            {
                BulkResponse bulk = new BulkResponse();
                bulk.ReceivedRecords = Dt.Rows.Count;
                bulk.TotalInsertedRecord = new clsHelper(IntegrationConnectionString).BulkInsert(Dt, "Temp_Patient_DentrixAdjustmentDetails");
                bulk.TotalNotInsertedRecord = Dt.Rows.Count - bulk.TotalInsertedRecord;
                return bulk;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--BulkInsertOfPatientAdjustment", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static RewardResponse GetAdjustmentsRewardResponse(DataTable Dt)
        {
            RewardResponse retVal = new RewardResponse();
            try
            {
                DataSet dts = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@DataTable", SqlDbType.Structured);
                strParameter[0].Value = Dt;
                dts = new clsHelper(IntegrationConnectionString).GetDatasetData("Usp_GetNotInsertedRowOnTempAdjustments", strParameter, 5 * 1000);
                retVal.ReceivedRecords = Dt.Rows.Count;
                if (dts.Tables.Count > 0)
                {
                    retVal.TotalInsertedRecord = dts.Tables[0].Rows.Count;

                    if (dts.Tables[0].Rows.Count > 0)
                    {
                        retVal.InsertedList = (from p in dts.Tables[0].AsEnumerable()
                                               select new InsertedRecord
                                               {
                                                   DentrixId = Convert.ToInt32(p["adjustid"]),
                                                   RLId = 0,
                                                   ProviderId = 0
                                               }
                                            ).ToList();
                    }
                    else
                    {
                        retVal.InsertedList = null;
                    }


                    if (retVal.InsertedList != null)
                    {
                        List<int> obj_insertedid = dts.Tables[0].AsEnumerable().Select(x => x.Field<int>("adjustid"))
                                                      .ToList();

                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  where !(obj_insertedid.Contains(Convert.ToInt32(p["adjustid"])))
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["adjustid"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                      ).ToList();
                    }
                    else
                    {
                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["adjustid"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                        ).ToList();
                    }

                }
                else
                {
                    retVal.TotalInsertedRecord = 0;
                    retVal.TotalNotInsertedRecord = 0;
                }


                return retVal;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--GetNotInsertedTempAdjustmentsRecordsList", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        public static BulkResponse BulkInsertOfPatientFinance(DataTable dt)
        {
            try
            {
                BulkResponse bulk = new BulkResponse();
                bulk.ReceivedRecords = dt.Rows.Count;
                bulk.TotalInsertedRecord = new clsHelper(IntegrationConnectionString).BulkInsert(dt, "Temp_Patient_DentrixFinanceCharges");
                bulk.TotalNotInsertedRecord = dt.Rows.Count - bulk.TotalInsertedRecord;
                return bulk;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--BulkInsertOfPatientFinance", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static RewardResponse GetFinanceRewardResponse(DataTable Dt)
        {
            RewardResponse retVal = new RewardResponse();
            try
            {
                DataSet dts = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@DataTable", SqlDbType.Structured);
                strParameter[0].Value = Dt;
                dts = new clsHelper(IntegrationConnectionString).GetDatasetData("Usp_GetNotInsertedRowOnTempFinanceCharges", strParameter, 5 * 1000);
                retVal.ReceivedRecords = Dt.Rows.Count;
                if (dts.Tables.Count > 0)
                {
                    retVal.TotalInsertedRecord = dts.Tables[0].Rows.Count;

                    if (dts.Tables[0].Rows.Count > 0)
                    {
                        retVal.InsertedList = (from p in dts.Tables[0].AsEnumerable()
                                               select new InsertedRecord
                                               {
                                                   DentrixId = Convert.ToInt32(p["chargeId"]),
                                                   RLId = 0,
                                                   ProviderId = 0
                                               }
                                            ).ToList();
                    }
                    else
                    {
                        retVal.InsertedList = null;
                    }


                    if (retVal.InsertedList != null)
                    {
                        List<int> obj_insertedid = dts.Tables[0].AsEnumerable().Select(x => x.Field<int>("chargeId"))
                                                         .ToList();

                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  where !(obj_insertedid.Contains(Convert.ToInt32(p["chargeId"])))
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["chargeId"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                      ).ToList();
                    }
                    else
                    {
                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["chargeId"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                     ).ToList();
                    }

                }
                else
                {
                    retVal.TotalInsertedRecord = 0;
                    retVal.TotalNotInsertedRecord = 0;
                }


                return retVal;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--Usp_GetNotInsertedRowOnTempFinanceCharges", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        public static BulkResponse BulkInsertOfPatientStandard(DataTable Dt)
        {
            try
            {
                BulkResponse bulk = new BulkResponse();
                bulk.ReceivedRecords = Dt.Rows.Count;
                bulk.TotalInsertedRecord = new clsHelper(IntegrationConnectionString).BulkInsert(Dt, "Temp_Patient_DentrixStandardPayment");
                bulk.TotalNotInsertedRecord = Dt.Rows.Count - bulk.TotalInsertedRecord;
                return bulk;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--BulkInsertOfPatientStandard", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static RewardResponse GetStandardRewardResponse(DataTable Dt)
        {
            RewardResponse retVal = new RewardResponse();
            try
            {
                DataSet dts = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@DataTable", SqlDbType.Structured);
                strParameter[0].Value = Dt;
                dts = new clsHelper(IntegrationConnectionString).GetDatasetData("Usp_GetNotInsertedRowOnTempStandardPayment", strParameter, 5 * 1000);
                retVal.ReceivedRecords = Dt.Rows.Count;
                if (dts.Tables.Count > 0)
                {
                    retVal.TotalInsertedRecord = dts.Tables[0].Rows.Count;

                    if (dts.Tables[0].Rows.Count > 0)
                    {
                        retVal.InsertedList = (from p in dts.Tables[0].AsEnumerable()
                                               select new InsertedRecord
                                               {
                                                   DentrixId = Convert.ToInt32(p["paymentId"]),
                                                   RLId = 0,
                                                   ProviderId = 0
                                               }
                                            ).ToList();
                    }
                    else
                    {
                        retVal.InsertedList = null;
                    }


                    if (retVal.InsertedList != null)
                    {
                        List<int> obj_insertedid = dts.Tables[0].AsEnumerable().Select(x => x.Field<int>("paymentId"))
                                                         .ToList();

                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  where !(obj_insertedid.Contains(Convert.ToInt32(p["paymentId"])))
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["paymentId"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                      ).ToList();
                    }
                    else
                    {
                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["paymentId"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                    ).ToList();
                    }

                }
                else
                {
                    retVal.TotalInsertedRecord = 0;
                    retVal.TotalNotInsertedRecord = 0;
                }


                return retVal;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--Usp_GetNotInsertedRowOnTempStandardPayment", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        public static BulkResponse BulkInsertOfPatientInsurance(DataTable Dt)
        {
            try
            {
                BulkResponse bulk = new BulkResponse();
                bulk.ReceivedRecords = Dt.Rows.Count;
                bulk.TotalInsertedRecord = new clsHelper(IntegrationConnectionString).BulkInsert(Dt, "Temp_Patient_DentrixInsurancePayment");
                bulk.TotalNotInsertedRecord = Dt.Rows.Count - bulk.TotalInsertedRecord;
                return bulk;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--BulkInsertOfPatientInsurance", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static RewardResponse GetInsuranceRewardResponse(DataTable Dt)
        {
            RewardResponse retVal = new RewardResponse();
            try
            {
                DataSet dts = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@DataTable", SqlDbType.Structured);
                strParameter[0].Value = Dt;
                dts = new clsHelper(IntegrationConnectionString).GetDatasetData("Usp_GetNotInsertedRowOnTempInsurancePayment", strParameter, 5 * 1000);
                retVal.ReceivedRecords = Dt.Rows.Count;
                if (dts.Tables.Count > 0)
                {
                    retVal.TotalInsertedRecord = dts.Tables[0].Rows.Count;

                    if (dts.Tables[0].Rows.Count > 0)
                    {
                        retVal.InsertedList = (from p in dts.Tables[0].AsEnumerable()
                                               select new InsertedRecord
                                               {
                                                   DentrixId = Convert.ToInt32(p["paymentId"]),
                                                   RLId = 0,
                                                   ProviderId = 0
                                               }
                                            ).ToList();
                    }
                    else
                    {
                        retVal.InsertedList = null;
                    }


                    if (retVal.InsertedList != null)
                    {
                        List<int> obj_insertedid = dts.Tables[0].AsEnumerable().Select(x => x.Field<int>("paymentId"))
                                                       .ToList();

                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  where !(obj_insertedid.Contains(Convert.ToInt32(p["paymentId"])))
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["paymentId"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                      ).ToList();
                    }
                    else
                    {
                        retVal.NotInsertedList = (from p in Dt.AsEnumerable()
                                                  select new NotInsertedRecord
                                                  {
                                                      DentrixId = Convert.ToInt32(p["paymentId"]),
                                                      ErrorMessage = "Unknown error occured !"
                                                  }
                                     ).ToList();
                    }

                }
                else
                {
                    retVal.TotalInsertedRecord = 0;
                    retVal.TotalNotInsertedRecord = 0;
                }


                return retVal;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--Usp_GetNotInsertedRowOnTempInsurancePayment", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        public static DataTable GetMessageDetailsForSMSandEmail(int MessageId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
               {
                    new SqlParameter{ParameterName="@MessageId",Value=MessageId},
               };
                return new clsHelper().DataTable("Usp_GetMessageDetailsByMessageId", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--GetMessageDetailsForSMSandEmail", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static BO.Enums.Common.RedimRewardStatus RedimRewards(RedimRewardsRequestModel model)
        {
            return (BO.Enums.Common.RedimRewardStatus)Convert.ToInt32(new clsHelper().ExecuteScalar("USP_SUP_RedimRewards", new SqlParameter[]
               {
                    new SqlParameter{ParameterName="@RewardPartnerId",Value=model.RewardPartnerId},
                    new SqlParameter { ParameterName="@PointsToRedim",Value=model.PointsToRedim },
                    new SqlParameter { ParameterName = "@AmountToRedim", Value =model.AmountToRedim },
                    new SqlParameter { ParameterName = "@TransactionDate", Value =model.TransactionDate },
                    new SqlParameter { ParameterName = "@TransactionId", Value =model.TransactionId }
               }));
        }
        public static DataTable PatientHistory(int PatientId)
        {
            try
            {
                SqlParameter[] GetColleagueParams = new SqlParameter[1];

                GetColleagueParams[0] = new SqlParameter();
                GetColleagueParams[0].ParameterName = "@PatientId";
                GetColleagueParams[0].Value = PatientId;

                return new clsHelper().DataTable("GetPatientHistory", GetColleagueParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }



        public static DataTable GetPatientInsuranceDetails(int intPatientId)
        {
            try
            {
                SqlParameter[] sqlparam = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@PatientId",Value=intPatientId},
                };

                return new clsHelper().DataTable("Usp_GetPatientInsuranceDetails", sqlparam);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static DataTable GetPatientInsuranceDataDetails(int intPatientId)
        {
            try
            {
                SqlParameter[] sqlparam = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@PatientId",Value=intPatientId},
                };

                return new clsHelper().DataTable("UPS_Get_InsuranceData", sqlparam);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool ChangeCoordinator(Coordinator objCoordinate)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@MessageId",Value=objCoordinate.MessageId},
                    new SqlParameter{ParameterName="@MemberId",Value=objCoordinate.MemberId}
                };
                bool result = new clsHelper().ExecuteNonQuery("USP_ChangeCoordintor", sqlpara);                
                return result;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--ChangeCoordinator", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool DeleteProviderSchedule(RemoveProcedure remove)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@DentrixConnectorKey",Value=remove.DentrixConnectorId},
                    new SqlParameter{ParameterName="@DentrixId",Value=remove.DentrixId}
                };
                return new clsHelper().ExecuteNonQuery("Usp_DeleteProviderSchedule", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--DeleteProviderSchedule", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool DeletePracticeSchedule(RemoveProcedure remove)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@DentrixConnectorKey",Value=remove.DentrixConnectorId},
                    new SqlParameter{ParameterName="@DentrixId",Value=remove.DentrixId}
                };
                return new clsHelper().ExecuteNonQuery("Usp_DeletePracticeSchedule", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--DeletePracticeSchedule", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool DeleteOperatorySchedule(RemoveProcedure remove)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@DentrixConnectorKey",Value=remove.DentrixConnectorId},
                    new SqlParameter{ParameterName="@DentrixId",Value=remove.DentrixId}
                };
                return new clsHelper().ExecuteNonQuery("Usp_DeleteOperatorySchedule", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsPatientData--DeleteOperatorySchedule", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
