﻿using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Models;
using System.Data.SqlClient;

namespace DataAccessLayer
{
    public class ApiLogDAL
    {
        public static bool InsertLog(ApiLogEntry objApiLogEntry)
         {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[30];
                sqlpara[0] = new SqlParameter("@RequestID", objApiLogEntry.RequestID);
                sqlpara[1] = new SqlParameter("@ManualTransactionID", objApiLogEntry.ManualTransactionId);
                sqlpara[2] = new SqlParameter("@Status", objApiLogEntry.Status);
                sqlpara[3] = new SqlParameter("@InOut", objApiLogEntry.InOut);
                sqlpara[4] = new SqlParameter("@AppVendor", objApiLogEntry.AppVendor);
                sqlpara[5] = new SqlParameter("@APICommandID", objApiLogEntry.APICommandID);
                
                sqlpara[6] = new SqlParameter("@Version", objApiLogEntry.APIVersion);
                sqlpara[7] = new SqlParameter("@UserName", objApiLogEntry.UserName);
                sqlpara[8] = new SqlParameter("@Password", objApiLogEntry.Password);
                sqlpara[9] = new SqlParameter("@Latitude", objApiLogEntry.Latitude);
                sqlpara[10] = new SqlParameter("@Longitude", objApiLogEntry.Longitude);
                
                sqlpara[11] = new SqlParameter("@ClientTime", objApiLogEntry.ClientTime);
                sqlpara[12] = new SqlParameter("@DeviceName", objApiLogEntry.DeviceName);
                sqlpara[13] = new SqlParameter("@DeviceAddress", objApiLogEntry.DeviceAddress);
                sqlpara[14] = new SqlParameter("@Notes", objApiLogEntry.Notes);
                
                sqlpara[15] = new SqlParameter("@RequestIpAddress", objApiLogEntry.RequestIpAddress);
                sqlpara[16] = new SqlParameter("@RequestContentType", objApiLogEntry.RequestContentType);
                sqlpara[17] = new SqlParameter("@RequestContentBody", objApiLogEntry.RequestContentBody);
                sqlpara[18] = new SqlParameter("@RequestUri", objApiLogEntry.RequestUri);
                sqlpara[19] = new SqlParameter("@RequestMethod", objApiLogEntry.RequestMethod);
                
                sqlpara[20] = new SqlParameter("@RequestRouteTemplate", objApiLogEntry.RequestRouteTemplate);
                sqlpara[21] = new SqlParameter("@RequestRouteData", objApiLogEntry.RequestRouteData);
                sqlpara[22] = new SqlParameter("@RequestHeaders", objApiLogEntry.RequestHeaders);
                sqlpara[23] = new SqlParameter("@RequestTime", objApiLogEntry.RequestTimestamp);
                sqlpara[24] = new SqlParameter("@ResponseContentType", objApiLogEntry.ResponseContentType);
                
                sqlpara[25] = new SqlParameter("@ResponseContentBody", objApiLogEntry.ResponseContentBody);
                sqlpara[26] = new SqlParameter("@ResponseStatusCode", objApiLogEntry.ResponseStatusCode);
                sqlpara[27] = new SqlParameter("@ResponseHeaders", objApiLogEntry.ResponseHeaders);
                sqlpara[28] = new SqlParameter("@ResponseTime", objApiLogEntry.ResponseTimestamp);
                sqlpara[29] = new SqlParameter("@TokenNumber", objApiLogEntry.TokenNumber);
                return new clsHelper().ExecuteNonQuery("Usp_WebAPI_InsertWebApiEventLog", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ApiLogDAL--InsertLog", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
