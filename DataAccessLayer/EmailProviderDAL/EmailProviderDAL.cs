﻿using BO.Models;
using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DataAccessLayer.EmailProviderDAL
{
    public class EmailProviderDAL
    {
        public static clsHelper clshelper = new clsHelper();
        public static clsCommon objcommon = new clsCommon();
        public static DataTable GetAllEmailProviderList()
        {
            try
            {
                DataTable dt = new DataTable();                
                dt = clshelper.DataTable("USP_GetAllEmailProviderList", null);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("EmailProviderDAL--USP_GetAllEmailProviderList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool DeleteEmailProviderDetailsById(int id, int userid)
        {
            int result = 0;
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@epd_id", SqlDbType.Int);
                strParameter[0].Value = Convert.ToInt32(id);
                strParameter[1] = new SqlParameter("@member_id", SqlDbType.Int);
                strParameter[1].Value = Convert.ToInt32(userid);

                result = Convert.ToInt32(clshelper.ExecuteNonQuery("DeleteFromEmailProviderDetails", strParameter));

                return result > 0;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("EmailProviderDAL--DeleteFromEmailProviderDetails", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public static bool InsertEmailProviderDetails(EmailProviderDetails model)
        {
            int result = 0;
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[13];
                strParameter[0] = new SqlParameter("@Epd_Id", SqlDbType.Int);
                strParameter[0].Value = model.providerDetailsId;
                strParameter[1] = new SqlParameter("@Provider_Id", SqlDbType.Int);
                strParameter[1].Value = model.providerId;
                strParameter[2] = new SqlParameter("@EmailAddress", SqlDbType.NVarChar);
                strParameter[2].Value = model.emailAddress;
                strParameter[3] = new SqlParameter("@Password", SqlDbType.NVarChar);
                strParameter[3].Value = model.password;
                strParameter[4] = new SqlParameter("@AccessToken", SqlDbType.NVarChar);
                strParameter[4].Value = model.accessToken;
                strParameter[5] = new SqlParameter("@RefreshToken", SqlDbType.NVarChar);
                strParameter[5].Value = model.refreshToken;
                strParameter[6] = new SqlParameter("@MemberId", SqlDbType.Int);
                strParameter[6].Value = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                strParameter[7] = new SqlParameter("@SmtpServerName", SqlDbType.NVarChar);
                strParameter[7].Value = model.smtpServerName;
                strParameter[8] = new SqlParameter("@SmtpPortNumber", SqlDbType.Int);
                strParameter[8].Value = model.smtpPortNumber;
                strParameter[9] = new SqlParameter("@RequireSSL", SqlDbType.Bit);
                strParameter[9].Value = model.requireSSL;
                strParameter[10] = new SqlParameter("@SyncEnabled", SqlDbType.Bit);
                strParameter[10].Value = model.syncEnabled;
                strParameter[11] = new SqlParameter("@SendEnabled", SqlDbType.Bit);
                strParameter[11].Value = model.sendEnabled;                
                strParameter[12] = new SqlParameter("@IsDefault", SqlDbType.Bit);
                strParameter[12].Value = model.isDefault;               

                result = Convert.ToInt32(clshelper.ExecuteNonQuery("SP_InsertEmailProviderDetails", strParameter));

                return result > 0;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("EmailProviderDAL--SP_InsertEmailProviderDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool UpdateEmailProviderDetails(EmailProviderDetails model)
        {
            int result = 0;
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[13];
                strParameter[0] = new SqlParameter("@Epd_Id", SqlDbType.Int);
                strParameter[0].Value = model.providerDetailsId;
                strParameter[1] = new SqlParameter("@Provider_Id", SqlDbType.Int);
                strParameter[1].Value = model.providerId;
                strParameter[2] = new SqlParameter("@EmailAddress", SqlDbType.NVarChar);
                strParameter[2].Value = model.emailAddress;
                strParameter[3] = new SqlParameter("@Password", SqlDbType.NVarChar);
                strParameter[3].Value = model.password;
                strParameter[4] = new SqlParameter("@AccessToken", SqlDbType.NVarChar);
                strParameter[4].Value = model.accessToken;
                strParameter[5] = new SqlParameter("@RefreshToken", SqlDbType.NVarChar);
                strParameter[5].Value = model.refreshToken;
                strParameter[6] = new SqlParameter("@MemberId", SqlDbType.Int);
                strParameter[6].Value = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                strParameter[7] = new SqlParameter("@SmtpServerName", SqlDbType.NVarChar);
                strParameter[7].Value = model.smtpServerName;
                strParameter[8] = new SqlParameter("@SmtpPortNumber", SqlDbType.Int);
                strParameter[8].Value = model.smtpPortNumber;
                strParameter[9] = new SqlParameter("@RequireSSL", SqlDbType.Bit);
                strParameter[9].Value = model.requireSSL;
                strParameter[10] = new SqlParameter("@SyncEnabled", SqlDbType.Bit);
                strParameter[10].Value = model.syncEnabled;
                strParameter[11] = new SqlParameter("@SendEnabled", SqlDbType.Bit);
                strParameter[11].Value = model.sendEnabled;
                strParameter[12] = new SqlParameter("@IsDefault", SqlDbType.Bit);
                strParameter[12].Value = model.isDefault;

                result = Convert.ToInt32(clshelper.ExecuteNonQuery("SP_InsertEmailProviderDetails", strParameter));

                return result > 0;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("EmailProviderDAL--SP_InsertEmailProviderDetails", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public static DataTable GetAllEmailProviderDetailsList(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] para = new SqlParameter[1];
                para[0] = new SqlParameter("@UserId", SqlDbType.Int);
                para[0].Value = UserId;
                dt = clshelper.DataTable("USP_GetAllEmailProviderDetailsList", para);
                return dt;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("EmailProviderDAL--USP_GetAllEmailProviderDetailsList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static DataTable GetAllEmailProviderDetailsById(int epdId, int userId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] para = new SqlParameter[2];
                para[0] = new SqlParameter("@Epd_Id", SqlDbType.Int);
                para[0].Value = epdId;
                para[1] = new SqlParameter("@MemberId", SqlDbType.Int);
                para[1].Value = userId;
                dt = clshelper.DataTable("USP_GetAllEmailProviderDetailsById", para);
                return dt;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("EmailProviderDAL--USP_GetAllEmailProviderDetailsById", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public static bool UpdateEmailProviderDetailsForDefault(string epdId, string userId)
        {
            int result = 0;
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@Epd_Id", SqlDbType.Int);
                strParameter[0].Value = Convert.ToInt32(epdId);
                strParameter[1] = new SqlParameter("@MemberId", SqlDbType.Int);
                strParameter[1].Value = Convert.ToInt32(userId);

                result = Convert.ToInt32(clshelper.ExecuteNonQuery("SP_UpdateEmailProviderDetailsForDefault", strParameter));

                return result > 0;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("EmailProviderDAL--USP_UpdateEmailProviderDetailsForDefault", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public static bool CheckEmailAlreadyExists(string user, int pid, string userid)
        {
            int result = 0;
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@EmailAddress", SqlDbType.NVarChar);
                strParameter[0].Value = Convert.ToString(user);
                strParameter[1] = new SqlParameter("@Provider_Id", SqlDbType.Int);
                strParameter[1].Value = Convert.ToInt32(pid);
                strParameter[2] = new SqlParameter("@MemberId", SqlDbType.Int);
                strParameter[2].Value = Convert.ToInt32(userid);

                result = Convert.ToInt32(clshelper.ExecuteScalar("SP_CheckEmailAlreadyExists", strParameter));

                return result > 0;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("EmailProviderDAL--SP_CheckEmailAlreadyExists", ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
