﻿using BO.Models;
using DataAccessLayer.Common;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DataAccessLayer
{
    public class DentistDAL
    {
        public static bool IsDentistIntegrationPartner(int UserId)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@UserId", SqlDbType.Int);
                param[0].Value = UserId;

                DataTable dt = new clsHelper().DataTable("USP_IsDentistIntegrationPartner", param);
                return dt.Rows.Count > 0;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("USP_IsDentistIntegrationPartner for userId=" + UserId , Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool SaveReferralStatusOfUser(OCR_ReferralStatus _ReferralStatus)
        {
            try
            {
                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@UserId", SqlDbType.Int);
                param[0].Value = _ReferralStatus.UserId;
                param[1] = new SqlParameter("@IsReceiveReferral", SqlDbType.Bit);
                param[1].Value = _ReferralStatus.IsReceiveReferral;
                param[2] = new SqlParameter("@IsSendReferral", SqlDbType.Bit);
                param[2].Value = _ReferralStatus.IsSendReferral;
                bool Result = new clsHelper().ExecuteNonQuery("Usp_InsertOCRReferralStatus", param);
                return Result;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("DentistDAL--SaveReferralStatusOfUser", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
