﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using DataAccessLayer.Common;

using BO.Models;

namespace DataAccessLayer.ColleaguesData
{
    public class clsAdmin
    {
        public static clsHelper clshelper = new clsHelper();
        public static clsCommon objcommon = new clsCommon();

        // method for get Active and Inactive user list

        public DataTable GetActiveInActiveUserList(int index, int size, string searchtext, int status,string SortByValue,string WebsiteURL=null)
        {
            DataTable dtuser = new DataTable();

            SqlParameter[] alluser = new SqlParameter[6];
            alluser[0] = new SqlParameter("@index", SqlDbType.Int);
            alluser[0].Value = index;
            alluser[1] = new SqlParameter("@size", SqlDbType.Int);
            alluser[1].Value = size;
            alluser[2] = new SqlParameter("@searchtext", SqlDbType.VarChar);
            alluser[2].Value = searchtext;
            alluser[3] = new SqlParameter("@status", SqlDbType.Int);
            alluser[3].Value = status;
            alluser[4] = new SqlParameter("@SortBy", SqlDbType.VarChar);
            alluser[4].Value = SortByValue;
            alluser[5] = new SqlParameter("@WebsiteURL", SqlDbType.NVarChar);
            alluser[5].Value = WebsiteURL;            
            return dtuser = clshelper.DataTableForExportData("USP_Admin_GetDoctor", alluser);

        }


        //Method For Advance Search Admin Active user
        public DataTable BindAllUsersListAdvanceSearch(int index, int size, string fname, string lname, string speciality, string state, string city, string zipcode, string email)
        {
            DataTable dtAllUsers = new DataTable();
            SqlParameter[] alluser = new SqlParameter[9];
            alluser[0] = new SqlParameter("@index", SqlDbType.Int);
            alluser[0].Value = index;
            alluser[1] = new SqlParameter("@size", SqlDbType.Int);
            alluser[1].Value = size;
            alluser[2] = new SqlParameter("@fname", SqlDbType.VarChar);
            alluser[2].Value = fname;
            alluser[3] = new SqlParameter("@lname", SqlDbType.VarChar);
            alluser[3].Value = lname;
            alluser[4] = new SqlParameter("@specialities", SqlDbType.VarChar);
            alluser[4].Value = speciality;
            alluser[5] = new SqlParameter("@state", SqlDbType.VarChar);
            alluser[5].Value = state;
            alluser[6] = new SqlParameter("@city", SqlDbType.VarChar);
            alluser[6].Value = city;
            alluser[7] = new SqlParameter("@zipcode", SqlDbType.VarChar);
            alluser[7].Value = zipcode;
            alluser[8] = new SqlParameter("@email", SqlDbType.VarChar);
            alluser[8].Value = email;
            return dtAllUsers = clshelper.DataTable("AllUsersList", alluser);
        }

        //Method For Admin Deactive user List
        public DataTable DeactiveUsersList(int index, int size, string Searchname)
        {
            DataTable dtAllUsers = new DataTable();
            SqlParameter[] alluser = new SqlParameter[3];
            alluser[0] = new SqlParameter("@index", SqlDbType.Int);
            alluser[0].Value = index;
            alluser[1] = new SqlParameter("@size", SqlDbType.Int);
            alluser[1].Value = size;
            alluser[2] = new SqlParameter("@Searchname", SqlDbType.VarChar);
            alluser[2].Value = Searchname;
            return dtAllUsers = clshelper.DataTable("DeactiveUsersList", alluser);

        }

        //mehtod for get all  Email Templates
        public DataTable BindAllEmailTemp(int PageIndex, int PageSize, string searchtext, int SortColumn, int SortDirection)
        {

            SqlParameter[] SummaryParams = new SqlParameter[5];
            SummaryParams[0] = new SqlParameter("@PageIndex", SqlDbType.Int);
            SummaryParams[0].Value = PageIndex;
            SummaryParams[1] = new SqlParameter("@PageSize", SqlDbType.Int);
            SummaryParams[1].Value = PageSize;
            SummaryParams[2] = new SqlParameter("@searchtext", SqlDbType.VarChar);
            SummaryParams[2].Value = searchtext;
            SummaryParams[3] = new SqlParameter("@SortColumn", SqlDbType.Int);
            SummaryParams[3].Value = SortColumn;
            SummaryParams[4] = new SqlParameter("@SortDirection", SqlDbType.Int);
            SummaryParams[4].Value = SortDirection;


            DataTable dtSummaryDetails = clshelper.DataTable("USP_GetAllEmailTemplate", SummaryParams);
            return dtSummaryDetails;
        }

        // Method for User Logged Information List
        public DataTable BindUserLogedInfoList(string type)
        {
            DataTable dtAllUsers = new DataTable();
            try
            {
                SqlParameter[] strtypeParams = new SqlParameter[1];
                strtypeParams[0] = new SqlParameter("@type", SqlDbType.Text);
                strtypeParams[0].Value = type;


                return dtAllUsers = clshelper.DataTable("to_get_counter_for_the_users", strtypeParams);
            }
            catch (Exception)
            {
                throw;
            }

        }


        #region Email Template GetTemplate by id, Edit

        public DataTable GetEmailTemplateById(int TemplateId)
        {
            DataTable dtAllUsers = new DataTable();
            try
            {
                SqlParameter[] strtypeParams = new SqlParameter[1];
                strtypeParams[0] = new SqlParameter("@TemplateId", SqlDbType.Int);
                strtypeParams[0].Value = TemplateId;


                return dtAllUsers = clshelper.DataTable("USP_GetEmailTemplateById", strtypeParams);
            }
            catch (Exception)
            {
                throw;
            }

        }


        public bool EditEmailTemplateById(int TemplateId, string TemplateSubject, string TemplateDesc)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[3];
                addphone[0] = new SqlParameter("@TemplateId", SqlDbType.Int);
                addphone[0].Value = TemplateId;
                addphone[1] = new SqlParameter("@TemplateSubject", SqlDbType.NVarChar);
                addphone[1].Value = TemplateSubject;
                addphone[2] = new SqlParameter("@TemplateDesc", SqlDbType.Text);
                addphone[2].Value = TemplateDesc;




                UpdateStatus = clshelper.ExecuteNonQuery("USP_EditEmailTemplateById", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion



        public DataTable GetUserLoggedInfo(int index, int size, string searchtext, int SortColumn, int SortDirection)
        {
            DataTable dtuser = new DataTable();

            SqlParameter[] alluser = new SqlParameter[5];
            alluser[0] = new SqlParameter("@index", SqlDbType.Int);
            alluser[0].Value = index;
            alluser[1] = new SqlParameter("@size", SqlDbType.Int);
            alluser[1].Value = size;
            alluser[2] = new SqlParameter("@searchtext", SqlDbType.VarChar);
            alluser[2].Value = searchtext;

            alluser[3] = new SqlParameter("@SortColumn", SqlDbType.Int);
            alluser[3].Value = SortColumn;

            alluser[4] = new SqlParameter("@SortDirection", SqlDbType.Int);
            alluser[4].Value = SortDirection;

            return dtuser = clshelper.DataTable("USP_Admin_LogedInfo", alluser);

        }



        #region Code For Email Histroy add,get
        public bool AddAdminEmailHistoryByUserId(int UserId, string EmailSubject, string EmailContent)
        {
            bool result = false;
            try
            {

                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@EmailSubject", SqlDbType.Text);
                strParameter[1].Value = EmailSubject;
                strParameter[2] = new SqlParameter("@EmailContent", SqlDbType.Text);
                strParameter[2].Value = EmailContent;

                result = clshelper.ExecuteNonQuery("USP_AddAdminEmailHistoryByUserId", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }
        public DataTable GetAllEmailHistoryByUserId(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext)
        {
            DataTable dtAllUsers = new DataTable();
            SqlParameter[] alluser = new SqlParameter[6];
            alluser[0] = new SqlParameter("@UserId", SqlDbType.Int);
            alluser[0].Value = UserId;
            alluser[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
            alluser[1].Value = PageIndex;
            alluser[2] = new SqlParameter("@PageSize", SqlDbType.Int);
            alluser[2].Value = PageSize;
            alluser[3] = new SqlParameter("@SortColumn", SqlDbType.Int);
            alluser[3].Value = SortColumn;
            alluser[4] = new SqlParameter("@SortDirection", SqlDbType.Int);
            alluser[4].Value = SortDirection;
            alluser[5] = new SqlParameter("@Searchtext", SqlDbType.VarChar);
            alluser[5].Value = Searchtext;
            return dtAllUsers = clshelper.DataTable("USP_GetAllEmailHistoryByUserId", alluser);

        }

        public bool DeleteAdminEmailHistoryById(int Id)
        {
            bool result = false;
            try
            {

                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@EmailId", SqlDbType.Int);
                strParameter[0].Value = Id;


                result = clshelper.ExecuteNonQuery("USP_DeleteAdminEmailHistoryById", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }
        public DataTable GetSentEmailHistoryById(int Id)
        {
            DataTable dtAllUsers = new DataTable();
            SqlParameter[] alluser = new SqlParameter[1];
            alluser[0] = new SqlParameter("@EmailId", SqlDbType.Int);
            alluser[0].Value = Id;

            return dtAllUsers = clshelper.DataTable("USP_GetSentEmailHistoryById", alluser);

        }
        #endregion

        #region Method for update user status
        public bool UpdateUserStatus(int UserId, int Status)
        {
            bool result = false;
            try
            {

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@Status", SqlDbType.Int);
                strParameter[1].Value = Status;

                result = clshelper.ExecuteNonQuery("USP_UpdateUserStatus", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public bool UpdateUserPassword(int UserId, string Password)
        {
            bool result = false;
            try
            {

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@Password", SqlDbType.VarChar);
                strParameter[1].Value = Password;

                result = clshelper.ExecuteNonQuery("USP_UpdateUserPassword", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        #endregion

        #region Methods for sign up with just email

        public DataTable GetAllUserSignUpWithJustEmail(int PageIndex, int PageSize, string Searchtext, int SortColumn, int SortDirection)
        {
            DataTable dtAllUsers = new DataTable();
            SqlParameter[] alluser = new SqlParameter[5];
            alluser[0] = new SqlParameter("@PageIndex", SqlDbType.Int);
            alluser[0].Value = PageIndex;
            alluser[1] = new SqlParameter("@PageSize", SqlDbType.Int);
            alluser[1].Value = PageSize;
            alluser[2] = new SqlParameter("@Searchtext", SqlDbType.NVarChar);
            alluser[2].Value = Searchtext;
            alluser[3] = new SqlParameter("@SortColumn", SqlDbType.Int);
            alluser[3].Value = SortColumn;
            alluser[4] = new SqlParameter("@SortDirection", SqlDbType.Int);
            alluser[4].Value = SortDirection;

            return dtAllUsers = clshelper.DataTable("USP_GetAllUserSignUpWithJustEmail", alluser);

        }


        public bool RemoveSignup_JustEmail(int Id)
        {
            bool result = false;
            try
            {

                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Id", SqlDbType.Int);
                strParameter[0].Value = Id;


                result = clshelper.ExecuteNonQuery("USP_RemoveSignup_JustEmail", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public bool UpdateStatusInTempTableUserById(int Id, int status)
        {
            bool result = false;
            try
            {

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@Id", SqlDbType.Int);
                strParameter[0].Value = Id;
                strParameter[1] = new SqlParameter("@status", SqlDbType.Int);
                strParameter[1].Value = status;


                result = clshelper.ExecuteNonQuery("USP_UpdateSignup_JustEmail", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }
        public DataTable GetTempSignupById(int Id)
        {
            DataTable dtAllUsers = new DataTable();
            SqlParameter[] alluser = new SqlParameter[1];
            alluser[0] = new SqlParameter("@Id", SqlDbType.Int);
            alluser[0].Value = Id;


            return dtAllUsers = clshelper.DataTable("USP_GetTemp_SignupById", alluser);

        }
        public DataTable GetTempSignupByEmail(string Email)
        {
            DataTable dtAllUsers = new DataTable();
            SqlParameter[] alluser = new SqlParameter[1];
            alluser[0] = new SqlParameter("@Email", SqlDbType.NVarChar);
            alluser[0].Value = Email;
            return dtAllUsers = clshelper.DataTable("USP_GetTemp_SignupByEmail", alluser);
        }
        #endregion


        #region Edit Update Account Name Of Doctor
        public bool UpdateAccountNameOfDoctor(int UserId, string AccountName)
        {
            bool result = false;
            try
            {

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@AccountName", SqlDbType.NVarChar);
                strParameter[1].Value = AccountName;



                result = clshelper.ExecuteNonQuery("USP_UpdateAccountNameOfDoctor", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public DataTable GetAccountNameOfDoctorToEdit(int UserId)
        {
            try
            {
                 SqlParameter[] strParameter = new SqlParameter[1];
                 strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                 strParameter[0].Value = UserId;

                 DataTable result = clshelper.DataTable("USP_EditAccountNameOfDoctor", strParameter);

                 return result;
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        #endregion


        #region Vtiger setting
        public DataTable GetImportSettings(string Operation)
        {
            try
            {
                SqlParameter[] PotentialMemberParameters = new SqlParameter[1];

                PotentialMemberParameters[0] = new SqlParameter("@Operation", SqlDbType.VarChar);
                PotentialMemberParameters[0].Value = Operation;
                DataTable result = clshelper.DataTable("USP_AddUpdateAdminSettings", PotentialMemberParameters);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool AddUpdateImportSettings(bool ServiceRun, bool ImportWithDate, string LastModifiedDate, string Operation)
        {

            try
            {
                SqlParameter[] ImportSettings = new SqlParameter[4];

                ImportSettings[0] = new SqlParameter("@rdServiceRun", SqlDbType.Bit);
                ImportSettings[0].Value = ServiceRun;
                ImportSettings[1] = new SqlParameter("@rdImportWithDate", SqlDbType.Bit);
                ImportSettings[1].Value = ImportWithDate;
                ImportSettings[2] = new SqlParameter("@Operation", SqlDbType.VarChar);
                ImportSettings[2].Value = Operation;
                ImportSettings[3] = new SqlParameter("@LastModifiedDate", SqlDbType.VarChar);
                ImportSettings[3].Value = LastModifiedDate;
                bool result = clshelper.ExecuteNonQuery("USP_AddUpdateAdminSettings", ImportSettings);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region UpdateAdminPassword
        public bool UpdateAdminPassword(string Password, int UserId)
        {
            try
            {
                SqlParameter[] strAdmPwdParams = new SqlParameter[2];
                strAdmPwdParams[0] = new SqlParameter("@pUserId", SqlDbType.Int);
                strAdmPwdParams[0].Value = UserId;
                strAdmPwdParams[1] = new SqlParameter("@pPassword", SqlDbType.VarChar);
                strAdmPwdParams[1].Value = Password;

                bool result = clshelper.ExecuteNonQuery("USP_UpdateAdminPassword", strAdmPwdParams);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion



        #region Block Users
        public DataTable GetAllBlockUsers(int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext)
        {
            DataTable dtuser = new DataTable();

            SqlParameter[] alluser = new SqlParameter[5];
            alluser[0] = new SqlParameter("@PageIndex", SqlDbType.Int);
            alluser[0].Value = PageIndex;
            alluser[1] = new SqlParameter("@PageSize", SqlDbType.Int);
            alluser[1].Value = PageSize;
            alluser[2] = new SqlParameter("@SortColumn", SqlDbType.Int);
            alluser[2].Value = SortColumn;
            alluser[3] = new SqlParameter("@SortDirection", SqlDbType.Int);
            alluser[3].Value = SortDirection;
            alluser[4] = new SqlParameter("@Searchtext", SqlDbType.NVarChar);
            alluser[4].Value = Searchtext;

            return dtuser = clshelper.DataTable("USP_GetAllBlockUsers", alluser);

        }
        #endregion


        #region CompanyList

        public bool AddComapanyList(int comp_id, string comp_name, string comp_connectionstring, string comp_website, string comp_salesEmail, string comp_supportEmail, string comp_phone, string comp_ownername, string comp_ownertitle, string comp_ownerEmail, string comp_Facebook, string comp_blog, string comp_youtube,
           string comp_linkedin, string comp_twitter, string comp_gplus, string comp_FBlike, string comp_alexa, int Isactive, string comp_logopath, string comp_SMTPserver, string comp_port, string comp_login, string comp_password, int comp_SSL, string comp_add1, string comp_add2, string comp_city,
           string comp_state, string comp_zipcode, string comp_country, int Isprimary)
        {
            try
            {

                bool billphonestatus;
                SqlParameter[] addbillphone = new SqlParameter[32];
                addbillphone[0] = new SqlParameter("@CompanyId", SqlDbType.Int);
                addbillphone[0].Value = comp_id;
                addbillphone[1] = new SqlParameter("@CompanyName", SqlDbType.NVarChar);
                addbillphone[1].Value = comp_name;
                addbillphone[2] = new SqlParameter("@CompanyConnectionstring", SqlDbType.NVarChar);
                addbillphone[2].Value = comp_connectionstring;
                addbillphone[3] = new SqlParameter("@CompanyWebsite", SqlDbType.NVarChar);
                addbillphone[3].Value = comp_website;
                addbillphone[4] = new SqlParameter("@SalesEmail", SqlDbType.NVarChar);
                addbillphone[4].Value = comp_salesEmail;
                addbillphone[5] = new SqlParameter("@SupportEmail", SqlDbType.NVarChar);
                addbillphone[5].Value = comp_supportEmail;
                addbillphone[6] = new SqlParameter("@PhoneNo", SqlDbType.NVarChar);
                addbillphone[6].Value = comp_phone;
                addbillphone[7] = new SqlParameter("@CompanyOwnerName", SqlDbType.NVarChar);
                addbillphone[7].Value = comp_ownername;
                addbillphone[8] = new SqlParameter("@CompanyOwnerTitle", SqlDbType.NVarChar);
                addbillphone[8].Value = comp_ownertitle;
                addbillphone[9] = new SqlParameter("@CompanyOwnerEmail", SqlDbType.NVarChar);
                addbillphone[9].Value = comp_ownerEmail;
                addbillphone[10] = new SqlParameter("@CompanyFacebook", SqlDbType.NVarChar);
                addbillphone[10].Value = comp_Facebook;
                addbillphone[11] = new SqlParameter("@CompanyBlog", SqlDbType.NVarChar);
                addbillphone[11].Value = comp_blog;
                addbillphone[12] = new SqlParameter("@CompanyYoutube", SqlDbType.NVarChar);
                addbillphone[12].Value = comp_youtube;
                addbillphone[13] = new SqlParameter("@CompanyLinkedin", SqlDbType.NVarChar);
                addbillphone[13].Value = comp_linkedin;
                addbillphone[14] = new SqlParameter("@CompanyTwitter", SqlDbType.NVarChar);
                addbillphone[14].Value = comp_twitter;
                addbillphone[15] = new SqlParameter("@CompanyGooglePlus", SqlDbType.NVarChar);
                addbillphone[15].Value = comp_gplus;
                addbillphone[16] = new SqlParameter("@CompanyFacebookLike", SqlDbType.NVarChar);
                addbillphone[16].Value = comp_FBlike;
                addbillphone[17] = new SqlParameter("@CompanyAlexa", SqlDbType.NVarChar);
                addbillphone[17].Value = comp_alexa;
                addbillphone[18] = new SqlParameter("@Isactive", SqlDbType.Bit);
                addbillphone[18].Value = Isactive;
                addbillphone[19] = new SqlParameter("@LogoPath", SqlDbType.NVarChar);
                addbillphone[19].Value = comp_logopath;
                addbillphone[20] = new SqlParameter("@SMTPServer", SqlDbType.NVarChar);
                addbillphone[20].Value = comp_SMTPserver;
                addbillphone[21] = new SqlParameter("@SMTPPort", SqlDbType.NVarChar);
                addbillphone[21].Value = comp_port;
                addbillphone[22] = new SqlParameter("@SMTPLogin", SqlDbType.NVarChar);
                addbillphone[22].Value = comp_login;
                addbillphone[23] = new SqlParameter("@SMTPPassword", SqlDbType.NVarChar);
                addbillphone[23].Value = comp_password;
                addbillphone[24] = new SqlParameter("@SMTPSsl", SqlDbType.Bit);
                addbillphone[24].Value = comp_SSL;
                addbillphone[25] = new SqlParameter("@CompanyAddress1", SqlDbType.NVarChar);
                addbillphone[25].Value = comp_add1;
                addbillphone[26] = new SqlParameter("@CompanyAddress2", SqlDbType.NVarChar);
                addbillphone[26].Value = comp_add2;
                addbillphone[27] = new SqlParameter("@CompanyCity", SqlDbType.NVarChar);
                addbillphone[27].Value = comp_city;
                addbillphone[28] = new SqlParameter("@CompanyState", SqlDbType.NVarChar);
                addbillphone[28].Value = comp_state;
                addbillphone[29] = new SqlParameter("@CompanyZipCode", SqlDbType.NVarChar);
                addbillphone[29].Value = comp_zipcode;
                addbillphone[30] = new SqlParameter("@CompanyCountry", SqlDbType.NVarChar);
                addbillphone[30].Value = comp_country;
                addbillphone[31] = new SqlParameter("@IsPrimary", SqlDbType.Bit);
                addbillphone[31].Value = Isprimary;



                billphonestatus = clshelper.ExecuteNonQuery("USP_AddCompanyList", addbillphone);
                return billphonestatus;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public DataTable GetAllComapanyList(int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] alluser = new SqlParameter[5];
                alluser[0] = new SqlParameter("@PageIndex", SqlDbType.Int);
                alluser[0].Value = PageIndex;
                alluser[1] = new SqlParameter("@PageSize", SqlDbType.Int);
                alluser[1].Value = PageSize;
                alluser[2] = new SqlParameter("@SortColumn", SqlDbType.Int);
                alluser[2].Value = SortColumn;
                alluser[3] = new SqlParameter("@SortDirection", SqlDbType.Int);
                alluser[3].Value = SortDirection;
                alluser[4] = new SqlParameter("@Searchtext", SqlDbType.NVarChar);
                alluser[4].Value = Searchtext;
                dt = clshelper.DataTable("USP_GetComapanyList", alluser);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateComapanyList(int comp_id, string comp_name, string comp_connectionstring, string comp_website, string comp_salesEmail, string comp_supportEmail, string comp_phone, string comp_ownername, string comp_ownertitle, string comp_ownerEmail, string comp_Facebook, string comp_blog, string comp_youtube,
         string comp_linkedin, string comp_twitter, string comp_gplus, string comp_FBlike, string comp_alexa, int Isactive, string comp_logopath, string comp_SMTPserver, string comp_port, string comp_login, string comp_password, int comp_SSL, string comp_add1, string comp_add2, string comp_city,
         string comp_state, string comp_zipcode, string comp_country, int Isprimary)
        {
            try
            {
                bool billphonestatus;
                SqlParameter[] addbillphone = new SqlParameter[32];
                addbillphone[0] = new SqlParameter("@CompanyId", SqlDbType.Int);
                addbillphone[0].Value = comp_id;
                addbillphone[1] = new SqlParameter("@CompanyName", SqlDbType.NVarChar);
                addbillphone[1].Value = comp_name;
                addbillphone[2] = new SqlParameter("@CompanyConnectionstring", SqlDbType.NVarChar);
                addbillphone[2].Value = comp_connectionstring;
                addbillphone[3] = new SqlParameter("@CompanyWebsite", SqlDbType.NVarChar);
                addbillphone[3].Value = comp_website;
                addbillphone[4] = new SqlParameter("@SalesEmail", SqlDbType.NVarChar);
                addbillphone[4].Value = comp_salesEmail;
                addbillphone[5] = new SqlParameter("@SupportEmail", SqlDbType.NVarChar);
                addbillphone[5].Value = comp_supportEmail;
                addbillphone[6] = new SqlParameter("@PhoneNo", SqlDbType.NVarChar);
                addbillphone[6].Value = comp_phone;
                addbillphone[7] = new SqlParameter("@CompanyOwnerName", SqlDbType.NVarChar);
                addbillphone[7].Value = comp_ownername;
                addbillphone[8] = new SqlParameter("@CompanyOwnerTitle", SqlDbType.NVarChar);
                addbillphone[8].Value = comp_ownertitle;
                addbillphone[9] = new SqlParameter("@CompanyOwnerEmail", SqlDbType.NVarChar);
                addbillphone[9].Value = comp_ownerEmail;
                addbillphone[10] = new SqlParameter("@CompanyFacebook", SqlDbType.Text);
                addbillphone[10].Value = comp_Facebook;
                addbillphone[11] = new SqlParameter("@CompanyBlog", SqlDbType.Text);
                addbillphone[11].Value = comp_blog;
                addbillphone[12] = new SqlParameter("@CompanyYoutube", SqlDbType.Text);
                addbillphone[12].Value = comp_youtube;
                addbillphone[13] = new SqlParameter("@CompanyLinkedin", SqlDbType.Text);
                addbillphone[13].Value = comp_linkedin;
                addbillphone[14] = new SqlParameter("@CompanyTwitter", SqlDbType.Text);
                addbillphone[14].Value = comp_twitter;
                addbillphone[15] = new SqlParameter("@CompanyGooglePlus", SqlDbType.Text);
                addbillphone[15].Value = comp_gplus;
                addbillphone[16] = new SqlParameter("@CompanyFacebookLike", SqlDbType.Text);
                addbillphone[16].Value = comp_FBlike;
                addbillphone[17] = new SqlParameter("@CompanyAlexa", SqlDbType.Text);
                addbillphone[17].Value = comp_alexa;
                addbillphone[18] = new SqlParameter("@Isactive", SqlDbType.Bit);
                addbillphone[18].Value = Isactive;
                addbillphone[19] = new SqlParameter("@LogoPath", SqlDbType.NVarChar);
                addbillphone[19].Value = comp_logopath;
                addbillphone[20] = new SqlParameter("@SMTPServer", SqlDbType.NVarChar);
                addbillphone[20].Value = comp_SMTPserver;
                addbillphone[21] = new SqlParameter("@SMTPPort", SqlDbType.NVarChar);
                addbillphone[21].Value = comp_port;
                addbillphone[22] = new SqlParameter("@SMTPLogin", SqlDbType.NVarChar);
                addbillphone[22].Value = comp_login;
                addbillphone[23] = new SqlParameter("@SMTPPassword", SqlDbType.NVarChar);
                addbillphone[23].Value = comp_password;
                addbillphone[24] = new SqlParameter("@SMTPSsl", SqlDbType.Bit);
                addbillphone[24].Value = comp_SSL;
                addbillphone[25] = new SqlParameter("@CompanyAddress1", SqlDbType.NVarChar);
                addbillphone[25].Value = comp_add1;
                addbillphone[26] = new SqlParameter("@CompanyAddress2", SqlDbType.NVarChar);
                addbillphone[26].Value = comp_add2;
                addbillphone[27] = new SqlParameter("@CompanyCity", SqlDbType.NVarChar);
                addbillphone[27].Value = comp_city;
                addbillphone[28] = new SqlParameter("@CompanyState", SqlDbType.NVarChar);
                addbillphone[28].Value = comp_state;
                addbillphone[29] = new SqlParameter("@CompanyZipCode", SqlDbType.NVarChar);
                addbillphone[29].Value = comp_zipcode;
                addbillphone[30] = new SqlParameter("@CompanyCountry", SqlDbType.NVarChar);
                addbillphone[30].Value = comp_country;
                addbillphone[31] = new SqlParameter("@IsPrimary", SqlDbType.Bit);
                addbillphone[31].Value = Isprimary;

                billphonestatus = clshelper.ExecuteNonQuery("USP_EditCompanyList", addbillphone);
                return billphonestatus;
            }
            catch (Exception)
            {
                throw;
            }

        }


        public DataTable GetComapanyDetailsById(int id)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] SearchUnivDetails = new SqlParameter[1];
                SearchUnivDetails[0] = new SqlParameter("@CompanyId", SqlDbType.Int);
                SearchUnivDetails[0].Value = id;

                dt = clshelper.DataTable("USP_GetComapanyDetailsById", SearchUnivDetails);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetAllComapanyList_Address(int compid, int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] SearchUnivDetails = new SqlParameter[6];
                SearchUnivDetails[0] = new SqlParameter("@CompanyId", SqlDbType.Int);
                SearchUnivDetails[0].Value = compid;
                SearchUnivDetails[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                SearchUnivDetails[1].Value = PageIndex;
                SearchUnivDetails[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                SearchUnivDetails[2].Value = PageSize;
                SearchUnivDetails[3] = new SqlParameter("@SortColumn", SqlDbType.Int);
                SearchUnivDetails[3].Value = SortColumn;
                SearchUnivDetails[4] = new SqlParameter("@SortDirection", SqlDbType.Int);
                SearchUnivDetails[4].Value = SortDirection;
                SearchUnivDetails[5] = new SqlParameter("@Searchtext", SqlDbType.NVarChar);
                SearchUnivDetails[5].Value = Searchtext;

                dt = clshelper.DataTable("USP_GetComapanyList_Address", SearchUnivDetails);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetComapanyList_Address_ID(int id)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] SearchUnivDetails = new SqlParameter[1];
                SearchUnivDetails[0] = new SqlParameter("@AddressId", SqlDbType.Int);
                SearchUnivDetails[0].Value = id;

                dt = clshelper.DataTable("USP_GetComapanyList_Address_Id", SearchUnivDetails);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool AddComapanyList_Address(int comp_id, string comp_add1, string comp_add2, string comp_city,
          string comp_state, string comp_zipcode, string comp_country, int Isprimary)
        {
            try
            {
                bool billphonestatus;
                SqlParameter[] addbillphone = new SqlParameter[8];
                addbillphone[0] = new SqlParameter("@CompanyId", SqlDbType.Int);
                addbillphone[0].Value = comp_id;
                addbillphone[1] = new SqlParameter("@CompanyAddress1", SqlDbType.NVarChar);
                addbillphone[1].Value = comp_add1;
                addbillphone[2] = new SqlParameter("@CompanyAddress2", SqlDbType.NVarChar);
                addbillphone[2].Value = comp_add2;
                addbillphone[3] = new SqlParameter("@CompanyCity", SqlDbType.NVarChar);
                addbillphone[3].Value = comp_city;
                addbillphone[4] = new SqlParameter("@CompanyState", SqlDbType.NVarChar);
                addbillphone[4].Value = comp_state;
                addbillphone[5] = new SqlParameter("@CompanyZipCode", SqlDbType.NVarChar);
                addbillphone[5].Value = comp_zipcode;
                addbillphone[6] = new SqlParameter("@CompanyCountry", SqlDbType.NVarChar);
                addbillphone[6].Value = comp_country;
                addbillphone[7] = new SqlParameter("@IsPrimary", SqlDbType.Bit);
                addbillphone[7].Value = Isprimary;



                billphonestatus = clshelper.ExecuteNonQuery("USP_AddCompanyList_Address", addbillphone);
                return billphonestatus;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public bool UpdateComapanyList_Address(int id, int comp_id, string comp_add1, string comp_add2, string comp_city,
          string comp_state, string comp_zipcode, string comp_country, int Isprimary)
        {
            try
            {
                bool billphonestatus;
                SqlParameter[] addbillphone = new SqlParameter[9];
                addbillphone[0] = new SqlParameter("@AddressId", SqlDbType.Int);
                addbillphone[0].Value = id;
                addbillphone[1] = new SqlParameter("@CompanyId", SqlDbType.Int);
                addbillphone[1].Value = comp_id;
                addbillphone[2] = new SqlParameter("@CompanyAddress1", SqlDbType.NVarChar);
                addbillphone[2].Value = comp_add1;
                addbillphone[3] = new SqlParameter("@CompanyAddress2", SqlDbType.NVarChar);
                addbillphone[3].Value = comp_add2;
                addbillphone[4] = new SqlParameter("@CompanyCity", SqlDbType.NVarChar);
                addbillphone[4].Value = comp_city;
                addbillphone[5] = new SqlParameter("@CompanyState", SqlDbType.NVarChar);
                addbillphone[5].Value = comp_state;
                addbillphone[6] = new SqlParameter("@CompanyZipCode", SqlDbType.NVarChar);
                addbillphone[6].Value = comp_zipcode;
                addbillphone[7] = new SqlParameter("@CompanyCountry", SqlDbType.NVarChar);
                addbillphone[7].Value = comp_country;
                addbillphone[8] = new SqlParameter("@IsPrimary", SqlDbType.Bit);
                addbillphone[8].Value = Isprimary;



                billphonestatus = clshelper.ExecuteNonQuery("USP_EditCompanyList_Address", addbillphone);
                return billphonestatus;
            }
            catch (Exception)
            {
                throw;
            }

        }
        #endregion


        #region Custom settings
        public DataTable GetCustomSettingDetials(int PageIndex, int PageSize, int SelectedValue, string Searchtext)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] SearchUnivDetails = new SqlParameter[4];
                SearchUnivDetails[0] = new SqlParameter("@PageIndex", SqlDbType.Int);
                SearchUnivDetails[0].Value = PageIndex;
                SearchUnivDetails[1] = new SqlParameter("@PageSize", SqlDbType.Int);
                SearchUnivDetails[1].Value = PageSize;
                SearchUnivDetails[2] = new SqlParameter("@SelectedValue", SqlDbType.Int);
                SearchUnivDetails[2].Value = SelectedValue;
                SearchUnivDetails[3] = new SqlParameter("@Searchtext", SqlDbType.NVarChar);
                SearchUnivDetails[3].Value = Searchtext;

                dt = clshelper.DataTable("USP_GetCustomSettingDetials", SearchUnivDetails);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool RemoveCustomSettingDetials(int Id, int SelectedValue)
        {
            bool add = false;
            try
            {
                SqlParameter[] Addcustomsetting = new SqlParameter[2];

                Addcustomsetting[0] = new SqlParameter("@Id", SqlDbType.Int);
                Addcustomsetting[0].Value = Id;
                Addcustomsetting[1] = new SqlParameter("@SelectedValue", SqlDbType.Int);
                Addcustomsetting[1].Value = SelectedValue;


                add = clshelper.ExecuteNonQuery("USP_RemoveCustomSettingDetials", Addcustomsetting);

                return add;
            }
            catch (Exception)
            {
                return add;
            }
        }


        public DataTable GetCustomSettingDetialsByID(int Id, int SelectedValue)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] SearchUnivDetails = new SqlParameter[2];
                SearchUnivDetails[0] = new SqlParameter("@Id", SqlDbType.Int);
                SearchUnivDetails[0].Value = Id;
                SearchUnivDetails[1] = new SqlParameter("@SelectedValue", SqlDbType.Int);
                SearchUnivDetails[1].Value = SelectedValue;


                dt = clshelper.DataTable("USP_GetCustomSettingDetials_ID", SearchUnivDetails);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool AddCustomSettingDetials(int UserId, int SelectedValue, string Editstring)
        {
            bool add = false;
            try
            {
                SqlParameter[] Updatecustomsetting = new SqlParameter[3];
                Updatecustomsetting[0] = new SqlParameter("@UserId", SqlDbType.Int);
                Updatecustomsetting[0].Value = UserId;
                Updatecustomsetting[1] = new SqlParameter("@SelectedValue", SqlDbType.Int);
                Updatecustomsetting[1].Value = SelectedValue;
                Updatecustomsetting[2] = new SqlParameter("@Editstring", SqlDbType.VarChar);
                Updatecustomsetting[2].Value = Editstring;

                add = clshelper.ExecuteNonQuery("USP_AddCustomSettingDetials", Updatecustomsetting);

                return add;
            }
            catch (Exception)
            {
                return add;
            }
        }
        public bool EditCustomSettingDetials(int Id, int SelectedValue, string Editstring)
        {
            bool update = false;
            try
            {
                SqlParameter[] Updatecustomsetting = new SqlParameter[3];
                Updatecustomsetting[0] = new SqlParameter("@Id", SqlDbType.Int);
                Updatecustomsetting[0].Value = Id;
                Updatecustomsetting[1] = new SqlParameter("@SelectedValue", SqlDbType.Int);
                Updatecustomsetting[1].Value = SelectedValue;
                Updatecustomsetting[2] = new SqlParameter("@Editstring", SqlDbType.VarChar);
                Updatecustomsetting[2].Value = Editstring;

                update = clshelper.ExecuteNonQuery("USP_EditCustomSettingDetials", Updatecustomsetting);

                return update;
            }
            catch (Exception)
            {
                return update;
            }
        }
        public bool CheckSpecialityName(int SpecialityId, string SpecialityName)
        {
            bool add = false;
            try
            {
                return clshelper.DataTable("USP_SpecialityName", new SqlParameter[] {
                new SqlParameter() { ParameterName = "@SpecialityId", Value = SpecialityId },
                 new SqlParameter() { ParameterName = "@SpecialityName", Value = SpecialityName },
                
            }).Rows.Count > 0 ? true : false;
            }
            catch (Exception)
            {
                return add;
            }
        }

        #endregion


        #region Get Templates Admin Can Send
        public DataTable GetTemplatesAdminCanSend()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = clshelper.DataTable("USP_GetTemplatesAdminCanSend");
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Add new template from admin side in system
        public bool AddEmailTempalteInSystem(string TemplateName, string TemplateDesc, string Templatesub, int Isadmin)
        {
            try
            {
                bool phonestatus;
                SqlParameter[] addphone = new SqlParameter[5];
                addphone[0] = new SqlParameter("@TemplateName", SqlDbType.NVarChar);
                addphone[0].Value = TemplateName;
                addphone[1] = new SqlParameter("@TemplateDesc", SqlDbType.Text);
                addphone[1].Value = TemplateDesc;
                addphone[2] = new SqlParameter("@TemplateSubject", SqlDbType.NVarChar);
                addphone[2].Value = Templatesub;
                addphone[3] = new SqlParameter("@IsAdmin", SqlDbType.Bit);
                addphone[3].Value = Isadmin;
                addphone[4] = new SqlParameter("@Result", SqlDbType.Bit);
                addphone[4].Direction = ParameterDirection.Output;

                phonestatus = clshelper.ExecuteNonQuery("USP_AddNewEmailTemplate", addphone);

                return Convert.ToBoolean(addphone[4].Value);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion


        #region GetAllCustomPageDetails

        public DataTable GetAllCustomPageDetails()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = clshelper.DataTable("USP_GetAllCustomPageDetails");
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetAllCustomPageDetailsById(int Id)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] SearchUnivDetails = new SqlParameter[1];
                SearchUnivDetails[0] = new SqlParameter("@Id", SqlDbType.Int);
                SearchUnivDetails[0].Value = Id;
                dt = clshelper.DataTable("USP_GetAllCustomPageDetailsById", SearchUnivDetails);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool AddEditPageContentById(int Id, string PageName, string PageDesc)
        {
            bool result = false;
            try
            {

                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@Id", SqlDbType.Int);
                strParameter[0].Value = Id;
                strParameter[1] = new SqlParameter("@PageName", SqlDbType.NVarChar);
                strParameter[1].Value = PageName;
                strParameter[2] = new SqlParameter("@PageDesc", SqlDbType.VarChar);
                strParameter[2].Value = PageDesc;


                result = clshelper.ExecuteNonQuery("Usp_AddEditPageContentById", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }



        #endregion


        #region Merge Doctor
        public bool MergeUser(int doctor1id, int doctor2id)
        {
            try
            {
                SqlParameter[] strmergeuser = new SqlParameter[2];
                strmergeuser[0] = new SqlParameter("@doctor1id", SqlDbType.Int);
                strmergeuser[0].Value = doctor1id;
                strmergeuser[1] = new SqlParameter("@doctor2id", SqlDbType.Int);
                strmergeuser[1].Value = doctor2id;
                bool result = clshelper.ExecuteNonQuery("USP_MergeUser", strmergeuser);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Insurance

        public bool InsertUpdateInsurance(int InsuranceId, string Name, string Logo, string Description,
            string Link)
        {
            try
            {
                bool result;
                SqlParameter[] addParameter = new SqlParameter[5];
                addParameter[0] = new SqlParameter("@InsuranceId", SqlDbType.Int);
                addParameter[0].Value = InsuranceId;
                addParameter[1] = new SqlParameter("@Name", SqlDbType.VarChar);
                addParameter[1].Value = Name;
                addParameter[2] = new SqlParameter("@Logo", SqlDbType.VarChar);
                addParameter[2].Value = Logo;
                addParameter[3] = new SqlParameter("@Description", SqlDbType.Text);
                addParameter[3].Value = Description;
                addParameter[4] = new SqlParameter("@Link", SqlDbType.VarChar);
                addParameter[4].Value = Link;

                result = clshelper.ExecuteNonQuery("USP_InsertUpdateInsurance", addParameter);

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetAllInsuranceDetail(int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] addParameter = new SqlParameter[5];
                addParameter[0] = new SqlParameter("@PageIndex", SqlDbType.Int);
                addParameter[0].Value = PageIndex;
                addParameter[1] = new SqlParameter("@PageSize", SqlDbType.Int);
                addParameter[1].Value = PageSize;
                addParameter[2] = new SqlParameter("@SortColumn", SqlDbType.Int);
                addParameter[2].Value = SortColumn;
                addParameter[3] = new SqlParameter("@SortDirection", SqlDbType.Int);
                addParameter[3].Value = SortDirection;
                addParameter[4] = new SqlParameter("@Searchtext", SqlDbType.NVarChar);
                addParameter[4].Value = Searchtext;
                dt = clshelper.DataTable("USP_GetAllInsuranceDetail", addParameter);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetInsuranceDetailById(int InsuranceId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] addParam = new SqlParameter[1];
                addParam[0] = new SqlParameter("@InsuranceId", SqlDbType.Int);
                addParam[0].Value = InsuranceId;
                dt = clshelper.DataTable("USP_GetInsuranceDetailById", addParam);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public bool DeleteInsuranceDetail(int InsuranceId)
        {
            bool result = false;
            try
            {
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@InsuranceId", SqlDbType.Int);
                strParameter[0].Value = InsuranceId;
                result = clshelper.ExecuteNonQuery("USP_DeleteInsurance", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public int GetCountOfInsurance(string Searchtext)
        {
            int Count = 0;
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] addParam = new SqlParameter[1];
                addParam[0] = new SqlParameter("@Searchtext", SqlDbType.VarChar);
                addParam[0].Value = Searchtext;
                Count = Convert.ToInt32(clshelper.ExecuteScalar("USP_GetCountOfInsurance", addParam));
                return Count;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region FeatureMaster

        public DataTable GetAllFeatureMaster()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = clshelper.DataTable("USP_GetAllFeatureMaster");
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int InsertUpdateFeatureMaster(FeatureMaster obj)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[5];
                strParameter[0] = new SqlParameter("@Id", SqlDbType.Int);
                strParameter[0].Value = obj.Id;
                strParameter[1] = new SqlParameter("@Feature", SqlDbType.NVarChar);
                strParameter[1].Value = obj.Feature;
                strParameter[2] = new SqlParameter("@IsActive", SqlDbType.Bit);
                strParameter[2].Value = obj.IsActive;
                strParameter[3] = new SqlParameter("@CreatedBy", SqlDbType.Int);
                strParameter[3].Value = obj.CreatedBy;
                strParameter[4] = new SqlParameter("@FMId", SqlDbType.Int);
                strParameter[4].Direction = ParameterDirection.Output;
                clshelper.DataTable("USP_InsertUpdateFeatureMaster", strParameter);
                int result = Convert.ToInt32(strParameter[4].Value);
                return result;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("clsAdmin--InsertUpdateFeatureMaster", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public DataTable GetFeatureMasterById(int FeatureId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] addParam = new SqlParameter[1];
                addParam[0] = new SqlParameter("@Id", SqlDbType.Int);
                addParam[0].Value = FeatureId;
                dt = clshelper.DataTable("USP_GetFeatureMasterById", addParam);
                return dt;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("clsAdmin--GetFeatureMasterById", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public bool CheckFeatureMasterExist(string Feature)
        {
            try
            {
                bool status = false;
                DataTable dt = new DataTable();
                SqlParameter[] addParam = new SqlParameter[1];
                addParam[0] = new SqlParameter("@Feature", SqlDbType.NVarChar);
                addParam[0].Value = Feature;
                dt = clshelper.DataTable("USP_CheckFeatureMasterExist", addParam);
                if (dt.Rows.Count > 0)
                    status = true;
                return status;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("clsAdmin--GetFeatureMasterById", ex.Message, ex.StackTrace);
                throw;
            }
        }

        #endregion

        #region Account Feature Setting

        public int InsertUpdateAccountFeatureSetting(AccountFeatureSetting obj,bool IsUpdateByAdmin)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[8];
                strParameter[0] = new SqlParameter("@Id", SqlDbType.Int);
                strParameter[0].Value = obj.id;
                strParameter[1] = new SqlParameter("@AccountId", SqlDbType.Int);
                strParameter[1].Value = obj.AccountId;
                strParameter[2] = new SqlParameter("@FeatureMasterId", SqlDbType.Int);
                strParameter[2].Value = obj.FeatureMasterId;
                strParameter[3] = new SqlParameter("@AdminStatus", SqlDbType.Bit);
                strParameter[3].Value = obj.AdminStatus;
                obj.DentistStatus = (IsUpdateByAdmin == true) ? true : obj.DentistStatus;
                strParameter[4] = new SqlParameter("@DentistStatus", SqlDbType.Bit);
                strParameter[4].Value = obj.DentistStatus;
                strParameter[5] = new SqlParameter("@CreatedBy", SqlDbType.Int);
                strParameter[5].Value = obj.CreatedBy;
                strParameter[6] = new SqlParameter("@AFSId", SqlDbType.Int);
                strParameter[6].Direction = ParameterDirection.Output;
                strParameter[7] = new SqlParameter("@IsUpdateByAdmin", SqlDbType.Bit);
                strParameter[7].Value = IsUpdateByAdmin;

                clshelper.DataTable("USP_InsertUpdateAccountFeatureSetting", strParameter);
                int result = Convert.ToInt32(strParameter[6].Value);
                return result;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("clsAdmin--InsertUpdateAccountFeatureSetting", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public DataTable GetAccountList(int Index,int Size,string Searchtext)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] addParam = new SqlParameter[3];
                addParam[0] = new SqlParameter("@index", SqlDbType.Int);
                addParam[0].Value = Index;
                addParam[1] = new SqlParameter("@size", SqlDbType.Int);
                addParam[1].Value = Size;
                addParam[2] = new SqlParameter("@searchtext", SqlDbType.NVarChar);
                addParam[2].Value = Searchtext;
                dt = clshelper.DataTable("USP_GetAccountList", addParam);
                return dt;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("clsAdmin--GetAccountList", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public DataTable GetAccountFeatureSettingByAccountId(int AccountId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] addParam = new SqlParameter[1];
                addParam[0] = new SqlParameter("@AccountId", SqlDbType.Int);
                addParam[0].Value = AccountId;
                dt = clshelper.DataTable("USP_GetAccountFeatureSettingByAccountId", addParam);
                return dt;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("clsAdmin--GetAccountFeatureSettingByAccountId", ex.Message, ex.StackTrace);
                throw;
            }
        }

        #endregion

        public DataTable DentistLastSync()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = clshelper.DataTable("USP_LastSyncDate");
                return dt;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("clsAdmin--DentistLastSync", ex.Message, ex.StackTrace);
                throw;
            }
        }


        public DataTable GetEmailVerificationListing(EmailFilter objEmailFilter)
        {
            try
            {
                DataTable dt = new DataTable();                
                SqlParameter[] alluser = new SqlParameter[5];
                alluser[0] = new SqlParameter("@PageIndex", SqlDbType.Int);
                alluser[0].Value = objEmailFilter.PageIndex;
                alluser[1] = new SqlParameter("@PageSize", SqlDbType.Int);
                alluser[1].Value = objEmailFilter.PageSize;
                alluser[2] = new SqlParameter("@Searchtext", SqlDbType.NVarChar);
                alluser[2].Value = objEmailFilter.Searchtext;                
                alluser[3] = new SqlParameter("@SortColumn", SqlDbType.Int);
                alluser[3].Value = objEmailFilter.SortColumn;
                alluser[4] = new SqlParameter("@SortDirection", SqlDbType.Int);
                alluser[4].Value = objEmailFilter.SortDirection;
               return dt = clshelper.DataTable("USP_GetEmailVerificationReport", alluser);                
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("clsAdmin--GetEmailVerificationListing", ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
