using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using DataAccessLayer.Common;
using BO.Models;
using BO.ViewModel;
using static BO.Enums.Common;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using System.Configuration;

namespace DataAccessLayer.ColleaguesData
{
    public class clsColleaguesData
    {
        clsHelper clshelper = new clsHelper();
        clsCommon ObjCommon = new clsCommon();

        public string GetAccountID(int locationid)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@locationid", SqlDbType.VarChar);
                strParameter[0].Value = locationid;
                string AccountID = clshelper.ExecuteScalar("SP_Get_AccountID", strParameter);
                return AccountID;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable LoginUserBasedOnUsernameandPassword(string UserName, string Password)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@Username", SqlDbType.VarChar);
                strParameter[0].Value = UserName;
                strParameter[1] = new SqlParameter("@Password", SqlDbType.VarChar);
                strParameter[1].Value = Password;


                dt = clshelper.DataTable("USP_Login_UserBasedOnUsernameandPassword", strParameter);

                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool UpdateLocationIdOfDoctor(int UserId, int LocationId)
        {
            bool UpdateStatus = false;
            SqlParameter[] addphone = new SqlParameter[2];
            addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
            addphone[0].Value = UserId;
            addphone[1] = new SqlParameter("@LocationId", SqlDbType.Int);
            addphone[1].Value = LocationId;


            UpdateStatus = clshelper.ExecuteNonQuery("USP_UpdateLocationIdOfDoctor", addphone);

            return UpdateStatus;

        }

        public DataTable GetLocationIdforSyncinAppointmentSide(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] addphone = new SqlParameter[1];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;

                dt = clshelper.DataTable("Usp_GetLocationIdForSyncAppointmentSide", addphone);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData - GetLocationIdforSyncinAppointmentSide", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool InsertPerioTeableValue(string FirstName, string LastName, string Email, string Phone)
        {
            bool result = false;
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[4];
                sqlpara[0] = new SqlParameter("@FirstName", SqlDbType.VarChar);
                sqlpara[0].Value = FirstName;
                sqlpara[1] = new SqlParameter("@LastName", SqlDbType.VarChar);
                sqlpara[1].Value = LastName;
                sqlpara[2] = new SqlParameter("@Email", SqlDbType.VarChar);
                sqlpara[2].Value = Email;
                sqlpara[3] = new SqlParameter("@Phone", SqlDbType.VarChar);
                sqlpara[3].Value = Phone;

                result = clshelper.ExecuteNonQuery("USP_InsertPerioTable", sqlpara);
                return result;
            }
            catch (Exception EX)
            {

                throw;
            }
        }
        public DataTable GetLocationSyncStatus(int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[1];
                sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlpara[0].Value = UserId;
                return clshelper.DataTable("Usp_GetSyncDataForAppointmentSide", sqlpara);
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesdata - GetLocationSyncStatus", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetCommunicationHistoryForPatient(int UserId, int PatientId, int PageIndex, int PageSize, int SortCoumn, int SortDirection)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[6];
                sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlpara[0].Value = UserId;
                sqlpara[1] = new SqlParameter("@PatientId", SqlDbType.Int);
                sqlpara[1].Value = PatientId;
                sqlpara[2] = new SqlParameter("@PageIndex", SqlDbType.Int);
                sqlpara[2].Value = PageIndex;
                sqlpara[3] = new SqlParameter("@PageSize", SqlDbType.Int);
                sqlpara[3].Value = PageSize;
                sqlpara[4] = new SqlParameter("@SortColumn", SqlDbType.Int);
                sqlpara[4].Value = SortCoumn;
                sqlpara[5] = new SqlParameter("@SortDirection", SqlDbType.Int);
                sqlpara[5].Value = SortDirection;
                return clshelper.DataTable("GetCommunicationForPatientHistoy", sqlpara);

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public int InsertDentrixConnector(int UserId, string GUIDKey, string LocationId, int IntegrationId = 1)
        {
            try
            {
                SqlParameter[] para = new SqlParameter[5];
                para[0] = new SqlParameter("@UserId", SqlDbType.Int);
                para[0].Value = UserId;
                para[1] = new SqlParameter("@GUIDKey", SqlDbType.NVarChar);
                para[1].Value = GUIDKey;
                para[2] = new SqlParameter("@LocationId", SqlDbType.Int);
                para[2].Value = LocationId;
                para[3] = new SqlParameter("@IntegrationId", SqlDbType.Int);
                para[3].Value = IntegrationId;
                para[4] = new SqlParameter("@ConnectorId", SqlDbType.Int);
                para[4].Value = ParameterDirection.Output;
                clshelper.DataTable("Usp_InsertDentrixConnector", para);
                int ConnectorId = Convert.ToInt32(para[3].Value);
                return ConnectorId;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("Insert Dentrix Connector Time Error ", Ex.Message, Ex.StackTrace);
                return 0;
            }
        }
        public bool UpdateDentrixProvideId(int UserId, string DentrixProviderId)
        {
            try
            {
                SqlParameter[] para = new SqlParameter[2];
                para[0] = new SqlParameter("@UserId", SqlDbType.Int);
                para[0].Value = UserId;
                para[1] = new SqlParameter("@DentrixProviderId", SqlDbType.NVarChar);
                para[1].Value = DentrixProviderId;
                clshelper.DataTable("UpdateDentrixProvideId", para);
                return true;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("Insert Dentrix Connector Time Error ", Ex.Message, Ex.StackTrace);
                return false;
            }
        }
        public DataTable GetDentrixConnectorValuesofUser(int UserId, int IntegrationId = 1)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] para = new SqlParameter[2];
                para[0] = new SqlParameter("@UserId", SqlDbType.Int);
                para[0].Value = UserId;
                para[1] = new SqlParameter("@IntegrationId", SqlDbType.Int);
                para[1].Value = IntegrationId;

                dt = clshelper.DataTable("Usp_GetDentrixConnectorData", para);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("GetDentrixConnectorValue", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public bool DeleteDentrixConnectorId(int DentrixConnectorId)
        {
            try
            {
                bool result = false;
                SqlParameter[] para = new SqlParameter[1];
                para[0] = new SqlParameter("@DentrixConnectorId", SqlDbType.Int);
                para[0].Value = DentrixConnectorId;

                result = clshelper.ExecuteNonQuery("Usp_DeleteDentrixConnector", para);
                return result;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("Delete Dentrix Connector Time Error", Ex.Message, Ex.StackTrace);
                return false;
            }
        }
        public DataTable GetDentrixConnectorOfDoctor(int UserId)
        {
            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] para = new SqlParameter[1];
                para[0] = new SqlParameter("@UserId", SqlDbType.Int);
                para[0].Value = UserId;

                dt = clshelper.DataTable("Usp_GetDentrixConnectorOfDoctor", para);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("Usp_GetDentrixConnectorOfDoctor  Connector Time Error", Ex.Message, Ex.StackTrace);
                return null;
            }
        }
        public DataTable GetNewReferralMessageViewByRefCardId(int ReferralCardId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] Para = new SqlParameter[1];
                Para[0] = new SqlParameter("@ReferralCardID", SqlDbType.Int);
                Para[0].Value = ReferralCardId;

                dt = clshelper.DataTable("USP_GetReferralCategoryValues", Para);
                return dt;
            }
            catch (Exception EX)
            {
                ObjCommon.InsertErrorLog("ClsColleagues--GetNewReferralMessageViewByRefCardId", EX.Message, EX.StackTrace);
                throw;
            }
        }
        public DataTable SendPatientHistoryMessageforall(int PatientId, int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[2];
                sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlpara[0].Value = UserId;
                sqlpara[1] = new SqlParameter("@PatientId", SqlDbType.Int);
                sqlpara[1].Value = PatientId;

                dt = clshelper.DataTable("Usp_SendPatientHistoryMessage", sqlpara);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static DataTable IsFileOwnedByDoctor(int FileId, int UserId, BO.Enums.Common.FileFromFolder FileType, string FileName)
        {
            DataTable dt = null;
            clsHelper clshelper = new clsHelper();
            SqlParameter[] sqlpara = new SqlParameter[4];
            sqlpara[0] = new SqlParameter("@FileId", SqlDbType.Int);
            sqlpara[0].Value = FileId;
            sqlpara[1] = new SqlParameter("@UserId", SqlDbType.Int);
            sqlpara[1].Value = UserId;
            sqlpara[2] = new SqlParameter("@FileType", SqlDbType.Int);
            sqlpara[2].Value = FileType;
            sqlpara[3] = new SqlParameter("@FileName", SqlDbType.NVarChar);
            sqlpara[3].Value = FileName;

            dt = clshelper.DataTable("USP_CheckFileOwnedByUser", sqlpara);
            return dt;
        }

        public DataSet GetColleaguesCommunicationbyDoctor(int UserId, int PatientId)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlParameter[] sqlpara = new SqlParameter[2];
                sqlpara[0] = new SqlParameter("@Userid", SqlDbType.Int);
                sqlpara[0].Value = UserId;
                sqlpara[1] = new SqlParameter("@PatientId", SqlDbType.Int);
                sqlpara[1].Value = PatientId;
                ds = clshelper.GetDatasetData("Usp_GetDoctorCommunications", sqlpara);
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public DataSet GetColleagueMessageFormDoctorbyMessageId(int MessageId)
        {
            try
            {
                DataSet dt = new DataSet();
                SqlParameter[] sqlpara = new SqlParameter[1];
                sqlpara[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                sqlpara[0].Value = MessageId;
                dt = clshelper.GetDatasetData("Usp_GetColleagueMessageforDoctorbyMessageId", sqlpara);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }

        //PRCSP-511 Function user for get the website for team member and also their Parent website.
        public DataSet GetSecondaryWebsitelistByParentUserId(int UserId)
        {

            try
            {
                DataSet ds = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Userid", SqlDbType.Int);
                strParameter[0].Value = UserId;

                ds = clshelper.GetDatasetData("Usp_GetWebsitesBYParentUserId", strParameter);
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public DataTable GetDoctorsByLocaiton(int LocationId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@LocationId", SqlDbType.Int);
                strParameter[0].Value = LocationId;

                dt = clshelper.DataTable("GetAllDoctorByLocation", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public DataTable GetTimeZones()
        {
            DataTable dt = new DataTable();
            dt = clshelper.DataTable("Usp_GetTimeZones", null);
            return dt;
        }

        public DataTable GetDoctorLocaitonById(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Userid", SqlDbType.Int);
                strParameter[0].Value = UserId;

                dt = clshelper.DataTable("Usp_GetLocationOfDoctorbyUserId", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        #region Methods For Address
        public DataTable GetAddressDetails(int UserId, int Usertype)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@Userid", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@Usertype", SqlDbType.Int);
                strParameter[1].Value = Usertype;



                dt = clshelper.DataTable("USP_GetAddressDetails", strParameter);


                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetTimeZoneValue(int timezoneId)
        {
            DataTable ds = new DataTable();
            SqlParameter[] strParameter = new SqlParameter[1];
            strParameter[0] = new SqlParameter("@TimezoneId", SqlDbType.Int);
            strParameter[0].Value = timezoneId;

            return clshelper.ExecuteScalar("USP_GetTimeZoneValueBasedonTimeZoneId", strParameter);
        }

        public DataTable GetLocationOfColleagues(int AddressInfoId)
        {//DD
            try
            {
                DataTable ds = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@AddressInfoId", SqlDbType.Int);
                strParameter[0].Value = AddressInfoId;

                ds = clshelper.DataTable("USP_GetLocationOfColleagues", strParameter);


                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Get Message Attchement list by Messageid
        public DataTable GetColleagueMessageAttchmentListbyId(int MessageId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                strParameter[0].Value = MessageId;

                dt = clshelper.DataTable("Usp_GetAttachmentlistofMessagebyId", strParameter);


                return dt;

            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        #endregion

        public DataTable GetAddressDetailsByAddressInfoID(int AddressInfoID)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@AddressInfoID", SqlDbType.Int);
                strParameter[0].Value = AddressInfoID;

                dt = clshelper.DataTable("USP_GetAddressDetails_By_AddressInfoID", strParameter);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static DataTable GetExtrnalPMSIdByAccountId(int UserId, string EPMSId, int LocationId)
        {
            try
            {
                DataTable dt = new DataTable();
                clsHelper clshelper = new clsHelper();
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@ExternalPMSId", SqlDbType.NVarChar);
                strParameter[1].Value = EPMSId;
                strParameter[2] = new SqlParameter("@LocationId", SqlDbType.Int);
                strParameter[2].Value = LocationId;
                dt = clshelper.DataTable("USP_GetExtrnalPMSIdByAccountId", strParameter);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        //Olivia changes on 10-04-2018 for Dr. Lan Allan's website. - DLADCETK-1
        public int UpdateAddressDetailsByAddressInfoID(int AddressInfoID, string ExactAddress, string Address2, string City, string State, string Country, string Zipcode, int UserId, int ContactType, string Email, string Phone, string Fax, string Location, int TimezoneId, string Mobile, string ExternalPMSId, string SchedulingLink)
        {//DD Changes
            try
            {
                int returnAddressInfoId;
                SqlParameter[] addphone = new SqlParameter[17];
                addphone[0] = new SqlParameter("@AddressInfoID", SqlDbType.Int);
                addphone[0].Value = AddressInfoID;
                addphone[1] = new SqlParameter("@ExactAddress", SqlDbType.NVarChar);
                addphone[1].Value = ExactAddress;
                addphone[2] = new SqlParameter("@Address2", SqlDbType.NVarChar);
                addphone[2].Value = Address2;
                addphone[3] = new SqlParameter("@City", SqlDbType.NVarChar);
                addphone[3].Value = City;
                addphone[4] = new SqlParameter("@State", SqlDbType.NVarChar);
                addphone[4].Value = State;
                addphone[5] = new SqlParameter("@Country", SqlDbType.NVarChar);
                addphone[5].Value = Country;
                addphone[6] = new SqlParameter("@Zipcode", SqlDbType.NVarChar);
                addphone[6].Value = Zipcode;
                addphone[7] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[7].Value = UserId;
                addphone[8] = new SqlParameter("@ContactType", SqlDbType.Int);
                addphone[8].Value = ContactType;

                addphone[9] = new SqlParameter("@Email", SqlDbType.NVarChar);
                addphone[9].Value = Email;
                addphone[10] = new SqlParameter("@Phone", SqlDbType.NVarChar);
                addphone[10].Value = Phone;
                addphone[11] = new SqlParameter("@Fax", SqlDbType.NVarChar);
                addphone[11].Value = Fax;
                addphone[12] = new SqlParameter("@Location", SqlDbType.NVarChar);
                addphone[12].Value = Location;
                addphone[13] = new SqlParameter("@TimezoneId", SqlDbType.Int);
                addphone[13].Value = TimezoneId;
                addphone[14] = new SqlParameter("@Mobile", SqlDbType.NVarChar);
                addphone[14].Value = Mobile;
                addphone[15] = new SqlParameter("@ExternalPMSId", SqlDbType.NVarChar);
                addphone[15].Value = ExternalPMSId;
                addphone[16] = new SqlParameter("@SchedulingLink", SqlDbType.NVarChar);
                addphone[16].Value = SchedulingLink;
                string strReturnAddressInfoId = clshelper.ExecuteScalar("USP_InsertUpdateAddressDetails_By_AddressInfoID", addphone);

                returnAddressInfoId = !string.IsNullOrEmpty(strReturnAddressInfoId) ? Convert.ToInt32(strReturnAddressInfoId) : 0;
                return returnAddressInfoId;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetDoctorDetailsByEmail(string Email)
        {
            SqlParameter[] strParameter = new SqlParameter[1];
            strParameter[0] = new SqlParameter("@email", Email);

            return clshelper.DataTable("getDoctorDetailsByEmail", strParameter);
        }

        public DataTable GetDoctorDetailsByUsername(string Username)
        {
            SqlParameter[] strParameter = new SqlParameter[1];
            strParameter[0] = new SqlParameter("@username", Username);

            return clshelper.DataTable("getDoctorDetailsByUsername", strParameter);
        }

        public DataTable GetDoctorFromTempByEmail(string Email)
        {
            SqlParameter[] strParameter = new SqlParameter[1];
            strParameter[0] = new SqlParameter("@email", Email);

            return clshelper.DataTable("GetDoctorFromTempByEmail", strParameter);
        }

        public bool RemoveDoctorAddressDetailsByAddressInfoID(int Id)
        {

            try
            {
                //bool UpdateStatus = false;

                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[1];
                addphone[0] = new SqlParameter("@AddressInfoId", SqlDbType.Int);
                addphone[0].Value = Id;
                UpdateStatus = clshelper.ExecuteNonQuery("USP_RemoveDoctorAddressDetailsByAddressInfoID", addphone);
                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        DateTime BeforCall;
        DateTime AfterCall;
        #region Methods For Socila Media
        public DataTable GetSocialMediaDetailsForColleagues(int UserId, int StartIndex, int EndSize, string searchtext, int SortColumn, int SortDirection)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[6];
                strParameter[0] = new SqlParameter("@userid", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@index", SqlDbType.Int);
                strParameter[1].Value = StartIndex;
                strParameter[2] = new SqlParameter("@size", SqlDbType.Int);
                strParameter[2].Value = EndSize;
                strParameter[3] = new SqlParameter("@searchtext", SqlDbType.NVarChar);
                strParameter[3].Value = searchtext;

                strParameter[4] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[4].Value = SortColumn;
                strParameter[5] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[5].Value = SortDirection;


                BeforCall = DateTime.Now;
                dt = clshelper.DataTable("USP_GetColleaguesSocialMediaDetails", strParameter);
                BeforCall = DateTime.Now;

                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("On DLL---Before Getting data :" + BeforCall + "After Getting Data" + BeforCall, Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetSocialMediaDetailByUserId(int Userid)
        {
            try
            {
                SqlParameter[] GetColleagueParams = new SqlParameter[1];

                GetColleagueParams[0] = new SqlParameter("@Userid", SqlDbType.Int);
                GetColleagueParams[0].Value = Userid;


                return clshelper.DataTable("USP_GetSocialMediaDetails_By_UserId", GetColleagueParams);

            }
            catch (Exception)
            {
                throw;
            }

        }
        public bool UpdateSocialMediaDetailsByUserId(int UserId, string FacebookUrl, string LinkedinUrl, string TwitterUrl, string InstagramUrl, string GoogleplusUrl, string YoutubeUrl, string PinterestUrl, string BlogUrl, string YelpUrl)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[10];
                addphone[0] = new SqlParameter("@Userid", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@FacebookUrl", SqlDbType.NVarChar);
                addphone[1].Value = FacebookUrl;
                addphone[2] = new SqlParameter("@LinkedinUrl", SqlDbType.NVarChar);
                addphone[2].Value = LinkedinUrl;
                addphone[3] = new SqlParameter("@TwitterUrl", SqlDbType.NVarChar);
                addphone[3].Value = TwitterUrl;
                addphone[4] = new SqlParameter("@GoogleplusUrl", SqlDbType.NVarChar);
                addphone[4].Value = GoogleplusUrl;
                addphone[5] = new SqlParameter("@YoutubeUrl", SqlDbType.NVarChar);
                addphone[5].Value = YoutubeUrl;
                addphone[6] = new SqlParameter("@PinterestUrl", SqlDbType.NVarChar);
                addphone[6].Value = PinterestUrl;
                addphone[7] = new SqlParameter("@BlogUrl", SqlDbType.NVarChar);
                addphone[7].Value = BlogUrl;
                addphone[8] = new SqlParameter("@YelpUrl", SqlDbType.NVarChar);
                addphone[8].Value = YelpUrl;
                addphone[9] = new SqlParameter("@InstagramUrl", SqlDbType.NVarChar);
                addphone[9].Value = InstagramUrl;


                UpdateStatus = clshelper.ExecuteNonQuery("USP_UpdateSocialMediaDetails_By_UserId", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion


        #region Transaction Hisotry
        public int InsertTransactionDetails(int UserId, DateTime PackageStartDate, DateTime PacKageEndDate, decimal Amount, bool Status, string CardType, string CardNumber, string Expdate, string PaymentTransactionId, string Cvv, bool IsActive)
        {

            DataTable dt = new DataTable();
            SqlParameter[] strParameter = new SqlParameter[10];
            strParameter[0] = new SqlParameter("@UserId", SqlDbType.VarChar);
            strParameter[0].Value = UserId;
            strParameter[1] = new SqlParameter("@PackageStartDate", SqlDbType.VarChar);
            strParameter[1].Value = PackageStartDate;
            strParameter[2] = new SqlParameter("@PackageEndDate", SqlDbType.VarChar);
            strParameter[2].Value = PacKageEndDate;
            strParameter[3] = new SqlParameter("@Amount", SqlDbType.VarChar);
            strParameter[3].Value = Amount;
            strParameter[4] = new SqlParameter("@Stetus", DbType.Boolean);
            strParameter[4].Value = Status;

            strParameter[5] = new SqlParameter("@CardNumber", SqlDbType.VarChar);
            strParameter[5].Value = CardNumber;

            strParameter[6] = new SqlParameter("@PaymentTransactioId", SqlDbType.VarChar);
            strParameter[6].Value = PaymentTransactionId;
            strParameter[7] = new SqlParameter("@CVV", SqlDbType.VarChar);
            strParameter[7].Value = Cvv;
            strParameter[8] = new SqlParameter("@TranasactionHisotryId", SqlDbType.Int);
            strParameter[8].Direction = ParameterDirection.Output;
            strParameter[9] = new SqlParameter("@IsAvtive", DbType.Boolean);
            strParameter[9].Value = IsActive;

            clshelper.DataTable("InsertUsertransactionDetails", strParameter);
            return Convert.ToInt32(strParameter[8].Value);
        }

        #region Reocurring Details                
        public int NewInsertUsertransactionDetails(int memberId, string Amount, string transactionId, string membershipId, string customerId, DateTime createdDate, string subscriptionType, string subscriptionId)
        {
            try
            {

                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[9];
                strParameter[0] = new SqlParameter("@memberId", SqlDbType.Int);
                strParameter[0].Value = memberId;
                strParameter[1] = new SqlParameter("@Amount", SqlDbType.NVarChar);
                strParameter[1].Value = Amount;
                strParameter[2] = new SqlParameter("@transactionId", SqlDbType.NVarChar);
                strParameter[2].Value = transactionId;
                strParameter[3] = new SqlParameter("@membershipId", SqlDbType.NVarChar);
                strParameter[3].Value = membershipId;
                strParameter[4] = new SqlParameter("@customerId", SqlDbType.NVarChar);
                strParameter[4].Value = customerId;
                strParameter[5] = new SqlParameter("@createdDate", SqlDbType.VarChar);
                strParameter[5].Value = createdDate;
                strParameter[6] = new SqlParameter("@subscriptionType", SqlDbType.NVarChar);
                strParameter[6].Value = subscriptionType;
                strParameter[7] = new SqlParameter("@subscriptionId", SqlDbType.NVarChar);
                strParameter[7].Value = subscriptionId;
                strParameter[8] = new SqlParameter("@returnId", SqlDbType.Int);
                strParameter[8].Direction = ParameterDirection.Output;

                clshelper.DataTable("NewInsertUsertransactionDetails", strParameter);
                return Convert.ToInt32(strParameter[8].Value);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        #endregion

        public DataTable GetLicenseDetialsByUserId(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;

                dt = clshelper.DataTable("Usp_GetLicenseDetailsByUserId", strParameter);
                return dt;

            }
            catch (Exception)
            {

                throw;
            }
        }
        public DataTable GetSecondaryWebsiteDetailsbyId(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                dt = clshelper.DataTable("Usp_GetSecondaryWebsitesbyUserId", strParameter);
                return dt;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public bool RemovePrimaryWebSite(int UserId)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[1];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;


                UpdateStatus = clshelper.ExecuteNonQuery("Usp_RemovePrimaryWebsiteUrl", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool RemoveSecondaryWebSite(int SecondaryId, int UserId)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@SecondaryId", SqlDbType.Int);
                addphone[1].Value = SecondaryId;

                UpdateStatus = clshelper.ExecuteNonQuery("Usp_RemoveSecondaryWebsiteUrl", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetTransactionHistoryByUserId(int UserID)
        {
            SqlParameter[] strParameter = new SqlParameter[1];
            strParameter[0] = new SqlParameter("@UserId", UserID);

            return clshelper.DataTable("GetTransactionHiostory", strParameter);

        }

        public bool UpdateNewUserID(int TransactionId, int NewUserId, bool IsActive)
        {
            SqlParameter[] addphone = new SqlParameter[3];
            addphone[0] = new SqlParameter("@NewUserId", SqlDbType.Int);
            addphone[0].Value = NewUserId;
            addphone[1] = new SqlParameter("@TransactionId", SqlDbType.NVarChar);
            addphone[1].Value = TransactionId;
            addphone[2] = new SqlParameter("@IsAvtive", DbType.Boolean);
            addphone[2].Value = IsActive;

            return clshelper.ExecuteNonQuery("UpdateUserIdOfTransactionHistoryAfterUserActive", addphone);
        }

        public bool UpdatePaymentStatusOfExistingUser(int UserID, bool IsPaid)
        {
            SqlParameter[] strParameter = new SqlParameter[2];
            strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
            strParameter[0].Value = UserID;
            strParameter[1] = new SqlParameter("@PaymentStatus", DbType.Boolean);
            strParameter[1].Value = IsPaid;
            return clshelper.ExecuteNonQuery("UpdatePaymentStatus", strParameter);
        }

        public DataTable getPaymentHistoryOnUserId(int userId)
        {
            SqlParameter[] strParameter = new SqlParameter[1];
            strParameter[0] = new SqlParameter("@userid", userId);

            return clshelper.DataTable("getPaymentHistoryOnUserId", strParameter);
        }
        #endregion

        #region Get Recuring Payment History
        public DataTable getNewPaymentHistoryOnUserId(int userId)
        {
            SqlParameter[] strParameter = new SqlParameter[1];
            strParameter[0] = new SqlParameter("@memberId", SqlDbType.Int);
            strParameter[0].Value = userId;

            return clshelper.DataTable("GetNewPaymentHistoryOnUserId", strParameter);
        }
        #endregion

        #region Update Recurring Payment On Cancel Operation
        public DataTable UpdateSubscriptionStatus(int memberId, string subscriptionId)
        {
            SqlParameter[] strParameter = new SqlParameter[2];
            strParameter[0] = new SqlParameter("@memberId", SqlDbType.Int);
            strParameter[0].Value = memberId;
            strParameter[1] = new SqlParameter("@subscriptionId", SqlDbType.NVarChar);
            strParameter[1].Value = subscriptionId;

            return clshelper.DataTable("UpdateSubscriptionStatus", strParameter);
        }

        public static DataTable CheckSubscriptionExistingUser(int userid, int events)
        {
            SqlParameter[] strParameter = new SqlParameter[1];
            strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
            strParameter[0].Value = userid;
            clsHelper objHelper = new clsHelper();
            return objHelper.DataTable("CheckSubscriptionExistingUser", strParameter);
        }

        public static bool InsertPaymentBlocking(int userid, bool isSuccess, int? RSId)
        {
            SqlParameter[] strParameter = new SqlParameter[3];
            strParameter[0] = new SqlParameter("@userid", SqlDbType.Int);
            strParameter[0].Value = userid;
            strParameter[1] = new SqlParameter("@isSuccess", SqlDbType.Bit);
            strParameter[1].Value = isSuccess;
            strParameter[2] = new SqlParameter("@RSid", SqlDbType.Int);
            strParameter[2].Value = RSId;
            clsHelper objHelper = new clsHelper();
            return objHelper.ExecuteNonQuery("Usp_InsertPaymentBlocking", strParameter);
        }

        #endregion

        public bool UpdateMemberDetailsByUserId(int UserId, string Operation, string UserData)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[3];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@Operation", SqlDbType.NVarChar);
                addphone[1].Value = Operation;
                addphone[2] = new SqlParameter("@UserData", SqlDbType.NVarChar);
                addphone[2].Value = UserData;




                UpdateStatus = clshelper.ExecuteNonQuery("USP_UpdateMemberDetails_By_UserId", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }


        #region Methods For Colleagues

        public bool RemoveColleagues(int UserId, int Colleagueid)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@ColleagueId", SqlDbType.Int);
                addphone[1].Value = Colleagueid;

                UpdateStatus = clshelper.ExecuteNonQuery("USP_RemoveColleagues", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetColleaguesDetailsForDoctor(int UserId, int StartIndex, int EndSize, string Searchtext, int SortColumn, int SortDirection)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[6];
                strParameter[0] = new SqlParameter("@user_id", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@index", SqlDbType.Int);
                strParameter[1].Value = StartIndex;
                strParameter[2] = new SqlParameter("@size", SqlDbType.Int);
                strParameter[2].Value = EndSize;
                strParameter[3] = new SqlParameter("@searchtext", SqlDbType.NVarChar);
                strParameter[3].Value = Searchtext;

                strParameter[4] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[4].Value = SortColumn;
                strParameter[5] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[5].Value = SortDirection;



                dt = clshelper.DataTable("USP_GetColleaguesDetailsOfDoctor", strParameter);


                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataSet GetColleaguesDetailsForDoctorWithSpeacilities(int UserId, int StartIndex, int EndSize, string Searchtext, int SortColumn, int SortDirection, string NewSearchText, int FilterBy)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[8];
                strParameter[0] = new SqlParameter("@user_id", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@index", SqlDbType.Int);
                strParameter[1].Value = StartIndex;
                strParameter[2] = new SqlParameter("@size", SqlDbType.Int);
                strParameter[2].Value = EndSize;
                strParameter[3] = new SqlParameter("@searchtext", SqlDbType.NVarChar);
                strParameter[3].Value = Searchtext;

                strParameter[4] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[4].Value = SortColumn;
                strParameter[5] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[5].Value = SortDirection;
                strParameter[6] = new SqlParameter("@NewSearchText", SqlDbType.NVarChar);
                strParameter[6].Value = NewSearchText;
                strParameter[7] = new SqlParameter("@FilterBy", SqlDbType.Int);
                strParameter[7].Value = FilterBy;

                ds = clshelper.GetDatasetData("USP_GetColleaguesDetailsOfDoctor", strParameter);


                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public DataTable GetSearchColleagueForDoctor(int UserId, int PageIndex, int PageSize, string Name, string Email, string phone, string City, string State, string Zipcode, string Institute, string SpecialtityList, string Keywords)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[12];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[1].Value = PageIndex;
                strParameter[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[2].Value = PageSize;
                strParameter[3] = new SqlParameter("@Name", SqlDbType.NVarChar);
                strParameter[3].Value = Name;
                strParameter[4] = new SqlParameter("@Email", SqlDbType.NVarChar);
                strParameter[4].Value = Email;
                strParameter[5] = new SqlParameter("@phone", SqlDbType.NVarChar);
                strParameter[5].Value = phone;
                strParameter[6] = new SqlParameter("@City", SqlDbType.NVarChar);
                strParameter[6].Value = City;
                strParameter[7] = new SqlParameter("@State", SqlDbType.NVarChar);
                strParameter[7].Value = State;
                strParameter[8] = new SqlParameter("@Zipcode", SqlDbType.NVarChar);
                strParameter[8].Value = Zipcode;
                strParameter[9] = new SqlParameter("@Institute", SqlDbType.NVarChar);
                strParameter[9].Value = Institute;
                strParameter[10] = new SqlParameter("@SpecialtityList", SqlDbType.NVarChar);
                strParameter[10].Value = SpecialtityList;
                strParameter[11] = new SqlParameter("@Keywords", SqlDbType.NVarChar);
                strParameter[11].Value = Keywords;
                dt = clshelper.DataTable("USP_GetSearchColleagueForDoctor", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                throw;
            }
        }

        //GetStateName
        public DataTable GetStateCode(string stateName)
        {
            SqlParameter[] strParameter = new SqlParameter[1];
            strParameter[0] = new SqlParameter("@StateName", SqlDbType.NVarChar);
            strParameter[0].Value = stateName;
            return clshelper.DataTable("GetStateCode", strParameter); ;
        }

        // This Method Check Email in Member and TEmp_signup Table and Secondary Email in Member Table
        public DataTable CheckEmailInSystem(string Email)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Email", SqlDbType.NVarChar);
                strParameter[0].Value = Email;

                dt = clshelper.DataTable("USP_CheckDoctorEmailExists", strParameter);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable CheckEmailInSystemForColleguesInvite(string Email)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Email", SqlDbType.NVarChar);
                strParameter[0].Value = Email;

                dt = clshelper.DataTable("USP_CheckDoctorEmailExistsColleguesInvite", strParameter);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }




        // This Method Check Email As Staff Member or Not
        public bool CheckEmailAsStaffMember(string Email)
        {
            try
            {
                string strReturn = "0";
                bool Check = false;
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Email", SqlDbType.NVarChar);
                strParameter[0].Value = Email;

                strReturn = clshelper.ExecuteScalar("USP_CheckEmailAsStaffMember", strParameter);
                if (!string.IsNullOrWhiteSpace(strReturn) && strReturn == "1")
                {
                    Check = true;
                }
                return Check;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ColleaguesBLL---CheckEmailAsStaffMember", Ex.Message, Ex.StackTrace);
                throw;
            }

        }





        public DataTable CheckEmailForDoctorSetting(string Email, int DoctorId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@Email", SqlDbType.NVarChar);
                strParameter[0].Value = Email;
                strParameter[1] = new SqlParameter("@DoctorId", SqlDbType.Int);
                strParameter[1].Value = DoctorId;
                dt = clshelper.DataTable("Usp_CheckEmailAndLoginForDoctorSettings", strParameter);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetPetientPassword(string Email)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Email", SqlDbType.NVarChar);
                strParameter[0].Value = Email;

                dt = clshelper.DataTable("USP_GetPetientPassword", strParameter);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public DataTable CheckEmailExistsAsDoctor(string Email)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Email", SqlDbType.NVarChar);
                strParameter[0].Value = Email;

                dt = clshelper.DataTable("USP_CheckEmailExistsAsDoctor", strParameter);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion

        public DataTable GetMemberSpecialities(int docid)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@docid", SqlDbType.Int);
                strParameter[0].Value = docid;

                dt = clshelper.DataTable("USP_GetProfile_Member_Specialities", strParameter);

                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public bool UpdateDoctorPersonalInformationFromProfile(int UserId, string FirstName, string MiddleName, string LastName, string OfficeName, string Title, string specialities, string Salutation)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[8];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@FirstName", SqlDbType.NVarChar);
                addphone[1].Value = FirstName;
                addphone[2] = new SqlParameter("@MiddleName", SqlDbType.NVarChar);
                addphone[2].Value = MiddleName;
                addphone[3] = new SqlParameter("@LastName", SqlDbType.NVarChar);
                addphone[3].Value = LastName;
                addphone[4] = new SqlParameter("@OfficeName", SqlDbType.NVarChar);
                addphone[4].Value = OfficeName;
                addphone[5] = new SqlParameter("@Title", SqlDbType.NVarChar);
                addphone[5].Value = Title;
                addphone[6] = new SqlParameter("@Specialities", SqlDbType.NVarChar);
                addphone[6].Value = specialities;
                addphone[7] = new SqlParameter("@Salutation", SqlDbType.NVarChar);
                addphone[7].Value = Salutation;


                UpdateStatus = clshelper.ExecuteNonQuery("USP_UpdateDoctorPersonalInformation", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool InsertUpdateLicenseDetils(int Userid, string LicenseNumber, string LicenseState, DateTime? LicenseExpiration)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] sqlpara = new SqlParameter[4];
                sqlpara[0] = new SqlParameter("@Userid", SqlDbType.Int);
                sqlpara[0].Value = Userid;
                sqlpara[1] = new SqlParameter("@LicenseNumber", SqlDbType.NVarChar);
                sqlpara[1].Value = LicenseNumber;
                sqlpara[2] = new SqlParameter("@LicenseState", SqlDbType.NVarChar);
                sqlpara[2].Value = LicenseState;
                sqlpara[3] = new SqlParameter("@LicenseExpiration", SqlDbType.DateTime);
                sqlpara[3].Value = LicenseExpiration;

                UpdateStatus = clshelper.ExecuteNonQuery("Usp_InsertUpdateLicenseDetailsByUserId", sqlpara);

                return UpdateStatus;

            }
            catch (Exception)
            {
                throw;
            }
        }
        #region Methods for Education and Training
        public bool EducationAndTrainingInsertAndUpdate(int Id, int UserId, string Institute, string Specialisation, string YearAttended)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[5];
                addphone[0] = new SqlParameter("@id", SqlDbType.Int);
                addphone[0].Value = Id;
                addphone[1] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[1].Value = UserId;
                addphone[2] = new SqlParameter("@Institute", SqlDbType.NVarChar);
                addphone[2].Value = Institute;
                addphone[3] = new SqlParameter("@Specialisation", SqlDbType.NVarChar);
                addphone[3].Value = Specialisation;
                addphone[4] = new SqlParameter("@YearAttended", SqlDbType.NVarChar);
                addphone[4].Value = YearAttended;




                UpdateStatus = clshelper.ExecuteNonQuery("USP_EducationAndTraining_InsertAndUpdate", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetEducationTrainingByUserId(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;

                dt = clshelper.DataTable("USP_GetEducationTrainingByUserId", strParameter);

                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetEducationTrainingByUniqueId(int Id)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Id", SqlDbType.Int);
                strParameter[0].Value = Id;

                dt = clshelper.DataTable("USP_GetEducationTrainingByUniqueId", strParameter);

                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool RemoveEducationTrainingByUniqueId(int Id)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[1];
                addphone[0] = new SqlParameter("@Id", SqlDbType.Int);
                addphone[0].Value = Id;


                UpdateStatus = clshelper.ExecuteNonQuery("USP_RemoveEducationTrainingByUniqueId", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Methods For Professional Membership
        public bool ProfessionalMembershipsInsertAndUpdate(int Id, int UserId, string Membership)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[3];
                addphone[0] = new SqlParameter("@id", SqlDbType.Int);
                addphone[0].Value = Id;
                addphone[1] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[1].Value = UserId;
                addphone[2] = new SqlParameter("@Membership", SqlDbType.NVarChar);
                addphone[2].Value = Membership;





                UpdateStatus = clshelper.ExecuteNonQuery("USP_ProfessionalMemberships_InsertAndUpdate", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetProfessionalMembershipsByUserId(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;

                dt = clshelper.DataTable("USP_GetProfessionalMembershipsByUserId", strParameter);

                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetProfessionalMembershipsByUniqueId(int Id)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Id", SqlDbType.Int);
                strParameter[0].Value = Id;

                dt = clshelper.DataTable("USP_GetProfessionalMembershipsByUniqueId", strParameter);

                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool RemoveProfessionalMembershipsByUniqueId(int Id)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[1];
                addphone[0] = new SqlParameter("@Id", SqlDbType.Int);
                addphone[0].Value = Id;


                UpdateStatus = clshelper.ExecuteNonQuery("USP_RemoveProfessionalMembershipsByUniqueId", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Methods For Team member
        public bool TeamMemberInsertAndUpdate(int UserId, int ParentUserId, string Email, string FirstName, string LastName, string OfficeName, string ExactAddress, string Address2, string City, string State, string Country, string ZipCode, string Specialty, string Password, string ProviderId, int LocationId, bool provtype)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[17];
                addphone[0] = new SqlParameter("@ParentUserId", SqlDbType.Int);
                addphone[0].Value = ParentUserId;
                addphone[1] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[1].Value = UserId;
                addphone[2] = new SqlParameter("@Email", SqlDbType.NVarChar);
                addphone[2].Value = Email;
                addphone[3] = new SqlParameter("@FirstName", SqlDbType.NVarChar);
                addphone[3].Value = FirstName;
                addphone[4] = new SqlParameter("@LastName", SqlDbType.NVarChar);
                addphone[4].Value = LastName;
                addphone[5] = new SqlParameter("@OfficeName", SqlDbType.NVarChar);
                addphone[5].Value = OfficeName;
                addphone[6] = new SqlParameter("@ExactAddress", SqlDbType.NVarChar);
                addphone[6].Value = ExactAddress;
                addphone[7] = new SqlParameter("@Address2", SqlDbType.NVarChar);
                addphone[7].Value = Address2;
                addphone[8] = new SqlParameter("@City", SqlDbType.NVarChar);
                addphone[8].Value = City;
                addphone[9] = new SqlParameter("@State", SqlDbType.NVarChar);
                addphone[9].Value = State;
                addphone[10] = new SqlParameter("@Country", SqlDbType.NVarChar);
                addphone[10].Value = Country;
                addphone[11] = new SqlParameter("@ZipCode", SqlDbType.NVarChar);
                addphone[11].Value = ZipCode;
                addphone[12] = new SqlParameter("@Specialty", SqlDbType.NVarChar);
                addphone[12].Value = Specialty;
                addphone[13] = new SqlParameter("@Password", SqlDbType.VarChar);
                addphone[13].Value = Password;
                addphone[14] = new SqlParameter("@LocationId", SqlDbType.Int);
                addphone[14].Value = LocationId;
                addphone[15] = new SqlParameter("@ProviderId", SqlDbType.NVarChar);
                addphone[15].Value = ProviderId;
                addphone[16] = new SqlParameter("@provtype", SqlDbType.Int);
                addphone[16].Value = provtype;

                UpdateStatus = clshelper.ExecuteNonQuery("USP_TeamMember_InsertAndUpdate", addphone);

                return UpdateStatus;
            }
            catch (Exception Ex)
            {
                throw;
            }
        }


        public DataTable GetTeamMemberDetailsOfDoctor(int ParentUserId, int PageIndex, int PageSize)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@ParentUserId", SqlDbType.Int);
                strParameter[0].Value = ParentUserId;
                strParameter[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[1].Value = PageIndex;
                strParameter[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[2].Value = PageSize;

                dt = clshelper.DataTable("USP_GetTeamMemberDetailsOfDoctor", strParameter);

                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetDoctorDetailsbyId(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] addphone = new SqlParameter[1];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                dt = clshelper.DataTable("Usp_GetDoctorDetalisbyId", addphone);
                return dt;
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public bool RemoveTeamMemberOfDoctor(int UserId, int ParentUserId)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@ParentUserId", SqlDbType.Int);
                addphone[1].Value = ParentUserId;
                UpdateStatus = clshelper.ExecuteNonQuery("USP_RemoveTeamMemberOfDoctor", addphone);
                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion


        #region Method For Add as Colleague


        public bool AddAsColleague(int UserId, int ColleagueId)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@ColleagueId", SqlDbType.Int);
                addphone[1].Value = ColleagueId;
                UpdateStatus = clshelper.ExecuteNonQuery("USP_AddColleague", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion


        #region Methods Regarding  Referral

        public int[] InsertReferral(string FromField, string ToField, string Subject, string Body, int Refer, int OwnerId, int PatientId, string RegardOption, string RequestingOption, int ReferStatus, int ReferedUserId, string Comments, string OtherComments, string reqother, string teethlist, string imageid, string documentid, string LocationName, string XMLString)
        {
            // Subject: for referall is Referral Card and Body: for referral is Refer Patient

            // Refer: is 1 for message and 2 for referral
            // ReferStatus: is 1 for refer,2 for accept and 3 for denied
            // ReferedUserId: is Selected colleague id for send referral


            bool result = false;
            try
            {

                // Build parameter array
                SqlParameter[] addReferParameters = new SqlParameter[18];
                addReferParameters[0] = new SqlParameter("@pFromField", SqlDbType.VarChar);
                addReferParameters[0].Value = FromField; // The from field
                addReferParameters[1] = new SqlParameter("@pToField", SqlDbType.VarChar);
                addReferParameters[1].Value = ToField; // The to field
                addReferParameters[2] = new SqlParameter("@pSubject", SqlDbType.VarChar);
                addReferParameters[2].Value = Subject; // The subject
                addReferParameters[3] = new SqlParameter("@pBody", SqlDbType.VarChar);
                addReferParameters[3].Value = Body; // The body
                addReferParameters[4] = new SqlParameter("@pMessageTypeId", SqlDbType.Int);
                addReferParameters[4].Value = Refer; // The message type for Identify Message or Referral
                addReferParameters[5] = new SqlParameter("@pOwnerId", SqlDbType.Int);
                addReferParameters[5].Value = OwnerId;// The owner id
                addReferParameters[6] = new SqlParameter("@pPatientId", SqlDbType.Int);
                addReferParameters[6].Value = PatientId; // The patient id
                addReferParameters[7] = new SqlParameter("@pRegardOption", SqlDbType.VarChar);
                addReferParameters[7].Value = RegardOption;
                addReferParameters[8] = new SqlParameter("@pRequestingOption", SqlDbType.VarChar);
                addReferParameters[8].Value = RequestingOption;
                addReferParameters[9] = new SqlParameter("@pReferralStatus", SqlDbType.Int);
                addReferParameters[9].Value = ReferStatus;
                addReferParameters[10] = new SqlParameter("@pReferedUserId", SqlDbType.Int);
                addReferParameters[10].Value = ReferedUserId;
                addReferParameters[11] = new SqlParameter("@oMessageId", SqlDbType.Int);
                addReferParameters[11].Direction = ParameterDirection.Output;
                addReferParameters[12] = new SqlParameter("@pComments", SqlDbType.VarChar);
                addReferParameters[12].Value = Comments;
                addReferParameters[13] = new SqlParameter("@pOtherComments", SqlDbType.VarChar);
                addReferParameters[13].Value = OtherComments;
                addReferParameters[14] = new SqlParameter("@pReqOther", SqlDbType.VarChar);
                addReferParameters[14].Value = reqother;
                addReferParameters[15] = new SqlParameter("@LocationName", SqlDbType.NVarChar);
                addReferParameters[15].Value = LocationName;
                addReferParameters[16] = new SqlParameter("@XMLString", SqlDbType.NVarChar);
                addReferParameters[16].Value = XMLString;
                addReferParameters[17] = new SqlParameter("@oReferralCardId", SqlDbType.Int);
                addReferParameters[17].Direction = ParameterDirection.Output;


                result = clshelper.ExecuteNonQuery("New_USP_InsertReferral", addReferParameters);
                int[] referralids = new int[2];
                int referidnew = Convert.ToInt32(addReferParameters[17].Value);
                referralids[0] = referidnew;
                referralids[1] = Convert.ToInt32(addReferParameters[11].Value);
                if (referidnew > 0 && !string.IsNullOrEmpty(teethlist))
                {
                    string[] arryteethlist = teethlist.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < arryteethlist.Count(); i++)
                    {
                        InsertReferral_Toothdetails(referidnew, Convert.ToInt32(arryteethlist[i]), 1, 1);
                    }
                }

                if (referidnew > 0 && !string.IsNullOrEmpty(imageid))
                {
                    string[] arryimage = imageid.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < arryimage.Count(); i++)
                    {
                        InsertReferral_Image_Refer(referidnew, Convert.ToInt32(arryimage[i]));
                    }
                }

                if (referidnew > 0 && !string.IsNullOrEmpty(documentid))
                {
                    string[] arrydocument = documentid.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < arrydocument.Count(); i++)
                    {
                        InsertReferral_Document_Refer(referidnew, Convert.ToInt32(arrydocument[i]));
                    }
                }
                AddPatientToReferredDoctor(OwnerId, PatientId);




                return referralids;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public bool AddPatientToReferredDoctor(int UserId, int Patientid)
        {
            bool result = false;
            try
            {
                SqlParameter[] param = new SqlParameter[5];
                param[0] = new SqlParameter("@pPatientId", Patientid);
                param[0].Value = Patientid;
                param[1] = new SqlParameter("@pUserId", UserId);
                param[1].Value = UserId;
                param[2] = new SqlParameter("@pStatusFlag", 0);
                param[2].Value = 0;
                param[3] = new SqlParameter("@pPatientIdAlias", string.Empty);
                param[3].Value = string.Empty;
                param[4] = new SqlParameter("@oAddFlag", SqlDbType.Int);
                param[4].Direction = ParameterDirection.Output;
                result = clshelper.ExecuteNonQuery("addAttendingDoctorToPatient", param);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool InsertReferral_Image_Refer(int ReferralCardId, int Image_Refer_Id)
        {
            bool result = false;
            try
            {
                SqlParameter[] add_refer_image = new SqlParameter[2];
                add_refer_image[0] = new SqlParameter("@ReferralCardId", ReferralCardId);
                add_refer_image[0].Value = ReferralCardId;
                add_refer_image[1] = new SqlParameter("@Image_Refer_id1", Image_Refer_Id);
                add_refer_image[1].Value = Image_Refer_Id;
                result = clshelper.ExecuteNonQuery("USP_InsertReferral_Image_Refer", add_refer_image);

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool InsertReferral_Document_Refer(int ReferralCardId, int Image_Refer_Id)
        {
            bool result = false;
            try
            {
                SqlParameter[] add_refer_image = new SqlParameter[2];
                add_refer_image[0] = new SqlParameter("@ReferralCardId", ReferralCardId);
                add_refer_image[0].Value = ReferralCardId;
                add_refer_image[1] = new SqlParameter("@Image_Refer_id1", Image_Refer_Id);
                add_refer_image[1].Value = Image_Refer_Id;
                result = clshelper.ExecuteNonQuery("USP_InsertReferral_Document_Refer", add_refer_image);

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool InsertReferral_Toothdetails(int referidnew, int toothcode, int toothposition, int toothproblem)
        {
            bool result = false;
            try
            {
                SqlParameter[] addrefertoothdetails = new SqlParameter[4];
                addrefertoothdetails[0] = new SqlParameter("@ReferralCardId", SqlDbType.Int);
                addrefertoothdetails[0].Value = referidnew;
                addrefertoothdetails[1] = new SqlParameter("@ToothCode", SqlDbType.Int);
                addrefertoothdetails[1].Value = toothcode;
                addrefertoothdetails[2] = new SqlParameter("@tooth_position_id", SqlDbType.Int);
                addrefertoothdetails[2].Value = toothposition;
                addrefertoothdetails[3] = new SqlParameter("@tooth_problem_id", SqlDbType.Int);
                addrefertoothdetails[3].Value = toothproblem;
                result = clshelper.ExecuteNonQuery("USP_InsertReferral_Toothdetails", addrefertoothdetails);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }



        public bool Insert_TreatingDoctor(int PatientId, int UserId, int UserStatusId, string PatientIdAlias, string Comments)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[5];
                addphone[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                addphone[0].Value = PatientId;
                addphone[1] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[1].Value = UserId;
                addphone[2] = new SqlParameter("@UserStatusId", SqlDbType.Int);
                addphone[2].Value = UserStatusId;
                addphone[3] = new SqlParameter("@PatientIdAlias", SqlDbType.VarChar);
                addphone[3].Value = PatientIdAlias;
                addphone[4] = new SqlParameter("@Comments", SqlDbType.VarChar);
                addphone[4].Value = Comments;

                UpdateStatus = clshelper.ExecuteNonQuery("USP_Insert_TreatingDoctor", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }







        public bool RemoveReferral(int MessageId, int ReferralCardId)
        {
            bool result = false;
            try
            {
                SqlParameter[] add_refer_image = new SqlParameter[2];
                add_refer_image[0] = new SqlParameter("@MessageId", MessageId);
                add_refer_image[0].Value = MessageId;
                add_refer_image[1] = new SqlParameter("@ReferralCardId", ReferralCardId);
                add_refer_image[1].Value = ReferralCardId;
                result = clshelper.ExecuteNonQuery("USP_RemoveReferral", add_refer_image);
                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Inbox


        public DataTable InboxOfDoctor(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext, string IsRefarral)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[7];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[1].Value = PageIndex;
                strParameter[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[2].Value = PageSize;
                strParameter[3] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[3].Value = SortColumn;
                strParameter[4] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[4].Value = SortDirection;
                strParameter[5] = new SqlParameter("@Searchtext", SqlDbType.NVarChar);
                strParameter[5].Value = Searchtext;
                strParameter[6] = new SqlParameter("@IsReferral", SqlDbType.NVarChar);
                strParameter[6].Value = IsRefarral;

                dt = clshelper.DataTable("USP_InboxOfDoctor", strParameter);

                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion

        #region SentBox

        public DataTable SentboxOfDoctor(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext, string IsReferral)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[7];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[1].Value = PageIndex;
                strParameter[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[2].Value = PageSize;
                strParameter[3] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[3].Value = SortColumn;
                strParameter[4] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[4].Value = SortDirection;
                strParameter[5] = new SqlParameter("@Searchtext", SqlDbType.NVarChar);
                strParameter[5].Value = Searchtext;
                strParameter[6] = new SqlParameter("@IsReferral", SqlDbType.NVarChar);
                strParameter[6].Value = IsReferral;

                dt = clshelper.DataTable("USP_SentBoxOfDoctor", strParameter);

                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Compose Message For Colleague

        public int SentMessageFromPatientHistory(string Subject, string Body, int SenderId, int MessageType, string AttachedPatientIds, string RecieverIds)
        {
            bool bval;
            int oNewId = 0;
            SqlParameter[] Officeinfo = new SqlParameter[7];
            Officeinfo[0] = new SqlParameter();
            Officeinfo[0] = new SqlParameter("@Subject", SqlDbType.VarChar);
            Officeinfo[0].Value = Subject;
            Officeinfo[1] = new SqlParameter("@Body", SqlDbType.NVarChar);
            Officeinfo[1].Value = Body;
            Officeinfo[2] = new SqlParameter("@SenderId", SqlDbType.Int);
            Officeinfo[2].Value = SenderId;
            Officeinfo[3] = new SqlParameter("@MessageType", SqlDbType.Int);
            Officeinfo[3].Value = MessageType;
            Officeinfo[4] = new SqlParameter("@RecieverIds", SqlDbType.NVarChar);
            Officeinfo[4].Value = RecieverIds;
            Officeinfo[5] = new SqlParameter("@AttachedPatientIds", SqlDbType.VarChar);
            Officeinfo[5].Value = AttachedPatientIds;
            Officeinfo[6] = new SqlParameter("@Newmessageid", SqlDbType.Int);
            Officeinfo[6].Direction = ParameterDirection.Output;




            bval = clshelper.ExecuteNonQuery("USP_SendMessageFromPatientHistoryMessage", Officeinfo);
            oNewId = Convert.ToInt32(Officeinfo[6].Value);
            return oNewId;
        }
        #endregion

        #region Insert Attachment For Patient in Message
        public bool InsertMessageAttachemntsForColleague(int MessageId, string AttachemntName)
        {
            bool result = false;
            try
            {

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                strParameter[0].Value = MessageId;
                strParameter[1] = new SqlParameter("@AttachemntName", SqlDbType.VarChar);
                strParameter[1].Value = AttachemntName;

                result = clshelper.ExecuteNonQuery("InsertMessageAttachemntsForColleague", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }
        #endregion

        #region Get Inbox Message Details By Id


        public DataSet GetInboxMessageDetailsById(int UserId, int MessageId)
        {
            try
            {
                DataSet dt = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@MessageId", SqlDbType.Int);
                strParameter[1].Value = MessageId;

                dt = clshelper.GetDatasetData("USP_GetInboxMessageDetailsById", strParameter);


                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetPatientAttachedDetails(string PatientIds)
        {
            SqlParameter[] strParameter = new SqlParameter[1];
            strParameter[0] = new SqlParameter("@AttachedPatientIds", SqlDbType.VarChar);
            strParameter[0].Value = PatientIds;

            return clshelper.DataTable("GetMessageAttachedPatientDetials", strParameter);
        }

        public DataTable GetInBoxPatientMessageDetailsById(int UserId, int MessageId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@MessageId", SqlDbType.Int);
                strParameter[1].Value = MessageId;

                dt = clshelper.DataTable("USP_GetInBoxPatientMessageDetailsById", strParameter);


                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion


        #region Get Sentbox Message Details By Id


        public DataSet GetSentboxMessageDetailsById(int UserId, int MessageId)
        {
            try
            {
                DataSet dt = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@MessageId", SqlDbType.Int);
                strParameter[1].Value = MessageId;

                dt = clshelper.GetDatasetData("USP_GetSentboxMessageDetailsById", strParameter);


                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public DataTable GetSentboxPatientMessageDetailsById(int UserId, int MessageId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@MessageId", SqlDbType.Int);
                strParameter[1].Value = MessageId;

                dt = clshelper.DataTable("USP_GetSentboxPatientMessageDetailsById", strParameter);


                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion


        #region Get Colleague Message Attachment by Message Id


        public DataTable GetColleageMessageAttachemntsById(int MessageId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                strParameter[0].Value = MessageId;


                dt = clshelper.DataTable("USP_GetColleageMessageAttachemntsById", strParameter);


                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public DataTable GetPatientMessageAttachemntsById(int MessageId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                strParameter[0].Value = MessageId;


                dt = clshelper.DataTable("USP_GetPatientMessageAttachemntsById", strParameter);


                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion


        #region Update Primary Email Of Doctor
        public bool UpdatePrimaryEmail(int UserId, string Email)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@Email", SqlDbType.NVarChar);
                addphone[1].Value = Email;


                UpdateStatus = clshelper.ExecuteNonQuery("USP_UpdatePrimaryEmailOfDoctor", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region Update Login UserName
        public bool UpdateUserName(int UserId, string UserName)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@UserName", SqlDbType.NVarChar);
                addphone[1].Value = UserName;

                UpdateStatus = clshelper.ExecuteNonQuery("UpdateUserName", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
        #region Update Secondary Email Of Doctor
        public bool UpdateSecondaryEmail(int UserId, string Email)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@Email", SqlDbType.NVarChar);
                addphone[1].Value = Email;


                UpdateStatus = clshelper.ExecuteNonQuery("USP_UpdateSecondaryEmailOfDoctor", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Update Password Of Doctor
        public bool UpdatePasswordOfDoctor(int UserId, string Password)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@Password", SqlDbType.VarChar);
                addphone[1].Value = Password;


                UpdateStatus = clshelper.ExecuteNonQuery("USP_UpdatePasswordOfDoctor", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion


        #region Update Staylogged Mins Of Doctor
        public bool UpdateStayloggedMinsOfDoctor(int UserId, string StayloggedMins)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@StayloggedMins", SqlDbType.NVarChar);
                addphone[1].Value = StayloggedMins;


                UpdateStatus = clshelper.ExecuteNonQuery("USP_UpdateStayloggedMinsOfDoctor", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion



        public DataTable GetReferralReceivedCountOfDoctor(int UserId, int ColleagueId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@ColleagueId", SqlDbType.Int);
                strParameter[1].Value = ColleagueId;

                dt = clshelper.DataTable("USP_GetReferralReceivedCountOfDoctor", strParameter);


                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }



        public DataTable GetReferralSentCountOfDoctor(int UserId, int ColleagueId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@ColleagueId", SqlDbType.Int);
                strParameter[1].Value = ColleagueId;

                dt = clshelper.DataTable("USP_GetReferralSentCountOfDoctor", strParameter);


                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetRefferalsentCountandReferralDetails(int UserId, int ColleagueId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@ColleagueId", SqlDbType.Int);
                strParameter[1].Value = ColleagueId;
                dt = clshelper.DataTable("Usp_GetCountandPatientDetailsofReferral", strParameter);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DataTable GetRefferalReceivedCountandReferralDetails(int UserId, int ColleagueId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@ColleagueId", SqlDbType.Int);
                strParameter[1].Value = ColleagueId;
                dt = clshelper.DataTable("Usp_GetCountandPatientDetailsofReferralReceived", strParameter);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }


        #region Remove Message From Inbox of Doctor
        public bool RemoveMessageFromInboxOfDoctor(int UserId, int MessageId)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@MessageId", SqlDbType.Int);
                addphone[1].Value = MessageId;


                UpdateStatus = clshelper.ExecuteNonQuery("USP_RemoveMessageFromInboxOfDoctor", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetTimeZoneBasedOnLocation(int locationId)
        {
            string LocationName = string.Empty;
            SqlParameter[] addphone = new SqlParameter[1];
            addphone[0] = new SqlParameter("@locationId", SqlDbType.Int);
            addphone[0].Value = locationId;

            LocationName = Convert.ToString(clshelper.ExecuteScalar("USP_GetTimeZoneBasedOnLocation", addphone));

            return LocationName;
        }

        public bool RemovePateintMessageFromInBoxOfDoctor(int UserId, int MessageId)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@MessageId", SqlDbType.Int);
                addphone[1].Value = MessageId;


                UpdateStatus = clshelper.ExecuteNonQuery("USP_RemovePateintMessageFromInBoxOfDoctor", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public bool RemovereferralFromDoctorInbox(int UserId, int MessageId)
        {

            SqlParameter[] addphone = new SqlParameter[2];
            addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
            addphone[0].Value = UserId;
            addphone[1] = new SqlParameter("@messageid", SqlDbType.Int);
            addphone[1].Value = MessageId;


            return clshelper.ExecuteNonQuery("DeleteReferralOfInbox", addphone); ;
        }
        #endregion


        #region Remove Message From Sent of Doctor
        public bool RemoveMessageFromSentBoxOfDoctor(int UserId, int MessageId)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@MessageId", SqlDbType.Int);
                addphone[1].Value = MessageId;


                UpdateStatus = clshelper.ExecuteNonQuery("USP_RemoveMessageFromSentBoxOfDoctor", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool RemovePatientMessageFromSentBoxOfDoctor(int UserId, int MessageId)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@MessageId", SqlDbType.Int);
                addphone[1].Value = MessageId;


                UpdateStatus = clshelper.ExecuteNonQuery("USP_RemovePateintMessageFromSentBoxOfDoctor", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool RemoveReferralFromSentBox(int UserId, int MessageId)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@MessageId", SqlDbType.Int);
                addphone[1].Value = MessageId;


                UpdateStatus = clshelper.ExecuteNonQuery("USP_RemoveReferralFromSentBoxOfDoctor", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        public int[] ResendMessage(int MessageId, int ReferralCardId)
        {
            bool result = false;
            try
            {

                // Build parameter array
                SqlParameter[] addReferParameters = new SqlParameter[4];
                addReferParameters[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                addReferParameters[0].Value = MessageId; // The from field
                addReferParameters[1] = new SqlParameter("@ReferralCardId", SqlDbType.Int);
                addReferParameters[1].Value = ReferralCardId; // The to field
                addReferParameters[2] = new SqlParameter("@oMessageId", SqlDbType.Int);
                addReferParameters[2].Direction = ParameterDirection.Output;
                addReferParameters[3] = new SqlParameter("@oReferralCardId", SqlDbType.Int);
                addReferParameters[3].Direction = ParameterDirection.Output;
                result = clshelper.ExecuteNonQuery("CreateResendMessage", addReferParameters);
                int[] referralids = new int[2];
                int referidnew = Convert.ToInt32(addReferParameters[3].Value);
                referralids[0] = referidnew;
                referralids[1] = Convert.ToInt32(addReferParameters[2].Value);
                return referralids;
            }
            catch (Exception)
            {
                throw;
            }

        }
        #region Search Doctor

        public DataSet GetDentistListBySearch(int PageIndex, int PageSize, string Keywords, string Address, string LastName, int Speciality, int Miles, string FirstName, string Title, string CompanyName, string School, string Email, string phone, string zipcode, string SpecialtityList, string City, string Alpha, string Country, string State)
        {
            try
            {
                DataSet dsdentist = new DataSet();
                SqlParameter[] GetDentistParams = new SqlParameter[19];

                GetDentistParams[0] = new SqlParameter("@Address", SqlDbType.VarChar);
                GetDentistParams[0].Value = Address;
                GetDentistParams[1] = new SqlParameter("@LastName", SqlDbType.VarChar);
                GetDentistParams[1].Value = LastName;
                GetDentistParams[2] = new SqlParameter("@Speciality", SqlDbType.Int);
                GetDentistParams[2].Value = Speciality;
                GetDentistParams[3] = new SqlParameter("@Miles", SqlDbType.Int);
                GetDentistParams[3].Value = Miles;
                GetDentistParams[4] = new SqlParameter("@index", SqlDbType.Int);
                GetDentistParams[4].Value = PageIndex;
                GetDentistParams[5] = new SqlParameter("@size", SqlDbType.Int);
                GetDentistParams[5].Value = PageSize;
                GetDentistParams[6] = new SqlParameter("@FirstName", SqlDbType.VarChar);
                GetDentistParams[6].Value = FirstName;
                GetDentistParams[7] = new SqlParameter("@Title", SqlDbType.VarChar);
                GetDentistParams[7].Value = Title;
                GetDentistParams[8] = new SqlParameter("@CompanyName", SqlDbType.VarChar);
                GetDentistParams[8].Value = CompanyName;
                GetDentistParams[9] = new SqlParameter("@School", SqlDbType.VarChar);
                GetDentistParams[9].Value = School;
                GetDentistParams[10] = new SqlParameter("@Email", SqlDbType.VarChar);
                GetDentistParams[10].Value = Email;
                GetDentistParams[11] = new SqlParameter("@phone", SqlDbType.VarChar);
                GetDentistParams[11].Value = phone;
                GetDentistParams[12] = new SqlParameter("@zipcode", SqlDbType.VarChar);
                GetDentistParams[12].Value = zipcode;
                GetDentistParams[13] = new SqlParameter("@Keywords", SqlDbType.VarChar);
                GetDentistParams[13].Value = Keywords;
                GetDentistParams[14] = new SqlParameter("@SpecialtityList", SqlDbType.VarChar);
                GetDentistParams[14].Value = SpecialtityList;
                GetDentistParams[15] = new SqlParameter("@City", SqlDbType.VarChar);
                GetDentistParams[15].Value = City;
                GetDentistParams[16] = new SqlParameter("@Alpha", SqlDbType.VarChar);
                GetDentistParams[16].Value = Alpha;
                GetDentistParams[17] = new SqlParameter("@Country", SqlDbType.VarChar);
                GetDentistParams[17].Value = Country;
                GetDentistParams[18] = new SqlParameter("@State", SqlDbType.VarChar);
                GetDentistParams[18].Value = State;
                dsdentist = clshelper.GetDatasetData("GetDentistListBySearch", GetDentistParams);
                return dsdentist;
            }
            catch (Exception)
            {
                throw;
            }

        }
        #endregion


        #region Get ReferralView in Message
        public DataTable GetReferralViewByID(int MessageId, string Type)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                strParameter[0].Value = MessageId;
                strParameter[1] = new SqlParameter("@Type", SqlDbType.NVarChar);
                strParameter[1].Value = Type;
                dt = clshelper.DataTable("USP_GetReferralDetailsById", strParameter);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        public DataTable GetSendReferredPatientList(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[6];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[1].Value = PageIndex;
                strParameter[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[2].Value = PageSize;
                strParameter[3] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[3].Value = SortColumn;
                strParameter[4] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[4].Value = SortDirection;
                strParameter[5] = new SqlParameter("@Searchtext", SqlDbType.NVarChar);
                strParameter[5].Value = Searchtext;
                dt = clshelper.DataTable("Usp_GetSendReferredPatientList", strParameter);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public DataTable GetReceivedReferredPatientList(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[6];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[1].Value = PageIndex;
                strParameter[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[2].Value = PageSize;
                strParameter[3] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[3].Value = SortColumn;
                strParameter[4] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[4].Value = SortDirection;
                strParameter[5] = new SqlParameter("@Searchtext", SqlDbType.NVarChar);
                strParameter[5].Value = Searchtext;
                dt = clshelper.DataTable("Usp_GetReceiveReferredPatientList", strParameter);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #region Get Refferal Message Attachment by Message Id
        public DataTable GetRefferalDocumentAttachemntsById(int ReferralCardId, int Patientid)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@Patientid", SqlDbType.Int);
                strParameter[0].Value = Patientid;
                strParameter[1] = new SqlParameter("@ReferralCardId", SqlDbType.Int);
                strParameter[1].Value = ReferralCardId;
                dt = clshelper.DataTable("USP_GetPatientDocumentByReferralSendFromDoctor", strParameter);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get Refferal Message Attachment Images by PationID and RefferalID
        public DataTable GetRefferalImageById(int ReferralCardId, int Patientid)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@Patientid", SqlDbType.Int);
                strParameter[0].Value = Patientid;
                strParameter[1] = new SqlParameter("@ReferralCardId", SqlDbType.Int);
                strParameter[1].Value = ReferralCardId;
                dt = clshelper.DataTable("USP_GetPatientImagesByReferralSendFromDoctor", strParameter);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region GetReferral ToothDetails By ReferralCardId
        public DataTable GetReferralToothDetailsByReferralCardId(int ReferralCardId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@ReferralCardId", SqlDbType.Int);
                strParameter[0].Value = ReferralCardId;
                dt = clshelper.DataTable("USP_GetReferralToothDetailsByReferralCardId", strParameter);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion


        #region Update Profile Image
        public bool UpdateDoctorProfileImageById(int UserId, String ImageName)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@ImageName", SqlDbType.Text);
                addphone[1].Value = ImageName;
                UpdateStatus = clshelper.ExecuteNonQuery("USP_UpdateDoctorProfileImageById", addphone);
                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region NewImportUpdateDoctor
        public bool UpdateMemberTableImportTime(int UserId, string Password, string ImageName, string Website, string JobTitle, string FirstName, string Lastname)
        {
            bool result = false;
            try
            {
                SqlParameter[] Sqlpara = new SqlParameter[7];
                Sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                Sqlpara[0].Value = UserId;
                Sqlpara[1] = new SqlParameter("@Password", SqlDbType.NVarChar);
                Sqlpara[1].Value = Password;
                Sqlpara[2] = new SqlParameter("@ImageName", SqlDbType.NVarChar);
                Sqlpara[2].Value = ImageName;
                Sqlpara[3] = new SqlParameter("@Website", SqlDbType.NVarChar);
                Sqlpara[3].Value = Website;
                Sqlpara[4] = new SqlParameter("@JobTitle", SqlDbType.NVarChar);
                Sqlpara[4].Value = JobTitle;
                Sqlpara[5] = new SqlParameter("@FirstName", SqlDbType.NVarChar);
                Sqlpara[5].Value = FirstName;
                Sqlpara[6] = new SqlParameter("@LastName", SqlDbType.NVarChar);
                Sqlpara[6].Value = Lastname;

                result = clshelper.ExecuteNonQuery("USP_Import_Member", Sqlpara);

                return result;

            }
            catch (Exception EX)
            {

                throw;
            }
        }

        public bool UpdateMemberEducationImportTime(int UserId, string School, string Year, string Degree)
        {
            bool result = false;
            try
            {
                SqlParameter[] Sqlpara = new SqlParameter[4];
                Sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                Sqlpara[0].Value = UserId;
                Sqlpara[1] = new SqlParameter("@School", SqlDbType.NVarChar);
                Sqlpara[1].Value = School;
                Sqlpara[2] = new SqlParameter("@Year", SqlDbType.NVarChar);
                Sqlpara[2].Value = Year;
                Sqlpara[3] = new SqlParameter("@Degree", SqlDbType.NVarChar);
                Sqlpara[3].Value = Degree;

                result = clshelper.ExecuteNonQuery("Usp_Import_MemberEducation", Sqlpara);
                return result;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool UpdateMemberSpecialtiesImportTime(int UserId, string Specialties)
        {
            bool result = false;
            try
            {
                SqlParameter[] Sqlpara = new SqlParameter[2];
                Sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                Sqlpara[0].Value = UserId;
                Sqlpara[1] = new SqlParameter("@Specialities", SqlDbType.NVarChar);
                Sqlpara[1].Value = Specialties;
                result = clshelper.ExecuteNonQuery("Usp_Import_MemberSpecialties", Sqlpara);
                return result;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }
        public bool UpdateProfessionalMemberShipImportTIme(int UserId, string ProfessionalMemberShip)
        {
            bool result;
            try
            {
                SqlParameter[] Sqlpara = new SqlParameter[2];
                Sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                Sqlpara[0].Value = UserId;
                Sqlpara[1] = new SqlParameter("@ProfessionalMemberShip", SqlDbType.NVarChar);
                Sqlpara[1].Value = ProfessionalMemberShip;
                result = clshelper.ExecuteNonQuery("Usp_Import_ProfessionalMemberShip", Sqlpara);
                return result;
            }
            catch (Exception EX)
            {

                throw;
            }
        }
        public bool UpdateAccountNameImportTime(int UserId, string AccountName)
        {
            bool Result = false;
            try
            {
                SqlParameter[] Sqlpara = new SqlParameter[2];
                Sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                Sqlpara[0].Value = UserId;
                Sqlpara[1] = new SqlParameter("@AccountName", SqlDbType.NVarChar);
                Sqlpara[1].Value = AccountName;
                Result = clshelper.ExecuteNonQuery("USP_IMPORT_AccountNameUpdate", Sqlpara);
                return Result;
            }
            catch (Exception EX)
            {

                throw;
            }
        }
        public bool UpdateMemberDetailsImportTime(int UserId, string LicenseNumber, string LicenseState, string LicenseExpDate)
        {
            bool Result = false;
            try
            {
                SqlParameter[] Sqlpara = new SqlParameter[4];
                Sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                Sqlpara[0].Value = UserId;
                Sqlpara[1] = new SqlParameter("@LicenseNumber", SqlDbType.NVarChar);
                Sqlpara[1].Value = LicenseNumber;
                Sqlpara[2] = new SqlParameter("@LicenseState", SqlDbType.NVarChar);
                Sqlpara[2].Value = LicenseState;
                Sqlpara[3] = new SqlParameter("@LicenseExpDate", SqlDbType.NVarChar);
                Sqlpara[3].Value = LicenseExpDate;

                Result = clshelper.ExecuteNonQuery("USP_Import_MemberDetails", Sqlpara);
                return Result;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }
        public bool UpdateAddressInfoImportTime(int UserId, string Address, string State, string city, string ZipCode, string Phone, string Fax, string Email)
        {
            bool Result = false;
            try
            {
                SqlParameter[] Sqlpara = new SqlParameter[8];
                Sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                Sqlpara[0].Value = UserId;
                Sqlpara[1] = new SqlParameter("@Address", SqlDbType.NVarChar);
                Sqlpara[1].Value = Address;
                Sqlpara[2] = new SqlParameter("@State", SqlDbType.NVarChar);
                Sqlpara[2].Value = State;
                Sqlpara[3] = new SqlParameter("@City", SqlDbType.NVarChar);
                Sqlpara[3].Value = city;
                Sqlpara[4] = new SqlParameter("@ZipCode", SqlDbType.NVarChar);
                Sqlpara[4].Value = ZipCode;
                Sqlpara[5] = new SqlParameter("@Phone", SqlDbType.NVarChar);
                Sqlpara[5].Value = Phone;
                Sqlpara[6] = new SqlParameter("@Fax", SqlDbType.NVarChar);
                Sqlpara[6].Value = Fax;
                Sqlpara[7] = new SqlParameter("@Email", SqlDbType.NVarChar);
                Sqlpara[7].Value = Email;
                Result = clshelper.ExecuteNonQuery("USP_IMPOERT_AddressInfo", Sqlpara);
                return Result;
            }
            catch (Exception EX)
            {

                throw;
            }
        }
        #endregion

        #region NewImportDoctorSignUp
        public int NewSingUpDoctor(string Email, string Password, string FirstName, string LastName, string MiddelName, string PublicPath,
            string AccountName, string AccountKey, string Address, string Address2, string City, string State, string Country, string Zip,
            string Phone, string Fax, bool IsPaid, string Facebook, string LinkedIn, string Twitter, string Blog, string YouTube, string Description,
            string School, int? YearGraduated, string Degree, string ProfessionalMemberships, string WebSite, string JobTitle, string DentalSpecialty,
            int IsCalledFromImportMember, string LicenseState, string LicenseNumber, DateTime? LicenseExpiration, int PrimaryTimeZone, object DentrixProviderId = null, object Mobile = null)
        {
            int NewId = 0;
            bool result = false;
            PrimaryTimeZone = PrimaryTimeZone == 0 ? 74 : PrimaryTimeZone;
            DentrixProviderId = DentrixProviderId != null ? DentrixProviderId : DBNull.Value;
            Mobile = Mobile != null ? Mobile : DBNull.Value;
            string Accountkey = Crypto.SHA1(Convert.ToString(DateTime.Now.Ticks));
            try
            {
                // Build parameter array
                SqlParameter[] addReferParameters = new SqlParameter[38];
                addReferParameters[0] = new SqlParameter("@Username", SqlDbType.NVarChar);
                addReferParameters[0].Value = Email;
                addReferParameters[1] = new SqlParameter("@Password", SqlDbType.VarChar);
                addReferParameters[1].Value = Password;
                addReferParameters[2] = new SqlParameter("@FirstName", SqlDbType.NVarChar);
                addReferParameters[2].Value = FirstName;
                addReferParameters[3] = new SqlParameter("@LastName", SqlDbType.NVarChar);
                addReferParameters[3].Value = LastName;
                addReferParameters[4] = new SqlParameter("@MiddleName", SqlDbType.NVarChar);
                addReferParameters[4].Value = MiddelName;
                addReferParameters[5] = new SqlParameter("@PublicProfileUrl", SqlDbType.NVarChar);
                addReferParameters[5].Value = PublicPath;
                addReferParameters[6] = new SqlParameter("@AccountName", SqlDbType.NVarChar);
                addReferParameters[6].Value = AccountName;
                addReferParameters[7] = new SqlParameter("@AccountKey", SqlDbType.NVarChar);
                addReferParameters[7].Value = AccountKey;
                addReferParameters[8] = new SqlParameter("@ExactAddress", SqlDbType.NVarChar);
                addReferParameters[8].Value = Address;


                addReferParameters[9] = new SqlParameter("@Address2", SqlDbType.NVarChar);
                addReferParameters[9].Value = Address2;

                addReferParameters[10] = new SqlParameter("@City", SqlDbType.NVarChar);
                addReferParameters[10].Value = City;
                addReferParameters[11] = new SqlParameter("@State", SqlDbType.NVarChar);
                addReferParameters[11].Value = State;
                addReferParameters[12] = new SqlParameter("@Country", SqlDbType.NVarChar);
                addReferParameters[12].Value = Country;
                addReferParameters[13] = new SqlParameter("@ZipCode", SqlDbType.NVarChar);
                addReferParameters[13].Value = Zip;
                addReferParameters[14] = new SqlParameter("@Phone", SqlDbType.NVarChar);
                addReferParameters[14].Value = Phone;

                addReferParameters[15] = new SqlParameter("@Fax", SqlDbType.NVarChar);
                addReferParameters[15].Value = Fax;

                addReferParameters[16] = new SqlParameter("@oUserId", SqlDbType.Int);
                addReferParameters[16].Direction = ParameterDirection.Output;
                addReferParameters[17] = new SqlParameter("@IsPaid", DbType.Boolean);
                addReferParameters[17].Value = IsPaid;

                addReferParameters[18] = new SqlParameter("@FacebookUrl", SqlDbType.NVarChar);
                addReferParameters[18].Value = Facebook;

                addReferParameters[19] = new SqlParameter("@LinkedinUrl", SqlDbType.NVarChar);
                addReferParameters[19].Value = LinkedIn;
                addReferParameters[20] = new SqlParameter("@TwitterUrl", SqlDbType.NVarChar);
                addReferParameters[20].Value = Twitter;
                addReferParameters[21] = new SqlParameter("@BlogUrl", DbType.Boolean);
                addReferParameters[21].Value = Blog;
                addReferParameters[22] = new SqlParameter("@YoutubeUrl", SqlDbType.NVarChar);
                addReferParameters[22].Value = YouTube;
                addReferParameters[23] = new SqlParameter("@Description", DbType.Boolean);
                addReferParameters[23].Value = Description;
                addReferParameters[24] = new SqlParameter("@Institute", SqlDbType.NVarChar);
                addReferParameters[24].Value = School;
                addReferParameters[25] = new SqlParameter("@Specialisation", SqlDbType.NVarChar);
                addReferParameters[25].Value = Degree;
                addReferParameters[26] = new SqlParameter("@YearAttended", SqlDbType.Int);
                addReferParameters[26].Value = YearGraduated;
                addReferParameters[27] = new SqlParameter("@Membership", SqlDbType.NVarChar);
                addReferParameters[27].Value = ProfessionalMemberships;
                addReferParameters[28] = new SqlParameter("@WebSite", SqlDbType.NVarChar);
                addReferParameters[28].Value = WebSite;
                addReferParameters[29] = new SqlParameter("@JobTitle", SqlDbType.NVarChar);
                addReferParameters[29].Value = JobTitle;
                addReferParameters[30] = new SqlParameter("@DentalSpecialty", SqlDbType.NVarChar);
                addReferParameters[30].Value = DentalSpecialty;
                addReferParameters[31] = new SqlParameter("@IsCalledFromImportMember", SqlDbType.Int);
                addReferParameters[31].Value = IsCalledFromImportMember;
                addReferParameters[32] = new SqlParameter("@LicenseState", SqlDbType.NVarChar);
                addReferParameters[32].Value = LicenseState;
                addReferParameters[33] = new SqlParameter("@LicenseNumber", SqlDbType.NVarChar);
                addReferParameters[33].Value = LicenseNumber;
                addReferParameters[34] = new SqlParameter("@LicenseExpiration", SqlDbType.DateTime);
                addReferParameters[34].Value = LicenseExpiration;
                addReferParameters[35] = new SqlParameter("@PrimaryTimeZone", SqlDbType.Int);
                addReferParameters[35].Value = PrimaryTimeZone;
                addReferParameters[36] = new SqlParameter("@DentrixProviderId", SqlDbType.VarChar);
                addReferParameters[36].Value = DentrixProviderId;
                addReferParameters[37] = new SqlParameter("@Mobile", SqlDbType.VarChar);
                addReferParameters[37].Value = Mobile;



                result = clshelper.ExecuteNonQuery("USP_SingUpDoctor", addReferParameters);
                NewId = Convert.ToInt32(addReferParameters[16].Value); // Get New Doctor Id 
            }
            catch (Exception)
            {
                throw;
            }
            return NewId;
        }

        #endregion
        #region Doctor Sign up

        public int SingUpDoctor(string Email, string Password, string FirstName, string LastName, string MiddelName, string PublicPath,
            string AccountName, string AccountKey, string Address, string Address2, string City, string State, string Country, string Zip,
            string Phone, string Fax, bool IsPaid, string Facebook, string LinkedIn, string Twitter, string Blog, string YouTube, string Description,
            string School, int? YearGraduated, string Degree, string ProfessionalMemberships, string WebSite, string JobTitle, string DentalSpecialty,
            int IsCalledFromImportMember, string LicenseState, string LicenseNumber, DateTime? LicenseExpiration, int PrimaryTimeZone, object DentrixProviderId = null, object Mobile = null,string NpiNumber =null)
        {
            int NewId = 0;
            bool result = false;
            PrimaryTimeZone = PrimaryTimeZone == 0 ? 74 : PrimaryTimeZone;
            DentrixProviderId = DentrixProviderId != null ? DentrixProviderId : DBNull.Value;
            Mobile = Mobile != null ? Mobile : DBNull.Value;
            try
            {

                // Build parameter array
                SqlParameter[] addReferParameters = new SqlParameter[39];
                addReferParameters[0] = new SqlParameter("@Username", SqlDbType.NVarChar);
                addReferParameters[0].Value = Email;
                addReferParameters[1] = new SqlParameter("@Password", SqlDbType.VarChar);
                addReferParameters[1].Value = Password;
                addReferParameters[2] = new SqlParameter("@FirstName", SqlDbType.NVarChar);
                addReferParameters[2].Value = FirstName;
                addReferParameters[3] = new SqlParameter("@LastName", SqlDbType.NVarChar);
                addReferParameters[3].Value = LastName;
                addReferParameters[4] = new SqlParameter("@MiddleName", SqlDbType.NVarChar);
                addReferParameters[4].Value = MiddelName;
                addReferParameters[5] = new SqlParameter("@PublicProfileUrl", SqlDbType.NVarChar);
                addReferParameters[5].Value = PublicPath;
                addReferParameters[6] = new SqlParameter("@AccountName", SqlDbType.NVarChar);
                addReferParameters[6].Value = AccountName;
                addReferParameters[7] = new SqlParameter("@AccountKey", SqlDbType.NVarChar);
                addReferParameters[7].Value = AccountKey;
                addReferParameters[8] = new SqlParameter("@ExactAddress", SqlDbType.NVarChar);
                addReferParameters[8].Value = Address;


                addReferParameters[9] = new SqlParameter("@Address2", SqlDbType.NVarChar);
                addReferParameters[9].Value = Address2;

                addReferParameters[10] = new SqlParameter("@City", SqlDbType.NVarChar);
                addReferParameters[10].Value = City;
                addReferParameters[11] = new SqlParameter("@State", SqlDbType.NVarChar);
                addReferParameters[11].Value = State;
                addReferParameters[12] = new SqlParameter("@Country", SqlDbType.NVarChar);
                addReferParameters[12].Value = Country;
                addReferParameters[13] = new SqlParameter("@ZipCode", SqlDbType.NVarChar);
                addReferParameters[13].Value = Zip;
                addReferParameters[14] = new SqlParameter("@Phone", SqlDbType.NVarChar);
                addReferParameters[14].Value = Phone;

                addReferParameters[15] = new SqlParameter("@Fax", SqlDbType.NVarChar);
                addReferParameters[15].Value = Fax;

                addReferParameters[16] = new SqlParameter("@oUserId", SqlDbType.Int);
                addReferParameters[16].Direction = ParameterDirection.Output;
                addReferParameters[17] = new SqlParameter("@IsPaid", DbType.Boolean);
                addReferParameters[17].Value = IsPaid;

                addReferParameters[18] = new SqlParameter("@FacebookUrl", SqlDbType.NVarChar);
                addReferParameters[18].Value = Facebook;

                addReferParameters[19] = new SqlParameter("@LinkedinUrl", SqlDbType.NVarChar);
                addReferParameters[19].Value = LinkedIn;
                addReferParameters[20] = new SqlParameter("@TwitterUrl", SqlDbType.NVarChar);
                addReferParameters[20].Value = Twitter;
                addReferParameters[21] = new SqlParameter("@BlogUrl", DbType.Boolean);
                addReferParameters[21].Value = Blog;
                addReferParameters[22] = new SqlParameter("@YoutubeUrl", SqlDbType.NVarChar);
                addReferParameters[22].Value = YouTube;

                addReferParameters[23] = new SqlParameter("@Description", DbType.Boolean);
                addReferParameters[23].Value = Description;

                addReferParameters[24] = new SqlParameter("@Institute", SqlDbType.NVarChar);
                addReferParameters[24].Value = School;
                addReferParameters[25] = new SqlParameter("@Specialisation", SqlDbType.NVarChar);
                addReferParameters[25].Value = Degree;
                addReferParameters[26] = new SqlParameter("@YearAttended", SqlDbType.Int);
                addReferParameters[26].Value = YearGraduated;
                addReferParameters[27] = new SqlParameter("@Membership", SqlDbType.NVarChar);
                addReferParameters[27].Value = ProfessionalMemberships;
                addReferParameters[28] = new SqlParameter("@WebSite", SqlDbType.NVarChar);
                addReferParameters[28].Value = WebSite;
                addReferParameters[29] = new SqlParameter("@JobTitle", SqlDbType.NVarChar);
                addReferParameters[29].Value = JobTitle;
                addReferParameters[30] = new SqlParameter("@DentalSpecialty", SqlDbType.NVarChar);
                addReferParameters[30].Value = DentalSpecialty;
                addReferParameters[31] = new SqlParameter("@IsCalledFromImportMember", SqlDbType.Int);
                addReferParameters[31].Value = IsCalledFromImportMember;
                addReferParameters[32] = new SqlParameter("@LicenseState", SqlDbType.NVarChar);
                addReferParameters[32].Value = LicenseState;
                addReferParameters[33] = new SqlParameter("@LicenseNumber", SqlDbType.NVarChar);
                addReferParameters[33].Value = LicenseNumber;
                addReferParameters[34] = new SqlParameter("@LicenseExpiration", SqlDbType.DateTime);
                addReferParameters[34].Value = LicenseExpiration;
                addReferParameters[35] = new SqlParameter("@PrimaryTimeZone", SqlDbType.Int);
                addReferParameters[35].Value = PrimaryTimeZone;
                addReferParameters[36] = new SqlParameter("@DentrixProviderId", SqlDbType.VarChar);
                addReferParameters[36].Value = DentrixProviderId;
                addReferParameters[37] = new SqlParameter("@Mobile", SqlDbType.VarChar);
                addReferParameters[37].Value = Mobile;
                addReferParameters[38] = new SqlParameter("@NpiNumber", SqlDbType.VarChar);
                addReferParameters[38].Value = NpiNumber;


                result = clshelper.ExecuteNonQuery("USP_SingUpDoctor", addReferParameters);
                NewId = Convert.ToInt32(addReferParameters[16].Value); // Get New Doctor Id 
            }
            catch (Exception Ex)
            {
                throw;
            }
            return NewId;
        }


        #endregion


        #region Doctor sign up in Temp table
        public int Insert_Temp_Signup(string email, string password, Boolean status,
            string image_url, string first_name, string last_name, string age, string gender, string location,
            string company, string title, string facebook, string twitter, string linkedin, string influence_scores, string phoneno, string Description,string Speciality ="" ,string PracticeName="", string NpiNumber="")
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[21];
                strParameter[0] = new SqlParameter("@email", SqlDbType.VarChar);
                strParameter[1] = new SqlParameter("@password", SqlDbType.VarChar);
                strParameter[2] = new SqlParameter("@Status", SqlDbType.Bit);

                //update the rest of the fields here
                strParameter[3] = new SqlParameter("@image_url", SqlDbType.VarChar);
                strParameter[4] = new SqlParameter("@first_name", SqlDbType.VarChar);
                strParameter[5] = new SqlParameter("@last_name", SqlDbType.VarChar);
                strParameter[6] = new SqlParameter("@age", SqlDbType.VarChar);
                strParameter[7] = new SqlParameter("@gender", SqlDbType.VarChar);
                strParameter[8] = new SqlParameter("@location", SqlDbType.VarChar);
                strParameter[9] = new SqlParameter("@company", SqlDbType.VarChar);
                strParameter[10] = new SqlParameter("@title", SqlDbType.VarChar);
                strParameter[11] = new SqlParameter("@facebook", SqlDbType.VarChar);
                strParameter[12] = new SqlParameter("@twitter", SqlDbType.VarChar);
                strParameter[13] = new SqlParameter("@linkedin", SqlDbType.VarChar);
                strParameter[14] = new SqlParameter("@influence_scores", SqlDbType.VarChar);

                strParameter[15] = new SqlParameter("@phoneno", SqlDbType.VarChar);
                strParameter[16] = new SqlParameter("@description", SqlDbType.NVarChar);
                strParameter[17] = new SqlParameter("@oNewDoctorId", SqlDbType.Int);
                strParameter[18] = new SqlParameter("@Speciality", SqlDbType.VarChar);
                strParameter[19] = new SqlParameter("@PracticeName", SqlDbType.VarChar);
                strParameter[20] = new SqlParameter("@NpiNumber", SqlDbType.VarChar);


                strParameter[0].Value = email;
                strParameter[1].Value = password;
                strParameter[2].Value = status;

                //update the rest of the fields here
                strParameter[3].Value = image_url;
                strParameter[4].Value = first_name;
                strParameter[5].Value = last_name;
                strParameter[6].Value = age;
                strParameter[7].Value = gender;
                strParameter[8].Value = location;
                strParameter[9].Value = company;
                strParameter[10].Value = title;
                strParameter[11].Value = facebook;
                strParameter[12].Value = twitter;
                strParameter[13].Value = linkedin;
                strParameter[14].Value = influence_scores;
                strParameter[15].Value = phoneno;
                strParameter[16].Value = Description;
                strParameter[17].Direction = ParameterDirection.Output;
                strParameter[18].Value = Speciality;
                strParameter[19].Value = PracticeName;
                strParameter[20].Value = NpiNumber;

                int x = Convert.ToInt32(clshelper.ExecuteNonQuery("USP_Insert_Temp_Signup", strParameter));
                x = Convert.ToInt32(strParameter[17].Value);

                return x;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Edit Public Profile of Doctor
        public DataTable CheckPublicProfileUrlIsExists(int UserId, string PublicProfileUrl)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@PublicProfileUrl", SqlDbType.NVarChar);
                strParameter[1].Value = PublicProfileUrl;

                dt = clshelper.DataTable("USP_CheckPublicProfileUrlIsExists", strParameter);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetRecivedReferralDetailsbyUserId(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;

                dt = clshelper.DataTable("Usp_GetReceivedReferralList", strParameter);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public DataTable GetSendReferralDetailsbyUserId(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;

                dt = clshelper.DataTable("Usp_GetSendReferralList", strParameter);
                return dt;

            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool UpdateDoctorPublicProfileUrlById(int UserId, string PublicProfileUrl)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@PublicProfileUrl", SqlDbType.Text);
                addphone[1].Value = PublicProfileUrl;
                UpdateStatus = clshelper.ExecuteNonQuery("USP_UpdateDoctorPublicProfileUrlById", addphone);
                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateDoctorWebsiteUrlById(int UserId, string WebsiteUrl, int WebsiteUrlId = 0, int? LocationId = null)
        {
            int NewLocation = LocationId ?? 0;
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[4];
                addphone[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[0].Value = UserId;
                addphone[1] = new SqlParameter("@WebsiteURL", SqlDbType.Text);
                addphone[1].Value = WebsiteUrl;
                addphone[2] = new SqlParameter("@LocationId", SqlDbType.Int);
                addphone[2].Value = NewLocation;
                addphone[3] = new SqlParameter("@WebsiteUrlId", SqlDbType.Int);
                addphone[3].Value = WebsiteUrlId;
                UpdateStatus = clshelper.ExecuteNonQuery("USP_UpdateDoctorWebsiteUrlById", addphone);
                return UpdateStatus;
            }
            catch (Exception Ex)
            {
                throw;
            }
        }

        #endregion



        #region Check Login First Count
        public DataTable GetFirstLoginCount(int Userid)
        {
            DataTable dtgetfirstlogincount = new DataTable();
            try
            {
                SqlParameter[] getfirstlogin = new SqlParameter[1];
                getfirstlogin[0] = new SqlParameter("@Userid", SqlDbType.Int);
                getfirstlogin[0].Value = Userid;

                dtgetfirstlogincount = clshelper.DataTable("USP_GetFirstLoginCount", getfirstlogin);
                return dtgetfirstlogincount;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion


        #region Method for update
        public bool UpdateInBoxReadStatus(int MessageTypeId, int MessageId)
        {

            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@MessageTypeId", SqlDbType.Int);
                addphone[0].Value = MessageTypeId;
                addphone[1] = new SqlParameter("@MessageId", SqlDbType.Int);
                addphone[1].Value = MessageId;


                UpdateStatus = clshelper.ExecuteNonQuery("USP_UpdateInBoxReadStatus", addphone);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region Code to update patient read flag
        public bool EditPatientMessageReadStatus(int PatientmessageId, string Operation)
        {
            bool ResultTrue;
            try
            {
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientmessageId", SqlDbType.Int);
                strParameter[0].Value = PatientmessageId;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;

                ResultTrue = clshelper.ExecuteNonQuery("PatientMessagesReadStatusFrom", strParameter);
                return ResultTrue;




            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public bool RemoveDoctorProfileImage(int DoctorId)
        {
            SqlParameter[] strParam = new SqlParameter[] { new SqlParameter("@DoctorId", DoctorId) };
            return clshelper.ExecuteNonQuery("removeDoctorProfileImage", strParam);
        }



        #region Sync API
        public DataSet GetDataSyncInfusionSoft(int UserId)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Userid", SqlDbType.Int);
                strParameter[0].Value = UserId;

                ds = clshelper.GetDatasetData("USP_Get_Sync_InfusionSoft", strParameter);
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }


        public bool SaveDataSyncInfusionSoft(int MemberId, int SyncPerDay, bool IsActive)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addSyncInfusionSoft = new SqlParameter[3];
                addSyncInfusionSoft[0] = new SqlParameter("@MemberId", SqlDbType.Int);
                addSyncInfusionSoft[0].Value = MemberId;

                addSyncInfusionSoft[1] = new SqlParameter("@SyncPerDay", SqlDbType.Int);
                addSyncInfusionSoft[1].Value = SyncPerDay;

                addSyncInfusionSoft[2] = new SqlParameter("@IsActive", SqlDbType.Bit);
                addSyncInfusionSoft[2].Value = IsActive;


                UpdateStatus = clshelper.ExecuteNonQuery("USP_Save_Sync_InfusionSoft", addSyncInfusionSoft);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool InsertDentrixSyncData(int RequestedUserId, bool IsProcessed, string EventType, string EventSource, DateTime CreatedDate, string ProcessDate, string RequestedUserType, string DentrixConnectorId)
        {
            try
            {
                bool Result = false;
                SqlParameter[] Para = new SqlParameter[8];
                Para[0] = new SqlParameter("@RequestedUserId", SqlDbType.Int);
                Para[0].Value = RequestedUserId;
                Para[1] = new SqlParameter("@IsProcessed", SqlDbType.Bit);
                Para[1].Value = IsProcessed;
                Para[2] = new SqlParameter("@EventType", SqlDbType.NVarChar);
                Para[2].Value = EventType;
                Para[3] = new SqlParameter("@EventSource", SqlDbType.NVarChar);
                Para[3].Value = EventSource;
                Para[4] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                Para[4].Value = CreatedDate;
                Para[5] = new SqlParameter("@ProcessDate", SqlDbType.NVarChar);
                Para[5].Value = string.Empty;
                Para[6] = new SqlParameter("@RequestedUserType", SqlDbType.NVarChar);
                Para[6].Value = RequestedUserType;
                Para[7] = new SqlParameter("@DentrixConnectorId", SqlDbType.NVarChar);
                Para[7].Value = DentrixConnectorId;
                Result = clshelper.ExecuteNonQuery("Usp_InsertUpdateEventLog", Para);
                return Result;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("InsertDentrixSyncData Method =", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public bool SyncNowAppointmentSide(int LocationId, int RequestedUserId, bool IsProcessed, string EventType, string EventSource, DateTime CreatedDate, string ProcessDate, string RequestedUserType)
        {
            try
            {
                bool IsInserted = false;
                SqlParameter[] Para = new SqlParameter[8];
                Para[0] = new SqlParameter("@LocationId", SqlDbType.Int);
                Para[0].Value = LocationId;
                Para[1] = new SqlParameter("@RequestedUserId", SqlDbType.Int);
                Para[1].Value = RequestedUserId;
                Para[2] = new SqlParameter("@IsProcessed", SqlDbType.Bit);
                Para[2].Value = IsProcessed;
                Para[3] = new SqlParameter("@EventType", SqlDbType.NVarChar);
                Para[3].Value = EventType;
                Para[4] = new SqlParameter("@EventSource", SqlDbType.NVarChar);
                Para[4].Value = EventSource;
                Para[5] = new SqlParameter("@CreatedDate", SqlDbType.DateTime);
                Para[5].Value = CreatedDate;
                Para[6] = new SqlParameter("@ProcessDate", SqlDbType.NVarChar);
                Para[6].Value = ProcessDate;
                Para[7] = new SqlParameter("@RequestedUserType", SqlDbType.NVarChar);
                Para[7].Value = RequestedUserType;
                IsInserted = clshelper.ExecuteNonQuery("Usp_SyncNowFromAppointmentSide", Para);
                return IsInserted;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SyncNowAppointmentSide Method =", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool CheckSyncNowButtoninAppointmentSide(int LocationId)
        {
            try
            {
                DataTable Dt = new DataTable();
                SqlParameter[] Para = new SqlParameter[1];
                Para[0] = new SqlParameter("@LocationId", SqlDbType.Int);
                Para[0].Value = LocationId;
                Dt = clshelper.DataTable("Usp_CheckAppointmentSync", Para);
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SyncNowAppointmentSide Method =", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public int CheckUserSyncProcessIsRunning(int UserId)
        {
            try
            {
                string IsSyncRunning = string.Empty;
                SqlParameter[] para = new SqlParameter[1];

                para[0] = new SqlParameter("@UserId", SqlDbType.Int);
                para[0].Value = UserId;

                IsSyncRunning = clshelper.ExecuteScalar("Usp_CheckIsSyncInProcess", para);

                if (IsSyncRunning != null && IsSyncRunning != "" && IsSyncRunning != "False")
                    return 1;
                else
                    return 0;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("CheckUserSyncProcessIsRunning", Ex.Message, Ex.StackTrace);
                return 0;
            }
        }
        public bool UpdateDataSyncInfusionSoft(int MemberId, bool IsActive)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] updateSyncInfusionSoft = new SqlParameter[2];
                updateSyncInfusionSoft[0] = new SqlParameter("@MemberId", SqlDbType.Int);
                updateSyncInfusionSoft[0].Value = MemberId;

                updateSyncInfusionSoft[1] = new SqlParameter("@IsActive", SqlDbType.Bit);
                updateSyncInfusionSoft[1].Value = IsActive;


                UpdateStatus = clshelper.ExecuteNonQuery("USP_Update_Sync_InfusionSoft", updateSyncInfusionSoft);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetLastSyncDateOfDentrix(int UserId)
        {
            try
            {
                string IsSyncRunning = string.Empty;
                SqlParameter[] para = new SqlParameter[1];

                para[0] = new SqlParameter("@UserId", SqlDbType.Int);
                para[0].Value = UserId;

                IsSyncRunning = clshelper.ExecuteScalar("Usp_GetLastSyncDateOfDentrix", para);
                return IsSyncRunning;

            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("GetLastSyncDateOfDentrix", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public bool UpdateDataSyncTokenInfusionSoft(int MemberId, string AccessToken, string RefreshToken, string APIKey)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] updateSyncInfusionSoft = new SqlParameter[4];
                updateSyncInfusionSoft[0] = new SqlParameter("@MemberId", SqlDbType.Int);
                updateSyncInfusionSoft[0].Value = MemberId;

                updateSyncInfusionSoft[1] = new SqlParameter("@AccessToken", SqlDbType.NVarChar);
                updateSyncInfusionSoft[1].Value = AccessToken;

                updateSyncInfusionSoft[2] = new SqlParameter("@RefreshToken", SqlDbType.NVarChar);
                updateSyncInfusionSoft[2].Value = RefreshToken;

                updateSyncInfusionSoft[3] = new SqlParameter("@APIKey", SqlDbType.NVarChar);
                updateSyncInfusionSoft[3].Value = APIKey;

                UpdateStatus = clshelper.ExecuteNonQuery("USP_Update_Sync_token_InfusionSoft", updateSyncInfusionSoft);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion

        //AK changes for Getting Draft Message List functionality 02-28-2017
        public DataTable GetDraftMessagesofDoctor(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext, bool IsReferral)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[7];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[1].Value = PageIndex;
                strParameter[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[2].Value = PageSize;
                strParameter[3] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[3].Value = SortColumn;
                strParameter[4] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[4].Value = SortDirection;
                strParameter[5] = new SqlParameter("@Searchtext", SqlDbType.NVarChar);
                strParameter[5].Value = Searchtext;
                strParameter[6] = new SqlParameter("@IsReferral", SqlDbType.Bit);
                strParameter[6].Value = IsReferral;
                dt = clshelper.DataTable("Usp_GetDraftMessages", strParameter);

                return dt;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }
        #region Compose Message For Colleague

        public int ComposeMessageOfDoctor(string Subject, string Body, int SenderId, int ColleagueId, int MessageType, string AttachedPatientIds, bool status, int CreatedByUserId)
        {
            try
            {
                bool bval;
                int oNewId = 0;
                SqlParameter[] Officeinfo = new SqlParameter[9];
                Officeinfo[0] = new SqlParameter();
                Officeinfo[0] = new SqlParameter("@Subject", SqlDbType.VarChar);
                Officeinfo[0].Value = Subject;
                Officeinfo[1] = new SqlParameter("@Body", SqlDbType.VarChar);
                Officeinfo[1].Value = Body;
                Officeinfo[2] = new SqlParameter("@SenderId", SqlDbType.Int);
                Officeinfo[2].Value = SenderId;
                Officeinfo[3] = new SqlParameter("@ColleagueId", SqlDbType.Int);
                Officeinfo[3].Value = ColleagueId;
                Officeinfo[4] = new SqlParameter("@MessageType", SqlDbType.Int);
                Officeinfo[4].Value = MessageType;
                Officeinfo[5] = new SqlParameter("@AttachedPatientIds", SqlDbType.VarChar);
                Officeinfo[5].Value = AttachedPatientIds;
                Officeinfo[6] = new SqlParameter("@Status", SqlDbType.Bit);
                Officeinfo[6].Value = status;
                Officeinfo[7] = new SqlParameter("@CreatedByUserId", SqlDbType.VarChar);
                Officeinfo[7].Value = CreatedByUserId;
                Officeinfo[8] = new SqlParameter("@Newmessageid", SqlDbType.Int);
                Officeinfo[8].Direction = ParameterDirection.Output;

                bval = clshelper.ExecuteNonQuery("USP_ComposeMessageOfDoctor", Officeinfo);
                oNewId = Convert.ToInt32(Officeinfo[8].Value);
                return oNewId;
            }
            catch (Exception)
            {

                throw;
            }

        }
        #endregion

        #region Code for Banner

        public bool InsertUpdateBanner(int BannerId, int UserId, string Title, string Path, string ColorCode, int Position)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addParam = new SqlParameter[6];
                addParam[0] = new SqlParameter("@BannerId", SqlDbType.Int);
                addParam[0].Value = BannerId;
                addParam[1] = new SqlParameter("@UserId", SqlDbType.Int);
                addParam[1].Value = UserId;
                addParam[2] = new SqlParameter("@Title", SqlDbType.VarChar);
                addParam[2].Value = string.Empty; //RM-355 Title is removed form add banner
                addParam[3] = new SqlParameter("@Path", SqlDbType.VarChar);
                addParam[3].Value = Path;
                addParam[4] = new SqlParameter("@ColorCode", SqlDbType.VarChar);
                addParam[4].Value = string.Empty; //RM-355 Title is removed form add banner
                addParam[5] = new SqlParameter("@Position", SqlDbType.Int);
                addParam[5].Value = Position;

                UpdateStatus = clshelper.ExecuteNonQuery("USP_InsertUpdateBanner", addParam);
                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetBannerDetailByUserId(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;

                dt = clshelper.DataTable("GetBannerDetailByUserId", strParameter);
                return dt;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool RemoveBanner(int BannerId)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addParam = new SqlParameter[1];
                addParam[0] = new SqlParameter("@BannerId", SqlDbType.Int);
                addParam[0].Value = BannerId;

                UpdateStatus = clshelper.ExecuteNonQuery("USP_RemoveBannerImage", addParam);
                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DataTable GetBannerDetailById(int BannerId)
        {
            {
                try
                {
                    DataTable dt = new DataTable();
                    SqlParameter[] strParameter = new SqlParameter[1];
                    strParameter[0] = new SqlParameter("@BannerId", SqlDbType.Int);
                    strParameter[0].Value = BannerId;

                    dt = clshelper.DataTable("GetBannerDetailById", strParameter);
                    return dt;
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        #endregion
        public DataTable GetMessageDetailByMessageId(int MessageId)
        {

            try
            {
                DataTable ds = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                strParameter[0].Value = MessageId;
                ds = clshelper.DataTable("GetMessageByMessageId", strParameter);
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public int GetIdOnPublicProfileURL(string url)
        {
            DataTable dt = new DataTable();
            clsCommon objCommon = new clsCommon();
            dt = objCommon.GetUserIDFromProfilePath(url);
            if (dt != null && dt.Rows.Count > 0)
            {
                return Convert.ToInt32(dt.Rows[0]["UserID"]);

            }
            else
            {
                return -1;
            }
        }
        //AK changes for Draft 03-03-2017
        public bool InsertDraftAttachments(int MessageId, string FileName, int UserId, int AttachmentType, int ComposeType)
        {
            bool Result = false;
            try
            {
                SqlParameter[] strParameter = new SqlParameter[5];
                strParameter[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                strParameter[0].Value = MessageId;
                strParameter[1] = new SqlParameter("@ImageName", SqlDbType.NVarChar);
                strParameter[1].Value = FileName;
                strParameter[2] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[2].Value = UserId;
                strParameter[3] = new SqlParameter("@AttachmentType", SqlDbType.Int);
                strParameter[3].Value = AttachmentType;
                strParameter[4] = new SqlParameter("@ComposeType", SqlDbType.Int);
                strParameter[4].Value = ComposeType;

                Result = clshelper.ExecuteNonQuery("Usp_InsertDraftAttachments", strParameter);
                return Result;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData--InsertDraftAttachments", Ex.Message, Ex.StackTrace);
                return Result;
                throw;
            }
        }
        public DataTable GetColleaguesListForAutoComplete(int UserId, string Searchtext, string ColleagueId = null)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@user_id", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@searchtext", SqlDbType.NVarChar);
                strParameter[1].Value = Searchtext;
                strParameter[2] = new SqlParameter("@ColleagueId", SqlDbType.NVarChar);
                strParameter[2].Value = ColleagueId;
                dt = clshelper.DataTable("usp_AutoComplete_GetColleagues", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData--usp_AutoComplete_GetColleagues", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public bool DeleteDraftAttachments(string ColleagueMessageId, string PatientMessageId)
        {
            bool Result = false;
            try
            {
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@ColleagueMessageId", SqlDbType.NVarChar);
                strParameter[0].Value = ColleagueMessageId;
                strParameter[1] = new SqlParameter("@PatientMessageId", SqlDbType.NVarChar);
                strParameter[1].Value = PatientMessageId;

                Result = clshelper.ExecuteNonQuery("SP_DeleteDraftMessage", strParameter);
                return Result;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData--DeleteDraftAttachments", Ex.Message, Ex.StackTrace);
                return Result;

            }
        }
        public DataTable GetColleagueAttachmentforMessage(int MessageId, int AttachmentId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@MessageId", SqlDbType.NVarChar);
                strParameter[0].Value = MessageId;
                strParameter[1] = new SqlParameter("@AttachmentId", SqlDbType.NVarChar);
                strParameter[1].Value = AttachmentId;

                dt = clshelper.DataTable("Usp_GetColleagueAttachmentFileForDownload", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }
        public DataTable GetPatientAttachmentforMessage(int MessageId, int AttachmentId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@MessageId", SqlDbType.NVarChar);
                strParameter[0].Value = MessageId;
                strParameter[1] = new SqlParameter("@AttachmentId", SqlDbType.NVarChar);
                strParameter[1].Value = AttachmentId;

                dt = clshelper.DataTable("Usp_GetPatientAttachmentfileforDownload", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData--GetPatientAttachmentforMessage", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetPerticulerFileForDownloadReferral(int ReferralCardId, int DocumentId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@ReferralCardId", SqlDbType.NVarChar);
                strParameter[0].Value = ReferralCardId;
                strParameter[1] = new SqlParameter("@DocumentId", SqlDbType.Int);
                strParameter[1].Value = DocumentId;

                dt = clshelper.DataTable("Usp_GetPatientDocumentForDownload", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData--GetPerticulerFileForDownloadReferral", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetPerticulerImagesForDownloadReferral(int ReferralCardId, int DocumentId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@ReferralCardId", SqlDbType.NVarChar);
                strParameter[0].Value = ReferralCardId;
                strParameter[1] = new SqlParameter("@ImageId", SqlDbType.Int);
                strParameter[1].Value = DocumentId;

                dt = clshelper.DataTable("Usp_GetDownloadPerticulerImageforReferral", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData--GetPerticulerImagesForDownloadReferral", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool DeleteInboxMessages(string PatientMessageId, string ColleagueMessageId, int UserId)
        {
            bool Result = false;
            try
            {
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@PatientMessageId", SqlDbType.NVarChar);
                strParameter[0].Value = PatientMessageId;
                strParameter[1] = new SqlParameter("@ColleaguesMessageId", SqlDbType.NVarChar);
                strParameter[1].Value = ColleagueMessageId;
                strParameter[2] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[2].Value = UserId;

                Result = clshelper.ExecuteNonQuery("Sp_DeleteInboxMessage", strParameter);
                return Result;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData--DeleteInboxMessages", Ex.Message, Ex.StackTrace);
                return Result;
            }
        }
        public bool DeleteSentBoxMessages(string PatientMessageId, string ColleagueMessageId, int UserId)
        {
            bool Result = false;
            try
            {
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@PatientMessageid", SqlDbType.NVarChar);
                strParameter[0].Value = PatientMessageId;
                strParameter[1] = new SqlParameter("@ColleaguesMessageId", SqlDbType.NVarChar);
                strParameter[1].Value = ColleagueMessageId;
                strParameter[2] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[2].Value = UserId;

                Result = clshelper.ExecuteNonQuery("Sp_DeleteSentBoxMessage", strParameter);
                return Result;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData--DeleteSentBoxMessages", Ex.Message, Ex.StackTrace);
                return Result;
            }
        }
        public DataTable GetMessageDetails(int MessageId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] paramsd = new SqlParameter[1];
                paramsd[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                paramsd[0].Value = MessageId;
                dt = clshelper.DataTable("SP_GetMessageDetails", paramsd);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData - GetMessageDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetPatientMessageDetails(int MessageId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] paramsd = new SqlParameter[1];
                paramsd[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                paramsd[0].Value = MessageId;
                dt = clshelper.DataTable("SP_GetPatientMessageDetails", paramsd);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData - GetPatientMessageDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public int InsertUpdateDraftMessage(int MessageId, int SenderId, string Body, string AttachedPatientIds, string DraftUserID, int MessageTypeId, int CreatedByUserId)
        {
            try
            {
                bool bval;
                int oNewId = 0;
                SqlParameter[] param = new SqlParameter[8];
                param[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                param[0].Value = MessageId;
                param[1] = new SqlParameter("@SenderId", SqlDbType.Int);
                param[1].Value = SenderId;
                param[2] = new SqlParameter("@Body", SqlDbType.NVarChar);
                param[2].Value = Body;
                param[3] = new SqlParameter("@AttachedPatientIds", SqlDbType.NVarChar);
                param[3].Value = AttachedPatientIds;
                param[4] = new SqlParameter("@DraftUserID", SqlDbType.NVarChar);
                param[4].Value = DraftUserID;
                param[5] = new SqlParameter("@MessageTypeId", SqlDbType.Int);
                param[5].Value = MessageTypeId;
                param[6] = new SqlParameter("@CreatedByUserId", SqlDbType.Int);
                param[6].Value = CreatedByUserId;
                param[7] = new SqlParameter("@Newmessageid", SqlDbType.Int);
                param[7].Direction = ParameterDirection.Output;

                bval = clshelper.ExecuteNonQuery("SP_InsertDraftMessage", param);
                oNewId = Convert.ToInt32(param[7].Value);
                return oNewId;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData - InsertUpdateDraftMessage", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        public int InsertUpdateColleagueMessage(string Subject, string Body, int SenderId, int ColleagueId, int MessageType, string AttachedPatientIds, int status, int CreatedByUserId, int MessageId = 0)
        {
            try
            {
                bool bval;
                int oNewId = 0;
                SqlParameter[] sqlparam = new SqlParameter[10];
                sqlparam[0] = new SqlParameter();
                sqlparam[0] = new SqlParameter("@Subject", SqlDbType.VarChar);
                sqlparam[0].Value = Subject;
                sqlparam[1] = new SqlParameter("@Body", SqlDbType.VarChar);
                sqlparam[1].Value = Body;
                sqlparam[2] = new SqlParameter("@SenderId", SqlDbType.Int);
                sqlparam[2].Value = SenderId;
                sqlparam[3] = new SqlParameter("@ColleagueId", SqlDbType.Int);
                sqlparam[3].Value = ColleagueId;
                sqlparam[4] = new SqlParameter("@MessageType", SqlDbType.Int);
                sqlparam[4].Value = MessageType;
                sqlparam[5] = new SqlParameter("@AttachedPatientIds", SqlDbType.NVarChar);
                sqlparam[5].Value = AttachedPatientIds;
                sqlparam[6] = new SqlParameter("@Status", SqlDbType.Int);
                sqlparam[6].Value = status;
                sqlparam[7] = new SqlParameter("@CreatedByUserId", SqlDbType.Int);
                sqlparam[7].Value = CreatedByUserId;
                sqlparam[8] = new SqlParameter("@MessageId", SqlDbType.Int);
                sqlparam[8].Value = MessageId;
                sqlparam[9] = new SqlParameter("@Newmessageid", SqlDbType.Int);
                sqlparam[9].Direction = ParameterDirection.Output;
                bval = clshelper.ExecuteNonQuery("USP_NewComposeMessageOfDoctor", sqlparam);
                oNewId = Convert.ToInt32(sqlparam[9].Value);
                return oNewId;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData - InsertUpdateColleagueMessage", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        public bool InsertMessageAssociation(int MessageId, int ReplyMessageId, int MessageTypeId, int ComposeTypeId)
        {
            try
            {
                bool bval = false;
                int oNewId = 0;
                SqlParameter[] sqlparam = new SqlParameter[4];
                sqlparam[0] = new SqlParameter();
                sqlparam[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                sqlparam[0].Value = MessageId;
                sqlparam[1] = new SqlParameter("@ReplyMessageId", SqlDbType.Int);
                sqlparam[1].Value = ReplyMessageId;
                sqlparam[2] = new SqlParameter("@MessageTypeId", SqlDbType.Int);
                sqlparam[2].Value = MessageTypeId;
                sqlparam[3] = new SqlParameter("@ComposeTypeId", SqlDbType.Int);
                sqlparam[3].Value = ComposeTypeId;
                bval = clshelper.ExecuteNonQuery("USP_InsertMessageAssociation", sqlparam);
                return bval;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData - InsertMessageAssociation", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        public DataTable GetDoctorDetails(int DoctorId)
        {

            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                strParameter[0].Value = DoctorId;
                dt = clshelper.DataTable("USP_GetDoctorDetails", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData - GetDoctorDetails", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        public DataTable GetUnreadedMessageCountOfDoctor(int UserId)
        {
            DataTable Count = new DataTable();
            try
            {
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;

                Count = clshelper.DataTable("Usp_GetCountOfUnreadedMessage", strParameter);
                return Count;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData - Usp_GetCountOfUnreadedMessage", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static DataTable GetManageSuperBucks(int UserId, int IsFrom, int PlanId)
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@IsFrom", SqlDbType.Int);
                strParameter[1].Value = IsFrom;
                strParameter[2] = new SqlParameter("@PlanId", SqlDbType.Int);
                strParameter[2].Value = PlanId;
                ds = new clsHelper().DataTable("USP_GetManageSuperbucksData", strParameter);
                return ds;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData - GetManageSuperBucks", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        public bool UpdateReadMessageStatus(int? PatientMessageId, int? ColleagueMessageId)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientMessageId", SqlDbType.Int);
                strParameter[0].Value = PatientMessageId;
                strParameter[1] = new SqlParameter("@ColleagueMessageId", SqlDbType.Int);
                strParameter[1].Value = ColleagueMessageId;

                bool Result = clshelper.ExecuteNonQuery("Usp_UpdateMessageReadStatus", strParameter);
                return Result;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData - UpdateReadMessageStatus", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetForwardAttachementDetails(int MessageId, int ComposeType)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                strParameter[0].Value = MessageId;
                strParameter[1] = new SqlParameter("@ComposeType", SqlDbType.Int);
                strParameter[1].Value = ComposeType;
                dt = clshelper.DataTable("SP_GetForwardAttachementDetails", strParameter);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetUpgreadeMembership(int MembershipID)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                     new SqlParameter() {ParameterName = "@MembershipId",Value= MembershipID },
                };
                return dt = clshelper.DataTable("GetMembershipUpgreadAccount", sqlpara);
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("Membership_FeatureDLL--GetUpgreadeMembership", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        #region PublicProfileSections
        public DataTable GetPublicProfileSectionDetials(int Userid)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[1];
                sqlpara[0] = new SqlParameter("@UserId", SqlDbType.NVarChar);
                sqlpara[0].Value = Userid;
                dt = clshelper.DataTable("Usp_GetPublicProfileSectionDetials", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsProfileData--GetPublicProfileSectionDetials", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool InsertUpdatePublicProfileDetails(PublicProfileSection Obj)
        {
            bool result = false;
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[10];
                sqlpara[0] = new SqlParameter("@SectionId", SqlDbType.Int);
                sqlpara[0].Value = Obj.SectionId;
                sqlpara[1] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlpara[1].Value = Obj.UserId;
                sqlpara[2] = new SqlParameter("@PatientForms", SqlDbType.Bit);
                sqlpara[2].Value = Obj.PatientForms;
                sqlpara[3] = new SqlParameter("@SpecialOffers", SqlDbType.Bit);
                sqlpara[3].Value = Obj.SpecialOffers;
                sqlpara[4] = new SqlParameter("@ReferPatient", SqlDbType.Bit);
                sqlpara[4].Value = Obj.ReferPatient;
                sqlpara[5] = new SqlParameter("@Reviews", SqlDbType.Bit);
                sqlpara[5].Value = Obj.Reviews;
                sqlpara[6] = new SqlParameter("@AppointmentBooking", SqlDbType.Bit);
                sqlpara[6].Value = Obj.AppointmentBooking;
                sqlpara[7] = new SqlParameter("@PatientLogin", SqlDbType.Bit);
                sqlpara[7].Value = Obj.PatientLogin;
                sqlpara[8] = new SqlParameter("@CreatedBy", SqlDbType.Int);
                sqlpara[8].Value = Obj.CreatedBy;
                sqlpara[9] = new SqlParameter("@ModifiedBy", SqlDbType.Int);
                sqlpara[9].Value = Obj.ModifiedBy;

                result = clshelper.ExecuteNonQuery("InsertUpdatePublicProfileSectiondetails", sqlpara);
                return result;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsProfileData--InsertUpdatePublicProfileSectiondetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        #endregion

        #region Update Membership By Recurring
        public DataTable UpdateMembershipByRecurring(string memberId, string membershipId)
        {
            SqlParameter[] strParameter = new SqlParameter[2];
            strParameter[0] = new SqlParameter("@memberId", SqlDbType.Int);
            strParameter[0].Value = memberId;
            strParameter[1] = new SqlParameter("@membershipId", SqlDbType.NVarChar);
            strParameter[1].Value = membershipId;

            return clshelper.DataTable("UpdateMembershipByRecurring", strParameter);
        }
        #endregion

        public DataTable GetUpgradeMembershipById(int MembershipID)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                     new SqlParameter() {ParameterName = "@MembershipId",Value= MembershipID },
                };
                return dt = clshelper.DataTable("GetMembershipListById", sqlpara);
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData--GetUpgradeMembershipById", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public bool UpdateUserMembershipManually(string UserId, string IntervalId, string MembershipId, string amount)
        {
            try
            {
                bool dt = false;
                UserPreviousMembershipRecurring(UserId, MembershipId);
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter() { ParameterName = "@UserId", Value = UserId},
                    new SqlParameter() { ParameterName = "@IntervalId", Value = IntervalId},
                    new SqlParameter() { ParameterName = "@MembershipId", Value = MembershipId},
                    new SqlParameter() { ParameterName = "@amount", Value = amount},
                };
                dt = clshelper.ExecuteNonQuery("USP_UpdateUserMembershipManually", sqlpara);
                return dt;
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("USP_UpdateUserMembershipManually", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public bool UserPreviousMembershipRecurring(string userid, string membershipid)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter() { ParameterName = "@UserId", Value = userid},
                };
                bool dt = clshelper.ExecuteNonQuery("USP_UserPreviousMembershipRecurring", sqlpara);
                return dt;
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("USP_UserPreviousMembershipRecurring", ex.Message, ex.StackTrace);
                throw;
            }
        }
        public DataTable GetPlanDetialsForTabletView()
        {
            try
            {
                DataTable dt = new DataTable();
                return dt = clshelper.DataTable("Usp_GetPlanDetialsForTablet");
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("clsColleaguesData--GetPlanDetialsForTabletView", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public DataTable GetAccessTokenForPMS(int UserId)
        {
            DataTable dt = new DataTable();
            SqlParameter[] strParameter = new SqlParameter[1];
            strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
            strParameter[0].Value = UserId;

            dt = clshelper.DataTable("USP_GetAccessTokenForPMS", strParameter);

            return dt;
        }
        public bool CreateAccessTokenForPMS(int UserId, string AccessToken)
        {

            DataTable dt = new DataTable();
            SqlParameter[] strParameter = new SqlParameter[2];
            strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
            strParameter[0].Value = UserId;
            strParameter[1] = new SqlParameter("@AccessToken", SqlDbType.NVarChar);
            strParameter[1].Value = AccessToken;

            // return clshelper.ExecuteNonQuery("USP_CreateAccessTokenForPMS", strParameter);
            return clshelper.ExecuteNonQuery("USP_GetAccessTokenForPMS_New", strParameter);

        }
        public DataTable ValidateAccessTokenForPMS(string AccessToken)
        {

            DataTable dt = new DataTable();
            SqlParameter[] strParameter = new SqlParameter[1];
            strParameter[0] = new SqlParameter("@AccessToken", SqlDbType.NVarChar);
            strParameter[0].Value = AccessToken;

            return clshelper.DataTable("USP_ValidateAccessTokenForPMS", strParameter);
        }

        public DataTable GetUserCredentilasBasedOnAccessTokenForPMS(string AccessToken)
        {

            DataTable dt = new DataTable();
            SqlParameter[] strParameter = new SqlParameter[1];
            strParameter[0] = new SqlParameter("@AccessToken", SqlDbType.NVarChar);
            strParameter[0].Value = AccessToken;

            return clshelper.DataTable("USP_GetUserCredentilasBasedOnAccessTokenForPMS", strParameter);
        }
        public DataTable GetDentistListSearch(Search Obj)
        {
            try
            {
                DataTable dsdentist = new DataTable();
                SqlParameter[] GetDentistParams = new SqlParameter[13];
                GetDentistParams[0] = new SqlParameter("@LastName", SqlDbType.VarChar);
                GetDentistParams[0].Value = Obj.LastName;
                GetDentistParams[1] = new SqlParameter("@Speciality", SqlDbType.Int);
                GetDentistParams[1].Value = Obj.Speciality;
                GetDentistParams[2] = new SqlParameter("@Miles", SqlDbType.Int);
                GetDentistParams[2].Value = Obj.Miles;
                GetDentistParams[3] = new SqlParameter("@index", SqlDbType.Int);
                GetDentistParams[3].Value = Obj.PageIndex;
                GetDentistParams[4] = new SqlParameter("@size", SqlDbType.Int);
                GetDentistParams[4].Value = Obj.PageSize;
                GetDentistParams[5] = new SqlParameter("@FirstName", SqlDbType.VarChar);
                GetDentistParams[5].Value = Obj.fname;
                GetDentistParams[6] = new SqlParameter("@zipcode", SqlDbType.VarChar);
                GetDentistParams[6].Value = ReplaceSingleQuote(Obj.ZipCode);
                GetDentistParams[7] = new SqlParameter("@Keywords", SqlDbType.VarChar);
                GetDentistParams[7].Value = ReplaceSingleQuote(Obj.keywords);
                GetDentistParams[8] = new SqlParameter("@SpecialtityList", SqlDbType.VarChar);
                GetDentistParams[8].Value = (Obj.SpecialityList == null ? null : string.Join(",", Obj.SpecialityList.Select(n => n.ToString()).ToArray()));
                GetDentistParams[9] = new SqlParameter("@City", SqlDbType.VarChar);
                GetDentistParams[9].Value = ReplaceSingleQuote(Obj.City);
                GetDentistParams[10] = new SqlParameter("@State", SqlDbType.VarChar);
                GetDentistParams[10].Value = ReplaceSingleQuote(Obj.State);
                GetDentistParams[11] = new SqlParameter("@Phone", SqlDbType.VarChar);
                GetDentistParams[11].Value = ReplaceSingleQuote(Obj.Phone);
                GetDentistParams[12] = new SqlParameter("@FilterBy", SqlDbType.VarChar);
                GetDentistParams[12].Value = Obj.FilterBy;
                dsdentist = clshelper.DataTable("GetDentistListBySearch", GetDentistParams);
                return dsdentist;
            }
            catch (Exception Ex)
            {
                throw;
            }

        }
        public static DataTable GetInsuranceCompaniesOfUser(int UserId)
        {
            clsHelper clshelper = new clsHelper();

            DataTable dt = new DataTable();
            SqlParameter[] strParameter = new SqlParameter[1];
            strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
            strParameter[0].Value = UserId;

            return clshelper.DataTable("USP_GetInsuranceCompaniesOfUser", strParameter);
        }
        //Olivia changes on 10-04-2018 for Dr. Lan Allan's website. - DLADCETK-1
        public static string GetAppointmentLink(int UserId)
        {
            clsHelper clshelper = new clsHelper();

            DataTable dt = new DataTable();
            SqlParameter[] strParameter = new SqlParameter[1];
            strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
            strParameter[0].Value = UserId;

            return clshelper.ExecuteScalar("USP_GetAppointmentLink", strParameter);
        }

        public static DataTable GetDentalProceduresListOfUser(int userid)
        {
            clsHelper clshelper = new clsHelper();

            DataTable dt = new DataTable();
            SqlParameter[] strParameter = new SqlParameter[1];
            strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
            strParameter[0].Value = userid;

            return clshelper.DataTable("USP_GetInsuranceProceduresOfUser", strParameter);
        }

        public static DataTable GetstateListOfUser()
        {
            DataSet ds = new DataSet();
            int i;
            SqlConnection cn = new SqlConnection(ConfigurationManager.ConnectionStrings["CONNECTIONSTRING"].ConnectionString);
            try
            {
                if (cn.State == ConnectionState.Open)
                    cn.Close();
                cn.Open();
                SqlCommand cmd = new SqlCommand("select StateName from StateMaster where CountryCode='US' order by StateName asc", cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                var stare = da.Fill(ds);
                cn.Close();
                return ds.Tables[0];
            }
            catch (Exception)
            {
                throw;
            }

        }

        public static bool UpdateDentalInsurananceHistory(SendInsuranceRequestBO model)
        {
            SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter() { ParameterName = "@FirstName", Value = model.objPatient.FirstName},
                new SqlParameter() { ParameterName = "@LastName", Value = model.objPatient.LastName},
                new SqlParameter() { ParameterName = "@Email", Value = model.objPatient.Email},
                new SqlParameter() { ParameterName = "@Phone", Value = model.objPatient.Phone},
                new SqlParameter() { ParameterName = "@HistoryId", Value = model.HistoryId},
                new SqlParameter() { ParameterName = "@SelectedChoice", Value = model.SelectedChoice}
            };
            return new clsHelper().ExecuteNonQuery("USP_UpdateDentalInsurananceHistory", sqlpara);
        }
        public static DataTable GetProcedures(int ProcedureId = 0, int UserId = 0)
        {
            SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter() {ParameterName = "@ProcedureId",Value=ProcedureId},
                new SqlParameter() {ParameterName = "@UserId",Value=UserId}
            };
            return new clsHelper().DataTable("Usp_GetDentalProcedure", sqlpara);
        }
        public static bool CheckProcedureAlreadyExist(Procedure Obj)
        {
            DataTable dt = new DataTable();
            SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter(){ParameterName="@ProcedureName",Value=Obj.ProcedureName}
            };
            dt = new clsHelper().DataTable("Usp_CheckProcedureAlreadyExist", sqlpara);
            return dt.Rows.Count > 0 ? true : false;
        }
        public static bool InsertProcedure(Procedure Obj)
        {
            SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter(){ParameterName ="@ProcedureName",Value=Obj.ProcedureName},
                new SqlParameter(){ParameterName ="@InsuranceCost",Value=Obj.CostPercentage},
                new SqlParameter(){ParameterName ="@StandardRate",Value=Obj.StandardFees},
                new SqlParameter(){ParameterName="@ProcedureId",Value=Obj.ProcedureId}
            };
            return new clsHelper().ExecuteNonQuery("Usp_InsertDentalProcedure", sqlpara);
        }
        public static bool DeleteProcedure(Procedure Obj, int UserId = 0)
        {
            SqlParameter[] sqlpara = new SqlParameter[]
           {
                new SqlParameter(){ParameterName="@ProcedureId",Value=Obj.ProcedureId},
                new SqlParameter() {ParameterName = "@UserId",Value=UserId}
           };
            return new clsHelper().ExecuteNonQuery("Usp_DeleteProcedure", sqlpara);
        }
        public static bool InsertProcedureForUser(Procedure obj, int UserId)
        {
            SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter(){ParameterName="@ProcedureId",Value=obj.ProcedureId},
                new SqlParameter() {ParameterName = "@UserId",Value=UserId},
                new SqlParameter(){ParameterName ="@ProcedureName",Value=obj.ProcedureName},
                new SqlParameter(){ParameterName ="@CostPercentage",Value=obj.CostPercentage},
                new SqlParameter(){ParameterName ="@StandardFees",Value=obj.StandardFees},
            };
            return new clsHelper().ExecuteNonQuery("Usp_InsertProcedureForUser", sqlpara);
        }
        public static bool InsertUpdateUserProcedureDetails(Procedure obj, int UserId)
        {
            SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter(){ParameterName="@ProcedureId",Value=obj.ProcedureId},
                new SqlParameter() {ParameterName = "@UserId",Value=UserId},
                new SqlParameter(){ParameterName ="@ProcedureName",Value=obj.ProcedureName},
                new SqlParameter(){ParameterName ="@InsuranceCostRatio",Value=obj.CostPercentage},
                new SqlParameter(){ParameterName ="@StandardRate",Value=obj.StandardFees},
            };
            return new clsHelper().ExecuteNonQuery("Usp_InsertUpdateUserProcedureDetails", sqlpara);
        }
        public bool GetCommunicationHistoryForPatientByDocID(int UserId, int PatientId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[2];
                sqlpara[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                sqlpara[0].Value = UserId;
                sqlpara[1] = new SqlParameter("@PatientId", SqlDbType.Int);
                sqlpara[1].Value = PatientId;
                dt = clshelper.DataTable("GetReferrelDetailByDoctorIdPatientID", sqlpara);
                if (dt != null && dt.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        public DataTable GetPivotQueryforInsuranceCostEstimator(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[1];
                sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlpara[0].Value = UserId;
                dt = clshelper.DataTable("Usp_GetInsuranceCostEstimatoinPivotQuery", sqlpara);
                //if(dt == null)
                //{
                //    DataTable dts = new DataTable();
                //    SqlParameter[] newsqlpara = new SqlParameter[1];
                //    newsqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                //    newsqlpara[0].Value = UserId;
                //    dts = clshelper.DataTable("USP_CopyStandardProcedureRatesOfUser", newsqlpara);
                //}
                return dt;
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("Usp_GetInsuranceCostEstimatoinPivotQuery", ex.Message, ex.StackTrace);
                throw;
            }
        }
        public DataTable GetInsuranceCompanyNamebyId(int InsuranceId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[1];
                sqlpara[0] = new SqlParameter("@InsuranceId", SqlDbType.Int);
                sqlpara[0].Value = InsuranceId;
                return dt = clshelper.DataTable("Usp_GetInsuranceNameById", sqlpara);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("GetInsuranceCompanyNamebyId", ex.Message, ex.StackTrace);
                throw;
            }
        }
        public DataTable GetProcedureDetials(int ProcedureId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[1];
                sqlpara[0] = new SqlParameter("@ProcedureId", SqlDbType.Int);
                sqlpara[0].Value = ProcedureId;
                return dt = clshelper.DataTable("Usp_GetProcedureDetials", sqlpara);
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("GetProcedureDetials", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool InsertProcedureandInsurenceMappingData(List<InsuranceProcedureMapp> Lst, int UserId)
        {

            bool result = false;
            SqlParameter[] sqlpara = new SqlParameter[2];
            sqlpara[0] = new SqlParameter("@List", SqlDbType.Structured);
            sqlpara[0].Value = clsCommon.ToDataTable(Lst);
            sqlpara[1] = new SqlParameter("@UserId", SqlDbType.Int);
            sqlpara[1].Value = UserId;
            return result = clshelper.ExecuteNonQuery("Usp_InsertUpdateProcedureInsurance", sqlpara);
        }
        public static bool UpdateProcedureDetails(Procedure obj)
        {
            SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter(){ParameterName="@ProcedureId",Value=obj.ProcedureId},
                new SqlParameter(){ParameterName ="@ProcedureName",Value=obj.ProcedureName},
                new SqlParameter(){ParameterName ="@CostRatio",Value=obj.CostPercentage},
                new SqlParameter(){ParameterName ="@StandardRate",Value=obj.StandardFees},
            };
            return new clsHelper().ExecuteNonQuery("Usp_UpdateProcedureDetials", sqlpara);
        }
        public static IDictionary<int, int> SignUpDoctor(InsertMember Obj, string AccountKey, string Password)
        {
            int NewId = 0; int Address = 0;
            var results = new Dictionary<int, int>();
            bool result = false;
            int PrimaryTimeZone = 0;
            PrimaryTimeZone = (PrimaryTimeZone == 0) ? 74 : PrimaryTimeZone;
            Obj.ProviderId = (Obj.ProviderId != null) ? Obj.ProviderId : DBNull.Value;
            try
            {
                string emailAddress = string.Empty;
                string SecondaryEmail = string.Empty;
                if (Obj.EmailAdresses != null)
                {
                    if (Obj.EmailAdresses.Count > 0)
                    {
                        emailAddress = Obj.EmailAdresses.FirstOrDefault();
                    }
                }
                // Build parameter array
                SqlParameter[] addReferParameters = new SqlParameter[19];
                addReferParameters[0] = new SqlParameter("@Username", SqlDbType.NVarChar);
                addReferParameters[0].Value = emailAddress;
                addReferParameters[1] = new SqlParameter("@Password", SqlDbType.VarChar);
                addReferParameters[1].Value = Password;
                addReferParameters[2] = new SqlParameter("@FirstName", SqlDbType.NVarChar);
                addReferParameters[2].Value = Obj.FirstName;
                addReferParameters[3] = new SqlParameter("@LastName", SqlDbType.NVarChar);
                addReferParameters[3].Value = Obj.LastName;
                addReferParameters[4] = new SqlParameter("@MiddleName", SqlDbType.NVarChar);
                addReferParameters[4].Value = Obj.MiddleName;
                addReferParameters[5] = new SqlParameter("@EmailAddresses", SqlDbType.VarChar);
                addReferParameters[5].Value = emailAddress;
                addReferParameters[6] = new SqlParameter("@oUserId", SqlDbType.Int);
                addReferParameters[6].Direction = ParameterDirection.Output;
                addReferParameters[7] = new SqlParameter("@JobTitle", SqlDbType.NVarChar);
                addReferParameters[7].Value = Obj.Title;
                addReferParameters[8] = new SqlParameter("@PrimaryTimeZone", SqlDbType.Int);
                addReferParameters[8].Value = PrimaryTimeZone;
                addReferParameters[9] = new SqlParameter("@DentrixProviderId", SqlDbType.VarChar);
                addReferParameters[9].Value = Obj.ProviderId;
                string fax = string.Empty;
                string home = string.Empty;
                string mobile = string.Empty;
                string work = string.Empty;
                string other = string.Empty;
                if (Obj.phonenumbers != null)
                {
                    foreach (var item in Obj.phonenumbers)
                    {
                        if (item.PhoneType == 0)//Home
                        {
                            home = item.PhoneNumber;
                        }
                        if (item.PhoneType == 1)//Mobile
                        {
                            mobile = item.PhoneNumber;
                        }
                        if (item.PhoneType == 2)//Work
                        {
                            work = item.PhoneNumber;
                        }
                        if (item.PhoneType == 3)//Fax
                        {
                            fax = item.PhoneNumber;
                        }
                    }
                }
                addReferParameters[10] = new SqlParameter("@Phone", SqlDbType.NVarChar);
                addReferParameters[10].Value = work;
                addReferParameters[11] = new SqlParameter("@IsPaid", DbType.Boolean);
                addReferParameters[11].Value = false;
                addReferParameters[12] = new SqlParameter("@AccountKey", SqlDbType.NVarChar);
                addReferParameters[12].Value = AccountKey;
                addReferParameters[13] = new SqlParameter("@Mobile", SqlDbType.NVarChar);
                addReferParameters[13].Value = mobile;
                addReferParameters[14] = new SqlParameter("@DentrixConnectorKey", SqlDbType.NVarChar);
                addReferParameters[14].Value = Obj.dentrixConnectorID;
                addReferParameters[15] = new SqlParameter("@Fax", SqlDbType.NVarChar);
                addReferParameters[15].Value = fax;
                addReferParameters[16] = new SqlParameter("@IsCalledFromImportMember", SqlDbType.Bit);
                addReferParameters[16].Value = 1;
                addReferParameters[17] = new SqlParameter("@DentalSpecialty", SqlDbType.NVarChar);
                addReferParameters[17].Value = Obj.Specialty;
                addReferParameters[18] = new SqlParameter("@Provtype", SqlDbType.Int);
                addReferParameters[18].Value = Obj.ProviderType;
                result = new clsHelper().ExecuteNonQuery("Usp_InsertProviderFromDentrix", addReferParameters);
                NewId = Convert.ToInt32(addReferParameters[6].Value); // Get New Doctor Id 
                if (NewId > 0)
                {
                    Address = InsertProviderAddressDetails(Obj, NewId);
                }
            }
            catch (Exception EX)
            {
                throw;
            }
            results.Add(NewId, Address);
            return results;
        }
        public static bool InsertProvierOtherDentrixDetials(InsertMember Obj, int UserId)
        {
            clsCommon Objcommon = new clsCommon();
            try
            {
                if (Convert.ToDateTime(Obj.automodifiedtimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    Obj.automodifiedtimestamp = null;
                }
                SqlParameter[] SQLpara = new SqlParameter[]
                {
                    new SqlParameter(){ParameterName ="@UserId",Value=UserId},
                    new SqlParameter(){ParameterName ="@Provtype",Value=Obj.ProviderType},
                    new SqlParameter(){ParameterName ="@Provclass",Value=Obj.ProviderClass},
                    new SqlParameter(){ParameterName ="@Providertypestring",Value=""},
                    new SqlParameter(){ParameterName ="@idnum",Value=Obj.DentrixId},
                    new SqlParameter(){ParameterName ="@ssn",Value=0.00},
                    new SqlParameter(){ParameterName ="@feesched",Value=0},
                    new SqlParameter(){ParameterName ="@phoneext",Value=0.00},
                    new SqlParameter(){ParameterName ="@isnonperson",Value=Obj.IsNonPerson},
                    new SqlParameter(){ParameterName ="@automodifiedtimestamp",Value=Obj.automodifiedtimestamp},
                    new SqlParameter(){ParameterName ="@provnum",Value=Convert.ToString(Obj.Identifier)},
                    new SqlParameter(){ParameterName ="@IsInactive",Value=Obj.IsInactive},
                    new SqlParameter(){ParameterName ="@DentrixConnectorId",Value=Obj.dentrixConnectorID},
                };
                return new clsHelper().ExecuteNonQuery("Usp_InsertUpdateProviderDentrixDetails", SQLpara);
            }
            catch (Exception Ex)
            {
                Objcommon.InsertErrorLog("InsertProvierOtherDentrixDetials", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable GetDentistRewardSettingList(int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter(){ParameterName="@UserId",Value=UserId}
                };
                return new clsHelper().DataTable("USP_GetDentistRewardSettingList", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData - GetDentistRewardSettingList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static DataTable GetRewardCodes()
        {
            try
            {
                return new clsHelper().DataTable("USP_RewardCodeList");

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData - GetRewardCodes", Ex.Message, Ex.StackTrace);
                throw;

            }

        }

        public static bool UpdateRewardListOfUser(DentistReward reward, int UserId)
        {
            SqlParameter[] param = new SqlParameter[]
            {
                new SqlParameter(){ParameterName="@RewardOptionId",Value=reward.OptionId },
                new SqlParameter(){ParameterName="@RewardMasterId",Value=reward.RewardMasterId },
                new SqlParameter(){ParameterName="@AccountId",Value=UserId},
                new SqlParameter(){ParameterName="@SettingId",Value=reward.SettingId},
                //new SqlParameter(){ParameterName="@EndRange",Value=reward.EndRange }
            };
            return new clsHelper().ExecuteNonQuery("Usp_UpdateRewardListOfUser", param);
        }

        public static bool CheckProviderEmailExists(List<string> Emails)
        {
            try
            {
                bool result = false;
                foreach (var item in Emails)
                {
                    DataTable dt = new DataTable();
                    SqlParameter[] strParameter = new SqlParameter[1];
                    strParameter[0] = new SqlParameter("@Email", SqlDbType.NVarChar);
                    strParameter[0].Value = item;
                    dt = new clsHelper().DataTable("USP_CheckEmailExistsAsDoctor", strParameter);
                    if (dt.Rows.Count > 0)
                    {
                        result = true;
                    }
                }
                return result;

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("InsertProvierOtherDentrixDetials", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int CheckProviderExists(int IdNumber, string DentrixConnectorId, int ProviderType,string ProviderId)
        {
            try
            {
                int result = 0;
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter { ParameterName="@IdNumber",Value=IdNumber },
                    new SqlParameter { ParameterName="@DentrixConnectorKey",Value=DentrixConnectorId },
                    new SqlParameter { ParameterName="@ProviderType", Value=ProviderType },
                    new SqlParameter{ParameterName="@DentrixProviderId",Value=ProviderId}
                };
                dt = new clsHelper().DataTable("Usp_CheckDentrixProviderExists", sqlpara);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(Convert.ToString(dt.Rows[0]["UserId"])))
                    {
                        result = Convert.ToInt32(dt.Rows[0]["UserId"]);
                    }
                }
                return result;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("CheckProviderExists", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int UpdateProviderDetailsForDentrixLoader(InsertMember Obj, int UserId)
        {
            try
            {
                int number = 0;
                string emailAddress = string.Empty;
                if (Obj.EmailAdresses != null)
                {
                    if (Obj.EmailAdresses.Count > 0)
                    {
                        emailAddress = Obj.EmailAdresses.FirstOrDefault();
                    }
                }
                // Build parameter array
                // Found the issue while test api
                SqlParameter[] addReferParameters = new SqlParameter[9];
                addReferParameters[0] = new SqlParameter("@Username", SqlDbType.NVarChar);
                addReferParameters[0].Value = emailAddress;
                addReferParameters[1] = new SqlParameter("@FirstName", SqlDbType.NVarChar);
                addReferParameters[1].Value = Obj.FirstName;
                addReferParameters[2] = new SqlParameter("@LastName", SqlDbType.NVarChar);
                addReferParameters[2].Value = Obj.LastName;
                addReferParameters[3] = new SqlParameter("@MiddleName", SqlDbType.NVarChar);
                addReferParameters[3].Value = Obj.MiddleName;
                addReferParameters[4] = new SqlParameter("@EmailAddresses", SqlDbType.VarChar);
                addReferParameters[4].Value = emailAddress;
                addReferParameters[5] = new SqlParameter("@UserId", SqlDbType.Int);
                addReferParameters[5].Value = UserId;
                addReferParameters[6] = new SqlParameter("@DentrixProviderId", SqlDbType.VarChar);
                addReferParameters[6].Value = Obj.ProviderId;
                addReferParameters[7] = new SqlParameter("@DentalSpecialty", SqlDbType.VarChar);
                addReferParameters[7].Value = Obj.Specialty;
                addReferParameters[8] = new SqlParameter("@Provtype", SqlDbType.Int);
                addReferParameters[8].Value = Obj.ProviderType;
                bool result = new clsHelper().ExecuteNonQuery("Usp_UpdateMemberDetailsForDentrix", addReferParameters);
                if (result)
                {
                    number = InsertProviderAddressDetails(Obj, UserId);
                }
                return number;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("UpdateProviderDetailsForDentrixLoader", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int InsertProviderAddressDetails(InsertMember Obj, int UserId)
        {
            try
            {
                int number = 0;
                bool result = false;
                if (Obj.Addresses != null && Obj.Addresses.Count > 0)
                {

                    int PrimaryTimeZone = 0;
                    PrimaryTimeZone = (PrimaryTimeZone == 0) ? 74 : PrimaryTimeZone;
                    Obj.ProviderId = (Obj.ProviderId != null) ? Obj.ProviderId : DBNull.Value;
                    string emailAddress = string.Empty;
                    string SecondaryEmail = string.Empty;
                    if (Obj.EmailAdresses != null)
                    {
                        if (Obj.EmailAdresses.Count > 0)
                        {
                            emailAddress = Obj.EmailAdresses.FirstOrDefault();
                        }
                    }
                    string fax = string.Empty;
                    string home = string.Empty;
                    string mobile = string.Empty;
                    string work = string.Empty;
                    string other = string.Empty;
                    if (Obj.phonenumbers != null)
                    {
                        foreach (var item in Obj.phonenumbers)
                        {
                            if (item.PhoneType == 0)//Home
                            {
                                home = item.PhoneNumber;
                            }
                            if (item.PhoneType == 1)//Mobile
                            {
                                mobile = item.PhoneNumber;
                            }
                            if (item.PhoneType == 2)//Work
                            {
                                work = item.PhoneNumber;
                            }
                            if (item.PhoneType == 3)//Fax
                            {
                                fax = item.PhoneNumber;
                            }
                        }
                    }
                    foreach (var item in Obj.Addresses)
                    {
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(item.DentrixId)))
                        {
                            SqlParameter[] sqlParam = new SqlParameter[14];
                            sqlParam[0] = new SqlParameter("@ExactAddress", SqlDbType.NVarChar);
                            sqlParam[0].Value = item.Street1;
                            sqlParam[1] = new SqlParameter("@Address2", SqlDbType.NVarChar);
                            sqlParam[1].Value = item.Street2;
                            sqlParam[2] = new SqlParameter("@City", SqlDbType.NVarChar);
                            sqlParam[2].Value = item.City;
                            sqlParam[3] = new SqlParameter("@State", SqlDbType.NVarChar);
                            sqlParam[3].Value = item.State;
                            sqlParam[4] = new SqlParameter("@ZipCode", SqlDbType.NVarChar);
                            sqlParam[4].Value = item.ZipCode;
                            sqlParam[5] = new SqlParameter("@Country", SqlDbType.NVarChar);
                            sqlParam[5].Value = "";
                            sqlParam[6] = new SqlParameter("@Username", SqlDbType.NVarChar);
                            sqlParam[6].Value = emailAddress;
                            sqlParam[7] = new SqlParameter("@UserId", SqlDbType.Int);
                            sqlParam[7].Value = UserId;
                            sqlParam[8] = new SqlParameter("@PrimaryTimeZone", SqlDbType.Int);
                            sqlParam[8].Value = PrimaryTimeZone;
                            sqlParam[9] = new SqlParameter("@Phone", SqlDbType.NVarChar);
                            sqlParam[9].Value = home;
                            sqlParam[10] = new SqlParameter("@Fax", SqlDbType.NVarChar);
                            sqlParam[10].Value = fax;
                            sqlParam[11] = new SqlParameter("@Mobile", SqlDbType.NVarChar);
                            sqlParam[11].Value = mobile;
                            sqlParam[12] = new SqlParameter("@DentrixAddressInfoId", SqlDbType.NVarChar);
                            sqlParam[12].Value = Convert.ToString(item.DentrixId);
                            sqlParam[13] = new SqlParameter("@WorkPhone", SqlDbType.NVarChar);
                            sqlParam[13].Value = work;
                            result = new clsHelper().ExecuteNonQuery("Usp_InsertProviderAddressDetailsFromDentrix", sqlParam);
                            number = 1;
                        }
                    }
                }
                else
                {
                    number = 1;
                }
                return number;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("InsertProviderAddressDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static string GetPlanAmountByPlanId(int PlanId)
        {
            try
            {
                string Amount = string.Empty;
                SqlParameter[] sqlpara = new SqlParameter[1]
                {
                    new SqlParameter{ParameterName="@PlanId",Value=PlanId}
                };
                DataTable dt = new clsHelper().DataTable("Usp_GetPlanDetailsByPlanId", sqlpara);
                if (dt != null && dt.Rows.Count > 0)
                {
                    int Amounts = Convert.ToInt32(dt.Rows[0]["MonthlyPrice"]);
                    Amount = Convert.ToString(Amounts);
                }
                return Amount;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("InsertProviderAddressDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int InsertDPMS_SyncDetails(DPMS_SyncDetails Obj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@DentrixConnectorKey",Value = Obj.DentrixConnectorId},
                    new SqlParameter{ParameterName="@RequestType",Value = Obj.RequestType},
                    new SqlParameter{ParameterName="@GUID",Value=Obj.GUID},
                    new SqlParameter{ParameterName="@RequestTime",Value=Obj.RequestTime},
                    new SqlParameter{ParameterName="@RequestId",Direction=ParameterDirection.Output,SqlDbType=SqlDbType.Int}
                };
                new clsHelper().ExecuteNonQuery("Usp_InsertDPMS_SyncDetails", sqlpara);
                int Result = Convert.ToInt32(sqlpara[4].Value);
                return Result;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--InsertDPMS_SyncDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool GetPatientListForDPMS(string DentrixConnectorId, int RequestId, bool SendAll)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@inDentrixConnectorKey",Value=DentrixConnectorId},
                    new SqlParameter{ParameterName="@RequestId",Value=RequestId},
                    new SqlParameter{ParameterName="@SendAll",Value=SendAll}
                };
                bool dt = new clsHelper().ExecuteNonQuery("Usp_InsertDPMSProcessQueue", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--GetPatientListForDPMS", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int GetPMSSyncDetailsByGUID(string GUID)
        {
            try
            {
                int RequestID = 0;
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@GUID",Value=GUID}
                };
                DataTable dt = new clsHelper().DataTable("USP_GetPMSSyncDetailsByGUID", sqlpara);
                if (dt != null && dt.Rows.Count > 0)
                {
                    RequestID = Convert.ToInt32(dt.Rows[0]["Id"]);
                }
                return RequestID;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--GetPMSSyncDetailsByGUID", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable GetPatientList(RetrievePatient Obj, int RequestId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@RequestId",Value=RequestId},
                    new SqlParameter{ParameterName="@Count",Value=Obj.FetchRecordCount},
                    new SqlParameter{ParameterName="@PageIndex",Value=Obj.PageIndex}
                };
                DataTable dt = new clsHelper().DataTable("USP_GetPatientRecordForDPMS", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--GetPMSSyncDetailsByGUID", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool UpdateDPMS_SyncDetails(int RequestId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@RequestId",Value=RequestId}
                };
                return new clsHelper().ExecuteNonQuery("USP_UpdateDPMSSync_Timing", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--UpdateDPMS_SyncDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static string GetTimeZoneOfDoctor(int doctorId)
        {
            SqlParameter[] sqlpara = new SqlParameter[1];
            sqlpara[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
            sqlpara[0].Value = doctorId;

            return new clsHelper().ExecuteScalar("USP_GetTimezoneOfDoctor", sqlpara);
        }
        public static bool AddUserAsRewardPartner(int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@UserId",Value=UserId}
                };
                return new clsHelper().ExecuteNonQuery("AddUserAsRewardPartner", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--AddUserAsRewardPartner", Ex.Message, Ex.StackTrace);
                throw;
            }

        }


        public static DataTable GetUserFullDetailsById(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@UserId",Value=UserId}
                };
                dt = new clsHelper().DataTable("Usp_GetUserFullDetailsById", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--GetUserFullDetailsById", Ex.Message, Ex.StackTrace);
                throw;
            }
        }



        public static DataTable GetMemberLocationDetails(int UserId, int LocationId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] param = new SqlParameter[]
                {
                new SqlParameter{ParameterName="@UserId",Value=UserId },
                 new SqlParameter{ParameterName="@LocationId",Value=LocationId }
                };
                dt = new clsHelper().DataTable("Usp_GetDentrixLocationDetails", param);
                return dt;
            }
            catch (Exception ex)
            {

                new clsCommon().InsertErrorLog("clsColleaguesData--GetMemberLocationDetails", ex.Message, ex.StackTrace);
                throw;
            }

        }

        /// <summary>
        /// Insert Company Details on Account reward Association table.
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="AccountId"></param>
        /// <param name="TransactionId"></param>
        /// <param name="RewardCompanyId"></param>
        /// <returns></returns>
        public static bool InsertCompanyonRewardPatform(int UserId, int AccountId, int TransactionId, int RewardCompanyId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@UserId",Value=UserId},
                    new SqlParameter{ParameterName="@RewardCompanyId",Value=RewardCompanyId},
                    new SqlParameter{ParameterName="@AccountId",Value=AccountId},
                    new SqlParameter{ParameterName="@TransactionId",Value=TransactionId},
                };
                return new clsHelper().ExecuteNonQuery("Usp_InsertRewardCompanyId", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--InsertCompanyonRewardPatform", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Insert Location Details on Location reward association table.
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="LocationId"></param>
        /// <param name="CompanyId"></param>
        /// <param name="TransactionId"></param>
        /// <param name="RewardLocationId"></param>
        /// <returns></returns>
        public static bool InsertLocationDetailsOfUser(int UserId, int LocationId, int CompanyId, int TransactionId, int RewardLocationId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@UserId",Value=UserId},
                    new SqlParameter{ParameterName="@LocationId",Value=LocationId},
                    new SqlParameter{ParameterName="@CompanyId",Value=CompanyId},
                    new SqlParameter{ParameterName="@TransactionId",Value=TransactionId},
                    new SqlParameter{ParameterName="@RewardLocationId",Value=RewardLocationId}
                };
                return new clsHelper().ExecuteNonQuery("Usp_InsertLocationDetailsOfUser", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--InsertLocationDetailsOfUser", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        //Procedure Code.
        public static string InsertRewardPointForProcedureCode(BO.ViewModel.ProcedureCode rc)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                    {
                    new SqlParameter{ParameterName="@ProcedureCodeId",Value=rc.ProcedureCodeId},
                    new SqlParameter{ParameterName="@UserId",Value=rc.AccountId},
                    new SqlParameter{ParameterName="@ProcedureCode",Value=rc.Procedurecode},
                    new SqlParameter{ParameterName="@ProcedureName",Value=rc.ProcedureName},
                    new SqlParameter{ParameterName="@SuperBucksReward",Value=rc.SuperBucksReward},
                    new SqlParameter{ParameterName="@CreatedBy",Value=rc.CreatedBy},
                    new SqlParameter{ParameterName="@ModifiedBy",Value=rc.ModifiedBy},
                    };
                return new clsHelper().ExecuteScalar("Usp_InsertProcedureCodesofUser", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--InsertRewardPointForProcedureCode", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool InsertProcedureCodesForExams(ProcedureExamCode ecobj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                    {
                        new SqlParameter{ParameterName="@ExamId",Value=ecobj.ExamId},
                        new SqlParameter{ParameterName="@AccountId",Value=ecobj.AccountId},
                        new SqlParameter{ParameterName="@ProcedureName",Value=ecobj.ProcedureName},
                        new SqlParameter{ParameterName="@ExamCode",Value=ecobj.ExamCode},
                        new SqlParameter{ParameterName="@CreatedBy",Value=ecobj.CreatedBy},
                        new SqlParameter{ParameterName="@ModifiedBy",Value=ecobj.ModifiedBy},
                    };
                return new clsHelper().ExecuteNonQuery("Usp_InsertExamCodeforUser", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--InsertProcedureCodesForExams", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static string InsertRewardMultiplierForLongevity(RewardLongevity relobj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                   {
                         new SqlParameter{ParameterName="@LMId",Value=relobj.LMId},
                         new SqlParameter{ParameterName="@UserId",Value=relobj.AccountId},
                         new SqlParameter{ParameterName="@MonthsofLongevity",Value=relobj.MonthsofLongevity},
                         new SqlParameter{ParameterName="@Multiplier",Value=relobj.Multiplier},
                         new SqlParameter{ParameterName="@CreatedBy",Value=relobj.CreatedBy},
                         new SqlParameter{ParameterName="@ModifiedBy",Value=relobj.ModifiedBy},
                         new SqlParameter{ParameterName="@PlanId",Value=relobj.PlanId}
                   };
                return new clsHelper().ExecuteScalar("Usp_InsertLongevityMultiplerByUserId", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--InsertRewardMultiplierForLongevity", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool InsertPointMultiplierForPointBalance(PointBalance pbobj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@PointId",Value=pbobj.PointId},
                    new SqlParameter{ParameterName="@AccountId",Value=pbobj.AccountId},
                    new SqlParameter{ParameterName="@PointLevel",Value=pbobj.PointLevel},
                    new SqlParameter{ParameterName="@Multiplier",Value=pbobj.Multiplier},
                    new SqlParameter{ParameterName="@CreatedBy",Value=pbobj.CreatedBy},
                    new SqlParameter{ParameterName="@ModifiedBy",Value=pbobj.ModifiedBy},
                };
                return new clsHelper().ExecuteNonQuery("Usp_InsertPointMultiplierofUser", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--InsertPointMultiplierForPointBalance", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool DeleteRewardPointForProcedureCode(ProcedureCodes rc)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@ProcedureCodeId",Value=rc.ProcedureCodeId},
                };
                return new clsHelper().ExecuteNonQuery("USP_DeleteProcedureCodesofUser", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--DeleteRewardPointForProcedureCode", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool DeleteProcedureCodesForExams(ProcedureExamCode ecobj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@ExamId",Value=ecobj.ExamId},
                };
                return new clsHelper().ExecuteNonQuery("USp_DeleteExamCodeforUser", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--DeleteProcedureCodesForExams", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static string DeleteRewardMultiplierForLongevity(int LMId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@LMId",Value=LMId}
                };
                return new clsHelper().ExecuteScalar("USP_DeleteLongevityMultiplerByUserId", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--DeleteRewardMultiplierForLongevity", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool DeletePointMultiplierForPointBalance(PointBalance pbobj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
               {
                    new SqlParameter{ParameterName="@PointId",Value=pbobj.PointId},
               };
                return new clsHelper().ExecuteNonQuery("USP_DeletePointMultiplierofUser", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData-DeletePointMultiplierForPointBalance", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static DataTable AddEditRewardCode(ProcedureCodes pc)
        {
            DataTable dt = null;
            clsHelper clshelper = new clsHelper();
            SqlParameter[] sqlpara = new SqlParameter[1];
            sqlpara[0] = new SqlParameter("@ProcedureCodeId", SqlDbType.Int);
            sqlpara[0].Value = pc.ProcedureCodeId;
            dt = clshelper.DataTable("USP_GetProcedureCodeById", sqlpara);
            return dt;
        }
        public static DataTable GetProcedureCodeDetails(int AccountId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[] {
                    new SqlParameter{ParameterName="@UserId",Value=AccountId}
                };
                dt = new clsHelper().DataTable("", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData-GetProcedureCodeDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        //For Super Plan
        public static DataTable AddEditSuperPlan(SuperPlan sp)
        {
            DataTable dt = null;
            clsHelper clshelper = new clsHelper();
            SqlParameter[] sqlpara = new SqlParameter[1];
            sqlpara[0] = new SqlParameter("@PlanId", SqlDbType.Int);
            sqlpara[0].Value = sp.PlanId;
            dt = clshelper.DataTable("USP_GetSUPPlanById", sqlpara);
            return dt;
        }

        public static DataTable GetSuperPlan(int UserId)
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                ds = new clsHelper().DataTable("USP_GetSuperPlanData", strParameter);
                return ds;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData - GetSuperPlanData", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        public static string AddSuperPlan(SuperPlan sp)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@PlanName",Value=sp.PlanName},
                    new SqlParameter{ParameterName="@UserId",Value=sp.UserId},
                    new SqlParameter{ParameterName="@Amount",Value=sp.Amount},
                    new SqlParameter{ParameterName="@CreatedBy",Value=sp.CreatedBy},
                    new SqlParameter{ParameterName="@PlanId",Value=sp.PlanId},
                    new SqlParameter{ParameterName="@RewardPoint",Value=sp.RewardPoint},
                     new SqlParameter{ParameterName="@Points",Value=sp.Points},
                     new SqlParameter{ParameterName="@ExamCodes",Value=sp.ExamCodes}
                };
                return new clsHelper().ExecuteScalar("Usp_SUP_InsertPlan", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--AddSuperPlan", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static string DeleteSuperPlan(SuperPlan sp)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@IsDeleted",Value=sp.IsDeleted},
                    new SqlParameter{ParameterName="@CreatedBy",Value=sp.CreatedBy},
                    new SqlParameter{ParameterName="@PlanId",Value=sp.PlanId},
                     new SqlParameter{ParameterName="@UserId",Value=sp.UserId}
                };
                return new clsHelper().ExecuteScalar("Usp_SUP_InsertPlan", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--DeleteSuperPlan", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static DataTable GetProcedureCodeData(int UserId, int PlanId)
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@PlanId", SqlDbType.Int);
                strParameter[1].Value = PlanId;
                ds = new clsHelper().DataTable("USP_GetProceCodeData", strParameter);
                return ds;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData - GetProcedureCodeData", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        public static string InsertProcedureExamAssociation(ProcedureCode sp, int UerId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@ProcedureId",Value=sp.ProcedureCodeId},
                    new SqlParameter{ParameterName="@IsExam",Value=sp.IsExam},
                    new SqlParameter{ParameterName="@PlanId",Value=sp.PlanId},
                    new SqlParameter{ParameterName="@SuperBucksRewards",Value=sp.SuperBucksReward},
                    new SqlParameter{ParameterName="@CreatedBy",Value=UerId},
                };
                return new clsHelper().ExecuteScalar("Usp_SUP_InsertProcedureExamAsociation", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--InsertProcedureExamAssociation", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static string InsertProcedureForPlan(ProcedureCode sp, int UerId, int PlanId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@ProcedureId",Value=sp.ProcedureCodeId},
                    new SqlParameter{ParameterName="@PlanId",Value=PlanId},
                    new SqlParameter{ParameterName="@CreatedBy",Value=UerId},
                };
                return new clsHelper().ExecuteScalar("Usp_SUP_InsertProcedure", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--InsertProcedureForPlan", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static string DeleteProcedureForPlan(int ProcedureCodeId, int PlanId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@PlanId",Value=PlanId},
                    new SqlParameter{ParameterName="@ProcedureCodeId",Value=ProcedureCodeId}
                };
                return new clsHelper().ExecuteScalar("Usp_SUP_DeleteProcedure", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--DeleteProcedureForPlan", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static string DeleteMapProcedure(int ProcedureCodeId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@ProcedureCodeId",Value=ProcedureCodeId}
                };
                return new clsHelper().ExecuteScalar("Usp_SUP_DeleteProcedure", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--DeleteProcedureForPlan", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        //For LongevityMultiplier
        public static DataTable Get_LongevityMultiplier(int UserId, int PlanId)
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@PlanId", SqlDbType.Int);
                strParameter[1].Value = PlanId;
                ds = new clsHelper().DataTable("USP_SUP_GetLongevityMultiplier", strParameter);
                return ds;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData - GetLongevityMultiplier", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        public static DataTable GetLongitivtyPopUp(RewardsLongevity rl)
        {
            DataTable dt = null;
            clsHelper clshelper = new clsHelper();
            SqlParameter[] sqlpara = new SqlParameter[2];
            sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
            sqlpara[0].Value = rl.AccountId;
            sqlpara[1] = new SqlParameter("@LM_Id", SqlDbType.Int);
            sqlpara[1].Value = rl.LM_Id;
            dt = clshelper.DataTable("USP_SUP_GetLongevityById", sqlpara);
            return dt;
        }

        public static DataTable GetAllProcedure(int UserId, int PlanId)
        {
            DataTable ds = new DataTable();
            try
            {
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@PlanId", SqlDbType.Int);
                strParameter[1].Value = PlanId;
                ds = new clsHelper().DataTable("USP_Sup_GetAllProcedure", strParameter);
                return ds;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData - GetAllProcedure", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        /// <summary>
        /// This Method Get Point Multiplier Details form database for Edit time.
        /// </summary>
        /// <param name="PointId"></param>
        /// <returns></returns>
        public static DataTable GetPointMultiplier(int PointId, int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@UserId",Value=UserId},
                    new SqlParameter{ParameterName="@PointId",Value=PointId}
                };
                return new clsHelper().DataTable("Usp_GetPointMultiplierById", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--InsertProcedureExamAssociation", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// This Method Insert Point Multiplier on DB
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public static int InsertPointMultiplierOfUser(PointsBalances Obj, int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@UserId",Value=UserId},
                    new SqlParameter{ParameterName="@PointLevel",Value=Obj.PointLevel},
                    new SqlParameter{ParameterName="@Multiplier",Value=Obj.Multiplier},
                    new SqlParameter{ParameterName="@PlanId",Value=Obj.PlanId},
                    new SqlParameter{ParameterName="@PM_Id",Value=Obj.PM_Id},
                    new SqlParameter{ParameterName="@Ooutput",Direction=ParameterDirection.Output,SqlDbType=SqlDbType.Int}
                };
                new clsHelper().ExecuteNonQuery("Usp_Sup_InsertPointMultiplierByUser", sqlpara);
                int point = Convert.ToInt32(sqlpara[5].Value);
                return point;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--InsertPointMultiplierOfUser", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// This Method is used for delete the Point Multiplier.
        /// </summary>
        /// <param name="PointId"></param>
        /// <returns></returns>
        public static bool DeletePointMulitplier(int PMId, int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@PMId",Value=PMId},
                    new SqlParameter{ParameterName="@UserId",Value=UserId}
                };
                return new clsHelper().ExecuteNonQuery("Usp_Sup_DeletePointMultiplier", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--DeletePointMulitplier", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool AddPointsAndExamCodes(int UserId, decimal points, int Examcodes)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@UserId",Value=UserId},
                    new SqlParameter{ParameterName="@Points",Value=points},
                    new SqlParameter{ParameterName="@ExamCodes",Value=Examcodes}
                };
                return new clsHelper().ExecuteNonQuery("Usp_Sup_InsertUpdatePointsAndExamCodeOfUser", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--AddPointsAndExamCodes", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static string GetPointsAndExamExists(int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@UserId",Value=UserId}
                };
                return new clsHelper().ExecuteScalar("Usp_Sup_GetPointsAndExamCodeOfUser", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--GetPointsAndExamExists", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        //For WebHook
        public DataTable GetMemberExists(string CustomerId)
        {
            DataTable dt = new DataTable();
            SqlParameter[] strParameter = new SqlParameter[1];
            strParameter[0] = new SqlParameter("@CustomerId", CustomerId);
            dt = clshelper.DataTable("GetMemberExists", strParameter);
            return dt;
        }

        /// <summary>
        /// Insert the data of subscription
        /// </summary>
        /// <param name="SEObj"></param>
        /// <returns></returns>
        public string InsertRecurringData(subscriptionEvent SEObj)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[14];
                strParameter[0] = new SqlParameter("@NotificationId", SEObj.notificationId);
                strParameter[1] = new SqlParameter("@EventType", SEObj.eventType);
                strParameter[2] = new SqlParameter("@EventDate", SEObj.eventDate);
                strParameter[3] = new SqlParameter("@WebhookId", SEObj.webhookId);
                strParameter[4] = new SqlParameter("@Status", SEObj.payload.status);
                strParameter[5] = new SqlParameter("@Name", SEObj.payload.name);
                strParameter[6] = new SqlParameter("@Amount", SEObj.payload.amount);
                strParameter[7] = new SqlParameter("@CustomerProfileId", SEObj.payload.profile.customerProfileId);
                strParameter[8] = new SqlParameter("@CustomerPaymentProfileId", SEObj.payload.profile.customerPaymentProfileId);
                strParameter[9] = new SqlParameter("@EntityName", SEObj.payload.entityName);
                strParameter[10] = new SqlParameter("@SubscriptionId", SEObj.payload.id);
                strParameter[11] = new SqlParameter("@CustomerShippingAddressId", SEObj.payload.profile.customerShippingAddressId);
                strParameter[12] = new SqlParameter("@CustomerType", SEObj.payload.profile.customerType);
                strParameter[13] = new SqlParameter("@Events", (int)WebhookEventType.SubscriptionEvent);
                return new clsHelper().ExecuteScalar("USP_AddRecurringsubscription", strParameter);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--InsertRecurringData--subscriptionEvent", Ex.Message, Ex.StackTrace);
                throw;
            }
        }


        /// <summary>
        /// Insert the data of customer event
        /// </summary>
        /// <param name="CustObj"></param>
        /// <returns></returns>
        public string InsertRecurringData(customerEvent CustObj)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[11];
                strParameter[0] = new SqlParameter("@NotificationId", CustObj.notificationId);
                strParameter[1] = new SqlParameter("@EventType", CustObj.eventType);
                strParameter[2] = new SqlParameter("@EventDate", CustObj.eventDate);
                strParameter[3] = new SqlParameter("@WebhookId", CustObj.webhookId);
                strParameter[4] = new SqlParameter("@CustomerPaymentProfileId", CustObj.payload.paymentProfiles[0].id);
                strParameter[5] = new SqlParameter("@CustomerType", CustObj.payload.paymentProfiles[0].customerType);
                strParameter[6] = new SqlParameter("@MerchantCustomerId", CustObj.payload.merchantCustomerId);
                strParameter[7] = new SqlParameter("@Description", CustObj.payload.description);
                strParameter[8] = new SqlParameter("@EntityName", CustObj.payload.entityName);
                strParameter[9] = new SqlParameter("@CustomerProfileId", CustObj.payload.id);
                strParameter[10] = new SqlParameter("@Events", (int)WebhookEventType.CustomerEvent);
                return new clsHelper().ExecuteScalar("USP_AddRecurringsubscription", strParameter);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--InsertRecurringData--customerEvent", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Insert the data of payment event
        /// </summary>
        /// <param name="PayObj"></param>
        /// <returns></returns>
        public string InsertRecurringData(paymentEvent PayObj)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[11];
                strParameter[0] = new SqlParameter("@NotificationId", PayObj.notificationId);
                strParameter[1] = new SqlParameter("@EventType", PayObj.eventType);
                strParameter[2] = new SqlParameter("@EventDate", PayObj.eventDate);
                strParameter[3] = new SqlParameter("@WebhookId", PayObj.webhookId);
                strParameter[4] = new SqlParameter("@ResponseCode", PayObj.payload.responseCode);
                strParameter[5] = new SqlParameter("@AuthCode", PayObj.payload.authCode);
                strParameter[6] = new SqlParameter("@AvsResponse", PayObj.payload.avsResponse);
                strParameter[7] = new SqlParameter("@Amount", PayObj.payload.authAmount);
                strParameter[8] = new SqlParameter("@EntityName", PayObj.payload.entityName);
                strParameter[9] = new SqlParameter("@TransactionId", PayObj.payload.id);
                strParameter[10] = new SqlParameter("@Events", (int)WebhookEventType.PaymentEvent);
                return new clsHelper().ExecuteScalar("USP_AddRecurringsubscription", strParameter);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--InsertRecurringData--paymentEvent", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Insert the data of payment profile event
        /// </summary>
        /// <param name="PayProObj"></param>
        /// <returns></returns>
        public string InsertRecurringData(paymentProfileEvent PayProObj)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[9];
                strParameter[0] = new SqlParameter("@NotificationId", PayProObj.notificationId);
                strParameter[1] = new SqlParameter("@EventType", PayProObj.eventType);
                strParameter[2] = new SqlParameter("@EventDate", PayProObj.eventDate);
                strParameter[3] = new SqlParameter("@WebhookId", PayProObj.webhookId);
                strParameter[4] = new SqlParameter("@CustomerProfileId", PayProObj.payload.customerProfileId);
                strParameter[5] = new SqlParameter("@EntityName", PayProObj.payload.entityName);
                strParameter[6] = new SqlParameter("@CustomerPaymentProfileId", PayProObj.payload.id);
                strParameter[7] = new SqlParameter("@customerType", PayProObj.payload.customerType);
                strParameter[8] = new SqlParameter("@Events", (int)WebhookEventType.PaymentProfileEvent);
                return new clsHelper().ExecuteScalar("USP_AddRecurringsubscription", strParameter);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--InsertRecurringData--paymentProfileEvent", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Insert the data of fraud event
        /// </summary>
        /// <param name="FraObj"></param>
        /// <returns></returns>
        public string InsertRecurringData(fraudEvent FraObj)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[13];
                strParameter[0] = new SqlParameter("@NotificationId", FraObj.notificationId);
                strParameter[1] = new SqlParameter("@EventType", FraObj.eventType);
                strParameter[2] = new SqlParameter("@EventDate", FraObj.eventDate);
                strParameter[3] = new SqlParameter("@WebhookId", FraObj.webhookId);
                strParameter[4] = new SqlParameter("@ResponseCode", FraObj.payload.responseCode);
                strParameter[5] = new SqlParameter("@AuthCode", FraObj.payload.authCode);
                strParameter[6] = new SqlParameter("@AvsResponse", FraObj.payload.avsResponse);
                strParameter[7] = new SqlParameter("@Amount", FraObj.payload.authAmount);
                strParameter[8] = new SqlParameter("@EntityName", FraObj.payload.entityName);
                strParameter[9] = new SqlParameter("@TransactionId", FraObj.payload.id);
                strParameter[10] = new SqlParameter("@FraudFilter", FraObj.payload.fraudList[0].fraudFilter);
                strParameter[11] = new SqlParameter("@FraudAction", FraObj.payload.fraudList[0].fraudAction);
                strParameter[12] = new SqlParameter("@Events", (int)WebhookEventType.FraudEvent);
                return new clsHelper().ExecuteScalar("USP_AddRecurringsubscription", strParameter);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--InsertRecurringData--fraudEvent", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool RegisterSuperBuck(int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@UserId",Value=UserId}

                };
                return new clsHelper().ExecuteNonQuery("Usp_Sup_InsertUpdatePointsAndExamCodeOfUser", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--AddPointsAndExamCodes", Ex.Message, Ex.StackTrace);
                throw;
            }
        }


        public static bool Inserthasbuttonval(ColleagueDetails ClgData, int UserId)
        {
            try
            {
                bool res = true;
                SqlParameter[] para = new SqlParameter[3];
                para[0] = new SqlParameter("@HasButton", SqlDbType.Bit);
                para[0].Value = ClgData.HasButton;
                para[1] = new SqlParameter("@ReceiverUserId", SqlDbType.Int);
                para[1].Value = ClgData.ColleagueId;
                para[2] = new SqlParameter("@GivenUserId", SqlDbType.Int);
                para[2].Value = UserId;
                res = new clsHelper().ExecuteNonQuery("USP_InsertHasButton", para);
                return res;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--Inserthasbuttonval", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool EnableDisablehasbuttonval(int ColleagueId, int GivenUserId)
        {
            try
            {
                bool res = true;
                SqlParameter[] para = new SqlParameter[2];
                para[0] = new SqlParameter("@ReceiverUserId", SqlDbType.Int);
                para[0].Value = ColleagueId;
                para[1] = new SqlParameter("@GivenUserId", SqlDbType.Int);
                para[1].Value = GivenUserId;
                res = new clsHelper().ExecuteNonQuery("USP_InsertHasButton", para);
                return res;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--EnableDisablehasbuttonval", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool DisableHasButton(int ColleagueId, int GivenUserId)
        {
            try
            {
                bool res = true;
                SqlParameter[] para = new SqlParameter[2];
                para[0] = new SqlParameter("@ReceiverUserId", SqlDbType.Int);
                para[0].Value = ColleagueId;
                para[1] = new SqlParameter("@GivenUserId", SqlDbType.Int);
                para[1].Value = GivenUserId;
                res = new clsHelper().ExecuteNonQuery("USP_DisableHasButton", para);
                return res;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--EnableDisablehasbuttonval", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Getting Dentists list based on Zip-code from database.
        /// </summary>
        /// <param name="ZipCode"></param>
        /// <param name="Mile"></param>
        /// <returns></returns>
        public DataTable GetDentistByLocation(string ZipCode, int Mile, BO.Enums.Common.PMSProviderFilterType provFilter)
        {
            try
            {
                DataTable dsdentist = new DataTable();
                SqlParameter[] GetDentistParams = new SqlParameter[3];
                GetDentistParams[0] = new SqlParameter("@ZipCode", SqlDbType.VarChar);
                GetDentistParams[0].Value = ZipCode;
                GetDentistParams[1] = new SqlParameter("@Miles", SqlDbType.Int);
                GetDentistParams[1].Value = Mile;
                GetDentistParams[2] = new SqlParameter("@ProvTypefilter", SqlDbType.Int);
                GetDentistParams[2].Value = (int)provFilter;
                dsdentist = clshelper.DataTable("GetDentistListByZipCode", GetDentistParams);
                return dsdentist;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--GetDentistByLocation", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        /// <summary>
        /// Getting Zip-code based on passing Latitude and Longitude.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public DataTable GetZipCodeByLatitudeLongitude(Searchlatitudelongitude Obj)
        {
            try
            {
                DataTable dsdentist = new DataTable();
                SqlParameter[] GetDentistParams = new SqlParameter[2];
                GetDentistParams[0] = new SqlParameter("@Latitude", SqlDbType.Decimal);
                GetDentistParams[0].Value = Obj.Latitude;
                GetDentistParams[1] = new SqlParameter("@Longitude", SqlDbType.Decimal);
                GetDentistParams[1].Value = Obj.Longitude;
                dsdentist = clshelper.DataTable("GetZipCodeByLatitudeLongitude", GetDentistParams);
                return dsdentist;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public DataTable GetColleagueListById(int userId, PMSProviderFilterType provfilter, string strSearchText)
        {
            try
            {
                DataTable dsdentist = new DataTable();
                SqlParameter[] GetDentistParams = new SqlParameter[3];
                GetDentistParams[0] = new SqlParameter("@userId", SqlDbType.Int);
                GetDentistParams[0].Value = userId;
                GetDentistParams[1] = new SqlParameter("@ProvTypefilter", SqlDbType.Int);
                GetDentistParams[1].Value = provfilter;
                GetDentistParams[2] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
                GetDentistParams[2].Value = strSearchText;
                dsdentist = clshelper.DataTable("GetColleagueListById_NEW", GetDentistParams);
                return dsdentist;
            }
            catch (Exception)
            {
                throw;
            }

        }

     
        public DataTable GetColleagueExistByEmail(int UserId, string Email)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.NVarChar);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@Email", SqlDbType.NVarChar);
                strParameter[1].Value = Email;
                dt = clshelper.DataTable("USP_GetColleagueExistByEmail", strParameter);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static DataTable GetOneclickReferralDetails()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = new clsHelper().DataTable("USP_GetReferralByOneClickReferral", null);
                return dt;
            }
            catch (Exception ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--GetOneclickReferralDetails", ex.Message, ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This method is use for get the list of plan for user from SUP_Plan table
        /// </summary>
        /// <param name="AccountId"></param>
        /// <returns></returns>
        public static DataTable GetPlanListForUser(int AccountId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@AccountId", SqlDbType.Int);
                strParameter[0].Value = AccountId;
                dt = new clsHelper().DataTable("USP_SUP_GetPlanByUserId", strParameter);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData-GetPlanListForUser", Ex.Message, Ex.StackTrace);
                return null;
            }
        }
        /// <summary>
        /// This Method used for getting Reward Master code of Register reward.
        /// </summary>
        /// <param name="RewardPartnerId"></param>
        /// <returns></returns>
        public static string GetRegisterRewardMasterCode(int RewardPartnerId)
        {
            try
            {
                string MasterCode = string.Empty;
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@RewardPartnerId",Value=RewardPartnerId}
                };
                DataTable dt = new clsHelper().DataTable("Usp_GetRegisterRewardMasterCode", sqlpara);
                if (dt != null && dt.Rows.Count > 0)
                {
                    MasterCode = Convert.ToString(dt.Rows[0]["Code"]);
                }
                return MasterCode;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData-GetPlanListForUser", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static DataTable GetAccountIdByUserId(int ID, string Operation)
        {
            DataTable dt = new DataTable();
            clsHelper clshelper = new clsHelper();
            try
            {
                SqlParameter[] AddMemberFirstParams = new SqlParameter[2];
                AddMemberFirstParams[0] = new SqlParameter("@UserID", SqlDbType.Int);
                AddMemberFirstParams[0].Value = ID;
                AddMemberFirstParams[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                AddMemberFirstParams[1].Value = Operation;
                dt = clshelper.DataTable("GetAccountIdByUserId", AddMemberFirstParams);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetColleagueDetailById(int userId)
        {
            try
            {
                DataTable dsdentist = new DataTable();
                SqlParameter[] GetDentistParams = new SqlParameter[1];
                GetDentistParams[0] = new SqlParameter("@userId", SqlDbType.Int);
                GetDentistParams[0].Value = userId;

                dsdentist = clshelper.DataTable("GetColleagueDetailById", GetDentistParams);
                return dsdentist;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public static bool InsertDefaultRewardSettingOfDoctor(int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@UserId",Value=UserId}
                };
                return new clsHelper().ExecuteNonQuery("Usp_InsertDefaultRewardSetting", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData---InsertDefaultRewardSettingOfDoctor", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This method used for getting referral form setting.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static DataTable GetUserReferrlaFormSetting(int UserId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                new SqlParameter{ParameterName="@ColleagueId",Value=UserId}
                };
                DataTable dt = new clsHelper().DataTable("Usp_GetReferralSpecialityVisibility", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData---GetUserReferrlaFormSetting", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static DataTable GetUserInnerReferralFormSetting(int UserId, int regardId)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter{ParameterName="@regardId",Value=regardId},
                new SqlParameter{ParameterName="@ColleagueId",Value=UserId}
            };
                DataTable dt = new clsHelper().DataTable("Usp_GetReferralItemVisibility", sqlpara);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData---GetUserInnerReferralFormSetting", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// This method get S1p token from database.
        /// </summary>
        /// <returns></returns>
        public static string GetS1pTokenFromDatabase()
        {
            try
            {
                string Token = string.Empty;
                DataTable dt = new clsHelper().DataTable("Usp_GetS1pTokenFromDatabase");
                if (dt != null && dt.Rows.Count > 0)
                {
                    Token = Convert.ToString(dt.Rows[0]["Token"]);
                }
                return Token;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData---GetS1pTokenFromDatabase", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Insert/Update S1p API Logs on Recordlinc database.
        /// No need to Insert an Error log if we are inserting that then we will stuck on loop.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public static int InsertUpdateS1pAPICallLogHistory(S1p_LogHistory Obj)
        {
            int LogId = 0;
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter{ParameterName="@LogId",Value =Obj.LogId},
                    new SqlParameter{ParameterName="@StatusCode",Value =Obj.StatusCode},
                    new SqlParameter{ParameterName="@CreatedDate",Value =Obj.CreatedDate},
                    new SqlParameter{ParameterName="@Notes",Value =Obj.Notes},
                    new SqlParameter{ParameterName="@RequestContentType",Value =Obj.RequestContentType},
                    new SqlParameter{ParameterName="@RequestContentBody",Value =Obj.RequestContentBody},
                    new SqlParameter{ParameterName="@ReqeustUri",Value =Obj.RequestUri},
                    new SqlParameter{ParameterName="@RequestMethod",Value =Obj.RequestMethod},
                    new SqlParameter{ParameterName="@RequestRouteTemplete",Value =Obj.RequestRouteTemplete},
                    new SqlParameter{ParameterName="@RequestRouteData",Value =Obj.RequestRouteData},
                    new SqlParameter{ParameterName="@RequestHeaders",Value =Obj.RequestHeaders},
                    new SqlParameter{ParameterName="@RequestTime",Value =Obj.RequestTime},
                    new SqlParameter{ParameterName="@ResponseContentType",Value =Obj.ResponseContentBody},
                    new SqlParameter{ParameterName="@ResponseContentBody",Value =Obj.ResponseContentBody},
                    new SqlParameter{ParameterName="@ResponseStatusCode",Value =Obj.ResponseStatusCode},
                    new SqlParameter{ParameterName="@ResponseHeaaders",Value =Obj.ResponseHeaders},
                    new SqlParameter{ParameterName="@ResponseTime",Value =Obj.ResponseTime},
                    new SqlParameter{ParameterName="@Token",Value =Obj.Token},
                    new SqlParameter{ParameterName="@ErrorMessage",Value =Obj.ErrorMessage},
                    new SqlParameter{ParameterName="@ErrorStackTrace",Value =Obj.ErrorStackTrace},
                    new SqlParameter{ParameterName="@ErrorInnerException",Value =Obj.ErrorInnerException},
                    new SqlParameter{ParameterName="@ErrorInnerStackTrace",Value =Obj.ErrorInnerStackTrace},
                    new SqlParameter{ParameterName="@IsSuccess",Value =Obj.IsSuccess},
                    new SqlParameter{ParameterName="@OoutputLogId",Direction=ParameterDirection.Output,SqlDbType=SqlDbType.Int}
                };
                new clsHelper().ExecuteNonQuery("Usp_S1p_InsertUpdateLogHistory", sqlpara);
                LogId = Convert.ToInt32(sqlpara[23].Value);

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData---GetS1pTokenFromDatabase", Ex.Message, Ex.StackTrace);
            }
            return LogId;
        }

        public DataSet GetDoctorDetailsById(int UserId)
        {

            try
            {
                DataSet ds = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Userid", SqlDbType.Int);
                strParameter[0].Value = UserId;

                ds = clshelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", strParameter);
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public DataSet DashboardColleaguePatientName(string SearchName)
        {

            try
            {
                DataSet ds = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Name", SqlDbType.NVarChar);
                strParameter[0].Value = SearchName;

                ds = clshelper.GetDatasetData("GetColeegueOrPatientByName", strParameter);
                return ds;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public static string ReplaceSingleQuote(string Inputstring)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(Inputstring))
                {
                    Inputstring = Inputstring.Replace("\'", "\''");
                    return Inputstring;
                }
                else
                {
                    return Inputstring;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--RemoveSpecialCaracters", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable CheckUserHasEnableOneClick(int UserId)
        {
            try
            {
                DataTable ds = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                ds = new clsHelper().DataTable("Usp_GetReferralStustsofUser", strParameter);
                return ds;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--CheckUserHasEnableOneClick", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable GetMessageConversationalView(FilterMessageConversation Obj)
        {
            try
            {
                DataTable Dt = new DataTable();
                SqlParameter[] sqlParameters = new SqlParameter[15];
                sqlParameters[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                sqlParameters[0].Value = Obj.MessageId;
                sqlParameters[1] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlParameters[1].Value = Obj.UserId;
                sqlParameters[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                sqlParameters[2].Value = Obj.PageSize;
                sqlParameters[3] = new SqlParameter("@PageIndex", SqlDbType.Int);
                sqlParameters[3].Value = Obj.PageIndex;
                sqlParameters[4] = new SqlParameter("@SortColumn", SqlDbType.Int);
                sqlParameters[4].Value = Obj.SortColumn;
                sqlParameters[5] = new SqlParameter("@SortDirection", SqlDbType.Int);
                sqlParameters[5].Value = Obj.SortDirection;
                sqlParameters[6] = new SqlParameter("@DoctorName", SqlDbType.NVarChar);
                sqlParameters[6].Value = Obj.DoctorName;
                sqlParameters[7] = new SqlParameter("@FromDate", SqlDbType.DateTime);
                sqlParameters[7].Value = Obj.FromDate;
                sqlParameters[8] = new SqlParameter("@Todate", SqlDbType.DateTime);
                sqlParameters[8].Value = Obj.Todate;
                sqlParameters[9] = new SqlParameter("@Disposition", SqlDbType.NVarChar);
                sqlParameters[9].Value = Obj.Disposition;
                sqlParameters[10] = new SqlParameter("@Message", SqlDbType.NVarChar);
                sqlParameters[10].Value = Obj.Message;
                sqlParameters[11] = new SqlParameter("@MessageStatus", SqlDbType.Int);
                sqlParameters[11].Value = Obj.MessageStatus;
                sqlParameters[12] = new SqlParameter("@PatietName", SqlDbType.NVarChar);
                sqlParameters[12].Value = Obj.PatientName;
                sqlParameters[13] = new SqlParameter("@MessageFrom", SqlDbType.Int);
                sqlParameters[13].Value = Obj.MessageFrom;
                sqlParameters[14] = new SqlParameter("@InsuraceStatus", SqlDbType.Int);
                sqlParameters[14].Value = Obj.InsuranceStatus;

                Dt = new clsHelper().DataTable("USP_GetMessageConversation", sqlParameters);
                return Dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--GetMessageConversationalView", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static DataTable GetCommunicationList(FilterMessageConversation Obj)
        {
            try
            {
                DataTable Dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[] {
                new SqlParameter() {ParameterName = "@MessageId",Value= Obj.MessageId },
                new SqlParameter() {ParameterName = "@UserId",Value= Obj.UserId },
                new SqlParameter() {ParameterName = "@PageSize",Value= Obj.PageSize },
                new SqlParameter() {ParameterName = "@PageIndex",Value= Obj.PageIndex },
                new SqlParameter() {ParameterName = "@SortColumn",Value= Obj.SortColumn },
                new SqlParameter() {ParameterName = "@SortDirection",Value= Obj.SortDirection },
                new SqlParameter() {ParameterName = "@MessageStatus",Value= Obj.MessageStatus },
                new SqlParameter() {ParameterName = "@MessageFrom",Value= Obj.MessageFrom },
                 new SqlParameter() {ParameterName = "@InsuraceStatus",Value= Obj.InsuranceStatus }
            };
                //var dt = objhelper.DataTable("Usp_GetAppointmentRosources", sqlpara);
                Dt = new clsHelper().DataTable("USP_GetMessageConversationDetails", sqlpara);
                return Dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--USP_GetMessageConversationDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// This Method used for Update address from OCR side.
        /// </summary>
        /// <param name="Obj"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static int InsertUpdateAddressDetails(AddressDetails Obj, int UserId)
        {
            try
            {
                int returnAddressInfoId;
                SqlParameter[] addphone = new SqlParameter[17];
                addphone[0] = new SqlParameter("@AddressInfoID", SqlDbType.Int);
                addphone[0].Value = Obj.AddressInfoID;
                addphone[1] = new SqlParameter("@ExactAddress", SqlDbType.NVarChar);
                addphone[1].Value = Obj.ExactAddress;
                addphone[2] = new SqlParameter("@Address2", SqlDbType.NVarChar);
                addphone[2].Value = Obj.Address2;
                addphone[3] = new SqlParameter("@City", SqlDbType.NVarChar);
                addphone[3].Value = Obj.City;
                addphone[4] = new SqlParameter("@State", SqlDbType.NVarChar);
                addphone[4].Value = Obj.State;
                addphone[5] = new SqlParameter("@Country", SqlDbType.NVarChar);
                addphone[5].Value = Obj.Country;
                addphone[6] = new SqlParameter("@Zipcode", SqlDbType.NVarChar);
                addphone[6].Value = Obj.ZipCode;
                addphone[7] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[7].Value = UserId;
                addphone[8] = new SqlParameter("@ContactType", SqlDbType.Int);
                addphone[8].Value = Obj.ContactType;
                addphone[9] = new SqlParameter("@Email", SqlDbType.NVarChar);
                addphone[9].Value = Obj.EmailAddress;
                addphone[10] = new SqlParameter("@Phone", SqlDbType.NVarChar);
                addphone[10].Value = Obj.Phone;
                addphone[11] = new SqlParameter("@Fax", SqlDbType.NVarChar);
                addphone[11].Value = Obj.Fax;
                addphone[12] = new SqlParameter("@Location", SqlDbType.NVarChar);
                addphone[12].Value = Obj.Location;
                addphone[13] = new SqlParameter("@TimezoneId", SqlDbType.Int);
                addphone[13].Value = Obj.TimeZoneId;
                addphone[14] = new SqlParameter("@Mobile", SqlDbType.NVarChar);
                addphone[14].Value = Obj.Mobile;
                addphone[15] = new SqlParameter("@ExternalPMSId", SqlDbType.NVarChar);
                addphone[15].Value = Obj.ExternalPMSId;
                addphone[16] = new SqlParameter("@SchedulingLink", SqlDbType.NVarChar);
                addphone[16].Value = Obj.SchedulingLink;
                string strReturnAddressInfoId = new clsHelper().ExecuteScalar("USP_InsertUpdateAddressDetails_By_AddressInfoID", addphone);

                returnAddressInfoId = !string.IsNullOrEmpty(strReturnAddressInfoId) ? Convert.ToInt32(strReturnAddressInfoId) : 0;
                return returnAddressInfoId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--UpdateAddressDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool InsertUpdateTeamMember(InsertTeamMember Obj, int ParentUserId)
        {
            try
            {
                SqlParameter[] addphone = new SqlParameter[16];
                addphone[0] = new SqlParameter("@ParentUserId", SqlDbType.Int);
                addphone[0].Value = ParentUserId;
                addphone[1] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[1].Value = Obj.UserId;
                addphone[2] = new SqlParameter("@Email", SqlDbType.NVarChar);
                addphone[2].Value = Obj.Email;
                addphone[3] = new SqlParameter("@FirstName", SqlDbType.NVarChar);
                addphone[3].Value = Obj.FirstName;
                addphone[4] = new SqlParameter("@LastName", SqlDbType.NVarChar);
                addphone[4].Value = Obj.LastName;
                addphone[5] = new SqlParameter("@OfficeName", SqlDbType.NVarChar);
                addphone[5].Value = Obj.OfficeName;
                addphone[6] = new SqlParameter("@ExactAddress", SqlDbType.NVarChar);
                addphone[6].Value = Obj.ExactAddress;
                addphone[7] = new SqlParameter("@Address2", SqlDbType.NVarChar);
                addphone[7].Value = Obj.Address2;
                addphone[8] = new SqlParameter("@City", SqlDbType.NVarChar);
                addphone[8].Value = Obj.City;
                addphone[9] = new SqlParameter("@State", SqlDbType.NVarChar);
                addphone[9].Value = Obj.State;
                addphone[10] = new SqlParameter("@Country", SqlDbType.NVarChar);
                addphone[10].Value = Obj.Country;
                addphone[11] = new SqlParameter("@ZipCode", SqlDbType.NVarChar);
                addphone[11].Value = Obj.ZipCode;
                addphone[12] = new SqlParameter("@Specialty", SqlDbType.NVarChar);
                addphone[12].Value = Obj.Specialty;
                addphone[13] = new SqlParameter("@Password", SqlDbType.VarChar);
                addphone[13].Value = Obj.Password;
                addphone[14] = new SqlParameter("@LocationId", SqlDbType.Int);
                addphone[14].Value = Obj.LocationId;
                addphone[15] = new SqlParameter("@ProviderId", SqlDbType.NVarChar);
                addphone[15].Value = Obj.ProviderId;

                return new clsHelper().ExecuteNonQuery("USP_TeamMember_InsertAndUpdate", addphone);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--InsertUpdateTeamMember", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable GetWebsiteDetail(int Website, int UserId)
        {
            try
            {
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@WEBSITEID", SqlDbType.Int);
                addphone[0].Value = Website;
                addphone[1] = new SqlParameter("@USERID", SqlDbType.Int);
                addphone[1].Value = UserId;
                return new clsHelper().DataTable("USP_GETWEBSITEDETAILS", addphone);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--GetWebsiteDetail", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable GetIntegrationMasterDetails()
        {
            try
            {
                string qry = "SELECT * FROM IntegrationMaster where IsActive = 1";
                return new clsCommon().DataTable(qry);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--GetIntegrationMasterDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable GetTeamMemberList(FilterTeamMember _filter)
        {
            try
            {
                SqlParameter[] addphone = new SqlParameter[9];
                addphone[0] = new SqlParameter("@ParentUserId", SqlDbType.Int);
                addphone[0].Value = _filter.UserId;
                addphone[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                addphone[1].Value = _filter.PageIndex;
                addphone[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                addphone[2].Value = _filter.PageSize;
                addphone[3] = new SqlParameter("@SortColumn", SqlDbType.Int);
                addphone[3].Value = _filter.SortColumn;
                addphone[4] = new SqlParameter("@SortDirection", SqlDbType.Int);
                addphone[4].Value = _filter.SortDirection;
                addphone[5] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
                addphone[5].Value = _filter.SearchText;
                addphone[6] = new SqlParameter("@LocationId", SqlDbType.NVarChar);
                addphone[6].Value = _filter.LocationBy;
                addphone[7] = new SqlParameter("@ProviderTypefilter", SqlDbType.Int);
                addphone[7].Value = _filter.ProviderTypefilter;
                addphone[8] = new SqlParameter("@IncludeUser", SqlDbType.Bit);
                addphone[8].Value = _filter.IncludeUser;

                return new clsHelper().DataTable("USP_GetTeamMemberListOfDoctor", addphone);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--GetTeamMemberList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool DisableIntegrations(string DentrixConnectorIds, int UserId)
        {
            try
            {
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@DentrixConnectorIds", SqlDbType.NVarChar);
                addphone[0].Value = DentrixConnectorIds;
                addphone[1] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[1].Value = UserId;
                return new clsHelper().ExecuteNonQuery("Usp_DisableIntegrations", addphone);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--DisableIntegrations", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool EnableIntegrations(int Id, int UserId)
        {
            try
            {
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@IntegrationId", SqlDbType.Int);
                addphone[0].Value = Id;
                addphone[1] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[1].Value = UserId;
                return new clsHelper().ExecuteNonQuery("Usp_EnableIntegration", addphone);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--EnableIntegrations", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable IsLocationUsedinIntegration(int LocationId)
        {
            try
            {
                SqlParameter[] addphone = new SqlParameter[1];
                addphone[0] = new SqlParameter("@LocationId", SqlDbType.Int);
                addphone[0].Value = LocationId;
                return new clsHelper().DataTable("Usp_IsLocationUsedinIntegration", addphone);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--IsLocationUsedinIntegration", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Enable Integration Connector
        /// </summary>
        /// <param name="id">Connector Id</param>
        /// <param name="AccountId">Account Id</param>
        /// <param name="UserId">User Id</param>
        /// <returns></returns>
        public static bool EnableConnector(int id, int AccountId, int UserId)
        {
            try
            {
                SqlParameter[] addphone = new SqlParameter[3];
                addphone[0] = new SqlParameter("@ConnectorId", SqlDbType.Int);
                addphone[0].Value = id;
                addphone[1] = new SqlParameter("@AccountId", SqlDbType.Int);
                addphone[1].Value = AccountId;
                addphone[2] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[2].Value = UserId;
                return new clsHelper().ExecuteNonQuery("Usp_EnableConnector", addphone);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--EnableConnector", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Disable Integration Connector
        /// </summary>
        /// <param name="id">Connector Id</param>
        /// <param name="AccountId">Account Id</param>
        /// <param name="UserId">User Id</param>
        /// <returns></returns>
        public static bool DisableConnector(int id, int AccountId, int UserId)
        {
            try
            {
                SqlParameter[] addphone = new SqlParameter[3];
                addphone[0] = new SqlParameter("@ConnectorId", SqlDbType.Int);
                addphone[0].Value = id;
                addphone[1] = new SqlParameter("@AccountId", SqlDbType.Int);
                addphone[1].Value = AccountId;
                addphone[2] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[2].Value = UserId;
                return new clsHelper().ExecuteNonQuery("Usp_DisableConnector", addphone);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--DisableConnector", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool UpdateMessageStatus(int MessageId, int UserId)
        {
            try
            {
                SqlParameter[] addphone = new SqlParameter[2];
                addphone[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                addphone[0].Value = MessageId;
                addphone[1] = new SqlParameter("@UserId", SqlDbType.Int);
                addphone[1].Value = UserId;
                return new clsHelper().ExecuteNonQuery("Usp_UpdateMessageStatus", addphone);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--UpdateMessageStatus", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool RemoveProfileImage(int UserId)
        {
            try
            {
                SqlParameter[] sqlParam = new SqlParameter[1];
                sqlParam[0] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlParam[0].Value = UserId;

                return new clsHelper().ExecuteNonQuery("USP_RemoveProfileImage", sqlParam);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsColleaguesData--ReomveProfileImage", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
