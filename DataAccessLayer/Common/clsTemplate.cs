﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.Web;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.PatientsData;
using BO.Models;
using System.Linq;
using System.Collections.Generic;
using BO.ViewModel;
using BO.Enums;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using Twilio;
using Twilio.Rest.Lookups.V1;

namespace DataAccessLayer.Common
{
    public class clsTemplate
    {

        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        clsPatientsData ObjPatientsData = new clsPatientsData();
        clsCommon ObjCommon = new clsCommon();
        clsCompany ObjCompany = new clsCompany();
        clsHelper ObjHelper = new clsHelper();
        clsAdmin ObjAdmin = new clsAdmin();
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        string LoveDentistlink = ConfigurationManager.AppSettings["iLuvMyDentistLink"];
        MailMessage NewMeggage = new MailMessage();
        SmtpClient smtpClient = new SmtpClient();
        string strOwnerEmail = ConfigurationManager.AppSettings["OwnerEmail"];
        #region Template Name:Invitation to connect on Company
        public void InviteDoctor(string EmailAddress, int UserId, string PersonalNote)
        {
            try
            {

                DataSet dtInviteDoctor;
                SqlParameter[] addStudyGroupParams;
                addStudyGroupParams = new SqlParameter[1];
                addStudyGroupParams[0] = new SqlParameter("@UserId ", SqlDbType.Int);
                addStudyGroupParams[0].Value = UserId;

                dtInviteDoctor = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", addStudyGroupParams);
                string invitingDoctorFirstName = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["FirstName"]), string.Empty);
                string invitingDoctorLastName = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["LastName"]), string.Empty);

                string Email = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["Username"]), string.Empty);
                string UserName = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["FullName"]), string.Empty);
                string ImageUrl = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["ImageName"]), string.Empty);
                string Speciality = ""; string State = ""; string City = ""; string Country = "";
                if (dtInviteDoctor.Tables[7].Rows.Count > 0)
                {
                    Speciality = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[7].Rows[0]["Speciality"]), string.Empty);
                }

                if (dtInviteDoctor.Tables[2].Rows.Count > 0)
                {
                    State = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["State"]), string.Empty);
                    City = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["City"]), string.Empty);
                    Country = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["Country"]), string.Empty);
                }

                string officename = "";
                if (dtInviteDoctor.Tables[1].Rows.Count > 0)
                {
                    officename = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[1].Rows[0]["AccountName"]), string.Empty);
                }
                string publicpath = "";
                publicpath = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["PublicProfileUrl"]), string.Empty);

                string Doctorimage = ConfigurationManager.AppSettings.Get("DefaultDoctorImageForTemplate");
                string checkimage = ConfigurationManager.AppSettings.Get("DoctorImage") + ImageUrl;
                if (!string.IsNullOrEmpty(ImageUrl))
                {
                    Doctorimage = ConfigurationManager.AppSettings.Get("TemplatePeth") + ImageUrl;
                }
                else
                {
                    Doctorimage = ConfigurationManager.AppSettings.Get("DefaultDoctorImageForTemplate");
                }
                Doctorimage = Doctorimage.Replace(" ", "%20");

                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                // objCustomeMailPropery.FromMailAddress = Email;
                objCustomeMailPropery.ToMailAddress = new List<string> { EmailAddress }; ;
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "invitation-to-connect-on-company";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Doctor", UserName);
                dt.Rows.Add("UserName", UserName);
                dt.Rows.Add("Speciality", Speciality);
                dt.Rows.Add("OfficeName", officename);
                dt.Rows.Add("State", State);
                dt.Rows.Add("City", City);
                dt.Rows.Add("Email", Email);
                dt.Rows.Add("Country", Country);
                dt.Rows.Add("PersonalNote", PersonalNote);
                dt.Rows.Add("publicpath", publicpath);
                dt.Rows.Add("Image1", Doctorimage);
                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, UserId);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("invitation-to-connect-on-company", ex.Message, ex.StackTrace);

            }
        }
        #endregion

        #region Template Name: Added New Doctor as a Team Member OR Team Member Invitation From Doctor
        public void TemplateForTeamMember(int FromUserId, string ToName, string ToEmail, string Password, string EmailTemplate, bool provtype)
        {
            try
            {
                // Added New Doctor as a Team Member - This Template send when new user comes
                //Team Member Invitation From Doctor - This template send whne existing user

                DataSet dtInviteDoctor;
                SqlParameter[] addStudyGroupParams;
                addStudyGroupParams = new SqlParameter[1];
                addStudyGroupParams[0] = new SqlParameter("@UserId ", SqlDbType.Int);
                addStudyGroupParams[0].Value = FromUserId;

                dtInviteDoctor = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", addStudyGroupParams);
                string FromDoctorFirstName = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["FirstName"]), string.Empty);
                string FromDoctorLastName = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["LastName"]), string.Empty);
                string Email = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["Username"]), string.Empty);
                string officeName = "";
                if (dtInviteDoctor.Tables[1].Rows.Count > 0)
                {
                    officeName = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[1].Rows[0]["AccountName"]), string.Empty);
                }

                string UserName = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["FullName"]), string.Empty);
                string ImageUrl = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["ImageName"]), string.Empty);
                string City = ""; string Speciality = ""; string State = ""; string Country = "";
                if (dtInviteDoctor.Tables[2].Rows.Count > 0)
                {
                    City = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["City"].ToString()), string.Empty);
                    State = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["State"]), string.Empty);
                    Country = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["Country"]), string.Empty);
                }
                string publicpath = "";
                publicpath = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["PublicProfileUrl"]), string.Empty);
                if (dtInviteDoctor.Tables[7].Rows.Count > 0)
                {
                    Speciality = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[7].Rows[0]["Speciality"]), string.Empty);
                }

                string strProvType = "Team";
                if (provtype)
                {
                    strProvType = "Staff";
                }
                string strEmailBody = "";
                DataTable dt = new DataTable();
                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");
                if (EmailTemplate == "NewDoctor")
                {


                    TripleDESCryptoHelper cryptoHelper = new TripleDESCryptoHelper();
                    string encryptedPassword = cryptoHelper.encryptText(Password);



                    string Doctorimage = ConfigurationManager.AppSettings.Get("DefaultDoctorImageForTemplate");

                    string checkimage = ConfigurationManager.AppSettings.Get("DoctorImage") + ImageUrl;
                    if (!string.IsNullOrEmpty(ImageUrl))
                    {
                        Doctorimage = ConfigurationManager.AppSettings.Get("TemplatePeth") + ImageUrl;
                    }
                    else
                    {
                        Doctorimage = ConfigurationManager.AppSettings.Get("DefaultDoctorImageForTemplate");
                    }

                    Doctorimage = Doctorimage.Replace(" ", "%20");

                    //Mandrill Code
                    dt.Rows.Add("Doctor", UserName);
                    dt.Rows.Add("City", City);
                    dt.Rows.Add("ToName", ToName);
                    dt.Rows.Add("ToMail", ToEmail);
                    dt.Rows.Add("DoctorName", UserName);
                    dt.Rows.Add("Speciality", Speciality);
                    dt.Rows.Add("City", City);
                    dt.Rows.Add("State", State);
                    dt.Rows.Add("Country", Country);
                    dt.Rows.Add("DoctorOfficeName", officeName);
                    dt.Rows.Add("publicpath", publicpath);
                    dt.Rows.Add("ToUserName", ToEmail);
                    dt.Rows.Add("Password", Password);
                    dt.Rows.Add("EncryptPassword", encryptedPassword);
                    dt.Rows.Add("Image1", Doctorimage);


                }
                else if (EmailTemplate == "OldDoctor")
                {


                    TripleDESCryptoHelper cryptoHelper = new TripleDESCryptoHelper();
                    //string encryptedPassword11 = cryptoHelper.decryptText(Password);

                    string Doctorimage = ConfigurationManager.AppSettings.Get("DefaultDoctorImageForTemplate");

                    string checkimage = ConfigurationManager.AppSettings.Get("DoctorImage") + ImageUrl;
                    if (!string.IsNullOrEmpty(ImageUrl))
                    {
                        Doctorimage = ConfigurationManager.AppSettings.Get("TemplatePeth") + ImageUrl;
                    }
                    else
                    {
                        Doctorimage = ConfigurationManager.AppSettings.Get("DefaultDoctorImageForTemplate");
                    }
                    Doctorimage = Doctorimage.Replace(" ", "%20");


                    //ManDrill Code
                    dt.Rows.Add("Doctor", UserName);
                    dt.Rows.Add("City", City);
                    dt.Rows.Add("ToName", ToName);
                    dt.Rows.Add("ToMail", ToEmail);
                    dt.Rows.Add("DoctorName", UserName);
                    dt.Rows.Add("Speciality", Speciality);
                    dt.Rows.Add("City", City);
                    dt.Rows.Add("State", State);
                    dt.Rows.Add("Country", Country);
                    dt.Rows.Add("DoctorOfficeName", officeName);
                    dt.Rows.Add("publicpath", publicpath);
                    dt.Rows.Add("ToUserName", ToEmail);
                    dt.Rows.Add("Pass", Password);

                    dt.Rows.Add("Image1", Doctorimage);
                    // dt.Rows.Add("CompanyName", "Recordlinc");
                    dt.Rows.Add("Member", strProvType);
                }

                //Get Company Details, Replace Content



                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                DataTable Secdt = ObjCommon.GetDoctorSecondaryEmail(ToEmail);
                if (Secdt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(Secdt.Rows[0]["SecondaryEmail"].ToString()))
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { ToEmail, Secdt.Rows[0]["SecondaryEmail"].ToString() };
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { ToEmail };
                    }
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { ToEmail };
                }
                //objCustomeMailPropery.FromMailAddress = Email;

                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "team-member-invitation-from-doctor";


                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, FromUserId);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("team-member-invitation-from-doctor", ex.Message, ex.StackTrace);
            }

        }
        #endregion
        #region Template Name :- Sign-In Notification
        public void SigninNotification(int UserId)
        {
            try
            {
                DataSet SignIn = new DataSet();
                SqlParameter[] addpara;
                addpara = new SqlParameter[1];
                addpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                addpara[0].Value = UserId;
                string Email = string.Empty; string Phone = string.Empty; string Name = string.Empty;
                SignIn = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", addpara);
                if (SignIn.Tables[0].Rows.Count > 0 && SignIn != null)
                {
                    Email = ObjCommon.CheckNull(Convert.ToString(SignIn.Tables[0].Rows[0]["Username"]), string.Empty);
                    Name = ObjCommon.CheckNull(Convert.ToString(SignIn.Tables[0].Rows[0]["FullName"]), string.Empty);
                }
                if (SignIn.Tables[2].Rows.Count > 0 && SignIn != null)
                {
                    Phone = ObjCommon.CheckNull(Convert.ToString(SignIn.Tables[2].Rows[0]["Phone"]), string.Empty);
                    if (!string.IsNullOrEmpty(Phone))
                    {
                        Phone = "";
                        Phone = Convert.ToString(SignIn.Tables[2].Rows[0]["Phone"]);
                        Phone = Phone.Replace("(", "");
                        Phone = Phone.Replace(")", "");
                        Phone = Phone.Replace("-", "");
                        Phone = Phone.Replace(" ", "");
                        Phone = ObjCommon.PhoneFormat(Phone);
                    }
                }
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                objCustomeMailPropery.ToMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("MailNotificationEmail_To") };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("MailNotificationEmail_BCC") };
                objCustomeMailPropery.MailTemplateName = "sign-in-notification";
                DataTable dt = new DataTable();
                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Email", Email);
                dt.Rows.Add("phone", Phone);
                dt.Rows.Add("Name", Name);

                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, UserId);
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("Sign-in Notification Template Side", Ex.Message, Ex.StackTrace);
            }
        }
        #endregion
        #region Template Name:Colleague Invitation Received
        public void ColleagueInviteDoctor(int UserId, int ColleagueId, string PersonalNotes)
        {
            try
            {
                string SecondEmail = "";

                DataSet dtInviteDoctor;
                SqlParameter[] addStudyGroupParams;
                addStudyGroupParams = new SqlParameter[1];
                addStudyGroupParams[0] = new SqlParameter("@UserId ", SqlDbType.Int);
                addStudyGroupParams[0].Value = UserId;

                string Email = string.Empty; string Password = ""; string PublicPath = ""; string invitingDoctorFirstName = string.Empty;
                string invitingDoctorLastName = string.Empty; string UserName = string.Empty; string ImageUrl = string.Empty;
                dtInviteDoctor = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", addStudyGroupParams);
                if (dtInviteDoctor.Tables[0].Rows.Count > 0)
                {

                    Email = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["Username"]), string.Empty);
                    Password = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["Password"]), string.Empty);

                    PublicPath = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["PublicProfileUrl"]), string.Empty);
                    invitingDoctorFirstName = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["FirstName"]), string.Empty);
                    invitingDoctorLastName = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["LastName"]), string.Empty);
                    UserName = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["FullName"]), string.Empty);
                    ImageUrl = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["ImageName"]), string.Empty);


                }


                string officeName = "";
                if (dtInviteDoctor.Tables[1].Rows.Count > 0)
                {
                    officeName = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[1].Rows[0]["AccountName"]), string.Empty);
                }


                string City = ""; string Country = ""; string State = ""; string Speciality = ""; string Phone = "";
                if (dtInviteDoctor.Tables[7].Rows.Count > 0)
                {
                    Speciality = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[7].Rows[0]["Speciality"]), string.Empty);
                }

                if (dtInviteDoctor.Tables[2].Rows.Count > 0)
                {
                    City = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["City"]), string.Empty);
                    State = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["State"]), string.Empty);
                    Country = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["Country"]), string.Empty);
                    Phone = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["Phone"]), string.Empty);
                    if (!string.IsNullOrEmpty(Phone))
                    {
                        Phone = "";
                        Phone = Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["Phone"]);
                        Phone = Phone.Replace("(", "");
                        Phone = Phone.Replace(")", "");
                        Phone = Phone.Replace("-", "");
                        Phone = Phone.Replace(" ", "");
                        Phone = ObjCommon.PhoneFormat(Phone);
                    }
                }




                string Notes = "";
                if (!string.IsNullOrEmpty(PersonalNotes))
                {
                    Notes = "Personal Note: " + PersonalNotes.Replace("\n","<br/>");
                }
                else
                {
                    Notes = "Personal Note: I'd like to add you to my professional network on RecordLinc.";
                }

                // Get Colleague details by id
                DataSet dscolleague = new DataSet();
                SqlParameter[] addcolleague;
                addcolleague = new SqlParameter[1];
                addcolleague[0] = new SqlParameter("@UserId ", SqlDbType.Int);
                addcolleague[0].Value = ColleagueId;
                dscolleague = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", addcolleague);

                string ColleagueFullName = ObjCommon.CheckNull(Convert.ToString(dscolleague.Tables[0].Rows[0]["FullName"]), string.Empty);
                string inviteDoctorEmail = ObjCommon.CheckNull(Convert.ToString(dscolleague.Tables[0].Rows[0]["Username"]), string.Empty);
                Password = ObjCommon.CheckNull(Convert.ToString(dscolleague.Tables[0].Rows[0]["Password"]), string.Empty);


                string Doctorimage = ConfigurationManager.AppSettings.Get("TemplatedafaultImage");

                string checkimage = ConfigurationManager.AppSettings.Get("DoctorImage") + ImageUrl;
                if (!string.IsNullOrEmpty(ImageUrl))
                {
                    Doctorimage = ConfigurationManager.AppSettings.Get("TemplatePeth") + ImageUrl;
                }
                else
                {
                    Doctorimage = ConfigurationManager.AppSettings.Get("TemplatedafaultImage");
                }
                Doctorimage = Doctorimage.Replace(" ", "%20");

                //ManDrill Code

                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                DataTable SecDt = ObjCommon.GetDoctorSecondaryEmail(inviteDoctorEmail);
                if (SecDt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(Convert.ToString(SecDt.Rows[0]["SecondaryEmail"])))
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { inviteDoctorEmail, Convert.ToString(SecDt.Rows[0]["SecondaryEmail"]) };
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { inviteDoctorEmail };
                    }
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { inviteDoctorEmail };
                }


                //objCustomeMailPropery.FromMailAddress = Email;
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "colleague-invitation-received";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Doctor", UserName);
                dt.Rows.Add("City", City);
                dt.Rows.Add("ColleagueFullName", ColleagueFullName);
                dt.Rows.Add("Speciality", Speciality);
                dt.Rows.Add("State", State);
                dt.Rows.Add("City", City);
                dt.Rows.Add("Country", Country);
                dt.Rows.Add("invitationnote", Notes);
                dt.Rows.Add("UserName", UserName);
                dt.Rows.Add("OfficeName", officeName);
                dt.Rows.Add("ToMail", inviteDoctorEmail);
                dt.Rows.Add("Pass", Password);
                dt.Rows.Add("publicpath", PublicPath);
                dt.Rows.Add("phone", Phone);
                dt.Rows.Add("Image1", Doctorimage);

                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, UserId);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("colleague-invitation-received", ex.Message, ex.StackTrace);
            }

        }
        #endregion

        #region Template Name:Colleague Invite staff member
        public void  StaffMemberInviteDoctor(int UserId, List<string> StaffMemberEmails)
        {
            try
            {
             
                DataSet dtInviteDoctor;
                SqlParameter[] addStudyGroupParams;
                addStudyGroupParams = new SqlParameter[1];
                addStudyGroupParams[0] = new SqlParameter("@UserId ", SqlDbType.Int);
                addStudyGroupParams[0].Value = UserId;

                string Email = string.Empty; 
                string UserName = string.Empty; 
                dtInviteDoctor = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", addStudyGroupParams);
                if (dtInviteDoctor.Tables[0].Rows.Count > 0)
                {
                    UserName = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["FullName"]), string.Empty);
                }

                string strEmail = string.Empty;
                foreach (var item in StaffMemberEmails)
                {
                    if (!string.IsNullOrEmpty(strEmail))
                    {
                        strEmail += "<tr><td>" + item + "</td></tr>";
                    }
                    else
                    {
                        strEmail = "<tr><td>" + item + "</td></tr>";
                    }
                }

               
                //ManDrill Code

                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();               
                objCustomeMailPropery.ToMailAddress = new List<string> { strOwnerEmail };            
                
                objCustomeMailPropery.MailTemplateName = "doctor-invitation-staffmember";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("DOCTOR", UserName);
                dt.Rows.Add("EMAILS", strEmail);             

                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, UserId);

            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion



        #region Template Name:Suggestion (Suggest a Change)
        public bool SuggestChange(int UserId, int ColleagueId, string Subject, string Body)
        {

            bool Status = false;
            try
            {


                DataSet dtTransferOwner = new DataSet();
                SqlParameter[] addTransferOwnerParams;
                addTransferOwnerParams = new SqlParameter[1];
                addTransferOwnerParams[0] = new SqlParameter("@UserId ", SqlDbType.Int);
                addTransferOwnerParams[0].Value = UserId;
                dtTransferOwner = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", addTransferOwnerParams);

                string invitingDoctorFirstName = ObjCommon.CheckNull(Convert.ToString(dtTransferOwner.Tables[0].Rows[0]["FirstName"]), string.Empty);
                string invitingDoctorLastName = ObjCommon.CheckNull(Convert.ToString(dtTransferOwner.Tables[0].Rows[0]["LastName"]), string.Empty);
                string OwnerFullName = ObjCommon.CheckNull(Convert.ToString(dtTransferOwner.Tables[0].Rows[0]["FullName"]), string.Empty);
                string OwnerImageUrl = ObjCommon.CheckNull(Convert.ToString(dtTransferOwner.Tables[0].Rows[0]["ImageName"]), string.Empty);
                string Email = ObjCommon.CheckNull(Convert.ToString(dtTransferOwner.Tables[0].Rows[0]["Username"]), string.Empty);
                string publibpath = "";
                publibpath = ObjCommon.CheckNull(Convert.ToString(dtTransferOwner.Tables[0].Rows[0]["PublicProfileUrl"]), string.Empty);

                string Password = "";
                Password = ObjCommon.CheckNull(Convert.ToString(dtTransferOwner.Tables[0].Rows[0]["Password"]), string.Empty);
                string OwnerSpeciality = "";
                if (dtTransferOwner.Tables[7].Rows.Count > 0)
                {
                    OwnerSpeciality = ObjCommon.CheckNull(Convert.ToString(dtTransferOwner.Tables[7].Rows[0]["Speciality"]), string.Empty);
                }


                string OwnerOfficeName = "";
                if (dtTransferOwner.Tables[1].Rows.Count > 0)
                {
                    OwnerOfficeName = ObjCommon.CheckNull(Convert.ToString(dtTransferOwner.Tables[1].Rows[0]["AccountName"]), string.Empty);
                }

                string OwnerCity = "";
                string OwnerCountry = "";
                string OwnerState = "";
                if (dtTransferOwner.Tables[2].Rows.Count > 0)
                {
                    OwnerCity = ObjCommon.CheckNull(Convert.ToString(dtTransferOwner.Tables[2].Rows[0]["City"]), string.Empty);
                    OwnerCountry = ObjCommon.CheckNull(Convert.ToString(dtTransferOwner.Tables[2].Rows[0]["Country"]), string.Empty);
                    OwnerState = ObjCommon.CheckNull(Convert.ToString(dtTransferOwner.Tables[2].Rows[0]["State"]), string.Empty);
                }


                DataSet dtTransferDoctor = new DataSet();
                SqlParameter[] addTransferDoctorParams;
                addTransferDoctorParams = new SqlParameter[1];
                addTransferDoctorParams[0] = new SqlParameter("@UserId ", SqlDbType.Int);
                addTransferDoctorParams[0].Value = ColleagueId;
                dtTransferDoctor = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", addTransferDoctorParams);

                string DoctorFullName = ObjCommon.CheckNull(Convert.ToString(dtTransferDoctor.Tables[0].Rows[0]["FullName"]), string.Empty);
                string DoctorImageUrl = ObjCommon.CheckNull(Convert.ToString(dtTransferDoctor.Tables[0].Rows[0]["ImageName"]), string.Empty);
                string DoctorofficeName = "";
                if (dtTransferDoctor.Tables[1].Rows.Count > 0)
                {
                    DoctorofficeName = ObjCommon.CheckNull(Convert.ToString(dtTransferDoctor.Tables[1].Rows[0]["AccountName"]), string.Empty);
                }

                string DoctorSpeciality = "";
                if (dtTransferDoctor.Tables[7].Rows.Count > 0)
                {
                    DoctorSpeciality = ObjCommon.CheckNull(Convert.ToString(dtTransferDoctor.Tables[7].Rows[0]["Speciality"]), string.Empty);
                }


                string OwnerEmailAddress = ObjCommon.CheckNull(Convert.ToString(dtTransferDoctor.Tables[0].Rows[0]["Username"]), string.Empty);
                string Publicpath1 = ObjCommon.CheckNull(Convert.ToString(dtTransferDoctor.Tables[0].Rows[0]["PublicProfileUrl"]), string.Empty);

                string DoctorCity = "";
                string DoctorState = "";
                string DoctorCountry = "";
                if (dtTransferDoctor.Tables[2].Rows.Count > 0)
                {
                    DoctorState = ObjCommon.CheckNull(Convert.ToString(dtTransferDoctor.Tables[2].Rows[0]["State"]), string.Empty);
                    DoctorCity = ObjCommon.CheckNull(Convert.ToString(dtTransferOwner.Tables[2].Rows[0]["City"]), string.Empty);
                    DoctorCountry = ObjCommon.CheckNull(Convert.ToString(dtTransferDoctor.Tables[2].Rows[0]["Country"]), string.Empty);
                }


                string DoctorImage = ConfigurationManager.AppSettings.Get("DefaultDoctorImageForTemplate");
                string OwnerImage = ConfigurationManager.AppSettings.Get("DefaultDoctorImageForTemplate");


                if (!string.IsNullOrEmpty(OwnerImageUrl))
                {
                    OwnerImage = ConfigurationManager.AppSettings.Get("TemplatePeth") + OwnerImageUrl;
                }
                else
                {
                    OwnerImage = ConfigurationManager.AppSettings.Get("TemplatedafaultImage").ToString();
                }



                if (!string.IsNullOrEmpty(DoctorImageUrl))
                {
                    DoctorImage = ConfigurationManager.AppSettings.Get("TemplatePeth") + DoctorImageUrl;
                }
                else
                {
                    DoctorImage = ConfigurationManager.AppSettings.Get("TemplatedafaultImage").ToString();
                }

                OwnerImage = OwnerImage.Replace(" ", "%20");

                DoctorImage = DoctorImage.Replace(" ", "%20");


                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                DataTable SecDt = ObjCommon.GetDoctorSecondaryEmail(OwnerEmailAddress);
                if (SecDt.Rows != null && SecDt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(SecDt.Rows[0]["SecondaryEmail"].ToString()))
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { OwnerEmailAddress, SecDt.Rows[0]["SecondaryEmail"].ToString() };
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { OwnerEmailAddress };
                    }
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { OwnerEmailAddress };
                }
                //objCustomeMailPropery.FromMailAddress = Email;
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "suggestion";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("OwnerFullName", OwnerFullName);
                dt.Rows.Add("OwnerOfficeName", OwnerOfficeName);
                dt.Rows.Add("OwnerSpeciality", OwnerSpeciality);
                dt.Rows.Add("OwnerState", OwnerState);
                dt.Rows.Add("OwnerCity", OwnerCity);
                dt.Rows.Add("OwnerCountry", OwnerCountry);
                dt.Rows.Add("DoctorFullName", DoctorFullName);
                dt.Rows.Add("DoctorSpeciality", DoctorSpeciality);
                dt.Rows.Add("DoctorCity", DoctorCity);
                dt.Rows.Add("DoctorState", DoctorState);
                dt.Rows.Add("DoctorCountry", DoctorCountry);
                dt.Rows.Add("DoctorOfficeName", DoctorofficeName);
                dt.Rows.Add("ToMail", OwnerEmailAddress);
                dt.Rows.Add("Pass", Password);
                dt.Rows.Add("publicpath", publibpath);
                dt.Rows.Add("publicpath1", Publicpath1);
                dt.Rows.Add("OwnerImage", OwnerImage);
                dt.Rows.Add("DoctorImage", DoctorImage);
                dt.Rows.Add("SuggestDetails", Body);
                dt.Rows.Add("SuggestDetailsasas", Body);
                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, UserId);

            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("suggestion", ex.Message, ex.StackTrace);
            }

            Status = true;
            return Status;
        }
        #endregion

        #region Template Name: Message Received Notification For Patient
        public void SendMessageToPatient(int DoctorId, int PatientId, string content, int MessageId)
        {
            try
            {
                DataSet dtToAddress;
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = DoctorId;
                dtToAddress = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", strParameter);

                if (dtToAddress != null && dtToAddress.Tables.Count > 0 && dtToAddress.Tables[0].Rows[0]["Username"] != null)
                {
                    TripleDESCryptoHelper cryptoHelper123 = new TripleDESCryptoHelper();

                    string Email = ObjCommon.CheckNull(Convert.ToString(dtToAddress.Tables[0].Rows[0]["Username"]), string.Empty);
                    string FromField = ObjCommon.CheckNull(Convert.ToString(dtToAddress.Tables[0].Rows[0]["FullName"]), string.Empty);

                    string Publicpath = ObjCommon.CheckNull(Convert.ToString(dtToAddress.Tables[0].Rows[0]["PublicProfileUrl"]), string.Empty);

                    string FromSpeciality = string.Empty;
                    if (dtToAddress.Tables[7].Rows.Count > 0)
                    {
                        FromSpeciality = ObjCommon.CheckNull(Convert.ToString(dtToAddress.Tables[7].Rows[0]["Speciality"]), string.Empty);
                        if (!string.IsNullOrWhiteSpace(FromSpeciality))
                        {
                            FromSpeciality = "(" + FromSpeciality + ")";
                        }
                        else
                        {
                            FromSpeciality = string.Empty;
                        }
                    }

                    string City = string.Empty; string State = string.Empty;
                    if (dtToAddress.Tables[2].Rows.Count > 0)
                    {
                        City = ObjCommon.CheckNull(Convert.ToString(dtToAddress.Tables[2].Rows[0]["City"]), string.Empty);
                        State = ObjCommon.CheckNull(Convert.ToString(dtToAddress.Tables[2].Rows[0]["State"]), string.Empty);
                    }

                    string Address = string.Empty;
                    if (!string.IsNullOrEmpty(City))
                    {
                        Address = City;
                    }
                    if (!string.IsNullOrEmpty(State))
                    {
                        if (!string.IsNullOrEmpty(Address))
                        {
                            Address = Address + "," + State;
                        }
                        else
                        {
                            Address = State;
                        }
                    }

                    if (!string.IsNullOrEmpty(Address))
                    {
                        Address = "from " + Address + " has";
                    }

                    string officename = string.Empty;
                    if (dtToAddress.Tables[1].Rows.Count > 0)
                    {
                        officename = ObjCommon.CheckNull(Convert.ToString(dtToAddress.Tables[1].Rows[0]["AccountName"]), string.Empty);
                    }



                    DataSet dsPatient;
                    SqlParameter[] addStudyGroupParams;
                    addStudyGroupParams = new SqlParameter[2];
                    addStudyGroupParams[0] = new SqlParameter("@PatientId ", SqlDbType.Int);
                    addStudyGroupParams[0].Value = PatientId;
                    //Passing doctor id to Patient Hisotry SP
                    addStudyGroupParams[1] = new SqlParameter("@Doctorid ", SqlDbType.Int);
                    addStudyGroupParams[1].Value = DoctorId;

                    dsPatient = ObjHelper.GetDatasetData("USP_GetPateintDetailsByPatientId", addStudyGroupParams);

                    string ToAddress = ObjCommon.CheckNull(Convert.ToString(dsPatient.Tables[0].Rows[0]["Email"]), string.Empty);
                    string encryptedPassword = cryptoHelper123.encryptText(Convert.ToString(dsPatient.Tables[0].Rows[0]["ExactPassword"]));
                    string ToField = ObjCommon.CheckNull(Convert.ToString(dsPatient.Tables[0].Rows[0]["FullName"]), string.Empty);


                    string joinstring = ToAddress + "|" + encryptedPassword + "|" + MessageId;
                    string ecriptedstring = cryptoHelper123.encryptText(joinstring);


                    CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                    DataTable SecDt = ObjCommon.GetPatientSecondaryEmail(ToAddress);
                    if (SecDt.Rows != null && SecDt.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(SecDt.Rows[0]["SecondaryEmail"].ToString()))
                        {
                            objCustomeMailPropery.ToMailAddress = new List<string> { ToAddress, SecDt.Rows[0]["SecondaryEmail"].ToString() };
                        }
                        else
                        {
                            objCustomeMailPropery.ToMailAddress = new List<string> { ToAddress };
                        }
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { ToAddress };

                    }
                    //objCustomeMailPropery.FromMailAddress = Email;

                    objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                    objCustomeMailPropery.MailTemplateName = "message-received-notification-for-patient";

                    DataTable dt = new DataTable();

                    dt.Columns.Add("FieldName");
                    dt.Columns.Add("FieldValue");

                    dt.Rows.Add("Doctor", FromField);
                    dt.Rows.Add("ToField", ToField);
                    dt.Rows.Add("FromField", FromField);
                    dt.Rows.Add("encriptedstring", ecriptedstring);
                    dt.Rows.Add("FromSpeciality", FromSpeciality);
                    dt.Rows.Add("Address", Address);
                    dt.Rows.Add("State", State);
                    dt.Rows.Add("City", City);
                    dt.Rows.Add("OfficeName", officename);
                    dt.Rows.Add("publicpath", Publicpath);
                    dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                    dt = ObjCompany.GetEmailBodyForTemplate(dt);
                    objCustomeMailPropery.MergeTags = dt;

                    SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, DoctorId);

                }
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("message-received-notification-for-patient", ex.Message, ex.StackTrace);
            }
        }
        #endregion
        #region Perio registration Details
        public void SendRegistrationDetailstioTraivs(string firstname, string lastname, string email, string Phone)
        {

            try
            {
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                objCustomeMailPropery.ToMailAddress = new List<string> { "travis@recordlinc.com" };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "perio-registration-userdetails";
                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("CompanyName", "Recordlinc");
                dt.Rows.Add("FirstName", firstname);
                dt.Rows.Add("LastName", lastname);
                dt.Rows.Add("ContactNumber", Phone);
                dt.Rows.Add("Email", email);



                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception EX)
            {
                ObjCommon.InsertErrorLog("perio-registration-userdetails", EX.Message, EX.StackTrace);
                throw;
            }

        }

        #endregion
        #region Template Name:Patient Referral Received
        public void SendPatientReferral(int Id, string ToAddress, string TreatingDoctorName, string Comments, int ReferralId, int MessageId, int Messagetypeid)
        {
            try
            {
                TripleDESCryptoHelper cryptoHelper123 = new TripleDESCryptoHelper();
                DataSet dtInviteDoctor = new DataSet();
                SqlParameter[] addStudyGroupParams;
                addStudyGroupParams = new SqlParameter[1];
                addStudyGroupParams[0] = new SqlParameter("@UserId ", SqlDbType.Int);
                addStudyGroupParams[0].Value = Id;

                dtInviteDoctor = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", addStudyGroupParams);

                string UserName = string.Empty; string ImageUrl = string.Empty; string Speciality = string.Empty; string State = string.Empty;
                string City = string.Empty; string Country = string.Empty; string Email = string.Empty; string officeName = string.Empty;
                string ReceiverPassword = string.Empty; string PublicPath = string.Empty; string newcomment = string.Empty; string Street = string.Empty;
                string ZipCode = string.Empty; string phone = string.Empty; string Fax = string.Empty; string AddressDetail = string.Empty;

                if (dtInviteDoctor != null && dtInviteDoctor.Tables.Count > 0)
                {
                    UserName = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["FullName"]), string.Empty);
                    PublicPath = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["PublicProfileUrl"]), string.Empty);
                    ImageUrl = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["ImageName"]), string.Empty);
                    Email = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[0].Rows[0]["Username"]), string.Empty);

                    if (dtInviteDoctor.Tables[1].Rows.Count > 0)
                    {
                        officeName = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[1].Rows[0]["AccountName"]), string.Empty);
                    }
                    if (dtInviteDoctor.Tables[2].Rows.Count > 0)
                    {
                        Street = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["ExactAddress"]), string.Empty);
                        State = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["State"]), string.Empty);
                        City = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["City"]), string.Empty);
                        Country = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["Country"]), string.Empty);
                        phone = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["Phone"]), string.Empty);
                        Fax = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["Fax"]), string.Empty);
                        ZipCode = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["ZipCode"]), string.Empty);

                        if (!string.IsNullOrEmpty(AddressDetail))
                        {
                            if (!string.IsNullOrEmpty(Street) && !string.IsNullOrWhiteSpace(Street))
                                AddressDetail = AddressDetail + ", " + Street;
                        }
                        else
                        {
                            AddressDetail = Street;
                        }
                        if (!string.IsNullOrEmpty(AddressDetail))
                        {
                            if (!string.IsNullOrEmpty(State) && !string.IsNullOrWhiteSpace(State))
                                AddressDetail = AddressDetail + ", " + State;
                        }
                        else
                        {
                            AddressDetail = State;
                        }
                        if (!string.IsNullOrEmpty(AddressDetail))
                        {
                            if (!string.IsNullOrEmpty(City) && !string.IsNullOrWhiteSpace(City))
                                AddressDetail = AddressDetail + ", " + City;
                        }
                        else
                        {
                            AddressDetail = City;
                        }
                        if (!string.IsNullOrEmpty(AddressDetail))
                        {
                            if (!string.IsNullOrEmpty(Country) && !string.IsNullOrWhiteSpace(Country))
                                AddressDetail = AddressDetail + ", " + Country;
                        }
                        else
                        {
                            AddressDetail = Country;
                        }

                        //if (!string.IsNullOrEmpty(AddressDetail))
                        //{
                        //    if (!string.IsNullOrEmpty(phone) && !string.IsNullOrWhiteSpace(phone))
                        //        AddressDetail = AddressDetail + "," + phone;
                        //}
                        //else
                        //{
                        //    AddressDetail = phone;
                        //}

                        //if (!string.IsNullOrEmpty(AddressDetail))
                        //{
                        //    if (!string.IsNullOrEmpty(Fax) && !string.IsNullOrWhiteSpace(Fax))
                        //        AddressDetail = AddressDetail + "," + Fax;
                        //}
                        //else
                        //{
                        //    AddressDetail = Fax;
                        //}

                        if (!string.IsNullOrEmpty(AddressDetail))
                        {
                            if (!string.IsNullOrEmpty(ZipCode) && !string.IsNullOrWhiteSpace(ZipCode))
                                AddressDetail = AddressDetail + ", " + ZipCode;
                        }
                        else
                        {
                            AddressDetail = ZipCode;
                        }
                        if (!string.IsNullOrEmpty(Fax) && !string.IsNullOrWhiteSpace(Fax))
                        {
                            Fax = "Fax : " + Fax;
                        }
                        if (!string.IsNullOrEmpty(phone) && !string.IsNullOrWhiteSpace(phone))
                        {
                            phone = "Phone : <a href ='tel:" + phone + "'> " + phone + "</a>";
                        }
                    }

                    if (dtInviteDoctor.Tables[7].Rows.Count > 0)
                    {
                        Speciality = ObjCommon.CheckNull(Convert.ToString(dtInviteDoctor.Tables[7].Rows[0]["Speciality"]), string.Empty);
                    }
                }

                if (!string.IsNullOrEmpty(ToAddress))
                {
                    DataTable dt = new DataTable();
                    dt = ObjColleaguesData.CheckEmailInSystem(ToAddress);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        ReceiverPassword = cryptoHelper123.encryptText(ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["Password"]), string.Empty));
                    }
                }

                newcomment = Comments;
                if (!string.IsNullOrEmpty(Comments))
                {
                    newcomment = "<font color=#555555><strong>Comments :</strong>" + " " + Comments + "</font>";
                }
                else
                {
                    newcomment = "";
                }





                string EncriptString = string.Empty; string StringToEncript = string.Empty;
                StringToEncript = ToAddress + "||" + ReceiverPassword + "||" + MessageId.ToString() + "||" + Messagetypeid.ToString();
                EncriptString = cryptoHelper123.encryptText(StringToEncript);
                //  EncriptString = HttpContext.Current.Session["CompanyWebsite"] + "?" + EncriptString;


                string Doctorimage = ConfigurationManager.AppSettings.Get("DefaultDoctorImageForTemplate");

                string checkimage = ConfigurationManager.AppSettings.Get("DoctorImage") + ImageUrl;
                if (!string.IsNullOrEmpty(ImageUrl))
                {
                    Doctorimage = ConfigurationManager.AppSettings.Get("TemplatePeth") + ImageUrl;
                }
                else
                {
                    Doctorimage = ConfigurationManager.AppSettings.Get("DefaultDoctorImageForTemplate");
                }
                Doctorimage = Doctorimage.Replace(" ", "%20");


                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                DataTable SecDt = ObjCommon.GetDoctorSecondaryEmail(ToAddress);
                if (SecDt.Rows != null && SecDt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(SecDt.Rows[0]["SecondaryEmail"].ToString()))
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { ToAddress, SecDt.Rows[0]["SecondaryEmail"].ToString() };
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { ToAddress };
                    }
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { ToAddress };
                }
                //objCustomeMailPropery.FromMailAddress = Email;


                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "patient-referral-received";

                DataTable dt1 = new DataTable();

                dt1.Columns.Add("FieldName");
                dt1.Columns.Add("FieldValue");

                dt1.Rows.Add("Doctor", UserName);
                dt1.Rows.Add("TreatingDoctorName", TreatingDoctorName);
                dt1.Rows.Add("UserName", UserName);
                dt1.Rows.Add("Speciality", Speciality);
                dt1.Rows.Add("State", State);
                dt1.Rows.Add("City", City);
                //dt1.Rows.Add("Street", Street);
                dt1.Rows.Add("Phone", phone);
                //dt1.Rows.Add("ZipCode", ZipCode);
                dt1.Rows.Add("Fax", Fax);
                //dt1.Rows.Add("Country", Country);
                dt1.Rows.Add("AddressDetail", AddressDetail);
                dt1.Rows.Add("Comments", newcomment);
                dt1.Rows.Add("OfficeName", officeName);
                dt1.Rows.Add("CompanyWebsite", HttpContext.Current.Session["CompanyWebsite"]);
                // dt1.Rows.Add("ToMail", ToAddress);
                // dt1.Rows.Add("Pass", ReceiverPassword);

                dt1.Rows.Add("encriptedstring", EncriptString);
                dt1.Rows.Add("publicpath", PublicPath);
                dt1.Rows.Add("ReferralID", Convert.ToString(ReferralId));
                dt1.Rows.Add("Image1", Doctorimage);

                dt1 = ObjCompany.GetEmailSubjectForTemplate(dt1);
                dt1 = ObjCompany.GetEmailBodyForTemplate(dt1);
                objCustomeMailPropery.MergeTags = dt1;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, Id);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("patient-referral-received", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Template Name:Message Received Notification
        public void SendMessageToColleague(int UserId, int ColleagueId, int MessageId, int MessageTypeId)
        {
            try
            {

                string Password = string.Empty;
                string FromUserName = string.Empty;
                string FromImageUrl = string.Empty;
                string FromSpeciality = string.Empty;
                string FromState = string.Empty;
                string FromCity = string.Empty;
                string FromCountry = string.Empty;
                string Email = string.Empty;
                string Publicpath = string.Empty;
                string officename = string.Empty; string ToField = string.Empty;
                string ToEmailAddress = string.Empty; string FromField = string.Empty;

                TripleDESCryptoHelper cryptoHelper123 = new TripleDESCryptoHelper();
                DataSet dtFromDoctorDetails;
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                dtFromDoctorDetails = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", strParameter);

                if (dtFromDoctorDetails != null && dtFromDoctorDetails.Tables.Count > 0 && dtFromDoctorDetails.Tables[0].Rows[0]["Username"] != null)
                {
                    FromField = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[0].Rows[0]["FullName"]), string.Empty);
                    Publicpath = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[0].Rows[0]["PublicProfileUrl"]), string.Empty);
                    Email = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[0].Rows[0]["Username"]), string.Empty);


                    if (dtFromDoctorDetails.Tables[2].Rows.Count > 0)
                    {
                        FromState = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[2].Rows[0]["State"]), string.Empty);
                        FromCity = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[2].Rows[0]["City"]), string.Empty);
                        FromCountry = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[2].Rows[0]["Country"]), string.Empty);
                    }

                    if (dtFromDoctorDetails.Tables[7].Rows.Count > 0)
                    {
                        FromSpeciality = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[7].Rows[0]["Speciality"]), string.Empty);
                        if (!string.IsNullOrWhiteSpace(FromSpeciality))
                        {
                            FromSpeciality = "(" + FromSpeciality + ")";
                        }
                        else
                        {
                            FromSpeciality = string.Empty;
                        }
                    }
                }



                DataSet dtToDoctorDetails = new DataSet();
                SqlParameter[] strParameterTo = new SqlParameter[1];
                strParameterTo[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameterTo[0].Value = ColleagueId;
                dtToDoctorDetails = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", strParameterTo);

                if (dtToDoctorDetails != null && dtToDoctorDetails.Tables.Count > 0 && dtToDoctorDetails.Tables[0].Rows[0]["Username"] != null)
                {
                    ToField = ObjCommon.CheckNull(Convert.ToString(dtToDoctorDetails.Tables[0].Rows[0]["FullName"]), string.Empty);
                    Password = ObjCommon.CheckNull(Convert.ToString(dtToDoctorDetails.Tables[0].Rows[0]["Password"]), string.Empty);
                    ToEmailAddress = ObjCommon.CheckNull(Convert.ToString(dtToDoctorDetails.Tables[0].Rows[0]["Username"]), string.Empty);

                }

                //Encripted string For message show from Email
                string EncriptString = string.Empty; string StringToEncript = string.Empty;
                StringToEncript = ToEmailAddress + "||" + Password + "||" + MessageId.ToString() + "||" + MessageTypeId.ToString();
                EncriptString = cryptoHelper123.encryptText(StringToEncript);


                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                DataTable Secdt = ObjCommon.GetDoctorSecondaryEmail(ToEmailAddress);
                if (Secdt.Rows != null && Secdt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(Secdt.Rows[0]["SecondaryEmail"].ToString()))
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { ToEmailAddress, Secdt.Rows[0]["SecondaryEmail"].ToString() };
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { ToEmailAddress };
                    }
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { ToEmailAddress };
                }
                //objCustomeMailPropery.FromMailAddress = Email;


                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "message-received-notification";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Doctor", FromField);
                dt.Rows.Add("ToField", ToField);
                dt.Rows.Add("FromField", FromField);
                dt.Rows.Add("encriptedstring", EncriptString);
                dt.Rows.Add("FromSpeciality", FromSpeciality);
                dt.Rows.Add("City", FromCity);
                dt.Rows.Add("State", FromState);
                dt.Rows.Add("OfficeName", officename);
                dt.Rows.Add("publicpath", Publicpath);
                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, UserId);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("message-received-notification", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Template Name:Patient Forgot Password
        public void SendPatientPassword(string PatientEmail, string Password, string PatientFirstName, string PatientLastName)
        {
            try
            {
                TripleDESCryptoHelper cryptoHelper123 = new TripleDESCryptoHelper();
                string encryptedPassword = cryptoHelper123.encryptText(Password);


                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();

                DataTable SecDt = ObjCommon.GetPatientSecondaryEmail(PatientEmail);
                if (SecDt.Rows != null && SecDt.Rows.Count > 0)
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { PatientEmail, SecDt.Rows[0]["SecondaryEmail"].ToString() };
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { PatientEmail };

                }
                //objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");

                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "patient-forgot-password";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Name", PatientFirstName + ' ' + PatientLastName);
                dt.Rows.Add("PatientEmail", PatientEmail);
                dt.Rows.Add("PatientPassword", Password);
                dt.Rows.Add("EncPassword", encryptedPassword);


                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("patient-forgot-password", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Template Name: Patient Reset Password

        public void ResetPatientPassword(string PatientEmail, string PatientFirstName, string PatientLastName)
        {
            try
            {
                ObjCommon = new clsCommon();
                TripleDESCryptoHelper cryptoHelper123 = new TripleDESCryptoHelper();
                string encryptedEmail = cryptoHelper123.encryptText(PatientEmail);

                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                //DataTable SecDt = ObjCommon.GetPatientSecondaryEmail(PatientEmail);
                //if (SecDt.Rows != null && SecDt.Rows.Count > 0)
                //{
                //    objCustomeMailPropery.ToMailAddress = new List<string> { PatientEmail, SecDt.Rows[0]["SecondaryEmail"].ToString() };
                //}
                //else
                //{
                //    objCustomeMailPropery.ToMailAddress = new List<string> { PatientEmail };
                //}
                //objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");
                objCustomeMailPropery.ToMailAddress = new List<string> { PatientEmail };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "patient-reset-password";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Name", PatientFirstName + ' ' + PatientLastName);
                dt.Rows.Add("ToMail", encryptedEmail);


                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("patient-reset-password", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Template Name: Patient Added Notification 
        public void PatientAddedNotification(int DoctorId, int PatientId, string PatientEmail, string PatientFirstName, string PatientLastName, string Phone, string Address, string Address2, string City, string State, string Country, string ZipCode)
        {
            try
            {

                //Changes Made to send Patient Id into Mail 
                string DoctorFullName = string.Empty;

                string DoctorSpeciality = string.Empty;
                string DoctorState = string.Empty;
                string publicpath = string.Empty;

                string DoctorCity = string.Empty;

                string DoctorCountry = string.Empty;
                string DoctorEmail = string.Empty;
                string Password = string.Empty;
                string encryptedPassword = string.Empty;
                string encryptedPatientId = string.Empty;

                DataSet dtFromDoctorDetails;
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = DoctorId;
                dtFromDoctorDetails = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", strParameter);

                if (dtFromDoctorDetails != null && dtFromDoctorDetails.Tables.Count > 0 && dtFromDoctorDetails.Tables[0].Rows[0]["Username"] != null)
                {
                    DoctorFullName = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[0].Rows[0]["FullName"]), string.Empty);
                    publicpath = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[0].Rows[0]["PublicProfileUrl"]), string.Empty);
                    DoctorEmail = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[0].Rows[0]["Username"]), string.Empty);
                    Password = Convert.ToString(ObjTripleDESCryptoHelper.decryptText(dtFromDoctorDetails.Tables[0].Rows[0]["Password"].ToString()));
                    encryptedPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                    encryptedPatientId = ObjTripleDESCryptoHelper.encryptText(PatientId.ToString());
                    if (dtFromDoctorDetails.Tables[2].Rows.Count > 0)
                    {
                        DoctorState = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[2].Rows[0]["State"]), string.Empty);
                        DoctorCity = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[2].Rows[0]["City"]), string.Empty);
                        DoctorCountry = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[2].Rows[0]["Country"]), string.Empty);
                    }

                    if (dtFromDoctorDetails.Tables[7].Rows.Count > 0)
                    {
                        DoctorSpeciality = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[7].Rows[0]["Speciality"]), string.Empty);
                    }
                }

                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                DataTable SecDt = ObjCommon.GetDoctorSecondaryEmail(DoctorEmail);
                if (SecDt.Rows != null && SecDt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(SecDt.Rows[0]["SecondaryEmail"].ToString()))
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail, SecDt.Rows[0]["SecondaryEmail"].ToString() };
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                    }
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                }
                //objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");

                // objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "patient-added-notification";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("UserName", PatientFirstName + ' ' + PatientLastName);
                dt.Rows.Add("City", City);

                dt.Rows.Add("DoctorName", DoctorFullName);
                dt.Rows.Add("Address", Address);
                dt.Rows.Add("Address2", Address2);
                dt.Rows.Add("State", State);
                dt.Rows.Add("Country", Country);
                dt.Rows.Add("ZipCode", ZipCode);
                dt.Rows.Add("Email", PatientEmail);
                dt.Rows.Add("Phone", Phone);
                dt.Rows.Add("publicpath", publicpath);
                dt.Rows.Add("ToMail", DoctorEmail);
                dt.Rows.Add("Pass", encryptedPassword);
                dt.Rows.Add("PatientId", encryptedPatientId);
                //RM-299 websiteurl is not supplied.
                dt.Rows.Add("websiteurl", ConfigurationManager.AppSettings.Get("websiteurl"));
                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, DoctorId);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("patient-added-notification", ex.Message, ex.StackTrace);
            }
        }



        #endregion

        #region Template Name: Patient Added Notification
        public void PatientAddedNotificationFromPublicProfile(int DoctorId, string PatientEmail, string PatientFirstName, string PatientLastName, string Phone, string Address, string Address2, string City, string State, string Country, string ZipCode)
        {
            try
            {
                string DoctorFullName = string.Empty;
                string DoctorSpeciality = string.Empty;
                string DoctorState = string.Empty;
                string publicpath = string.Empty;
                string DoctorCity = string.Empty;
                string DoctorCountry = string.Empty;
                string DoctorEmail = string.Empty;
                string Password = string.Empty;
                string encryptedPassword = string.Empty;
                DataSet dtFromDoctorDetails;
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = DoctorId;
                dtFromDoctorDetails = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", strParameter);

                if (dtFromDoctorDetails != null && dtFromDoctorDetails.Tables.Count > 0 && dtFromDoctorDetails.Tables[0].Rows[0]["Username"] != null)
                {
                    DoctorFullName = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[0].Rows[0]["FullName"]), string.Empty);
                    publicpath = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[0].Rows[0]["PublicProfileUrl"]), string.Empty);
                    DoctorEmail = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[0].Rows[0]["Username"]), string.Empty);
                    Password = Convert.ToString(ObjTripleDESCryptoHelper.decryptText(dtFromDoctorDetails.Tables[0].Rows[0]["Password"].ToString()));
                    encryptedPassword = ObjTripleDESCryptoHelper.encryptText(Password);

                    if (dtFromDoctorDetails.Tables[2].Rows.Count > 0)
                    {
                        DoctorState = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[2].Rows[0]["State"]), string.Empty);
                        DoctorCity = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[2].Rows[0]["City"]), string.Empty);
                        DoctorCountry = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[2].Rows[0]["Country"]), string.Empty);
                    }

                    if (dtFromDoctorDetails.Tables[7].Rows.Count > 0)
                    {
                        DoctorSpeciality = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[7].Rows[0]["Speciality"]), string.Empty);
                    }
                }

                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                DataTable SecDt = ObjCommon.GetDoctorSecondaryEmail(DoctorEmail);
                if (SecDt.Rows != null && SecDt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(SecDt.Rows[0]["SecondaryEmail"].ToString()))
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail, SecDt.Rows[0]["SecondaryEmail"].ToString() };
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                    }
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                }
                //objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");

                // objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "patient-added-notification";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("UserName", PatientFirstName + ' ' + PatientLastName);
                dt.Rows.Add("City", City);

                dt.Rows.Add("DoctorName", DoctorFullName);
                dt.Rows.Add("Address", Address);
                dt.Rows.Add("Address2", Address2);
                dt.Rows.Add("State", State);
                dt.Rows.Add("Country", Country);
                dt.Rows.Add("ZipCode", ZipCode);
                dt.Rows.Add("Email", PatientEmail);
                dt.Rows.Add("Phone", Phone);
                dt.Rows.Add("publicpath", publicpath);
                dt.Rows.Add("ToMail", DoctorEmail);
                dt.Rows.Add("Pass", encryptedPassword);


                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("patient-added-notification-from-public-profile", ex.Message, ex.StackTrace);
            }
        }



        #endregion

        #region Template Name:Patient SignUp Email
        public void PatientSignUpEMail(string Email, string password, int DoctorId)
        {
            try
            {
                TripleDESCryptoHelper cryptoHelper = new TripleDESCryptoHelper();
                string encryptedPassword = cryptoHelper.encryptText(password);

                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();

                //objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");

                objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "patient-signUp-email";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Email", Email);
                dt.Rows.Add("Password", password);
                dt.Rows.Add("EncPassword", encryptedPassword);
                dt.Rows.Add("websiteurl", ConfigurationManager.AppSettings.Get("websiteurl"));

                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, DoctorId);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("patient-signUp-email", ex.Message, ex.StackTrace);
            }

        }
        #endregion

        #region Template Name:Contact To Doctor On Public Profile
        public void PublicProfileContactme(int DoctorId, string Name, string Fromemail, string phoneno, string note)
        {
            try
            {
                string Doctorname = string.Empty;
                string Publicpath = string.Empty;
                string DoctorEmail = string.Empty;

                DataSet dtFromDoctorDetails;
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = DoctorId;
                dtFromDoctorDetails = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", strParameter);

                if (dtFromDoctorDetails != null && dtFromDoctorDetails.Tables.Count > 0 && dtFromDoctorDetails.Tables[0].Rows[0]["Username"] != null)
                {
                    Doctorname = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[0].Rows[0]["FullName"]), string.Empty);
                    Publicpath = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[0].Rows[0]["PublicProfileUrl"]), string.Empty);
                    DoctorEmail = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[0].Rows[0]["Username"]), string.Empty);

                }


                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                DataTable SecDt = ObjCommon.GetDoctorSecondaryEmail(DoctorEmail);
                if (SecDt.Rows != null && SecDt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(SecDt.Rows[0]["SecondaryEmail"].ToString()))
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail, SecDt.Rows[0]["SecondaryEmail"].ToString() };
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                    }
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                }
                //objCustomeMailPropery.FromMailAddress = Fromemail;

                // objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "contact-to-doctor-on-public-profile";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Name", Name);
                dt.Rows.Add("Email", Fromemail);
                dt.Rows.Add("PhoneNo", phoneno);
                dt.Rows.Add("Notes", note);
                dt.Rows.Add("Doctorname", Doctorname);
                dt.Rows.Add("publicpath", Publicpath);



                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("contact-to-doctor-on-public-profile", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Template Name:The Company administrator has sent you your login and password
        public void ResetPasswordOfDoctor(int UserId, string Password, string EncPassword)
        {

            try
            {

                string ToMail = string.Empty;
                string Name = string.Empty;
                string EncUsername = string.Empty;

                DataSet dtFromDoctorDetails;
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                dtFromDoctorDetails = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", strParameter);

                if (dtFromDoctorDetails != null && dtFromDoctorDetails.Tables.Count > 0 && dtFromDoctorDetails.Tables[0].Rows[0]["Username"] != null)
                {
                    Name = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[0].Rows[0]["FullName"]), string.Empty);
                    ToMail = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[0].Rows[0]["Username"]), string.Empty);

                }
                EncUsername = ObjTripleDESCryptoHelper.encryptText(ToMail);

                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                DataTable SecDt = ObjCommon.GetDoctorSecondaryEmail(ToMail);
                if (SecDt.Rows != null && SecDt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(SecDt.Rows[0]["SecondaryEmail"].ToString()))
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { ToMail, SecDt.Rows[0]["SecondaryEmail"].ToString() };
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { ToMail };
                    }
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { ToMail };
                }
                //objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");

                // objCustomeMailPropery.ToMailAddress = new List<string> { ToMail };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "the-company-administrator-has-sent-you-your-login-and-password";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Name", Name);
                dt.Rows.Add("ToMail", EncUsername);

                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);

            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("the-company-administrator-has-sent-you-your-login-and-password", ex.Message, ex.StackTrace);
            }


        }
        #endregion

        #region Template Name:Welcome To Company
        public void NewUserEmailFormat(string Email, string loginlink, string password, string companywebsite)
        {
            try
            {
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                // objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");

                objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "welcome-to-company";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Doctor", Email);
                dt.Rows.Add("Email", Email);
                dt.Rows.Add("Password", password);
                dt.Rows.Add("newloginlink", loginlink);
                dt.Rows.Add("CompanyWebsite", companywebsite);

                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("welcome-to-company", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Template Name:Reminder Mail Notification
        public void ReminderMailNotification(string Email, string loginlink, string password)
        {
            try
            {
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                DataTable SecDt = ObjCommon.GetDoctorSecondaryEmail(Email);
                if (SecDt.Rows != null && SecDt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(SecDt.Rows[0]["SecondaryEmail"].ToString()))
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { Email, SecDt.Rows[0]["SecondaryEmail"].ToString() };
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                    }
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                }
                // objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");

                objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "reminder-mail-notification";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Doctor", Email);
                dt.Rows.Add("Email", Email);
                dt.Rows.Add("Password", password);
                dt.Rows.Add("newloginlink", loginlink);


                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("reminder-mail-notification", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        #region Send Seleted Temapltes from admin side
        public bool SendEmailFromAdmin(int ToUserId, int TemplateId, string TemplateName, string NewTemplatesubject, string NewTemplate)
        {
            bool result = true;
            try
            {
                string ToUserFirstName = string.Empty; string ToUserLastName = string.Empty; string ToUserName = string.Empty;
                string ToImageUrl = string.Empty; string ToSpeciality = string.Empty; string ToState = string.Empty;
                string OfficeName = string.Empty; string PublicPath = string.Empty; string Phone = string.Empty;
                string FullAddress = string.Empty; string ToCity = string.Empty; string ToCountry = string.Empty;
                string ToEmail = string.Empty;
                string ToGuardianLetter = string.Empty; string ToGuardianLetterSubject = string.Empty; string ToMiddleName = string.Empty; string ToPatientLetter = string.Empty;
                string ToPatientLetterSubject = string.Empty; string ToRegistrationDate = string.Empty; string ToRelativePathToImage = string.Empty;
                string ToSalutation = string.Empty; string ToTwitterUrl = string.Empty; string Towebsite = string.Empty;
                string ToFacebookUrl = string.Empty; string ToPassword = string.Empty; string ToLinkedinUrl = string.Empty;

                DataSet dtInviteDoctor = new DataSet();
                SqlParameter[] addStudyGroupParams;
                addStudyGroupParams = new SqlParameter[1];
                addStudyGroupParams[0] = new SqlParameter("@UserId ", SqlDbType.Int);
                addStudyGroupParams[0].Value = ToUserId;

                dtInviteDoctor = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", addStudyGroupParams);


                if (dtInviteDoctor != null && dtInviteDoctor.Tables.Count > 0)
                {

                    if (dtInviteDoctor.Tables[0].Rows.Count > 0)
                    {
                        ToUserFirstName = dtInviteDoctor.Tables[0].Rows[0]["FirstName"].ToString();
                        ToMiddleName = dtInviteDoctor.Tables[0].Rows[0]["MiddleName"].ToString();
                        ToUserLastName = dtInviteDoctor.Tables[0].Rows[0]["LastName"].ToString();
                        ToUserName = dtInviteDoctor.Tables[0].Rows[0]["FullName"].ToString();
                        PublicPath = dtInviteDoctor.Tables[0].Rows[0]["PublicProfileUrl"].ToString();
                        ToImageUrl = dtInviteDoctor.Tables[0].Rows[0]["ImageName"].ToString();
                        ToEmail = dtInviteDoctor.Tables[0].Rows[0]["Username"].ToString();
                        ToPassword = dtInviteDoctor.Tables[0].Rows[0]["Password"].ToString();
                        ToRegistrationDate = dtInviteDoctor.Tables[0].Rows[0]["RegistrationDate"].ToString();
                        ToSalutation = dtInviteDoctor.Tables[0].Rows[0]["Salutation"].ToString();

                    }

                    if (dtInviteDoctor.Tables[1].Rows.Count > 0)
                    {
                        OfficeName = dtInviteDoctor.Tables[1].Rows[0]["AccountName"].ToString();
                    }



                    if (dtInviteDoctor.Tables[2].Rows.Count > 0)
                    {
                        ToState = dtInviteDoctor.Tables[2].Rows[0]["State"].ToString();
                        Phone = dtInviteDoctor.Tables[2].Rows[0]["Phone"].ToString();
                        ToCity = dtInviteDoctor.Tables[2].Rows[0]["City"].ToString();
                        ToCountry = dtInviteDoctor.Tables[2].Rows[0]["Country"].ToString();
                        FullAddress = Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["ExactAddress"]) + ", " + ToCity + " " + ToState + Convert.ToString(dtInviteDoctor.Tables[2].Rows[0]["ZipCode"]);

                    }


                    if (dtInviteDoctor.Tables[3].Rows.Count > 0)
                    {

                        ToFacebookUrl = dtInviteDoctor.Tables[3].Rows[0]["FacebookUrl"].ToString();
                        ToLinkedinUrl = dtInviteDoctor.Tables[3].Rows[0]["LinkedinUrl"].ToString();
                        ToTwitterUrl = dtInviteDoctor.Tables[3].Rows[0]["TwitterUrl"].ToString();
                    }


                    if (dtInviteDoctor.Tables[7].Rows.Count > 0)
                    {
                        ToSpeciality = dtInviteDoctor.Tables[7].Rows[0]["Speciality"].ToString();
                    }

                }


                string Invitation = "I would like to invite you to be Part of Recordlic.";


                string Actualpassword = "";
                Actualpassword = ObjTripleDESCryptoHelper.decryptText(ToPassword);
                string Doctorimage = ConfigurationManager.AppSettings.Get("DefaultDoctorImageForTemplate");


                if (!string.IsNullOrEmpty(ToImageUrl))
                {
                    Doctorimage = ConfigurationManager.AppSettings.Get("TemplatePeth") + ToImageUrl;
                }
                Doctorimage = Doctorimage.Replace(" ", "%20");

                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                DataTable SecDt = ObjCommon.GetDoctorSecondaryEmail(ToEmail);
                if (SecDt.Rows != null && SecDt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(SecDt.Rows[0]["SecondaryEmail"].ToString()))
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { ToEmail, SecDt.Rows[0]["SecondaryEmail"].ToString() };
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { ToEmail };
                    }
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { ToEmail };
                }
                //objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");

                // objCustomeMailPropery.ToMailAddress = new List<string> { ToEmail };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = TemplateName;

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Doctor", ToUserName);
                dt.Rows.Add("City", ToCity);
                dt.Rows.Add("Name", ToUserName);
                dt.Rows.Add("Doctor", ToUserName);
                dt.Rows.Add("Email", ToEmail);


                dt.Rows.Add("Doctor", ToUserName);
                dt.Rows.Add("Speciality", ToSpeciality);
                dt.Rows.Add("State", ToState);
                dt.Rows.Add("City", ToCity);
                dt.Rows.Add("Country", ToCountry);
                dt.Rows.Add("UserName", ToUserName);

                dt.Rows.Add("InvitetoName", ToUserName);
                dt.Rows.Add("Password", Actualpassword);
                dt.Rows.Add("EncPassword", ToPassword);
                dt.Rows.Add("LinkedinUrl", ToLinkedinUrl);
                dt.Rows.Add("MiddleName", ToMiddleName);
                dt.Rows.Add("PublicPath", PublicPath);
                dt.Rows.Add("RegistrationDate", ToRegistrationDate);
                dt.Rows.Add("Salutation", ToSalutation);
                dt.Rows.Add("TwitterUrl", ToTwitterUrl);
                dt.Rows.Add("website", Towebsite);
                dt.Rows.Add("Name", ToUserName);
                dt.Rows.Add("ToMail", ToEmail);
                dt.Rows.Add("OfficeName", OfficeName);
                dt.Rows.Add("Phone", Phone);
                dt.Rows.Add("Address", FullAddress);
                dt.Rows.Add("publicpath", PublicPath);
                dt.Rows.Add("Email", ToEmail);
                dt.Rows.Add("FirstName", HttpContext.Current.Session["CompanyName"] + " Admin");
                dt.Rows.Add("LastName", "");
                dt.Rows.Add("Invitation", Invitation);
                dt.Rows.Add("socialmediaurl", ToFacebookUrl + "<br/>" + ToLinkedinUrl + "<br/>" + ToTwitterUrl);
                dt.Rows.Add("Image1", Doctorimage);




                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog(TemplateName, ex.Message, ex.StackTrace);
                return false;
            }
            return result;
        }
        #endregion


        #region Tempate Name: SignUp Email
        public void SignUpEMail(string Email, string Name, string Phone, string facebookUrl, string twitterUrl, string linkedInUrl)
        {
            try
            {
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                //objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");

                objCustomeMailPropery.ToMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("emailoftravis") };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "signup-email";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Email", Email);
                dt.Rows.Add("Name", Name);
                dt.Rows.Add("Phone", Phone);
                dt.Rows.Add("facebookUrl", facebookUrl);
                dt.Rows.Add("twitterUrl", twitterUrl);
                dt.Rows.Add("linkedInUrl", linkedInUrl);


                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("signup-email", ex.Message, ex.StackTrace);

            }
        }
        #endregion

        #region Recover Password From Admin
        public void SendingForgetPassword(string ColleagueFullName, string DoctorEmail, string Password)
        {
            try
            {
                string EncUsername = string.Empty;

                EncUsername = ObjTripleDESCryptoHelper.encryptText(DoctorEmail);

                string Actualpassword = ObjTripleDESCryptoHelper.decryptText(Password);

                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                // objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");
                DataTable SecDt = ObjCommon.GetDoctorSecondaryEmail(DoctorEmail);
                if (SecDt.Rows != null && SecDt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(SecDt.Rows[0]["SecondaryEmail"].ToString()))
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail, SecDt.Rows[0]["SecondaryEmail"].ToString() };
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                    }
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                }
                //objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "recover-password-from-admin";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Name", ColleagueFullName);
                dt.Rows.Add("ToMail", EncUsername);
                dt.Rows.Add("Password", Actualpassword);
                dt.Rows.Add("EncPassword", Password);


                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("recover-password-from-admin", ex.Message, ex.StackTrace);

            }

        }
        #endregion

        #region Account Upgraded succesfully
        public void AccountUpgradedSuccessfully(int UserId, string CardNumber, string CardType, string transactionId, string Amount)
        {
            try
            {


                DataSet dtFromDoctorDetails;
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                dtFromDoctorDetails = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", strParameter);

                string FullName = string.Empty; string Email = string.Empty;
                if (dtFromDoctorDetails != null && dtFromDoctorDetails.Tables.Count > 0 && dtFromDoctorDetails.Tables[0].Rows[0]["Username"] != null)
                {
                    FullName = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[0].Rows[0]["FullName"]), string.Empty);

                    Email = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[0].Rows[0]["Username"]), string.Empty);
                }

                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                DataTable SecDt = ObjCommon.GetDoctorSecondaryEmail(Email);
                if (SecDt.Rows != null && SecDt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(SecDt.Rows[0]["SecondaryEmail"].ToString()))
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { Email, SecDt.Rows[0]["SecondaryEmail"].ToString() };
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                    }
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                }
                //objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");

                objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "account-upgraded-successfully";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("CompanyName", "Recordlinc");
                dt.Rows.Add("FullName", FullName);
                dt.Rows.Add("Amount", Amount + "(USD)");
                dt.Rows.Add("CardNumber", CardNumber);
                dt.Rows.Add("CardType", CardType);
                dt.Rows.Add("TransactionId", transactionId);


                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("account-upgraded-successfully", ex.Message, ex.StackTrace);

            }
        }
        #endregion

        #region Template Name:Welcome To Company With Paid Account
        public void SignUpWithPaidAccountMail(string Email, string FullName, string loginlink, string password, string companywebsite, string CardNumber, string CardType, string transactionId, string amount)
        {
            try
            {

                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                // objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");
                DataTable SecDt = ObjCommon.GetDoctorSecondaryEmail(Email);
                if (SecDt.Rows != null && SecDt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(SecDt.Rows[0]["SecondaryEmail"].ToString()))
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { Email, SecDt.Rows[0]["SecondaryEmail"].ToString() };
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                    }
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                }
                objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "welcome-to-company-with-paid-account";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("CompanyName", "Recordlinc");
                dt.Rows.Add("Email", Email);
                dt.Rows.Add("Amount", amount + " (USD)");
                dt.Rows.Add("CardNumber", CardNumber);
                dt.Rows.Add("CardType", CardType);
                dt.Rows.Add("TransactionId", transactionId);
                dt.Rows.Add("FullName", FullName);
                dt.Rows.Add("Password", password);
                dt.Rows.Add("newloginlink", loginlink);
                dt.Rows.Add("CompanyWebsite", companywebsite);


                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("welcome-to-company-with-paid-account", ex.Message, ex.StackTrace);

            }
        }
        #endregion

        #region Payment Failed

        public void PaymentFailed(string Mail, string Fname, string Lname, string Phone, string PaymentFailReason, string Url)
        {
            try
            {
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();

                //objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");
                objCustomeMailPropery.ToMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("emailoftravis") };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "payment-failed";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("DoctorFirstName", Fname);
                dt.Rows.Add("DoctorLastName", Lname);
                dt.Rows.Add("PaymentFailReason", PaymentFailReason);
                dt.Rows.Add("DoctorEmail", Mail);
                dt.Rows.Add("DoctorPhone", Phone);
                dt.Rows.Add("Url", Url);

                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {

                ObjCommon.InsertErrorLog("payment-failed", ex.Message, ex.StackTrace);
            }
        }


        #endregion

        #region Recurring Payment Failed
        public void RecurringPaymentFailed(string Mail, string Fname, string Lname, string Phone, string PaymentFailReason)
        {
            try
            {
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                //objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");
                objCustomeMailPropery.ToMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("emailoftravis") };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "recurring-payment-failed";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("DoctorFirstName", Fname);
                dt.Rows.Add("DoctorLastName", Lname);
                dt.Rows.Add("PaymentFailReason", PaymentFailReason);
                dt.Rows.Add("DoctorEmail", Mail);
                dt.Rows.Add("DoctorPhone", Phone);


                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Create Appointment for Patient
        //public void CreateAppointmentNotificationforPatient(DataTable dts)
        //{
        //    try
        //    {
        //        CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
        //        //objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");
        //        DataTable SecDt = ObjCommon.GetDoctorSecondaryEmail(dts.Rows[0]["PatientEmail"].ToString());
        //        if (SecDt.Rows != null && SecDt.Rows.Count > 0)
        //        {
        //            if (!string.IsNullOrWhiteSpace(SecDt.Rows[0]["SecondaryEmail"].ToString()))
        //            {
        //                objCustomeMailPropery.ToMailAddress = new List<string> { dts.Rows[0]["PatientEmail"].ToString(), SecDt.Rows[0]["SecondaryEmail"].ToString() };
        //            }
        //            else
        //            {
        //                objCustomeMailPropery.ToMailAddress = new List<string> { dts.Rows[0]["PatientEmail"].ToString() };
        //            }
        //        }
        //        else
        //        {
        //            objCustomeMailPropery.ToMailAddress = new List<string> { dts.Rows[0]["PatientEmail"].ToString() };
        //        }
        //        objCustomeMailPropery.ToMailAddress = new List<string> { dts.Rows[0]["PatientEmail"].ToString() };
        //        objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
        //        objCustomeMailPropery.MailTemplateName = "new-appointment-for-patient";

        //        DataTable dt = new DataTable();
        //        dt.Columns.Add("FieldName");
        //        dt.Columns.Add("FieldValue");

        //        dt.Rows.Add("DoctorName", dts.Rows[0]["DoctorName"]);
        //        dt.Rows.Add("PatientName", dts.Rows[0]["PatientName"]);
        //        dt.Rows.Add("DoctorEmail", dts.Rows[0]["DoctorEmail"]);
        //        dt.Rows.Add("PatientEmail", dts.Rows[0]["PatientEmail"]);
        //        dt.Rows.Add("DoctorAddress", dts.Rows[0]["DoctorAddress"]);
        //        dt.Rows.Add("PatientAddress", dts.Rows[0]["PatientAddress"]);
        //        dt.Rows.Add("AppointmentDate", DateTime.Parse(Convert.ToString(dts.Rows[0]["AppointmentDate"])).ToString(@"dd\:MM\:yyyy"));
        //        dt.Rows.Add("AppointmentTime", TimeSpan.Parse(Convert.ToString(dts.Rows[0]["AppointmentTime"])).ToString(@"hh\:mm")); //TimeSpan.Parse(dts.Rows[0]["AppointmentTime"].ToString()) (@"hh\:mm"));
        //        dt.Rows.Add("ServiceType", dts.Rows[0]["ServiceType"]);
        //        dt.Rows.Add("DoctorPhoneNumber", dts.Rows[0]["DoctorPhoneNo"]);
        //        dt.Rows.Add("PatientPhoneNumber", dts.Rows[0]["PatientPhoneNo"]);
        //        dt.Rows.Add("Where", "Recordlinc");
        //        dt.Rows.Add("DoctorNote", dts.Rows[0]["Note"]);
        //        dt.Rows.Add("CompanyName", "MyDentalfiles");

        //        dt = ObjCompany.GetEmailSubjectForTemplate(dt);
        //        dt = ObjCompany.GetEmailBodyForTemplate(dt);
        //        objCustomeMailPropery.MergeTags = dt;

        //        SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}


        #endregion

        #region Create Appointment for Doctor
        //public void CreateAppointmentNotificationforDoctor(DataTable dts)
        //{
        //    try
        //    {
        //        CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
        //        DataTable SecDt = ObjCommon.GetDoctorSecondaryEmail(dts.Rows[0]["DoctorEmail"].ToString());
        //        if (SecDt.Rows != null && SecDt.Rows.Count > 0)
        //        {
        //            if (!string.IsNullOrWhiteSpace(SecDt.Rows[0]["SecondaryEmail"].ToString()))
        //            {
        //                objCustomeMailPropery.ToMailAddress = new List<string> { dts.Rows[0]["DoctorEmail"].ToString(), SecDt.Rows[0]["SecondaryEmail"].ToString() };
        //            }
        //            else
        //            {
        //                objCustomeMailPropery.ToMailAddress = new List<string> { dts.Rows[0]["DoctorEmail"].ToString() };
        //            }
        //        }
        //        else
        //        {
        //            objCustomeMailPropery.ToMailAddress = new List<string> { dts.Rows[0]["DoctorEmail"].ToString() };
        //        }
        //        //objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");
        //        // objCustomeMailPropery.ToMailAddress = new List<string> { dts.Rows[0]["DoctorEmail"].ToString() };
        //        objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
        //        objCustomeMailPropery.MailTemplateName = "new-appointment-for-doctor";

        //        DataTable dt = new DataTable();
        //        dt.Columns.Add("FieldName");
        //        dt.Columns.Add("FieldValue");

        //        dt.Rows.Add("DoctorName", dts.Rows[0]["DoctorName"]);
        //        dt.Rows.Add("PatientName", dts.Rows[0]["PatientName"]);
        //        dt.Rows.Add("DoctorEmail", dts.Rows[0]["DoctorEmail"]);
        //        dt.Rows.Add("PatientEmail", dts.Rows[0]["PatientEmail"]);
        //        dt.Rows.Add("DoctorAddress", dts.Rows[0]["DoctorAddress"]);
        //        dt.Rows.Add("PatientAddress", dts.Rows[0]["PatientAddress"]);
        //        dt.Rows.Add("AppointmentDate", DateTime.Parse(Convert.ToString(dts.Rows[0]["AppointmentDate"])).ToString(@"dd\:MM\:yyyy"));
        //        dt.Rows.Add("AppointmentTime", TimeSpan.Parse(Convert.ToString(dts.Rows[0]["AppointmentTime"])).ToString(@"hh\:mm"));
        //        dt.Rows.Add("ServiceType", dts.Rows[0]["ServiceType"]);
        //        dt.Rows.Add("DoctorPhoneNumber", dts.Rows[0]["DoctorPhoneNo"]);
        //        dt.Rows.Add("PatientPhoneNumber", dts.Rows[0]["PatientPhoneNo"]);
        //        dt.Rows.Add("Where", "MyDentalfiles");
        //        dt.Rows.Add("PatientNote", dts.Rows[0]["Note"]);
        //        dt.Rows.Add("CompanyName", "Recordlinc");

        //        dt = ObjCompany.GetEmailSubjectForTemplate(dt);
        //        dt = ObjCompany.GetEmailBodyForTemplate(dt);
        //        objCustomeMailPropery.MergeTags = dt;

        //        SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        #endregion

        #region Send Referral details to Patient
        public void SendReferralDetailsToPatients(int PatientId, int ColleaguesId, string FromField, int LocationId, int DoctorId)
        {
            try
            {
                string PatientEmail = string.Empty; string PublicPath = string.Empty;
                string ReciverName = string.Empty; string ReciverPhone = string.Empty;
                string Address = string.Empty; string ImageName = string.Empty;
                string LocationMap = string.Empty; string Specialty = string.Empty;
                string DoctorImage = string.Empty; string FaceBookURL = string.Empty;
                string GoogleplusURL = string.Empty; string TwitterURL = string.Empty;
                string LinkedinURL = string.Empty; string YoutubeURL = string.Empty;
                string PinterestUrl = string.Empty; string BlogUrl = string.Empty;
                string AddressTitle = string.Empty;
                string SocialMediaStr = string.Empty;
                ObjColleaguesData = new clsColleaguesData();
                ObjPatientsData = new clsPatientsData();
                DataSet ds = new DataSet();
                ds = ObjColleaguesData.GetDoctorDetailsById(ColleaguesId);
                if (ds != null && ds.Tables.Count > 0)
                {
                    PublicPath = ds.Tables[0].Rows[0]["PublicProfileUrl"].ToString();
                    ReciverName = ds.Tables[0].Rows[0]["FirstName"] + " " + ds.Tables[0].Rows[0]["LastName"];

                    ReciverPhone = ObjCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["Phone"]), string.Empty);
                    ImageName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["ImageName"]), string.Empty);
                    Specialty = ObjCommon.CheckNull(Convert.ToString(ds.Tables[7].Rows[0]["Speciality"]), string.Empty);
                    List<int> locationid = new List<int>();

                    if (LocationId == 0)
                    {
                        LocationId = Convert.ToInt32(ds.Tables[2].Rows[0]["AddressInfoId"]);
                    }

                    for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
                    {
                        if (LocationId == Convert.ToInt32(ds.Tables[2].Rows[i]["AddressInfoId"]))
                        {
                            if (!string.IsNullOrEmpty(Address))
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[2].Rows[i]["ExactAddress"])) && !string.IsNullOrWhiteSpace(Convert.ToString(ds.Tables[2].Rows[i]["ExactAddress"])))
                                    Address = Address + ", " + ds.Tables[2].Rows[i]["ExactAddress"].ToString();
                            }
                            else
                            {
                                Address = ds.Tables[2].Rows[i]["ExactAddress"].ToString();
                            }

                            if (!string.IsNullOrEmpty(Address))
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[2].Rows[i]["City"]))
                                    && !string.IsNullOrWhiteSpace(Convert.ToString(ds.Tables[2].Rows[i]["City"])))
                                    Address = Address + ", " + ds.Tables[2].Rows[i]["City"].ToString();
                            }
                            else
                            {
                                Address = ds.Tables[2].Rows[i]["City"].ToString();
                            }

                            if (!string.IsNullOrEmpty(Address))
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[2].Rows[i]["State"]))
                                    && !string.IsNullOrWhiteSpace(Convert.ToString(ds.Tables[2].Rows[i]["State"])))
                                    Address = Address + ", " + ds.Tables[2].Rows[i]["State"].ToString();
                            }
                            else
                            {
                                Address = ds.Tables[2].Rows[i]["State"].ToString();
                            }

                            if (!string.IsNullOrEmpty(Address))
                            {
                                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[2].Rows[i]["ZipCode"]))
                                    && !string.IsNullOrWhiteSpace(Convert.ToString(ds.Tables[2].Rows[i]["ZipCode"])))
                                    Address = Address + ", " + ds.Tables[2].Rows[i]["ZipCode"].ToString();
                            }
                            else
                            {
                                Address = ds.Tables[2].Rows[i]["ZipCode"].ToString();
                            }

                            LocationMap = ds.Tables[2].Rows[i]["ExactAddress"] + "+" + ds.Tables[2].Rows[i]["City"] + "+" + ds.Tables[2].Rows[i]["State"] + "+" + ds.Tables[2].Rows[i]["Country"] + "+" + ds.Tables[2].Rows[i]["ZipCode"];
                        }
                    }
                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        FaceBookURL = ds.Tables[3].Rows[0]["FacebookUrl"].ToString();
                        LinkedinURL = ds.Tables[3].Rows[0]["LinkedinUrl"].ToString();
                        GoogleplusURL = ds.Tables[3].Rows[0]["GoogleplusUrl"].ToString();
                        TwitterURL = ds.Tables[3].Rows[0]["TwitterUrl"].ToString();
                        YoutubeURL = ds.Tables[3].Rows[0]["YoutubeUrl"].ToString();
                        PinterestUrl = ds.Tables[3].Rows[0]["PinterestUrl"].ToString();
                        BlogUrl = ds.Tables[3].Rows[0]["BlogUrl"].ToString();
                        if (!string.IsNullOrWhiteSpace(FaceBookURL)) { SocialMediaStr += "<a href='" + FaceBookURL + "' style='cursor:pointer;' target='_blank'><img src='https://www.recordlinc.com/images/facebook.png' /></a>&nbsp;"; }
                        if (!string.IsNullOrWhiteSpace(TwitterURL)) { SocialMediaStr += "<a href='" + TwitterURL + "' style='cursor:pointer;' target='_blank'><img src='https://www.recordlinc.com/images/twitter.png' /></a>&nbsp;"; }
                        if (!string.IsNullOrWhiteSpace(GoogleplusURL)) { SocialMediaStr += "<a href='" + GoogleplusURL + "' style='cursor:pointer;' target='_blank'><img src='https://www.recordlinc.com/images/googleplus.png' /></a>&nbsp;"; }
                        if (!string.IsNullOrWhiteSpace(YoutubeURL)) { SocialMediaStr += "<a href ='" + YoutubeURL + "' style='cursor:pointer;' target='_blank'><img src='https://www.recordlinc.com/images/youtube.png' /></a>&nbsp;"; }
                        if (!string.IsNullOrWhiteSpace(BlogUrl)) { SocialMediaStr += "<a href='" + BlogUrl + "' style='cursor:pointer;' target='_blank'><img src='https://www.recordlinc.com/images/blog.png' /></a>&nbsp;"; }
                        if (!string.IsNullOrWhiteSpace(PinterestUrl)) { SocialMediaStr += "<a href='" + PinterestUrl + "' style='cursor:pointer;' target='_blank'><img src='https://www.recordlinc.com/images/pinterest.png' /></a>&nbsp;"; }
                        if (!string.IsNullOrWhiteSpace(LinkedinURL)) { SocialMediaStr += "<a href='" + LinkedinURL + "' style='cursor:pointer;' target='_blank'><img src='https://www.recordlinc.com/images/linkedin.png' /></a>"; }
                    }
                    var tbl = ds.Tables[2].AsEnumerable().Where(x => x.Field<int>("AddressInfoId").Equals(LocationId));

                    if (!string.IsNullOrWhiteSpace(ImageName))
                    {
                        DoctorImage = ConfigurationManager.AppSettings.Get("TemplatePeth") + ImageName;
                    }
                    if (!string.IsNullOrEmpty(Address) && !string.IsNullOrWhiteSpace(Address))
                    {
                        AddressTitle = "Here is the address for Dr. " + ReciverName + " office:";
                    }
                }
                DataSet des = new DataSet();
                des = ObjPatientsData.GetPateintDetailsByPatientId(PatientId, DoctorId);
                if (des != null && des.Tables.Count > 0)
                {
                    PatientEmail = des.Tables[0].Rows[0]["Email"].ToString();
                }

                int EndRecId = Convert.ToInt32(ColleaguesId) + 999;
                int EncrPatientId = PatientId + 999;
                string EncrptedString = Convert.ToString(EncrPatientId) + '|' + Convert.ToString(EndRecId);
                string UrlLink = string.Empty;
                UrlLink = ConfigurationManager.AppSettings["OneClickreferralURL"] + "AccountSettings/GetPatientReferralForm?TYHJNABGA=" + EncrptedString;

                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                objCustomeMailPropery.ToMailAddress = new List<string> { PatientEmail };
                objCustomeMailPropery.MailTemplateName = "referral-details-send-to-patient";
                DataTable dt = new DataTable();
                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("SenderName", FromField);
                dt.Rows.Add("ReciverName", ReciverName);
                dt.Rows.Add("PhoneNumber", ReciverPhone);
                dt.Rows.Add("Address", Address);
                dt.Rows.Add("publicpath", PublicPath);
                dt.Rows.Add("LocationMap", LocationMap);
                dt.Rows.Add("DoctorImage", DoctorImage);
                dt.Rows.Add("Speciality", Specialty);
                //dt.Rows.Add("FacebookURL", FaceBookURL);
                //dt.Rows.Add("TwitterURL", TwitterURL);
                //dt.Rows.Add("GoogleplusURL", GoogleplusURL);
                //dt.Rows.Add("YoutubeURL", YoutubeURL);
                //dt.Rows.Add("BlogUrl", BlogUrl);
                //dt.Rows.Add("PinterestUrl", PinterestUrl);
                //dt.Rows.Add("LinkedinURL", LinkedinURL);
                dt.Rows.Add("SocialMediaDetails", SocialMediaStr);
                dt.Rows.Add("AddressTitle", AddressTitle);
                dt.Rows.Add("PageUrl", UrlLink);

                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, Convert.ToInt32(HttpContext.Current.Session["UserId"]));

            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        #endregion

        #region Send Support Mail Form First Time Login
        public void Firstlimeloginsupportmail(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = ObjColleaguesData.GetDoctorDetailsbyId(UserId);
                string FullName = string.Empty; string Email = string.Empty; string SecondaryEmail = string.Empty;
                if (dt.Rows.Count > 0 && dt != null)
                {
                    FullName = Convert.ToString(dt.Rows[0]["FirstName"]) + " " + Convert.ToString(dt.Rows[0]["LastName"]);
                    Email = ObjCommon.CheckNull((Convert.ToString(dt.Rows[0]["UserName"])), string.Empty);
                    SecondaryEmail = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["SecondaryEmail"]), string.Empty);
                }
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                if (!string.IsNullOrWhiteSpace(SecondaryEmail))
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { Email, SecondaryEmail };
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                }

                objCustomeMailPropery.MailTemplateName = "first-time-login-support-template";

                DataTable dts = new DataTable();

                dts.Columns.Add("FieldName");
                dts.Columns.Add("FieldValue");

                dts.Rows.Add("Doctorname", FullName);

                dts = ObjCompany.GetEmailSubjectForTemplate(dts);
                dts = ObjCompany.GetEmailBodyForTemplate(dts);
                objCustomeMailPropery.MergeTags = dts;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("First time login mail teplate", Ex.Message, Ex.StackTrace);
            }
        }
        #endregion


        #region Send Landing Email
        public bool SendLandingEmailToUser(string emailId, string PdfTypeLink, string pdftye)
        {
            try
            {
                DataTable dts = new DataTable();
                dts.Columns.Add("FieldName");
                dts.Columns.Add("FieldValue");
                dts.Rows.Add("PdfType", pdftye);
                dts.Rows.Add("PdfTypeLink", PdfTypeLink);
                dts.Rows.Add("Subject", pdftye);
                dts.Rows.Add("CompanyName", "RecordLinc");
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                objCustomeMailPropery.MailTemplateName = "landing-template";
                objCustomeMailPropery.ToMailAddress = new List<string> { emailId };
                objCustomeMailPropery.MergeTags = dts;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
                return true;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("Landing template error", Ex.Message, Ex.StackTrace);
                return false;
            }
        }
        #endregion

        #region Send email to sales from landing
        public bool SendEmailToSalesFromLanding(string useremail, string toemail, string fullname, string message, string pageUrl)
        {
            try
            {
                DataTable dts = new DataTable();
                dts.Columns.Add("FieldName");
                dts.Columns.Add("FieldValue");
                dts.Rows.Add("UserEmail", useremail);
                dts.Rows.Add("FullName", fullname);
                dts.Rows.Add("Message", message);
                dts.Rows.Add("PageUrl", pageUrl);
                dts.Rows.Add("CompanyName", "RecordLinc");
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                objCustomeMailPropery.MailTemplateName = "receive-email-to-sales-team-from-landing-page";
                objCustomeMailPropery.ToMailAddress = new List<string> { toemail };
                objCustomeMailPropery.MergeTags = dts;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
                return true;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("Landing template error", Ex.Message, Ex.StackTrace);
                return false;
            }
        }
        #endregion

        #region  Send Support Welcome Email for landing page user.
        public bool SendSupportWelcomeEmail(string emailaddress)
        {
            try
            {
                DataTable dts = new DataTable();
                dts.Columns.Add("FieldName");
                dts.Columns.Add("FieldValue");
                dts.Rows.Add("CompanyName", "RecordLinc");
                dts.Rows.Add("Subject", "Recordlinc - Welcome email from support");
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                objCustomeMailPropery.MailTemplateName = "welcome-template-from-support";
                objCustomeMailPropery.ToMailAddress = new List<string> { emailaddress };
                objCustomeMailPropery.MergeTags = dts;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
                return true;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("Landing template error", Ex.Message, Ex.StackTrace);
                return false;
            }
        }
        #endregion

        #region Send Email to block Doctor
        public bool SendBlockDoctorEmail(string email, string fullname)
        {
            try
            {
                DataTable dts = new DataTable();
                dts.Columns.Add("FieldName");
                dts.Columns.Add("FieldValue");
                dts.Rows.Add("CompanyName", "RecordLinc");
                dts.Rows.Add("FullName", fullname);
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                objCustomeMailPropery.MailTemplateName = "email-for-blocked-user";
                objCustomeMailPropery.ToMailAddress = new List<string> { email };
                objCustomeMailPropery.MergeTags = dts;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
                return true;
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("Send Email to block Doctor", ex.Message, ex.StackTrace);
                return false;
            }
        }
        #endregion
        #region IluvMyDentist Uses
        public bool SendMailfromIluvmydentist(MailSendMendrill Obj, string _serverPath)
        {
            try
            {
                DataTable dts = new DataTable();
                dts.Columns.Add("FieldName");
                dts.Columns.Add("FieldValue");
                dts.Rows.Add("Companyname", "RecordLinc");
                dts.Rows.Add("Link", Obj.Message);
                dts.Rows.Add("Username", Obj.Username);
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                objCustomeMailPropery.MailTemplateName = "email-to-friend-from-i-luv-my-dentist";
                objCustomeMailPropery.ToMailAddress = new List<string> { Obj.Email };
                dts = ObjCompany.GetEmailSubjectForTemplate(dts);
                dts = ObjCompany.GetEmailBodyForTemplate(dts);
                objCustomeMailPropery.MergeTags = dts;
                objCustomeMailPropery.attachment = _serverPath;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
                return true;
            }
            catch (Exception EX)
            {
                ObjCommon.InsertErrorLog("SendMailfromIluvmydentist", EX.Message, EX.StackTrace);
                throw;
            }
        }
        #endregion

        #region
        public bool SendMailToSupportforRequestDemo(MDLPatientSignUp obj)
        {
            try
            {
                DataTable dts = new DataTable();
                dts.Columns.Add("FieldName");
                dts.Columns.Add("FieldValue");
                dts.Rows.Add("Companyname", "RecordLinc");
                dts.Rows.Add("FullName", obj.FirstName + " " + obj.LastName);
                dts.Rows.Add("UserEmail", obj.Email);
                dts.Rows.Add("Phone", obj.Phone);
                dts.Rows.Add("PageUrl", "I Luv My Dentist");
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                objCustomeMailPropery.MailTemplateName = "request-for-demo-i-luv-my-dentist";
                objCustomeMailPropery.ToMailAddress = new List<string> { "support@recordlinc.com" };
                objCustomeMailPropery.MergeTags = dts;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
                return true;
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("SendMailToSupportforRequestDemo", ex.Message, ex.StackTrace);
                throw;
            }
        }
        #endregion
        #region For Claim this Profile new Template
        public void ClaimThisProfile(string ColleagueFullName, string DoctorEmail, string Password)
        {
            try
            {
                string EncUsername = string.Empty;

                EncUsername = ObjTripleDESCryptoHelper.encryptText(DoctorEmail);

                string Actualpassword = ObjTripleDESCryptoHelper.decryptText(Password);

                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                // objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");
                DataTable SecDt = ObjCommon.GetDoctorSecondaryEmail(DoctorEmail);
                if (SecDt.Rows != null && SecDt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(SecDt.Rows[0]["SecondaryEmail"].ToString()))
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail, SecDt.Rows[0]["SecondaryEmail"].ToString() };
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                    }
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                }
                //objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "claim-this-profile";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Name", ColleagueFullName);
                dt.Rows.Add("ToMail", EncUsername);
                dt.Rows.Add("Password", Actualpassword);
                dt.Rows.Add("EncPassword", Password);


                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("claim-this-profile", ex.Message, ex.StackTrace);

            }

        }
        #endregion

        #region Request for IluvMyDentist
        public bool SendRequstMailtoPatient(int UserId, int PatientId, string PatientName, string PatientEmail)
        {
            bool Result = false;
            try
            {
                string FullName = string.Empty; string link = LoveDentistlink; string PublicProfileName = string.Empty; string Phone = string.Empty;
                DataSet ds = new DataSet();
                ds = ObjColleaguesData.GetDoctorDetailsById(UserId);
                if (ds.Tables.Count > 0 && ds != null)
                {
                    FullName = Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]) + " " + Convert.ToString(ds.Tables[0].Rows[0]["LastName"]);
                    PublicProfileName = Convert.ToString(ds.Tables[0].Rows[0]["PublicProfileUrl"]);
                }
                if (ds.Tables[2].Rows.Count > 0 && ds.Tables[2].Rows != null)
                {
                    DataTable dtt = ds.Tables[2];

                    var query = from r in dtt.AsEnumerable()
                                where r.Field<string>("Phone") != "" &&
                                        r.Field<string>("Phone") != null
                                let objectArray = new object[]
                                {
                                    r.Field<string>("Phone")
                                }
                                select objectArray;
                    if (query.Count() > 0)
                    {
                        Phone = Convert.ToString(query.First()[0]);
                    }
                }
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                objCustomeMailPropery.ToMailAddress = new List<string> { PatientEmail };
                objCustomeMailPropery.MailTemplateName = "request-for-iluvmydentist";

                DataTable dts = new DataTable();

                dts.Columns.Add("FieldName");
                dts.Columns.Add("FieldValue");

                dts.Rows.Add("DoctorName", FullName);
                dts.Rows.Add("PatientName", PatientName);
                dts.Rows.Add("link", link + PublicProfileName);
                dts.Rows.Add("phone", Phone);

                dts = ObjCompany.GetEmailSubjectForTemplate(dts);
                dts = ObjCompany.GetEmailBodyForTemplate(dts);
                objCustomeMailPropery.MergeTags = dts;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
                MailHistory obj = new MailHistory();
                obj.PatientId = PatientId;
                obj.UserId = UserId;
                obj.TemplateId = (int)BO.Enums.Common.MailTemplate.RequestForIluvMyDentist;
                ObjCommon.InsertupdateMailHistory(obj);
                return Result = true;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        #endregion

        #region Insruance Cost Estimator
        public bool SendCostEstimationRequest(BO.ViewModel.SendInsuranceRequestBO model, int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = ObjColleaguesData.GetDoctorDetailsbyId(UserId);
                string Email = string.Empty; string SecondaryEmail = string.Empty;
                if (dt.Rows.Count > 0 && dt != null)
                {
                    if (Convert.ToString(dt.Rows[0]["PublicProfileUrl"]).ToLower() == "lanallen" || Convert.ToString(dt.Rows[0]["PublicProfileUrl"]).ToLower() == "gregsmith")
                    {
                        Email = "estimates@tucsonteeth.com";
                        SecondaryEmail = null;
                    }
                    else
                    {
                        Email = ObjCommon.CheckNull((Convert.ToString(dt.Rows[0]["UserName"])), string.Empty);
                        SecondaryEmail = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["SecondaryEmail"]), string.Empty);
                    }

                }
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                if (!string.IsNullOrWhiteSpace(SecondaryEmail))
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { Email, SecondaryEmail };
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                }
                objCustomeMailPropery.MailTemplateName = "insurance-cost-estimation-request";


                DataTable dts = new DataTable();

                dts.Columns.Add("FieldName");
                dts.Columns.Add("FieldValue");
                dts.Rows.Add("InsuranceName", model.InsuranceCompanyName);
                dts.Rows.Add("ProceduresName", model.ProceduresName);
                dts.Rows.Add("PatientEmail", !string.IsNullOrWhiteSpace(model.EstimateViaEmail) ? model.EstimateViaEmail : "Not Provided");
                dts.Rows.Add("PatientPhone", !string.IsNullOrWhiteSpace(model.EstimateViaCall) ? model.EstimateViaCall : "Not Provided");
                dts.Rows.Add("PatientText", !string.IsNullOrWhiteSpace(model.EstimateViaText) ? model.EstimateViaText : "Not Provided");
                dts.Rows.Add("PatientName", model.objPatient.FirstName + " " + model.objPatient.LastName);
                dts.Rows.Add("TotalCost", model.TotalCost.ToString("#.00", System.Globalization.CultureInfo.InvariantCulture));
                if (model.ServiceCost > 0)
                {
                    dts.Rows.Add("PatientCost", model.ServiceCost.ToString("#.00", System.Globalization.CultureInfo.InvariantCulture));
                }
                else
                {
                    dts.Rows.Add("PatientCost", 0);
                }


                dts = ObjCompany.GetEmailSubjectForTemplate(dts);
                dts = ObjCompany.GetEmailBodyForTemplate(dts);
                objCustomeMailPropery.MergeTags = dts;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
                return true;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public bool SendCostEstimationToUser(CalculateInsuranceModel model, int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = ObjColleaguesData.GetDoctorDetailsbyId(UserId);
                string Email = string.Empty; string SecondaryEmail = string.Empty;
                if (dt.Rows.Count > 0 && dt != null)
                {
                    if (Convert.ToString(dt.Rows[0]["PublicProfileUrl"]).ToLower() == "lanallen" || Convert.ToString(dt.Rows[0]["PublicProfileUrl"]).ToLower() == "gregsmith")
                    {
                        Email = "estimates@tucsonteeth.com";
                        SecondaryEmail = null;
                    }
                    else
                    {
                        Email = ObjCommon.CheckNull((Convert.ToString(dt.Rows[0]["UserName"])), string.Empty);
                        SecondaryEmail = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["SecondaryEmail"]), string.Empty);
                    }

                }
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                if (!string.IsNullOrWhiteSpace(SecondaryEmail))
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { Email, SecondaryEmail };
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                }
                objCustomeMailPropery.MailTemplateName = string.IsNullOrWhiteSpace(model.PatientName) ? "insurance-cost-estimation-before-appointment-request" : "insurance-cost-estimation-request";
                //if (!string.IsNullOrWhiteSpace(model.PatientName))
                //{
                //    objCustomeMailPropery.MailTemplateName = "insurance-cost-estimation-before-appointment-request";
                //}
                //else
                //{
                //    objCustomeMailPropery.MailTemplateName = "insurance-cost-estimation-before-appointment-request";
                //}

                DataTable dts = new DataTable();

                dts.Columns.Add("FieldName");
                dts.Columns.Add("FieldValue");
                dts.Rows.Add("InsuranceName", model.InsuranceCompanyName);
                dts.Rows.Add("ProceduresName", model.ProceduresName);
                dts.Rows.Add("PatientEmail", !string.IsNullOrWhiteSpace(model.EstimateViaEmail) ? model.EstimateViaEmail : "Not Provided");
                dts.Rows.Add("PatientPhone", !string.IsNullOrWhiteSpace(model.EstimateViaCall) ? model.EstimateViaCall : "Not Provided");
                dts.Rows.Add("PatientText", !string.IsNullOrWhiteSpace(model.EstimateViaText) ? model.EstimateViaText : "Not Provided");
                if (!string.IsNullOrWhiteSpace(model.PatientName))
                {
                    dts.Rows.Add("PatientName", model.PatientName);
                }
                dts.Rows.Add("TotalCost", model.TotalCost.ToString("#.00", System.Globalization.CultureInfo.InvariantCulture));
                if (model.ServiceCost > 0)
                {
                    dts.Rows.Add("PatientCost", model.ServiceCost.ToString("#.00", System.Globalization.CultureInfo.InvariantCulture));
                }
                else
                {
                    dts.Rows.Add("PatientCost", 0);
                }


                dts = ObjCompany.GetEmailSubjectForTemplate(dts);
                dts = ObjCompany.GetEmailBodyForTemplate(dts);
                objCustomeMailPropery.MergeTags = dts;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
                return true;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        #endregion

        #region Send Notification when some one add attachments on patient history tab
        public static bool SendFileuploadNotification(string ToDoctorName, string PatientName, string Link, string DoctorEmail, string DoctorName)
        {
            try
            {
                clsCompany ObjCompany = new clsCompany();
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                DataTable SecDt = new clsCommon().GetDoctorSecondaryEmail(DoctorEmail);
                if (SecDt.Rows != null && SecDt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(SecDt.Rows[0]["SecondaryEmail"].ToString()))
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail, SecDt.Rows[0]["SecondaryEmail"].ToString() };
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                    }
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                }
                objCustomeMailPropery.MailTemplateName = "patient-fileupload-notification";

                DataTable dts = new DataTable();

                dts.Columns.Add("FieldName");
                dts.Columns.Add("FieldValue");

                dts.Rows.Add("ToDoctorName", ToDoctorName);
                dts.Rows.Add("PatientName", PatientName);
                dts.Rows.Add("DoctorName", DoctorName);
                dts.Rows.Add("encriptedstring", Link);


                dts = ObjCompany.GetEmailSubjectForTemplate(dts);
                dts = ObjCompany.GetEmailBodyForTemplate(dts);
                objCustomeMailPropery.MergeTags = dts;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
                return true;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        #endregion
        #region Send Mail to Justin and Mailsa if doctor invite colleagues
        public bool InviteColleaguesMailForSupport(string HTML, int UserId)
        {
            try
            {
                string supportmail = ConfigurationManager.AppSettings.Get("SupportMailId");
                string Melissamail = ConfigurationManager.AppSettings.Get("MelissaMailId");
                DataTable dt = new DataTable();
                string DoctorName = string.Empty; string SecondaryEmail = string.Empty;
                dt = ObjColleaguesData.GetDoctorDetailsbyId(UserId);
                if (dt.Rows.Count > 0 && dt != null)
                {
                    DoctorName = ObjCommon.CheckNull((Convert.ToString(dt.Rows[0]["FirstName"]) + " " + Convert.ToString(dt.Rows[0]["LastName"])), string.Empty);
                }
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                objCustomeMailPropery.ToMailAddress = new List<string> { supportmail, Melissamail };
                // objCustomeMailPropery.ToMailAddress = new List<string> { "ankitsolanki92@gmail.com" };
                objCustomeMailPropery.MailTemplateName = "invite-colleague-mail-for-support";

                DataTable dts = new DataTable();

                dts.Columns.Add("FieldName");
                dts.Columns.Add("FieldValue");

                dts.Rows.Add("DoctorName", DoctorName);
                dts.Rows.Add("HTML", HTML);

                dts = ObjCompany.GetEmailSubjectForTemplate(dts);
                dts = ObjCompany.GetEmailBodyForTemplate(dts);
                objCustomeMailPropery.MergeTags = dts;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
                return true;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        #endregion
        #region Send Mail to Patient For Check and Update Patient Referral settings.
        public bool SendMailToPatientForRefferalSettings(string Email, string DoctorName, string UrlLink, int SenderId, string Phone)
        {
            try
            {
                DataTable dts = new DataTable();
                dts.Columns.Add("FieldName");
                dts.Columns.Add("FieldValue");
                dts.Rows.Add("DoctorName", DoctorName);
                dts.Rows.Add("PageUrl", UrlLink);
                string strPhone = string.Empty;
                if (!string.IsNullOrWhiteSpace(Phone))
                {
                    strPhone = "You can also reach them at " + Phone;
                }
                dts.Rows.Add("Phone", strPhone);
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                objCustomeMailPropery.MailTemplateName = "request-for-patient-referral-setting";
                objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                objCustomeMailPropery.subject = "Mail for save referral setting";
                objCustomeMailPropery.MergeTags = dts;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, SenderId);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("", Ex.Message, Ex.StackTrace);
            }
            return true;
        }
        #endregion

        public bool SendEmailForOneClickReferral(ReferralDetailsOneClick item, BO.Enums.Common.ReferralType type)
        {
            try
            {
                string CountryCode = ConfigurationManager.AppSettings.Get("CountryCode");
                string WebsiteConversationViewLink = ConfigurationManager.AppSettings.Get("WebsiteConversationViewLink") + item.MessageId;
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                DataTable dts = new DataTable();
                dts.Columns.Add("FieldName");
                dts.Columns.Add("FieldValue");
                dts.Rows.Add("WebsiteConversationViewLink", WebsiteConversationViewLink);
                string dynamicMessage = "";
                if (type == BO.Enums.Common.ReferralType.SENDER)
                {
                    //Changes for New Mail Templete for Sender
                    objCustomeMailPropery.ToMailAddress = new List<string> { item.SenderEmail };
                    dts.Rows.Add("PName", item.PatientName);
                    dts.Rows.Add("RName", item.ReceiverName);
                    dts.Rows.Add("RPhone", item.ReceiverPhone);
                    dts.Rows.Add("REmail", item.ReceiverEmail);
                    dts.Rows.Add("Specility", item.Specility);
                    dynamicMessage = "You have sent a One Click Referral to " + item.ReceiverName + " for Patient " + item.PatientName + "";
                    if (!string.IsNullOrWhiteSpace(item.SenderMobile))
                    {
                        try
                        {
                            string toSenderPhone = Convert.ToString(item.SenderMobile.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""));
                            new SendSMS().SendCustomeSMS(item.SenderId,item.ReceiverId, dynamicMessage, CountryCode + toSenderPhone,"DENTIST","DENTIST");
                        }
                        catch (Exception Ex)
                        {
                            new clsCommon().InsertErrorLog("clsTemplate---SendEmailForOneClickReferral", Ex.Message, Ex.StackTrace);
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(item.SenderPhone))
                    {
                        try
                        {
                            string toSenderPhone = Convert.ToString(item.SenderPhone.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""));
                            new SendSMS().SendCustomeSMS(item.SenderId, item.ReceiverId, dynamicMessage, CountryCode + toSenderPhone, "DENTIST", "DENTIST");
                        }
                        catch (Exception Ex)
                        {
                            new clsCommon().InsertErrorLog("clsTemplate---SendEmailForOneClickReferral", Ex.Message, Ex.StackTrace);
                        }
                    }
                }
                else if (type == BO.Enums.Common.ReferralType.PATIENT)
                {
                    //Template changes Suggeted by Travis for Patient
                    objCustomeMailPropery.ToMailAddress = new List<string> { item.PatientEmail };
                    dts.Rows.Add("SenderName", item.SenderName);
                    dts.Rows.Add("ReceiverName", item.ReceiverName);
                    if (!string.IsNullOrWhiteSpace(item.ReceiverPhone))
                    {
                        dts.Rows.Add("Phone", item.ReceiverPhone);
                    }
                    else if (!string.IsNullOrWhiteSpace(item.ReceiverMobile))
                    {
                        dts.Rows.Add("Phone", item.ReceiverMobile);
                    }
                    dts.Rows.Add("Email", item.ReceiverEmail);
                    dts.Rows.Add("Specility", item.Specility);
                    int EndRecId = Convert.ToInt32(item.ReceiverId) + 999;
                    int EncrPatientId = item.PatientId + 999;
                    string EncrptedString = Convert.ToString(EncrPatientId) + '|' + Convert.ToString(EndRecId);
                    string Urllink = ConfigurationManager.AppSettings["OneClickreferralURL"] + "AccountSettings/GetPatientReferralForm?TYHJNABGA=" + EncrptedString;
                    dynamicMessage = "Thanks for coming in today. We’ve referred you to " + item.ReceiverName + ". They’ll take great care of you and will reach out to you soon. You can also reach them at  " + item.ReceiverPhone + ". Please click below to fill out some additional info." + Urllink;
                    if (!string.IsNullOrWhiteSpace(item.PatientPhone))
                    {
                        try
                        {
                            string toPhoneNumber = Convert.ToString(item.PatientPhone.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""));
                            new SendSMS().SendCustomeSMS(item.SenderId, item.PatientId, dynamicMessage, CountryCode + toPhoneNumber, "DENTIST", "PATIENT");
                        }
                        catch (Exception Ex)
                        {
                            new clsCommon().InsertErrorLog("clsTemplate---SendEmailForOneClickReferral", Ex.Message, Ex.StackTrace);
                        }

                    }
                }
                else if (type == BO.Enums.Common.ReferralType.REFERRED)
                {
                    //Changes for New Mail Templete for Receiver
                    objCustomeMailPropery.ToMailAddress = new List<string> { item.ReceiverEmail };
                    dts.Rows.Add("SenderName", item.SenderName);
                    dts.Rows.Add("PatientName", item.PatientName);
                    dts.Rows.Add("PatientPhone", item.PatientPhone);
                    dts.Rows.Add("SenderPhone", item.SenderPhone);
                    dts.Rows.Add("SenderEmail", item.SenderEmail);
                    dts.Rows.Add("SenderSpecility", item.SenderSpecility);
                    dts.Rows.Add("PatientPhone", item.PatientPhone);
                    //string my = "You have received a One Click Referral from Dr. Lauri Turner for Olivia at (480)999-3342 Please login for more information www.OneClickReferral.com";
                    dynamicMessage = "You have received a One Click Referral from " + item.SenderName + " for " + item.PatientName + " at " + item.PatientPhone + ". Please login for more information www.OneClickReferral.com";

                    //XQ1-262 : creating dynamic contact message to include sender's email and phone.  
                    var dynamicContactMessage = "";
                    if (!string.IsNullOrEmpty(item.SenderPhone) || !string.IsNullOrEmpty(item.SenderEmail))
                        dynamicContactMessage += "Please ";
                    if (!string.IsNullOrEmpty(item.SenderPhone))
                        dynamicContactMessage += "give their office a call at " + item.SenderPhone + "<br>Or ";
                    if (!string.IsNullOrEmpty(item.SenderEmail))
                        dynamicContactMessage += "email their office at " + item.SenderEmail;
                    dts.Rows.Add("ContactMessage", dynamicContactMessage);

                    if (!string.IsNullOrWhiteSpace(item.ReceiverMobile))
                    {
                        try
                        {
                            string toWorkPhone = Convert.ToString(item.ReceiverMobile.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""));
                            new SendSMS().SendCustomeSMS(item.SenderId, item.ReceiverId, dynamicMessage, CountryCode + toWorkPhone, "DENTIST", "DENTIST");
                        }
                        catch (Exception Ex)
                        {
                            new clsCommon().InsertErrorLog("clsTemplate---SendEmailForOneClickReferral", Ex.Message, Ex.StackTrace);
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(item.ReceiverPhone))
                    {
                        try
                        {
                            string toWorkPhone = Convert.ToString(item.ReceiverPhone.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""));
                            new SendSMS().SendCustomeSMS(item.SenderId, item.ReceiverId, dynamicMessage, CountryCode + toWorkPhone, "DENTIST", "DENTIST");
                        }
                        catch (Exception Ex)
                        {
                            new clsCommon().InsertErrorLog("clsTemplate---SendEmailForOneClickReferral", Ex.Message, Ex.StackTrace);
                        }
                    }
                }
                //Changes for New Mail Templete for Sender/Receiver
                if (type == BO.Enums.Common.ReferralType.REFERRED)
                {
                    objCustomeMailPropery.MailTemplateName = "one-click-referral-receiver-dentist";
                }
                else if (type == BO.Enums.Common.ReferralType.SENDER)
                {
                    objCustomeMailPropery.MailTemplateName = "one-click-referral-sender-dentist";
                }
                else
                {
                    objCustomeMailPropery.MailTemplateName = "one-click-referral";
                }
                dts = ObjCompany.GetEmailSubjectForTemplate(dts);
                dts = ObjCompany.GetEmailBodyForTemplate(dts);
                objCustomeMailPropery.MergeTags = dts;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, item.SenderId);

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsTemplate---SendEmailForOneClickReferral", Ex.Message, Ex.StackTrace);
            }
            return true;
        }

        public bool SendZiftPayEmail(ZPTransactionResponse obj, int patientid, int ZPAccountID, int DoctorId)
        {
            try
            {
                DataSet dtFromPatientDetails;
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = patientid;
                //Passing Doctor ID to Patient Hisotry  SP
                strParameter[1] = new SqlParameter("@Doctorid", SqlDbType.Int);
                strParameter[1].Value = DoctorId;
                dtFromPatientDetails = ObjHelper.GetDatasetData("USP_GetPateintDetailsByPatientId", strParameter);

                string FullName = string.Empty; string Email = string.Empty; string Phone = string.Empty;
                if (dtFromPatientDetails != null && dtFromPatientDetails.Tables[0].Rows.Count > 0 && dtFromPatientDetails.Tables[0].Rows[0]["Email"] != null)
                {
                    FullName = ObjCommon.CheckNull(Convert.ToString(dtFromPatientDetails.Tables[0].Rows[0]["FullName"]), string.Empty);

                    Email = ObjCommon.CheckNull(Convert.ToString(dtFromPatientDetails.Tables[0].Rows[0]["Email"]), string.Empty);
                    Phone = ObjCommon.CheckNull(Convert.ToString(dtFromPatientDetails.Tables[0].Rows[0]["Phone"]), string.Empty);
                    if (!string.IsNullOrEmpty(Phone))
                    {
                        SendSMS(Phone, "");
                    }
                }

                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                DataTable SecDt = ObjCommon.GetPatientSecondaryEmail(Email);
                if (SecDt.Rows != null && SecDt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(SecDt.Rows[0]["SecondaryEmail"].ToString()))
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { Email, SecDt.Rows[0]["SecondaryEmail"].ToString() };
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                    }
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                }
                //objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");

                objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "zift-payment-successfully";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("CompanyName", "Patient Pay Hub");
                dt.Rows.Add("FullName", FullName);
                dt.Rows.Add("Amount", obj.originalAmount + "(USD)");
                dt.Rows.Add("CardNumber", obj.accountNumberMasked);
                dt.Rows.Add("CardType", obj.accountType);
                dt.Rows.Add("TransactionId", obj.transactionId);


                //dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                //dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);


                //Send email to admin

                DataSet dtFromPatientDetailsAdmin;
                SqlParameter[] strParameterAdmin = new SqlParameter[2];
                strParameterAdmin[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameterAdmin[0].Value = patientid;
                //Passing Doctor ID to  Patient Hisotry  SP
                strParameterAdmin[1] = new SqlParameter("@Doctorid", SqlDbType.Int);
                strParameterAdmin[1].Value = DoctorId;

                dtFromPatientDetailsAdmin = ObjHelper.GetDatasetData("USP_GetPateintDetailsByPatientId", strParameterAdmin);

                string fullname = string.Empty; string email = string.Empty; string phone = string.Empty;
                if (dtFromPatientDetails != null && dtFromPatientDetails.Tables[0].Rows.Count > 0 && dtFromPatientDetails.Tables[0].Rows[0]["Email"] != null)
                {
                    fullname = ObjCommon.CheckNull(Convert.ToString(dtFromPatientDetails.Tables[0].Rows[0]["FullName"]), string.Empty);

                    email = ObjCommon.CheckNull(Convert.ToString(dtFromPatientDetails.Tables[0].Rows[0]["Email"]), string.Empty);
                    phone = ObjCommon.CheckNull(Convert.ToString(dtFromPatientDetails.Tables[0].Rows[0]["Phone"]), string.Empty);
                    if (!string.IsNullOrEmpty(Phone))
                    {
                        SendSMS(Phone, "");
                    }
                }


                CustomeMailPropery objCustomeMailadmin = new CustomeMailPropery();
                objCustomeMailadmin.ToMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("emailoftravis") };
                objCustomeMailadmin.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailadmin.MailTemplateName = "email-ziftpay-admin";

                DataTable dtadmin = new DataTable();
                dtadmin.Columns.Add("FieldName");
                dtadmin.Columns.Add("FieldValue");

                dtadmin.Rows.Add("CompanyName", "Patient Pay Hub");
                dtadmin.Rows.Add("FullName", "Travis");
                dtadmin.Rows.Add("PatientName", fullname);
                dtadmin.Rows.Add("PatientEmail", email);
                dtadmin.Rows.Add("PatientPhone", phone);

                objCustomeMailadmin.MergeTags = dtadmin;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailadmin, 0);


                //Send email to Doctor/Merchant                
                DataSet dtFromDoctorDetails;
                SqlParameter[] strParameters = new SqlParameter[1];
                strParameters[0] = new SqlParameter("@AccountId", SqlDbType.Int);
                strParameters[0].Value = ZPAccountID;
                dtFromDoctorDetails = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_AccountId", strParameters);

                string DoctorName = string.Empty; string DoctorEmail = string.Empty; string DoctorPhone = string.Empty;
                if (dtFromDoctorDetails != null && dtFromDoctorDetails.Tables.Count > 0 && dtFromDoctorDetails.Tables[0].Rows[0]["Username"] != null)
                {
                    DoctorName = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[0].Rows[0]["FullName"]), string.Empty);

                    DoctorEmail = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[0].Rows[0]["Username"]), string.Empty);

                    DoctorPhone = ObjCommon.CheckNull(Convert.ToString(dtFromDoctorDetails.Tables[0].Rows[0]["Phone"]), string.Empty);

                    if (!string.IsNullOrEmpty(DoctorPhone))
                    {
                        SendSMS(DoctorPhone, "");
                    }
                }

                CustomeMailPropery objCustomeMails = new CustomeMailPropery();
                objCustomeMails.ToMailAddress = new List<string> { DoctorEmail };
                objCustomeMails.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMails.MailTemplateName = "email-ziftpay-admin";

                DataTable dtdoc = new DataTable();
                dtdoc.Columns.Add("FieldName");
                dtdoc.Columns.Add("FieldValue");

                dtdoc.Rows.Add("CompanyName", "Patient Pay Hub");
                dtdoc.Rows.Add("FullName", DoctorName);
                dtdoc.Rows.Add("PatientName", FullName);
                dtdoc.Rows.Add("PatientEmail", Email);
                dtdoc.Rows.Add("PatientPhone", Phone);

                objCustomeMails.MergeTags = dtdoc;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMails, 0);

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SendMerchantEmail(MerchantResponse obj, string userid)
        {
            try
            {
                DataSet dtFromPatientDetails;
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[0].Value = Convert.ToInt32(userid);
                dtFromPatientDetails = ObjHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", strParameter);

                string FullName = string.Empty; string Email = string.Empty; string Phone = string.Empty;
                if (dtFromPatientDetails != null && dtFromPatientDetails.Tables[0].Rows.Count > 0 && dtFromPatientDetails.Tables[0].Rows[0]["Username"] != null)
                {
                    FullName = ObjCommon.CheckNull(Convert.ToString(dtFromPatientDetails.Tables[0].Rows[0]["FullName"]), string.Empty);

                    Email = ObjCommon.CheckNull(Convert.ToString(dtFromPatientDetails.Tables[0].Rows[0]["Username"]), string.Empty);


                }
                if (dtFromPatientDetails != null && dtFromPatientDetails.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow item in dtFromPatientDetails.Tables[2].Rows)
                    {
                        if (Convert.ToString(item["ContactType"]) == "1")
                        {
                            Phone = ObjCommon.CheckNull(Convert.ToString(item["Phone"]), string.Empty);
                        }
                    }
                    if (!string.IsNullOrEmpty(Phone))
                    {
                        SendSMS(Phone, "");
                    }
                }
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                DataTable SecDt = ObjCommon.GetPatientSecondaryEmail(Email);
                if (SecDt.Rows != null && SecDt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(SecDt.Rows[0]["SecondaryEmail"].ToString()))
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { Email, SecDt.Rows[0]["SecondaryEmail"].ToString() };
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                    }
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                }
                //objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");

                objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("emailoftravis") };
                objCustomeMailPropery.MailTemplateName = "zift-merchant-successfully";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("CompanyName", "Patient Pay Hub");
                dt.Rows.Add("FullName", FullName);
                dt.Rows.Add("MerchantId", obj.merchantId);
                dt.Rows.Add("AccountId", obj.accountId);

                //dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                //dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);

                return true;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void SendSMS(string Phone, string Message)
        {
            try
            {
                //string AccountSid = ConfigurationSettings.AppSettings["TwillioAccountSid"];
                //string AuthToken = ConfigurationSettings.AppSettings["TwillioAuthToken"];
                //string TwillioPhone = ConfigurationSettings.AppSettings["TwillioPhone"];
                //string CountryCode = ConfigurationSettings.AppSettings["CountryCode"];
                //TwilioClient.Init(AccountSid, AuthToken);
                //var message = MessageResource.Create(
                //                to: new PhoneNumber(CountryCode + Phone),
                //                from: new PhoneNumber(TwillioPhone),
                //                body: Message);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool SendFormToPatientEmail(int PatientId, int DoctorId, string Email, string Name, string PatientName)
        {
            try
            {
                //string UrlLink = ConfigurationManager.AppSettings["MyDentalFilePatientFormURL"] + "PatientForm/Fill?TYHJNABGA=" + (PatientId + 999) + "|" + (DoctorId + 999) + "|" + 1;
                string UrlLink = ConfigurationManager.AppSettings["MyDentalFilePatientFormURL"] + (PatientId + 999) + "|" + (DoctorId + 999) + "|" + 1;
                DataTable dts = new DataTable();
                dts.Columns.Add("FieldName");
                dts.Columns.Add("FieldValue");
                dts.Rows.Add("PageUrl", UrlLink);
                dts.Rows.Add("CompanyName", "Recordlinc");
                dts.Rows.Add("DoctorName", Name);
                dts.Rows.Add("PatientName", PatientName);
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                objCustomeMailPropery.MailTemplateName = "patient-form-sendto-patient";
                objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                objCustomeMailPropery.subject = "Mail for patient form";
                objCustomeMailPropery.MergeTags = dts;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
                return true;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsTemplate---SendFormToPatientEmail", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public bool SendFormToDoctorEmail(int PatientId, int DoctorId, string PatientName)
        {
            try
            {
                DataTable dt = new clsColleaguesData().GetDoctorDetailsbyId(DoctorId);
                if (dt != null)
                {

                    string UrlLink = ConfigurationManager.AppSettings["OneClickreferralURL"] + "Patients/PatientHistory?PatientId=" + PatientId;
                    DataTable dts = new DataTable();
                    dts.Columns.Add("FieldName");
                    dts.Columns.Add("FieldValue");
                    dts.Rows.Add("PageUrl", UrlLink);
                    dts.Rows.Add("CompanyName", "Recordlinc");
                    dts.Rows.Add("PatientName", PatientName);
                    dts.Rows.Add("DoctorName", (Convert.ToString(dt.Rows[0]["FirstName"]) + ' ' + Convert.ToString(dt.Rows[0]["LastName"])).Trim());
                    CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                    objCustomeMailPropery.MailTemplateName = "send-doctorto-patient-form";
                    objCustomeMailPropery.ToMailAddress = new List<string> { Convert.ToString(dt.Rows[0]["Username"]), Convert.ToString(dt.Rows[0]["SecondaryEmail"]) };
                    objCustomeMailPropery.subject = "Mail for patient form";
                    objCustomeMailPropery.MergeTags = dts;
                    SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsTemplate---SendFormToPatientEmail", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public bool SendRequestPaymentEmail(int PatientId, int DoctorId, string Amount)
        {
            try
            {
                DataTable dt = new clsColleaguesData().GetDoctorDetailsbyId(DoctorId);
                clsPatientsData objpatientdetail = new clsPatientsData();
                DataSet ds = objpatientdetail.GetPateintDetailsByPatientId(PatientId, DoctorId);

                if (dt != null && ds != null)
                {
                    TripleDESCryptoHelper cryptoHelper = new TripleDESCryptoHelper();
                    string UrlLink = ConfigurationManager.AppSettings["websiteurl"] + "patientlogin?username=" + Convert.ToString(ds.Tables[0].Rows[0]["Email"]) + "&pass=" + cryptoHelper.encryptText(Convert.ToString(ds.Tables[0].Rows[0]["ExactPassword"])) + "&ispay=" + 1 + "&doctorid=" + DoctorId + "&amount=" + Amount;
                    DataTable dts = new DataTable();
                    dts.Columns.Add("FieldName");
                    dts.Columns.Add("FieldValue");
                    dts.Rows.Add("PageUrl", UrlLink);
                    dts.Rows.Add("CompanyName", "Recordlinc");
                    dts.Rows.Add("PatientName", (Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]) + ' ' + Convert.ToString(ds.Tables[0].Rows[0]["LastName"]).Trim()));
                    dts.Rows.Add("DoctorName", (Convert.ToString(dt.Rows[0]["FirstName"]) + ' ' + Convert.ToString(dt.Rows[0]["LastName"])).Trim());
                    dts.Rows.Add("Amount", "$" + Amount);
                    CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                    objCustomeMailPropery.MailTemplateName = "request-payment-patient";
                    objCustomeMailPropery.ToMailAddress = new List<string> { Convert.ToString(ds.Tables[0].Rows[0]["Email"]) };
                    objCustomeMailPropery.subject = "Mail for request payment";
                    objCustomeMailPropery.MergeTags = dts;
                    SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsTemplate---SendRequestPaymentEmail", Ex.Message, Ex.StackTrace);
                throw;
            }
        }



        public bool RequestReviewsToPatientEmail(int PatientId, string Email, string PatientFirstName, string PatientLastName, string PublicProfile, string UserFirstName, string UserLastName)
        {
            try
            {
                ////string UrlLink = ConfigurationManager.AppSettings["OneClickreferralURL"] + "AccountSettings/GetPatientReferralForm?TYHJNABGA=" + (PatientId + 999);
                ////string UrlLink = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/p/" + PublicProfile + "/reviews";
                ////string UrlLink = ConfigurationManager.AppSettings["OneClickreferralURL"] + "p/test?PatientId=" + (PatientId);

                //string UrlLink = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/p/RequestReview?PatientId=" + PatientId+"&UserName=" + PublicProfile;

                ////string UrlLink = ConfigurationManager.AppSettings["OneClickreferralURL"] + "/p/" + "RequetReview?PatientId=" + PatientId;
                //DataTable dts = new DataTable();
                //dts.Columns.Add("FieldName");
                //dts.Columns.Add("FieldValue");               

                //dts.Rows.Add("PageUrl", UrlLink);
                //dts.Rows.Add("CompanyName", "Recordlinc");
                //dts.Rows.Add("UserName", UserFirstName + " " + UserLastName);
                //dts.Rows.Add("PatientName", PatientFirstName + " " + PatientLastName);

                //CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                //objCustomeMailPropery.MailTemplateName = "patient-request-review";
                //objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                //objCustomeMailPropery.subject = "Mail for Request Review";
                //objCustomeMailPropery.MergeTags = dts;
                //SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
                //return true;


                //string UrlLink = ConfigurationManager.AppSettings["OneClickreferralURL"] + "AccountSettings/GetPatientReferralForm?TYHJNABGA=" + (PatientId + 999);
                //string UrlLink = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/p/" + PublicProfile + "/reviews";
                //string UrlLink = ConfigurationManager.AppSettings["OneClickreferralURL"] + "p/test?PatientId=" + (PatientId);

                string UrlLink = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/p/" + PublicProfile + "/RequestReview?PatientId=" + PatientId;

                //string UrlLink = ConfigurationManager.AppSettings["OneClickreferralURL"] + "/p/" + "RequetReview?PatientId=" + PatientId;
                DataTable dts = new DataTable();
                dts.Columns.Add("FieldName");
                dts.Columns.Add("FieldValue");

                dts.Rows.Add("PageUrl", UrlLink);
                dts.Rows.Add("CompanyName", "Recordlinc");
                dts.Rows.Add("UserName", UserFirstName + " " + UserLastName);
                dts.Rows.Add("PatientName", PatientFirstName + " " + PatientLastName);

                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                objCustomeMailPropery.MailTemplateName = "patient-request-review";
                objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                objCustomeMailPropery.subject = "Mail for Request Review";
                objCustomeMailPropery.MergeTags = dts;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
                return true;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsTemplate---RequestReviewsToPatientEmail", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public bool SendEmailForDispositionStatusChange(MailContain mail)
        {
            try
            {
                string CountryCode = ConfigurationManager.AppSettings.Get("CountryCode");
                string dynamicMessage = "";
                #region Sender Mail and SMS send portion
                DataTable dts = new DataTable();
                dts.Columns.Add("FieldName");
                dts.Columns.Add("FieldValue");
                dts.Rows.Add("SenderDoctor", mail.SendarName);
                dts.Rows.Add("ReceiverDoctor", mail.ReceiverName);
                dts.Rows.Add("DispositionStatus", mail.Status);
                dts.Rows.Add("URL", mail.URL);
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                objCustomeMailPropery.MailTemplateName = "change-disposition-status";
                objCustomeMailPropery.ToMailAddress = new List<string> { mail.SendarEmail };
                objCustomeMailPropery.MergeTags = dts;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, mail.ReceiverId);
                dynamicMessage = $"Dr {mail.SendarName} - be advised that your referral to Dr {mail.ReceiverName} has moved to a status of {mail.Status}. Please click on " + mail.URL + " to review the details of this referral";
                try
                {
                    if (!string.IsNullOrWhiteSpace(mail.SendarPhone))
                    {
                        string toSenderPhone = Convert.ToString(mail.SendarPhone.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""));
                        new SendSMS().SendCustomeSMS(mail.SendarId, mail.ReceiverId, dynamicMessage, CountryCode + toSenderPhone, "DENTIST", "DENTIST");
                    }
                }
                catch (Exception Ex)
                {
                    new clsCommon().InsertErrorLog("clsTemplate---Sender Mail and SMS send portion", Ex.Message, Ex.StackTrace);

                }
                #endregion

                #region Patient Mail and SMS send portion
                DataTable dt = new DataTable();
                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");
                dt.Rows.Add("SenderDoctor", mail.SendarName);
                dt.Rows.Add("ReceiverDoctor", mail.ReceiverName);
                dt.Rows.Add("DispositionStatus", mail.Status);
                dt.Rows.Add("PatientName", mail.PatientName);
                dt.Rows.Add("URL", mail.URL);
                CustomeMailPropery objCustomeMailProperys = new CustomeMailPropery();
                objCustomeMailProperys.MailTemplateName = "change-disposition-status-patient";
                objCustomeMailProperys.ToMailAddress = new List<string> { mail.PatientEmail };
                objCustomeMailProperys.MergeTags = dt;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailProperys, mail.ReceiverId);
                dynamicMessage = $"{mail.PatientName} - be advised that you are referred to Dr {mail.ReceiverName} has changed the referral status to {mail.Status}.";
                try
                {
                    if (!string.IsNullOrWhiteSpace(mail.PatientPhone))
                    {
                        string toSenderPhone = Convert.ToString(mail.PatientPhone.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""));
                        new SendSMS().SendCustomeSMS(mail.SendarId, mail.PatientId, dynamicMessage, CountryCode + toSenderPhone, "DENTIST", "PATIENT");
                    }
                }
                catch (Exception Ex)
                {
                    new clsCommon().InsertErrorLog("clsTemplate---SendSMSForDispositionStatusChange", Ex.Message, Ex.StackTrace);
                }
                #endregion

                return true;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsTemplate---Patient Mail and SMS send portion", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public bool SendEmailForDispositionStatusToScheduled(MailContain mail,string strNote, string strScheduledDate, bool VisibleToPatient)
        {
            try
            {
                string CountryCode = ConfigurationManager.AppSettings.Get("CountryCode");
                string dynamicMessage = "";
                string strSocialLink = "",strEmailNote="";
                string strRecordLincUrl = ConfigurationManager.AppSettings.Get("WebSiteURL");
                string strImagePath = strRecordLincUrl + "Images/";
                if (!string.IsNullOrWhiteSpace(mail.FacebookUrl))
                {
                    strSocialLink += $"<a href='{mail.FacebookUrl}' target='_blank'><img src='{strImagePath}facebook.png' alt ='Facebook'/></a>";       
                }
                if (!string.IsNullOrWhiteSpace(mail.LinkedinUrl))
                {
                    strSocialLink += $"<a href='{mail.LinkedinUrl}' target='_blank'><img src='{strImagePath}linkedin.png' alt='Linkedin'/></a>";
                }
                if (!string.IsNullOrWhiteSpace(mail.TwitterUrl))
                {
                    strSocialLink += $"<a href='{mail.TwitterUrl}' target='_blank'><img src='{strImagePath}twitter.png' alt='Twitter'/></a>";
                }
                if (!string.IsNullOrWhiteSpace(mail.GoogleplusUrl))
                {
                    strSocialLink += $"<a href='{mail.GoogleplusUrl}' target='_blank'><img src='{strImagePath}googleplus.png' alt='Googleplus'/></a>";
                }
                if (!string.IsNullOrWhiteSpace(mail.YoutubeUrl))
                {
                    strSocialLink += $"<a href='{mail.YoutubeUrl}' target='_blank'><img src='{strImagePath}youtube.png' alt='Youtube'/></a>";
                }
                if (!string.IsNullOrWhiteSpace(mail.PinterestUrl))
                {
                    strSocialLink += $"<a href='{mail.PinterestUrl}' target ='_blank'><img src='{strImagePath}pinterest.png' alt='Pinterest'/></a>";
                }
                if (!string.IsNullOrWhiteSpace(mail.BlogUrl))
                {
                    strSocialLink += $"<a href='{mail.BlogUrl}' target='_blank'><img src='{strImagePath}blog.png' alt='Blog'/></a>";
                }
                if (!string.IsNullOrWhiteSpace(mail.YelpUrl))
                {
                    strSocialLink += $"<a href='{mail.YelpUrl}' target='_blank'><img src='{strImagePath}linkedin.png' alt='Yelp'/></a>";
                }
                if (!string.IsNullOrWhiteSpace(mail.InstagramUrl))
                {
                    strSocialLink += $"<a href ='{mail.InstagramUrl}' target='_blank'><img src='{strImagePath}linkedin.png' alt ='Instagram'/></a>";
                }

                if (VisibleToPatient)
                {
                    strEmailNote = $"(Note : {strNote})";
                }


                #region Sender Mail and SMS send portion

                DataTable dts = new DataTable();
                dts.Columns.Add("FieldName");
                dts.Columns.Add("FieldValue");
                dts.Rows.Add("SenderDoctor", mail.SendarName);
                dts.Rows.Add("ReceiverDoctor", mail.ReceiverName);
                dts.Rows.Add("DispositionStatus", mail.Status);
                dts.Rows.Add("URL", mail.URL);
                //dts.Rows.Add("PhoneNumber", mail.ReceiverPhone);
                //dts.Rows.Add("Address", mail.Address);

                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                objCustomeMailPropery.MailTemplateName = "change-disposition-status";
                objCustomeMailPropery.ToMailAddress = new List<string> { mail.SendarEmail };
                objCustomeMailPropery.MergeTags = dts;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, mail.ReceiverId);
                dynamicMessage = $"Dr {mail.SendarName} - be advised that your referral to Dr {mail.ReceiverName} has moved to a status of {mail.Status}. Please click on " + mail.URL + " to review the details of this referral";
                try
                {
                    if (!string.IsNullOrWhiteSpace(mail.SendarPhone))
                    {
                        string toSenderPhone = Convert.ToString(mail.SendarPhone.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""));
                        new SendSMS().SendCustomeSMS(mail.SendarId, mail.ReceiverId, dynamicMessage, CountryCode + toSenderPhone, "DENTIST", "DENTIST");
                    }
                }
                catch (Exception Ex)
                {
                    new clsCommon().InsertErrorLog("clsTemplate---Sender Mail and SMS send portion", Ex.Message, Ex.StackTrace);

                }
                #endregion

                #region Patient Mail and SMS send portion
                DataTable dt = new DataTable();
                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");
                dt.Rows.Add("SenderDoctor", mail.SendarName);
                dt.Rows.Add("ReceiverDoctor", mail.ReceiverName);
                dt.Rows.Add("DispositionStatus", mail.Status);
                dt.Rows.Add("PatientName", mail.PatientName);
                dt.Rows.Add("Address", mail.Address);
                dt.Rows.Add("PhoneNumber", mail.ReceiverPhone);
                dt.Rows.Add("SocialLink", strSocialLink);
                dt.Rows.Add("Notes", strEmailNote);
                dt.Rows.Add("ScheduleDate", strScheduledDate);
                dt.Rows.Add("URL", mail.URL);
                CustomeMailPropery objCustomeMailProperys = new CustomeMailPropery();
                objCustomeMailProperys.MailTemplateName = "change-disposition-status-to-scheduled-patient";
                objCustomeMailProperys.ToMailAddress = new List<string> { mail.PatientEmail };
                objCustomeMailProperys.MergeTags = dt;
                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailProperys, mail.ReceiverId);
                //dynamicMessage = $"{mail.PatientName} - be advised that you are referred to Dr {mail.ReceiverName} has changed the referral status to {mail.Status}.";
                //try
                //{
                //    if (!string.IsNullOrWhiteSpace(mail.PatientPhone))
                //    {
                //        string toSenderPhone = Convert.ToString(mail.PatientPhone.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", ""));
                //        new SendSMS().SendCustomeSMS(mail.SendarId, mail.PatientId, dynamicMessage, CountryCode + toSenderPhone, "DENTIST", "PATIENT");
                //    }
                //}
                //catch (Exception Ex)
                //{
                //    new clsCommon().InsertErrorLog("clsTemplate---SendSMSForDispositionStatusChange", Ex.Message, Ex.StackTrace);
                //}
                #endregion

                return true;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsTemplate---Patient Mail and SMS send portion", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        #region Recover Password From Verident
        public void SendForgetPassword(string DentistFullName,string ResetPasswordLink, string DoctorEmail,string CompanyWebsite)
        {
            try
            {
                string EncUsername = string.Empty;
               EncUsername = ObjTripleDESCryptoHelper.encryptText(DoctorEmail);                
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();                
                DataTable SecDt = ObjCommon.GetDoctorSecondaryEmail(DoctorEmail);
                if (SecDt.Rows != null && SecDt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(SecDt.Rows[0]["SecondaryEmail"].ToString()))
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail, SecDt.Rows[0]["SecondaryEmail"].ToString() };
                    }
                    else
                    {
                        objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                    }
                }
                else
                {
                    objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                }

                //objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "recover-password-from-verident";

                DataTable dt = new DataTable();
                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");
                dt.Rows.Add("Name", DentistFullName);
                dt.Rows.Add("ResetPasswordlink", ResetPasswordLink);
                dt.Rows.Add("ToMail", EncUsername);
                dt.Rows.Add("VeridentWebsite", CompanyWebsite);
                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("recover-password-from-admin", ex.Message, ex.StackTrace);
            }
        }
        #endregion



        #region Template Name:Welcome To Verident
        public void NewUserEmail(string Email, string loginlink, string password, string companywebsite)
        {
            try
            {
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                // objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");

                objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "welcome-to-company";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Doctor", Email);
                dt.Rows.Add("Email", Email);
                dt.Rows.Add("Password", password);
                dt.Rows.Add("newloginlink", loginlink);
                dt.Rows.Add("CompanyWebsite", companywebsite);

                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("welcome-to-company", ex.Message, ex.StackTrace);
            }
        }
        #endregion

        public void NewUserSendEmailOrPassword(string FullName,string Email, string Password,string VeridentLink)
        {
            try
            {
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                // objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");

                objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "Send-Username-Password-Verident";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("DoctorName", FullName);
                dt.Rows.Add("Email", Email);
                dt.Rows.Add("Password", Password);
                dt.Rows.Add("VeridentLink", VeridentLink);
                dt = ObjCompany.GetEmailSubjectForTemplate(dt);
                dt = ObjCompany.GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("welcome-to-company", ex.Message, ex.StackTrace);
            }
        }
    }
}
