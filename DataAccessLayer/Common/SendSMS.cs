﻿using System;
using BO.Models;
using System.Configuration;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;
using System.Net.Http;
using System.Net;
using System.Collections.Generic;
using System.Net.Http.Formatting;
using Newtonsoft.Json;
using RestSharp;
using System.Data.SqlClient;

namespace DataAccessLayer.Common
{
    public class SendSMS
    {
        public static string ACCOUNTSID = ConfigurationManager.AppSettings.Get("TwillioAccountSid");
        public static string AUTHTOKEN = ConfigurationManager.AppSettings.Get("TwillioAuthToken");
        public static string TWILLIOPHONE = ConfigurationManager.AppSettings.Get("TwillioPhone");
        public static string COUNTRYCODE = ConfigurationManager.AppSettings.Get("CountryCode");
        public static string SESSION_KEY = ConfigurationManager.AppSettings.Get("ZIPWHIP_SESSIONKEY");
        public static string ZIPWHIP_URL = ConfigurationManager.AppSettings.Get("ZIPWHIP_MESSAGE_SEND_URL");

        public bool SendCustomeSMS(int SenderId, int RecevierId, string Message, string PhoneNumber, string SenderType, string ReceiverType,int MessageType = 1)
        {
            clsCommon objCommon = new clsCommon();
            try
            {
                return InsertSMSHistory(new SMSDetails()
                {
                    SenderId = SenderId,
                    ReceiverId = RecevierId,
                    MessageBody = Message,
                    RecepientNumber = PhoneNumber,
                    ReceiverType = ReceiverType,
                    SenderType = SenderType,
                    MessageType = MessageType
                });
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("SendSMS--SendCustomeSMS", Ex.Message, Ex.StackTrace);
                return false;
            }
        }
        public static bool InsertSMSHistory(SMSDetails Obj)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                    {
                    new SqlParameter{ParameterName="@Id",Value=Obj.Id},
                    new SqlParameter{ParameterName="@SenderId",Value=Obj.SenderId},
                    new SqlParameter{ParameterName="@SenderType",Value=Obj.SenderType},
                    new SqlParameter{ParameterName="@ReceiverId",Value=Obj.ReceiverId},
                    new SqlParameter{ParameterName="@ReceiverType",Value=Obj.ReceiverType},
                    new SqlParameter{ParameterName="@RecepientNumber",Value=Obj.RecepientNumber},
                    new SqlParameter{ParameterName="@MessageBody",Value=Obj.MessageBody},
                    new SqlParameter{ParameterName="@Status",Value=Obj.Status},
                    new SqlParameter{ParameterName="@MessageType",Value=Obj.MessageType}
                    };
                return new clsHelper().ExecuteNonQuery("Usp_InsertUpdateSMSHistory", sqlpara);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("SendSMS--InsertSMSHistory", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
