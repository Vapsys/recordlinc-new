﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.Configuration;

namespace DataAccessLayer.Common
{
    public class clsMandrillTemplate
    {
        public static List<string> GetMandrillTemplate(string templateName)
        {
            List<string> strTemplate = new List<string>();
            //HttpClient client = new HttpClient(new HttpClientHandler { UseProxy = false });
            string strUri = "https://mandrillapp.com/api/1.0/templates/info.json";
            //client.BaseAddress = new Uri(strUri);
            //client.DefaultRequestHeaders.Accept.Clear();
            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            TemplateRequestCls objtemp = new TemplateRequestCls();
            //objtemp.key = "Q0XKdMYMNHGqoysh_tioSg";

            objtemp.key = ConfigurationManager.AppSettings["mandrillapikey"];
            objtemp.name = templateName;            
            string jsonData = JsonConvert.SerializeObject(objtemp);
            try
            {
                var webRequest = WebRequest.Create(strUri);
                if (webRequest != null)
                {
                    webRequest.Method = "POST";
                    webRequest.Timeout = 20000;
                    webRequest.ContentType = "application/json";

                    using (System.IO.Stream s = webRequest.GetRequestStream())
                    {
                        using (System.IO.StreamWriter sw = new System.IO.StreamWriter(s))
                            sw.Write(jsonData);
                    }

                    using (System.IO.Stream s = webRequest.GetResponse().GetResponseStream())
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(s))
                        {
                            var jsonResponse = sr.ReadToEnd();
                            TemplateResponsecls obj = JsonConvert.DeserializeObject<TemplateResponsecls>(jsonResponse);
                            strTemplate.Add(obj.publish_code.Replace('\r', ' ').Replace('\n', ' ').Replace('\t', ' '));
                            strTemplate.Add(obj.subject);
                            return strTemplate;
                        }
                    }
                }
                else
                {
                    return strTemplate;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            //StringContent content = new StringContent(JsonConvert.SerializeObject(objtemp), Encoding.UTF8, "application/json");            
            //HttpResponseMessage responseMessage = await client.PostAsync(strUri, content);
            //if (responseMessage.IsSuccessStatusCode)
            //{
            //    var responseData = responseMessage.Content.ReadAsStringAsync().Result;
            //    TemplateResponsecls obj = JsonConvert.DeserializeObject<TemplateResponsecls>(responseData);
            //    obj.publish_code = obj.publish_code.Replace('\r', ' ').Replace('\n', ' ').Replace('\t', ' ');                
            //    strTemplate = obj.publish_code;
            //    return strTemplate;
            //}
            //else
            //{
            //    return strTemplate;
            //}
        }

        public class TemplateRequestCls
        {
            public string key { get; set; }
            public string name { get; set; }
        }

        public class TemplateResponsecls
        {
            public string code { get; set; }
            public string created_at { get; set; }
            public string draft_updated_at { get; set; }
            public string from_email { get; set; }
            public string from_name { get; set; }
            public string[] labels { get; set; }
            public string name { get; set; }
            public string publish_code { get; set; }
            public string publish_from_email { get; set; }
            public string publish_from_name { get; set; }
            public string publish_name { get; set; }
            public string publish_subject { get; set; }
            public string publish_text { get; set; }
            public string publish_at { get; set; }
            public string slug { get; set; }
            public string subject { get; set; }
            public string text { get; set; }
            public string updated_at { get; set; }

        }
    }
}
