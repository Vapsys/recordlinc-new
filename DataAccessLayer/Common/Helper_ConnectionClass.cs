﻿using System.Configuration;
using System.Data.SqlClient;

namespace DataAccessLayer.Common
{
    public class Helper_ConnectionClass
    {
        private SqlConnection objsqlConnection;
        string ConnectionString = null;
        public Helper_ConnectionClass()
        {
            objsqlConnection = null;
            ConnectionString = ConfigurationManager.ConnectionStrings["CONNECTIONSTRING"].ConnectionString;
        }
        public Helper_ConnectionClass(string ConnectionString = null)
        {
            this.ConnectionString = ConnectionString;
            objsqlConnection = null;
        }

        public SqlConnection Open()
        {
            objsqlConnection = new SqlConnection();
            objsqlConnection.ConnectionString = ConnectionString;
            objsqlConnection.Open();
            return objsqlConnection;
        }
        public void Close(SqlConnection con)
        {
            con.Close();
        }
    }
}