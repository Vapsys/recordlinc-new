﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Collections.Specialized;

using System.IO;
using System.Web;
using RestSharp;
using System.Web.Routing;
using System.Web.Mvc;
using MailBee;
using MailBee.SmtpMail;
using MailBee.Security;
using System.Net.Mail;
using System.Net;
using Mandrill.Requests.Messages;
using Mandrill;
using Mandrill.Models;
using Nito.AsyncEx.Synchronous;
using Nito.AsyncEx;

namespace DataAccessLayer.Common
{
    public static class SendCustomeMail
    {
        

        /// <summary>
        /// Send Email
        /// </summary>
        /// <param name="MailType"></param>
        /// <param name="objCustomeMailPropery"></param>
        /// <param name="DoctorId"></param>
        public static void SendMail(Mailtype MailType, CustomeMailPropery objCustomeMailPropery, int DoctorId)
        {
            if (DoctorId != 0)
            {
                var dtt = EmailProviderDAL.EmailProviderDAL.GetAllEmailProviderDetailsList(DoctorId);
                bool IsDefault, IsSendEnabled, IsUse = false;
                CustomeMailPropery ObjOauthMailProperty = new CustomeMailPropery();
                if (dtt != null && dtt.Rows.Count > 0)
                {
                    for (int i = 0; i < dtt.Rows.Count; i++)
                    {
                        IsDefault = Convert.ToBoolean(dtt.Rows[i]["IsDefault"]);
                        IsSendEnabled = Convert.ToBoolean(dtt.Rows[i]["SendEnabled"]);
                        if (IsDefault && IsSendEnabled)
                        {
                            IsUse = true;
                            ObjOauthMailProperty.ProviderId = Convert.ToInt32(dtt.Rows[i]["Provider_Id"]);
                            ObjOauthMailProperty.FromOAuthEmail = Convert.ToString(dtt.Rows[i]["EmailAddress"]);
                            int P_id = ObjOauthMailProperty.ProviderId;
                            if (P_id != (int)BO.Enums.Common.EmailType.GmailOAuth2 && P_id != (int)BO.Enums.Common.EmailType.OutlookOAuth2)
                            {
                                ObjOauthMailProperty.smtpName = Convert.ToString(dtt.Rows[i]["SmtpServerName"]);
                                ObjOauthMailProperty.smtpPort = Convert.ToInt32(dtt.Rows[i]["SmtpPortNumber"]);
                                ObjOauthMailProperty.Password = Convert.ToString(dtt.Rows[i]["Password"]);
                                ObjOauthMailProperty.SSLEnabled = Convert.ToBoolean(dtt.Rows[i]["RequiresSSL"]);
                            }
                            else
                            {
                                ObjOauthMailProperty.OAuthAccessToken = Convert.ToString(dtt.Rows[i]["Accesstoken"]);
                                ObjOauthMailProperty.OAuthRefreshToken = Convert.ToString(dtt.Rows[i]["Refreshtoken"]);
                            }
                            ObjOauthMailProperty.ToMailAddress = objCustomeMailPropery.ToMailAddress;
                            ObjOauthMailProperty.MergeTags = objCustomeMailPropery.MergeTags;
                            ObjOauthMailProperty.MailTemplateName = objCustomeMailPropery.MailTemplateName;
                            break;
                        }
                        else
                        {
                            IsUse = false;
                        }
                    }
                    if (IsUse)
                    {
                        var _mailTemplate = clsMandrillTemplate.GetMandrillTemplate(ObjOauthMailProperty.MailTemplateName);
                        if (_mailTemplate != null && _mailTemplate.Count > 0)
                        {
                            ObjOauthMailProperty.body = Convert.ToString(_mailTemplate[0]);
                            ObjOauthMailProperty.subject = Convert.ToString(_mailTemplate[1]);
                        }

                        foreach (DataRow item in ObjOauthMailProperty.MergeTags.Rows)
                        {
                            var strFieldName = Convert.ToString(item["FieldName"]);
                            ObjOauthMailProperty.body = ObjOauthMailProperty.body.Replace("*|" + strFieldName + "|*", Convert.ToString(item["FieldValue"]));
                            ObjOauthMailProperty.subject = ObjOauthMailProperty.subject.Replace("*|" + strFieldName + "|*", Convert.ToString(item["FieldValue"]));
                        }

                        if (ObjOauthMailProperty.ProviderId == (int)BO.Enums.Common.EmailType.GmailOAuth2 || ObjOauthMailProperty.ProviderId == (int)BO.Enums.Common.EmailType.OutlookOAuth2)
                        {
                            try
                            {
                                Global.LicenseKey = "MN110-E52DD2042C532D302D41A7833516-E31C";
                                Smtp mailer = new Smtp(Global.LicenseKey);
                                mailer.Subject = ObjOauthMailProperty.subject;
                                mailer.BodyHtmlText = ObjOauthMailProperty.body;
                                mailer.From.Email = ObjOauthMailProperty.FromOAuthEmail;
                                mailer.Bcc.Add(Convert.ToString(ConfigurationManager.AppSettings.Get("bccemail")));
                                foreach (var item in ObjOauthMailProperty.ToMailAddress)
                                {
                                    mailer.To.Add(item);
                                }

                                if (ObjOauthMailProperty.ProviderId == (int)BO.Enums.Common.EmailType.GmailOAuth2)
                                {
                                    string clientid = Convert.ToString(ConfigurationManager.AppSettings["google_clientID"]);
                                    string clientsecret = Convert.ToString(ConfigurationManager.AppSettings["google_clientsecret"]);
                                    string redirectUrl = Convert.ToString(ConfigurationManager.AppSettings["google_redirectUri"]);
                                    var str = BO.OAuthHelperClass.GetAccessTokenbyRefresh(ObjOauthMailProperty.OAuthRefreshToken, clientid, clientsecret);

                                    string xoauthKey = OAuth2.GetXOAuthKeyStatic(ObjOauthMailProperty.FromOAuthEmail, str[0]);
                                    mailer.SmtpServers.Add("smtp.gmail.com", null, xoauthKey, AuthenticationMethods.SaslOAuth2);
                                    mailer.SmtpServers[0].SslMode = SslStartupMode.UseStartTls;
                                    mailer.SmtpServers[0].SmtpOptions = ExtendedSmtpOptions.NoChunking;
                                    mailer.Send();
                                }
                                //if (ObjOauthMailProperty.ProviderId == (int)BO.Enums.Common.EmailType.OutlookOAuth2)
                                //{
                                //    string clientid = Convert.ToString(ConfigurationManager.AppSettings["outlook_clientID"]);
                                //    string clientsecret = Convert.ToString(ConfigurationManager.AppSettings["outlook_clientsecret"]);
                                //    string redirectUrl = Convert.ToString(ConfigurationManager.AppSettings["outlook_redirectUri"]);
                                //    //var str = BO.OAuthHelperClass.GetAccessTokenbyRefreshOutlook(ObjOauthMailProperty.OAuthRefreshToken, clientid, redirectUrl, clientsecret);

                                //    //string xoauthKey = OAuth2.GetXOAuthKeyStatic(ObjOauthMailProperty.FromOAuthEmail, str[0]);
                                //    string xoauthKey = OAuth2.GetXOAuthKeyStatic(ObjOauthMailProperty.FromOAuthEmail, ObjOauthMailProperty.OAuthAccessToken);
                                //    mailer.SmtpServers.Add("smtp.office365.com", null, xoauthKey, AuthenticationMethods.SaslOAuth2);
                                //    mailer.SmtpServers[0].SslMode = SslStartupMode.UseStartTls;
                                //    mailer.SmtpServers[0].SmtpOptions = ExtendedSmtpOptions.NoChunking;
                                //    mailer.Send();
                                //}
                            }
                            catch (Exception ex)
                            {
                                clsCommon objCommon = new clsCommon();
                                objCommon.InsertErrorLog("SendMailMethod--Mail sending issue from oauth2", ex.Message, ex.StackTrace);
                            }
                        }
                        else
                        {
                            try
                            {
                                MailMessage mail = new MailMessage();
                                SmtpClient SmtpServer = new SmtpClient(ObjOauthMailProperty.smtpName);

                                mail.From = new MailAddress(ObjOauthMailProperty.FromOAuthEmail);
                                foreach (var item in ObjOauthMailProperty.ToMailAddress)
                                {
                                    mail.To.Add(item);
                                }
                                mail.Subject = ObjOauthMailProperty.subject;
                                mail.Body = ObjOauthMailProperty.body;
                                mail.IsBodyHtml = true;
                                mail.Bcc.Add(Convert.ToString(ConfigurationManager.AppSettings.Get("bccemail")));                                
                                SmtpServer.Port = ObjOauthMailProperty.smtpPort;
                                SmtpServer.UseDefaultCredentials = false;                                
                                SmtpServer.Credentials = new NetworkCredential(ObjOauthMailProperty.FromOAuthEmail, ObjOauthMailProperty.Password);
                                SmtpServer.EnableSsl = ObjOauthMailProperty.SSLEnabled;
                                SmtpServer.Send(mail);
                            }
                            catch (Exception ex)
                            {
                                clsCommon objCommon = new clsCommon();
                                objCommon.InsertErrorLog("SendMailMethod--Mail sending issue from normal smtp", ex.Message, ex.StackTrace);
                            }
                        }
                    }
                }
                else
                {
                    //Send email using mandrill
                    try
                    {
                        string MandrillApiKey = ConfigurationManager.AppSettings["mandrillapikey"];
                        MandrillApi mandrillWrapper = new MandrillApi(MandrillApiKey);

                        #region To Email List
                        List<EmailAddress> toList = new List<EmailAddress>();
                        for (int i = 0; i < objCustomeMailPropery.ToMailAddress.Count; i++)
                        {
                            EmailAddress receiver = new EmailAddress(objCustomeMailPropery.ToMailAddress[i], "");
                            toList.Add(receiver);
                        }
                        #endregion

                        EmailMessage msg = new EmailMessage();
                        msg.To = toList;            // Add the recipients.
                        msg.FromEmail = objCustomeMailPropery.FromMailAddress;
                        msg.FromName = objCustomeMailPropery.FromMailAddress;
                        msg.BccAddress = Convert.ToString(ConfigurationManager.AppSettings.Get("bccemail"));

                        #region Email Attachment
                        if (!string.IsNullOrEmpty(objCustomeMailPropery.attachment))
                        {
                            string vCardname = "VCard";
                            if (HttpContext.Current.Session["filename"] != null)
                            {
                                vCardname = (string)HttpContext.Current.Session["filename"];
                            }
                            var attach = new[] { new EmailAttachment
                                    {
                                        Content = Convert.ToBase64String(File.ReadAllBytes(objCustomeMailPropery.attachment)),
                                        Name = vCardname,
                                        Type = "text/x-vcard"
                                    }};
                            msg.Attachments = attach;
                        }
                        #endregion

                        msg.TrackOpens = true;
                        msg.TrackClicks = true;
                        msg.AutoText = true;
                        msg.PreserveRecipients = false;
                        msg.Merge = true;


                        #region Tages
                        List<string> myTags = new List<string>();
                        myTags.Add(objCustomeMailPropery.MailTemplateName);
                        msg.Tags = myTags;
                        #endregion

                        #region AddRecipientVariable
                        for (int i = 0; i < objCustomeMailPropery.ToMailAddress.Count; i++)
                        {
                            foreach (DataRow item in objCustomeMailPropery.MergeTags.Rows)
                            {
                                msg.AddRecipientVariable(objCustomeMailPropery.ToMailAddress[i], Convert.ToString(item["FieldName"]), Convert.ToString(item["FieldValue"]));
                            }
                        }
                        #endregion

                        //TemplateContent[] tempContents = new TemplateContent[1];
                        //var results = mandrillWrapper.SendMessage(new SendMessageRequest(msg)); //Id = 22, Status = WaitingForActivation, Method = "{null}", Result = "{Not yet computed}"
                        // Comment Bcoz occur mandrill error 29-3-2019
                        //var task = AsyncContext.Run(MyAsyncMethod);
                        var task = TaskEx.Run(async () => await mandrillWrapper.SendMessageTemplate(new SendMessageTemplateRequest(msg, objCustomeMailPropery.MailTemplateName)));
                        var result = task.WaitAndUnwrapException();
                        //var task = mandrillWrapper.SendMessageTemplate(new SendMessageTemplateRequest(msg, objCustomeMailPropery.MailTemplateName));
                        //var result = task.WaitAndUnwrapException();
                        //var results = mandrillWrapper.SendMessageTemplate(new SendMessageTemplateRequest(msg, objCustomeMailPropery.MailTemplateName));

                        //results.ContinueWith(data =>
                        //{
                        //    var emailstatus = data.Result;
                        //});

                    }
                    catch (Exception ex)
                    {
                        clsCommon objCommon = new clsCommon();
                        objCommon.InsertErrorLog("SendMailMethod--Mail sending issue from mandrill", ex.Message, ex.StackTrace);
                    }
                }
            }
            else
            {
                //Send email using mandrill
                try
                {
                    string MandrillApiKey = ConfigurationManager.AppSettings["mandrillapikey"];
                    MandrillApi mandrillWrapper = new MandrillApi(MandrillApiKey);

                    #region To Email List
                    List<EmailAddress> toList = new List<EmailAddress>();
                    for (int i = 0; i < objCustomeMailPropery.ToMailAddress.Count; i++)
                    {
                        EmailAddress receiver = new EmailAddress(objCustomeMailPropery.ToMailAddress[i], "");
                        toList.Add(receiver);
                    }
                    #endregion

                    EmailMessage msg = new EmailMessage();
                    msg.To = toList;            // Add the recipients.
                    msg.FromEmail = objCustomeMailPropery.FromMailAddress;
                    msg.FromName = objCustomeMailPropery.FromMailAddress;
                    msg.BccAddress = Convert.ToString(ConfigurationManager.AppSettings.Get("bccemail"));

                    #region Email Attachment
                    if (!string.IsNullOrEmpty(objCustomeMailPropery.attachment))
                    {
                        string vCardname = "VCard";
                        if (HttpContext.Current.Session["filename"] != null)
                        {
                            vCardname = (string)HttpContext.Current.Session["filename"];
                        }
                        var attach = new[] { new EmailAttachment
                                    {
                                        Content = Convert.ToBase64String(File.ReadAllBytes(objCustomeMailPropery.attachment)),
                                        Name = vCardname,
                                        Type = "text/x-vcard"
                                    }};
                        msg.Attachments = attach;
                    }
                    #endregion

                    msg.TrackOpens = true;
                    msg.TrackClicks = true;
                    msg.AutoText = true;
                    msg.PreserveRecipients = false;
                    msg.Merge = true;


                    #region Tages
                    List<string> myTags = new List<string>();
                    myTags.Add(objCustomeMailPropery.MailTemplateName);
                    msg.Tags = myTags;
                    #endregion

                    #region AddRecipientVariable
                    for (int i = 0; i < objCustomeMailPropery.ToMailAddress.Count; i++)
                    {
                        foreach (DataRow item in objCustomeMailPropery.MergeTags.Rows)
                        {
                            msg.AddRecipientVariable(objCustomeMailPropery.ToMailAddress[i], Convert.ToString(item["FieldName"]), Convert.ToString(item["FieldValue"]));
                        }
                    }
                    #endregion

                    var task = TaskEx.Run(async () => await mandrillWrapper.SendMessageTemplate(new SendMessageTemplateRequest(msg, objCustomeMailPropery.MailTemplateName)));
                    var result = task.WaitAndUnwrapException();

                    //TemplateContent[] tempContents = new TemplateContent[1];
                    //var results = mandrillWrapper.SendMessage(new SendMessageRequest(msg)); //Id = 22, Status = WaitingForActivation, Method = "{null}", Result = "{Not yet computed}"

                    //var results = mandrillWrapper.SendMessageTemplate(new SendMessageTemplateRequest(msg, objCustomeMailPropery.MailTemplateName));
                    //results.ContinueWith(data =>
                    //{
                    //    var emailstatus = data.Result;
                    //}, TaskContinuationOptions.AttachedToParent);

                }
                catch (Exception ex)
                {
                    clsCommon objCommon = new clsCommon();
                    objCommon.InsertErrorLog("SendMailMethod--Mail sending issue from mandrill", ex.Message, ex.StackTrace);
                }
            }
        }

        /// <summary>
        /// Send email using mandrill api
        /// </summary>
        /// <param name="objCustomeMailPropery"></param>
        public static void SendingEmail(CustomeMailPropery objCustomeMailPropery)
        {
            
        }
    }


    public class CustomeMailPropery
    {
        private string FromAddress;
        public string FromMailAddress
        {
            get { return FromAddress = ConfigurationManager.AppSettings.Get("DefaultFromAddress"); }
            set { FromAddress = value; }
        }
        public string FromOAuthEmail { get; set; }
        public string OAuthAccessToken { get; set; }
        public string OAuthRefreshToken { get; set; }
        public int ProviderId { get; set; }
        public List<string> ToMailAddress { get; set; }
        public List<string> CCMailAddress { get; set; }
        public List<string> BCCMailAddress { get; set; }
        public string MailTemplateName { get; set; }
        public DataTable MergeTags { get; set; }
        public string attachment { get; set; }
        public string body { get; set; }
        public string smtpName { get; set; }
        public int smtpPort { get; set; }
        public string Password { get; set; }
        public bool SSLEnabled { get; set; }
        public string subject { get; set; }
    }

    public enum Mailtype
    {
        ManDrill,
        OAuth
    }    

}
