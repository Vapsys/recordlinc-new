﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Reflection;

namespace DataAccessLayer.Common
{

    public class clsHelper
    {
        Helper_ConnectionClass obj_connection;

        private SqlConnection objCon = new SqlConnection();
        private SqlCommand objCommand;
        private SqlDataAdapter objAdapter;
        private SqlDataReader objReader;
        private DataTable dt = new DataTable();
        private DataSet ds = new DataSet();


        public clsHelper()
        {
            objCommand = null;
            objAdapter = null;
            objReader = null;
            dt = null;
            obj_connection = new Helper_ConnectionClass();
        }

        public clsHelper(string ConnectionString)
        {
            objCommand = null;
            objAdapter = null;
            objReader = null;
            dt = null;
            obj_connection = new Helper_ConnectionClass(ConnectionString);
        }


        //returning data in datatable without passing any parameter....
        public DataTable DataTable(string spProcedureName, int timeout = 120)
        {

            try
            {
                dt = null;
                DataSet ds = new DataSet();
                objCon = obj_connection.Open();
                objCommand = new SqlCommand();
                objCommand.Connection = objCon;
                objCommand.CommandText = spProcedureName;
                objCommand.CommandTimeout = timeout;
                objCommand.CommandType = CommandType.StoredProcedure;
                DebugCommand(objCommand);
                objAdapter = new SqlDataAdapter(objCommand);
                objAdapter.Fill(ds, "a");
                dt = ds.Tables["a"];
                ds.Dispose();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                obj_connection.Close(objCon);
            }

            return dt;
        }

        //returning data in datatable after passing parameter....
        public DataTable DataTable(string spProcedureName, SqlParameter[] pParameter, int timeout = 120)
        {
            try
            {
                dt = null;
                DataSet ds = new DataSet();
                objCon = obj_connection.Open();
                objCommand = new SqlCommand();
                objCommand.Connection = objCon;
                objCommand.CommandText = spProcedureName;
                objCommand.CommandTimeout = timeout;
                objCommand.CommandType = CommandType.StoredProcedure;

                if (pParameter != null)
                {
                    foreach (SqlParameter p in pParameter)
                    {
                        objCommand.Parameters.Add(p);
                    }
                }

                DebugCommand(objCommand);
                objAdapter = new SqlDataAdapter(objCommand);
                objAdapter.Fill(ds, "a");
                dt = ds.Tables["a"];
                ds.Dispose();


            }
            catch (Exception Ex)
            {

               throw;
            }

            finally
            {
                obj_connection.Close(objCon);

            }

            return dt;
        }

        // this method is only use to export doctor record from admin 
        public DataTable DataTableForExportData(string spProcedureName, SqlParameter[] pParameter, int timeout = 120)
        {
            try
            {
                dt = new DataTable();

                objCon = obj_connection.Open();
                objCommand = new SqlCommand();
                objCommand.Connection = objCon;
                objCommand.CommandTimeout = timeout;
                objCommand.CommandText = spProcedureName;

                objCommand.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter p in pParameter)
                {
                    objCommand.Parameters.Add(p);
                }

                //DebugCommand(objCommand);
                //objAdapter = new SqlDataAdapter(objCommand);
                //objAdapter.Fill(dt);



                SqlDataReader rdr = objCommand.ExecuteReader();
                if (rdr.HasRows)
                    dt.Load(rdr);


            }
            catch (Exception)
            {
                throw;
            }

            finally
            {
                obj_connection.Close(objCon);

            }

            return dt;
        }

        //reading data through datareader with parameter that returning nothing...
        private SqlDataReader DataReaderData(string spProcedureName, int timeout = 120)
        {
            try
            {
                objCon = obj_connection.Open();
                objCommand = new SqlCommand();
                objCommand.Connection = objCon;
                objCommand.CommandText = spProcedureName;
                objCommand.CommandTimeout = timeout;
                objCommand.CommandType = CommandType.StoredProcedure;
                DebugCommand(objCommand);
                objReader = objCommand.ExecuteReader();

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                obj_connection.Close(objCon);
            }
            return objReader;
        }

        private SqlDataReader DataReader(string spProcedureName, SqlParameter[] pParameter, int timeout = 120)
        {
            try
            {
                objCon = obj_connection.Open();
                objCommand = new SqlCommand();
                objCommand.Connection = objCon;
                objCommand.CommandText = spProcedureName;
                objCommand.CommandTimeout = timeout;
                objCommand.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter p in pParameter)
                {
                    objCommand.Parameters.Add(p);
                }
                DebugCommand(objCommand);
                objReader = objCommand.ExecuteReader();

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                obj_connection.Close(objCon);
            }
            return objReader;
        }



        public DataSet GetDatasetData(string spProcedureName, SqlParameter[] pParameter, int timeout = 120)
        {
            try
            {
                dt = null;
                ds = new DataSet();
                objCon = obj_connection.Open();
                objCommand = new SqlCommand();
                objCommand.CommandTimeout = timeout;
                objCommand.CommandText = spProcedureName;
                objCommand.Connection = objCon;

                objCommand.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter p in pParameter)
                {
                    objCommand.Parameters.Add(p);
                }

                if (objCon.State == ConnectionState.Closed)
                    objCon.Open();

                objAdapter = new SqlDataAdapter(objCommand);
                objAdapter.Fill(ds);

            }
            catch (Exception)
            {
                throw;
            }

            finally
            {
                obj_connection.Close(objCon);

            }

            return ds;
        }

        //return scalar value from database....

        public string ExecuteScalar(string spProcedureName, SqlParameter[] pParameter, int timeout = 120)
        {
            string strValue = "";
            try
            {
                objCon = obj_connection.Open();

                objCommand = new SqlCommand();
                objCommand.Connection = objCon;
                objCommand.CommandText = spProcedureName;
                objCommand.CommandTimeout = timeout;
                objCommand.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter p in pParameter)
                {
                    objCommand.Parameters.Add(p);
                }

                DebugCommand(objCommand);
                //if (objCommand.ExecuteScalar() != null)
                //{
                //Olivia changes on 10-04-2018 for Dr. Lan Allan's website. - DLADCETK-1
                strValue = Convert.ToString(objCommand.ExecuteScalar());
                //}
                //else
                //{
                //    strValue = "";
                //}
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                obj_connection.Close(objCon);
            }
            return strValue;

        }

        public int BulkInsert(DataTable dataTable,string TableName)
        {
            try
            {
                int I = 0;
                objCon = obj_connection.Open();
                using (SqlBulkCopy bc = new SqlBulkCopy(objCon))
                {
                    bc.BatchSize = 10000;
                    bc.DestinationTableName = TableName;
                    foreach (var item in dataTable.Columns)
                    {
                        bc.ColumnMappings.Add(Convert.ToString(item),Convert.ToString(item));
                    }
                    bc.WriteToServer(dataTable);
                    I = SqlBulkCopyHelper.GetRowsCopied(bc);
                }
                return I;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                obj_connection.Close(objCon);
            }
        }
        public int BulkInsertWithArray(DataTable dataTable,string TableName,List<string> RequestArray, List<string> Response)
        {
            try
            {
                int I = 0;
                objCon = obj_connection.Open();
                using (SqlBulkCopy bc = new SqlBulkCopy(objCon))
                {
                    bc.BatchSize = 10000;
                    bc.DestinationTableName = TableName;
                    for (int i = 0; i < RequestArray.Count; i++)
                    {
                        bc.ColumnMappings.Add(Convert.ToString(RequestArray[i]), Convert.ToString(Response[i]));
                    }
                    //foreach (var item in dataTable.Columns)
                    //{
                    //    bc.ColumnMappings.Add(Convert.ToString(item), Convert.ToString(item));
                    //}
                    bc.WriteToServer(dataTable);
                    I = SqlBulkCopyHelper.GetRowsCopied(bc);
                }
                return I;

            }
            catch (Exception Ex)
            {
                throw;
            }
            finally
            {
                obj_connection.Close(objCon);
            }
        }
        //update the database ....
        public bool ExecuteNonQuery(string spStoredProcedure, SqlParameter[] pParameter, int timeout = 120)
        {
            bool boolSuccess = false;

            try
            {
                objCon = obj_connection.Open();
                objCommand = new SqlCommand();
                objCommand.Connection = objCon;
                objCommand.CommandTimeout = timeout;
                objCommand.CommandText = spStoredProcedure;

                objCommand.CommandType = CommandType.StoredProcedure;
                if (pParameter != null)
                {
                    foreach (SqlParameter p in pParameter)
                    {
                        objCommand.Parameters.Add(p);
                    }
                }
               
                DebugCommand(objCommand);
                int intReturnValue = objCommand.ExecuteNonQuery();

                if (intReturnValue > 0)
                {
                    boolSuccess = true;

                }
                else
                {
                    boolSuccess = false;
                }

            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                obj_connection.Close(objCon);
            }

            return boolSuccess;

        }

        public static DateTime ConvertToUTC(DateTime dtInput, string TimeZoneSystemName)
        {
            TimeZoneSystemName = String.IsNullOrEmpty(TimeZoneSystemName) ? "Eastern Standard Time" : TimeZoneSystemName;
            var zone = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneSystemName);
            return TimeZoneInfo.ConvertTimeToUtc(dtInput, zone);
        }

        public static DateTime ConvertFromUTC(DateTime dtUTC, string TimeZoneSystemName)
        {
            TimeZoneSystemName = string.IsNullOrEmpty(TimeZoneSystemName) ? "Eastern Standard Time" : TimeZoneSystemName;
            var zone = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneSystemName);
            return TimeZoneInfo.ConvertTimeFromUtc(dtUTC, zone);
        }

        public ArrayList DataReader(string ProcedureName)
        {
            ArrayList arrData = new ArrayList();
            try
            {
                SqlDataReader dr = DataReaderData(ProcedureName);

                while (dr.Read())
                {
                    for (int i = 0; i < dr.FieldCount; i++)
                    {
                        arrData.Add(dr[i].ToString());
                    }
                }
                dr.Close();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                obj_connection.Close(objCon);
            }
            return arrData;
        }

        ~clsHelper()
        {
            objCon = null;
            objCommand = null;
            objAdapter = null;
            objReader = null;
            dt = null;
        }
        public string ExecuteNonQueryString(string spStoredProcedure, SqlParameter[] pParameter)
        {
            string strRetrun = string.Empty;

            try
            {
                objCon = obj_connection.Open();
                objCommand = new SqlCommand();
                objCommand.Connection = objCon;
                objCommand.CommandText = spStoredProcedure;
                objCommand.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter p in pParameter)
                {
                    objCommand.Parameters.Add(p);
                }
                DebugCommand(objCommand);
                int intReturnValue = objCommand.ExecuteNonQuery();
                strRetrun = pParameter[1].Value.ToString();

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                obj_connection.Close(objCon);
            }

            return strRetrun;

        }

        [Conditional("DEBUG")]
        private void DebugCommand(SqlCommand cmd)
        {
            // these 2 get hammered by ajax polling
            if (cmd.CommandText == "stp_GetMessage" || cmd.CommandText == "Get_Online_Colleagues")
                return;
            Debug.WriteLine(cmd.CommandText);
        }

        //public object ExecuteScalar(string spStoredProcedure, SqlParameter[] pParameter)
        //{
        //    objCon = obj_connection.Open();
        //    objCommand = new SqlCommand();
        //    objCommand.Connection = objCon;
        //    objCommand.CommandText = spStoredProcedure;
        //    objCommand.CommandType = CommandType.StoredProcedure;

        //    foreach (SqlParameter p in pParameter)
        //    {
        //        objCommand.Parameters.Add(p);
        //    }
        //    DebugCommand(objCommand);
        //    return objCommand.ExecuteScalar();
        //}

        public static string EscapeUriString(string Url)
        {
            string ReturnValue = Url;
            //ReturnValue = ReturnValue.Replace(" ", "%20");
            //ReturnValue = System.Web.HttpContext.Current.Server.UrlEncode(ReturnValue);
            ReturnValue = Uri.EscapeUriString(Url);
            ReturnValue = ReturnValue.Replace("#", "%23");
            ReturnValue = ReturnValue.Replace("&", "%26");
            ReturnValue = ReturnValue.Replace(",", "%2c");
            ReturnValue = ReturnValue.Replace("=", "%3d");
            ReturnValue = ReturnValue.Replace("+", "%2b");
            ReturnValue = ReturnValue.Replace(";", "%3b");
            ReturnValue = ReturnValue.Replace("'", "%27");
            ReturnValue = ReturnValue.Replace("@", "%40");
            ReturnValue = ReturnValue.Replace("$", "%24");

            return ReturnValue;
        }

        public static object ConvertFromUTC(DateTime dateTime, object timeZoneSystemName)
        {
            throw new NotImplementedException();
        }

        public static string GetImageBase64(string filePhysicalPath)
        {
            var base64 = Convert.ToBase64String(System.IO.File.ReadAllBytes(filePhysicalPath));
            var imgSrc = String.Format("data:image/gif;base64,{0}", base64);
            return imgSrc;
        }
    }
    /// <summary>
    /// Helper class to process the SqlBulkCopy class
    /// </summary>
    static class SqlBulkCopyHelper
    {
        static FieldInfo rowsCopiedField = null;

        /// <summary>
        /// Gets the rows copied from the specified SqlBulkCopy object
        /// </summary>
        /// <param name="bulkCopy">The bulk copy.</param>
        /// <returns></returns>
        public static int GetRowsCopied(SqlBulkCopy bulkCopy)
        {
            if (rowsCopiedField == null)
            {
                rowsCopiedField = typeof(SqlBulkCopy).GetField("_rowsCopied", BindingFlags.NonPublic | BindingFlags.GetField | BindingFlags.Instance);
            }
            return (int)rowsCopiedField.GetValue(bulkCopy);
        }
    }
}