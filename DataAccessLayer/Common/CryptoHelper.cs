﻿#region FileHeader
// CryptoHelper:
// =============
// 
// Functional Description:
// =======================
// An abstract class that provide utility methods for encrypt/decrypting text. This
// class should be used a parent class for concrete classes that wish to provide
// symmetric cryptography methods.
// 
// Parent class:
// =============
// System.Object
// 
// Implemented interfaces:
// =======================
// 
// 
// Related classes:
// ================
// 
// 
// Author:
// =======
// Ronald Hernández Cisneros - rhernandez@avantica.net
// 
// Creation date:
// ==============
// Tuesday, March 30, 2004
// 
// Last modified date:
// ===================
// Tuesday, March 30, 2004
// 
// Copyright(c) 2004 RecordLinc. All Rights Reserved
#endregion

using System;

namespace DataAccessLayer.Common
{
    public abstract class CryptoHelper : Object
    {
        // OPTIONAL
        #region Constants

        #endregion

        // OPTIONAL
        #region Properties

        protected byte[] Key
        {
            get
            {
                return key;
            }
            set
            {
                key = value;
            }
        }

        #endregion

        // OPTIONAL
        #region Variables

        #region Public

        #endregion

        #region Protected

        /// <summary>
        /// The key used for encryption/decryption purposes.
        /// </summary>
        protected byte[] key;

        #endregion

        #region Private

        #endregion

        #region Internal

        #endregion

        #endregion

        #region Constructors

        #endregion

        // OPTIONAL
        #region Methods

        #region EncryptionSection

        /// <summary>
        /// Encrypts a text string.
        /// </summary>
        /// <param name="plainText">A string representing the plain text</param>
        /// <returns>A normalized string representing the encrypted text, or null if the text could not be encrypted</returns>
        public abstract string encryptText(string plainText);

        #endregion

        #region DecryptionSection

        /// <summary>
        /// Decrypts the specified cypher.
        /// </summary>
        /// <param name="encryptedText">A normalized string representing the cipher text</param>
        /// <returns>A string representing the plain text, or null if the text could not be decrypted</returns>
        public abstract string decryptText(string encryptedText);

        #endregion

        #endregion
    }
}
