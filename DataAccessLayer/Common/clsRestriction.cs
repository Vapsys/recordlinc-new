﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
namespace DataAccessLayer.Common
{
    public class clsRestriction
    {
        clsCommon objcommon = new clsCommon();
        clsHelper objHelper = new clsHelper();
        public DataTable GetUserFetauresList(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[] {
                     new SqlParameter() {ParameterName = "@UserId",Value= UserId }
                };
                return dt = objHelper.DataTable("Usp_GetFeaturesDetailsOfUser", sqlpara);
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("Membership_FeatureDLL--GetUserFetauresList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public int GetCountOfPatientByUserId(int UserId)
        {
            try
            {
                int Count = 0;
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[] {
                     new SqlParameter() {ParameterName = "@UserId",Value= UserId }
                };
                dt = objHelper.DataTable("GetPatientCountOfDoctor", sqlpara);
                if(dt != null && dt.Rows.Count > 0)
                {
                    Count = Convert.ToInt32(dt.Rows[0]["TotalCountOfPatient"]);
                }
                return Count;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("Membership_FeatureDLL--GetUserFetauresList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public int GetCountOfPatientAttachments(int PatientId,int UserId)
        {
            try
            {
                int Count = 0;
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[] {
                    new SqlParameter() {ParameterName = "@PatientId",Value= PatientId },
                     new SqlParameter() {ParameterName = "@UserId",Value= UserId }
                };
                dt = objHelper.DataTable("GetCountOfPatientImages", sqlpara);
                if (dt != null && dt.Rows.Count > 0)
                {
                    Count = Convert.ToInt32(dt.Rows[0]["counting"]);
                }
                return Count;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("Membership_FeatureDLL--GetUserFetauresList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public int CheckUserHasAccessOCR(int UserId)
        {
            try
            {
                int Count = 0;
                SqlParameter[] sqlpara = new SqlParameter[] {
                     new SqlParameter() {ParameterName = "@UserId",Value= UserId }
                };
                DataTable dt = objHelper.DataTable("Usp_CheckOCRUserAccess", sqlpara);
                if (dt != null && dt.Rows.Count > 0)
                {
                    Count = Convert.ToInt32(dt.Rows[0]["MaxCount"]);
                }
                return Count;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("Membership_FeatureDLL--CheckUserHasAccessOCR", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public int GetCountOfColleaguesByUserId(int UserId)
        {
            try
            {
                int Count = 0;
                DataTable dt = new DataTable();
                SqlParameter[] sqlpara = new SqlParameter[] {
                     new SqlParameter() {ParameterName = "@user_id",Value= UserId }
                };
                dt = objHelper.DataTable("Usp_GetColleagueCountOfDoctor", sqlpara);
                if(dt != null && dt.Rows.Count > 0)
                {
                    Count = Convert.ToInt32(dt.Rows[0]["TotalRecord"]);
                }
                return Count;
            }
            catch (Exception Ex)
            {
                objcommon.InsertErrorLog("Membership_FeatureDLL--GetCountOfColleaguesByUserId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
