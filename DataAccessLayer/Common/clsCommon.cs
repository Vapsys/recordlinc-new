﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Image = System.Drawing.Image;
using BO.ViewModel;
using BO.Models;

namespace DataAccessLayer.Common
{
    public enum ReturnStatus
    {
        IsDeleted = 1,
        IsExist = 2,
        IsAtleastOne = 3,
    }

    public class clsCommon
    {
        clsHelper objHelper = new clsHelper();
        Helper_ConnectionClass obj_connection = new Helper_ConnectionClass();
        DataSet ds = new DataSet();
        private SqlConnection objCon = new SqlConnection();
        private SqlCommand objCommand;
        private SqlDataAdapter objAdapter;

        public string CheckNull(string objVal, string DefaultValue)
        {
            //RM-178     
            if (objVal == "01/01/1900 12:00:00 AM")
            {
                return "";
            }

            if (objVal == null || objVal == "")
                return DefaultValue;

            return objVal;
        }

        public string ConvertPhoneNumber(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                value = new System.Text.RegularExpressions.Regex(@"\D")
                    .Replace(value, string.Empty);
                value = value.TrimStart('1');
                if (value.Length == 7)
                    return Convert.ToInt64(value).ToString("###-####");
                if (value.Length == 10)
                    return Convert.ToInt64(value).ToString("(###)###-####");
                if (value.Length > 10)
                    return Convert.ToInt64(value)
                        .ToString("(###)###-#### " + new String('#', (value.Length - 10)));

            }
            return value;
        }
        public static bool AllStringPropertyValuesAreNonEmpty(object myObject)
        {
            var allStringPropertyValues =
                from property in myObject.GetType().GetProperties()
                where property.PropertyType == typeof(string) && property.CanRead
                select (string)property.GetValue(myObject);

            return allStringPropertyValues.All(value => !string.IsNullOrEmpty(value));
        }
        #region Method for Get All Speciality
        public DataTable GetAllSpeciality()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = objHelper.DataTable("GetAllSpeciality");
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        public System.Drawing.Image DownloadImageFromUrl(string imageUrl)
        {
            System.Drawing.Image image = null;

            try
            {
                System.Net.HttpWebRequest webRequest = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(imageUrl);
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.Timeout = 30000;

                System.Net.WebResponse webResponse = webRequest.GetResponse();

                System.IO.Stream stream = webResponse.GetResponseStream();

                image = System.Drawing.Image.FromStream(stream);

                webResponse.Close();
            }
            catch (Exception ex)
            {
                return null;
            }

            return image;
        }

        #region Method for Get All Country
        public DataTable GetAllCountry()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = objHelper.DataTable("GetAllCountry");
                return dt;
            }
            catch (Exception)
            {
                throw;
            }

        }
        #endregion

        #region Get All State By Country Code
        public DataTable GetAllStateByCountryCode(string CountryCode)
        {

            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] Patparameters = new SqlParameter[1];
                Patparameters[0] = new SqlParameter();
                Patparameters[0].ParameterName = "@CountryCode";
                Patparameters[0].Value = CountryCode;

                dt = objHelper.DataTable("GetAllStateByCountryCode", Patparameters);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }

        }
        #endregion

        #region Remove HTML tages from string
        public string RemoveHTML(string input)
        {
            // remove comments
            input = Regex.Replace(input, "<!--(.|\\s)*?-->", string.Empty);
            // remove HTML
            return Regex.Replace(input, "<(.|\\s)*?>", string.Empty);
        }
        #endregion


        #region Get All Template
        public string GetTemplate(string TemplateName, string Operation)
        {
            string Desc;
            try
            {
                SqlParameter[] patientIdParams = new SqlParameter[1];
                patientIdParams[0] = new SqlParameter("@TemplateName", SqlDbType.VarChar);
                patientIdParams[0].Value = TemplateName;
                DataTable temp = objHelper.DataTable("USP_GetAllEmailTemplateByName", patientIdParams);

                if (temp.Rows.Count > 0)
                {
                    if (Operation == "Desc")
                    {
                        Desc = temp.Rows[0]["TemplateDesc"].ToString();
                    }
                    else if (Operation == "Subject")
                    {
                        Desc = temp.Rows[0]["TemplateSubject"].ToString();
                    }
                    else
                    {
                        Desc = "";
                    }
                }
                else
                {
                    Desc = "";
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Desc;
        }
        #endregion


        #region Methode For Errorlog
        public bool InsertErrorLog(string URL, string ErrorMessage, string StackTrace)
        {
            bool res;
            try
            {
                SqlParameter[] updatepwd = new SqlParameter[3];
                updatepwd[0] = new SqlParameter("@URL", SqlDbType.VarChar);
                updatepwd[0].Value = URL;
                updatepwd[1] = new SqlParameter("@ErrorMessage", SqlDbType.VarChar);
                updatepwd[1].Value = ErrorMessage;
                updatepwd[2] = new SqlParameter("@StackTrace", SqlDbType.VarChar);
                updatepwd[2].Value = StackTrace;

                res = objHelper.ExecuteNonQuery("InsertErrorLog", updatepwd);
                return res;
            }
            catch (Exception)
            {
                throw;
            }

        }
        #endregion
        public int GetMaxSortOrder(string TableName)
        {
            int res;
            try
            {
                SqlParameter[] sqlparams = new SqlParameter[1];
                sqlparams[0] = new SqlParameter("@TableName", SqlDbType.VarChar);
                sqlparams[0].Value = TableName;

                res = Convert.ToInt32(objHelper.ExecuteScalar("SP_GetMaxCount", sqlparams));
                return res;
            }
            catch (Exception)
            {
                throw;
            }

        }

        #region Method for make phone number in US format ex:(321)523-5639
        public string PhoneFormat(string phnumber)
        {
            string result;
            if ((!string.IsNullOrEmpty(phnumber)) && (phnumber.Length >= 10))
                result = string.Format("{0:(###)###-" + new string('#', phnumber.Length - 6) + "}",
                Convert.ToInt64(phnumber)

                );
            else
                result = phnumber;
            return result;

            //RESULT :(321)321-4569 output looks
        }
        #endregion


        #region Export Data
        public void ExportToExcel(DataTable dt, string fileName)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName + ".xls");


            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";

            StringWriter stringWriter = new StringWriter();
            HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWriter);
            DataGrid dataExportExcel = new DataGrid();
            dataExportExcel.ItemDataBound += dataExportExcel_ItemDataBound;
            dataExportExcel.DataSource = dt;
            dataExportExcel.DataBind();
            dataExportExcel.RenderControl(htmlWrite);
            StringBuilder sbResponseString = new StringBuilder();
            sbResponseString.Append("<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:x=\"urn:schemas-microsoft-com:office:excel\" xmlns=\"https://www.w3.org/TR/REC-html40\"> <head><meta http-equiv=\"Content-Type\" content=\"text/html;charset=windows-1252\"><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>" + fileName + "</x:Name><x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head> <body>");
            sbResponseString.Append(stringWriter + "</body></html>");
            HttpContext.Current.Response.Write(sbResponseString.ToString());

            HttpContext.Current.Response.End();
        }

        void dataExportExcel_ItemDataBound(object sender, DataGridItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Header)
            {
                //Header Text Format can be done as follows
                e.Item.Font.Bold = true;

                //Adding Filter/Sorting functionality for the Excel
                int cellIndex = 0;
                while (cellIndex < e.Item.Cells.Count)
                {
                    e.Item.Cells[cellIndex].Attributes.Add("x:autofilter", "all");
                    e.Item.Cells[cellIndex].Width = 200;
                    e.Item.Cells[cellIndex].HorizontalAlign = HorizontalAlign.Center;
                    cellIndex++;
                }
            }


            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                int cellIndex = 0;
                while (cellIndex < e.Item.Cells.Count)
                {
                    //Any Cell specific formatting should be done here
                    e.Item.Cells[cellIndex].HorizontalAlign = HorizontalAlign.Left;
                    cellIndex++;
                }
            }
        }

        #endregion

        #region Get Records from Temp Table for Doctor
        public DataTable GetRecordsFromTempTablesForDoctor(int UserId, string SessionId)
        {

            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] Patparameters = new SqlParameter[2];
                Patparameters[0] = new SqlParameter("@UserId", SqlDbType.Int);
                Patparameters[0].Value = UserId;
                Patparameters[1] = new SqlParameter("@SessionId", SqlDbType.NVarChar);
                Patparameters[1].Value = SessionId;

                dt = objHelper.DataTable("USP_GetRecordsFromTempTables_For_Doctor", Patparameters);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public DataTable Get_UploadedImagesByDoctor(int UserId, string SessionId)
        {

            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] Patparameters = new SqlParameter[2];
                Patparameters[0] = new SqlParameter("@UserId", SqlDbType.Int);
                Patparameters[0].Value = UserId;
                Patparameters[1] = new SqlParameter("@SessionId", SqlDbType.VarChar);
                Patparameters[1].Value = SessionId;

                dt = objHelper.DataTable("USP_Temp_UploadedImagesByDoctor", Patparameters);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public DataTable Get_UploadedFilesByDoctor(int UserId, string SessionId)
        {

            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] Patparameters = new SqlParameter[2];
                Patparameters[0] = new SqlParameter("@UserId", SqlDbType.Int);
                Patparameters[0].Value = UserId;
                Patparameters[1] = new SqlParameter("@SessionId", SqlDbType.VarChar);
                Patparameters[1].Value = SessionId;

                dt = objHelper.DataTable("USP_Temp_UploadedFilesByDoctor", Patparameters);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }

        }


        #endregion

        #region Upload Images In Temp Table
        public int Insert_Temp_UploadedImages(int UserId, string Filename, string SessionId)
        {

            try
            {
                bool bval;
                int oNewId = 0;
                SqlParameter[] TempUpload = new SqlParameter[4];
                TempUpload[0] = new SqlParameter();
                TempUpload[0] = new SqlParameter("@UserId", SqlDbType.Int);
                TempUpload[0].Value = UserId;
                TempUpload[1] = new SqlParameter("@FileName", SqlDbType.VarChar);
                TempUpload[1].Value = Filename;
                TempUpload[2] = new SqlParameter("@SessionId", SqlDbType.NVarChar);
                TempUpload[2].Value = SessionId;
                TempUpload[3] = new SqlParameter("@Newrecordid", SqlDbType.Int);
                TempUpload[3].Direction = ParameterDirection.Output;


                bval = objHelper.ExecuteNonQuery("USP_Temp_UploadedImages", TempUpload);
                oNewId = Convert.ToInt32(TempUpload[3].Value);
                return oNewId;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public DataTable Get_UploadedImagesById(int UploadImageId)
        {
            try
            {
                SqlParameter[] Getuploadimages = new SqlParameter[1];
                Getuploadimages[0] = new SqlParameter("@UploadId", SqlDbType.Int);
                Getuploadimages[0].Value = UploadImageId;

                return objHelper.DataTable("GetTempUploadedImagesByUploadId", Getuploadimages);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region GetPatientSecondaryEmail
        public DataTable GetPatientSecondaryEmail(string Email)
        {
            try
            {

                clsCommon ObjCommon = new clsCommon();
                DataTable dt = new DataTable();
                string qre = "select SecondaryEmail from Patient where Email ='" + Email.Replace("'", "''") + "'";
                dt = ObjCommon.DataTable(qre);
                return dt;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        #endregion

        #region GetDoctorSecondaryEmail
        public DataTable GetDoctorSecondaryEmail(string Email)
        {
            try
            {
                clsCommon ObjCommon = new clsCommon();
                DataTable dt = new DataTable();
                string qre = "select SecondaryEmail From Member where Username = '" + Email.Replace("'", "''") + "'";
                dt = ObjCommon.DataTable(qre);
                return dt;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }


        #endregion

        #region Upload Files In Temp Table
        public int Insert_Temp_UploadedFiles(int UserId, string Filename, string SessionId)
        {
            try
            {
                bool bval;
                int oNewId = 0;
                SqlParameter[] TempUpload = new SqlParameter[4];
                TempUpload[0] = new SqlParameter();
                TempUpload[0] = new SqlParameter("@UserId", SqlDbType.Int);
                TempUpload[0].Value = UserId;
                TempUpload[1] = new SqlParameter("@FileName", SqlDbType.VarChar);
                TempUpload[1].Value = Filename;
                TempUpload[2] = new SqlParameter("@SessionId", SqlDbType.NVarChar);
                TempUpload[2].Value = SessionId;
                TempUpload[3] = new SqlParameter("@Newrecordid", SqlDbType.Int);
                TempUpload[3].Direction = ParameterDirection.Output;

                bval = objHelper.ExecuteNonQuery("USP_Temp_UploadedFiles", TempUpload);
                oNewId = Convert.ToInt32(TempUpload[3].Value);
                return oNewId;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public DataTable GetTempAttachments(string ImageId, string FileId, int MessageId = 0, int ComposeType = 0)
        {
            try
            {
                SqlParameter[] Sqlparams = new SqlParameter[4];
                Sqlparams[0] = new SqlParameter("@ImageId", SqlDbType.NVarChar);
                Sqlparams[0].Value = ImageId;
                Sqlparams[1] = new SqlParameter("@FileId", SqlDbType.NVarChar);
                Sqlparams[1].Value = FileId;
                Sqlparams[2] = new SqlParameter("@MessageId", SqlDbType.Int);
                Sqlparams[2].Value = MessageId;
                Sqlparams[3] = new SqlParameter("@ComposeType", SqlDbType.Int);
                Sqlparams[3].Value = ComposeType;
                return objHelper.DataTable("SP_GetAttachMentDetails", Sqlparams);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetForwardAttachments(string ImageId, string FileId, int MessageId = 0, int ComposeType = 0)
        {
            try
            {
                SqlParameter[] Sqlparams = new SqlParameter[4];
                Sqlparams[0] = new SqlParameter("@ImageId", SqlDbType.NVarChar);
                Sqlparams[0].Value = ImageId;
                Sqlparams[1] = new SqlParameter("@FileId", SqlDbType.NVarChar);
                Sqlparams[1].Value = FileId;
                Sqlparams[2] = new SqlParameter("@MessageId", SqlDbType.Int);
                Sqlparams[2].Value = MessageId;
                Sqlparams[3] = new SqlParameter("@ComposeType", SqlDbType.Int);
                Sqlparams[3].Value = ComposeType;
                return objHelper.DataTable("SP_GetForwardAttachMentDetails", Sqlparams);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetForwardAttachementDetailsById(string FileIds, int ComposeType = 0)
        {
            try
            {
                SqlParameter[] Sqlparams = new SqlParameter[2];
                Sqlparams[0] = new SqlParameter("@FileIds", SqlDbType.NVarChar);
                Sqlparams[0].Value = FileIds;
                Sqlparams[1] = new SqlParameter("@ComposeType", SqlDbType.Int);
                Sqlparams[1].Value = ComposeType;
                return objHelper.DataTable("SP_GetForwardAttachementDetailsById", Sqlparams);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetDraftAttachmentsByMultiMessage(string ColleagueMessageId, string PatientMessageId)
        {
            try
            {
                SqlParameter[] Sqlparams = new SqlParameter[2];
                Sqlparams[0] = new SqlParameter("@ColleagueMessageId", SqlDbType.NVarChar);
                Sqlparams[0].Value = ColleagueMessageId;
                Sqlparams[1] = new SqlParameter("@PatientMessageId", SqlDbType.NVarChar);
                Sqlparams[1].Value = PatientMessageId;
                return objHelper.DataTable("SP_GetDraftAttachmentsByMultiMessage", Sqlparams);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool DeleteAllAttachements(string ImageId, string FileId, int MessageId = 0)
        {
            bool res;
            try
            {
                SqlParameter[] Sqlparams = new SqlParameter[3];
                Sqlparams[0] = new SqlParameter("@ImageId", SqlDbType.NVarChar);
                Sqlparams[0].Value = ImageId;
                Sqlparams[1] = new SqlParameter("@FileId", SqlDbType.NVarChar);
                Sqlparams[1].Value = FileId;
                Sqlparams[2] = new SqlParameter("@MessageId", SqlDbType.Int);
                Sqlparams[2].Value = MessageId;
                return res = objHelper.ExecuteNonQuery("SP_DeleteAllAttachement", Sqlparams);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool Temp_AttachedFileDeleteById(int FileId, int FileFrom, int ComposeType = 0)
        {
            bool res;
            try
            {
                SqlParameter[] sqlparams = new SqlParameter[3];
                sqlparams[0] = new SqlParameter("@FileId", SqlDbType.Int);
                sqlparams[0].Value = FileId;
                sqlparams[1] = new SqlParameter("@FileFrom", SqlDbType.Int);
                sqlparams[1].Value = FileFrom;
                sqlparams[2] = new SqlParameter("@ComposeType", SqlDbType.Int);
                sqlparams[2].Value = ComposeType;
                res = objHelper.ExecuteNonQuery("SP_DeleteAttachement", sqlparams);
            }
            catch (Exception)
            {
                throw;
            }
            return res;
        }
        public DataTable Get_UploadedFilesById(int UploadedFileId)
        {
            try
            {
                SqlParameter[] Getuploadimages = new SqlParameter[1];
                Getuploadimages[0] = new SqlParameter("@UploadId", SqlDbType.Int);
                Getuploadimages[0].Value = UploadedFileId;


                return objHelper.DataTable("GetTempUploadedFilesByUploadId", Getuploadimages);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetLocationHistory()
        {
            try
            {
                SqlParameter[] Sqlparams = new SqlParameter[0];
                return objHelper.DataTable("SP_GetLocationHistory", Sqlparams);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public DataTable GetSpecialityHistory()
        {
            try
            {
                SqlParameter[] Sqlparams = new SqlParameter[0];
                return objHelper.DataTable("SP_GetSpecialityHistory", Sqlparams);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int AddDocumenttoMontage(int MontageId, string DocumentName, string uploadby)
        {
            int x = 0;
            try
            {
                string query = "insert into Montage_Document(MontageId,DocumentName,uploadby)values('" + MontageId + "','" + DocumentName + "','" + uploadby + "')";

                x = SqlHelper.ExecuteNonQuery(obj_connection.Open(), CommandType.Text, query);
                return x;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region RandomPasswordGenrate
        public string CreateRandomPassword(int passwordLength)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            char[] chars = new char[passwordLength];
            Random rd = new Random();

            for (int i = 0; i < passwordLength; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }
        #endregion

        public static class FileExtension
        {

            public static string CheckFileExtenssion(string DocName)
            {
                try
                {
                    string Ext = string.IsNullOrEmpty(DocName) ? "" : Path.GetExtension(DocName);
                    switch (Ext.ToLower())
                    {
                        case ".doc": return "../../Content/images/word.png";
                        case ".docx": return "../../Content/images/word.png";
                        case ".xls": return "../../Content/images/excel.png";
                        case ".csv": return "../../Content/images/excel.png";
                        case ".xlsx": return "../../Content/images/excel.png";
                        case ".pdf": return "../../Content/images/pdf.png";
                        case ".dex": return "../../Content/images/dex.png";
                        case ".dcm": return "../../Content/images/img_1.jpg";
                        case ".tif": return "../../Content/images/img_1.jpg";
                        case ".TIF": return "../../Content/images/img_1.jpg";
                        case ".pspimage": return "../../Content/images/img_1.jpg";
                        case ".thm": return "../../Content/images/img_1.jpg";
                        default: return "../../Content/images/img_1.jpg";
                    }
                }
                catch (Exception)
                {
                    return "";
                }
            }
        }

        public static string GetUniquenumberForAttachments()
        {
            if (HttpContext.Current.Session["UniquenumberForAttachments"] == null || String.IsNullOrWhiteSpace(Convert.ToString(HttpContext.Current.Session["UniquenumberForAttachments"])))
                HttpContext.Current.Session["UniquenumberForAttachments"] = Convert.ToString(DateTime.Now.Ticks);

            return Convert.ToString(HttpContext.Current.Session["UniquenumberForAttachments"]);

        }

        public static void SetUniquenumberForAttachmentsToNull()
        {
            HttpContext.Current.Session["UniquenumberForAttachments"] = string.Empty;
        }


        #region Remove Temp Uploded file by ID (Uploaded by  Doctor)

        public bool Temp_DeleteUploadedFileById(int UserId, int UploadedFileId)
        {
            bool res;
            try
            {
                SqlParameter[] updatepwd = new SqlParameter[2];
                updatepwd[0] = new SqlParameter("@UserId", SqlDbType.Int);
                updatepwd[0].Value = UserId;
                updatepwd[1] = new SqlParameter("@UploadedFileId", SqlDbType.Int);
                updatepwd[1].Value = UploadedFileId;


                res = objHelper.ExecuteNonQuery("USP_Temp_DeleteUploadedFileById", updatepwd);
            }
            catch (Exception)
            {
                throw;
            }
            return res;
        }
        #endregion

        #region Remove Temp Uploaded Image by Id (Uploaded by  Doctor)
        public bool Temp_DeleteUploadedImageById(int UserId, int UploadedImageId)
        {
            bool res;
            try
            {
                SqlParameter[] updatepwd = new SqlParameter[2];
                updatepwd[0] = new SqlParameter("@UserId", SqlDbType.Int);
                updatepwd[0].Value = UserId;
                updatepwd[1] = new SqlParameter("@ImageId", SqlDbType.Int);
                updatepwd[1].Value = UploadedImageId;


                res = objHelper.ExecuteNonQuery("USP_Temp_DeleteUploadedImageById", updatepwd);
            }
            catch (Exception)
            {
                throw;
            }
            return res;
        }
        #endregion

        #region Remove All Temp Images and Files uploaded by Doctor
        public bool Temp_RemoveAllByUserId(int UserId, string SessionId)
        {
            bool res;
            try
            {
                SqlParameter[] updatepwd = new SqlParameter[2];
                updatepwd[0] = new SqlParameter("@UserId", SqlDbType.Int);
                updatepwd[0].Value = UserId;
                updatepwd[1] = new SqlParameter("@SessionId", SqlDbType.NVarChar);
                updatepwd[1].Value = SessionId;
                res = objHelper.ExecuteNonQuery("USP_Temp_RemoveAllByUserId", updatepwd);
                SetUniquenumberForAttachmentsToNull();
            }
            catch (Exception)
            {
                throw;
            }
            return res;
        }
        #endregion


        #region GetThumbnail
        public void GenerateThumbnailImage(string SourceFilePath, string SourceFileName, string DestinationFilePath)
        {
            try
            {
                Image image;
                if (SourceFilePath.EndsWith("\\"))
                    image = Image.FromFile(SourceFilePath + SourceFileName);
                else
                    image = Image.FromFile(SourceFilePath + @"\" + SourceFileName);

                // Compute thumbnail size.
                Size thumbnailSize = GetThumbnailSize(image);

                // Get thumbnail.
                Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                    thumbnailSize.Height, null, IntPtr.Zero);

                // Save thumbnail.            
                thumbnail.Save(DestinationFilePath + @"\" + SourceFileName);
            }
            catch (Exception ex)
            {

                bool status = InsertErrorLog("", ex.Message + ex.StackTrace, ex.Message + ex.StackTrace);
            }
        }

        public Size GetThumbnailSize(Image original)
        {
            // Maximum size of any dimension.
            int maxPixels = Convert.ToInt32(ConfigurationManager.AppSettings["Thumbimagediamention"]);

            // Width and height.
            int originalWidth = original.Width;
            int originalHeight = original.Height;

            // Compute best factor to scale entire image based on larger dimension.
            double factor = 1;
            if (originalHeight > maxPixels || originalWidth > maxPixels)
            {
                if (originalWidth > originalHeight)
                {
                    factor = (double)maxPixels / originalWidth;
                }
                else
                {
                    factor = (double)maxPixels / originalHeight;
                }
            }
            // Return thumbnail size.
            return new Size((int)(originalWidth * factor), (int)(originalHeight * factor));
        }
        #endregion


        #region Get Image Type this Use in Patient History
        public DataTable GetImageType()
        {
            try
            {
                return objHelper.DataTable("USP_GetImageType");
            }
            catch (Exception)
            {
                throw;
            }
        }


        public DataTable GetImageTypeById(int ImageTypeId)
        {

            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] Patparameters = new SqlParameter[1];
                Patparameters[0] = new SqlParameter();
                Patparameters[0].ParameterName = "@ImageTypeId";
                Patparameters[0].Value = ImageTypeId;

                dt = objHelper.DataTable("USP_GetImageTypeById", Patparameters);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }

        }
        #endregion

        #region Get Import Patient Columns

        public DataTable GetColumnsForImportPatient()
        {
            try
            {
                DataTable dt = new DataTable();
                dt = objHelper.DataTable("USP_GetColumnsForImportPatient");
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public DataTable GetUserPlanDetails(int UserId)
        {

            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] Patparameters = new SqlParameter[1];
                Patparameters[0] = new SqlParameter("@UserId", SqlDbType.Int);
                Patparameters[0].Value = UserId;
                dt = objHelper.DataTable("SP_GetUserPlanDetails", Patparameters);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }

        }
        public string ExecuteQuery(string query)
        {
            try
            {
                objCon = obj_connection.Open();
                objCommand = new SqlCommand();
                objCommand.Connection = objCon;
                objCommand.CommandText = query;
                objCommand.CommandType = CommandType.Text;
                return Convert.ToString(objCommand.ExecuteNonQuery());
            }
            catch (Exception Ex)
            {
                return Ex.Message;
            }
            finally
            {
                obj_connection.Close(objCon);
            }

        }

        public DataTable DataTable(string Qry)
        {
            DataTable dt = new DataTable();
            try
            {
                DataSet ds = new DataSet();
                objCon = obj_connection.Open();
                objCommand = new SqlCommand();
                objCommand.Connection = objCon;
                objCommand.CommandText = Qry;
                objCommand.CommandType = CommandType.Text;
                objAdapter = new SqlDataAdapter(objCommand);
                objAdapter.Fill(ds, "a");
                dt = ds.Tables["a"];
                ds.Dispose();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                obj_connection.Close(objCon);
            }

            return dt;
        }

        public bool IsValidPhone(string Number)
        {
            string strPhone = Regex.Replace(Number, @"[- ()\*\!]", String.Empty);


            Regex regexObjDeg = new Regex(@"^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$");
            Match matTenDigits = regexObjDeg.Match(Number);
            return matTenDigits.Success;
        }

        public bool IsEmail(string inputEmail)
        {
            string strRegex = @"^[a-zA-Z0-9][a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputEmail.Trim()))
            {
                return (true);
            }
            else
            {
                return false;
            }
        }
        public bool IsValidZipCode(string inputZipCode)
        {
            string strRegex = @"^([FG]?\d{5}|\d{5}[AB])$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputZipCode))
                return (true);
            return false;
        }

        public int GetGender(string Gender)
        {
            int Gen = 0;
            if (Gender.ToLower() == "female" || Gender.ToLower() == "f")
            {
                return Gen = 1;
            }
            return Gen;
        }

        public DataTable GetUserIDFromProfilePath(string ProfilePath)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] SearchUnivDetails = new SqlParameter[1];
                SearchUnivDetails[0] = new SqlParameter("@ProfilePath", SqlDbType.VarChar);
                SearchUnivDetails[0].Value = ProfilePath;
                dt = objHelper.DataTable("USP_GetUserIDFromProfilePath", SearchUnivDetails);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool USP_InsertLogedInfo(string Number)
        {
            string strPhone = Regex.Replace(Number, @"[- ()\*\!]", String.Empty);


            Regex regexObjDeg = new Regex(@"^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$");
            Match matTenDigits = regexObjDeg.Match(Number);
            return matTenDigits.Success;
        }

        #region Logged In and Out Information
        public bool InsertLogedInfo(int UserId)
        {
            bool res;
            try
            {
                SqlParameter[] updatepwd = new SqlParameter[1];
                updatepwd[0] = new SqlParameter("@UserId", SqlDbType.Int);
                updatepwd[0].Value = UserId;
                res = objHelper.ExecuteNonQuery("USP_InsertLogedInfo", updatepwd);
            }
            catch (Exception)
            {
                throw;
            }
            return res;
        }

        public bool LogedOutInfo(int UserId)
        {
            bool res;
            try
            {
                SqlParameter[] updatepwd = new SqlParameter[1];
                updatepwd[0] = new SqlParameter("@UserId", SqlDbType.Int);
                updatepwd[0].Value = UserId;
                res = objHelper.ExecuteNonQuery("USP_LogedOutInfo", updatepwd);
            }
            catch (Exception)
            {
                throw;
            }
            return res;
        }
        #endregion


        #region Search University Names
        public DataTable SearchUniversityNames(string SelectedInstituteValue)
        {
            DataTable dt = new DataTable();
            try
            {

                SqlParameter[] SearchUnivDetails = new SqlParameter[1];
                SearchUnivDetails[0] = new SqlParameter("@pUnivName", SqlDbType.VarChar);
                SearchUnivDetails[0].Value = SelectedInstituteValue;
                dt = objHelper.DataTable("USP_SearchUniversityNames", SearchUnivDetails);

            }
            catch (Exception)
            {
                throw;
            }
            return dt;
        }

        #endregion

        #region Search ProMembership
        public DataTable SearchProMembership(string MembershiValue)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] SearchUnivDetails = new SqlParameter[1];
                SearchUnivDetails[0] = new SqlParameter("@pMembershipName", SqlDbType.VarChar);
                SearchUnivDetails[0].Value = MembershiValue;
                dt = objHelper.DataTable("USP_SearchProMembershipNames", SearchUnivDetails);

            }
            catch (Exception)
            {
                throw;
            }
            return dt;
        }
        #endregion

        public DataTable GetTempAttachments(string ImageId, string FileId, int MessageId = 0)
        {
            try
            {
                SqlParameter[] Sqlparams = new SqlParameter[3];
                Sqlparams[0] = new SqlParameter("@ImageId", SqlDbType.NVarChar);
                Sqlparams[0].Value = ImageId;
                Sqlparams[1] = new SqlParameter("@FileId", SqlDbType.NVarChar);
                Sqlparams[1].Value = FileId;
                Sqlparams[2] = new SqlParameter("@MessageId", SqlDbType.Int);
                Sqlparams[2].Value = MessageId;
                return objHelper.DataTable("SP_GetAttachMentDetails", Sqlparams);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool Temp_AttachedFileDeleteById(int FileId, int FileFrom)
        {
            bool res;
            try
            {
                SqlParameter[] sqlparams = new SqlParameter[2];
                sqlparams[0] = new SqlParameter("@FileId", SqlDbType.Int);
                sqlparams[0].Value = FileId;
                sqlparams[1] = new SqlParameter("@FileFrom", SqlDbType.Int);
                sqlparams[1].Value = FileFrom;
                res = objHelper.ExecuteNonQuery("SP_DeleteAttachement", sqlparams);
            }
            catch (Exception)
            {
                throw;
            }
            return res;
        }

        public DataTable Get_UploadedFilesListByDoctor(string FileIds, string ImageIds)
        {

            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] Patparameters = new SqlParameter[2];
                Patparameters[0] = new SqlParameter("@FileId", SqlDbType.NVarChar);
                Patparameters[0].Value = FileIds;
                Patparameters[1] = new SqlParameter("@ImageId", SqlDbType.NVarChar);
                Patparameters[1].Value = ImageIds;

                return objHelper.DataTable("SP_GetAttachMentDetails", Patparameters);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public DataTable GetFooterCity()
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] Sqlparams = new SqlParameter[0];
                return objHelper.DataTable("Usp_GetFooterCity", Sqlparams);
            }
            catch (Exception Ex)
            {
                InsertErrorLog("clsCommon--GetFooterCity", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public DataTable GetLocationOfDoctor(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] SearchUnivDetails = new SqlParameter[1];
                SearchUnivDetails[0] = new SqlParameter("@UserId", SqlDbType.Int);
                SearchUnivDetails[0].Value = UserId;
                dt = objHelper.DataTable("usp_getLocationIdOfDoctor", SearchUnivDetails);
                return dt;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool InsertupdateMailHistory(MailHistory Obj)
        {
            bool Result = false;
            try
            {
                SqlParameter[] sqlparams = new SqlParameter[3];
                sqlparams[0] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlparams[0].Value = Obj.UserId;
                sqlparams[1] = new SqlParameter("@PatientId", SqlDbType.Int);
                sqlparams[1].Value = Obj.PatientId;
                sqlparams[2] = new SqlParameter("@TemplateId", SqlDbType.Int);
                sqlparams[2].Value = Obj.TemplateId;
                return Result = objHelper.ExecuteNonQuery("Usp_InsertUpdateMailHistory", sqlparams);
            }
            catch (Exception Ex)
            {
                InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool IsPhoneNumber(string number)
        {
            return Regex.Match(number, @"\(?\d{3}\)?-? *\d{3}-? *-?\d{4}").Success;
        }
        public static DataTable ToDataTable<T>(List<T> items, bool IsYesNo = false)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                if (IsYesNo && type == typeof(bool))
                {
                    type = typeof(string);
                }
                dataTable.Columns.Add(prop.Name, type);

            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    if (Props[i].PropertyType == typeof(bool) && IsYesNo)
                    {
                        values[i] = Convert.ToBoolean(Props[i].GetValue(item, null)) ? "Y" : "N";
                    }
                    else
                    {
                        //inserting property values to datatable rows
                        values[i] = Props[i].GetValue(item, null);
                    }
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
        public static DataTable ToDataTableProviderSchedule<T>(List<T> items, bool IsYesNo = false)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                if (IsYesNo && type == typeof(bool))
                {
                    type = typeof(string);
                }
                if (prop.Name.ToLower() == "times")
                {
                    dataTable.Columns.Add("times", typeof(string));
                }
                else
                {
                    dataTable.Columns.Add(prop.Name, type);
                }
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    if (Props[i].PropertyType == typeof(bool) && IsYesNo)
                    {
                        values[i] = Convert.ToBoolean(Props[i].GetValue(item, null)) ? "Y" : "N";
                    }
                    else
                    {
                        if(Props[i].PropertyType == typeof(List<string>))
                        {
                            values[i] = GetValueWithComm(((List<string>)Props[i].GetValue(item)));
                        }
                        else
                        {
                            //inserting property values to datatable rows
                            values[i] = Props[i].GetValue(item, null);
                        }
                    }
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
        public static DataTable ToDataTableAppointments<T>(List<T> items, bool IsYesNo = false)
        {
            if (items == null)
            {
                return null;
            }
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                if (IsYesNo && type == typeof(bool))
                {
                    type = typeof(string);
                }

                if (type == typeof(NewPatientInfo)) // Executes only when appointments are converted to the datatable.
                {
                    dataTable.Columns.Add("PatientName", typeof(string));
                    dataTable.Columns.Add("Addresses", typeof(string));
                    dataTable.Columns.Add("phonenumbers", typeof(string));
                    dataTable.Columns.Add("EmailAdresses", typeof(string));
                } else if (prop.Name.ToLower() == "txprocs")
                {
                    dataTable.Columns.Add("TxProcs", typeof(string));
                }
                else if (prop.Name.ToLower() == "adacodes")
                {
                    dataTable.Columns.Add("AdaCodes", typeof(string));
                }
                else
                {
                    dataTable.Columns.Add(prop.Name, type);
                }
            }
            foreach (T item in items)
            {
                var values = new List<object>();
                //var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    if (Props[i].PropertyType == typeof(bool) && IsYesNo)
                    {
                        values.Add(Convert.ToBoolean(Props[i].GetValue(item, null)) ? "Y" : "N");
                    }
                    else if (Props[i].PropertyType == typeof(NewPatientInfo))
                    {
                        values.Add(GetPropValue(Props[i].GetValue(item), "PatientName"));
                        values.Add(DataTableToXml(ToDataTableAppointments<Addresses>((List<Addresses>)GetPropValue(Props[i].GetValue(item), "Addresses")), "Addresses"));
                        values.Add(DataTableToXml(ToDataTableAppointments<PhoneNumbers>((List<PhoneNumbers>)GetPropValue(Props[i].GetValue(item), "phonenumbers")), "phonenumbers"));
                        values.Add(GetValueWithComm<string>(((List<string>)GetPropValue(Props[i].GetValue(item), "EmailAdresses"))));
                    }
                    else
                    {
                        if (Props[i].Name.ToLower() == "txprocs")
                        {
                            values.Add(GetValueWithComm((List<int>)(Props[i].GetValue(item))));
                            //values.Add(GetValueWithComm<string>((List<string>)(Props[i].GetValue(item))));
                        }
                        else
                        if (Props[i].Name.ToLower() == "adacodes")
                        {
                            // values.Add(GetValueWithComm<string>(((List<string>)GetPropValue(Props[i].GetType(), "AdaCodes"))));
                            values.Add(GetValueWithComm((List<string>)Props[i].GetValue(item)));
                        }
                        else
                        {
                            //inserting property values to datatable rows
                            values.Add(Props[i].GetValue(item, null));
                        }
                    }
                }
                dataTable.Rows.Add(values.ToArray());
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        public static DataTable ToDataTablePatients<T>(List<T> items, bool IsYesNo = false)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                if (IsYesNo && type == typeof(bool))
                {
                    type = typeof(string);
                }
                if (prop.Name.ToLower() == "addresses")
                {
                    dataTable.Columns.Add("Addresses", typeof(string));
                }
                else if (prop.Name.ToLower() == "phonenumbers")
                {
                    dataTable.Columns.Add("PhoneNumbers", typeof(string));
                }
                else if (prop.Name.ToLower() == "emails")
                {
                    dataTable.Columns.Add("Emails", typeof(string));
                }
                else
                {
                    dataTable.Columns.Add(prop.Name, type);
                }

            }

            foreach (T item in items)
            {
                var values = new List<object>();
                //var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {

                    if (Props[i].PropertyType == typeof(bool) && IsYesNo)
                    {
                        values.Add(Convert.ToBoolean(Props[i].GetValue(item, null)) ? "Y" : "N");
                    }
                    else
                    {
                        if (Props[i].Name.ToLower() == "addresses")
                        {

                            values.Add(DataTableToXml(ToDataTable<Addresses>((List<Addresses>)Props[i].GetValue(item)), "Addresses"));

                            //values.Add(GetValueWithComm<string>((List<string>)(Props[i].GetValue(item)))); DataTableToXml
                        }
                        else if (Props[i].Name.ToLower() == "phonenumbers")
                        {
                            // values.Add(GetValueWithComm<string>(((List<string>)GetPropValue(Props[i].GetType(), "AdaCodes"))));
                            values.Add(DataTableToXml(ToDataTable<PhoneNumbers>((List<PhoneNumbers>)Props[i].GetValue(item)), "PhoneNumbers"));
                        }
                        else if (Props[i].Name.ToLower() == "emails")
                        {                         
                            values.Add(GetValueWithComm((List<string>)Props[i].GetValue(item)));
                        }
                        else
                        {
                            //inserting property values to datatable rows
                            values.Add(Props[i].GetValue(item, null));
                        }
                    }
                }

                dataTable.Rows.Add(values.ToArray());
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }



        public static object GetPropValue(object src, string propName)
        {
            if (src ==null)
            {
                return null;
            }
            return src.GetType().GetProperty(propName).GetValue(src, null);
        }

        public static string DataTableToXml(DataTable dt, string propName)
        {
            if (dt == null)
            {
                return null;
            }
            var ds = new DataSet(propName);
            ds.Tables.Add(dt);
            return ds.GetXml();
        }
        public static string GetValueWithComm<T>(List<T> ListParam)
        {
            try
            {
                if (ListParam ==null)
                {
                    return null;
                }
                string Strings;
                return Strings = string.Join(",", ListParam.ToArray());
            }
            catch (Exception Ex)
            {

                throw;
            }
        }

       



        public DataSet getMembers(int filterby, DateTime? Date = null)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@MethodType", SqlDbType.Int);
            param[0].Value = filterby;
            param[1] = new SqlParameter("@date", SqlDbType.Date);
            param[1].Value = Date;
            ds = objHelper.GetDatasetData("getApiData", param);
            return ds;
        }

        public DataSet getRewardAssocation(int accountId)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@AccountId", SqlDbType.Int);
            param[0].Value = accountId;
            ds = objHelper.GetDatasetData("USP_GetAccountRewardAssociationByAccountId", param);
            return ds;
        }

        public DataSet getLocationAssociation(int accountId)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[1];
            param[0] = new SqlParameter("@AccountId", SqlDbType.Int);
            param[0].Value = accountId;
            ds = objHelper.GetDatasetData("USP_getLocationAssociationByAccountId", param);
            return ds;
        }

        public string GetStoredProcName(int flag, string spReward, string spSuper)
        {
            string _storedProcName = string.Empty;
            switch (flag)
            {
                case (int)APIFilter.FilterFlag.IsRewardPlatform:
                    _storedProcName = spReward;
                    break;
                case (int)APIFilter.FilterFlag.IsSuperBucks:
                    _storedProcName = spSuper;
                    break;
                default:
                    _storedProcName = spReward;
                    break;
            }
            return _storedProcName;
        }

        public DataSet getSUPAccountAssociation(int accountId, int? patientid)
        {
            DataSet ds = new DataSet();
            SqlParameter[] param = new SqlParameter[2];
            param[0] = new SqlParameter("@AccountId", SqlDbType.Int);
            param[0].Value = accountId;
            param[1] = new SqlParameter("@PatientId", SqlDbType.Int);
            param[1].Value = patientid;
            ds = objHelper.GetDatasetData("USP_SUPAccountAssociationByAccountId", param);
            return ds;
        }
        public static DataTable GetDentistDetailsByAccessToken(string accesstoken)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@access_token", SqlDbType.NVarChar);
                param[0].Value = accesstoken;
                dt = new clsHelper().DataTable("Usp_GetDentistDetailsByAccessToken", param);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsComman--GetDentistDetailsByAccessToken", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable GetDentistDetailsByUserId(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@UserId", SqlDbType.Int);
                param[0].Value = UserId;
                dt = new clsHelper().DataTable("Usp_DentistLoginWithUserId", param);
                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsComman--GetDentistDetailsByUserId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DataTable GetRecordsFromTempTables(int PatientId, string viewstatvalue)
        {
            try
            {
                SqlParameter[] Getuploadfiles = new SqlParameter[2];
                Getuploadfiles[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                Getuploadfiles[0].Value = PatientId;
                Getuploadfiles[1] = new SqlParameter("@Guid", SqlDbType.VarChar);
                Getuploadfiles[1].Value = viewstatvalue;
                return new clsHelper().DataTable("GetRecordsFromTempTablespatient", Getuploadfiles);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("clsComman--GetDentistDetailsByUserId", Ex.Message, Ex.StackTrace);
                throw Ex;
            }
        }

        /// <summary>
        /// Safe Value use for check DBNULL error.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T SafeValue<T>(object value, bool IsAllowNull = false)
        {
            if (value == DBNull.Value || value == null)
            {
               if(typeof(T) == typeof(string))
                {
                    if (IsAllowNull)
                    {
                        return default(T);
                    }
                    else
                    {
                        return (T)Convert.ChangeType(string.Empty, typeof(T));
                    }
                }
               else
                {
                    return default(T);
                }
            }
            else if (value == null)
            {
                return (T)Convert.ChangeType(value, typeof(T));
            }
            if (typeof(T).IsEnum)
            {
                return (T)Enum.Parse(typeof(T), value.ToString());
            }
            if (value.GetType() == typeof(StringBuilder))
            {
                value = value.ToString();
            }
            if (value.GetType() == typeof(Guid))
            {
                return (T)Convert.ChangeType(Convert.ToString(value), typeof(T));
            }
            //else if (value.GetType() == typeof(StringBuilder) && typeof(T) == typeof(string))
            //{
            //    return (T)Convert.ChangeType(value.ToString(), typeof(T));
            //}
            if (value.GetType() == typeof(byte[]))
            {
                return (T)Convert.ChangeType(Convert.ToString(value), typeof(T));
            }
            else
            {
                return (T)Convert.ChangeType(value, typeof(T));
            }
        }

        /// <summary>
        /// Safe Search avoid ' from Search text and replace with ''
        /// </summary>
        /// <param name="Text"></param>
        /// <returns></returns>
        public static string SafeSearch(string Text)
        {
            if (!string.IsNullOrWhiteSpace(Text))
            {
                if (Text.Contains("'"))
                {
                    return "''";
                }
                else
                {
                    return Text;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Safe Date Time return defult value whenever parameter contains string.empty values. 
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public static DateTime SafeDateTime(object parm)
        {
            if (parm.GetType() == typeof(string))
            {
                if (!string.IsNullOrWhiteSpace(Convert.ToString(parm)))
                {
                    return Convert.ToDateTime(parm);
                }
                else
                {
                    return DateTime.MinValue;
                }
            }
            else
            {
                return Convert.ToDateTime(parm);
            }
        }

        /// <summary>
        /// SAFE Boolean value return defult value whenever parameter contains empty values.
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public static Boolean SafeBoolean(object parm)
        {
            if (parm.GetType() == typeof(string))
            {
                if (!string.IsNullOrWhiteSpace(Convert.ToString(parm)))
                {
                    return Convert.ToBoolean(parm);
                }
                else
                {
                    return default(Boolean);
                }
            }
            else
            {
                return Convert.ToBoolean(parm);
            }
        }

        /// <summary>
        /// SAFE Integer value return defult value whenever parameter contains empty values.
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public static int SafeInteger(object parm)
        {
            if (parm.GetType() == typeof(string))
            {
                if (!string.IsNullOrWhiteSpace(Convert.ToString(parm)))
                {
                    return Convert.ToInt32(parm);
                }
                else
                {
                    return default(int);
                }
            }
            else
            {
                return Convert.ToInt32(parm);
            }
        }

        /// <summary>
        /// SAFE Decimal value return defult value whenever parameter contains empty values.
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public static Decimal SafeDeciaml(object parm)
        {
            if (parm.GetType() == typeof(string))
            {
                if (!string.IsNullOrWhiteSpace(Convert.ToString(parm)))
                {
                    return Convert.ToDecimal(parm);
                }
                else
                {
                    return default(decimal);
                }
            }
            else
            {
                return Convert.ToDecimal(parm);
            }
        }

        /// <summary>
        /// SAFE string value return defult value whenever parameter contains empty values.
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public static string SafeString(object parm)
        {
            if (parm.GetType() == typeof(string))
            {
                if (!string.IsNullOrWhiteSpace(Convert.ToString(parm)))
                {
                    return Convert.ToString(parm);
                }
                else
                {
                    return default(string);
                }
            }
            else
            {
                return Convert.ToString(parm);
            }
        }

        /// <summary>
        /// Check Email Domain
        /// </summary>
        /// <param name="strEmail"></param>
        /// <returns></returns>
        public static bool checkEmailDomain(string strEmail)
        {
            bool result = false;
            if (!string.IsNullOrWhiteSpace(strEmail) && !strEmail.Contains("@domain.com"))
            {
                result = true;
            }
            return result;
        }


    }
    public static class ConvertorHelper
    {
        public static List<T> DataTableToList<T>(this DataTable table) where T : class, new()
        {
            try
            {
                List<T> list = new List<T>();

                foreach (var row in table.AsEnumerable())
                {
                    T obj = new T();

                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        try
                        {
                            PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                            propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                        }
                        catch
                        {
                            continue;
                        }
                    }

                    list.Add(obj);
                }

                return list;
            }
            catch
            {
                return null;
            }
        }

    }


   






}
