﻿using BO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlClient;
using DataAccessLayer.Common;

namespace DataAccessLayer
{
    public class ZipwhipDAL
    {
        clsHelper clshelper;
        clsCommon objcommon = new clsCommon();
        public ZipwhipDAL()
        {
            clshelper = new clsHelper();
        }

        /// <summary>
        /// Insert zipwhip provider config details in SMSIntegration table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int InsertProviderConfig(ZipwhipAccount model)
        {
            string result = "0";
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[5];
                sqlpara[0] = new SqlParameter("@Id", SqlDbType.Int);
                sqlpara[0].Value = model.RefId;
                sqlpara[1] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlpara[1].Value = model.UserId;
                sqlpara[2] = new SqlParameter("@ProviderConfig", SqlDbType.VarChar);
                sqlpara[2].Value = model.ProviderConfigJson;
                sqlpara[3] = new SqlParameter("@LocationId", SqlDbType.Int);
                sqlpara[3].Value = model.LocationId;
                sqlpara[4] = new SqlParameter("@PhoneNumber", SqlDbType.NVarChar);
                sqlpara[4].Value = model.ProviderConfig.username;
                result = clshelper.ExecuteScalar("Insert_Provider_Config", sqlpara);
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("ZipwhipDAL--InsertProviderConfig", ex.Message, ex.StackTrace);
            }
            return Convert.ToInt32(result);
        }

        /// <summary>
        /// Insert zipwhip provider config details in SMSIntegration table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public DataTable ListOfProviderConfig(int UserId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[1];
                sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlpara[0].Value = UserId;
                dt = clshelper.DataTable("GetZipwhipSMSIntegrationList", sqlpara);
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("ZipwhipDAL--ListOfProviderConfig", ex.Message, ex.StackTrace);
            }
            return dt;
        }

        public int SendMessage(SMSDetails model)
        {
            string result = "0";
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter() {ParameterName = "@Id",Value= model.Id },
                    new SqlParameter() {ParameterName = "@SenderId",Value= model.SenderId },
                    new SqlParameter() {ParameterName = "@SenderType",Value= model.SenderType },
                    new SqlParameter() {ParameterName = "@ReceiverId",Value= model.ReceiverId },
                    new SqlParameter() {ParameterName = "@ReceiverType",Value= model.ReceiverType },
                    new SqlParameter() {ParameterName = "@RecepientNumber",Value= model.RecepientNumber },
                    new SqlParameter() {ParameterName = "@MessageBody",Value= model.MessageBody },
                    new SqlParameter() {ParameterName = "@Status",Value= model.Status },
                    new SqlParameter() {ParameterName = "@Response",Value= model.ResponseBody },
                    new SqlParameter() {ParameterName = "@MessageType",Value= model.MessageType },
                    new SqlParameter() {ParameterName = "@SMSIntegrationMasterId",Value= model.SMSIntegrationMasterId }
                 };
                result = clshelper.ExecuteScalar("Usp_InsertUpdateSMSHistory", sqlpara);
                //SqlParameter[] sqlpara = new SqlParameter[4];
                //sqlpara[0] = new SqlParameter("@Id", SqlDbType.Int);
                //sqlpara[0].Value = model.Id;
                //sqlpara[1] = new SqlParameter("@SenderId", SqlDbType.Int);
                //sqlpara[1].Value = model.SenderId;
                //sqlpara[2] = new SqlParameter("@SenderType", SqlDbType.NVarChar);
                //sqlpara[2].Value = model.SenderType;
                //sqlpara[3] = new SqlParameter("@ReceiverId", SqlDbType.Int);
                //sqlpara[3].Value = model.ReceiverId;
                //sqlpara[4] = new SqlParameter("@ReceiverType", SqlDbType.NVarChar);
                //sqlpara[4].Value = model.ReceiverType;
                //sqlpara[5] = new SqlParameter("@RecepientNumber", SqlDbType.NVarChar);
                //sqlpara[5].Value = model.RecepientNumber;
                //sqlpara[6] = new SqlParameter("@MessageBody", SqlDbType.NVarChar);
                //sqlpara[6].Value = model.ResponseBody;
                //sqlpara[7] = new SqlParameter("@Status", SqlDbType.Int);
                //sqlpara[7].Value = model.Status;
                //sqlpara[8] = new SqlParameter("@Response", SqlDbType.NVarChar);
                //sqlpara[8].Value = model.ResponseBody;
                //sqlpara[9] = new SqlParameter("@MessageType", SqlDbType.Int);
                //sqlpara[9].Value = model.MessageType;

            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("ZipwhipDAL--SendMessage", ex.Message, ex.StackTrace);
            }
            return Convert.ToInt32(string.IsNullOrWhiteSpace(result)?"0": result);
        }

        /// <summary>
        /// Insert zipwhip provider config details in SMSIntegration table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public DataTable ListOfSMSHistory(int UserId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[1];
                sqlpara[0] = new SqlParameter("@UserId", SqlDbType.Int);
                sqlpara[0].Value = UserId;
                dt = clshelper.DataTable("GetZipwhipSMSHistoryList", sqlpara);
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("ZipwhipDAL--ListOfSMSHistory", ex.Message, ex.StackTrace);
            }
            return dt;
        }

        public int FindDentistId(string PhoneNumber, string Type)
        {
            string result = "0";
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter() {ParameterName = "@PhoneNumber",Value= PhoneNumber },
                    new SqlParameter() {ParameterName = "@Type",Value= Type }
                 };
                result = clshelper.ExecuteScalar("GetDentistIdUsingPhoneNumber", sqlpara);
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("ZipwhipDAL--FindDentistId", ex.Message, ex.StackTrace);
            }
            return Convert.ToInt32(string.IsNullOrWhiteSpace(result) ? "0" : result);
        }

        public DataTable AssignZipwhipToken()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = clshelper.DataTable("Get_without_Token_UserList");
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("ZipwhipDAL--AssignZipwhipToken", ex.Message, ex.StackTrace);
            }
            return dt;
        }

        public void UpdateZipwhipToken(string ProviderConfigJson, int Id)
        {
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter() {ParameterName = "@ProviderConfigJson",Value= ProviderConfigJson },
                    new SqlParameter() {ParameterName = "@Id",Value= Id }
                 };
                clshelper.ExecuteNonQuery("Update_Zipwhip_ProviderConfigJson", sqlpara);
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("ZipwhipDAL--UpdateZipwhipToken", ex.Message, ex.StackTrace);
            }
        }

        public DataTable CheckZipwhipAccountActivated(int UserId, int LocationId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] sqlpara = new SqlParameter[]
                {
                    new SqlParameter() {ParameterName = "@UserId",Value= UserId },
                    new SqlParameter() {ParameterName = "@LocationId",Value= LocationId }
                 };
                dt = clshelper.DataTable("Check_Zipwhip_Account_Activated", sqlpara);
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("ZipwhipDAL--CheckZipwhipAccountActivated", ex.Message, ex.StackTrace);
            }
            return dt;
        }

        public DataTable GetDentistLocationList(int UserId)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Userid", SqlDbType.Int);
                strParameter[0].Value = UserId;

                dt = clshelper.DataTable("Usp_GetLocationOfDoctorbyUserId", strParameter);
                return dt;
            }
            catch (Exception ex)
            {
                objcommon.InsertErrorLog("ZipwhipDAL--GetDentistLocationList", ex.Message, ex.StackTrace);
            }
            return dt;
        }


    }
}
