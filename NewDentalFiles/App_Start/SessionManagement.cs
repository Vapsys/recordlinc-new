﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using DentalFiles.Models;
using System.Configuration;

namespace DentalFiles.App_Start
{
    public class SessionManagement
    {
        CommonDAL objCommonDAL = new CommonDAL();
        DataTable dt = new DataTable();

        

        public static int PatientId
        {

           
            get { return Convert.ToInt32(HttpContext.Current.Session["PatientId"]); }
            set { HttpContext.Current.Session["PatientId"] = value; }
        }

        public static int UserId
        {

            get { return Convert.ToInt32(HttpContext.Current.Session["UserId"]); }
            set { HttpContext.Current.Session["UserId"] = value; }
        }

        public static string CompanyName
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyName"]); }
            set { HttpContext.Current.Session["CompanyName"] = value; }
        }

        public static string CompanyAddress
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyAddress"]); }
            set { HttpContext.Current.Session["CompanyAddress"] = value; }
        }

        public static string CompanyCity
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyCity"]); }
            set { HttpContext.Current.Session["CompanyCity"] = value; }
        }

        public static string CompanyState
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyState"]); }
            set { HttpContext.Current.Session["CompanyState"] = value; }
        }

        public static string CompanyZipCode
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyZipCode"]); }
            set { HttpContext.Current.Session["CompanyZipCode"] = value; }
        }

        public static string CompanyId
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyId"]); }
            set { HttpContext.Current.Session["CompanyId"] = value; }
        }
        public static string LogoPath
        {
            get { return Convert.ToString(HttpContext.Current.Session["LogoPath"]); }
            set { HttpContext.Current.Session["LogoPath"] = value; }
        }

        public static string CompanyWebsite
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyWebsite"]); }
            set { HttpContext.Current.Session["CompanyWebsite"] = value; }
        }

        public static string CompanySalesEmail
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanySalesEmail"]); }
            set { HttpContext.Current.Session["CompanySalesEmail"] = value; }
        }

        public static string CompanySupportEmail
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanySupportEmail"]); }
            set { HttpContext.Current.Session["CompanySupportEmail"] = value; }
        }

        public static string PhoneNo
        {
            get { return Convert.ToString(HttpContext.Current.Session["PhoneNo"]); }
            set { HttpContext.Current.Session["PhoneNo"] = value; }
        }

        public static string CompanyOwnerName
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyOwnerName"]); }
            set { HttpContext.Current.Session["CompanyOwnerName"] = value; }
        }

        public static string CompanyOwnerTitle
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyOwnerTitle"]); }
            set { HttpContext.Current.Session["CompanyOwnerTitle"] = value; }
        }

        public static string CompanyOwnerEmail
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyOwnerEmail"]); }
            set { HttpContext.Current.Session["CompanyOwnerEmail"] = value; }
        }

        public static string CompanyFacebook
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyFacebook"]); }
            set { HttpContext.Current.Session["CompanyFacebook"] = value; }
        }

        public static string CompanyBlog
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyBlog"]); }
            set { HttpContext.Current.Session["CompanyBlog"] = value; }
        }

        public static string CompanyYoutube
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyYoutube"]); }
            set { HttpContext.Current.Session["CompanyYoutube"] = value; }
        }

        public static string CompanyLinkedin
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyLinkedin"]); }
            set { HttpContext.Current.Session["CompanyLinkedin"] = value; }
        }

        public static string CompanyTwitter
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyTwitter"]); }
            set { HttpContext.Current.Session["CompanyTwitter"] = value; }
        }

        public static string CompanyGooglePlus
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyGooglePlus"]); }
            set { HttpContext.Current.Session["CompanyGooglePlus"] = value; }
        }

        public static string CompanyFacebookLike
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyFacebookLike"]); }
            set { HttpContext.Current.Session["CompanyFacebookLike"] = value; }
        }


        public static string CompanyAlexa
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyAlexa"]); }
            set { HttpContext.Current.Session["CompanyAlexa"] = value; }
        }

        public static string CompanyPinterest
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyPinterest"]); }
            set { HttpContext.Current.Session["CompanyPinterest"] = value; }
        }

        public static string CompanyVimeo
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyVimeo"]); }
            set { HttpContext.Current.Session["CompanyVimeo"] = value; }
        }

        public static string CompanyGoogle
        {
            get { return Convert.ToString(HttpContext.Current.Session["CompanyGoogle"]); }
            set { HttpContext.Current.Session["CompanyGoogle"] = value; }
        }

        public static string TimeZoneSystemName
        {
            get { return Convert.ToString(HttpContext.Current.Session["TimeZoneSystemName"]); }
            set { HttpContext.Current.Session["TimeZoneSystemName"] = value; }
        }


    }
}