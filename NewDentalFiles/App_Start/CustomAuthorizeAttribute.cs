﻿using DentalFiles.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DentalFilesNew.App_Start
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (SessionManagement.PatientId == 0 && Convert.ToInt32(SessionManagement.PatientId) == 0)
            {

                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    var urlHelper = new UrlHelper(filterContext.RequestContext);
                    filterContext.HttpContext.Response.StatusCode = 403;
                    filterContext.Result = new JsonResult
                    {
                        Data = new
                        {
                            Error = "NotAuthorized",
                            LogOnUrl = urlHelper.Action("Index","Index")
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                else
                {
                    filterContext.Result = new RedirectToRouteResult(new
                                            RouteValueDictionary(new { controller = "Index", action = "Index" }));
                    //base.OnAuthorization(filterContext); 
                    filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
                }
            }

        }
    }
  
}