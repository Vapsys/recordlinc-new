﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentalFiles.Models;

namespace DentalFiles.Controllers
{
    public class _AdvancedSearchCollController : Controller
    {
        //
        // GET: /_AdvancedSearchColl/
        CommonDAL objCommonDAL = new CommonDAL();
        [ChildActionOnly]
        public ActionResult Index()
        {
            ViewBag.Sending = objCommonDAL.GetSpeciality();
            ViewBag.Distance = objCommonDAL.GetDistance();
            return PartialView();
        }

        //
        // GET: /_AdvancedSearchColl/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /_AdvancedSearchColl/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /_AdvancedSearchColl/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /_AdvancedSearchColl/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /_AdvancedSearchColl/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /_AdvancedSearchColl/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /_AdvancedSearchColl/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


     
    }
}
