﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentalFiles.Models.ViewModel;
using JQueryDataTables.Models.Repository;
using DentalFiles.App_Start;
namespace DentalFiles.Controllers
{
    public class PrintChildDentalMedicalHistoryController : Controller
    {
        //
        // GET: /PrintChildDentalMedicalHistory/
        DataRepository objRepository = new DataRepository();
        public ActionResult Index()
        {
            if (Request.QueryString["PatientId"] != null && Convert.ToInt32(Request.QueryString["PatientId"]) != 0)
            {
                SessionManagement.PatientId =Convert.ToInt32(  Request.QueryString["PatientId"]);
            }
            var model = new DentalFormsMaster();
            model.ChildDentalMEDICALHISTORY = objRepository.GetChildHistory(Convert.ToInt32(SessionManagement.PatientId));

            return PartialView(model);
        }

    }
}
