﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using DentalFiles.Models;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Configuration;
using System.Text.RegularExpressions;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.PatientsData;
using DataAccessLayer.Common;
using DataAccessLayer.AppointmentData;
using DentalFilesNew.Models;
namespace DentalFiles.Controllers
{
    public class MyDentalServiceController : Controller
    {

        CommonDAL objCommonDAL = new CommonDAL();
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        private clsAppointment objAppointment;
        DataTable dt = new DataTable();
        APIResponse objResponse = new APIResponse { IsSuccess = true, StatusMessage = string.Empty, ResultCode = 200 };
        bool status = false;
        string DentalPath = Convert.ToString(ConfigurationManager.AppSettings.Get("DentistImagespath"));
        string DoctorImage = Convert.ToString(ConfigurationManager.AppSettings.Get("Doctorimage1"));

        /// <summary>
        /// Method Name :RegisterUser
        /// Desciprtion :This Method Use for Sign up new Patient
        /// Request : Email,Password,FirstName,Lastname,Phone
        /// Response : {Thank you for registration. Admin will contact you shortly}
        /// </summary>
        [HttpPost]
        public string RegisterUser(string Email, string FirstName, string Lastname, string Phone, int MagicNumber)
        {
            int Result = 0;
            string MethodName = "RegisterUser";
            string Password = objCommonDAL.CreateRandomPassword(4);
            string json = "";
            try
            {
                DataTable dtPatientEmail = objCommonDAL.CheckPatientEmail(Email);
                if (dtPatientEmail == null || dtPatientEmail.Rows.Count == 0)
                {
                    Result = Convert.ToInt32(objCommonDAL.AddPatient("", FirstName, "", Lastname, 0, "", "", Phone, Email, Password, "", "", "", "", "", "", "", 55, 0, 0, 0, "", "", "", "", "", false, ""));
                    if (Result != 0)
                    {
                        objCommonDAL.SignUpEMail(Email, Password);
                        string Sucess = "Thank you for registration.";
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.StatusMessage = Sucess;
                        objResponse.ResultDetail = string.Empty;
                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                    }
                    else
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "Failed to Signup";
                        objResponse.ResultDetail = string.Empty;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                }
                else
                {
                    DataTable dtGetPass = new DataTable();
                    dtGetPass = objCommonDAL.GetPatientPassword(Email);
                    if (dtGetPass != null && dtGetPass.Rows.Count > 0)
                    {

                        string recoverPassword = Convert.ToString(dtGetPass.Rows[0]["Password"].ToString());
                        string recoverPatientFirstName = Convert.ToString(dtGetPass.Rows[0]["FirstName"].ToString());
                        string recoverPatientLastName = Convert.ToString(dtGetPass.Rows[0]["LastName"].ToString());
                        string recoverPatientEmail = Convert.ToString(Email);

                        //Send Password To Patient Email
                        objCommonDAL.SendPatientPassword(recoverPatientEmail, recoverPassword, recoverPatientFirstName, recoverPatientLastName);

                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.StatusMessage = "This email address already exists.  We have emailed you a password reminder.";
                        objResponse.ResultDetail = string.Empty;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 403;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "Failed to send password reminder.";
                        objResponse.ResultDetail = string.Empty;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }

                }

                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :AuthenticateUser
        /// Desciprtion :This Method Use for Authenticate Patient
        /// Request : UserName,Password
        /// Response : {4c20456f-e177-4104-b11e-63b936381a01}
        /// </summary>
        [HttpPost]
        public string AuthenticateUser(string UserName, string Password, int MagicNumber)
        {
            string json = "";
            string MethodName = "AuthenticateUser";
            objCommonDAL.GetActiveCompany();
            try
            {

                dt = objCommonDAL.PatientLoginform(UserName, Password);

                if (dt != null && dt.Rows.Count > 0)
                {
                    Session["PatientId"] = Convert.ToString(dt.Rows[0]["PatientId"]);
                    Session["PatientFullName"] = Convert.ToString(dt.Rows[0]["FirstName"] + " " + dt.Rows[0]["LastName"]);
                    Session["SessionCode"] = Guid.NewGuid().ToString();


                    dynamic dResult = new { SessionCode = Convert.ToString(Session["SessionCode"]) };

                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultDetail = dResult;


                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 402;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Invalid username or password.";
                    objResponse.ResultDetail = string.Empty;
                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }

                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }

        }

        /// <summary>
        /// Method Name :Inbox
        /// Desciprtion :This Method Use for Get Messages of Patient
        /// Request : SessionCode,pageindex,pagesize
        /// Response : {SenderId : 672,SenderName : test,Subject : hi this is test,MessageBody : testtest,Date : Oct 21 2013  4:16PM,MessageId : 32,ReadFlag : True }
        /// </summary>
        [HttpPost]
        public string Inbox(string SessionCode, int PageIndex, int PageSize, int MagicNumber)
        {
            string json = "";
            string MethodName = "Inbox";

            DataTable dtInbox = new DataTable();

            try
            {
                if (IsSessionExpired(SessionCode))
                {

                    List<Inbox> Messages = new List<Inbox>();

                    int patientid = Convert.ToInt32(Session["PatientId"]);
                    dtInbox = objCommonDAL.GetAllMessages(patientid, 0, PageIndex, PageSize);
                    int TotalNumberOfPages = 0;
                    int TotalRecord = 0;
                    if (dtInbox != null && dtInbox.Rows.Count > 0)
                    {


                        foreach (DataRow item in dtInbox.Rows)
                        {
                            Inbox objInbox = new Inbox();
                            DataTable dtAttachment = new DataTable();

                            objInbox.SenderId = Convert.ToInt32(item["DoctorId"]);
                            objInbox.SenderName = objCommonDAL.CheckNull(Convert.ToString(item["fromfield"]), string.Empty);
                            //string FirstLine = Regex.Replace(Regex.Replace(objCommonDAL.CheckNull(objCommonDAL.RemoveHTML(Convert.ToString(item["body"])), string.Empty), @"<[^>]+>|&nbsp;", string.Empty), @"[\r\n\t]+", string.Empty);
                            string FirstLine = Regex.Replace(Regex.Replace(objCommonDAL.CheckNull(objCommonDAL.RemoveHTML(HttpUtility.UrlDecode(Convert.ToString(item["Body"]))), string.Empty), @"<[^>]+>|&nbsp;", string.Empty), @"[\r\n\t]+", string.Empty);
                            objInbox.MessageBodyFirstLine = FirstLine;
                            if (objInbox.MessageBodyFirstLine.Length > 25)
                            {
                                objInbox.MessageBodyFirstLine = objInbox.MessageBodyFirstLine.Substring(0, 25);
                            }
                            objInbox.Date = objCommonDAL.CheckNull(Convert.ToString(item["DateForAPI"]), string.Empty);
                            objInbox.MessageId = Convert.ToInt32(item["patientmessageid"]);


                            objInbox.ReadFlag = Convert.ToBoolean(item["Read_flag"]);

                            dtAttachment = objCommonDAL.GetMessageAttachmentByPatientmessageId(Convert.ToInt32(item["patientmessageid"]));
                            if (dtAttachment != null && dtAttachment.Rows.Count > 0)
                            {
                                objInbox.HasAttachment = true;
                            }

                            Messages.Add(objInbox);


                        }
                        TotalRecord = Convert.ToInt32(dtInbox.Rows[0]["TotalRecord"]);
                        if (TotalRecord < PageSize)
                        {
                            TotalNumberOfPages = 1;
                        }
                        else
                        {
                            TotalNumberOfPages = (TotalRecord % PageSize == 0 ? (TotalRecord / PageSize) : (TotalRecord / PageSize) + 1);
                        }

                        if (TotalRecord <= PageSize)
                        {
                            PageSize = TotalRecord;
                        }
                        dynamic dResult = new { PageIndex, PageSize, TotalNumberOfPages, Messages };
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = dResult;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "No Messages Found.";
                        objResponse.ResultDetail = string.Empty;
                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                    }



                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;
                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }

                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;

            }

        }

        /// <summary>
        /// Method Name :ReadMessage
        /// Desciprtion :This Method Use for Read Message of Patient
        /// Request : SessionCode,Messageid
        /// Response : {SenderId : 672,MessageBody : testtest }
        /// </summary>
        [HttpPost]
        public string ReadMessage(string SessionCode, int MessageId, int MagicNumber)
        {
            string json = "";
            string MethodName = "ReadMessage";

            DataTable dtInbox = new DataTable();
            DataTable dtAttachment = new DataTable();
            bool Result = false;

            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    ReadMessage Message = new ReadMessage();
                    List<Attachments> Attachments = new List<Attachments>();
                    dtInbox = objCommonDAL.GetAllMessages(0, MessageId, 1, 100000);
                    Result = objCommonDAL.ReadFlagUpdate(0, MessageId);
                    dtAttachment = objCommonDAL.GetMessageAttachmentByPatientmessageId(MessageId);
                    if (dtInbox != null && dtInbox.Rows.Count > 0)
                    {

                        Message.MessageId = Convert.ToInt32(dtInbox.Rows[0]["patientmessageid"]);
                        //Message.MessageBody = objCommonDAL.CheckNull(Convert.ToString(dtInbox.Rows[0]["body"]), string.Empty);
                        //Message.MessageBody = Regex.Replace(Regex.Replace(objCommonDAL.CheckNull(objCommonDAL.RemoveHTML(HttpUtility.UrlDecode(Convert.ToString(dtInbox.Rows[0]["body"]))), string.Empty), @"<[^>]+>|&nbsp;", string.Empty), @"[\r\n\t]+", string.Empty);
                        Message.MessageBody = HttpUtility.UrlDecode(Convert.ToString(dtInbox.Rows[0]["body"]));


                        if (dtAttachment != null && dtAttachment.Rows.Count > 0)
                        {
                            foreach (DataRow item in dtAttachment.Rows)
                            {

                                Attachments objAttachments = new Attachments();
                                string AttachmentName = "";
                                string Type = "";
                                Message.AttachmentsCount = objCommonDAL.CheckNull(Convert.ToString(dtAttachment.Rows[0]["TotalRecord"]), string.Empty);
                                objAttachments.AttachmentId = Convert.ToInt32(item["Id"]);

                                AttachmentName = objCommonDAL.CheckNull(Convert.ToString(item["AttachmentName"]), string.Empty);
                                AttachmentName = AttachmentName.Substring(AttachmentName.ToString().LastIndexOf("$") + 1);

                                objAttachments.AttachmentName = AttachmentName;
                                Type = Path.GetExtension(AttachmentName);
                                Type = Type.Replace(".", "");
                                objAttachments.AttachmentType = Type;

                                objAttachments.AttachmentUrl = "http://www.mydentalfiles.com" + "/ImageBank/" + objCommonDAL.CheckNull(Convert.ToString(item["AttachmentName"]), string.Empty);
                                // objAttachments.AttachmentUrl = "http://192.168.1.13:1250" + "/ImageBank/" + objCommonDAL.CheckNull(Convert.ToString(item["AttachmentName"]), string.Empty);


                                Attachments.Add(objAttachments);
                                // Message.
                                Message.Attachments.Add(objAttachments);

                            }
                        }



                        dynamic dResult = new { Message };
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = dResult;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "No Message Found.";
                        objResponse.ResultDetail = string.Empty;
                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                    }

                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;
                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }
                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }

        }

        /// <summary>
        /// Method Name :ComposeMessage
        /// Desciprtion :This Method Use for Compose Message of Patient
        /// Request : SessionCode,DoctorId,Doctorname,subject,messagebody
        /// Response : { Message send successfully }
        /// </summary>
        [HttpPost]
        public string ComposeMessage(string SessionCode, string Recipients, string Subject, string MessageBody, int MagicNumber, string Attachments)
        {
            string json = "";
            string MethodName = "ComposeMessage";

            string Doctorname = "";
            try
            {


                DataTable dtInbox = new DataTable();

                List<Recipients> lstRecepients = new List<Recipients>();
                lstRecepients = (List<Recipients>)JsonConvert.DeserializeObject(Recipients, typeof(List<Recipients>));

                List<MessageAttachments> listAttachments = new List<MessageAttachments>();
                if (!string.IsNullOrEmpty(Attachments))
                {
                    listAttachments = (List<MessageAttachments>)JsonConvert.DeserializeObject(Attachments, typeof(List<MessageAttachments>));
                }


                if (IsSessionExpired(SessionCode))
                {

                    foreach (var item in lstRecepients)
                    {
                        int id = 0;

                        DataSet ds = new DataSet();
                        ds = ObjColleaguesData.GetDoctorDetailsById(Convert.ToInt32(item.RecipientId));
                        if (ds.Tables != null && ds.Tables.Count > 0)
                        {
                            Doctorname = objCommonDAL.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FullName"]), string.Empty);
                        }


                        id = objCommonDAL.Insert_PatientMessages(Convert.ToString(Session["PatientFullName"]), Doctorname, Subject, MessageBody, Convert.ToInt32(item.RecipientId), Convert.ToInt32(Session["PatientId"]), false, false, true);
                        if (id > 0)
                        {


                            foreach (var Nesteditem in listAttachments)
                            {
                                DataTable dtfileupload = new DataTable();

                                if (Nesteditem.RecordStatus == 1) //-- Upload from gallery OR camera
                                {
                                    if (Nesteditem.AttachmentType.ToLower() != "jpg" && Nesteditem.AttachmentType.ToLower() != "jpeg" && Nesteditem.AttachmentType.ToLower() != "gif" && Nesteditem.AttachmentType.ToLower() != "png" && Nesteditem.AttachmentType.ToLower() != "bmp" && Nesteditem.AttachmentType.ToLower() != "psd" && Nesteditem.AttachmentType.ToLower() != "pspimage" && Nesteditem.AttachmentType.ToLower() != "thm" && Nesteditem.AttachmentType.ToLower() != "tif" && Nesteditem.AttachmentType.ToLower() != "yuv")
                                    {
                                        dtfileupload = objCommonDAL.Get_UploadedFiles(Convert.ToInt32(Nesteditem.AttachmentId));
                                    }
                                    else
                                    {

                                        dtfileupload = objCommonDAL.Get_UploadedImages(Convert.ToInt32(Nesteditem.AttachmentId));
                                    }


                                    if (dtfileupload != null && dtfileupload.Rows.Count > 0)
                                    {
                                        foreach (DataRow FileUpload in dtfileupload.Rows)
                                        {
                                            string DocName = Convert.ToString(FileUpload["DocumentName"]);
                                            DocName = DocName.Substring(DocName.ToString().LastIndexOf("$") + 1);
                                            string NewFileName = Convert.ToString("$" + System.DateTime.Now.Ticks + "$" + DocName);
                                            string Extension = Path.GetExtension(NewFileName);
                                            DocName = Convert.ToString(FileUpload["DocumentName"]);
                                            DocName = DocName.Replace("../TempUploads/", "");

                                            string sourceFile = Server.MapPath(ConfigurationManager.AppSettings.Get("TempUploadsFolderPath")) + DocName;

                                            string tempPath = System.Configuration.ConfigurationManager.AppSettings["imagebankpath"];
                                            string destinationFile = Server.MapPath(tempPath);
                                            string DestinationFilePath = destinationFile;
                                            destinationFile = destinationFile + NewFileName;


                                            if (System.IO.File.Exists(sourceFile))
                                            {
                                                System.IO.File.Copy(sourceFile, destinationFile);
                                                System.IO.File.Delete(sourceFile);
                                                if (Extension.ToLower() == ".jpg" || Extension.ToLower() == ".jpeg" || Extension.ToLower() == ".gif" || Extension.ToLower() == ".png" || Extension.ToLower() == ".bmp" || Extension.ToLower() == ".psd" || Extension.ToLower() == ".pspimage" || Extension.ToLower() == ".thm" || Extension.ToLower() == ".tif")
                                                {
                                                    string ThubimagePath = Server.MapPath("~/" + System.Configuration.ConfigurationManager.AppSettings["FolderPathThumb"]);

                                                    objCommonDAL.GenerateThumbnailImage(DestinationFilePath, NewFileName, ThubimagePath);
                                                }

                                            }

                                            bool Attach = objCommonDAL.InsertMessageAttachemnts(id, NewFileName);

                                        }
                                    }
                                }
                                else if (Nesteditem.RecordStatus == 0) //-- upload from dental records 
                                {
                                    bool isImage = false;
                                    var objPatientData = new DataAccessLayer.PatientsData.clsPatientsData();
                                    if (Nesteditem.AttachmentType.ToLower() != "jpg" 
                                            && Nesteditem.AttachmentType.ToLower() != "jpeg" 
                                            && Nesteditem.AttachmentType.ToLower() != "gif" 
                                            && Nesteditem.AttachmentType.ToLower() != "png" 
                                            && Nesteditem.AttachmentType.ToLower() != "bmp" 
                                            && Nesteditem.AttachmentType.ToLower() != "psd" 
                                            && Nesteditem.AttachmentType.ToLower() != "pspimage" 
                                            && Nesteditem.AttachmentType.ToLower() != "thm" 
                                            && Nesteditem.AttachmentType.ToLower() != "tif" 
                                            && Nesteditem.AttachmentType.ToLower() != "yuv")
                                    {
                                        dtfileupload = objPatientData.GetDetailByDocumentId(Convert.ToInt32(Nesteditem.AttachmentId));
                                    }
                                    else
                                    {
                                        isImage = true;
                                        dtfileupload = objPatientData.GetDetailByImageId(Convert.ToInt32(Nesteditem.AttachmentId));
                                    }

                                    if (dtfileupload != null && dtfileupload.Rows.Count > 0)
                                    {
                                        foreach (DataRow FileUpload in dtfileupload.Rows)
                                        {
                                            string paramName = isImage ? "Name" : "DocumentName";
                                            string NewFileName = Convert.ToString(FileUpload[paramName]);

                                            bool Attach = objCommonDAL.InsertMessageAttachemnts(id, NewFileName);

                                        }
                                    }
                                }
                                
                            }

                            objResponse.MethodName = MethodName;
                            objResponse.MagicNumber = MagicNumber;
                            objResponse.ResultCode = 200;
                            objResponse.StatusMessage = "Message send successfully.";
                            objResponse.ResultDetail = string.Empty;

                            json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                        }
                        else
                        {
                            objResponse.MethodName = MethodName;
                            objResponse.MagicNumber = MagicNumber;
                            objResponse.ResultCode = 402;
                            objResponse.IsSuccess = false;

                            objResponse.StatusMessage = "Message send failed.";
                            objResponse.ResultDetail = string.Empty;


                            json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                        }
                    }


                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;
                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }

        }

        /// <summary>
        /// Method Name :UploadDentalRecord
        /// Desciprtion :This Method Use for Upload DentalRecords of Patient
        /// Request :
        /// Response : { Successfully logged out from MyDentalFiles }
        /// </summary>
        [HttpPost]
        public string UploadAttachment(int MagicNumber, string SessionCode, string RecordType, string RecordName, string RecordContent)
        {

            string json = "";
            string MethodName = "UploadAttachment";
            DataTable dtPatient = new DataTable();
            string strExtension = "";
            int RecordId = 0;
            string FileName = Path.GetFileNameWithoutExtension(RecordName);
            string FileType = RecordType;

            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    FileName = "$" + System.DateTime.Now.Ticks + "$" + FileName;
                    strExtension = "." + FileType.ToLower();
                    string FileName1 = System.Configuration.ConfigurationManager.AppSettings.Get("TempUploads") + FileName + strExtension;


                    if (strExtension != ".jpg" && strExtension != ".jpeg" && strExtension != ".gif" && strExtension != ".png" && strExtension != ".bmp" && strExtension != ".psd" && strExtension != ".pspimage" && strExtension != ".thm" && strExtension != ".tif")
                    {
                        RecordId = objCommonDAL.Insert_UploadedFiles(FileName1, Convert.ToInt32(Session["PatientId"]), "");
                    }
                    else
                    {
                        RecordId = objCommonDAL.Insert_UploadedImages(FileName1, Convert.ToInt32(Session["PatientId"]), "");


                    }

                    if (RecordId > 0)
                    {
                        byte[] data = Convert.FromBase64String(RecordContent);

                        string savepath = "";
                        string tempPath = "";
                        //string ImageBank = "";
                        //ImageBank = System.Configuration.ConfigurationManager.AppSettings["ImageBankFolderPath"].ToString();
                        tempPath = System.Configuration.ConfigurationManager.AppSettings["TempUploadsFolderPath"].ToString();
                        savepath = Server.MapPath(tempPath);
                        // savepath = Server.MapPath(ImageBank);
                        savepath = savepath.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");
                        System.IO.File.WriteAllBytes(savepath + FileName + strExtension, data);
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.StatusMessage = "Document uploaded successfully.";
                        objResponse.ResultDetail = new { RecordId, RecordName, RecordType };
                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "Failed to upload document.";
                        objResponse.ResultDetail = string.Empty;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }

                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }
                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }

        }

        /// <summary>
        /// Method Name :GetPatientDetails
        /// Desciprtion :This Method Use for Get Details of Patient
        /// Request : SessionCode
        /// Response : { PatientId :2312 ,FirstName : john ,LastName :martin ,Email : john123@gmail.com,Phone :(234) 324-4324 }
        /// </summary>
        [HttpPost]
        public string GetPatientDetails(string SessionCode, int MagicNumber)
        {
            string json = "";

            string MethodName = "GetPatientDetails";
            int Gender;
            DataTable dtPatient = new DataTable();
            DataTable dtRegi = new DataTable();
            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    List<PateintDetailsForAPI> PatientDetails = new List<PateintDetailsForAPI>();

                    dtPatient = objCommonDAL.GetPatientsDetails(Convert.ToInt32(Session["PatientId"]));

                    if (dtPatient != null && dtPatient.Rows.Count > 0)
                    {


                        PateintDetailsForAPI objpatientdetails = new PateintDetailsForAPI();
                        objpatientdetails.FirstName = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["FirstName"]), string.Empty);
                        objpatientdetails.LastName = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["LastName"]), string.Empty);
                        Gender = Convert.ToInt32(dtPatient.Rows[0]["Gender"]);
                        if (Gender == 0)
                        {
                            objpatientdetails.Gender = "Male";
                        }
                        else
                        {
                            objpatientdetails.Gender = "Female";
                        }

                        objpatientdetails.DateOfBirth = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["DateOfBirth"]), string.Empty);
                        objpatientdetails.Address = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["Address"]), string.Empty);
                        objpatientdetails.City = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["City"]), string.Empty);
                        objpatientdetails.State = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["State"]), string.Empty);
                        objpatientdetails.Zipcode = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["Zip"]), string.Empty);
                        objpatientdetails.PrimaryPhone = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["Phone"]), string.Empty);
                        objpatientdetails.SecondaryPhone = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["SecondaryPhone"]), string.Empty);
                        objpatientdetails.EmailAddress = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["Email"]), string.Empty);

                        dtRegi = objCommonDAL.GetRegistration(Convert.ToInt32(Session["PatientId"]), "GetRegistrationform");
                        if (dtRegi != null && dtRegi.Rows.Count > 0)
                        {
                            objpatientdetails.EmergencyContactName = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtemergencyname"]), string.Empty);
                            objpatientdetails.EmergencyContactPhone = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtemergency"]), string.Empty);
                        }


                        PatientDetails.Add(objpatientdetails);

                        dynamic dResult = new { PatientDetails };
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.StatusMessage = "Patient detail successfully retrieved.";
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = dResult;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "No record found.";
                        objResponse.ResultDetail = string.Empty;
                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                    }

                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;
                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }
                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :UpdatePatientDetails
        /// Desciprtion :This Method Use for Update Details of Patient
        /// Request : SessionCode
        /// Response : { Patient details updated successfully }
        /// </summary>
        [HttpPost]
        public string UpdatePatientDetails(string SessionCode, string FirstName, string LastName, string Gender, DateTime DateOfBirth, string Address, string City, string State, string PrimaryPhone, int MagicNumber, string Zipcode, string SecondaryPhone, string EmailAddress, string EmergencyContactName, string EmergencyContactPhone)
        {
            string json = "";
            string MethodName = "UpdatePatientDetails";
            DataTable dtRegi = new DataTable();
            DataTable dtpatient = new DataTable();
            DataTable dtPatientEmail = new DataTable();

            bool Result = false;
            bool ResultRegi = false;
            string drpStatus = "";
            string Fax = "0";
            bool x = false;
            try
            {

                if (IsSessionExpired(SessionCode))
                {
                    dtPatientEmail = objCommonDAL.CheckPatientEmail(EmailAddress);
                    if (Gender == "Female")
                    {
                        Gender = "1";
                    }
                    else
                    {
                        Gender = "0";
                    }
                    if (dtPatientEmail != null && dtPatientEmail.Rows.Count > 0)
                    {
                        if (dtPatientEmail.Rows[0]["Email"] != null && Convert.ToString(dtPatientEmail.Rows[0]["Email"]) != "")
                        {
                            if (Convert.ToString(dtPatientEmail.Rows[0]["Email"]) == EmailAddress)
                            {

                                // Method for update
                                dtpatient = objCommonDAL.GetPatientsDetails(Convert.ToInt32(Session["PatientId"]));
                                x = objCommonDAL.UpdatePatientEmailAddress(Convert.ToInt32(Session["PatientId"]), EmailAddress);
                                Result = objCommonDAL.EditPatientDetails(Convert.ToInt32(Session["PatientId"]), FirstName, LastName, Gender, Convert.ToDateTime(DateOfBirth), Address, objCommonDAL.CheckNull(Convert.ToString(dtpatient.Rows[0]["Address2"]), string.Empty), City, State, PrimaryPhone);


                                dtRegi = objCommonDAL.GetRegistration(Convert.ToInt32(Session["PatientId"]), "GetRegistrationform");

                                if (dtpatient != null && dtpatient.Rows.Count > 0)
                                {
                                    drpStatus = objCommonDAL.CheckNull(Convert.ToString(dtpatient.Rows[0]["PatientStatus"]), null);
                                }
                                if (dtRegi != null && dtRegi.Rows.Count > 0)
                                {

                                    string txtResponsiblepartyFname = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsiblepartyFname"]), null);
                                    string txtResponsiblepartyLname = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsiblepartyLname"]), null);
                                    string txtResponsibleRelationship = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleRelationship"]), null);
                                    string txtResponsibleAddress = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleAddress"]), null);
                                    string txtResponsibleCity = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleCity"]), null);
                                    string txtResponsibleState = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleState"]), null);
                                    string txtResponsibleZipCode = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleZipCode"]), null);
                                    string txtResponsibleDOB = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleDOB"]), null);
                                    string txtResponsibleContact = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleContact"]), null);
                                    string MethodOfPayment = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["chkMethodOfPayment"]), null);
                                    string txtEmployeeName1 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployeeName1"]), null);
                                    string txtEmployeeDob1 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployeeDob1"]), null);
                                    string txtEmployerName1 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployerName1"]), null);
                                    string txtYearsEmployed1 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtYearsEmployed1"]), null);
                                    string txtNameofInsurance1 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtNameofInsurance1"]), null);
                                    string txtInsuranceAddress1 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsuranceAddress1"]), null);
                                    string txtInsuranceTelephone1 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsuranceTelephone1"]), null);
                                    string txtEmployeeName2 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployeeName2"]), null);
                                    string txtEmployeeDob2 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployeeDob2"]), null);
                                    string txtEmployerName2 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployerName2"]), null);
                                    string txtYearsEmployed2 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtYearsEmployed2"]), null);
                                    string txtNameofInsurance2 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtNameofInsurance2"]), null);
                                    string txtInsuranceAddress2 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsuranceAddress2"]), null);
                                    string txtInsuranceTelephone2 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsuranceTelephone2"]), null);


                                    ResultRegi = objCommonDAL.RegistrationFormUpdateForAPI(Convert.ToInt32(Session["PatientId"]), Convert.ToString(DateOfBirth), Gender, drpStatus, Address, City, State, Zipcode, PrimaryPhone, SecondaryPhone, Fax, txtResponsiblepartyFname, txtResponsiblepartyLname, txtResponsibleRelationship, txtResponsibleAddress, txtResponsibleCity, txtResponsibleState, txtResponsibleZipCode, txtResponsibleDOB, txtResponsibleContact, txtResponsiblepartyFname, EmergencyContactName, EmergencyContactPhone, MethodOfPayment, txtEmployeeName1, txtEmployeeDob1, txtEmployerName1, txtYearsEmployed1, txtNameofInsurance1, txtInsuranceAddress1, txtInsuranceTelephone1, txtEmployeeName2, txtEmployeeDob2, txtEmployerName2, txtYearsEmployed2, txtNameofInsurance2, txtInsuranceAddress2, txtInsuranceTelephone2);
                                }
                                if (Result)
                                {
                                    objResponse.MethodName = MethodName;
                                    objResponse.MagicNumber = MagicNumber;
                                    objResponse.ResultCode = 200;
                                    objResponse.StatusMessage = "Patient details updated successfully.";
                                    objResponse.ResultDetail = string.Empty;
                                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                                }
                                else
                                {
                                    if (ResultRegi == false)
                                    {
                                        objResponse.MethodName = MethodName;
                                        objResponse.MagicNumber = MagicNumber;
                                        objResponse.ResultCode = 401;
                                        objResponse.IsSuccess = false;
                                        objResponse.StatusMessage = "Failed to update patient registration form details.";
                                        objResponse.ResultDetail = string.Empty;
                                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                                    }
                                    else
                                    {
                                        objResponse.MethodName = MethodName;
                                        objResponse.MagicNumber = MagicNumber;
                                        objResponse.ResultCode = 401;
                                        objResponse.IsSuccess = false;
                                        objResponse.StatusMessage = "Failed to update patient details.";
                                        objResponse.ResultDetail = string.Empty;
                                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                                    }
                                }


                            }
                            else
                            {
                                objResponse.MethodName = MethodName;
                                objResponse.MagicNumber = MagicNumber;
                                objResponse.ResultCode = 402;
                                objResponse.IsSuccess = false;
                                objResponse.StatusMessage = "Email Address Already Exists";
                                objResponse.ResultDetail = string.Empty;
                                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                            }

                        }



                    }
                    else
                    {
                        // Method for update
                        dtpatient = objCommonDAL.GetPatientsDetails(Convert.ToInt32(Session["PatientId"]));
                        x = objCommonDAL.UpdatePatientEmailAddress(Convert.ToInt32(Session["PatientId"]), EmailAddress);
                        Result = objCommonDAL.EditPatientDetails(Convert.ToInt32(Session["PatientId"]), FirstName, LastName, Gender, Convert.ToDateTime(DateOfBirth), Address, objCommonDAL.CheckNull(Convert.ToString(dtpatient.Rows[0]["Address2"]), string.Empty), City, State, PrimaryPhone);


                        dtRegi = objCommonDAL.GetRegistration(Convert.ToInt32(Session["PatientId"]), "GetRegistrationform");

                        if (dtpatient != null && dtpatient.Rows.Count > 0)
                        {
                            drpStatus = objCommonDAL.CheckNull(Convert.ToString(dtpatient.Rows[0]["PatientStatus"]), null);
                        }
                        if (dtRegi != null && dtRegi.Rows.Count > 0)
                        {



                            string txtResponsiblepartyFname = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsiblepartyFname"]), null);
                            string txtResponsiblepartyLname = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsiblepartyLname"]), null);
                            string txtResponsibleRelationship = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleRelationship"]), null);
                            string txtResponsibleAddress = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleAddress"]), null);
                            string txtResponsibleCity = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleCity"]), null);
                            string txtResponsibleState = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleState"]), null);
                            string txtResponsibleZipCode = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleZipCode"]), null);
                            string txtResponsibleDOB = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleDOB"]), null);
                            string txtResponsibleContact = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleContact"]), null);


                            string MethodOfPayment = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["chkMethodOfPayment"]), null);
                            string txtEmployeeName1 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployeeName1"]), null);
                            string txtEmployeeDob1 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployeeDob1"]), null);
                            string txtEmployerName1 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployerName1"]), null);
                            string txtYearsEmployed1 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtYearsEmployed1"]), null);
                            string txtNameofInsurance1 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtNameofInsurance1"]), null);
                            string txtInsuranceAddress1 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsuranceAddress1"]), null);
                            string txtInsuranceTelephone1 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsuranceTelephone1"]), null);
                            string txtEmployeeName2 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployeeName2"]), null);
                            string txtEmployeeDob2 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployeeDob2"]), null);
                            string txtEmployerName2 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployerName2"]), null);
                            string txtYearsEmployed2 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtYearsEmployed2"]), null);
                            string txtNameofInsurance2 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtNameofInsurance2"]), null);
                            string txtInsuranceAddress2 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsuranceAddress2"]), null);
                            string txtInsuranceTelephone2 = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsuranceTelephone2"]), null);


                            ResultRegi = objCommonDAL.RegistrationFormUpdateForAPI(Convert.ToInt32(Session["PatientId"]), Convert.ToString(DateOfBirth), Gender, drpStatus, Address, City, State, Zipcode, PrimaryPhone, SecondaryPhone, Fax, txtResponsiblepartyFname, txtResponsiblepartyLname, txtResponsibleRelationship, txtResponsibleAddress, txtResponsibleCity, txtResponsibleState, txtResponsibleZipCode, txtResponsibleDOB, txtResponsibleContact, txtResponsiblepartyFname, EmergencyContactName, EmergencyContactPhone, MethodOfPayment, txtEmployeeName1, txtEmployeeDob1, txtEmployerName1, txtYearsEmployed1, txtNameofInsurance1, txtInsuranceAddress1, txtInsuranceTelephone1, txtEmployeeName2, txtEmployeeDob2, txtEmployerName2, txtYearsEmployed2, txtNameofInsurance2, txtInsuranceAddress2, txtInsuranceTelephone2);
                        }
                        if (Result)
                        {
                            objResponse.MethodName = MethodName;
                            objResponse.MagicNumber = MagicNumber;
                            objResponse.ResultCode = 200;
                            objResponse.StatusMessage = "Patient details updated successfully.";
                            objResponse.ResultDetail = string.Empty;
                            json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                        }
                        else
                        {
                            if (ResultRegi == false)
                            {
                                objResponse.MethodName = MethodName;
                                objResponse.MagicNumber = MagicNumber;
                                objResponse.ResultCode = 401;
                                objResponse.IsSuccess = false;
                                objResponse.StatusMessage = "Failed to update patient registration form details.";
                                objResponse.ResultDetail = string.Empty;
                                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                            }
                            else
                            {
                                objResponse.MethodName = MethodName;
                                objResponse.MagicNumber = MagicNumber;
                                objResponse.ResultCode = 401;
                                objResponse.IsSuccess = false;
                                objResponse.StatusMessage = "Failed to update patient details.";
                                objResponse.ResultDetail = string.Empty;
                                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                            }
                        }
                    }






                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;
                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :GetTreatingDoctor
        /// Desciprtion :This Method Use for Get All Treating Doctors of Patient
        /// Request : SessionCode
        /// Response : { DoctorId : 672, Name : test ,Image : ../ImageBank/Lighthouse_634758169305710988.jpg ,Specialities : Dental Vendor, Aesthetic Dentist  }
        /// </summary>
        [HttpPost]
        public string GetTreatingDoctors(string SessionCode, int MagicNumber)
        {
            string json = "";
            string MethodName = "GetTreatingDoctors";
            DataTable dtTreatingDoc = new DataTable();

            try
            {


                if (IsSessionExpired(SessionCode))
                {

                    List<GetTreatingDoctors> TreatingDoctors = new List<GetTreatingDoctors>();

                    dtTreatingDoc = objCommonDAL.GetTratingdoctors(Convert.ToInt32(Session["PatientId"]), 1, int.MaxValue);

                    if (dtTreatingDoc != null && dtTreatingDoc.Rows.Count > 0)
                    {
                        foreach (DataRow TreatingDoctor in dtTreatingDoc.Rows)
                        {

                            GetTreatingDoctors objUserList = new GetTreatingDoctors();
                            objUserList.DoctorId = Convert.ToInt32(TreatingDoctor["ColleagueId"]);
                            objUserList.Name = objCommonDAL.CheckNull(Convert.ToString(TreatingDoctor["name"]), string.Empty);

                            string imagename = objCommonDAL.CheckNull(Convert.ToString(TreatingDoctor["Dentistimage"]), string.Empty);
                            string Sourcefile = Server.MapPath("~/" + System.Configuration.ConfigurationManager.AppSettings["DentistImagespath"]) + imagename;
                            Sourcefile = Sourcefile.Replace("\\mydentalfiles\\", "\\");

                            if (System.IO.File.Exists(Sourcefile))
                            {
                                objUserList.ThumbImageUrl = Session["CompanyWebsite"] + "/" + DentalPath + objCommonDAL.CheckNull(Convert.ToString(TreatingDoctor["Dentistimage"]), string.Empty);
                                // objUserList.ThumbImageUrl = "http://192.168.1.13:1250/" + DentalPath + objCommonDAL.CheckNull(Convert.ToString(TreatingDoctor["Dentistimage"]), string.Empty);

                            }
                            else
                            {
                                objUserList.ThumbImageUrl = DoctorImage;
                            }

                            objUserList.Specialties = objCommonDAL.CheckNull(Convert.ToString(TreatingDoctor["specialities"]), string.Empty);
                            objUserList.PracticeName = objCommonDAL.CheckNull(Convert.ToString(TreatingDoctor["AccountName"]), string.Empty);
                            DataTable dtEdu = new DataTable();
                            dtEdu = objCommonDAL.GetMemberEducation(objUserList.DoctorId);

                            if (dtEdu != null && dtEdu.Rows.Count > 0)
                            {
                                string Education = "";

                                Education = objCommonDAL.CheckNull(Convert.ToString(dtEdu.Rows[0]["specialisation"]), string.Empty);
                                if (dtEdu.Rows.Count > 1)
                                {
                                    foreach (DataRow Educations in dtEdu.Rows)
                                    {
                                        string Edu = objCommonDAL.CheckNull(Convert.ToString(Educations["specialisation"]), string.Empty);
                                        if (!string.IsNullOrEmpty(Edu))
                                        {
                                            Education = Education + "," + Edu;
                                        }
                                    }
                                }


                                objUserList.Education = Education;
                            }
                            else
                            {
                                objUserList.Education = string.Empty;
                            }
                            TreatingDoctors.Add(objUserList);

                        }
                        dynamic dResult = new { TreatingDoctors };
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = dResult;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "No Record Found";
                        objResponse.ResultDetail = string.Empty;
                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }

                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;
                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :SearchDentist
        /// Desciprtion :This Method Use for SearchDentist
        /// Request : SessionCode,index,size,Keywords,Miles,zipcode,Specialtity
        /// Response : { DoctorId :94 ,Name : Test,specialty :Dental Professor ,Image :../ImageBank/q_635050123241409575.jpg }
        /// </summary>
        [HttpPost]
        public string SearchDentists(string SessionCode, int PageIndex, int PageSize, string Keywords, string Zipcode, string Specialties, int MagicNumber)
        {
            string json = "";
            string MethodName = "SearchDentists";
            DataTable dtSearchDentist = new DataTable();

            DataTable dtEdu = new DataTable();
            try
            {


                if (IsSessionExpired(SessionCode))
                {

                    List<SearchDentist> Dentists = new List<SearchDentist>();
                    if (Zipcode == "")
                    {
                        Zipcode = null;
                    }
                    dtSearchDentist = objCommonDAL.GetInviteTreatingDoctorList(PageIndex, PageSize, Keywords, null, null, 0, 0, null, null, null, null, null, null, Zipcode, null, 0, Specialties);
                    if (dtSearchDentist != null && dtSearchDentist.Rows.Count > 0)
                    {
                        foreach (DataRow SearchDentist in dtSearchDentist.Rows)
                        {

                            SearchDentist objSearchDentist = new SearchDentist();
                            objSearchDentist.DoctorId = Convert.ToInt32(SearchDentist["UserId"]);
                            objSearchDentist.Name = objCommonDAL.CheckNull(Convert.ToString(SearchDentist["name"]), string.Empty);
                            objSearchDentist.Specialties = objCommonDAL.CheckNull(Convert.ToString(SearchDentist["Type"]), string.Empty);

                            string checkimage = "";
                            checkimage = objCommonDAL.CheckNull(Convert.ToString(SearchDentist["Image"]), string.Empty);
                            if (string.IsNullOrEmpty(checkimage))
                            {
                                objSearchDentist.ThumbImageUrl = DoctorImage;
                            }
                            else
                            {
                                string Sourcefile = Server.MapPath("~/" + System.Configuration.ConfigurationManager.AppSettings["DentistImagespath"]) + checkimage;
                                Sourcefile = Sourcefile.Replace("\\mydentalfiles\\", "\\");

                                if (System.IO.File.Exists(Sourcefile))
                                {
                                    objSearchDentist.ThumbImageUrl = Session["CompanyWebsite"] + "/" + DentalPath + objCommonDAL.CheckNull(Convert.ToString(SearchDentist["Image"]), string.Empty);

                                }
                                else
                                {
                                    objSearchDentist.ThumbImageUrl = DoctorImage;
                                }

                            }

                            dtEdu = objCommonDAL.GetMemberEducation(Convert.ToInt32(objSearchDentist.DoctorId));
                            if (dtEdu != null && dtEdu.Rows.Count > 0)
                            {
                                string Education = objCommonDAL.CheckNull(Convert.ToString(dtEdu.Rows[0]["specialisation"]), string.Empty);

                                if (dtEdu.Rows.Count > 1)
                                {
                                    foreach (DataRow Educations in dtEdu.Rows)
                                    {
                                        string Edu = objCommonDAL.CheckNull(Convert.ToString(Educations["specialisation"]), string.Empty);
                                        if (!string.IsNullOrEmpty(Edu))
                                        {
                                            Education = Education + "," + Edu;
                                        }
                                    }
                                }

                                objSearchDentist.Education = Education;

                            }
                            else
                            {
                                objSearchDentist.Education = string.Empty;
                            }
                            objSearchDentist.PracticeName = objCommonDAL.CheckNull(Convert.ToString(SearchDentist["AccountName"]), string.Empty);
                            Dentists.Add(objSearchDentist);
                        }

                        dynamic dResult = new { Dentists };
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = dResult;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "No Record Found.";
                        objResponse.ResultDetail = string.Empty;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }

                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }



            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);

            }
            return json;
        }

        /// <summary>
        /// Method Name :AddTreatingDoctor
        /// Desciprtion :This Method Use for Add Treating Doctor of Patient
        /// Request : SessionCode,DoctorId
        /// Response : { Doctor added successfully }
        /// </summary>
        [HttpPost]
        public string AddTreatingDoctor(string SessionCode, int DoctorId, int MagicNumber)
        {
            string json = "";
            string MethodName = "AddTreatingDoctor";

            bool Result = false;
            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    Result = objCommonDAL.AddPatientMember(Convert.ToInt32(Session["PatientId"]), Convert.ToInt32(DoctorId), Convert.ToInt32(UserStatusEnum.ACTIVE), 0, System.DateTime.Now, System.DateTime.Now, "");
                    if (Result)
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.StatusMessage = "Doctor added successfully.";
                        objResponse.ResultDetail = new { DoctorId };


                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "Failed to add treatingdoctor.";
                        objResponse.ResultDetail = new { DoctorId };

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }



                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :ForgotPassword
        /// Desciprtion :This Method Use for send password to Patient
        /// Request : Email
        /// Response : { Password sent successfully to your email }
        /// </summary>
        [HttpPost]
        public string ForgotPassword(string Email, int MagicNumber)
        {
            string json = "";
            string MethodName = "ForgotPassword";

            try
            {
                DataTable dtPatient = objCommonDAL.GetPatientPassword(Email);

                if (dtPatient == null || dtPatient.Rows.Count == 0)
                {
                    dtPatient = objCommonDAL.Dental_GetPatientPasswordTemp(Email);
                }

                if (dtPatient != null && dtPatient.Rows.Count > 0)
                {
                    string Password = dtPatient.Rows[0]["Password"].ToString();
                    string PatientFirstName = dtPatient.Rows[0]["FirstName"].ToString();
                    string PatientLastName = dtPatient.Rows[0]["LastName"].ToString();
                    string PatientEmail = Email;

                    //Send Password To Patient Email
                    objCommonDAL.SendPatientPassword(PatientEmail, Password, PatientFirstName, PatientLastName);

                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 200;
                    objResponse.StatusMessage = "Password sent successfully to your email";
                    objResponse.ResultDetail = string.Empty;
                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 402;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Email is not available.Please try again or contact your dental office.";
                    objResponse.ResultDetail = string.Empty;
                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                }






                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :GetDentalRecord
        /// Desciprtion :This Method Use for retrive all documents of Patient
        /// Request : SessionCode
        /// Response : { Doctor added successfully }
        /// </summary>
        [HttpPost]
        public string GetDentalRecords(string SessionCode, int MagicNumber)
        {
            string json = "";
            string MethodName = "GetDentalRecords";
            DataTable dtImages = new DataTable();
            DataTable dtDocs = new DataTable();
            List<GetDentalRecord> DentalRecords = new List<GetDentalRecord>();
            try
            {

                if (IsSessionExpired(SessionCode))
                {
                    dtImages = objCommonDAL.GetPatientMontages(Convert.ToInt32(Session["PatientId"]));
                    dtDocs = objCommonDAL.GetPatientDocs(Convert.ToInt32(Session["PatientId"]));

                    if (dtImages != null && dtImages.Rows.Count > 0)
                    {
                        foreach (DataRow Img in dtImages.Rows)
                        {
                            string CanDelete = "";
                            GetDentalRecord objDentalRecord = new GetDentalRecord();
                            objDentalRecord.RecordId = Convert.ToInt32(Img["ImageId"]);
                            objDentalRecord.RecordType = objCommonDAL.Getextension(objCommonDAL.CheckNull(Convert.ToString(Img["Name"]), string.Empty));
                            objDentalRecord.RecordUrl = "https://www.recordlinc.com" + objCommonDAL.CheckNull(Convert.ToString(Img["RelativePath"]).Replace("..", ""), string.Empty);
                            if (objDentalRecord.RecordType.ToLower() == "gif" || objDentalRecord.RecordType.ToLower() == "jpg" || objDentalRecord.RecordType.ToLower() == "png" || objDentalRecord.RecordType.ToLower() == "tif" || objDentalRecord.RecordType.ToLower() == "bmp" || objDentalRecord.RecordType.ToLower() == "psd" || objDentalRecord.RecordType.ToLower() == "yuv" || objDentalRecord.RecordType.ToLower() == "thm" || objDentalRecord.RecordType.ToLower() == "pspimage")
                            {
                                string DocName = objCommonDAL.CheckNull(Convert.ToString(Img["Name"]), string.Empty);

                                string ThubimagePath = Server.MapPath("~/" + System.Configuration.ConfigurationManager.AppSettings["FolderPathThumb"]) + "\\" + DocName;

                                if (System.IO.File.Exists(ThubimagePath))
                                {
                                   
                                     objDentalRecord.ThumbImageUrl = "https://www.recordlinc.com" + "/ImageBank/Thumb/" + DocName;
                                  
                                }
                                else
                                {

                                    string tempPath = System.Configuration.ConfigurationManager.AppSettings["imagebankpath"];
                                    string destinationFile = Server.MapPath(tempPath);
                                    string ThubimagePathNew = Server.MapPath("~/" + System.Configuration.ConfigurationManager.AppSettings["FolderPathThumb"]);



                                    objCommonDAL.GenerateThumbnailImage(destinationFile, DocName, ThubimagePathNew);

                                    objDentalRecord.ThumbImageUrl = "https://www.recordlinc.com" + "/ImageBank/Thumb/" + DocName;
                                }

                            }
                            objDentalRecord.Date = objCommonDAL.CheckNull(Convert.ToString(Img["DateForAPI"]), string.Empty);
                            objDentalRecord.Notes = objCommonDAL.CheckNull(Convert.ToString(Img["Description"]), string.Empty);
                            string RecordName = objCommonDAL.CheckNull(Convert.ToString(Img["Name"]), string.Empty);
                            RecordName = RecordName.Substring(RecordName.LastIndexOf("$") + 1);
                            objDentalRecord.RecordName = RecordName;
                            CanDelete = objCommonDAL.CheckNull(Convert.ToString(Img["uploadby"]), string.Empty);
                            if (CanDelete == "Patient")
                            {
                                objDentalRecord.CanDelete = true;
                            }
                            else
                            {
                                objDentalRecord.CanDelete = false;
                            }

                            DentalRecords.Add(objDentalRecord);
                        }
                    }
                    if (dtDocs != null && dtDocs.Rows.Count > 0)
                    {
                        foreach (DataRow Documents in dtDocs.Rows)
                        {
                            string CanDelete = "";
                            GetDentalRecord objDentalRecord = new GetDentalRecord();
                            objDentalRecord.RecordId = Convert.ToInt32(Documents["DocumentId"]);
                            objDentalRecord.RecordType = objCommonDAL.Getextension(objCommonDAL.CheckNull(Convert.ToString(Documents["DocumentName"]), string.Empty));
                            objDentalRecord.RecordUrl = "https://www.recordlinc.com" + objCommonDAL.CheckNull(Convert.ToString(Documents["DocumentName"]).Replace("..", ""), string.Empty);
                            objDentalRecord.Date = objCommonDAL.CheckNull(Convert.ToString(Documents["DateForAPI"]), string.Empty);

                            objDentalRecord.ThumbImageUrl = "https://www.recordlinc.com" + clsCommon.FileExtension.CheckFileExtenssion(Convert.ToString(Documents["DocumentName"])).Replace("..", "");
                            objDentalRecord.Notes = objCommonDAL.CheckNull(Convert.ToString(Documents["Description"]), string.Empty);


                            string RecordName = objCommonDAL.CheckNull(Convert.ToString(Documents["DocumentName"]), string.Empty);
                            RecordName = RecordName.Substring(RecordName.ToString().LastIndexOf("$") + 1);
                            objDentalRecord.RecordName = RecordName;
                            CanDelete = objCommonDAL.CheckNull(Convert.ToString(Documents["uploadby"]), string.Empty);
                            if (CanDelete == "Patient")
                            {
                                objDentalRecord.CanDelete = true;
                            }
                            else
                            {
                                objDentalRecord.CanDelete = false;
                            }
                            DentalRecords.Add(objDentalRecord);
                        }
                    }
                    if (dtImages == null && dtDocs == null)
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "No record found.";
                        objResponse.ResultDetail = string.Empty;
                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        dynamic dResult = new { DentalRecords };
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = dResult;


                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }


                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }


        /// <summary>
        /// Method Name :UploadDentalRecord
        /// Desciprtion :This Method Use for Upload DentalRecords of Patient
        /// Request :
        /// Response : { Successfully logged out from MyDentalFiles }
        /// </summary>
        [HttpPost]
        public string UploadDentalRecord(int MagicNumber, string SessionCode, string RecordType, string RecordName, string RecordContent)
        {

            string json = "";
            string MethodName = "UploadDentalRecord";
            DataTable dtPatient = new DataTable();



            string strExtension = "";

            int RecordId = 0;
            string FileName = Path.GetFileNameWithoutExtension(RecordName);
            string FileType = RecordType;

            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    FileName = "$" + System.DateTime.Now.Ticks + "$" + FileName;
                    strExtension = "." + FileType.ToLower();
                    string FileName1 = System.Configuration.ConfigurationManager.AppSettings.Get("imagebankpath") + FileName + strExtension;

                    if (strExtension != ".jpg" && strExtension != ".jpeg" && strExtension != ".gif" && strExtension != ".png" && strExtension != ".bmp" && strExtension != ".psd" && strExtension != ".pspimage" && strExtension != ".thm" && strExtension != ".tif")
                    {
                        RecordId = objCommonDAL.Insert_UploadedFiles(FileName1, Convert.ToInt32(Session["PatientId"]), "");
                    }
                    else
                    {
                        RecordId = objCommonDAL.Insert_UploadedImages(FileName1, Convert.ToInt32(Session["PatientId"]), "");


                    }

                    if (RecordId > 0)
                    {
                        byte[] data = Convert.FromBase64String(RecordContent);
                        System.IO.File.WriteAllBytes(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings.Get("imagebankpath")) + FileName + strExtension, data);
                        if (strExtension != ".jpg" && strExtension != ".jpeg" && strExtension != ".gif" && strExtension != ".png" && strExtension != ".bmp" && strExtension != ".psd" && strExtension != ".pspimage" && strExtension != ".thm" && strExtension != ".tif")
                        {
                        }
                        else
                        {
                            string tempPath = System.Configuration.ConfigurationManager.AppSettings["imagebankpath"];
                            string destinationFile = Server.MapPath(tempPath);
                            string ThubimagePathNew = Server.MapPath("~/" + System.Configuration.ConfigurationManager.AppSettings["FolderPathThumb"]);



                            objCommonDAL.GenerateThumbnailImage(destinationFile, FileName + strExtension, ThubimagePathNew);
                        }

                        int PatientId = Convert.ToInt32(Session["PatientId"]);
                        int MontageId = 0;
                        int Monatgetype = 0;
                        int imageId = 0;
                        int UploadImageId = 0;
                        int DoctorId = 0;

                        DataTable DtPatientInfo = objCommonDAL.PatientHistory(PatientId);
                        if (DtPatientInfo != null && DtPatientInfo.Rows.Count != 0)
                        {
                            if (DtPatientInfo.Rows[0]["column1"] != null)
                                DoctorId = Convert.ToInt32(DtPatientInfo.Rows[0]["column1"]);

                        }


                        DataTable dtowner = objCommonDAL.GetOwnerId(DoctorId);
                        int ownerId = (int)dtowner.Rows[0]["OwnerId"];
                        int ownertype = 0; int images;
                        /// ahiya PatientId thi je record get karya 6 aene badle RecordId je image k document temp ma upload thase te return id ma lavi ne table fill karavi
                        /// 

                        DataTable dtimageupload = objCommonDAL.Get_UploadedImages(RecordId);
                        DataTable dtfileupload = objCommonDAL.Get_UploadedFiles(RecordId);


                        DataTable dtPatientImages = objCommonDAL.getImageRecordsIfPatients(PatientId);
                        if (dtPatientImages.Rows.Count != 0)
                        {
                            MontageId = Convert.ToInt32(dtPatientImages.Rows[0]["MontageId"]);
                        }
                        if (MontageId == 0)
                        {

                            string MontageName = PatientId + "Montage";
                            int montageid = Convert.ToInt32(objCommonDAL.AddMontage(MontageName, Monatgetype, PatientId, ownerId, ownertype));

                            for (images = 0; images < dtimageupload.Rows.Count; images++)
                            {
                                string fullpath = System.Web.HttpContext.Current.Server.MapPath(dtimageupload.Rows[images]["Name"].ToString().Replace("..", ""));
                                string filename = Convert.ToString(dtimageupload.Rows[images]["Name"]).Replace(System.Configuration.ConfigurationManager.AppSettings.Get("imagebankpath"), "");
                                int Imagetype;
                                if (dtimageupload.Rows[images]["Category"].ToString() != "Get Category" && dtimageupload.Rows[images]["Category"].ToString() != "")
                                {
                                    Imagetype = Convert.ToInt32(dtimageupload.Rows[images]["Category"]);
                                }
                                else
                                {
                                    Imagetype = 500;
                                }
                                int imageid = objCommonDAL.AddImagetoPatient(PatientId, 1, filename, 1000, 1000, 0, Imagetype, fullpath, dtimageupload.Rows[images]["Name"].ToString(), ownerId, ownertype, 1, "Patient");
                                if (imageid > 0)
                                {

                                    bool resmonimg = objCommonDAL.AddImagetoMontage(imageid, Imagetype, montageid);
                                }


                            }


                            foreach (DataRow FileUpload in dtfileupload.Rows)
                            {
                                int Docstatus = objCommonDAL.AddDocumenttoMontage(montageid, FileUpload["DocumentName"].ToString(), "Patient");
                            }

                        }

                        else if (MontageId != 0)
                        {
                            foreach (DataRow MontageImage in dtimageupload.Rows)
                            {

                                if (UploadImageId == 0)
                                {


                                    string fullpath = System.Web.HttpContext.Current.Server.MapPath(MontageImage["Name"].ToString().Replace("..", ""));

                                    string imagename = Convert.ToString(MontageImage["Name"]).Replace(System.Configuration.ConfigurationManager.AppSettings.Get("imagebankpath"), "");
                                    int Imagetype;
                                    if (MontageImage["Category"].ToString() != "Get Category" && MontageImage["Category"].ToString() != "")
                                    {
                                        Imagetype = Convert.ToInt32(MontageImage["Category"]);
                                    }
                                    else
                                    {
                                        Imagetype = 500;
                                    }
                                    imageId = objCommonDAL.AddImagetoPatient(PatientId, 1, imagename, 1000, 1000, 0, Imagetype, fullpath, MontageImage["Name"].ToString(), ownerId, ownertype, 1, "Patient");
                                    if (imageId > 0)
                                    {
                                        bool resmonimg = objCommonDAL.AddImagetoMontage(imageId, Imagetype, MontageId);

                                    }
                                }


                            }


                            foreach (DataRow DentalDoc in dtfileupload.Rows)
                            {
                                int Docstatus = objCommonDAL.AddDocumenttoMontage(MontageId, DentalDoc["DocumentName"].ToString(), "Patient");
                            }


                        }


                        if (dtimageupload != null && dtimageupload.Rows.Count > 0)
                        {
                            objCommonDAL.DeleteImagesUploded(PatientId);

                        }
                        dtfileupload = objCommonDAL.getuploadfiles(PatientId, "");
                        if (dtfileupload != null && dtfileupload.Rows.Count > 0)
                        {
                            objCommonDAL.DeleteFilesUploaded(Convert.ToInt32(PatientId));
                        }

                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.StatusMessage = "Document uploaded successfully.";
                        objResponse.ResultDetail = new { RecordId, RecordName, RecordType };
                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "Failed to upload document.";
                        objResponse.ResultDetail = string.Empty;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }

                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }
                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }

        }

        /// <summary>
        /// Method Name :Logout
        /// Desciprtion :This Method Use for Logout Patient
        /// Request :
        /// Response : { Successfully logged out from MyDentalFiles }
        /// </summary>
        [HttpPost]
        public string Logout(int MagicNumber, string SessionCode)
        {
            string json = "";
            string MethodName = "Logout";

            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    Session["SessionCode"] = null;
                    Session["PatientId"] = null;
                    Session.RemoveAll();

                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 200;
                    objResponse.StatusMessage = "Successfully logged out from MyDentalFiles.";
                    objResponse.ResultDetail = string.Empty;
                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                }


            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);

            }
            return json;
        }

        /// <summary>
        /// Method Name :IsSessionExpired
        /// Desciprtion :This Method Use for Check Session States of Patient
        /// Request : sessioncode
        /// Response : { True }
        /// </summary>
        public bool IsSessionExpired(string sessioncode)
        {
            bool Result = false;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["SessionCode"])) && Convert.ToString(Session["SessionCode"]) == sessioncode)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["PatientId"])))
                {
                    Result = true;
                }
                else
                {
                    Result = false;
                }
            }
            else
            {
                Result = false;
            }
            return Result;

        }

        /// <summary>
        /// Method Name :ViewDentist
        /// Desciprtion :This Method Use for get Doctor Public Profile details
        /// Request : DoctorId
        /// Response : {  }
        /// </summary>
        [HttpPost]
        public string ViewDentist(int DoctorId, int MagicNumber, string SessionCode)
        {
            string json = "";
            string MethodName = "ViewDentist";

            DataTable dtDoctorDetails = new DataTable();
            DataTable dtDoctorSpeacility = new DataTable();
            DataTable dtAddresses = new DataTable();
            DataTable dtDescription = new DataTable();
            DataTable dtWebsites = new DataTable();
            DataTable dtEdu = new DataTable();
            DataTable dtProfMem = new DataTable();
            DataTable dtTeamMem = new DataTable();
            DataTable dtTitle = new DataTable();
            DataSet ds = new DataSet();
            DataTable dtGetSocialMedia = new DataTable();
            int pageindex = 1;
            int pagesize = 10;


            List<BasicDetailsOfDoctor> DoctorDetails = new List<BasicDetailsOfDoctor>();
            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    dtDoctorDetails = objCommonDAL.GetMemberProfile(Convert.ToInt32(DoctorId));
                    dtDoctorSpeacility = objCommonDAL.GetDentalSpeacilityTypes(Convert.ToInt32(DoctorId));
                    dtAddresses = objCommonDAL.GetOffice_Address_PubProfile(Convert.ToInt32(DoctorId));
                    dtWebsites = objCommonDAL.GetWebsite(pageindex, pagesize, Convert.ToInt32(DoctorId));
                    dtEdu = objCommonDAL.GetMemberEducation(Convert.ToInt32(DoctorId));
                    dtProfMem = objCommonDAL.GetMember_ProfessionalMembership(Convert.ToInt32(DoctorId));
                    dtTeamMem = objCommonDAL.GetOtherDoctors(Convert.ToInt32(DoctorId), pageindex, pagesize, null, null, null);
                    dtTitle = objCommonDAL.GetAccountIdByUserId(DoctorId, "GetAccountByUserId");
                    dtGetSocialMedia = objCommonDAL.GetSocialMediaDetailOfDoctor(DoctorId);
                    if (dtDoctorDetails != null && dtDoctorDetails.Rows.Count > 0)
                    {

                        BasicDetailsOfDoctor objBasicDetailsOfDoctor = new BasicDetailsOfDoctor();


                        objBasicDetailsOfDoctor.DoctorId = DoctorId;
                        objBasicDetailsOfDoctor.DoctorFullName = objCommonDAL.CheckNull(Convert.ToString(dtDoctorDetails.Rows[0]["patientname"]), string.Empty);
                        objBasicDetailsOfDoctor.Description = objCommonDAL.CheckNull(objCommonDAL.RemoveHTML(Convert.ToString(dtDoctorDetails.Rows[0]["Description"])), string.Empty);

                        string imagename = objCommonDAL.CheckNull(Convert.ToString(dtDoctorDetails.Rows[0]["Dentistimage"]), string.Empty);
                        string Sourcefile = Server.MapPath("~/" + System.Configuration.ConfigurationManager.AppSettings["DentistImagespath"]) + imagename;
                        Sourcefile = Sourcefile.Replace("\\mydentalfiles\\", "\\");

                        if (System.IO.File.Exists(Sourcefile))
                        {
                            objBasicDetailsOfDoctor.ImageUrl = Session["CompanyWebsite"] + "/" + DentalPath + objCommonDAL.CheckNull(Convert.ToString(dtDoctorDetails.Rows[0]["Dentistimage"]), string.Empty);

                        }
                        else
                        {
                            objBasicDetailsOfDoctor.ImageUrl = DoctorImage;
                        }





                        if (dtEdu != null && dtEdu.Rows.Count > 0)
                        {
                            string Education = objCommonDAL.CheckNull(Convert.ToString(dtEdu.Rows[0]["specialisation"]), string.Empty);

                            if (dtEdu.Rows.Count > 1)
                            {
                                foreach (DataRow Educations in dtEdu.Rows)
                                {
                                    string Edu = objCommonDAL.CheckNull(Convert.ToString(Educations["specialisation"]), string.Empty);
                                    if (!string.IsNullOrEmpty(Edu))
                                    {
                                        Education = Education + "," + Edu;
                                    }
                                }
                            }

                            objBasicDetailsOfDoctor.Education = Education;

                        }
                        else
                        {
                            objBasicDetailsOfDoctor.Education = string.Empty;
                        }

                        if (dtDoctorSpeacility != null && dtDoctorSpeacility.Rows.Count > 0)
                        {
                            string Specialities = objCommonDAL.CheckNull(Convert.ToString(dtDoctorSpeacility.Rows[0]["Specialities"]), string.Empty);

                            if (dtDoctorSpeacility.Rows.Count > 1)
                            {
                                foreach (DataRow DoctorSpeciality in dtDoctorSpeacility.Rows)
                                {
                                    string Edu = objCommonDAL.CheckNull(Convert.ToString(DoctorSpeciality["Specialities"]), string.Empty);
                                    if (!string.IsNullOrEmpty(Edu))
                                    {
                                        Specialities = Specialities + "," + Edu;
                                    }
                                }
                            }

                            objBasicDetailsOfDoctor.Specialties = Specialities;

                        }
                        else
                        {
                            objBasicDetailsOfDoctor.Specialties = string.Empty;
                        }

                        if (dtTitle != null && dtTitle.Rows.Count > 0)
                        {
                            objBasicDetailsOfDoctor.PracticeName = objCommonDAL.CheckNull(Convert.ToString(dtTitle.Rows[0]["AccountName"]), string.Empty);
                        }
                        else
                        {
                            objBasicDetailsOfDoctor.PracticeName = string.Empty;
                        }

                        DoctorDetails.Add(objBasicDetailsOfDoctor);




                        string FacebookUrl = objCommonDAL.CheckNull(Convert.ToString(dtGetSocialMedia.Rows[0]["FacebookUrl"]), string.Empty);
                        string LinkedinUrl = objCommonDAL.CheckNull(Convert.ToString(dtGetSocialMedia.Rows[0]["LinkedinUrl"]), string.Empty);
                        string TwitterUrl = objCommonDAL.CheckNull(Convert.ToString(dtGetSocialMedia.Rows[0]["TwitterUrl"]), string.Empty);
                        string GoogleUrl = objCommonDAL.CheckNull(Convert.ToString(dtGetSocialMedia.Rows[0]["GoogleplusUrl"]), string.Empty);
                        string YoutubeUrl = objCommonDAL.CheckNull(Convert.ToString(dtGetSocialMedia.Rows[0]["YoutubeUrl"]), string.Empty);
                        string PinterestUrl = objCommonDAL.CheckNull(Convert.ToString(dtGetSocialMedia.Rows[0]["PinterestUrl"]), string.Empty);
                        string BlogUrl = objCommonDAL.CheckNull(Convert.ToString(dtGetSocialMedia.Rows[0]["BlogUrl"]), string.Empty);

                        string OtherUrl = objCommonDAL.CheckNull(Convert.ToString(dtGetSocialMedia.Rows[0]["OtherUrl"]), string.Empty);
                        DataTable dtSocialMedia = new DataTable();
                        dtSocialMedia.Columns.Add(new DataColumn("Name", typeof(string)));
                        dtSocialMedia.Columns.Add(new DataColumn("Url", typeof(string)));

                        if (!string.IsNullOrEmpty(FacebookUrl))
                        {
                            DataRow dt = dtSocialMedia.NewRow();
                            dt[0] = "Facebook";
                            dt[1] = FacebookUrl;
                            dtSocialMedia.Rows.Add(dt);
                        }
                        if (!string.IsNullOrEmpty(TwitterUrl))
                        {
                            DataRow dt = dtSocialMedia.NewRow();
                            dt[0] = "Twitter";
                            dt[1] = TwitterUrl;

                            dtSocialMedia.Rows.Add(dt);
                        }
                        if (!string.IsNullOrEmpty(LinkedinUrl))
                        {
                            DataRow dt = dtSocialMedia.NewRow();
                            dt[0] = "LinkedIn";
                            dt[1] = LinkedinUrl;

                            dtSocialMedia.Rows.Add(dt);
                        }
                        if (!string.IsNullOrEmpty(YoutubeUrl))
                        {
                            DataRow dt = dtSocialMedia.NewRow();

                            dt[0] = "Youtube";
                            dt[1] = YoutubeUrl;

                            dtSocialMedia.Rows.Add(dt);
                        }
                        if (!string.IsNullOrEmpty(GoogleUrl))
                        {
                            DataRow dt = dtSocialMedia.NewRow();

                            dt[0] = "GooglePlus";
                            dt[1] = GoogleUrl;

                            dtSocialMedia.Rows.Add(dt);
                        }
                        if (!string.IsNullOrEmpty(BlogUrl))
                        {
                            DataRow dt = dtSocialMedia.NewRow();

                            dt[0] = "Blog";
                            dt[1] = BlogUrl;

                            dtSocialMedia.Rows.Add(dt);

                        }



                        if (!string.IsNullOrEmpty(PinterestUrl))
                        {
                            DataRow dt = dtSocialMedia.NewRow();

                            dt[0] = "Pinterest";
                            dt[1] = PinterestUrl;

                            dtSocialMedia.Rows.Add(dt);

                        }


                        if (!string.IsNullOrEmpty(OtherUrl))
                        {
                            DataRow dt = dtSocialMedia.NewRow();

                            dt[0] = "Other";
                            dt[1] = OtherUrl;

                            dtSocialMedia.Rows.Add(dt);

                        }




                        if (dtSocialMedia != null && dtSocialMedia.Rows.Count > 0)
                        {
                            foreach (DataRow SocialMedia in dtSocialMedia.Rows)
                            {
                                SocialMediaOfDoctor ObjSocialMediaOfDoctor = new SocialMediaOfDoctor();
                                ObjSocialMediaOfDoctor.SocialMediaWebsiteId = Convert.ToString(SocialMedia["Name"]);
                                ObjSocialMediaOfDoctor.SocialMediaWebsiteUrl = Convert.ToString(SocialMedia["Url"]);
                                objBasicDetailsOfDoctor.SocialMediaWebsites.Add(ObjSocialMediaOfDoctor);

                            }
                        }

                        if (dtDoctorSpeacility != null && dtDoctorSpeacility.Rows.Count > 0)
                        {
                            foreach (DataRow DoctorSpeciality in dtDoctorSpeacility.Rows)
                            {
                                SpeacilitiesOfDoctor objSpeacilitiesOfDoctor = new SpeacilitiesOfDoctor();
                                objSpeacilitiesOfDoctor.SpecialtyId = Convert.ToInt32(DoctorSpeciality["Id"]);
                                objSpeacilitiesOfDoctor.SpecialtyDescription = objCommonDAL.CheckNull(Convert.ToString(DoctorSpeciality["Specialities"]), string.Empty);
                                objBasicDetailsOfDoctor.SpecialitesDetails.Add(objSpeacilitiesOfDoctor);
                            }
                        }

                        if (dtAddresses != null && dtAddresses.Rows.Count > 0)
                        {
                            foreach (DataRow Address in dtAddresses.Rows)
                            {
                                AddressOfDoctor objAddressOfDoctor = new AddressOfDoctor();
                                objAddressOfDoctor.AddressId = Convert.ToInt32(Address["AddressId"]);
                                objAddressOfDoctor.StreetAddress = objCommonDAL.CheckNull(Convert.ToString(Address["ExactAddress"]), string.Empty);
                                objAddressOfDoctor.City = objCommonDAL.CheckNull(Convert.ToString(Address["City"]), string.Empty);
                                objAddressOfDoctor.State = objCommonDAL.CheckNull(Convert.ToString(Address["State"]), string.Empty);
                                objAddressOfDoctor.Country = objCommonDAL.CheckNull(Convert.ToString(Address["Country"]), string.Empty);
                                objAddressOfDoctor.Zipcode = objCommonDAL.CheckNull(Convert.ToString(Address["ZipCode"]), string.Empty);
                                objAddressOfDoctor.Phone = objCommonDAL.CheckNull(Convert.ToString(Address["phone"]), string.Empty);
                                objAddressOfDoctor.AddressType = objCommonDAL.CheckNull(Convert.ToString(Address["AddressTypeForAPI"]), string.Empty);

                                objBasicDetailsOfDoctor.Addressess.Add(objAddressOfDoctor);
                            }
                        }


                        if (dtWebsites != null && dtWebsites.Rows.Count > 0)
                        {
                            foreach (DataRow WebSites in dtWebsites.Rows)
                            {
                                WebsitesOfDoctor objWebsitesOfDoctor = new WebsitesOfDoctor();
                                objWebsitesOfDoctor.PracticeWebsiteId = Convert.ToInt32(WebSites["WebsiteId"]);
                                objWebsitesOfDoctor.PracticeWebsiteUrl = objCommonDAL.CheckNull(Convert.ToString(WebSites["WebsiteUrl"]), string.Empty);

                                objBasicDetailsOfDoctor.PracticeWebsites.Add(objWebsitesOfDoctor);
                            }
                        }

                        if (dtEdu != null && dtEdu.Rows.Count > 0)
                        {
                            foreach (DataRow Educatons in dtEdu.Rows)
                            {
                                EductionDetailsOfDoctor objEductionDetailsOfDoctor = new EductionDetailsOfDoctor();
                                objEductionDetailsOfDoctor.EducationId = Convert.ToInt32(Educatons["Id"]);
                                objEductionDetailsOfDoctor.Institute = objCommonDAL.CheckNull(Convert.ToString(Educatons["institute"]), string.Empty);
                                objEductionDetailsOfDoctor.Course = objCommonDAL.CheckNull(Convert.ToString(Educatons["specialisation"]), string.Empty);// Need To Change what we will provide course 
                                objEductionDetailsOfDoctor.Degree = objCommonDAL.CheckNull(Convert.ToString(Educatons["specialisation"]), string.Empty);
                                objEductionDetailsOfDoctor.EducationCity = objCommonDAL.CheckNull(Convert.ToString(Educatons["city"]), string.Empty);
                                objEductionDetailsOfDoctor.EducationState = objCommonDAL.CheckNull(Convert.ToString(Educatons["state"]), string.Empty);

                                objEductionDetailsOfDoctor.StartYear = objCommonDAL.CheckNull(Convert.ToString(Educatons["startdate"]), string.Empty);
                                objEductionDetailsOfDoctor.EndYear = objCommonDAL.CheckNull(Convert.ToString(Educatons["enddate"]), string.Empty);

                                objBasicDetailsOfDoctor.EducationDetails.Add(objEductionDetailsOfDoctor);
                            }
                        }
                        if (dtProfMem != null && dtProfMem.Rows.Count > 0)
                        {
                            foreach (DataRow ProfMem in dtProfMem.Rows)
                            {
                                ProfessionalMembershipsOfDoctor objProfessionalMembershipsOfDoctor = new ProfessionalMembershipsOfDoctor();
                                objProfessionalMembershipsOfDoctor.MembershipId = Convert.ToInt32(ProfMem["Id"]);
                                objProfessionalMembershipsOfDoctor.Membership = objCommonDAL.CheckNull(Convert.ToString(ProfMem["Membership"]), string.Empty);

                                objBasicDetailsOfDoctor.ProfessionalMemberships.Add(objProfessionalMembershipsOfDoctor);
                            }
                        }
                        if (dtTeamMem != null && dtTeamMem.Rows.Count > 0)
                        {
                            foreach (DataRow TeamMem in dtTeamMem.Rows)
                            {
                                TeamMembersOfDoctor objTeamMembersOfDoctor = new TeamMembersOfDoctor();
                                objTeamMembersOfDoctor.DoctorId = Convert.ToInt32(TeamMem["DoctorId"]);
                                objTeamMembersOfDoctor.Name = objCommonDAL.CheckNull(Convert.ToString(TeamMem["Name"]), string.Empty);
                                objBasicDetailsOfDoctor.TeamMembers.Add(objTeamMembersOfDoctor);
                            }
                        }


                        dynamic Result = new { DoctorDetails };
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = Result;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);


                    }
                    else
                    {

                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "No record found.";
                        objResponse.ResultDetail = string.Empty;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                    }

                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                }

                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :GetVersion
        /// Desciprtion :This Method Use for Get Version
        /// Request : 
        /// Response : { Version:1.3 }
        /// </summary>
        [HttpPost]
        public string GetVersion(int MagicNumber)
        {
            string json = "";
            string MethodName = "GetVersion";
            try
            {

                string Version = "1.11";

                dynamic Version1 = new { Version };
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 200;
                objResponse.ResultDetail = Version1;


                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;

                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :ChangePassword
        /// Desciprtion :This Method Use for Change Password of patient
        /// Request : 
        /// Response : { Password change successfully}
        /// </summary>
        /// 
        // AS Per Mail : Fwd: Password strength enforcement

        //This would validate the password to contain at least two digits, two alphas, be at least 8 characters long, and contain only alpha-numeric characters 

        //http://stackoverflow.com/questions/198974/asp-net-regular-expression-validator-password-strength
        [HttpPost]
        public string ChangePassword(string SessionCode, string CurrentPassword, string NewPassword, int MagicNumber)
        {
            string json = "";
            string MethodName = "ChangePassword";
            DataTable dtPatient = new DataTable();
            string Password = string.Empty;
            bool Result = false;
            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    dtPatient = objCommonDAL.GetPatientsDetails(Convert.ToInt32(Session["PatientId"]));
                    if (dtPatient != null && dtPatient.Rows.Count > 0)
                    {
                        Password = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["Password"]), string.Empty);
                    }
                    if (Password == CurrentPassword)
                    {
                        string validate = @"^.*(?=.{8,15})(?=.*[a-zA-Z])(?=.*\d)[a-zA-Z0-9!@#$%]+$";
                        if (Regex.IsMatch(NewPassword, validate))
                        {
                            Result = objCommonDAL.UpdatePatientPassword(Convert.ToInt32(Session["PatientId"]), NewPassword);
                            if (Result)
                            {

                                objResponse.MethodName = MethodName;
                                objResponse.MagicNumber = MagicNumber;
                                objResponse.ResultCode = 200;
                                objResponse.StatusMessage = "Password change successfully.";
                                objResponse.ResultDetail = string.Empty;


                                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                            }
                            else
                            {
                                objResponse.MethodName = MethodName;
                                objResponse.MagicNumber = MagicNumber;
                                objResponse.ResultCode = 402;
                                objResponse.IsSuccess = false;
                                objResponse.StatusMessage = "Password not changed. Try again.";
                                objResponse.ResultDetail = string.Empty;


                                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                            }
                        }
                        else
                        {
                            objResponse.MethodName = MethodName;
                            objResponse.MagicNumber = MagicNumber;
                            objResponse.ResultCode = 402;
                            objResponse.IsSuccess = false;
                            objResponse.StatusMessage = "You must have at least one number and one of the +, _, -, *, &... Please try again.";
                            objResponse.ResultDetail = string.Empty;


                            json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                        }
                    }
                    else
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "Current password is invalid.";
                        objResponse.ResultDetail = string.Empty;


                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }





                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :GetDescriptionofDentalRecord
        /// Desciprtion :This Method Use to Get Description of DentalRecord of patient
        /// Request : 
        /// Response : {"MethodName": "GetDescriptionofDentalRecord","MagicNumber": 12345,"Description": [{"CreationDate" : "10/27/2013 10:47 PM","LastModifiedDate" : "10/27/2013 10:47 PM","Description":"hi this is test" }}
        /// </summary>
        [HttpPost]
        public string GetDescriptionofDentalRecord(string SessionCode, int Id, string Type, int MagicNumber)
        {
            string json = "";
            string MethodName = "GetDescriptionofDentalRecord";

            DataTable dtDescri = new DataTable();
            List<GetDescriptionofDentalRecord> Description = new List<GetDescriptionofDentalRecord>();

            try
            {
                if (IsSessionExpired(SessionCode))
                {

                    if (Type.ToLower() != "jpg" && Type.ToLower() != "jpeg" && Type.ToLower() != "gif" && Type.ToLower() != "png" && Type.ToLower() != "bmp" && Type.ToLower() != "psd" && Type.ToLower() != "pspimage" && Type.ToLower() != "thm" && Type.ToLower() != "tif" && Type.ToLower() != "yuv")
                    {
                        dtDescri = objCommonDAL.GetDescriptionofDocumentById(Convert.ToInt32(Session["PatientId"]), Id);

                    }
                    else
                    {
                        dtDescri = objCommonDAL.GetDetailByImageId(Id);


                    }
                    if (dtDescri != null && dtDescri.Rows.Count > 0)
                    {


                        GetDescriptionofDentalRecord objGetDescriptionofDentalRecord = new GetDescriptionofDentalRecord();
                        objGetDescriptionofDentalRecord.CreationDate = objCommonDAL.CheckNull(Convert.ToString(dtDescri.Rows[0]["CreationDateForAPI"]), string.Empty);
                        objGetDescriptionofDentalRecord.LastModifiedDate = objCommonDAL.CheckNull(Convert.ToString(dtDescri.Rows[0]["LastModifiedDateForAPI"]), string.Empty);
                        objGetDescriptionofDentalRecord.Description = objCommonDAL.CheckNull(Convert.ToString(dtDescri.Rows[0]["Description"]), string.Empty);

                        Description.Add(objGetDescriptionofDentalRecord);

                        dynamic Result = new { Description };
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = Result;



                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "No record found.";
                        objResponse.ResultDetail = string.Empty;


                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }



                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :UpdateDescriptionofDentalRecord
        /// Desciprtion :This Method Use to Get Description of DentalRecord of patient
        /// Request : 
        /// Response : {Description uploaded successfully}
        /// </summary>
        [HttpPost]
        public string UpdateDescriptionofDentalRecord(string SessionCode, int RecordId, string Notes, string Type, int MagicNumber)
        {
            string json = "";
            string MethodName = "UpdateDescriptionofDentalRecord";


            bool Result = false;
            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    if (Type.ToLower() != "jpg" && Type.ToLower() != "jpeg" && Type.ToLower() != "gif" && Type.ToLower() != "png" && Type.ToLower() != "bmp" && Type.ToLower() != "psd" && Type.ToLower() != "pspimage" && Type.ToLower() != "thm" && Type.ToLower() != "tif" && Type.ToLower() != "yuv")
                    {
                        Result = objCommonDAL.UpdateDescriptionofDocument(RecordId, Notes, 1);
                    }
                    else
                    {
                        Result = objCommonDAL.UpdateDescriptionofImage(RecordId, Notes, 1);
                    }

                    if (Result)
                    {



                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.StatusMessage = "Description uploaded successfully.";
                        objResponse.ResultDetail = string.Empty;


                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "Failed to upload document.";
                        objResponse.ResultDetail = string.Empty;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }



                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;
                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :ContactUs
        /// Desciprtion :This Method Use for Get All Treating Doctors of Patient
        /// Request : int MagicNumber, string FirstName, string LastName, string Email, string PhoneNumber, string Message
        /// Response : { Request received at RecordLinc. }
        /// </summary>
        [HttpPost]
        public string ContactUs(int MagicNumber, string FirstName, string LastName, string Email, string PhoneNumber, string Message)
        {
            string json = "";
            string MethodName = "ContactUs";
            string Sucess = "";
            bool status = false;
            try
            {
                status = objCommonDAL.SendContactMessageFromAPI(FirstName + LastName, Email, PhoneNumber, Message);
                if (status)
                {
                    Sucess = "Request received at RecordLinc.";
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 200;
                    objResponse.StatusMessage = Sucess;
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                }
                else
                {
                    Sucess = "Request sending failed.";
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 402;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = Sucess;
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                }



                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :SuggestImprovement
        /// Desciprtion :This Method Use for send mail to travis regarding SuggestImprovement 
        /// Request : string SessionCode, int MagicNumber, string Suggestion
        /// Response : { Suggestion send at RecordLinc. }
        /// </summary>
        [HttpPost]
        public string SuggestImprovement(string SessionCode, int MagicNumber, string Suggestion)
        {
            string json = "";
            string MethodName = "SuggestImprovement";
            string Sucess = "";
            bool status = false;
            string fromemail = "";
            DataTable dtDetials = new DataTable();
            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    dtDetials = objCommonDAL.GetPatientsDetails(Convert.ToInt32(Session["PatientId"]));
                    if (dtDetials != null && dtDetials.Rows.Count > 0)
                    {
                        fromemail = objCommonDAL.CheckNull(Convert.ToString(dtDetials.Rows[0]["Email"]), null);
                        status = objCommonDAL.SendSuggestionOrBugFromAPI(fromemail, Suggestion);
                        if (status)
                        {
                            Sucess = "Suggestion send at RecordLinc.";
                            objResponse.MethodName = MethodName;
                            objResponse.MagicNumber = MagicNumber;
                            objResponse.ResultCode = 200;
                            objResponse.StatusMessage = Sucess;
                            objResponse.ResultDetail = string.Empty;

                            json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                        }
                        else
                        {
                            Sucess = "Suggestion sending failed.";
                            objResponse.MethodName = MethodName;
                            objResponse.MagicNumber = MagicNumber;
                            objResponse.ResultCode = 402;
                            objResponse.IsSuccess = false;
                            objResponse.StatusMessage = Sucess;
                            objResponse.ResultDetail = string.Empty;

                            json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                        }
                    }
                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                }



                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :ReportBug
        /// Desciprtion :This Method Use for send mail to travis regarding bugs
        /// Request : string SessionCode, int MagicNumber, string BugDetail
        /// Response : { Request received at RecordLinc. }
        /// </summary>
        [HttpPost]
        public string ReportBug(string SessionCode, int MagicNumber, string BugDetail)
        {
            string json = "";
            string MethodName = "ReportBug";
            string Sucess = "";
            bool status = false;
            string fromemail = "";
            DataTable dtDetials = new DataTable();
            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    dtDetials = objCommonDAL.GetPatientsDetails(Convert.ToInt32(Session["PatientId"]));
                    if (dtDetials != null && dtDetials.Rows.Count > 0)
                    {
                        fromemail = objCommonDAL.CheckNull(Convert.ToString(dtDetials.Rows[0]["Email"]), null);
                        status = objCommonDAL.SendSuggestionOrBugFromAPI(fromemail, BugDetail);
                        if (status)
                        {
                            Sucess = "Bug send at RecordLinc.";
                            objResponse.MethodName = MethodName;
                            objResponse.MagicNumber = MagicNumber;
                            objResponse.ResultCode = 200;
                            objResponse.StatusMessage = Sucess;
                            objResponse.ResultDetail = string.Empty;

                            json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                        }
                        else
                        {
                            Sucess = "Bug sending failed.";
                            objResponse.MethodName = MethodName;
                            objResponse.MagicNumber = MagicNumber;
                            objResponse.ResultCode = 402;
                            objResponse.IsSuccess = false;
                            objResponse.StatusMessage = Sucess;
                            objResponse.ResultDetail = string.Empty;

                            json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                        }
                    }
                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                }



                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :TellAFriend
        /// Desciprtion :This Method Use for Get All Treating Doctors of Patient
        /// Request : int MagicNumber, string FirstName, string LastName, string Email, string PhoneNumber, string Message
        /// Response : { Request received at RecordLinc. }
        /// </summary>
        [HttpPost]
        public string TellAFriend(string SessionCode, int MagicNumber, string Email, string Message, string FriendName)
        {
            string json = "";
            string MethodName = "TellAFriend";
            string Sucess = "";
            bool status = false;
            string fromemail = "";
            DataTable dtDetials = new DataTable();

            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    dtDetials = objCommonDAL.GetPatientsDetails(Convert.ToInt32(Session["PatientId"]));
                    if (dtDetials != null && dtDetials.Rows.Count > 0)
                    {
                        fromemail = objCommonDAL.CheckNull(Convert.ToString(dtDetials.Rows[0]["Email"]), null);
                        status = objCommonDAL.TellAFriendFromAPI(fromemail, Email, Message, FriendName);
                        if (status)
                        {
                            Sucess = "Mail send successfully.";
                            objResponse.MethodName = MethodName;
                            objResponse.MagicNumber = MagicNumber;
                            objResponse.ResultCode = 200;
                            objResponse.StatusMessage = Sucess;
                            objResponse.ResultDetail = string.Empty;

                            json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                        }
                        else
                        {
                            Sucess = "Mail send failed.";
                            objResponse.MethodName = MethodName;
                            objResponse.MagicNumber = MagicNumber;
                            objResponse.ResultCode = 402;
                            objResponse.IsSuccess = false;
                            objResponse.StatusMessage = Sucess;
                            objResponse.ResultDetail = string.Empty;

                            json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                        }
                    }
                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                }



                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :GetPrivacyPolicyLink
        /// Desciprtion :This Method return PrivacyPolicy page Link
        /// Request : string SessionCode, int MagicNumber
        /// Response : { https://www.recordlinc.com/privacypolicy.aspx. }
        /// </summary>
        [HttpPost]
        public string GetPrivacyPolicyLink(int MagicNumber)
        {
            string json = "";
            string MethodName = "GetPrivacyPolicyLink";
            string Sucess = "";
            try
            {
                Sucess = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host +
(Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port);
                Sucess += "/PrivacyPolicy?fromDevice=Iphone";
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 200;
                objResponse.StatusMessage = Sucess;
                objResponse.ResultDetail = string.Empty;

                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);





                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :GetTermsAndConditionLink
        /// Desciprtion :This Method return TermsAndCondition page Link
        /// Request : string SessionCode, int MagicNumber
        /// Response : { https://www.recordlinc.com/termsconditions.aspx. }
        /// </summary>
        [HttpPost]
        public string GetTermsAndConditionsLink(int MagicNumber)
        {
            string json = "";
            string MethodName = "GetTermsAndConditionsLink";
            string Sucess = "";
            try
            {

                Sucess = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host +
(Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port);
                Sucess += "/TermsAndConditions?fromDevice=Iphone";
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 200;
                objResponse.StatusMessage = Sucess;
                objResponse.ResultDetail = string.Empty;

                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);






                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        private byte[] imageToByteArray(System.Drawing.Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, System.Drawing.Imaging.ImageFormat.Gif);
            return ms.ToArray();
        }

        [HttpPost]
        public string DeleteRecord(int MagicNumber, string SessionCode, string RecordType, int RecordId)
        {
            string json = "";
            string MethodName = "DeleteRecord";
            DataTable dtimage = new DataTable();
            int Delsucess = 0;
            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    if (RecordType.ToLower() != "jpg" && RecordType.ToLower() != "jpeg" && RecordType.ToLower() != "gif" && RecordType.ToLower() != "png" && RecordType.ToLower() != "bmp" && RecordType.ToLower() != "psd" && RecordType.ToLower() != "pspimage" && RecordType.ToLower() != "thm" && RecordType.ToLower() != "tif" && RecordType.ToLower() != "yuv")
                    {

                        Delsucess = objCommonDAL.DeletefileofPatientNew("", RecordId);
                    }
                    else
                    {
                        dtimage = objCommonDAL.GetPatientMontages(Convert.ToInt32(Session["PatientId"]));
                        if (dtimage != null && dtimage.Rows.Count > 0)
                        {
                            Delsucess = objCommonDAL.DeleteimageofPatient(Convert.ToInt32(RecordId), Convert.ToInt32(dtimage.Rows[0]["MontageId"]));
                        }
                    }
                    if (Delsucess != 0)
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.StatusMessage = "Record deleted successfully.";
                        objResponse.ResultDetail = new { RecordId, RecordType };


                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "Failed to delete record.";
                        objResponse.ResultDetail = new { RecordId, RecordType };

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }



                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }



        }

        [HttpPost]
        public string DeleteMessage(int MagicNumber, string SessionCode, int MessageId)
        {
            string json = "";
            string MethodName = "DeleteMessage";
            DataTable dtMessage = new DataTable();
            int Result = 0;
            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    Result = objCommonDAL.EditPatientMessageReadStatusForAPI(MessageId, "PatientPFlag");


                    if (Result == 1)
                    {

                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.StatusMessage = "Message deleted successfully.";
                        objResponse.ResultDetail = new { MessageId };


                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                    }
                    else
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "Failed to delete message.";
                        objResponse.ResultDetail = new { MessageId };

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }



                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }



        }

        [HttpPost]
        public string DeviceToken(int MagicNumber, string SessionCode, string DeviceToken)
        {
            string json = "";
            string MethodName = "DeviceToken";
            DataTable dtToken = new DataTable();

            bool Result = false;

            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    dtToken = objCommonDAL.GetPatientDevice(Convert.ToInt32(Session["PatientId"]), DeviceToken);
                    if (dtToken != null && dtToken.Rows.Count > 0)
                    {
                        Result = objCommonDAL.UpdatePatientDeviceToken(Convert.ToInt32(Session["PatientId"]), DeviceToken);
                        if (Result)
                        {
                            objResponse.MethodName = MethodName;
                            objResponse.MagicNumber = MagicNumber;
                            objResponse.ResultCode = 200;
                            objResponse.StatusMessage = string.Empty;
                            objResponse.ResultDetail = string.Empty;


                            json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                        }

                    }
                    else
                    {
                        int id = 0;
                        id = objCommonDAL.Insert_DeviceToken(Convert.ToInt32(Session["PatientId"]), DeviceToken);
                        if (id > 0)
                        {
                            objResponse.MethodName = MethodName;
                            objResponse.MagicNumber = MagicNumber;
                            objResponse.ResultCode = 200;
                            objResponse.StatusMessage = string.Empty;
                            objResponse.ResultDetail = string.Empty;


                            json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                        }


                    }



                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }



        }

        [HttpPost]
        public string GetDentalHistory(string SessionCode, int MagicNumber)
        {
            string json = "";
            string MethodName = "GetDentalHistory";
            DataTable dtDentalHistory = new DataTable();
            List<DentalHistoryAPI> DentalHistory = new List<DentalHistoryAPI>();
            try
            {

                if (IsSessionExpired(SessionCode))
                {
                    dtDentalHistory = objCommonDAL.GetDentalHistory(Convert.ToInt32(Session["PatientId"]), "GetDentalHistory");

                    if (dtDentalHistory != null && dtDentalHistory.Rows.Count > 0)
                    {
                        dtDentalHistory.Columns.Remove("txtQue1");
                        dtDentalHistory.Columns.Remove("txtQue2");
                        dtDentalHistory.Columns.Remove("txtQue3");
                        dtDentalHistory.Columns.Remove("txtQue4");

                        dtDentalHistory.Columns["txtQue5"].ColumnName = "Previous Dentists Name";
                        dtDentalHistory.Columns["txtQue5a"].ColumnName = "Previous Dentists Name|Address";
                        dtDentalHistory.Columns["txtQue5b"].ColumnName = "Previous Dentists Name|Phone";
                        dtDentalHistory.Columns["txtQue5c"].ColumnName = "Previous Dentists Name|Email";

                        dtDentalHistory.Columns["txtQue6"].ColumnName = "When was the last time you had your teeth cleaned?";


                        dtDentalHistory.Columns["rdQue7a"].ColumnName = "Do you make regular visits to the dentist?";
                        dtDentalHistory.Columns["txtQue7"].ColumnName = "Do you make regular visits to the dentist?|How often";


                        dtDentalHistory.Columns["txtQue12"].ColumnName = "Please explain any problems or complications with previous dental treatment?";

                        dtDentalHistory.Columns["rdoQue11aFixedbridge"].ColumnName = "Have any of your teeth been replaced with the following?|Fixed Bridge";
                        dtDentalHistory.Columns["rdoQue11bRemoveablebridge"].ColumnName = "Have any of your teeth been replaced with the following?|Removeable Bridge";
                        dtDentalHistory.Columns["rdoQue11cDenture"].ColumnName = "Have any of your teeth been replaced with the following?|Denture";
                        dtDentalHistory.Columns["rdQue11dImplant"].ColumnName = "Have any of your teeth been replaced with the following?|Implant";


                        dtDentalHistory.Columns["rdQue15"].ColumnName = "Do you clench or grind your teeth?";

                        dtDentalHistory.Columns["rdQue16"].ColumnName = "Does your jaw click or pop?";
                        dtDentalHistory.Columns["rdQue17"].ColumnName = "Are you experiencing any pain or soreness in the muscles of your face?";
                        dtDentalHistory.Columns["rdQue18"].ColumnName = "Do you have frequent headaches, neckaches or shoulder aches?";
                        dtDentalHistory.Columns["rdQue19"].ColumnName = "Does food get caught in your teeth?";

                        dtDentalHistory.Columns["chkQue20"].ColumnName = "Are any of your teeth sensitive to|Hot|Cold|Sweets|Pressure";

                        dtDentalHistory.Columns["rdQue21"].ColumnName = "Do your gums bleed or hurt?";
                        dtDentalHistory.Columns["txtQue21a"].ColumnName = "Do your gums bleed or hurt?|When";


                        dtDentalHistory.Columns["txtQue22"].ColumnName = "How often do you brush your teeth?";
                        dtDentalHistory.Columns["txtQue23"].ColumnName = "How often do you use dental floss?";
                        dtDentalHistory.Columns["rdQue24"].ColumnName = "Are any of your teeth loose, tipped, shifted or chipped?";

                        dtDentalHistory.Columns["rdQue28"].ColumnName = "Do you feel your breath is often offensive?";
                        dtDentalHistory.Columns["txtQue28a"].ColumnName = "Do you feel your breath is often offensive?|What";
                        dtDentalHistory.Columns["txtQue28b"].ColumnName = "Do you feel your breath is often offensive?|Where";
                        dtDentalHistory.Columns["txtQue28c"].ColumnName = "Do you feel your breath is often offensive?|When";



                        dtDentalHistory.Columns["txtQue29"].ColumnName = "Explain any orthodonic work you have had done?";
                        dtDentalHistory.Columns["txtQue29a"].ColumnName = "Explain any dental work you have had done (Perio, Oral Surgery, etc.)";
                        dtDentalHistory.Columns["txtQue26"].ColumnName = "How do you feel about your teeth in general?";
                        dtDentalHistory.Columns["txtComments"].ColumnName = "Additional Information";



                        dtDentalHistory.Columns.Remove("txtQue9a");
                        dtDentalHistory.Columns.Remove("rdQue13");
                        dtDentalHistory.Columns.Remove("rdQue14");
                        dtDentalHistory.Columns.Remove("txtQue14a");
                        dtDentalHistory.Columns.Remove("txtDigiSign");


                        dtDentalHistory.Columns.Remove("rdQue8");
                        dtDentalHistory.Columns.Remove("rdQue9");
                        dtDentalHistory.Columns.Remove("rdQue10");
                        dtDentalHistory.Columns.Remove("rdQue27");
                        dtDentalHistory.Columns.Remove("rdQue30");


                        dtDentalHistory.Columns.Remove("Do you feel your breath is often offensive?|What");
                        dtDentalHistory.Columns.Remove("Do you feel your breath is often offensive?|Where");


                        dtDentalHistory.Columns["Previous Dentists Name"].SetOrdinal(0);

                        dtDentalHistory.Columns["When was the last time you had your teeth cleaned?"].SetOrdinal(1);
                        dtDentalHistory.Columns["Do you make regular visits to the dentist?"].SetOrdinal(2);

                        dtDentalHistory.Columns["Please explain any problems or complications with previous dental treatment?"].SetOrdinal(3);

                        dtDentalHistory.Columns["Have any of your teeth been replaced with the following?|Fixed Bridge"].SetOrdinal(4);




                        dtDentalHistory.Columns["Do you clench or grind your teeth?"].SetOrdinal(5);

                        dtDentalHistory.Columns["Does your jaw click or pop?"].SetOrdinal(6);
                        dtDentalHistory.Columns["Are you experiencing any pain or soreness in the muscles of your face?"].SetOrdinal(7);
                        dtDentalHistory.Columns["Do you have frequent headaches, neckaches or shoulder aches?"].SetOrdinal(8);
                        dtDentalHistory.Columns["Does food get caught in your teeth?"].SetOrdinal(9);

                        dtDentalHistory.Columns["Are any of your teeth sensitive to|Hot|Cold|Sweets|Pressure"].SetOrdinal(10);

                        dtDentalHistory.Columns["Do your gums bleed or hurt?"].SetOrdinal(11);


                        dtDentalHistory.Columns["How often do you brush your teeth?"].SetOrdinal(12);
                        dtDentalHistory.Columns["How often do you use dental floss?"].SetOrdinal(13);
                        dtDentalHistory.Columns["Are any of your teeth loose, tipped, shifted or chipped?"].SetOrdinal(14);

                        dtDentalHistory.Columns["Do you feel your breath is often offensive?"].SetOrdinal(15);



                        dtDentalHistory.Columns["Explain any orthodonic work you have had done?"].SetOrdinal(16);
                        dtDentalHistory.Columns["Explain any dental work you have had done (Perio, Oral Surgery, etc.)"].SetOrdinal(17);
                        dtDentalHistory.Columns["How do you feel about your teeth in general?"].SetOrdinal(18);
                        dtDentalHistory.Columns["Additional Information"].SetOrdinal(19);



                        dtDentalHistory.AcceptChanges();

                        if (dtDentalHistory != null && dtDentalHistory.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtDentalHistory.Columns.Count; i++)
                            {

                                DentalHistoryAPI objDentalHistoryAPI = new DentalHistoryAPI();

                                objDentalHistoryAPI.HistoryId = i + 1;
                                objDentalHistoryAPI.ItemNumber = i + 1;




                                objDentalHistoryAPI.Question = objCommonDAL.CheckNull(Convert.ToString(dtDentalHistory.Columns[i].ColumnName), string.Empty);

                                string CheckType = objCommonDAL.CheckNull(Convert.ToString(dtDentalHistory.Rows[0][i]), string.Empty);

                                objDentalHistoryAPI.ItemType = "YesNo";
                                objDentalHistoryAPI.SingleLine = string.Empty;
                                string QuestionTest = Convert.ToString(objDentalHistoryAPI.Question);
                                string Answer = objCommonDAL.CheckNull(Convert.ToString(dtDentalHistory.Rows[0][i]), string.Empty);
                                if (Answer == "Y")
                                {
                                    objDentalHistoryAPI.Answer = "Yes";
                                }
                                else if (Answer == "N")
                                {
                                    objDentalHistoryAPI.Answer = "No";
                                }
                                else
                                {
                                    objDentalHistoryAPI.Answer = Answer;
                                }

                                if (objDentalHistoryAPI.ItemType == "YesNo")
                                {
                                    if (!objDentalHistoryAPI.Question.Contains("|"))
                                    {
                                        objDentalHistoryAPI.MultiPart = false;
                                    }
                                    else
                                    {
                                        objDentalHistoryAPI.MultiPart = true;
                                    }

                                }
                                else
                                {
                                    objDentalHistoryAPI.MultiPart = false;
                                }




                                if (objDentalHistoryAPI.Question.Contains("Have any of your teeth been replaced with the following?"))
                                {
                                    int Id = 1;
                                    for (int j = 0; j < dtDentalHistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtDentalHistory.Columns[j].ColumnName), string.Empty);


                                        if (Question.Contains("Have any of your teeth been replaced with the following?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);

                                            string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtDentalHistory.Rows[0]["Have any of your teeth been replaced with the following?|" + Question]), string.Empty);

                                            if (!string.IsNullOrEmpty(AnswerForQ1) && AnswerForQ1 == "Y")
                                            {
                                                AnswerForQ1 = "Yes";
                                            }
                                            else
                                            {
                                                AnswerForQ1 = "No";
                                            }
                                            MultiPartArray objMultiPartArray = new MultiPartArray();

                                            objMultiPartArray.HistoryId = Id;
                                            objMultiPartArray.ItemNumber = Id;
                                            objMultiPartArray.ItemType = "YesNo";
                                            objMultiPartArray.MultiPart = false;
                                            objMultiPartArray.Question = Question;
                                            objMultiPartArray.Answer = AnswerForQ1;
                                            objMultiPartArray.SingleLine = string.Empty;
                                            objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                            Id++;
                                            AnswerForQ1 = "";
                                        }
                                        Question = "";


                                    }
                                    dtDentalHistory.Columns.Remove("Have any of your teeth been replaced with the following?|Removeable Bridge");
                                    dtDentalHistory.Columns.Remove("Have any of your teeth been replaced with the following?|Denture");
                                    dtDentalHistory.Columns.Remove("Have any of your teeth been replaced with the following?|Implant");
                                    dtDentalHistory.AcceptChanges();

                                    objDentalHistoryAPI.Question = "Have any of your teeth been replaced with the following?";
                                    objDentalHistoryAPI.ItemType = "YesNo";
                                    objDentalHistoryAPI.Answer = string.Empty;
                                    objDentalHistoryAPI.SingleLine = string.Empty;
                                }



                                if (objDentalHistoryAPI.Question.Contains("Are any of your teeth sensitive to|Hot|Cold|Sweets|Pressure"))
                                {
                                    string Question = objDentalHistoryAPI.Question;
                                    Question = Question.Substring(Question.IndexOf("|") + 1); ;


                                    string[] QuestionSplit = Question.Split('|');

                                    string[] Ans = objDentalHistoryAPI.Answer.Split(',');

                                    int IDQ2 = 1;
                                    foreach (string QuestionSplits in QuestionSplit)
                                    {

                                        MultiPartArray objMultiPartArray = new MultiPartArray();

                                        objMultiPartArray.HistoryId = IDQ2;
                                        objMultiPartArray.ItemNumber = IDQ2;
                                        objMultiPartArray.ItemType = "YesNo";
                                        objMultiPartArray.MultiPart = false;
                                        objMultiPartArray.Question = QuestionSplits;

                                        objMultiPartArray.SingleLine = string.Empty;



                                        if (Ans.Contains(Convert.ToString(IDQ2)))
                                        {
                                            objMultiPartArray.Answer = "Yes";
                                        }
                                        else
                                        {
                                            objMultiPartArray.Answer = "No";
                                        }

                                        objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                        IDQ2++;
                                    }


                                    Question = "";

                                    objDentalHistoryAPI.Question = "Are any of your teeth sensitive to";
                                    objDentalHistoryAPI.ItemType = "YesNo";
                                    objDentalHistoryAPI.Answer = string.Empty;
                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.SingleLine = string.Empty;
                                }




                                if (QuestionTest.Contains("Previous Dentists Name"))
                                {
                                    int Id = 1;
                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "Descriptive";
                                    for (int j = 0; j < dtDentalHistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtDentalHistory.Columns[j].ColumnName), string.Empty);




                                        if (Question.Contains("Previous Dentists Name"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);

                                            if (Question != "Previous Dentists Name")
                                            {
                                                string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtDentalHistory.Rows[0]["Previous Dentists Name|" + Question]), string.Empty);
                                                MultiPartArray objMultiPartArray = new MultiPartArray();

                                                objMultiPartArray.HistoryId = Id;
                                                objMultiPartArray.ItemNumber = Id;
                                                objMultiPartArray.ItemType = "Descriptive";
                                                objMultiPartArray.MultiPart = false;
                                                objMultiPartArray.Question = Question;
                                                objMultiPartArray.Answer = AnswerForQ1;
                                                objMultiPartArray.SingleLine = "Yes";
                                                objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                                Id++;
                                                AnswerForQ1 = "";
                                            }
                                            else
                                            {
                                                objDentalHistoryAPI.Question = "Previous Dentist's Name";
                                                objDentalHistoryAPI.SingleLine = "Yes";
                                            }

                                        }


                                        Question = "";


                                    }


                                    dtDentalHistory.Columns.Remove("Previous Dentists Name|Address");
                                    dtDentalHistory.Columns.Remove("Previous Dentists Name|Phone");
                                    dtDentalHistory.Columns.Remove("Previous Dentists Name|Email");
                                    dtDentalHistory.AcceptChanges();
                                }



                                if (QuestionTest.Contains("Do you make regular visits to the dentist?"))
                                {
                                    int Id = 1;

                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "YesNo";
                                    objDentalHistoryAPI.Question = "Do you make regular visits to the dentist?";

                                    string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtDentalHistory.Rows[0]["Do you make regular visits to the dentist?"]), string.Empty);

                                    if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                    {
                                        AnswerForQ12 = "Yes";
                                    }
                                    else
                                    {
                                        AnswerForQ12 = "No";
                                    }
                                    objDentalHistoryAPI.Answer = AnswerForQ12;
                                    for (int j = 0; j < dtDentalHistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtDentalHistory.Columns[j].ColumnName), string.Empty);




                                        if (Question.Contains("Do you make regular visits to the dentist?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                            if (Question != "Do you make regular visits to the dentist?")
                                            {
                                                string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtDentalHistory.Rows[0]["Do you make regular visits to the dentist?|" + Question]), string.Empty);
                                                MultiPartArray objMultiPartArray = new MultiPartArray();

                                                objMultiPartArray.HistoryId = Id;
                                                objMultiPartArray.ItemNumber = Id;
                                                objMultiPartArray.ItemType = "Descriptive";
                                                objMultiPartArray.MultiPart = false;
                                                objMultiPartArray.Question = Question;
                                                objMultiPartArray.Answer = AnswerForQ1;
                                                objMultiPartArray.SingleLine = "Yes";
                                                objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                                Id++;
                                                AnswerForQ1 = "";
                                            }

                                        }
                                        Question = "";


                                    }

                                    dtDentalHistory.Columns.Remove("Do you make regular visits to the dentist?|How often");
                                    dtDentalHistory.AcceptChanges();
                                    objDentalHistoryAPI.SingleLine = string.Empty;
                                }

                                if (QuestionTest.Contains("Do your gums bleed or hurt?"))
                                {
                                    int Id = 1;
                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "YesNo";

                                    objDentalHistoryAPI.Question = "Do your gums bleed or hurt?";

                                    string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtDentalHistory.Rows[0]["Do your gums bleed or hurt?"]), string.Empty);

                                    if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                    {
                                        AnswerForQ12 = "Yes";
                                    }
                                    else
                                    {
                                        AnswerForQ12 = "No";
                                    }
                                    objDentalHistoryAPI.Answer = AnswerForQ12;

                                    for (int j = 0; j < dtDentalHistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtDentalHistory.Columns[j].ColumnName), string.Empty);




                                        if (Question.Contains("Do your gums bleed or hurt?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);

                                            if (Question != "Do your gums bleed or hurt?")
                                            {
                                                string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtDentalHistory.Rows[0]["Do your gums bleed or hurt?|" + Question]), string.Empty);
                                                MultiPartArray objMultiPartArray = new MultiPartArray();

                                                objMultiPartArray.HistoryId = Id;
                                                objMultiPartArray.ItemNumber = Id;
                                                objMultiPartArray.ItemType = "Descriptive";
                                                objMultiPartArray.MultiPart = false;
                                                objMultiPartArray.Question = Question;
                                                objMultiPartArray.Answer = AnswerForQ1;
                                                objMultiPartArray.SingleLine = "Yes";
                                                objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                                Id++;
                                                AnswerForQ1 = "";
                                            }

                                        }
                                        Question = "";


                                    }
                                    objDentalHistoryAPI.SingleLine = string.Empty;
                                    dtDentalHistory.Columns.Remove("Do your gums bleed or hurt?|When");
                                    dtDentalHistory.AcceptChanges();
                                }


                                if (QuestionTest.Contains("Do you feel your breath is often offensive?"))
                                {
                                    int Id = 1;
                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "YesNo";

                                    objDentalHistoryAPI.Question = "Do you feel your breath is often offensive?";

                                    string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtDentalHistory.Rows[0]["Do you feel your breath is often offensive?"]), string.Empty);

                                    if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                    {
                                        AnswerForQ12 = "Yes";
                                    }
                                    else
                                    {
                                        AnswerForQ12 = "No";
                                    }
                                    objDentalHistoryAPI.Answer = AnswerForQ12;
                                    for (int j = 0; j < dtDentalHistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtDentalHistory.Columns[j].ColumnName), string.Empty);




                                        if (Question.Contains("Do you feel your breath is often offensive?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);
                                            if (Question != "Do you feel your breath is often offensive?")
                                            {
                                                string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtDentalHistory.Rows[0]["Do you feel your breath is often offensive?|" + Question]), string.Empty);
                                                MultiPartArray objMultiPartArray = new MultiPartArray();

                                                objMultiPartArray.HistoryId = Id;
                                                objMultiPartArray.ItemNumber = Id;
                                                objMultiPartArray.ItemType = "Descriptive";
                                                objMultiPartArray.MultiPart = false;
                                                objMultiPartArray.Question = Question;
                                                objMultiPartArray.Answer = AnswerForQ1;
                                                objMultiPartArray.SingleLine = "Yes";
                                                objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                                Id++;
                                                AnswerForQ1 = "";
                                            }

                                        }
                                        Question = "";


                                    }
                                    objDentalHistoryAPI.SingleLine = string.Empty;
                                    dtDentalHistory.Columns.Remove("Do you feel your breath is often offensive?|When");
                                    dtDentalHistory.AcceptChanges();
                                }

                                if (objDentalHistoryAPI.HistoryId == 2 || objDentalHistoryAPI.HistoryId == 4 || objDentalHistoryAPI.HistoryId == 13 || objDentalHistoryAPI.HistoryId == 14 || objDentalHistoryAPI.HistoryId == 17 || objDentalHistoryAPI.HistoryId == 18 || objDentalHistoryAPI.HistoryId == 19 || objDentalHistoryAPI.HistoryId == 20)
                                {
                                    objDentalHistoryAPI.ItemType = "Descriptive";
                                    if (objDentalHistoryAPI.HistoryId == 17 || objDentalHistoryAPI.HistoryId == 18 || objDentalHistoryAPI.HistoryId == 19 || objDentalHistoryAPI.HistoryId == 20)
                                    {
                                        objDentalHistoryAPI.SingleLine = "No";
                                    }
                                    else
                                    {
                                        objDentalHistoryAPI.SingleLine = "Yes";
                                    }

                                    objDentalHistoryAPI.Answer = objCommonDAL.CheckNull(Convert.ToString(dtDentalHistory.Rows[0][i]), string.Empty);
                                }

                                DentalHistory.Add(objDentalHistoryAPI);
                                CheckType = "";

                            }



                        }


                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.StatusMessage = string.Empty;
                        objResponse.ResultDetail = new { DentalHistory };


                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                        //For Update Dental History
                        string jsonUpdate = JsonConvert.SerializeObject(DentalHistory, Formatting.Indented);
                        List<DentalHistoryAPI> UpdateDentalHistory = (List<DentalHistoryAPI>)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonUpdate, typeof(List<DentalHistoryAPI>));

                    }
                    else
                    {



                        DataTable dtGetDentalHistory = new DataTable();
                        dtGetDentalHistory = GetBlankTableForDentalHistory();


                        if (dtGetDentalHistory != null && dtGetDentalHistory.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtGetDentalHistory.Columns.Count; i++)
                            {

                                DentalHistoryAPI objDentalHistoryAPI = new DentalHistoryAPI();

                                objDentalHistoryAPI.HistoryId = i + 1;
                                objDentalHistoryAPI.ItemNumber = i + 1;




                                objDentalHistoryAPI.Question = objCommonDAL.CheckNull(Convert.ToString(dtGetDentalHistory.Columns[i].ColumnName), string.Empty);

                                string CheckType = objCommonDAL.CheckNull(Convert.ToString(dtGetDentalHistory.Rows[0][i]), string.Empty);

                                objDentalHistoryAPI.ItemType = "YesNo";
                                objDentalHistoryAPI.SingleLine = string.Empty;
                                string QuestionTest = Convert.ToString(objDentalHistoryAPI.Question);
                                string Answer = objCommonDAL.CheckNull(Convert.ToString(dtGetDentalHistory.Rows[0][i]), string.Empty);
                                if (Answer == "Y")
                                {
                                    objDentalHistoryAPI.Answer = "Yes";
                                }
                                else if (Answer == "N")
                                {
                                    objDentalHistoryAPI.Answer = "No";
                                }
                                else
                                {
                                    objDentalHistoryAPI.Answer = Answer;
                                }

                                if (objDentalHistoryAPI.ItemType == "YesNo")
                                {
                                    if (!objDentalHistoryAPI.Question.Contains("|"))
                                    {
                                        objDentalHistoryAPI.MultiPart = false;
                                    }
                                    else
                                    {
                                        objDentalHistoryAPI.MultiPart = true;
                                    }

                                }
                                else
                                {
                                    objDentalHistoryAPI.MultiPart = false;
                                }




                                if (objDentalHistoryAPI.Question.Contains("Have any of your teeth been replaced with the following?"))
                                {
                                    int Id = 1;
                                    for (int j = 0; j < dtGetDentalHistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtGetDentalHistory.Columns[j].ColumnName), string.Empty);


                                        if (Question.Contains("Have any of your teeth been replaced with the following?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);

                                            string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtGetDentalHistory.Rows[0]["Have any of your teeth been replaced with the following?|" + Question]), string.Empty);

                                            if (!string.IsNullOrEmpty(AnswerForQ1) && AnswerForQ1 == "Y")
                                            {
                                                AnswerForQ1 = "Yes";
                                            }
                                            else
                                            {
                                                AnswerForQ1 = "No";
                                            }
                                            MultiPartArray objMultiPartArray = new MultiPartArray();

                                            objMultiPartArray.HistoryId = Id;
                                            objMultiPartArray.ItemNumber = Id;
                                            objMultiPartArray.ItemType = "YesNo";
                                            objMultiPartArray.MultiPart = false;
                                            objMultiPartArray.Question = Question;
                                            objMultiPartArray.Answer = AnswerForQ1;
                                            objMultiPartArray.SingleLine = string.Empty;
                                            objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                            Id++;
                                            AnswerForQ1 = "";
                                        }
                                        Question = "";


                                    }
                                    dtGetDentalHistory.Columns.Remove("Have any of your teeth been replaced with the following?|Removeable Bridge");
                                    dtGetDentalHistory.Columns.Remove("Have any of your teeth been replaced with the following?|Denture");
                                    dtGetDentalHistory.Columns.Remove("Have any of your teeth been replaced with the following?|Implant");
                                    dtGetDentalHistory.AcceptChanges();

                                    objDentalHistoryAPI.Question = "Have any of your teeth been replaced with the following?";
                                    objDentalHistoryAPI.ItemType = "YesNo";
                                    objDentalHistoryAPI.Answer = string.Empty;
                                    objDentalHistoryAPI.SingleLine = string.Empty;
                                }



                                if (objDentalHistoryAPI.Question.Contains("Are any of your teeth sensitive to|Hot|Cold|Sweets|Pressure"))
                                {
                                    string Question = objDentalHistoryAPI.Question;
                                    Question = Question.Substring(Question.IndexOf("|") + 1); ;


                                    string[] QuestionSplit = Question.Split('|');

                                    string[] Ans = objDentalHistoryAPI.Answer.Split(',');

                                    int IDQ2 = 1;
                                    foreach (string QuestionSplits in QuestionSplit)
                                    {

                                        MultiPartArray objMultiPartArray = new MultiPartArray();

                                        objMultiPartArray.HistoryId = IDQ2;
                                        objMultiPartArray.ItemNumber = IDQ2;
                                        objMultiPartArray.ItemType = "YesNo";
                                        objMultiPartArray.MultiPart = false;
                                        objMultiPartArray.Question = QuestionSplits;

                                        objMultiPartArray.SingleLine = string.Empty;



                                        if (Ans.Contains(Convert.ToString(IDQ2)))
                                        {
                                            objMultiPartArray.Answer = "Yes";
                                        }
                                        else
                                        {
                                            objMultiPartArray.Answer = "No";
                                        }

                                        objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                        IDQ2++;
                                    }


                                    Question = "";

                                    objDentalHistoryAPI.Question = "Are any of your teeth sensitive to";
                                    objDentalHistoryAPI.ItemType = "YesNo";
                                    objDentalHistoryAPI.Answer = string.Empty;
                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.SingleLine = string.Empty;
                                }




                                if (QuestionTest.Contains("Previous Dentists Name"))
                                {
                                    int Id = 1;
                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "Descriptive";
                                    for (int j = 0; j < dtGetDentalHistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtGetDentalHistory.Columns[j].ColumnName), string.Empty);




                                        if (Question.Contains("Previous Dentists Name"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);

                                            if (Question != "Previous Dentists Name")
                                            {
                                                string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtGetDentalHistory.Rows[0]["Previous Dentists Name|" + Question]), string.Empty);
                                                MultiPartArray objMultiPartArray = new MultiPartArray();

                                                objMultiPartArray.HistoryId = Id;
                                                objMultiPartArray.ItemNumber = Id;
                                                objMultiPartArray.ItemType = "Descriptive";
                                                objMultiPartArray.MultiPart = false;
                                                objMultiPartArray.Question = Question;
                                                objMultiPartArray.Answer = AnswerForQ1;
                                                objMultiPartArray.SingleLine = "Yes";
                                                objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                                Id++;
                                                AnswerForQ1 = "";
                                            }
                                            else
                                            {
                                                objDentalHistoryAPI.Question = "Previous Dentist's Name";
                                                objDentalHistoryAPI.SingleLine = "Yes";
                                            }

                                        }


                                        Question = "";


                                    }


                                    dtGetDentalHistory.Columns.Remove("Previous Dentists Name|Address");
                                    dtGetDentalHistory.Columns.Remove("Previous Dentists Name|Phone");
                                    dtGetDentalHistory.Columns.Remove("Previous Dentists Name|Email");
                                    dtGetDentalHistory.AcceptChanges();
                                }



                                if (QuestionTest.Contains("Do you make regular visits to the dentist?"))
                                {
                                    int Id = 1;

                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "YesNo";
                                    objDentalHistoryAPI.Question = "Do you make regular visits to the dentist?";

                                    string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtGetDentalHistory.Rows[0]["Do you make regular visits to the dentist?"]), string.Empty);

                                    if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                    {
                                        AnswerForQ12 = "Yes";
                                    }
                                    else
                                    {
                                        AnswerForQ12 = "No";
                                    }
                                    objDentalHistoryAPI.Answer = AnswerForQ12;
                                    for (int j = 0; j < dtGetDentalHistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtGetDentalHistory.Columns[j].ColumnName), string.Empty);




                                        if (Question.Contains("Do you make regular visits to the dentist?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                            if (Question != "Do you make regular visits to the dentist?")
                                            {
                                                string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtGetDentalHistory.Rows[0]["Do you make regular visits to the dentist?|" + Question]), string.Empty);
                                                MultiPartArray objMultiPartArray = new MultiPartArray();

                                                objMultiPartArray.HistoryId = Id;
                                                objMultiPartArray.ItemNumber = Id;
                                                objMultiPartArray.ItemType = "Descriptive";
                                                objMultiPartArray.MultiPart = false;
                                                objMultiPartArray.Question = Question;
                                                objMultiPartArray.Answer = AnswerForQ1;
                                                objMultiPartArray.SingleLine = "Yes";
                                                objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                                Id++;
                                                AnswerForQ1 = "";
                                            }

                                        }
                                        Question = "";


                                    }

                                    dtGetDentalHistory.Columns.Remove("Do you make regular visits to the dentist?|How often");
                                    dtGetDentalHistory.AcceptChanges();
                                    objDentalHistoryAPI.SingleLine = string.Empty;
                                }

                                if (QuestionTest.Contains("Do your gums bleed or hurt?"))
                                {
                                    int Id = 1;
                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "YesNo";

                                    objDentalHistoryAPI.Question = "Do your gums bleed or hurt?";

                                    string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtGetDentalHistory.Rows[0]["Do your gums bleed or hurt?"]), string.Empty);

                                    if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                    {
                                        AnswerForQ12 = "Yes";
                                    }
                                    else
                                    {
                                        AnswerForQ12 = "No";
                                    }
                                    objDentalHistoryAPI.Answer = AnswerForQ12;

                                    for (int j = 0; j < dtGetDentalHistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtGetDentalHistory.Columns[j].ColumnName), string.Empty);




                                        if (Question.Contains("Do your gums bleed or hurt?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);

                                            if (Question != "Do your gums bleed or hurt?")
                                            {
                                                string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtGetDentalHistory.Rows[0]["Do your gums bleed or hurt?|" + Question]), string.Empty);
                                                MultiPartArray objMultiPartArray = new MultiPartArray();

                                                objMultiPartArray.HistoryId = Id;
                                                objMultiPartArray.ItemNumber = Id;
                                                objMultiPartArray.ItemType = "Descriptive";
                                                objMultiPartArray.MultiPart = false;
                                                objMultiPartArray.Question = Question;
                                                objMultiPartArray.Answer = AnswerForQ1;
                                                objMultiPartArray.SingleLine = "Yes";
                                                objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                                Id++;
                                                AnswerForQ1 = "";
                                            }

                                        }
                                        Question = "";


                                    }
                                    objDentalHistoryAPI.SingleLine = string.Empty;
                                    dtGetDentalHistory.Columns.Remove("Do your gums bleed or hurt?|When");
                                    dtGetDentalHistory.AcceptChanges();
                                }


                                if (QuestionTest.Contains("Do you feel your breath is often offensive?"))
                                {
                                    int Id = 1;
                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "YesNo";

                                    objDentalHistoryAPI.Question = "Do you feel your breath is often offensive?";

                                    string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtGetDentalHistory.Rows[0]["Do you feel your breath is often offensive?"]), string.Empty);

                                    if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                    {
                                        AnswerForQ12 = "Yes";
                                    }
                                    else
                                    {
                                        AnswerForQ12 = "No";
                                    }
                                    objDentalHistoryAPI.Answer = AnswerForQ12;
                                    for (int j = 0; j < dtGetDentalHistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtGetDentalHistory.Columns[j].ColumnName), string.Empty);




                                        if (Question.Contains("Do you feel your breath is often offensive?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);
                                            if (Question != "Do you feel your breath is often offensive?")
                                            {
                                                string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtGetDentalHistory.Rows[0]["Do you feel your breath is often offensive?|" + Question]), string.Empty);
                                                MultiPartArray objMultiPartArray = new MultiPartArray();

                                                objMultiPartArray.HistoryId = Id;
                                                objMultiPartArray.ItemNumber = Id;
                                                objMultiPartArray.ItemType = "Descriptive";
                                                objMultiPartArray.MultiPart = false;
                                                objMultiPartArray.Question = Question;
                                                objMultiPartArray.Answer = AnswerForQ1;
                                                objMultiPartArray.SingleLine = "Yes";
                                                objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                                Id++;
                                                AnswerForQ1 = "";
                                            }

                                        }
                                        Question = "";


                                    }
                                    objDentalHistoryAPI.SingleLine = string.Empty;
                                    dtGetDentalHistory.Columns.Remove("Do you feel your breath is often offensive?|When");
                                    dtGetDentalHistory.AcceptChanges();
                                }

                                if (objDentalHistoryAPI.HistoryId == 2 || objDentalHistoryAPI.HistoryId == 4 || objDentalHistoryAPI.HistoryId == 13 || objDentalHistoryAPI.HistoryId == 14 || objDentalHistoryAPI.HistoryId == 17 || objDentalHistoryAPI.HistoryId == 18 || objDentalHistoryAPI.HistoryId == 19 || objDentalHistoryAPI.HistoryId == 20)
                                {
                                    objDentalHistoryAPI.ItemType = "Descriptive";
                                    if (objDentalHistoryAPI.HistoryId == 17 || objDentalHistoryAPI.HistoryId == 18 || objDentalHistoryAPI.HistoryId == 19 || objDentalHistoryAPI.HistoryId == 20)
                                    {
                                        objDentalHistoryAPI.SingleLine = "No";
                                    }
                                    else
                                    {
                                        objDentalHistoryAPI.SingleLine = "Yes";
                                    }

                                    objDentalHistoryAPI.Answer = objCommonDAL.CheckNull(Convert.ToString(dtGetDentalHistory.Rows[0][i]), string.Empty);
                                }
                                DentalHistory.Add(objDentalHistoryAPI);
                                CheckType = "";
                            }
                        }

                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.IsSuccess = true;
                        objResponse.StatusMessage = string.Empty;
                        objResponse.ResultDetail = new { DentalHistory };

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }



                }
                else
                {





                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }



        }

        [HttpPost]
        public string GetInsuranceCoverage(string SessionCode, int MagicNumber)
        {
            string json = "";
            string MethodName = "GetInsuranceCoverage";
            DataTable dtInsurance = new DataTable();
            List<InsuranceCoverage> InsuranceCoverage = new List<InsuranceCoverage>();
            try
            {
                if (IsSessionExpired(SessionCode))
                {


                    dtInsurance = objCommonDAL.GetRegistration(Convert.ToInt32(Session["PatientId"]), "GetRegistrationform");

                    if (dtInsurance != null && dtInsurance.Rows.Count > 0)
                    {


                        InsuranceCoverage objInsuranceCoverage = new InsuranceCoverage();

                        objInsuranceCoverage.ResponsiblePartysFirstName = objCommonDAL.CheckNull(Convert.ToString(dtInsurance.Rows[0]["txtResponsiblepartyFname"]), string.Empty);
                        objInsuranceCoverage.ResponsiblePartysLastName = objCommonDAL.CheckNull(Convert.ToString(dtInsurance.Rows[0]["txtResponsiblepartyLname"]), string.Empty);
                        objInsuranceCoverage.Relationship = objCommonDAL.CheckNull(Convert.ToString(dtInsurance.Rows[0]["txtResponsibleRelationship"]), string.Empty);
                        objInsuranceCoverage.Address = objCommonDAL.CheckNull(Convert.ToString(dtInsurance.Rows[0]["txtResponsibleAddress"]), string.Empty);
                        objInsuranceCoverage.City = objCommonDAL.CheckNull(Convert.ToString(dtInsurance.Rows[0]["txtResponsibleCity"]), string.Empty);

                        objInsuranceCoverage.State = objCommonDAL.CheckNull(Convert.ToString(dtInsurance.Rows[0]["txtResponsibleState"]), string.Empty);
                        objInsuranceCoverage.Zipcode = objCommonDAL.CheckNull(Convert.ToString(dtInsurance.Rows[0]["txtResponsibleZipCode"]), string.Empty);
                        objInsuranceCoverage.PhoneNumber = objCommonDAL.CheckNull(Convert.ToString(dtInsurance.Rows[0]["txtResponsibleContact"]), string.Empty);
                        objInsuranceCoverage.PrimaryInsuranceCompany = objCommonDAL.CheckNull(Convert.ToString(dtInsurance.Rows[0]["txtEmployeeName1"]), string.Empty);
                        objInsuranceCoverage.PrimaryNameOfInsured = objCommonDAL.CheckNull(Convert.ToString(dtInsurance.Rows[0]["txtEmployerName1"]), string.Empty);
                        objInsuranceCoverage.PrimaryDateOfBirth = objCommonDAL.CheckNull(Convert.ToString(dtInsurance.Rows[0]["txtEmployeeDob1"]), string.Empty);

                        objInsuranceCoverage.PrimaryMemberID = objCommonDAL.CheckNull(Convert.ToString(dtInsurance.Rows[0]["txtNameofInsurance1"]), string.Empty);
                        objInsuranceCoverage.PrimaryGroupNumber = objCommonDAL.CheckNull(Convert.ToString(dtInsurance.Rows[0]["txtInsuranceTelephone1"]), string.Empty);
                        objInsuranceCoverage.SecondaryInsuranceCompany = objCommonDAL.CheckNull(Convert.ToString(dtInsurance.Rows[0]["txtEmployeeName2"]), string.Empty);
                        objInsuranceCoverage.SecondaryNameOfInsured = objCommonDAL.CheckNull(Convert.ToString(dtInsurance.Rows[0]["txtEmployerName2"]), string.Empty);
                        objInsuranceCoverage.SecondaryDateOfBirth = objCommonDAL.CheckNull(Convert.ToString(dtInsurance.Rows[0]["txtYearsEmployed2"]), string.Empty);

                        objInsuranceCoverage.SecondaryMemberID = objCommonDAL.CheckNull(Convert.ToString(dtInsurance.Rows[0]["txtNameofInsurance2"]), string.Empty);
                        objInsuranceCoverage.SecondaryGroupNumber = objCommonDAL.CheckNull(Convert.ToString(dtInsurance.Rows[0]["txtInsuranceTelephone2"]), string.Empty);


                        InsuranceCoverage.Add(objInsuranceCoverage);

                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.StatusMessage = string.Empty;
                        objResponse.ResultDetail = new { InsuranceCoverage };


                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                    }
                    else
                    {

                        InsuranceCoverage objInsuranceCoverage = new InsuranceCoverage();

                        objInsuranceCoverage.ResponsiblePartysFirstName = string.Empty;
                        objInsuranceCoverage.ResponsiblePartysLastName = string.Empty;
                        objInsuranceCoverage.Relationship = string.Empty;
                        objInsuranceCoverage.Address = string.Empty;
                        objInsuranceCoverage.City = string.Empty;
                        objInsuranceCoverage.State = string.Empty;
                        objInsuranceCoverage.Zipcode = string.Empty;
                        objInsuranceCoverage.PhoneNumber = string.Empty;

                        objInsuranceCoverage.PrimaryInsuranceCompany = string.Empty;
                        objInsuranceCoverage.PrimaryNameOfInsured = string.Empty;
                        objInsuranceCoverage.PrimaryDateOfBirth = string.Empty;
                        objInsuranceCoverage.PrimaryMemberID = string.Empty;
                        objInsuranceCoverage.PrimaryGroupNumber = string.Empty;

                        objInsuranceCoverage.SecondaryInsuranceCompany = string.Empty;
                        objInsuranceCoverage.SecondaryNameOfInsured = string.Empty;
                        objInsuranceCoverage.SecondaryDateOfBirth = string.Empty;
                        objInsuranceCoverage.SecondaryMemberID = string.Empty;
                        objInsuranceCoverage.SecondaryGroupNumber = string.Empty;


                        InsuranceCoverage.Add(objInsuranceCoverage);

                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = string.Empty;
                        objResponse.ResultDetail = new { InsuranceCoverage };

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }



                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }



        }

        [HttpPost]
        public string GetMedicalHistory(string SessionCode, int MagicNumber)
        {
            string json = "";
            string MethodName = "GetMedicalHistory";
            DataTable dtMedicalhistory = new DataTable();
            List<DentalHistoryAPI> MedicalHistory = new List<DentalHistoryAPI>();
            try
            {
                if (IsSessionExpired(SessionCode))
                {


                    dtMedicalhistory = objCommonDAL.GetMedicalHistory(Convert.ToInt32(Session["PatientId"]), "GetMedicalHistory");


                    if (dtMedicalhistory != null && dtMedicalhistory.Rows.Count > 0)
                    {


                        dtMedicalhistory.Columns["MrdQue1"].ColumnName = "Are you under a physicians care right now?";
                        dtMedicalhistory.Columns["Mtxtphysicians"].ColumnName = "Are you under a physicians care right now?|Please explain";
                        dtMedicalhistory.Columns["MrdQue2"].ColumnName = "Have you ever been hospitalized or had a major operation?";
                        dtMedicalhistory.Columns["Mtxthospitalized"].ColumnName = "Have you ever been hospitalized or had a major operation?|Please explain";
                        dtMedicalhistory.Columns["MrdQue3"].ColumnName = "Have you ever had a serious head or neck injury?";


                        dtMedicalhistory.Columns["Mtxtserious"].ColumnName = "Have you ever had a serious head or neck injury?|Please explain";
                        dtMedicalhistory.Columns["MrdQue4"].ColumnName = "Are taking any medications, pills, or drugs?";
                        dtMedicalhistory.Columns["Mtxtmedications"].ColumnName = "Are taking any medications, pills, or drugs?|Please list";
                        dtMedicalhistory.Columns["MrdQue5"].ColumnName = "Do you take, or have you taken, Phen-Fen or Redux?";
                        dtMedicalhistory.Columns["MtxtRedux"].ColumnName = "Do you take, or have you taken, Phen-Fen or Redux?|Please explain";


                        dtMedicalhistory.Columns["MrdQue6"].ColumnName = "Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?";
                        dtMedicalhistory.Columns["MtxtFosamax"].ColumnName = "Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?|Please explain";
                        dtMedicalhistory.Columns["MrdQuediet7"].ColumnName = "Are you on a special diet?";
                        dtMedicalhistory.Columns["Mtxt7"].ColumnName = "Are you on a special diet?|Please explain";
                        dtMedicalhistory.Columns["Mrdotobacco8"].ColumnName = "Do you use tobacco?";


                        dtMedicalhistory.Columns["Mtxt8"].ColumnName = "Do you use tobacco?|How often";
                        dtMedicalhistory.Columns["Mrdosubstances"].ColumnName = "Do you use controlled substances?";
                        dtMedicalhistory.Columns["Mtxt9"].ColumnName = "Do you use controlled substances?|Please explain";
                        dtMedicalhistory.Columns["Mrdopregnant"].ColumnName = "Are you pregnant/Trying to get pregnant(women)?";
                        dtMedicalhistory.Columns["Mtxt10"].ColumnName = "Are you pregnant/Trying to get pregnant(women)?|Due date";

                        dtMedicalhistory.Columns["Mrdocontraceptives"].ColumnName = "Taking oral contraceptives(women)?";
                        dtMedicalhistory.Columns["MrdoNursing"].ColumnName = "Nursing(women)?";
                        dtMedicalhistory.Columns["MchkQue_1"].ColumnName = "Are you allergic to any of the following?|Aspirin";
                        dtMedicalhistory.Columns["MchkQue_2"].ColumnName = "Are you allergic to any of the following?|Penicillin";
                        dtMedicalhistory.Columns["MchkQue_3"].ColumnName = "Are you allergic to any of the following?|Codeine";


                        dtMedicalhistory.Columns["MchkQue_4"].ColumnName = "Are you allergic to any of the following?|Local Anesthetics";
                        dtMedicalhistory.Columns["MchkQue_5"].ColumnName = "Are you allergic to any of the following?|Acrylic";
                        dtMedicalhistory.Columns["MchkQue_6"].ColumnName = "Are you allergic to any of the following?|Metal";
                        dtMedicalhistory.Columns["MchkQue_7"].ColumnName = "Are you allergic to any of the following?|Latex";
                        dtMedicalhistory.Columns["MchkQue_8"].ColumnName = "Are you allergic to any of the following?|Sulfa Drugs";


                        dtMedicalhistory.Columns["MchkQue_9"].ColumnName = "Are you allergic to any of the following?|Other";
                        dtMedicalhistory.Columns["MtxtchkQue_9"].ColumnName = "Are you allergic to any of the following?|If other, please explain list";
                        dtMedicalhistory.Columns["MrdQueAIDS_HIV_Positive"].ColumnName = "Do you have, or have you had, any of the following?|AIDS/HIV Positive";
                        dtMedicalhistory.Columns["MrdQueAlzheimer"].ColumnName = "Do you have, or have you had, any of the following?|Alzheimer’s Disease";
                        dtMedicalhistory.Columns["MrdQueAnaphylaxis"].ColumnName = "Do you have, or have you had, any of the following?|Anaphylaxis";

                        dtMedicalhistory.Columns["MrdQueAnemia"].ColumnName = "Do you have, or have you had, any of the following?|Anemia";
                        dtMedicalhistory.Columns["MrdQueAngina"].ColumnName = "Do you have, or have you had, any of the following?|Angina";
                        dtMedicalhistory.Columns["MrdQueArthritis_Gout"].ColumnName = "Do you have, or have you had, any of the following?|Arthritis/Gout";
                        dtMedicalhistory.Columns["MrdQueArtificialHeartValve"].ColumnName = "Do you have, or have you had, any of the following?|Artificial Heart Valve";
                        dtMedicalhistory.Columns["MrdQueArtificialJoint"].ColumnName = "Do you have, or have you had, any of the following?|Artificial Joint";


                        dtMedicalhistory.Columns["MrdQueAsthma"].ColumnName = "Do you have, or have you had, any of the following?|Asthma";
                        dtMedicalhistory.Columns["MrdQueBloodDisease"].ColumnName = "Do you have, or have you had, any of the following?|Blood Disease";
                        dtMedicalhistory.Columns["MrdQueBloodTransfusion"].ColumnName = "Do you have, or have you had, any of the following?|Blood Transfusion";
                        dtMedicalhistory.Columns["MrdQueBreathing"].ColumnName = "Do you have, or have you had, any of the following?|Breathing Problem";
                        dtMedicalhistory.Columns["MrdQueBruise"].ColumnName = "Do you have, or have you had, any of the following?|Bruise Easily";

                        dtMedicalhistory.Columns["MrdQueCancer"].ColumnName = "Do you have, or have you had, any of the following?|Cancer";
                        dtMedicalhistory.Columns["MrdQueChemotherapy"].ColumnName = "Do you have, or have you had, any of the following?|Chemotherapy";
                        dtMedicalhistory.Columns["MrdQueChest"].ColumnName = "Do you have, or have you had, any of the following?|Chest Pains";
                        dtMedicalhistory.Columns["MrdQueCold_Sores_Fever"].ColumnName = "Do you have, or have you had, any of the following?|Cold Sores/Fever Blisters";
                        dtMedicalhistory.Columns["MrdQueCongenital"].ColumnName = "Do you have, or have you had, any of the following?|Congenital Heart Disorder";


                        dtMedicalhistory.Columns["MrdQueConvulsions"].ColumnName = "Do you have, or have you had, any of the following?|Convulsions";
                        dtMedicalhistory.Columns["MrdQueCortisone"].ColumnName = "Do you have, or have you had, any of the following?|Cortisone Medicine";
                        dtMedicalhistory.Columns["MrdQueDiabetes"].ColumnName = "Do you have, or have you had, any of the following?|Diabetes";
                        dtMedicalhistory.Columns["MrdQueDrug"].ColumnName = "Do you have, or have you had, any of the following?|Drug Addiction";
                        dtMedicalhistory.Columns["MrdQueEasily"].ColumnName = "Do you have, or have you had, any of the following?|Easily Winded";

                        dtMedicalhistory.Columns["MrdQueEmphysema"].ColumnName = "Do you have, or have you had, any of the following?|Emphysema";
                        dtMedicalhistory.Columns["MrdQueEpilepsy"].ColumnName = "Do you have, or have you had, any of the following?|Epilepsy or Seizures";
                        dtMedicalhistory.Columns["MrdQueExcessiveBleeding"].ColumnName = "Do you have, or have you had, any of the following?|Excessive Bleeding";
                        dtMedicalhistory.Columns["MrdQueExcessiveThirst"].ColumnName = "Do you have, or have you had, any of the following?|Excessive Thirst";
                        dtMedicalhistory.Columns["MrdQueFainting"].ColumnName = "Do you have, or have you had, any of the following?|Fainting Spells/Dizziness";


                        dtMedicalhistory.Columns["MrdQueFrequentCough"].ColumnName = "Do you have, or have you had, any of the following?|Frequent Cough";
                        dtMedicalhistory.Columns["MrdQueFrequentDiarrhea"].ColumnName = "Do you have, or have you had, any of the following?|Frequent Diarrhea";
                        dtMedicalhistory.Columns["MrdQueFrequentHeadaches"].ColumnName = "Do you have, or have you had, any of the following?|Frequent Headaches";
                        dtMedicalhistory.Columns["MrdQueGenital"].ColumnName = "Do you have, or have you had, any of the following?|Genital Herpes";
                        dtMedicalhistory.Columns["MrdQueGlaucoma"].ColumnName = "Do you have, or have you had, any of the following?|Glaucoma";

                        dtMedicalhistory.Columns["MrdQueHay"].ColumnName = "Do you have, or have you had, any of the following?|Hay Fever";
                        dtMedicalhistory.Columns["MrdQueHeartAttack_Failure"].ColumnName = "Do you have, or have you had, any of the following?|Heart Attack/Failure";
                        dtMedicalhistory.Columns["MrdQueHeartMurmur"].ColumnName = "Do you have, or have you had, any of the following?|Heart Murmur";
                        dtMedicalhistory.Columns["MrdQueHeartPacemaker"].ColumnName = "Do you have, or have you had, any of the following?|Heart Pacemaker";
                        dtMedicalhistory.Columns["MrdQueHeartTrouble_Disease"].ColumnName = "Do you have, or have you had, any of the following?|Heart Trouble/Disease";


                        dtMedicalhistory.Columns["MrdQueHemophilia"].ColumnName = "Do you have, or have you had, any of the following?|Hemophilia";
                        dtMedicalhistory.Columns["MrdQueHepatitisA"].ColumnName = "Do you have, or have you had, any of the following?|Hepatitis A";
                        dtMedicalhistory.Columns["MrdQueHepatitisBorC"].ColumnName = "Do you have, or have you had, any of the following?|Hepatitis B or C";
                        dtMedicalhistory.Columns["MrdQueHerpes"].ColumnName = "Do you have, or have you had, any of the following?|Herpes";
                        dtMedicalhistory.Columns["MrdQueHighBloodPressure"].ColumnName = "Do you have, or have you had, any of the following?|High Blood Pressure";

                        dtMedicalhistory.Columns["MrdQueHighCholesterol"].ColumnName = "Do you have, or have you had, any of the following?|High Cholesterol";
                        dtMedicalhistory.Columns["MrdQueHives"].ColumnName = "Do you have, or have you had, any of the following?|Hives or Rash";
                        dtMedicalhistory.Columns["MrdQueHypoglycemia"].ColumnName = "Do you have, or have you had, any of the following?|Hypoglycemia";
                        dtMedicalhistory.Columns["MrdQueIrregular"].ColumnName = "Do you have, or have you had, any of the following?|Irregular Heartbeat";
                        dtMedicalhistory.Columns["MrdQueKidney"].ColumnName = "Do you have, or have you had, any of the following?|Kidney Problems";


                        dtMedicalhistory.Columns["MrdQueLeukemia"].ColumnName = "Do you have, or have you had, any of the following?|Leukemia";
                        dtMedicalhistory.Columns["MrdQueLiver"].ColumnName = "Do you have, or have you had, any of the following?|Liver Disease";
                        dtMedicalhistory.Columns["MrdQueLow"].ColumnName = "Do you have, or have you had, any of the following?|Low Blood Pressure";
                        dtMedicalhistory.Columns["MrdQueLung"].ColumnName = "Do you have, or have you had, any of the following?|Lung Disease";
                        dtMedicalhistory.Columns["MrdQueMitral"].ColumnName = "Do you have, or have you had, any of the following?|Mitral Valve Prolapse";

                        dtMedicalhistory.Columns["MrdQueOsteoporosis"].ColumnName = "Do you have, or have you had, any of the following?|Osteoporosis";
                        dtMedicalhistory.Columns["MrdQuePain"].ColumnName = "Do you have, or have you had, any of the following?|Pain in Jaw Joints";
                        dtMedicalhistory.Columns["MrdQueParathyroid"].ColumnName = "Do you have, or have you had, any of the following?|Parathyroid Disease";
                        dtMedicalhistory.Columns["MrdQuePsychiatric"].ColumnName = "Do you have, or have you had, any of the following?|Psychiatric Care";
                        dtMedicalhistory.Columns["MrdQueRadiation"].ColumnName = "Do you have, or have you had, any of the following?|Radiation Treatments";


                        dtMedicalhistory.Columns["MrdQueRecent"].ColumnName = "Do you have, or have you had, any of the following?|Recent Weight Loss";
                        dtMedicalhistory.Columns["MrdQueRenal"].ColumnName = "Do you have, or have you had, any of the following?|Renal Dialysis";
                        dtMedicalhistory.Columns["MrdQueRheumatic"].ColumnName = "Do you have, or have you had, any of the following?|Rheumatic Fever";
                        dtMedicalhistory.Columns["MrdQueRheumatism"].ColumnName = "Do you have, or have you had, any of the following?|Rheumatism";
                        dtMedicalhistory.Columns["MrdQueScarlet"].ColumnName = "Do you have, or have you had, any of the following?|Scarlet Fever";

                        dtMedicalhistory.Columns["MrdQueShingles"].ColumnName = "Do you have, or have you had, any of the following?|Shingles";
                        dtMedicalhistory.Columns["MrdQueSickle"].ColumnName = "Do you have, or have you had, any of the following?|Sickle Cell Disease";
                        dtMedicalhistory.Columns["MrdQueSinus"].ColumnName = "Do you have, or have you had, any of the following?|Sinus Trouble";
                        dtMedicalhistory.Columns["MrdQueSpina"].ColumnName = "Do you have, or have you had, any of the following?|Spina Bifida";
                        dtMedicalhistory.Columns["MrdQueStomach"].ColumnName = "Do you have, or have you had, any of the following?|Stomach/Intestinal Disease";

                        dtMedicalhistory.Columns["MrdQueStroke"].ColumnName = "Do you have, or have you had, any of the following?|Stroke";
                        dtMedicalhistory.Columns["MrdQueSwelling"].ColumnName = "Do you have, or have you had, any of the following?|Swelling of Limbs";
                        dtMedicalhistory.Columns["MrdQueThyroid"].ColumnName = "Do you have, or have you had, any of the following?|Thyroid Disease";
                        dtMedicalhistory.Columns["MrdQueTonsillitis"].ColumnName = "Do you have, or have you had, any of the following?|Tonsillitis";
                        dtMedicalhistory.Columns["MrdQueTuberculosis"].ColumnName = "Do you have, or have you had, any of the following?|Tuberculosis";


                        dtMedicalhistory.Columns["MrdQueTumors"].ColumnName = "Do you have, or have you had, any of the following?|Tumors or Growths";
                        dtMedicalhistory.Columns["MrdQueUlcers"].ColumnName = "Do you have, or have you had, any of the following?|Ulcers";
                        dtMedicalhistory.Columns["MrdQueVenereal"].ColumnName = "Do you have, or have you had, any of the following?|Venereal Disease";
                        dtMedicalhistory.Columns["MrdQueYellow"].ColumnName = "Do you have, or have you had, any of the following?|Yellow Jaundice";
                        dtMedicalhistory.Columns["Mtxtillness"].ColumnName = "List any serious illness not listed above";

                        dtMedicalhistory.Columns["MtxtComments"].ColumnName = "Additional Information";

                        dtMedicalhistory.Columns.Remove("Mtxtbiphosphonates");
                        dtMedicalhistory.Columns.Remove("Mrdoillness");

                        dtMedicalhistory.Columns.Remove("Are you allergic to any of the following?|Other");

                        dtMedicalhistory.Columns["Are you under a physicians care right now?"].SetOrdinal(0);
                        dtMedicalhistory.Columns["Have you ever been hospitalized or had a major operation?"].SetOrdinal(1);
                        dtMedicalhistory.Columns["Have you ever had a serious head or neck injury?"].SetOrdinal(2);
                        dtMedicalhistory.Columns["Are taking any medications, pills, or drugs?"].SetOrdinal(3);
                        dtMedicalhistory.Columns["Do you take, or have you taken, Phen-Fen or Redux?"].SetOrdinal(4);

                        dtMedicalhistory.Columns["Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?"].SetOrdinal(5);
                        dtMedicalhistory.Columns["Are you on a special diet?"].SetOrdinal(6);
                        dtMedicalhistory.Columns["Do you use tobacco?"].SetOrdinal(7);
                        dtMedicalhistory.Columns["Do you use controlled substances?"].SetOrdinal(8);
                        dtMedicalhistory.Columns["Are you pregnant/Trying to get pregnant(women)?"].SetOrdinal(9);


                        dtMedicalhistory.Columns["Taking oral contraceptives(women)?"].SetOrdinal(10);
                        dtMedicalhistory.Columns["Nursing(women)?"].SetOrdinal(11);
                        dtMedicalhistory.Columns["Are you allergic to any of the following?|Aspirin"].SetOrdinal(12);
                        dtMedicalhistory.Columns["Do you have, or have you had, any of the following?|AIDS/HIV Positive"].SetOrdinal(13);
                        dtMedicalhistory.Columns["List any serious illness not listed above"].SetOrdinal(14);

                        dtMedicalhistory.Columns["Additional Information"].SetOrdinal(15);

                        dtMedicalhistory.AcceptChanges();



                        for (int i = 0; i < dtMedicalhistory.Columns.Count; i++)
                        {
                            DentalHistoryAPI objDentalHistoryAPI = new DentalHistoryAPI();
                            objDentalHistoryAPI.HistoryId = i + 1;
                            objDentalHistoryAPI.ItemNumber = i + 1;


                            string CheckType = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0][i]), string.Empty);

                            if (CheckType == "Y" || CheckType == "N")
                            {
                                objDentalHistoryAPI.ItemType = "YesNo";

                            }
                            else
                            {
                                objDentalHistoryAPI.ItemType = "Descriptive";
                            }



                            objDentalHistoryAPI.Question = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Columns[i].ColumnName), string.Empty);
                            string Answer = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0][i]), string.Empty);
                            if (Answer == "Y")
                            {
                                objDentalHistoryAPI.Answer = "Yes";
                            }
                            else if (Answer == "N")
                            {
                                objDentalHistoryAPI.Answer = "No";
                            }
                            else
                            {
                                objDentalHistoryAPI.Answer = Answer;
                            }

                            objDentalHistoryAPI.SingleLine = string.Empty;
                            if (objDentalHistoryAPI.ItemType == "YesNo")
                            {
                                if (!objDentalHistoryAPI.Question.Contains("|"))
                                {
                                    objDentalHistoryAPI.MultiPart = false;
                                }
                                else
                                {
                                    objDentalHistoryAPI.MultiPart = true;
                                }

                            }
                            else
                            {
                                objDentalHistoryAPI.MultiPart = false;
                            }
                            if (objDentalHistoryAPI.Question.Contains("Are you allergic to any of the following?"))
                            {
                                int Id = 1;
                                for (int j = 0; j < dtMedicalhistory.Columns.Count; j++)
                                {

                                    string Question = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Columns[j].ColumnName), string.Empty);
                                    string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0][j]), string.Empty);

                                    if (!string.IsNullOrEmpty(AnswerForQ1) && AnswerForQ1 == "Y")
                                    {
                                        AnswerForQ1 = "Yes";
                                    }
                                    else
                                    {
                                        AnswerForQ1 = "No";
                                    }






                                    if (Question.Contains("Are you allergic to any of the following?"))
                                    {
                                        Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                        MultiPartArray objMultiPartArray = new MultiPartArray();

                                        objMultiPartArray.HistoryId = Id;
                                        objMultiPartArray.ItemNumber = Id;
                                        objMultiPartArray.ItemType = "YesNo";
                                        objMultiPartArray.MultiPart = false;
                                        objMultiPartArray.Question = Question;
                                        objMultiPartArray.Answer = AnswerForQ1;
                                        objMultiPartArray.SingleLine = string.Empty;



                                        //"If other, please explain list"
                                        if (Question == "If other, please explain list")
                                        {

                                            objMultiPartArray.HistoryId = Id;
                                            objMultiPartArray.ItemNumber = Id;
                                            objMultiPartArray.ItemType = "Descriptive";
                                            objMultiPartArray.MultiPart = false;
                                            objMultiPartArray.Question = "If other, please explain list";
                                            objMultiPartArray.Answer = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Are you allergic to any of the following?|If other, please explain list"]), string.Empty);
                                            objMultiPartArray.SingleLine = "Yes";
                                        }


                                        objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                        Id++;
                                    }
                                    Question = "";
                                    AnswerForQ1 = "";

                                }
                                dtMedicalhistory.Columns.Remove("Are you allergic to any of the following?|Penicillin");
                                dtMedicalhistory.Columns.Remove("Are you allergic to any of the following?|Codeine");
                                dtMedicalhistory.Columns.Remove("Are you allergic to any of the following?|Local Anesthetics");

                                dtMedicalhistory.Columns.Remove("Are you allergic to any of the following?|Acrylic");
                                dtMedicalhistory.Columns.Remove("Are you allergic to any of the following?|Metal");
                                dtMedicalhistory.Columns.Remove("Are you allergic to any of the following?|Latex");
                                dtMedicalhistory.Columns.Remove("Are you allergic to any of the following?|Sulfa Drugs");


                                dtMedicalhistory.Columns.Remove("Are you allergic to any of the following?|If other, please explain list");

                                dtMedicalhistory.AcceptChanges();

                                objDentalHistoryAPI.Question = "Are you allergic to any of the following?";
                                objDentalHistoryAPI.Answer = string.Empty;
                                objDentalHistoryAPI.MultiPart = true;
                                objDentalHistoryAPI.ItemType = "YesNo";
                                objDentalHistoryAPI.SingleLine = string.Empty;
                            }

                            if (objDentalHistoryAPI.Question.Contains("Do you have, or have you had, any of the following?"))
                            {
                                int Id = 1;
                                for (int j = 0; j < dtMedicalhistory.Columns.Count; j++)
                                {

                                    string Question = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Columns[j].ColumnName), string.Empty);
                                    string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0][j]), string.Empty);

                                    if (!string.IsNullOrEmpty(AnswerForQ1) && AnswerForQ1 == "Y")
                                    {
                                        AnswerForQ1 = "Yes";
                                    }
                                    else
                                    {
                                        AnswerForQ1 = "No";
                                    }

                                    if (Question.Contains("Do you have, or have you had, any of the following?"))
                                    {
                                        Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                        MultiPartArray objMultiPartArray = new MultiPartArray();

                                        objMultiPartArray.HistoryId = Id;
                                        objMultiPartArray.ItemNumber = Id;
                                        objMultiPartArray.ItemType = "YesNo";
                                        objMultiPartArray.MultiPart = false;
                                        objMultiPartArray.Question = Question;
                                        objMultiPartArray.Answer = AnswerForQ1;
                                        objMultiPartArray.SingleLine = string.Empty;
                                        objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                        Id++;
                                    }
                                    Question = "";
                                    AnswerForQ1 = "";

                                }







                                // dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|AIDS/HIV Positive");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Alzheimer’s Disease");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Anaphylaxis");

                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Anemia");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Angina");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Arthritis/Gout");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Artificial Heart Valve");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Artificial Joint");


                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Asthma");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Blood Disease");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Blood Transfusion");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Breathing Problem");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Bruise Easily");

                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Cancer");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Chemotherapy");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Chest Pains");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Cold Sores/Fever Blisters");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Congenital Heart Disorder");


                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Convulsions");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Cortisone Medicine");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Diabetes");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Drug Addiction");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Easily Winded");

                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Emphysema");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Epilepsy or Seizures");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Excessive Bleeding");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Excessive Thirst");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Fainting Spells/Dizziness");


                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Frequent Cough");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Frequent Diarrhea");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Frequent Headaches");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Genital Herpes");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Glaucoma");

                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Hay Fever");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Heart Attack/Failure");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Heart Murmur");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Heart Pacemaker");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Heart Trouble/Disease");


                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Hemophilia");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Hepatitis A");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Hepatitis B or C");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Herpes");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|High Blood Pressure");

                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|High Cholesterol");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Hives or Rash");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Hypoglycemia");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Irregular Heartbeat");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Kidney Problems");


                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Leukemia");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Liver Disease");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Low Blood Pressure");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Lung Disease");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Mitral Valve Prolapse");

                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Osteoporosis");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Pain in Jaw Joints");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Parathyroid Disease");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Psychiatric Care");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Radiation Treatments");


                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Recent Weight Loss");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Renal Dialysis");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Rheumatic Fever");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Rheumatism");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Scarlet Fever");

                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Shingles");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Sickle Cell Disease");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Sinus Trouble");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Spina Bifida");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Stomach/Intestinal Disease");

                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Stroke");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Swelling of Limbs");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Thyroid Disease");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Tonsillitis");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Tuberculosis");


                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Tumors or Growths");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Ulcers");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Venereal Disease");
                                dtMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Yellow Jaundice");




                                dtMedicalhistory.AcceptChanges();

                                objDentalHistoryAPI.Question = "Do you have, or have you had, any of the following?";
                                objDentalHistoryAPI.Answer = string.Empty;
                                objDentalHistoryAPI.MultiPart = true;
                                objDentalHistoryAPI.ItemType = "YesNo";
                                objDentalHistoryAPI.SingleLine = string.Empty;
                            }
                            if (objDentalHistoryAPI.Question.Contains("Are you under a physicians care right now?"))
                            {
                                int Id = 1;

                                objDentalHistoryAPI.MultiPart = true;
                                objDentalHistoryAPI.ItemType = "YesNo";
                                objDentalHistoryAPI.Question = "Are you under a physicians care right now?";

                                string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Are you under a physicians care right now?"]), string.Empty);

                                if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                {
                                    AnswerForQ12 = "Yes";
                                }
                                else
                                {
                                    AnswerForQ12 = "No";
                                }
                                objDentalHistoryAPI.Answer = AnswerForQ12;
                                for (int j = 0; j < dtMedicalhistory.Columns.Count; j++)
                                {

                                    string Question = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Columns[j].ColumnName), string.Empty);




                                    if (Question.Contains("Are you under a physicians care right now?"))
                                    {
                                        Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                        if (Question != "Are you under a physicians care right now?")
                                        {
                                            string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Are you under a physicians care right now?|" + Question]), string.Empty);
                                            MultiPartArray objMultiPartArray = new MultiPartArray();

                                            objMultiPartArray.HistoryId = Id;
                                            objMultiPartArray.ItemNumber = Id;
                                            objMultiPartArray.ItemType = "Descriptive";
                                            objMultiPartArray.MultiPart = false;
                                            objMultiPartArray.Question = Question;
                                            objMultiPartArray.Answer = AnswerForQ1;
                                            objMultiPartArray.SingleLine = "No";
                                            objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                            Id++;
                                            AnswerForQ1 = "";
                                        }

                                    }
                                    Question = "";


                                }

                                dtMedicalhistory.Columns.Remove("Are you under a physicians care right now?|Please explain");
                                dtMedicalhistory.AcceptChanges();

                            }

                            if (objDentalHistoryAPI.Question.Contains("Have you ever been hospitalized or had a major operation?"))
                            {
                                int Id = 1;

                                objDentalHistoryAPI.MultiPart = true;
                                objDentalHistoryAPI.ItemType = "YesNo";
                                objDentalHistoryAPI.Question = "Have you ever been hospitalized or had a major operation?";

                                string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Have you ever been hospitalized or had a major operation?"]), string.Empty);

                                if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                {
                                    AnswerForQ12 = "Yes";
                                }
                                else
                                {
                                    AnswerForQ12 = "No";
                                }
                                objDentalHistoryAPI.Answer = AnswerForQ12;
                                for (int j = 0; j < dtMedicalhistory.Columns.Count; j++)
                                {

                                    string Question = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Columns[j].ColumnName), string.Empty);




                                    if (Question.Contains("Have you ever been hospitalized or had a major operation?"))
                                    {
                                        Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                        if (Question != "Have you ever been hospitalized or had a major operation?")
                                        {
                                            string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Have you ever been hospitalized or had a major operation?|" + Question]), string.Empty);
                                            MultiPartArray objMultiPartArray = new MultiPartArray();

                                            objMultiPartArray.HistoryId = Id;
                                            objMultiPartArray.ItemNumber = Id;
                                            objMultiPartArray.ItemType = "Descriptive";
                                            objMultiPartArray.MultiPart = false;
                                            objMultiPartArray.Question = Question;
                                            objMultiPartArray.Answer = AnswerForQ1;
                                            objMultiPartArray.SingleLine = "No";
                                            objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                            Id++;
                                            AnswerForQ1 = "";
                                        }

                                    }
                                    Question = "";


                                }

                                dtMedicalhistory.Columns.Remove("Have you ever been hospitalized or had a major operation?|Please explain");
                                dtMedicalhistory.AcceptChanges();

                            }

                            if (objDentalHistoryAPI.Question.Contains("Have you ever had a serious head or neck injury?"))
                            {
                                int Id = 1;

                                objDentalHistoryAPI.MultiPart = true;
                                objDentalHistoryAPI.ItemType = "YesNo";
                                objDentalHistoryAPI.Question = "Have you ever had a serious head or neck injury?";

                                string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Have you ever had a serious head or neck injury?"]), string.Empty);

                                if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                {
                                    AnswerForQ12 = "Yes";
                                }
                                else
                                {
                                    AnswerForQ12 = "No";
                                }
                                objDentalHistoryAPI.Answer = AnswerForQ12;
                                for (int j = 0; j < dtMedicalhistory.Columns.Count; j++)
                                {

                                    string Question = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Columns[j].ColumnName), string.Empty);




                                    if (Question.Contains("Have you ever had a serious head or neck injury?"))
                                    {
                                        Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                        if (Question != "Have you ever had a serious head or neck injury?")
                                        {
                                            string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Have you ever had a serious head or neck injury?|" + Question]), string.Empty);
                                            MultiPartArray objMultiPartArray = new MultiPartArray();

                                            objMultiPartArray.HistoryId = Id;
                                            objMultiPartArray.ItemNumber = Id;
                                            objMultiPartArray.ItemType = "Descriptive";
                                            objMultiPartArray.MultiPart = false;
                                            objMultiPartArray.Question = Question;
                                            objMultiPartArray.Answer = AnswerForQ1;
                                            objMultiPartArray.SingleLine = "No";
                                            objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                            Id++;
                                            AnswerForQ1 = "";
                                        }

                                    }
                                    Question = "";


                                }

                                dtMedicalhistory.Columns.Remove("Have you ever had a serious head or neck injury?|Please explain");
                                dtMedicalhistory.AcceptChanges();

                            }
                            if (objDentalHistoryAPI.Question.Contains("Are taking any medications, pills, or drugs?"))
                            {
                                int Id = 1;

                                objDentalHistoryAPI.MultiPart = true;
                                objDentalHistoryAPI.ItemType = "YesNo";
                                objDentalHistoryAPI.Question = "Are taking any medications, pills, or drugs?";

                                string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Are taking any medications, pills, or drugs?"]), string.Empty);

                                if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                {
                                    AnswerForQ12 = "Yes";
                                }
                                else
                                {
                                    AnswerForQ12 = "No";
                                }
                                objDentalHistoryAPI.Answer = AnswerForQ12;
                                for (int j = 0; j < dtMedicalhistory.Columns.Count; j++)
                                {

                                    string Question = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Columns[j].ColumnName), string.Empty);




                                    if (Question.Contains("Are taking any medications, pills, or drugs?"))
                                    {
                                        Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                        if (Question != "Are taking any medications, pills, or drugs?")
                                        {
                                            string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Are taking any medications, pills, or drugs?|" + Question]), string.Empty);
                                            MultiPartArray objMultiPartArray = new MultiPartArray();

                                            objMultiPartArray.HistoryId = Id;
                                            objMultiPartArray.ItemNumber = Id;
                                            objMultiPartArray.ItemType = "Descriptive";
                                            objMultiPartArray.MultiPart = false;
                                            objMultiPartArray.Question = Question;
                                            objMultiPartArray.Answer = AnswerForQ1;
                                            objMultiPartArray.SingleLine = "No";
                                            objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                            Id++;
                                            AnswerForQ1 = "";
                                        }

                                    }
                                    Question = "";


                                }

                                dtMedicalhistory.Columns.Remove("Are taking any medications, pills, or drugs?|Please list");
                                dtMedicalhistory.AcceptChanges();

                            }

                            if (objDentalHistoryAPI.Question.Contains("Do you take, or have you taken, Phen-Fen or Redux?"))
                            {
                                int Id = 1;

                                objDentalHistoryAPI.MultiPart = true;
                                objDentalHistoryAPI.ItemType = "YesNo";
                                objDentalHistoryAPI.Question = "Do you take, or have you taken, Phen-Fen or Redux?";

                                string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Do you take, or have you taken, Phen-Fen or Redux?"]), string.Empty);

                                if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                {
                                    AnswerForQ12 = "Yes";
                                }
                                else
                                {
                                    AnswerForQ12 = "No";
                                }
                                objDentalHistoryAPI.Answer = AnswerForQ12;
                                for (int j = 0; j < dtMedicalhistory.Columns.Count; j++)
                                {

                                    string Question = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Columns[j].ColumnName), string.Empty);




                                    if (Question.Contains("Do you take, or have you taken, Phen-Fen or Redux?"))
                                    {
                                        Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                        if (Question != "Do you take, or have you taken, Phen-Fen or Redux?")
                                        {
                                            string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Do you take, or have you taken, Phen-Fen or Redux?|" + Question]), string.Empty);
                                            MultiPartArray objMultiPartArray = new MultiPartArray();

                                            objMultiPartArray.HistoryId = Id;
                                            objMultiPartArray.ItemNumber = Id;
                                            objMultiPartArray.ItemType = "Descriptive";
                                            objMultiPartArray.MultiPart = false;
                                            objMultiPartArray.Question = Question;
                                            objMultiPartArray.Answer = AnswerForQ1;
                                            objMultiPartArray.SingleLine = "No";
                                            objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                            Id++;
                                            AnswerForQ1 = "";
                                        }

                                    }
                                    Question = "";


                                }

                                dtMedicalhistory.Columns.Remove("Do you take, or have you taken, Phen-Fen or Redux?|Please explain");
                                dtMedicalhistory.AcceptChanges();

                            }

                            if (objDentalHistoryAPI.Question.Contains("Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?"))
                            {
                                int Id = 1;

                                objDentalHistoryAPI.MultiPart = true;
                                objDentalHistoryAPI.ItemType = "YesNo";
                                objDentalHistoryAPI.Question = "Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?";

                                string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?"]), string.Empty);

                                if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                {
                                    AnswerForQ12 = "Yes";
                                }
                                else
                                {
                                    AnswerForQ12 = "No";
                                }
                                objDentalHistoryAPI.Answer = AnswerForQ12;
                                for (int j = 0; j < dtMedicalhistory.Columns.Count; j++)
                                {

                                    string Question = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Columns[j].ColumnName), string.Empty);




                                    if (Question.Contains("Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?"))
                                    {
                                        Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                        if (Question != "Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?")
                                        {
                                            string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?|" + Question]), string.Empty);
                                            MultiPartArray objMultiPartArray = new MultiPartArray();

                                            objMultiPartArray.HistoryId = Id;
                                            objMultiPartArray.ItemNumber = Id;
                                            objMultiPartArray.ItemType = "Descriptive";
                                            objMultiPartArray.MultiPart = false;
                                            objMultiPartArray.Question = Question;
                                            objMultiPartArray.Answer = AnswerForQ1;
                                            objMultiPartArray.SingleLine = "No";
                                            objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                            Id++;
                                            AnswerForQ1 = "";
                                        }

                                    }
                                    Question = "";


                                }

                                dtMedicalhistory.Columns.Remove("Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?|Please explain");
                                dtMedicalhistory.AcceptChanges();

                            }

                            if (objDentalHistoryAPI.Question.Contains("Are you on a special diet?"))
                            {
                                int Id = 1;

                                objDentalHistoryAPI.MultiPart = true;
                                objDentalHistoryAPI.ItemType = "YesNo";
                                objDentalHistoryAPI.Question = "Are you on a special diet?";

                                string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Are you on a special diet?"]), string.Empty);

                                if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                {
                                    AnswerForQ12 = "Yes";
                                }
                                else
                                {
                                    AnswerForQ12 = "No";
                                }
                                objDentalHistoryAPI.Answer = AnswerForQ12;
                                for (int j = 0; j < dtMedicalhistory.Columns.Count; j++)
                                {

                                    string Question = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Columns[j].ColumnName), string.Empty);




                                    if (Question.Contains("Are you on a special diet?"))
                                    {
                                        Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                        if (Question != "Are you on a special diet?")
                                        {
                                            string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Are you on a special diet?|" + Question]), string.Empty);
                                            MultiPartArray objMultiPartArray = new MultiPartArray();

                                            objMultiPartArray.HistoryId = Id;
                                            objMultiPartArray.ItemNumber = Id;
                                            objMultiPartArray.ItemType = "Descriptive";
                                            objMultiPartArray.MultiPart = false;
                                            objMultiPartArray.Question = Question;
                                            objMultiPartArray.Answer = AnswerForQ1;
                                            objMultiPartArray.SingleLine = "No";
                                            objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                            Id++;
                                            AnswerForQ1 = "";
                                        }

                                    }
                                    Question = "";


                                }

                                dtMedicalhistory.Columns.Remove("Are you on a special diet?|Please explain");
                                dtMedicalhistory.AcceptChanges();

                            }

                            if (objDentalHistoryAPI.Question.Contains("Do you use tobacco?"))
                            {
                                int Id = 1;

                                objDentalHistoryAPI.MultiPart = true;
                                objDentalHistoryAPI.ItemType = "YesNo";
                                objDentalHistoryAPI.Question = "Do you use tobacco?";

                                string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Do you use tobacco?"]), string.Empty);

                                if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                {
                                    AnswerForQ12 = "Yes";
                                }
                                else
                                {
                                    AnswerForQ12 = "No";
                                }
                                objDentalHistoryAPI.Answer = AnswerForQ12;
                                for (int j = 0; j < dtMedicalhistory.Columns.Count; j++)
                                {

                                    string Question = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Columns[j].ColumnName), string.Empty);




                                    if (Question.Contains("Do you use tobacco?"))
                                    {
                                        Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                        if (Question != "Do you use tobacco?")
                                        {
                                            string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Do you use tobacco?|" + Question]), string.Empty);
                                            MultiPartArray objMultiPartArray = new MultiPartArray();

                                            objMultiPartArray.HistoryId = Id;
                                            objMultiPartArray.ItemNumber = Id;
                                            objMultiPartArray.ItemType = "Descriptive";
                                            objMultiPartArray.MultiPart = false;
                                            objMultiPartArray.Question = Question;
                                            objMultiPartArray.Answer = AnswerForQ1;
                                            objMultiPartArray.SingleLine = "No";
                                            objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                            Id++;
                                            AnswerForQ1 = "";
                                        }

                                    }
                                    Question = "";


                                }

                                dtMedicalhistory.Columns.Remove("Do you use tobacco?|How often");
                                dtMedicalhistory.AcceptChanges();

                            }

                            if (objDentalHistoryAPI.Question.Contains("Do you use controlled substances?"))
                            {
                                int Id = 1;

                                objDentalHistoryAPI.MultiPart = true;
                                objDentalHistoryAPI.ItemType = "YesNo";
                                objDentalHistoryAPI.Question = "Do you use controlled substances?";

                                string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Do you use controlled substances?"]), string.Empty);

                                if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                {
                                    AnswerForQ12 = "Yes";
                                }
                                else
                                {
                                    AnswerForQ12 = "No";
                                }
                                objDentalHistoryAPI.Answer = AnswerForQ12;
                                for (int j = 0; j < dtMedicalhistory.Columns.Count; j++)
                                {

                                    string Question = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Columns[j].ColumnName), string.Empty);




                                    if (Question.Contains("Do you use controlled substances?"))
                                    {
                                        Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                        if (Question != "Do you use controlled substances?")
                                        {
                                            string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Do you use controlled substances?|" + Question]), string.Empty);
                                            MultiPartArray objMultiPartArray = new MultiPartArray();

                                            objMultiPartArray.HistoryId = Id;
                                            objMultiPartArray.ItemNumber = Id;
                                            objMultiPartArray.ItemType = "Descriptive";
                                            objMultiPartArray.MultiPart = false;
                                            objMultiPartArray.Question = Question;
                                            objMultiPartArray.Answer = AnswerForQ1;
                                            objMultiPartArray.SingleLine = "No";
                                            objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                            Id++;
                                            AnswerForQ1 = "";
                                        }

                                    }
                                    Question = "";


                                }

                                dtMedicalhistory.Columns.Remove("Do you use controlled substances?|Please explain");
                                dtMedicalhistory.AcceptChanges();

                            }
                            if (objDentalHistoryAPI.Question.Contains("Are you pregnant/Trying to get pregnant(women)?"))
                            {
                                int Id = 1;

                                objDentalHistoryAPI.MultiPart = true;
                                objDentalHistoryAPI.ItemType = "YesNo";
                                objDentalHistoryAPI.Question = "Are you pregnant/Trying to get pregnant(women)?";

                                string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Are you pregnant/Trying to get pregnant(women)?"]), string.Empty);

                                if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                {
                                    AnswerForQ12 = "Yes";
                                }
                                else
                                {
                                    AnswerForQ12 = "No";
                                }
                                objDentalHistoryAPI.Answer = AnswerForQ12;
                                for (int j = 0; j < dtMedicalhistory.Columns.Count; j++)
                                {

                                    string Question = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Columns[j].ColumnName), string.Empty);




                                    if (Question.Contains("Are you pregnant/Trying to get pregnant(women)?"))
                                    {
                                        Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                        if (Question != "Are you pregnant/Trying to get pregnant(women)?")
                                        {
                                            string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0]["Are you pregnant/Trying to get pregnant(women)?|" + Question]), string.Empty);
                                            MultiPartArray objMultiPartArray = new MultiPartArray();

                                            objMultiPartArray.HistoryId = Id;
                                            objMultiPartArray.ItemNumber = Id;
                                            objMultiPartArray.ItemType = "Descriptive";
                                            objMultiPartArray.MultiPart = false;
                                            objMultiPartArray.Question = Question;
                                            objMultiPartArray.Answer = AnswerForQ1;
                                            objMultiPartArray.SingleLine = "No";
                                            objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                            Id++;
                                            AnswerForQ1 = "";
                                        }

                                    }
                                    Question = "";


                                }

                                dtMedicalhistory.Columns.Remove("Are you pregnant/Trying to get pregnant(women)?|Due date");
                                dtMedicalhistory.AcceptChanges();
                                objDentalHistoryAPI.SingleLine = string.Empty;
                            }



                            if (objDentalHistoryAPI.HistoryId == 15 || objDentalHistoryAPI.HistoryId == 16)
                            {
                                objDentalHistoryAPI.ItemType = "Descriptive";
                                objDentalHistoryAPI.Answer = objCommonDAL.CheckNull(Convert.ToString(dtMedicalhistory.Rows[0][i]), string.Empty);
                                objDentalHistoryAPI.SingleLine = "No";
                            }


















                            CheckType = "";
                            MedicalHistory.Add(objDentalHistoryAPI);
                        }

                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.StatusMessage = string.Empty;
                        objResponse.ResultDetail = new { MedicalHistory };


                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                        string jsonUpdate = JsonConvert.SerializeObject(MedicalHistory, Formatting.Indented);
                        List<DentalHistoryAPI> UpdateMedicalHistory = (List<DentalHistoryAPI>)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonUpdate, typeof(List<DentalHistoryAPI>));


                    }
                    else
                    {
                        DataTable dtGetMedicalhistory = new DataTable();
                        dtGetMedicalhistory = GetBlankTableForMedicalHistory();

                        if (dtGetMedicalhistory != null && dtGetMedicalhistory.Rows.Count > 0)
                        {
                            for (int i = 0; i < dtGetMedicalhistory.Columns.Count; i++)
                            {
                                DentalHistoryAPI objDentalHistoryAPI = new DentalHistoryAPI();
                                objDentalHistoryAPI.HistoryId = i + 1;
                                objDentalHistoryAPI.ItemNumber = i + 1;


                                string CheckType = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0][i]), string.Empty);

                                if (CheckType == "Y" || CheckType == "N")
                                {
                                    objDentalHistoryAPI.ItemType = "YesNo";

                                }
                                else
                                {
                                    objDentalHistoryAPI.ItemType = "Descriptive";
                                }



                                objDentalHistoryAPI.Question = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Columns[i].ColumnName), string.Empty);
                                string Answer = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0][i]), string.Empty);
                                if (Answer == "Y")
                                {
                                    objDentalHistoryAPI.Answer = "Yes";
                                }
                                else if (Answer == "N")
                                {
                                    objDentalHistoryAPI.Answer = "No";
                                }
                                else
                                {
                                    objDentalHistoryAPI.Answer = Answer;
                                }

                                objDentalHistoryAPI.SingleLine = string.Empty;
                                if (objDentalHistoryAPI.ItemType == "YesNo")
                                {
                                    if (!objDentalHistoryAPI.Question.Contains("|"))
                                    {
                                        objDentalHistoryAPI.MultiPart = false;
                                    }
                                    else
                                    {
                                        objDentalHistoryAPI.MultiPart = true;
                                    }

                                }
                                else
                                {
                                    objDentalHistoryAPI.MultiPart = false;
                                }


                                if (objDentalHistoryAPI.Question.Contains("Are you allergic to any of the following?"))
                                {
                                    int Id = 1;
                                    for (int j = 0; j < dtGetMedicalhistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Columns[j].ColumnName), string.Empty);
                                        string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0][j]), string.Empty);

                                        if (!string.IsNullOrEmpty(AnswerForQ1) && AnswerForQ1 == "Y")
                                        {
                                            AnswerForQ1 = "Yes";
                                        }
                                        else
                                        {
                                            AnswerForQ1 = "No";
                                        }

                                        if (Question.Contains("Are you allergic to any of the following?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                            MultiPartArray objMultiPartArray = new MultiPartArray();

                                            objMultiPartArray.HistoryId = Id;
                                            objMultiPartArray.ItemNumber = Id;
                                            objMultiPartArray.ItemType = "YesNo";
                                            objMultiPartArray.MultiPart = false;
                                            objMultiPartArray.Question = Question;
                                            objMultiPartArray.Answer = AnswerForQ1;
                                            objMultiPartArray.SingleLine = string.Empty;
                                            //"If other, please explain list"
                                            if (Question == "If other, please explain list")
                                            {

                                                objMultiPartArray.HistoryId = Id;
                                                objMultiPartArray.ItemNumber = Id;
                                                objMultiPartArray.ItemType = "Descriptive";
                                                objMultiPartArray.MultiPart = false;
                                                objMultiPartArray.Question = "If other, please explain list";
                                                objMultiPartArray.Answer = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Are you allergic to any of the following?|If other, please explain list"]), string.Empty);
                                                objMultiPartArray.SingleLine = "Yes";

                                            }


                                            objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                            Id++;
                                        }
                                        Question = "";
                                        AnswerForQ1 = "";

                                    }
                                    dtGetMedicalhistory.Columns.Remove("Are you allergic to any of the following?|Penicillin");
                                    dtGetMedicalhistory.Columns.Remove("Are you allergic to any of the following?|Codeine");
                                    dtGetMedicalhistory.Columns.Remove("Are you allergic to any of the following?|Local Anesthetics");

                                    dtGetMedicalhistory.Columns.Remove("Are you allergic to any of the following?|Acrylic");
                                    dtGetMedicalhistory.Columns.Remove("Are you allergic to any of the following?|Metal");
                                    dtGetMedicalhistory.Columns.Remove("Are you allergic to any of the following?|Latex");
                                    dtGetMedicalhistory.Columns.Remove("Are you allergic to any of the following?|Sulfa Drugs");

                                    dtGetMedicalhistory.Columns.Remove("Are you allergic to any of the following?|If other, please explain list");

                                    dtGetMedicalhistory.AcceptChanges();

                                    objDentalHistoryAPI.Question = "Are you allergic to any of the following?";
                                    objDentalHistoryAPI.Answer = string.Empty;
                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "YesNo";
                                    objDentalHistoryAPI.SingleLine = string.Empty;
                                }

                                if (objDentalHistoryAPI.Question.Contains("Do you have, or have you had, any of the following?"))
                                {
                                    int Id = 1;
                                    for (int j = 0; j < dtGetMedicalhistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Columns[j].ColumnName), string.Empty);
                                        string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0][j]), string.Empty);

                                        if (!string.IsNullOrEmpty(AnswerForQ1) && AnswerForQ1 == "Y")
                                        {
                                            AnswerForQ1 = "Yes";
                                        }
                                        else
                                        {
                                            AnswerForQ1 = "No";
                                        }

                                        if (Question.Contains("Do you have, or have you had, any of the following?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                            MultiPartArray objMultiPartArray = new MultiPartArray();

                                            objMultiPartArray.HistoryId = Id;
                                            objMultiPartArray.ItemNumber = Id;
                                            objMultiPartArray.ItemType = "YesNo";
                                            objMultiPartArray.MultiPart = false;
                                            objMultiPartArray.Question = Question;
                                            objMultiPartArray.Answer = AnswerForQ1;
                                            objMultiPartArray.SingleLine = string.Empty;
                                            objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                            Id++;
                                        }
                                        Question = "";
                                        AnswerForQ1 = "";

                                    }


                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Alzheimer’s Disease");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Anaphylaxis");

                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Anemia");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Angina");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Arthritis/Gout");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Artificial Heart Valve");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Artificial Joint");


                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Asthma");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Blood Disease");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Blood Transfusion");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Breathing Problem");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Bruise Easily");

                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Cancer");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Chemotherapy");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Chest Pains");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Cold Sores/Fever Blisters");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Congenital Heart Disorder");


                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Convulsions");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Cortisone Medicine");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Diabetes");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Drug Addiction");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Easily Winded");

                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Emphysema");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Epilepsy or Seizures");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Excessive Bleeding");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Excessive Thirst");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Fainting Spells/Dizziness");


                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Frequent Cough");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Frequent Diarrhea");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Frequent Headaches");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Genital Herpes");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Glaucoma");

                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Hay Fever");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Heart Attack/Failure");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Heart Murmur");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Heart Pacemaker");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Heart Trouble/Disease");


                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Hemophilia");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Hepatitis A");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Hepatitis B or C");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Herpes");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|High Blood Pressure");

                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|High Cholesterol");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Hives or Rash");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Hypoglycemia");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Irregular Heartbeat");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Kidney Problems");


                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Leukemia");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Liver Disease");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Low Blood Pressure");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Lung Disease");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Mitral Valve Prolapse");

                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Osteoporosis");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Pain in Jaw Joints");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Parathyroid Disease");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Psychiatric Care");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Radiation Treatments");


                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Recent Weight Loss");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Renal Dialysis");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Rheumatic Fever");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Rheumatism");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Scarlet Fever");

                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Shingles");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Sickle Cell Disease");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Sinus Trouble");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Spina Bifida");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Stomach/Intestinal Disease");

                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Stroke");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Swelling of Limbs");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Thyroid Disease");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Tonsillitis");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Tuberculosis");


                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Tumors or Growths");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Ulcers");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Venereal Disease");
                                    dtGetMedicalhistory.Columns.Remove("Do you have, or have you had, any of the following?|Yellow Jaundice");




                                    dtGetMedicalhistory.AcceptChanges();

                                    objDentalHistoryAPI.Question = "Do you have, or have you had, any of the following?";
                                    objDentalHistoryAPI.Answer = string.Empty;
                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "YesNo";
                                    objDentalHistoryAPI.SingleLine = string.Empty;
                                }


                                if (objDentalHistoryAPI.Question.Contains("Are you under a physicians care right now?"))
                                {
                                    int Id = 1;

                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "YesNo";
                                    objDentalHistoryAPI.Question = "Are you under a physicians care right now?";

                                    string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Are you under a physicians care right now?"]), string.Empty);

                                    if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                    {
                                        AnswerForQ12 = "Yes";
                                    }
                                    else
                                    {
                                        AnswerForQ12 = "No";
                                    }
                                    objDentalHistoryAPI.Answer = AnswerForQ12;
                                    for (int j = 0; j < dtGetMedicalhistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Columns[j].ColumnName), string.Empty);




                                        if (Question.Contains("Are you under a physicians care right now?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                            if (Question != "Are you under a physicians care right now?")
                                            {
                                                string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Are you under a physicians care right now?|" + Question]), string.Empty);
                                                MultiPartArray objMultiPartArray = new MultiPartArray();

                                                objMultiPartArray.HistoryId = Id;
                                                objMultiPartArray.ItemNumber = Id;
                                                objMultiPartArray.ItemType = "Descriptive";
                                                objMultiPartArray.MultiPart = false;
                                                objMultiPartArray.Question = Question;
                                                objMultiPartArray.Answer = AnswerForQ1;
                                                objMultiPartArray.SingleLine = "No";
                                                objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                                Id++;
                                                AnswerForQ1 = "";
                                            }

                                        }
                                        Question = "";


                                    }

                                    dtGetMedicalhistory.Columns.Remove("Are you under a physicians care right now?|Please explain");
                                    dtGetMedicalhistory.AcceptChanges();

                                }


                                if (objDentalHistoryAPI.Question.Contains("Have you ever been hospitalized or had a major operation?"))
                                {
                                    int Id = 1;

                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "YesNo";
                                    objDentalHistoryAPI.Question = "Have you ever been hospitalized or had a major operation?";

                                    string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Have you ever been hospitalized or had a major operation?"]), string.Empty);

                                    if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                    {
                                        AnswerForQ12 = "Yes";
                                    }
                                    else
                                    {
                                        AnswerForQ12 = "No";
                                    }
                                    objDentalHistoryAPI.Answer = AnswerForQ12;
                                    for (int j = 0; j < dtGetMedicalhistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Columns[j].ColumnName), string.Empty);




                                        if (Question.Contains("Have you ever been hospitalized or had a major operation?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                            if (Question != "Have you ever been hospitalized or had a major operation?")
                                            {
                                                string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Have you ever been hospitalized or had a major operation?|" + Question]), string.Empty);
                                                MultiPartArray objMultiPartArray = new MultiPartArray();

                                                objMultiPartArray.HistoryId = Id;
                                                objMultiPartArray.ItemNumber = Id;
                                                objMultiPartArray.ItemType = "Descriptive";
                                                objMultiPartArray.MultiPart = false;
                                                objMultiPartArray.Question = Question;
                                                objMultiPartArray.Answer = AnswerForQ1;
                                                objMultiPartArray.SingleLine = "No";
                                                objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                                Id++;
                                                AnswerForQ1 = "";
                                            }

                                        }
                                        Question = "";


                                    }

                                    dtGetMedicalhistory.Columns.Remove("Have you ever been hospitalized or had a major operation?|Please explain");
                                    dtGetMedicalhistory.AcceptChanges();

                                }

                                if (objDentalHistoryAPI.Question.Contains("Have you ever had a serious head or neck injury?"))
                                {
                                    int Id = 1;

                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "YesNo";
                                    objDentalHistoryAPI.Question = "Have you ever had a serious head or neck injury?";

                                    string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Have you ever had a serious head or neck injury?"]), string.Empty);

                                    if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                    {
                                        AnswerForQ12 = "Yes";
                                    }
                                    else
                                    {
                                        AnswerForQ12 = "No";
                                    }
                                    objDentalHistoryAPI.Answer = AnswerForQ12;
                                    for (int j = 0; j < dtGetMedicalhistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Columns[j].ColumnName), string.Empty);




                                        if (Question.Contains("Have you ever had a serious head or neck injury?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                            if (Question != "Have you ever had a serious head or neck injury?")
                                            {
                                                string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Have you ever had a serious head or neck injury?|" + Question]), string.Empty);
                                                MultiPartArray objMultiPartArray = new MultiPartArray();

                                                objMultiPartArray.HistoryId = Id;
                                                objMultiPartArray.ItemNumber = Id;
                                                objMultiPartArray.ItemType = "Descriptive";
                                                objMultiPartArray.MultiPart = false;
                                                objMultiPartArray.Question = Question;
                                                objMultiPartArray.Answer = AnswerForQ1;
                                                objMultiPartArray.SingleLine = "No";
                                                objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                                Id++;
                                                AnswerForQ1 = "";
                                            }

                                        }
                                        Question = "";


                                    }

                                    dtGetMedicalhistory.Columns.Remove("Have you ever had a serious head or neck injury?|Please explain");
                                    dtGetMedicalhistory.AcceptChanges();

                                }

                                if (objDentalHistoryAPI.Question.Contains("Are taking any medications, pills, or drugs?"))
                                {
                                    int Id = 1;

                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "YesNo";
                                    objDentalHistoryAPI.Question = "Are taking any medications, pills, or drugs?";

                                    string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Are taking any medications, pills, or drugs?"]), string.Empty);

                                    if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                    {
                                        AnswerForQ12 = "Yes";
                                    }
                                    else
                                    {
                                        AnswerForQ12 = "No";
                                    }
                                    objDentalHistoryAPI.Answer = AnswerForQ12;
                                    for (int j = 0; j < dtGetMedicalhistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Columns[j].ColumnName), string.Empty);




                                        if (Question.Contains("Are taking any medications, pills, or drugs?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                            if (Question != "Are taking any medications, pills, or drugs?")
                                            {
                                                string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Are taking any medications, pills, or drugs?|" + Question]), string.Empty);
                                                MultiPartArray objMultiPartArray = new MultiPartArray();

                                                objMultiPartArray.HistoryId = Id;
                                                objMultiPartArray.ItemNumber = Id;
                                                objMultiPartArray.ItemType = "Descriptive";
                                                objMultiPartArray.MultiPart = false;
                                                objMultiPartArray.Question = Question;
                                                objMultiPartArray.Answer = AnswerForQ1;
                                                objMultiPartArray.SingleLine = "No";
                                                objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                                Id++;
                                                AnswerForQ1 = "";
                                            }

                                        }
                                        Question = "";


                                    }

                                    dtGetMedicalhistory.Columns.Remove("Are taking any medications, pills, or drugs?|Please list");
                                    dtGetMedicalhistory.AcceptChanges();

                                }

                                if (objDentalHistoryAPI.Question.Contains("Do you take, or have you taken, Phen-Fen or Redux?"))
                                {
                                    int Id = 1;

                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "YesNo";
                                    objDentalHistoryAPI.Question = "Do you take, or have you taken, Phen-Fen or Redux?";

                                    string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Do you take, or have you taken, Phen-Fen or Redux?"]), string.Empty);

                                    if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                    {
                                        AnswerForQ12 = "Yes";
                                    }
                                    else
                                    {
                                        AnswerForQ12 = "No";
                                    }
                                    objDentalHistoryAPI.Answer = AnswerForQ12;
                                    for (int j = 0; j < dtGetMedicalhistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Columns[j].ColumnName), string.Empty);




                                        if (Question.Contains("Do you take, or have you taken, Phen-Fen or Redux?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                            if (Question != "Do you take, or have you taken, Phen-Fen or Redux?")
                                            {
                                                string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Do you take, or have you taken, Phen-Fen or Redux?|" + Question]), string.Empty);
                                                MultiPartArray objMultiPartArray = new MultiPartArray();

                                                objMultiPartArray.HistoryId = Id;
                                                objMultiPartArray.ItemNumber = Id;
                                                objMultiPartArray.ItemType = "Descriptive";
                                                objMultiPartArray.MultiPart = false;
                                                objMultiPartArray.Question = Question;
                                                objMultiPartArray.Answer = AnswerForQ1;
                                                objMultiPartArray.SingleLine = "No";
                                                objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                                Id++;
                                                AnswerForQ1 = "";
                                            }

                                        }
                                        Question = "";


                                    }

                                    dtGetMedicalhistory.Columns.Remove("Do you take, or have you taken, Phen-Fen or Redux?|Please explain");
                                    dtGetMedicalhistory.AcceptChanges();

                                }

                                if (objDentalHistoryAPI.Question.Contains("Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?"))
                                {
                                    int Id = 1;

                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "YesNo";
                                    objDentalHistoryAPI.Question = "Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?";

                                    string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?"]), string.Empty);

                                    if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                    {
                                        AnswerForQ12 = "Yes";
                                    }
                                    else
                                    {
                                        AnswerForQ12 = "No";
                                    }
                                    objDentalHistoryAPI.Answer = AnswerForQ12;
                                    for (int j = 0; j < dtGetMedicalhistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Columns[j].ColumnName), string.Empty);




                                        if (Question.Contains("Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                            if (Question != "Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?")
                                            {
                                                string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?|" + Question]), string.Empty);
                                                MultiPartArray objMultiPartArray = new MultiPartArray();

                                                objMultiPartArray.HistoryId = Id;
                                                objMultiPartArray.ItemNumber = Id;
                                                objMultiPartArray.ItemType = "Descriptive";
                                                objMultiPartArray.MultiPart = false;
                                                objMultiPartArray.Question = Question;
                                                objMultiPartArray.Answer = AnswerForQ1;
                                                objMultiPartArray.SingleLine = "No";
                                                objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                                Id++;
                                                AnswerForQ1 = "";
                                            }

                                        }
                                        Question = "";


                                    }

                                    dtGetMedicalhistory.Columns.Remove("Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?|Please explain");
                                    dtGetMedicalhistory.AcceptChanges();

                                }

                                if (objDentalHistoryAPI.Question.Contains("Are you on a special diet?"))
                                {
                                    int Id = 1;

                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "YesNo";
                                    objDentalHistoryAPI.Question = "Are you on a special diet?";

                                    string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Are you on a special diet?"]), string.Empty);

                                    if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                    {
                                        AnswerForQ12 = "Yes";
                                    }
                                    else
                                    {
                                        AnswerForQ12 = "No";
                                    }
                                    objDentalHistoryAPI.Answer = AnswerForQ12;
                                    for (int j = 0; j < dtGetMedicalhistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Columns[j].ColumnName), string.Empty);




                                        if (Question.Contains("Are you on a special diet?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                            if (Question != "Are you on a special diet?")
                                            {
                                                string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Are you on a special diet?|" + Question]), string.Empty);
                                                MultiPartArray objMultiPartArray = new MultiPartArray();

                                                objMultiPartArray.HistoryId = Id;
                                                objMultiPartArray.ItemNumber = Id;
                                                objMultiPartArray.ItemType = "Descriptive";
                                                objMultiPartArray.MultiPart = false;
                                                objMultiPartArray.Question = Question;
                                                objMultiPartArray.Answer = AnswerForQ1;
                                                objMultiPartArray.SingleLine = "No";
                                                objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                                Id++;
                                                AnswerForQ1 = "";
                                            }

                                        }
                                        Question = "";


                                    }

                                    dtGetMedicalhistory.Columns.Remove("Are you on a special diet?|Please explain");
                                    dtGetMedicalhistory.AcceptChanges();

                                }

                                if (objDentalHistoryAPI.Question.Contains("Do you use tobacco?"))
                                {
                                    int Id = 1;

                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "YesNo";
                                    objDentalHistoryAPI.Question = "Do you use tobacco?";

                                    string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Do you use tobacco?"]), string.Empty);

                                    if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                    {
                                        AnswerForQ12 = "Yes";
                                    }
                                    else
                                    {
                                        AnswerForQ12 = "No";
                                    }
                                    objDentalHistoryAPI.Answer = AnswerForQ12;
                                    for (int j = 0; j < dtGetMedicalhistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Columns[j].ColumnName), string.Empty);




                                        if (Question.Contains("Do you use tobacco?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                            if (Question != "Do you use tobacco?")
                                            {
                                                string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Do you use tobacco?|" + Question]), string.Empty);
                                                MultiPartArray objMultiPartArray = new MultiPartArray();

                                                objMultiPartArray.HistoryId = Id;
                                                objMultiPartArray.ItemNumber = Id;
                                                objMultiPartArray.ItemType = "Descriptive";
                                                objMultiPartArray.MultiPart = false;
                                                objMultiPartArray.Question = Question;
                                                objMultiPartArray.Answer = AnswerForQ1;
                                                objMultiPartArray.SingleLine = "No";
                                                objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                                Id++;
                                                AnswerForQ1 = "";
                                            }

                                        }
                                        Question = "";


                                    }

                                    dtGetMedicalhistory.Columns.Remove("Do you use tobacco?|How often");
                                    dtGetMedicalhistory.AcceptChanges();

                                }

                                if (objDentalHistoryAPI.Question.Contains("Do you use controlled substances?"))
                                {
                                    int Id = 1;

                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "YesNo";
                                    objDentalHistoryAPI.Question = "Do you use controlled substances?";

                                    string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Do you use controlled substances?"]), string.Empty);

                                    if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                    {
                                        AnswerForQ12 = "Yes";
                                    }
                                    else
                                    {
                                        AnswerForQ12 = "No";
                                    }
                                    objDentalHistoryAPI.Answer = AnswerForQ12;
                                    for (int j = 0; j < dtGetMedicalhistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Columns[j].ColumnName), string.Empty);




                                        if (Question.Contains("Do you use controlled substances?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                            if (Question != "Do you use controlled substances?")
                                            {
                                                string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Do you use controlled substances?|" + Question]), string.Empty);
                                                MultiPartArray objMultiPartArray = new MultiPartArray();

                                                objMultiPartArray.HistoryId = Id;
                                                objMultiPartArray.ItemNumber = Id;
                                                objMultiPartArray.ItemType = "Descriptive";
                                                objMultiPartArray.MultiPart = false;
                                                objMultiPartArray.Question = Question;
                                                objMultiPartArray.Answer = AnswerForQ1;
                                                objMultiPartArray.SingleLine = "No";
                                                objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                                Id++;
                                                AnswerForQ1 = "";
                                            }

                                        }
                                        Question = "";


                                    }

                                    dtGetMedicalhistory.Columns.Remove("Do you use controlled substances?|Please explain");
                                    dtGetMedicalhistory.AcceptChanges();

                                }

                                if (objDentalHistoryAPI.Question.Contains("Are you pregnant/Trying to get pregnant(women)?"))
                                {
                                    int Id = 1;

                                    objDentalHistoryAPI.MultiPart = true;
                                    objDentalHistoryAPI.ItemType = "YesNo";
                                    objDentalHistoryAPI.Question = "Are you pregnant/Trying to get pregnant(women)?";

                                    string AnswerForQ12 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Are you pregnant/Trying to get pregnant(women)?"]), string.Empty);

                                    if (!string.IsNullOrEmpty(AnswerForQ12) && AnswerForQ12 == "Y")
                                    {
                                        AnswerForQ12 = "Yes";
                                    }
                                    else
                                    {
                                        AnswerForQ12 = "No";
                                    }
                                    objDentalHistoryAPI.Answer = AnswerForQ12;
                                    for (int j = 0; j < dtGetMedicalhistory.Columns.Count; j++)
                                    {

                                        string Question = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Columns[j].ColumnName), string.Empty);




                                        if (Question.Contains("Are you pregnant/Trying to get pregnant(women)?"))
                                        {
                                            Question = Question.Substring(Question.LastIndexOf("|") + 1);


                                            if (Question != "Are you pregnant/Trying to get pregnant(women)?")
                                            {
                                                string AnswerForQ1 = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0]["Are you pregnant/Trying to get pregnant(women)?|" + Question]), string.Empty);
                                                MultiPartArray objMultiPartArray = new MultiPartArray();

                                                objMultiPartArray.HistoryId = Id;
                                                objMultiPartArray.ItemNumber = Id;
                                                objMultiPartArray.ItemType = "Descriptive";
                                                objMultiPartArray.MultiPart = false;
                                                objMultiPartArray.Question = Question;
                                                objMultiPartArray.Answer = AnswerForQ1;
                                                objMultiPartArray.SingleLine = "No";
                                                objDentalHistoryAPI.MultiPartArray.Add(objMultiPartArray);
                                                Id++;
                                                AnswerForQ1 = "";
                                            }

                                        }
                                        Question = "";


                                    }

                                    dtGetMedicalhistory.Columns.Remove("Are you pregnant/Trying to get pregnant(women)?|Due date");
                                    dtGetMedicalhistory.AcceptChanges();
                                    objDentalHistoryAPI.SingleLine = string.Empty;
                                }



                                if (objDentalHistoryAPI.HistoryId == 15 || objDentalHistoryAPI.HistoryId == 16)
                                {
                                    objDentalHistoryAPI.ItemType = "Descriptive";
                                    objDentalHistoryAPI.Answer = objCommonDAL.CheckNull(Convert.ToString(dtGetMedicalhistory.Rows[0][i]), string.Empty);
                                    objDentalHistoryAPI.SingleLine = "No";
                                }



                                if (objDentalHistoryAPI.HistoryId == 11 || objDentalHistoryAPI.HistoryId == 12)
                                {
                                    objDentalHistoryAPI.ItemType = "YesNo";
                                    objDentalHistoryAPI.Answer = "No";
                                    objDentalHistoryAPI.SingleLine = string.Empty;
                                }














                                CheckType = "";
                                MedicalHistory.Add(objDentalHistoryAPI);
                            }


                            objResponse.MethodName = MethodName;
                            objResponse.MagicNumber = MagicNumber;
                            objResponse.ResultCode = 200;
                            objResponse.IsSuccess = false;
                            objResponse.StatusMessage = string.Empty;
                            objResponse.ResultDetail = new { MedicalHistory };

                            json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                        }
                    }



                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }



        }

        [HttpPost]
        public string UpdateDentalHistory(string SessionCode, int MagicNumber, [ModelBinder(typeof(DefaultModelBinder))] List<DentalHistoryAPIUpdate> Data)
        {

            string json = "";
            string MethodName = "UpdateDentalHistory";
            DataTable dtDentalHistoryData = new DataTable();

            try
            {

                if (IsSessionExpired(SessionCode))
                {
                    dtDentalHistoryData = objCommonDAL.GetDentalHistory(Convert.ToInt32(Session["PatientId"]), "GetDentalHistory");


                    List<DentalHistoryAPIUpdate> DentalHistory = new List<DentalHistoryAPIUpdate>();
                    DentalHistory = Data;



                    if (dtDentalHistoryData == null && dtDentalHistoryData.Rows.Count == 0)
                    {
                        DataRow dr = dtDentalHistoryData.NewRow();
                        dtDentalHistoryData.Rows.Add(dr);
                    }



                    string txtQue1 = string.Empty;
                    string txtQue2 = string.Empty;
                    string txtQue3 = string.Empty;
                    string txtQue4 = string.Empty;
                    string rdQue8 = string.Empty;
                    string txtQue9a = string.Empty;
                    string rdQue9 = string.Empty;
                    string rdQue10 = string.Empty;
                    string rdQue27 = string.Empty;
                    string rdQue30 = string.Empty;


                    string txtQue5 = string.Empty; string txtQue5a = string.Empty; string txtQue5b = string.Empty; string txtQue5c = string.Empty; //Previous Dentists Name 1
                    string txtQue6 = string.Empty;//When was the last time you had your teeth cleaned? 2 

                    string rdQue7a = string.Empty; //Do you make regular visits to the dentist? 3
                    string txtQue7 = string.Empty;

                    string txtQue12 = string.Empty; //Please explain any problems or complications with previous dental treatment? 4 
                    string rdoQue11aFixedbridge = string.Empty; string rdoQue11bRemoveablebridge = string.Empty; string rdoQue11cDenture = string.Empty; string rdQue11dImplant = string.Empty;//Have any of your teeth been replaced with the following? 5 




                    string rdQue15 = string.Empty; //Do you clench or grind your teeth? 6
                    string rdQue16 = string.Empty; //Does your jaw click or pop? 7 
                    string rdQue17 = string.Empty;//Are you experiencing any pain or soreness in the muscles of your face? 8
                    string rdQue18 = string.Empty; //Do you have frequent headaches, neckaches or shoulder aches? 9
                    string rdQue19 = string.Empty;//Does food get caught in your teeth? 10

                    StringBuilder chkQue20 = new StringBuilder(); //Are any of your teeth sensitive to|Hot|Cold|Sweets|Pressure 11

                    string chkQue20_1 = string.Empty; string chkQue20_2 = string.Empty; string chkQue20_3 = string.Empty; string chkQue20_4 = string.Empty;



                    string rdQue21 = string.Empty; string txtQue21a = string.Empty; //Do your gums bleed or hurt? 12


                    string txtQue22 = string.Empty; //How often do you brush your teeth? 13
                    string txtQue23 = string.Empty;//How often do you use dental floss? 14
                    string rdQue24 = string.Empty; // Are any of your teeth loose, tipped, shifted or chipped? 15




                    string rdQue28 = string.Empty; string txtQue28a = string.Empty; string txtQue28b = string.Empty; string txtQue28c = string.Empty; //Do you feel your breath is often offensive? 16

                    string txtQue29 = string.Empty;//Explain any orthodonic work you have had done? 17

                    string txtQue29a = string.Empty; // Explain any dental work you have had done (Perio, Oral Surgery, etc.) 18

                    string txtQue26 = string.Empty; // How do you feel about your teeth in general? 19

                    string txtComments = string.Empty; // Additional Information 20
                    string txtDigiSign = string.Empty;



                    for (int i = 0; i < DentalHistory.Count; i++)
                    {
                        int HistoryID = Convert.ToInt32(DentalHistory[i].HistoryId);


                        if (HistoryID == 1)
                        {

                            txtQue5 = objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].Answer), string.Empty);

                            if (DentalHistory[i].MultiPartArray.Count > 2)
                            {
                                for (int j = 0; j < DentalHistory[i].MultiPartArray.Count; j++)
                                {
                                    int SubQuestionHistoryId = Convert.ToInt32(DentalHistory[i].MultiPartArray[j].HistoryId);

                                    if (SubQuestionHistoryId == 1)
                                    {
                                        txtQue5a = objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].MultiPartArray[j].Answer), string.Empty);
                                    }
                                    if (SubQuestionHistoryId == 2)
                                    {
                                        txtQue5b = objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].MultiPartArray[j].Answer), string.Empty);
                                    }
                                    if (SubQuestionHistoryId == 3)
                                    {
                                        txtQue5c = objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].MultiPartArray[j].Answer), string.Empty);
                                    }
                                }
                            }
                        }

                        if (HistoryID == 2)
                        {
                            txtQue6 = objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].Answer), string.Empty);
                        }


                        if (HistoryID == 3)
                        {
                            rdQue7a = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].Answer), string.Empty));
                            if (DentalHistory[i].MultiPartArray.Count > 0)
                            {
                                txtQue7 = objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].MultiPartArray[0].Answer), string.Empty);
                            }
                        }

                        if (HistoryID == 4)
                        {
                            txtQue12 = objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].Answer), string.Empty);
                        }

                        if (HistoryID == 5)
                        {
                            if (DentalHistory[i].MultiPartArray.Count > 3)
                            {
                                for (int j = 0; j < DentalHistory[i].MultiPartArray.Count; j++)
                                {
                                    int SubQuestionHistoryId = Convert.ToInt32(DentalHistory[i].MultiPartArray[j].HistoryId);
                                    if (SubQuestionHistoryId == 1)
                                    {
                                        rdoQue11aFixedbridge = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 2)
                                    {
                                        rdoQue11bRemoveablebridge = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 3)
                                    {
                                        rdoQue11cDenture = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 4)
                                    {
                                        rdQue11dImplant = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }
                                }
                            }
                        }

                        if (HistoryID == 6)
                        {
                            rdQue15 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].Answer), string.Empty));


                        }

                        if (HistoryID == 7)
                        {
                            rdQue16 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].Answer), string.Empty));

                        }

                        if (HistoryID == 8)
                        {
                            rdQue17 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].Answer), string.Empty));

                        }

                        if (HistoryID == 9)
                        {
                            rdQue18 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].Answer), string.Empty));

                        }

                        if (HistoryID == 10)
                        {
                            rdQue19 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].Answer), string.Empty));

                        }

                        if (HistoryID == 11)
                        {
                            if (DentalHistory[i].MultiPartArray.Count > 3)
                            {

                                for (int j = 0; j < DentalHistory[i].MultiPartArray.Count; j++)
                                {
                                    int SubQuestionHistoryId = Convert.ToInt32(DentalHistory[i].MultiPartArray[j].HistoryId);
                                    if (SubQuestionHistoryId == 1)
                                    {
                                        chkQue20_1 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 2)
                                    {
                                        chkQue20_2 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 3)
                                    {
                                        chkQue20_3 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 4)
                                    {
                                        chkQue20_4 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }
                                }


                                if (chkQue20_1 == "Y")
                                {
                                    if (!String.IsNullOrEmpty(chkQue20.ToString()))
                                    {
                                        chkQue20.Append("," + "1");
                                    }
                                    else
                                    {
                                        chkQue20.Append("1");
                                    }
                                }
                                if (chkQue20_2 == "Y")
                                {
                                    if (!String.IsNullOrEmpty(chkQue20.ToString()))
                                    {
                                        chkQue20.Append("," + "2");
                                    }
                                    else
                                    {
                                        chkQue20.Append("2");
                                    }
                                }
                                if (chkQue20_3 == "Y")
                                {
                                    if (!String.IsNullOrEmpty(chkQue20.ToString()))
                                    {
                                        chkQue20.Append("," + "3");
                                    }
                                    else
                                    {
                                        chkQue20.Append("3");
                                    }
                                }
                                if (chkQue20_4 == "Y")
                                {
                                    if (!String.IsNullOrEmpty(chkQue20.ToString()))
                                    {
                                        chkQue20.Append("," + "4");
                                    }
                                    else
                                    {
                                        chkQue20.Append("4");
                                    }
                                }
                            }
                        }

                        if (HistoryID == 12)
                        {

                            rdQue21 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].Answer), string.Empty));
                            if (DentalHistory[i].MultiPartArray.Count > 0)
                            {
                                txtQue21a = objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].MultiPartArray[0].Answer), string.Empty);
                            }
                        }

                        if (HistoryID == 13)
                        {
                            txtQue22 = objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].Answer), string.Empty);
                        }

                        if (HistoryID == 14)
                        {
                            txtQue23 = objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].Answer), string.Empty);
                        }

                        if (HistoryID == 15)
                        {
                            rdQue24 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].Answer), string.Empty));

                        }

                        if (HistoryID == 16)
                        {
                            rdQue28 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].Answer), string.Empty));
                            if (DentalHistory[i].MultiPartArray.Count > 0)
                            {
                                txtQue28c = objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].MultiPartArray[0].Answer), string.Empty);
                            }
                        }

                        if (HistoryID == 17)
                        {
                            txtQue29 = objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].Answer), string.Empty);
                        }

                        if (HistoryID == 18)
                        {
                            txtQue29a = objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].Answer), string.Empty);
                        }

                        if (HistoryID == 19)
                        {
                            txtQue26 = objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].Answer), string.Empty);
                        }

                        if (HistoryID == 20)
                        {
                            txtComments = objCommonDAL.CheckNull(Convert.ToString(DentalHistory[i].Answer), string.Empty);
                        }



                    }
                    DataTable tblDentalHistory = new DataTable("DentalHistory");
                    tblDentalHistory.Columns.Add("txtQue1");
                    tblDentalHistory.Columns.Add("txtQue2");
                    tblDentalHistory.Columns.Add("txtQue3");
                    tblDentalHistory.Columns.Add("txtQue4");
                    tblDentalHistory.Columns.Add("txtQue5");
                    tblDentalHistory.Columns.Add("txtQue5a");
                    tblDentalHistory.Columns.Add("txtQue5b");
                    tblDentalHistory.Columns.Add("txtQue5c");
                    tblDentalHistory.Columns.Add("txtQue6");
                    tblDentalHistory.Columns.Add("txtQue7");
                    tblDentalHistory.Columns.Add("rdQue7a");
                    tblDentalHistory.Columns.Add("rdQue8");
                    tblDentalHistory.Columns.Add("rdQue9");
                    tblDentalHistory.Columns.Add("txtQue9a");
                    tblDentalHistory.Columns.Add("rdQue10");
                    tblDentalHistory.Columns.Add("rdoQue11aFixedbridge");
                    tblDentalHistory.Columns.Add("rdoQue11bRemoveablebridge");
                    tblDentalHistory.Columns.Add("rdoQue11cDenture");
                    tblDentalHistory.Columns.Add("rdQue11dImplant");
                    tblDentalHistory.Columns.Add("txtQue12");
                    tblDentalHistory.Columns.Add("rdQue15");
                    tblDentalHistory.Columns.Add("rdQue16");
                    tblDentalHistory.Columns.Add("rdQue17");
                    tblDentalHistory.Columns.Add("rdQue18");
                    tblDentalHistory.Columns.Add("rdQue19");
                    tblDentalHistory.Columns.Add("chkQue20");

                    tblDentalHistory.Columns.Add("rdQue21");
                    tblDentalHistory.Columns.Add("txtQue21a");
                    tblDentalHistory.Columns.Add("txtQue22");


                    tblDentalHistory.Columns.Add("txtQue23");
                    tblDentalHistory.Columns.Add("rdQue24");

                    tblDentalHistory.Columns.Add("txtQue26");
                    tblDentalHistory.Columns.Add("rdQue27");
                    tblDentalHistory.Columns.Add("rdQue28");
                    tblDentalHistory.Columns.Add("txtQue28a");
                    tblDentalHistory.Columns.Add("txtQue28b");
                    tblDentalHistory.Columns.Add("txtQue28c");
                    tblDentalHistory.Columns.Add("txtQue29");
                    tblDentalHistory.Columns.Add("txtQue29a");
                    tblDentalHistory.Columns.Add("rdQue30");

                    tblDentalHistory.Columns.Add("txtComments");
                    tblDentalHistory.Columns.Add("txtDigiSign");
                    tblDentalHistory.Columns.Add("CreatedDate");
                    tblDentalHistory.Columns.Add("ModifiedDate");


                    tblDentalHistory.Rows.Add(txtQue1, txtQue2, txtQue3, txtQue4, txtQue5, txtQue5a, txtQue5b, txtQue5c,
                        txtQue6, txtQue7, rdQue7a, rdQue8, rdQue9, txtQue9a, rdQue10,
                        rdoQue11aFixedbridge, rdoQue11bRemoveablebridge, rdoQue11cDenture,
                        rdQue11dImplant, txtQue12, rdQue15, rdQue16, rdQue17, rdQue18,
                        rdQue19, chkQue20.ToString(), rdQue21, txtQue21a, txtQue22,
                        txtQue23, rdQue24, txtQue26, rdQue27, rdQue28,
                        txtQue28a, txtQue28b, txtQue28c, txtQue29, txtQue29a, rdQue30, txtComments, txtDigiSign, System.DateTime.Now, System.DateTime.Now);


                    //Join Two Table
                    DataSet dsDentalHistory = new DataSet("DentalHistory");
                    dsDentalHistory.Tables.Add(tblDentalHistory);

                    DataSet ds = dsDentalHistory.Copy();
                    dsDentalHistory.Clear();

                    string UpdatedDentalHistory = ds.GetXml();

                    bool status = objCommonDAL.UpdateDentalHistory(Convert.ToInt32(Session["PatientId"]), "UpdateDentalHistory", UpdatedDentalHistory);


                    if (status)
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.StatusMessage = "Form updated successfully";
                        objResponse.ResultDetail = string.Empty;


                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {

                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "Failed to update form.";
                        objResponse.ResultDetail = string.Empty;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                    }

                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;
                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Data=##.Server error occured - Please contact administrator." + ex.Message + "-" + ex.StackTrace;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        [HttpPost]
        public string UpdateInsuranceCoverage(string SessionCode, int MagicNumber, string ResponsiblePartysFirstName, string ResponsiblePartysLastName, string Relationship, string Address, string City, string State, string Zipcode, string PhoneNumber, string PrimaryInsuranceCompany, string PrimaryNameOfInsured, string PrimaryDateOfBirth, string PrimaryMemberID, string PrimaryGroupNumber, string SecondaryInsuranceCompany, string SecondaryNameOfInsured, string SecondaryDateOfBirth, string SecondaryMemberID, string SecondaryGroupNumber)
        {
            string json = "";
            string MethodName = "UpdateInsuranceCoverage";
            DataTable dtRegi = new DataTable();
            DataTable dtPatient = new DataTable();


            try
            {

                if (IsSessionExpired(SessionCode))
                {
                    dtPatient = objCommonDAL.GetPatientsDetails(Convert.ToInt32(Session["PatientId"]));
                    dtRegi = objCommonDAL.GetRegistration(Convert.ToInt32(Session["PatientId"]), "GetRegistrationform");

                    if (dtPatient != null && dtPatient.Rows.Count > 0)
                    {

                        string Dob = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["DateOfBirth"]), string.Empty);
                        if (string.IsNullOrEmpty(Dob))
                        {
                            Dob = "1900-01-01 00:00:00.000";
                        }
                        string Gender = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["Gender"]), string.Empty);
                        string Status = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["Status"]), string.Empty);
                        string PatientAddress = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["Address"]), string.Empty);


                        string PatientCity = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["City"]), string.Empty);
                        string PatientState = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["State"]), string.Empty);
                        string PatientZipcode = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["Zip"]), string.Empty);
                        string PatientPrimaryPhone = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["Phone"]), string.Empty);
                        string MobilePhone = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["MobilePhone"]), string.Empty);
                        string PatiantFax = objCommonDAL.CheckNull(Convert.ToString(dtPatient.Rows[0]["Fax"]), string.Empty);

                        string EmergencyContactName = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtemergencyname"]), string.Empty);
                        string EmergencyContactPhoneNumber = objCommonDAL.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtemergency"]), string.Empty);


                        if (dtRegi == null && dtRegi.Rows.Count == 0)
                        {
                            DataRow dr = dtRegi.NewRow();
                            dtRegi.Rows.Add(dr);
                        }

                        DataTable tblRegistration = new DataTable("Registration");
                        tblRegistration.Columns.Add("txtResponsiblepartyFname");
                        tblRegistration.Columns.Add("txtResponsiblepartyLname");
                        tblRegistration.Columns.Add("txtResponsibleRelationship");
                        tblRegistration.Columns.Add("txtResponsibleAddress");
                        tblRegistration.Columns.Add("txtResponsibleCity");
                        tblRegistration.Columns.Add("txtResponsibleState");
                        tblRegistration.Columns.Add("txtResponsibleZipCode");
                        tblRegistration.Columns.Add("txtResponsibleDOB");
                        tblRegistration.Columns.Add("txtResponsibleContact");
                        tblRegistration.Columns.Add("txtPatientPresentPosition");
                        tblRegistration.Columns.Add("txtPatientHowLongHeld");
                        tblRegistration.Columns.Add("txtParentPresentPosition");
                        tblRegistration.Columns.Add("txtParentHowLongHeld");
                        tblRegistration.Columns.Add("txtDriversLicense");
                        tblRegistration.Columns.Add("chkMethodOfPayment");
                        tblRegistration.Columns.Add("txtPurposeCall");
                        tblRegistration.Columns.Add("txtOtherFamily");
                        tblRegistration.Columns.Add("txtWhommay");
                        tblRegistration.Columns.Add("txtSomeonetonotify");
                        tblRegistration.Columns.Add("txtemergency");
                        tblRegistration.Columns.Add("txtemergencyname");
                        tblRegistration.Columns.Add("txtEmployeeName1");
                        tblRegistration.Columns.Add("txtEmployeeDob1");
                        tblRegistration.Columns.Add("txtEmployerName1");
                        tblRegistration.Columns.Add("txtYearsEmployed1");
                        tblRegistration.Columns.Add("txtNameofInsurance1");
                        tblRegistration.Columns.Add("txtInsuranceAddress1");
                        tblRegistration.Columns.Add("txtInsuranceTelephone1");
                        tblRegistration.Columns.Add("txtProgramorpolicy1");
                        tblRegistration.Columns.Add("txtSocialSecurity1");
                        tblRegistration.Columns.Add("txtUnionLocal1");
                        tblRegistration.Columns.Add("txtEmployeeName2");
                        tblRegistration.Columns.Add("txtEmployeeDob2");
                        tblRegistration.Columns.Add("txtEmployerName2");
                        tblRegistration.Columns.Add("txtYearsEmployed2");
                        tblRegistration.Columns.Add("txtNameofInsurance2");
                        tblRegistration.Columns.Add("txtInsuranceAddress2");
                        tblRegistration.Columns.Add("txtInsuranceTelephone2");
                        tblRegistration.Columns.Add("txtProgramorpolicy2");
                        tblRegistration.Columns.Add("txtSocialSecurity2");
                        tblRegistration.Columns.Add("txtUnionLocal2");
                        tblRegistration.Columns.Add("txtDigiSign");
                        string chkMethodOfPayment = string.Empty;
                        tblRegistration.Columns.Add("CreatedDate");
                        tblRegistration.Columns.Add("ModifiedDate");


                        tblRegistration.Rows.Add(ResponsiblePartysFirstName, ResponsiblePartysLastName, Relationship, Address, City, State,
                    Zipcode, "", PhoneNumber, "", "", "", "", "", chkMethodOfPayment, "", "", "", "", EmergencyContactPhoneNumber, EmergencyContactName, PrimaryInsuranceCompany,
                    PrimaryDateOfBirth, PrimaryNameOfInsured, "", PrimaryMemberID, "", PrimaryGroupNumber, "", "", "", SecondaryInsuranceCompany, "",
                    SecondaryNameOfInsured, SecondaryDateOfBirth, SecondaryMemberID, "", SecondaryGroupNumber, "", "", "", "");




                        DataSet dsRegistration = new DataSet("Registration");
                        dsRegistration.Tables.Add(tblRegistration);

                        DataSet ds = dsRegistration.Copy();
                        dsRegistration.Clear();

                        string Registration = ds.GetXml();

                        bool status = objCommonDAL.UpdateRegistrationForm(Convert.ToInt32(Session["PatientId"]), "UpdatePatient", Convert.ToDateTime(Dob), Gender,
                   Status, PatientAddress, PatientCity, PatientState, PatientZipcode,
                    PatientPrimaryPhone, MobilePhone, PatiantFax, Registration);


                        if (status)
                        {

                            objResponse.MethodName = MethodName;
                            objResponse.MagicNumber = MagicNumber;
                            objResponse.ResultCode = 200;
                            objResponse.StatusMessage = "Form updated successfully";
                            objResponse.ResultDetail = string.Empty;


                            json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                        }
                        else
                        {

                            objResponse.MethodName = MethodName;
                            objResponse.MagicNumber = MagicNumber;
                            objResponse.ResultCode = 402;
                            objResponse.IsSuccess = false;
                            objResponse.StatusMessage = "Failed to update form.";
                            objResponse.ResultDetail = string.Empty;

                            json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                        }


                    }






                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;
                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        [HttpPost]
        public string UpdateMedicalHistory(string SessionCode, int MagicNumber, [ModelBinder(typeof(DefaultModelBinder))] List<DentalHistoryAPIUpdate> Data)
        {
            string json = "";
            string MethodName = "UpdateMedicalHistory";
            DataTable dtMedicalhistoryData = new DataTable();
            try
            {

                if (IsSessionExpired(SessionCode))
                {
                    dtMedicalhistoryData = objCommonDAL.GetMedicalHistory(Convert.ToInt32(Session["PatientId"]), "GetMedicalHistory");

                    List<DentalHistoryAPIUpdate> UpdateMedicalHistory = new List<DentalHistoryAPIUpdate>();

                    UpdateMedicalHistory = Data;

                    if (dtMedicalhistoryData == null && dtMedicalhistoryData.Rows.Count == 0)
                    {
                        DataRow dr = dtMedicalhistoryData.NewRow();
                        dtMedicalhistoryData.Rows.Add(dr);
                    }

                    string Mrdosubstances = string.Empty;

                    string MrdQue1 = string.Empty; string Mtxtphysicians = string.Empty;
                    string MrdQue2 = string.Empty; string Mtxthospitalized = string.Empty;
                    string MrdQue3 = string.Empty; string Mtxtserious = string.Empty;
                    string MrdQue4 = string.Empty; string Mtxtmedications = string.Empty;
                    string MrdQue5 = string.Empty; string MtxtRedux = string.Empty;
                    string MrdQue6 = string.Empty; string MtxtFosamax = string.Empty;
                    string MrdQuediet7 = string.Empty; string Mtxt7 = string.Empty;
                    string Mrdotobacco8 = string.Empty; string Mtxt8 = string.Empty;
                    string Mtxt9 = string.Empty; string Mrdopregnant = string.Empty;
                    string Mtxt10 = string.Empty;
                    string Mrdocontraceptives = string.Empty;

                    string MrdoNursing = string.Empty;

                    string MchkQue_1 = string.Empty; string MchkQue_2 = string.Empty; string MchkQue_3 = string.Empty;
                    string MchkQue_4 = string.Empty; string MchkQue_5 = string.Empty; string MchkQue_6 = string.Empty;
                    string MchkQue_7 = string.Empty; string MchkQue_8 = string.Empty; string MchkQue_9 = string.Empty;
                    string MtxtchkQue_9 = string.Empty;


                    string MrdQueAIDS_HIV_Positive = string.Empty;
                    string MrdQueAlzheimer = string.Empty;
                    string MrdQueAnaphylaxis = string.Empty;
                    string MrdQueAnemia = string.Empty;
                    string MrdQueAngina = string.Empty;
                    string MrdQueArthritis_Gout = string.Empty;

                    string MrdQueArtificialHeartValve = string.Empty;
                    string MrdQueArtificialJoint = string.Empty;
                    string MrdQueAsthma = string.Empty;
                    string MrdQueBloodDisease = string.Empty;
                    string MrdQueBloodTransfusion = string.Empty;

                    string MrdQueBreathing = string.Empty;
                    string MrdQueBruise = string.Empty;
                    string MrdQueCancer = string.Empty;
                    string MrdQueChemotherapy = string.Empty;
                    string MrdQueChest = string.Empty;
                    string MrdQueCold_Sores_Fever = string.Empty;

                    string MrdQueCongenital = string.Empty;
                    string MrdQueConvulsions = string.Empty;
                    string MrdQueCortisone = string.Empty;
                    string MrdQueDiabetes = string.Empty;
                    string MrdQueDrug = string.Empty;
                    string MrdQueEasily = string.Empty;

                    string MrdQueEmphysema = string.Empty;
                    string MrdQueEpilepsy = string.Empty;
                    string MrdQueExcessiveBleeding = string.Empty;
                    string MrdQueExcessiveThirst = string.Empty;
                    string MrdQueFainting = string.Empty;

                    string MrdQueFrequentCough = string.Empty;
                    string MrdQueFrequentDiarrhea = string.Empty;
                    string MrdQueFrequentHeadaches = string.Empty;
                    string MrdQueGenital = string.Empty;
                    string MrdQueGlaucoma = string.Empty;

                    string MrdQueHay = string.Empty;
                    string MrdQueHeartAttack_Failure = string.Empty;
                    string MrdQueHeartMurmur = string.Empty;
                    string MrdQueHeartPacemaker = string.Empty;
                    string MrdQueHeartTrouble_Disease = string.Empty;

                    string MrdQueHemophilia = string.Empty;
                    string MrdQueHepatitisA = string.Empty;
                    string MrdQueHepatitisBorC = string.Empty;
                    string MrdQueHerpes = string.Empty;
                    string MrdQueHighBloodPressure = string.Empty;

                    string MrdQueHighCholesterol = string.Empty;
                    string MrdQueHives = string.Empty;
                    string MrdQueHypoglycemia = string.Empty;
                    string MrdQueIrregular = string.Empty;
                    string MrdQueKidney = string.Empty;
                    string MrdQueLeukemia = string.Empty;
                    string MrdQueLiver = string.Empty;
                    string MrdQueLow = string.Empty;
                    string MrdQueLung = string.Empty;
                    string MrdQueMitral = string.Empty;


                    string MrdQueOsteoporosis = string.Empty;
                    string MrdQuePain = string.Empty;
                    string MrdQueParathyroid = string.Empty;
                    string MrdQuePsychiatric = string.Empty;
                    string MrdQueRadiation = string.Empty;
                    string MrdQueRecent = string.Empty;
                    string MrdQueRenal = string.Empty;
                    string MrdQueRheumatic = string.Empty;



                    string MrdQueRheumatism = string.Empty;
                    string MrdQueScarlet = string.Empty;
                    string MrdQueShingles = string.Empty;
                    string MrdQueSickle = string.Empty;
                    string MrdQueSinus = string.Empty;
                    string MrdQueSpina = string.Empty;
                    string MrdQueStomach = string.Empty;


                    string MrdQueStroke = string.Empty;
                    string MrdQueSwelling = string.Empty;
                    string MrdQueThyroid = string.Empty;
                    string MrdQueTonsillitis = string.Empty;
                    string MrdQueTuberculosis = string.Empty;
                    string MrdQueTumors = string.Empty;


                    string MrdQueUlcers = string.Empty;
                    string MrdQueVenereal = string.Empty;
                    string MrdQueYellow = string.Empty;


                    string Mtxtillness = string.Empty;
                    string MtxtComments = string.Empty;



                    for (int i = 0; i < UpdateMedicalHistory.Count; i++)
                    {
                        int HistoryID = Convert.ToInt32(UpdateMedicalHistory[i].HistoryId);





                        if (HistoryID == 1)
                        {
                            MrdQue1 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].Answer), string.Empty));
                            if (UpdateMedicalHistory[i].MultiPartArray.Count > 0)
                            {
                                Mtxtphysicians = objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[0].Answer), string.Empty);
                            }
                        }

                        if (HistoryID == 2)
                        {
                            MrdQue2 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].Answer), string.Empty));
                            if (UpdateMedicalHistory[i].MultiPartArray.Count > 0)
                            {
                                Mtxthospitalized = objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[0].Answer), string.Empty);
                            }
                        }

                        if (HistoryID == 3)
                        {
                            MrdQue3 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].Answer), string.Empty));
                            if (UpdateMedicalHistory[i].MultiPartArray.Count > 0)
                            {
                                Mtxtserious = objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[0].Answer), string.Empty);
                            }
                        }

                        if (HistoryID == 4)
                        {
                            MrdQue4 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].Answer), string.Empty));
                            if (UpdateMedicalHistory[i].MultiPartArray.Count > 0)
                            {
                                Mtxtmedications = objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[0].Answer), string.Empty);
                            }
                        }

                        if (HistoryID == 5)
                        {
                            MrdQue5 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].Answer), string.Empty));
                            if (UpdateMedicalHistory[i].MultiPartArray.Count > 0)
                            {
                                MtxtRedux = objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[0].Answer), string.Empty);
                            }
                        }

                        if (HistoryID == 6)
                        {
                            MrdQue6 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].Answer), string.Empty));
                            if (UpdateMedicalHistory[i].MultiPartArray.Count > 0)
                            {
                                MtxtFosamax = objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[0].Answer), string.Empty);
                            }
                        }

                        if (HistoryID == 7)
                        {
                            MrdQuediet7 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].Answer), string.Empty));
                            if (UpdateMedicalHistory[i].MultiPartArray.Count > 0)
                            {
                                Mtxt7 = objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[0].Answer), string.Empty);
                            }
                        }

                        if (HistoryID == 8)
                        {
                            Mrdotobacco8 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].Answer), string.Empty));
                            if (UpdateMedicalHistory[i].MultiPartArray.Count > 0)
                            {
                                Mtxt8 = objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[0].Answer), string.Empty);
                            }
                        }

                        if (HistoryID == 9)
                        {
                            Mrdosubstances = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].Answer), string.Empty));
                            if (UpdateMedicalHistory[i].MultiPartArray.Count > 0)
                            {
                                Mtxt9 = objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[0].Answer), string.Empty);
                            }
                        }

                        if (HistoryID == 10)
                        {
                            Mrdopregnant = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].Answer), string.Empty));
                            if (UpdateMedicalHistory[i].MultiPartArray.Count > 0)
                            {
                                Mtxt10 = objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[0].Answer), string.Empty);
                            }
                        }

                        if (HistoryID == 11)
                        {

                            Mrdocontraceptives = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].Answer), string.Empty));


                        }

                        if (HistoryID == 12)
                        {
                            MrdoNursing = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].Answer), string.Empty));

                        }

                        if (HistoryID == 13)
                        {
                            if (UpdateMedicalHistory[i].MultiPartArray.Count > 8)
                            {
                                for (int j = 0; j < UpdateMedicalHistory[i].MultiPartArray.Count; j++)
                                {
                                    int SubQuestionHistoryId = Convert.ToInt32(UpdateMedicalHistory[i].MultiPartArray[j].HistoryId);

                                    if (SubQuestionHistoryId == 1)
                                    {
                                        MchkQue_1 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 2)
                                    {
                                        MchkQue_2 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 3)
                                    {
                                        MchkQue_3 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 4)
                                    {
                                        MchkQue_4 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 5)
                                    {
                                        MchkQue_5 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 6)
                                    {
                                        MchkQue_6 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 7)
                                    {
                                        MchkQue_7 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 8)
                                    {
                                        MchkQue_8 = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 9)
                                    {
                                        MtxtchkQue_9 = objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty);
                                    }
                                }
                            }
                        }

                        if (HistoryID == 14)
                        {
                            if (UpdateMedicalHistory[i].MultiPartArray.Count > 76)
                            {

                                for (int j = 0; j < UpdateMedicalHistory[i].MultiPartArray.Count; j++)
                                {
                                    int SubQuestionHistoryId = Convert.ToInt32(UpdateMedicalHistory[i].MultiPartArray[j].HistoryId);

                                    if (SubQuestionHistoryId == 1)
                                    {
                                        MrdQueAIDS_HIV_Positive = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 2)
                                    {
                                        MrdQueAlzheimer = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }


                                    if (SubQuestionHistoryId == 3)
                                    {
                                        MrdQueAnaphylaxis = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 4)
                                    {
                                        MrdQueAnemia = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 5)
                                    {
                                        MrdQueAngina = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 6)
                                    {
                                        MrdQueArthritis_Gout = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 7)
                                    {
                                        MrdQueArtificialHeartValve = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 8)
                                    {
                                        MrdQueArtificialJoint = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 9)
                                    {
                                        MrdQueAsthma = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 10)
                                    {
                                        MrdQueBloodDisease = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }


                                    if (SubQuestionHistoryId == 11)
                                    {
                                        MrdQueBloodTransfusion = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 12)
                                    {
                                        MrdQueBreathing = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 13)
                                    {
                                        MrdQueBruise = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 14)
                                    {
                                        MrdQueCancer = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 15)
                                    {
                                        MrdQueChemotherapy = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 16)
                                    {
                                        MrdQueChest = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 17)
                                    {
                                        MrdQueCold_Sores_Fever = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 18)
                                    {
                                        MrdQueCongenital = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 19)
                                    {
                                        MrdQueConvulsions = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 20)
                                    {
                                        MrdQueCortisone = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }


                                    if (SubQuestionHistoryId == 21)
                                    {
                                        MrdQueDiabetes = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 22)
                                    {
                                        MrdQueDrug = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }


                                    if (SubQuestionHistoryId == 23)
                                    {
                                        MrdQueEasily = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 24)
                                    {
                                        MrdQueEmphysema = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 25)
                                    {
                                        MrdQueEpilepsy = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 26)
                                    {
                                        MrdQueExcessiveBleeding = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 27)
                                    {
                                        MrdQueExcessiveThirst = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 28)
                                    {
                                        MrdQueFainting = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 29)
                                    {
                                        MrdQueFrequentCough = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 30)
                                    {
                                        MrdQueFrequentDiarrhea = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }


                                    if (SubQuestionHistoryId == 31)
                                    {
                                        MrdQueFrequentHeadaches = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 32)
                                    {
                                        MrdQueGenital = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 33)
                                    {
                                        MrdQueGlaucoma = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 34)
                                    {
                                        MrdQueHay = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 35)
                                    {
                                        MrdQueHeartAttack_Failure = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 36)
                                    {
                                        MrdQueHeartMurmur = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 37)
                                    {
                                        MrdQueHeartPacemaker = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 38)
                                    {
                                        MrdQueHeartTrouble_Disease = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 39)
                                    {
                                        MrdQueHemophilia = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 40)
                                    {
                                        MrdQueHepatitisA = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 41)
                                    {
                                        MrdQueHepatitisBorC = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 42)
                                    {
                                        MrdQueHerpes = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }


                                    if (SubQuestionHistoryId == 43)
                                    {
                                        MrdQueHighBloodPressure = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 44)
                                    {
                                        MrdQueHighCholesterol = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 45)
                                    {
                                        MrdQueHives = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 46)
                                    {
                                        MrdQueHypoglycemia = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 47)
                                    {
                                        MrdQueIrregular = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 48)
                                    {
                                        MrdQueKidney = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 49)
                                    {
                                        MrdQueLeukemia = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 50)
                                    {
                                        MrdQueLiver = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }


                                    if (SubQuestionHistoryId == 51)
                                    {
                                        MrdQueLow = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 52)
                                    {
                                        MrdQueLung = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 53)
                                    {
                                        MrdQueMitral = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 54)
                                    {
                                        MrdQueOsteoporosis = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 55)
                                    {
                                        MrdQuePain = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 56)
                                    {
                                        MrdQueParathyroid = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 57)
                                    {
                                        MrdQuePsychiatric = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 58)
                                    {
                                        MrdQueRadiation = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 59)
                                    {
                                        MrdQueRecent = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 60)
                                    {
                                        MrdQueRenal = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }


                                    if (SubQuestionHistoryId == 61)
                                    {
                                        MrdQueRheumatic = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 62)
                                    {
                                        MrdQueRheumatism = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }


                                    if (SubQuestionHistoryId == 63)
                                    {
                                        MrdQueScarlet = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 64)
                                    {
                                        MrdQueShingles = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 65)
                                    {
                                        MrdQueSickle = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 66)
                                    {
                                        MrdQueSinus = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 67)
                                    {
                                        MrdQueSpina = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 68)
                                    {
                                        MrdQueStomach = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 69)
                                    {
                                        MrdQueStroke = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 70)
                                    {
                                        MrdQueSwelling = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }


                                    if (SubQuestionHistoryId == 71)
                                    {
                                        MrdQueThyroid = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 72)
                                    {
                                        MrdQueTonsillitis = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 73)
                                    {
                                        MrdQueTuberculosis = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 74)
                                    {
                                        MrdQueTumors = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 75)
                                    {
                                        MrdQueUlcers = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 76)
                                    {
                                        MrdQueVenereal = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                    if (SubQuestionHistoryId == 77)
                                    {
                                        MrdQueYellow = CheckOutput(objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].MultiPartArray[j].Answer), string.Empty));
                                    }

                                }

                            }
                        }

                        if (HistoryID == 15)
                        {
                            Mtxtillness = objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].Answer), string.Empty);
                        }

                        if (HistoryID == 16)
                        {
                            MtxtComments = objCommonDAL.CheckNull(Convert.ToString(UpdateMedicalHistory[i].Answer), string.Empty);
                        }

                    }

                    DataTable tblMedicalHistory = new DataTable("MedicalHistory");
                    tblMedicalHistory.Columns.Add("MrdQue1");
                    tblMedicalHistory.Columns.Add("Mtxtphysicians");
                    tblMedicalHistory.Columns.Add("MrdQue2");
                    tblMedicalHistory.Columns.Add("Mtxthospitalized");
                    tblMedicalHistory.Columns.Add("MrdQue3");
                    tblMedicalHistory.Columns.Add("Mtxtserious");
                    tblMedicalHistory.Columns.Add("MrdQue4");
                    tblMedicalHistory.Columns.Add("Mtxtmedications");
                    tblMedicalHistory.Columns.Add("MrdQue5");
                    tblMedicalHistory.Columns.Add("MtxtRedux");
                    tblMedicalHistory.Columns.Add("MrdQue6");
                    tblMedicalHistory.Columns.Add("MtxtFosamax");

                    tblMedicalHistory.Columns.Add("MrdQuediet7");
                    tblMedicalHistory.Columns.Add("Mtxt7");
                    tblMedicalHistory.Columns.Add("Mrdotobacco8");
                    tblMedicalHistory.Columns.Add("Mtxt8");
                    tblMedicalHistory.Columns.Add("Mrdosubstances");
                    tblMedicalHistory.Columns.Add("Mtxt9");
                    tblMedicalHistory.Columns.Add("Mrdopregnant");
                    tblMedicalHistory.Columns.Add("Mtxt10");
                    tblMedicalHistory.Columns.Add("Mrdocontraceptives");
                    tblMedicalHistory.Columns.Add("MrdoNursing");
                    tblMedicalHistory.Columns.Add("MchkQue_1");
                    tblMedicalHistory.Columns.Add("MchkQue_2");
                    tblMedicalHistory.Columns.Add("MchkQue_3");
                    tblMedicalHistory.Columns.Add("MchkQue_4");
                    tblMedicalHistory.Columns.Add("MchkQue_5");
                    tblMedicalHistory.Columns.Add("MchkQue_6");
                    tblMedicalHistory.Columns.Add("MchkQue_7");
                    tblMedicalHistory.Columns.Add("MchkQue_8");
                    tblMedicalHistory.Columns.Add("MchkQue_9");
                    tblMedicalHistory.Columns.Add("MtxtchkQue_9");
                    tblMedicalHistory.Columns.Add("MrdQueAIDS_HIV_Positive");
                    tblMedicalHistory.Columns.Add("MrdQueAlzheimer");
                    tblMedicalHistory.Columns.Add("MrdQueAnaphylaxis");
                    tblMedicalHistory.Columns.Add("MrdQueAnemia");
                    tblMedicalHistory.Columns.Add("MrdQueAngina");
                    tblMedicalHistory.Columns.Add("MrdQueArthritis_Gout");
                    tblMedicalHistory.Columns.Add("MrdQueArtificialHeartValve");
                    tblMedicalHistory.Columns.Add("MrdQueArtificialJoint");
                    tblMedicalHistory.Columns.Add("MrdQueAsthma");
                    tblMedicalHistory.Columns.Add("MrdQueBloodDisease");
                    tblMedicalHistory.Columns.Add("MrdQueBloodTransfusion");
                    tblMedicalHistory.Columns.Add("MrdQueBreathing");
                    tblMedicalHistory.Columns.Add("MrdQueBruise");
                    tblMedicalHistory.Columns.Add("MrdQueCancer");
                    tblMedicalHistory.Columns.Add("MrdQueChemotherapy");
                    tblMedicalHistory.Columns.Add("MrdQueChest");
                    tblMedicalHistory.Columns.Add("MrdQueCold_Sores_Fever");
                    tblMedicalHistory.Columns.Add("MrdQueCongenital");
                    tblMedicalHistory.Columns.Add("MrdQueConvulsions");
                    tblMedicalHistory.Columns.Add("MrdQueCortisone");
                    tblMedicalHistory.Columns.Add("MrdQueDiabetes");
                    tblMedicalHistory.Columns.Add("MrdQueDrug");
                    tblMedicalHistory.Columns.Add("MrdQueEasily");
                    tblMedicalHistory.Columns.Add("MrdQueEmphysema");
                    tblMedicalHistory.Columns.Add("MrdQueEpilepsy");
                    tblMedicalHistory.Columns.Add("MrdQueExcessiveBleeding");
                    tblMedicalHistory.Columns.Add("MrdQueExcessiveThirst");
                    tblMedicalHistory.Columns.Add("MrdQueFainting");
                    tblMedicalHistory.Columns.Add("MrdQueFrequentCough");
                    tblMedicalHistory.Columns.Add("MrdQueFrequentDiarrhea");
                    tblMedicalHistory.Columns.Add("MrdQueFrequentHeadaches");
                    tblMedicalHistory.Columns.Add("MrdQueGenital");
                    tblMedicalHistory.Columns.Add("MrdQueGlaucoma");
                    tblMedicalHistory.Columns.Add("MrdQueHay");
                    tblMedicalHistory.Columns.Add("MrdQueHeartAttack_Failure");
                    tblMedicalHistory.Columns.Add("MrdQueHeartMurmur");
                    tblMedicalHistory.Columns.Add("MrdQueHeartPacemaker");
                    tblMedicalHistory.Columns.Add("MrdQueHeartTrouble_Disease");
                    tblMedicalHistory.Columns.Add("MrdQueHemophilia");
                    tblMedicalHistory.Columns.Add("MrdQueHepatitisA");
                    tblMedicalHistory.Columns.Add("MrdQueHepatitisBorC");
                    tblMedicalHistory.Columns.Add("MrdQueHerpes");
                    tblMedicalHistory.Columns.Add("MrdQueHighBloodPressure");
                    tblMedicalHistory.Columns.Add("MrdQueHighCholesterol");
                    tblMedicalHistory.Columns.Add("MrdQueHives");
                    tblMedicalHistory.Columns.Add("MrdQueHypoglycemia");
                    tblMedicalHistory.Columns.Add("MrdQueIrregular");
                    tblMedicalHistory.Columns.Add("MrdQueKidney");
                    tblMedicalHistory.Columns.Add("MrdQueLeukemia");
                    tblMedicalHistory.Columns.Add("MrdQueLiver");
                    tblMedicalHistory.Columns.Add("MrdQueLow");
                    tblMedicalHistory.Columns.Add("MrdQueLung");
                    tblMedicalHistory.Columns.Add("MrdQueMitral");
                    tblMedicalHistory.Columns.Add("MrdQueOsteoporosis");
                    tblMedicalHistory.Columns.Add("MrdQuePain");
                    tblMedicalHistory.Columns.Add("MrdQueParathyroid");
                    tblMedicalHistory.Columns.Add("MrdQuePsychiatric");
                    tblMedicalHistory.Columns.Add("MrdQueRadiation");
                    tblMedicalHistory.Columns.Add("MrdQueRecent");
                    tblMedicalHistory.Columns.Add("MrdQueRenal");
                    tblMedicalHistory.Columns.Add("MrdQueRheumatic");
                    tblMedicalHistory.Columns.Add("MrdQueRheumatism");
                    tblMedicalHistory.Columns.Add("MrdQueScarlet");
                    tblMedicalHistory.Columns.Add("MrdQueShingles");
                    tblMedicalHistory.Columns.Add("MrdQueSickle");
                    tblMedicalHistory.Columns.Add("MrdQueSinus");
                    tblMedicalHistory.Columns.Add("MrdQueSpina");
                    tblMedicalHistory.Columns.Add("MrdQueStomach");
                    tblMedicalHistory.Columns.Add("MrdQueStroke");
                    tblMedicalHistory.Columns.Add("MrdQueSwelling");
                    tblMedicalHistory.Columns.Add("MrdQueThyroid");
                    tblMedicalHistory.Columns.Add("MrdQueTonsillitis");
                    tblMedicalHistory.Columns.Add("MrdQueTuberculosis");
                    tblMedicalHistory.Columns.Add("MrdQueTumors");
                    tblMedicalHistory.Columns.Add("MrdQueUlcers");
                    tblMedicalHistory.Columns.Add("MrdQueVenereal");
                    tblMedicalHistory.Columns.Add("MrdQueYellow");

                    tblMedicalHistory.Columns.Add("Mtxtillness");
                    tblMedicalHistory.Columns.Add("MtxtComments");
                    tblMedicalHistory.Columns.Add("CreatedDate");
                    tblMedicalHistory.Columns.Add("ModifiedDate");


                    tblMedicalHistory.Rows.Add(MrdQue1, Mtxtphysicians, MrdQue2, Mtxthospitalized, MrdQue3, Mtxtserious, MrdQue4, Mtxtmedications, MrdQue5, MtxtRedux, MrdQue6,
MtxtFosamax, MrdQuediet7, Mtxt7, Mrdotobacco8, Mtxt8, Mrdosubstances, Mtxt9, Mrdopregnant, Mtxt10, Mrdocontraceptives, MrdoNursing, MchkQue_1, MchkQue_2, MchkQue_3, MchkQue_4, MchkQue_5,
MchkQue_6, MchkQue_7, MchkQue_8, MchkQue_9, MtxtchkQue_9, MrdQueAIDS_HIV_Positive, MrdQueAlzheimer, MrdQueAnaphylaxis, MrdQueAnemia, MrdQueAngina, MrdQueArthritis_Gout, MrdQueArtificialHeartValve,
MrdQueArtificialJoint, MrdQueAsthma, MrdQueBloodDisease, MrdQueBloodTransfusion, MrdQueBreathing, MrdQueBruise, MrdQueCancer, MrdQueChemotherapy, MrdQueChest, MrdQueCold_Sores_Fever,
MrdQueCongenital, MrdQueConvulsions, MrdQueCortisone, MrdQueDiabetes, MrdQueDrug, MrdQueEasily, MrdQueEmphysema, MrdQueEpilepsy, MrdQueExcessiveBleeding, MrdQueExcessiveThirst, MrdQueFainting,
MrdQueFrequentCough, MrdQueFrequentDiarrhea, MrdQueFrequentHeadaches, MrdQueGenital, MrdQueGlaucoma, MrdQueHay, MrdQueHeartAttack_Failure, MrdQueHeartMurmur, MrdQueHeartPacemaker,
MrdQueHeartTrouble_Disease, MrdQueHemophilia, MrdQueHepatitisA, MrdQueHepatitisBorC, MrdQueHerpes, MrdQueHighBloodPressure, MrdQueHighCholesterol, MrdQueHives, MrdQueHypoglycemia,
MrdQueIrregular, MrdQueKidney, MrdQueLeukemia, MrdQueLiver, MrdQueLow, MrdQueLung, MrdQueMitral, MrdQueOsteoporosis, MrdQuePain, MrdQueParathyroid, MrdQuePsychiatric,
MrdQueRadiation, MrdQueRecent, MrdQueRenal, MrdQueRheumatic, MrdQueRheumatism, MrdQueScarlet, MrdQueShingles, MrdQueSickle, MrdQueSinus, MrdQueSpina, MrdQueStomach, MrdQueStroke, MrdQueSwelling,
MrdQueThyroid, MrdQueTonsillitis, MrdQueTuberculosis, MrdQueTumors, MrdQueUlcers, MrdQueVenereal, MrdQueYellow, Mtxtillness, MtxtComments);


                    DataSet dsMedicalHistory = new DataSet("MedicalHistory");
                    dsMedicalHistory.Tables.Add(tblMedicalHistory);

                    DataSet ds = dsMedicalHistory.Copy();
                    dsMedicalHistory.Clear();

                    string MedicalHistory = ds.GetXml();

                    bool status = objCommonDAL.UpdateMedicalHistory(Convert.ToInt32(Session["PatientId"]), "UpdateMedicalHistory", MedicalHistory);


                    if (status)
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 200;
                        objResponse.StatusMessage = "Form updated successfully";
                        objResponse.ResultDetail = string.Empty;


                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {

                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "Failed to update form.";
                        objResponse.ResultDetail = string.Empty;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                    }

                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;
                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        private DataTable GetBlankTableForDentalHistory()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("Previous Dentists Name"));
            dt.Columns.Add(new DataColumn("Previous Dentists Name|Address"));
            dt.Columns.Add(new DataColumn("Previous Dentists Name|Phone"));
            dt.Columns.Add(new DataColumn("Previous Dentists Name|Email"));
            dt.Columns.Add(new DataColumn("When was the last time you had your teeth cleaned?"));
            dt.Columns.Add(new DataColumn("Do you make regular visits to the dentist?"));
            dt.Columns.Add(new DataColumn("Do you make regular visits to the dentist?|How often"));
            dt.Columns.Add(new DataColumn("Please explain any problems or complications with previous dental treatment?"));
            dt.Columns.Add(new DataColumn("Have any of your teeth been replaced with the following?|Fixed Bridge"));
            dt.Columns.Add(new DataColumn("Have any of your teeth been replaced with the following?|Removeable Bridge"));

            dt.Columns.Add(new DataColumn("Have any of your teeth been replaced with the following?|Denture"));
            dt.Columns.Add(new DataColumn("Have any of your teeth been replaced with the following?|Implant"));
            dt.Columns.Add(new DataColumn("Do you clench or grind your teeth?"));
            dt.Columns.Add(new DataColumn("Does your jaw click or pop?"));
            dt.Columns.Add(new DataColumn("Are you experiencing any pain or soreness in the muscles of your face?"));
            dt.Columns.Add(new DataColumn("Do you have frequent headaches, neckaches or shoulder aches?"));
            dt.Columns.Add(new DataColumn("Does food get caught in your teeth?"));
            dt.Columns.Add(new DataColumn("Are any of your teeth sensitive to|Hot|Cold|Sweets|Pressure"));
            dt.Columns.Add(new DataColumn("Do your gums bleed or hurt?"));
            dt.Columns.Add(new DataColumn("Do your gums bleed or hurt?|When"));

            dt.Columns.Add(new DataColumn("How often do you brush your teeth?"));
            dt.Columns.Add(new DataColumn("How often do you use dental floss?"));
            dt.Columns.Add(new DataColumn("Are any of your teeth loose, tipped, shifted or chipped?"));
            dt.Columns.Add(new DataColumn("Do you feel your breath is often offensive?"));
            dt.Columns.Add(new DataColumn("Do you feel your breath is often offensive?|When"));
            dt.Columns.Add(new DataColumn("Explain any orthodonic work you have had done?"));
            dt.Columns.Add(new DataColumn("Explain any dental work you have had done (Perio, Oral Surgery, etc.)"));
            dt.Columns.Add(new DataColumn("How do you feel about your teeth in general?"));

            dt.Columns.Add(new DataColumn("Additional Information"));
            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            return dt;
        }

        private DataTable GetBlankTableForMedicalHistory()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add(new DataColumn("Are you under a physicians care right now?"));
            dt.Columns.Add(new DataColumn("Are you under a physicians care right now?|Please explain"));
            dt.Columns.Add(new DataColumn("Have you ever been hospitalized or had a major operation?"));
            dt.Columns.Add(new DataColumn("Have you ever been hospitalized or had a major operation?|Please explain"));
            dt.Columns.Add(new DataColumn("Have you ever had a serious head or neck injury?"));


            dt.Columns.Add(new DataColumn("Have you ever had a serious head or neck injury?|Please explain"));
            dt.Columns.Add(new DataColumn("Are taking any medications, pills, or drugs?"));
            dt.Columns.Add(new DataColumn("Are taking any medications, pills, or drugs?|Please list"));
            dt.Columns.Add(new DataColumn("Do you take, or have you taken, Phen-Fen or Redux?"));
            dt.Columns.Add(new DataColumn("Do you take, or have you taken, Phen-Fen or Redux?|Please explain"));


            dt.Columns.Add(new DataColumn("Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?"));
            dt.Columns.Add(new DataColumn("Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?|Please explain"));
            dt.Columns.Add(new DataColumn("Are you on a special diet?"));
            dt.Columns.Add(new DataColumn("Are you on a special diet?|Please explain"));
            dt.Columns.Add(new DataColumn("Do you use tobacco?"));


            dt.Columns.Add(new DataColumn("Do you use tobacco?|How often"));
            dt.Columns.Add(new DataColumn("Do you use controlled substances?"));
            dt.Columns.Add(new DataColumn("Do you use controlled substances?|Please explain"));
            dt.Columns.Add(new DataColumn("Are you pregnant/Trying to get pregnant(women)?"));
            dt.Columns.Add(new DataColumn("Are you pregnant/Trying to get pregnant(women)?|Due date"));

            dt.Columns.Add(new DataColumn("Taking oral contraceptives(women)?"));
            dt.Columns.Add(new DataColumn("Nursing(women)?"));
            dt.Columns.Add(new DataColumn("Are you allergic to any of the following?|Aspirin"));
            dt.Columns.Add(new DataColumn("Are you allergic to any of the following?|Penicillin"));
            dt.Columns.Add(new DataColumn("Are you allergic to any of the following?|Codeine"));


            dt.Columns.Add(new DataColumn("Are you allergic to any of the following?|Local Anesthetics"));
            dt.Columns.Add(new DataColumn("Are you allergic to any of the following?|Acrylic"));
            dt.Columns.Add(new DataColumn("Are you allergic to any of the following?|Metal"));
            dt.Columns.Add(new DataColumn("Are you allergic to any of the following?|Latex"));
            dt.Columns.Add(new DataColumn("Are you allergic to any of the following?|Sulfa Drugs"));

            dt.Columns.Add(new DataColumn("Are you allergic to any of the following?|If other, please explain list"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|AIDS/HIV Positive"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Alzheimer’s Disease"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Anaphylaxis"));

            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Anemia"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Angina"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Arthritis/Gout"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Artificial Heart Valve"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Artificial Joint"));


            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Asthma"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Blood Disease"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Blood Transfusion"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Breathing Problem"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Bruise Easily"));

            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Cancer"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Chemotherapy"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Chest Pains"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Cold Sores/Fever Blisters"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Congenital Heart Disorder"));


            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Convulsions"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Cortisone Medicine"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Diabetes"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Drug Addiction"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Easily Winded"));

            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Emphysema"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Epilepsy or Seizures"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Excessive Bleeding"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Excessive Thirst"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Fainting Spells/Dizziness"));


            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Frequent Cough"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Frequent Diarrhea"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Frequent Headaches"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Genital Herpes"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Glaucoma"));

            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Hay Fever"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Heart Attack/Failure"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Heart Murmur"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Heart Pacemaker"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Heart Trouble/Disease"));


            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Hemophilia"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Hepatitis A"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Hepatitis B or C"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Herpes"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|High Blood Pressure"));

            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|High Cholesterol"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Hives or Rash"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Hypoglycemia"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Irregular Heartbeat"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Kidney Problems"));


            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Leukemia"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Liver Disease"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Low Blood Pressure"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Lung Disease"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Mitral Valve Prolapse"));

            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Osteoporosis"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Pain in Jaw Joints"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Parathyroid Disease"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Psychiatric Care"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Radiation Treatments"));


            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Recent Weight Loss"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Renal Dialysis"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Rheumatic Fever"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Rheumatism"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Scarlet Fever"));

            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Shingles"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Sickle Cell Disease"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Sinus Trouble"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Spina Bifida"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Stomach/Intestinal Disease"));

            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Stroke"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Swelling of Limbs"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Thyroid Disease"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Tonsillitis"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Tuberculosis"));


            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Tumors or Growths"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Ulcers"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Venereal Disease"));
            dt.Columns.Add(new DataColumn("Do you have, or have you had, any of the following?|Yellow Jaundice"));
            dt.Columns.Add(new DataColumn("List any serious illness not listed above"));

            dt.Columns.Add(new DataColumn("Additional Information"));


            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            return dt;
        }

        private string CheckOutput(string value)
        {
            if (!string.IsNullOrEmpty(value) && value == "Yes")
            {
                value = "Y";
            }
            else
            {
                value = "N";
            }
            return value;
        }

        /// <summary>
        /// Method Name :GetPatientListByDoctor
        /// Desciprtion :This Method Use for Get Details of Patient
        /// Request : SessionCode
        /// Response : { FirstName : john ,LastName :martin ,Email : john123@gmail.com}
        /// </summary>
        [HttpPost]
        public string GetPatientListByDoctor(string SessionCode, int MagicNumber, int DoctorId)
        {

            string json = "";

            string MethodName = "GetPatientListByDoctor";

            DataTable dtPatient = new DataTable();

            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    List<PateintDetailsByParticularDoctor> PatientDetails = new List<PateintDetailsByParticularDoctor>();

                    dtPatient = objCommonDAL.GetPatientsDetailsByDoctor(DoctorId);

                    if (dtPatient != null && dtPatient.Rows.Count > 0)
                    {



                        foreach (DataRow item in dtPatient.Rows)
                        {
                            PateintDetailsByParticularDoctor objpatientdetails = new PateintDetailsByParticularDoctor();
                            objpatientdetails.FirstName = objCommonDAL.CheckNull(Convert.ToString(item["FirstName"]), string.Empty);
                            objpatientdetails.LastName = objCommonDAL.CheckNull(Convert.ToString(item["LastName"]), string.Empty);
                            objpatientdetails.EmailAddress = objCommonDAL.CheckNull(Convert.ToString(item["Email"]), string.Empty);
                            PatientDetails.Add(objpatientdetails);
                        }



                        dynamic dResult = new { PatientDetails };
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.StatusMessage = "Patient detail successfully retrieved.";
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = dResult;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.MethodName = MethodName;
                        objResponse.MagicNumber = MagicNumber;
                        objResponse.ResultCode = 402;
                        objResponse.IsSuccess = false;
                        objResponse.StatusMessage = "No record found.";
                        objResponse.ResultDetail = string.Empty;
                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                    }

                }
                else
                {
                    objResponse.MethodName = MethodName;
                    objResponse.MagicNumber = MagicNumber;
                    objResponse.ResultCode = 400;
                    objResponse.IsSuccess = false;
                    objResponse.StatusMessage = "Your Session has been logout.";
                    objResponse.ResultDetail = string.Empty;
                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }
                return json;
            }
            catch (Exception ex)
            {
                objResponse.MethodName = MethodName;
                objResponse.MagicNumber = MagicNumber;
                objResponse.ResultCode = 401;
                objResponse.IsSuccess = false;
                objResponse.StatusMessage = "Server error occured - Please contact administrator." + ex.Message;
                objResponse.ResultDetail = string.Empty;
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }


        }
        /// <summary>
        /// Method Name :GetPatientAppointmentList
        /// Desciprtion :This Method Use for Get Details of Patient
        /// Request : SessionCode
        /// Response : { FirstName : john ,LastName :martin ,Email : john123@gmail.com}
        /// </summary>
        [HttpPost]
        public string GetPatientAppointmentList(string SessionCode, int MagicNumber, DateTime Fromdate, DateTime ToDate)
        {
            string json = "";
            string MethodName = "GetPatientAppointmentList";
            DataTable dt = new DataTable();
            Appointment appointment = new Appointment();
            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    int PatientId = Convert.ToInt32(Session["PatientId"]);
                    objAppointment = new clsAppointment();
                    dt = objAppointment.GetPatientAppointmentView(PatientId, 3, 2,Fromdate,ToDate);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            appointment.ViewApplst.Add(new ViewAppointment()
                            {
                                AppointmentId = Convert.ToInt32(item["AppointmentId"]),
                                AppointmentDate = Convert.ToDateTime(item["AppointmentDate"]).ToString("MM/dd/yyyy"),
                                AppointmentTime = DateTime.Today.Add(TimeSpan.Parse(item["AppointmentTime"].ToString())).ToString(AppointmentSetting.CONSTTIMEFORMATE),
                                ServiceType = item["ServiceType"].ToString(),
                                AppointmentLength = Convert.ToUInt32(item["AppointmentLength"]),
                                DoctorName = Convert.ToString(item["DoctorName"]),
                                Status = Convert.ToString(item["Status"]),
                                Rating = Convert.ToString(item["Rating"]),
                                AppointmentStatusId = Convert.ToInt32(item["AppointmentStatusId"]),
                                AppointmentDateTime = Convert.ToString(item["AppointmentDateTime"]),
                            });
                        }
                        dynamic dResult = new { appointment.ViewApplst };
                        json = JsonConvert.SerializeObject(GetResponse(MethodName, MagicNumber, 200, true, "Appointment detail successfully retrieved.", dResult), Formatting.Indented);

                    }
                    else
                    {
                        json = JsonConvert.SerializeObject(GetResponse(MethodName, MagicNumber, 402, false, "No record found.", string.Empty), Formatting.Indented);

                    }

                }
                else
                {
                    json = JsonConvert.SerializeObject(GetResponse(MethodName, MagicNumber, 400, false, "Your Session has been logout.", string.Empty), Formatting.Indented);
                }
                return json;

            }
            catch (Exception ex)
            {

                json = JsonConvert.SerializeObject(GetResponse(MethodName, MagicNumber, 401, false, "Server error occured - Please contact administrator." + ex.Message, string.Empty), Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }


        }
        [HttpPost]
        public string GetDoctorForAppointment(string SessionCode, int MagicNumber)
        {
            string json = "";
            string MethodName = "GetDoctorForAppointment";
            DataTable dt = new DataTable();
            Appointment appointment = new Appointment();
            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    int PatientId = Convert.ToInt32(Session["PatientId"]);
                    objAppointment = new clsAppointment();
                    dt = objAppointment.GetTreatingDoctorListForBookAppointmentByPatientId(PatientId, 0);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            appointment.DoctorList.Add(new SelectListItem
                            {
                                Value = Convert.ToString(item["DoctorId"]),
                                Text = item["DoctorName"].ToString(),
                            });
                        }
                        dynamic dResult = new { appointment.DoctorList };
                        json = JsonConvert.SerializeObject(GetResponse(MethodName, MagicNumber, 200, true, "Appointment Doctor detail successfully retrieved.", dResult), Formatting.Indented);

                    }
                    else
                    {
                        json = JsonConvert.SerializeObject(GetResponse(MethodName, MagicNumber, 402, false, "No record found.", string.Empty), Formatting.Indented);

                    }

                }
                else
                {
                    json = JsonConvert.SerializeObject(GetResponse(MethodName, MagicNumber, 400, false, "Your Session has been logout.", string.Empty), Formatting.Indented);
                }
                return json;

            }
            catch (Exception ex)
            {

                json = JsonConvert.SerializeObject(GetResponse(MethodName, MagicNumber, 401, false, "Server error occured - Please contact administrator." + ex.Message, string.Empty), Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }


        }
        [HttpPost]
        public string GetServiceForAppointment(string SessionCode, int MagicNumber, int DoctorId)
        {
            string json = "";
            string MethodName = "GetServiceForAppointment";
            DataTable dt = new DataTable();
            Appointment appointment = new Appointment();
            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    objAppointment = new clsAppointment();
                    var doctorDetails = objAppointment.GetDoctorDetails(DoctorId);
                    int ParentUserId = 0;
                    if (doctorDetails.Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(doctorDetails.Rows[0]["ParentUserId"])))
                        {
                            ParentUserId = Convert.ToInt32(doctorDetails.Rows[0]["ParentUserId"]);
                            if (ParentUserId == 0)
                            {
                                ParentUserId = DoctorId;
                            }
                        }
                        else { ParentUserId = DoctorId; }
                    }
                    dt = objAppointment.GetDoctorServicesByDoctorId(ParentUserId, 0);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            appointment.DoctorServiceslst.Add(new SelectListItem
                            {
                                Value = Convert.ToString(item["AppointmentServiceId"]),
                                Text = item["ServiceType"].ToString()
                            });
                        }
                        dynamic dResult = new { appointment.DoctorServiceslst };
                        json = JsonConvert.SerializeObject(GetResponse(MethodName, MagicNumber, 200, true, "Appointment Doctor detail successfully retrieved.", dResult), Formatting.Indented);
                    }
                    else
                    {
                        json = JsonConvert.SerializeObject(GetResponse(MethodName, MagicNumber, 402, false, "No record found.", string.Empty), Formatting.Indented);

                    }

                }
                else
                {
                    json = JsonConvert.SerializeObject(GetResponse(MethodName, MagicNumber, 400, false, "Your Session has been logout.", string.Empty), Formatting.Indented);
                }
                return json;

            }
            catch (Exception ex)
            {

                json = JsonConvert.SerializeObject(GetResponse(MethodName, MagicNumber, 401, false, "Server error occured - Please contact administrator." + ex.Message, string.Empty), Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }


        }

        public APIResponse GetResponse(string MethodName, int MagicNumber, int ResultCode, bool IsSuccess, string StatusMessage, dynamic ResultDetail)
        {
            APIResponse objResponse = new APIResponse();
            objResponse.MethodName = MethodName;
            objResponse.MagicNumber = MagicNumber;
            objResponse.ResultCode = ResultCode;
            objResponse.IsSuccess = IsSuccess;
            objResponse.StatusMessage = StatusMessage;
            objResponse.ResultDetail = ResultDetail;
            return objResponse;

        }
    }

}
