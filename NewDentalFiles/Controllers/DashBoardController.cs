﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using DentalFiles.Models;
using System.Configuration;
using JQueryDataTables.Models.Repository;
using DentalFiles.App_Start;
using System.Web.Security;

namespace DentalFiles.Controllers
{
    public class DashBoardController : Controller
    {
        int PatientId = 0;
        CommonDAL objCommonDAL = new CommonDAL();
        DataRepository objRepository = new DataRepository();

        public ActionResult Index()
        {


            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                ViewBag.PatientId = PatientId = Convert.ToInt32(SessionManagement.PatientId);
                ViewBag.Title = "DashBoard";
                objCommonDAL.GetActiveCompany();
                ViewBag.ReturnUrl = "DashBoard";
                List<PatientDetails> PatientDetialList = new List<PatientDetails>();
                PatientDetialList = objRepository.GetPatientDetails(PatientId, SessionManagement.TimeZoneSystemName);
                if (PatientDetialList.Count != 0)
                {
                    ViewBag.FirstName = PatientDetialList[0].FirstName.ToString();
                    if (!string.IsNullOrEmpty(PatientDetialList[0].ProfileImage.ToString()))
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["imagebankpath"] + PatientDetialList[0].ProfileImage.ToString();
                    }
                    else if (PatientDetialList[0].Gender == 1)
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"];
                    }
                    else
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["Doctorimage1"];
                        
                    }
                    
                    if (PatientDetialList[0].StayloggedMins != null && PatientDetialList[0].StayloggedMins != "" && Convert.ToInt32(PatientDetialList[0].StayloggedMins) != 0)
                    {
                        Session.Timeout = Convert.ToInt32(PatientDetialList[0].StayloggedMins);
                    }
                    else
                    {
                        Session.Timeout = 60;
                    }

                    HttpCookie myCookie = new HttpCookie("Timeout");
                    myCookie.Value = Convert.ToString(Session.Timeout);
                    myCookie.Expires = DateTime.Now.AddDays(1d);
                    Response.Cookies.Add(myCookie);
                }

                return View(PatientDetialList);
            }
            else
            {
                return RedirectToAction("Index", "Index");
            }
        }
        [HttpGet]
        public ActionResult Logout()
        {
            SessionManagement.UserId = 0;
            SessionManagement.PatientId = 0;
            Session.RemoveAll();
            Session.Abandon();
            FormsAuthentication.SignOut();
            Response.Cookies.Remove("Timeout");
            return RedirectToAction("Index", "Index");
        }



    }
}
