﻿using BO.Enums;
using BO.Models;
using BusinessLogicLayer;
using DentalFiles.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BO.ViewModel;
using DataAccessLayer;
using System.Data;
using DentalFilesNew.App_Start;

namespace DentalFilesNew.Controllers
{
    public class PaymentController : Controller
    {
        /// <summary>
        ///  This is the Main page of payment 
        /// </summary>
        /// <returns></returns>
        [CustomAuthorizeAttribute]
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// This is Get Method of Make Transaction taken Patients data from database and fill-out on the view.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorizeAttribute]
        public ActionResult MakeTransaction()
        {
            ViewBag.ReturnUrl = "Payment";
            ViewBag.doctorList = ZPPaymentBAL.GetDoctorList(Convert.ToInt32(SessionManagement.PatientId));
            Patients mdlNewPatient = new Patients();
            mdlNewPatient = PatientBLL.GetPatientDetailsById(SessionManagement.PatientId,null);
            ZPTransactionRequest model = new ZPTransactionRequest();
            if (mdlNewPatient != null)
            {
                model.holderName = (mdlNewPatient.FirstName + " " + mdlNewPatient.LastName).Trim();
            }
            model.lstCard = ZPPaymentBAL.GetPatientPaymentTokenByPatientId(SessionManagement.PatientId);
            model.amount = Convert.ToString(TempData["pay_amount"]);
            TempData["pay_ziftpayaccountid"] = ZPPaymentBAL.GetAccountZiftPayDetailsByUserId(Convert.ToInt32(TempData["pay_doctorid"]));
            return View(model);
        }

        /// <summary>
        /// This method is Post and Call the S1p side for Make a Transaction of the Patient.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        [CustomAuthorizeAttribute]
        public ActionResult MakeTransaction(ZPTransactionRequest obj)
        {
            if (ModelState.IsValid)
            {
                //This Function mapped the ZPTransactionRequest with Patients object. 
                obj = ZPPaymentBAL.MappObjectOfZpTransactionReqeust(Convert.ToInt32(SessionManagement.PatientId),obj);
                string TransactionId = string.Empty;
                string strMessage = string.Empty;
                var isDone = ZPPaymentBAL.SaleTransaction(obj, out TransactionId, out strMessage);
                if (isDone)
                {
                    TempData["strDone"] = isDone;
                }
                else
                {
                    TempData["strErrorMsg"] = strMessage;
                }
                TempData["transactionId"] = TransactionId;
                ViewBag.doctorList = ZPPaymentBAL.GetDoctorList(Convert.ToInt32(SessionManagement.PatientId));
                return RedirectToAction("MakeTransaction");
            }
            else
            {
                ViewBag.doctorList = ZPPaymentBAL.GetDoctorList(Convert.ToInt32(SessionManagement.PatientId));//NTD for this
                return View("MakeTransaction", obj);
            }
        }

        /// <summary>
        /// This Method Get Dentist profile data from data.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [CustomAuthorizeAttribute]
        public ActionResult GetDentistProfileData(int UserId)
        {
            DentistProfileBLL objdentistprofile = new DentistProfileBLL();
            DentistProfileDetail objProfileDetails = new DentistProfileDetail();
            objProfileDetails = objdentistprofile.DoctorPublicProfile(UserId);
            string imagename = objProfileDetails.ImageName;
            if (imagename.IndexOf("Profile_No_Image.png") > 0)
            {
                objProfileDetails.ImageName = "";
            }
            return View("_PartialProfileData", objProfileDetails);
        }

        /// <summary>
        /// This method remove patient profile token form database.
        /// </summary>
        /// <param name="PPTId"></param>
        /// <returns></returns>
        [CustomAuthorizeAttribute]
        public JsonResult RemovePatientPaymentToken(int PPTId)
        {
            bool result = ZPPaymentBAL.RemovePatientPaymentToken(PPTId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetZiftPayDoctorForPatient(int PatientId)
        {
            bool NotZift = false;
            NotZift = ZPPaymentBAL.GetZiftPayDoctorForPatient(PatientId);
            return Json(NotZift, JsonRequestBehavior.AllowGet);
        }
    }
}