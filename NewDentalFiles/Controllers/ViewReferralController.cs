﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Data;
using DentalFiles.Models;
using JQueryDataTables.Models.Repository;
using DentalFiles.Models.ViewModel;
using System.Collections;
using DentalFiles.App_Start;

namespace DentalFiles.Controllers
{
    public class ViewReferralController : Controller
    {
        ArrayList regarding = new ArrayList();
        ArrayList regardlinglist = new ArrayList();
        ArrayList requestinglist = new ArrayList();
        ArrayList requesting = new ArrayList();
        string[] splitregard; string[] splitrequest;
        ArrayList teethlist = new ArrayList();
        CommonDAL objCommonDAL = new CommonDAL();
        DataRepository objRepository = new DataRepository();
        public ActionResult Index()
        {

            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0 && Request.QueryString["ReferralId"] != null && Convert.ToString(Request.QueryString["ReferralId"]) != "")
            {
                objCommonDAL.GetActiveCompany();
                List<PatientDetails> PatientDetialList = objRepository.GetPatientDetails(Convert.ToInt32(SessionManagement.PatientId), SessionManagement.TimeZoneSystemName);
                if (PatientDetialList != null && PatientDetialList.Count != 0)
                {
                    ViewBag.FirstName = PatientDetialList[0].FirstName;
                    ViewBag.ImageURL = PatientDetialList[0].ProfileImage;
                }
                ViewBag.Title = "View Referral";
                ViewBag.ReturnUrl = "ViewReferral";
                var model = new ViewReferralMaster();

                DataTable toothdetails = objCommonDAL.getReferCardDetailsById(Convert.ToInt32(Request.QueryString["ReferralId"]));
                addTeethMap(toothdetails);
                Splitmethods(toothdetails);
                model.ViewReferral = objRepository.getReferCardDetailsByIdSingle(Convert.ToInt32(Request.QueryString["ReferralId"]));
                model.PatientUploadedImages = objRepository.GetPatientMontages(Convert.ToInt32(Request.QueryString["ReferralId"]));
                model.Notes = objRepository.getPatientNotes(Convert.ToInt32(SessionManagement.PatientId), Convert.ToInt32(Request.QueryString["ReferralId"]));
                string comments = "";
                foreach (var Referral in model.ViewReferral)
                {

                    if (Referral.Comments.ToString() != "" && comments != Referral.Comments)
                    {
                        comments = Referral.Comments;
                        ViewBag.lblcomments += Referral.Comments;
                    }
                }
                Messages objmsg = new Messages();
                DataTable dt = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0,1,5);
                if (dt != null && dt.Rows.Count > 0)
                {
                    @ViewBag.MsgCount = dt.Rows[0]["TotalRecord"];
                }
                else
                {
                    @ViewBag.MsgCount = 0;
                }

                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Index");
            }
        }

        public void addTeethMap(DataTable teethMapSet)
        {
            try
            {

                DataTable teethMapTable = teethMapSet;

                @ViewBag.div1 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div2 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div3 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div4 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div5 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div6 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div7 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div8 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div9 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div10 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div11 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div12 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div13 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div14 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div15 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div16 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div17 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div18 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div19 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div20 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div21 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div22 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div23 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div24 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div25 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div26 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div27 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div28 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div29 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div30 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div31 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div32 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div33 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div34 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div35 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div36 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div37 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div38 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div39 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div40 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div41 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div42 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div43 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div44 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div45 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div46 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div47 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div48 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div49 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div50 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div51 = "/DentalImages/seprat_teeth_blue.jpg";
                @ViewBag.div52 = "/DentalImages/seprat_teeth_blue.jpg";

                foreach (DataRow Teeth in teethMapTable.Rows)
                {
                    string val = Teeth["ToothType"].ToString();
                    #region Permenant Tooth
                    if (val == "1")
                    {
                        @ViewBag.div1 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "2")
                    {

                        @ViewBag.div2 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "3")
                    {

                        @ViewBag.div3 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "4")
                    {

                        @ViewBag.div4 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "5")
                    {

                        @ViewBag.div5 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "6")
                    {

                        @ViewBag.div6 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "7")
                    {

                        @ViewBag.div7 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "8")
                    {

                        @ViewBag.div8 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "9")
                    {

                        @ViewBag.div9 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "10")
                    {

                        @ViewBag.div10 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "11")
                    {

                        @ViewBag.div11 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "12")
                    {

                        @ViewBag.div12 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "13")
                    {

                        @ViewBag.div13 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "14")
                    {

                        @ViewBag.div14 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "15")
                    {

                        @ViewBag.div15 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "16")
                    {

                        @ViewBag.div16 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "17")
                    {

                        @ViewBag.div17 = "/DentalImages/seprat_teeth_black.jpg";

                    } if (val == "18")
                    {

                        @ViewBag.div18 = "/DentalImages/seprat_teeth_black.jpg";

                    } if (val == "19")
                    {

                        @ViewBag.div19 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "20")
                    {

                        @ViewBag.div20 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "21")
                    {

                        @ViewBag.div21 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "22")
                    {

                        @ViewBag.div22 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "23")
                    {

                        @ViewBag.div23 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "24")
                    {

                        @ViewBag.div24 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "25")
                    {

                        @ViewBag.div25 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "26")
                    {

                        @ViewBag.div26 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "27")
                    {

                        @ViewBag.div27 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "28")
                    {

                        @ViewBag.div28 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "29")
                    {

                        @ViewBag.div29 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "30")
                    {

                        @ViewBag.div30 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "31")
                    {

                        @ViewBag.div31 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "32")
                    {

                        @ViewBag.div32 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "33")
                    {

                        @ViewBag.div33 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "34")
                    {

                        @ViewBag.div34 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "35")
                    {

                        @ViewBag.div35 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "36")
                    {

                        @ViewBag.div36 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "37")
                    {

                        @ViewBag.div37 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "38")
                    {

                        @ViewBag.div38 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "39")
                    {

                        @ViewBag.div39 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "40")
                    {

                        @ViewBag.div40 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "41")
                    {

                        @ViewBag.div41 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "42")
                    {

                        @ViewBag.div42 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "43")
                    {

                        @ViewBag.div43 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "44")
                    {

                        @ViewBag.div44 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "45")
                    {

                        @ViewBag.div45 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "46")
                    {

                        @ViewBag.div46 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "47")
                    {

                        @ViewBag.div47 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "48")
                    {

                        @ViewBag.div48 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "49")
                    {

                        @ViewBag.div49 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "50")
                    {

                        @ViewBag.div50 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "51")
                    {

                        @ViewBag.div51 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "52")
                    {

                        @ViewBag.div52 = "/DentalImages/seprat_teeth_black.jpg";

                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void Splitmethods(DataTable toothdetail)
        {
            try
            {
                #region RegardingBind
                foreach (DataRow Tooth in toothdetail.Rows)
                {
                    string regard = Tooth["RegardOption"].ToString();

                    if (regard != "")
                    {
                        splitregard = regard.Split(',');

                        foreach (string strreg in splitregard)
                        {
                            if (!regarding.Contains(strreg))
                            {
                                if (strreg != "")
                                    regarding.Add(strreg);

                            }
                        }

                    }
                }
                foreach (var regcount in regarding)
                {

                    {
                        if (regcount.ToString() == "1")
                        {
                            if (!regarding.Contains(RegardingOption.Orthodontics.ToString()))
                            {
                                regardlinglist.Add(RegardingOption.Orthodontics.ToString());


                            }

                        }
                        if (regcount.ToString() == "2")
                        {
                            if (!regarding.Contains(RegardingOption.Periodontist.ToString()))
                            {
                                regardlinglist.Add(RegardingOption.Periodontist.ToString());
                            }
                        }
                        if (regcount.ToString() == "3")
                        {
                            if (!regarding.Contains(RegardingOption.OralSurgery.ToString()))
                            {
                                regardlinglist.Add("Oral Surgery");
                            }
                        }
                        if (regcount.ToString() == "4")
                        {
                            if (!regarding.Contains(RegardingOption.Prosthodontics.ToString()))
                            {
                                regardlinglist.Add(RegardingOption.Prosthodontics.ToString());
                            }
                        }
                        if (regcount.ToString() == "5")
                        {
                            if (!regarding.Contains(RegardingOption.Radiology.ToString()))
                            {
                                regardlinglist.Add(RegardingOption.Radiology.ToString());
                            }
                        }
                        if (regcount.ToString() == "6")
                        {
                            if (!regarding.Contains(RegardingOption.GeneralDentistry.ToString()))

                                regardlinglist.Add("General Dentistry");
                        }
                        if (regcount.ToString() == "7")
                        {
                            if (!regarding.Contains(RegardingOption.Endodontics.ToString()))
                            {
                                regardlinglist.Add(RegardingOption.Endodontics.ToString());
                            }
                        }
                        if (regcount.ToString() == "9")
                        {
                            if (!regarding.Contains(RegardingOption.LabWork.ToString()))
                            {
                                regardlinglist.Add(RegardingOption.LabWork.ToString());
                            }
                        }
                        string prevothercomments = "";
                        if (regcount.ToString() == "8")
                        {

                            foreach (DataRow othercount in toothdetail.Rows)
                            {
                                if (!regarding.Contains(othercount["OtherComments"].ToString()))
                                {
                                    if (othercount["OtherComments"].ToString() != "" && prevothercomments != othercount["OtherComments"].ToString())
                                    {
                                        prevothercomments = othercount["OtherComments"].ToString();
                                        regardlinglist.Add(othercount["OtherComments"].ToString());
                                    }

                                }
                            }
                        }

                    }
                }
                #endregion
                #region RequestBind

                foreach (DataRow Tooth in toothdetail.Rows)
                {
                    string request = Tooth["RequestingOption"].ToString();

                    if (request != "")
                    {
                        splitrequest = request.Split(',');
                        foreach (string strreq in splitrequest)
                        {
                            if (!requesting.Contains(strreq))
                            {
                                if (strreq != "")
                                    requesting.Add(strreq);

                            }
                        }
                    }
                }


                foreach (var reqcount in requesting)
                {

                    {
                        if (reqcount.ToString() == "1")
                        {
                            if (!requesting.Contains(RequestOption.Checkfor.ToString()))
                            {
                                requestinglist.Add("Check for");

                            }

                        }
                        if (reqcount.ToString() == "2")
                        {
                            if (!requesting.Contains(RequestOption.CheckPeriodontolCindition.ToString()))
                            {
                                requestinglist.Add("Check PeriodontolCindition");
                            }
                        }
                        if (reqcount.ToString() == "3")
                        {
                            if (!requesting.Contains(RequestOption.OralSurgery.ToString()))
                            {
                                requestinglist.Add("Oral Surgery");
                            }
                        }

                        if (reqcount.ToString() == "5")
                        {
                            if (!requesting.Contains(RequestOption.Extractions.ToString()))
                            {
                                requestinglist.Add(RequestOption.Extractions.ToString());
                            }
                        }
                        if (reqcount.ToString() == "6")
                        {
                            if (!requesting.Contains(RequestOption.OrthodonticConsultation.ToString()))

                                requestinglist.Add("Orthodontic Consultation");
                        }
                        if (reqcount.ToString() == "7")
                        {
                            if (!requesting.Contains(RequestOption.OrthognathicSurgery.ToString()))
                            {

                                requestinglist.Add("Orthognathic Surgery");
                            }
                        }
                        if (reqcount.ToString() == "8")
                        {
                            if (!requesting.Contains(RequestOption.Implants.ToString()))
                            {
                                requestinglist.Add(RequestOption.Implants.ToString());
                            }
                        }
                        if (reqcount.ToString() == "9")
                        {
                            if (!requesting.Contains(RequestOption.Pathology.ToString()))
                            {
                                requestinglist.Add(RequestOption.Pathology.ToString());
                            }

                        }
                        string Prev_Othercomments = "";
                        if (reqcount.ToString() == "10")
                        {
                            foreach (DataRow othercount in toothdetail.Rows)
                            {
                                if (!regarding.Contains(othercount["RequestComments"].ToString()))
                                {
                                    if (othercount["RequestComments"].ToString() != "" && Prev_Othercomments != othercount["RequestComments"].ToString())
                                    {
                                        Prev_Othercomments = othercount["RequestComments"].ToString();
                                        requestinglist.Add(othercount["RequestComments"].ToString());
                                    }

                                }
                            }
                        }
                    }
                }



                #endregion
                ViewBag.regardlinglist = regardlinglist;
                ViewBag.requestinglist = requestinglist;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

    }
}
