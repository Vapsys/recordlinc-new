﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using DentalFiles.Models;
using System.Configuration;
using JQueryDataTables.Models.Repository;
using DentalFiles.Models.ViewModel;
using System.IO;

using DentalFiles.App_Start;
using System.Text;
using Ionic.Zip;
using DentalFilesNew.App_Start;

namespace DentalFiles.Controllers
{
    public class DentalFilesController : Controller
    {
        //
        // GET: /DentalFiles/
        int PatientId = 0;
        DataTable getFilenames = new DataTable();
        CommonDAL objCommonDAL = new CommonDAL();
        DataRepository objRepository = new DataRepository();
        [CustomAuthorize]
        public ActionResult Index()
        {

            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                ViewBag.PatientId = PatientId = Convert.ToInt32(SessionManagement.PatientId);
                ViewBag.Title = "My Dental Files";
                objCommonDAL.GetActiveCompany();
                ViewBag.ReturnUrl = "DentalFiles";

                var model = new PatientAndUploadFiles();
                model.PatientDetails = objRepository.GetPatientDetails(PatientId, SessionManagement.TimeZoneSystemName);
                model.PatientUploadedImages = objRepository.PatientUploadedImages(PatientId, SessionManagement.TimeZoneSystemName);
                model.PatientUploadedFiles = objRepository.PatientUploadedFiles(PatientId, SessionManagement.TimeZoneSystemName);
                if (model.PatientDetails.Count != 0)
                {
                    ViewBag.FirstName = model.PatientDetails[0].FirstName.ToString();
                    if (!string.IsNullOrEmpty(model.PatientDetails[0].ProfileImage.ToString()))
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["imagebankpath"] + model.PatientDetails[0].ProfileImage.ToString();
                    }
                    else if (model.PatientDetails[0].Gender == 1)
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"];
                    }
                    else
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["Doctorimage1"];

                    }
                }
                Messages objmsg = new Messages();
                DataTable dt = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0, 1, 5);
                DataTable dtCount = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0, 1, 100000);
                int count = 0;
                foreach (DataRow Count in dtCount.Rows)
                {
                    if (!Convert.ToBoolean(Count["Read_flag"]))
                    {
                        count = count + 1;
                    }
                }
                @ViewBag.MsgCount = count;
                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Index");
            }
        }


        [HttpGet]
        public ActionResult GetAllFileZip(string ReturnUrl)
        {
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                PatientId = Convert.ToInt32(SessionManagement.PatientId);
                DataTable dtMontages = objCommonDAL.GetPatientMontages(PatientId);
                DataTable getFilenames = new DataTable();

                getFilenames.Columns.Add("FileName", System.Type.GetType("System.String"));
                if (dtMontages != null && dtMontages.Rows.Count > 0)
                {
                    foreach (DataRow item in dtMontages.Rows)
                    {
                        DataRow dr = getFilenames.NewRow();
                        dr["FileName"] = ConfigurationManager.AppSettings.Get("imagebankpath") + item["Name"].ToString();
                        getFilenames.Rows.Add(dr);
                    }
                }

                DataTable documentDetail = objCommonDAL.GetPatientDocs(PatientId);
                if (documentDetail != null && documentDetail.Rows.Count > 0)
                {
                    foreach (DataRow item in documentDetail.Rows)
                    {
                        DataRow dr = getFilenames.NewRow();
                        dr["FileName"] = item["DocumentName"].ToString();
                        getFilenames.Rows.Add(dr);
                    }
                }
                if (getFilenames != null && getFilenames.Rows.Count > 0)
                {

                    Session.Add("getFilenames", getFilenames);

                    ZipAllFiles();
                }
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index", "Index");
            }
        }

        private void ZipAllFiles()
        {



            byte[] buffer = new byte[4096];
            // the path on the server where the temp file will be created! 
            string tempfolder = ConfigurationManager.AppSettings.Get("tempfolder");
            var tempFileName = tempfolder + "\\" + Guid.NewGuid().ToString() + ".zip";

            var filePath = String.Empty;
            var fileName = String.Empty;


            getFilenames = Session["getFilenames"] as DataTable;
            foreach (DataRow item in getFilenames.Rows)
            {
                if (item["FileName"].ToString() != "")
                {
                    fileName = item["FileName"].ToString();
                    filePath = Server.MapPath(fileName);


                    FileInfo fi = new FileInfo(filePath);
                    string PF_Name = String.Empty;
                    if (fi.Exists)
                    {


                        String[] History_Pat = fileName.Split('$');
                        if (History_Pat[0].Length > 0)
                        {
                            if (History_Pat[0].Equals(ConfigurationManager.AppSettings.Get("imagebankpath")))
                            {
                                PF_Name = "../Patient_Documents/" + History_Pat[History_Pat.Length - 1];
                            }
                            else
                            {
                                PF_Name = fileName;
                            }
                        }
                        System.IO.File.Copy(filePath, tempfolder + "\\" + fi.Name, true);


                    }
                }
            }

            using (ZipFile zip = new ZipFile())
            {
                string[] files = Directory.GetFiles(tempfolder);
                // add all those files to the ProjectX folder in the zip file
                zip.AddFiles(files, PatientId.ToString());


                zip.Save(tempFileName);
            }

            Response.ContentType = "application/x-zip-compressed";
            Response.AppendHeader("Content-Disposition", "attachment; filename=Patient_" + PatientId + "_Records.zip");
            Response.WriteFile(tempFileName);
            Response.Flush();
            Response.Close();

            // delete the temp file  
            if (System.IO.File.Exists(tempFileName))
                System.IO.File.Delete(tempFileName);

           

            // delete the temp file  from tempzip folder
            foreach (DataRow item in getFilenames.Rows)
            {
                if (item["FileName"].ToString() != "")
                {
                    string Name = item["FileName"].ToString();
                    Name = Name.Replace("../ImageBank/", "");

                    string DeleteDoc = tempfolder + "\\" + Name;

                    FileInfo fi1 = new FileInfo(DeleteDoc);
                    if (fi1.Exists)
                    {
                        System.IO.File.Delete(tempfolder + "\\" + fi1.Name);
                    }
                }
            }

        }
        private void ZipAllFilesNew(string FileName)
        {



            byte[] buffer = new byte[4096];
            // the path on the server where the temp file will be created! 
            string tempfolder = ConfigurationManager.AppSettings.Get("tempfolder");
            var tempFileName = tempfolder + "\\" + Guid.NewGuid().ToString() + ".zip";

            var filePath = String.Empty;
            filePath = Server.MapPath(FileName);
            FileInfo fi = new FileInfo(filePath);
            string PF_Name = String.Empty;
            if (fi.Exists)
            {


                String[] History_Pat = FileName.Split('$');
                if (History_Pat[0].Length > 0)
                {
                    if (History_Pat[0].Equals(ConfigurationManager.AppSettings.Get("imagebankpath")))
                    {
                        PF_Name = "../Patient_Documents/" + History_Pat[History_Pat.Length - 1];
                    }
                    else
                    {
                        PF_Name = FileName;
                    }
                }
                System.IO.File.Copy(filePath, tempfolder + "\\" + fi.Name, true);


            }

            PatientId = Convert.ToInt32(SessionManagement.PatientId);
            using (ZipFile zip = new ZipFile())
            {
                string[] files = Directory.GetFiles(tempfolder);
                // add all those files to the ProjectX folder in the zip file
                zip.AddFiles(files, PatientId.ToString());


                zip.Save(tempFileName);
            }

            Response.ContentType = "application/x-zip-compressed";
            Response.AppendHeader("Content-Disposition", "attachment; filename=Patient_" + PatientId + "_Records.zip");
            Response.WriteFile(tempFileName);
            Response.Flush();
            Response.Close();

            // delete the temp file  
            if (System.IO.File.Exists(tempFileName))
                System.IO.File.Delete(tempFileName);

            // delete the temp file  from tempzip folder
           
                if (FileName != "")
                {
                    string Name = FileName.ToString();
                    Name = Name.Replace("../ImageBank/", "");

                    string DeleteDoc = tempfolder + "\\" + Name;

                    FileInfo fi1 = new FileInfo(DeleteDoc);
                    if (fi1.Exists)
                    {
                        System.IO.File.Delete(tempfolder + "\\" + fi1.Name);
                    }
                }
            

        }
        [HttpPost]
        public JsonResult GetDetailByImageId(string ImageId)
        {
            object obj = null;

            DataTable dt = objCommonDAL.GetDetailByImageId(Convert.ToInt32(ImageId));
            if (dt != null && dt.Rows.Count > 0)
            {
                obj = new
                {
                    Success = true,
                    ImageId = Convert.ToString(dt.Rows[0]["ImageId"]),
                    CreationDate = Convert.ToString(dt.Rows[0]["CreationDateNew"]),
                    LastModifiedDate = Convert.ToString(dt.Rows[0]["LastModifiedDateNew"]),
                    Notes = Convert.ToString(dt.Rows[0]["Description"]),

                };
            }


            return Json(obj, JsonRequestBehavior.DenyGet);


        }





        [HttpPost]
        public JsonResult GetDetailByMontageId(string MontageId)
        {
            object obj = null;

            DataTable dt = objCommonDAL.GetDescriptionofDocumentById(SessionManagement.PatientId, Convert.ToInt32(MontageId));
            if (dt != null && dt.Rows.Count > 0)
            {
                obj = new
                {
                    Success = true,
                    ImageId = Convert.ToString(dt.Rows[0]["DocumentId"]),
                    CreationDate = Convert.ToString(dt.Rows[0]["CreationDateNew"]),
                    LastModifiedDate = Convert.ToString(dt.Rows[0]["LastModifiedDateNew"]),
                    Notes = Convert.ToString(dt.Rows[0]["Description"]),
                };
            }


            return Json(obj, JsonRequestBehavior.DenyGet);


        }


        [HttpPost]
        public JsonResult DeleteImage(string ImageId, string MontageId, string RelativePath = null)
        {
            object obj = null;
            try
            {
                RelativePath = RelativePath.Replace("..","");
                int Delsucess = objCommonDAL.DeleteimageofPatient(Convert.ToInt32(ImageId), Convert.ToInt32(MontageId));
                if (Delsucess == 1)
                {
                    String path = Server.MapPath("~/" + RelativePath);
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            obj = new
            {
                Success = true,
                Message = "Image Removed successfully",

            };
            return Json(obj, JsonRequestBehavior.DenyGet);


        }




        [HttpPost]
        public JsonResult Deleteattach(string DocumentName)
        {
            object obj = null;
            try
            {
                int Delsucess = objCommonDAL.DeletefileofPatientNew("", Convert.ToInt32(DocumentName));

            }
            catch (Exception ex)
            {
                throw ex;
            }

            obj = new
            {
                Success = true,
                Message = "File Removed successfully",

            };
            return Json(obj, JsonRequestBehavior.DenyGet);


        }



        public ActionResult Downloadattach(string filename)
        {

            filename = filename.Replace("/", "");
            
            filename = "../ImageBank/" + filename;
            filename = filename.Replace("_Dollar_", "$");
            filename = filename.Replace("_Dot_", ".");
            //File Path and File Name
            ZipAllFilesNew(filename);
            //string filePath = Server.MapPath("~/ImageBank");
            //string _DownloadableProductFileName = filename;

            //System.IO.FileInfo FileName = new System.IO.FileInfo(filePath + "\\" + _DownloadableProductFileName);
            //if (FileName.Exists)
            //{
            //    FileStream myFile = new FileStream(filePath + "\\" + _DownloadableProductFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

            //    //Reads file as binary values
            //    BinaryReader _BinaryReader = new BinaryReader(myFile);

            //    //Ckeck whether user is eligible to download the file if (IsEligibleUser())
            //    if (FileName.Exists)
            //    {
            //        //Check whether file exists in specified location
            //        if (FileName.Exists)
            //        {
            //            try
            //            {
            //                string Patient_File = FileName.Name;
            //                String[] FileSplit = Patient_File.Split('$');
            //                string File_Name;
            //                if (FileSplit[0] == "")
            //                {
            //                    File_Name = FileSplit[2];
            //                }
            //                else
            //                {
            //                    File_Name = Patient_File;
            //                }

            //                long startBytes = 0;
            //                string lastUpdateTiemStamp = System.IO.File.GetLastWriteTimeUtc(filePath).ToString("r");
            //                string _EncodedData = HttpUtility.UrlEncode(File_Name.Replace(" ", ""), Encoding.UTF8) + lastUpdateTiemStamp;

            //                Response.Clear();
            //                Response.Buffer = false;
            //                Response.AddHeader("Accept-Ranges", "bytes");
            //                Response.AppendHeader("ETag", "\"" + _EncodedData + "\"");
            //                Response.AppendHeader("Last-Modified", lastUpdateTiemStamp);
            //                Response.ContentType = "application/octet-stream";
            //                Response.AddHeader("Content-Disposition", "attachment;filename=" + File_Name.Replace(" ", ""));
            //                Response.AddHeader("Content-Length", (File_Name.Replace(" ", "").Length - startBytes).ToString());
            //                Response.AddHeader("Connection", "Keep-Alive");
            //                Response.ContentEncoding = Encoding.UTF8;

            //                //Send data
            //                _BinaryReader.BaseStream.Seek(startBytes, SeekOrigin.Begin);

            //                //Dividing the data in 1024 bytes package
            //                int maxCount = (int)Math.Ceiling((File_Name.Replace(" ", "").Length - startBytes + 0.0) / 1024);

            //                //Download in block of 1024 bytes
            //                int i;
            //                for (i = 0; i < maxCount && Response.IsClientConnected; i++)
            //                {
            //                    Response.BinaryWrite(_BinaryReader.ReadBytes(1024));
            //                    Response.Flush();
            //                }


            //            }
            //            catch
            //            {

            //            }
            //            finally
            //            {

            //                _BinaryReader.Close();
            //                myFile.Close();
            //                Response.End();
            //            }
            //        }

            //    }
            //    else
            //    {

            //    }
            //}
            //else
            //{

            //}
            return RedirectToAction("Index");

        }


        [HttpPost]
        public JsonResult UpdateDescriptionofImage(int ImageId, DateTime CreationDate, DateTime LastModifiedDate, string Notes)
        {
            object obj = null;
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                bool Result = objCommonDAL.UpdateDescriptionofImageFromMyDental(ImageId, CreationDate, LastModifiedDate, Notes, 0);



                obj = new
                {
                    Success = true,
                    Message = "Description  Updated successfully",
                    ImageId = ImageId,
                    CreationDate = CreationDate.ToString("MM-dd-yy"),
                    LastModifiedDate = LastModifiedDate.ToString("MM-dd-yy"),
                    Notes = Notes,

                };
                return Json(obj, JsonRequestBehavior.DenyGet);
            }
            else
            {
                return Json(new
                {
                    redirectUrl = Url.Action("Index", "Index"),
                    isRedirect = true
                });
            }
        }


        [HttpPost]
        public JsonResult UpdateDescriptionofDocument(int DocMontageId, DateTime CreationDate, DateTime LastModifiedDate, string Notes)
        {
            object obj = null;
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {

                bool Result = objCommonDAL.UpdateDescriptionofDocumentFromMyDental(DocMontageId, CreationDate, LastModifiedDate, Notes, 0);

                obj = new
                {
                    Success = true,
                    Message = "Description  Updated successfully",
                    DocMontageId = DocMontageId,
                    CreationDate = CreationDate.ToString("MM-dd-yy"),
                    LastModifiedDate = LastModifiedDate.ToString("MM-dd-yy"),
                    Notes = Notes,

                };
                return Json(obj, JsonRequestBehavior.DenyGet);
            }
            else
            {
                return Json(new
                {
                    redirectUrl = Url.Action("Index", "Index"),
                    isRedirect = true
                });
            }
        }





    }
}