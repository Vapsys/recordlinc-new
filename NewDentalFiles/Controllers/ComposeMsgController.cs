﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using DentalFiles.Models;
using System.Configuration;
using JQueryDataTables.Models.Repository;
using DentalFiles.Models.ViewModel;
using System.IO;

using DentalFiles.App_Start;
using System.Text;
using System.Drawing;
using System.Web.Script.Serialization;
using DataAccessLayer.Common;

namespace DentalFiles.Controllers
{
    public class ComposeMsgController : Controller
    {
        //
        // GET: /ComposeMsg/
        int PatientId = 0;
        DataTable getFilenames = new DataTable();
        CommonDAL objCommonDAL = new CommonDAL();
        DataRepository objRepository = new DataRepository();
        string viewstatvalue = null;



        public ActionResult Index()
        {
            var model = new PatientAndUploadFiles();


            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {

                try
                {
                    if (Convert.ToInt32(Request.QueryString["PatientmessageId"]) > 0 && Session["ForwordFlag"] == null)
                    {
                        objCommonDAL = new CommonDAL();

                        objCommonDAL.DeleteImagesUploded(Convert.ToInt32(SessionManagement.PatientId));
                        objCommonDAL.DeleteFilesUploaded(Convert.ToInt32(SessionManagement.PatientId));

                        DataTable tempdt = objCommonDAL.GetMessageAttachmentByPatientmessageId(Convert.ToInt32(Request.QueryString["PatientmessageId"]));
                        
                        foreach (DataRow item in tempdt.Rows)
                        {
                            ReCreateAttachment(item["AttachmentName"].ToString());
                        }
                        Session["ForwordFlag"] = true;
                    }
                }
                catch (Exception)
                {

                }
                if (Request.RawUrl != null && Request.RawUrl != "" && (Convert.ToString(Request.RawUrl).Contains("?fromstatus=fu") || Convert.ToString(Request.RawUrl).Contains("&fromstatus=fu")))
                {
                    if (Session["viewstatvalue"] == null || Convert.ToString(Session["viewstatvalue"]) == "")
                    {

                    }

                    viewstatvalue = Convert.ToString(Session["viewstatvalue"]);


                }
                else
                {
                    if (Session["ForwordFlag"] == null)
                    {
                        objCommonDAL.DeleteImagesUploded(Convert.ToInt32(SessionManagement.PatientId));
                        objCommonDAL.DeleteFilesUploaded(Convert.ToInt32(SessionManagement.PatientId));
                        Session["viewstatvalue"] = null;
                    }

                }
                objCommonDAL.GetActiveCompany();
                ViewBag.PatientId = PatientId = Convert.ToInt32(SessionManagement.PatientId);
                ViewBag.Title = "Compose Message";

                ViewBag.ReturnUrl = "ComposeMsg";



                List<PatientDetails> PatientDetialList = new List<PatientDetails>();
                PatientDetialList = objRepository.GetPatientDetails(PatientId, SessionManagement.TimeZoneSystemName);
                if (PatientDetialList.Count != 0)
                {
                    ViewBag.FirstName = PatientDetialList[0].FirstName.ToString();
                    if (!string.IsNullOrEmpty(PatientDetialList[0].ProfileImage.ToString()))
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["imagebankpath"] + PatientDetialList[0].ProfileImage.ToString();
                    }
                    else if (PatientDetialList[0].Gender == 1)
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"];

                    }
                    else
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["Doctorimage1"];
                    }
                    Session["PatientFullName"] = PatientDetialList[0].FirstName.ToString() + " " + PatientDetialList[0].LastName.ToString();
                }

                model.PatientUploadedImages = objRepository.PatientUploadedImagesTemp(PatientId, clsCommon.GetUniquenumberForAttachments());
                model.PatientUploadedFiles = objRepository.PatientUploadedFilesTemp(PatientId, clsCommon.GetUniquenumberForAttachments());

                ViewBag.countatt = model.PatientUploadedImages.Count + model.PatientUploadedFiles.Count;

                Messages objmsg = new Messages();
                DataTable dt = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0, 1, 5);
                DataTable dtCount = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0, 1, 100000);
                int count = 0;
                foreach (DataRow Count in dtCount.Rows)
                {
                    if (!Convert.ToBoolean(Count["Read_flag"]))
                    {
                        count = count + 1;
                    }
                }
                @ViewBag.MsgCount = count;


                Session["mode"] = "True";


                if (Request.QueryString["PatientmessageId"] != null && Convert.ToString(Request.QueryString["PatientmessageId"]) != "" && Request.QueryString["type"] != null && Convert.ToString(Request.QueryString["type"]) != "" && Request.QueryString["fromstatus"] == null && Convert.ToString(Request.QueryString["fromstatus"]) != "fu")
                {

                    model.Message = objRepository.GetMessagesById(Convert.ToInt32(Request.QueryString["PatientmessageId"]));

                    HttpCookie myCookie = new HttpCookie("subject");
                    myCookie.Value = model.Message[0].Subject;
                    myCookie.Expires = DateTime.Now.AddDays(1d);
                    Response.Cookies.Add(myCookie);

                    HttpCookie myCookie1 = new HttpCookie("Message");
                    myCookie1.Value = model.Message[0].Body;
                    myCookie1.Expires = DateTime.Now.AddDays(1d);
                    Response.Cookies.Add(myCookie1);

                }

                else if (Request.QueryString["DoctorId"] != null && Convert.ToString(Request.QueryString["DoctorId"]) != "")
                {
                    TempData["UserId"] = Request.QueryString["DoctorId"];
                    Messages objM = new Messages();
                    List<Messages> objMessages = new List<Messages>();
                    objMessages.Add(objM);
                    model.Message = objMessages;

                }
                else
                {
                    Messages objM = new Messages();
                    List<Messages> objMessages = new List<Messages>();
                    objMessages.Add(objM);
                    model.Message = objMessages;
                   // model.Message = objRepository.GetMessagesById(0);

                }




                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Index");
            }
        }


        public void ReCreateAttachment(string DocumentName)
        {

            string viewstatvalue = null;
            int userid = Convert.ToInt32(Session["PatientId"].ToString());

            if (Session["viewstatvalue"] == null || Convert.ToString(Session["viewstatvalue"]) == "")
            {
                Session["viewstatvalue"] = Guid.NewGuid().ToString();
            }
            viewstatvalue = Convert.ToString(Session["viewstatvalue"]);

            string Sesionid = Session["PatientId"].ToString();




            string tempPath = System.Configuration.ConfigurationManager.AppSettings["imagebankpath"];

            string destinationFile = Server.MapPath(tempPath.Replace("..", ""));
            destinationFile = destinationFile.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");
            string sourceFile = destinationFile + DocumentName;








            string filename1 = System.Configuration.ConfigurationManager.AppSettings.Get("TempUploads") + "$" + System.DateTime.Now.Ticks + "$" + DocumentName;

            string extension;

            extension = Path.GetExtension(DocumentName);


            if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd" && extension.ToLower() != ".pspimage" && extension.ToLower() != ".thm" && extension.ToLower() != ".tif")
            {
                objCommonDAL.InsertFiles(filename1, Convert.ToInt32(Sesionid.Split('?')[0]), viewstatvalue);

            }
            else
            {
                objCommonDAL.InsertImages(filename1, Convert.ToInt32(Sesionid.Split('?')[0]), viewstatvalue);


            }









            string savepath = "";
            string tempPath1 = System.Configuration.ConfigurationManager.AppSettings["TempUploadsFolderPath"].ToString();
            savepath = Server.MapPath(tempPath1);
            savepath = savepath.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");
            string newpath = Path.Combine(savepath + filename1);



            System.IO.File.Copy(sourceFile, Path.Combine(savepath + filename1));


        }


        [HttpPost]
        [ValidateInput(false)]
        public JsonResult SendMsg(string SelectedUserId, string UserName, string txtsubject, string txtmsg, string sendtoall)
        {
            object obj = null;
            int id = 0;
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                try
                {
                    string PatientFullName = "";
                    txtmsg = HttpUtility.UrlDecode(txtmsg);
                    List<PatientDetails> PatientDetialList = new List<PatientDetails>();
                    PatientDetialList = objRepository.GetPatientDetails(SessionManagement.PatientId, SessionManagement.TimeZoneSystemName);
                    if (PatientDetialList.Count != 0)
                    {
                        PatientFullName = PatientDetialList[0].FirstName.ToString() + " " + PatientDetialList[0].LastName.ToString();
                    }

                    string[] arrUserid = SelectedUserId.Split(',');
                    #region  code
                    foreach (string singleuserid in arrUserid)
                    {
                        if (singleuserid != null && Convert.ToInt32(singleuserid) != 0)
                        {
                            if (UserName == null || UserName == "")
                            {
                                DataTable dtDoctorDetails = objCommonDAL.GetMemberProfile(Convert.ToInt32(singleuserid));
                                if (dtDoctorDetails != null && dtDoctorDetails.Rows.Count > 0)
                                {
                                    UserName = objCommonDAL.CheckNull(Convert.ToString(dtDoctorDetails.Rows[0]["patientname"]), null);
                                }
                            }



                            id = objCommonDAL.Insert_PatientMessages(PatientFullName, UserName, txtsubject, txtmsg, Convert.ToInt32(singleuserid), Convert.ToInt32(SessionManagement.PatientId), false, false, true);
                            if (id > 0 && Convert.ToBoolean(sendtoall) == true)
                            {
                                if (Session["viewstatvalue"] != null || Convert.ToString(Session["viewstatvalue"]) != "")
                                {

                                    viewstatvalue = clsCommon.GetUniquenumberForAttachments();
                                }

                                DataTable dtfileupload = objCommonDAL.GetRecordsFromTempTables(Convert.ToInt32(SessionManagement.PatientId), viewstatvalue);
                                if (dtfileupload != null && dtfileupload.Rows.Count > 0)
                                {
                                   
                                    foreach (DataRow item in dtfileupload.Rows)
                                    {
                                        string DocName = Convert.ToString(item["DocumentName"]);
                                        DocName = DocName.Substring(DocName.ToString().LastIndexOf("$") + 1);
                                        string NewFileName = Convert.ToString("$" + System.DateTime.Now.Ticks + "$" + DocName);
                                        string Extension = Path.GetExtension(NewFileName);
                                        DocName = Convert.ToString(item["DocumentName"]);
                                        DocName = DocName.Replace("../TempUploads/", "");

                                        string sourceFile = Server.MapPath(ConfigurationManager.AppSettings.Get("TempUploadsFolderPath")) + DocName;

                                        sourceFile = sourceFile.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");
                                        string tempPath = System.Configuration.ConfigurationManager.AppSettings["imagebankpath"];
                                        string destinationFile = Server.MapPath(tempPath);
                                        destinationFile = destinationFile.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");
                                        string DestinationFilePath = destinationFile;
                                        destinationFile = destinationFile + NewFileName;


                                        if (System.IO.File.Exists(sourceFile))
                                        {
                                            System.IO.File.Copy(sourceFile, destinationFile);

                                            string MontageName = Convert.ToInt32(SessionManagement.PatientId) + "Montage";
                                            int montageid = Convert.ToInt32(objCommonDAL.AddMontage(MontageName, 0, Convert.ToInt32(SessionManagement.PatientId), Convert.ToInt32(singleuserid), 0));

                                            if (Extension.ToLower() == ".jpg" || Extension.ToLower() == ".jpeg" || Extension.ToLower() == ".gif" || Extension.ToLower() == ".png" || Extension.ToLower() == ".bmp" || Extension.ToLower() == ".psd" || Extension.ToLower() == ".pspimage" || Extension.ToLower() == ".thm" || Extension.ToLower() == ".tif")
                                            {
                                                string ThubimagePath = Server.MapPath("~/" + System.Configuration.ConfigurationManager.AppSettings["FolderPathThumb"]);

                                                objCommonDAL.GenerateThumbnailImage(DestinationFilePath, NewFileName, ThubimagePath);

                                                int imageid = objCommonDAL.AddImagetoPatient(Convert.ToInt32(SessionManagement.PatientId), 1, NewFileName, 1000, 1000, 0, 500, sourceFile, tempPath + NewFileName, Convert.ToInt32(singleuserid), 0, 1, "Patient");
                                                if (imageid > 0)
                                                {

                                                    bool resmonimg = objCommonDAL.AddImagetoMontage(imageid, 500, montageid);
                                                }
                                            }
                                            else
                                            {
                                                int Docstatus = objCommonDAL.AddDocumenttoMontage(montageid, NewFileName, "Patient");
                                            }

                                        }


                                        bool Attach = objCommonDAL.InsertMessageAttachemnts(id, NewFileName);

                                    }



                                }

                                objCommonDAL.ToInviteMessageMail(Convert.ToInt32(SessionManagement.PatientId), Convert.ToInt32(singleuserid), id);
                            }
                            UserName = "";

                        }
                    }

                    #endregion





                    DataTable dtfileuploaddelete = objCommonDAL.GetRecordsFromTempTables(Convert.ToInt32(SessionManagement.PatientId), viewstatvalue);

                    foreach (DataRow item in dtfileuploaddelete.Rows)
                    {
                        string DocName = Convert.ToString(item["DocumentName"]);
                        DocName = DocName.Substring(DocName.ToString().LastIndexOf("$") + 1);

                        DocName = Convert.ToString(item["DocumentName"]);
                        DocName = DocName.Replace("../TempUploads/", "");

                        string sourceFile = Server.MapPath(ConfigurationManager.AppSettings.Get("TempUploadsFolderPath")) + DocName;

                        sourceFile = sourceFile.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");

                        if (System.IO.File.Exists(sourceFile))
                        {
                            System.IO.File.Delete(sourceFile);
                        }

                    }


                    objCommonDAL.DeleteImagesUploded(Convert.ToInt32(SessionManagement.PatientId));
                    objCommonDAL.DeleteFilesUploaded(Convert.ToInt32(SessionManagement.PatientId));
                }


                catch (Exception ex)
                {
                    throw ex;
                }

                obj = new
                {
                    Success = true,

                };

                Session["ForwordFlag"] = null;
                return Json(obj, JsonRequestBehavior.DenyGet);

            }
            else
            {
                Session["ForwordFlag"] = null;
                return Json(new
                {
                    redirectUrl = Url.Action("Index", "Index"),
                    isRedirect = true
                });

            }

        }




        public JsonResult GetUserList(string selected)
        {

            List<UserList> objUserList = new List<UserList>();
            objUserList = objRepository.GetTratingDoctors(Convert.ToInt32(SessionManagement.PatientId), 1, int.MaxValue);
            StringBuilder sb = new StringBuilder();

            sb.Append("<select id='ddlUserList' data-placeholder='Type doctor name here' class='chosen'  style='width:65%' multiple='multiple' >");

            foreach (var UserLst in objUserList)
            {
                if (selected.Split(',').Contains(UserLst.UserID.ToString()))
                {
                    sb.Append("<option selected='selected' value='" + UserLst.UserID + "'>" + UserLst.name + "</option>");
                }
                else
                {
                    sb.Append("<option value='" + UserLst.UserID + "'>" + UserLst.name + "</option>");
                }
            }
            sb.Append("</select>");
            return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAllUserList(string selected)
        {
            List<UserList> objUserList = new List<UserList>();
            objUserList = objRepository.GetDentistListBySearchDistinctName(selected, null, null, 0, 0, null, null, null, null, null, null, null, 1, 10, null, null);




            return Json(objUserList, JsonRequestBehavior.AllowGet);



        }

        [HttpPost]
        public JsonResult DeleteImage(string ImageId, string RelativePath)
        {
            object obj = null;
            try
            {
                RelativePath = RelativePath.Replace("_Dollar_", "$").Replace("_Dot_", ".");
                bool Delsucess = objCommonDAL.DeleteImagesUplodedByImageId(Convert.ToInt32(ImageId));
                if(Delsucess == true)
                {
                    String path = Server.MapPath("~/" + RelativePath);
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            obj = new
            {
                Success = true,
                Message = "Image Removed successfully",

            };
            return Json(obj, JsonRequestBehavior.DenyGet);


        }



        [HttpPost]
        public ActionResult btncancel()
        {

            try
            {
                objCommonDAL.DeleteImagesUploded(Convert.ToInt32(SessionManagement.PatientId));
                objCommonDAL.DeleteFilesUploaded(Convert.ToInt32(SessionManagement.PatientId));
            }
            catch (Exception ex)
            {
                throw ex;
            }


            return RedirectToAction("Index", "MyInbox");


        }


        [HttpPost]
        public JsonResult Deleteattach(string UploadedFileId, string DocumentName)
        {
            object obj = null;
            try
            {
                DocumentName = "/TempUploads/" + DocumentName.Replace("_Dollar_", "$").Replace("_Dot_", ".");
                bool Delsucess = objCommonDAL.DeleteFilesUplodedByFilesId(Convert.ToInt32(UploadedFileId));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            obj = new
            {
                Success = true,
                Message = "File Removed successfully",

            };
            return Json(obj, JsonRequestBehavior.DenyGet);


        }


        public ActionResult Downloadattach(string filename)
        {


            filename = filename.Replace("_Dollar_", "$");
            filename = filename.Replace("_Dot_", ".");
            //File Path and File Name
            string filePath = Server.MapPath("~/TempUploads");
            string _DownloadableProductFileName = filename;

            System.IO.FileInfo FileName = new System.IO.FileInfo(filePath + "\\" + _DownloadableProductFileName);
            if (FileName.Exists)
            {
                FileStream myFile = new FileStream(filePath + "\\" + _DownloadableProductFileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

                //Reads file as binary values
                BinaryReader _BinaryReader = new BinaryReader(myFile);

                //Ckeck whether user is eligible to download the file if (IsEligibleUser())
                if (FileName.Exists)
                {
                    //Check whether file exists in specified location
                    if (FileName.Exists)
                    {
                        try
                        {
                            string Patient_File = FileName.Name;
                            String[] FileSplit = Patient_File.Split('$');
                            string File_Name;
                            if (FileSplit[0] == "")
                            {
                                File_Name = FileSplit[2];
                            }
                            else
                            {
                                File_Name = Patient_File;
                            }

                            long startBytes = 0;
                            string lastUpdateTiemStamp = System.IO.File.GetLastWriteTimeUtc(filePath).ToString("r");
                            string _EncodedData = HttpUtility.UrlEncode(File_Name.Replace(" ", ""), Encoding.UTF8) + lastUpdateTiemStamp;

                            Response.Clear();
                            Response.Buffer = false;
                            Response.AddHeader("Accept-Ranges", "bytes");
                            Response.AppendHeader("ETag", "\"" + _EncodedData + "\"");
                            Response.AppendHeader("Last-Modified", lastUpdateTiemStamp);
                            Response.ContentType = "application/octet-stream";
                            Response.AddHeader("Content-Disposition", "attachment;filename=" + File_Name.Replace(" ", ""));
                            Response.AddHeader("Content-Length", (File_Name.Replace(" ", "").Length - startBytes).ToString());
                            Response.AddHeader("Connection", "Keep-Alive");
                            Response.ContentEncoding = Encoding.UTF8;

                            //Send data
                            _BinaryReader.BaseStream.Seek(startBytes, SeekOrigin.Begin);

                            //Dividing the data in 1024 bytes package
                            int maxCount = (int)Math.Ceiling((File_Name.Replace(" ", "").Length - startBytes + 0.0) / 1024);

                            //Download in block of 1024 bytes
                            int i;
                            for (i = 0; i < maxCount && Response.IsClientConnected; i++)
                            {
                                Response.BinaryWrite(_BinaryReader.ReadBytes(1024));
                                Response.Flush();
                            }


                        }
                        catch
                        {

                        }
                        finally
                        {

                            _BinaryReader.Close();
                            myFile.Close();
                            Response.End();
                        }
                    }

                }
                else
                {

                }
            }
            else
            {

            }
            return View();

        }
    }
}
