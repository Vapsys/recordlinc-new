﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentalFiles.Models;
using System.Data;
using Recordlinc.DAL;
using Recordlinc.FlipTop;
using System.Net;
using DentalFiles.App_Start;
namespace DentalFiles.Controllers
{
    public class PatientSignUpController : Controller
    {
        DataTable dt = new DataTable();
        Person objPerson = new Person();
        CommonDAL objCommonDAL = new CommonDAL();
        int UserId = 0;
        //
        // GET: /PatientSignUp/

        public ActionResult Index()
        {
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                return RedirectToAction("Index", "DashBoard");
            }
            else
            {
                PatientSignUp obj = new PatientSignUp();
                objCommonDAL.GetActiveCompany();

                if (TempData["UserId"] != null && Convert.ToInt32(TempData["UserId"]) != 0)
                {
                    SessionManagement.UserId = Convert.ToInt32(TempData["UserId"]);
                    if (Session["Firstnamepatient"] != null && Convert.ToString(Session["Firstnamepatient"]) != "")
                    {
                        obj.Firstname = Convert.ToString(Session["Firstnamepatient"]);

                    }
                    if (Session["Lastnamepatient"] != null && Convert.ToString(Session["Lastnamepatient"]) != "")
                    {
                        obj.Lastname = Convert.ToString(Session["Lastnamepatient"]);

                    }
                    if (Session["Emailpatient"] != null && Convert.ToString(Session["Emailpatient"]) != "")
                    {
                        obj.Email = Convert.ToString(Session["Emailpatient"]);

                    }
                    if (Session["PhoneNoPatient"] != null && Convert.ToString(Session["PhoneNoPatient"]) != "")
                    {
                        string p= Convert.ToString(Session["PhoneNoPatient"]);
                        string ph = p.Replace('(',' ').Replace(')',' ').Replace('-',' ');
                        string phone = ph.Replace(" ", "");
                        obj.PhoneNo = phone;
                    }

                }
                return View(obj);
            }
        }

        [HttpPost]
        public ActionResult Index(PatientSignUp objpatientsignup, string returnUrl)
        {
            if (SessionManagement.UserId != 0 && Convert.ToInt32(SessionManagement.UserId) != 0)
            {
                UserId = Convert.ToInt32(SessionManagement.UserId);
            }
            string txtfirstname = objpatientsignup.Firstname;
            string txtlastname = objpatientsignup.Lastname;
            string txtemail = objpatientsignup.Email;
            string txtphoneno = objpatientsignup.PhoneNo;
            string txtpassword = objpatientsignup.Password;
            string txtstreet = objpatientsignup.Street;
            string txtcity = objpatientsignup.City;
            string txtstate = objpatientsignup.State;
            string txtcountry = objpatientsignup.Country;
            string txtzip = objpatientsignup.ZipCode;
            string txtcomment = objpatientsignup.Comment;
            if (txtcomment == null)
            {
                txtcomment = "";
            }

            DataTable dtPatientEmail = objCommonDAL.CheckPatientEmail(txtemail);
            if (dtPatientEmail != null && dtPatientEmail.Rows.Count > 0)
            {
                dt = objCommonDAL.GetPatientPassword(txtemail);
                if (dt != null && dt.Rows.Count > 0)
                {

                    string recoverPassword = Convert.ToString(dt.Rows[0]["Password"].ToString());
                    string recoverPatientFirstName = Convert.ToString(dt.Rows[0]["FirstName"].ToString());
                    string recoverPatientLastName = Convert.ToString(dt.Rows[0]["LastName"].ToString());
                    string recoverPatientEmail = Convert.ToString(txtemail);

                    //Send Password To Patient Email
                    objCommonDAL.SendPatientPassword(recoverPatientEmail, recoverPassword, recoverPatientFirstName, recoverPatientLastName);

                    TempData["MessagePatient"] = "This email is already associated with another account. Please use a different email address.";
                    return RedirectToAction(returnUrl);
                }
            }


            int Result = 0;
            Person person = GetSocialMediaPerson(txtemail); //Get Social Media detail
            //ensure that first characters are capitalized
            person.Title = CapitalizeFirstCharacter(person.Title);
            person.Company = CapitalizeFirstCharacter(person.Company);
            if (string.IsNullOrEmpty(person.ImageUrl) == false)
            {
                DownloadFlipTopImage(person.ImageUrl, person.FullName); //download the imag

                person.ImageUrl = person.FullName + ".gif"; //rename it and point it to the local path
            }

            //if (UserId != 0)
            //{
             //   Result = Convert.ToInt32(objCommonDAL.AddPatient("", txtfirstname, "", txtlastname, 10, "", "", txtphoneno, txtemail, txtpassword, "", "", "", "", "", "", person.ImageUrl, UserId, 0, 0, 0, "", "", "", "", "", false, ""));
            //}
            if (UserId != 0)
            {
                bool Test = objCommonDAL.AddPatientToTemp(txtemail, txtpassword, txtfirstname, txtlastname, "", "", "", "", "", "", txtphoneno, "", "", UserId);
                if (Test)
                {
                    Result = 0;
                    objCommonDAL.SignUpEMail(txtemail, txtpassword);
                    TempData["MessagePatient"] = "Thank you for registration. We will contact you shortly.";

                }
            }


            if (Result != 0)
            {
                //Send Email To Doctor Which Is Selected By User
                if (UserId != 0)
                {
                    //objCommonDAL.PatientRequestReceived(UserId, txtemail, txtfirstname, txtlastname, txtphoneno, txtstreet, txtstreet, txtcity, txtstate, txtcountry, txtzip);
                    //NTD for PatId here
                    objCommonDAL.PatientAddedNotification(UserId,0, txtemail, txtfirstname, txtlastname, txtphoneno, txtstreet, txtstreet, txtcity, txtstate, txtcountry, txtzip);
                }
                objCommonDAL.SignUpEMail(txtemail, txtpassword);
                dt = objCommonDAL.PatientLoginform(txtemail, txtpassword);
                if (dt != null && dt.Rows.Count > 0)
                {
                    SessionManagement.PatientId = Convert.ToInt32(dt.Rows[0]["PatientId"]);
                    Session["PatientFullName"] = dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
                    return RedirectToAction("Index", "DashBoard");
                }
                else
                {
                    TempData["MessagePatient"] = "Your login information was not valid.<br />Please try again or contact your dental office.";
                }


            }
            return RedirectToAction(returnUrl);
        }
        string NewPatatientEmail = "";
        protected Recordlinc.FlipTop.Person GetSocialMediaPerson(string email)
        {
            try
            {
                //connect to fliptop and pull the information down
                SocialMedia sm = new SocialMedia();
                Person person = sm.LookUpPerson(NewPatatientEmail);

                return person;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected string CapitalizeFirstCharacter(string data)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(data))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(data[0]) + data.Substring(1);
        }

        protected void DownloadFlipTopImage(string imageUrl, string memberName)
        {
            try
            {
                if (string.IsNullOrEmpty(imageUrl))
                {
                    return; //nothing to save
                }

                memberName = memberName + ".gif";

                using (WebClient client = new WebClient())
                {
                    client.DownloadFile(imageUrl, Server.MapPath(@"~\ImageBank\" + memberName));
                }
            }
            catch
            {
                //if error occurs then image doesn't save, move on
            }
        }
    }
}