﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentalFiles.Models.ViewModel;
using JQueryDataTables.Models.Repository;
using DentalFiles.App_Start;
namespace DentalFiles.Controllers
{
    public class PrintMedicalHistoryUpdateController : Controller
    {
        //
        // GET: /PrintMedicalHistoryUpdate/
        DataRepository objRepository = new DataRepository();
        public ActionResult Index()
        {
            if (Request.QueryString["PatientId"] != null && Convert.ToInt32(Request.QueryString["PatientId"]) != 0)
            {
                SessionManagement.PatientId = Convert.ToInt32( Request.QueryString["PatientId"]);
            }
            var model = new DentalFormsMaster();
            model.MedicalHISTORYUpdate = objRepository.GetMedicalUpdate(Convert.ToInt32(SessionManagement.PatientId));

            
        
            return View(model);
        }

    }
}
