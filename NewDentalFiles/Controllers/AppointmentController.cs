﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentalFilesNew.Models;
using System.Data;
using DataAccessLayer.AppointmentData;
using DentalFiles.App_Start;
using DentalFiles.Models;
using DataAccessLayer.Common;
using DentalFilesNew.App_Start;
using JQueryDataTables.Models.Repository;
using BusinessLogicLayer;
using BO.Models;

namespace DentalFilesNew.Controllers
{
    public class AppointmentController : Controller
    {
        DataRepository objRepository = new DataRepository();
        private clsAppointment objAppointment;
        const string CONSTTIMEFORMATE = AppointmentSetting.CONSTTIMEFORMATE;// "hh:mm tt";
        string DentistImage = System.Configuration.ConfigurationManager.AppSettings["DentistImagespath"];
        // GET: Appointment
        CommonDAL objCommonDAL = new CommonDAL();
        [CustomAuthorizeAttribute]
        public ActionResult Index()
        {
            ViewBag.ReturnUrl = "Appointment";
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {

                List<PatientDetails> PatientDetialList = objRepository.GetPatientDetails(Convert.ToInt32(SessionManagement.PatientId), SessionManagement.TimeZoneSystemName);
                if (PatientDetialList != null && PatientDetialList.Count != 0)
                {
                    ViewBag.FirstName = PatientDetialList[0].FirstName;
                    if (!string.IsNullOrEmpty(PatientDetialList[0].ProfileImage.ToString()))
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["imagebankpath"] + PatientDetialList[0].ProfileImage.ToString();
                    }
                    else if (PatientDetialList[0].Gender == 1)
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"];
                    }
                    else
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["Doctorimage1"];

                    }
                }

                HttpCookie myCookie = new HttpCookie("Timeout");
                myCookie.Value = Convert.ToString(Session.Timeout);
                myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);

                DataTable dtCount = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0, 1, 100000);
                int count = 0;
                foreach (DataRow Count in dtCount.Rows)
                {
                    if (!Convert.ToBoolean(Count["Read_flag"]))
                    {
                        count = count + 1;
                    }
                }
                @ViewBag.MsgCount = count;

                return View();
            }
            else
            {
                return RedirectToAction("Index", "Index");
            }
        }
        [CustomAuthorizeAttribute]
        public ActionResult BookAppointment()
        {
            Appointment app = new Appointment();
            DataTable dt = new DataTable();
            int PatientId = Convert.ToInt32(Session["PatientId"]);
            objAppointment = new clsAppointment();
            dt = objAppointment.GetTreatingDoctorListForBookAppointmentByPatientId(PatientId, 0);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    app.DoctorList.Add(new SelectListItem
                    {
                        Value = Convert.ToString(item["DoctorId"]),
                        Text = item["DoctorName"].ToString(),
                    });
                }
            }
            return View(app);
        }
        [CustomAuthorizeAttribute]
        public ActionResult GetDoctorServices(int DoctorId)
        {
            Appointment app = new Appointment();
            DataTable dt = new DataTable();
            objAppointment = new clsAppointment();
            var doctorDetails = objAppointment.GetDoctorDetails(DoctorId);
            int ParentUserId = 0;
            if (doctorDetails.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(doctorDetails.Rows[0]["ParentUserId"])))
                {
                    ParentUserId = Convert.ToInt32(doctorDetails.Rows[0]["ParentUserId"]);
                    if (ParentUserId == 0)
                    {
                        ParentUserId = DoctorId;
                    }
                }
                else { ParentUserId = DoctorId; }
            }
            dt = objAppointment.GetDoctorServicesByDoctorId(ParentUserId, 0);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    app.DoctorServiceslst.Add(new SelectListItem
                    {
                        Value = Convert.ToString(item["AppointmentServiceId"]),
                        Text = item["ServiceType"].ToString()
                    });
                }
            }
            return Json(app.DoctorServiceslst.OrderBy(t => t.Text));
        }

        [CustomAuthorizeAttribute]
        public ActionResult AppointmentTimeSlot(int DoctorId, string ServiceId)
        {
            objAppointment = new clsAppointment();
            var Service = objAppointment.GetDoctorServicesByDoctorId(0, Convert.ToInt32(ServiceId));
            var DoctorName = objAppointment.GetTreatingDoctorListForBookAppointmentByPatientId(0, DoctorId);

            var DoctorAvalailableTimeSlot = new CreateAppointment();
            DoctorAvalailableTimeSlot.DoctorName = DoctorName.Rows[0]["DoctorName"].ToString();
            if (!string.IsNullOrWhiteSpace(DoctorName.Rows[0]["ImageName"].ToString()))
            {

                if (!System.IO.File.Exists(Convert.ToString(SessionManagement.CompanyWebsite + "/" + DentistImage + DoctorName.Rows[0]["ImageName"].ToString())))
                {
                    DoctorAvalailableTimeSlot.DoctorImage = SessionManagement.CompanyWebsite + "/" + DentistImage  + Convert.ToString(DoctorName.Rows[0]["ImageName"]);
                }
                else
                {
                    if (DoctorName.Rows[0]["Gender"].ToString() == "Male")
                    {
                        DoctorAvalailableTimeSlot.DoctorImage = System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"];
                    }
                    else
                    {
                        DoctorAvalailableTimeSlot.DoctorImage = System.Configuration.ConfigurationManager.AppSettings["Doctorimage1"];
                    }
                }
                //DoctorAvalailableTimeSlot.DoctorImage = "https://www.RecordLinc.com/DentistImages/" + DoctorName.Rows[0]["ImageName"].ToString();
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(DoctorName.Rows[0]["Gender"].ToString()))
                {
                    if (DoctorName.Rows[0]["Gender"].ToString() == "Male")
                    {
                        DoctorAvalailableTimeSlot.DoctorImage = System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"];
                    }
                    else
                    {
                        DoctorAvalailableTimeSlot.DoctorImage = System.Configuration.ConfigurationManager.AppSettings["Doctorimage1"];
                    }
                }
                else
                {
                    DoctorAvalailableTimeSlot.DoctorImage = System.Configuration.ConfigurationManager.AppSettings["Doctorimage1"];
                }
            }
            //DoctorAvalailableTimeSlot.DoctorCercleName = String.Join(String.Empty, Convert.ToString(DoctorName.Rows[0]["DoctorName"]).Split(new[] { ' ' }).Select(word => word.First())).ToUpper();
            DoctorAvalailableTimeSlot.ServiceId = ServiceId;
            DoctorAvalailableTimeSlot.DoctorId = DoctorId;
            DoctorAvalailableTimeSlot.Servicelenght = Convert.ToInt32(TimeSpan.Parse(Service.Rows[0]["TimeDuration"].ToString()).TotalMinutes);
            DoctorAvalailableTimeSlot.ServiceCount = DoctorAvalailableTimeSlot.Servicelenght / AppointmentSetting.CONSTTIME;
            DoctorAvalailableTimeSlot.ServiceType = Service.Rows[0]["ServiceType"].ToString();

            return View(DoctorAvalailableTimeSlot);
        }

        [CustomAuthorizeAttribute]
        public ActionResult AppointmentSlotGrid(int DoctorId, int ServiceId, string StartDate, int TheatreId)
        {
            objAppointment = new clsAppointment();
            var Service = objAppointment.GetDoctorServicesByDoctorId(0, ServiceId);
            var TimeSlot = CommonAppointment.GetAppointmentDataIntoList(objAppointment.GetCreateAppointmentTimeSlotViewData(DoctorId, TheatreId, 0), SessionManagement.TimeZoneSystemName);
            var DoctorAvalailableTimeSlot = CommonAppointment.TimeSlotAllocation(TimeSlot, Service, StartDate, SessionManagement.TimeZoneSystemName);


            return View(DoctorAvalailableTimeSlot);
        }
        [HttpGet]
        public JsonResult GetDefaultOP(int DoctorId)
        {
            int UserId = 0;
            var ResourceList = new CalendarResource();
            objAppointment = new clsAppointment();
            var doctorDetails = objAppointment.GetDoctorDetails(DoctorId);
            if (doctorDetails.Rows.Count > 0)
            {
                UserId = Convert.ToInt32(doctorDetails.Rows[0]["ParentUserId"]);
                if (UserId == 0)
                {
                    Int32 LocationId = Convert.ToInt32(doctorDetails.Rows[0]["LocationId"]);
                    ResourceList = CommonAppointment.CalendarResourceIntoList(objAppointment.GetOprationTheatorForCalendarByLocation(LocationId)).FirstOrDefault();
                }
                else
                {
                    var ParentDoctorDetails = objAppointment.GetDoctorDetails(DoctorId);
                    if (ParentDoctorDetails.Rows.Count > 0)
                    {
                        Int32 LocationId = Convert.ToInt32(ParentDoctorDetails.Rows[0]["LocationId"]);
                        ResourceList = CommonAppointment.CalendarResourceIntoList(objAppointment.GetOprationTheatorForCalendarByLocation(LocationId)).FirstOrDefault();
                    }
                }
            }
            return Json(ResourceList, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorizeAttribute]
        public ActionResult ShortingListOfAppointmentData()
        {
            Appointment appointment = new Appointment();
            //DataTable dt = new DataTable();
            //int PatientId = Convert.ToInt32(Session["PatientId"]);
            //objAppointment = new clsAppointment();
            //dt = objAppointment.GetPatientAppointmentView(PatientId, 3, 2);
            //if (dt.Rows.Count > 0)
            //{
            //    foreach (DataRow item in dt.Rows)
            //    {

            //        appointment.ViewApplst.Add(new ViewAppointment()
            //        {
            //            AppointmentId = Convert.ToInt32(item["AppointmentId"]),
            //            AppointmentDate = Convert.ToDateTime(item["AppointmentDate"]).ToString("dd-MM-yyyy"),
            //            AppointmentTime = DateTime.Today.Add(TimeSpan.Parse(item["AppointmentTime"].ToString())).ToString("h:m tt"),
            //            ServiceType = item["ServiceType"].ToString(),
            //            AppointmentLength = Convert.ToInt32(item["AppointmentLength"]),
            //            DoctorName = Convert.ToString(item["DoctorName"]),
            //            Status = Convert.ToString(item["Status"]),
            //            Rating = Convert.ToString(item["Rating"])
            //        });

            //    }
            //}
            return View();

        }

        [CustomAuthorizeAttribute]
        public ActionResult JqGrideForShowAppointment()
        {
            DateTime date = DateTime.Today;
            Appointment appointment = new Appointment();
            DataTable dt = new DataTable();
            int PatientId = Convert.ToInt32(Session["PatientId"]);
            objAppointment = new clsAppointment();
            dt = objAppointment.GetPatientAppointmentView(PatientId, 4, 2);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    //if (item["Status"].ToString() != "CANCELLED")
                    //{
                    var TempAppt = new ViewAppointment()
                    {
                        AppointmentId = Convert.ToInt32(item["AppointmentId"]),
                        AppointmentDate = Convert.ToDateTime(item["AppointmentDate"]).ToString("MM/dd/yyyy"),
                        AppointmentTime = DateTime.Today.Add(TimeSpan.Parse(item["AppointmentTime"].ToString())).ToString(AppointmentSetting.CONSTTIMEFORMATE),
                        ServiceType = item["ServiceType"].ToString(),
                        AppointmentLength = Convert.ToUInt32(item["AppointmentLength"]),
                        DoctorName = Convert.ToString(item["DoctorName"]),
                        Status = Convert.ToString(item["Status"]),
                        Rating = Convert.ToString(item["Rating"]),
                        AppointmentStatusId = Convert.ToInt32(item["AppointmentStatusId"]),
                        AppointmentDateTime = Convert.ToString(item["AppointmentDateTime"]),
                        SystemDateTime = clsHelper.ConvertFromUTC(Convert.ToDateTime(DateTime.Now.ToString()), SessionManagement.TimeZoneSystemName).ToString(),
                        AppoinmentEndTime = DateTime.Today.Add(TimeSpan.Parse(item["AppointmentTime"].ToString()).Add(TimeSpan.FromMinutes(Convert.ToUInt32(item["AppointmentLength"])))).ToString(CONSTTIMEFORMATE)
                    };

                    if(!string.IsNullOrWhiteSpace(TempAppt.AppointmentDate) && !string.IsNullOrWhiteSpace(TempAppt.AppointmentTime))
                    {
                        //try
                        //{
                            var dtAppointmentDate = Convert.ToDateTime(TempAppt.AppointmentDate + " " + TempAppt.AppointmentTime);
                            dtAppointmentDate = clsHelper.ConvertFromUTC(dtAppointmentDate, SessionManagement.TimeZoneSystemName);
                        // TempAppt.AppointmentDate = dtAppointmentDate.Date.ToString("MM/dd/yyyy");
                        // TempAppt.AppointmentTime = dtAppointmentDate.ToString(AppointmentSetting.CONSTTIMEFORMATE);
                        TempAppt.AppointmentDate = new CommonBLL().ConvertToDate(dtAppointmentDate, (int)BO.Enums.Common.DateFormat.GENERAL);
                        TempAppt.AppointmentTime = new CommonBLL().ConvertToTime(dtAppointmentDate, (int)BO.Enums.Common.DateFormat.GENERAL);
                        TempAppt.AppoinmentEndTime = new CommonBLL().ConvertToTime(clsHelper.ConvertFromUTC(Convert.ToDateTime(TempAppt.AppoinmentEndTime), SessionManagement.TimeZoneSystemName),(int)BO.Enums.Common.DateFormat.GENERAL);

                        dtAppointmentDate = Convert.ToDateTime(TempAppt.AppointmentDateTime);
                        dtAppointmentDate = clsHelper.ConvertFromUTC(dtAppointmentDate, SessionManagement.TimeZoneSystemName);
                        TempAppt.AppointmentDateTime = dtAppointmentDate.ToString();
                        //}
                        //catch(Exception e)
                        //{
                        //    return Content("Value : " + TempAppt.AppointmentDate + " " + TempAppt.AppointmentTime + "<br>"
                        //        + e.ToString(), "text/html");
                        //}

                    }
                    appointment.ViewApplst.Add(TempAppt);

                }
                //}
            }
            return Json(new { data = appointment.ViewApplst }, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorizeAttribute]
        public JsonResult CheckOverLapping(AppointmentData AppData)
        {
            var objAppointment = new clsAppointment();
            var objCreateAppointment = new CreateAppointment();
            var Service = objAppointment.GetDoctorServicesByDoctorId(0, Convert.ToInt32(AppData.ServiceTypeId));
            int Applength = Convert.ToInt32(Service.Rows[0]["TimeDuration"].ToString());
            TimeSpan temptime = AppData.AppointmentTime.Add(TimeSpan.FromMinutes(Convert.ToInt32(Applength)));
            objCreateAppointment.FromTime = Convert.ToDateTime(AppData.AppointmentDate.ToString(AppointmentSetting.CONSTDATEFORMATE1) + " " + AppData.AppointmentTime).ToString();
            objCreateAppointment.ToTime = Convert.ToDateTime(AppData.AppointmentDate.ToString(AppointmentSetting.CONSTDATEFORMATE1) + " " + temptime).ToString();
            objCreateAppointment.ApptResourceId = AppData.ApptResourceId;
            string message = CommonAppointment.CheckOverLappingAndDoctorAvailability(objCreateAppointment, AppData.DoctorId, SessionManagement.TimeZoneSystemName);
            objCreateAppointment.AppointmentLength = Applength;
            objCreateAppointment.CurrentDate = Convert.ToDateTime(AppData.AppointmentDate.ToString(AppointmentSetting.CONSTDATEFORMATE1));
            objCreateAppointment.DoctorId = AppData.DoctorId;
            if (!string.IsNullOrEmpty(message))
            {
                return Json("Your selected service is " + Applength + " minute, it can't be booked at selected time slot.");
            }
            else if ((CommonAppointment.CheckAvalibleDoctorByDateTimeTheatreDoctor(objCreateAppointment, objCreateAppointment.DoctorId, SessionManagement.TimeZoneSystemName) > 0))
            {

                return Json("Provider is not available for specified time slot.");
            }
            else if ((CommonAppointment.CheckAvalibleDoctorByDateTimeTheatrePatient(objCreateAppointment, Convert.ToInt32(Session["PatientId"]), SessionManagement.TimeZoneSystemName) > 0))
            {

                return Json("Patient is not available for specified time slot.");
            }
            return Json("");
              
        }

        [CustomAuthorizeAttribute]
        public ActionResult SubmitAppointmentBookingData(AppointmentData objAppointmentData)
        {
            objAppointment = new clsAppointment();
            DateTime AppointmentDate;
            //TimeSpan AppointmentTime;
            AppointmentDate = objAppointmentData.AppointmentDate.Date.Add(objAppointmentData.AppointmentTime);
            AppointmentDate = clsHelper.ConvertToUTC(AppointmentDate, SessionManagement.TimeZoneSystemName);
            //AppointmentDate = clsHelper.ConvertToUTC(AppointmentDate);
            //AppointmentTime = objAppointmentData.AppointmentTime;
            
            var Service = objAppointment.GetDoctorServicesByDoctorId(0, Convert.ToInt32(objAppointmentData.ServiceTypeId));

            if (Service.Rows.Count > 0)
            {
                objAppointmentData.AppointmentLength = Convert.ToInt32(Service.Rows[0]["TimeDuration"].ToString());
            }
            int NewAppId = objAppointment.SubmitAppointmentBookingData(objAppointmentData.DoctorId, Convert.ToInt32(Session["PatientId"]), AppointmentDate.Date, AppointmentDate.TimeOfDay, objAppointmentData.AppointmentLength, Convert.ToInt32(objAppointmentData.ServiceTypeId), objAppointmentData.AppNote, Convert.ToInt32(Session["PatientId"]), 1, objAppointmentData.ApptResourceId, 0, 0,0);
            DataTable dt = new DataTable();
            MailNotiFicationForCreateAppointment obj = new MailNotiFicationForCreateAppointment();
            clsTemplate clstemp = new clsTemplate();
            return Json("");
        }

        [CustomAuthorizeAttribute]
        public ActionResult CancleAppointment(int AppointmentId)
        {
            return View();
        }

        [CustomAuthorizeAttribute]
        public JsonResult CancelAppointment(CancelAppointment objcancelApp)
        {
            DataTable dt = new DataTable();
            objAppointment = new clsAppointment();
            dt = objAppointment.CancleAppointmentByPatient(objcancelApp.AppointmentId, Convert.ToInt32(Session["PatientId"]), 1, objcancelApp.CancelNotes, 11);
            return Json(JsonRequestBehavior.AllowGet);

        }
    }
}