﻿using BO.Models;
using BusinessLogicLayer;
using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DentalFilesNew.Controllers
{
    public class PatientFormController : Controller
    {
        // GET: PatientForm
        public ActionResult Fill()
        {

            Compose1Click ComposeReferral = new Compose1Click();
            string DecryptString = string.Empty;
            if (ViewBag.alert != null)
            {
                DecryptString = (string)TempData["String"];
            }
            else
            {
                DecryptString = Convert.ToString(Request.QueryString["TYHJNABGA"]);
            }
            TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
            int PatientId = 0; int DoctorId = 0;
            bool flag = false;
            if (!string.IsNullOrEmpty(DecryptString))
            {
                //DecryptString = DecryptString;
                string[] DecryptedDoctorId = DecryptString.Split('|');
                if (DecryptedDoctorId.Length == 3)
                {
                    flag = true;
                }
                //PatientId = Convert.ToInt32(ObjTripleDESCryptoHelper.decryptText(DecryptedDoctorId[0]));
                PatientId = Convert.ToInt32(DecryptedDoctorId[0]) - 999;
                TempData["PatientId"] = PatientId;
                //DoctorId = Convert.ToInt32(ObjTripleDESCryptoHelper.decryptText(DecryptedDoctorId[1]));
                DoctorId = Convert.ToInt32(DecryptedDoctorId[1]) - 999;
                TempData["DoctorId"] = DoctorId;
                TempData.Keep();
            }
            else
            {
                PatientId = Convert.ToInt32(TempData["PatientId"]);
                DoctorId = Convert.ToInt32(TempData["DoctorId"]);
            }
            //string DecryptedPatientId = ObjTripleDESCryptoHelper.decryptText(Request.QueryString["PatientId"]);
            ComposeReferral = ReferralFormBLL.GetPatientInfo(PatientId);
            ComposeReferral._innerService = ReferralFormBLL.GetSpecialityForPatientForm(DoctorId);
            ComposeReferral._insuranceCoverage = ReferralFormBLL.GetInsuranceDetails(PatientId);
            ComposeReferral._specialtyService = ReferralFormBLL.GetPatientReferalVisibleSection(Convert.ToInt32(DoctorId));
            ComposeReferral._medicalHistory = ReferralFormBLL.GetMedicalHistoryDetails(PatientId);
            ComposeReferral._dentalhistory = ReferralFormBLL.GetDentalHistoryDetails(PatientId);
            ComposeReferral._contactInfo.EMGContactName = ComposeReferral._insuranceCoverage.EMGContactName;
            ComposeReferral._contactInfo.EMGContactNo = ComposeReferral._insuranceCoverage.EMGContactNo;
            ComposeReferral.StateList = PatientBLL.GetStateList("US");
            ComposeReferral.ReceiverId = Convert.ToString(DoctorId);
            ComposeReferral.SenderId = Convert.ToString(PatientId);
            ComposeReferral.IsReferral = flag;
            ViewBag.alert = TempData["success"];
            TempData["success"] = null;
            return View("Fill", ComposeReferral);
        }
        /// <summary>
        /// Method use for update Patient detail when patient click on link from mail. Use in One Click Referral.
        /// </summary>
        /// <param name="mdlpatientsignup"></param>
        /// <returns></returns>
        [HttpPost]
        //[CustomAuthorizeAttribute]
        public ActionResult SubmitPatientDetail(Compose1Click mdlpatientsignup)
        {
            clsCommon objCommon = new clsCommon();
            bool result = false;
            int intResult = ReferralFormBLL.ComposeReferral1Click(mdlpatientsignup, true);
            //Change for XQ1-1059 
            if (intResult > 0)
            {
                result = true;
            }
            TempData["success"] = result;
            
            return RedirectToAction("Fill");
        }
    }

}