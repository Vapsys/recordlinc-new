﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using DentalFiles.Models;
using System.Configuration;
using DentalFiles.App_Start;
using System.Web.Security;

namespace DentalFiles.Controllers
{
    public class _LoginController : Controller
    {

        CommonDAL objCommonDAL = new CommonDAL();
        DataTable dt = new DataTable();
        public ActionResult Index()
        {
            objCommonDAL.GetActiveCompany();


            Session["ReturnUrl"] = Request.Url.AbsolutePath;
            return View();
        }

        [HttpPost]
        public JsonResult ReqLogin(string Email, string temppass)
        {
            object obj = null;
            string ReturnUrl = "";
            SessionManagement.PatientId = 0;

            

            dt = objCommonDAL.PatientLoginform(Email, temppass);
            if (dt != null && dt.Rows.Count > 0)
            {

                SessionManagement.PatientId = Convert.ToInt32(dt.Rows[0]["PatientId"]);
                SessionManagement.TimeZoneSystemName = Convert.ToString(dt.Rows[0]["TimeZoneSystemName"]);
                if (string.IsNullOrWhiteSpace(SessionManagement.TimeZoneSystemName))
                    SessionManagement.TimeZoneSystemName = "Eastern Standard Time";

                FormsAuthentication.SetAuthCookie(Email, false /* createPersistentCookie */);
                Session["UserIdPublic"] = string.Empty;
                TempData["message"] = null;
                obj = new
                {
                    Success = true,
                    Message = "",
                };
                return Json(obj, JsonRequestBehavior.DenyGet);

            }
            else
            {
                DataTable dttemp = new DataTable();

                dttemp = objCommonDAL.CheckTempTable(Email);
                if (dttemp != null && dttemp.Rows.Count > 0)
                {
                    if (Session["ReturnUrl"] != null && Convert.ToString(Session["ReturnUrl"]) != "")
                    {
                        ReturnUrl = Convert.ToString(Session["ReturnUrl"]);
                    }
                    else
                    {
                        ReturnUrl = "Index";
                    }

                    return Json(new
                    {
                        Success = false,
                        Message = "Your registration is in process. We will contact you shortly.",
                        redirectUrl = Url.Action(ReturnUrl),
                        isRedirect = true
                    });

                }
                else
                {

                    return Json(new
                    {
                        Success = false,
                        Message = "Your login information was not valid. Please try again or contact your dental office.",
                        redirectUrl = Url.Action(ReturnUrl),
                        isRedirect = true
                    });
                }

            }



        }




    }
}
