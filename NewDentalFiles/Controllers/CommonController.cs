﻿using BO.ViewModel;
using BusinessLogicLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DentalFilesNew.Controllers
{
    public class CommonController : Controller
    {
        // GET: Common
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult PartialFooter()
        {
            try
            {
                CommonBLL objbll = new CommonBLL();
                FooterDetails objfooter = new FooterDetails();
                if (!Convert.ToBoolean(Session["isWidget"]))
                {
                    objfooter.lstFooterCity = objbll.GetFooterCity();
                    //objfooter.lstlocationList = objbll.GetLocationHistory();
                    objfooter.lstSpecialityList = objbll.GetSpecialityHistory();
                }
                return PartialView("PartialFooter", objfooter);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}