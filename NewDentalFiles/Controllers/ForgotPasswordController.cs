﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentalFiles.Models;
using System.Data;
using DentalFiles.App_Start;
using Recordlinc.Core.Common;
using System.Web.Security;

namespace DentalFiles.Controllers
{
    public class ForgotPasswordController : Controller
    {
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        CommonDAL objCommonDAL = new CommonDAL();
        public ActionResult Index()
        {
            ViewBag.Title = "Forgot Password";
            objCommonDAL.GetActiveCompany();
            ViewBag.ReturnUrl = "ForgotPassword";
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                SessionManagement.PatientId = 0;
            }
            return View();
        }

        [HttpPost]
        public JsonResult MForgotpassword(string Email)
        {
            object obj = null;

            try
            {
                DataTable dtPatient = objCommonDAL.GetPatientPassword(Email);

                if (dtPatient == null || dtPatient.Rows.Count == 0)
                {
                    dtPatient = objCommonDAL.Dental_GetPatientPasswordTemp(Email);
                }

                if (dtPatient != null && dtPatient.Rows.Count > 0)
                {
                    string Password = dtPatient.Rows[0]["Password"].ToString();
                    string PatientFirstName = dtPatient.Rows[0]["FirstName"].ToString();
                    string PatientLastName = dtPatient.Rows[0]["LastName"].ToString();
                    string PatientEmail = Email;
                  
                    //Send Password To Patient Email
                    objCommonDAL.ResetPatientPassword(PatientEmail , PatientFirstName, PatientLastName);

                    obj = new
                    {
                        Success = true,
                        Message = "Password sent successfully to your email",
                    };
                }
                else
                {
                    obj = new
                    {
                        Success = false,
                        Message = "Your Email is not available.<br />Please try again or contact your dental office.",
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Json(obj, JsonRequestBehavior.DenyGet);



        }

        public ActionResult ResetPassword(string XSDFHI)
        {
            string Email = "";
            ViewBag.PatientName = "";
            DataTable dt = new DataTable();
            if (!string.IsNullOrWhiteSpace(XSDFHI))
            {
                XSDFHI = XSDFHI.Replace(" ","+");
                
                Email = ObjTripleDESCryptoHelper.decryptText(XSDFHI);
                dt = objCommonDAL.GetPatientDetailsByEmail(Email);
                if (dt.Rows.Count > 0)
                {
                    ViewBag.PatientName = dt.Rows[0]["FirstName"].ToString() + " " + dt.Rows[0]["LastName"].ToString();
                }
            }
            return View();
        }

        public string UpdatePasswordFromLink(string Password, string Email)
        {
            string email = ObjTripleDESCryptoHelper.decryptText(Email);
            string status = "";
            bool obj = objCommonDAL.UpdatePatientPasswordByLink(email, Password);
            if (obj)
            {
                status = "1";
                LogIn(email, Password);
            }
            else
            {
                status = "0";
            }
            return status;
        }
        public void LogIn(string Email, string Pass)
        {
            DataTable dt = new DataTable();
            object obj = null;
            string ReturnUrl = "";
            SessionManagement.PatientId = 0;



            dt = objCommonDAL.PatientLoginform(Email, Pass);
            if (dt != null && dt.Rows.Count > 0)
            {

                SessionManagement.PatientId = Convert.ToInt32(dt.Rows[0]["PatientId"]);

                FormsAuthentication.SetAuthCookie(Email, false /* createPersistentCookie */);
                Session["UserIdPublic"] = string.Empty;
                

            }
        }
    }
}
