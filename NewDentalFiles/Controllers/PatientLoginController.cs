﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using DentalFiles.Models;
using System.Configuration;
using DentalFiles.App_Start;
using Recordlinc.Core.Common;
using System.Web.Security;
using DataAccessLayer.PatientsData;
using DataAccessLayer.ColleaguesData;

namespace DentalFiles.Controllers
{
    public class PatientLoginController : Controller
    {
        //
        // GET: /PatientLogin/
        CommonDAL objCommonDAL = new CommonDAL();

        DataTable dt = new DataTable();
        TripleDESCryptoHelper cryptoHelper = new TripleDESCryptoHelper();
        public ActionResult Index()
        {
            Session["ReturnUrl"] = "DashBoard";

            if (Request.QueryString["username"] != null && Convert.ToString(Request.QueryString["username"]) != "" && Request.QueryString["pass"] != null && Convert.ToString(Request.QueryString["pass"]) != "")
            {
                string password = Request.QueryString["pass"].ToString();
                password = password.Replace(" ", "+");
                string DecryptedPassword = cryptoHelper.decryptText(password);


                SessionManagement.PatientId = 0;
                var email = Request.QueryString["username"].ToString();
                var Password = DecryptedPassword;

                dt = objCommonDAL.PatientLoginform(email, Password);

                if (dt != null && dt.Rows.Count > 0)
                {
                    SessionManagement.TimeZoneSystemName = Convert.ToString(dt.Rows[0]["TimeZoneSystemName"]);
                    if (string.IsNullOrWhiteSpace(SessionManagement.TimeZoneSystemName))
                        SessionManagement.TimeZoneSystemName = "Eastern Standard Time";
                    FormsAuthentication.SetAuthCookie(email, false /* createPersistentCookie */);
                    SessionManagement.PatientId = Convert.ToInt32(dt.Rows[0]["PatientId"]);
                    Session["UserIdPublic"] = string.Empty;
                    string IsPay = Convert.ToString(Request.QueryString["ispay"]);
                    if(IsPay == "1")
                    {
                        TempData["pay_doctorid"] = Convert.ToString(Request.QueryString["doctorid"]);
                        TempData["pay_amount"] = Convert.ToString(Request.QueryString["amount"]);
                        TempData.Keep();
                        return RedirectToAction("MakeTransaction", "Payment");
                    }
                    else
                    {
                        return RedirectToAction("Index", Convert.ToString(Session["ReturnUrl"]));
                    }
                    

                }
                else
                {
                    DataTable dttemp = new DataTable();

                    dttemp = objCommonDAL.CheckTempTable(email);
                    if (dttemp != null && dttemp.Rows.Count > 0)
                    {
                        //TempData["message"] = "Your registration is in process. We will contact you shortly.";
                        
                        int Result = Convert.ToInt32(objCommonDAL.AddPatient("", Convert.ToString(dttemp.Rows[0]["FirstName"]), "", Convert.ToString(dttemp.Rows[0]["LastName"]), 0, "", "",
                            Convert.ToString(dttemp.Rows[0]["HomePhone"]), Convert.ToString(dttemp.Rows[0]["Email"]),
                            Convert.ToString(Password), "", "", "", "", "", "", "", Convert.ToInt32(dttemp.Rows[0]["DoctorId"]), 0, 0, 0, "", "", "", "", "", false, ""));
                        if (Result > 0)
                        {
                            clsPatientsData objPatientsData = new clsPatientsData();
                            clsColleaguesData objcolleaguesdata = new clsColleaguesData();
                            DataTable dt = new DataTable();
                            dt = objcolleaguesdata.GetDoctorDetailsbyId(Convert.ToInt32(dttemp.Rows[0]["DoctorId"]));
                            string UserName = string.Empty;
                            if(dt != null && dt.Rows.Count > 0)
                            {
                                UserName = Convert.ToString(dt.Rows[0]["FirstName"])+" "+ Convert.ToString(dt.Rows[0]["LastName"]);
                            }
                            int id = objPatientsData.Insert_PatientMessages(Convert.ToString(dttemp.Rows[0]["FirstName"])+""+ Convert.ToString(dttemp.Rows[0]["LastName"]), UserName, "Patient sign-up", "<a href=\"javascript:;\" onclick=\"getAttachedPatientHistory(" + Result + ")\">" + Convert.ToString(dttemp.Rows[0]["FirstName"]) + "" + Convert.ToString(dttemp.Rows[0]["LastName"]) + "</a> recently signed up as patient.", Convert.ToInt32(dttemp.Rows[0]["DoctorId"]), Result, false, false, true);
                            bool status = objPatientsData.RemovePatient(Result,0);
                            if (status == false)
                            {
                                TempData["message"] = "Failed to activate your account.";
                                return RedirectToAction("Index", "Index");
                            }
                            TempData["message"] = "Your account activated successfully. Kindly login with your credentials";
                            return RedirectToAction("Index", "Index");
                            //This Method Is commented because we have not able to login Directly with _Login Controller.
                            //return RedirectToAction("ReqLogin", "_Login", new { Email= email, temppass = Password });
                        }
                    }
                    else
                    {
                        TempData["message"] = "Your login information was not valid. Please try again or contact your dental office.";
                        return RedirectToAction("Index", "Index");
                    }

                    if (Session["ReturnUrl"] != null || Convert.ToString(Session["ReturnUrl"]) != "")
                    {
                        return RedirectToAction("Index", "PatientLogin", new { returnUrl = Convert.ToString(Session["ReturnUrl"]) });
                    }
                    else
                    {
                        return RedirectToAction("Index", "PatientLogin");
                    }


                }

            }
            else if (Request["ZDKRFSDJB"] != null && Request["ZDKRFSDJB"] != "")
            {
                string firststr = Request["ZDKRFSDJB"].ToString();
                firststr = firststr.Replace(" ", "+");
                string decriptdstring = cryptoHelper.decryptText(firststr);
                string[] stringArr = decriptdstring.Split('|');

                string username = stringArr[0];
                string password = cryptoHelper.decryptText(stringArr[1]);
                string Mid = stringArr[2];

                SessionManagement.PatientId = 0;
                dt = objCommonDAL.PatientLoginform(username, password);
                if (dt != null && dt.Rows.Count > 0)
                {
                    FormsAuthentication.SetAuthCookie(username, false /* createPersistentCookie */);
                    SessionManagement.PatientId = Convert.ToInt32(dt.Rows[0]["PatientId"]);
                    Session["UserIdPublic"] = string.Empty;
                    SessionManagement.TimeZoneSystemName = Convert.ToString(dt.Rows[0]["TimeZoneSystemName"]);
                    if (string.IsNullOrWhiteSpace(SessionManagement.TimeZoneSystemName))
                        SessionManagement.TimeZoneSystemName = "Eastern Standard Time";
                    return RedirectToAction("Index", "MyInbox", new { messageid = Mid });
                }

            }
            else if (!string.IsNullOrEmpty(Request["usrem"]) && !string.IsNullOrEmpty(Request["uspswd"]))
            {
                string Email = Request["usrem"].ToString().Replace(" ", "+");
                string Password = Request["uspswd"].ToString().Replace(" ", "+");
                Email = cryptoHelper.decryptText(Email);
                Password = cryptoHelper.decryptText(Password);

                SessionManagement.PatientId = 0;
                dt = objCommonDAL.PatientLoginform(Email, Password);
                FormsAuthentication.SetAuthCookie(Email, false /* createPersistentCookie */);
                SessionManagement.PatientId = Convert.ToInt32(dt.Rows[0]["PatientId"]);
                /*Testing Purpose*/
                Session["UserIdPublic"] = string.Empty;
                SessionManagement.TimeZoneSystemName = Convert.ToString(dt.Rows[0]["TimeZoneSystemName"]);
                if (string.IsNullOrWhiteSpace(SessionManagement.TimeZoneSystemName))
                    SessionManagement.TimeZoneSystemName = "Eastern Standard Time";
                return RedirectToAction("Index", "Appointment");
            }
            else if (string.IsNullOrEmpty(Request["usrem"]) && string.IsNullOrEmpty(Request["uspswd"]))
            {
                string Email = Request["usrem"].ToString().Replace(" ", "+");
                string Password = Request["uspswd"].ToString().Replace(" ", "+");
                Email = cryptoHelper.decryptText(Email);
                Password = cryptoHelper.decryptText(Password);

                SessionManagement.PatientId = 0;
                dt = objCommonDAL.PatientLoginform(Email, Password);
                FormsAuthentication.SetAuthCookie(Email, false /* createPersistentCookie */);
                SessionManagement.PatientId = Convert.ToInt32(dt.Rows[0]["PatientId"]);
                Session["UserIdPublic"] = string.Empty;
                SessionManagement.TimeZoneSystemName = Convert.ToString(dt.Rows[0]["TimeZoneSystemName"]);
                if (string.IsNullOrWhiteSpace(SessionManagement.TimeZoneSystemName))
                    SessionManagement.TimeZoneSystemName = "Eastern Standard Time";
                return RedirectToAction("Index", "Appointment");
            }


            if (Request.QueryString["returnUrl"] != null && Convert.ToString(Request.QueryString["returnUrl"]) != "")
            {
                Session["ReturnUrl"] = Convert.ToString(Request.QueryString["returnUrl"]);
            }


            return View();
        }

        [HttpPost]
        public ActionResult Login(string Email, string password)
        {
            if (Session["ReturnUrl"] != null || Convert.ToString(Session["ReturnUrl"]) != "")
            {
            }
            else
            {
                Session["ReturnUrl"] = "DashBoard";
            }

            SessionManagement.PatientId = 0;
            var email = Email;
            var Password = password;

            dt = objCommonDAL.PatientLoginform(email, Password);


            if (dt != null && dt.Rows.Count > 0)
            {
                FormsAuthentication.SetAuthCookie(Email, false /* createPersistentCookie */);
                SessionManagement.PatientId = Convert.ToInt32(dt.Rows[0]["PatientId"]);
                Session["UserIdPublic"] = string.Empty;
                return RedirectToAction("Index", Convert.ToString(Session["ReturnUrl"]));

            }
            else
            {
                DataTable dttemp = new DataTable();

                dttemp = objCommonDAL.CheckTempTable(email);
                if (dttemp != null && dttemp.Rows.Count > 0)
                {
                    TempData["message"] = "your registration is in process. we will contact you shortly.";
                }
                else
                {
                    TempData["message"] = "Your login information was not valid. Please try again or contact your dental office.";
                }

                if (Session["ReturnUrl"] != null || Convert.ToString(Session["ReturnUrl"]) != "")
                {
                    return RedirectToAction("Index", "PatientLogin", new { returnUrl = Convert.ToString(Session["ReturnUrl"]) });
                }
                else
                {
                    return RedirectToAction("Index", "PatientLogin");
                }
            }

        }

    }
}
