﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using DentalFiles.Models;
using JQueryDataTables.Models.Repository;
using DentalFiles.Models.ViewModel;
using PagedList;
using DentalFiles.App_Start;
using System.IO;
using DataAccessLayer.Common;

namespace DentalFiles.Controllers
{
    public class MyInboxController : Controller
    {
        //
        // GET: /MyInbox/

        DataTable dt = new DataTable();
        CommonDAL objCommonDAL = new CommonDAL();
        DataRepository objRepository = new DataRepository();


        public ActionResult Index(string PatientName, int? Refpage, int? Msgpage, int? MsgSentpage, string RefPageSize, string MsgPageSize, string MsgSentPageSize, string RefSortBy, string MsgSortBy, string MsgSentSortBy, string ViewType)
         {
            TempData["DltInboxMsg"] = null;
            TempData["DltSentMsg"] = null;
            Session["ForwordFlag"] = null;

            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {

                ViewBag.RefFromFieldSortBy = "FromField asc";
                ViewBag.RefToFieldSortBy = "ToField asc";
                ViewBag.RefCreationDateSortBy = "CreationDate asc";
                ViewBag.RefStatusSortBy = "Status asc";

                ViewBag.RefFromFieldSortBy = "FromField asc";
                ViewBag.MsgCreationDateSortBy = "CreationDate asc";
                ViewBag.MsgSubjectSortBy = "Subject asc";

                ViewBag.MsgSentCreationDateSortBy = "CreationDate asc";
                ViewBag.MsgSentSubjectSortBy = "Subject asc";

                if (Request["messageid"] != null)
                {
                    ViewBag.messageid = Request["messageid"].ToString();
                    HttpCookie myCookie = new HttpCookie("Timeout");
                    myCookie.Value = Convert.ToString(Session.Timeout);
                    myCookie.Expires = DateTime.Now.AddDays(1d);
                    Response.Cookies.Add(myCookie);
                }


                int RefdefaultPageSize = 5;
                int MsgdefaultPageSize = 5;
                int MsgSentdefaultPageSize = 5;
                if (RefPageSize != null)
                {
                    RefdefaultPageSize = Convert.ToInt32(RefPageSize);
                }

                if (MsgPageSize != null)
                {
                    MsgdefaultPageSize = Convert.ToInt32(MsgPageSize);
                }
                

                if (MsgSentPageSize != null)
                {
                    MsgSentdefaultPageSize = Convert.ToInt32(MsgSentPageSize);
                }

                objCommonDAL.GetActiveCompany();
                List<PatientDetails> PatientDetialList = objRepository.GetPatientDetails(Convert.ToInt32(SessionManagement.PatientId), SessionManagement.TimeZoneSystemName);
                if (PatientDetialList != null && PatientDetialList.Count != 0)
                {
                    ViewBag.FirstName = PatientDetialList[0].FirstName;
                    if (!string.IsNullOrEmpty(PatientDetialList[0].ProfileImage.ToString()))
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["imagebankpath"] + PatientDetialList[0].ProfileImage.ToString();
                    }
                    else if (PatientDetialList[0].Gender == 1)
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"];
                    }
                    else
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["Doctorimage1"];

                    }
                }
                ViewBag.Title = "My Inbox";
                ViewBag.ReturnUrl = "MyInbox";
                ViewData["PatientName"] = PatientName;
                int currentRefPageIndex = Refpage.HasValue ? Refpage.Value : 1;
                int currentMsgPageIndex = Msgpage.HasValue ? Msgpage.Value : 1;
                int currentMsgSentPageIndex = MsgSentpage.HasValue ? MsgSentpage.Value : 1;
                var model = new MyInboxMaster();

                @ViewBag.MsgCount = 0;


                if (ViewType == "RefPartial")
                {
                    model.Messages = objRepository.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), SessionManagement.TimeZoneSystemName).ToPagedList(currentMsgPageIndex, MsgdefaultPageSize);
                    if (model.Messages != null && model.Messages.Count > 0)
                    {
                        @ViewBag.MsgCount = model.Messages[0].TotalRecord;
                    }
                    model.ReferralDetails = objRepository.GetAllPatientReferrals(Convert.ToInt32(SessionManagement.PatientId), SessionManagement.TimeZoneSystemName).ToPagedList(currentRefPageIndex, RefdefaultPageSize);

                    return PartialView("_Referral", model);
                }
                else if (ViewType == "MsgPartial")
                {

                    model.Messages = objRepository.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), SessionManagement.TimeZoneSystemName).ToPagedList(currentMsgPageIndex, MsgdefaultPageSize);
                    int count = 0;
                    if (model.Messages != null && model.Messages.Count > 0)
                    {

                        foreach (var ReadFlag in model.Messages)
                        {
                            if (ReadFlag.Read_flag == "False")
                            {
                                count = count + 1;
                            }
                        }
                    }
                    ViewBag.MsgCount = count;
                    return PartialView("_Message", model);
                }


                else if (ViewType == "MsgSentPartial")
                {
                    model.PatientSentMsg = objRepository.AllPatientSent(Convert.ToInt32(SessionManagement.PatientId),SessionManagement.TimeZoneSystemName).ToPagedList(currentMsgSentPageIndex, MsgSentdefaultPageSize);

                    return PartialView("_SentMsg", model);
                }
                else
                {

                    List<ReferralDetails> lstRefDetail = objRepository.GetAllPatientReferrals(Convert.ToInt32(SessionManagement.PatientId), SessionManagement.TimeZoneSystemName);
                    IOrderedEnumerable<ReferralDetails> ionRefDetail = lstRefDetail.OrderByDescending(m => m.CreationDate);


                    List<Messages> lstMsgDetail = objRepository.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), SessionManagement.TimeZoneSystemName);
                    IOrderedEnumerable<Messages> ionMsgDetail = lstMsgDetail.OrderByDescending(m => m.CreationDate);

                    List<Sent> lstSentMsgDetail = objRepository.AllPatientSent(Convert.ToInt32(SessionManagement.PatientId), SessionManagement.TimeZoneSystemName);
                    IOrderedEnumerable<Sent> ionSentMsgDetail = lstSentMsgDetail.OrderByDescending(m => m.CreationDate);

                    switch (RefSortBy)
                    {
                        case "FromField desc":
                            ionRefDetail = lstRefDetail.OrderByDescending(m => m.FromField);
                            ViewBag.RefFromFieldSortBy = RefSortBy;
                            break;
                        case "FromField asc":
                            ionRefDetail = lstRefDetail.OrderBy(m => m.FromField);
                            ViewBag.RefFromFieldSortBy = RefSortBy;
                            break;
                        case "ToField desc":
                            ionRefDetail = lstRefDetail.OrderByDescending(m => m.ToField);
                            ViewBag.RefToFieldSortBy = RefSortBy;
                            break;
                        case "ToField asc":
                            ionRefDetail = lstRefDetail.OrderBy(m => m.ToField);
                            ViewBag.RefToFieldSortBy = RefSortBy;
                            break;

                        case "CreationDate desc":
                            ionRefDetail = lstRefDetail.OrderByDescending(m => m.CreationDate);
                            ViewBag.RefCreationDateSortBy = RefSortBy;
                            break;
                        case "CreationDate asc":
                            ionRefDetail = lstRefDetail.OrderBy(m => m.CreationDate);
                            ViewBag.RefCreationDateSortBy = RefSortBy;
                            break;

                        case "Status desc":
                            ionRefDetail = lstRefDetail.OrderByDescending(m => m.Status);
                            ViewBag.RefStatusSortBy = RefSortBy;
                            break;
                        case "Status asc":
                            ionRefDetail = lstRefDetail.OrderBy(m => m.Status);
                            ViewBag.RefStatusSortBy = RefSortBy;
                            break;

                        default:
                            ionRefDetail = lstRefDetail.OrderByDescending(m => m.CreationDate);
                            ViewBag.RefCreationDateSortBy = "CreationDate desc";
                            break;
                    }


                    ViewBag.MsgSortBy = MsgSortBy;
                    switch (MsgSortBy)
                    {
                        case "FromField desc":
                            ionMsgDetail = lstMsgDetail.OrderByDescending(m => m.FromField);
                            ViewBag.MsgFromFieldSortBy = MsgSortBy;
                            break;
                        case "FromField asc":
                            ionMsgDetail = lstMsgDetail.OrderBy(m => m.FromField);
                            ViewBag.MsgFromFieldSortBy = MsgSortBy;
                            break;


                        case "CreationDate desc":
                            ionMsgDetail = lstMsgDetail.OrderByDescending(m => m.CreationDate);
                            ViewBag.MSgCreationDateSortBy = MsgSortBy;
                            break;
                        case "CreationDate asc":
                            ionMsgDetail = lstMsgDetail.OrderBy(m => m.CreationDate);
                            ViewBag.MsgCreationDateSortBy = MsgSortBy;
                            break;

                        case "Subject desc":
                            ionMsgDetail = lstMsgDetail.OrderByDescending(m => m.Body);
                            ViewBag.MsgSubjectSortBy = MsgSortBy;
                            break;
                        case "Subject asc":
                            ionMsgDetail = lstMsgDetail.OrderBy(m => m.Body);
                            ViewBag.MsgSubjectSortBy = MsgSortBy;
                            break;

                        default:
                            ionMsgDetail = lstMsgDetail.OrderByDescending(m => m.CreationDate);
                            ViewBag.MSgCreationDateSortBy = "CreationDate desc";
                            break;
                    }

                    ViewBag.MsgSentSortBy = MsgSentSortBy;
                    switch (MsgSentSortBy)
                    {

                        case "ToField asc":
                            ionSentMsgDetail = lstSentMsgDetail.OrderBy(n => n.ToField);
                            ViewBag.MsgSentToFieldSortBy = MsgSentSortBy;
                            break;


                        case "ToField desc":
                            ionSentMsgDetail = lstSentMsgDetail.OrderByDescending(n => n.ToField);
                            ViewBag.MsgSentToFieldSortBy = MsgSentSortBy;
                            break;



                        case "CreationDate desc":
                            ionSentMsgDetail = lstSentMsgDetail.OrderByDescending(m => m.CreationDate);
                            ViewBag.MSgSentCreationDateSortBy = MsgSentSortBy;
                            break;
                        case "CreationDate asc":
                            ionSentMsgDetail = lstSentMsgDetail.OrderBy(m => m.CreationDate);
                            ViewBag.MsgSentCreationDateSortBy = MsgSentSortBy;
                            break;

                        case "Subject desc":
                            ionSentMsgDetail = lstSentMsgDetail.OrderByDescending(m => m.Body);
                            ViewBag.MsgSentSubjectSortBy = MsgSentSortBy;
                            break;
                        case "Subject asc":
                            ionSentMsgDetail = lstSentMsgDetail.OrderBy(m => m.Body);
                            ViewBag.MsgSentSubjectSortBy = MsgSentSortBy;
                            break;

                        default:
                            ionSentMsgDetail = lstSentMsgDetail.OrderByDescending(m => m.CreationDate);
                            ViewBag.MSgSentCreationDateSortBy = "CreationDate desc";
                            break;
                    }


                    model.ReferralDetails = ionRefDetail.ToPagedList(currentRefPageIndex, RefdefaultPageSize);
                    model.Messages = ionMsgDetail.ToPagedList(currentMsgPageIndex, MsgdefaultPageSize);
                    DataTable dtCount = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0, 1, 100000);
                    int count = 0;
                    foreach (DataRow item in dtCount.Rows)
                    {
                        if (!Convert.ToBoolean(item["Read_flag"]))
                        {
                            count = count + 1;
                        }
                    }
                    @ViewBag.MsgCount = count;
                    model.PatientSentMsg = ionSentMsgDetail.ToPagedList(currentMsgSentPageIndex, MsgSentdefaultPageSize);

                    return View(model);
                }
            }
            else
            {
                return RedirectToAction("Index", "Index");
            }
        }

        public string PartialMessage(int currentMsgPageIndex, string MsgSortBy)
        {
           
            int MsgdefaultPageSize = 5;
            var model = new MyInboxMaster();

            List<Messages> lstMsgDetail = objRepository.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), SessionManagement.TimeZoneSystemName);
            IOrderedEnumerable<Messages> ionMsgDetail = lstMsgDetail.OrderByDescending(m => m.CreationDate);


            switch (MsgSortBy)
            {
                case "FromField desc":
                    ionMsgDetail = lstMsgDetail.OrderByDescending(m => m.FromField);
                    ViewBag.MsgFromFieldSortBy = MsgSortBy;
                    break;
                case "FromField asc":
                    ionMsgDetail = lstMsgDetail.OrderBy(m => m.FromField);
                    ViewBag.MsgFromFieldSortBy = MsgSortBy;
                    break;


                case "CreationDate desc":
                    ionMsgDetail = lstMsgDetail.OrderByDescending(m => m.CreationDate);
                    ViewBag.MSgCreationDateSortBy = MsgSortBy;
                    break;
                case "CreationDate asc":
                    ionMsgDetail = lstMsgDetail.OrderBy(m => m.CreationDate);
                    ViewBag.MsgCreationDateSortBy = MsgSortBy;
                    break;

                case "Subject desc":
                    ionMsgDetail = lstMsgDetail.OrderByDescending(m => m.Body);
                    ViewBag.MsgSubjectSortBy = MsgSortBy;
                    break;
                case "Subject asc":
                    ionMsgDetail = lstMsgDetail.OrderBy(m => m.Body);
                    ViewBag.MsgSubjectSortBy = MsgSortBy;
                    break;

                default:
                    ionMsgDetail = lstMsgDetail.OrderByDescending(m => m.CreationDate);
                    ViewBag.MSgCreationDateSortBy = "CreationDate desc";
                    break;
            }

            model.Messages = ionMsgDetail.ToPagedList(currentMsgPageIndex, MsgdefaultPageSize);

            if (model.Messages.Count == 0 && currentMsgPageIndex > 1)
            {
                return "<center>No more record found</center>";
            }
            else if (model.Messages.Count == 0 && currentMsgPageIndex == 1)
            {
                return "<center>No record found</center>";
            }
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, "_Message");
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
               
                return sw.GetStringBuilder().ToString();
            }

        }


        public string PartialSentMessage(int currentMsgSentPageIndex, string MsgSentSortBy)
        {
            int MsgsentdefaultPageSize = 5;
            var model = new MyInboxMaster();

            List<Sent> lstSentMsgDetail = objRepository.AllPatientSent(Convert.ToInt32(SessionManagement.PatientId), SessionManagement.TimeZoneSystemName);
            IOrderedEnumerable<Sent> ionSentMsgDetail = lstSentMsgDetail.OrderByDescending(m => m.CreationDate);



            switch (MsgSentSortBy)
            {

                case "ToField asc":
                    ionSentMsgDetail = lstSentMsgDetail.OrderBy(n => n.ToField);
                    ViewBag.MsgSentToFieldSortBy = MsgSentSortBy;
                    break;


                case "ToField desc":
                    ionSentMsgDetail = lstSentMsgDetail.OrderByDescending(n => n.ToField);
                    ViewBag.MsgSentToFieldSortBy = MsgSentSortBy;
                    break;



                case "CreationDate desc":
                    ionSentMsgDetail = lstSentMsgDetail.OrderByDescending(m => m.CreationDate);
                    ViewBag.MSgSentCreationDateSortBy = MsgSentSortBy;
                    break;
                case "CreationDate asc":
                    ionSentMsgDetail = lstSentMsgDetail.OrderBy(m => m.CreationDate);
                    ViewBag.MsgSentCreationDateSortBy = MsgSentSortBy;
                    break;

                case "Subject desc":
                    ionSentMsgDetail = lstSentMsgDetail.OrderByDescending(m => m.Body);
                    ViewBag.MsgSentSubjectSortBy = MsgSentSortBy;
                    break;
                case "Subject asc":
                    ionSentMsgDetail = lstSentMsgDetail.OrderBy(m => m.Body);
                    ViewBag.MsgSentSubjectSortBy = MsgSentSortBy;
                    break;

                default:
                    ionSentMsgDetail = lstSentMsgDetail.OrderByDescending(m => m.CreationDate);
                    ViewBag.MSgSentCreationDateSortBy = "CreationDate desc";
                    break;
            }

            model.PatientSentMsg = ionSentMsgDetail.ToPagedList(currentMsgSentPageIndex, MsgsentdefaultPageSize);

            if (model.PatientSentMsg.Count == 0 && MsgsentdefaultPageSize > 1)
            {
                return "<center>No more record found</center>";
            }
            else if (model.PatientSentMsg.Count == 0 && MsgsentdefaultPageSize == 1)
            {
                return "<center>No record found</center>";
            }
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, "_SentMsg");
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }


        public ActionResult DltRefMsg(int ReferralId)
        {
            bool x = objCommonDAL.DltRefMsg(ReferralId);
            TempData["DltRefMsg"] = true;

            return RedirectToAction("Index");
        }


        public JsonResult DltRefcheck(string Dltall)
        {
            object obj = null;

            try
            {
                if (Dltall != null && Dltall != "")
                {
                    string[] DltallID = Dltall.Split(',');
                    foreach (string str in DltallID)
                    {
                        if (str != null && Convert.ToInt32(str) != 0)
                        {
                            bool x = objCommonDAL.DltRefMsg(Convert.ToInt32(str));
                        }
                    }
                }


                TempData["DltRefMsg"] = true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            obj = new
            {
                Success = true,
            };

            return Json(obj, JsonRequestBehavior.DenyGet);
        }





        public ActionResult DltInboxMsg(int PatientmessageId)
        {
            bool x = objCommonDAL.EditPatientMessageReadStatus(PatientmessageId, "PatientPFlag");
            TempData["DltInboxMsg"] = true;
            TempData["DltMsg"] = true;
            return RedirectToAction("Index");
        }

        public string DeleteInboxMsg(int PatientmessageId)
        {
            try
            {
                bool x = objCommonDAL.EditPatientMessageReadStatus(PatientmessageId, "PatientPFlag");
                return x.ToString();
            }
            catch (Exception)
            {

                throw;
            }

        }

        public ActionResult DltSentMsg(int PatientmessageId)
        {
            bool x = objCommonDAL.dltsentmsgpatient(PatientmessageId);
            TempData["DltSentMsg"] = true;
            TempData["DltsendMsg"] = true;
            return RedirectToAction("Index");
        }

        public string DeleteSentMessage(int MessageId)
        {
            try
            {
                bool x = objCommonDAL.dltsentmsgpatient(MessageId);
                return x.ToString();
            }
            catch (Exception)
            {

                throw;
            }

        }


        public JsonResult DltMsgcheck(string Dltall)
        {
            object obj = null;

            try
            {
                if (Dltall != null && Dltall != "")
                {
                    string[] DltallID = Dltall.Split(',');
                    foreach (string str in DltallID)
                    {
                        if (str != null && Convert.ToInt32(str) != 0)
                        {
                            bool x = objCommonDAL.EditPatientMessageReadStatus(Convert.ToInt32(str), "PatientPFlag");
                        }
                    }
                }


                TempData["DltInboxMsg"] = true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            obj = new
            {
                Success = true,
            };

            return Json(obj, JsonRequestBehavior.DenyGet);
        }


        public JsonResult DltSentMsgcheck(string Dltall)
        {
            object obj = null;

            try
            {
                if (Dltall != null && Dltall != "")
                {
                    string[] DltallID = Dltall.Split(',');
                    foreach (string str in DltallID)
                    {
                        if (str != null && Convert.ToInt32(str) != 0)
                        {
                            bool x = objCommonDAL.dltsentmsgpatient(Convert.ToInt32(str));
                        }
                    }
                }


                TempData["DltSentMsg"] = true;

            }
            catch (Exception ex)
            {
                throw ex;
            }
            obj = new
            {
                Success = true,
            };

            return Json(obj, JsonRequestBehavior.DenyGet);
        }


        [HttpPost]
        public JsonResult MSgView(string PatientmessageId)
        {
            object obj = null;
            dt = objCommonDAL.GetAllMessages(0, Convert.ToInt32(PatientmessageId), 1, 5);
            if (dt != null && dt.Rows.Count > 0)
            {
                obj = new
                {
                    Success = true,
                    body = Convert.ToString(dt.Rows[0]["body"]),
                };
            }
            return Json(obj, JsonRequestBehavior.DenyGet);

        }




        [HttpPost]
        public JsonResult GetMessageAttachmentByPatientmessageId(string PatientmessageId, string Type)
        {
            object obj = null;
            string dynamicbody = "";
            string finalbody = "";
            string Doctor = "To:";
            string DoctorName = "";
            int DoctorIdReceived = 0;
            if (Type == "Receive")
            {
                Doctor = "From:";
            }


            DataTable dtmsg = objCommonDAL.GetAllMessages(0, Convert.ToInt32(PatientmessageId), 1, 5);
            if (dtmsg != null && dtmsg.Rows.Count > 0)
            {
                DoctorIdReceived = Convert.ToInt32(dtmsg.Rows[0]["DoctorId"]);
                if (dtmsg.Rows[0]["body"] != null && Convert.ToString(dtmsg.Rows[0]["body"]) != null)
                {
                    DoctorName = Convert.ToString(dtmsg.Rows[0]["tofield"]);
                    if (Doctor == "From:")
                    {
                        DoctorName = Convert.ToString(dtmsg.Rows[0]["fromfield"]);
                    }
                    var strCreationDate = Convert.ToString(dtmsg.Rows[0]["CreationDate"]);
                    if(!string.IsNullOrWhiteSpace(strCreationDate))
                    {
                        var dtCreationDate = Convert.ToDateTime(strCreationDate);
                        dtCreationDate = clsHelper.ConvertFromUTC(dtCreationDate, SessionManagement.TimeZoneSystemName);
                        strCreationDate = dtCreationDate.ToString();
                    }
                    finalbody = "<div><b style=color:#17335C>Date: </b>" + strCreationDate + "<br/><b style=color:#17335C>" + Doctor + " " + "</b>" + DoctorName + "<br/><br/>" + HttpUtility.UrlDecode(Convert.ToString(dtmsg.Rows[0]["body"])) + "</div>";
                }
                if (Type == "Receive")
                {
                    objCommonDAL.EditPatientMessageReadStatus(Convert.ToInt32(PatientmessageId), "Updatepatientrecievemessagestatus");
                }
            }
            dt = objCommonDAL.GetMessageAttachmentByPatientmessageId(Convert.ToInt32(PatientmessageId));

            if (dt != null && dt.Rows.Count > 0)
            {

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string DocumentName = Convert.ToString(dt.Rows[i]["attachmentname"]);
                    DocumentName = DocumentName.Substring(DocumentName.ToString().LastIndexOf("$") + 1);
                    if (dt.Rows[0]["attachmentname"] != null && Convert.ToString(dt.Rows[i]["attachmentname"]) != "")
                    {
                        int j = i + 1;
                        if (dynamicbody == "")
                        {
                            string Docname = Convert.ToString(dt.Rows[i]["attachmentname"]);
                            //Docname = Docname.Replace(" ", "%20");
                            Docname = clsHelper.EscapeUriString(Docname);
                            dynamicbody = "<div class=ViewMessageAttachmentDiv ><br/><b>Attachments:<b><br/>";
                            dynamicbody = dynamicbody + "<a onclick=OpenDocument('" + Docname + "') >" + j + "." + DocumentName + "</a> <br/>";


                        }
                        else
                        {
                            string Docname = Convert.ToString(dt.Rows[i]["attachmentname"]);
                            Docname = clsHelper.EscapeUriString(Docname);
                            Docname = Docname.Replace(" ", "%20");
                            dynamicbody = dynamicbody + "<a onclick=OpenDocument('" + Docname + "') >" + j + "." + DocumentName + "</a> <br/>";
                        }

                    }
                    DocumentName = "";
                }
                dynamicbody = dynamicbody + "</div>";
                finalbody = finalbody + dynamicbody;


            }
            obj = new
            {
                Success = true,
                body = finalbody,
                DoctorId = DoctorIdReceived,

            };


            return Json(obj, JsonRequestBehavior.DenyGet);

        }

    }
}
