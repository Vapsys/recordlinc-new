﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using DentalFiles.Models;
using System.Configuration;
using DentalFiles.App_Start;

namespace DentalFilesNew.Controllers
{
    public class SignupController : Controller
    {
        CommonDAL objCommonDAL = new CommonDAL();

        DataTable dt = new DataTable();
        public ActionResult Index()
        {
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                return RedirectToAction("Index", "DashBoard");
            }
            else
            {
                SessionManagement.PatientId = 0;
                ViewBag.Sending = objCommonDAL.GetSpeciality();
                ViewBag.Distance = objCommonDAL.GetDistance();
                objCommonDAL.GetActiveCompany();
                ViewBag.ReturnUrl = "Index";
                return View();
            }

        }

        [HttpPost]
        public ActionResult GetPara(FormCollection objFormCollection)
        {
            Session["_ZipCodeOrCity"] = objFormCollection["CityOrZip"];
            Session["_LastName"] = objFormCollection["Lastname"];
            Session["_Speciality"] = objFormCollection["Speciality"];
            Session["_OnlyCity"] = null;
            return RedirectToAction("Index", "searchresult");
        }

        [HttpPost]
        public ActionResult CreateSession(FormCollection objFormCollection)
        {
            Session["Firstnamepatient"] = objFormCollection["Firstname"];
            Session["Lastnamepatient"] = objFormCollection["Lastname"];
            Session["Emailpatient"] = objFormCollection["Email"];
            Session["PhoneNoPatient"] = objFormCollection["PhoneNo"];

            return RedirectToAction("Index", "searchresult");
        }

        [HttpPost]
        public ActionResult RequestInfo(RequiredFieldForReqInfo objRequiredFieldForReqInfo, string returnUrl)
        {

            if (ModelState.IsValid)
            {
                var txtfirstname = objRequiredFieldForReqInfo.Firstname.Trim();
                var txtlastname = objRequiredFieldForReqInfo.Lastname.Trim();
                var txtphoneno = objRequiredFieldForReqInfo.PhoneNo.Trim();
                var txtemail = objRequiredFieldForReqInfo.Email.Trim();
                var txtcomment = objRequiredFieldForReqInfo.Comment;
                bool status = objCommonDAL.SendContactMessage(txtfirstname + " " + txtlastname, txtemail, txtphoneno, txtcomment);
                if (status = true && SessionManagement.CompanyName != null && Convert.ToString(SessionManagement.CompanyName) != "")
                {
                    TempData["ReqInfomessage"] = "Request received at " + SessionManagement.CompanyName.ToString();
                }

            }
            return RedirectToAction("Index", returnUrl);
        }


    }
}
