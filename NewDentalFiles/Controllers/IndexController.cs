﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using DentalFiles.Models;
using System.Configuration;
using DentalFiles.App_Start;
using DataAccessLayer;
using DataAccessLayer.Common;
namespace DentalFiles.Controllers
{
    public class IndexController : Controller
    {
        CommonDAL objCommonDAL = new CommonDAL();

        DataTable dt = new DataTable();
        public ActionResult Index()
        {
            try
            {
                if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
                {
                    objCommonDAL.InsertErrorLog("UserLoged in", "", "");
                    return RedirectToAction("Index", "DashBoard");

                }
                else
                {
                    SessionManagement.PatientId = 0;
                    ViewBag.Sending = objCommonDAL.GetSpeciality();
                    ViewBag.Distance = objCommonDAL.GetDistance();
                    objCommonDAL.GetActiveCompany();
                    ViewBag.ReturnUrl = "Index";
                    TempData.Keep();
                    objCommonDAL.InsertErrorLog("User not Loged  in", "", "");
                    return View("NewIndex");
                }
            }
            catch (Exception e)
            {
                objCommonDAL.InsertErrorLog("Exception block", e.Message, e.StackTrace);
                throw;
            }


        }

        [HttpPost]
        public ActionResult GetPara(FormCollection objFormCollection)
        {
            Session["_ZipCodeOrCity"] = objFormCollection["CityOrZip"].Trim();
            Session["_LastName"] = objFormCollection["Lastname"].Trim();
            Session["_Speciality"] = objFormCollection["Speciality"];
            Session["_OnlyCity"] = null;
            return RedirectToAction("Index", "searchresult");
        }

        [HttpPost]
        public ActionResult CreateSession(FormCollection objFormCollection)
        {
            Session["Firstnamepatient"] = objFormCollection["Firstname"];
            Session["Lastnamepatient"] = objFormCollection["Lastname"];
            Session["Emailpatient"] = objFormCollection["Email"];
            Session["PhoneNoPatient"] = objFormCollection["PhoneNo"];

            return RedirectToAction("Index", "searchresult");
        }

        [HttpPost]
        public ActionResult RequestInfo(RequiredFieldForReqInfo objRequiredFieldForReqInfo, string returnUrl)
        {
            clsCommon OnjCommon = new clsCommon();
            if (ModelState.IsValid)
            {
                try
                {
                    var txtfirstname = objRequiredFieldForReqInfo.Firstname.Trim();
                    var txtlastname = objRequiredFieldForReqInfo.Lastname.Trim();
                    var txtphoneno = objRequiredFieldForReqInfo.PhoneNo.Trim();
                    var txtemail = objRequiredFieldForReqInfo.Email.Trim();
                    var txtcomment = objRequiredFieldForReqInfo.Comment;
                    bool status = objCommonDAL.SendContactMessage(txtfirstname + " " + txtlastname, txtemail, txtphoneno, txtcomment);
                    if (status = true && SessionManagement.CompanyName != null && Convert.ToString(SessionManagement.CompanyName) != "")
                    {

                        TempData["ReqInfomessage"] = "Request received at " + SessionManagement.CompanyName.ToString();
                    }

                }
                catch (Exception)
                {

                    throw;
                }
            }
            return RedirectToAction("Index", returnUrl);
        }
        public ActionResult NewIndex()
        {
            try
            {
                if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
                {
                    objCommonDAL.InsertErrorLog("UserLoged in", "", "");
                    return RedirectToAction("Index", "DashBoard");

                }
                else
                {
                    SessionManagement.PatientId = 0;
                    ViewBag.Sending = objCommonDAL.GetSpeciality();
                    ViewBag.Distance = objCommonDAL.GetDistance();
                    objCommonDAL.GetActiveCompany();
                    ViewBag.ReturnUrl = "Index";
                    TempData.Keep();
                    objCommonDAL.InsertErrorLog("User not Loged  in", "", "");
                    return View();
                }
            }
            catch (Exception e)
            {
                objCommonDAL.InsertErrorLog("Exception block", e.Message, e.StackTrace);
                throw;
            }
        }
    }
}
