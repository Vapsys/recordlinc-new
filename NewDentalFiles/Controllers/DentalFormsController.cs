﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using DentalFiles.Models;
using System.Configuration;
using JQueryDataTables.Models.Repository;
using DentalFiles.Models.ViewModel;
using System.Web.UI;
using DentalFiles.App_Start;
using System.Text;
using DataAccessLayer.Common;
namespace DentalFiles.Controllers
{

    public class DentalFormsController : Controller
    {

        int PatientId = 0;
        CommonDAL objCommonDAL = new CommonDAL();
        DataRepository objRepository = new DataRepository();
        List<BasicInfo> objBasicInfo = new List<BasicInfo>();
        private IList<BasicInfo> allEmployee = new List<BasicInfo>();
        public ActionResult Index()
        {



            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                List<PatientDetails> PatientDetialList = objRepository.GetPatientDetails(Convert.ToInt32(SessionManagement.PatientId), SessionManagement.TimeZoneSystemName);
                if (PatientDetialList != null && PatientDetialList.Count != 0)
                {
                    ViewBag.FirstName = PatientDetialList[0].FirstName;
                    if (!string.IsNullOrEmpty(PatientDetialList[0].ProfileImage.ToString()))
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["imagebankpath"] + PatientDetialList[0].ProfileImage.ToString();
                    }
                    else if (PatientDetialList[0].Gender == 1)
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"];

                    }
                    else
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["Doctorimage1"];
                    }
                }
                IList<BasicInfo> employees = this.allEmployee;
                objCommonDAL.GetActiveCompany();
                ViewBag.PatientId = PatientId = Convert.ToInt32(SessionManagement.PatientId);
                ViewBag.Title = "DentalForms";
                ViewBag.ReturnUrl = "DentalForms";

                var model = getallvalue();
                ViewBag.Gender = objCommonDAL.GetGender();
                ViewBag.Status = objCommonDAL.GetStatus();
                Messages objmsg = new Messages();
                DataTable dt = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0, 1, 5);


                DataTable dtCount = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0, 1, 100000);
                int count = 0;
                foreach (DataRow item in dtCount.Rows)
                {
                    if (!Convert.ToBoolean(item["Read_flag"]))
                    {
                        count = count + 1;
                    }
                }
                @ViewBag.MsgCount = count;
                return PartialView(model);


            }
            else
            {
                return RedirectToAction("Index", "Index");
            }
        }

        private DentalFormsMaster getallvalue()
        {
            var model = new DentalFormsMaster();
            model.BasicInfo = objRepository.GetPatientBasicInfo(PatientId, SessionManagement.TimeZoneSystemName);
            model.RegistrationForm = objRepository.GetRegistration(PatientId);
            model.ChildDentalMEDICALHISTORY = objRepository.GetChildHistory(PatientId);
            model.DentalHistory = objRepository.GetDentalHistory(PatientId);
            model.MedicalHistory = objRepository.GetMedicalHistory(PatientId);
            model.MedicalHISTORYUpdate = objRepository.GetMedicalUpdate(PatientId);
            model.Review = objRepository.PatientFormsSent(PatientId);

            return model;
        }

        [HttpPost]
        public ActionResult BasicInfo(string txtFirstName, string txtLastName, string txtEmail, string txtpassword, string hdnprimaryemail)
        {

            object obj = null;
            bool Isprimaryupdate = true;
            string IsprimaryAddressUpdate = "";
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                DataTable dtPatientEmail = objCommonDAL.CheckPatientEmail(txtEmail);
                if (dtPatientEmail != null && dtPatientEmail.Rows.Count > 0)
                {
                    if (dtPatientEmail.Rows[0]["Email"] != null && Convert.ToString(dtPatientEmail.Rows[0]["Email"]) != "")
                    {
                        if (Convert.ToString(dtPatientEmail.Rows[0]["Email"]) == hdnprimaryemail)
                        {
                            bool x = objCommonDAL.UpdatePatientEmailAddress(SessionManagement.PatientId, txtEmail);
                            Isprimaryupdate = true;
                            bool boolpatient = objCommonDAL.UpdatePatientBasicForm(Convert.ToInt32(SessionManagement.PatientId), "UpdatePatient", txtFirstName.Trim(), txtLastName.Trim(), "0", "");
                            var model = getallvalue();
                        }
                        else
                        {
                            IsprimaryAddressUpdate = "Email Address Already Exists";
                            Isprimaryupdate = false;
                        }

                    }
                    else
                    {

                        bool x = objCommonDAL.UpdatePatientEmailAddress(SessionManagement.PatientId, txtEmail);
                        Isprimaryupdate = true;
                        bool boolpatient = objCommonDAL.UpdatePatientBasicForm(Convert.ToInt32(SessionManagement.PatientId), "UpdatePatient", txtFirstName.Trim(), txtLastName.Trim(), "0", "");
                        var model = getallvalue();
                    }


                }
                else
                {

                    bool x = objCommonDAL.UpdatePatientEmailAddress(SessionManagement.PatientId, txtEmail);
                    Isprimaryupdate = true;
                    bool boolpatient = objCommonDAL.UpdatePatientBasicForm(Convert.ToInt32(SessionManagement.PatientId), "UpdatePatient", txtFirstName.Trim(), txtLastName.Trim(), "0", "");
                    var model = getallvalue();
                }

                obj = new
                {
                    Success = true,
                    body = "",
                    Isprimaryupdate = Isprimaryupdate,
                    IsprimaryAddressUpdate = IsprimaryAddressUpdate,


                };
                return Json(obj, JsonRequestBehavior.DenyGet);


            }
            else
            {
                return RedirectToAction("Index", "Index");
            }

        }


        [HttpPost]
        public ActionResult GuardianInfo(string txtGuarFirstName, string txtGuarLastName, string txtGuarPhoneNum, string txtGuarEmail)
        {

            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                string PatientResult = objCommonDAL.UpdatePatientGuardian(Convert.ToInt32(SessionManagement.PatientId), txtGuarFirstName.Trim(), txtGuarLastName, txtGuarPhoneNum, txtGuarEmail);
                var model = getallvalue();
                return PartialView("DentalForms/_NoticeOfPrivacy", model);
            }
            else
            {
                return RedirectToAction("Index", "Index");
            }

        }

        [HttpPost]
        public JsonResult RegistrationForm(string txtDob, string ddlgenderreg, string drpStatus, string txtResidenceStreet, string txtCity, string txtState, string txtZip, string txtResidenceTelephone, string txtWorkPhone, string txtFax, string txtResponsiblepartyFname, string txtResponsiblepartyLname, string txtResponsibleRelationship, string txtResponsibleAddress, string txtResponsibleCity, string txtResponsibleState, string txtResponsibleZipCode, string txtResponsibleDOB, string txtResponsibleContact, string txtWhommay, string txtemergencyname, string txtemergency, string chkInsurance, string chkCash, string chkCrediteCard, string txtEmployeeName1, string txtInsurancePhone1, string txtEmployeeDob1, string txtEmployerName1, string txtYearsEmployed1, string txtNameofInsurance1, string txtInsuranceAddress1, string txtInsuranceTelephone1, string txtEmployeeName2, string txtInsurancePhone2, string txtEmployeeDob2, string txtEmployerName2, string txtYearsEmployed2, string txtNameofInsurance2, string txtInsuranceAddress2, string txtInsuranceTelephone2)
        {
            if (txtDob == null || txtDob == "")
            {
                txtDob = "1900-01-01 00:00:00.000";
            }
            DateTime DateOfBirth = clsHelper.ConvertToUTC(Convert.ToDateTime(txtDob), SessionManagement.TimeZoneSystemName);
            object obj = null;
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                try
                {
                    // Dental History Table
                    DataTable tblRegistration = new DataTable("Registration");
                    tblRegistration.Columns.Add("txtResponsiblepartyFname");
                    tblRegistration.Columns.Add("txtResponsiblepartyLname");
                    tblRegistration.Columns.Add("txtResponsibleRelationship");
                    tblRegistration.Columns.Add("txtResponsibleAddress");
                    tblRegistration.Columns.Add("txtResponsibleCity");
                    tblRegistration.Columns.Add("txtResponsibleState");
                    tblRegistration.Columns.Add("txtResponsibleZipCode");
                    tblRegistration.Columns.Add("txtResponsibleDOB");
                    tblRegistration.Columns.Add("txtResponsibleContact");
                    tblRegistration.Columns.Add("txtPatientPresentPosition");
                    tblRegistration.Columns.Add("txtPatientHowLongHeld");
                    tblRegistration.Columns.Add("txtParentPresentPosition");
                    tblRegistration.Columns.Add("txtParentHowLongHeld");
                    tblRegistration.Columns.Add("txtDriversLicense");
                    tblRegistration.Columns.Add("chkMethodOfPayment");
                    tblRegistration.Columns.Add("txtPurposeCall");
                    tblRegistration.Columns.Add("txtOtherFamily");
                    tblRegistration.Columns.Add("txtWhommay");
                    tblRegistration.Columns.Add("txtSomeonetonotify");
                    tblRegistration.Columns.Add("txtemergency");
                    tblRegistration.Columns.Add("txtemergencyname");
                    tblRegistration.Columns.Add("txtEmployeeName1");
                    tblRegistration.Columns.Add("txtInsurancePhone1");
                    tblRegistration.Columns.Add("txtEmployeeDob1");
                    tblRegistration.Columns.Add("txtEmployerName1");
                    tblRegistration.Columns.Add("txtYearsEmployed1");
                    tblRegistration.Columns.Add("txtNameofInsurance1");
                    tblRegistration.Columns.Add("txtInsuranceAddress1");
                    tblRegistration.Columns.Add("txtInsuranceTelephone1");
                    tblRegistration.Columns.Add("txtProgramorpolicy1");
                    tblRegistration.Columns.Add("txtSocialSecurity1");
                    tblRegistration.Columns.Add("txtUnionLocal1");
                    tblRegistration.Columns.Add("txtEmployeeName2");
                    tblRegistration.Columns.Add("txtInsurancePhone2");
                    tblRegistration.Columns.Add("txtEmployeeDob2");
                    tblRegistration.Columns.Add("txtEmployerName2");
                    tblRegistration.Columns.Add("txtYearsEmployed2");
                    tblRegistration.Columns.Add("txtNameofInsurance2");
                    tblRegistration.Columns.Add("txtInsuranceAddress2");
                    tblRegistration.Columns.Add("txtInsuranceTelephone2");
                    tblRegistration.Columns.Add("txtProgramorpolicy2");
                    tblRegistration.Columns.Add("txtSocialSecurity2");
                    tblRegistration.Columns.Add("txtUnionLocal2");
                    tblRegistration.Columns.Add("txtDigiSign");
                    StringBuilder chkMethodOfPayment = new StringBuilder();










                    if (chkCash == "1")
                    {
                        if (!String.IsNullOrEmpty(chkMethodOfPayment.ToString()))
                        {
                            chkMethodOfPayment.Append("," + "Cash");
                        }
                        else
                        {
                            chkMethodOfPayment.Append("Cash");
                        }
                    }
                    if (chkInsurance == "1")
                    {
                        if (!String.IsNullOrEmpty(chkMethodOfPayment.ToString()))
                        {
                            chkMethodOfPayment.Append("," + "Insurance");
                        }
                        else
                        {
                            chkMethodOfPayment.Append("Insurance");
                        }
                    }
                    if (chkCrediteCard == "1")
                    {
                        if (!String.IsNullOrEmpty(chkMethodOfPayment.ToString()))
                        {
                            chkMethodOfPayment.Append("," + "Credit Card");
                        }
                        else
                        {
                            chkMethodOfPayment.Append("Credit Card");
                        }
                    }

                    tblRegistration.Columns.Add("CreatedDate");
                    tblRegistration.Columns.Add("ModifiedDate");


                    tblRegistration.Rows.Add(txtResponsiblepartyFname, txtResponsiblepartyLname, txtResponsibleRelationship, txtResponsibleAddress, txtResponsibleCity, txtResponsibleState,
                txtResponsibleZipCode, txtResponsibleDOB, txtResponsibleContact, "", "", "", "", "", chkMethodOfPayment.ToString(), "", "", txtWhommay, "", txtemergency, txtemergencyname, txtEmployeeName1, txtInsurancePhone1,
                txtEmployeeDob1, txtEmployerName1, txtYearsEmployed1, txtNameofInsurance1, txtInsuranceAddress1, txtInsuranceTelephone1, "", "", "", txtEmployeeName2, txtInsurancePhone2, txtEmployeeDob2,
                txtEmployerName2, txtYearsEmployed2, txtNameofInsurance2, txtInsuranceAddress2, txtInsuranceTelephone2, "", "", "", "");




                    DataSet dsRegistration = new DataSet("Registration");
                    dsRegistration.Tables.Add(tblRegistration);

                    DataSet ds = dsRegistration.Copy();
                    dsRegistration.Clear();

                    string Registration = ds.GetXml();

                    bool status = objCommonDAL.UpdateRegistrationForm(Convert.ToInt32(SessionManagement.PatientId), "UpdatePatient", DateOfBirth, ddlgenderreg,
                        drpStatus, txtResidenceStreet, txtCity, txtState, txtZip,
                         txtResidenceTelephone, txtWorkPhone, txtFax, Registration);

                }
                catch (Exception ex)
                {
                    throw ex;
                }

                var model = getallvalue();

                obj = new
                {
                    Success = true,
                    Message = "Detailed Info Form Updated successfully",
                    Html = this.PartialView("DentalForms/_Insurance_Coverage", model)
                };
                return Json(obj, JsonRequestBehavior.DenyGet);

            }
            else
            {

                return Json(new
                {
                    redirectUrl = Url.Action("Index", "Index"),
                    isRedirect = true
                });

            }

        }

        [HttpPost]
        public ActionResult ChildHISTORYForm(string txtDob, string rdDentalQue1, string txtDentalQue2, string rdDentalQue3, string rdDentalQue5, string chkDentalQue6_1, string chkDentalQue6_2, string chkDentalQue6_3, string chkDentalQue6_4, string chkDentalQue7_1, string chkDentalQue7_2, string chkDentalQue7_3, string chkDentalQue7_4, string rdDentalQue8, string rdDentalQue9, string rdDentalQue10, string rdDentalQue11, string txtDentalQue11a, string rdDentalQue12, string rdDentalQue13, string rdDentalQue14, string rdDentalQue15, string rdDentalQue16, string rdMedicalQue1, string txtrdMedicalQue1, string rdMedicalQue2, string txtMedicalQue2a, string txtMedicalQue3, string txtMedicalQue3b, string rdMedicalQue4, string txtMedicalQue4a, string rdMedicalQue5, string rdMedicalQue6, string rdMedicalQue7, string rdMedicalQue8, string txtMedicalQue8a, string txtMedicalQue8b, string rdMedicalQue9, string rdMedicalQue10, string rdMedicalQue11, string rdMedicalQue12, string rdMedicalQue13, string rdMedicalQue14, string rdMedicalQue15, string chkMedicalQue15a_1, string chkMedicalQue15a_2, string chkMedicalQue15a_3, string chkMedicalQue15a_4, string rdMedicalQue16, string chkMedicalQue17_1, string chkMedicalQue17_2, string chkMedicalQue17_3, string chkMedicalQue17_4, string chkMedicalQue17_5, string chkMedicalQue17_6, string chkMedicalQue17_7, string chkMedicalQue17_8, string chkMedicalQue17_9, string chkMedicalQue17_10, string chkMedicalQue17_11, string chkMedicalQue17_12, string chkMedicalQue17_13, string chkMedicalQue17_14, string chkMedicalQue17_15, string txtComments, string txtDigiSign)
        {
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                try
                {
                    int age = 0;
                    if (txtDob == null || txtDob == "")
                    {
                        age = 17;
                    }
                    else
                    {
                        DateTime dateOfBirth = Convert.ToDateTime(txtDob);
                        var today = DateTime.Today;
                        var a = (today.Year * 100 + today.Month) * 100 + today.Day;
                        var b = (dateOfBirth.Year * 100 + dateOfBirth.Month) * 100 + dateOfBirth.Day;
                        age = (a - b) / 10000;
                    }
                    if (age < 18)
                    {

                        // Dental History Table
                        DataTable tblDentalHistory = new DataTable("DentalHistory");
                        tblDentalHistory.Columns.Add("rdDentalQue1");
                        tblDentalHistory.Columns.Add("txtDentalQue2");
                        tblDentalHistory.Columns.Add("rdDentalQue3");
                        tblDentalHistory.Columns.Add("rdDentalQue4");
                        tblDentalHistory.Columns.Add("rdDentalQue5");
                        tblDentalHistory.Columns.Add("chkDentalQue6");
                        StringBuilder rdDentalQue6 = new StringBuilder();




                        if (chkDentalQue6_1 == "1")
                        {
                            if (!String.IsNullOrEmpty(rdDentalQue6.ToString()))
                            {
                                rdDentalQue6.Append("," + "1");
                            }
                            else
                            {
                                rdDentalQue6.Append("1");
                            }
                        }
                        if (chkDentalQue6_2 == "1")
                        {
                            if (!String.IsNullOrEmpty(rdDentalQue6.ToString()))
                            {
                                rdDentalQue6.Append("," + "2");
                            }
                            else
                            {
                                rdDentalQue6.Append("2");
                            }
                        }
                        if (chkDentalQue6_3 == "1")
                        {
                            if (!String.IsNullOrEmpty(rdDentalQue6.ToString()))
                            {
                                rdDentalQue6.Append("," + "3");
                            }
                            else
                            {
                                rdDentalQue6.Append("3");
                            }
                        }

                        if (chkDentalQue6_4 == "1")
                        {
                            if (!String.IsNullOrEmpty(rdDentalQue6.ToString()))
                            {
                                rdDentalQue6.Append("," + "4");
                            }
                            else
                            {
                                rdDentalQue6.Append("4");
                            }
                        }



                        tblDentalHistory.Columns.Add("chkDentalQue7");
                        StringBuilder rdDentalQue7 = new StringBuilder();




                        if (chkDentalQue7_1 == "1")
                        {
                            if (!String.IsNullOrEmpty(rdDentalQue7.ToString()))
                            {
                                rdDentalQue7.Append("," + "1");
                            }
                            else
                            {
                                rdDentalQue6.Append("1");
                            }
                        }
                        if (chkDentalQue7_2 == "1")
                        {
                            if (!String.IsNullOrEmpty(rdDentalQue7.ToString()))
                            {
                                rdDentalQue6.Append("," + "2");
                            }
                            else
                            {
                                rdDentalQue6.Append("2");
                            }
                        }
                        if (chkDentalQue7_3 == "1")
                        {
                            if (!String.IsNullOrEmpty(rdDentalQue7.ToString()))
                            {
                                rdDentalQue6.Append("," + "3");
                            }
                            else
                            {
                                rdDentalQue6.Append("3");
                            }
                        }

                        if (chkDentalQue7_4 == "1")
                        {
                            if (!String.IsNullOrEmpty(rdDentalQue7.ToString()))
                            {
                                rdDentalQue6.Append("," + "4");
                            }
                            else
                            {
                                rdDentalQue6.Append("4");
                            }
                        }
                        tblDentalHistory.Columns.Add("txtDentalQue7a");
                        tblDentalHistory.Columns.Add("txtDentalQue7b");
                        tblDentalHistory.Columns.Add("rdDentalQue8");
                        tblDentalHistory.Columns.Add("rdDentalQue9");
                        tblDentalHistory.Columns.Add("rdDentalQue10");
                        tblDentalHistory.Columns.Add("rdDentalQue10a");
                        tblDentalHistory.Columns.Add("rdDentalQue10b");
                        tblDentalHistory.Columns.Add("rdDentalQue11");
                        tblDentalHistory.Columns.Add("txtDentalQue11a");
                        tblDentalHistory.Columns.Add("rdDentalQue12");
                        tblDentalHistory.Columns.Add("rdDentalQue13");
                        tblDentalHistory.Columns.Add("rdDentalQue14");
                        tblDentalHistory.Columns.Add("rdDentalQue15");
                        tblDentalHistory.Columns.Add("rdDentalQue16");

                        tblDentalHistory.Rows.Add(rdDentalQue1, txtDentalQue2, rdDentalQue3, "",
                            rdDentalQue5, rdDentalQue6.ToString(), rdDentalQue7.ToString(), "", "", rdDentalQue8,
                            rdDentalQue9, rdDentalQue10, "", "",
                            rdDentalQue11, txtDentalQue11a, rdDentalQue12, rdDentalQue13, rdDentalQue14,
                            rdDentalQue15, rdDentalQue16);




                        // Medical History Table
                        DataTable tblMedicalHistory = new DataTable("MedicalHistory");
                        tblMedicalHistory.Columns.Add("rdMedicalQue1");
                        tblMedicalHistory.Columns.Add("txtrdMedicalQue1");
                        tblMedicalHistory.Columns.Add("rdMedicalQue2");
                        tblMedicalHistory.Columns.Add("txtMedicalQue2a");
                        tblMedicalHistory.Columns.Add("txtMedicalQue3");
                        tblMedicalHistory.Columns.Add("txtMedicalQue3b");
                        tblMedicalHistory.Columns.Add("rdMedicalQue4");
                        tblMedicalHistory.Columns.Add("txtMedicalQue4a");
                        tblMedicalHistory.Columns.Add("rdMedicalQue5");
                        tblMedicalHistory.Columns.Add("rdMedicalQue6");
                        tblMedicalHistory.Columns.Add("rdMedicalQue7");
                        tblMedicalHistory.Columns.Add("rdMedicalQue8");
                        tblMedicalHistory.Columns.Add("txtMedicalQue8a");
                        tblMedicalHistory.Columns.Add("txtMedicalQue8b");
                        tblMedicalHistory.Columns.Add("rdMedicalQue9");
                        tblMedicalHistory.Columns.Add("rdMedicalQue10");
                        tblMedicalHistory.Columns.Add("rdMedicalQue11");
                        tblMedicalHistory.Columns.Add("rdMedicalQue12");
                        tblMedicalHistory.Columns.Add("rdMedicalQue13");
                        tblMedicalHistory.Columns.Add("rdMedicalQue14");

                        tblMedicalHistory.Columns.Add("rdMedicalQue15");
                        tblMedicalHistory.Columns.Add("chkMedicalQue15a");
                        StringBuilder srdMedicalQue15 = new StringBuilder();

                        if (chkMedicalQue15a_1 == "Y")
                        {
                            if (!String.IsNullOrEmpty(srdMedicalQue15.ToString()))
                            {
                                srdMedicalQue15.Append("," + "1");
                            }
                            else
                            {
                                srdMedicalQue15.Append("1");
                            }
                        }
                        if (chkMedicalQue15a_2 == "Y")
                        {
                            if (!String.IsNullOrEmpty(srdMedicalQue15.ToString()))
                            {
                                srdMedicalQue15.Append("," + "2");
                            }
                            else
                            {
                                srdMedicalQue15.Append("2");
                            }
                        }
                        if (chkMedicalQue15a_3 == "Y")
                        {
                            if (!String.IsNullOrEmpty(srdMedicalQue15.ToString()))
                            {
                                srdMedicalQue15.Append("," + "3");
                            }
                            else
                            {
                                srdMedicalQue15.Append("3");
                            }
                        }

                        if (chkMedicalQue15a_4 == "Y")
                        {
                            if (!String.IsNullOrEmpty(srdMedicalQue15.ToString()))
                            {
                                srdMedicalQue15.Append("," + "4");
                            }
                            else
                            {
                                srdMedicalQue15.Append("4");
                            }
                        }

                        tblMedicalHistory.Columns.Add("rdMedicalQue16");
                        tblMedicalHistory.Columns.Add("chkMedicalQue17");
                        StringBuilder schkMedicalQue17 =new StringBuilder();



                        if (chkMedicalQue17_1 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "1");
                            }
                            else
                            {
                                schkMedicalQue17.Append("1");
                            }
                        }
                        if (chkMedicalQue17_2 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "2");
                            }
                            else
                            {
                                schkMedicalQue17.Append("2");
                            }
                        }
                        if (chkMedicalQue17_3 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "3");
                            }
                            else
                            {
                                schkMedicalQue17.Append("3");
                            }
                        }

                        if (chkMedicalQue17_4 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "4");
                            }
                            else
                            {
                                schkMedicalQue17.Append("4");
                            }
                        }

                        if (chkMedicalQue17_5 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "5");
                            }
                            else
                            {
                                schkMedicalQue17.Append("5");
                            }
                        }
                        if (chkMedicalQue17_6 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "6");
                            }
                            else
                            {
                                schkMedicalQue17.Append("6");
                            }
                        }
                        if (chkMedicalQue17_7 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "7");
                            }
                            else
                            {
                                schkMedicalQue17.Append("7");
                            }
                        }
                        if (chkMedicalQue17_8 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "8");
                            }
                            else
                            {
                                schkMedicalQue17.Append("8");
                            }
                        }
                        if (chkMedicalQue17_9 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "9");
                            }
                            else
                            {
                                schkMedicalQue17.Append("9");
                            }
                        }
                        if (chkMedicalQue17_10 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "10");
                            }
                            else
                            {
                                schkMedicalQue17.Append("10");
                            }
                        }
                        if (chkMedicalQue17_11 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "11");
                            }
                            else
                            {
                                schkMedicalQue17.Append("11");
                            }
                            if (chkMedicalQue17_12 == "1")
                            {
                                if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                                {
                                    schkMedicalQue17.Append("," + "12");
                                }
                                else
                                {
                                    schkMedicalQue17.Append("12");
                                }
                            }
                            if (chkMedicalQue17_13 == "Y")
                            {
                                if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                                {
                                    schkMedicalQue17.Append("," + "13");
                                }
                                else
                                {
                                    schkMedicalQue17.Append("13");
                                }
                            }
                            if (chkMedicalQue17_14 == "Y")
                            {
                                if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                                {
                                    schkMedicalQue17.Append("," + "14");
                                }
                                else
                                {
                                    schkMedicalQue17.Append("14");
                                }
                            }
                            if (chkMedicalQue17_15 == "Y")
                            {
                                if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                                {
                                    schkMedicalQue17.Append("," + "15");
                                }
                                else
                                {
                                    schkMedicalQue17.Append("15");
                                }
                            }

                        }


                        tblMedicalHistory.Columns.Add("txtComments");
                        tblMedicalHistory.Columns.Add("txtDigiSign");
                        tblMedicalHistory.Columns.Add("CreatedDate");
                        tblMedicalHistory.Columns.Add("ModifiedDate");

                        tblMedicalHistory.Rows.Add(rdMedicalQue1, txtrdMedicalQue1, rdMedicalQue2, txtMedicalQue2a, txtMedicalQue3, txtMedicalQue3b,
                           rdMedicalQue4, txtMedicalQue4a, rdMedicalQue5, rdMedicalQue6, rdMedicalQue7,
                           rdMedicalQue8, txtMedicalQue8a, txtMedicalQue8b, rdMedicalQue9, rdMedicalQue10,
                           rdMedicalQue11, rdMedicalQue12, rdMedicalQue13, rdMedicalQue14, rdMedicalQue15,
                           srdMedicalQue15.ToString(), rdMedicalQue16, schkMedicalQue17.ToString(), txtComments, txtDigiSign, Convert.ToString(System.DateTime.Now), Convert.ToString(System.DateTime.Now));



                        //Join Two Table
                        DataSet dsChildHistory = new DataSet("ChildHistory");
                        dsChildHistory.Tables.Add(tblDentalHistory);
                        dsChildHistory.Tables.Add(tblMedicalHistory);

                        DataSet ds = dsChildHistory.Copy();
                        dsChildHistory.Clear();

                        string ChildDentalMedicalHistory = ds.GetXml();

                        bool status = objCommonDAL.UpdateChildMedicalDentalForm(Convert.ToInt32(SessionManagement.PatientId), "UpdateChildHistoryPatient", ChildDentalMedicalHistory);



                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                var model = getallvalue();
                return PartialView("DentalForms/_DentalHistory", model);

            }
            else
            {
                return RedirectToAction("Index", "Index");
            }
        }


        [HttpPost]
        public ActionResult DentalHistoryForm(string txtQue1, string txtQue2, string txtQue3, string txtQue4, string txtQue5, string txtQue5a, string txtQue5b, string txtQue5c, string txtQue6, string txtQue7, string rdQue7a, string rdQue8, string txtQue9a, string rdQue9, string rdQue10, string rdoQue11aFixedbridge, string rdoQue11bRemoveablebridge, string rdoQue11cDenture, string rdQue11dImplant, string txtQue12, string rdQue15, string rdQue16, string rdQue17, string rdQue18, string rdQue19, string chkQue20_1, string chkQue20_2, string chkQue20_3, string chkQue20_4, string rdQue21, string txtQue21a, string txtQue22, string txtQue23, string rdQue24, string txtQue26, string rdQue27, string rdQue28, string txtQue28a, string txtQue28b, string txtQue28c, string txtQue29, string txtQue29a, string rdQue30, string txtComments, string txtDigiSign)
        {
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                try
                {
                    DataTable tblDentalHistory = new DataTable("DentalHistory");
                    tblDentalHistory.Columns.Add("txtQue1");
                    tblDentalHistory.Columns.Add("txtQue2");
                    tblDentalHistory.Columns.Add("txtQue3");
                    tblDentalHistory.Columns.Add("txtQue4");
                    tblDentalHistory.Columns.Add("txtQue5");
                    tblDentalHistory.Columns.Add("txtQue5a");
                    tblDentalHistory.Columns.Add("txtQue5b");
                    tblDentalHistory.Columns.Add("txtQue5c");
                    tblDentalHistory.Columns.Add("txtQue6");
                    tblDentalHistory.Columns.Add("txtQue7");
                    tblDentalHistory.Columns.Add("rdQue7a");
                    tblDentalHistory.Columns.Add("rdQue8");
                    tblDentalHistory.Columns.Add("rdQue9");
                    tblDentalHistory.Columns.Add("txtQue9a");
                    tblDentalHistory.Columns.Add("rdQue10");
                    tblDentalHistory.Columns.Add("rdoQue11aFixedbridge");
                    tblDentalHistory.Columns.Add("rdoQue11bRemoveablebridge");
                    tblDentalHistory.Columns.Add("rdoQue11cDenture");
                    tblDentalHistory.Columns.Add("rdQue11dImplant");
                    tblDentalHistory.Columns.Add("txtQue12");
                    tblDentalHistory.Columns.Add("rdQue15");
                    tblDentalHistory.Columns.Add("rdQue16");
                    tblDentalHistory.Columns.Add("rdQue17");
                    tblDentalHistory.Columns.Add("rdQue18");
                    tblDentalHistory.Columns.Add("rdQue19");
                    tblDentalHistory.Columns.Add("chkQue20");
                    StringBuilder chkQue20 = new StringBuilder();




                    if (chkQue20_1 == "Y")
                    {
                        if (!String.IsNullOrEmpty(chkQue20.ToString()))
                        {
                            chkQue20.Append("," + "1");
                        }
                        else
                        {
                            chkQue20.Append("1");
                        }
                    }
                    if (chkQue20_2 == "Y")
                    {
                        if (!String.IsNullOrEmpty(chkQue20.ToString()))
                        {
                            chkQue20.Append("," + "2");
                        }
                        else
                        {
                            chkQue20.Append("2");
                        }
                    }
                    if (chkQue20_3 == "Y")
                    {
                        if (!String.IsNullOrEmpty(chkQue20.ToString()))
                        {
                            chkQue20.Append("," + "3");
                        }
                        else
                        {
                            chkQue20.Append("3");
                        }
                    }
                    if (chkQue20_4 == "Y")
                    {
                        if (!String.IsNullOrEmpty(chkQue20.ToString()))
                        {
                            chkQue20.Append("," + "4");
                        }
                        else
                        {
                            chkQue20.Append("4");
                        }
                    }
                    tblDentalHistory.Columns.Add("rdQue21");
                    tblDentalHistory.Columns.Add("txtQue21a");
                    tblDentalHistory.Columns.Add("txtQue22");


                    tblDentalHistory.Columns.Add("txtQue23");
                    tblDentalHistory.Columns.Add("rdQue24");

                    tblDentalHistory.Columns.Add("txtQue26");
                    tblDentalHistory.Columns.Add("rdQue27");
                    tblDentalHistory.Columns.Add("rdQue28");
                    tblDentalHistory.Columns.Add("txtQue28a");
                    tblDentalHistory.Columns.Add("txtQue28b");
                    tblDentalHistory.Columns.Add("txtQue28c");
                    tblDentalHistory.Columns.Add("txtQue29");
                    tblDentalHistory.Columns.Add("txtQue29a");
                    tblDentalHistory.Columns.Add("rdQue30");

                    tblDentalHistory.Columns.Add("txtComments");
                    tblDentalHistory.Columns.Add("txtDigiSign");
                    tblDentalHistory.Columns.Add("CreatedDate");
                    tblDentalHistory.Columns.Add("ModifiedDate");

                    tblDentalHistory.Rows.Add(txtQue1, txtQue2, txtQue3, txtQue4, txtQue5, txtQue5a, txtQue5b, txtQue5c,
                        txtQue6, txtQue7, rdQue7a, rdQue8, rdQue9, txtQue9a, rdQue10,
                        rdoQue11aFixedbridge, rdoQue11bRemoveablebridge, rdoQue11cDenture,
                        rdQue11dImplant, txtQue12, rdQue15, rdQue16, rdQue17, rdQue18,
                        rdQue19, chkQue20.ToString(), rdQue21, txtQue21a, txtQue22,
                        txtQue23, rdQue24, txtQue26, rdQue27, rdQue28,
                        txtQue28a, txtQue28b, txtQue28c, txtQue29, txtQue29a, rdQue30, txtComments, txtDigiSign, System.DateTime.Now, System.DateTime.Now);


                    //Join Two Table
                    DataSet dsDentalHistory = new DataSet("DentalHistory");
                    dsDentalHistory.Tables.Add(tblDentalHistory);

                    DataSet ds = dsDentalHistory.Copy();
                    dsDentalHistory.Clear();

                    string DentalHistory = ds.GetXml();

                    bool status = objCommonDAL.UpdateDentalHistory(Convert.ToInt32(SessionManagement.PatientId), "UpdateDentalHistory", DentalHistory);

                }
                catch (Exception ex)
                {
                    throw ex;
                }

                var model = getallvalue();
                return PartialView("DentalForms/_MedicalHistory", model);
            }
            else
            {
                return RedirectToAction("Index", "Index");
            }

        }



        [HttpPost]
        public ActionResult MedicalHistoryForm(string MrdQue1, string Mtxtphysicians, string MrdQue2, string Mtxthospitalized, string MrdQue3, string Mtxtserious, string MrdQue4, string Mtxtmedications, string MrdQue5, string MtxtRedux, string MrdQue6, string MtxtFosamax, string MrdQuediet7, string Mtxt7, string Mrdotobacco8, string Mtxt8, string Mrdosubstances, string Mtxt9, string Mrdopregnant, string Mtxt10, string Mrdocontraceptives,string Mtxt11, string MrdoNursing, string Mtxt12, string MchkQue_1, string MchkQue_2, string MchkQue_3, string MchkQue_4, string MchkQue_5, string MchkQue_6, string MchkQue_7, string MchkQue_8, string MchkQue_9, string MtxtchkQue_9
            , string MrdQueAIDS_HIV_Positive, string MrdQueAlzheimer, string MrdQueAnaphylaxis, string MrdQueAnemia, string MrdQueAngina, string MrdQueArthritis_Gout
            , string MrdQueArtificialHeartValve, string MrdQueArtificialJoint, string MrdQueAsthma, string MrdQueBloodDisease, string MrdQueBloodTransfusion, string MrdQueBreathing
            , string MrdQueBruise, string MrdQueCancer, string MrdQueChemotherapy, string MrdQueChest, string MrdQueCold_Sores_Fever, string MrdQueCongenital, string MrdQueConvulsions
            , string MrdQueCortisone, string MrdQueDiabetes, string MrdQueDrug, string MrdQueEasily, string MrdQueEmphysema, string MrdQueEpilepsy, string MrdQueExcessiveBleeding, string MrdQueExcessiveThirst
            , string MrdQueFainting, string MrdQueFrequentCough, string MrdQueFrequentDiarrhea, string MrdQueFrequentHeadaches, string MrdQueGenital, string MrdQueGlaucoma
           , string MrdQueHay, string MrdQueHeartAttack_Failure, string MrdQueHeartMurmur, string MrdQueHeartPacemaker, string MrdQueHeartTrouble_Disease, string MrdQueHemophilia
           , string MrdQueHepatitisA, string MrdQueHepatitisBorC, string MrdQueHerpes, string MrdQueHighBloodPressure, string MrdQueHighCholesterol, string MrdQueHives
           , string MrdQueHypoglycemia, string MrdQueIrregular, string MrdQueKidney, string MrdQueLeukemia, string MrdQueLiver, string MrdQueLow, string MrdQueLung
           , string MrdQueMitral, string MrdQueOsteoporosis, string MrdQuePain, string MrdQueParathyroid, string MrdQuePsychiatric, string MrdQueRadiation, string MrdQueRecent
           , string MrdQueRenal, string MrdQueRheumatic, string MrdQueRheumatism, string MrdQueScarlet, string MrdQueShingles, string MrdQueSickle, string MrdQueSinus, string MrdQueSpina
           , string MrdQueStomach, string MrdQueStroke, string MrdQueSwelling, string MrdQueThyroid, string MrdQueTonsillitis, string MrdQueTuberculosis, string MrdQueTumors
           , string MrdQueUlcers, string MrdQueVenereal, string MrdQueYellow, string Mtxtillness, string MtxtComments)
        {
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                try
                {
                    // Dental History Table
                    DataTable tblMedicalHistory = new DataTable("MedicalHistory");
                    tblMedicalHistory.Columns.Add("MrdQue1");
                    tblMedicalHistory.Columns.Add("Mtxtphysicians");
                    tblMedicalHistory.Columns.Add("MrdQue2");
                    tblMedicalHistory.Columns.Add("Mtxthospitalized");
                    tblMedicalHistory.Columns.Add("MrdQue3");
                    tblMedicalHistory.Columns.Add("Mtxtserious");
                    tblMedicalHistory.Columns.Add("MrdQue4");
                    tblMedicalHistory.Columns.Add("Mtxtmedications");
                    tblMedicalHistory.Columns.Add("MrdQue5");
                    tblMedicalHistory.Columns.Add("MtxtRedux");
                    tblMedicalHistory.Columns.Add("MrdQue6");
                    tblMedicalHistory.Columns.Add("MtxtFosamax");

                    tblMedicalHistory.Columns.Add("MrdQuediet7");
                    tblMedicalHistory.Columns.Add("Mtxt7");
                    tblMedicalHistory.Columns.Add("Mrdotobacco8");
                    tblMedicalHistory.Columns.Add("Mtxt8");
                    tblMedicalHistory.Columns.Add("Mrdosubstances");
                    tblMedicalHistory.Columns.Add("Mtxt9");
                    tblMedicalHistory.Columns.Add("Mrdopregnant");
                    tblMedicalHistory.Columns.Add("Mtxt10");
                    tblMedicalHistory.Columns.Add("Mrdocontraceptives");
                    tblMedicalHistory.Columns.Add("Mtxt11");
                    tblMedicalHistory.Columns.Add("MrdoNursing");
                    tblMedicalHistory.Columns.Add("Mtxt12");
                    tblMedicalHistory.Columns.Add("MchkQue_1");
                    tblMedicalHistory.Columns.Add("MchkQue_2");
                    tblMedicalHistory.Columns.Add("MchkQue_3");
                    tblMedicalHistory.Columns.Add("MchkQue_4");
                    tblMedicalHistory.Columns.Add("MchkQue_5");
                    tblMedicalHistory.Columns.Add("MchkQue_6");
                    tblMedicalHistory.Columns.Add("MchkQue_7");
                    tblMedicalHistory.Columns.Add("MchkQue_8");
                    tblMedicalHistory.Columns.Add("MchkQue_9");
                    tblMedicalHistory.Columns.Add("MtxtchkQue_9");
                    tblMedicalHistory.Columns.Add("MrdQueAIDS_HIV_Positive");
                    tblMedicalHistory.Columns.Add("MrdQueAlzheimer");
                    tblMedicalHistory.Columns.Add("MrdQueAnaphylaxis");
                    tblMedicalHistory.Columns.Add("MrdQueAnemia");
                    tblMedicalHistory.Columns.Add("MrdQueAngina");
                    tblMedicalHistory.Columns.Add("MrdQueArthritis_Gout");
                    tblMedicalHistory.Columns.Add("MrdQueArtificialHeartValve");
                    tblMedicalHistory.Columns.Add("MrdQueArtificialJoint");
                    tblMedicalHistory.Columns.Add("MrdQueAsthma");
                    tblMedicalHistory.Columns.Add("MrdQueBloodDisease");
                    tblMedicalHistory.Columns.Add("MrdQueBloodTransfusion");
                    tblMedicalHistory.Columns.Add("MrdQueBreathing");
                    tblMedicalHistory.Columns.Add("MrdQueBruise");
                    tblMedicalHistory.Columns.Add("MrdQueCancer");
                    tblMedicalHistory.Columns.Add("MrdQueChemotherapy");
                    tblMedicalHistory.Columns.Add("MrdQueChest");
                    tblMedicalHistory.Columns.Add("MrdQueCold_Sores_Fever");
                    tblMedicalHistory.Columns.Add("MrdQueCongenital");
                    tblMedicalHistory.Columns.Add("MrdQueConvulsions");
                    tblMedicalHistory.Columns.Add("MrdQueCortisone");
                    tblMedicalHistory.Columns.Add("MrdQueDiabetes");
                    tblMedicalHistory.Columns.Add("MrdQueDrug");
                    tblMedicalHistory.Columns.Add("MrdQueEasily");
                    tblMedicalHistory.Columns.Add("MrdQueEmphysema");
                    tblMedicalHistory.Columns.Add("MrdQueEpilepsy");
                    tblMedicalHistory.Columns.Add("MrdQueExcessiveBleeding");
                    tblMedicalHistory.Columns.Add("MrdQueExcessiveThirst");
                    tblMedicalHistory.Columns.Add("MrdQueFainting");
                    tblMedicalHistory.Columns.Add("MrdQueFrequentCough");
                    tblMedicalHistory.Columns.Add("MrdQueFrequentDiarrhea");
                    tblMedicalHistory.Columns.Add("MrdQueFrequentHeadaches");
                    tblMedicalHistory.Columns.Add("MrdQueGenital");
                    tblMedicalHistory.Columns.Add("MrdQueGlaucoma");
                    tblMedicalHistory.Columns.Add("MrdQueHay");
                    tblMedicalHistory.Columns.Add("MrdQueHeartAttack_Failure");
                    tblMedicalHistory.Columns.Add("MrdQueHeartMurmur");
                    tblMedicalHistory.Columns.Add("MrdQueHeartPacemaker");
                    tblMedicalHistory.Columns.Add("MrdQueHeartTrouble_Disease");
                    tblMedicalHistory.Columns.Add("MrdQueHemophilia");
                    tblMedicalHistory.Columns.Add("MrdQueHepatitisA");
                    tblMedicalHistory.Columns.Add("MrdQueHepatitisBorC");
                    tblMedicalHistory.Columns.Add("MrdQueHerpes");
                    tblMedicalHistory.Columns.Add("MrdQueHighBloodPressure");
                    tblMedicalHistory.Columns.Add("MrdQueHighCholesterol");
                    tblMedicalHistory.Columns.Add("MrdQueHives");
                    tblMedicalHistory.Columns.Add("MrdQueHypoglycemia");
                    tblMedicalHistory.Columns.Add("MrdQueIrregular");
                    tblMedicalHistory.Columns.Add("MrdQueKidney");
                    tblMedicalHistory.Columns.Add("MrdQueLeukemia");
                    tblMedicalHistory.Columns.Add("MrdQueLiver");
                    tblMedicalHistory.Columns.Add("MrdQueLow");
                    tblMedicalHistory.Columns.Add("MrdQueLung");
                    tblMedicalHistory.Columns.Add("MrdQueMitral");
                    tblMedicalHistory.Columns.Add("MrdQueOsteoporosis");
                    tblMedicalHistory.Columns.Add("MrdQuePain");
                    tblMedicalHistory.Columns.Add("MrdQueParathyroid");
                    tblMedicalHistory.Columns.Add("MrdQuePsychiatric");
                    tblMedicalHistory.Columns.Add("MrdQueRadiation");
                    tblMedicalHistory.Columns.Add("MrdQueRecent");
                    tblMedicalHistory.Columns.Add("MrdQueRenal");
                    tblMedicalHistory.Columns.Add("MrdQueRheumatic");
                    tblMedicalHistory.Columns.Add("MrdQueRheumatism");
                    tblMedicalHistory.Columns.Add("MrdQueScarlet");
                    tblMedicalHistory.Columns.Add("MrdQueShingles");
                    tblMedicalHistory.Columns.Add("MrdQueSickle");
                    tblMedicalHistory.Columns.Add("MrdQueSinus");
                    tblMedicalHistory.Columns.Add("MrdQueSpina");
                    tblMedicalHistory.Columns.Add("MrdQueStomach");
                    tblMedicalHistory.Columns.Add("MrdQueStroke");
                    tblMedicalHistory.Columns.Add("MrdQueSwelling");
                    tblMedicalHistory.Columns.Add("MrdQueThyroid");
                    tblMedicalHistory.Columns.Add("MrdQueTonsillitis");
                    tblMedicalHistory.Columns.Add("MrdQueTuberculosis");
                    tblMedicalHistory.Columns.Add("MrdQueTumors");
                    tblMedicalHistory.Columns.Add("MrdQueUlcers");
                    tblMedicalHistory.Columns.Add("MrdQueVenereal");
                    tblMedicalHistory.Columns.Add("MrdQueYellow");

                    tblMedicalHistory.Columns.Add("Mtxtillness");
                    tblMedicalHistory.Columns.Add("MtxtComments");
                    tblMedicalHistory.Columns.Add("CreatedDate");
                    tblMedicalHistory.Columns.Add("ModifiedDate");


                    tblMedicalHistory.Rows.Add(MrdQue1, Mtxtphysicians, MrdQue2, Mtxthospitalized, MrdQue3, Mtxtserious, MrdQue4, Mtxtmedications, MrdQue5, MtxtRedux, MrdQue6,
MtxtFosamax, MrdQuediet7, Mtxt7, Mrdotobacco8, Mtxt8, Mrdosubstances, Mtxt9, Mrdopregnant, Mtxt10, Mrdocontraceptives, Mtxt11, MrdoNursing, Mtxt12, MchkQue_1, MchkQue_2, MchkQue_3, MchkQue_4, MchkQue_5,
MchkQue_6, MchkQue_7, MchkQue_8, MchkQue_9, MtxtchkQue_9, MrdQueAIDS_HIV_Positive, MrdQueAlzheimer, MrdQueAnaphylaxis, MrdQueAnemia, MrdQueAngina, MrdQueArthritis_Gout, MrdQueArtificialHeartValve,
MrdQueArtificialJoint, MrdQueAsthma, MrdQueBloodDisease, MrdQueBloodTransfusion, MrdQueBreathing, MrdQueBruise, MrdQueCancer, MrdQueChemotherapy, MrdQueChest, MrdQueCold_Sores_Fever,
MrdQueCongenital, MrdQueConvulsions, MrdQueCortisone, MrdQueDiabetes, MrdQueDrug, MrdQueEasily, MrdQueEmphysema, MrdQueEpilepsy, MrdQueExcessiveBleeding, MrdQueExcessiveThirst, MrdQueFainting,
MrdQueFrequentCough, MrdQueFrequentDiarrhea, MrdQueFrequentHeadaches, MrdQueGenital, MrdQueGlaucoma, MrdQueHay, MrdQueHeartAttack_Failure, MrdQueHeartMurmur, MrdQueHeartPacemaker,
MrdQueHeartTrouble_Disease, MrdQueHemophilia, MrdQueHepatitisA, MrdQueHepatitisBorC, MrdQueHerpes, MrdQueHighBloodPressure, MrdQueHighCholesterol, MrdQueHives, MrdQueHypoglycemia,
MrdQueIrregular, MrdQueKidney, MrdQueLeukemia, MrdQueLiver, MrdQueLow, MrdQueLung, MrdQueMitral, MrdQueOsteoporosis, MrdQuePain, MrdQueParathyroid, MrdQuePsychiatric,
MrdQueRadiation, MrdQueRecent, MrdQueRenal, MrdQueRheumatic, MrdQueRheumatism, MrdQueScarlet, MrdQueShingles, MrdQueSickle, MrdQueSinus, MrdQueSpina, MrdQueStomach, MrdQueStroke, MrdQueSwelling,
MrdQueThyroid, MrdQueTonsillitis, MrdQueTuberculosis, MrdQueTumors, MrdQueUlcers, MrdQueVenereal, MrdQueYellow, Mtxtillness, MtxtComments);


                    DataSet dsMedicalHistory = new DataSet("MedicalHistory");
                    dsMedicalHistory.Tables.Add(tblMedicalHistory);

                    DataSet ds = dsMedicalHistory.Copy();
                    dsMedicalHistory.Clear();

                    string MedicalHistory = ds.GetXml();

                    bool status = objCommonDAL.UpdateMedicalHistory(Convert.ToInt32(SessionManagement.PatientId), "UpdateMedicalHistory", MedicalHistory);

                }
                catch (Exception ex)
                {
                    throw ex;
                }

                var model = getallvalue();
                return PartialView("DentalForms/_MedicalHistoryUpdate", model);
            }
            else
            {
                return RedirectToAction("Index", "Index");
            }

        }


        [HttpPost]
        public ActionResult DentalHistoryUpdateForm(string txt1b, string txtDigiSignhistoryupdate)
        {
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                try
                {
                    DataTable tblMedicalUpdate = new DataTable("MedicalUpdate");
                    tblMedicalUpdate.Columns.Add("txt1aDate");
                    tblMedicalUpdate.Columns.Add("txt1b");
                    tblMedicalUpdate.Columns.Add("txt2aDate");
                    tblMedicalUpdate.Columns.Add("txt2b");
                    tblMedicalUpdate.Columns.Add("txt3aDate");
                    tblMedicalUpdate.Columns.Add("txt3b");
                    tblMedicalUpdate.Columns.Add("txt4aDate");
                    tblMedicalUpdate.Columns.Add("txt4b");
                    tblMedicalUpdate.Columns.Add("txt5aDate");
                    tblMedicalUpdate.Columns.Add("txt5b");
                    tblMedicalUpdate.Columns.Add("txt6aDate");
                    tblMedicalUpdate.Columns.Add("txt6b");
                    tblMedicalUpdate.Columns.Add("txt7aDate");
                    tblMedicalUpdate.Columns.Add("txt7b");
                    tblMedicalUpdate.Columns.Add("txt8aDate");
                    tblMedicalUpdate.Columns.Add("txt8b");
                    tblMedicalUpdate.Columns.Add("txtDigiSign");
                    tblMedicalUpdate.Columns.Add("CreatedDate");
                    tblMedicalUpdate.Columns.Add("ModifiedDate");


                    tblMedicalUpdate.Rows.Add("", txt1b, "", "", "", "", "",
                        "", "", "", "", "", "", "", "", "",
                        txtDigiSignhistoryupdate, System.DateTime.Now, System.DateTime.Now);

                    DataSet dsMedicalUpdate = new DataSet("MedicalUpdate");
                    dsMedicalUpdate.Tables.Add(tblMedicalUpdate);

                    DataSet ds = dsMedicalUpdate.Copy();
                    dsMedicalUpdate.Clear();

                    string MedicalUpdate = ds.GetXml();

                    bool status = objCommonDAL.UpdateMedicalHistoryUpdate(Convert.ToInt32(SessionManagement.PatientId), "UpdateMedicalHistoryUpdate", MedicalUpdate);

                }
                catch (Exception ex)
                {
                    throw ex;
                }

                var model = getallvalue();
                return PartialView("DentalForms/_Review", model);
            }
            else
            {
                return RedirectToAction("Index", "Index");
            }
        }
    }
}
