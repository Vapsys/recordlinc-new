﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using DentalFiles.Models;
using System.Configuration;
using JQueryDataTables.Models.Repository;
using DentalFiles.App_Start;
namespace DentalFiles.Controllers
{
    public class DentalHelpController : Controller
    {
        
        DataRepository objRepository = new DataRepository();
        CommonDAL objCommonDAL = new CommonDAL();
        public ActionResult Index()
            {
            ViewBag.Title = "Help & Training";
            objCommonDAL.GetActiveCompany();
            ViewBag.ReturnUrl = "DentalHelp";
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                List<PatientDetails> PatientDetialList = new List<PatientDetails>();
                PatientDetialList = objRepository.GetPatientDetails(SessionManagement.PatientId, SessionManagement.TimeZoneSystemName);
                if (PatientDetialList.Count != 0)
                {
                    ViewBag.FirstName = PatientDetialList[0].FirstName.ToString();
                    if (!string.IsNullOrEmpty(PatientDetialList[0].ProfileImage.ToString()))
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["imagebankpath"] + PatientDetialList[0].ProfileImage.ToString();
                    }
                    else if (PatientDetialList[0].Gender == 1)
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"];
                    }
                    else
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["Doctorimage1"];
                        
                    }
                }
                ViewBag.RequestHelp = "Request Help";
                Messages objmsg = new Messages();
                DataTable dt = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0,1,5);

                DataTable dtCount = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0, 1, 100000);
                int count = 0;
                foreach (DataRow item in dtCount.Rows)
                {
                    if (!Convert.ToBoolean(item["Read_flag"]))
                    {
                        count = count + 1;
                    }
                }
                @ViewBag.MsgCount = count;
                return View();
            }
            else
            {
                return View();
            }
        }

    }
}
