﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentalFiles.Models.ViewModel;
using JQueryDataTables.Models.Repository;
using DentalFiles.App_Start;
using DentalFiles.Models;
using System.Data;
namespace DentalFiles.Controllers
{
    public class PrintInsuranceCoverageController : Controller
    {
        CommonDAL objCommonDAL = new CommonDAL();
        DataRepository objRepository = new DataRepository();
        //
        // GET: /PrintInsuranceCoverage/

        public ActionResult Index()
        {
            if (Request.QueryString["Printid"] != null && Convert.ToInt32(Request.QueryString["Printid"]) != 0)
            {
                SessionManagement.PatientId = (Convert.ToInt32(Request.QueryString["Printid"]) - 45844584);


            }


            if (Convert.ToInt32(SessionManagement.PatientId) != 0)
            {

                if (Request.QueryString["UserId"] != null && Convert.ToInt32(Request.QueryString["UserId"]) != 0)
                {
                    DataSet dsMyPatients = objCommonDAL.GetPatientdetailsOfDoctordataset((Convert.ToInt32(Request.QueryString["UserId"]) - 45844584), 1, 10, SessionManagement.PatientId.ToString(), null, null, null, null, null, null, null, null);


                    if (dsMyPatients != null && dsMyPatients.Tables.Count > 0 && dsMyPatients.Tables[0].Rows.Count > 0)
                    { }
                    else
                    {
                        SessionManagement.PatientId = 0;
                        TempData["result"] = "false";


                    }
                }
                else
                {
                    SessionManagement.PatientId = 0;
                    TempData["result"] = "false";


                }
            }
            else
            {
                return RedirectToAction("Index", "Index");
            }
            var model = new DentalFormsMaster();
            model.RegistrationForm = objRepository.GetRegistration(Convert.ToInt32(SessionManagement.PatientId));
            objCommonDAL.GetActiveCompany();
            return PartialView(model);
        }

    }
}
