﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using DentalFiles.Models;
using JQueryDataTables.Models.Repository;
using DentalFiles.Models.ViewModel;
using PagedList;
using System.IO;

using DentalFiles.App_Start;


namespace DentalFiles.Controllers
{
    public class InviteDoctorController : Controller
    {
        int PatientId = 0;
        DataTable getFilenames = new DataTable();
        CommonDAL objCommonDAL = new CommonDAL();
        DataRepository objRepository = new DataRepository();
        string txtcityorzip = null;
        string txtlastname = null;
        int drpspeciality = 0;
        int drpdistance = 0;
        string keyword = null;
        string Firstname = null;
        string Title = null;
        string CompanyName = null;
        string School = null;
        string Email = null;
        string phone = null;
        string zipcode = null;
        string SpecialtityList = null;
        private const int defaultPageSize = 30;
        public ActionResult Index(int? Userpage, string _keywords, string _FirstName, string _LastName, string _Title, string _CompanyName, string _School, string _Email, string _Phone, string _Distance, string _ZipCode, string _SpecialtityList, string Partial)
        {
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                int currentRefPageIndex = Userpage.HasValue ? Userpage.Value : 1;
                objCommonDAL.GetActiveCompany();
                ViewBag.PatientId = PatientId = Convert.ToInt32(SessionManagement.PatientId);
                ViewBag.Title = "Invite Doctor";
                ViewBag.ReturnUrl = "InviteDoctor";
                List<PatientDetails> PatientDetialList = new List<PatientDetails>();
                PatientDetialList = objRepository.GetPatientDetails(PatientId, SessionManagement.TimeZoneSystemName);
                if (PatientDetialList.Count != 0)
                {
                    ViewBag.FirstName = PatientDetialList[0].FirstName.ToString();
                    if (!string.IsNullOrEmpty(PatientDetialList[0].ProfileImage.ToString()))
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["imagebankpath"] + PatientDetialList[0].ProfileImage.ToString();
                    }
                    else if (PatientDetialList[0].Gender == 1)
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"];
                    }
                    else
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["Doctorimage1"];

                    }
                }
                Messages objmsg = new Messages();
                DataTable dtCount = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0, 1, 100000);
                int count = 0;
                foreach (DataRow item in dtCount.Rows)
                {
                    if (!Convert.ToBoolean(item["Read_flag"]))
                    {
                        count = count + 1;
                    }
                }
                @ViewBag.MsgCount = count;

                if (_keywords != null && Convert.ToString(_keywords) != "" && Convert.ToString(_keywords) != "Keywords")
                {
                    keyword = _keywords;
                }
                else
                {
                    keyword = null;
                }
                if (_FirstName != null && Convert.ToString(_FirstName) != "" && Convert.ToString(_FirstName) != "First Name")
                {
                    Firstname = _FirstName;
                }
                else
                {
                    Firstname = null;
                }
                if (_LastName != null && Convert.ToString(_LastName) != "" && Convert.ToString(_LastName) != "Last Name")
                {
                    txtlastname = _LastName;
                }
                else
                {
                    txtlastname = null;
                }
                if (_Title != null && Convert.ToString(_Title) != "" && Convert.ToString(_Title) != "Title")
                {
                    Title = _Title;
                }
                else
                {
                    Title = null;
                }
                if (_CompanyName != null && Convert.ToString(_CompanyName) != "" && Convert.ToString(_CompanyName) != "Company")
                {
                    CompanyName = _CompanyName;
                }
                else
                {
                    CompanyName = null;
                }
                if (_School != null && Convert.ToString(_School) != "" && Convert.ToString(_School) != "School")
                {
                    School = _School;
                }
                else
                {
                    School = null;
                }
                if (_Email != null && Convert.ToString(_Email) != "" && Convert.ToString(_Email) != "Email")
                {
                    Email = _Email;
                }
                else
                {
                    Email = null;
                }
                if (_Phone != null && Convert.ToString(_Phone) != "" && Convert.ToString(_Phone) != "Phone Number")
                {
                    phone = _Phone;
                }
                else
                {
                    phone = null;
                }
                if (_Distance != null && Convert.ToString(_Distance) != "")
                {
                    drpdistance = Convert.ToInt32(_Distance);
                }
                if (_ZipCode != null && Convert.ToString(_ZipCode) != "" && Convert.ToString(_ZipCode) != "Zip Code")
                {
                    zipcode = Convert.ToString(_ZipCode);
                }
                else
                {
                    zipcode = null;
                }
                if (_SpecialtityList != null && Convert.ToString(_SpecialtityList) != "")
                {
                    SpecialtityList = Convert.ToString(_SpecialtityList);
                }
                GetSeachDetailsOfInviteDoctor objInviteDoctor = new GetSeachDetailsOfInviteDoctor();

                var model = new MyInboxMaster();
                objInviteDoctor.keywords = keyword;
                objInviteDoctor.FirstName = Firstname;
                objInviteDoctor.LastName = txtlastname;
                objInviteDoctor.Title = Title;
                objInviteDoctor.CompanyName = CompanyName;
                objInviteDoctor.School = School;
                objInviteDoctor.Email = Email;
                objInviteDoctor.Phone = phone;
                objInviteDoctor.ZipCode = zipcode;
                objInviteDoctor.SpecialtityList = SpecialtityList;
                objInviteDoctor.Distance = drpdistance;
                TempData["SearchDetails"] = objInviteDoctor;
                List<UserList> list = new List<UserList>();
                list = objRepository.GetTratingDoctors(Convert.ToInt32(SessionManagement.PatientId), 1, 15);
                model.InviteTreatingDoctor = objRepository.GetInviteTreatingDoctorList(keyword, txtcityorzip, txtlastname, drpspeciality, drpdistance, Firstname, Title, CompanyName, School, Email, phone, zipcode, 1, 1000, SpecialtityList, Convert.ToInt32(SessionManagement.PatientId)).ToPagedList(currentRefPageIndex, defaultPageSize);
                model.InviteTreatingDoctor = model.InviteTreatingDoctor.Where(t => !list.Any(x => x.UserID == t.UserID)).ToPagedList(currentRefPageIndex, defaultPageSize);
                    if (model.InviteTreatingDoctor.Count > 0)
                {
                    ViewBag.IsRecord = "";
                }
                else
                {
                    ViewBag.IsRecord = "No Record Found";
                }

                if (Partial == "TRUE")
                {
                    return PartialView("_UserSearch", model);
                }
                else
                {
                    return View(model);
                }


            }
            else
            {
                return RedirectToAction("Index", "Index");
            }
        }

        public string GetTreatingDoctorOnscroll(int PageIndex)
        {
            GetSeachDetailsOfInviteDoctor obj = new GetSeachDetailsOfInviteDoctor();
            obj = TempData["SearchDetails"] as GetSeachDetailsOfInviteDoctor;
            TempData["SearchDetails"] = obj;
            keyword = obj.keywords;

            Firstname = obj.FirstName;
            txtlastname = obj.LastName;
            Title = obj.Title;
            CompanyName = obj.CompanyName;
            School = obj.School;
            Email = obj.Email;
            phone = obj.Phone;
            zipcode = obj.ZipCode;
            SpecialtityList = obj.SpecialtityList;
            drpdistance = obj.Distance;
            List<UserList> list = new List<UserList>();
            list = objRepository.GetTratingDoctors(Convert.ToInt32(SessionManagement.PatientId), 1, 15);
            var model = new MyInboxMaster();
            model.InviteTreatingDoctor = objRepository.GetInviteTreatingDoctorList(keyword, txtcityorzip, txtlastname, drpspeciality, drpdistance, Firstname, Title, CompanyName, School, Email, phone, zipcode, 1, 1000, SpecialtityList, Convert.ToInt32(SessionManagement.PatientId)).ToPagedList(PageIndex, defaultPageSize);
            model.InviteTreatingDoctor = model.InviteTreatingDoctor.Where(t => !list.Any(x => x.UserID == t.UserID)).ToPagedList(PageIndex, defaultPageSize);
            if (model.InviteTreatingDoctor.Count == 0 && PageIndex > 1)
            {
                return "<div><center style=\"display: inline-block; margin-left:42%;\">No more record found</center></div>";
            }
            else if (model.InviteTreatingDoctor.Count == 0 && PageIndex == 1)
            {
                return "<center>No record found</center>";
            }
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, "_GetInviteDoctors");
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }

        }

        public JsonResult AddTreatingDoctor(string DoctorId)
        {
            object obj = null;
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                try
                {

                    objCommonDAL.AddPatientMember(Convert.ToInt32(SessionManagement.PatientId), Convert.ToInt32(DoctorId), Convert.ToInt32(UserStatusEnum.ACTIVE), 0, System.DateTime.Now, System.DateTime.Now, "");

                    //When Patient add that owner doctor again
                    DataTable dtPatient = new DataTable();
                    dtPatient = objCommonDAL.GetPatientsDetails(Convert.ToInt32(SessionManagement.PatientId));

                    int OwnerId = 0;
                    int patientId = 0;
                    patientId = Convert.ToInt32(SessionManagement.PatientId);
                    OwnerId = Convert.ToInt32(dtPatient.Rows[0]["OwnerId"]);
                    if (OwnerId == Convert.ToInt32(DoctorId))
                    {
                        bool Result = false;
                        Result = objCommonDAL.UpdatePatientStatus(Convert.ToInt32(SessionManagement.PatientId));
                    }
                    if (Convert.ToInt32(DoctorId) != 0)
                    {
                        //objCommonDAL.PatientRequestReceived(UserId, txtemail, txtfirstname, txtlastname, txtphoneno, txtstreet, txtstreet, txtcity, txtstate, txtcountry, txtzip);
                       objCommonDAL.PatientAddedNotification(Convert.ToInt32(DoctorId), patientId, Convert.ToString(dtPatient.Rows[0]["Email"]), Convert.ToString(dtPatient.Rows[0]["FirstName"]), Convert.ToString(dtPatient.Rows[0]["LastName"]), Convert.ToString(dtPatient.Rows[0]["Phone"]), Convert.ToString(dtPatient.Rows[0]["Address"]), "", Convert.ToString(dtPatient.Rows[0]["City"]), Convert.ToString(dtPatient.Rows[0]["STATE"]), Convert.ToString(dtPatient.Rows[0]["Country"]), Convert.ToString(dtPatient.Rows[0]["Zip"]));
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                obj = new
                {
                    Success = true,

                };
                return Json(obj, JsonRequestBehavior.DenyGet);
            }
            else
            {

                return Json(new
                {
                    redirectUrl = Url.Action("Index", "Index"),
                    isRedirect = true
                });

            }
        }
    }
}