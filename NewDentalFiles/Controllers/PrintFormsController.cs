﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentalFiles.Models.ViewModel;
using JQueryDataTables.Models.Repository;
using DentalFiles.App_Start;
using DentalFiles.Models;
using System.Data;

namespace DentalFiles.Controllers
{
    public class PrintFormsController : Controller
    {
        DataRepository objRepository = new DataRepository();
        CommonDAL objCommonDAL = new CommonDAL();
        
        public ActionResult Index() 
        {
            if (Request.QueryString["Printid"] != null && Convert.ToInt32(Request.QueryString["Printid"]) != 0)
            {
                SessionManagement.PatientId = (Convert.ToInt32(Request.QueryString["Printid"]) - 45844584);
            }


            else if (Request.QueryString["PatientId"] != null && Convert.ToInt32(Request.QueryString["PatientId"]) != 0)
            {
                SessionManagement.PatientId = Convert.ToInt32(Request.QueryString["PatientId"]);


            }
            


            if (Convert.ToInt32(SessionManagement.PatientId) != 0)
            {

                if (Request.QueryString["UserId"] != null && Convert.ToInt32(Request.QueryString["UserId"]) != 0)
                {
                     DataSet dsMyPatients = objCommonDAL.GetPatientdetailsOfDoctordataset((Convert.ToInt32(Request.QueryString["UserId"]) - 45844584), 1, 10, SessionManagement.PatientId.ToString(), null, null, null, null, null, null, null, null);


                    if (dsMyPatients != null && dsMyPatients.Tables.Count > 0 && dsMyPatients.Tables[0].Rows.Count > 0)
                    { }
                    else
                    {
                        SessionManagement.PatientId = 0;
                        TempData["result"] = "false";
                        

                    }
                }
                else
                {
                  

                }
            }
            else
            {
                return RedirectToAction("Index", "Index");
            }
            var model = new DentalFormsMaster();
            ViewBag.Gender = objCommonDAL.GetGender();
            ViewBag.Status = objCommonDAL.GetStatus();

            model.BasicInfo = objRepository.GetPatientBasicInfo(Convert.ToInt32(SessionManagement.PatientId), SessionManagement.TimeZoneSystemName);
            model.RegistrationForm = objRepository.GetRegistration(Convert.ToInt32(SessionManagement.PatientId));
            model.ChildDentalMEDICALHISTORY = objRepository.GetChildHistory(Convert.ToInt32(SessionManagement.PatientId));
            model.DentalHistory = objRepository.GetDentalHistory(Convert.ToInt32(SessionManagement.PatientId));
            model.MedicalHistory = objRepository.GetMedicalHistory(Convert.ToInt32(SessionManagement.PatientId));
            model.MedicalHISTORYUpdate = objRepository.GetMedicalUpdate(Convert.ToInt32(SessionManagement.PatientId));
            model.Review = objRepository.PatientFormsSent(Convert.ToInt32(SessionManagement.PatientId));
            model.PatientDetialList = objRepository.GetPatientDetails(Convert.ToInt32(SessionManagement.PatientId), SessionManagement.TimeZoneSystemName);

            
            return PartialView(model);
        }

    }
}
