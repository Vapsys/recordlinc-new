﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentalFiles.App_Start;
using DentalFiles.Models;
using JQueryDataTables.Models.Repository;
using System.Data;
using DentalFiles.Models.ViewModel;
using BusinessLogicLayer;
using DataAccessLayer.Common;
using BO.Models;

namespace DentalFilesNew.Controllers
{
    public class SettingsController : Controller
    {
        //
        // GET: /Settings/
        CommonDAL objCommonDAL = new CommonDAL();
        DataRepository objRepository = new DataRepository();
        public ActionResult Index()
        {

            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                objCommonDAL.GetActiveCompany();
                List<PatientDetails> PatientDetialList = objRepository.GetPatientDetails(Convert.ToInt32(SessionManagement.PatientId), SessionManagement.TimeZoneSystemName);
                if (PatientDetialList != null && PatientDetialList.Count != 0)
                {
                    ViewBag.FirstName = PatientDetialList[0].FirstName;
                    if (!string.IsNullOrEmpty(PatientDetialList[0].ProfileImage.ToString()))
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["imagebankpath"] + PatientDetialList[0].ProfileImage.ToString();
                    }
                    else if (PatientDetialList[0].Gender == 1)
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"];
                    }
                    else
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["Doctorimage1"];

                    }
                }
                ViewBag.Title = "DentalForms";
                ViewBag.ReturnUrl = "Settings";
                DataTable dt = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0, 1, 5);
                DataTable dtCount = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0, 1, 100000);
                int count = 0;
                foreach (DataRow Count in dtCount.Rows)
                {
                    if (!Convert.ToBoolean(Count["Read_flag"]))
                    {
                        count = count + 1;
                    }
                }
                @ViewBag.MsgCount = count;

                ViewBag.StayloggedMins = objCommonDAL.GetStayloggedMins();


                var model = getallvalue();


                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Index");
            }
        }

        private DentalFormsMaster getallvalue()
        {
            var model = new DentalFormsMaster();
            model.BasicInfo = objRepository.GetPatientBasicInfo(SessionManagement.PatientId, SessionManagement.TimeZoneSystemName);


            return model;
        }



        [HttpPost]
        public JsonResult UpdateSettings(string ddlStayloggedMins, string txtEmail, string hdnprimaryemail, string txtSecondaryEmail, string hdnsecondaryemail, string txtpassword)
        {
            object obj = null;
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                bool Isprimaryupdate = false;
                string IsprimaryAddressUpdate = "";
                string Body = string.Empty;
                bool Issecondaryupdate = false;
                string IssecondaryAddressUpdate = "";
                DataTable dtPatientEmail = objCommonDAL.CheckPatientEmail(txtEmail);
                if (dtPatientEmail != null && dtPatientEmail.Rows.Count > 0)
                {
                    if (dtPatientEmail.Rows[0]["Email"] != null && Convert.ToString(dtPatientEmail.Rows[0]["Email"]) != "")
                    {
                        if (Convert.ToString(dtPatientEmail.Rows[0]["Email"]) == hdnprimaryemail)
                        {

                        }
                        else
                        {
                            IsprimaryAddressUpdate = "Primary Email address already exists";
                            Isprimaryupdate = false;
                        }

                    }
                    else
                    {

                        bool x = objCommonDAL.UpdatePatientEmailAddress(SessionManagement.PatientId, txtEmail);
                        Body = "Basic settings updated successfully";
                        Isprimaryupdate = true;
                    }


                }
                else
                {

                    bool x = objCommonDAL.UpdatePatientEmailAddress(SessionManagement.PatientId, txtEmail);
                    Body = "Basic settings updated successfully";
                    Isprimaryupdate = true;
                }






                if (txtSecondaryEmail != null && txtSecondaryEmail != "")
                {
                    DataTable dtPatientSecondaryEmail = objCommonDAL.CheckPatientEmail(txtSecondaryEmail);
                    if (dtPatientSecondaryEmail != null && dtPatientSecondaryEmail.Rows.Count > 0)
                    {
                        if (dtPatientSecondaryEmail.Rows[0]["Email"] != null && Convert.ToString(dtPatientSecondaryEmail.Rows[0]["Email"]) != "")
                        {
                            if (Convert.ToString(dtPatientSecondaryEmail.Rows[0]["Email"]) == hdnsecondaryemail)
                            {

                            }
                            else
                            {
                                IssecondaryAddressUpdate = "Secondary Email address already exists";
                                Issecondaryupdate = false;
                            }

                        }
                        else
                        {

                            bool x = objCommonDAL.UpdatePatientSecondaryEmailAddress(SessionManagement.PatientId, txtSecondaryEmail);
                            Body = "Basic settings updated successfully";
                            Issecondaryupdate = true;
                        }


                    }
                    else
                    {

                        bool x = objCommonDAL.UpdatePatientSecondaryEmailAddress(SessionManagement.PatientId, txtSecondaryEmail);
                        Body = "Basic settings updated successfully";
                        Issecondaryupdate = true;
                    }

                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(txtSecondaryEmail))
                    {
                        bool x = objCommonDAL.UpdatePatientSecondaryEmailAddress(SessionManagement.PatientId, txtSecondaryEmail);
                        Body = "Basic settings updated successfully";
                        Issecondaryupdate = true;
                    }
                }

                string PatientResult = objCommonDAL.UpdatestayloggedminsPwd(Convert.ToInt32(SessionManagement.PatientId), ddlStayloggedMins.Trim(), txtpassword);
                Session.Timeout = Convert.ToInt32(ddlStayloggedMins.Trim());
                HttpCookie myCookie = new HttpCookie("Timeout");
                myCookie.Value = Convert.ToString(Session.Timeout);
                myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);
                var model = getallvalue();
                obj = new
                {
                    Success = true,
                    body = Body,
                    Isprimaryupdate = Isprimaryupdate,
                    IsprimaryAddressUpdate = IsprimaryAddressUpdate,
                    Issecondaryupdate = Issecondaryupdate,
                    IssecondaryAddressUpdate = IssecondaryAddressUpdate,

                };

            }
            return Json(obj, JsonRequestBehavior.DenyGet);




        }



        //public ActionResult GetPatientReferralForm()
        //{
        //    Compose1Click ComposeReferral = new Compose1Click();
        //    string DecryptString = string.Empty;
        //    if (ViewBag.alert != null)
        //    {
        //        DecryptString = (string)TempData["String"];
        //    }
        //    else
        //    {
        //        DecryptString = Convert.ToString(Request.QueryString["TYHJNABGA"]);
        //    }
        //    TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        //    int PatientId = 0; int DoctorId = 0;
        //    bool flag = false;
        //    if (!string.IsNullOrEmpty(DecryptString))
        //    {
        //        //DecryptString = DecryptString;
        //        string[] DecryptedDoctorId = DecryptString.Split('|');
        //        if (DecryptedDoctorId.Length == 3)
        //        {
        //            flag = true;
        //        }
        //        //PatientId = Convert.ToInt32(ObjTripleDESCryptoHelper.decryptText(DecryptedDoctorId[0]));
        //        PatientId = Convert.ToInt32(DecryptedDoctorId[0]) - 999;
        //        TempData["PatientId"] = PatientId;
        //        //DoctorId = Convert.ToInt32(ObjTripleDESCryptoHelper.decryptText(DecryptedDoctorId[1]));
        //        DoctorId = Convert.ToInt32(DecryptedDoctorId[1]) - 999;
        //        TempData["DoctorId"] = DoctorId;
        //        TempData.Keep();
        //    }
        //    else
        //    {
        //        PatientId = Convert.ToInt32(TempData["PatientId"]);
        //        DoctorId = Convert.ToInt32(TempData["DoctorId"]);
        //    }
        //    //string DecryptedPatientId = ObjTripleDESCryptoHelper.decryptText(Request.QueryString["PatientId"]);
        //    ComposeReferral = ReferralFormBLL.GetPatientInfo(PatientId);
        //    ComposeReferral._innerService = ReferralFormBLL.GetSpecialityForPatientForm(DoctorId);
        //    ComposeReferral._insuranceCoverage = ReferralFormBLL.GetInsuranceDetails(PatientId);
        //    ComposeReferral._specialtyService = ReferralFormBLL.GetPatientReferalVisibleSection(Convert.ToInt32(DoctorId));
        //    ComposeReferral._medicalHistory = ReferralFormBLL.GetMedicalHistoryDetails(PatientId);
        //    ComposeReferral._dentalhistory = ReferralFormBLL.GetDentalHistoryDetails(PatientId);
        //    ComposeReferral._contactInfo.EMGContactName = ComposeReferral._insuranceCoverage.EMGContactName;
        //    ComposeReferral._contactInfo.EMGContactNo = ComposeReferral._insuranceCoverage.EMGContactNo;
        //    ComposeReferral.StateList = PatientBLL.GetStateList("US");
        //    ComposeReferral.ReceiverId = Convert.ToString(DoctorId);
        //    ComposeReferral.SenderId = Convert.ToString(PatientId);
        //    ComposeReferral.IsReferral = flag;
        //    ViewBag.alert = TempData["success"];
        //    TempData["success"] = null;
        //    return View("GetPatientReferralForm", ComposeReferral);
        //}
        ///// <summary>
        ///// Method use for update Patient detail when patient click on link from mail. Use in One Click Referral.
        ///// </summary>
        ///// <param name="mdlpatientsignup"></param>
        ///// <returns></returns>
        //[HttpPost]
        ////[CustomAuthorizeAttribute]
        //public ActionResult SubmitPatientDetail(Compose1Click mdlpatientsignup)
        //{
        //    clsCommon objCommon = new clsCommon();
        //    bool result = ReferralFormBLL.ComposeReferral1Click(mdlpatientsignup, true);
        //    TempData["success"] = result;
        //    return RedirectToAction("GetPatientReferralForm");
        //}
    }
}