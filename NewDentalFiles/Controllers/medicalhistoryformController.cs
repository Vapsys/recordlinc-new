﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using DentalFiles.Models;
using System.Configuration;
using JQueryDataTables.Models.Repository;
using DentalFiles.Models.ViewModel;
using System.Web.UI;
using DentalFiles.App_Start;

namespace DentalFilesNew.Controllers
{
    public class medicalhistoryformController : Controller
    {
        CommonDAL objCommonDAL = new CommonDAL();
        DataRepository objRepository = new DataRepository();
        List<BasicInfo> objBasicInfo = new List<BasicInfo>();
        public ActionResult Index()
        {
            objCommonDAL.GetActiveCompany();
            if (Request.QueryString["PatientId"] != null && Convert.ToInt32(Request.QueryString["PatientId"]) != 0)
            {
                SessionManagement.PatientId = (Convert.ToInt32(Request.QueryString["PatientId"]) - 45844584);


            }
            if (Convert.ToInt32(SessionManagement.PatientId) != 0)
            {

                if (Request.QueryString["UserId"] != null && Convert.ToInt32(Request.QueryString["UserId"]) != 0)
                {
                    DataSet dsMyPatients = objCommonDAL.GetPatientdetailsOfDoctordataset((Convert.ToInt32(Request.QueryString["UserId"]) - 45844584), 1, 10, SessionManagement.PatientId.ToString(), null, null, null, null, null, null, null, null);


                    if (dsMyPatients != null && dsMyPatients.Tables.Count > 0 && dsMyPatients.Tables[0].Rows.Count > 0)
                    { }
                    else
                    {
                        SessionManagement.PatientId = 0;
                        TempData["result"] = "false";
                        

                    }
                }
                else
                {
                    SessionManagement.PatientId = 0;
                    TempData["result"] = "false";
                    

                }
            }


            else
            {
                return RedirectToAction("Index", "Index");
            }

            objCommonDAL.GetActiveCompany();
            ViewBag.Gender = objCommonDAL.GetGender();
            ViewBag.Status = objCommonDAL.GetStatus();
            var model = new DentalFormsMaster();
            model.RegistrationForm = objRepository.GetRegistration(Convert.ToInt32(SessionManagement.PatientId));
            model.MedicalHistory = objRepository.GetMedicalHistory(Convert.ToInt32(SessionManagement.PatientId));
            return PartialView(model);
        }

    }
}
