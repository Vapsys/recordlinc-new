﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using DentalFiles.Models;
using System.Configuration;
using JQueryDataTables.Models.Repository;
using DentalFiles.Models.ViewModel;
using DentalFiles.App_Start;
using System.Web.Script.Serialization;
using System.IO;
using System.Web.Helpers;
using DataAccessLayer.Common;

namespace DentalFiles.Controllers
{
    public class HistoryController : Controller
    {
        int PatientId = 0;
        CommonDAL objCommonDAL = new CommonDAL();
        DataRepository objRepository = new DataRepository();
        DataTable dt = new DataTable();
        public ActionResult Index()
        {

            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                objCommonDAL.GetActiveCompany();
                ViewBag.PatientId = PatientId = Convert.ToInt32(SessionManagement.PatientId);
                ViewBag.Title = "My Info";
                ViewBag.ReturnUrl = "History";
                var model = new PatientAndForms();
                model.PatientDetialList = objRepository.GetPatientDetails(PatientId, SessionManagement.TimeZoneSystemName);

                if (model.PatientDetialList != null && model.PatientDetialList.Count != 0)
                {
                    ViewBag.FirstName =  model.PatientDetialList[0].FirstName;
                    if (!string.IsNullOrEmpty(model.PatientDetialList[0].ProfileImage.ToString()))
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["imagebankpath"] + model.PatientDetialList[0].ProfileImage.ToString();
                    }
                    else if (model.PatientDetialList[0].Gender == 1)
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"];
                    }
                    else
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["Doctorimage1"];

                    }

                    model.PatientDetialList[0].ProfileImage = ViewBag.ImageURL;

                }

                Messages objmsg = new Messages();
                DataTable dt = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0, 1, 5);
                DataTable dtCount = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0, 1, 100000);
                int count = 0;
                foreach (DataRow item in dtCount.Rows)
                {
                    if (!Convert.ToBoolean(item["Read_flag"]))
                    {
                        count = count + 1;
                    }
                }
                @ViewBag.MsgCount = count;


                model.Forms = objRepository.PatientForms("GetAllForms");
                model.FormStatus = objRepository.PatientFormsStatus(Convert.ToInt32(SessionManagement.PatientId), "SentPatientForms");
                if (model.PatientDetialList.Count != 0)
                {
                    ViewBag.Name = model.PatientDetialList[0].FirstName.ToString() + " " + model.PatientDetialList[0].LastName.ToString();
                    if (model.PatientDetialList[0].Gender.ToString() == "0")
                    {
                        ViewBag.Gender = "Male";
                    }
                    else
                    {
                        ViewBag.Gender = "Female";
                    }
                    if (model.PatientDetialList[0].Email != null && Convert.ToString(model.PatientDetialList[0].Email) != "")
                    {
                        Session["Email"] = model.PatientDetialList[0].Email;
                    }

                    ViewBag.ImageURL = model.PatientDetialList[0].ProfileImage;
                    ViewBag.DateOfBirth = model.PatientDetialList[0].DateOfBirth;
                    //-- Hardipsinh: No need to convert dateOfBirh again as per timezone as it is already converted.
                    //if (!string.IsNullOrEmpty(ViewBag.DateOfBirth))
                    //{
                    //    DateTime TempBirthDate = Convert.ToDateTime(ViewBag.DateOfBirth);
                    //    TempBirthDate = clsHelper.ConvertFromUTC(TempBirthDate, SessionManagement.TimeZoneSystemName);
                    //    ViewBag.DateOfBirth = TempBirthDate.ToString("MM/dd/yyyy");
                    //}

                }
                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Index");
            }
        }


        [HttpPost]
        public JsonResult GetPatientDetailOnEdit(string PatientId)
        {
            object obj = null;
            dt = objCommonDAL.GetPatientsDetails(Convert.ToInt32(PatientId));
            if (dt != null && dt.Rows.Count > 0)
            {
                obj = new
                {
                    Success = true,

                    PatientId = Convert.ToInt32(dt.Rows[0]["PatientId"]),
                    AssignedPatientId = Convert.ToString(dt.Rows[0]["AssignedPatientId"]),
                    FirstName = Convert.ToString(dt.Rows[0]["FirstName"]),
                    LastName = Convert.ToString(dt.Rows[0]["LastName"]),
                    MiddelName = Convert.ToString(dt.Rows[0]["MiddelName"]),
                    Country = Convert.ToString(dt.Rows[0]["Country"]),
                    DateOfBirth = objCommonDAL.CheckNull(dt.Rows[0]["DateOfBirth"].ToString(), null),
                    Gender = Convert.ToInt32(dt.Rows[0]["Gender"]),
                    ProfileImage = Convert.ToString(dt.Rows[0]["ProfileImage"]),
                    Phone = Convert.ToString(dt.Rows[0]["Phone"]),
                    Email = Convert.ToString(dt.Rows[0]["Email"]),
                    City = Convert.ToString(dt.Rows[0]["City"]),
                    Address = Convert.ToString(dt.Rows[0]["Address"]),
                    State = Convert.ToString(dt.Rows[0]["State"]),
                    Zip = Convert.ToString(dt.Rows[0]["Zip"]),
                    StartDate = Convert.ToString(dt.Rows[0]["StartDate"]),
                    Address2 = Convert.ToString(dt.Rows[0]["Address2"]),
                    GuarFirstName = Convert.ToString(dt.Rows[0]["GuarFirstName"]),
                    GuarLastName = Convert.ToString(dt.Rows[0]["GuarLastName"]),
                    GuarPhone = Convert.ToString(dt.Rows[0]["GuarPhone"]),
                    GuarEmail = Convert.ToString(dt.Rows[0]["GuarEmail"]),
                    FacebookUrl = objCommonDAL.GetFullExternalUrl(Convert.ToString(dt.Rows[0]["FacebookUrl"])),
                    TwitterUrl = objCommonDAL.GetFullExternalUrl(Convert.ToString(dt.Rows[0]["TwitterUrl"])),
                    LinkedinUrl = objCommonDAL.GetFullExternalUrl(Convert.ToString(dt.Rows[0]["LinkedinUrl"])),
                    GoogleUrl = objCommonDAL.GetFullExternalUrl(Convert.ToString(dt.Rows[0]["GoogleUrl"])),
                    BlogUrl = objCommonDAL.GetFullExternalUrl(Convert.ToString(dt.Rows[0]["BlogUrl"])),
                    OwnerId = Convert.ToInt32(dt.Rows[0]["OwnerId"]),

                };
            }


            return Json(obj, JsonRequestBehavior.DenyGet);

        }


        [HttpGet]
        public ActionResult PrintAll()
        {
            try
            {
                if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
                {


                    return RedirectToAction("Index", "PrintForms", new { print = "AllForms" });

                }
                else
                {
                    return RedirectToAction("Index", "Index");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        [HttpGet]
        public ActionResult ViewForms(String FormCode, String ReturnUrl)
        {

            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {


                if (FormCode.ToString() == "F1")
                {

                    TempData["FromId"] = "1";
                    return RedirectToAction("Index", "DentalForms");
                }
                else if (FormCode.ToString() == "F2")
                {

                    return RedirectToAction("Index", "DentalForms");
                }
                else if (FormCode.ToString() == "F3")
                {
                    TempData["FromId"] = "6";
                    return RedirectToAction("Index", "DentalForms");
                }
                else if (FormCode.ToString() == "F4")
                {
                    TempData["FromId"] = "5";
                    return RedirectToAction("Index", "DentalForms");

                }
                else if (FormCode.ToString() == "F5")
                {
                    TempData["FromId"] = "7";
                    return RedirectToAction("Index", "DentalForms");

                }
                else if (FormCode.ToString() == "F6")
                {
                    TempData["FromId"] = "4";
                    return RedirectToAction("Index", "DentalForms");

                }
                else if (FormCode.ToString() == "F7")
                {
                    TempData["FromId"] = "1";
                    return RedirectToAction("Index", "DentalForms");
                }

                else if (FormCode.ToString() == "F8")
                {
                    TempData["FromId"] = "9";
                    return RedirectToAction("Index", "DentalForms");
                }
                else
                {
                    return RedirectToAction("Index", "History");
                }
            }
            else
            {
                return RedirectToAction("Index", "History");
            }


        }


        [HttpGet]
        public ActionResult SendForms(String FormCode, String ReturnUrl, string FormId)
        {

            PatientDetails ob = new PatientDetails();
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                string Email = "";
                string FirstName = "";
                var model = new PatientAndForms();
                model.PatientDetialList = objRepository.GetPatientDetails(Convert.ToInt32(SessionManagement.PatientId), SessionManagement.TimeZoneSystemName);
                if (model.PatientDetialList[0].Email != null && Convert.ToString(model.PatientDetialList[0].Email) != "")
                {
                    Email = model.PatientDetialList[0].Email;
                }
                if (model.PatientDetialList[0].FirstName != null && Convert.ToString(model.PatientDetialList[0].FirstName) != "")
                {
                    FirstName = model.PatientDetialList[0].FirstName;
                }

                bool status = objCommonDAL.InsertPatientForms(Convert.ToInt32(SessionManagement.PatientId), "InsertPatientForms", model.PatientDetialList[0].OwnerId, Convert.ToInt32(FormId));

                // send form to patient

                TempData["Send"] = "Successfully Sent Forms To Patient";
                return View("Index", "History");
            }
            else
            {
                return RedirectToAction("Index", "Index");
            }


        }


        [HttpPost]
        public JsonResult EditPatientDetails(string FirstName, string LastName, string Gender, DateTime DateOfBirth, string Address, string Address2, string City, string State, string Phone)
        {
            object obj = null;
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                bool boolpatient = objCommonDAL.EditPatientDetails(Convert.ToInt32(SessionManagement.PatientId), FirstName, LastName, Gender, DateOfBirth, Address, Address2, City, State, Phone);

                string tempbirth = "";
                if (DateOfBirth != null)
                {
                    tempbirth = (DateOfBirth).ToString("MM/dd/yyyy");
                }


                if (tempbirth == "01/1/1900")
                {
                    tempbirth = "";
                }

                if (Phone == null || Phone == "")
                {
                    Phone = "-";
                }

                if (Address == null || Address == "")
                {
                    Address = "-";
                }



                obj = new
                {
                    Success = true,
                    Message = "Patient Details Updated successfully",
                    FirstName = FirstName,
                    LastName = LastName,
                    Gender = Gender,
                    DateOfBirth = tempbirth,
                    Address = Address,
                    Address2 = Address2,
                    City = City,
                    State = State,
                    Phone = Phone,

                };
                return Json(obj, JsonRequestBehavior.DenyGet);
            }
            else
            {
                return Json(new
                {
                    redirectUrl = Url.Action("Index", "Index"),
                    isRedirect = true
                });
            }
        }

        public string RemovepatientProfileImage()
        {
            bool status;
            status = objCommonDAL.RemovePatientprifileImage(SessionManagement.PatientId);
            return status.ToString();
        }

        
    }
}
