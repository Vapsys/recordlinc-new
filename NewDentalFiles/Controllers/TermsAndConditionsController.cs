﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using DentalFiles.Models;
using System.Configuration;
using JQueryDataTables.Models.Repository;
using DentalFiles.App_Start;

namespace DentalFilesNew.Controllers
{
    public class TermsAndConditionsController : Controller
    {

        DataRepository objRepository = new DataRepository();
        CommonDAL objCommonDAL = new CommonDAL();
        //
        // GET: /TermsAndConditions/

        public ActionResult Index()
        {
            ViewBag.Title = "Terms And Conditions";
            objCommonDAL.GetActiveCompany();
            ViewBag.ReturnUrl = "TermsAndConditions";
            if(Request.QueryString["m"] != null)
            {
                ViewBag.Ismobileview = true;
            }
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                ViewBag.PatientId = Convert.ToInt32(SessionManagement.PatientId);
                ViewBag.RequestHelp = "Request Help";
                objCommonDAL.GetActiveCompany();
                List<PatientDetails> PatientDetialList = new List<PatientDetails>();
                PatientDetialList = objRepository.GetPatientDetails(SessionManagement.PatientId, SessionManagement.TimeZoneSystemName);
                if (PatientDetialList.Count != 0)
                {
                    ViewBag.FirstName = PatientDetialList[0].FirstName.ToString();
                    if (!string.IsNullOrEmpty(PatientDetialList[0].ProfileImage.ToString()))
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["imagebankpath"] + PatientDetialList[0].ProfileImage.ToString();
                    }
                    else if (PatientDetialList[0].Gender == 1)
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"];
                        
                    }
                    else
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["Doctorimage1"];
                    }
                    if (PatientDetialList[0].StayloggedMins != null && PatientDetialList[0].StayloggedMins != "" && Convert.ToInt32(PatientDetialList[0].StayloggedMins) != 0)
                    {
                        Session.Timeout = Convert.ToInt32(PatientDetialList[0].StayloggedMins);
                    }
                    else
                    {
                        Session.Timeout = 60;
                    }
                    Messages objmsg = new Messages();
                    DataTable dt = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0, 1, 5);

                    DataTable dtCount = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0, 1, 100000);
                    int count = 0;
                    foreach (DataRow Count in dtCount.Rows)
                    {
                        if (!Convert.ToBoolean(Count["Read_flag"]))
                        {
                            count = count + 1;
                        }
                    }
                    @ViewBag.MsgCount = count;
                    HttpCookie myCookie = new HttpCookie("Timeout");
                    myCookie.Value = Convert.ToString(Session.Timeout);
                    myCookie.Expires = DateTime.Now.AddDays(1d);
                    Response.Cookies.Add(myCookie);
                }

                return View();
            }
            else
            {

                return View();
            }
        }

    }
}
