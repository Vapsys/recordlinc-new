﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentalFiles.Models;
using JQueryDataTables.Models.Repository;
using System.Data;
using DentalFiles.App_Start;
using System.IO;

namespace DentalFiles.Controllers
{
    public class TreatingDoctorsController : Controller
    {

        DataRepository objRepository = new DataRepository();
        CommonDAL objCommonDAL = new CommonDAL();
        public ActionResult Index()
        {
            objCommonDAL.GetActiveCompany();
            ViewBag.Title = "My Treating Doctors";
            ViewBag.ReturnUrl = "TreatingDoctors";
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                 
                 List<PatientDetails> PatientDetialList = objRepository.GetPatientDetails(Convert.ToInt32(SessionManagement.PatientId), SessionManagement.TimeZoneSystemName);
                 if (PatientDetialList != null &&  PatientDetialList.Count != 0)
                {
                    ViewBag.FirstName = PatientDetialList[0].FirstName;
                    if (!string.IsNullOrEmpty(PatientDetialList[0].ProfileImage.ToString()))
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["imagebankpath"]+ PatientDetialList[0].ProfileImage.ToString();
                    }
                    else if (PatientDetialList[0].Gender == 1)
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"];
                    }
                    else
                    {
                        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["Doctorimage1"];
                        
                    }
                    //-- Hardipsinh Jadeja - https://mantis.varianceinfotech.com/view.php?id=6717
                    //if (!System.IO.File.Exists(Convert.ToString(SessionManagement.CompanyWebsite+"/"+System.Configuration.ConfigurationManager.AppSettings["DentistImagespath"] + PatientDetialList[0].ProfileImage)))
                    //{
                    //    if (PatientDetialList[0].Gender == 1)
                    //    {
                    //        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"];
                    //    }
                    //    else
                    //    {
                    //        ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["Doctorimage1"];

                    //    }
                    //}
                }
               
                List<UserList> list = new List<UserList>();
                list = objRepository.GetTratingDoctors(Convert.ToInt32(SessionManagement.PatientId),1,15);
                Messages objmsg = new Messages();
                DataTable dt = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0,1,5);
                DataTable dtCount = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0, 1, 100000);
                int count = 0;
                foreach (DataRow Count in dtCount.Rows)
                {
                    if (!Convert.ToBoolean(Count["Read_flag"]))
                    {
                        count = count + 1;
                    }
                }
                @ViewBag.MsgCount = count;
                return View(list);
            }
            else
            {
                return RedirectToAction("Index", "Index");
            }
        }

        public string GetSearchResultOnScroll(int PageIndex)
        {
            List<UserList> list = new List<UserList>();
            list = objRepository.GetTratingDoctors(Convert.ToInt32(SessionManagement.PatientId), PageIndex, 15);
            if (list.Count == 0 && PageIndex > 1)
            {
                return "<center>No more record found</center>";
            }
            else if (list.Count == 0 && PageIndex == 1)
            {
                return "<center>No record found</center>";
            }
            ViewData.Model = list;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, "_partialListOfTreatingDoctors");
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
            
        }

        [HttpGet]
        public ActionResult sendreferral(int UserId)
        { 
            TempData["UserId"] = UserId;
            return RedirectToAction("Index", "sendreferral");
        }

    }
}
