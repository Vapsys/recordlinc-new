﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using DentalFiles.Models;
using System.Configuration;
using JQueryDataTables.Models.Repository;
using DentalFiles.Models.ViewModel;
using System.Collections;
using Recordlinc.Core.Common;
using DentalFiles.App_Start;
namespace DentalFiles.Controllers
{
    public class SendReferralController : Controller
    {

        int PatientId = 0;
        
        ArrayList regarding = new ArrayList();
        ArrayList regardlinglist = new ArrayList();
        ArrayList requestinglist = new ArrayList();
        ArrayList requesting = new ArrayList();
        string[] splitregard; string[] splitrequest;
        ArrayList teethlist = new ArrayList();
        DataTable getFilenames = new DataTable();
        CommonDAL objCommonDAL = new CommonDAL();
        DataRepository objRepository = new DataRepository();
        Hashtable tab = new Hashtable();
        public ActionResult Index()
        {



            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0 && Request.QueryString["mode"] != null && Request.QueryString["ReferralId"] != null && Convert.ToInt32(Request.QueryString["ReferralId"]) != 0 && Convert.ToString(Request.QueryString["mode"]) == "Forword")
            {
                ViewBag.Title = "Send Referral";
                ViewBag.ReturnUrl = "SendReferral";


                var model = getallvalue();


                DataTable toothdetails = objCommonDAL.getReferCardDetailsById(Convert.ToInt32(Request.QueryString["ReferralId"]));
                addTeethMap(toothdetails);
                Splitmethods(toothdetails);
                model.ViewReferral = objRepository.getReferCardDetailsByIdSingle(Convert.ToInt32(Request.QueryString["ReferralId"]));
                model.PatientUploadedImages = objRepository.GetPatientMontages(Convert.ToInt32(Request.QueryString["ReferralId"]));
                model.Notes = objRepository.getPatientNotes(Convert.ToInt32(SessionManagement.PatientId), Convert.ToInt32(Request.QueryString["ReferralId"]));
                string comments = "";
                foreach (var referral in model.ViewReferral)
                {

                    if (referral.Comments != null && referral.Comments.ToString() != "" && comments != referral.Comments)
                    {
                        comments = referral.Comments;
                        ViewBag.lblcomments += referral.Comments;
                    }
                }
                List<UserList> list = new List<UserList>();
                ViewBag.UserList = objRepository.GetTratingDoctors(Convert.ToInt32(SessionManagement.PatientId),1,int.MaxValue);
                Messages objmsg = new Messages();
                DataTable dt = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0,1,5);
                if (dt != null && dt.Rows.Count > 0)
                {
                    @ViewBag.MsgCount = dt.Rows[0]["TotalRecord"];
                }
                else
                {
                    @ViewBag.MsgCount = 0;
                }
                Session["mode"] = "True";
                return View(model);
            }

            else if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0 && Request.QueryString["mode"] != null && Convert.ToString(Request.QueryString["mode"]) == "Compose")
            {


                ViewBag.Title = "Send Referral";
                ViewBag.ReturnUrl = "SendReferral";
                var model = getallvalue();
                model.ViewReferral = objRepository.getReferCardDetailsByIdSingle(Convert.ToInt32(Request.QueryString["ReferralId"]));
                model.PatientUploadedImages = objRepository.PatientUploadedImages(PatientId, SessionManagement.TimeZoneSystemName);
                model.PatientUploadedFiles = objRepository.PatientUploadedFiles(PatientId, SessionManagement.TimeZoneSystemName);
                Session["mode"] = "True";
                List<UserList> list = new List<UserList>();
                ViewBag.UserList = objRepository.GetTratingDoctors(Convert.ToInt32(SessionManagement.PatientId),1,int.MaxValue);
                Messages objmsg = new Messages();
                DataTable dt = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0,1,5);
                if (dt != null && dt.Rows.Count > 0)
                {
                    @ViewBag.MsgCount = dt.Rows[0]["TotalRecord"];
                }
                else
                {
                    @ViewBag.MsgCount = 0;
                }
                return View(model);

            }

            else if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                if (TempData["UserId"] != null && Convert.ToInt32(TempData["UserId"]) != 0)
                {
                    SessionManagement.UserId = Convert.ToInt32(TempData["UserId"]);
                }
                ViewBag.Title = "Send Referral";
                ViewBag.ReturnUrl = "SendReferral";
                var model = getallvalue();
                model.ViewReferral = objRepository.getReferCardDetailsByIdSingle(Convert.ToInt32(Request.QueryString["ReferralId"]));
                model.PatientUploadedImages = objRepository.PatientUploadedImages(PatientId, SessionManagement.TimeZoneSystemName);
                model.PatientUploadedFiles = objRepository.PatientUploadedFiles(PatientId, SessionManagement.TimeZoneSystemName);
                Session["mode"] = "False";
                Messages objmsg = new Messages();
                DataTable dt = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0,1,5);
                if (dt != null && dt.Rows.Count > 0)
                {
                    @ViewBag.MsgCount = dt.Rows[0]["TotalRecord"];
                }
                else
                {
                    @ViewBag.MsgCount = 0;
                }
                return View(model);
            }


            else
            {
                return RedirectToAction("Index", "Index");
            }
        }


        private PatientAndUploadFiles getallvalue()
        {
            fillViewBagTeeth();
            objCommonDAL.GetActiveCompany();
            ViewBag.PatientId = PatientId = Convert.ToInt32(SessionManagement.PatientId);
            var model = new PatientAndUploadFiles();
            model.PatientDetails = objRepository.GetPatientDetails(PatientId, SessionManagement.TimeZoneSystemName);


            List<PatientDetails> PatientDetialList = new List<PatientDetails>();
            if (model.PatientDetails.Count != 0)
            {
                ViewBag.FirstName = model.PatientDetails[0].FirstName.ToString();
                ViewBag.ImageURL = model.PatientDetails[0].ProfileImage.ToString();
            }

            return model;


        }

        
        public JsonResult Submitdata(string SelectedUserId, string Regardingvalues, string Requestvalues, string Imagevalues, string Formsvalues, string hdnfeild, string hdnremove, string strother, string strother1, string comment)
        {
            if (SelectedUserId != null && Convert.ToString(SelectedUserId) != "" && Convert.ToString(SelectedUserId) != "undefined")
            {
                SessionManagement.UserId = Convert.ToInt32(SelectedUserId);
            }


            object obj = null;
            if (SessionManagement.UserId != 0 && SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.UserId) != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {


                sendingOperation(Convert.ToInt32(SessionManagement.UserId), Regardingvalues, Requestvalues, Imagevalues, Formsvalues, hdnfeild, hdnremove, strother, strother1, comment);

                obj = new
                {
                    Success = true,

                };
                return Json(obj, JsonRequestBehavior.AllowGet);


            }
            else
            {
                return Json(new
                {
                    redirectUrl = Url.Action("Index", "Index"),
                    isRedirect = true
                });
            }
        }

        private bool sendingOperation(int UserId, string Regardingvalues, string Requestvalues, string Imagevalues, string Formsvalues, string hdnfeild, string hdnremove, string strother, string strother1, string comment)
        {

            bool success;
            performSend(UserId, Regardingvalues, Requestvalues, Imagevalues, Formsvalues, hdnfeild, hdnremove, strother, strother1, comment);

            int status = Convert.ToInt32(ReferralStatus.Refered);
            success = true;
            return success;
        }

        private void performSend(int UserId, string Regardingvalues, string Requestvalues, string Imagevalues, string Formsvalues, string hdnfeild, string hdnremove, string strother, string strother1, string comment)
        {
            ArrayList ImgID = new ArrayList();
            ArrayList FormsID = new ArrayList();
            tab.Add("A", "33");
            tab.Add("B", "34");
            tab.Add("C", "35");
            tab.Add("D", "36");
            tab.Add("E", "37");
            tab.Add("F", "38");
            tab.Add("G", "39");
            tab.Add("H", "40");
            tab.Add("I", "41");
            tab.Add("J", "42");
            tab.Add("K", "43");
            tab.Add("L", "44");
            tab.Add("M", "45");
            tab.Add("N", "46");
            tab.Add("O", "47");
            tab.Add("P", "48");
            tab.Add("Q", "49");
            tab.Add("R", "50");
            tab.Add("S", "51");
            tab.Add("T", "52");

            #region Regarding Checked

            string regardstr = Regardingvalues;

            #endregion

            #region Request Checked
            string requeststr = Requestvalues;
            #endregion


            if (Imagevalues != null && Imagevalues != "")
            {
                string[] Image = Imagevalues.Split(',');
                foreach (string str in Image)
                {
                    ImgID.Add(str);
                }
            }

            else
            {
                ImgID.Clear();
            }


            if (Formsvalues != null && Formsvalues != "")
            {
                string[] Forms = Formsvalues.Split(',');
                foreach (string str in Forms)
                {
                    FormsID.Add(str);
                }
            }

            else
            {
                FormsID.Clear();
            }

            string val = "";
            val = hdnfeild;
            string removevals = hdnremove;
            string[] removetokens = removevals.Split(',');
            string[] tokens = val.Split(',');
            foreach (string str in tokens)
            {
                if (str != "")
                {
                    if (TempData["teethlist"] != null)
                        teethlist = (ArrayList)TempData["teethlist"];
                    if (!teethlist.Contains(str))
                    {
                        if (tab.Contains(str))
                        {
                            int value = Convert.ToInt32(tab[str]);

                            teethlist.Add(Convert.ToString(value));
                            for (int i = 0; i < removetokens.Length; i++)
                            {
                                if (removetokens[i].Contains(str))
                                {

                                    removetokens[i] = Convert.ToString(value);
                                    break;
                                }
                            }
                        }
                        else if (!tab.Contains(str))
                        {
                            teethlist.Add(str);
                        }
                        TempData["teethlist"] = teethlist;
                    }

                }
            }
            if (removevals != "")
            {
                for (int i = 0; i < removetokens.Length; i++)
                {
                    if (removetokens[i] != "")
                    {
                        if (TempData["teethlist"] != null)
                            teethlist = (ArrayList)TempData["teethlist"];
                        if (teethlist.Contains(removetokens[i]))
                        {
                            teethlist.Remove(removetokens[i]);
                            TempData["teethlist"] = teethlist;
                        }

                    }
                }
            }

            int ReferedUserId = UserId;

            if (TempData["teethlist"] != null)

                teethlist = (ArrayList)TempData["teethlist"];
            bool referresult = objCommonDAL.AddReferCard(Convert.ToInt32(SessionManagement.PatientId), UserId, Regardingvalues, Requestvalues, comment, strother, strother1, "0", teethlist, ImgID, FormsID);



        }


        public void addTeethMap(DataTable teethMapSet)
        {
            try
            {

                DataTable teethMapTable = teethMapSet;



                foreach (DataRow TeethMap in teethMapTable.Rows)
                {
                    string val = TeethMap["ToothType"].ToString();
                    #region Permenant Tooth
                    if (val == "1")
                    {
                        @ViewBag.div1 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "2")
                    {

                        @ViewBag.div2 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "3")
                    {

                        @ViewBag.div3 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "4")
                    {

                        @ViewBag.div4 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "5")
                    {

                        @ViewBag.div5 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "6")
                    {

                        @ViewBag.div6 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "7")
                    {

                        @ViewBag.div7 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "8")
                    {

                        @ViewBag.div8 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "9")
                    {

                        @ViewBag.div9 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "10")
                    {

                        @ViewBag.div10 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "11")
                    {

                        @ViewBag.div11 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "12")
                    {

                        @ViewBag.div12 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "13")
                    {

                        @ViewBag.div13 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "14")
                    {

                        @ViewBag.div14 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "15")
                    {

                        @ViewBag.div15 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "16")
                    {

                        @ViewBag.div16 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "17")
                    {

                        @ViewBag.div17 = "/DentalImages/seprat_teeth_black.jpg";

                    } if (val == "18")
                    {

                        @ViewBag.div18 = "/DentalImages/seprat_teeth_black.jpg";

                    } if (val == "19")
                    {

                        @ViewBag.div19 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "20")
                    {

                        @ViewBag.div20 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "21")
                    {

                        @ViewBag.div21 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "22")
                    {

                        @ViewBag.div22 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "23")
                    {

                        @ViewBag.div23 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "24")
                    {

                        @ViewBag.div24 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "25")
                    {

                        @ViewBag.div25 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "26")
                    {

                        @ViewBag.div26 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "27")
                    {

                        @ViewBag.div27 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "28")
                    {

                        @ViewBag.div28 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "29")
                    {

                        @ViewBag.div29 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "30")
                    {

                        @ViewBag.div30 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "31")
                    {

                        @ViewBag.div31 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "32")
                    {

                        @ViewBag.div32 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "33")
                    {

                        @ViewBag.div33 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "34")
                    {

                        @ViewBag.div34 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "35")
                    {

                        @ViewBag.div35 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "36")
                    {

                        @ViewBag.div36 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "37")
                    {

                        @ViewBag.div37 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "38")
                    {

                        @ViewBag.div38 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "39")
                    {

                        @ViewBag.div39 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "40")
                    {

                        @ViewBag.div40 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "41")
                    {

                        @ViewBag.div41 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "42")
                    {

                        @ViewBag.div42 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "43")
                    {

                        @ViewBag.div43 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "44")
                    {

                        @ViewBag.div44 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "45")
                    {

                        @ViewBag.div45 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "46")
                    {

                        @ViewBag.div46 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "47")
                    {

                        @ViewBag.div47 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "48")
                    {

                        @ViewBag.div48 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "49")
                    {

                        @ViewBag.div49 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "50")
                    {

                        @ViewBag.div50 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "51")
                    {

                        @ViewBag.div51 = "/DentalImages/seprat_teeth_black.jpg";

                    }
                    if (val == "52")
                    {

                        @ViewBag.div52 = "/DentalImages/seprat_teeth_black.jpg";

                    }

                    #endregion
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private void fillViewBagTeeth()
        {
            @ViewBag.div1 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div2 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div3 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div4 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div5 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div6 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div7 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div8 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div9 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div10 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div11 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div12 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div13 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div14 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div15 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div16 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div17 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div18 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div19 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div20 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div21 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div22 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div23 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div24 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div25 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div26 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div27 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div28 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div29 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div30 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div31 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div32 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div33 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div34 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div35 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div36 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div37 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div38 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div39 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div40 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div41 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div42 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div43 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div44 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div45 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div46 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div47 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div48 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div49 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div50 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div51 = "/DentalImages/seprat_teeth_blue.jpg";
            @ViewBag.div52 = "/DentalImages/seprat_teeth_blue.jpg";
        }

        public void Splitmethods(DataTable toothdetail)
        {
            try
            {
                #region RegardingBind
                foreach (DataRow Tooth in  toothdetail.Rows)
                {
                    string regard = Tooth["RegardOption"].ToString();

                    if (regard != "")
                    {
                        splitregard = regard.Split(',');

                        foreach (string strreg in splitregard)
                        {
                            if (!regarding.Contains(strreg))
                            {
                                if (strreg != "")
                                    regarding.Add(strreg);

                            }
                        }

                    }

                }

                #endregion
                #region RequestBind

                foreach (DataRow Tooth in toothdetail.Rows)
                {
                    string request = Tooth["RequestingOption"].ToString();

                    if (request != "")
                    {
                        splitrequest = request.Split(',');
                        foreach (string strreq in splitrequest)
                        {
                            if (!requesting.Contains(strreq))
                            {
                                if (strreq != "")
                                    requesting.Add(strreq);

                            }
                        }
                    }
                }

                #endregion
                ViewBag.regardlinglist = regarding;
                ViewBag.requestinglist = requesting;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
