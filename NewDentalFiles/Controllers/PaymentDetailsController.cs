﻿using BO.Models;
using BusinessLogicLayer;
using DentalFiles.App_Start;
using DentalFiles.Models;
using DentalFiles.Models.ViewModel;
using DentalFilesNew.App_Start;
using JQueryDataTables.Models.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DentalFilesNew.Controllers
{
    public class PaymentDetailsController : Controller
    {
        // GET: PaymentDetail
        public ActionResult Index()
        {
            CommonDAL objCommonDAL = new CommonDAL();
            DataRepository objRepository = new DataRepository();
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                List<PatientDetails> PatientDetialList = objRepository.GetPatientDetails(Convert.ToInt32(SessionManagement.PatientId), SessionManagement.TimeZoneSystemName);
                ViewBag.Title = "PaymentDetails";
                ViewBag.ReturnUrl = "PaymentDetails";
                ViewBag.FirstName = PatientDetialList[0].FirstName;
                if (!string.IsNullOrEmpty(PatientDetialList[0].ProfileImage.ToString()))
                {
                    ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["imagebankpath"] + PatientDetialList[0].ProfileImage.ToString();
                }
                else if (PatientDetialList[0].Gender == 1)
                {
                    ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"];
                }
                else
                {
                    ViewBag.ImageURL = System.Configuration.ConfigurationManager.AppSettings["Doctorimage1"];

                }
                HttpCookie myCookie = new HttpCookie("Timeout");
                myCookie.Value = Convert.ToString(Session.Timeout);
                myCookie.Expires = DateTime.Now.AddDays(1d);
                Response.Cookies.Add(myCookie);

                DataTable dtCount = objCommonDAL.GetAllMessages(Convert.ToInt32(SessionManagement.PatientId), 0, 1, 100000);
                int count = 0;
                foreach (DataRow Count in dtCount.Rows)
                {
                    if (!Convert.ToBoolean(Count["Read_flag"]))
                    {
                        count = count + 1;
                    }
                }
                @ViewBag.MsgCount = count;
            }
            //List<MerchantOwner> objPatinentPaymentList = new List<MerchantOwner>();
            //objPatinentPaymentList = ZPPaymentBAL.PartialMerchantDetail(Convert.ToInt32(SessionManagement.PatientId));
            return View();
        }
        [CustomAuthorizeAttribute]
        public ActionResult PaymentDetails()
        {
            List<MerchantOwner> objPatinentPaymentList = new List<MerchantOwner>();
            objPatinentPaymentList = ZPPaymentBAL.PartialMerchantDetail(Convert.ToInt32(SessionManagement.PatientId));
            return Json(new { data = objPatinentPaymentList }, JsonRequestBehavior.AllowGet);
        }
    }
}