﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentalFiles.Models;
using System.Data;
using JQueryDataTables.Models.Repository;
using BusinessLogicLayer;

namespace DentalFiles.Controllers
{

    public class SearchResultController : Controller
    {
        CommonDAL objCommonDAL = new CommonDAL();
        string txtcityorzip = null;
        string txtlastname = null;
        int drpspeciality = 0;
        int drpdistance = 0;
        string keyword = null;
        string Firstname = null;
        string Title = null;
        string CompanyName = null;
        string School = null;
        string Email = null;
        string phone = null;
        string zipcode = null;
        string SpecialtityList = null;
        DataRepository objRepository = new DataRepository();
        //
        // GET: /SearchResult/

        public ActionResult NEWIndex()
        {
            TempData["NoRecord"] = null;
            TempData["_ZipCodeOrCity"] = null;
            TempData["Count"] = "0";
            objCommonDAL.GetActiveCompany();

            ModelState.Clear();
            if (Session["_ZipCodeOrCity"] != null && Convert.ToString(Session["_ZipCodeOrCity"]) != "")
            {
                TempData["_ZipCodeOrCity"] = Session["_ZipCodeOrCity"].ToString();
            }


            if (Session["_LastName"] != null && Convert.ToString(Session["_LastName"]) != "")
            {
                TempData["_LastName"] = Session["_LastName"].ToString();
            }

            if (Session["_Speciality"] != null && Convert.ToString(Session["_Speciality"]) != "")
            {
                TempData["_Speciality"] = Session["_Speciality"].ToString();
            }
            return View();



        }



        [HttpPost]
        public ActionResult GetNextUsersOnScroll(UserStatus status, int limit, int fromRowNumber, string _keywords, string _FirstName, string _LastName, string _Title, string _CompanyName, string _School, string _Email, string _Phone, string _Distance, string _ZipCode, string _SpecialtityList, string _City)
        {


            RequiredFiedModel obj = new RequiredFiedModel();
            string a = obj.CityOrZip;
            string City = null;

            List<UserList> list = new List<UserList>();

            if (Session["_ZipCodeOrCity"] != null && Convert.ToString(Session["_ZipCodeOrCity"]) != "")
            {
                bool IsDigit = objCommonDAL.isNumeric(Convert.ToString(Session["_ZipCodeOrCity"]));
                if (IsDigit == true)
                {
                    zipcode = Session["_ZipCodeOrCity"].ToString();

                }
                else
                {

                    City = Session["_ZipCodeOrCity"].ToString();
                    Session["_OnlyCity"] = Session["_ZipCodeOrCity"].ToString();

                }
                TempData["_ZipCodeOrCity"] = Session["_ZipCodeOrCity"].ToString();


            }

            Session["_ZipCodeOrCity"] = null;



            if (Session["_LastName"] != null && Convert.ToString(Session["_LastName"]) != "")
            {
                txtlastname = Session["_LastName"].ToString();
                Session["_LastName"] = null;
            }
            if (Session["_Speciality"] != null && Convert.ToString(Session["_Speciality"]) != "")
            {
                drpspeciality = Convert.ToInt32(Session["_Speciality"]);
                Session["_Speciality"] = null;
            }
            else
            {
                drpspeciality = 0;
            }
            if (Session["_Distance"] != null && Convert.ToString(Session["_Distance"]) != "")
            {
                drpdistance = Convert.ToInt32(Session["_Distance"]);
                Session["_Distance"] = null;
            }
            else
            {
                drpdistance = 0;
            }
            if (_keywords != null && Convert.ToString(_keywords) != "" && Convert.ToString(_keywords) != "Keywords")
            {
                keyword = _keywords;
            }
            else
            {
                keyword = null;
            }
            if (_FirstName != null && Convert.ToString(_FirstName) != "" && Convert.ToString(_FirstName) != "First Name")
            {
                Firstname = _FirstName;
            }
            else
            {
                Firstname = null;
            }
            if (_LastName != null && Convert.ToString(_LastName) != "" && Convert.ToString(_LastName) != "Last Name")
            {
                txtlastname = _LastName;
            }
            else
            {
                txtlastname = null;
            }
            if (_Title != null && Convert.ToString(_Title) != "" && Convert.ToString(_Title) != "Title")
            {
                Title = _Title;
            }
            else
            {
                Title = null;
            }
            if (_CompanyName != null && Convert.ToString(_CompanyName) != "" && Convert.ToString(_CompanyName) != "Company")
            {
                CompanyName = _CompanyName;
            }
            else
            {
                CompanyName = null;
            }
            if (_School != null && Convert.ToString(_School) != "" && Convert.ToString(_School) != "School")
            {
                School = _School;
            }
            else
            {
                School = null;
            }
            if (_Email != null && Convert.ToString(_Email) != "" && Convert.ToString(_Email) != "Email")
            {
                Email = _Email;
            }
            else
            {
                Email = null;
            }
            if (_Phone != null && Convert.ToString(_Phone) != "" && Convert.ToString(_Phone) != "Phone Number")
            {
                string p = _Phone.Replace('(', ' ').Replace(')', ' ').Replace('-', ' ');
                string ph = p.Replace(" ","");
                phone = ph;
            }
            else
            {
                phone = null;
            }
            if (_Distance != null && Convert.ToString(_Distance) != "")
            {
                drpdistance = Convert.ToInt32(_Distance);
            }
            if (_ZipCode != null && Convert.ToString(_ZipCode) != "" && Convert.ToString(_ZipCode) != "Zip Code")
            {
                zipcode = Convert.ToString(_ZipCode);

            }
            else
            {
                zipcode = null;
            }
            if (_SpecialtityList != null && Convert.ToString(_SpecialtityList) != "")
            {
                SpecialtityList = Convert.ToString(_SpecialtityList);
            }
            if (_City != null && Convert.ToString(_City) != "")
            {
                City = _City;
            }

            if (Session["_OnlyCity"] != null && Convert.ToString(Session["_OnlyCity"]) != "" && _City != "false")
            {
                City = Convert.ToString(Session["_OnlyCity"]);
                Session["_OnlyCity"] = null;
            }
            else
            {
                City = null;
                Session["_OnlyCity"] = null;
            }

            list = objRepository.GetDentistListBySearch(keyword, txtcityorzip, txtlastname, drpspeciality, drpdistance, Firstname, Title, CompanyName, School, Email, phone, zipcode, fromRowNumber, limit, SpecialtityList, City);
            ModelState.Clear();

            if (list.Count == 0 && fromRowNumber == 1)
            {
                TempData["NoRecord"] = "No Record Found";
            }
            else if (list.Count == 0)
            {
                TempData["NoRecord"] = "No More Record Found";

            }
            return PartialView("_UserList", list);


        }





        [HttpGet]
        public ActionResult SelectedDentist(string UserId)
        {
            TempData["UserId"] = UserId;

            return RedirectToAction("Index", "PatientSignUp", FormMethod.Post);

        }


        // GET: /SearchResult/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /SearchResult/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /SearchResult/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /SearchResult/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /SearchResult/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /SearchResult/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /SearchResult/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
        public ActionResult Index()
        {
            TempData["NoRecord"] = null;
            TempData["_ZipCodeOrCity"] = null;
            TempData["Count"] = "0";
            objCommonDAL.GetActiveCompany();
            _AdvancedSearchColl obj = new _AdvancedSearchColl();
            ModelState.Clear();
            if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["_ZipCodeOrCity"])))
            {
                string Mystring = Convert.ToString(Session["_ZipCodeOrCity"]);
                if (Mystring.All(char.IsDigit))
                {
                    obj._ZipCode = Mystring;
                }
                else
                {
                    obj._City = Mystring;
                }
                TempData["_ZipCodeOrCity"] = Session["_ZipCodeOrCity"].ToString();
            }
            if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["_LastName"])))
            {
                obj._Lastname = Convert.ToString(Session["_LastName"]);
                TempData["_LastName"] = Session["_LastName"].ToString();
            }
            if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["_Speciality"])))
            {
                obj._Title = Convert.ToString(Session["_Speciality"]);
                TempData["_Speciality"] = Convert.ToString(Session["_Speciality"]);
            }
            SearchProfileOfDoctor ObjOfBLL = new SearchProfileOfDoctor();
            obj.lst = ObjOfBLL.DoctorSpecialities();
            return View("NewIndex",obj);
        }
        public ActionResult PartialSearchResult()
        {

            return View();
        }
        public ActionResult UserList(UserStatus status, int limit, int fromRowNumber, string _keywords, string _FirstName, string _LastName, string _Title, string _CompanyName, string _School, string _Email, string _Phone, string _Distance, string _ZipCode, string _SpecialtityList, string _City)
        {

            RequiredFiedModel obj = new RequiredFiedModel();
            string a = obj.CityOrZip;
            string City = null;

            List<UserList> list = new List<UserList>();

            if (Session["_ZipCodeOrCity"] != null && Convert.ToString(Session["_ZipCodeOrCity"]) != "")
            {
                bool IsDigit = objCommonDAL.isNumeric(Convert.ToString(Session["_ZipCodeOrCity"]));
                if (IsDigit == true)
                {
                    zipcode = Session["_ZipCodeOrCity"].ToString();

                }
                else
                {

                    City = Session["_ZipCodeOrCity"].ToString();
                    Session["_OnlyCity"] = Session["_ZipCodeOrCity"].ToString();

                }
                TempData["_ZipCodeOrCity"] = Session["_ZipCodeOrCity"].ToString();


            }

            Session["_ZipCodeOrCity"] = null;



            if (Session["_LastName"] != null && Convert.ToString(Session["_LastName"]) != "")
            {
                txtlastname = Session["_LastName"].ToString();
                Session["_LastName"] = null;
            }
            if (Session["_Speciality"] != null && Convert.ToString(Session["_Speciality"]) != "")
            {
                drpspeciality = Convert.ToInt32(Session["_Speciality"]);
                Session["_Speciality"] = null;
            }
            else
            {
                drpspeciality = 0;
            }
            if (Session["_Distance"] != null && Convert.ToString(Session["_Distance"]) != "")
            {
                drpdistance = Convert.ToInt32(Session["_Distance"]);
                Session["_Distance"] = null;
            }
            else
            {
                drpdistance = 0;
            }
            if (_keywords != null && Convert.ToString(_keywords) != "" && Convert.ToString(_keywords) != "Keywords")
            {
                keyword = _keywords;
            }
            else
            {
                keyword = null;
            }
            if (_FirstName != null && Convert.ToString(_FirstName) != "" && Convert.ToString(_FirstName) != "First Name")
            {
                Firstname = _FirstName;
            }
            else
            {
                Firstname = null;
            }
            if (_LastName != null && Convert.ToString(_LastName) != "" && Convert.ToString(_LastName) != "Last Name")
            {
                txtlastname = _LastName;
            }
            else
            {
                txtlastname = null;
            }
            if (_Title != null && Convert.ToString(_Title) != "" && Convert.ToString(_Title) != "Title")
            {
                Title = _Title;
            }
            else
            {
                Title = null;
            }
            if (_CompanyName != null && Convert.ToString(_CompanyName) != "" && Convert.ToString(_CompanyName) != "Company")
            {
                CompanyName = _CompanyName;
            }
            else
            {
                CompanyName = null;
            }
            if (_School != null && Convert.ToString(_School) != "" && Convert.ToString(_School) != "School")
            {
                School = _School;
            }
            else
            {
                School = null;
            }
            if (_Email != null && Convert.ToString(_Email) != "" && Convert.ToString(_Email) != "Email")
            {
                Email = _Email;
            }
            else
            {
                Email = null;
            }
            if (_Phone != null && Convert.ToString(_Phone) != "" && Convert.ToString(_Phone) != "Phone Number")
            {
                string p = _Phone.Replace('(', ' ').Replace(')', ' ').Replace('-', ' ');
                string ph = p.Replace(" ", "");
                phone = ph;
            }
            else
            {
                phone = null;
            }
            if (_Distance != null && Convert.ToString(_Distance) != "")
            {
                drpdistance = Convert.ToInt32(_Distance);
            }
            if (_ZipCode != null && Convert.ToString(_ZipCode) != "" && Convert.ToString(_ZipCode) != "Zip Code")
            {
                zipcode = Convert.ToString(_ZipCode);

            }
            else
            {
                zipcode = null;
            }
            if (_SpecialtityList != null && Convert.ToString(_SpecialtityList) != "")
            {
                SpecialtityList = Convert.ToString(_SpecialtityList);
            }
            if (_City != null && Convert.ToString(_City) != "")
            {
                City = _City;
            }

            if (Session["_OnlyCity"] != null && Convert.ToString(Session["_OnlyCity"]) != "" && _City != "false")
            {
                City = Convert.ToString(Session["_OnlyCity"]);
                Session["_OnlyCity"] = null;
            }
            else
            {
                //City = null;
                Session["_OnlyCity"] = null;
            }

            list = objRepository.GetDentistListBySearch(keyword, txtcityorzip, txtlastname, drpspeciality, drpdistance, Firstname, Title, CompanyName, School, Email, phone, zipcode, fromRowNumber, limit, SpecialtityList, City);
            ModelState.Clear();

            if (list.Count == 0 && fromRowNumber == 1)
            {
                TempData["NoRecord"] = "No Record Found";
            }
            else if (list.Count == 0)
            {
                TempData["NoRecord"] = "No More Record Found";

            }
            return PartialView("ListofUser", list);
        }
    }
}
