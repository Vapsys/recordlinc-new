﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using DentalFiles.Models;
using System.Configuration;
using DentalFiles.App_Start;
namespace DentalFiles.Controllers
{

    public class AboutUsController : Controller
    {
        CommonDAL objCommonDAL = new CommonDAL();
     
        public ActionResult Index()
        {
            if (SessionManagement.PatientId != 0 && Convert.ToInt32(SessionManagement.PatientId) != 0)
            {
                return RedirectToAction("Index", "DashBoard");
            }
            else
            {
                ViewBag.Title = "About Us";
                objCommonDAL.GetActiveCompany();
                ViewBag.ReturnUrl = "aboutus";
                return View();
            }
            
        }

    }
}
