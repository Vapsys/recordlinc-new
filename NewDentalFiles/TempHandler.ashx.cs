﻿using System;
using System.Web;
using System.Web.Script.Serialization;

using System.IO;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using DentalFiles.Models;
using System.Configuration;
using JQueryDataTables.Models.Repository;
using DataAccessLayer.Common;

namespace DentalFilesNew
{
    /// <summary>
    /// Summary description for TempHandler
    /// </summary>
    public class TempHandler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        CommonDAL objCommonDAL = new CommonDAL();
        public void ProcessRequest(HttpContext context)
        {
            string viewstatvalue = clsCommon.GetUniquenumberForAttachments();
            context.Response.ContentType = "text/plain";//"application/json";
            int userid = Convert.ToInt32(context.Session["PatientId"].ToString());


            if (context.Session["viewstatvalue"] == null || Convert.ToString(context.Session["viewstatvalue"]) == "")
            {
                context.Session["viewstatvalue"] = Guid.NewGuid().ToString();

            }
            HttpPostedFile postedFile = context.Request.Files["Filedata"];
            string Sesionid = context.Session["PatientId"].ToString();

            var r = new System.Collections.Generic.List<ViewDataUploadFilesResult>();
            JavaScriptSerializer js = new JavaScriptSerializer();

           
            for (int filecount = 0; filecount < context.Request.Files.Count; filecount++)
            {
                HttpPostedFile hpf =  context.Request.Files[filecount] as HttpPostedFile;
                string FileName = string.Empty;
                if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
                {
                    string[] files = hpf.FileName.Split(new char[] { '\\' });
                    FileName = files[files.Length - 1];
                }
                else
                {
                    FileName = hpf.FileName;
                }
                if (FileName.Contains("\\"))
                {
                    string[] files = hpf.FileName.Split(new char[] { '\\' });
                    FileName = files[files.Length - 1];
                }
                if (FileName.Contains("/"))
                {
                    string[] files = hpf.FileName.Split(new char[] { '/' });
                    FileName = files[files.Length - 1];
                }
                string filename1 = System.Configuration.ConfigurationManager.AppSettings.Get("TempUploads") + "$" + System.DateTime.Now.Ticks + "$" + FileName;

                string extension;
                if (hpf.ContentLength == 0)
                    continue;
                extension = Path.GetExtension(FileName);


                if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd" )
                {
                    objCommonDAL.InsertFiles(filename1, Convert.ToInt32(Sesionid.Split('?')[0]), viewstatvalue);

                }
                else
                {
                    objCommonDAL.InsertImages(filename1, Convert.ToInt32(Sesionid.Split('?')[0]), viewstatvalue);


                }

                string savepath = "";
                string tempPath = "";
                tempPath = System.Configuration.ConfigurationManager.AppSettings["TempUploadsFolderPath"].ToString();
                savepath = context.Server.MapPath(tempPath);
                savepath = savepath.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");
                string newpath = Path.Combine(savepath + filename1);

                hpf.SaveAs(Path.Combine(savepath + filename1));
                
            }
            
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

    }
}