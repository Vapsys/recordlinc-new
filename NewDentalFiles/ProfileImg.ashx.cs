﻿using System;
using System.Web;
using System.Web.Script.Serialization;

using System.IO;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using DentalFiles.Models;
using System.Configuration;
using JQueryDataTables.Models.Repository;
using System.Web.Helpers;

namespace DentalFilesNew
{
    /// <summary>
    /// Summary description for ProfileImg
    /// </summary>
    public class ProfileImg : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        CommonDAL objCommonDAL = new CommonDAL();
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";//"application/json";
            int userid = Convert.ToInt32(context.Session["PatientId"].ToString());


            try
            {
                HttpPostedFile postedFile = context.Request.Files["Filedata"];
                string Sesionid = context.Session["PatientId"].ToString();

                var r = new System.Collections.Generic.List<ViewDataUploadFilesResult>();
                JavaScriptSerializer js = new JavaScriptSerializer();
                for (int filecount = 0; filecount < context.Request.Files.Count; filecount++)
                {


                    HttpPostedFile hpf = context.Request.Files[filecount] as HttpPostedFile;
                    string FileName = string.Empty;
                    if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
                    {
                        string[] files = hpf.FileName.Split(new char[] { '\\' });
                        FileName = files[files.Length - 1];
                    }
                    else
                    {
                        FileName = hpf.FileName;
                        string[] files = hpf.FileName.Split(new char[] { '\\' });
                        FileName = files[files.Length - 1];
                    }


                    string imgName = FileName + "_" + System.DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss") + ".jpeg";
                    string OrgImagePath = ConfigurationManager.AppSettings.Get("imagebankpath") + imgName;
                    string ImagePath = imgName;
                    
                    string filename1 = System.Configuration.ConfigurationManager.AppSettings.Get("imagebankpath") + "$" + System.DateTime.Now.Ticks + "$" + FileName;

                    string thambnailimage = filename1.Replace(System.Configuration.ConfigurationManager.AppSettings.Get("imagebankpath"), System.Configuration.ConfigurationManager.AppSettings.Get("imagebankpathThumb"));
                    string extension;
                    if (hpf.ContentLength == 0)
                        continue;
                    
                    extension = Path.GetExtension(FileName);


                    if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd" && extension.ToLower() != ".pspimage" && extension.ToLower() != ".thm" && extension.ToLower() != ".tif")
                    {
                    }
                    else
                    {

                        objCommonDAL.UpdatePatientProfileImg(userid, filename1);
                        string savepath = "";
                        string tempPath = "";
                        tempPath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
                        savepath = context.Server.MapPath(tempPath);
                        savepath = savepath.Replace("PublicProfile\\", "");
                        //hpf.SaveAs(savepath + @"\" + filename1);

                        /////////////// Croping //////////////////////////

                        int Bottom = Convert.ToInt32(Math.Floor(Convert.ToDouble(context.Request["Bottom"].ToString())));
                        int Right = Convert.ToInt32(Math.Floor(Convert.ToDouble(context.Request["Right"].ToString())));
                        int Top = Convert.ToInt32(Math.Floor(Convert.ToDouble(context.Request["Top"].ToString())));
                        int Left = Convert.ToInt32(Math.Floor(Convert.ToDouble(context.Request["Left"].ToString())));
                        
                        var imagecrop = new WebImage(hpf.InputStream);
                        var height = imagecrop.Height;
                        var width = imagecrop.Width;
                        imagecrop.Crop((int)Top, (int)Left, (int)(height - Bottom), (int)(width - Right));
                        int maxPixels = Convert.ToInt32(ConfigurationManager.AppSettings["Thumbimagediamention"]);
                        imagecrop.Resize(maxPixels, maxPixels, true, false);
                        imagecrop.Save(savepath + @"\" + filename1);
                    
                    }
                }

                context.Response.Redirect(context.Request["RedirectURL"]);  

            }
            catch(Exception ex) {
                throw;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}