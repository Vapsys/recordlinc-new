﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Drawing;
using System.IO;

namespace DentalFiles.Models
{
    /// <summary>
    /// Company class contains information that shoudl be shown in table
    /// </summary>
    public class UserList
    {
        static int nextID = 17;

        public UserList()
        {
            UserID = nextID++;
             Parameters = new Dictionary<string, object>();
        }
        public int UserID { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string Email { set; get; }
        public string Description { set; get; }
        public string Username { set; get; }
        public string image { set; get; }
        public string name { set; get; }
        public string State { set; get; }
        public string City { set; get; }
        public string Zipcode { set; get; }
        public string Country { set; get; }
        public string ExactAddress { set; get; }
        public string Address2 { set; get; }
        public string Miles { set; get; }
        public string PublicPath { set; get; }
        public string RowNumber { set; get; }
        public string TotalRecord { set; get; }
        public string Specialities { set; get; }
        public string Phone { set; get; }
        public string AccountName { set; get; }
        public string Gender { get; set; }
        public string MapAddress { get; set; }
        public int SpecialityId { set; get; }
        public string Location { get; set; }
        public string ImageName { get; set; }
        public string Specialty { get; set; }
        public int Id { set; get; }
        public string Website { get;set;}

      



        public UserList(int limit, int fromRowNumber, string containerId,
            string ajaxActionUrl, IDictionary<string, object> parameters = null)
            {
            Limit = limit;
            FromRowNumber = fromRowNumber;
            ContainerId = containerId;
            AjaxActionUrl = ajaxActionUrl;
            if (parameters != null)
                Parameters = parameters;

        }

        public int Limit { get; set; }
        public int FromRowNumber { get; set; }
        
        public string ContainerId { get; set; }
        public string AjaxActionUrl { get; set; }
        public IDictionary<string, object> Parameters { get; set; }
    }
   
    public class GetTreatingDoctors
    {
        public int DoctorId { set; get; }
        public string Name { set; get; }
        public string Specialties { set; get; }
        public string ThumbImageUrl { set; get; }
        public string PracticeName { get; set; }
        public string Education { get; set; }
        
    }

    public class SearchDentist
    {
        public int DoctorId { get; set; }
        public string Name { get; set; }
        public string Specialties { get; set; }
        public string ThumbImageUrl { get; set; }
        public string Education { get; set; }
        public string PracticeName { get; set; }
        

    }

    public class GetDentalRecord
    {
        public int RecordId { get; set; }
        public string RecordType { get; set; }
        public string RecordUrl { get; set; }
        public string ThumbImageUrl { get; set; }
        public string Date { get; set; }
        public string Notes { get; set; }
        public string RecordName { get; set; }
        public bool CanDelete { get; set; }
        
    }

    public class Specialities
    
    {
        public int SpecialityId { get; set; }
        public string SpecialityDescription { get; set; }
       
    }

    //View Dentist for Public profile details
    public class BasicDetailsOfDoctor
    {
        public int DoctorId { get; set; }
        public string DoctorFullName { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public string Education { get; set; }
        public string Specialties { get; set; }
        public string PracticeName { get; set; }
        
        public List<SpeacilitiesOfDoctor> SpecialitesDetails = new List<SpeacilitiesOfDoctor>();
        public List<WebsitesOfDoctor> PracticeWebsites = new List<WebsitesOfDoctor>();
        public List<SocialMediaOfDoctor> SocialMediaWebsites = new List<SocialMediaOfDoctor>();
        public List<AddressOfDoctor> Addressess = new List<AddressOfDoctor>();
        public List<EductionDetailsOfDoctor> EducationDetails = new List<EductionDetailsOfDoctor>();
        public List<ProfessionalMembershipsOfDoctor> ProfessionalMemberships = new List<ProfessionalMembershipsOfDoctor>();
        public List<TeamMembersOfDoctor> TeamMembers = new List<TeamMembersOfDoctor>();
      
    }
    public class SpeacilitiesOfDoctor
    {
        public int SpecialtyId { set; get; }
        public string SpecialtyDescription { set; get; }
    }
    public class GetDescriptionofDentalRecord
    {
        public string CreationDate { set; get; }
        public string LastModifiedDate { set; get; }
        public string Description { set; get; }
    }
   
    public class WebsitesOfDoctor
    {
        public int PracticeWebsiteId { set; get; }
        public string PracticeWebsiteUrl { set; get; }
    }


    public class SocialMediaOfDoctor
    {
        public string  SocialMediaWebsiteId { set; get; }
        public string SocialMediaWebsiteUrl { set; get; }
    }

    public class Recipients
    {
        public int RecipientId { set; get; }
    }
    public class MessageAttachments
    {
        public int AttachmentId { set; get; }
        public string AttachmentType { set; get; }
        public int RecordStatus { get; set; } //-- 1 for gallery AND 0 for Datal Record
    }


    public class AddressOfDoctor
    {
        public int AddressId { set; get; }
        public string StreetAddress { set; get; }
        public string City { set; get; }
        public string State { set; get; }
        public string Country { set; get; }
        public string Zipcode { set; get; }
        public string Phone { set; get; }
        public string AddressType { get; set; }
        
    }
    public class EductionDetailsOfDoctor
    {
        public int EducationId { set; get; }
        public string Institute { get; set; }
        public string Course { get; set; }
        public string Degree { get; set; }
        public string EducationCity { set; get; }
        public string EducationState { set; get; }

        public string StartYear { get; set; }
        public string EndYear { get; set; }

    }
    public class ProfessionalMembershipsOfDoctor
    {
        public int MembershipId { set; get; }
        public string Membership { get; set; }

    }
    public class TeamMembersOfDoctor
    {
        public int DoctorId { set; get; }
        public string Name { get; set; }

    }

    public class ViewDataUploadFilesResult
    {
        public string Thumbnail_url { get; set; }
        public string Name { get; set; }
        public int Length { get; set; }
        public string Type { get; set; }
    }
    public enum UserStatus
    {
        [Description("All")]
        All = 0,
        [Description("Inactive")]
        Inactive = 1,
        [Description("Active")]
        Active = 2,
        [Description("Deactivated")]
        Deactivated = 3
    }

    public class DentalHistoryAPI
    {
        public int HistoryId { get; set; }
        public int ItemNumber { get; set; }
        public string ItemType { get; set; }
        public bool MultiPart{get;set;}
        public string Question { get; set; }
        public string Answer { get; set; }
        public string SingleLine { get; set; }
        public List<MultiPartArray> MultiPartArray = new List<MultiPartArray>();
       
    }


    public class DentalHistoryAPIUpdate
    {
        public int HistoryId { get; set; }
        public int ItemNumber { get; set; }
        public string ItemType { get; set; }
        public bool MultiPart { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string SingleLine { get; set; }
        public List<MultiPartArray> MultiPartArray { get; set; }

    }


    public class MultiPartArray
    {
        public int HistoryId { get; set; }
        public int ItemNumber { get; set; }
        public string ItemType { get; set; }
        public bool MultiPart { get; set; }
        public string Question { get; set; }
        public string Answer { get; set; }
        public string SingleLine { get; set; }
       
    }

   

    

    public class InsuranceCoverage
{


public string ResponsiblePartysFirstName{ get; set; }
public string ResponsiblePartysLastName{ get; set; }
public string Relationship{ get; set; }
public string Address{ get; set; }
public string City{ get; set; }
public string State{ get; set; }
public string Zipcode { get; set; }
public string PhoneNumber{ get; set; }
public string PrimaryInsuranceCompany{ get; set; }
public string PrimaryNameOfInsured{ get; set; }
public string PrimaryDateOfBirth{ get; set; }
public string PrimaryMemberID{ get; set; }
public string PrimaryGroupNumber{ get; set; }
public string SecondaryInsuranceCompany{ get; set; }
public string SecondaryNameOfInsured{ get; set; }
public string SecondaryDateOfBirth{ get; set; }
public string SecondaryMemberID{ get; set; }
public string SecondaryGroupNumber { get; set; }


}
   
}