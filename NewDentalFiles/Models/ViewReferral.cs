﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DentalFiles.Models
{
    public class ViewReferral
    {
        public string ToothType { get; set; }
        public string ToothPosition { get; set; }
        public string RegardOption { get; set; }
        public string RequestingOption { get; set; }
        public string Comments { get; set; }
        public string OtherComments { get; set; }
        public string RequestComments { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string HomePhone { get; set; }
        public string Address { get; set; }
        public string ProfileImage { get; set; }
        public string CreationDate {get;set;}
        public string ToField{ get;set;}
        public string FromField{ get;set;}
        public int PatientId{ get;set;}
        public int ReferralCardId{ get;set;}

        
    }
    

}