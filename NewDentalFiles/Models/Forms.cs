﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DentalFiles.Models
{
    public class Forms
    {
        static int nextID = 17;
         public Forms()
        {
            
            FormId = nextID++;
        
        }

        public int FormId { get; set; }
        public string FormName { get; set; }
        public string FormCode { get; set; }
    }
    public class FormsForAPI
    {
        public int FormId { get; set; }
        public string FormName { get; set; }
        public string FormType { get; set; }
    }
}