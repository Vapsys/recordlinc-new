﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DentalFiles.Models
{
    public class MedicalUpdate
    {
          static int nextID = 17;
          public MedicalUpdate()
        {
            
            MedicalUpdateId = nextID++;
        
        }
          public int MedicalUpdateId { get ;set; }
        public string PatientId { get; set; }
        public string CommentMedicalUpdate { get; set; }
        public string SignatureMedicalUpdate { get; set; }
    }
}