﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DentalFiles.Models
{
    public class PatientUploadedImages
    {
        static int nextID = 17;
        public PatientUploadedImages()
        {
            
            ImageId = nextID++;
        
        }
        public int ImageId { get; set; }
        public int PatientId { get; set; }
        public string Name { get; set; }
        public string RelativePath { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public int MontageId { get; set; }
        public string uploadby { get; set; }
        public string Notes { get; set; }
    }
}