﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace DentalFiles.Models
{
    public class Index
    {
    }
    public class RequiredFiedModel
    {
        [RegularExpression(@"^[a-zA-Z0-9\s]+$", ErrorMessage = "Invalid city OR zip code ")]
        public string CityOrZip { get; set; }

        [RegularExpression(@"^[a-zA-Z\s]+$", ErrorMessage = "The last name should be non-numeric.")]
        public string Lastname { get; set; }
        public SelectList Speciality { get; set; }
        public SelectList Distance { get; set; }
                        
        [RegularExpression(@"^[a-zA-Z\s]+$", ErrorMessage = "The first name should be non-numeric.")]
        public string Firstname { get; set; }

        [RegularExpression(@"^[\(\d3\)]+\s[\+0-9\-]+|^[\+0-9\-]+$", ErrorMessage = "The phone number should be numeric.")]
        public string PhoneNo { get; set; }
        
        [Required(ErrorMessage = "Email address is required.")]
        [EmailAddress(ErrorMessage = "Invalid email address entered.")]
        public string Email { get; set; }
    }

    public class ReqLogin
    {
        [Required(ErrorMessage = "Enter valid email or patient id")]
        public string Email { get; set; }

        [Required(ErrorMessage = " Please Enter Password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        

    }


    public class RequiredFieldForReqInfo
    {
        
        [Required(ErrorMessage = "Please Enter First Name")]
        public string Firstname { get; set; }

        [Required(ErrorMessage = "Please Enter Last Name")]
        public string Lastname { get; set; }


        [Required(ErrorMessage = "Please Enter Phone Number")]
        public string PhoneNo { get; set; }

        public string Email { get; set; }


        public string Comment { get; set; }
    }


    public class Forgotpassword
    {

        [Required(ErrorMessage = "Please Enter EmailId")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$", ErrorMessage = "Please Enter Valid Email")]
        public string Email { get; set; }

    }

}