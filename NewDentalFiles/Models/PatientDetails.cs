﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using JQueryDataTables.Models.Repository;
using DentalFiles.App_Start;
namespace DentalFiles.Models
{

    public class PatientDetails
    {
        DataRepository objDataRepository = new DataRepository();
        static int nextID = 17;
        public PatientDetails()
        {
            PatientId = nextID++;
            Parameters = new Dictionary<string, object>();
        }
        public int PatientId { set; get; }
        public string AssignedPatientId { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string MiddelName { set; get; }
        public string Country { set; get; }
        public string DateOfBirth { set; get; }
        public int Gender { set; get; }
        public string ProfileImage { set; get; }
        public string Phone { set; get; }
        public string Email { set; get; }
        public string Address { set; get; }
        public string City { set; get; }
        public string State { set; get; }
        public string Zip { set; get; }
        public string Comments { set; get; }
        public string StartDate { set; get; }
        public string Address2 { set; get; }
        public string GuarFirstName { set; get; }
        public string GuarLastName { set; get; }
        public string GuarPhone { set; get; }
        public string GuarEmail { set; get; }
        public string FacebookUrl { set; get; }
        public string TwitterUrl { set; get; }
        public string LinkedinUrl { set; get; }
        public string GoogleUrl { set; get; }
        public string BlogUrl { set; get; }
        public int OwnerId { set; get; }
        public string StayloggedMins { get; set; }
        public string SecondaryEmail { get; set; }
        public int Limit { get; set; }
        public int FromRowNumber { get; set; }
        public string ContainerId { get; set; }
        public string AjaxActionUrl { get; set; }
        public IDictionary<string, object> Parameters { get; set; }



        public PatientDetails(int limit, int fromRowNumber, string containerId,
            string ajaxActionUrl, IDictionary<string, object> parameters = null)
        {
            Limit = limit;
            FromRowNumber = fromRowNumber;
            ContainerId = containerId;
            AjaxActionUrl = ajaxActionUrl;
            if (parameters != null)
                Parameters = parameters;

        }
        public List<PatientUploadedImages> PatientUploadedImages
        {
            get { return objDataRepository.PatientUploadedImages(PatientId, SessionManagement.TimeZoneSystemName); }
        }


    }

    public class PateintDetailsForAPI
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string  Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string Address { get; set; }
        //public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PrimaryPhone { get; set; }
        public string SecondaryPhone { get; set; }
        public string EmailAddress { get; set; }
       // public string MethodOfPayment { get; set; }
        //public string ReferralName { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyContactPhone { get; set; }
        public string Zipcode { get; set; }

    }

    public class PateintDetailsByParticularDoctor
    {
        //patient detials by particular doctor
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        

    }

}