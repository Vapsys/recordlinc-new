﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DentalFiles.Models
{
    public class FormStatus
    {
         static int nextID = 17;
         public FormStatus()
        {
            
            FormId = nextID++;
        
        }

        public int FormId { get; set; }
        public string FormName { get; set; }
        public string FormCode { get; set; }
        public string FormStatusFlag { get;set;}
    }
}