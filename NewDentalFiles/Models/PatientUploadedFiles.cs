﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DentalFiles.Models
{
    public class PatientUploadedFiles
    {
         static int nextID = 17;
         public PatientUploadedFiles()
        {
            
            MontageId = nextID++;
        
        }
        public string Name { get; set; }
        public int PatientId { get; set; }
        public string RelativePath { get; set; }
        public int UserId { get; set; } 
        public int MontageId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string DocumentName { get; set; }
        public string Notes { get; set; }
        public string uploadby { get; set; }
    }
}