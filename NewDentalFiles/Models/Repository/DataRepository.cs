﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JQueryDataTables.Models;
using DentalFiles.Models;
using System.Data;
using System.Text.RegularExpressions;
using DataAccessLayer.Common;
using BusinessLogicLayer;
using System.Configuration;

namespace JQueryDataTables.Models.Repository
{
    /// <summary>
    /// Repository class - contains hardcoded data
    /// </summary>
    public class DataRepository
    {
        string DoctorImage = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("DoctorImage"));
        string DefaultDoctorImage = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("DefaultDoctorImage"));
        CommonDAL objCommonDAL = new CommonDAL();
        DataTable dt = new DataTable();
        static List<UserList> UserListData;
        static List<InviteTreatingDoctor> InviteTreatingDoctorListData;
        static List<PatientDetails> PatientListData;
        static List<PatientUploadedImages> PatientListUploadedImages;
        static List<PatientUploadedFiles> PatientListUploadedFiles;
        static List<Forms> ListForms;
        static List<FormStatus> ListFormsStatus;
        static List<ReferralDetails> ListReferralDetails;
        static List<ViewReferral> ListViewReferral;
        static List<Messages> ListMessages;
        static List<Sent> ListSentMsg;
        static List<BasicInfo> PatientListBasicInfo;
        static List<MedicalUpdate> PatientListMedicalUpdate;
        static List<RegistrationForm> PatientListRegistrationForm;
        static List<ChildDentalMEDICALHISTORY> PatientListChildDentalMEDICALHISTORY;
        static List<DentalHistory> PatientListDentalHistoryForm;
        static List<MedicalHistory> PatientListMedicalHistoryForm;
        static List<MedicalHISTORYUpdate> PatientListMedicalHISTORYUpdateForm;
        static List<Review> PatientListReviewForm;
        static List<Notes> ListNote;

        public List<PatientDetails> GetPatientDetails(int PatientId, string TimeZoneSystemName)
        {
            PatientListData = new List<PatientDetails>();
            dt = objCommonDAL.GetPatientsDetails(PatientId);
            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    string strDateOfBirth = string.Empty;

                    DateTime? dt_DateOfBirth = null;
                    if (!string.IsNullOrEmpty(Convert.ToString(row["DateOfBirth"])))
                    {
                        dt_DateOfBirth = Convert.ToDateTime(row["DateOfBirth"]);
                        dt_DateOfBirth = clsHelper.ConvertFromUTC(Convert.ToDateTime(dt_DateOfBirth), TimeZoneSystemName);
                    }
                    if (dt_DateOfBirth.HasValue)
                        strDateOfBirth = new CommonBLL().ConvertToDate(Convert.ToDateTime(dt_DateOfBirth), (int)BO.Enums.Common.DateFormat.GENERAL);
                    PatientListData.Add(new PatientDetails()
                    {
                        PatientId = Convert.ToInt32(row["PatientId"]),
                        AssignedPatientId = Convert.ToString(row["AssignedPatientId"]),
                        FirstName = Convert.ToString(row["FirstName"]),
                        LastName = Convert.ToString(row["LastName"]),
                        MiddelName = Convert.ToString(row["MiddelName"]),
                        Country = Convert.ToString(row["Country"]),
                        DateOfBirth = strDateOfBirth,
                        Gender = Convert.ToInt32(row["Gender"]),
                        ProfileImage = Convert.ToString(row["ProfileImage"]),
                        Phone = Convert.ToString(row["Phone"]),
                        Email = Convert.ToString(row["Email"]),
                        City = Convert.ToString(row["City"]),
                        Address = Convert.ToString(row["Address"]),
                        State = Convert.ToString(row["State"]),
                        Zip = Convert.ToString(row["Zip"]),
                        StartDate = Convert.ToString(row["StartDate"]),
                        Address2 = Convert.ToString(row["Address2"]),
                        GuarFirstName = Convert.ToString(row["GuarFirstName"]),
                        GuarLastName = Convert.ToString(row["GuarLastName"]),
                        GuarPhone = Convert.ToString(row["GuarPhone"]),
                        GuarEmail = Convert.ToString(row["GuarEmail"]),
                        FacebookUrl = objCommonDAL.GetFullExternalUrl(Convert.ToString(row["FacebookUrl"])),
                        TwitterUrl = objCommonDAL.GetFullExternalUrl(Convert.ToString(row["TwitterUrl"])),
                        LinkedinUrl = objCommonDAL.GetFullExternalUrl(Convert.ToString(row["LinkedinUrl"])),
                        GoogleUrl = objCommonDAL.GetFullExternalUrl(Convert.ToString(row["GoogleUrl"])),
                        BlogUrl = objCommonDAL.GetFullExternalUrl(Convert.ToString(row["BlogUrl"])),
                        OwnerId = Convert.ToInt32(row["OwnerId"]),
                        StayloggedMins = (Convert.ToString(row["StayloggedMins"]) == null || Convert.ToString(row["StayloggedMins"]) == "") ? "60" : Convert.ToString(row["StayloggedMins"]),




                    });
                }
            }
            return PatientListData;
        }

        public List<UserList> GetDentistListBySearch(string Keywords, string Address, string LastName, int Speciality, int Miles, string FirstName, string Title, string CompanyName, string School, string Email, string phone, string zipcode, int index, int size, string SpecialtityList, string City)
        {

            UserListData = new List<UserList>();

            dt = objCommonDAL.GetDentistListBySearch(index, size, Keywords, Address, LastName, Speciality, Miles, FirstName, Title, CompanyName, School, Email, phone, zipcode, SpecialtityList, City);

            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Add("newRowcount");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["newRowcount"] = i + 1;
                }
                foreach (DataRow row in dt.Rows)
                {
                    UserList objUserList = new UserList();
                    string ImageName = objCommonDAL.CheckNull(Convert.ToString(row["ImageName"]), string.Empty);
                    if (string.IsNullOrWhiteSpace(ImageName))
                    {
                        ImageName = ConfigurationManager.AppSettings.Get("DoctorProfileImage");
                    }
                    else
                    {
                        ImageName = DoctorImage + ImageName;
                    }

                    UserListData.Add(new UserList()
                    {
                        UserID = Convert.ToInt32(row["UserID"]),
                        FirstName = row["FirstName"].ToString(),
                        LastName = Convert.ToString(row["LastName"]),
                        Description = Convert.ToString(row["Type"]),
                        Email = Convert.ToString(row["Email"]),
                        Username = Convert.ToString(row["Username"]),
                        image = objCommonDAL.CheckImageNull(Convert.ToString(row["image"]), "../DentalImages/Doctor_no_image.gif"),
                        name = Convert.ToString(row["name"]),
                        State = Convert.ToString(row["State"]),
                        City = Convert.ToString(row["City"]),
                        Country = Convert.ToString(row["Country"]),
                        ExactAddress = Convert.ToString(row["ExactAddress"]),
                        Address2 = Convert.ToString(row["Address2"]),
                        Miles = Convert.ToString(row["Miles"]),
                        PublicPath = Convert.ToString(row["PublicPath"]).Replace(" ", "%20"),
                        RowNumber = Convert.ToString(row["newRowcount"]),
                        Gender = Convert.ToString(row["Gender"]),
                        TotalRecord = Convert.ToString(row["TotalRecord"]),
                        ImageName = Convert.ToString(row["ImageName"]),
                        MapAddress = Convert.ToString(row["MapAddress"]),
                    });


                }
            }


            return UserListData;
        }



        public List<UserList> GetDentistListBySearchDistinctName(string Keywords, string Address, string LastName, int Speciality, int Miles, string FirstName, string Title, string CompanyName, string School, string Email, string phone, string zipcode, int index, int size, string SpecialtityList, string City)
        {

            UserListData = new List<UserList>();

            dt = objCommonDAL.GetDentistListBySearch(index, size, Keywords, Address, LastName, Speciality, Miles, FirstName, Title, CompanyName, School, Email, phone, zipcode, SpecialtityList, City);

            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Add("newRowcount");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["newRowcount"] = i + 1;
                }
                DataView dv = dt.DefaultView;



                DataView view = new DataView(dt);
                dt = view.ToTable(true, "name");



                foreach (DataRow row in dt.Rows)
                {
                    UserList objUserList = new UserList();


                    UserListData.Add(new UserList()
                    {

                        name = Convert.ToString(row["name"]),

                    });


                }
            }


            return UserListData;
        }



        public List<InviteTreatingDoctor> GetInviteTreatingDoctorList(string Keywords, string Address, string LastName, int Speciality, int Miles, string FirstName, string Title, string CompanyName, string School, string Email, string phone, string zipcode, int index, int size, string SpecialtityList, int PatientId)
        {
            InviteTreatingDoctorListData = new List<InviteTreatingDoctor>();

            dt = objCommonDAL.GetInviteTreatingDoctorList(index, size, Keywords, Address, LastName, Speciality, Miles, FirstName, Title, CompanyName, School, Email, phone, zipcode, SpecialtityList, PatientId);




            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Add("newRowcount");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dt.Rows[i]["newRowcount"] = i + 1;
                }

                foreach (DataRow row in dt.Rows)
                {
                    InviteTreatingDoctorListData.Add(new InviteTreatingDoctor()
                    {
                        UserID = Convert.ToInt32(row["UserID"]),
                        FirstName = row["FirstName"].ToString(),
                        LastName = Convert.ToString(row["LastName"]),
                        Description = Convert.ToString(row["Type"]),
                        Email = Convert.ToString(row["Email"]),
                        Username = Convert.ToString(row["Username"]),
                        image = objCommonDAL.CheckImageNull(Convert.ToString(row["image"]), "../DentalImages/Doctor_no_image.gif"),

                        name = Convert.ToString(row["name"]),
                        State = Convert.ToString(row["State"]),
                        City = Convert.ToString(row["City"]),
                        Country = Convert.ToString(row["Country"]),
                        ExactAddress = Convert.ToString(row["ExactAddress"]),
                        Address2 = Convert.ToString(row["Address2"]),
                        Miles = Convert.ToString(row["Miles"]),
                        PublicPath = Convert.ToString(row["PublicPath"]).Replace(" ", "%20"),
                        RowNumber = Convert.ToString(row["newRowcount"]),
                        TotalRecord = Convert.ToString(row["TotalRecord"]),

                    });


                }
            }


            return InviteTreatingDoctorListData;
        }


        public List<UserList> GetTratingDoctors(int PatientId, int index, int size)
        {

            UserListData = new List<UserList>();

            dt = objCommonDAL.GetTratingdoctors(PatientId, index, size);

            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    UserList objUserList = new UserList();


                    UserListData.Add(new UserList()
                    {
                        UserID = Convert.ToInt32(row["ColleagueId"]),


                        name = Convert.ToString(row["name"]),
                        image = Convert.ToString(row["image"]).Replace(System.Configuration.ConfigurationManager.AppSettings["imagebankpath"], System.Configuration.ConfigurationManager.AppSettings["DentistImagespath"]),
                        State = Convert.ToString(row["State"]),
                        City = Convert.ToString(row["City"]),
                        Country = Convert.ToString(row["Country"]),
                        ExactAddress = Convert.ToString(row["ExactAddress"]),
                        Address2 = Convert.ToString(row["Address2"]),
                        Specialities = Convert.ToString(row["specialities"]),
                        Phone = Convert.ToString(row["phone"]),
                        PublicPath = Convert.ToString(row["PublicPath"]).Replace(" ", "%20"),
                        Zipcode = Convert.ToString(row["Zipcode"]),
                        AccountName = Convert.ToString(row["AccountName"]),
                    });


                }
            }


            return UserListData;
        }

        public List<PatientUploadedImages> PatientUploadedImages(int PatientId, string TimeZoneSystemName)
        {



            PatientListUploadedImages = new List<PatientUploadedImages>();

            dt = objCommonDAL.GetPatientMontages(PatientId);

            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    PatientUploadedImages objUserList = new PatientUploadedImages();
                    DateTime CreationDate = Convert.ToDateTime(row["CreationDate"]);
                    CreationDate = clsHelper.ConvertFromUTC(CreationDate, TimeZoneSystemName);
                    DateTime LastModifiedDate = Convert.ToDateTime(row["LastModifiedDate"]);
                    LastModifiedDate = clsHelper.ConvertFromUTC(LastModifiedDate, TimeZoneSystemName);

                    PatientListUploadedImages.Add(new PatientUploadedImages()
                    {
                        ImageId = Convert.ToInt32(row["ImageId"]),
                        PatientId = Convert.ToInt32(row["PatientId"]),
                        Name = Convert.ToString(row["Name"]).Substring(Convert.ToString(row["Name"]).ToString().LastIndexOf("$") + 1),
                        RelativePath = Convert.ToString(row["RelativePath"]),
                        CreationDate = CreationDate,
                        LastModifiedDate = LastModifiedDate,
                        MontageId = Convert.ToInt32(row["MontageId"]),
                        uploadby = Convert.ToString(row["uploadby"]),
                        Notes = Convert.ToString(row["Description"]),
                    });


                }
            }


            return PatientListUploadedImages;
        }

        public List<PatientUploadedImages> PatientUploadedImagesTemp(int PatientId, string viewstatvalue)
        {



            PatientListUploadedImages = new List<PatientUploadedImages>();

            dt = objCommonDAL.getuploadimages(PatientId, viewstatvalue);

            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    PatientUploadedImages objUserList = new PatientUploadedImages();


                    PatientListUploadedImages.Add(new PatientUploadedImages()
                    {
                        ImageId = Convert.ToInt32(row["UploadImageId"]),
                        PatientId = Convert.ToInt32(row["patientid"]),
                        RelativePath = System.Configuration.ConfigurationManager.AppSettings["TempUploadsFolderPath"] + Convert.ToString(row["Name"]),
                        Name = Convert.ToString(row["Name"]).Replace("../ImageBank/", "").Replace("$", "_Dollar_").Replace(".", "_Dot_"),
                        Notes = objCommonDAL.Getpurename(Convert.ToString(row["Name"]).Replace("../TempUploads/", ""), ""),
                    });


                }
            }


            return PatientListUploadedImages;
        }


        public List<PatientUploadedFiles> PatientUploadedFiles(int PatientId, string TimeZoneSystemName)
        {



            PatientListUploadedFiles = new List<PatientUploadedFiles>();

            dt = objCommonDAL.GetPatientDocs(PatientId);

            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    PatientUploadedFiles objUserList = new PatientUploadedFiles();
                    DateTime CreationDate = Convert.ToDateTime(row["CreationDate"]);
                    CreationDate = clsHelper.ConvertFromUTC(CreationDate, TimeZoneSystemName);
                    DateTime LastModifiedDate = Convert.ToDateTime(row["LastModifiedDate"]);
                    LastModifiedDate = clsHelper.ConvertFromUTC(LastModifiedDate, TimeZoneSystemName);

                    PatientListUploadedFiles.Add(new PatientUploadedFiles()
                    {
                        MontageId = Convert.ToInt32(row["DocumentId"]),
                        Name = objCommonDAL.Getpurename(Convert.ToString(row["DocumentName"]).Replace("../TempUploads/", ""), ""),
                        PatientId = Convert.ToInt32(row["PatientId"]),
                        CreationDate = CreationDate,
                        LastModifiedDate = LastModifiedDate,
                        DocumentName = Convert.ToString(row["DocumentName"].ToString().Replace("../ImageBank/", "").Replace("$", "_Dollar_")).Replace(".", "_Dot_").Replace("/imagebank/", ""),
                        RelativePath = Convert.ToString(row["DocumentName"].ToString().Replace("../ImageBank/", "")),
                        Notes = Convert.ToString(row["Description"]),
                        uploadby = Convert.ToString(row["uploadby"]),
                    });


                }
            }


            return PatientListUploadedFiles;
        }




        public List<PatientUploadedFiles> PatientUploadedFilesTemp(int PatientId, string viewstatvalue)
        {



            PatientListUploadedFiles = new List<PatientUploadedFiles>();

            dt = objCommonDAL.getuploadfiles(PatientId, viewstatvalue);

            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    PatientUploadedFiles objUserList = new PatientUploadedFiles();


                    PatientListUploadedFiles.Add(new PatientUploadedFiles()
                    {
                        MontageId = Convert.ToInt32(row["UploadedFileId"]),
                        Name = objCommonDAL.Getpurename(Convert.ToString(row["DocumentName"]).Replace("../TempUploads/", ""), ""),
                        RelativePath = Convert.ToString(row["DocumentName"]).Replace("../DentistImages/", "").Replace("$", "_Dollar_").Replace(".", "_Dot_"),
                        DocumentName = Convert.ToString(row["DocumentName"]).Replace("../DentistImages/", ""),
                    });


                }
            }


            return PatientListUploadedFiles;
        }








        public List<Forms> PatientForms(string Operation)
        {
            ListForms = new List<Forms>();
            dt = objCommonDAL.PatientForms(Operation);

            if (dt != null && dt.Rows.Count > 0)
            {
                DataView dv = new DataView();
                dv.Table = dt;
                dv.RowFilter = "FormName <> 'Insurance Coverage'";
                if (dv != null && dv.Count > 0)
                {
                    foreach (DataRow row in dv.Table.Rows)
                    {
                        Forms objFormList = new Forms();
                        ListForms.Add(new Forms()
                        {
                            FormId = Convert.ToInt32(row["FormId"]),
                            FormName = Convert.ToString(row["FormName"]),
                            FormCode = Convert.ToString(row["FormCode"]),
                        });
                    }
                }
            }


            return ListForms;
        }

        public List<FormStatus> PatientFormsStatus(int PatientId, string Operation)
        {
            ListFormsStatus = new List<FormStatus>();
            dt = objCommonDAL.PatientFormsSent(PatientId, Operation);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    FormStatus objFormListStatus = new FormStatus();
                    ListFormsStatus.Add(new FormStatus()
                    {
                        FormId = Convert.ToInt32(row["FormId"]),
                        FormName = Convert.ToString(row["FormName"]),
                        FormCode = Convert.ToString(row["FormCode"]),
                        FormStatusFlag = Convert.ToString(row["Status"]),
                    });
                }
            }
            return ListFormsStatus;
        }

        public List<ReferralDetails> GetAllPatientReferrals(int PatientId, string TimeZoneSystemName)
        {
            ListReferralDetails = new List<ReferralDetails>();
            dt = objCommonDAL.GetAllPatientReferrals(PatientId);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    string strCreationDate = new CommonBLL().ConvertToDateTime(clsHelper.ConvertFromUTC(Convert.ToDateTime(row["CreationDate"]), TimeZoneSystemName), (int)BO.Enums.Common.DateFormat.GENERAL);
                    ListReferralDetails.Add(new ReferralDetails()
                    {
                        CreationDate = strCreationDate,
                        FromField = Convert.ToString(row["FromField"]),
                        MessageTypeId = Convert.ToInt32(row["MessageTypeId"]),
                        OwnerId = Convert.ToInt32(row["OwnerId"]),
                        PatientId = Convert.ToInt32(row["PatientId"]),
                        PatientName = Convert.ToString(row["PatientName"]),
                        ReferralCardId = Convert.ToInt32(row["ReferralCardId"]),
                        Status = Convert.ToString(row["Status"]),
                        ToField = Convert.ToString(row["ToField"]),
                        FromPublicPath = Convert.ToString(row["FromPublicPath"]),
                        ToPublicPath = Convert.ToString(row["ToPublicPath"]),


                    });
                }
            }
            return ListReferralDetails;
        }



        public List<ViewReferral> getReferCardDetailsByIdSingle(int ReferralId)
        {
            ListViewReferral = new List<ViewReferral>();
            dt = objCommonDAL.getReferCardDetailsById(ReferralId);




            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    ViewReferral objReferralDetails = new ViewReferral();
                    ListViewReferral.Add(new ViewReferral()
                    {

                        ToothType = Convert.ToString(row["ToothType"]),
                        ToothPosition = Convert.ToString(row["ToothPosition"]),
                        RegardOption = Convert.ToString(row["RegardOption"]),
                        RequestingOption = Convert.ToString(row["RequestingOption"]),
                        Comments = Convert.ToString(row["Comments"]),
                        OtherComments = Convert.ToString(row["OtherComments"]),
                        RequestComments = Convert.ToString(row["RequestComments"]),
                        CreationDate = Convert.ToString(row["CreationDate"]),
                        ToField = Convert.ToString(row["ToField"]),
                        FromField = Convert.ToString(row["FromField"]),
                        PatientId = Convert.ToInt32(row["PatientId"]),
                        FirstName = Convert.ToString(row["FirstName"]),
                        LastName = Convert.ToString(row["LastName"]),
                        Email = Convert.ToString(row["Email"]),
                        HomePhone = Convert.ToString(row["HomePhone"]),
                        Address = Convert.ToString(row["Address"]),
                        ProfileImage = Convert.ToString(row["ProfileImage"]),

                    });
                    break;
                }
            }
            else
            {
                ViewReferral objVR = new ViewReferral();

                objVR.ToothType = "";
                objVR.ToothPosition = "";
                objVR.RegardOption = "";
                objVR.RequestingOption = "";
                objVR.Comments = "";
                objVR.OtherComments = "";
                objVR.RequestComments = "";











                ListViewReferral.Add(objVR);
            }


            return ListViewReferral;
        }

        public List<ViewReferral> getReferCardDetailsById(int ReferralId)
        {
            ListViewReferral = new List<ViewReferral>();
            dt = objCommonDAL.getReferCardDetailsById(ReferralId);




            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    ViewReferral objReferralDetails = new ViewReferral();
                    ListViewReferral.Add(new ViewReferral()
                    {

                        ToothType = Convert.ToString(row["ToothType"]),
                        ToothPosition = Convert.ToString(row["ToothPosition"]),
                        RegardOption = Convert.ToString(row["RegardOption"]),
                        RequestingOption = Convert.ToString(row["RequestingOption"]),
                        Comments = Convert.ToString(row["Comments"]),
                        OtherComments = Convert.ToString(row["OtherComments"]),
                        RequestComments = Convert.ToString(row["RequestComments"]),
                        CreationDate = Convert.ToString(row["CreationDate"]),
                        ToField = Convert.ToString(row["ToField"]),
                        FromField = Convert.ToString(row["FromField"]),
                        PatientId = Convert.ToInt32(row["PatientId"]),
                        FirstName = Convert.ToString(row["FirstName"]),
                        LastName = Convert.ToString(row["LastName"]),
                        Email = Convert.ToString(row["Email"]),
                        HomePhone = Convert.ToString(row["HomePhone"]),
                        Address = Convert.ToString(row["Address"]),
                        ProfileImage = Convert.ToString(row["ProfileImage"]),

                    });

                }
            }

            else
            {
                ViewReferral objVR = new ViewReferral();
                ListViewReferral.Add(objVR);
            }
            return ListViewReferral;
        }



        public List<Messages> GetAllMessages(int PatientId, string TimeZoneSystemName)
        {
            ListMessages = new List<Messages>();
            dt = objCommonDAL.GetAllMessages(PatientId, 0, 1, 100000);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Messages objMessages = new Messages();
                    ListMessages.Add(new Messages()
                    {

                        PatientmessageId = Convert.ToInt32(row["PatientmessageId"]),
                        FromField = Convert.ToString(row["FromField"]),
                        ToField = Convert.ToString(row["ToField"]),
                        Subject = Convert.ToString(row["Subject"]),
                        Body = HttpUtility.UrlDecode(Convert.ToString(row["Body"])),
                        DoctorId = Convert.ToInt32(row["DoctorId"]),
                        PatientId = Convert.ToInt32(row["PatientId"]),
                        CreationDate = clsHelper.ConvertFromUTC(Convert.ToDateTime(row["CreationDate"]), TimeZoneSystemName),
                        TotalRecord = Convert.ToInt32(row["TotalRecord"]),
                        PublicPath = Convert.ToString(row["PublicPath"]),
                        MessageFirstLine = Regex.Replace(Regex.Replace(objCommonDAL.CheckNull(objCommonDAL.RemoveHTML(HttpUtility.UrlDecode(Convert.ToString(row["Body"]))), string.Empty), @"<[^>]+>|&nbsp;", string.Empty), @"[\r\n\t]+", string.Empty),
                        Read_flag = Convert.ToString(row["Read_flag"]),



                    });
                }
            }
            return ListMessages;
        }



        public List<Sent> AllPatientSent(int PatientId, string TimeZoneSystemName)
        {
            ListSentMsg = new List<Sent>();
            dt = objCommonDAL.PatientSentMsg(PatientId, 1, 100000);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Sent objMessages = new Sent();
                    ListSentMsg.Add(new Sent()
                    {

                        PatientmessageId = Convert.ToInt32(row["PatientmessageId"]),
                        FromField = Convert.ToString(row["FromField"]),
                        ToField = Convert.ToString(row["ToField"]),
                        Subject = Convert.ToString(row["Subject"]),
                        Body = HttpUtility.UrlDecode(Convert.ToString(row["Body"])),
                        DoctorId = Convert.ToInt32(row["DoctorId"]),
                        PatientId = Convert.ToInt32(row["PatientId"]),
                        CreationDate = clsHelper.ConvertFromUTC(Convert.ToDateTime(row["CreationDate"]), TimeZoneSystemName),
                        TotalRecord = Convert.ToInt32(row["TotalRecord"]),
                        PublicPath = Convert.ToString(row["PublicPath"]),
                        MessageBodyFirstLine = Regex.Replace(Regex.Replace(objCommonDAL.CheckNull(objCommonDAL.RemoveHTML(HttpUtility.UrlDecode(Convert.ToString(row["Body"]))), string.Empty), @"<[^>]+>|&nbsp;", string.Empty), @"[\r\n\t]+", string.Empty),


                    });
                }
            }
            return ListSentMsg;
        }


        public List<Messages> GetMessagesById(int PatientmessageId)
        {
            Messages objM = new Messages();
            ListMessages = new List<Messages>();
            dt = objCommonDAL.GetAllMessages(0, Convert.ToInt32(PatientmessageId), 1, 100000);

            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Messages objMessages = new Messages();
                    string body = string.Empty;
                    ListMessages.Add(new Messages()
                    {

                        PatientmessageId = Convert.ToInt32(row["PatientmessageId"]),
                        FromField = Convert.ToString(row["FromField"]),
                        ToField = Convert.ToString(row["ToField"]),
                        Subject = Convert.ToString(row["Subject"]),
                        Body = Regex.Replace(Regex.Replace(objCommonDAL.CheckNull(objCommonDAL.RemoveHTML(Uri.UnescapeDataString(Convert.ToString(row["Body"]))), string.Empty), @"<[^>]+>|&nbsp;", string.Empty), @"[\r\n\t]+", string.Empty),
                        DoctorId = Convert.ToInt32(row["DoctorId"]),
                        PatientId = Convert.ToInt32(row["PatientId"]),
                        CreationDate = Convert.ToDateTime(row["CreationDate"]),




                    });
                }
            }
            else
            {
                ListMessages.Add(objM);
            }
            return ListMessages;
        }


        public List<BasicInfo> GetPatientBasicInfo(int PatientId, string TimeZoneSystemName)
        {
            PatientListBasicInfo = new List<BasicInfo>();
            dt = objCommonDAL.GetPatientsDetails(PatientId);
            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    //string strDateOfBirth = !string.IsNullOrEmpty(Convert.ToString(row["DateOfBirth"])) ? new CommonBLL().ConvertToDate(clsHelper.ConvertFromUTC(Convert.ToDateTime(row["DateOfBirth"]), TimeZoneSystemName), (int)BO.Enums.Common.DateFormat.GENERAL) : null;
                    //SUP-119  Changes for UTC
                    string strDateOfBirth = (!string.IsNullOrEmpty(Convert.ToString(row["DateOfBirth"]))) ? new CommonBLL().ConvertToDate(Convert.ToDateTime(row["DateOfBirth"]), (int)BO.Enums.Common.DateFormat.GENERAL) : null;

                    string strStartDate = !string.IsNullOrEmpty(Convert.ToString(row["StartDate"])) ? new CommonBLL().ConvertToDateTime(clsHelper.ConvertFromUTC(Convert.ToDateTime(row["StartDate"]), TimeZoneSystemName), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL)) : "";

                    PatientListBasicInfo.Add(new BasicInfo()
                    {
                        PatientId = Convert.ToInt32(row["PatientId"]),
                        // priya : issue no : 5206
                        //Password = Convert.ToString(row["Password"]),
                        //ConfirmPassword = Convert.ToString(row["Password"]),
                        AssignedPatientId = Convert.ToString(row["AssignedPatientId"]),
                        FirstName = Convert.ToString(row["FirstName"]),
                        LastName = Convert.ToString(row["LastName"]),
                        MiddelName = Convert.ToString(row["MiddelName"]),
                        Country = Convert.ToString(row["Country"]),
                        // DateOfBirth = objCommonDAL.CheckNull(Convert.ToString(row["DateOfBirth"]), null),
                       // DateOfBirth = objCommonDAL.CheckNull(Convert.ToString(row["DateOfBirth"]), null) == null ? null : Convert.ToDateTime(row["DateOfBirth"]).ToString("MM/dd/yyyy"),
                       DateOfBirth = strDateOfBirth,
                        Gender = Convert.ToInt32(row["Gender"]),
                        ProfileImage = Convert.ToString(row["ProfileImage"]),
                        Phone = Convert.ToString(row["Phone"]),
                        Email = Convert.ToString(row["Email"]),
                        Address = Convert.ToString(row["Address"]),
                        City = Convert.ToString(row["City"]),
                        State = Convert.ToString(row["State"]),
                        Zip = Convert.ToString(row["Zip"]),
                        StartDate = strStartDate,
                        Address2 = Convert.ToString(row["Address2"]),
                        GuarFirstName = Convert.ToString(row["GuarFirstName"]),
                        GuarLastName = Convert.ToString(row["GuarLastName"]),
                        GuarPhone = Convert.ToString(row["GuarPhone"]),
                        GuarEmail = Convert.ToString(row["GuarEmail"]),
                        FacebookUrl = Convert.ToString(row["FacebookUrl"]),
                        TwitterUrl = Convert.ToString(row["TwitterUrl"]),
                        LinkedinUrl = Convert.ToString(row["LinkedinUrl"]),
                        OwnerId = Convert.ToInt32(row["OwnerId"]),
                        Status = Convert.ToString(row["Status"]),
                        StayloggedMins = (Convert.ToString(row["StayloggedMins"]) == null || Convert.ToString(row["StayloggedMins"]) == "") ? "60" : Convert.ToString(row["StayloggedMins"]),
                        SecondaryEmail = Convert.ToString(row["SecondaryEmail"]),
                        txtWorkPhone = Convert.ToString(row["SecondaryPhone"]),

                    });
                }
            }
            return PatientListBasicInfo;
        }

        public List<MedicalUpdate> GetPatientMedicalUpdate(int PatientId)
        {
            PatientListMedicalUpdate = new List<MedicalUpdate>();
            dt = objCommonDAL.GetMedicalUpdate(PatientId, "GetMedicalUpdate");
            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    PatientListMedicalUpdate.Add(new MedicalUpdate()
                    {
                        CommentMedicalUpdate = Convert.ToString(row["txt1b"]),
                        SignatureMedicalUpdate = Convert.ToString(row["txtDigiSign"]),
                    });
                }
            }
            return PatientListMedicalUpdate;
        }

        public List<RegistrationForm> GetRegistration(int PatientId)
        {
            PatientListRegistrationForm = new List<RegistrationForm>();
            RegistrationForm objRf = new RegistrationForm();

            dt = objCommonDAL.GetRegistration(PatientId, "GetRegistrationform");
            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    objRf.txtResponsiblepartyFname = Convert.ToString(row["txtResponsiblepartyFname"]);
                    objRf.txtResponsiblepartyLname = Convert.ToString(row["txtResponsiblepartyLname"]);
                    objRf.txtResponsibleRelationship = Convert.ToString(row["txtResponsibleRelationship"]);
                    objRf.txtResponsibleAddress = Convert.ToString(row["txtResponsibleAddress"]);
                    objRf.txtResponsibleCity = Convert.ToString(row["txtResponsibleCity"]);
                    objRf.txtResponsibleState = Convert.ToString(row["txtResponsibleState"]);
                    objRf.txtResponsibleZipCode = Convert.ToString(row["txtResponsibleZipCode"]);
                    objRf.txtResponsibleDOB = objCommonDAL.CheckNull(Convert.ToString(row["txtResponsibleDOB"]), null) == null ? null : new CommonBLL().ConvertToDate(Convert.ToDateTime(row["txtResponsibleDOB"]), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL));
                    objRf.txtResponsibleContact = Convert.ToString(row["txtResponsibleContact"]);



                    objRf.chkMethodOfPayment = Convert.ToString(row["chkMethodOfPayment"]);
                    objRf.txtWhommay = Convert.ToString(row["txtWhommay"]);
                    objRf.txtSomeonetonotify = Convert.ToString(row["txtSomeonetonotify"]);
                    objRf.txtemergencyname = Convert.ToString(row["txtemergencyname"]);
                    objRf.txtemergency = Convert.ToString(row["txtemergency"]);
                    objRf.txtEmployeeName1 = Convert.ToString(row["txtEmployeeName1"]);
                    objRf.Insurance_Phone1 = Convert.ToString(row["txtInsurancePhone1"]);
                    objRf.txtEmployeeDob1 = objCommonDAL.CheckNull(Convert.ToString(row["txtEmployeeDob1"]), null) == null ? null : new CommonBLL().ConvertToDate(Convert.ToDateTime(row["txtEmployeeDob1"]), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL)); 
                    objRf.txtEmployerName1 = Convert.ToString(row["txtEmployerName1"]);
                    objRf.txtYearsEmployed1 = Convert.ToString(row["txtYearsEmployed1"]);
                    objRf.txtNameofInsurance1 = Convert.ToString(row["txtNameofInsurance1"]);
                    objRf.txtInsuranceAddress1 = Convert.ToString(row["txtInsuranceAddress1"]);
                    objRf.txtInsuranceTelephone1 = Convert.ToString(row["txtInsuranceTelephone1"]);
                    objRf.txtEmployeeName2 = Convert.ToString(row["txtEmployeeName2"]);
                    objRf.Insurance_Phone2 = Convert.ToString(row["txtInsurancePhone2"]);
                    objRf.txtEmployeeDob2 = objCommonDAL.CheckNull(Convert.ToString(row["txtEmployeeDob2"]), null) == null ? null : new CommonBLL().ConvertToDate(Convert.ToDateTime(row["txtEmployeeDob2"]), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL));
                    objRf.txtEmployerName2 = Convert.ToString(row["txtEmployerName2"]);
                    objRf.txtYearsEmployed2 = Convert.ToString(row["txtYearsEmployed2"]);
                    objRf.txtNameofInsurance2 = Convert.ToString(row["txtNameofInsurance2"]);
                    objRf.txtInsuranceAddress2 = Convert.ToString(row["txtInsuranceAddress2"]);
                    objRf.txtInsuranceTelephone2 = Convert.ToString(row["txtInsuranceTelephone2"]);
                    if (objRf.chkMethodOfPayment != null && objRf.chkMethodOfPayment != "")
                    {
                        string Values = objRf.chkMethodOfPayment;
                        if (Values.Contains("Insurance"))
                        {
                            objRf.Insurance = true;
                        }
                        else
                        {
                            objRf.Insurance = false;
                        }
                        if (Values.Contains("Cash"))
                        {
                            objRf.Cash = true;
                        }
                        else
                        {
                            objRf.Cash = false;
                        }

                        if (Values.Contains("Credit"))
                        {
                            objRf.CrediteCard = true;
                        }
                        else
                        {
                            objRf.CrediteCard = false;
                        }

                    }
                }
            }
            PatientListRegistrationForm.Add(objRf);
            return PatientListRegistrationForm;
        }


        public List<ChildDentalMEDICALHISTORY> GetChildHistory(int PatientId)
        {
            PatientListChildDentalMEDICALHISTORY = new List<ChildDentalMEDICALHISTORY>();
            ChildDentalMEDICALHISTORY objCM = new ChildDentalMEDICALHISTORY();

            dt = objCommonDAL.GetChildHistory(PatientId, "GetChildHistory");
            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {

                }
            }
            PatientListChildDentalMEDICALHISTORY.Add(objCM);
            return PatientListChildDentalMEDICALHISTORY;
        }


        public List<DentalHistory> GetDentalHistory(int PatientId)
        {
            PatientListDentalHistoryForm = new List<DentalHistory>();
            DentalHistory objDH = new DentalHistory();

            dt = objCommonDAL.GetDentalHistory(PatientId, "GetDentalHistory");
            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    objDH.txtQue1 = Convert.ToString(row["txtQue1"]);
                    objDH.txtQue2 = Convert.ToString(row["txtQue2"]);
                    objDH.txtQue3 = Convert.ToString(row["txtQue3"]);
                    objDH.txtQue4 = Convert.ToString(row["txtQue4"]);
                    objDH.txtQue5 = Convert.ToString(row["txtQue5"]);
                    objDH.txtQue5a = Convert.ToString(row["txtQue5a"]);
                    objDH.txtQue5b = Convert.ToString(row["txtQue5b"]);
                    objDH.txtQue5c = Convert.ToString(row["txtQue5c"]);
                    objDH.txtQue6 = Convert.ToString(row["txtQue6"]);
                    objDH.txtQue7 = Convert.ToString(row["txtQue7"]);

                    if (row["rdQue7a"] != null && Convert.ToString(row["rdQue7a"]) != "")
                    {
                        string Values = Convert.ToString(row["rdQue7a"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.rdQue7a = true;
                        }
                        else
                        {
                            objDH.rdQue7a = false;
                        }
                    }



                    if (row["rdQue8"] != null && Convert.ToString(row["rdQue8"]) != "")
                    {
                        string Values = Convert.ToString(row["rdQue8"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.rdQue8 = true;
                        }
                        else
                        {
                            objDH.rdQue8 = false;
                        }
                    }

                    if (row["rdQue9"] != null && Convert.ToString(row["rdQue9"]) != "")
                    {
                        string Values = Convert.ToString(row["rdQue9"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.rdQue9 = true;
                        }
                        else
                        {
                            objDH.rdQue9 = false;
                        }
                    }
                    objDH.txtQue9a = Convert.ToString(row["txtQue9a"]);

                    if (row["rdQue10"] != null && Convert.ToString(row["rdQue10"]) != "")
                    {
                        string Values = Convert.ToString(row["rdQue10"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.rdQue10 = true;
                        }
                        else
                        {
                            objDH.rdQue10 = false;
                        }
                    }

                    if (row["rdoQue11aFixedbridge"] != null && Convert.ToString(row["rdoQue11aFixedbridge"]) != "")
                    {
                        string Values = Convert.ToString(row["rdoQue11aFixedbridge"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.rdoQue11aFixedbridge = true;
                        }
                        else
                        {
                            objDH.rdoQue11aFixedbridge = false;
                        }
                    }


                    if (row["rdoQue11bRemoveablebridge"] != null && Convert.ToString(row["rdoQue11bRemoveablebridge"]) != "")
                    {
                        string Values = Convert.ToString(row["rdoQue11bRemoveablebridge"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.rdoQue11bRemoveablebridge = true;
                        }
                        else
                        {
                            objDH.rdoQue11bRemoveablebridge = false;
                        }
                    }


                    if (row["rdoQue11cDenture"] != null && Convert.ToString(row["rdoQue11cDenture"]) != "")
                    {
                        string Values = Convert.ToString(row["rdoQue11cDenture"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.rdoQue11cDenture = true;
                        }
                        else
                        {
                            objDH.rdoQue11cDenture = false;
                        }
                    }


                    if (row["rdQue11dImplant"] != null && Convert.ToString(row["rdQue11dImplant"]) != "")
                    {
                        string Values = Convert.ToString(row["rdQue11dImplant"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.rdQue11dImplant = true;
                        }
                        else
                        {
                            objDH.rdQue11dImplant = false;
                        }
                    }

                    objDH.txtQue12 = Convert.ToString(row["txtQue12"]);

                    if (row["rdQue15"] != null && Convert.ToString(row["rdQue15"]) != "")
                    {
                        string Values = Convert.ToString(row["rdQue15"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.rdQue15 = true;
                        }
                        else
                        {
                            objDH.rdQue15 = false;
                        }
                    }


                    if (row["rdQue16"] != null && Convert.ToString(row["rdQue16"]) != "")
                    {
                        string Values = Convert.ToString(row["rdQue16"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.rdQue16 = true;
                        }
                        else
                        {
                            objDH.rdQue16 = false;
                        }
                    }

                    if (row["rdQue17"] != null && Convert.ToString(row["rdQue17"]) != "")
                    {
                        string Values = Convert.ToString(row["rdQue17"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.rdQue17 = true;
                        }
                        else
                        {
                            objDH.rdQue17 = false;
                        }
                    }

                    if (row["rdQue18"] != null && Convert.ToString(row["rdQue18"]) != "")
                    {
                        string Values = Convert.ToString(row["rdQue18"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.rdQue18 = true;
                        }
                        else
                        {
                            objDH.rdQue18 = false;
                        }
                    }

                    if (row["rdQue19"] != null && Convert.ToString(row["rdQue19"]) != "")
                    {
                        string Values = Convert.ToString(row["rdQue19"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.rdQue19 = true;
                        }
                        else
                        {
                            objDH.rdQue19 = false;
                        }
                    }
                    objDH.chkQue20 = Convert.ToString(row["chkQue20"]);

                    if (objDH.chkQue20 != null && objDH.chkQue20 != "")
                    {
                        string Values = objDH.chkQue20;
                        if (Values.Contains("1"))
                        {
                            objDH.chkQue20_1 = true;
                        }
                        else
                        {
                            objDH.chkQue20_1 = false;
                        }

                        if (Values.Contains("2"))
                        {
                            objDH.chkQue20_2 = true;
                        }
                        else
                        {
                            objDH.chkQue20_2 = false;
                        }

                        if (Values.Contains("3"))
                        {
                            objDH.chkQue20_3 = true;
                        }
                        else
                        {
                            objDH.chkQue20_3 = false;
                        }

                        if (Values.Contains("4"))
                        {
                            objDH.chkQue20_4 = true;
                        }
                        else
                        {
                            objDH.chkQue20_4 = false;
                        }
                    }


                    if (row["rdQue21"] != null && Convert.ToString(row["rdQue21"]) != "")
                    {
                        string Values = Convert.ToString(row["rdQue21"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.rdQue21 = true;
                        }
                        else
                        {
                            objDH.rdQue21 = false;
                        }
                    }
                    objDH.txtQue21a = Convert.ToString(row["txtQue21a"]);
                    objDH.txtQue22 = Convert.ToString(row["txtQue22"]);

                    objDH.txtQue23 = Convert.ToString(row["txtQue23"]);

                    if (row["rdQue24"] != null && Convert.ToString(row["rdQue24"]) != "")
                    {
                        string Values = Convert.ToString(row["rdQue24"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.rdQue24 = true;
                        }
                        else
                        {
                            objDH.rdQue24 = false;
                        }
                    }


                    objDH.txtQue26 = Convert.ToString(row["txtQue26"]);


                    if (row["rdQue28"] != null && Convert.ToString(row["rdQue28"]) != "")
                    {
                        string Values = Convert.ToString(row["rdQue28"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.rdQue28 = true;
                        }
                        else
                        {
                            objDH.rdQue28 = false;
                        }
                    }
                    objDH.txtQue28a = Convert.ToString(row["txtQue28a"]);
                    objDH.txtQue28b = Convert.ToString(row["txtQue28b"]);
                    objDH.txtQue28c = Convert.ToString(row["txtQue28c"]);
                    objDH.txtQue29 = Convert.ToString(row["txtQue29"]);
                    objDH.txtQue29a = Convert.ToString(row["txtQue29a"]);
                    if (row["rdQue30"] != null && Convert.ToString(row["rdQue30"]) != "")
                    {
                        string Values = Convert.ToString(row["rdQue30"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.rdQue30 = true;
                        }
                        else
                        {
                            objDH.rdQue30 = false;
                        }
                    }

                    objDH.txtComments = Convert.ToString(row["txtComments"]);


                }
            }
            PatientListDentalHistoryForm.Add(objDH);
            return PatientListDentalHistoryForm;
        }


        public List<MedicalHistory> GetMedicalHistory(int PatientId)
        {
            PatientListMedicalHistoryForm = new List<MedicalHistory>();
            MedicalHistory objDH = new MedicalHistory();

            dt = objCommonDAL.GetMedicalHistory(PatientId, "GetMedicalHistory");
            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    if (row["MrdQue1"] != null && Convert.ToString(row["MrdQue1"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQue1"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQue1 = true;
                        }
                        else
                        {
                            objDH.MrdQue1 = false;
                        }

                    }


                    objDH.Mtxtphysicians = Convert.ToString(row["Mtxtphysicians"]);

                    if (row["MrdQue2"] != null && Convert.ToString(row["MrdQue2"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQue2"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQue2 = true;
                        }
                        else
                        {
                            objDH.MrdQue2 = false;
                        }

                    }
                    objDH.Mtxthospitalized = Convert.ToString(row["Mtxthospitalized"]);

                    if (row["MrdQue3"] != null && Convert.ToString(row["MrdQue3"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQue3"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQue3 = true;
                        }
                        else
                        {
                            objDH.MrdQue3 = false;
                        }

                    }
                    objDH.Mtxtserious = Convert.ToString(row["Mtxtserious"]);

                    if (row["MrdQue4"] != null && Convert.ToString(row["MrdQue4"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQue4"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQue4 = true;
                        }
                        else
                        {
                            objDH.MrdQue4 = false;
                        }

                    }
                    objDH.Mtxtmedications = Convert.ToString(row["Mtxtmedications"]);

                    if (row["MrdQue5"] != null && Convert.ToString(row["MrdQue5"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQue5"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQue5 = true;
                        }
                        else
                        {
                            objDH.MrdQue5 = false;
                        }

                    }
                    objDH.MtxtRedux = Convert.ToString(row["MtxtRedux"]);

                    if (row["MrdQue6"] != null && Convert.ToString(row["MrdQue6"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQue6"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQue6 = true;
                        }
                        else
                        {
                            objDH.MrdQue6 = false;
                        }

                    }
                    objDH.MtxtFosamax = Convert.ToString(row["MtxtFosamax"]);


                    objDH.Mtxt7 = Convert.ToString(row["Mtxt7"]);
                    objDH.Mtxt8 = Convert.ToString(row["Mtxt8"]);
                    objDH.Mtxt9 = Convert.ToString(row["Mtxt9"]);
                    objDH.Mtxt10 = Convert.ToString(row["Mtxt10"]);
                    objDH.Mtxt11 = Convert.ToString(row["Mtxt11"]);
                    objDH.Mtxt12 = Convert.ToString(row["Mtxt12"]);

                    if (row["MrdQuediet7"] != null && Convert.ToString(row["MrdQuediet7"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQuediet7"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQuediet7 = true;
                        }
                        else
                        {
                            objDH.MrdQuediet7 = false;
                        }

                    }

                    if (row["Mrdotobacco8"] != null && Convert.ToString(row["Mrdotobacco8"]) != "")
                    {
                        string Values = Convert.ToString(row["Mrdotobacco8"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.Mrdotobacco8 = true;
                        }
                        else
                        {
                            objDH.Mrdotobacco8 = false;
                        }

                    }

                    if (row["Mrdosubstances"] != null && Convert.ToString(row["Mrdosubstances"]) != "")
                    {
                        string Values = Convert.ToString(row["Mrdosubstances"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.Mrdosubstances = true;
                        }
                        else
                        {
                            objDH.Mrdosubstances = false;
                        }

                    }

                    if (row["Mrdopregnant"] != null && Convert.ToString(row["Mrdopregnant"]) != "")
                    {
                        string Values = Convert.ToString(row["Mrdopregnant"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.Mrdopregnant = true;
                        }
                        else
                        {
                            objDH.Mrdopregnant = false;
                        }

                    }

                    if (row["Mrdocontraceptives"] != null && Convert.ToString(row["Mrdocontraceptives"]) != "")
                    {
                        string Values = Convert.ToString(row["Mrdocontraceptives"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.Mrdocontraceptives = true;
                        }
                        else
                        {
                            objDH.Mrdocontraceptives = false;
                        }

                    }
                    if (row["MrdoNursing"] != null && Convert.ToString(row["MrdoNursing"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdoNursing"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdoNursing = true;
                        }
                        else
                        {
                            objDH.MrdoNursing = false;
                        }

                    }

                    if (row["MchkQue_1"] != null && Convert.ToString(row["MchkQue_1"]) != "")
                    {
                        string Values = Convert.ToString(row["MchkQue_1"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MchkQue_1 = true;
                        }
                        else
                        {
                            objDH.MchkQue_1 = false;
                        }

                    }

                    if (row["MchkQue_2"] != null && Convert.ToString(row["MchkQue_2"]) != "")
                    {
                        string Values = Convert.ToString(row["MchkQue_2"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MchkQue_2 = true;
                        }
                        else
                        {
                            objDH.MchkQue_2 = false;
                        }

                    }

                    if (row["MchkQue_3"] != null && Convert.ToString(row["MchkQue_3"]) != "")
                    {
                        string Values = Convert.ToString(row["MchkQue_3"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MchkQue_3 = true;
                        }
                        else
                        {
                            objDH.MchkQue_3 = false;
                        }
                    }
                    if (row["MchkQue_4"] != null && Convert.ToString(row["MchkQue_4"]) != "")
                    {
                        string Values = Convert.ToString(row["MchkQue_4"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MchkQue_4 = true;
                        }
                        else
                        {
                            objDH.MchkQue_4 = false;
                        }
                    }

                    if (row["MchkQue_5"] != null && Convert.ToString(row["MchkQue_5"]) != "")
                    {
                        string Values = Convert.ToString(row["MchkQue_5"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MchkQue_5 = true;
                        }
                        else
                        {
                            objDH.MchkQue_5 = false;
                        }
                    }

                    if (row["MchkQue_6"] != null && Convert.ToString(row["MchkQue_6"]) != "")
                    {
                        string Values = Convert.ToString(row["MchkQue_6"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MchkQue_6 = true;
                        }
                        else
                        {
                            objDH.MchkQue_6 = false;
                        }
                    }

                    if (row["MchkQue_7"] != null && Convert.ToString(row["MchkQue_7"]) != "")
                    {
                        string Values = Convert.ToString(row["MchkQue_7"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MchkQue_7 = true;
                        }
                        else
                        {
                            objDH.MchkQue_7 = false;
                        }
                    }

                    if (row["MchkQue_8"] != null && Convert.ToString(row["MchkQue_8"]) != "")
                    {
                        string Values = Convert.ToString(row["MchkQue_8"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MchkQue_8 = true;
                        }
                        else
                        {
                            objDH.MchkQue_8 = false;
                        }
                    }

                    if (row["MchkQue_9"] != null && Convert.ToString(row["MchkQue_9"]) != "")
                    {
                        string Values = Convert.ToString(row["MchkQue_9"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MchkQue_9 = true;
                        }
                        else
                        {
                            objDH.MchkQue_9 = false;
                        }
                    }


                    objDH.MtxtchkQue_9 = Convert.ToString(row["MtxtchkQue_9"]);


                    if (row["MrdQueAIDS_HIV_Positive"] != null && Convert.ToString(row["MrdQueAIDS_HIV_Positive"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueAIDS_HIV_Positive"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueAIDS_HIV_Positive = true;
                        }
                        else
                        {
                            objDH.MrdQueAIDS_HIV_Positive = false;
                        }
                    }


                    if (row["MrdQueAlzheimer"] != null && Convert.ToString(row["MrdQueAlzheimer"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueAlzheimer"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueAlzheimer = true;
                        }
                        else
                        {
                            objDH.MrdQueAlzheimer = false;
                        }
                    }



                    if (row["MrdQueAnaphylaxis"] != null && Convert.ToString(row["MrdQueAnaphylaxis"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueAnaphylaxis"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueAnaphylaxis = true;
                        }
                        else
                        {
                            objDH.MrdQueAnaphylaxis = false;
                        }
                    }


                    if (row["MrdQueAnemia"] != null && Convert.ToString(row["MrdQueAnemia"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueAnemia"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueAnemia = true;
                        }
                        else
                        {
                            objDH.MrdQueAnemia = false;
                        }
                    }

                    if (row["MrdQueAngina"] != null && Convert.ToString(row["MrdQueAngina"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueAngina"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueAngina = true;
                        }
                        else
                        {
                            objDH.MrdQueAngina = false;
                        }
                    }

                    if (row["MrdQueArthritis_Gout"] != null && Convert.ToString(row["MrdQueArthritis_Gout"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueArthritis_Gout"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueArthritis_Gout = true;
                        }
                        else
                        {
                            objDH.MrdQueArthritis_Gout = false;
                        }
                    }


                    if (row["MrdQueArtificialHeartValve"] != null && Convert.ToString(row["MrdQueArtificialHeartValve"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueArtificialHeartValve"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueArtificialHeartValve = true;
                        }
                        else
                        {
                            objDH.MrdQueArtificialHeartValve = false;
                        }
                    }

                    if (row["MrdQueArtificialJoint"] != null && Convert.ToString(row["MrdQueArtificialJoint"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueArtificialJoint"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueArtificialJoint = true;
                        }
                        else
                        {
                            objDH.MrdQueArtificialJoint = false;
                        }
                    }


                    if (row["MrdQueAsthma"] != null && Convert.ToString(row["MrdQueAsthma"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueAsthma"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueAsthma = true;
                        }
                        else
                        {
                            objDH.MrdQueAsthma = false;
                        }
                    }

                    if (row["MrdQueBloodDisease"] != null && Convert.ToString(row["MrdQueBloodDisease"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueBloodDisease"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueBloodDisease = true;
                        }
                        else
                        {
                            objDH.MrdQueBloodDisease = false;
                        }
                    }
                    if (row["MrdQueBloodTransfusion"] != null && Convert.ToString(row["MrdQueBloodTransfusion"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueBloodTransfusion"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueBloodTransfusion = true;
                        }
                        else
                        {
                            objDH.MrdQueBloodTransfusion = false;
                        }
                    }

                    if (row["MrdQueBreathing"] != null && Convert.ToString(row["MrdQueBreathing"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueBreathing"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueBreathing = true;
                        }
                        else
                        {
                            objDH.MrdQueBreathing = false;
                        }
                    }


                    if (row["MrdQueBruise"] != null && Convert.ToString(row["MrdQueBruise"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueBruise"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueBruise = true;
                        }
                        else
                        {
                            objDH.MrdQueBruise = false;
                        }
                    }

                    if (row["MrdQueCancer"] != null && Convert.ToString(row["MrdQueCancer"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueCancer"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueCancer = true;
                        }
                        else
                        {
                            objDH.MrdQueCancer = false;
                        }
                    }


                    if (row["MrdQueChemotherapy"] != null && Convert.ToString(row["MrdQueChemotherapy"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueChemotherapy"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueChemotherapy = true;
                        }
                        else
                        {
                            objDH.MrdQueChemotherapy = false;
                        }
                    }


                    if (row["MrdQueChest"] != null && Convert.ToString(row["MrdQueChest"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueChest"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueChest = true;
                        }
                        else
                        {
                            objDH.MrdQueChest = false;
                        }
                    }


                    if (row["MrdQueCold_Sores_Fever"] != null && Convert.ToString(row["MrdQueCold_Sores_Fever"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueCold_Sores_Fever"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueCold_Sores_Fever = true;
                        }
                        else
                        {
                            objDH.MrdQueCold_Sores_Fever = false;
                        }
                    }

                    if (row["MrdQueCongenital"] != null && Convert.ToString(row["MrdQueCongenital"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueCongenital"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueCongenital = true;
                        }
                        else
                        {
                            objDH.MrdQueCongenital = false;
                        }
                    }


                    if (row["MrdQueConvulsions"] != null && Convert.ToString(row["MrdQueConvulsions"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueConvulsions"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueConvulsions = true;
                        }
                        else
                        {
                            objDH.MrdQueConvulsions = false;
                        }
                    }

                    if (row["MrdQueCortisone"] != null && Convert.ToString(row["MrdQueCortisone"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueCortisone"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueCortisone = true;
                        }
                        else
                        {
                            objDH.MrdQueCortisone = false;
                        }
                    }

                    if (row["MrdQueDiabetes"] != null && Convert.ToString(row["MrdQueDiabetes"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueDiabetes"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueDiabetes = true;
                        }
                        else
                        {
                            objDH.MrdQueDiabetes = false;
                        }
                    }

                    if (row["MrdQueDrug"] != null && Convert.ToString(row["MrdQueDrug"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueDrug"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueDrug = true;
                        }
                        else
                        {
                            objDH.MrdQueDrug = false;
                        }
                    }

                    if (row["MrdQueEasily"] != null && Convert.ToString(row["MrdQueEasily"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueEasily"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueEasily = true;
                        }
                        else
                        {
                            objDH.MrdQueEasily = false;
                        }
                    }

                    if (row["MrdQueEmphysema"] != null && Convert.ToString(row["MrdQueEmphysema"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueEmphysema"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueEmphysema = true;
                        }
                        else
                        {
                            objDH.MrdQueEmphysema = false;
                        }
                    }


                    if (row["MrdQueEpilepsy"] != null && Convert.ToString(row["MrdQueEpilepsy"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueEpilepsy"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueEpilepsy = true;
                        }
                        else
                        {
                            objDH.MrdQueEpilepsy = false;
                        }
                    }

                    if (row["MrdQueExcessiveBleeding"] != null && Convert.ToString(row["MrdQueExcessiveBleeding"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueExcessiveBleeding"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueExcessiveBleeding = true;
                        }
                        else
                        {
                            objDH.MrdQueExcessiveBleeding = false;
                        }
                    }

                    if (row["MrdQueExcessiveThirst"] != null && Convert.ToString(row["MrdQueExcessiveThirst"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueExcessiveThirst"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueExcessiveThirst = true;
                        }
                        else
                        {
                            objDH.MrdQueExcessiveThirst = false;
                        }
                    }


                    if (row["MrdQueFainting"] != null && Convert.ToString(row["MrdQueFainting"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueFainting"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueFainting = true;
                        }
                        else
                        {
                            objDH.MrdQueFainting = false;
                        }
                    }


                    if (row["MrdQueFrequentCough"] != null && Convert.ToString(row["MrdQueFrequentCough"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueFrequentCough"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueFrequentCough = true;
                        }
                        else
                        {
                            objDH.MrdQueFrequentCough = false;
                        }
                    }


                    if (row["MrdQueFrequentDiarrhea"] != null && Convert.ToString(row["MrdQueFrequentDiarrhea"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueFrequentDiarrhea"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueFrequentDiarrhea = true;
                        }
                        else
                        {
                            objDH.MrdQueFrequentDiarrhea = false;
                        }
                    }

                    if (row["MrdQueFrequentHeadaches"] != null && Convert.ToString(row["MrdQueFrequentHeadaches"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueFrequentHeadaches"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueFrequentHeadaches = true;
                        }
                        else
                        {
                            objDH.MrdQueFrequentHeadaches = false;
                        }
                    }

                    if (row["MrdQueGenital"] != null && Convert.ToString(row["MrdQueGenital"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueGenital"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueGenital = true;
                        }
                        else
                        {
                            objDH.MrdQueGenital = false;
                        }
                    }

                    if (row["MrdQueGlaucoma"] != null && Convert.ToString(row["MrdQueGlaucoma"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueGlaucoma"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueGlaucoma = true;
                        }
                        else
                        {
                            objDH.MrdQueGlaucoma = false;
                        }
                    }
                    if (row["MrdQueHay"] != null && Convert.ToString(row["MrdQueHay"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueHay"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueHay = true;
                        }
                        else
                        {
                            objDH.MrdQueHay = false;
                        }
                    }
                    if (row["MrdQueHeartAttack_Failure"] != null && Convert.ToString(row["MrdQueHeartAttack_Failure"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueHeartAttack_Failure"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueHeartAttack_Failure = true;
                        }
                        else
                        {
                            objDH.MrdQueHeartAttack_Failure = false;
                        }
                    }

                    if (row["MrdQueHeartMurmur"] != null && Convert.ToString(row["MrdQueHeartMurmur"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueHeartMurmur"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueHeartMurmur = true;
                        }
                        else
                        {
                            objDH.MrdQueHeartMurmur = false;
                        }
                    }


                    if (row["MrdQueHeartPacemaker"] != null && Convert.ToString(row["MrdQueHeartPacemaker"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueHeartPacemaker"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueHeartPacemaker = true;
                        }
                        else
                        {
                            objDH.MrdQueHeartPacemaker = false;
                        }
                    }


                    if (row["MrdQueHeartTrouble_Disease"] != null && Convert.ToString(row["MrdQueHeartTrouble_Disease"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueHeartTrouble_Disease"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueHeartTrouble_Disease = true;
                        }
                        else
                        {
                            objDH.MrdQueHeartTrouble_Disease = false;
                        }
                    }


                    if (row["MrdQueHemophilia"] != null && Convert.ToString(row["MrdQueHemophilia"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueHemophilia"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueHemophilia = true;
                        }
                        else
                        {
                            objDH.MrdQueHemophilia = false;
                        }
                    }


                    if (row["MrdQueHepatitisA"] != null && Convert.ToString(row["MrdQueHepatitisA"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueHepatitisA"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueHepatitisA = true;
                        }
                        else
                        {
                            objDH.MrdQueHepatitisA = false;
                        }
                    }


                    if (row["MrdQueHepatitisBorC"] != null && Convert.ToString(row["MrdQueHepatitisBorC"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueHepatitisBorC"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueHepatitisBorC = true;
                        }
                        else
                        {
                            objDH.MrdQueHepatitisBorC = false;
                        }
                    }


                    if (row["MrdQueHerpes"] != null && Convert.ToString(row["MrdQueHerpes"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueHerpes"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueHerpes = true;
                        }
                        else
                        {
                            objDH.MrdQueHerpes = false;
                        }
                    }


                    if (row["MrdQueHighBloodPressure"] != null && Convert.ToString(row["MrdQueHighBloodPressure"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueHighBloodPressure"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueHighBloodPressure = true;
                        }
                        else
                        {
                            objDH.MrdQueHighBloodPressure = false;
                        }
                    }


                    if (row["MrdQueHighCholesterol"] != null && Convert.ToString(row["MrdQueHighCholesterol"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueHighCholesterol"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueHighCholesterol = true;
                        }
                        else
                        {
                            objDH.MrdQueHighCholesterol = false;
                        }
                    }


                    if (row["MrdQueHives"] != null && Convert.ToString(row["MrdQueHives"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueHives"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueHives = true;
                        }
                        else
                        {
                            objDH.MrdQueHives = false;
                        }
                    }


                    if (row["MrdQueHypoglycemia"] != null && Convert.ToString(row["MrdQueHypoglycemia"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueHypoglycemia"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueHypoglycemia = true;
                        }
                        else
                        {
                            objDH.MrdQueHypoglycemia = false;
                        }
                    }


                    if (row["MrdQueIrregular"] != null && Convert.ToString(row["MrdQueIrregular"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueIrregular"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueIrregular = true;
                        }
                        else
                        {
                            objDH.MrdQueIrregular = false;
                        }
                    }

                    if (row["MrdQueKidney"] != null && Convert.ToString(row["MrdQueKidney"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueKidney"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueKidney = true;
                        }
                        else
                        {
                            objDH.MrdQueKidney = false;
                        }
                    }

                    if (row["MrdQueLeukemia"] != null && Convert.ToString(row["MrdQueLeukemia"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueLeukemia"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueLeukemia = true;
                        }
                        else
                        {
                            objDH.MrdQueLeukemia = false;
                        }
                    }
                    if (row["MrdQueLiver"] != null && Convert.ToString(row["MrdQueLiver"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueLiver"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueLiver = true;
                        }
                        else
                        {
                            objDH.MrdQueLiver = false;
                        }
                    }


                    if (row["MrdQueLow"] != null && Convert.ToString(row["MrdQueLow"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueLow"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueLow = true;
                        }
                        else
                        {
                            objDH.MrdQueLow = false;
                        }
                    }

                    if (row["MrdQueLung"] != null && Convert.ToString(row["MrdQueLung"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueLung"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueLung = true;
                        }
                        else
                        {
                            objDH.MrdQueLung = false;
                        }
                    }

                    if (row["MrdQueMitral"] != null && Convert.ToString(row["MrdQueMitral"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueMitral"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueMitral = true;
                        }
                        else
                        {
                            objDH.MrdQueMitral = false;
                        }
                    }

                    if (row["MrdQueOsteoporosis"] != null && Convert.ToString(row["MrdQueOsteoporosis"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueOsteoporosis"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueOsteoporosis = true;
                        }
                        else
                        {
                            objDH.MrdQueOsteoporosis = false;
                        }
                    }

                    if (row["MrdQuePain"] != null && Convert.ToString(row["MrdQuePain"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQuePain"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQuePain = true;
                        }
                        else
                        {
                            objDH.MrdQuePain = false;
                        }
                    }

                    if (row["MrdQueParathyroid"] != null && Convert.ToString(row["MrdQueParathyroid"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueParathyroid"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueParathyroid = true;
                        }
                        else
                        {
                            objDH.MrdQueParathyroid = false;
                        }
                    }

                    if (row["MrdQuePsychiatric"] != null && Convert.ToString(row["MrdQuePsychiatric"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQuePsychiatric"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQuePsychiatric = true;
                        }
                        else
                        {
                            objDH.MrdQuePsychiatric = false;
                        }
                    }

                    if (row["MrdQueRadiation"] != null && Convert.ToString(row["MrdQueRadiation"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueRadiation"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueRadiation = true;
                        }
                        else
                        {
                            objDH.MrdQueRadiation = false;
                        }
                    }

                    if (row["MrdQueRecent"] != null && Convert.ToString(row["MrdQueRecent"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueRecent"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueRecent = true;
                        }
                        else
                        {
                            objDH.MrdQueRecent = false;
                        }
                    }

                    if (row["MrdQueRenal"] != null && Convert.ToString(row["MrdQueRenal"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueRenal"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueRenal = true;
                        }
                        else
                        {
                            objDH.MrdQueRenal = false;
                        }
                    }

                    if (row["MrdQueRheumatic"] != null && Convert.ToString(row["MrdQueRheumatic"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueRheumatic"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueRheumatic = true;
                        }
                        else
                        {
                            objDH.MrdQueRheumatic = false;
                        }
                    }




                    if (row["MrdQueRheumatism"] != null && Convert.ToString(row["MrdQueRheumatism"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueRheumatism"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueRheumatism = true;
                        }
                        else
                        {
                            objDH.MrdQueRheumatism = false;
                        }
                    }

                    if (row["MrdQueScarlet"] != null && Convert.ToString(row["MrdQueScarlet"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueScarlet"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueScarlet = true;
                        }
                        else
                        {
                            objDH.MrdQueScarlet = false;
                        }
                    }

                    if (row["MrdQueShingles"] != null && Convert.ToString(row["MrdQueShingles"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueShingles"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueShingles = true;
                        }
                        else
                        {
                            objDH.MrdQueShingles = false;
                        }
                    }

                    if (row["MrdQueSickle"] != null && Convert.ToString(row["MrdQueSickle"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueSickle"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueSickle = true;
                        }
                        else
                        {
                            objDH.MrdQueSickle = false;
                        }
                    }

                    if (row["MrdQueSinus"] != null && Convert.ToString(row["MrdQueSinus"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueSinus"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueSinus = true;
                        }
                        else
                        {
                            objDH.MrdQueSinus = false;
                        }
                    }

                    if (row["MrdQueSpina"] != null && Convert.ToString(row["MrdQueSpina"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueSpina"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueSpina = true;
                        }
                        else
                        {
                            objDH.MrdQueSpina = false;
                        }
                    }

                    if (row["MrdQueStomach"] != null && Convert.ToString(row["MrdQueStomach"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueStomach"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueStomach = true;
                        }
                        else
                        {
                            objDH.MrdQueStomach = false;
                        }
                    }

                    if (row["MrdQueStroke"] != null && Convert.ToString(row["MrdQueStroke"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueStroke"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueStroke = true;
                        }
                        else
                        {
                            objDH.MrdQueStroke = false;
                        }
                    }

                    if (row["MrdQueSwelling"] != null && Convert.ToString(row["MrdQueSwelling"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueSwelling"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueSwelling = true;
                        }
                        else
                        {
                            objDH.MrdQueSwelling = false;
                        }
                    }

                    if (row["MrdQueThyroid"] != null && Convert.ToString(row["MrdQueThyroid"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueThyroid"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueThyroid = true;
                        }
                        else
                        {
                            objDH.MrdQueThyroid = false;
                        }
                    }

                    if (row["MrdQueTonsillitis"] != null && Convert.ToString(row["MrdQueTonsillitis"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueTonsillitis"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueTonsillitis = true;
                        }
                        else
                        {
                            objDH.MrdQueTonsillitis = false;
                        }
                    }





                    if (row["MrdQueTuberculosis"] != null && Convert.ToString(row["MrdQueTuberculosis"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueTuberculosis"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueTuberculosis = true;
                        }
                        else
                        {
                            objDH.MrdQueTuberculosis = false;
                        }
                    }

                    if (row["MrdQueTumors"] != null && Convert.ToString(row["MrdQueTumors"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueTumors"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueTumors = true;
                        }
                        else
                        {
                            objDH.MrdQueTumors = false;
                        }
                    }

                    if (row["MrdQueUlcers"] != null && Convert.ToString(row["MrdQueUlcers"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueUlcers"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueUlcers = true;
                        }
                        else
                        {
                            objDH.MrdQueUlcers = false;
                        }
                    }

                    if (row["MrdQueVenereal"] != null && Convert.ToString(row["MrdQueVenereal"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueVenereal"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueVenereal = true;
                        }
                        else
                        {
                            objDH.MrdQueVenereal = false;
                        }
                    }

                    if (row["MrdQueYellow"] != null && Convert.ToString(row["MrdQueYellow"]) != "")
                    {
                        string Values = Convert.ToString(row["MrdQueYellow"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.MrdQueYellow = true;
                        }
                        else
                        {
                            objDH.MrdQueYellow = false;
                        }
                    }


                    objDH.Mtxtillness = Convert.ToString(row["Mtxtillness"]);
                    objDH.MtxtComments = Convert.ToString(row["MtxtComments"]);

                }
            }
            PatientListMedicalHistoryForm.Add(objDH);
            return PatientListMedicalHistoryForm;
        }

        public List<MedicalHISTORYUpdate> GetMedicalUpdate(int PatientId)
        {
            PatientListMedicalHISTORYUpdateForm = new List<MedicalHISTORYUpdate>();
            MedicalHISTORYUpdate objDH = new MedicalHISTORYUpdate();

            dt = objCommonDAL.GetMedicalUpdate(PatientId, "GetMedicalUpdate");
            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    objDH.txt1b = Convert.ToString(row["txt1b"]);
                    objDH.txtDigiSignhistoryupdate = Convert.ToString(row["txtDigiSign"]);

                }
            }
            PatientListMedicalHISTORYUpdateForm.Add(objDH);
            return PatientListMedicalHISTORYUpdateForm;


        }


        public List<Review> PatientFormsSent(int PatientId)
        {
            PatientListReviewForm = new List<Review>();
            Review objDH = new Review();

            dt = objCommonDAL.PatientFormsSent(PatientId, "ReviewFormsList");
            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    PatientListReviewForm.Add(new Review()
                    {
                        FormId = Convert.ToString(row["FormId"]),
                        FormName = Convert.ToString(row["FormName"]),
                        FormCode = Convert.ToString(row["FormCode"]),
                        FormLink = Convert.ToString(row["FormLink"]),
                        ModifiedDate = Convert.ToString(row["ModifiedDate"]),
                        Clickid = Convert.ToString(row["Clickid"]),
                        position = Convert.ToString(row["position"]),
                    });
                }
            }
            PatientListReviewForm.Add(objDH);
            return PatientListReviewForm;
        }




        public List<PatientUploadedImages> GetPatientMontages(int PatientId)
        {
            PatientListUploadedImages = new List<PatientUploadedImages>();


            dt = objCommonDAL.GetPatientMontages(PatientId);
            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    PatientListUploadedImages.Add(new PatientUploadedImages()
                    {

                        RelativePath = Convert.ToString(row["RelativePath"]),
                    });
                }
            }

            return PatientListUploadedImages;
        }

        public List<Notes> getPatientNotes(int PatientId, int ReferralId)
        {
            ListNote = new List<Notes>();


            dt = objCommonDAL.getPatientNotes(PatientId, ReferralId);
            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    ListNote.Add(new Notes()
                    {

                        ColleagueFullName = Convert.ToString(row["ColleagueFullName"]),
                        RelativePathToImage = Convert.ToString(row["RelativePathToImage"]),
                        PatientNotes = Convert.ToString(row["PatientNotes"]),
                        CreatedDate = Convert.ToDateTime(row["CreatedDate"]),
                        Note_Id = Convert.ToString(row["Note_Id"]),
                        UserId = Convert.ToString(row["UserId"]),
                    });
                }
            }

            return ListNote;
        }


        private string gettruefalse(string value)
        {
            string Result = "";
            if (value == "Y")
            {
                Result = "0";
            }
            else if (value == "N")
            {
                Result = "1";
            }
            else
            {
                Result = "";
            }
            return Result;
        }

    }
}