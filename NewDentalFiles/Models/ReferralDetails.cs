﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
namespace DentalFiles.Models
{
    public class ReferralDetails
    {
        public int ReferralCardId { get; set; }
        public string FromField { get; set; }
        public string ToField { get; set; }
        public int MessageTypeId { get; set; }
        public int OwnerId { get; set; }
        public int PatientId { get; set; }
        public string Status { get; set; }
        public string CreationDate { get; set; }
        public string PatientName { get; set; }
        public string FromPublicPath { get; set; }
        public string ToPublicPath { get; set; }

    }

    public class Messages
    {
        public int PatientmessageId { get; set; }
        public string FromField { get; set; }
        public string ToField { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public int DoctorId { get; set; }
        public int PatientId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public bool IsSave { get; set; }
        public bool DFlag { get; set; }
        public bool PFlag { get; set; }
        public bool IsDoctor { get; set; }
        public bool IsPatient { get; set; }
        public string Read_flag { get; set; }
        public int MsgCount { get; set; }
        public int TotalRecord { get; set; }
        public string PublicPath { get; set; }
        public string MessageFirstLine { get; set; }

    }

    public class Inbox
    {
        public int SenderId { get; set; }
        public string SenderName { get; set; }
        public string MessageBodyFirstLine { get; set; }
        public int MessageId { get; set; }
        public string  Date { get; set; }
        public bool ReadFlag { get; set; }
        public bool HasAttachment { get; set; }
       
    }

    public class Sent
    {
        public int PatientmessageId { get; set; }
        public string FromField { get; set; }
        public string ToField { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public int DoctorId { get; set; }
        public int PatientId { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public bool IsSave { get; set; }
        public bool DFlag { get; set; }
        public bool PFlag { get; set; }
        public bool IsDoctor { get; set; }
        public bool IsPatient { get; set; }
        public string Read_flag { get; set; }
        public int TotalRecord { get; set; }
        public string PublicPath { get; set; }
        public string MessageBodyFirstLine { get; set; }

    }

    public class ReadMessage
    {
        public int MessageId { get; set; }
        
        public string MessageBody { get; set; }
        public string AttachmentsCount { get; set; }
        public  List<Attachments> Attachments = new List<Attachments>();
    }

    public class Attachments
    {
        public int AttachmentId { get; set; }
        public string AttachmentName { get; set; }
        public string AttachmentType { get; set; }
        public string AttachmentUrl { get; set; }
        
    }

    public class Notes
    {
        
        public string ColleagueFullName { get; set; }
        public string RelativePathToImage { get; set; }
        public string PatientNotes { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Note_Id { get; set; }
        public string UserId { get; set; }

    }

    public class InviteTreatingDoctor
    {
        public int UserID { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string Email { set; get; }
        public string Description { set; get; }
        public string Username { set; get; }
        public string image { set; get; }
        public string name { set; get; }
        public string State { set; get; }
        public string City { set; get; }
        public string Country { set; get; }
        public string ExactAddress { set; get; }
        public string Address2 { set; get; }
        public string Miles { set; get; }
        public string PublicPath { set; get; }
        public string RowNumber { set; get; }
        public string TotalRecord { set; get; }
        public string Specialities { set; get; }
        public string Phone { set; get; }
    }
}