﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//using Mandrill;
//using Mandrill.Models;
//using Mandrill.Requests.Messages;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Threading.Tasks;


namespace DentalFilesNew.Models
{
    public class SendCustomeMail
    {
        static string MandrillApiKey = ConfigurationManager.AppSettings["mandrillapikey"];
        public async Task SendMail(Mailtype MailType, CustomeMailPropery objCustomeMailPropery)
        {
            if (Mailtype.ManDrill == MailType)
            {
                //MandrillApi api = new MandrillApi(MandrillApiKey);
                //Mandrill.Models.EmailMessage objMailMessage = new Mandrill.Models.EmailMessage();
                //List<EmailAddress> ToMaillst = new List<EmailAddress>();

                //foreach (var item in objCustomeMailPropery.ToMailAddress)
                //{
                //    ToMaillst.Add(new EmailAddress { Email = item });
                //}
                //objMailMessage.To = ToMaillst;
                //objMailMessage.FromEmail = objCustomeMailPropery.FromMailAddress;
                //objMailMessage.FromName = objCustomeMailPropery.FromMailAddress;
                //if (objCustomeMailPropery.BCCMailAddress.Count > 0)
                //{
                //    objMailMessage.BccAddress = objCustomeMailPropery.BCCMailAddress[0];
                //}
                //foreach (DataRow item in objCustomeMailPropery.MergeTags.Rows)
                //{
                //    objMailMessage.AddGlobalVariable(Convert.ToString(item["FieldName"]), Convert.ToString(item["FieldValue"]));
                //}

                //Mandrill.Requests.Messages.SendMessageTemplateRequest objSMTR = new SendMessageTemplateRequest(objMailMessage, objCustomeMailPropery.MailTemplateName, null);
                // List<EmailResult> result = await api.SendMessageTemplate(objSMTR);
            }
        }
    }
    public class CustomeMailPropery
    {
        public string FromMailAddress { get; set; }
        public List<string> ToMailAddress { get; set; }
        public List<string> CCMailAddress { get; set; }
        public List<string> BCCMailAddress { get; set; }
        public string MailTemplateName { get; set; }
        public DataTable MergeTags { get; set; }
    }
    public enum Mailtype
    {
        ManDrill
    }
}