﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace DentalFiles.Models
{
    public class _AdvancedSearchColl
    {
        
        public string _Keywords { get; set; }

        public string _Firstname { get; set; }

        public string _Lastname { get; set; }

        public string _Title { get; set; }

        public string _Company { get; set; }

        public string _School { get; set; }

        public string _Email { get; set; }

        public string _PhoneNumber { get; set; }

        public SelectList _Distance { get; set; }
        public List<SelectListItem> lst { get; set; }
        public string _ZipCode { get; set; }
        public string _City { get; set; }
    }


}