﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Configuration;
using System.IO;
using System.Web.Mvc;
using System.Collections;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Collections.Specialized;
using DataAccessLayer.PatientsData;
using Recordlinc.Core;
using Recordlinc.DAL;
using Recordlinc.Core.Common;
using Recordlinc.Core.DAL;
using System.Net;
using DentalFilesNew.Models;
using System.Threading.Tasks;

namespace DentalFiles.Models
{
    public class CommonDAL
    {

        DataSet ds = new DataSet();
        SqlDataAdapter da = new SqlDataAdapter();
        clsHelper clshelper = new clsHelper();
        Helper_ConnectionClass obj_connection = new Helper_ConnectionClass();
        TripleDESCryptoHelper cryptoHelper = new TripleDESCryptoHelper();
        DataTable dtsummary = new DataTable();
        DataTable dt = new DataTable();
        MailMessage NewMeggage = new MailMessage();
        SmtpClient smtpClient = new SmtpClient();


        bool result = false;
        int x = 0;
        int ImageId = 0;
        string UserName = "";
        string ImageUrl = "";

        string Speciality = "";
        string State = "";
        string Country = "";
        string TravisEmailId = Convert.ToString(ConfigurationManager.AppSettings.Get("TravisEmailId"));

        public bool SendContactMessage(string Name, string Email, string Phone, string Comment)
        {
            bool Result = false;
            string Commentcheck = "";
            if (Phone == "webinar")
            {
                Commentcheck = "";
            }
            else
            {
                Commentcheck = Phone;
            }
            SmtpClient smtpClient = new SmtpClient();
            smtpClient = new SmtpClient(ConfigurationManager.AppSettings.Get("SMTPServer"));
            MailMessage message = new MailMessage();
            MailAddress fromAddress = new MailAddress(Email);
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings.Get("SMTPLogin"), ConfigurationManager.AppSettings.Get("SMTPPassword"));

            message.From = new MailAddress(ConfigurationManager.AppSettings.Get("SMTPLogin"));
            message.To.Clear();

            message.To.Add(ConfigurationManager.AppSettings.Get("SaleEmail"));
            message.ReplyToList.Add(fromAddress);
            message.Subject = "Request received at RecordLinc";
            message.Body = Email + System.Environment.NewLine + Comment + System.Environment.NewLine + "From:" + Name + System.Environment.NewLine + "PhoneNo: " + Commentcheck;
            message.Priority = MailPriority.High;
            try
            {
                smtpClient.Send(message);
                Result = true;
            }
            catch (Exception)
            {
                return Result;

            }
            return Result;
        }

        public DataTable GetAllSpeciality()
        {
            try
            {
                dt = clshelper.DataTable("GetAllSpeciality");
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<SelectListItem> GetSpeciality()
        {

            List<SelectListItem> _Speciality = new List<SelectListItem>();

            DataTable dt = GetAllSpeciality();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    _Speciality.Add(new SelectListItem { Text = item["Description"].ToString(), Value = item["Specialityid"].ToString() });
                }
            }
            return _Speciality;
        }

        public DataTable CheckUserDetails(string Email)
        {
            try
            {
                SqlParameter[] Patparameters = new SqlParameter[1];
                Patparameters[0] = new SqlParameter();
                Patparameters[0].ParameterName = "@Email";
                Patparameters[0].Value = Email;
                dt = clshelper.DataTable("USP_CheckDoctorEmailExists", Patparameters); return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string GetTemplate(string TemplateName, string Operation)
        {
            string Desc;
            try
            {
                SqlParameter[] patientIdParams = new SqlParameter[1];
                patientIdParams[0] = new SqlParameter("@TemplateName", SqlDbType.VarChar);
                patientIdParams[0].Value = TemplateName;
                DataTable temp = clshelper.DataTable("USP_GetAllEmailTemplateByName", patientIdParams);

                if (temp.Rows.Count > 0)
                {
                    if (Operation == "Desc")
                    {
                        Desc = temp.Rows[0]["TemplateDesc"].ToString();
                    }
                    else if (Operation == "Subject")
                    {
                        Desc = temp.Rows[0]["TemplateSubject"].ToString();
                    }
                    else
                    {
                        Desc = "";
                    }
                }
                else
                {
                    Desc = "";
                }
            }
            catch (Exception)
            {
                throw;
            }

            return Desc;
        }
        public DataTable GetEmailSubjectForTemplate(DataTable dt)
        {
            try
            {
                DataSet dsCompany = new DataSet();
                dsCompany = ChechActiveCompany();
                if (dsCompany != null && dsCompany.Tables.Count > 0 && dsCompany.Tables[0].Rows.Count > 0)
                {
                    string CompanyName = dsCompany.Tables[0].Rows[0]["CompanyName"].ToString();
                    string CompanyCity = dsCompany.Tables[0].Rows[0]["CompanyCity"].ToString();
                    string CompanyState = dsCompany.Tables[0].Rows[0]["CompanyState"].ToString();
                    string CompanyCountry = dsCompany.Tables[0].Rows[0]["CompanyCountry"].ToString();
                    string CompanyAddress = "@ " + DateTime.Now.Year + " " + CompanyName + " Inc." + " " + CompanyCity + " " + CompanyState + ", " + CompanyCountry + ".";
                    string CompanyWebsite = dsCompany.Tables[0].Rows[0]["CompanyWebsite"].ToString();
                    string CompanyLogo = dsCompany.Tables[0].Rows[0]["LogoPath"].ToString();
                    CompanyLogo = CompanyLogo.Replace("~/", "/");
                    CompanyLogo = CompanyWebsite + CompanyLogo;

                    if (HttpContext.Current.Session["DonateDentist.org"] != null && Convert.ToString(HttpContext.Current.Session["DonateDentist.org"]) == "DonateDentist.org")
                    {
                        dt.Rows.Add("CompanyName", "DonateDentist.org");


                    }
                    else
                    {
                        dt.Rows.Add("CompanyName", CompanyName);

                    }
                    dt.Rows.Add("CompanyCity", CompanyCity);
                    dt.Rows.Add("CompanyState", CompanyCity);
                    dt.Rows.Add("CompanyCountry", CompanyCity);



                }
                return dt;
            }
            catch (Exception)
            {
                return dt;
            }

        }
        public DataTable GetEmailBodyForTemplate(DataTable dt)
        {
            try
            {
                DataSet dsCompany = new DataSet();
                dsCompany = ChechActiveCompany();
                if (dsCompany != null && dsCompany.Tables.Count > 0 && dsCompany.Tables[0].Rows.Count > 0)
                {
                    string CompanyName = dsCompany.Tables[0].Rows[0]["CompanyName"].ToString();
                    string CompanyCity = dsCompany.Tables[0].Rows[0]["CompanyCity"].ToString();
                    string CompanyState = dsCompany.Tables[0].Rows[0]["CompanyState"].ToString();
                    string CompanyCountry = dsCompany.Tables[0].Rows[0]["CompanyCountry"].ToString();
                    string CompanyZipCode = dsCompany.Tables[0].Rows[0]["CompanyZipCode"].ToString();
                    string CompanyPhone = dsCompany.Tables[0].Rows[0]["PhoneNo"].ToString();
                    string CompanyOwnerName = dsCompany.Tables[0].Rows[0]["CompanyOwnerName"].ToString();
                    string CompanyOwnerTitle = dsCompany.Tables[0].Rows[0]["CompanyOwnerTitle"].ToString();
                    string CompanyOwnerEmail = dsCompany.Tables[0].Rows[0]["CompanyOwnerEmail"].ToString();

                    string CompanyAddress = "@ " + DateTime.Now.Year + " " + CompanyName + " Inc." + " " + CompanyCity + " " + CompanyState + ", " + CompanyCountry + ".";
                    string CompanyWebsite = dsCompany.Tables[0].Rows[0]["CompanyWebsite"].ToString();
                    string CompanyLogo = dsCompany.Tables[0].Rows[0]["LogoPath"].ToString();
                    string CompanySupportEmail = dsCompany.Tables[0].Rows[0]["SupportEmail"].ToString();
                    CompanyLogo = CompanyLogo.Replace("~/", "/");
                    CompanyLogo = CompanyLogo.Replace("../", "/");
                    CompanyLogo = CompanyWebsite + CompanyLogo;


                    dt.Rows.Add("CompanyLogo", CompanyLogo);
                    dt.Rows.Add("CompanyJoinUrl", CompanyWebsite);
                    dt.Rows.Add("loginlink", CompanyWebsite);
                    dt.Rows.Add("CompanyName", CompanyName);
                    dt.Rows.Add("CompanyCity", CompanyCity);
                    dt.Rows.Add("CompanyState", CompanyState);
                    dt.Rows.Add("CompanyCountry", CompanyCountry);
                    dt.Rows.Add("CompanyZipCode", CompanyZipCode);
                    dt.Rows.Add("CompanyPhone", CompanyPhone);
                    dt.Rows.Add("CompanyWebsite", CompanyWebsite);
                    dt.Rows.Add("CompanySupportEmail", CompanySupportEmail);
                    dt.Rows.Add("CompanyOwnerEmail", CompanyOwnerEmail);
                    dt.Rows.Add("CompanyOwnerName", CompanyOwnerName);
                    dt.Rows.Add("CompanyOwnerTitle", CompanyOwnerTitle);

                    dt.Rows.Add("CompanyYear", DateTime.Now.Year.ToString());
                }
                return dt;
            }
            catch (Exception)
            {
                return dt;
            }
        }
        public string EmailSubject(string TempSubject)
        {
            try
            {
                DataSet dsCompany = new DataSet();
                dsCompany = ChechActiveCompany();
                if (dsCompany != null && dsCompany.Tables.Count > 0 && dsCompany.Tables[0].Rows.Count > 0)
                {
                    string CompanyName = dsCompany.Tables[0].Rows[0]["CompanyName"].ToString();
                    string CompanyCity = dsCompany.Tables[0].Rows[0]["CompanyCity"].ToString();
                    string CompanyState = dsCompany.Tables[0].Rows[0]["CompanyState"].ToString();
                    string CompanyCountry = dsCompany.Tables[0].Rows[0]["CompanyCountry"].ToString();
                    string CompanyAddress = "@ " + DateTime.Now.Year.ToString() + " " + CompanyName + " Inc." + " " + CompanyCity + " " + CompanyState + ", " + CompanyCountry + ".";
                    string CompanyWebsite = dsCompany.Tables[0].Rows[0]["CompanyWebsite"].ToString();
                    string CompanyLogo = dsCompany.Tables[0].Rows[0]["LogoPath"].ToString();
                    CompanyLogo = CompanyLogo.Replace("~/", "/");
                    CompanyLogo = CompanyWebsite + CompanyLogo;

                    TempSubject = TempSubject.Replace("#CompanyName#", CompanyName);
                    TempSubject = TempSubject.Replace("#CompanyCity#", CompanyCity);
                    TempSubject = TempSubject.Replace("#CompanyState#", CompanyState);
                    TempSubject = TempSubject.Replace("#CompanyCountry#", CompanyCountry);

                }
                return TempSubject;
            }
            catch (Exception)
            {
                return TempSubject;
            }
        }

        public string EmailBodyContent(string strEmailBody)
        {
            try
            {
                DataSet dsCompany = new DataSet();
                dsCompany = ChechActiveCompany();
                if (dsCompany != null && dsCompany.Tables.Count > 0 && dsCompany.Tables[0].Rows.Count > 0)
                {
                    string CompanyName = dsCompany.Tables[0].Rows[0]["CompanyName"].ToString();
                    string CompanyCity = dsCompany.Tables[0].Rows[0]["CompanyCity"].ToString();
                    string CompanyState = dsCompany.Tables[0].Rows[0]["CompanyState"].ToString();
                    string CompanyCountry = dsCompany.Tables[0].Rows[0]["CompanyCountry"].ToString();
                    string CompanyZipCode = dsCompany.Tables[0].Rows[0]["CompanyZipCode"].ToString();
                    string CompanyPhone = dsCompany.Tables[0].Rows[0]["PhoneNo"].ToString();
                    string CompanyOwnerName = dsCompany.Tables[0].Rows[0]["CompanyOwnerName"].ToString();
                    string CompanyOwnerTitle = dsCompany.Tables[0].Rows[0]["CompanyOwnerTitle"].ToString();
                    string CompanyOwnerEmail = dsCompany.Tables[0].Rows[0]["CompanyOwnerEmail"].ToString();

                    string CompanyAddress = "@ " + DateTime.Now.Year.ToString() + " " + CompanyName + " Inc." + " " + CompanyCity + " " + CompanyState + ", " + CompanyCountry + ".";
                    string CompanyWebsite = dsCompany.Tables[0].Rows[0]["CompanyWebsite"].ToString();
                    string CompanyLogo = dsCompany.Tables[0].Rows[0]["LogoPath"].ToString();
                    string CompanySupportEmail = dsCompany.Tables[0].Rows[0]["SupportEmail"].ToString();
                    CompanyLogo = CompanyLogo.Replace("~/", "/");
                    CompanyLogo = CompanyLogo.Replace("../", "/");
                    CompanyLogo = CompanyWebsite + CompanyLogo;


                    strEmailBody = strEmailBody.Replace("#CompanyLogo#", CompanyLogo);
                    strEmailBody = strEmailBody.Replace("#CompanyJoinUrl#", CompanyWebsite);
                    strEmailBody = strEmailBody.Replace("#loginlink#", CompanyWebsite);

                    strEmailBody = strEmailBody.Replace("#CompanyName#", CompanyName);
                    strEmailBody = strEmailBody.Replace("#CompanyCity#", CompanyCity);
                    strEmailBody = strEmailBody.Replace("#CompanyState#", CompanyState);
                    strEmailBody = strEmailBody.Replace("#CompanyCountry#", CompanyCountry);
                    strEmailBody = strEmailBody.Replace("#CompanyZipCode#", CompanyZipCode);
                    strEmailBody = strEmailBody.Replace("#CompanyPhone#", CompanyPhone);
                    strEmailBody = strEmailBody.Replace("#CompanyWebsite#", CompanyWebsite);
                    strEmailBody = strEmailBody.Replace("#CompanySupportEmail#", CompanySupportEmail);
                    strEmailBody = strEmailBody.Replace("#CompanyOwnerEmail#", CompanyOwnerEmail);
                    strEmailBody = strEmailBody.Replace("#CompanyOwnerName#", CompanyOwnerName);
                    strEmailBody = strEmailBody.Replace("#CompanyOwnerTitle#", CompanyOwnerTitle);

                    strEmailBody = strEmailBody.Replace("#CompanyYear#", DateTime.Now.Year.ToString());
                }
                return strEmailBody;
            }
            catch (Exception)
            {
                return strEmailBody;
            }
        }

        public DataSet ChechActiveCompany()
        {
            try
            {
                SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                cn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = " SELECT *,CompantList_Address.CompanyAddress1+' '+CompantList_Address.CompanyAddress2 as CompanyAddress FROM CompantList_Address INNER JOIN CompanyList ON CompantList_Address.CompanyId = CompanyList.CompanyId INNER JOIN " +
                        " CompanyList_Logo ON CompanyList.CompanyId = CompanyList_Logo.CompanyId INNER JOIN " +
                         " CompanyList_NetworkCredential ON CompanyList.CompanyId = CompanyList_NetworkCredential.CompanyId  and (CompanyList.Isactive = 1) order by isprimary desc ";


                da.SelectCommand = cmd;
                cmd.Connection = cn;
                cmd.ExecuteNonQuery();
                da.Fill(ds);
                cn.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        public DataTable PatientLoginform(string PatientEmail, string PatientPassword)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientEmail", SqlDbType.VarChar);
                strParameter[0].Value = PatientEmail;

                strParameter[1] = new SqlParameter("@PatientPassword", SqlDbType.VarChar);
                strParameter[1].Value = PatientPassword;

                dtTable = clshelper.DataTable("Dental_PatientLogin", strParameter);
                return dtTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable CheckTempTable(string email)
        {
            try
            {
                SqlParameter[] Getuploadfiles = new SqlParameter[1];
                Getuploadfiles[0] = new SqlParameter("@email", SqlDbType.VarChar);
                Getuploadfiles[0].Value = email;
                return clshelper.DataTable("CheckEmailInPatientTemp", Getuploadfiles);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void GetActiveCompany()
        {
            if (HttpContext.Current.Session["CompanyId"] == null)
            {
                ds = ChechActiveCompany();
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    if (HttpContext.Current.Session["CompanyName"] == null)
                        HttpContext.Current.Session["CompanyName"] = ds.Tables[0].Rows[0]["CompanyName"].ToString();
                    if (HttpContext.Current.Session["CompanyAddress"] == null)
                        HttpContext.Current.Session["CompanyAddress"] = ds.Tables[0].Rows[0]["CompanyAddress"].ToString();
                    if (HttpContext.Current.Session["CompanyCity"] == null)
                        HttpContext.Current.Session["CompanyCity"] = ds.Tables[0].Rows[0]["CompanyCity"].ToString();
                    if (HttpContext.Current.Session["CompanyState"] == null)
                        HttpContext.Current.Session["CompanyState"] = ds.Tables[0].Rows[0]["CompanyState"].ToString();
                    if (HttpContext.Current.Session["CompanyZipCode"] == null)
                        HttpContext.Current.Session["CompanyZipCode"] = ds.Tables[0].Rows[0]["CompanyZipCode"].ToString();
                    if (HttpContext.Current.Session["CompanyId"] == null)
                        HttpContext.Current.Session["CompanyId"] = ds.Tables[0].Rows[0]["CompanyId"];
                    if (HttpContext.Current.Session["LogoPath"] == null)
                        HttpContext.Current.Session["LogoPath"] = ds.Tables[0].Rows[0]["LogoPath"].ToString();
                    if (HttpContext.Current.Session["CompanyWebsite"] == null)
                        HttpContext.Current.Session["CompanyWebsite"] = ds.Tables[0].Rows[0]["CompanyWebsite"].ToString();
                    if (HttpContext.Current.Session["CompanySalesEmail"] == null)
                        HttpContext.Current.Session["CompanySalesEmail"] = ds.Tables[0].Rows[0]["SalesEmail"].ToString();
                    if (HttpContext.Current.Session["CompanySupportEmail"] == null)
                        HttpContext.Current.Session["CompanySupportEmail"] = ds.Tables[0].Rows[0]["SupportEmail"].ToString();
                    if (HttpContext.Current.Session["PhoneNo"] == null)
                        HttpContext.Current.Session["PhoneNo"] = ds.Tables[0].Rows[0]["PhoneNo"].ToString();
                    if (HttpContext.Current.Session["CompanyOwnerName"] == null)
                        HttpContext.Current.Session["CompanyOwnerName"] = ds.Tables[0].Rows[0]["CompanyOwnerName"].ToString();
                    if (HttpContext.Current.Session["CompanyOwnerTitle"] == null)
                        HttpContext.Current.Session["CompanyOwnerTitle"] = ds.Tables[0].Rows[0]["CompanyOwnerTitle"].ToString();
                    if (HttpContext.Current.Session["CompanyOwnerEmail"] == null)
                        HttpContext.Current.Session["CompanyOwnerEmail"] = ds.Tables[0].Rows[0]["CompanyOwnerEmail"].ToString();

                    if (HttpContext.Current.Session["CompanyFacebook"] == null)
                        HttpContext.Current.Session["CompanyFacebook"] = ds.Tables[0].Rows[0]["CompanyFacebook"].ToString();
                    if (HttpContext.Current.Session["CompanyBlog"] == null)
                        HttpContext.Current.Session["CompanyBlog"] = ds.Tables[0].Rows[0]["CompanyBlog"].ToString();
                    if (HttpContext.Current.Session["CompanyYoutube"] == null)
                        HttpContext.Current.Session["CompanyYoutube"] = ds.Tables[0].Rows[0]["CompanyYoutube"].ToString();
                    if (HttpContext.Current.Session["CompanyLinkedin"] == null)
                        HttpContext.Current.Session["CompanyLinkedin"] = ds.Tables[0].Rows[0]["CompanyLinkedin"].ToString();
                    if (HttpContext.Current.Session["CompanyTwitter"] == null)
                        HttpContext.Current.Session["CompanyTwitter"] = ds.Tables[0].Rows[0]["CompanyTwitter"].ToString();
                    if (HttpContext.Current.Session["CompanyGooglePlus"] == null)
                        HttpContext.Current.Session["CompanyGooglePlus"] = ds.Tables[0].Rows[0]["CompanyGooglePlus"].ToString();
                    if (HttpContext.Current.Session["CompanyFacebookLike"] == null)
                        HttpContext.Current.Session["CompanyFacebookLike"] = ds.Tables[0].Rows[0]["CompanyFacebookLike"].ToString();
                    if (HttpContext.Current.Session["CompanyAlexa"] == null)
                        HttpContext.Current.Session["CompanyAlexa"] = ds.Tables[0].Rows[0]["CompanyAlexa"].ToString();

                    if (HttpContext.Current.Session["CompanyPinterest"] == null)
                        HttpContext.Current.Session["CompanyPinterest"] = ds.Tables[0].Rows[0]["CompanyPinterest"].ToString();

                    if (HttpContext.Current.Session["CompanyVimeo"] == null)
                        HttpContext.Current.Session["CompanyVimeo"] = ds.Tables[0].Rows[0]["CompanyVimeo"].ToString();

                    if (HttpContext.Current.Session["CompanyGoogle"] == null)
                        HttpContext.Current.Session["CompanyGoogle"] = ds.Tables[0].Rows[0]["CompanyGoogle"].ToString();

                }
            }

        }


        #region Template Name: Patient Reset Password

        public void ResetPatientPassword(string PatientEmail, string PatientFirstName, string PatientLastName)
        {
            try
            {
                TripleDESCryptoHelper cryptoHelper123 = new TripleDESCryptoHelper();
                string encryptedEmail = cryptoHelper123.encryptText(PatientEmail);

                //MailAddress fromAddress = new MailAddress(ConfigurationManager.AppSettings.Get("SMTPLogin"));
                //smtpClient = new SmtpClient(ConfigurationManager.AppSettings.Get("SMTPServer"));
                

                //smtpClient.UseDefaultCredentials = false;
                //smtpClient.Credentials = new NetworkCredential(ConfigurationManager.AppSettings.Get("SMTPLogin"), ConfigurationManager.AppSettings.Get("SMTPPassword"));
                

                //NewMeggage.From = new MailAddress(ConfigurationManager.AppSettings.Get("SMTPLogin"));
                //NewMeggage.To.Clear();
                //NewMeggage.To.Add(PatientEmail);
                //NewMeggage.Bcc.Add(ConfigurationManager.AppSettings.Get("bccemail"));
                

                //NewMeggage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings.Get("SMTPLogin")));

                //string TempSubejct = GetTemplate("Patient Forgot Password", "Subject");
                //TempSubejct = TempSubejct.Replace("#Name#", PatientFirstName + ' ' + PatientLastName);


                //string strEmailBody = GetTemplate("Patient Reset Password", "Desc");
                //strEmailBody = strEmailBody.Replace("#Name#", PatientFirstName + ' ' + PatientLastName);
                //strEmailBody = strEmailBody.Replace("#ToMail#", encryptedEmail);


                ////Get Company Details, Replace Content
                //TempSubejct = EmailSubject(TempSubejct);
                //strEmailBody = EmailBodyContent(strEmailBody);

                //NewMeggage.Subject = TempSubejct;
                //NewMeggage.IsBodyHtml = true;
                //NewMeggage.Body = strEmailBody;

                ////Send Email
                //smtpClient.Send(NewMeggage);

                DataAccessLayer.Common.CustomeMailPropery objCustomeMailPropery = new DataAccessLayer.Common.CustomeMailPropery();
                //CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                // objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");
                var myURL = HttpContext.Current.Request.Url.AbsoluteUri;
                objCustomeMailPropery.ToMailAddress = new List<string> { PatientEmail };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                if(myURL.Contains("qa"))
                {
                    objCustomeMailPropery.MailTemplateName = "qa-patient-reset-password";
                }
                else
                {
                    objCustomeMailPropery.MailTemplateName = "patient-reset-password";
                }
                

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Name", PatientFirstName + ' ' + PatientLastName);
                dt.Rows.Add("ToMail", encryptedEmail);
                


                dt = GetEmailSubjectForTemplate(dt);
                dt =GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

               // SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery);
                
               DataAccessLayer.Common.SendCustomeMail.SendMail(DataAccessLayer.Common.Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch(Exception ex)
            {
                InsertErrorLog("patient-reset-password", ex.Message, ex.StackTrace);
            }
        }
        #endregion
        public void SendPatientPassword(string PatientEmail, string Password, string PatientFirstName, string PatientLastName)
        {

            try
            {
                TripleDESCryptoHelper cryptoHelper123 = new TripleDESCryptoHelper();
                string encryptedPassword = cryptoHelper123.encryptText(Password);

                //MailAddress fromAddress = new MailAddress(ConfigurationManager.AppSettings.Get("SMTPLogin"));
                //smtpClient = new SmtpClient(ConfigurationManager.AppSettings.Get("SMTPServer"));
                //smtpClient.UseDefaultCredentials = false;
                //smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings.Get("SMTPLogin"), ConfigurationManager.AppSettings.Get("SMTPPassword"));

                //NewMeggage.From = new MailAddress(ConfigurationManager.AppSettings.Get("SMTPLogin"));
                //NewMeggage.To.Clear();
                //NewMeggage.To.Add(PatientEmail);
                //NewMeggage.Bcc.Add(ConfigurationManager.AppSettings.Get("bccemail"));
                //NewMeggage.ReplyToList.Add(new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings.Get("SMTPLogin")));

                //string TempSubejct = GetTemplate("Patient Forgot Password", "Subject");
                //TempSubejct = TempSubejct.Replace("#Name#", PatientFirstName + ' ' + PatientLastName);


                //string strEmailBody = GetTemplate("Patient Forgot Password", "Desc");
                //strEmailBody = strEmailBody.Replace("#Name#", PatientFirstName + ' ' + PatientLastName);
                //strEmailBody = strEmailBody.Replace("#PatientEmail#", PatientEmail);
                //strEmailBody = strEmailBody.Replace("#PatientPassword#", Password);
                //strEmailBody = strEmailBody.Replace("#EncPassword#", encryptedPassword);


                ////Get Company Details, Replace Content
                //TempSubejct = EmailSubject(TempSubejct);
                //strEmailBody = EmailBodyContent(strEmailBody);

                //NewMeggage.Subject = TempSubejct;
                //NewMeggage.IsBodyHtml = true;
                //NewMeggage.Body = strEmailBody;

                ////Send Email
                //smtpClient.Send(NewMeggage);

                DataAccessLayer.Common.CustomeMailPropery objCustomeMailPropery = new DataAccessLayer.Common.CustomeMailPropery();

               
                //objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");

                objCustomeMailPropery.ToMailAddress = new List<string> { PatientEmail };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "patient-forgot-password";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Name", PatientFirstName + ' ' + PatientLastName);
                dt.Rows.Add("PatientEmail", PatientEmail);
                dt.Rows.Add("PatientPassword", Password);
                dt.Rows.Add("EncPassword", encryptedPassword);

                dt = GetEmailSubjectForTemplate(dt);
                dt = GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;
                DataAccessLayer.Common.SendCustomeMail.SendMail(DataAccessLayer.Common.Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {
                InsertErrorLog("patient-forgot-password", ex.Message, ex.StackTrace);
            }
        }

        //Sent Message Template to Doctor
        public void ToInviteMessageMail(int PatientId, int DoctorId, int ID)
        {
            try
            {
                string Password = "";
                DataTable dtDoctorDetails = new DataTable();
                DataTable dtpatientDetails = new DataTable();

                string FromField = "";//Paitnet Name fill
                string ToEmailAddress = string.Empty;
                string ToSecondaryEmailAddress = string.Empty;
                TripleDESCryptoHelper cryptoHelper123 = new TripleDESCryptoHelper();

                SqlParameter[] getcolleaguepatientsparams = new SqlParameter[1];
                getcolleaguepatientsparams[0] = new SqlParameter();
                getcolleaguepatientsparams[0].ParameterName = "@PatientId ";
                getcolleaguepatientsparams[0].Value = PatientId;
                dtpatientDetails = clshelper.DataTable("getPatientProfiles", getcolleaguepatientsparams);

                FromField = Convert.ToString(dtpatientDetails.Rows[0]["FirstName"]) + " " + Convert.ToString(dtpatientDetails.Rows[0]["LastName"]);
                string FromCity = Convert.ToString(dtpatientDetails.Rows[0]["City"]);
                string FromState = Convert.ToString(dtpatientDetails.Rows[0]["State"]);

                if (!string.IsNullOrEmpty(FromState))
                {
                    FromCity = FromCity + ",";
                }

                SqlParameter[] addTransferDoctorParams;
                addTransferDoctorParams = new SqlParameter[1];
                addTransferDoctorParams[0] = new SqlParameter("@UserId ", SqlDbType.Int);
                addTransferDoctorParams[0].Value = DoctorId;
                dtDoctorDetails = clshelper.DataTable("USP_GetDoctorProfileDetails_By_Id", addTransferDoctorParams);
                string FromUserName = string.Empty;
                string Email = string.Empty;
                string SecondaryEmail = string.Empty;
                if (dtDoctorDetails != null && dtDoctorDetails.Rows.Count > 0)
                {
                    FromUserName = Convert.ToString(dtDoctorDetails.Rows[0]["FullName"]);
                    Email = Convert.ToString(dtDoctorDetails.Rows[0]["Username"]);
                    SecondaryEmail = Convert.ToString(dtDoctorDetails.Rows[0]["SecondaryEmail"]);
                    Password = Convert.ToString((dtDoctorDetails.Rows[0]["Password"]));
                }
                
                ToEmailAddress = Email;
                ToSecondaryEmailAddress = SecondaryEmail;
                
              //  MailAddress fromAddress = new MailAddress(ToEmailAddress);
              //// smtpClient = new SmtpClient(ConfigurationManager.AppSettings.Get("SMTPServer"));
              //  smtpClient = new SmtpClient("smtp.gmail.com", 587);

                //  smtpClient.UseDefaultCredentials = false;
                // //smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings.Get("SMTPLogin"), ConfigurationManager.AppSettings.Get("SMTPPassword"));
                //  smtpClient.Credentials = new System.Net.NetworkCredential("bharat.variance@gmail.com", "variance12*");
                //  smtpClient.EnableSsl = true;


                //  NewMeggage.From = new MailAddress(ConfigurationManager.AppSettings.Get("SMTPLogin"));
                //  if (NewMeggage.To.Count > 0)
                //  {
                //      for (int i = 0; NewMeggage.To.Count > i; i++)
                //      {
                //          NewMeggage.To.RemoveAt(i);
                //      }
                //  }
                //  NewMeggage.To.Clear();
                //  NewMeggage.To.Add(ToEmailAddress);
                // // NewMeggage.Bcc.Add(ConfigurationManager.AppSettings.Get("bccemail"));
                //  NewMeggage.Bcc.Add("mahesh.variance11@gmail.com");

                string EncriptString = string.Empty; string StringToEncript = string.Empty;
                StringToEncript = ToEmailAddress + "||" + Password + "||" + Convert.ToString(ID) + "||" + "0";
                EncriptString = cryptoHelper123.encryptText(StringToEncript);

              //  string TempSubject = GetTemplate("Message Received Notification From Patient", "Subject");
              //  TempSubject = TempSubject.Replace("#Patient#", FromField);

              //  string strEmailBody = GetTemplate("Message Received Notification From Patient", "Desc");
              //  strEmailBody = strEmailBody.Replace("#ToField#", FromUserName);
              //  strEmailBody = strEmailBody.Replace("#FromField#", FromField);
              //  strEmailBody = strEmailBody.Replace("#ToMail#", ToEmailAddress);
              //  strEmailBody = strEmailBody.Replace("#Pass#", Password);

              //  strEmailBody = strEmailBody.Replace("#encriptedstring#", EncriptString);

              //  strEmailBody = strEmailBody.Replace("#City#", FromCity);
              //  strEmailBody = strEmailBody.Replace("#State#", FromState);
                



              //  //Get Company Details, Replace Content
              //  TempSubject = EmailSubject(TempSubject);
              //  strEmailBody = EmailBodyContent(strEmailBody);

              //  NewMeggage.Subject = TempSubject;
              //  NewMeggage.IsBodyHtml = true;
              //  NewMeggage.Body = strEmailBody;

              //  smtpClient.Send(NewMeggage);

                DataAccessLayer.Common.CustomeMailPropery objCustomeMailPropery = new DataAccessLayer.Common.CustomeMailPropery();
                // CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                //objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");

                objCustomeMailPropery.ToMailAddress = new List<string> { ToEmailAddress, ToSecondaryEmailAddress };                
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings["bccemail"] };
                objCustomeMailPropery.MailTemplateName = "message-received-notification-from-patient";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Patient", FromField);
                dt.Rows.Add("ToField", FromUserName);
                dt.Rows.Add("FromField", FromField);
                dt.Rows.Add("ToMail", ToEmailAddress);
                dt.Rows.Add("Pass", Password);
                dt.Rows.Add("encriptedstring", EncriptString);
                dt.Rows.Add("City", FromCity);
                dt.Rows.Add("State", FromState);
                
                dt = GetEmailSubjectForTemplate(dt);
                dt = GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;
                DataAccessLayer.Common.SendCustomeMail.SendMail(DataAccessLayer.Common.Mailtype.ManDrill, objCustomeMailPropery, 0);
                //SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery);
            }
            catch (Exception ex)
            {
                InsertErrorLog("message-received-notification-from-patient", ex.Message, ex.StackTrace);
            }
        }

        public void SignUpEMail(string Email, string password)
        {

            try
            {

                TripleDESCryptoHelper cryptoHelper = new TripleDESCryptoHelper();
                string encryptedPassword = cryptoHelper.encryptText(password);

                //MailAddress fromAddress = new MailAddress(Email);
                //smtpClient = new SmtpClient(ConfigurationManager.AppSettings.Get("SMTPServer"));
                //smtpClient.UseDefaultCredentials = false;
                //// smtpClient.EnableSsl = true;
                //smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings.Get("SMTPLogin"), ConfigurationManager.AppSettings.Get("SMTPPassword"));
                //NewMeggage.From = new MailAddress(ConfigurationManager.AppSettings.Get("SMTPLogin"));

                //if (NewMeggage.To.Count > 0)
                //{
                //    for (int i = 0; NewMeggage.To.Count > i; i++)
                //    {
                //        NewMeggage.To.RemoveAt(i);
                //    }
                //}
                //NewMeggage.To.Clear();
                //NewMeggage.To.Add(Email);
                //NewMeggage.Bcc.Add(ConfigurationManager.AppSettings.Get("bccemail"));


                //string TempSubject = GetTemplate("Patient SignUp Email", "Subject");
                //TempSubject = TempSubject.Replace("#Email#", Email);

                //string strEmailBody = GetTemplate("Patient SignUp Email", "Desc");
                //strEmailBody = strEmailBody.Replace("#Email#", Email);
                //strEmailBody = strEmailBody.Replace("#Password#", password);
                //strEmailBody = strEmailBody.Replace("#EncPassword#", encryptedPassword);
                ////Get Company Details, Replace Content
                //TempSubject = EmailSubject(TempSubject);
                //strEmailBody = EmailBodyContent(strEmailBody);

                //NewMeggage.Subject = TempSubject;
                //NewMeggage.IsBodyHtml = true;
                //NewMeggage.Body = strEmailBody;

                ////Send Email
                //smtpClient.Send(NewMeggage);

                DataAccessLayer.Common.CustomeMailPropery objCustomeMailPropery = new DataAccessLayer.Common.CustomeMailPropery();
               // CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                //objCustomeMailPropery.FromMailAddress = ConfigurationManager.AppSettings.Get("SMTPLogin");

                objCustomeMailPropery.ToMailAddress = new List<string> { Email };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "patient-signup-email";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("Email", Email);
                dt.Rows.Add("Password", password);
                dt.Rows.Add("EncPassword", encryptedPassword);
                dt.Rows.Add("websiteurl", ConfigurationManager.AppSettings.Get("websiteurl"));

                dt = GetEmailSubjectForTemplate(dt);
                dt = GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                //SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery);
                DataAccessLayer.Common.SendCustomeMail.SendMail(DataAccessLayer.Common.Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {
                InsertErrorLog("patient-signup-email", ex.Message, ex.StackTrace);
            }

        }

        public DataTable CheckPatientEmail(string PatientEmail)
        {
            try
            {
                SqlParameter[] patientid = new SqlParameter[1];
                patientid[0] = new SqlParameter("@PatientEmail", SqlDbType.VarChar);
                patientid[0].Value = PatientEmail;

                dt = clshelper.DataTable("USP_CheckPatientEmail", patientid);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetPatientPassword(string PatientEmail)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@PatientEmail", SqlDbType.VarChar);
                strParameter[0].Value = PatientEmail;

                dtTable = clshelper.DataTable("Dental_GetPatientPassword", strParameter);
                return dtTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdatePatientPasswordByLink(string Email, string Password)
        {
            SqlParameter[] strParam = new SqlParameter[] { new SqlParameter("@Email", Email), new SqlParameter("@password",Password) };

            return clshelper.ExecuteNonQuery("UpdatePatientPasseord", strParam);
            
        }

        public DataTable GetPatientDetailsByEmail(string Email)
        {
            SqlParameter[] strParam = new SqlParameter[] { new SqlParameter("@Email", Email)};
            return clshelper.DataTable("GetPatientDetailsByEmail", strParam);
        }

        public DataTable Dental_GetPatientPasswordTemp(string PatientEmail)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@PatientEmail", SqlDbType.VarChar);
                strParameter[0].Value = PatientEmail;

                dtTable = clshelper.DataTable("Dental_GetPatientPasswordTemp", strParameter);
                return dtTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddPatientToTemp(string Email, string Password, string FirstName, string LastName, string Address, string Street, string City, string State, string Country, string ZipCode, string Phone, string dob, string Note, int DoctorId)
        {
            try
            {
                bool Result = false;
                SqlParameter[] AddPatient = new SqlParameter[15];

                AddPatient[0] = new SqlParameter();
                AddPatient[0].ParameterName = "@Email";
                AddPatient[0].Value = Email;

                AddPatient[1] = new SqlParameter();
                AddPatient[1].ParameterName = "@Password";
                AddPatient[1].Value = Password;

                AddPatient[2] = new SqlParameter();
                AddPatient[2].ParameterName = "@FirstName";
                AddPatient[2].Value = FirstName;

                AddPatient[3] = new SqlParameter();
                AddPatient[3].ParameterName = "@LastName";
                AddPatient[3].Value = LastName;

                AddPatient[4] = new SqlParameter();
                AddPatient[4].ParameterName = "@Address";
                AddPatient[4].Value = Address;

                AddPatient[5] = new SqlParameter();
                AddPatient[5].ParameterName = "@Street";
                AddPatient[5].Value = Street;

                AddPatient[6] = new SqlParameter();
                AddPatient[6].ParameterName = "@City";
                AddPatient[6].Value = City;

                AddPatient[7] = new SqlParameter();
                AddPatient[7].ParameterName = "@State";
                AddPatient[7].Value = State;

                AddPatient[8] = new SqlParameter();
                AddPatient[8].ParameterName = "@Country";
                AddPatient[8].Value = Country;

                AddPatient[9] = new SqlParameter();
                AddPatient[9].ParameterName = "@ZipCode";
                AddPatient[9].Value = ZipCode;

                AddPatient[10] = new SqlParameter();
                AddPatient[10].ParameterName = "@Phone";
                AddPatient[10].Value = Phone;

                AddPatient[11] = new SqlParameter();
                AddPatient[11].ParameterName = "@DoctorId";
                AddPatient[11].Value = DoctorId;

                AddPatient[12] = new SqlParameter();
                AddPatient[12].ParameterName = "@dob";
                AddPatient[12].Value = dob;

                AddPatient[13] = new SqlParameter();
                AddPatient[13].ParameterName = "@ResultFlag";
                AddPatient[13].Size = 1000;
                AddPatient[13].Direction = ParameterDirection.Output;

                AddPatient[14] = new SqlParameter();
                AddPatient[14].ParameterName = "@Note";
                AddPatient[14].Value = Note;

                clshelper.ExecuteNonQuery("AddPatientToTemp", AddPatient);

                int flagvalue = Convert.ToInt32(AddPatient[13].Value);
                if (flagvalue == 1)
                {
                    Result = true;
                }
                else
                {
                    Result = false;
                }
                return Result;
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }

        }

        public List<SelectListItem> GetDistance()
        {
            List<SelectListItem> _Distance = new List<SelectListItem>();


            _Distance.Add(new SelectListItem { Text = "5", Value = "5" });
            _Distance.Add(new SelectListItem { Text = "10", Value = "10" });
            _Distance.Add(new SelectListItem { Text = "20", Value = "20" });
            _Distance.Add(new SelectListItem { Text = "30", Value = "30" });
            _Distance.Add(new SelectListItem { Text = "50", Value = "50" });
            _Distance.Add(new SelectListItem { Text = "100", Value = "100" });
            _Distance.Add(new SelectListItem { Text = "150", Value = "150" });
            _Distance.Add(new SelectListItem { Text = "200", Value = "200" });

            return _Distance;
        }

        public List<SelectListItem> GetGender()
        {
            List<SelectListItem> _Gender = new List<SelectListItem>();


            _Gender.Add(new SelectListItem { Text = "Select Gender", Value = "10" });
            _Gender.Add(new SelectListItem { Text = "Male", Value = "0" });
            _Gender.Add(new SelectListItem { Text = "Female", Value = "1" });
            return _Gender;
        }

        public List<SelectListItem> GetStayloggedMins()
        {
            List<SelectListItem> _StayloggedMins = new List<SelectListItem>();


            _StayloggedMins.Add(new SelectListItem { Text = "--Select--", Value = "20" });
            _StayloggedMins.Add(new SelectListItem { Text = "1 Hour", Value = "60" });
            _StayloggedMins.Add(new SelectListItem { Text = "2 Hour", Value = "120" });
            _StayloggedMins.Add(new SelectListItem { Text = "3 Hour", Value = "180" });
            _StayloggedMins.Add(new SelectListItem { Text = "4 Hour", Value = "240" });
            _StayloggedMins.Add(new SelectListItem { Text = "5 Hour", Value = "300" });
            _StayloggedMins.Add(new SelectListItem { Text = "6 Hour", Value = "360" });
            _StayloggedMins.Add(new SelectListItem { Text = "7 Hour", Value = "420" });
            _StayloggedMins.Add(new SelectListItem { Text = "8 Hour", Value = "480" });
            _StayloggedMins.Add(new SelectListItem { Text = "1 Day", Value = "1440" });
            _StayloggedMins.Add(new SelectListItem { Text = "1 Week", Value = "10080" });

            return _StayloggedMins;
        }

        public List<SelectListItem> GetStatus()
        {
            List<SelectListItem> _Status = new List<SelectListItem>();


            _Status.Add(new SelectListItem { Text = "Select Status", Value = "10" });
            _Status.Add(new SelectListItem { Text = "Single", Value = "Single" });
            _Status.Add(new SelectListItem { Text = "Married", Value = "Married" });
            _Status.Add(new SelectListItem { Text = "Separated", Value = "Separated" });
            _Status.Add(new SelectListItem { Text = "Divorced", Value = "Divorced" });
            _Status.Add(new SelectListItem { Text = "Widowed", Value = "Widowed" });
            _Status.Add(new SelectListItem { Text = "Minor", Value = "Minor" });
            return _Status;
        }

        public DataTable GetDentistListBySearch(int Index, int Size, string Keywords, string Address, string LastName, int Speciality, int Miles, string FirstName, string Title, string CompanyName, string School, string Email, string phone, string zipcode, string SpecialtityList, string City)
        {
            try
            {
                SqlParameter[] GetDentistParams = new SqlParameter[16];

                GetDentistParams[0] = new SqlParameter("@Address", SqlDbType.VarChar);
                GetDentistParams[0].Value = Address;
                GetDentistParams[1] = new SqlParameter("@LastName", SqlDbType.VarChar);
                GetDentistParams[1].Value = LastName;
                GetDentistParams[2] = new SqlParameter("@Speciality", SqlDbType.Int);
                GetDentistParams[2].Value = Speciality;
                GetDentistParams[3] = new SqlParameter("@Miles", SqlDbType.Int);
                GetDentistParams[3].Value = Miles;
                GetDentistParams[4] = new SqlParameter("@index", SqlDbType.Int);
                GetDentistParams[4].Value = Index;
                GetDentistParams[5] = new SqlParameter("@size", SqlDbType.Int);
                GetDentistParams[5].Value = Size;
                GetDentistParams[6] = new SqlParameter("@FirstName", SqlDbType.VarChar);
                GetDentistParams[6].Value = FirstName;
                GetDentistParams[7] = new SqlParameter("@Title", SqlDbType.VarChar);
                GetDentistParams[7].Value = Title;
                GetDentistParams[8] = new SqlParameter("@CompanyName", SqlDbType.VarChar);
                GetDentistParams[8].Value = CompanyName;
                GetDentistParams[9] = new SqlParameter("@School", SqlDbType.VarChar);
                GetDentistParams[9].Value = School;
                GetDentistParams[10] = new SqlParameter("@Email", SqlDbType.VarChar);
                GetDentistParams[10].Value = Email;
                GetDentistParams[11] = new SqlParameter("@phone", SqlDbType.VarChar);
                GetDentistParams[11].Value = phone;
                GetDentistParams[12] = new SqlParameter("@zipcode", SqlDbType.VarChar);
                GetDentistParams[12].Value = zipcode;
                GetDentistParams[13] = new SqlParameter("@Keywords", SqlDbType.VarChar);
                GetDentistParams[13].Value = Keywords;
                GetDentistParams[14] = new SqlParameter("@SpecialtityList", SqlDbType.VarChar);
                GetDentistParams[14].Value = SpecialtityList;
                GetDentistParams[15] = new SqlParameter("@City", SqlDbType.VarChar);
                GetDentistParams[15].Value = City;


                return clshelper.DataTable("GetDentistListBySearch", GetDentistParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable GetInviteTreatingDoctorList(int Index, int Size, string Keywords, string Address, string LastName, int Speciality, int Miles, string FirstName, string Title, string CompanyName, string School, string Email, string phone, string zipcode, string SpecialtityList, int PatientId,string SpecialtityName = null)
        {
            try
            {
                SqlParameter[] GetDentistParams = new SqlParameter[16];

                GetDentistParams[0] = new SqlParameter("@Address", SqlDbType.VarChar);
                GetDentistParams[0].Value = Address;
                GetDentistParams[1] = new SqlParameter("@LastName", SqlDbType.VarChar);
                GetDentistParams[1].Value = LastName;
                GetDentistParams[2] = new SqlParameter("@Speciality", SqlDbType.Int);
                GetDentistParams[2].Value = Speciality;
                GetDentistParams[3] = new SqlParameter("@Miles", SqlDbType.Int);
                GetDentistParams[3].Value = Miles;
                GetDentistParams[4] = new SqlParameter("@index", SqlDbType.Int);
                GetDentistParams[4].Value = Index;
                GetDentistParams[5] = new SqlParameter("@size", SqlDbType.Int);
                GetDentistParams[5].Value = Size;
                GetDentistParams[6] = new SqlParameter("@FirstName", SqlDbType.VarChar);
                GetDentistParams[6].Value = FirstName;
                GetDentistParams[7] = new SqlParameter("@Title", SqlDbType.VarChar);
                GetDentistParams[7].Value = Title;
                GetDentistParams[8] = new SqlParameter("@CompanyName", SqlDbType.VarChar);
                GetDentistParams[8].Value = CompanyName;
                GetDentistParams[9] = new SqlParameter("@School", SqlDbType.VarChar);
                GetDentistParams[9].Value = School;
                GetDentistParams[10] = new SqlParameter("@Email", SqlDbType.VarChar);
                GetDentistParams[10].Value = Email;
                GetDentistParams[11] = new SqlParameter("@phone", SqlDbType.VarChar);
                GetDentistParams[11].Value = phone;
                GetDentistParams[12] = new SqlParameter("@zipcode", SqlDbType.VarChar);
                GetDentistParams[12].Value = zipcode;
                GetDentistParams[13] = new SqlParameter("@Keywords", SqlDbType.VarChar);
                GetDentistParams[13].Value = Keywords;
                GetDentistParams[14] = new SqlParameter("@SpecialtityList", SqlDbType.VarChar);
                GetDentistParams[14].Value = SpecialtityList;
                GetDentistParams[15] = new SqlParameter("@PatientId", SqlDbType.Int);
                GetDentistParams[15].Value = PatientId;
                GetDentistParams[15] = new SqlParameter("@SpecialtityName", SqlDbType.VarChar);
                GetDentistParams[15].Value = SpecialtityName;

                return clshelper.DataTable("GetDentistListBySearch", GetDentistParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }


        public bool AddPatientMember(int PatientId, int UserId, int UserStatus, int PatientAlias, DateTime CreationDate, DateTime lastModified, string Comments)
        {
            try
            {
                string Query = "insert into patient_Member(PatientId,UserId,UserStatusId,PatientIdAlias,CreationDate,LastModifieddate,Comments)values('" + PatientId + "','" + UserId + "','" + UserStatus + "','" + PatientAlias + "',getdate(),getdate(),'" + Comments + "')";

                result = Convert.ToBoolean(SqlHelper.ExecuteNonQuery(obj_connection.Open(), CommandType.Text, Query));
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int AddPatient(string AssignedPatientId, string FirstName, string MiddelName, string LastName, int Gender, string DataOfBirth, string TreatmentStartDate, string Phone, string Email, string Password, string Address, string City, string State, string Zip, string Country, string Comments, string ProfileImage, int OwnerId, int OwnerType, int TreatmentPhase, int PatientStatus, string Address2, string GuarFirstName, string GuarLastName, string GuarPhone, string GuarEmail, bool IsGuest, string Notes)
        {
            try
            {
                SqlParameter[] addPatientParameters = new SqlParameter[29];

                addPatientParameters[0] = new SqlParameter();
                addPatientParameters[0].ParameterName = "@pAssignedPatientId";
                addPatientParameters[0].Value = AssignedPatientId;

                addPatientParameters[1] = new SqlParameter();
                addPatientParameters[1].ParameterName = "@pFirstName";
                addPatientParameters[1].Value = FirstName;

                addPatientParameters[2] = new SqlParameter();
                addPatientParameters[2].ParameterName = "@pLastName";
                addPatientParameters[2].Value = LastName;

                addPatientParameters[3] = new SqlParameter();
                addPatientParameters[3].ParameterName = "@pDateOfBirth";
                addPatientParameters[3].Value = DataOfBirth;

                addPatientParameters[4] = new SqlParameter();
                addPatientParameters[4].ParameterName = "@pGender";
                addPatientParameters[4].Value = Gender;

                addPatientParameters[5] = new SqlParameter();
                addPatientParameters[5].ParameterName = "@pTreatmentPhase";
                addPatientParameters[5].Value = TreatmentPhase;

                addPatientParameters[6] = new SqlParameter();
                addPatientParameters[6].ParameterName = "@pTreatmentStartDate";
                addPatientParameters[6].Value = TreatmentStartDate;

                addPatientParameters[7] = new SqlParameter();
                addPatientParameters[7].ParameterName = "@pOwnerId";
                addPatientParameters[7].Value = OwnerId;

                addPatientParameters[8] = new SqlParameter();
                addPatientParameters[8].ParameterName = "@pOwnerType";
                addPatientParameters[8].Value = OwnerType;

                addPatientParameters[9] = new SqlParameter();
                addPatientParameters[9].ParameterName = "@pPatientStatus";
                addPatientParameters[9].Value = PatientStatus;

                addPatientParameters[10] = new SqlParameter();
                addPatientParameters[10].ParameterName = "@oNewPatientId";
                addPatientParameters[10].Size = 1000;
                addPatientParameters[10].Value = 1;
                addPatientParameters[10].Direction = ParameterDirection.Output;

                addPatientParameters[11] = new SqlParameter();
                addPatientParameters[11].ParameterName = "@pAddress";
                addPatientParameters[11].Value = Address;

                addPatientParameters[12] = new SqlParameter();
                addPatientParameters[12].ParameterName = "@pCity";
                addPatientParameters[12].Value = City;

                addPatientParameters[13] = new SqlParameter();
                addPatientParameters[13].ParameterName = "@pState";
                addPatientParameters[13].Value = State;

                addPatientParameters[14] = new SqlParameter();
                addPatientParameters[14].ParameterName = "@pZip";
                addPatientParameters[14].Value = Zip;

                addPatientParameters[15] = new SqlParameter();
                addPatientParameters[15].ParameterName = "@pComments";
                addPatientParameters[15].Value = Comments;

                addPatientParameters[16] = new SqlParameter();
                addPatientParameters[16].ParameterName = "@pPhone";
                addPatientParameters[16].Value = Phone;

                addPatientParameters[17] = new SqlParameter();
                addPatientParameters[17].ParameterName = "@pEmail";
                addPatientParameters[17].Value = Email;

                addPatientParameters[18] = new SqlParameter();
                addPatientParameters[18].ParameterName = "@pProfileImage";
                addPatientParameters[18].Value = ProfileImage;

                addPatientParameters[19] = new SqlParameter();
                addPatientParameters[19].ParameterName = "@pAddress2";
                addPatientParameters[19].Value = Address2;

                addPatientParameters[20] = new SqlParameter();
                addPatientParameters[20].ParameterName = "@pGuarFirstName";
                addPatientParameters[20].Value = GuarFirstName;

                addPatientParameters[21] = new SqlParameter();
                addPatientParameters[21].ParameterName = "@pGuarLastName";
                addPatientParameters[21].Value = GuarLastName;

                addPatientParameters[22] = new SqlParameter();
                addPatientParameters[22].ParameterName = "@pGuarPhone";
                addPatientParameters[22].Value = GuarPhone;

                addPatientParameters[23] = new SqlParameter();
                addPatientParameters[23].ParameterName = "@pGuarEmail";
                addPatientParameters[23].Value = GuarEmail;

                addPatientParameters[24] = new SqlParameter();
                addPatientParameters[24].ParameterName = "@pMiddelName";
                addPatientParameters[24].Value = MiddelName;

                addPatientParameters[25] = new SqlParameter();
                addPatientParameters[25].ParameterName = "@pCountry";
                addPatientParameters[25].Value = Country;

                addPatientParameters[26] = new SqlParameter();
                addPatientParameters[26].ParameterName = "@IsGuest";
                addPatientParameters[26].Value = IsGuest;

                addPatientParameters[27] = new SqlParameter();
                addPatientParameters[27].ParameterName = "@pNotes";
                addPatientParameters[27].Value = Notes;

                addPatientParameters[28] = new SqlParameter();
                addPatientParameters[28].ParameterName = "@pPassword";
                addPatientParameters[28].Value = Password;

                clshelper.ExecuteNonQuery("AddPatientNew", addPatientParameters);
                return Convert.ToInt32(addPatientParameters[10].Value);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void PatientRequestReceived(int DoctorId, string PatientEmail, string PatientFirstName, string PatientLastName, string Phone, string Address, string Address2, string City, string State, string Country, string ZipCode)
        {
            try
            {
                DataTable dtPatientRequest;
                SqlParameter[] addTransferDoctorParams;
                addTransferDoctorParams = new SqlParameter[1];
                addTransferDoctorParams[0] = new SqlParameter("@UserId ", SqlDbType.Int);
                addTransferDoctorParams[0].Value = DoctorId;
                dtPatientRequest = clshelper.DataTable("getUserName", addTransferDoctorParams);

                string DoctorFullName = dtPatientRequest.Rows[0]["FullName"].ToString();
                string DoctorImageUrl = dtPatientRequest.Rows[0]["image"].ToString();
                string DoctorSpeciality = dtPatientRequest.Rows[0]["speciality"].ToString();
                string DoctorState = dtPatientRequest.Rows[0]["State"].ToString();
                string publicpath = "";
                publicpath = dtPatientRequest.Rows[0]["publicpath"].ToString();
                string DoctorCity = "";
                if (dtPatientRequest.Rows[0]["City"] != null)
                    DoctorCity = dtPatientRequest.Rows[0]["City"].ToString();
                string DoctorCountry = dtPatientRequest.Rows[0]["Country"].ToString();
                string DoctorEmail = dtPatientRequest.Rows[0]["UserEmail"].ToString();
                string Password = "";
                Password = Convert.ToString(cryptoHelper.decryptText(dtPatientRequest.Rows[0]["Password"].ToString()));

                //MailAddress fromAddress = new MailAddress(PatientEmail);
                //smtpClient = new SmtpClient(ConfigurationManager.AppSettings.Get("SMTPServer"));
                //smtpClient.UseDefaultCredentials = false;
                //smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings.Get("SMTPLogin"), ConfigurationManager.AppSettings.Get("SMTPPassword"));

                //NewMeggage.From = new MailAddress(PatientEmail);
                //NewMeggage.To.Clear();
                //NewMeggage.To.Add(DoctorEmail);
                //NewMeggage.Bcc.Add(ConfigurationManager.AppSettings.Get("bccemail"));
                //NewMeggage.ReplyToList.Add(new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings.Get("SMTPLogin")));
                //string TempSubejct = "";
                //string strEmailBody = "";
                ////Get Company Details, Replace Content


                //TempSubejct = GetTemplate("Patient Request Received", "Subject");
                //TempSubejct = TempSubejct.Replace("#UserName#", PatientFirstName + ' ' + PatientLastName);
                //TempSubejct = TempSubejct.Replace("#City#", City);



                //strEmailBody = GetTemplate("Patient Request Received", "Desc");
                //strEmailBody = strEmailBody.Replace("#UserName#", PatientFirstName + ' ' + PatientLastName);
                //strEmailBody = strEmailBody.Replace("#DoctorName#", DoctorFullName);
                //strEmailBody = strEmailBody.Replace("#Address#", Address);
                //strEmailBody = strEmailBody.Replace("#Address2#", Address2);
                //strEmailBody = strEmailBody.Replace("#City#", City);
                //strEmailBody = strEmailBody.Replace("#State#", State);
                //strEmailBody = strEmailBody.Replace("#Country#", Country);
                //strEmailBody = strEmailBody.Replace("#ZipCode#", ZipCode);
                //strEmailBody = strEmailBody.Replace("#Email#", PatientEmail);
                //strEmailBody = strEmailBody.Replace("#Phone#", Phone);
                //strEmailBody = strEmailBody.Replace("#publicpath#", publicpath);
                //strEmailBody = strEmailBody.Replace("#ToMail#", DoctorEmail);
                //strEmailBody = strEmailBody.Replace("#Pass#", Password);

                //TempSubejct = EmailSubject(TempSubejct);
                //strEmailBody = EmailBodyContent(strEmailBody);

                //NewMeggage.Subject = TempSubejct;
                //NewMeggage.IsBodyHtml = true;
                //NewMeggage.Body = strEmailBody;

                ////Send Email
                //smtpClient.Send(NewMeggage);

                DataAccessLayer.Common.CustomeMailPropery objCustomeMailPropery = new DataAccessLayer.Common.CustomeMailPropery();
                //CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                //objCustomeMailPropery.FromMailAddress = PatientEmail;

                objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "patient-request-received";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("UserName", PatientFirstName + ' ' + PatientLastName);
                dt.Rows.Add("City", City);
                dt.Rows.Add("DoctorName", DoctorFullName);
                dt.Rows.Add("Address", Address);
                dt.Rows.Add("Address2", Address2);
                dt.Rows.Add("State", State);
                dt.Rows.Add("Country", Country);
                dt.Rows.Add("ZipCode", ZipCode);
                dt.Rows.Add("Email", PatientEmail);
                dt.Rows.Add("Phone", Phone);
                dt.Rows.Add("publicpath", publicpath);
                dt.Rows.Add("ToMail", DoctorEmail);
                dt.Rows.Add("Pass", Password);

                dt = GetEmailSubjectForTemplate(dt);
                dt = GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

               // SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery);
                DataAccessLayer.Common.SendCustomeMail.SendMail(DataAccessLayer.Common.Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {
                InsertErrorLog("patient-request-received", ex.Message, ex.StackTrace);
            }
        }

        public void PatientAddedNotification(int DoctorId,int PatientId, string PatientEmail, string PatientFirstName, string PatientLastName, string Phone, string Address, string Address2, string City, string State, string Country, string ZipCode)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlParameter[] addTransferDoctorParams;
                addTransferDoctorParams = new SqlParameter[1];
                addTransferDoctorParams[0] = new SqlParameter("@UserId ", SqlDbType.Int);
                addTransferDoctorParams[0].Value = DoctorId;
                ds = clshelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", addTransferDoctorParams);

                string DoctorFullName = string.Empty, DoctorImageUrl = string.Empty, DoctorSpeciality = string.Empty, DoctorState = string.Empty, publicpath = string.Empty, DoctorCity = string.Empty, DoctorCountry = string.Empty, DoctorEmail = string.Empty, Password = string.Empty;

                if (ds.Tables[0].Rows.Count > 0)
                {

                    DoctorFullName = ds.Tables[0].Rows[0]["FullName"].ToString();
                    DoctorImageUrl = ds.Tables[0].Rows[0]["ImageName"].ToString();
                    publicpath = ds.Tables[0].Rows[0]["PublicProfileUrl"].ToString();
                    DoctorEmail = ds.Tables[0].Rows[0]["Username"].ToString();
                    Password = Convert.ToString(cryptoHelper.decryptText(ds.Tables[0].Rows[0]["Password"].ToString()));

                }

                if (ds.Tables[2].Rows.Count > 0)
                {
                    DoctorCity = ds.Tables[2].Rows[0]["City"].ToString();
                    DoctorCountry = ds.Tables[2].Rows[0]["Country"].ToString();
                    DoctorState = ds.Tables[2].Rows[0]["State"].ToString();
                }

                if (ds.Tables[7].Rows.Count > 0)
                {
                    DoctorSpeciality = ds.Tables[7].Rows[0]["Speciality"].ToString();
                }



                string encryptedPassword = cryptoHelper.encryptText(Password.ToString());
                string encryptedPatientId = cryptoHelper.encryptText(PatientId.ToString());
                //MailAddress fromAddress = new MailAddress(PatientEmail);
                //smtpClient = new SmtpClient(ConfigurationManager.AppSettings.Get("SMTPServer"));
                //smtpClient.UseDefaultCredentials = false;
                //smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings.Get("SMTPLogin"), ConfigurationManager.AppSettings.Get("SMTPPassword"));

                //NewMeggage.From = new MailAddress(PatientEmail);
                //NewMeggage.To.Clear();
                //NewMeggage.To.Add(DoctorEmail);
                //NewMeggage.Bcc.Add(ConfigurationManager.AppSettings.Get("bccemail"));
                //NewMeggage.ReplyToList.Add(new System.Net.Mail.MailAddress(ConfigurationManager.AppSettings.Get("SMTPLogin")));
                //string TempSubejct = "";
                //string strEmailBody = "";
                ////Get Company Details, Replace Content


                //TempSubejct = GetTemplate("Patient Added Notification", "Subject");
                //TempSubejct = TempSubejct.Replace("#UserName#", PatientFirstName + ' ' + PatientLastName);
                //TempSubejct = TempSubejct.Replace("#City#", City);



                //strEmailBody = GetTemplate("Patient Added Notification", "Desc");
                //strEmailBody = strEmailBody.Replace("#UserName#", PatientFirstName + ' ' + PatientLastName);
                //strEmailBody = strEmailBody.Replace("#DoctorName#", DoctorFullName);
                //strEmailBody = strEmailBody.Replace("#Address#", Address);
                //strEmailBody = strEmailBody.Replace("#Address2#", Address2);
                //strEmailBody = strEmailBody.Replace("#City#", City);
                //strEmailBody = strEmailBody.Replace("#State#", State);
                //strEmailBody = strEmailBody.Replace("#Country#", Country);
                //strEmailBody = strEmailBody.Replace("#ZipCode#", ZipCode);
                //strEmailBody = strEmailBody.Replace("#Email#", PatientEmail);
                //strEmailBody = strEmailBody.Replace("#Phone#", Phone);
                //strEmailBody = strEmailBody.Replace("#publicpath#", publicpath);
                //strEmailBody = strEmailBody.Replace("#ToMail#", DoctorEmail);
                //strEmailBody = strEmailBody.Replace("#Pass#", encryptedPassword);

                //TempSubejct = EmailSubject(TempSubejct);
                //strEmailBody = EmailBodyContent(strEmailBody);

                //NewMeggage.Subject = TempSubejct;
                //NewMeggage.IsBodyHtml = true;
                //NewMeggage.Body = strEmailBody;

                ////Send Email
                //smtpClient.Send(NewMeggage);

                DataAccessLayer.Common.CustomeMailPropery objCustomeMailPropery = new DataAccessLayer.Common.CustomeMailPropery();
                //CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                //objCustomeMailPropery.FromMailAddress = PatientEmail;

                objCustomeMailPropery.ToMailAddress = new List<string> { DoctorEmail };
                objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
                objCustomeMailPropery.MailTemplateName = "patient-added-notification";

                DataTable dt = new DataTable();

                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");

                dt.Rows.Add("UserName", PatientFirstName + ' ' + PatientLastName);
                dt.Rows.Add("City", City);
                dt.Rows.Add("DoctorName", DoctorFullName);
                dt.Rows.Add("Address", Address);
                dt.Rows.Add("Address2", Address2);
                dt.Rows.Add("State", State);
                dt.Rows.Add("Country", Country);
                dt.Rows.Add("ZipCode", ZipCode);
                dt.Rows.Add("Email", PatientEmail);
                dt.Rows.Add("Phone", Phone);
                dt.Rows.Add("publicpath", publicpath);
                dt.Rows.Add("ToMail", DoctorEmail);
                dt.Rows.Add("Pass", Password.ToString());//Password issue for RM-283
                dt.Rows.Add("PatientId",encryptedPatientId);

                dt = GetEmailSubjectForTemplate(dt);
                dt = GetEmailBodyForTemplate(dt);
                objCustomeMailPropery.MergeTags = dt;

                //SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery);
                DataAccessLayer.Common.SendCustomeMail.SendMail(DataAccessLayer.Common.Mailtype.ManDrill, objCustomeMailPropery, 0);
            }
            catch (Exception ex)
            {
                InsertErrorLog("patient-added-notification", ex.Message, ex.StackTrace);
            }
        }

        public DataTable GetPatientsDetails(int PatientId)
        {
            try
            {
                SqlParameter[] getcolleaguepatientsparams = new SqlParameter[1];
                getcolleaguepatientsparams[0] = new SqlParameter();
                getcolleaguepatientsparams[0].ParameterName = "@PatientId ";
                getcolleaguepatientsparams[0].Value = PatientId;
                DataTable PatientsDt = clshelper.DataTable("getPatientProfiles", getcolleaguepatientsparams);
                return PatientsDt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetPatientsDetailsByDoctor(int DocotrId)
        {
            try
            {
                SqlParameter[] getcolleaguepatientsparams = new SqlParameter[1];
                getcolleaguepatientsparams[0] = new SqlParameter();
                getcolleaguepatientsparams[0].ParameterName = "@DoctorId ";
                getcolleaguepatientsparams[0].Value = DocotrId;
                DataTable PatientsDt = clshelper.DataTable("GetPatientDetailsOfParticularDoctor", getcolleaguepatientsparams);
                return PatientsDt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetTratingdoctors(int PatientId, int index, int size)
        {
            try
            {
                SqlParameter[] GetPatientId = new SqlParameter[3];
                GetPatientId[0] = new SqlParameter("@patientId", SqlDbType.Int);
                GetPatientId[0].Value = PatientId;
                GetPatientId[1] = new SqlParameter("@index", SqlDbType.Int);
                GetPatientId[1].Value = index;
                GetPatientId[2] = new SqlParameter("@size", SqlDbType.Int);
                GetPatientId[2].Value = size;
                return clshelper.DataTable("ReferTreatingDoctorDetailsNew_mydentailfile", GetPatientId);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable GetMemberProfile(int DoctorId)
        {
            try
            {
                SqlParameter[] GetColleagueParams = new SqlParameter[1];

                GetColleagueParams[0] = new SqlParameter("@docid", SqlDbType.Int);
                GetColleagueParams[0].Value = DoctorId;

                return clshelper.DataTable("GetMemberProfile", GetColleagueParams);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable GetDentalSpeacilityTypes(int UserId)
        {

            try
            {
                SqlParameter[] Patparameters = new SqlParameter[1];
                Patparameters[0] = new SqlParameter();
                Patparameters[0].ParameterName = "@docid";
                Patparameters[0].Value = UserId;

                return clshelper.DataTable("USP_GetProfile_Member_Specialities", Patparameters);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable GetOffice_Address_PubProfile(int userid)
        {
            try
            {
                SqlParameter[] Patparameters = new SqlParameter[1];
                Patparameters[0] = new SqlParameter();
                Patparameters[0].ParameterName = "@docid";
                Patparameters[0].Value = userid;

                return clshelper.DataTable("GetOffice_Address_PubProfile", Patparameters);

            }
            catch (Exception ex)
            {
                throw ex;
            }


        }

        public DataTable GetMemberSummary(int userid)
        {

            try
            {
                string Query = "select UserId as Id,Description as Summary from Member where UserId='" + userid + "'";
                DataSet dssummary = SqlHelper.ExecuteDataset(obj_connection.Open(), CommandType.Text, Query);
                dtsummary = dssummary.Tables[0];
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dtsummary;
        }

        public DataTable GetWebsite(int index, int size, int MemberID)
        {
            try
            {
                SqlParameter[] SearchPatientDetails = new SqlParameter[3];
                SearchPatientDetails[0] = new SqlParameter("@index", SqlDbType.Int);
                SearchPatientDetails[0].Value = index;
                SearchPatientDetails[1] = new SqlParameter("@size", SqlDbType.Int);
                SearchPatientDetails[1].Value = size;
                SearchPatientDetails[2] = new SqlParameter("@MemberID", SqlDbType.Int);
                SearchPatientDetails[2].Value = MemberID;

                return clshelper.DataTable("GetAllWebsite", SearchPatientDetails);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetMemberEducation(int userid)
        {

            try
            {
                SqlParameter[] Patparameters = new SqlParameter[1];
                Patparameters[0] = new SqlParameter();
                Patparameters[0].ParameterName = "@UserId";
                Patparameters[0].Value = userid;

                return clshelper.DataTable("USP_GetEducationTrainingByUserId", Patparameters);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable GetMember_ProfessionalMembership(int userid)
        {

            try
            {
                SqlParameter[] Patparameters = new SqlParameter[1];
                Patparameters[0] = new SqlParameter();
                Patparameters[0].ParameterName = "@docid";
                Patparameters[0].Value = userid;

                return clshelper.DataTable("GetProfile_Member_Membership", Patparameters);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable GetOtherDoctors(int UserId, int index, int size, string start, string end, string name)
        {

            try
            {
                SqlParameter[] Patparameters = new SqlParameter[6];
                Patparameters[0] = new SqlParameter();
                Patparameters[0].ParameterName = "@UserId";
                Patparameters[0].Value = UserId;
                Patparameters[1] = new SqlParameter();
                Patparameters[1].ParameterName = "@index";
                Patparameters[1].Value = index;

                Patparameters[2] = new SqlParameter();
                Patparameters[2].ParameterName = "@size";
                Patparameters[2].Value = size;

                Patparameters[3] = new SqlParameter();
                Patparameters[3].ParameterName = "@StartDate";
                Patparameters[3].Value = start;

                Patparameters[4] = new SqlParameter();
                Patparameters[4].ParameterName = "@EndDate";
                Patparameters[4].Value = end;

                Patparameters[5] = new SqlParameter();
                Patparameters[5].ParameterName = "@firstname";
                Patparameters[5].Value = name;


                return clshelper.DataTable("GetOtherDoctors", Patparameters);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetPatientMontages(int patientid)
        {
            try
            {
                SqlParameter[] GetColleagueParams = new SqlParameter[1];

                GetColleagueParams[0] = new SqlParameter();
                GetColleagueParams[0].ParameterName = "@pPatientId";
                GetColleagueParams[0].Value = patientid;

                return clshelper.DataTable("getImageRecordsOfPatient", GetColleagueParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }



        }


        public DataTable GetPatientDocs(int PatientId)
        {
            try
            {
                SqlParameter[] getPatientDocs = new SqlParameter[1];
                getPatientDocs[0] = new SqlParameter("@pPatientId", SqlDbType.Int);
                getPatientDocs[0].Value = PatientId;
                dt = clshelper.DataTable("GetPatientDocs", getPatientDocs);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable getDocumentDetails_mydentalfile(int PatientId, int MontageId)
        {
            clsHelper obj_Clshelper = new clsHelper();
            SqlParameter[] getreferoptions = new SqlParameter[2];
            getreferoptions[0] = new SqlParameter("@pPatientId", SqlDbType.Int);
            getreferoptions[0].Value = PatientId;
            getreferoptions[1] = new SqlParameter("@pMontageId", SqlDbType.Int);
            getreferoptions[1].Value = MontageId;



            DataTable gr = obj_Clshelper.DataTable("getDocumentDetails_mydentalfile", getreferoptions);
            return gr;
        }

        public DataTable GetDetailByImageId(int ImageId)
        {
            clsHelper obj_Clshelper = new clsHelper();
            SqlParameter[] getreferoptions = new SqlParameter[1];
            getreferoptions[0] = new SqlParameter("@ImageId", SqlDbType.Int);
            getreferoptions[0].Value = ImageId;



            DataTable gr = obj_Clshelper.DataTable("GetImageById", getreferoptions);
            return gr;
        }

        public DataTable PatientHistory(int PatientId)
        {
            try
            {
                SqlParameter[] GetColleagueParams = new SqlParameter[1];

                GetColleagueParams[0] = new SqlParameter();
                GetColleagueParams[0].ParameterName = "@PatientId";
                GetColleagueParams[0].Value = PatientId;

                return clshelper.DataTable("GetPatientHistory", GetColleagueParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable GetOwnerId(int UserId)
        {

            try
            {
                SqlParameter[] GetOwner = new SqlParameter[1];

                GetOwner[0] = new SqlParameter();
                GetOwner[0].ParameterName = "@UserId";
                GetOwner[0].Value = UserId;

                return clshelper.DataTable("GetOwnerIdNew", GetOwner);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable getuploadimages(int Patientid, string viewstatvalue)
        {
            try
            {
                SqlParameter[] Getuploadimages = new SqlParameter[2];
                Getuploadimages[0] = new SqlParameter("@pPatientid", SqlDbType.Int);
                Getuploadimages[0].Value = Patientid;
                Getuploadimages[1] = new SqlParameter("@pGuid", SqlDbType.VarChar);
                Getuploadimages[1].Value = viewstatvalue;

                return clshelper.DataTable("GetUploadedImagesPatient", Getuploadimages);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable getuploadfiles(int Patientid, string viewstatvalue)
        {
            try
            {
                SqlParameter[] Getuploadfiles = new SqlParameter[2];
                Getuploadfiles[0] = new SqlParameter("@pPaitentId", SqlDbType.Int);
                Getuploadfiles[0].Value = Patientid;
                Getuploadfiles[1] = new SqlParameter("@pGuid", SqlDbType.VarChar);
                Getuploadfiles[1].Value = viewstatvalue;
                return clshelper.DataTable("GetUploadedFilesPatient", Getuploadfiles);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }








        public DataTable getImageRecordsIfPatients(int intPatientId)
        {
            try
            {
                SqlParameter[] getpatientImagesparams = new SqlParameter[1];
                getpatientImagesparams[0] = new SqlParameter();
                getpatientImagesparams[0].ParameterName = "@pPatientId";
                getpatientImagesparams[0].Value = intPatientId;
                dt = clshelper.DataTable("getImageRecordsOfPatient", getpatientImagesparams);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddMontage(string MontageName, int MonatageType, int PatientId, int OwnerId, int OwnerType)
        {
            try
            {
                SqlParameter[] addmontage = new SqlParameter[6];
                addmontage[0] = new SqlParameter();
                addmontage[0].ParameterName = "@pMontageName";
                addmontage[0].Value = MontageName;
                addmontage[1] = new SqlParameter();
                addmontage[1].ParameterName = "@pMontageType";
                addmontage[1].Value = MonatageType;
                addmontage[2] = new SqlParameter();
                addmontage[2].ParameterName = "@pPatientId";
                addmontage[2].Value = PatientId;
                addmontage[3] = new SqlParameter();
                addmontage[3].ParameterName = "@pOwnerId";
                addmontage[3].Value = OwnerId;
                addmontage[4] = new SqlParameter();
                addmontage[4].ParameterName = "@pOwnerType";
                addmontage[4].Value = OwnerType;
                addmontage[5] = new SqlParameter();
                addmontage[5].ParameterName = "@oMontageId";
                addmontage[5].Direction = ParameterDirection.Output;
                addmontage[5].Value = 1;
                result = clshelper.ExecuteNonQuery("addMontageNew", addmontage);

                x = Convert.ToInt32(addmontage[5].Value);
                return x;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddImagetoPatient(int PatientId, int TimePoint, string Name, int Height, int Width, int ImageFormat, int ImageType, string ImageFullPath, string RelativePath, int OwnerId, int OwnerType, int ImageStatus, string Uploadby)
        {
            try
            {
                SqlParameter[] addImagetoPatientParameters = new SqlParameter[14];

                addImagetoPatientParameters[0] = new SqlParameter("@pPatientId", SqlDbType.Int);

                addImagetoPatientParameters[0].Value = PatientId;

                addImagetoPatientParameters[1] = new SqlParameter("@pTimepoint", SqlDbType.Int);

                addImagetoPatientParameters[1].Value = TimePoint;

                addImagetoPatientParameters[2] = new SqlParameter("@pName", SqlDbType.VarChar);

                addImagetoPatientParameters[2].Value = Name;

                addImagetoPatientParameters[3] = new SqlParameter("@pHeight", SqlDbType.Int);

                addImagetoPatientParameters[3].Value = Height;

                addImagetoPatientParameters[4] = new SqlParameter("@pWidth", SqlDbType.Int);

                addImagetoPatientParameters[4].Value = Width;

                addImagetoPatientParameters[5] = new SqlParameter("@pImageFormat", SqlDbType.Int);

                addImagetoPatientParameters[5].Value = ImageFormat;

                addImagetoPatientParameters[6] = new SqlParameter("@pImageType", SqlDbType.Int);

                addImagetoPatientParameters[6].Value = ImageType;

                addImagetoPatientParameters[7] = new SqlParameter("@pFullPathToImage", SqlDbType.VarChar);

                addImagetoPatientParameters[7].Value = ImageFullPath;

                addImagetoPatientParameters[8] = new SqlParameter("@pRelativePathToImage", SqlDbType.VarChar);

                addImagetoPatientParameters[8].Value = RelativePath;

                addImagetoPatientParameters[9] = new SqlParameter("@pOwnerId", SqlDbType.Int);

                addImagetoPatientParameters[9].Value = OwnerId;

                addImagetoPatientParameters[10] = new SqlParameter("@pOwnerType", SqlDbType.Int);

                addImagetoPatientParameters[10].Value = OwnerType;

                addImagetoPatientParameters[11] = new SqlParameter("@pImageStatus", SqlDbType.Int);

                addImagetoPatientParameters[11].Value = ImageStatus;

                addImagetoPatientParameters[12] = new SqlParameter("@oNewImageId", SqlDbType.Int);

                addImagetoPatientParameters[12].Direction = ParameterDirection.Output;


                addImagetoPatientParameters[13] = new SqlParameter("@pUploadby", SqlDbType.VarChar);

                addImagetoPatientParameters[13].Value = Uploadby;

                clshelper.ExecuteNonQuery("AddImageToPatientNew", addImagetoPatientParameters);

                ImageId = Convert.ToInt32(addImagetoPatientParameters[12].Value);

                return ImageId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddImagetoMontage(int ImagId, int PositioninMontage, int MontageId)
        {
            try
            {
                SqlParameter[] addmontageImage = new SqlParameter[3];
                addmontageImage[0] = new SqlParameter();
                addmontageImage[0].ParameterName = "@pMontageId";
                addmontageImage[0].Value = MontageId;
                addmontageImage[1] = new SqlParameter();
                addmontageImage[1].ParameterName = "@pImageId";
                addmontageImage[1].Value = ImageId;
                addmontageImage[2] = new SqlParameter();
                addmontageImage[2].ParameterName = "@pPosition";
                addmontageImage[2].Value = PositioninMontage;
                result = clshelper.ExecuteNonQuery("addMontageImageNew", addmontageImage);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int AddDocumenttoMontage(int MontageId, string DocumentName, string uploadby)
        {
            try
            {
                string query = "insert into Montage_Document(MontageId,DocumentName,uploadby,creationdate,lastmodifieddate)values('" + MontageId + "','" + DocumentName + "','" + uploadby + "','" + System.DateTime.Now + "','" + System.DateTime.Now + "')";

                x = SqlHelper.ExecuteNonQuery(obj_connection.Open(), CommandType.Text, query);
                return x;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteImagesUploded(int PatientId)
        {
            try
            {
                SqlParameter[] deleteuploadimage = new SqlParameter[1];
                deleteuploadimage[0] = new SqlParameter("@pPatientId", SqlDbType.Int);
                deleteuploadimage[0].Value = PatientId;
                return clshelper.ExecuteNonQuery("DeleteUploadedImagePatient", deleteuploadimage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteFilesUploaded(int PatientId)
        {
            try
            {
                SqlParameter[] deleteuploadimage = new SqlParameter[1];
                deleteuploadimage[0] = new SqlParameter("@pPatientId", SqlDbType.Int);
                deleteuploadimage[0].Value = PatientId;
                return clshelper.ExecuteNonQuery("DeleteUploadedFilePatient", deleteuploadimage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable PatientForms(string Operation)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[0].Value = Operation;

                dtTable = clshelper.DataTable("Dental_PatientForms", strParameter);
                return dtTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable PatientFormsSent(int PatientId, string Operation)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[0].Value = Operation;
                strParameter[1] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[1].Value = PatientId;

                dtTable = clshelper.DataTable("Dental_PatientForms", strParameter);
                return dtTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //public void SendFormToPatient(int Id, string ToAddress, string PatientName, int PatientId)
        //{
        //    try
        //    {
        //        DataTable dtInviteDoctor;
        //        SqlParameter[] addStudyGroupParams;
        //        addStudyGroupParams = new SqlParameter[1];
        //        addStudyGroupParams[0] = new SqlParameter("@UserId ", SqlDbType.Int);
        //        addStudyGroupParams[0].Value = Id;
        //        string officeName = "";
        //        string City = "";
        //        dtInviteDoctor = clshelper.DataTable("getUserName", addStudyGroupParams);
        //        UserName = dtInviteDoctor.Rows[0]["FullName"].ToString();
        //        ImageUrl = dtInviteDoctor.Rows[0]["image"].ToString();
        //        Speciality = dtInviteDoctor.Rows[0]["speciality"].ToString();
        //        City = dtInviteDoctor.Rows[0]["City"].ToString();
        //        State = dtInviteDoctor.Rows[0]["State"].ToString();
        //        Country = dtInviteDoctor.Rows[0]["Country"].ToString();
        //        officeName = dtInviteDoctor.Rows[0]["OfficeName"].ToString();
        //        string Email = dtInviteDoctor.Rows[0]["UserEmail"].ToString();
        //        string Publicpath = "";
        //        string Patientpass = "";
        //        Publicpath = dtInviteDoctor.Rows[0]["publicpath"].ToString();
        //        DataTable dtpatient = new DataTable();
        //        dtpatient = GetPatientPassword(ToAddress);
        //        if (dtpatient != null && dtpatient.Rows.Count > 0)
        //        {
        //            Patientpass = dtpatient.Rows[0]["Password"].ToString();
        //        }
        //        TripleDESCryptoHelper cryptoHelper = new TripleDESCryptoHelper();
        //        string encryptedPassword = cryptoHelper.encryptText(Patientpass);

        //        //MailAddress fromAddress = new MailAddress(ToAddress);
        //        //smtpClient = new SmtpClient(ConfigurationManager.AppSettings.Get("SMTPServer"));
        //        //smtpClient.UseDefaultCredentials = false;
        //        //smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings.Get("SMTPLogin"), ConfigurationManager.AppSettings.Get("SMTPPassword"));

        //        //NewMeggage.From = new MailAddress(Email);
        //        //NewMeggage.To.Clear();
        //        //NewMeggage.To.Add(ToAddress);
        //        //NewMeggage.Bcc.Add(ConfigurationManager.AppSettings.Get("bccemail"));
        //        //NewMeggage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings.Get("SMTPLogin")));

        //        //string TempSubject = GetTemplate("Send Form To Patient", "Subject");
        //        //TempSubject = TempSubject.Replace("#Doctor#", UserName);

        //        //string strEmailBody = GetTemplate("Send Form To Patient", "Desc");
        //        //strEmailBody = strEmailBody.Replace("#PatientId#", Convert.ToString(PatientId));
        //        //strEmailBody = strEmailBody.Replace("#PatientName#", PatientName);
        //        //strEmailBody = strEmailBody.Replace("#UserName#", UserName);
        //        //strEmailBody = strEmailBody.Replace("#State#", State);
        //        //strEmailBody = strEmailBody.Replace("#City#", City);
        //        //strEmailBody = strEmailBody.Replace("#Country#", Country);
        //        //strEmailBody = strEmailBody.Replace("#OfficeName#", officeName);
        //        //strEmailBody = strEmailBody.Replace("#publicpath#", Publicpath);
        //        //strEmailBody = strEmailBody.Replace("#PatientEmail#", ToAddress);
        //        //strEmailBody = strEmailBody.Replace("#Password#", Patientpass);
        //        //strEmailBody = strEmailBody.Replace("#EncPassword#", encryptedPassword);
        //        ////Get Company Details, Replace Content
        //        //TempSubject = EmailSubject(TempSubject);
        //        //strEmailBody = EmailBodyContent(strEmailBody);

        //        //NewMeggage.Subject = TempSubject;
        //        //NewMeggage.IsBodyHtml = true;
        //        //NewMeggage.Body = strEmailBody;

        //        ////Send Email
        //        //smtpClient.Send(NewMeggage);

        //        DataAccessLayer.Common.CustomeMailPropery objCustomeMailPropery = new DataAccessLayer.Common.CustomeMailPropery();
        //       // CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
        //       // objCustomeMailPropery.FromMailAddress = Email;

        //        objCustomeMailPropery.ToMailAddress = new List<string> { ToAddress };
        //        objCustomeMailPropery.BCCMailAddress = new List<string> { ConfigurationManager.AppSettings.Get("bccemail") };
        //        objCustomeMailPropery.MailTemplateName = "send-form-to-patient";

        //        DataTable dt = new DataTable();

        //        dt.Columns.Add("FieldName");
        //        dt.Columns.Add("FieldValue");

        //        dt.Rows.Add("Doctor", UserName);
        //        dt.Rows.Add("PatientId", Convert.ToString(PatientId));
        //        dt.Rows.Add("PatientName", PatientName);
        //        dt.Rows.Add("UserName", UserName);

        //        dt.Rows.Add("State", State);
        //        dt.Rows.Add("City", City);
        //        dt.Rows.Add("Country", Country);
        //        dt.Rows.Add("OfficeName", officeName);
        //        dt.Rows.Add("publicpath", Publicpath);
        //        dt.Rows.Add("PatientEmail", ToAddress);
        //        dt.Rows.Add("Password", Patientpass);
        //        dt.Rows.Add("EncPassword", encryptedPassword);

        //        dt = GetEmailSubjectForTemplate(dt);
        //        dt = GetEmailBodyForTemplate(dt);
        //        objCustomeMailPropery.MergeTags = dt;

        //        //SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery);
        //        DataAccessLayer.Common.SendCustomeMail.SendMail(DataAccessLayer.Common.Mailtype.ManDrill, objCustomeMailPropery);
        //    }
        //    catch (Exception ex)
        //    {
        //        InsertErrorLog("send-form-to-patient", ex.Message, ex.StackTrace);
        //    }
        //}

        public bool InsertPatientForms(int PatientID, string Operation, int UserId, int FormId)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[4];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;
                strParameter[2] = new SqlParameter("@UserId", SqlDbType.Int);
                strParameter[2].Value = UserId;
                strParameter[3] = new SqlParameter("@FormId", SqlDbType.Int);
                strParameter[3].Value = FormId;

                result = clshelper.ExecuteNonQuery("Dental_PatientForms", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public DataTable GetAllPatientReferrals(int PatientId)
        {
            try
            {
                clsHelper obj_Clshelper = new clsHelper();
                SqlParameter[] getreferoptions = new SqlParameter[2];
                getreferoptions[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                getreferoptions[0].Value = PatientId;
                getreferoptions[1] = new SqlParameter("@ISdltbypatient", SqlDbType.Bit);
                getreferoptions[1].Value = false;
                dt = clshelper.DataTable("getreferdetails_list_mydentalfile", getreferoptions);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable getReferCardDetailsById(int referid)
        {
            try
            {
                SqlParameter[] getreferdetails = new SqlParameter[1];
                getreferdetails[0] = new SqlParameter("@ReferCardId", SqlDbType.Int);
                getreferdetails[0].Value = referid;
                dt = clshelper.DataTable("getrefercarddetails", getreferdetails);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetAllMessages(int PatientId, int patientmessageid, int pageindex, int pagesize)
        {
            try
            {
                clsHelper obj_Clshelper = new clsHelper();
                SqlParameter[] getreferoptions = new SqlParameter[4];
                getreferoptions[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                getreferoptions[0].Value = PatientId;
                getreferoptions[1] = new SqlParameter("@patientmessageid", SqlDbType.Int);
                getreferoptions[1].Value = patientmessageid;

                getreferoptions[2] = new SqlParameter("@index", SqlDbType.Int);
                getreferoptions[2].Value = pageindex;
                getreferoptions[3] = new SqlParameter("@size", SqlDbType.Int);
                getreferoptions[3].Value = pagesize;
                dt = clshelper.DataTable("MessagesfromDoctor", getreferoptions);

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }






        public DataTable PatientSentMsg(int PatientId, int pageindex, int pagesize)
        {
            try
            {
                clsHelper obj_Clshelper = new clsHelper();
                SqlParameter[] getreferoptions = new SqlParameter[4];
                getreferoptions[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                getreferoptions[0].Value = PatientId;
                getreferoptions[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                getreferoptions[1].Value = "PatientSentMsg";
                getreferoptions[2] = new SqlParameter("@index", SqlDbType.Int);
                getreferoptions[2].Value = pageindex;
                getreferoptions[3] = new SqlParameter("@size", SqlDbType.Int);
                getreferoptions[3].Value = pagesize;
                dt = clshelper.DataTable("InboxPatientMessagesForDoctor", getreferoptions);

                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }





        public string CheckImageNull(string objVal, string DefaultValue)
        {
            if (objVal != "" && objVal != null && objVal != "NULL")
            {
                return (System.Configuration.ConfigurationManager.AppSettings["DentistImagespath"]) + objVal;
            }

            else
            {
                return DefaultValue;
            }

        }




        public string CheckNull(string objVal, string DefaultValue)
        {
            if (objVal == "1900-01-01 00:00:00.000" || objVal == "undefined")
            {
                return null;
            }

            else if (objVal == null || objVal == "")
                return DefaultValue;

            return objVal;
        }

        public string Getpurename(string objVal, string DefaultValue)
        {
            if (objVal == "")
            {
                return null;
            }

            else if (objVal == null || objVal == "")
                return objVal.Substring(objVal.ToString().LastIndexOf("$") + 1);

            return objVal.Substring(objVal.ToString().LastIndexOf("$") + 1);
        }

        public string Getextension(string fileName)
        {
            string ext = string.Empty;
            Boolean hita = false;
            int i = fileName.Length - 1;
            char[] arr = fileName.ToCharArray();
            while (i > 0 & !hita)
            {
                if (arr[i] == '.') hita = true;
                else ext = arr[i] + ext;
                i = i - 1;
            }
            return ext;
        }

        public DateTime CheckDateNull(DateTime objVal, DateTime DefaultValue)
        {
            if (objVal == Convert.ToDateTime("1900-01-01 00:00:00.000"))
            {
                return Convert.ToDateTime("1900-01-01 00:00:00.000");
            }

            else if (objVal == null || Convert.ToString(objVal) == "")
                return DefaultValue;

            return objVal;
        }

        public DataTable GetUserDataById(int UserId)
        {

            try
            {

                SqlParameter[] sqlparam = new SqlParameter[1];
                sqlparam[0] = new SqlParameter("@pUserId", SqlDbType.Int);
                sqlparam[0].Value = UserId;
                dt = clshelper.DataTable("getUserDataByIdNew", sqlparam);
                return dt;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public DataTable GetMedicalUpdate(int PatientID, string Operation)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientID", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;

                dtTable = clshelper.DataTable("Dental_MedicalUpdate", strParameter);
                return dtTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdatePatientBasicForm(int PatientID, string Operation, string FirstName, string LastName, string Gender, string DateOfBirth)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[6];
                strParameter[0] = new SqlParameter("@pPatientId", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@pOperation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;
                strParameter[2] = new SqlParameter("@pFirstName", SqlDbType.VarChar);
                strParameter[2].Value = FirstName;
                strParameter[3] = new SqlParameter("@pLastName", SqlDbType.VarChar);
                strParameter[3].Value = LastName;
                strParameter[4] = new SqlParameter("@pGender", SqlDbType.VarChar);
                strParameter[4].Value = Gender;
                strParameter[5] = new SqlParameter("@pDateOfBirth", SqlDbType.VarChar);
                strParameter[5].Value = DateOfBirth;

                result = clshelper.ExecuteNonQuery("Dental_BasicForm", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public string UpdatePatientGuardian(int PatientId, string GuarFirstName, string GuarLastName, string GuarPhone, string GuarEmail)
        {
            try
            {
                SqlParameter[] addPatientParameters = new SqlParameter[5];

                addPatientParameters[0] = new SqlParameter();
                addPatientParameters[0].ParameterName = "@GuarFirstName";
                addPatientParameters[0].Value = GuarFirstName;

                addPatientParameters[1] = new SqlParameter();
                addPatientParameters[1].ParameterName = "@GuarLastName";
                addPatientParameters[1].Value = GuarLastName;

                addPatientParameters[2] = new SqlParameter();
                addPatientParameters[2].ParameterName = "@GuarPhone";
                addPatientParameters[2].Value = GuarPhone;

                addPatientParameters[3] = new SqlParameter();
                addPatientParameters[3].ParameterName = "@GuarEmail";
                addPatientParameters[3].Value = GuarEmail;

                addPatientParameters[4] = new SqlParameter();
                addPatientParameters[4].ParameterName = "@PatientId";
                addPatientParameters[4].Value = PatientId;




                string result;
                return result = clshelper.ExecuteNonQuery("UpdatePatientGuardian", addPatientParameters).ToString();


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string UpdatePatientSocialMedia(int PatientId, string FacebookUrl, string TwitterUrl, string LinkedinUrl, string GoogleUrl, string BlogUrl)
        {
            try
            {
                SqlParameter[] addPatientParameters = new SqlParameter[6];

                addPatientParameters[0] = new SqlParameter();
                addPatientParameters[0].ParameterName = "@FacebookUrl";
                addPatientParameters[0].Value = FacebookUrl;

                addPatientParameters[1] = new SqlParameter();
                addPatientParameters[1].ParameterName = "@TwitterUrl";
                addPatientParameters[1].Value = TwitterUrl;

                addPatientParameters[2] = new SqlParameter();
                addPatientParameters[2].ParameterName = "@LinkedinUrl";
                addPatientParameters[2].Value = LinkedinUrl;

                addPatientParameters[3] = new SqlParameter();
                addPatientParameters[3].ParameterName = "@GoogleUrl";
                addPatientParameters[3].Value = GoogleUrl;

                addPatientParameters[4] = new SqlParameter();
                addPatientParameters[4].ParameterName = "@BlogUrl";
                addPatientParameters[4].Value = BlogUrl;


                addPatientParameters[5] = new SqlParameter();
                addPatientParameters[5].ParameterName = "@PatientId";
                addPatientParameters[5].Value = PatientId;




                string result;
                return result = clshelper.ExecuteNonQuery("UpdatePatientSocialMedia", addPatientParameters).ToString();


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetRegistration(int PatientID, string Operation)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientID", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;

                dtTable = clshelper.DataTable("Dental_Registration", strParameter);
                return dtTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateRegistrationForm(int PatientID, string Operation, DateTime DateofBirth, string Gender, string Status, string Address, string city, string state, string zip, string phone, string mobile, string fax, string Registration)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[13];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;
                strParameter[2] = new SqlParameter("@DateofBirth", SqlDbType.DateTime);
                strParameter[2].Value = DateofBirth;
                strParameter[3] = new SqlParameter("@Gender", SqlDbType.Int);
                strParameter[3].Value = Gender;
                strParameter[4] = new SqlParameter("@Status", SqlDbType.VarChar);
                strParameter[4].Value = Status;
                strParameter[5] = new SqlParameter("@Address", SqlDbType.VarChar);
                strParameter[5].Value = Address;
                strParameter[6] = new SqlParameter("@city", SqlDbType.VarChar);
                strParameter[6].Value = city;
                strParameter[7] = new SqlParameter("@state", SqlDbType.VarChar);
                strParameter[7].Value = state;
                strParameter[8] = new SqlParameter("@zip", SqlDbType.VarChar);
                strParameter[8].Value = zip;
                strParameter[9] = new SqlParameter("@phone", SqlDbType.VarChar);
                strParameter[9].Value = phone;
                strParameter[10] = new SqlParameter("@mobile", SqlDbType.VarChar);
                strParameter[10].Value = mobile;
                strParameter[11] = new SqlParameter("@fax", SqlDbType.VarChar);
                strParameter[11].Value = fax;
                strParameter[12] = new SqlParameter("@Registration", SqlDbType.VarChar);
                strParameter[12].Value = Registration;




                result = clshelper.ExecuteNonQuery("Dental_Registration_Form", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public DataTable GetChildHistory(int PatientID, string Operation)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientID", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;

                dtTable = clshelper.DataTable("Dental_ChildDentalMedicalHistory", strParameter);
                return dtTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateChildMedicalDentalForm(int PatientID, string Operation, string ChildMedicalDentalHistory)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;
                strParameter[2] = new SqlParameter("@ChildMedicalDentalHistory", SqlDbType.VarChar);
                strParameter[2].Value = ChildMedicalDentalHistory;
                result = clshelper.ExecuteNonQuery("Dental_Registration_Form", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public bool UpdateDentalHistory(int PatientID, string Operation, string DentalHistory)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;
                strParameter[2] = new SqlParameter("@DentalHistory", SqlDbType.VarChar);
                strParameter[2].Value = DentalHistory;
                result = clshelper.ExecuteNonQuery("Dental_Registration_Form", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public bool UpdateMedicalHistory(int PatientID, string Operation, string MedicalHistory)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;
                strParameter[2] = new SqlParameter("@MedicalHistory", SqlDbType.VarChar);
                strParameter[2].Value = MedicalHistory;
                result = clshelper.ExecuteNonQuery("Dental_Registration_Form", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public bool UpdateMedicalHistoryUpdate(int PatientID, string Operation, string MedicalUpdate)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;
                strParameter[2] = new SqlParameter("@MedicalUpdate", SqlDbType.VarChar);
                strParameter[2].Value = MedicalUpdate;
                result = clshelper.ExecuteNonQuery("Dental_Registration_Form", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public DataTable GetDentalHistory(int PatientID, string Operation)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientID", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;

                dtTable = clshelper.DataTable("Dental_DentalHistory", strParameter);
                return dtTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetMedicalHistory(int PatientID, string Operation)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientID", SqlDbType.Int);
                strParameter[0].Value = PatientID;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;

                dtTable = clshelper.DataTable("Dental_MedicalHistory", strParameter);
                return dtTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool EditPatientDetails(int PatientId, string FirstName, string LastName, string Gender, DateTime DateOfBirth, string Address, string Address2, string City, string State, string Phone)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[10];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = PatientId;
                strParameter[1] = new SqlParameter("@FirstName", SqlDbType.VarChar);
                strParameter[1].Value = FirstName;
                strParameter[2] = new SqlParameter("@LastName", SqlDbType.VarChar);
                strParameter[2].Value = LastName;
                strParameter[3] = new SqlParameter("@Gender", SqlDbType.Int);
                strParameter[3].Value = Convert.ToInt32(Gender);
                strParameter[4] = new SqlParameter("@DateOfBirth", SqlDbType.DateTime);
                strParameter[4].Value = DateOfBirth;
                strParameter[5] = new SqlParameter("@Address", SqlDbType.VarChar);
                strParameter[5].Value = Address;
                strParameter[6] = new SqlParameter("@Address2", SqlDbType.VarChar);
                strParameter[6].Value = Address2;
                strParameter[7] = new SqlParameter("@City", SqlDbType.VarChar);
                strParameter[7].Value = City;
                strParameter[8] = new SqlParameter("@State", SqlDbType.VarChar);
                strParameter[8].Value = State;
                strParameter[9] = new SqlParameter("@Phone", SqlDbType.VarChar);
                strParameter[9].Value = Phone;
                result = clshelper.ExecuteNonQuery("EditPatientDetails", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }


        #region Code For Send Referral Functionality
        public bool AddReferCard(int PatientId, int UserId, string RegardOption, string RequestingOption, string Comments, string RegOtherComments, string RequestOtherComments, string Status, ArrayList Patient_Referral_Tooth_Id, ArrayList Patient_Referral_Image_Id, ArrayList Patient_Referral_Form_Id)
        {

            try
            {

                SqlParameter[] addReferParameters = new SqlParameter[9];
                addReferParameters[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                addReferParameters[0].Value = PatientId;
                addReferParameters[1] = new SqlParameter("@UserId", SqlDbType.Int);
                addReferParameters[1].Value = UserId; // The patient id
                addReferParameters[2] = new SqlParameter("@RegardOption", SqlDbType.VarChar);
                addReferParameters[2].Value = RegardOption;	//The teeth map
                addReferParameters[3] = new SqlParameter("@RequestingOption", SqlDbType.VarChar);
                addReferParameters[3].Value = RequestingOption;	//The regarding option
                addReferParameters[4] = new SqlParameter("@Comments", SqlDbType.VarChar);
                addReferParameters[4].Value = Comments;	//The request option
                addReferParameters[5] = new SqlParameter("@RegOtherComments", SqlDbType.VarChar);
                addReferParameters[5].Value = RegOtherComments;
                addReferParameters[6] = new SqlParameter("@RequestOtherComments", SqlDbType.VarChar);
                addReferParameters[6].Value = RequestOtherComments;
                addReferParameters[7] = new SqlParameter("@Status", SqlDbType.VarChar);
                addReferParameters[7].Value = Status;
                addReferParameters[8] = new SqlParameter("@ReferralCardId", SqlDbType.Int);
                addReferParameters[8].Size = 1000;
                addReferParameters[8].Value = 1;
                addReferParameters[8].Direction = ParameterDirection.Output;




                result = clshelper.ExecuteNonQuery("spReferralByPatient", addReferParameters);
                int referidnew = Convert.ToInt32(addReferParameters[8].Value);
                if (referidnew > 0 && Patient_Referral_Tooth_Id.Count > 0)
                {
                    foreach (var PatientReferralTeeth_Id in Patient_Referral_Tooth_Id)
                    {
                        if (PatientReferralTeeth_Id != null && Convert.ToString(PatientReferralTeeth_Id) != "null" && Convert.ToString(PatientReferralTeeth_Id) != "")
                        {
                            Patient_Referral_Tooth(referidnew, Convert.ToInt32(PatientReferralTeeth_Id), 1, 1);
                        }
                    }

                }
                if (referidnew > 0 && Patient_Referral_Image_Id.Count > 0)
                {
                    foreach (var PatientReferralImage_Id in Patient_Referral_Image_Id)
                    {
                        if (PatientReferralImage_Id != null && Convert.ToString(PatientReferralImage_Id) != "null" && Convert.ToString(PatientReferralImage_Id) != "")
                        {

                            Patient_Referral_Image(referidnew, Convert.ToInt32(PatientReferralImage_Id));
                        }
                    }
                }

                if (referidnew > 0 && Patient_Referral_Form_Id.Count > 0)
                {
                    foreach (var PatientReferralForm_Id in Patient_Referral_Form_Id)
                    {
                        if (PatientReferralForm_Id != null && Convert.ToString(PatientReferralForm_Id) != "null" && Convert.ToString(PatientReferralForm_Id) != "")
                        {
                            Patient_Referral_Form(referidnew, Convert.ToString(PatientReferralForm_Id));
                        }
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public bool Patient_Referral_Tooth(int referidnew, int toothcode, int toothposition, int toothproblem)
        {
            try
            {
                SqlParameter[] addrefertoothdetails = new SqlParameter[4];
                addrefertoothdetails[0] = new SqlParameter("@ReferralCardId", SqlDbType.Int);
                addrefertoothdetails[0].Value = referidnew;
                addrefertoothdetails[1] = new SqlParameter("@ToothCode", SqlDbType.Int);
                addrefertoothdetails[1].Value = toothcode;
                addrefertoothdetails[2] = new SqlParameter("@tooth_position_id", SqlDbType.Int);
                addrefertoothdetails[2].Value = toothposition;
                addrefertoothdetails[3] = new SqlParameter("@tooth_problem_id", SqlDbType.Int);
                addrefertoothdetails[3].Value = toothproblem;
                result = clshelper.ExecuteNonQuery("spPatient_Referral_Tooth", addrefertoothdetails);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Patient_Referral_Image(int ReferralCardId, int Image_Refer_Id)
        {
            try
            {
                SqlParameter[] add_refer_image = new SqlParameter[2];
                add_refer_image[0] = new SqlParameter("@ReferralCardId", ReferralCardId);
                add_refer_image[0].Value = ReferralCardId;
                add_refer_image[1] = new SqlParameter("@Image_Refer_id1", Image_Refer_Id);
                add_refer_image[1].Value = Image_Refer_Id;
                result = clshelper.ExecuteNonQuery("spPatient_Referral_Image", add_refer_image);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool Patient_Referral_Form(int ReferralCardId, string FormId)
        {
            try
            {
                SqlParameter[] add_refer_image = new SqlParameter[2];
                add_refer_image[0] = new SqlParameter("@ReferralCardId", ReferralCardId);
                add_refer_image[0].Value = ReferralCardId;
                add_refer_image[1] = new SqlParameter("@FormId", FormId);
                add_refer_image[1].Value = FormId;
                result = clshelper.ExecuteNonQuery("spPatient_Referral_Form", add_refer_image);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        public string CreateRandomPassword(int passwordLength)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789";
            char[] chars = new char[passwordLength];
            Random rd = new Random();

            for (int i = 0; i < passwordLength; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }

        public DataTable getPatientNotes(int PatientId, int referid)
        {
            try
            {
                SqlParameter[] getpatientNotesParams = new SqlParameter[2];
                getpatientNotesParams[0] = new SqlParameter("@pPatientId", SqlDbType.Int);
                getpatientNotesParams[0].Value = PatientId;
                getpatientNotesParams[1] = new SqlParameter("@pReferralCardId", SqlDbType.Int);
                getpatientNotesParams[1].Value = referid;
                return clshelper.DataTable("getPatientNoteDetails", getpatientNotesParams);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetFullExternalUrl(string Url)
        {
            if (!Url.StartsWith("http") || !Url.StartsWith("https"))
            {
                Url = "http://" + Url;

            }

            return Url;
        }

        public bool DltRefMsg(int ReferralId)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@ReferralCardId", SqlDbType.Int);
                strParameter[0].Value = ReferralId;

                result = clshelper.ExecuteNonQuery("spDltRefMsg", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public bool EditPatientMessageReadStatus(int PatientmessageId, string Operation)
        {
            bool ResultTrue;
            try
            {
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientmessageId", SqlDbType.Int);
                strParameter[0].Value = PatientmessageId;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = Operation;

                ResultTrue = clshelper.ExecuteNonQuery("PatientMessagesReadStatusFrom", strParameter);
                return ResultTrue;




            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public int EditPatientMessageReadStatusForAPI(int PatientmessageId, string Operation)
        {
            try
            {
                SqlParameter[] addmontage = new SqlParameter[2];
                addmontage[0] = new SqlParameter();
                addmontage[0].ParameterName = "@PatientmessageId";
                addmontage[0].Value = PatientmessageId;
                addmontage[1] = new SqlParameter();
                addmontage[1].ParameterName = "@Operation";
                addmontage[1].Value = Operation;


                result = clshelper.ExecuteNonQuery("PatientMessagesReadStatusFrom", addmontage);

                x = 1;


                return x;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Insert_PatientMessages(string FromField, string ToField, string Subject, string Body, int DoctorId, int PatientId, bool IsSave, bool IsDoctor, bool IsPatient)
        {
            try
            {
                bool bval;
                int oNewId = 0;
                SqlParameter[] Officeinfo = new SqlParameter[10];
                Officeinfo[0] = new SqlParameter();
                Officeinfo[0] = new SqlParameter("@FromField", SqlDbType.VarChar);
                Officeinfo[0].Value = FromField;
                Officeinfo[1] = new SqlParameter("@ToField", SqlDbType.VarChar);
                Officeinfo[1].Value = ToField;
                Officeinfo[2] = new SqlParameter("@Subject", SqlDbType.VarChar);
                Officeinfo[2].Value = Subject;
                Officeinfo[3] = new SqlParameter("@Body", SqlDbType.Text);
                Officeinfo[3].Value = Body;
                Officeinfo[4] = new SqlParameter("@DoctorId", SqlDbType.Int);
                Officeinfo[4].Value = DoctorId;
                Officeinfo[5] = new SqlParameter("@PatientId", SqlDbType.Int);
                Officeinfo[5].Value = PatientId;
                Officeinfo[6] = new SqlParameter("@IsSave", SqlDbType.Bit);
                Officeinfo[6].Value = IsSave;

                Officeinfo[7] = new SqlParameter("@IsDoctor", SqlDbType.Bit);
                Officeinfo[7].Value = IsDoctor;

                Officeinfo[8] = new SqlParameter("@IsPatient", SqlDbType.Bit);
                Officeinfo[8].Value = IsPatient;

                Officeinfo[9] = new SqlParameter("@Newpatientmessageid", SqlDbType.Int);
                Officeinfo[9].Direction = ParameterDirection.Output;

                bval = clshelper.ExecuteNonQuery("USP_InsertPatientMessages", Officeinfo);
                oNewId = Convert.ToInt32(Officeinfo[9].Value);
                return oNewId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            // return bval;
        }

        public int DeleteimageofPatient(int ImageId, int MontageId)
        {
            try
            {
                string MontageQuery = "delete from Montage_Image where MontageId = '" + MontageId + "'and ImageId='" + ImageId + "'";
                x = SqlHelper.ExecuteNonQuery(obj_connection.Open(), CommandType.Text, MontageQuery);
                if (x > 0)
                {

                    string query = "delete from Image where ImageId='" + ImageId + "'";
                    x = SqlHelper.ExecuteNonQuery(obj_connection.Open(), CommandType.Text, query);
                }

                return x;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int DeletefileofPatient(string DocumentName)
        {
            try
            {

                string MontageQuery1 = "delete from Montage_Document where DocumentName = '" + DocumentName + "'";
                x = SqlHelper.ExecuteNonQuery(obj_connection.Open(), CommandType.Text, MontageQuery1);
                return x;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //Insert Error On Table
        public bool InsertErrorLog(string URL, string ErrorMessage, string StackTrace)
        {
            bool res;
            try
            {
                SqlParameter[] updatepwd = new SqlParameter[3];
                updatepwd[0] = new SqlParameter("@URL", SqlDbType.VarChar);
                updatepwd[0].Value = URL;
                updatepwd[1] = new SqlParameter("@ErrorMessage", SqlDbType.VarChar);
                updatepwd[1].Value = ErrorMessage;
                updatepwd[2] = new SqlParameter("@StackTrace", SqlDbType.VarChar);
                updatepwd[2].Value = StackTrace;

                res = clshelper.ExecuteNonQuery("InsertErrorLog", updatepwd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }

        public bool isNumeric(string str)
        {
            for (int i = 0; i < str.Length; i++)
            {
                if ((str[i] == '.') || (str[i] == ',')) continue;    //Decide what is valid, decimal point or decimal coma
                if ((str[i] < '0') || (str[i] > '9')) return false;
            }

            return true;
        }

        public int InsertImages(string filename, int userId, string viewstatvalue)
        {
            try
            {

                string MontageQuery1 = "INSERT into User_UploadedImages(Imagename,Category,PatientId,CreationDate,LastModifiedDate,Guid)values('" + filename + "','" + string.Empty + "','" + userId + "','" + DateTime.Today.ToString("MM/dd/yyyy") + "','" + DateTime.Today.ToString("MM/dd/yyyy") + "','" + viewstatvalue + "')";
                x = SqlHelper.ExecuteNonQuery(obj_connection.Open(), CommandType.Text, MontageQuery1);
                return x;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public int InsertFiles(string filename, int userId, string viewstatvalue)
        {
            try
            {

                string MontageQuery1 = "INSERT into User_UploadedFiles(FileName,PatientId,CreationDate,LastModifiedDate,Guid)values('" + filename + "','" + userId + "','" + DateTime.Today.ToString("MM/dd/yyyy") + "','" + DateTime.Today.ToString("MM/dd/yyyy") + "','" + viewstatvalue + "')";
                x = SqlHelper.ExecuteNonQuery(obj_connection.Open(), CommandType.Text, MontageQuery1);
                return x;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public string RemoveHTML(string input)
        {
            // remove comments
            input = Regex.Replace(input, "<!--(.|\\s)*?-->", string.Empty);
            // remove HTML
            return Regex.Replace(input, "<(.|\\s)*?>", string.Empty);
        }




        public bool dltsentmsgpatient(int @PatientmessageId)
        {
            bool ResultTrue;
            try
            {
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@PatientmessageId", SqlDbType.Int);
                strParameter[0].Value = PatientmessageId;

                ResultTrue = clshelper.ExecuteNonQuery("dltsentmsgpatient", strParameter);
                return ResultTrue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public bool UpdatePatientPassword(int PatientId, string NewPassword)
        {
            bool ResultTrue;
            try
            {
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@patientid", SqlDbType.Int);
                strParameter[0].Value = PatientId;
                strParameter[1] = new SqlParameter("@password", SqlDbType.VarChar);
                strParameter[1].Value = NewPassword;

                ResultTrue = clshelper.ExecuteNonQuery("UpdatePatientPassword", strParameter);
                return ResultTrue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetDescriptionofDocumentById(int PatientId, int MontageId)
        {

            try
            {
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@pPatientId", SqlDbType.Int);
                strParameter[0].Value = PatientId;
                strParameter[1] = new SqlParameter("@pMontageId", SqlDbType.Int);
                strParameter[1].Value = MontageId;

                return clshelper.DataTable("GetDescriptionofDocumentById", strParameter);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateDescriptionofDocument(int MontageId, string Description, int IsAPI)
        {
            bool ResultTrue;
            try
            {
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@MontageId", SqlDbType.Int);
                strParameter[0].Value = MontageId;
                strParameter[1] = new SqlParameter("@Description", SqlDbType.NVarChar);
                strParameter[1].Value = Description;
                strParameter[2] = new SqlParameter("@IsAPI", SqlDbType.Int);
                strParameter[2].Value = IsAPI;

                ResultTrue = clshelper.ExecuteNonQuery("UpdateDescriptionofDocument", strParameter);
                return ResultTrue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateDescriptionofImage(int ImageId, string Description, int IsAPI)
        {
            bool ResultTrue;
            try
            {
                SqlParameter[] strParameter = new SqlParameter[3];
                strParameter[0] = new SqlParameter("@ImageId", SqlDbType.Int);
                strParameter[0].Value = ImageId;
                strParameter[1] = new SqlParameter("@Description", SqlDbType.NVarChar);
                strParameter[1].Value = Description;
                strParameter[2] = new SqlParameter("@IsAPI", SqlDbType.Int);
                strParameter[2].Value = IsAPI;
                ResultTrue = clshelper.ExecuteNonQuery("UpdateDescriptionofImage", strParameter);
                return ResultTrue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateDescriptionofImageFromMyDental(int ImageId, DateTime CreationDate, DateTime LastModifiedDate, string Description, int IsAPI)
        {
            bool ResultTrue;
            try
            {
                SqlParameter[] strParameter = new SqlParameter[5];
                strParameter[0] = new SqlParameter("@ImageId", SqlDbType.Int);
                strParameter[0].Value = ImageId;
                strParameter[1] = new SqlParameter("@CreationDate", SqlDbType.DateTime);
                strParameter[1].Value = CreationDate;
                strParameter[2] = new SqlParameter("@LastModifiedDate", SqlDbType.DateTime);
                strParameter[2].Value = LastModifiedDate;
                strParameter[3] = new SqlParameter("@Description", SqlDbType.NVarChar);
                strParameter[3].Value = Description;
                strParameter[4] = new SqlParameter("@IsAPI", SqlDbType.Int);
                strParameter[4].Value = IsAPI;

                ResultTrue = clshelper.ExecuteNonQuery("UpdateDescriptionofImage", strParameter);
                return ResultTrue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateDescriptionofDocumentFromMyDental(int DocMontageId, DateTime CreationDate, DateTime LastModifiedDate, string Description, int IsAPI)
        {
            bool ResultTrue;
            try
            {
                SqlParameter[] strParameter = new SqlParameter[5];
                strParameter[0] = new SqlParameter("@MontageId", SqlDbType.Int);
                strParameter[0].Value = DocMontageId;
                strParameter[1] = new SqlParameter("@CreationDate", SqlDbType.DateTime);
                strParameter[1].Value = CreationDate;
                strParameter[2] = new SqlParameter("@LastModifiedDate", SqlDbType.DateTime);
                strParameter[2].Value = LastModifiedDate;
                strParameter[3] = new SqlParameter("@Description", SqlDbType.NVarChar);
                strParameter[3].Value = Description;
                strParameter[4] = new SqlParameter("@IsAPI", SqlDbType.Int);
                strParameter[4].Value = IsAPI;

                ResultTrue = clshelper.ExecuteNonQuery("UpdateDescriptionofDocument", strParameter);
                return ResultTrue;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Size GetThumbnailSize(Image original)
        {
            // Maximum size of any dimension.
            int maxPixels = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["Thumbimagediamention"]);

            // Width and height.
            int originalWidth = original.Width;
            int originalHeight = original.Height;

            // Compute best factor to scale entire image based on larger dimension.
            double factor = 1;
            if (originalHeight > maxPixels || originalWidth > maxPixels)
            {
                if (originalWidth > originalHeight)
                {
                    factor = (double)maxPixels / originalWidth;
                }
                else
                {
                    factor = (double)maxPixels / originalHeight;
                }
            }
            // Return thumbnail size.
            return new Size((int)(originalWidth * factor), (int)(originalHeight * factor));
        }

        public DataTable GetRecordsFromTempTables(int PatientId, string viewstatvalue)
        {
            try
            {
                SqlParameter[] Getuploadfiles = new SqlParameter[2];
                Getuploadfiles[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                Getuploadfiles[0].Value = PatientId;
                Getuploadfiles[1] = new SqlParameter("@Guid", SqlDbType.VarChar);
                Getuploadfiles[1].Value = viewstatvalue;
                return clshelper.DataTable("GetRecordsFromTempTablespatient", Getuploadfiles);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool InsertMessageAttachemnts(int MessageId, string @AttachemntName)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@MessageId", SqlDbType.Int);
                strParameter[0].Value = MessageId;
                strParameter[1] = new SqlParameter("@AttachemntName", SqlDbType.VarChar);
                strParameter[1].Value = AttachemntName;

                result = clshelper.ExecuteNonQuery("USP_InsertMessageAttachemnts", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public bool DeleteImagesUplodedByImageId(int ImageId)
        {
            try
            {
                SqlParameter[] deleteuploadimage = new SqlParameter[1];

                deleteuploadimage[0] = new SqlParameter("@ImageId", SqlDbType.Int);
                deleteuploadimage[0].Value = ImageId;
                return clshelper.ExecuteNonQuery("DeleteUploadedImageByImageIdPatient", deleteuploadimage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteFilesUplodedByFilesId(int UploadedFileId)
        {
            try
            {
                SqlParameter[] deleteuploadimage = new SqlParameter[1];

                deleteuploadimage[0] = new SqlParameter("@UploadedFileId", SqlDbType.Int);
                deleteuploadimage[0].Value = UploadedFileId;
                return clshelper.ExecuteNonQuery("DeleteUploadedFileByFileIdPatient", deleteuploadimage);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetMessageAttachmentByPatientmessageId(int PatientmessageId)
        {
            DataTable dtTable = new DataTable();
            //DataTable contactdt;
            try
            {

                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientmessageId", SqlDbType.Int);
                strParameter[0].Value = PatientmessageId;
                strParameter[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                strParameter[1].Value = "GetMessageAttachment";

                dtTable = clshelper.DataTable("PatientMessagesReadStatusFrom", strParameter);
                return dtTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SendContactMessageFromAPI(string Name, string Email, string Phone, string Comment)
        {
            bool Result = false;


            SmtpClient smtpClient = new SmtpClient();
            smtpClient = new SmtpClient(ConfigurationManager.AppSettings.Get("SMTPServer"));

            MailMessage message = new MailMessage();
            MailAddress fromAddress = new MailAddress(Email);


            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings.Get("SMTPLogin"), ConfigurationManager.AppSettings.Get("SMTPPassword"));

            message.From = new MailAddress(ConfigurationManager.AppSettings.Get("SMTPLogin"));
            NewMeggage.To.Clear();
            message.To.Add(TravisEmailId);
            message.ReplyToList.Add(fromAddress);
            message.Subject = "Request received at RecordLinc";
            message.Body = Email + System.Environment.NewLine + Comment + System.Environment.NewLine + "From:" + Name + System.Environment.NewLine + "PhoneNo: " + Phone;
            message.Priority = MailPriority.High;
            try
            {
                smtpClient.Send(message);
                Result = true;
            }
            catch (Exception)
            {
                return Result;
            }
            return Result;
        }

        public bool SendSuggestionOrBugFromAPI(string Fromemail, string Details)
        {
            bool Result = false;



            MailAddress fromAddress = new MailAddress(TravisEmailId);
            smtpClient = new SmtpClient(ConfigurationManager.AppSettings.Get("SMTPServer"));
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings.Get("SMTPLogin"), ConfigurationManager.AppSettings.Get("SMTPPassword"));


            NewMeggage.From = new MailAddress(Fromemail);
            NewMeggage.To.Clear();
            NewMeggage.To.Add(TravisEmailId);
            // NewMeggage.Bcc.Add(ConfigurationManager.AppSettings.Get("bccemail"));
            NewMeggage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings.Get("SMTPLogin")));
            NewMeggage.Body = "From:" + Fromemail + System.Environment.NewLine + Details;
            NewMeggage.Priority = MailPriority.High;




            try
            {
                //Send  Email
                smtpClient.Send(NewMeggage);
                smtpClient.Dispose();
                Result = true;
            }
            catch (Exception)
            {
                return Result;
            }
            return Result;
        }

        public bool TellAFriendFromAPI(string Fromemail, string Toemail, string Details, string FriendName)
        {
            bool Result = false;



            MailAddress fromAddress = new MailAddress(Toemail);
            smtpClient = new SmtpClient(ConfigurationManager.AppSettings.Get("SMTPServer"));
            smtpClient.UseDefaultCredentials = false;
            smtpClient.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings.Get("SMTPLogin"), ConfigurationManager.AppSettings.Get("SMTPPassword"));


            NewMeggage.From = new MailAddress(Fromemail);
            NewMeggage.To.Clear();
            NewMeggage.To.Add(Toemail);
            NewMeggage.Bcc.Add(TravisEmailId);
            NewMeggage.ReplyToList.Add(new MailAddress(ConfigurationManager.AppSettings.Get("SMTPLogin")));
            NewMeggage.Body = "From:" + Fromemail + System.Environment.NewLine + "Hello " + FriendName + System.Environment.NewLine + Details;
            NewMeggage.Priority = MailPriority.High;
            try
            {
                //Send  Email
                smtpClient.Send(NewMeggage);
                smtpClient.Dispose();
                Result = true;
            }
            catch (Exception)
            {
                return Result;
            }
            return Result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SourceFilePath">Path of ImageBank</param>
        /// <param name="SourceFileName">Image name in ImageBankFolder</param>
        /// <param name="DestinationFilePath">Path of Thumb folder inside ImageBank</param>
        public void GenerateThumbnailImage(string SourceFilePath, string SourceFileName, string DestinationFilePath)
        {
            try
            {
                Image image;
                if (SourceFilePath.EndsWith("\\"))
                    image = Image.FromFile(SourceFilePath + SourceFileName);
                else
                    image = Image.FromFile(SourceFilePath + @"\" + SourceFileName);

                // Compute thumbnail size.
                Size thumbnailSize = GetThumbnailSize(image);

                // Get thumbnail.
                Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                    thumbnailSize.Height, null, IntPtr.Zero);

                // Save thumbnail.            
                thumbnail.Save(DestinationFilePath + @"\" + SourceFileName);
            }
            catch (Exception ex)
            {

                bool status = InsertErrorLog("", ex.Message + ex.StackTrace, ex.Message + ex.StackTrace);
            }
        }


        public int DeletefileofPatientNew(string DocumentName, int MontageId)
        {
            try
            {

                string MontageQuery1 = "delete from Montage_Document where DocumentId = '" + MontageId + "'";
                x = SqlHelper.ExecuteNonQuery(obj_connection.Open(), CommandType.Text, MontageQuery1);

                return x;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int Insert_UploadedFiles(string filename, int userId, string viewstatvalue)
        {
            try
            {
                bool bval;
                int oNewId = 0;
                SqlParameter[] Officeinfo = new SqlParameter[6];
                Officeinfo[0] = new SqlParameter();
                Officeinfo[0] = new SqlParameter("@FileName", SqlDbType.VarChar);
                Officeinfo[0].Value = filename;
                Officeinfo[1] = new SqlParameter("@PatientId", SqlDbType.Int);
                Officeinfo[1].Value = userId;
                Officeinfo[2] = new SqlParameter("@CreationDate", SqlDbType.DateTime);
                Officeinfo[2].Value = DateTime.Now;
                Officeinfo[3] = new SqlParameter("@LastModifiedDate", SqlDbType.DateTime);
                Officeinfo[3].Value = DateTime.Now;
                Officeinfo[4] = new SqlParameter("@Guid", SqlDbType.VarChar);
                Officeinfo[4].Value = viewstatvalue;


                Officeinfo[5] = new SqlParameter("@Newrecordid", SqlDbType.Int);
                Officeinfo[5].Direction = ParameterDirection.Output;

                bval = clshelper.ExecuteNonQuery("InsertUploadedFiles", Officeinfo);
                oNewId = Convert.ToInt32(Officeinfo[5].Value);
                return oNewId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            // return bval;
        }



        public int Insert_UploadedImages(string filename, int userId, string viewstatvalue)
        {
            try
            {
                bool bval;
                int oNewId = 0;
                SqlParameter[] Officeinfo = new SqlParameter[6];
                Officeinfo[0] = new SqlParameter();
                Officeinfo[0] = new SqlParameter("@FileName", SqlDbType.VarChar);
                Officeinfo[0].Value = filename;
                Officeinfo[1] = new SqlParameter("@PatientId", SqlDbType.Int);
                Officeinfo[1].Value = userId;
                Officeinfo[2] = new SqlParameter("@CreationDate", SqlDbType.DateTime);
                Officeinfo[2].Value = DateTime.Now;
                Officeinfo[3] = new SqlParameter("@LastModifiedDate", SqlDbType.DateTime);
                Officeinfo[3].Value = DateTime.Now;
                Officeinfo[4] = new SqlParameter("@Guid", SqlDbType.VarChar);
                Officeinfo[4].Value = viewstatvalue;


                Officeinfo[5] = new SqlParameter("@Newrecordid", SqlDbType.Int);
                Officeinfo[5].Direction = ParameterDirection.Output;

                bval = clshelper.ExecuteNonQuery("InsertUploadedImages", Officeinfo);
                oNewId = Convert.ToInt32(Officeinfo[5].Value);
                return oNewId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            // return bval;
        }



        public DataTable Get_UploadedImages(int UploadImageId)
        {
            try
            {
                SqlParameter[] Getuploadimages = new SqlParameter[1];
                Getuploadimages[0] = new SqlParameter("@UploadId", SqlDbType.Int);
                Getuploadimages[0].Value = UploadImageId;


                return clshelper.DataTable("USP_Temp_Get_UploadedImagesById", Getuploadimages);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable Get_UploadedFiles(int UploadedFileId)
        {
            try
            {
                SqlParameter[] Getuploadimages = new SqlParameter[1];
                Getuploadimages[0] = new SqlParameter("@UploadId", SqlDbType.Int);
                Getuploadimages[0].Value = UploadedFileId;


                return clshelper.DataTable("USP_Temp_Get_UploadedFilesById", Getuploadimages);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool UpdatePatientEmailAddress(int PatientId, string EmailAddress)
        {
            try
            {
                SqlParameter[] UpdateUserEmailParams = new SqlParameter[2];

                UpdateUserEmailParams[0] = new SqlParameter("@pPatientId", SqlDbType.Int);
                UpdateUserEmailParams[0].Value = PatientId;
                UpdateUserEmailParams[1] = new SqlParameter("@pEmailAddress", SqlDbType.VarChar);
                UpdateUserEmailParams[1].Value = EmailAddress;
                bool result = clshelper.ExecuteNonQuery("UpdateEmailAddressPatient", UpdateUserEmailParams);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public bool UpdatePatientSecondaryEmailAddress(int PatientId, string EmailAddress)
        {
            try
            {
                SqlParameter[] UpdateUserEmailParams = new SqlParameter[2];

                UpdateUserEmailParams[0] = new SqlParameter("@pPatientId", SqlDbType.Int);
                UpdateUserEmailParams[0].Value = PatientId;
                UpdateUserEmailParams[1] = new SqlParameter("@pEmailAddress", SqlDbType.VarChar);
                UpdateUserEmailParams[1].Value = EmailAddress;
                bool result = clshelper.ExecuteNonQuery("UpdateSecondaryEmailAddressPatient", UpdateUserEmailParams);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public string UpdatestayloggedminsPwd(int PatientId, string stayloggedmins, string password)
        {
            try
            {
                SqlParameter[] addPatientParameters = new SqlParameter[3];

                addPatientParameters[0] = new SqlParameter();
                addPatientParameters[0].ParameterName = "@stayloggedmins";
                addPatientParameters[0].Value = stayloggedmins;

                addPatientParameters[1] = new SqlParameter();
                addPatientParameters[1].ParameterName = "@password";
                addPatientParameters[1].Value = password;

                addPatientParameters[2] = new SqlParameter();
                addPatientParameters[2].ParameterName = "@PatientId";
                addPatientParameters[2].Value = PatientId;


                string result;
                return result = clshelper.ExecuteNonQuery("UpdatestayloggedminsPwd", addPatientParameters).ToString();


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool RegistrationFormUpdateForAPI(int PatientId, string txtDob, string ddlgenderreg, string drpStatus, string txtResidenceStreet, string txtCity, string txtState, string txtZip, string txtResidenceTelephone, string txtWorkPhone, string txtFax, string txtResponsiblepartyFname, string txtResponsiblepartyLname, string txtResponsibleRelationship, string txtResponsibleAddress, string txtResponsibleCity, string txtResponsibleState, string txtResponsibleZipCode, string txtResponsibleDOB, string txtResponsibleContact, string txtWhommay, string txtemergencyname, string txtemergency, string MethodOfPayment, string txtEmployeeName1, string txtEmployeeDob1, string txtEmployerName1, string txtYearsEmployed1, string txtNameofInsurance1, string txtInsuranceAddress1, string txtInsuranceTelephone1, string txtEmployeeName2, string txtEmployeeDob2, string txtEmployerName2, string txtYearsEmployed2, string txtNameofInsurance2, string txtInsuranceAddress2, string txtInsuranceTelephone2)
        {
            bool status = false;
            if (txtDob == null || txtDob == "")
            {
                txtDob = "1900-01-01 00:00:00.000";
            }

            try
            {
                // Dental History Table 
                DataTable tblRegistration = new DataTable("Registration");


                tblRegistration.Columns.Add("txtResponsiblepartyFname");
                tblRegistration.Columns.Add("txtResponsiblepartyLname");
                tblRegistration.Columns.Add("txtResponsibleRelationship");
                tblRegistration.Columns.Add("txtResponsibleAddress");
                tblRegistration.Columns.Add("txtResponsibleCity");
                tblRegistration.Columns.Add("txtResponsibleState");
                tblRegistration.Columns.Add("txtResponsibleZipCode");
                tblRegistration.Columns.Add("txtResponsibleDOB");
                tblRegistration.Columns.Add("txtResponsibleContact");



                tblRegistration.Columns.Add("txtPatientPresentPosition");
                tblRegistration.Columns.Add("txtPatientHowLongHeld");





                tblRegistration.Columns.Add("txtParentPresentPosition");
                tblRegistration.Columns.Add("txtParentHowLongHeld");

                tblRegistration.Columns.Add("txtDriversLicense");
                tblRegistration.Columns.Add("chkMethodOfPayment");
                tblRegistration.Columns.Add("txtPurposeCall");
                tblRegistration.Columns.Add("txtOtherFamily");
                tblRegistration.Columns.Add("txtWhommay");
                tblRegistration.Columns.Add("txtSomeonetonotify");
                tblRegistration.Columns.Add("txtemergency");
                tblRegistration.Columns.Add("txtemergencyname");
                tblRegistration.Columns.Add("txtEmployeeName1");
                tblRegistration.Columns.Add("txtEmployeeDob1");
                tblRegistration.Columns.Add("txtEmployerName1");
                tblRegistration.Columns.Add("txtYearsEmployed1");
                tblRegistration.Columns.Add("txtNameofInsurance1");
                tblRegistration.Columns.Add("txtInsuranceAddress1");
                tblRegistration.Columns.Add("txtInsuranceTelephone1");
                tblRegistration.Columns.Add("txtProgramorpolicy1");
                tblRegistration.Columns.Add("txtSocialSecurity1");
                tblRegistration.Columns.Add("txtUnionLocal1");
                tblRegistration.Columns.Add("txtEmployeeName2");
                tblRegistration.Columns.Add("txtEmployeeDob2");
                tblRegistration.Columns.Add("txtEmployerName2");
                tblRegistration.Columns.Add("txtYearsEmployed2");
                tblRegistration.Columns.Add("txtNameofInsurance2");
                tblRegistration.Columns.Add("txtInsuranceAddress2");
                tblRegistration.Columns.Add("txtInsuranceTelephone2");
                tblRegistration.Columns.Add("txtProgramorpolicy2");
                tblRegistration.Columns.Add("txtSocialSecurity2");
                tblRegistration.Columns.Add("txtUnionLocal2");
                tblRegistration.Columns.Add("txtDigiSign");
                string chkMethodOfPayment = string.Empty;


                chkMethodOfPayment = MethodOfPayment;
                tblRegistration.Columns.Add("CreatedDate");
                tblRegistration.Columns.Add("ModifiedDate");


                tblRegistration.Rows.Add(txtResponsiblepartyFname, txtResponsiblepartyLname, txtResponsibleRelationship, txtResponsibleAddress, txtResponsibleCity, txtResponsibleState,
              txtResponsibleZipCode, txtResponsibleDOB, txtResponsibleContact, "", "", "", "", "", chkMethodOfPayment, "", "", txtWhommay, "", txtemergency, txtemergencyname, txtEmployeeName1,
              txtEmployeeDob1, txtEmployerName1, txtYearsEmployed1, txtNameofInsurance1, txtInsuranceAddress1, txtInsuranceTelephone1, "", "", "", txtEmployeeName2, txtEmployeeDob2,
              txtEmployerName2, txtYearsEmployed2, txtNameofInsurance2, txtInsuranceAddress2, txtInsuranceTelephone2, "", "", "", "");




                DataSet dsRegistration = new DataSet("Registration");
                dsRegistration.Tables.Add(tblRegistration);

                DataSet ds = dsRegistration.Copy();
                dsRegistration.Clear();

                string Registration = ds.GetXml();

                status = UpdateRegistrationForm(PatientId, "UpdatePatient", Convert.ToDateTime(txtDob), ddlgenderreg,
                   drpStatus, txtResidenceStreet, txtCity, txtState, txtZip,
                    txtResidenceTelephone, txtWorkPhone, txtFax, Registration);



            }
            catch (Exception ex)
            {
                throw ex;
            }

            return status;

        }


        #region Patient Device
        public int Insert_DeviceToken(int PatientId, string Token)
        {
            try
            {
                bool bval;
                int oNewId = 0;
                SqlParameter[] Officeinfo = new SqlParameter[3];
                Officeinfo[0] = new SqlParameter();

                Officeinfo[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                Officeinfo[0].Value = PatientId;

                Officeinfo[1] = new SqlParameter("@DeviceToken", SqlDbType.NVarChar);
                Officeinfo[1].Value = Token;


                Officeinfo[2] = new SqlParameter("@Newpatientdeviceid", SqlDbType.Int);
                Officeinfo[2].Direction = ParameterDirection.Output;

                bval = clshelper.ExecuteNonQuery("InsertPatientDeviceToken", Officeinfo);
                oNewId = Convert.ToInt32(Officeinfo[2].Value);
                return oNewId;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            // return bval;
        }

        public DataTable GetPatientDevice(int PatientId, string Token)
        {
            try
            {
                SqlParameter[] Getuploadimages = new SqlParameter[2];
                Getuploadimages[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                Getuploadimages[0].Value = PatientId;
                Getuploadimages[1] = new SqlParameter("@DeviceToken", SqlDbType.NVarChar);
                Getuploadimages[1].Value = Token;


                return clshelper.DataTable("GetPatientDeviceToken", Getuploadimages);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdatePatientDeviceToken(int PatientId, string Token)
        {
            try
            {
                SqlParameter[] UpdateUserEmailParams = new SqlParameter[2];

                UpdateUserEmailParams[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                UpdateUserEmailParams[0].Value = PatientId;
                UpdateUserEmailParams[1] = new SqlParameter("@DeviceToken", SqlDbType.NVarChar);
                UpdateUserEmailParams[1].Value = Token;
                bool result = clshelper.ExecuteNonQuery("UpdatePatientDeviceToken", UpdateUserEmailParams);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion


        public DataTable GetMemberTitle(int UserId)
        {
            try
            {
                SqlParameter[] getmembertitle = new SqlParameter[1];
                getmembertitle = new SqlParameter[1];
                getmembertitle[0] = new SqlParameter("@userid", SqlDbType.Int);
                getmembertitle[0].Value = UserId;
                dt = clshelper.DataTable("GetMemberTitle", getmembertitle);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public DataTable GetPatientAccountID(int PatientID)
        {
            try
            {
                DataSet dataset = new DataSet();
                DataTable dtTable;

                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@PatientID", SqlDbType.Int);
                strParameter[0].Value = PatientID;

                dtTable = clshelper.DataTable("GetPatientAccountID", strParameter);
                return dtTable;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable GetAccountIdByUserId(int ID, string Operation)
        {
            try
            {
                SqlParameter[] AddMemberFirstParams = new SqlParameter[2];
                AddMemberFirstParams[0] = new SqlParameter("@UserID", SqlDbType.Int);
                AddMemberFirstParams[0].Value = ID;
                AddMemberFirstParams[1] = new SqlParameter("@Operation", SqlDbType.VarChar);
                AddMemberFirstParams[1].Value = Operation;
                dt = clshelper.DataTable("GetAccountIdByUserId", AddMemberFirstParams);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }




        public DataSet GetPatientdetailsOfDoctordataset(int memberId, int index, int size, string patientid, string patientname, string gender, string city, string state, string zip, string Sort, string start, string end)
        {

            try
            {
                SqlParameter[] Patparameters = new SqlParameter[12];
                Patparameters[0] = new SqlParameter();
                Patparameters[0].ParameterName = "@pDoctorId";
                Patparameters[0].Value = memberId;

                Patparameters[1] = new SqlParameter();
                Patparameters[1].ParameterName = "@index";
                Patparameters[1].Value = index;

                Patparameters[2] = new SqlParameter();
                Patparameters[2].ParameterName = "@size";
                Patparameters[2].Value = size;


                Patparameters[3] = new SqlParameter();
                Patparameters[3].ParameterName = "@temppatientid";
                Patparameters[3].Value = patientid;

                Patparameters[4] = new SqlParameter();
                Patparameters[4].ParameterName = "@patientname";
                Patparameters[4].Value = patientname;

                Patparameters[5] = new SqlParameter();
                Patparameters[5].ParameterName = "@gender";
                Patparameters[5].Value = gender;

                Patparameters[6] = new SqlParameter();
                Patparameters[6].ParameterName = "@city";
                Patparameters[6].Value = city;

                Patparameters[7] = new SqlParameter();
                Patparameters[7].ParameterName = "@state";
                Patparameters[7].Value = state;

                Patparameters[8] = new SqlParameter();
                Patparameters[8].ParameterName = "@zipcode";
                Patparameters[8].Value = zip;

                Patparameters[9] = new SqlParameter();
                Patparameters[9].ParameterName = "@Sort";
                Patparameters[9].Value = Sort;

                Patparameters[10] = new SqlParameter();
                Patparameters[10].ParameterName = "@StartDate";
                Patparameters[10].Value = start;

                Patparameters[11] = new SqlParameter();
                Patparameters[11].ParameterName = "@EndDate";
                Patparameters[11].Value = end;


                ds = GetDatasetData("get_patient_profiles_for_login_user", Patparameters);
                return ds;
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }



        private SqlConnection objCon = new SqlConnection();
        private SqlCommand objCommand;
        private SqlDataAdapter objAdapter;

        public DataSet GetDatasetData(string spProcedureName, SqlParameter[] pParameter)
        {
            try
            {
                dt = null;

                objCon = obj_connection.Open();
                objCommand = new SqlCommand(spProcedureName, objCon);
                objCommand.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter p in pParameter)
                {
                    objCommand.Parameters.Add(p);
                }

                if (objCon.State == ConnectionState.Closed)
                    objCon.Open();

                objAdapter = new SqlDataAdapter(objCommand);
                objAdapter.Fill(ds);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                obj_connection.Close(objCon);

            }

            return ds;
        }










        public string UpdatePatientProfileImg(int PatientId, string ProfileImage)
        {
            try
            {
                SqlParameter[] addPatientParameters = new SqlParameter[2];

                addPatientParameters[0] = new SqlParameter();
                addPatientParameters[0].ParameterName = "@PatientId";
                addPatientParameters[0].Value = PatientId;

                addPatientParameters[1] = new SqlParameter();
                addPatientParameters[1].ParameterName = "@ProfileImage";
                addPatientParameters[1].Value = ProfileImage;




                string result;
                return result = clshelper.ExecuteNonQuery("UpdatePatientProfileImg", addPatientParameters).ToString();


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ReadFlagUpdate(int PatientId, int patientmessageid)
        {
            try
            {
                SqlParameter[] strParameter = new SqlParameter[2];
                strParameter[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                strParameter[0].Value = PatientId;
                strParameter[1] = new SqlParameter("@patientmessageid", SqlDbType.Int);
                strParameter[1].Value = patientmessageid;
                result = clshelper.ExecuteNonQuery("Dental_ReadFlagUpdate", strParameter);
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }


        public bool UpdatePatientStatus(int PatientId)
        {
            try
            {
                SqlParameter[] addmontageImage = new SqlParameter[1];
                addmontageImage[0] = new SqlParameter();
                addmontageImage[0].ParameterName = "@patientid";
                addmontageImage[0].Value = PatientId;

                result = clshelper.ExecuteNonQuery("UpdatePatientStatusFrom_mydentalside", addmontageImage);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public DataTable GetSocialMediaDetailOfDoctor(int Userid)
        {
            try
            {
                SqlParameter[] GetColleagueParams = new SqlParameter[1];

                GetColleagueParams[0] = new SqlParameter("@Userid", SqlDbType.Int);
                GetColleagueParams[0].Value = Userid;


                return clshelper.DataTable("USP_GetSocialMediaDetails_By_UserId", GetColleagueParams);

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public bool RemovePatientprifileImage(Int32 PatientId)
        {

            try
            {
                SqlParameter[] GetPatientId = new SqlParameter[1];
                GetPatientId[0] = new SqlParameter("@patientId", SqlDbType.Int);
                GetPatientId[0].Value = PatientId;
                return clshelper.ExecuteNonQuery("USP_RemovePatientProfileImage", GetPatientId);
            }
            catch (Exception)
            {

                throw;
            }

        }
    }

}
