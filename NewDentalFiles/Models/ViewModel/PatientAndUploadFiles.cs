﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DentalFiles.Models.ViewModel
{
    public class PatientAndUploadFiles
    {


        public List<PatientDetails> PatientDetails { get; set; }
        public List<PatientUploadedImages> PatientUploadedImages { get; set; }
        public List<PatientUploadedFiles> PatientUploadedFiles { get; set; }
        public List<Messages> Message { get; set; }
        public List<ViewReferral> ViewReferral { get; set; }
        public List<Notes> Notes { get; set; }
    }
}