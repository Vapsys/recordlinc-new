﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DentalFiles.Models.ViewModel
{
    public class PatientAndForms
    {
        public List<PatientDetails> PatientDetialList { get; set; }
        public List<Forms> Forms { get; set; }
        public List<FormStatus> FormStatus { get; set; }



    }
}