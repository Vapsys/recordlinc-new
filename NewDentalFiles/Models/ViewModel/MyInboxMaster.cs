﻿using PagedList;

namespace DentalFiles.Models.ViewModel
{
    public class MyInboxMaster
    {
        public IPagedList<ReferralDetails> ReferralDetails { get; set; }
        public IPagedList<Messages> Messages { get; set; }
        public IPagedList<Sent> PatientSentMsg { get; set; }
        public IPagedList<InviteTreatingDoctor> InviteTreatingDoctor { get; set; }
    }

    public class GetSeachDetailsOfInviteDoctor
    {
        public string keywords { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Title { get; set; }
        public string CompanyName { get; set; }
        public string School { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int Distance { get; set; }
        public string ZipCode { get; set; }
        public string SpecialtityList { get; set; }
        public string Partial { get; set; }
    }
}