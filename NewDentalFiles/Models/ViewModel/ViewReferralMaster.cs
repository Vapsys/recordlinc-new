﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DentalFiles.Models.ViewModel
{
    public class ViewReferralMaster
    {
        public List<ViewReferral> ViewReferral { get; set; }
        public List<PatientUploadedImages> PatientUploadedImages { get;set; }
        public List<Notes> Notes { get; set; }
        
    }


}