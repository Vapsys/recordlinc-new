﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DentalFiles.Models.ViewModel
{
    public class DentalFormsMaster
    {
        public List<BasicInfo> BasicInfo { get; set; }
        public List<RegistrationForm> RegistrationForm { get; set; }
        public List<ChildDentalMEDICALHISTORY> ChildDentalMEDICALHISTORY { get; set; }
        public List<DentalHistory> DentalHistory { get; set; }
        public List<MedicalHistory> MedicalHistory { get; set; }
        public List<MedicalHISTORYUpdate> MedicalHISTORYUpdate { get; set; }
        public List<Review> Review { get; set; }
        public List<PatientDetails> PatientDetialList { get; set; }
    }
}