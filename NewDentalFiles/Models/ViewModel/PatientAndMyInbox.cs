﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DentalFiles.Models.ViewModel
{
    public class PatientAndMyInbox
    {
        public IEnumerable<ReferralDetails> ReferralDetails { get; set; }

        public PatientAndMyInbox()
        {

        }
        public PatientAndMyInbox(IEnumerable<ReferralDetails> ListReferralDetails)
        {

            ReferralDetails = ListReferralDetails;
        }
    }
}