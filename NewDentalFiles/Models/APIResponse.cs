﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DentalFiles.Models
{
    public class APIResponse
    {
        public string MethodName { get; set; }
        public int MagicNumber { get; set; }
        public int ResultCode { get; set; }
        public bool IsSuccess { get; set; }
        public string StatusMessage { get; set; }
        public dynamic ResultDetail { get; set; }
    }
}