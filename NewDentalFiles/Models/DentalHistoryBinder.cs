﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DentalFiles.Models;

namespace DentalFilesNew.Models
{
    public class DentalHistoryBinder : DefaultModelBinder
    {
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType == typeof(DentalHistoryAPIUpdate))
            {
                HttpRequestBase request = controllerContext.HttpContext.Request;

                string title = request.Form.Get("Title");
                string day = request.Form.Get("Day");
                string month = request.Form.Get("Month");
                string year = request.Form.Get("Year");

                return new DentalHistoryAPIUpdate
                {
                    
                };

                //// call the default model binder this new binding context
            }
            else
            {
                return base.BindModel(controllerContext, bindingContext);
            }
        }
    }
}