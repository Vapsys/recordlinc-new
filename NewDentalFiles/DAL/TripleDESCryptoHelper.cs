#region FileHeader
	// TripleDESCryptoHelper:
	// =============
	// 
	// Functional Description:
	// =======================
	// Concrete class that provides encryption/decryption services by using the TripleDES
	// algorithm. This class uses a Triple DES provider with an MD5 hash key and the ECB
	// cipher mode. All encrypted keys are converted to Base64 to ensure a platfort-independent
	// view of the encrypted text. The same applies to decrypted text, it must have been normalized
	// to Base64; failure to comply might provide unwanted results in the decrypted text (probably,
	// the returned text will be composed of "garbage" content).
	// 
	// Parent class:
	// =============
	// CryptoHelper
	// 
	// Implemented interfaces:
	// =======================
	// 
	// 
	// Related classes:
	// ================
	// CryptoHelper
    // 
	// 
	// Author:
	// =======
	// Ronald Hernández Cisneros - rhernandez@avantica.net
	// 
	// Creation date:
	// ==============
	// Tuesday, March 30, 2004
	// 
	// Last modified date:
	// ===================
	// Tuesday, March 30, 2004
	// 
	// Copyright(c) 2004 RecordLinc. All Rights Reserved
#endregion

using System;
using System.Text;
using System.Security.Cryptography;



namespace Recordlinc.Core.Common
{	
	public sealed class TripleDESCryptoHelper : CryptoHelper
	{
		// OPTIONAL
		#region Constants
				
		#endregion

		// OPTIONAL
		#region Properties
				
		#endregion

		// OPTIONAL
		#region Variables
		
		#region Public
		
		#endregion

		#region Protected
		
		#endregion

		#region Private

		/// <summary>
		/// Triple DES provider object.
		/// </summary>
		private TripleDESCryptoServiceProvider tripleDESProvider;
		
		#endregion

		#region Internal
		
		#endregion

		#endregion
		
		#region Constructors
		
		/// <summary>
		/// Creates an instance of this encryption/decryption class with no associated key.
		/// The key will be generated from the default passphrase specified in the configuration
		/// files.
		/// </summary>
		public TripleDESCryptoHelper()
		{
            string passphrase = "a0HcR5$0plm";//ConfigurationManager.getSetting(ConfigurationConstants.CRYPTO_PASSPHRASE);

			if(passphrase != null)
			{
				MD5CryptoServiceProvider hashMD5 = new MD5CryptoServiceProvider();
				byte[] key = hashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(passphrase));
				base.Key = key;

				// Init triple DES provider class
				this.tripleDESProvider = new TripleDESCryptoServiceProvider();
				this.tripleDESProvider.Key = base.Key;
				this.tripleDESProvider.Mode = CipherMode.ECB;
			}
		}

		/// <summary>
		/// Creates an instance of this encryption/decryption class with the associated key
		/// </summary>
		public TripleDESCryptoHelper(byte[] key)
		{
			base.Key = key;

			// Init triple DES provider class
			this.tripleDESProvider = new TripleDESCryptoServiceProvider();
			this.tripleDESProvider.Key = base.Key;
			this.tripleDESProvider.Mode = CipherMode.ECB;
		}

		/// <summary>
		/// Creates an instance of this encryption/decryption class with the passphrase.
		/// The given passphrase will be used to build a valid key.
		/// </summary>
		public TripleDESCryptoHelper(string passphrase)
		{
			MD5CryptoServiceProvider hashMD5 = new MD5CryptoServiceProvider();
			byte[] key = hashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(passphrase));
			base.Key = key;

			// Init triple DES provider class
			this.tripleDESProvider = new TripleDESCryptoServiceProvider();
			this.tripleDESProvider.Key = base.Key;
			this.tripleDESProvider.Mode = CipherMode.ECB;
		}
		#endregion

		// OPTIONAL
		#region Methods

		#region EncryptionSection

		/// <summary>
		/// Encrypts a text string.
		/// </summary>
		/// <param name="plainText">A string representing the plain text</param>
		/// <returns>A normalized string representing the encrypted text, or null if the text could not be encrypted</returns>
		/// <exception cref="">Thrown if an exception issue occurs while encrypting</exception>
		public override string encryptText(string plainText)
		{
			string encryptedText = null;

			try
			{
				if(plainText != null)
				{
					// Get the byte array representation of the plain text.
					byte[] plainTextByteContent = ASCIIEncoding.ASCII.GetBytes(plainText);

					// Encrypt text and convert it to a Base64 representation
					if(this.tripleDESProvider != null)
					{
						ICryptoTransform cryptoTransform = this.tripleDESProvider.CreateEncryptor();
						byte[] encryptedBytes = cryptoTransform.TransformFinalBlock(plainTextByteContent,0,plainTextByteContent.Length);
						encryptedText = Convert.ToBase64String(encryptedBytes);
					}
					else
					{
						encryptedText = plainText;
					}
				}
			}
			catch(ApplicationException)
			{
                //string messageException = "An error has occurred while encrypting the specified text";
                //throw new CryptoException(messageException,appException);
			}

			return encryptedText;
		}

		#endregion

		#region DecryptionSection

		/// <summary>
		/// Decrypts the specified cipher text.
		/// </summary>
		/// <param name="encryptedText">A normalized string representing the cipher text</param>
		/// <returns>A string representing the plain text, or null if the text could not be decrypted</returns>
		/// <exception cref="">Thrown if an exception issue occurs while decrypting</exception>
		public override string decryptText(string encryptedText)
		{
			string decryptedText = null;

			try
			{
				if(encryptedText != null)
				{
					// Denormalize from Base64 representation
					byte[] encryptedTextBytes = Convert.FromBase64String(encryptedText);

					if(this.tripleDESProvider != null)
					{
						// Decrypt the text.
						ICryptoTransform cryptoTransform = this.tripleDESProvider.CreateDecryptor();
						byte[] decryptedBytes = cryptoTransform.TransformFinalBlock(encryptedTextBytes,0,encryptedTextBytes.Length);
						decryptedText = ASCIIEncoding.ASCII.GetString(decryptedBytes);
					}
					else
					{
						decryptedText = encryptedText;
					}
				}
			}
			catch(ApplicationException )
			{
                //string messageException = "An error has occurred while encrypting the specified text";
                //throw new CryptoException(messageException,appException);
			}

			return decryptedText;
		}
		
		#endregion

		#endregion
	}
}

#region HistoryLog

#region rhc - 3/30/2004
 /*
  * Initial version
  */
#endregion

#endregion
