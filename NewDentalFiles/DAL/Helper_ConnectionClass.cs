using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
/// <summary>
/// Summary description for Helper_ConnectionClass
/// </summary>
public class Helper_ConnectionClass
{
   private  SqlConnection objsqlConnection;

	public Helper_ConnectionClass()
	{
	   objsqlConnection=null;
	}
	
    public  SqlConnection  Open()
    {
            objsqlConnection = new SqlConnection();
            objsqlConnection.ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            objsqlConnection.Open();
            return objsqlConnection;
    }
    public void Close(SqlConnection con)
    {
        con.Close();
    }
}
