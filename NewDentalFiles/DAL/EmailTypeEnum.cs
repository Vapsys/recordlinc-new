#region FileHeader
	// EmailTypeEnum:
	// ================
	// 
	// Functional Description:
	// =======================
	// Enumeration container for the different types of e-mail addresses.
	// 
	// Parent class:
	// =============
	// System.Object
	// 
	// Implemented interfaces:
	// =======================
	// 
	// 
	// Related classes:
	// ================
	// 
	// 
	// Author:
	// =======
	// Ronald Hernández Cisneros - rhernandez@avantica.net
	// 
	// Creation date:
	// ==============
	// Friday, March 26, 2004
	// 
	// Last modified date:
	// ===================
	// Friday, March 26, 2004
	// 
	// Copyright(c) 2004 RecordLinc. All Rights Reserved
#endregion

using System;
using System.Collections;
using System.Collections.Generic;

namespace DentalFiles.Controllers
{
    public enum RegardingOption
    {
        Orthodontics = 1,
        Periodontist,
        OralSurgery,
        Prosthodontics,
        Radiology,
        GeneralDentistry,
        Endodontics,
        Other,
        LabWork

    }

    public enum MessageEnum
    {
        Inbox = 1,
        Sent,
    }
	public enum EmailTypeEnum
	{
		HOME,
		BUSINESS,
	}
   
    public enum OperationTypeEnum
    {
        REMOVE = 1,
        REVIEW
    }
    
    public enum PrimaryTeethEnum
    {
        
        A = 33,
        B,
        C,
        D,
        E,
        F,
        G,
        H,
        I,
        J,
        K,
        L,
        M,
        N,
        O,
        P,
        Q,
        R,
        S,
        T
    }

    public enum MessageTypeEnum
    {
        SIMPLE_MESSAGE,
        LETTER,
        REFERRAL_CARD
    }

    public enum AddressTypeEnum
    {
        HOME,
        BUSINESS,
    }

    public enum PhoneTypeEnum
    {
        HOME,
        MOBILE,
        BUSINESS,
        FAX
    }

    public enum DentalProfessionalEnum
    {
        ENDODONTIST,
        PROSTHODONTIST,
        ORAL_SURGEON,
        ORTHODONTIST,
        PEDODONTIST,
        GENERAL_DENTIST,
        PERIODONTIST,
        OTHER
    }

    public enum UserTypeEnum
    {
        TRIAL_USER,
        MEMBER_DENTAL_PROFESSIONAL,
        DENTAL_ASSISTANT,
        SALES_REPRESENTATIVE,
        OFFICE_MANAGER,
        SUPER_USER
    }

    public enum UserStatusEnum
    {
        ACTIVE,
        LOCK,
        DISABLE,
        TRIAL
    }


    public enum CreditCardTypeEnum
    {
        MASTERCARD,
        VISA,
        AMEX,
        DISCOVER
    }

    public enum RecordlincProductEnum
    {
        RECORDLINC_BASIC,
        RECORDLINC_PRO,
        RECORDLINC_SMARTREFER,
        RECORDLINC_TRUSTMAIL
    }

    public enum ContactTypeEnum
    {
        EMAIL,
        ADDRESS,
        PHONE
    }

    public enum InvoiceTypeEnum
    {
        FIRST_TIME,
        MONTHLY,
    }

    public enum ReferralToothPosition
    {
        LEFT = 1,
        RIGHT,
        TOP,
        BOTTOM,
        CENTER
    }

    public enum ReferralStatus
    {
        Refered=1,
        Accepted,
        Denied
    }
   
    public enum RequestOption
    {
        Checkfor = 1,
        CheckPeriodontolCindition,
        OralSurgery,
        DecayExtractions,
        Extractions,
        OrthodonticConsultation,
        OrthognathicSurgery,
        Implants,
        Pathology,
        Other
    }
}

#region HistoryLog

#region rhc - 3/26/2004
 /*
  * Initial version
  */
#endregion

#endregion
