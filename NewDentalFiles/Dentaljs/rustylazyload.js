﻿var IsScroll = false;
function LazyLoad(uniqueId) {

    var _uniqueId = uniqueId;

    var _containerId = "";
    var _ajaxLoadContainerId = "";
    var _ajaxActionUrl = "";
    var _parameters = {};

    this.init = function (option) {

        _containerId = option.containerId;
        _ajaxLoadContainerId = option.ajaxLoadContainerId;
        _ajaxActionUrl = option.ajaxActionUrl;
        _parameters = option.parameters;

        // Enable scroll event handler
        bindScrollHandler();

        // Load initial items
        load();
    };

    var bindScrollHandler = function () {
        
        $(window).scroll(function () {

            if ($(window).scrollTop() + $(window).height() > $(document).height() - 850) {
              
                var str = window.location.href;               
                var n = str.toLowerCase().indexOf('searchresult');
                var nomore = $("#dvnorecord").text();
                
                if (n > 0) {

                    var divid = document.getElementById("dvnorecord");
                    if (divid == null) {
                        IsScroll = true;
                        load();
                    }
                }

                else {

                }
            }
        });
    };

    var unbindScrollHandler = function () {
        $(window).unbind("scroll");
    };

    var load = function () {
        $.ajax({
            type: "POST",
            url: _ajaxActionUrl,
            data: _parameters,
            beforeSend: load_beforeSend,
            success: load_success,
            error: load_error
        });
    };

    var load_beforeSend = function () {
        // Disable scroll event handler
        unbindScrollHandler();

        // Show loading message
        $(_ajaxLoadContainerId).toggleClass("lazyload-hidden").html("<img src='/DentalImages/please_wait.gif'>");
    };
    var load_success = function (result) {
       
        // Delay a bit before displaying the result and re-enabling scroll event handler
        setTimeout(function () {
            // Display result with fade in effect
            if (result != null && result != "") {
                $(_containerId).append(result, { duration: 500 });
                // Add ui-first-child to the first child
                //   $(_containerId).find(">:first-child").removeClass("ui-first-child");
                //   $(_containerId).find(">:first-child").addClass("ui-first-child");
                // Remove ui-last-child from the old last child
                //    $(_containerId).find(">:nth-child(" + _parameters.fromRowNumber + ")").removeClass("ui-last-child");
                // Add ui-last-child to the new last child
                //   $(_containerId).find(">:last-child").addClass("ui-last-child");

                // Update fromRowNumber
               

                _parameters.fromRowNumber = $(_containerId).children().length + 1;
                var divid = document.getElementById("dvnorecord");
                if (divid !== null) {
                    if (IsScroll) {
                        var nomore = $('#dvnorecord').text();
                        if (nomore == "No More Record Found" || nomore == "No Record Found") {
                            $("#ulUsersLoad").hide();
                            return false;
                        }
                        $("#count").html(parseInt($("#totalCount").val()) + parseInt($("#loopCount").val()));
                        $("#totalCount").val(parseInt($("#totalCount").val()) + parseInt($("#loopCount").val()));
                        IsScroll = false;
                    } else {
                        $("#totalCount").val($("#loopCount").val());
                        $("#count").html($("#totalCount").val());
                    }
                } else {
                    if (IsScroll) {
                        $("#count").html(parseInt($("#totalCount").val()) + parseInt($("#loopCount").val()));
                        $("#totalCount").val(parseInt($("#totalCount").val()) + parseInt($("#loopCount").val()));
                        IsScroll = false;
                    } else {
                        $("#totalCount").val($("#loopCount").val());
                        $("#count").html($("#totalCount").val());
                    }
                }
            }

            if (_parameters.fromRowNumber == 0) {
                // Use loading container to display 'no item' message
                $(_ajaxLoadContainerId).html("There is no data to display");
            } else {
                // Remove loading message
                $(_ajaxLoadContainerId).toggleClass("lazyload-hidden").html("");
            }

            // Re-enable scroll handler
            bindScrollHandler();
        }, 500);

    };
    var load_error = function (result) {
        var message = result.responseText.substring(1, result.responseText.length - 2);
        $(_ajaxLoadContainerId).html("Error: " + message);
    };
}