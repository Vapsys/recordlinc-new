﻿function showErrorMsg(msg, delay) {
    delay = delay ? delay : 10000;
    $("#errormsg").show();
    $("#errormsg").html(msg);
   // $("html, body").animate({ scrollTop: $("#errormsg").offset().top }, "slow");
    if (delay >= 0) {
        setTimeout(function () {
            $("#errormsg").fadeOut(700);
            $("#errormsg").hide();
            $("#errormsg").html("");
        }, delay);

    }
}

function showSuccessMsg(msg, delay) {
    
    delay = delay ? delay : 5000;
    $("#successmsg").show();
    $("#successmsg").html(msg);
    setTimeout(function () {
        $("#successmsg").fadeOut(700);
        $("#successmsg").hide();
        $("#successmsg").html("");
    }, delay);
}

function getQueryStringParam(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        return uri + separator + key + "=" + value;
    }
}

$(document).ready(function () {

    $(".clsnumericOnly").bind("keypress", function (e) {
        var arr = [0, 8, 9, 16, 17, 20, 35, 36, 37, 38, 39, 40, 45, 46];
        // Allow numbers
        for (var i = 48; i <= 57; i++) {
            arr.push(i);
        }

        // Prevent default if not in array
        if (jQuery.inArray(e.which, arr) === -1) {
            e.preventDefault();
            return false;
        }
    });    $(".clsalphabetsOnly").on("keydown", function (event) {

        // Allow controls such as backspace
        var arr = [8, 9, 16, 17, 20, 35, 36, 37, 38, 39, 40, 45, 46, 32];
        // Allow letters
        for (var i = 65; i <= 90; i++) {
            arr.push(i);
        }
        // Prevent default if not in array
        if (jQuery.inArray(event.which, arr) === -1) {
            event.preventDefault();
        }
    });
});
