﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Optimization;
using DentalFilesNew.Models;
using DentalFiles.Models;
using DataAccessLayer.Common;

namespace DentalFiles
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        clsCommon OnjCommon = new clsCommon();
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

         
           
            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Index", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );


         
           

        }

        protected void Application_BeginRequest()
        {

            if (!Context.Request.IsSecureConnection)
            {
                //if (!Context.Request.Url.ToString().Contains("qa") && !Context.Request.Url.ToString().Contains("localhost") && !HttpContext.Current.Request.Url.ToString().ToLower().Contains("variance-8") && !HttpContext.Current.Request.Url.ToString().ToLower().Contains("http://192.168") && !HttpContext.Current.Request.Url.ToString().ToLower().Contains("http://162.42"))
                //{
                //    Response.Redirect(Context.Request.Url.ToString().Replace("http:", "https:"));
                //}
            }
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            
            RegisterRoutes(RouteTable.Routes);

            

            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //ModelBinders.Binders.Add(typeof(DentalHistoryInput), new DentalHistoryBinder());
        }
        void Application_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError().GetBaseException();
            bool status = OnjCommon.InsertErrorLog(Request.Url.ToString(), "MyDentalfiles.com: " +exc.Message, exc.StackTrace);
            Server.ClearError();
            if (exc is HttpException)
            {
                if (((HttpException)exc).GetHttpCode() == 404)
                {
                    return;
                }
            }
        }
    }
}