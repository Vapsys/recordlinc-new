﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using DentalFiles.Models;
using System.Data;
using System.Web.Script.Services;
using Newtonsoft.Json;
using JQueryDataTables.Models.Repository;
using DentalFiles.Models.ViewModel;
using DentalFiles.Controllers;
using System.Text;

namespace DentalFiles.Views.API
{
    /// <summary>
    /// Summary description for MyDentalService
    /// </summary>
    [WebService(Namespace = "http://www.recordlinc.com/mydentalfiles/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class MyDentalService : System.Web.Services.WebService
    {

        CommonDAL objCommonDAL = new CommonDAL();
        DataTable dt = new DataTable();
        APIResponse objResponse = new APIResponse { IsSuccess = true, StatusMessage = string.Empty };
        bool status = false;



        /// <summary>
        /// Method Name :AuthenticateUser
        /// Desciprtion :This Method Use for Authenticate Patient
        /// Request : UserName,Password
        /// Response : {4c20456f-e177-4104-b11e-63b936381a01}
        /// </summary>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]



        public string AuthenticateUser(string UserName, string Password)
        {
            string json = "";
            try
            {
                dt = objCommonDAL.PatientLoginform(UserName, Password);

                if (dt != null && dt.Rows.Count > 0)
                {
                    Session["PatientId"] = Convert.ToString(dt.Rows[0]["PatientId"]);
                    Session["PatientFullName"] = Convert.ToString(dt.Rows[0]["FirstName"] + " " + dt.Rows[0]["LastName"]);
                    Session["SessionCode"] = Guid.NewGuid().ToString();

                    dynamic dResult = new { SessionCode = Convert.ToString(Session["SessionCode"]) };


                    objResponse.ResultDetail = dResult;


                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }
                else
                {

                    objResponse.ResultCode = 400;
                    objResponse.StatusMessage = "Invalid username or password.";

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }

                return json;
            }
            catch (Exception ex)
            {
                objResponse.ResultCode = 400;
                objResponse.StatusMessage = "Server error occured - Please contact administrator.";
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }

        }


        /// <summary>
        /// Method Name :RegisterUser
        /// Desciprtion :This Method Use for Sign up new Patient
        /// Request : Email,Password,FirstName,Lastname,Phone
        /// Response : {Thank you for registration. Admin will contact you shortly}
        /// </summary>

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string RegisterUser(string Email, string Password, string FirstName, string Lastname, string Phone)
        {
            bool Result = false;
            string json = "";
            try
            {
                DataTable dtPatientEmail = objCommonDAL.CheckPatientEmail(Email);
                if (dtPatientEmail == null && dtPatientEmail.Rows.Count == 0)
                {
                    Result = objCommonDAL.AddPatientToTemp(Email, Password, FirstName, Lastname, "", "", "", "", "", "", Phone, "", "", 0);

                    if (Result)
                    {
                        objCommonDAL.SignUpEMail(Email, Password);
                        string Sucess = "Thank you for registration. Admin will contact you shortly.";
                        dynamic dResult = new { Sucess };

                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = Sucess;


                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                    }
                    else
                    {
                        objResponse.ResultCode = 400;
                        objResponse.StatusMessage = "Failed Signup";


                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                }
                else
                {
                    DataTable dtGetPass = new DataTable();
                    dtGetPass = objCommonDAL.GetPatientPassword(Email);
                    if (dtGetPass != null && dtGetPass.Rows.Count > 0)
                    {

                        string recoverPassword = Convert.ToString(dtGetPass.Rows[0]["Password"].ToString());
                        string recoverPatientFirstName = Convert.ToString(dtGetPass.Rows[0]["FirstName"].ToString());
                        string recoverPatientLastName = Convert.ToString(dtGetPass.Rows[0]["LastName"].ToString());
                        string recoverPatientEmail = Convert.ToString(Email);

                        //Send Password To Patient Email
                        objCommonDAL.SendPatientPassword(recoverPatientEmail, recoverPassword, recoverPatientFirstName, recoverPatientLastName);

                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = "This email address already exists.  We have emailed you a password reminder.";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.ResultCode = 400;
                        objResponse.StatusMessage = "Failed Signup";


                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }

                }

                return json;
            }
            catch (Exception ex)
            {
                objResponse.ResultCode = 400;
                objResponse.StatusMessage = "Server error occured - Please contact administrator.";
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :Inbox
        /// Desciprtion :This Method Use for Get Messages of Patient
        /// Request : SessionCode,pageindex,pagesize
        /// Response : {SenderId : 672,SenderName : test,Subject : hi this is test,MessageBody : testtest,Date : Oct 21 2013  4:16PM,MessageId : 32,ReadFlag : True }
        /// </summary>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Inbox(string SessionCode, int pageindex)
        {
            string json = "";
            DataTable dtInbox = new DataTable();
            try
            {
                if (IsSessionExpired(SessionCode))
                {

                    List<Messages> lstMessages = new List<Messages>();

                    int patientid = Convert.ToInt32(Session["PatientId"]);
                    dtInbox = objCommonDAL.GetAllMessages(patientid, 0, pageindex, 250);

                    if (dtInbox != null && dtInbox.Rows.Count > 0)
                    {


                        foreach (DataRow Inbox in dtInbox.Rows)
                        {
                            Messages objMessages = new Messages();
                            objMessages.PatientmessageId = Convert.ToInt32(Inbox["patientmessageid"]);
                            objMessages.FromField = objCommonDAL.CheckNull(Convert.ToString(Inbox["fromfield"]), null);
                            objMessages.Subject = objCommonDAL.CheckNull(Convert.ToString(Inbox["Subject"]), null);
                            objMessages.Body = objCommonDAL.CheckNull(Convert.ToString(Inbox["body"]), null);
                            if (objMessages.Body.Length > 25)
                            {
                                objMessages.Body = objMessages.Body.Substring(0, 25);
                            }


                            objMessages.DoctorId = Convert.ToInt32(Inbox["DoctorId"]);


                            objMessages.CreationDate = objCommonDAL.CheckDateNull(Convert.ToDateTime(Inbox["Date"]), Convert.ToDateTime("1900-01-01 00:00:00.000"));

                            objMessages.Read_flag = objCommonDAL.CheckNull(Convert.ToString(Inbox["Read_flag"]), null);
                            if (string.IsNullOrWhiteSpace(objMessages.Read_flag))
                            {
                                objMessages.Read_flag = "False";
                            }
                            else
                            {
                                objMessages.Read_flag = "True";
                            }
                            lstMessages.Add(objMessages);


                        }

                        dynamic dResult = new { pageindex, lstMessages };
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = dResult;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {

                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = "No Messages Found.";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                    }



                }
                else
                {

                    objResponse.ResultCode = 400;
                    objResponse.StatusMessage = "Your Session has been logout.";

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }

                return json;
            }
            catch (Exception ex)
            {
                objResponse.ResultCode = 400;
                objResponse.StatusMessage = "Server error occured - Please contact administrator.";
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }

        }


        /// <summary>
        /// Method Name :ReadMessage
        /// Desciprtion :This Method Use for Read Message of Patient
        /// Request : SessionCode,Messageid
        /// Response : {SenderId : 672,MessageBody : testtest }
        /// </summary>

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ReadMessage(string SessionCode, int Messageid)
        {
            string json = "";
            DataTable dtInbox = new DataTable();
            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    List<Messages> ListMessages = new List<Messages>();
                    int patientid = Convert.ToInt32(Session["PatientId"]);
                    dtInbox = objCommonDAL.GetAllMessages(0, Messageid, 1, 100000);

                    if (dtInbox != null && dtInbox.Rows.Count > 0)
                    {


                        foreach (DataRow Inbox in dtInbox.Rows)
                        {
                            Messages objMessages = new Messages();
                            objMessages.PatientmessageId = Convert.ToInt32(Inbox["patientmessageid"]);

                            objMessages.Body = objCommonDAL.CheckNull(Convert.ToString(Inbox["body"]), null);

                            ListMessages.Add(objMessages);
                        }

                        dynamic dResult = new { ListMessages };
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = dResult;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {

                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = "No Message Found.";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                    }

                }
                else
                {

                    objResponse.ResultCode = 400;
                    objResponse.StatusMessage = "Your Session has been logout.";

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }
                return json;
            }
            catch (Exception ex)
            {
                objResponse.ResultCode = 400;
                objResponse.StatusMessage = "Server error occured - Please contact administrator.";
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }

        }

        /// <summary>
        /// Method Name :ComposeMessage
        /// Desciprtion :This Method Use for Compose Message of Patient
        /// Request : SessionCode,DoctorId,Doctorname,subject,messagebody
        /// Response : { Message send successfully }
        /// </summary>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string ComposeMessage(string SessionCode, string DoctorId, string Doctorname, string subject, string messagebody)
        {
            string json = "";
            DataTable dtInbox = new DataTable();
            try
            {

                if (IsSessionExpired(SessionCode))
                {
                    int id = 0;
                    id = objCommonDAL.Insert_PatientMessages(Convert.ToString(Session["PatientFullName"]), Doctorname, subject, messagebody, Convert.ToInt32(DoctorId), Convert.ToInt32(Session["PatientId"]), false, false, true);
                    if (id > 0)
                    {
                        string Sucess = "Message send successfully.";
                        dynamic dResult = new { Sucess };

                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = Sucess;


                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        string failed = "Message send failed.";
                        dynamic dResult = new { failed };

                        objResponse.ResultCode = 400;
                        objResponse.StatusMessage = failed;


                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }


                }
                else
                {

                    objResponse.ResultCode = 400;
                    objResponse.StatusMessage = "Your Session has been logout.";

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.ResultCode = 400;
                objResponse.StatusMessage = "Server error occured - Please contact administrator.";
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }

        }


        /// <summary>
        /// Method Name :GetTreatingDoctor
        /// Desciprtion :This Method Use for Get All Treating Doctors of Patient
        /// Request : SessionCode
        /// Response : { DoctorId : 672, Name : test ,Image : ../ImageBank/Lighthouse_634758169305710988.jpg ,Specialities : Dental Vendor, Aesthetic Dentist  }
        /// </summary>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetTreatingDoctor(string SessionCode)
        {
            string json = "";
            DataTable dtTreatingDoc = new DataTable();
            try
            {


                if (IsSessionExpired(SessionCode))
                {

                    List<UserList> ListUserList = new List<UserList>();
                    dtTreatingDoc = objCommonDAL.GetTratingdoctors(Convert.ToInt32(Session["PatientId"]), 1, int.MaxValue);
                    if (dtTreatingDoc != null && dtTreatingDoc.Rows.Count > 0)
                    {
                        foreach (DataRow TreatingDoc in dtTreatingDoc.Rows)
                        {
                            UserList objUserList = new UserList();
                            objUserList.UserID = Convert.ToInt32(TreatingDoc["ColleagueId"]);
                            objUserList.name = objCommonDAL.CheckNull(Convert.ToString(TreatingDoc["name"]), null);
                            objUserList.image = objCommonDAL.CheckNull(Convert.ToString(TreatingDoc["image"]), null);
                            objUserList.Specialities = objCommonDAL.CheckNull(Convert.ToString(TreatingDoc["specialities"]), null);

                            ListUserList.Add(objUserList);

                        }
                        dynamic dResult = new { ListUserList };
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = dResult;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {

                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = "No Record Found";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }

                }
                else
                {

                    objResponse.ResultCode = 400;
                    objResponse.StatusMessage = "Your Session has been logout.";

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.ResultCode = 400;
                objResponse.StatusMessage = "Server error occured - Please contact administrator.";
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :AddTreatingDoctor
        /// Desciprtion :This Method Use for Add Treating Doctor of Patient
        /// Request : SessionCode,DoctorId
        /// Response : { Doctor added successfully }
        /// </summary>

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string AddTreatingDoctor(string SessionCode, string DoctorId)
        {
            string json = "";
            bool Result = false;
            try
            {

                if (IsSessionExpired(SessionCode))
                {
                    Result = objCommonDAL.AddPatientMember(Convert.ToInt32(Session["PatientId"]), Convert.ToInt32(DoctorId), Convert.ToInt32(UserStatusEnum.ACTIVE), 0, System.DateTime.Now, System.DateTime.Now, "");
                    if (Result)
                    {
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = "Doctor added successfully.";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.ResultCode = 400;
                        objResponse.StatusMessage = "Failed to add treatingdoctor.";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }



                }
                else
                {

                    objResponse.ResultCode = 400;
                    objResponse.StatusMessage = "Your Session has been logout.";

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.ResultCode = 400;
                objResponse.StatusMessage = "Server error occured - Please contact administrator.";
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :GetAllPatientForms
        /// Desciprtion :This Method Use for Get All Forms of Patient
        /// Request : SessionCode
        /// Response : {  FormId :1 ,FormName :Child Dental Medical History ,FormCode :F3 }
        /// </summary>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetAllPatientForms(string SessionCode)
        {
            string json = "";
            DataTable dtAllForm = new DataTable();
            try
            {

                if (IsSessionExpired(SessionCode))
                {
                    List<Forms> ListForms = new List<Forms>();


                    dtAllForm = objCommonDAL.PatientForms("GetAllForms");
                    if (dtAllForm != null && dtAllForm.Rows.Count > 0)
                    {
                        DataView dv = new DataView();
                        dv.Table = dtAllForm;
                        dv.RowFilter = "FormName <> 'Insurance Coverage'";
                        if (dv != null && dv.Count > 0)
                        {


                            foreach (DataRow AllForms in dtAllForm.Rows)
                            {
                                Forms objFormList = new Forms();
                                objFormList.FormId = Convert.ToInt32(AllForms["FormId"]);
                                objFormList.FormName = objCommonDAL.CheckNull(Convert.ToString(AllForms["FormName"]), null);
                                objFormList.FormCode = objCommonDAL.CheckNull(Convert.ToString(AllForms["FormCode"]), null);

                                ListForms.Add(objFormList);
                            }
                            dynamic dResult = new { ListForms };
                            objResponse.ResultCode = 200;
                            objResponse.ResultDetail = dResult;

                            json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                        }
                    }
                    else
                    {

                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = "No Forms Found";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }

                }
                else
                {

                    objResponse.ResultCode = 400;
                    objResponse.StatusMessage = "Your Session has been logout.";

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.ResultCode = 400;
                objResponse.StatusMessage = "Server error occured - Please contact administrator.";
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }



        /// <summary>
        /// Method Name :GetPatientUploadedImages
        /// Desciprtion :This Method Use for Upload Images of Patient
        /// Request : SessionCode
        /// Response : { ImageId :2312 ,Title :$635180576972057178$aero_left_dropdown.png  ,RelativePath :../ImageBank/$635180576972057178$aero_left_dropdown.png ,Date :2013-10-22 }
        /// </summary>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetPatientUploadedImages(string SessionCode)
        {
            string json = "";
            DataTable dtImages = new DataTable();
            try
            {
                if (IsSessionExpired(SessionCode))
                {

                    List<PatientUploadedImages> ListPatientImage = new List<PatientUploadedImages>();
                    dtImages = objCommonDAL.GetPatientMontages(Convert.ToInt32(Session["PatientId"]));

                    if (dtImages != null && dtImages.Rows.Count > 0)
                    {


                        foreach (DataRow Images in dtImages.Rows)
                        {
                            PatientUploadedImages objpatientimage = new PatientUploadedImages();
                            objpatientimage.ImageId = Convert.ToInt32(Images["ImageId"]);
                            objpatientimage.Name = objCommonDAL.CheckNull(Convert.ToString(Images["Name"]), null);
                            objpatientimage.RelativePath = objCommonDAL.CheckNull(Convert.ToString(Images["RelativePath"]), null);
                            objpatientimage.CreationDate = objCommonDAL.CheckDateNull(Convert.ToDateTime(Images["CreationDate"]), Convert.ToDateTime("1900-01-01 00:00:00.000"));

                            ListPatientImage.Add(objpatientimage);

                        }

                        dynamic dResult = new { ListPatientImage };
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = dResult;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);


                    }
                    else
                    {

                        objResponse.ResultCode = 400;
                        objResponse.StatusMessage = "Your Session has been logout.";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                    }
                }
                else
                {

                    objResponse.ResultCode = 400;
                    objResponse.StatusMessage = "Your Session has been logout.";

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }

                return json;
            }
            catch (Exception ex)
            {
                objResponse.ResultCode = 400;
                objResponse.StatusMessage = "Server error occured - Please contact administrator.";
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :GetPatientDetails
        /// Desciprtion :This Method Use for Get Details of Patient
        /// Request : SessionCode
        /// Response : { PatientId :2312 ,FirstName : john ,LastName :martin ,Email : john123@gmail.com,Phone :(234) 324-4324 }
        /// </summary>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetPatientDetails(string SessionCode)
        {
            string json = "";
            DataTable dtPatient = new DataTable();
            try
            {
                if (IsSessionExpired(SessionCode))
                {
                    List<PatientDetails> ListPatient = new List<PatientDetails>();

                    dtPatient = objCommonDAL.GetPatientsDetails(Convert.ToInt32(Session["PatientId"]));

                    if (dtPatient != null && dtPatient.Rows.Count > 0)
                    {

                        foreach (DataRow Patient in dtPatient.Rows)
                        {
                            PatientDetails objpatientdetails = new PatientDetails();
                            objpatientdetails.PatientId = Convert.ToInt32(Patient["PatientId"]);
                            objpatientdetails.FirstName = objCommonDAL.CheckNull(Convert.ToString(Patient["FirstName"]), null);
                            objpatientdetails.LastName = objCommonDAL.CheckNull(Convert.ToString(Patient["LastName"]), null);
                            objpatientdetails.Email = objCommonDAL.CheckNull(Convert.ToString(Patient["Email"]), null);
                            objpatientdetails.Phone = objCommonDAL.CheckNull(Convert.ToString(Patient["Phone"]), null);


                            ListPatient.Add(objpatientdetails);


                        }

                        dynamic dResult = new { ListPatient };
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = dResult;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {

                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = "No Message Found.";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                    }

                }
                else
                {

                    objResponse.ResultCode = 400;
                    objResponse.StatusMessage = "Your Session has been logout.";

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }
                return json;
            }
            catch (Exception ex)
            {
                objResponse.ResultCode = 400;
                objResponse.StatusMessage = "Server error occured - Please contact administrator.";
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name :UpdatePatientDetails
        /// Desciprtion :This Method Use for Update Details of Patient
        /// Request : SessionCode
        /// Response : { Patient details updated successfully }
        /// </summary>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string UpdatePatientDetails(string SessionCode, string FirstName, string LastName, string Gender, string DateOfBirth, string Address, string Address2, string City, string State, string Phone)
        {
            string json = "";
            bool Result = false;
            try
            {

                if (IsSessionExpired(SessionCode))
                {
                    Result = objCommonDAL.EditPatientDetails(Convert.ToInt32(Session["PatientId"]), FirstName, LastName, Gender, Convert.ToDateTime(DateOfBirth), Address, Address2, City, State, Phone);
                    if (Result)
                    {
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = "Patient details updated successfully.";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.ResultCode = 400;
                        objResponse.StatusMessage = "Failed to update patient details.";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }




                }
                else
                {

                    objResponse.ResultCode = 400;
                    objResponse.StatusMessage = "Your Session has been logout.";

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.ResultCode = 400;
                objResponse.StatusMessage = "Server error occured - Please contact administrator.";
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }



        /// <summary>
        /// Method Name :SearchDentist
        /// Desciprtion :This Method Use for SearchDentist
        /// Request : SessionCode,index,size,Keywords,Miles,zipcode,Specialtity
        /// Response : { DoctorId :94 ,Name : Test,specialty :Dental Professor ,Image :../ImageBank/q_635050123241409575.jpg }
        /// </summary>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string SearchDentist(string SessionCode, int index, int size, string Keywords, int Miles, string zipcode, string Specialtity)
        {
            string json = "";
            DataTable dtSearchDentist = new DataTable();
            try
            {


                if (IsSessionExpired(SessionCode))
                {

                    List<UserList> ListUserList = new List<UserList>();
                    dtSearchDentist = objCommonDAL.GetDentistListBySearch(index, size, Keywords, null, null, 0, Miles, null, null, null, null, null, null, zipcode, Specialtity, null);
                    if (dtSearchDentist != null && dtSearchDentist.Rows.Count > 0)
                    {
                        foreach (DataRow SearchDentist in dtSearchDentist.Rows)
                        {
                            UserList objuserlist = new UserList();
                            objuserlist.UserID = Convert.ToInt32(SearchDentist["UserId"]);
                            objuserlist.Username = objCommonDAL.CheckNull(Convert.ToString(SearchDentist["name"]), null);
                            objuserlist.Specialities = objCommonDAL.CheckNull(Convert.ToString(SearchDentist["Type"]), null);
                            objuserlist.image = objCommonDAL.CheckNull(Convert.ToString(SearchDentist["image"]), null);


                            ListUserList.Add(objuserlist);
                        }

                        dynamic dResult = new { ListUserList };
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = dResult;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = "No Record Found.";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }

                }
                else
                {

                    objResponse.ResultCode = 400;
                    objResponse.StatusMessage = "Your Session has been logout.";

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Method Name :SendContactMessage
        /// Desciprtion :This Method Use for Send Message from Contact us page
        /// Request : FirstName,Lastname,Email,Phone,Comment
        /// Response : { Request received at Recordlinc }
        /// </summary>

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string SendContactMessage(string FirstName, string Lastname, string Email, string Phone, string Comment)
        {
            string json = "";
            try
            {

                bool status = objCommonDAL.SendContactMessage(FirstName + " " + Lastname, Email, Phone, Comment);
                if (status)
                {
                    objResponse.ResultCode = 200;
                    objResponse.ResultDetail = "Request received at Recordlinc.";

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                }
                else
                {
                    objResponse.ResultCode = 400;
                    objResponse.StatusMessage = "Request sending failed.";

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                }




                return json;
            }
            catch (Exception ex)
            {
                objResponse.ResultCode = 400;
                objResponse.StatusMessage = "Server error occured - Please contact administrator.";
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }




        /// <summary>
        /// Method Name : GetChildHistory
        /// Desciprtion :This Method Use for Get ChildHistory Form details of Patient
        /// Request : sessioncode
        /// Response : { Get Entire Child Dental Medical History Form Details }
        /// </summary>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetChildHistoryForm(string SessionCode)
        {
            string json = "";

            DataTable dtChildHistory = new DataTable();

            try
            {

                if (IsSessionExpired(SessionCode))
                {

                    dtChildHistory = objCommonDAL.GetChildHistory(Convert.ToInt32(Session["PatientId"]), "GetChildHistory");

                    if (dtChildHistory != null && dtChildHistory.Rows.Count > 0)
                    {
                        dtChildHistory.Columns["rdDentalQue1"].ColumnName = dtChildHistory.Columns["rdDentalQue1"] + "|" + "Is this your child's first visit to a dentist?";
                        dtChildHistory.Columns["txtDentalQue2"].ColumnName = dtChildHistory.Columns["txtDentalQue2"] + "|" + "If not, how long since the last visit to the dentist?";
                        dtChildHistory.Columns["rdDentalQue3"].ColumnName = dtChildHistory.Columns["rdDentalQue3"] + "|" + "Were any x-rays or radiographs taken when your child previously visited the dentist?";
                        dtChildHistory.Columns["rdDentalQue5"].ColumnName = dtChildHistory.Columns["rdDentalQue5"] + "|" + "Does your child eat sweets, such as candy, soda pop, chewing gum?";
                        dtChildHistory.Columns["chkDentalQue6"].ColumnName = dtChildHistory.Columns["chkDentalQue6"] + "|" + "When does your child brush his/her teeth?,Upon arising,After eating any food,Right after meals, Before going to bed";

                        dtChildHistory.Columns["chkDentalQue7"].ColumnName = dtChildHistory.Columns["chkDentalQue7"] + "|" + "How does your child receive Fluoride?,Community water,Well water,Fluoride drops or tablets,Fluoride rinse or gel";
                        dtChildHistory.Columns["rdDentalQue8"].ColumnName = dtChildHistory.Columns["rdDentalQue8"] + "|" + "Have any cavities been noted in the past?";
                        dtChildHistory.Columns["rdDentalQue9"].ColumnName = dtChildHistory.Columns["rdDentalQue9"] + "|" + "Does your child suck his/her thumb or fingers?";
                        dtChildHistory.Columns["rdDentalQue10"].ColumnName = dtChildHistory.Columns["rdDentalQue10"] + "|" + "Were any teeth (baby or permanent) removed by extraction?";
                        dtChildHistory.Columns["rdDentalQue11"].ColumnName = dtChildHistory.Columns["rdDentalQue11"] + "|" + "Have there been any injuries to teeth, such as falls, blows, chips, etc?";
                        dtChildHistory.Columns["txtDentalQue11a"].ColumnName = dtChildHistory.Columns["txtDentalQue11a"] + "|" + "If so describe";

                        dtChildHistory.Columns["rdDentalQue12"].ColumnName = dtChildHistory.Columns["rdDentalQue12"] + "|" + "Has your child had any problem with dental treatment in the past?";
                        dtChildHistory.Columns["rdDentalQue13"].ColumnName = dtChildHistory.Columns["rdDentalQue13"] + "|" + "Has anyone in the family, including parents, had orthodontics?";
                        dtChildHistory.Columns["rdDentalQue14"].ColumnName = dtChildHistory.Columns["rdDentalQue14"] + "|" + "Has your child ever received a local anesthetic?";
                        dtChildHistory.Columns["rdDentalQue15"].ColumnName = dtChildHistory.Columns["rdDentalQue15"] + "|" + "Has your child ever had occlusal sealants?";
                        dtChildHistory.Columns["rdDentalQue16"].ColumnName = dtChildHistory.Columns["rdDentalQue16"] + "|" + "Does your child think there is anything wrong with his/her teeth?";


                        dtChildHistory.Columns["rdMedicalQue1"].ColumnName = dtChildHistory.Columns["rdMedicalQue1"] + "|" + "Does your child have a health problem?";
                        dtChildHistory.Columns["txtrdMedicalQue1"].ColumnName = dtChildHistory.Columns["txtrdMedicalQue1"] + "|" + "If so describe";
                        dtChildHistory.Columns["rdMedicalQue2"].ColumnName = dtChildHistory.Columns["rdMedicalQue2"] + "|" + "Is your child under care of physician?";
                        dtChildHistory.Columns["txtMedicalQue2a"].ColumnName = dtChildHistory.Columns["txtMedicalQue2a"] + "|" + "If so describe";
                        dtChildHistory.Columns["txtMedicalQue3"].ColumnName = dtChildHistory.Columns["txtMedicalQue3"] + "|" + "Name of physician ";


                        dtChildHistory.Columns["txtMedicalQue3b"].ColumnName = dtChildHistory.Columns["txtMedicalQue3b"] + "|" + "Physician's phone ";
                        dtChildHistory.Columns["rdMedicalQue4"].ColumnName = dtChildHistory.Columns["rdMedicalQue4"] + "|" + "Is your child receiving any medication?";
                        dtChildHistory.Columns["txtMedicalQue4a"].ColumnName = dtChildHistory.Columns["txtMedicalQue4a"] + "|" + "What?";
                        dtChildHistory.Columns["rdMedicalQue5"].ColumnName = dtChildHistory.Columns["rdMedicalQue5"] + "|" + "Is your child allergic to penicillin, antibiotics or other drugs? ";
                        dtChildHistory.Columns["rdMedicalQue6"].ColumnName = dtChildHistory.Columns["rdMedicalQue6"] + "|" + "Is your child allergic to or sensitive to any metals or latex? ";

                        dtChildHistory.Columns["rdMedicalQue7"].ColumnName = dtChildHistory.Columns["rdMedicalQue7"] + "|" + "Does your child have other allergies? ";
                        dtChildHistory.Columns["rdMedicalQue8"].ColumnName = dtChildHistory.Columns["rdMedicalQue8"] + "|" + "Has your child had any serious illness?";
                        dtChildHistory.Columns["txtMedicalQue8a"].ColumnName = dtChildHistory.Columns["txtMedicalQue8a"] + "|" + "When?";
                        dtChildHistory.Columns["txtMedicalQue8b"].ColumnName = dtChildHistory.Columns["txtMedicalQue8b"] + "|" + "What?";
                        dtChildHistory.Columns["rdMedicalQue9"].ColumnName = dtChildHistory.Columns["rdMedicalQue9"] + "|" + "Has your child ever had surgery?";

                        dtChildHistory.Columns["rdMedicalQue10"].ColumnName = dtChildHistory.Columns["rdMedicalQue10"] + "|" + "Does your child have a heart murmur?";
                        dtChildHistory.Columns["rdMedicalQue11"].ColumnName = dtChildHistory.Columns["rdMedicalQue11"] + "|" + "Is surgery contemplated?";
                        dtChildHistory.Columns["rdMedicalQue12"].ColumnName = dtChildHistory.Columns["rdMedicalQue12"] + "|" + "Does your child experience severe or prolongated bleeding?";
                        dtChildHistory.Columns["rdMedicalQue13"].ColumnName = dtChildHistory.Columns["rdMedicalQue13"] + "|" + "Does your child have AIDS or has he/she tested HIV positive?";
                        dtChildHistory.Columns["rdMedicalQue14"].ColumnName = dtChildHistory.Columns["rdMedicalQue14"] + "|" + "Has your child tested positive for hepatitis?";

                        dtChildHistory.Columns["rdMedicalQue15"].ColumnName = dtChildHistory.Columns["rdMedicalQue15"] + "|" + "Is your child subject to nervous disorders?";
                        dtChildHistory.Columns["chkMedicalQue15a"].ColumnName = dtChildHistory.Columns["chkMedicalQue15a"] + "|" + ", Fainting?,Seizures?,Dizziness?,Behavioral/Learning problems?";
                        dtChildHistory.Columns["rdMedicalQue16"].ColumnName = dtChildHistory.Columns["rdMedicalQue16"] + "|" + "Does your child have frequent headaches?";
                        dtChildHistory.Columns["chkMedicalQue17"].ColumnName = dtChildHistory.Columns["chkMedicalQue17"] + "|" + "Has your child had a history of, Diabetes,Heart trouble, Asthma, Kidney infection,Rheumatic fever,Epilepsy, Ceral palsebry, Liver problems,Congenital birth defects,Mental retardation,Eyesight problems,Cancer,Infections, Speech impairments,Hearing loss";
                        dtChildHistory.Columns["txtComments"].ColumnName = dtChildHistory.Columns["txtComments"] + "|" + "Comments";
                        dtChildHistory.Columns["txtDigiSign"].ColumnName = dtChildHistory.Columns["txtDigiSign"] + "|" + "Digital Signature";

                        dynamic dResult = new { dtChildHistory };
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = dResult;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = "Doctor doesn't send you child history form.";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                }
                else
                {

                    objResponse.ResultCode = 400;
                    objResponse.StatusMessage = "Your Session has been logout.";

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.ResultCode = 400;
                objResponse.StatusMessage = "Server error occured - Please contact administrator.";
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name : UpdateChildHistoryForm
        /// Desciprtion :This Method Use for Update ChildHistory Form details of Patient
        /// Request : Sessioncode,txtDob, rdDentalQue1, txtDentalQue2, rdDentalQue3, rdDentalQue5, chkDentalQue6_1, chkDentalQue6_2, chkDentalQue6_3, chkDentalQue6_4, chkDentalQue7_1, chkDentalQue7_2, chkDentalQue7_3, chkDentalQue7_4, rdDentalQue8, rdDentalQue9, rdDentalQue10, rdDentalQue11, txtDentalQue11a, rdDentalQue12, rdDentalQue13, rdDentalQue14, rdDentalQue15, rdDentalQue16, rdMedicalQue1, txtrdMedicalQue1, rdMedicalQue2, txtMedicalQue2a, txtMedicalQue3, txtMedicalQue3b, rdMedicalQue4, txtMedicalQue4a, rdMedicalQue5, rdMedicalQue6, rdMedicalQue7, rdMedicalQue8, txtMedicalQue8a, txtMedicalQue8b, rdMedicalQue9, rdMedicalQue10, rdMedicalQue11, rdMedicalQue12, rdMedicalQue13, rdMedicalQue14, rdMedicalQue15, chkMedicalQue15a_1, chkMedicalQue15a_2, chkMedicalQue15a_3, chkMedicalQue15a_4, rdMedicalQue16, chkMedicalQue17_1, chkMedicalQue17_2, chkMedicalQue17_3, chkMedicalQue17_4, chkMedicalQue17_5, chkMedicalQue17_6, chkMedicalQue17_7, chkMedicalQue17_8, chkMedicalQue17_9, chkMedicalQue17_10, chkMedicalQue17_11, chkMedicalQue17_12, chkMedicalQue17_13, chkMedicalQue17_14, chkMedicalQue17_15, txtComments, txtDigiSign
        /// Response : { Form updated successfully. }
        /// </summary>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string UpdateChildHistoryForm(string Sessioncode, string txtDob, string rdDentalQue1, string txtDentalQue2, string rdDentalQue3, string rdDentalQue5, string chkDentalQue6_1, string chkDentalQue6_2, string chkDentalQue6_3, string chkDentalQue6_4, string chkDentalQue7_1, string chkDentalQue7_2, string chkDentalQue7_3, string chkDentalQue7_4, string rdDentalQue8, string rdDentalQue9, string rdDentalQue10, string rdDentalQue11, string txtDentalQue11a, string rdDentalQue12, string rdDentalQue13, string rdDentalQue14, string rdDentalQue15, string rdDentalQue16, string rdMedicalQue1, string txtrdMedicalQue1, string rdMedicalQue2, string txtMedicalQue2a, string txtMedicalQue3, string txtMedicalQue3b, string rdMedicalQue4, string txtMedicalQue4a, string rdMedicalQue5, string rdMedicalQue6, string rdMedicalQue7, string rdMedicalQue8, string txtMedicalQue8a, string txtMedicalQue8b, string rdMedicalQue9, string rdMedicalQue10, string rdMedicalQue11, string rdMedicalQue12, string rdMedicalQue13, string rdMedicalQue14, string rdMedicalQue15, string chkMedicalQue15a_1, string chkMedicalQue15a_2, string chkMedicalQue15a_3, string chkMedicalQue15a_4, string rdMedicalQue16, string chkMedicalQue17_1, string chkMedicalQue17_2, string chkMedicalQue17_3, string chkMedicalQue17_4, string chkMedicalQue17_5, string chkMedicalQue17_6, string chkMedicalQue17_7, string chkMedicalQue17_8, string chkMedicalQue17_9, string chkMedicalQue17_10, string chkMedicalQue17_11, string chkMedicalQue17_12, string chkMedicalQue17_13, string chkMedicalQue17_14, string chkMedicalQue17_15, string txtComments, string txtDigiSign)
        {
            string json = "";
            if (IsSessionExpired(Sessioncode))
            {
                try
                {
                    int age = 0;
                    if (txtDob == null || txtDob == "")
                    {
                        age = 17;
                    }
                    else
                    {
                        DateTime dateOfBirth = Convert.ToDateTime(txtDob);
                        var today = DateTime.Today;
                        var a = (today.Year * 100 + today.Month) * 100 + today.Day;
                        var b = (dateOfBirth.Year * 100 + dateOfBirth.Month) * 100 + dateOfBirth.Day;
                        age = (a - b) / 10000;
                    }
                    if (age < 18)
                    {

                        // Dental History Table
                        DataTable tblDentalHistory = new DataTable("DentalHistory");
                        tblDentalHistory.Columns.Add("rdDentalQue1");
                        tblDentalHistory.Columns.Add("txtDentalQue2");
                        tblDentalHistory.Columns.Add("rdDentalQue3");
                        tblDentalHistory.Columns.Add("rdDentalQue4");
                        tblDentalHistory.Columns.Add("rdDentalQue5");
                        tblDentalHistory.Columns.Add("chkDentalQue6");
                        StringBuilder rdDentalQue6 = new StringBuilder();




                        if (chkDentalQue6_1 == "1")
                        {
                            if (!String.IsNullOrEmpty(rdDentalQue6.ToString()))
                            {
                                rdDentalQue6.Append("," + "1");
                            }
                            else
                            {
                                rdDentalQue6.Append("1");
                            }
                        }
                        if (chkDentalQue6_2 == "1")
                        {
                            if (!String.IsNullOrEmpty(rdDentalQue6.ToString()))
                            {
                                rdDentalQue6.Append("," + "2");
                            }
                            else
                            {
                                rdDentalQue6.Append("2");
                            }
                        }
                        if (chkDentalQue6_3 == "1")
                        {
                            if (!String.IsNullOrEmpty(rdDentalQue6.ToString()))
                            {
                                rdDentalQue6.Append("," + "3");
                            }
                            else
                            {
                                rdDentalQue6.Append("3");
                            }
                        }

                        if (chkDentalQue6_4 == "1")
                        {
                            if (!String.IsNullOrEmpty(rdDentalQue6.ToString()))
                            {
                                rdDentalQue6.Append("," + "4");
                            }
                            else
                            {
                                rdDentalQue6.Append("4");
                            }
                        }



                        tblDentalHistory.Columns.Add("chkDentalQue7");
                        StringBuilder rdDentalQue7 = new StringBuilder();




                        if (chkDentalQue7_1 == "1")
                        {
                            if (!String.IsNullOrEmpty(rdDentalQue7.ToString()))
                            {
                                rdDentalQue7.Append("," + "1");
                            }
                            else
                            {
                                rdDentalQue7.Append("1");
                            }
                        }
                        if (chkDentalQue7_2 == "1")
                        {
                            if (!String.IsNullOrEmpty(rdDentalQue7.ToString()))
                            {
                                rdDentalQue7.Append("," + "2");
                            }
                            else
                            {
                                rdDentalQue7.Append("2");
                            }
                        }
                        if (chkDentalQue7_3 == "1")
                        {
                            if (!String.IsNullOrEmpty(rdDentalQue7.ToString()))
                            {
                                rdDentalQue7.Append("," + "3");
                            }
                            else
                            {
                                rdDentalQue7.Append("3");
                            }
                        }

                        if (chkDentalQue7_4 == "1")
                        {
                            if (!String.IsNullOrEmpty(rdDentalQue7.ToString()))
                            {
                                rdDentalQue7.Append("," + "4");
                            }
                            else
                            {
                                rdDentalQue7.Append("4");
                            }
                        }
                        tblDentalHistory.Columns.Add("txtDentalQue7a");
                        tblDentalHistory.Columns.Add("txtDentalQue7b");
                        tblDentalHistory.Columns.Add("rdDentalQue8");
                        tblDentalHistory.Columns.Add("rdDentalQue9");
                        tblDentalHistory.Columns.Add("rdDentalQue10");
                        tblDentalHistory.Columns.Add("rdDentalQue10a");
                        tblDentalHistory.Columns.Add("rdDentalQue10b");
                        tblDentalHistory.Columns.Add("rdDentalQue11");
                        tblDentalHistory.Columns.Add("txtDentalQue11a");
                        tblDentalHistory.Columns.Add("rdDentalQue12");
                        tblDentalHistory.Columns.Add("rdDentalQue13");
                        tblDentalHistory.Columns.Add("rdDentalQue14");
                        tblDentalHistory.Columns.Add("rdDentalQue15");
                        tblDentalHistory.Columns.Add("rdDentalQue16");

                        tblDentalHistory.Rows.Add(rdDentalQue1, txtDentalQue2, rdDentalQue3, "",
                            rdDentalQue5, rdDentalQue6.ToString(), rdDentalQue7.ToString(), "", "", rdDentalQue8,
                            rdDentalQue9, rdDentalQue10, "", "",
                            rdDentalQue11, txtDentalQue11a, rdDentalQue12, rdDentalQue13, rdDentalQue14,
                            rdDentalQue15, rdDentalQue16);




                        // Medical History Table
                        DataTable tblMedicalHistory = new DataTable("MedicalHistory");
                        tblMedicalHistory.Columns.Add("rdMedicalQue1");
                        tblMedicalHistory.Columns.Add("txtrdMedicalQue1");
                        tblMedicalHistory.Columns.Add("rdMedicalQue2");
                        tblMedicalHistory.Columns.Add("txtMedicalQue2a");
                        tblMedicalHistory.Columns.Add("txtMedicalQue3");
                        tblMedicalHistory.Columns.Add("txtMedicalQue3b");
                        tblMedicalHistory.Columns.Add("rdMedicalQue4");
                        tblMedicalHistory.Columns.Add("txtMedicalQue4a");
                        tblMedicalHistory.Columns.Add("rdMedicalQue5");
                        tblMedicalHistory.Columns.Add("rdMedicalQue6");
                        tblMedicalHistory.Columns.Add("rdMedicalQue7");
                        tblMedicalHistory.Columns.Add("rdMedicalQue8");
                        tblMedicalHistory.Columns.Add("txtMedicalQue8a");
                        tblMedicalHistory.Columns.Add("txtMedicalQue8b");
                        tblMedicalHistory.Columns.Add("rdMedicalQue9");
                        tblMedicalHistory.Columns.Add("rdMedicalQue10");
                        tblMedicalHistory.Columns.Add("rdMedicalQue11");
                        tblMedicalHistory.Columns.Add("rdMedicalQue12");
                        tblMedicalHistory.Columns.Add("rdMedicalQue13");
                        tblMedicalHistory.Columns.Add("rdMedicalQue14");

                        tblMedicalHistory.Columns.Add("rdMedicalQue15");
                        tblMedicalHistory.Columns.Add("chkMedicalQue15a");
                        StringBuilder srdMedicalQue15 = new StringBuilder();

                        if (chkMedicalQue15a_1 == "Y")
                        {
                            if (!String.IsNullOrEmpty(srdMedicalQue15.ToString()))
                            {
                                srdMedicalQue15.Append("," + "1");
                            }
                            else
                            {
                                srdMedicalQue15.Append("1");
                            }
                        }
                        if (chkMedicalQue15a_2 == "Y")
                        {
                            if (!String.IsNullOrEmpty(srdMedicalQue15.ToString()))
                            {
                                srdMedicalQue15.Append("," + "2");
                            }
                            else
                            {
                                srdMedicalQue15.Append("2");
                            }
                        }
                        if (chkMedicalQue15a_3 == "Y")
                        {
                            if (!String.IsNullOrEmpty(srdMedicalQue15.ToString()))
                            {
                                srdMedicalQue15.Append("," + "3");
                            }
                            else
                            {
                                srdMedicalQue15.Append("3");
                            }
                        }

                        if (chkMedicalQue15a_4 == "Y")
                        {
                            if (!String.IsNullOrEmpty(srdMedicalQue15.ToString()))
                            {
                                srdMedicalQue15.Append("," + "4");
                            }
                            else
                            {
                                srdMedicalQue15.Append("4");
                            }
                        }

                        tblMedicalHistory.Columns.Add("rdMedicalQue16");
                        tblMedicalHistory.Columns.Add("chkMedicalQue17");
                        StringBuilder schkMedicalQue17 = new StringBuilder();



                        if (chkMedicalQue17_1 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "1");
                            }
                            else
                            {
                                schkMedicalQue17.Append("1");
                            }
                        }
                        if (chkMedicalQue17_2 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "2");
                            }
                            else
                            {
                                schkMedicalQue17.Append("2");
                            }
                        }
                        if (chkMedicalQue17_3 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "3");
                            }
                            else
                            {
                                schkMedicalQue17.Append("3");
                            }
                        }

                        if (chkMedicalQue17_4 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "4");
                            }
                            else
                            {
                                schkMedicalQue17.Append("4");
                            }
                        }

                        if (chkMedicalQue17_5 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "5");
                            }
                            else
                            {
                                schkMedicalQue17.Append("5");
                            }
                        }
                        if (chkMedicalQue17_6 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "6");
                            }
                            else
                            {
                                schkMedicalQue17.Append("6");
                            }
                        }
                        if (chkMedicalQue17_7 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "7");
                            }
                            else
                            {
                                schkMedicalQue17.Append("7");
                            }
                        }
                        if (chkMedicalQue17_8 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "8");
                            }
                            else
                            {
                                schkMedicalQue17.Append("8");
                            }
                        }
                        if (chkMedicalQue17_9 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "9");
                            }
                            else
                            {
                                schkMedicalQue17.Append("9");
                            }
                        }
                        if (chkMedicalQue17_10 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "10");
                            }
                            else
                            {
                                schkMedicalQue17.Append("10");
                            }
                        }
                        if (chkMedicalQue17_11 == "Y")
                        {
                            if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                            {
                                schkMedicalQue17.Append("," + "11");
                            }
                            else
                            {
                                schkMedicalQue17.Append("11");
                            }
                            if (chkMedicalQue17_12 == "1")
                            {
                                if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                                {
                                    schkMedicalQue17.Append("," + "12");
                                }
                                else
                                {
                                    schkMedicalQue17.Append("12");
                                }
                            }
                            if (chkMedicalQue17_13 == "Y")
                            {
                                if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                                {
                                    schkMedicalQue17.Append("," + "13");
                                }
                                else
                                {
                                    schkMedicalQue17.Append("13");
                                }
                            }
                            if (chkMedicalQue17_14 == "Y")
                            {
                                if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                                {
                                    schkMedicalQue17.Append("," + "14");
                                }
                                else
                                {
                                    schkMedicalQue17.Append("14");
                                }
                            }
                            if (chkMedicalQue17_15 == "Y")
                            {
                                if (!String.IsNullOrEmpty(schkMedicalQue17.ToString()))
                                {
                                    schkMedicalQue17.Append("," + "15");
                                }
                                else
                                {
                                    schkMedicalQue17.Append("15");
                                }
                            }

                        }


                        tblMedicalHistory.Columns.Add("txtComments");
                        tblMedicalHistory.Columns.Add("txtDigiSign");
                        tblMedicalHistory.Columns.Add("CreatedDate");
                        tblMedicalHistory.Columns.Add("ModifiedDate");

                        tblMedicalHistory.Rows.Add(rdMedicalQue1, txtrdMedicalQue1, rdMedicalQue2, txtMedicalQue2a, txtMedicalQue3, txtMedicalQue3b,
                           rdMedicalQue4, txtMedicalQue4a, rdMedicalQue5, rdMedicalQue6, rdMedicalQue7,
                           rdMedicalQue8, txtMedicalQue8a, txtMedicalQue8b, rdMedicalQue9, rdMedicalQue10,
                           rdMedicalQue11, rdMedicalQue12, rdMedicalQue13, rdMedicalQue14, rdMedicalQue15,
                           srdMedicalQue15.ToString(), rdMedicalQue16, schkMedicalQue17.ToString(), txtComments, txtDigiSign, Convert.ToString(System.DateTime.Now), Convert.ToString(System.DateTime.Now));



                        //Join Two Table
                        DataSet dsChildHistory = new DataSet("ChildHistory");
                        dsChildHistory.Tables.Add(tblDentalHistory);
                        dsChildHistory.Tables.Add(tblMedicalHistory);

                        DataSet ds = dsChildHistory.Copy();
                        dsChildHistory.Clear();

                        string ChildDentalMedicalHistory = ds.GetXml();

                        bool status = objCommonDAL.UpdateChildMedicalDentalForm(Convert.ToInt32(Session["PatientId"]), "UpdateChildHistoryPatient", ChildDentalMedicalHistory);

                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = "Form updated successfully.";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);



                    }

                    else
                    {
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = "You can't fill out this form because you are above 18 years";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);


                    }

                    return json;

                }
                catch (Exception ex)
                {
                    objResponse.ResultCode = 400;
                    objResponse.StatusMessage = "Server error occured - Please contact administrator.";
                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    status = objCommonDAL.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                    return json;
                }


            }
            else
            {
                objResponse.ResultCode = 400;
                objResponse.StatusMessage = "Your Session has been logout.";

                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

            }
            return json;
        }

        /// <summary>
        /// Method Name : GetChildHistory
        /// Desciprtion :This Method Use for Get DentalHistory Form details of Patient
        /// Request : sessioncode
        /// Response : { Get Entire  Dental History Form Details }
        /// </summary>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetDentalHistoryForm(string SessionCode)
        {
            string json = "";

            DataTable dtDentalHistory = new DataTable();

            try
            {
                SessionCode = Session["SessionCode"].ToString();

                if (IsSessionExpired(SessionCode))
                {

                    dtDentalHistory = objCommonDAL.GetDentalHistory(Convert.ToInt32(Session["PatientId"]), "GetDentalHistory");

                    if (dtDentalHistory != null && dtDentalHistory.Rows.Count > 0)
                    {

                        dtDentalHistory.Columns["txtQue1"].ColumnName = dtDentalHistory.Columns["txtQue1"] + "|" + "Purpose of initial visit";
                        dtDentalHistory.Columns["txtQue2"].ColumnName = dtDentalHistory.Columns["txtQue2"] + "|" + "Are you aware of a problem?";
                        dtDentalHistory.Columns["txtQue3"].ColumnName = dtDentalHistory.Columns["txtQue3"] + "|" + "How long since the patient last dental visit?";
                        dtDentalHistory.Columns["txtQue4"].ColumnName = dtDentalHistory.Columns["txtQue4"] + "|" + " What was done at that time?";
                        dtDentalHistory.Columns["txtQue5"].ColumnName = dtDentalHistory.Columns["txtQue5"] + "|" + "Previous dentists name";


                        dtDentalHistory.Columns["txtQue5a"].ColumnName = dtDentalHistory.Columns["txtQue5a"] + "|" + "Address";
                        dtDentalHistory.Columns["txtQue5b"].ColumnName = dtDentalHistory.Columns["txtQue5b"] + "|" + "Phone";
                        dtDentalHistory.Columns["txtQue5c"].ColumnName = dtDentalHistory.Columns["txtQue5c"] + "|" + "Email";
                        dtDentalHistory.Columns["txtQue6"].ColumnName = dtDentalHistory.Columns["txtQue6"] + "|" + "When was the last time you had your teeth cleaned?";
                        dtDentalHistory.Columns["txtQue7"].ColumnName = dtDentalHistory.Columns["txtQue7"] + "|" + "Do you make regular visits to the dentist?";


                        dtDentalHistory.Columns["rdQue7a"].ColumnName = dtDentalHistory.Columns["rdQue7a"] + "|" + "How often";
                        dtDentalHistory.Columns["rdQue8"].ColumnName = dtDentalHistory.Columns["rdQue8"] + "|" + "Were dental x-rays taken?";
                        dtDentalHistory.Columns["rdQue9"].ColumnName = dtDentalHistory.Columns["rdQue9"] + "|" + "Have the patient lost any teeth or have any teeth been removed?";
                        dtDentalHistory.Columns["txtQue9a"].ColumnName = dtDentalHistory.Columns["txtQue9a"] + "|" + "If yes, why?";
                        dtDentalHistory.Columns["rdQue10"].ColumnName = dtDentalHistory.Columns["rdQue10"] + "|" + "Have any of your teeth been replaced with the following?";


                        dtDentalHistory.Columns["txtQue11aFixedbridge"].ColumnName = dtDentalHistory.Columns["txtQue11aFixedbridge"] + "|" + "Fixed bridge";
                        dtDentalHistory.Columns["txtQue11aAge"].ColumnName = dtDentalHistory.Columns["txtQue11aAge"] + "|" + "Date";
                        dtDentalHistory.Columns["txtQue11bRemoveablebridge"].ColumnName = dtDentalHistory.Columns["txtQue11bRemoveablebridge"] + "|" + "Removeable bridge";
                        dtDentalHistory.Columns["txtQue11bAge"].ColumnName = dtDentalHistory.Columns["txtQue11bAge"] + "|" + "Date";
                        dtDentalHistory.Columns["txtQue11cDenture"].ColumnName = dtDentalHistory.Columns["txtQue11cDenture"] + "|" + "Denture";


                        dtDentalHistory.Columns["txtQue11cAge"].ColumnName = dtDentalHistory.Columns["txtQue11cAge"] + "|" + "Date";
                        dtDentalHistory.Columns["rdQue11dImplant"].ColumnName = dtDentalHistory.Columns["rdQue11dImplant"] + "|" + "Implant";
                        dtDentalHistory.Columns["txtQue11dAge"].ColumnName = dtDentalHistory.Columns["txtQue11dAge"] + "|" + "Date";
                        dtDentalHistory.Columns["txtQue12"].ColumnName = dtDentalHistory.Columns["txtQue12"] + "|" + "Please explain any problems or complications with previous dental treatment?";



                        dtDentalHistory.Columns["rdQue15"].ColumnName = dtDentalHistory.Columns["rdQue15"] + "|" + "Do you clench or grind your teeth?";
                        dtDentalHistory.Columns["rdQue16"].ColumnName = dtDentalHistory.Columns["rdQue16"] + "|" + "Does your jaw click or pop?";


                        dtDentalHistory.Columns["rdQue17"].ColumnName = dtDentalHistory.Columns["rdQue17"] + "|" + "Are you experiencing any pain or soreness in the muscles of your face?";
                        dtDentalHistory.Columns["rdQue18"].ColumnName = dtDentalHistory.Columns["rdQue18"] + "|" + "Do you have frequent headaches, neckaches or shoulder aches?";
                        dtDentalHistory.Columns["rdQue19"].ColumnName = dtDentalHistory.Columns["rdQue19"] + "|" + "Does food get caught in your teeth?";
                        dtDentalHistory.Columns["chkQue20"].ColumnName = dtDentalHistory.Columns["chkQue20"] + "|" + "Are any of your teeth sensitive to,Hot Cold Sweets Pressure ";
                        dtDentalHistory.Columns["rdQue21"].ColumnName = dtDentalHistory.Columns["rdQue21"] + "|" + "Do your gums bleed or hurt?";


                        dtDentalHistory.Columns["txtQue21a"].ColumnName = dtDentalHistory.Columns["txtQue21a"] + "|" + "When?";
                        dtDentalHistory.Columns["txtQue22"].ColumnName = dtDentalHistory.Columns["txtQue22"] + "|" + "How often do your brush your teeth?";

                        dtDentalHistory.Columns["txtQue23"].ColumnName = dtDentalHistory.Columns["txtQue23"] + "|" + "How often you use dental floss?";



                        dtDentalHistory.Columns["rdQue24"].ColumnName = dtDentalHistory.Columns["rdQue24"] + "|" + "Are any of your teeth loose, tipped, shifted or chipped?";

                        dtDentalHistory.Columns["txtQue26"].ColumnName = dtDentalHistory.Columns["txtQue26"] + "|" + "How do you feel about your teeth in general?";

                        dtDentalHistory.Columns["rdQue28"].ColumnName = dtDentalHistory.Columns["rdQue28"] + "|" + "Do you feel the patient breath is often offensive?";


                        dtDentalHistory.Columns["txtQue28a"].ColumnName = dtDentalHistory.Columns["txtQue28a"] + "|" + "What?";
                        dtDentalHistory.Columns["txtQue28b"].ColumnName = dtDentalHistory.Columns["txtQue28b"] + "|" + "Where?";
                        dtDentalHistory.Columns["txtQue28c"].ColumnName = dtDentalHistory.Columns["txtQue28c"] + "|" + "When?";
                        dtDentalHistory.Columns["txtQue29"].ColumnName = dtDentalHistory.Columns["txtQue29"] + "|" + "Explain any orthodonic work you have had done?";
                        dtDentalHistory.Columns["txtQue29a"].ColumnName = dtDentalHistory.Columns["txtQue29a"] + "|" + "Explain any dental work you have had done (Perio, Oral Surgery, etc.)";
                        dtDentalHistory.Columns["rdQue30"].ColumnName = dtDentalHistory.Columns["rdQue30"] + "|" + "Has the patient had any unpleasant dental experience or is there anything about dentistry that you strongly dislike?";



                        dtDentalHistory.Columns["txtComments"].ColumnName = dtDentalHistory.Columns["txtComments"] + "|" + "Comments";




                        dynamic dResult = new { dtDentalHistory };
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = dResult;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = "Doctor doesn't send you Dental history form.";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                }
                else
                {

                    objResponse.ResultCode = 400;
                    objResponse.StatusMessage = "Your Session has been logout.";

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.ResultCode = 400;
                objResponse.StatusMessage = "Server error occured - Please contact administrator.";
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name : DentalHistoryForm
        /// Desciprtion :This Method Use for Update Dental History Form details of Patient
        /// Request : SessionCode,txtQue1, txtQue2, txtQue3, txtQue4, txtQue5, txtQue5a, txtQue5b, txtQue5c, txtQue6, txtQue7, rdQue7a, rdQue8, txtQue9a, rdQue9, rdQue10, txtQue11aFixedbridge, txtQue11aAge, txtQue11bRemoveablebridge, txtQue11bAge, txtQue11cDenture, txtQue11cAge, rdQue11dImplant, txtQue11dAge, rdQue12, txtQue12a, rdQue13, rdQue14, txtQue14a, rdQue15, rdQue16, rdQue17, rdQue18, rdQue19, chkQue20_1, chkQue20_2, chkQue20_3, chkQue20_4, rdQue21, txtQue21a, txtQue22,  rdQue23, txtQue23a, rdQue24,  txtQue26,  rdQue28, txtQue28a, txtQue28b, txtQue28c, txtQue29, rdQue30,  txtComments, txtDigiSign
        /// Response : { Get Entire  Dental History Form Details }
        /// </summary>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string UpdateDentalHistoryForm(string SessionCode, string txtQue1, string txtQue2, string txtQue3, string txtQue4, string txtQue5, string txtQue5a, string txtQue5b, string txtQue5c, string txtQue6, string txtQue7, string rdQue7a, string rdQue8, string txtQue9a, string rdQue9, string rdQue10, string rdoQue11aFixedbridge, string rdoQue11bRemoveablebridge, string rdoQue11cDenture, string rdQue11dImplant, string txtQue12, string rdQue15, string rdQue16, string rdQue17, string rdQue18, string rdQue19, string chkQue20_1, string chkQue20_2, string chkQue20_3, string chkQue20_4, string rdQue21, string txtQue21a, string txtQue22, string txtQue23, string rdQue24, string txtQue26, string rdQue28, string txtQue28a, string txtQue28b, string txtQue28c, string txtQue29, string txtQue29a, string rdQue30, string txtComments)
        {
            string json = "";
            if (IsSessionExpired(SessionCode))
            {
                try
                {
                    DataTable tblDentalHistory = new DataTable("DentalHistory");
                    tblDentalHistory.Columns.Add("txtQue1");
                    tblDentalHistory.Columns.Add("txtQue2");
                    tblDentalHistory.Columns.Add("txtQue3");
                    tblDentalHistory.Columns.Add("txtQue4");
                    tblDentalHistory.Columns.Add("txtQue5");
                    tblDentalHistory.Columns.Add("txtQue5a");
                    tblDentalHistory.Columns.Add("txtQue5b");
                    tblDentalHistory.Columns.Add("txtQue5c");
                    tblDentalHistory.Columns.Add("txtQue6");
                    tblDentalHistory.Columns.Add("txtQue7");
                    tblDentalHistory.Columns.Add("rdQue7a");
                    tblDentalHistory.Columns.Add("rdQue8");
                    tblDentalHistory.Columns.Add("rdQue9");
                    tblDentalHistory.Columns.Add("txtQue9a");
                    tblDentalHistory.Columns.Add("rdQue10");
                    tblDentalHistory.Columns.Add("rdoQue11aFixedbridge");
                    tblDentalHistory.Columns.Add("rdoQue11bRemoveablebridge");
                    tblDentalHistory.Columns.Add("rdoQue11cDenture");
                    tblDentalHistory.Columns.Add("rdQue11dImplant");
                    tblDentalHistory.Columns.Add("txtQue12");
                    tblDentalHistory.Columns.Add("rdQue15");
                    tblDentalHistory.Columns.Add("rdQue16");
                    tblDentalHistory.Columns.Add("rdQue17");
                    tblDentalHistory.Columns.Add("rdQue18");
                    tblDentalHistory.Columns.Add("rdQue19");
                    tblDentalHistory.Columns.Add("chkQue20");
                    StringBuilder chkQue20 = new StringBuilder();




                    if (chkQue20_1 == "Y")
                    {
                        if (!String.IsNullOrEmpty(chkQue20.ToString()))
                        {
                            chkQue20.Append("," + "1");
                        }
                        else
                        {
                            chkQue20.Append("1");
                        }
                    }
                    if (chkQue20_2 == "Y")
                    {
                        if (!String.IsNullOrEmpty(chkQue20.ToString()))
                        {
                            chkQue20.Append("," + "2");
                        }
                        else
                        {
                            chkQue20.Append("2");
                        }
                    }
                    if (chkQue20_3 == "Y")
                    {
                        if (!String.IsNullOrEmpty(chkQue20.ToString()))
                        {
                            chkQue20.Append("," + "3");
                        }
                        else
                        {
                            chkQue20.Append("3");
                        }
                    }
                    if (chkQue20_4 == "Y")
                    {
                        if (!String.IsNullOrEmpty(chkQue20.ToString()))
                        {
                            chkQue20.Append("," + "4");
                        }
                        else
                        {
                            chkQue20.Append("4");
                        }
                    }
                    tblDentalHistory.Columns.Add("rdQue21");
                    tblDentalHistory.Columns.Add("txtQue21a");
                    tblDentalHistory.Columns.Add("txtQue22");

                    tblDentalHistory.Columns.Add("txtQue23");

                    tblDentalHistory.Columns.Add("rdQue24");

                    tblDentalHistory.Columns.Add("txtQue26");

                    tblDentalHistory.Columns.Add("rdQue28");
                    tblDentalHistory.Columns.Add("txtQue28a");
                    tblDentalHistory.Columns.Add("txtQue28b");
                    tblDentalHistory.Columns.Add("txtQue28c");
                    tblDentalHistory.Columns.Add("txtQue29");
                    tblDentalHistory.Columns.Add("txtQue29a");
                    tblDentalHistory.Columns.Add("rdQue30");

                    tblDentalHistory.Columns.Add("txtComments");

                    tblDentalHistory.Columns.Add("CreatedDate");
                    tblDentalHistory.Columns.Add("ModifiedDate");

                    tblDentalHistory.Rows.Add(txtQue1, txtQue2, txtQue3, txtQue4, txtQue5, txtQue5a, txtQue5b, txtQue5c,
                        txtQue6, txtQue7, rdQue7a, rdQue8, rdQue9, txtQue9a, rdQue10,
                        rdoQue11aFixedbridge, rdoQue11bRemoveablebridge, rdoQue11cDenture,
                        rdQue11dImplant, txtQue12, rdQue15, rdQue16, rdQue17, rdQue18,
                        rdQue19, chkQue20.ToString(), rdQue21, txtQue21a, txtQue22, txtQue23,
                         rdQue24, txtQue26, rdQue28, txtQue28a, txtQue28b, txtQue28c, txtQue29, txtQue29a, rdQue30, txtComments, System.DateTime.Now, System.DateTime.Now);


                    //Join Two Table
                    DataSet dsDentalHistory = new DataSet("DentalHistory");
                    dsDentalHistory.Tables.Add(tblDentalHistory);

                    DataSet ds = dsDentalHistory.Copy();
                    dsDentalHistory.Clear();

                    string DentalHistory = ds.GetXml();

                    bool status = objCommonDAL.UpdateDentalHistory(Convert.ToInt32(Session["PatientId"]), "UpdateDentalHistory", DentalHistory);
                    if (status)
                    {
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = "Form updated successfully.";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = "Failed Form update.";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }

                }
                catch (Exception ex)
                {
                    objResponse.ResultCode = 400;
                    objResponse.StatusMessage = "Server error occured - Please contact administrator.";
                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    status = objCommonDAL.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);

                }

            }
            else
            {
                objResponse.ResultCode = 400;
                objResponse.StatusMessage = "Your Session has been logout.";

                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
            }
            return json;

        }

        /// <summary>
        /// Method Name : GetMedaicalHistoryForm
        /// Desciprtion :This Method Use for Get Medaical History Form details of Patient
        /// Request : sessioncode
        /// Response : { Get Entire  Medical History Form Details }
        /// </summary>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string GetMedaicalHistoryForm(string SessionCode)
        {
            string json = "";

            DataTable dtMedicalHistory = new DataTable();

            try
            {
                SessionCode = Session["SessionCode"].ToString();

                if (IsSessionExpired(SessionCode))
                {

                    dtMedicalHistory = objCommonDAL.GetMedicalHistory(Convert.ToInt32(Session["PatientId"]), "GetMedicalHistory");
                    if (dtMedicalHistory != null && dtMedicalHistory.Rows.Count > 0)
                    {

                        dtMedicalHistory.Columns["txtPhysicianName"].ColumnName = dtMedicalHistory.Columns["txtPhysicianName"] + "|" + "Physician's Name";
                        dtMedicalHistory.Columns["txtPhysicianAddress"].ColumnName = dtMedicalHistory.Columns["txtPhysicianAddress"] + "|" + "Physician's Address";
                        dtMedicalHistory.Columns["txtPhysicianTel"].ColumnName = dtMedicalHistory.Columns["txtPhysicianTel"] + "|" + "Physician's Phone Number.";
                        dtMedicalHistory.Columns["rdQue3"].ColumnName = dtMedicalHistory.Columns["rdQue3"] + "|" + "Are you under a physician's care?";
                        dtMedicalHistory.Columns["txtSincewhen"].ColumnName = dtMedicalHistory.Columns["txtSincewhen"] + "|" + "Since when";
                        dtMedicalHistory.Columns["txtWhy"].ColumnName = dtMedicalHistory.Columns["txtWhy"] + "|" + "Why";
                        dtMedicalHistory.Columns["txtWhenwasyour"].ColumnName = dtMedicalHistory.Columns["txtWhenwasyour"] + "|" + "When was your last complete physical exam?";
                        dtMedicalHistory.Columns["rdQue4"].ColumnName = dtMedicalHistory.Columns["rdQue4"] + "|" + "Are you taking any medication or substances? (if yes, please list in comments section)";
                        dtMedicalHistory.Columns["rdQue5"].ColumnName = dtMedicalHistory.Columns["rdQue5"] + "|" + "Do you routinely take health related substances?";
                        dtMedicalHistory.Columns["rdQue6"].ColumnName = dtMedicalHistory.Columns["rdQue6"] + "|" + "Are you allergic to any medications or substances? (please list in comments section)";
                        dtMedicalHistory.Columns["rdQue7"].ColumnName = dtMedicalHistory.Columns["rdQue7"] + "|" + "Do you have any other allergies or hives?";
                        dtMedicalHistory.Columns["rdQue8"].ColumnName = dtMedicalHistory.Columns["rdQue8"] + "|" + "Do you have any problems with penicillin, antibiotics, anesthetics or other medications? (please list in comments section)";
                        dtMedicalHistory.Columns["rdQue9"].ColumnName = dtMedicalHistory.Columns["rdQue9"] + "|" + "Are you sensitive to any metals or latex?";
                        dtMedicalHistory.Columns["rdQue10"].ColumnName = dtMedicalHistory.Columns["rdQue10"] + "|" + "Are you pregnant or suspect you may be?";
                        dtMedicalHistory.Columns["rdQue11"].ColumnName = dtMedicalHistory.Columns["rdQue11"] + "|" + "Do you use any birth control medication?";
                        dtMedicalHistory.Columns["rdQue12"].ColumnName = dtMedicalHistory.Columns["rdQue12"] + "|" + "Have you ever been treated for or been told you might have heart disease?";
                        dtMedicalHistory.Columns["rdQue13"].ColumnName = dtMedicalHistory.Columns["rdQue13"] + "|" + "Do you have a pacemaker, an artificial heart valve implant, or been diagnosed with mitral valve prolapse?";
                        dtMedicalHistory.Columns["rdQue14"].ColumnName = dtMedicalHistory.Columns["rdQue14"] + "|" + "Have you ever had rheumatic fever?";
                        dtMedicalHistory.Columns["rdQue15"].ColumnName = dtMedicalHistory.Columns["rdQue15"] + "|" + "Are you aware of any heart murmurs?";
                        dtMedicalHistory.Columns["rdQue16"].ColumnName = dtMedicalHistory.Columns["rdQue16"] + "|" + "Do you have high or low blood pressure?";
                        dtMedicalHistory.Columns["rdQue17"].ColumnName = dtMedicalHistory.Columns["rdQue17"] + "|" + "Have you ever had a serious illness or major surgery?";
                        dtMedicalHistory.Columns["txtIfso"].ColumnName = dtMedicalHistory.Columns["txtIfso"] + "|" + "If so, explain";
                        dtMedicalHistory.Columns["rdQue18"].ColumnName = dtMedicalHistory.Columns["rdQue18"] + "|" + "Have you ever had radiation treatment, chemo treatment for tumor, growth or other condition?";
                        dtMedicalHistory.Columns["rdQue19"].ColumnName = dtMedicalHistory.Columns["rdQue19"] + "|" + "Have you ever taken Fosamax, Zometa, Aredia or any other oral or intravenous treatment (bisphosphonates) for bone tumors, excessive calcium in your blood, or osteoporosis?";
                        dtMedicalHistory.Columns["rdQue20"].ColumnName = dtMedicalHistory.Columns["rdQue20"] + "|" + "Do you have inflammatory diseases, such as arthritis or rheumatism?";
                        dtMedicalHistory.Columns["rdQue21"].ColumnName = dtMedicalHistory.Columns["rdQue21"] + "|" + "Do you have any artificial joints/prosthesis?";
                        dtMedicalHistory.Columns["rdQue22"].ColumnName = dtMedicalHistory.Columns["rdQue22"] + "|" + "Do you have any blood disorders, such as anemia, leukemia, etc?";
                        dtMedicalHistory.Columns["rdQue23"].ColumnName = dtMedicalHistory.Columns["rdQue23"] + "|" + "Have you ever bled excessively after being cut or injured?";
                        dtMedicalHistory.Columns["rdQue24"].ColumnName = dtMedicalHistory.Columns["rdQue24"] + "|" + "Do you have any stomach problems?";
                        dtMedicalHistory.Columns["rdQue25"].ColumnName = dtMedicalHistory.Columns["rdQue25"] + "|" + "Do you have any kidney problems?";
                        dtMedicalHistory.Columns["rdQue26"].ColumnName = dtMedicalHistory.Columns["rdQue26"] + "|" + "Do you have any liver problems?";
                        dtMedicalHistory.Columns["rdQue27"].ColumnName = dtMedicalHistory.Columns["rdQue27"] + "|" + "Are you diabetic?";
                        dtMedicalHistory.Columns["rdQue28"].ColumnName = dtMedicalHistory.Columns["rdQue28"] + "|" + "Do you have fainting or dizzy spells?";
                        dtMedicalHistory.Columns["rdQue29"].ColumnName = dtMedicalHistory.Columns["rdQue29"] + "|" + "Do you have asthma?";
                        dtMedicalHistory.Columns["rdQue30"].ColumnName = dtMedicalHistory.Columns["rdQue30"] + "|" + "Do you have epilepsy or seizure disorders?";
                        dtMedicalHistory.Columns["rdQue31"].ColumnName = dtMedicalHistory.Columns["rdQue31"] + "|" + "Do you or have you had venereal disease?";
                        dtMedicalHistory.Columns["rdQue32"].ColumnName = dtMedicalHistory.Columns["rdQue32"] + "|" + "Have you tested HIV positive?";
                        dtMedicalHistory.Columns["rdQue33"].ColumnName = dtMedicalHistory.Columns["rdQue33"] + "|" + "Do you have AIDS?";
                        dtMedicalHistory.Columns["rdQue34"].ColumnName = dtMedicalHistory.Columns["rdQue34"] + "|" + "Have you had or do you test positive for hepatitis?";
                        dtMedicalHistory.Columns["rdQue35"].ColumnName = dtMedicalHistory.Columns["rdQue35"] + "|" + "Do you or have you had T.B.?";
                        dtMedicalHistory.Columns["rdQue36"].ColumnName = dtMedicalHistory.Columns["rdQue36"] + "|" + "Do you smoke, chew, use snuff or any other forms of tobacco?";
                        dtMedicalHistory.Columns["rdQue37"].ColumnName = dtMedicalHistory.Columns["rdQue37"] + "|" + "Do you regularly consume more then one or two alcoholic beverages a day?";
                        dtMedicalHistory.Columns["rdQue38"].ColumnName = dtMedicalHistory.Columns["rdQue38"] + "|" + "Do you habitually use controlled substances?";
                        dtMedicalHistory.Columns["rdQue39"].ColumnName = dtMedicalHistory.Columns["rdQue39"] + "|" + "Have you had any psychiatric treatment?";
                        dtMedicalHistory.Columns["rdQue40"].ColumnName = dtMedicalHistory.Columns["rdQue40"] + "|" + "Have you taken any prescription drugs fenfluramine, fenfluramine combined with phentermine (fen-phen), dexfenfluramine (redux), or other weight loss products?";
                        dtMedicalHistory.Columns["txtDoyouhaveanydisease"].ColumnName = dtMedicalHistory.Columns["txtDoyouhaveanydisease"] + "|" + "Do you have any disease condition, or problem not listed? If so, explain";
                        dtMedicalHistory.Columns["txtIsthereanythingelse"].ColumnName = dtMedicalHistory.Columns["txtIsthereanythingelse"] + "|" + "Is there anything else we should know about your health that we have not covered in this form?";
                        dtMedicalHistory.Columns["rdQue43"].ColumnName = dtMedicalHistory.Columns["rdQue43"] + "|" + "Would you like to speak to the Doctor privately about any problem?";
                        dtMedicalHistory.Columns["txtComments"].ColumnName = dtMedicalHistory.Columns["txtComments"] + "|" + "Additional Information";



                        dynamic dResult = new { dtMedicalHistory };
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = dResult;

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = "Doctor doesn't send you Medical history form.";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                }
                else
                {

                    objResponse.ResultCode = 400;
                    objResponse.StatusMessage = "Your Session has been logout.";

                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);

                }


                return json;
            }
            catch (Exception ex)
            {
                objResponse.ResultCode = 400;
                objResponse.StatusMessage = "Server error occured - Please contact administrator.";
                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                status = objCommonDAL.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                return json;
            }
        }

        /// <summary>
        /// Method Name : UpdateMedicalHistoryForm
        /// Desciprtion :This Method Use for Update MedicalHistory Form details of Patient
        /// Request : Sessioncode,txtPhysicianName, txtPhysicianAddress, txtPhysicianTel, rdQue3, txtSincewhen, txtWhy, txtWhenwasyour, rdQue4, rdQue5, rdQue6, rdQue7, rdQue8, rdQue9, rdQue10, rdQue11, rdQue12, rdQue13, rdQue14, rdQue15, rdQue16, rdQue17, txtIfso, rdQue18, rdQue19, rdQue20, rdQue21, rdQue22, rdQue23, rdQue24,  rdQue26, rdQue27, rdQue28, rdQue29, rdQue30, rdQue31, rdQue32, rdQue33, rdQue34, rdQue35, rdQue36, rdQue37, rdQue38, rdQue39, rdQue40, txtDoyouhaveanydisease, txtIsthereanythingelse, rdQue43, txtComments, txtDigiSign
        /// Response : { Get Entire  Medical History Form Details }
        /// </summary>
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string UpdateMedicalHistoryForm(string Sessioncode, string txtPhysicianName, string txtPhysicianAddress, string txtPhysicianTel, string rdQue3, string txtSincewhen, string txtWhy, string txtWhenwasyour, string rdQue4, string rdQue5, string rdQue6, string rdQue7, string rdQue8, string rdQue9, string rdQue10, string rdQue11, string rdQue12, string rdQue13, string rdQue14, string rdQue15, string rdQue16, string rdQue17, string txtIfso, string rdQue18, string rdQue19, string rdQue20, string rdQue21, string rdQue22, string rdQue23, string rdQue24, string rdQue25, string rdQue26, string rdQue27, string rdQue28, string rdQue29, string rdQue30, string rdQue31, string rdQue32, string rdQue33, string rdQue34, string rdQue35, string rdQue36, string rdQue37, string rdQue38, string rdQue39, string rdQue40, string txtDoyouhaveanydisease, string txtIsthereanythingelse, string rdQue43, string txtComments, string txtDigiSign)
        {
            string json = "";
            if (IsSessionExpired(Sessioncode))
            {
                try
                {
                    // Dental History Table
                    DataTable tblMedicalHistory = new DataTable("MedicalHistory");
                    tblMedicalHistory.Columns.Add("txtPhysicianName");
                    tblMedicalHistory.Columns.Add("txtPhysicianAddress");
                    tblMedicalHistory.Columns.Add("txtPhysicianTel");
                    tblMedicalHistory.Columns.Add("rdQue3");
                    tblMedicalHistory.Columns.Add("txtSincewhen");
                    tblMedicalHistory.Columns.Add("txtWhy");
                    tblMedicalHistory.Columns.Add("txtWhenwasyour");
                    tblMedicalHistory.Columns.Add("rdQue4");
                    tblMedicalHistory.Columns.Add("rdQue5");
                    tblMedicalHistory.Columns.Add("rdQue6");
                    tblMedicalHistory.Columns.Add("rdQue7");
                    tblMedicalHistory.Columns.Add("rdQue8");
                    tblMedicalHistory.Columns.Add("rdQue9");
                    tblMedicalHistory.Columns.Add("rdQue10");
                    tblMedicalHistory.Columns.Add("rdQue11");
                    tblMedicalHistory.Columns.Add("rdQue12");
                    tblMedicalHistory.Columns.Add("rdQue13");
                    tblMedicalHistory.Columns.Add("rdQue14");
                    tblMedicalHistory.Columns.Add("rdQue15");
                    tblMedicalHistory.Columns.Add("rdQue16");
                    tblMedicalHistory.Columns.Add("rdQue17");
                    tblMedicalHistory.Columns.Add("txtIfso");
                    tblMedicalHistory.Columns.Add("rdQue18");
                    tblMedicalHistory.Columns.Add("rdQue19");
                    tblMedicalHistory.Columns.Add("rdQue20");
                    tblMedicalHistory.Columns.Add("rdQue21");
                    tblMedicalHistory.Columns.Add("rdQue22");
                    tblMedicalHistory.Columns.Add("rdQue23");
                    tblMedicalHistory.Columns.Add("rdQue24");
                    tblMedicalHistory.Columns.Add("rdQue25");
                    tblMedicalHistory.Columns.Add("rdQue26");
                    tblMedicalHistory.Columns.Add("rdQue27");
                    tblMedicalHistory.Columns.Add("rdQue28");
                    tblMedicalHistory.Columns.Add("rdQue29");
                    tblMedicalHistory.Columns.Add("rdQue30");
                    tblMedicalHistory.Columns.Add("rdQue31");
                    tblMedicalHistory.Columns.Add("rdQue32");
                    tblMedicalHistory.Columns.Add("rdQue33");
                    tblMedicalHistory.Columns.Add("rdQue34");
                    tblMedicalHistory.Columns.Add("rdQue35");
                    tblMedicalHistory.Columns.Add("rdQue36");
                    tblMedicalHistory.Columns.Add("rdQue37");
                    tblMedicalHistory.Columns.Add("rdQue38");
                    tblMedicalHistory.Columns.Add("rdQue39");
                    tblMedicalHistory.Columns.Add("rdQue40");
                    tblMedicalHistory.Columns.Add("txtDoyouhaveanydisease");
                    tblMedicalHistory.Columns.Add("txtIsthereanythingelse");
                    tblMedicalHistory.Columns.Add("rdQue43");
                    tblMedicalHistory.Columns.Add("txtComments");
                    tblMedicalHistory.Columns.Add("txtDigiSign");
                    tblMedicalHistory.Columns.Add("CreatedDate");
                    tblMedicalHistory.Columns.Add("ModifiedDate");


                    tblMedicalHistory.Rows.Add(txtPhysicianName, txtPhysicianAddress, txtPhysicianTel, rdQue3, txtSincewhen, txtWhy, txtWhenwasyour,
                                               rdQue4, rdQue5, rdQue6, rdQue7, rdQue8, rdQue9, rdQue10,
                                               rdQue11, rdQue12, rdQue13, rdQue14, rdQue15, rdQue16,
                                                rdQue17, txtIfso, rdQue18, rdQue19, rdQue20, rdQue21, rdQue22,
                                                rdQue23, rdQue24, rdQue25, rdQue26, rdQue27, rdQue28, rdQue29,
                                                rdQue30, rdQue31, rdQue32, rdQue33, rdQue34, rdQue35, rdQue36,
                                                rdQue37, rdQue38, rdQue39, rdQue40, txtDoyouhaveanydisease, txtIsthereanythingelse,
                                                rdQue43, txtComments, txtDigiSign, System.DateTime.Now, System.DateTime.Now);



                    DataSet dsMedicalHistory = new DataSet("MedicalHistory");
                    dsMedicalHistory.Tables.Add(tblMedicalHistory);

                    DataSet ds = dsMedicalHistory.Copy();
                    dsMedicalHistory.Clear();

                    string MedicalHistory = ds.GetXml();

                    bool status = objCommonDAL.UpdateMedicalHistory(Convert.ToInt32(Session["PatientId"]), "UpdateMedicalHistory", MedicalHistory);
                    if (status)
                    {
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = "Form updated successfully.";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }
                    else
                    {
                        objResponse.ResultCode = 200;
                        objResponse.ResultDetail = "Failed Form update.";

                        json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    }


                }
                catch (Exception ex)
                {
                    objResponse.ResultCode = 400;
                    objResponse.StatusMessage = "Server error occured - Please contact administrator.";
                    json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
                    status = objCommonDAL.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);

                }


            }
            else
            {
                objResponse.ResultCode = 400;
                objResponse.StatusMessage = "Your Session has been logout.";

                json = JsonConvert.SerializeObject(objResponse, Formatting.Indented);
            }
            return json;

        }







        /// <summary>
        /// Method Name : GetSpeacilityOfDoctor
        /// Desciprtion :This Method Use for Get Speacilities Of Doctor
        /// Request : DoctorId
        /// Response : {  "SpecialityId : 45","Description : Cosmetic Dentist" }
        /// </summary>



        /// <summary>
        /// Method Name :IsSessionExpired
        /// Desciprtion :This Method Use for Check Session States of Patient
        /// Request : sessioncode
        /// Response : { True }
        /// </summary>
        /// 

        public bool IsSessionExpired(string sessioncode)
        {
            bool Result = false;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["SessionCode"])) && Convert.ToString(Session["SessionCode"]) == sessioncode)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["PatientId"])))
                {
                    Result = true;
                }
                else
                {
                    Result = false;
                }
            }
            else
            {
                Result = false;
            }
            return Result;

        }





    }
}
