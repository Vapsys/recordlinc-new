﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Web.Script.Serialization;

using System.IO;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using DentalFiles.Models;
using System.Configuration;
using JQueryDataTables.Models.Repository;




public class Handler : IHttpHandler, System.Web.SessionState.IRequiresSessionState
{
    int DoctorId = 0;
    CommonDAL objCommonDAL = new CommonDAL();
    //HttpServerUtility Server = new HttpServerUtility();
    public void ProcessRequest(HttpContext context)
    {
        context.Response.ContentType = "text/plain";//"application/json";
        int userid = Convert.ToInt32(context.Session["PatientId"].ToString());


        try
        {
            HttpPostedFile postedFile = context.Request.Files["Filedata"];
            string Sesionid = context.Session["PatientId"].ToString();

            int RecordId = 0;


            var r = new System.Collections.Generic.List<ViewDataUploadFilesResult>();
            JavaScriptSerializer js = new JavaScriptSerializer();
            for (int i = 0; i < context.Request.Files.Count; i++ )
            {

                HttpPostedFile hpf = context.Request.Files[i] as HttpPostedFile;
                string FileName = string.Empty;
                if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
                {
                    string[] files = hpf.FileName.Split(new char[] { '\\' });
                    FileName = files[files.Length - 1];
                }
                else
                {
                    FileName = hpf.FileName;
                    string[] files = hpf.FileName.Split(new char[] { '\\' });
                    FileName = files[files.Length - 1];
                }
                if (FileName.Contains("/"))
                {
                    string[] files = hpf.FileName.Split(new char[] { '/' });
                    FileName = files[files.Length - 1];
                }
                string filename1 = System.Configuration.ConfigurationManager.AppSettings.Get("imagebankpath") + "$" + System.DateTime.Now.Ticks + "$" + FileName;

                string thambnailimage = filename1.Replace(System.Configuration.ConfigurationManager.AppSettings.Get("imagebankpath"), System.Configuration.ConfigurationManager.AppSettings.Get("imagebankpathThumb"));

                string extension;
                if (hpf.ContentLength == 0)
                    continue;


                extension = Path.GetExtension(FileName);


                if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd" && extension.ToLower() != ".tif" && extension.ToLower() != ".pspimage" && extension.ToLower() != ".yuv" && extension.ToLower() != ".thm" && extension.ToLower() != ".dcm")
                {

                    RecordId = objCommonDAL.Insert_UploadedFiles(filename1, Convert.ToInt32(Sesionid.Split('?')[0]), "");
                }
                else
                {

                    RecordId = objCommonDAL.Insert_UploadedImages(filename1, Convert.ToInt32(Sesionid.Split('?')[0]), "");


                }

                int PatientId = Convert.ToInt32(Sesionid);
                int MontageId = 0;
                int Monatgetype = 0;
                int imageId = 0;
                int UploadImageId = 0;


                DataTable DtPatientInfo = objCommonDAL.PatientHistory(PatientId);
                if (DtPatientInfo != null && DtPatientInfo.Rows.Count != 0)
                {
                    if (DtPatientInfo.Rows[0]["column1"] != null)
                        DoctorId = Convert.ToInt32(DtPatientInfo.Rows[0]["column1"]);

                }


                DataTable dtowner = objCommonDAL.GetOwnerId(DoctorId);
                int ownerId = (int)dtowner.Rows[0]["OwnerId"];
                int ownertype = 0; int images;

                
                DataTable dtimageupload = objCommonDAL.Get_UploadedImages(RecordId);
                DataTable dtfileupload = objCommonDAL.Get_UploadedFiles(RecordId);


                DataTable dtPatientImages = objCommonDAL.getImageRecordsIfPatients(PatientId);
                if (dtPatientImages.Rows.Count != 0)
                {
                    MontageId = Convert.ToInt32(dtPatientImages.Rows[0]["MontageId"]);
                }
                if (MontageId == 0)
                {

                    string MontageName = PatientId + "Montage";
                    int montageid = Convert.ToInt32(objCommonDAL.AddMontage(MontageName, Monatgetype, PatientId, ownerId, ownertype));

                    for (images = 0; images < dtimageupload.Rows.Count; images++)
                    {

                        string fullpath = HttpContext.Current.Server.MapPath(dtimageupload.Rows[images]["Name"].ToString().Replace("..", ""));
                        string filename = Convert.ToString(dtimageupload.Rows[images]["Name"]).Replace(ConfigurationManager.AppSettings.Get("imagebankpath"), "");
                        int Imagetype;
                        if (dtimageupload.Rows[images]["Category"].ToString() != "Get Category" && dtimageupload.Rows[images]["Category"].ToString() != "")
                        {
                            Imagetype = Convert.ToInt32(dtimageupload.Rows[images]["Category"]);
                        }
                        else
                        {
                            Imagetype = 500;
                        }
                        int imageid = objCommonDAL.AddImagetoPatient(PatientId, 1, filename, 1000, 1000, 0, Imagetype, fullpath, dtimageupload.Rows[images]["Name"].ToString(), ownerId, ownertype, 1, "Patient");
                        if (imageid > 0)
                        {

                            bool resmonimg = objCommonDAL.AddImagetoMontage(imageid, Imagetype, montageid);
                        }


                    }

                   
                    foreach (DataRow dentaldoc in dtfileupload.Rows)
                    {
                        int Docstatus = objCommonDAL.AddDocumenttoMontage(montageid, dentaldoc["DocumentName"].ToString(), "Patient");
                    }
                   
                }

                else if (MontageId != 0)
                {
                    foreach (DataRow MontageImage in dtimageupload.Rows)
                    {

                        if (UploadImageId == 0)
                        {


                            string fullpath = HttpContext.Current.Server.MapPath(MontageImage["Name"].ToString().Replace("..", ""));

                            string imagename = Convert.ToString(MontageImage["Name"]).Replace(ConfigurationManager.AppSettings.Get("imagebankpath"), "");
                            int Imagetype;
                            if (MontageImage["Category"].ToString() != "Get Category" && MontageImage["Category"].ToString() != "")
                            {
                                Imagetype = Convert.ToInt32(MontageImage["Category"]);
                            }
                            else
                            {
                                Imagetype = 500;
                            }
                            imageId = objCommonDAL.AddImagetoPatient(PatientId, 1, imagename, 1000, 1000, 0, Imagetype, fullpath, MontageImage["Name"].ToString(), ownerId, ownertype, 1, "Patient");
                            if (imageId > 0)
                            {
                                bool resmonimg = objCommonDAL.AddImagetoMontage(imageId, Imagetype, MontageId);

                            }
                        }


                    }

                    
                    foreach (DataRow dentaldoc in dtfileupload.Rows)
                    {
                        int Docstatus = objCommonDAL.AddDocumenttoMontage(MontageId, dentaldoc["DocumentName"].ToString(), "Patient");
                    }
                  
                }
               
                string savepath = "";
                string tempPath = "";
                tempPath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
                savepath = context.Server.MapPath(tempPath);
                savepath = savepath.Replace("PublicProfile\\", "");
                hpf.SaveAs(savepath + @"\" + filename1);



                bool status = objCommonDAL.InsertErrorLog("", "TempPath:->" + tempPath, "SavePath:->" + savepath);
                if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd" && extension.ToLower() != ".pspimage" && extension.ToLower() != ".thm" && extension.ToLower() != ".tif")
                { }
                else
                {

                    string savepathThumb = "";
                    string tempPathThumb = "";
                    tempPathThumb = System.Configuration.ConfigurationManager.AppSettings["FolderPathThumb"];

                    savepathThumb = context.Server.MapPath(tempPathThumb);



                    // Load image.
                    Image image = Image.FromFile(savepath + @"\" + filename1);

                    // Compute thumbnail size.
                    Size thumbnailSize = objCommonDAL.GetThumbnailSize(image);

                    // Get thumbnail.
                    Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                        thumbnailSize.Height, null, IntPtr.Zero);

                    // Save thumbnail.
                    thumbnail.Save(savepathThumb + @"\" + thambnailimage);

                }


                r.Add(new ViewDataUploadFilesResult()
                {
                    //Thumbnail_url = savedFileName,
                    Name = FileName,
                    Length = hpf.ContentLength,
                    Type = hpf.ContentType
                });
                var uploadedFiles = new
                {
                    files = r.ToArray()
                };
                var jsonObj = js.Serialize(uploadedFiles);
              
                context.Response.Write(jsonObj.ToString());
            }
            
        }
        catch { }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
  
    

}

public class ViewDataUploadFilesResult
{
    public string Thumbnail_url { get; set; }
    public string Name { get; set; }
    public int Length { get; set; }
    public string Type { get; set; }
}