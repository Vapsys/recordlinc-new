﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Models;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using System.Data;
using System.Xml;
using ArbApiSample;
using System.Net;
using System.IO;
using System.Configuration;
using BO.ViewModel;
using static BO.Enums.Common;
using System.Xml.Serialization;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public class PaymentBLL
    {
        static string _apiUrl = ConfigurationManager.AppSettings["recurring_apiurl"];
        static string _userLoginName = ConfigurationManager.AppSettings["x_login"];
        static string _transactionKey = ConfigurationManager.AppSettings["x_tran_key"];
        clsTemplate ObjTemplate = new clsTemplate();
        public static string CallSubscription(PaymentDetail Obj)
        {
            try
            {
                clsColleaguesData objcls = new clsColleaguesData();
                TripleDESCryptoHelper objtriple = new TripleDESCryptoHelper();
                DataTable dt = objcls.getNewPaymentHistoryOnUserId(SafeValue<int>(Obj.Userid));
                int subId = 0;
                bool hasSubscription = false;
                string createResponse = string.Empty;
                string updateResponse = string.Empty;
                string cancelResponse = string.Empty;
                int membershipId = 0;
                string strReturn = string.Empty;
                if (dt != null && dt.Rows.Count > 0)
                {
                    membershipId = SafeValue<int>(dt.Rows[0]["membershipId"]);
                    subId = SafeValue<int>(dt.Rows[0]["subscriptionId"]);
                    hasSubscription = true;
                }
                if (hasSubscription)
                {
                    //if (SafeValue<int>(objPyamentDetails.Planes) > membershipId || SafeValue<int>(objPyamentDetails.Planes) < membershipId)
                    //{
                    cancelResponse = new PaymentBLL().cancelSubscriptionBySystem(SafeValue<int>(subId));
                    bool isCancelUpdate = false;
                    if (!string.IsNullOrEmpty(cancelResponse))
                    {
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(cancelResponse);

                        XmlNodeList grandelemList = xmlDoc.GetElementsByTagName("messages");
                        if (grandelemList.Count > 0)
                        {
                            string code = grandelemList[0]["resultCode"].InnerText;
                            if (code == "Ok")
                            {
                                clsColleaguesData objCCData = new clsColleaguesData();
                                var isUpdate = objCCData.UpdateSubscriptionStatus(SafeValue<int>(Obj.Userid), SafeValue<string>(subId));
                                isCancelUpdate = true;
                            }
                        }
                    }
                    if (isCancelUpdate)
                    {
                        createResponse = new PaymentBLL().createSubscriptionBySystem(Obj);
                    }
                }
                else
                {
                    createResponse = new PaymentBLL().createSubscriptionBySystem(Obj);
                }
                if (!string.IsNullOrEmpty(createResponse))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(createResponse);

                    XmlNodeList grandelemList = xmlDoc.GetElementsByTagName("messages");
                    XmlNodeList elemList = xmlDoc.GetElementsByTagName("message");
                    XmlNodeList getSubscriptionName = xmlDoc.GetElementsByTagName("subscriptionId");
                    XmlNodeList getCustomerProfile = xmlDoc.GetElementsByTagName("profile");
                    if (grandelemList.Count > 0)
                    {
                        string code = grandelemList[0]["resultCode"].InnerText;
                        if (code != "Ok")
                        {
                            new clsTemplate().RecurringPaymentFailed(Obj.Email, Obj.Firstname, Obj.Lastname, Obj.Phone, elemList[0]["text"].InnerText);
                        }
                        else
                        {
                            if (getSubscriptionName.Count > 0 && getCustomerProfile.Count > 0)
                            {
                                string customerId = getCustomerProfile[0]["customerProfileId"].InnerText;
                                string subscriptionId = getSubscriptionName[0].InnerText;
                                clsColleaguesData objCCData = new clsColleaguesData();
                                string amount = Obj.Amount;
                                string suscriptionType = Obj.payInterval;
                                var isInsert = objCCData.NewInsertUsertransactionDetails(SafeValue<int>(Obj.Userid), amount,SafeValue<string>(Obj.TransactionId), Obj.Planes, customerId, DateTime.Now, suscriptionType, subscriptionId);
                                objCCData.UpdateMembershipByRecurring(Obj.Userid, Obj.Planes);
                                strReturn = "Done";
                                //This Code is used only for the I Luv My Dentist...
                                if (Obj.IsLuvMyDentist)
                                {
                                    Obj.PackageStartDate = System.DateTime.Now;
                                    Obj.PackageEndDate = GetPackageEndDate(Obj.payInterval);

                                    objCCData.InsertTransactionDetails(SafeValue<int>(Obj.Userid), Obj.PackageStartDate, Obj.PackageEndDate, SafeValue<decimal>(amount), true, Obj.CardType, Obj.CardNumber, Obj.expdate, SafeValue<string>(Obj.TransactionId), Obj.Cvv, true);
                                    objCCData.UpdatePaymentStatusOfExistingUser(SafeValue<int>(Obj.Userid), true);
                                    string CardNumberWithMask = Obj.CardNumber;

                                    CardNumberWithMask = CardNumberWithMask.Substring(CardNumberWithMask.Length - 4).PadLeft(CardNumberWithMask.Length, '*');

                                    string CardType = GetCardType(Obj.CardType);
                                    new clsTemplate().AccountUpgradedSuccessfully(SafeValue<int>(Obj.Userid), CardNumberWithMask, CardType, SafeValue<string>(Obj.TransactionId), Obj.Amount);
                                }
                            }
                        }
                    }
                }
                return strReturn;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PaymentBLL-CallSubscription", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static string GetCardType(string CardCode)
        {
            switch (CardCode)
            {
                case "A": return "American Express";
                case "C": return "Diners Club";
                case "D": return "Discover";
                case "J": return "JCB";
                case "M": return "Mastercard";
                case "V": return "Visa";
                case "CC": return "Credit Card";
                default: return "";
            }
        }
        public static DateTime GetPackageEndDate(string PlanInterval)
        {
            DateTime dttm = new DateTime();
            dttm = System.DateTime.Now;
            switch (PlanInterval)
            {

                case "1": return dttm.AddMonths(1);
                case "12": return dttm.AddYears(1);
                case "24": return dttm.AddYears(2);
                case "36": return dttm.AddYears(3);
                default: return dttm;
            }
        }
        //update subscription
        public string UpdateSubscriptionBySystem(PaymentDetail objPaymentDetails, string subId)
        {
            ARBUpdateSubscriptionRequest updateARBUpdateSubscriptionRequest = new ARBUpdateSubscriptionRequest();
            PopulateMerchantAuthentication(updateARBUpdateSubscriptionRequest);
            updateARBUpdateSubscriptionRequest.subscriptionId = SafeValue<string>(subId);
            PopulateUpdateSubscriptionBySystem(updateARBUpdateSubscriptionRequest, objPaymentDetails);
            XmlDocument xmldoc = null;
            PostRequestBySystem(updateARBUpdateSubscriptionRequest, out xmldoc);
            string resp = PostRequestBySystem(updateARBUpdateSubscriptionRequest, out xmldoc);
            return resp;
        }
        private static void PopulateMerchantAuthentication(ANetApiRequest request)
        {
            request.merchantAuthentication = new merchantAuthenticationType();
            request.merchantAuthentication.name = _userLoginName;
            request.merchantAuthentication.transactionKey = _transactionKey;
            request.refId = ConfigurationManager.AppSettings["refid"];
        }
        public string PostRequestBySystem(object apiRequest, out XmlDocument xmldoc)
        {
            // bool bResult = false;
            XmlSerializer serializer;

            xmldoc = null;
            String post_response = null;
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(_apiUrl);
                webRequest.Method = "POST";
                webRequest.ContentType = "text/xml";
                webRequest.KeepAlive = false;
                webRequest.ProtocolVersion = HttpVersion.Version10;
                webRequest.ServicePoint.ConnectionLimit = 1;
                System.Net.ServicePointManager.Expect100Continue = false;
                SecurityProtocolType origSecurityProtocol = System.Net.ServicePointManager.SecurityProtocol;
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                // Serialize the request
                serializer = new XmlSerializer(apiRequest.GetType());
                XmlWriter writer = new XmlTextWriter(webRequest.GetRequestStream(), Encoding.UTF8);
                serializer.Serialize(writer, apiRequest);
                writer.Close();



                // Get the response                
                //ARBGetSubscriptionResponse getsub = new ARBGetSubscriptionResponse();
                HttpWebResponse webResponse = (HttpWebResponse)webRequest.GetResponse();
                using (StreamReader responseStream = new StreamReader(webResponse.GetResponseStream()))
                {
                    post_response = responseStream.ReadToEnd();
                    responseStream.Close();
                }
                string[] response_array = post_response.Split('|');
                if(webResponse.StatusCode == HttpStatusCode.OK)
                {

                }
                //bResult = true;
            }
            catch (Exception ex)
            {
                post_response = null;
                //bResult = false;
            }

            return post_response;
        }
        public string cancelSubscriptionBySystem(int subId)
        {
            ARBCancelSubscriptionRequest cancelARBCancelSubscriptionRequest = new ARBCancelSubscriptionRequest();
            cancelARBCancelSubscriptionRequest.subscriptionId = SafeValue<string>(subId);
            PopulateMerchantAuthenticationBySystem(cancelARBCancelSubscriptionRequest);
            XmlDocument xmldoc = null;
            string resp = PostRequestBySystem(cancelARBCancelSubscriptionRequest, out xmldoc);
            return resp;
        }
        private static void PopulateMerchantAuthenticationBySystem(ANetApiRequest request)
        {
            request.merchantAuthentication = new merchantAuthenticationType();
            request.merchantAuthentication.name = _userLoginName;
            request.merchantAuthentication.transactionKey = _transactionKey;
            request.refId = ConfigurationManager.AppSettings["refid"];
        }
        //Update Subscription
        private static void PopulateUpdateSubscriptionBySystem(ARBUpdateSubscriptionRequest request, PaymentDetail objPyamentDetails)
        {
            try
            {

                clsColleaguesData ObjColleaguesData = new clsColleaguesData();
                ARBSubscriptionType sub = new ARBSubscriptionType();
                creditCardType creditCard = new creditCardType();

                DataTable dt = ObjColleaguesData.GetDoctorDetailsByEmail(objPyamentDetails.Email);
                if (dt.Rows.Count > 0)
                {
                    objPyamentDetails.Firstname = SafeValue<string>(dt.Rows[0]["FirstName"]);
                    objPyamentDetails.Lastname = SafeValue<string>(dt.Rows[0]["LastName"]);
                    objPyamentDetails.Address = SafeValue<string>(dt.Rows[0]["ExactAddress"]);
                    objPyamentDetails.City = SafeValue<string>(dt.Rows[0]["City"]);
                    objPyamentDetails.State = SafeValue<string>(dt.Rows[0]["State"]);
                    objPyamentDetails.Zip = SafeValue<string>(dt.Rows[0]["ZipCode"]);
                    objPyamentDetails.Phone = SafeValue<string>(dt.Rows[0]["Phone"]);
                }
                if (objPyamentDetails.IsLuvMyDentist)
                {
                    sub.name = "I Luv My Dentist Subscription";
                }
                else
                {
                    sub.name = "Recordlinc Subscription";
                }
                

                creditCard.cardNumber = objPyamentDetails.CardNumber;
                creditCard.expirationDate = objPyamentDetails.expdate;  // required format for API is YYYY-MM
                sub.payment = new paymentType();
                sub.payment.Item = creditCard;

                sub.billTo = new nameAndAddressType();
                sub.billTo.firstName = objPyamentDetails.Firstname;
                sub.billTo.lastName = objPyamentDetails.Lastname;
                sub.billTo.address = objPyamentDetails.Address;
                sub.billTo.city = objPyamentDetails.City;
                sub.billTo.state = objPyamentDetails.State;
                sub.billTo.zip = objPyamentDetails.Zip;

                string firstAmount = GetPlanAmount(SafeValue<int>(objPyamentDetails.Planes));
                decimal totalAmount = 0.0M;
                if (objPyamentDetails.payInterval == "1")
                {
                    totalAmount = SafeValue<decimal>(firstAmount + ".00");
                }
                if (objPyamentDetails.payInterval == "3")
                {
                    var amt = SafeValue<string>(SafeValue<int>(firstAmount) * 3);
                    totalAmount = SafeValue<decimal>(amt + ".00");
                }
                if (objPyamentDetails.payInterval == "6")
                {
                    var amt = SafeValue<string>(SafeValue<int>(firstAmount) * 6);
                    totalAmount = SafeValue<decimal>(amt + ".00");
                }
                if (objPyamentDetails.payInterval == "12")
                {
                    var amt = SafeValue<string>(SafeValue<int>(firstAmount) * 12);
                    totalAmount = SafeValue<decimal>(amt + ".00");
                }
                sub.amount = totalAmount; //47.00M;
                sub.amountSpecified = true;

                // Create a subscription that is 12 monthly payments starting on Jan 1, 2019
                // with 1 month free (trial period)

                sub.paymentSchedule = new paymentScheduleType();
                DateTime date = System.DateTime.Now;
                sub.paymentSchedule.startDate = date.AddMonths(0);
                sub.paymentSchedule.startDateSpecified = true;
                if (objPyamentDetails.payInterval == "12")
                {
                    sub.paymentSchedule.totalOccurrences = 3;
                }
                if (objPyamentDetails.payInterval == "6")
                {
                    sub.paymentSchedule.totalOccurrences = 6;
                }
                if (objPyamentDetails.payInterval == "3")
                {
                    sub.paymentSchedule.totalOccurrences = 12;
                }
                if (objPyamentDetails.payInterval == "1")
                {
                    sub.paymentSchedule.totalOccurrences = 36;
                }
                sub.paymentSchedule.totalOccurrencesSpecified = true;

                sub.paymentSchedule.interval = new paymentScheduleTypeInterval();
                if (objPyamentDetails.payInterval == "1")
                {
                    sub.paymentSchedule.interval.length = 1;
                    sub.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.months;
                }
                if (objPyamentDetails.payInterval == "3")
                {
                    sub.paymentSchedule.interval.length = 3;
                    sub.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.months;
                }
                if (objPyamentDetails.payInterval == "6")
                {
                    sub.paymentSchedule.interval.length = 6;
                    sub.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.months;
                }
                if (objPyamentDetails.payInterval == "12")
                {
                    sub.paymentSchedule.interval.length = 12;
                    sub.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.months;
                }

                //sub.order = new orderType();
                //sub.order.invoiceNumber = "invoice111";

                sub.customer = new customerType();
                sub.customer.email = objPyamentDetails.Email;
                sub.customer.phoneNumber = objPyamentDetails.Phone;


                // Include authentication information
                PopulateMerchantAuthenticationBySystem((ANetApiRequest)request);

                request.subscription = sub;
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        public string createSubscriptionBySystem(PaymentDetail objPyamentDetails)
        {
            ARBCreateSubscriptionRequest createSubscriptionRequest = new ARBCreateSubscriptionRequest();
            PopulateSubscriptionBySystem(createSubscriptionRequest, objPyamentDetails);
            XmlDocument xmldoc = null;

            string resp = PostRequestBySystem(createSubscriptionRequest, out xmldoc);
            return resp;

        }
        //Create Subscription
        private static void PopulateSubscriptionBySystem(ARBCreateSubscriptionRequest request, PaymentDetail objPyamentDetails)
        {
            try
            {

                clsColleaguesData ObjColleaguesData = new clsColleaguesData();
                ARBSubscriptionType sub = new ARBSubscriptionType();
                paymentScheduleType objschedulePayment = new paymentScheduleType();
                creditCardType creditCard = new creditCardType();
                DentistProfileBLL ObjBLL = new DentistProfileBLL();
                DentistProfileDetail Dentist = new DentistProfileDetail();
                Dentist = ObjBLL.GetDoctorDetials(SafeValue<int>(objPyamentDetails.Userid));
                if(Dentist != null)
                {
                    objPyamentDetails.Firstname = Dentist.FirstName;
                    objPyamentDetails.Lastname = Dentist.LastName;
                    objPyamentDetails.Userid = SafeValue<string>(Dentist.UserId);
                    objPyamentDetails.Address = Dentist.Address;
                    objPyamentDetails.City = Dentist.City;
                    objPyamentDetails.State = Dentist.State;
                    //objPyamentDetails.Zip = Dentist.ZipCode;
                    objPyamentDetails.Phone = Dentist.Phone;
                }
                //NewRecordlinc.Models.Colleagues.Member MdlMember = new NewRecordlinc.Models.Colleagues.Member();
                //DataSet ds = MdlMember.GetProfileDetailsOfDoctorByID(SafeValue<int>(objPyamentDetails.Userid));
                //if (ds.Tables[0].Rows.Count > 0)
                //{
                //    objPyamentDetails.Firstname = SafeValue<string>(ds.Tables[0].Rows[0]["FirstName"]);
                //    objPyamentDetails.Lastname = SafeValue<string>(ds.Tables[0].Rows[0]["LastName"]);
                //    objPyamentDetails.Userid = SafeValue<string>(ds.Tables[0].Rows[0]["UserId"]);
                //}
                //if (ds.Tables[2].Rows.Count > 0)
                //{
                //    objPyamentDetails.Address = SafeValue<string>(ds.Tables[2].Rows[0]["ExactAddress"]);
                //    objPyamentDetails.City = SafeValue<string>(ds.Tables[2].Rows[0]["City"]);
                //    objPyamentDetails.State = SafeValue<string>(ds.Tables[2].Rows[0]["State"]);
                //    objPyamentDetails.Zip = SafeValue<string>(ds.Tables[2].Rows[0]["ZipCode"]);
                //    objPyamentDetails.Phone = SafeValue<string>(ds.Tables[2].Rows[0]["Phone"]);
                //}
                if (objPyamentDetails.IsLuvMyDentist)
                {
                    sub.name = "I Luv My Dentist Subscription";
                }
                else
                {
                    sub.name = "Recordlinc Subscription";
                }
                

                creditCard.cardNumber = objPyamentDetails.CardNumber;
                creditCard.expirationDate = objPyamentDetails.expdate;  // required format for API is YYYY-MM
                sub.payment = new paymentType();
                sub.payment.Item = creditCard;

                sub.billTo = new nameAndAddressType();
                sub.billTo.firstName = objPyamentDetails.Firstname;
                sub.billTo.lastName = objPyamentDetails.Lastname;
                sub.billTo.address = objPyamentDetails.Address;
                sub.billTo.city = objPyamentDetails.City;
                sub.billTo.state = objPyamentDetails.State;
                sub.billTo.zip = objPyamentDetails.Zip;

                string firstAmount = GetPlanAmount(SafeValue<int>(objPyamentDetails.Planes));
                decimal totalAmount = 0.0M;
                if (objPyamentDetails.payInterval == "1")
                {
                    totalAmount = SafeValue<decimal>(firstAmount + ".00");
                }
                if (objPyamentDetails.payInterval == "3")
                {
                    var amt = SafeValue<string>(SafeValue<int>(firstAmount) * 3);
                    totalAmount = SafeValue<decimal>(amt + ".00");
                }
                if (objPyamentDetails.payInterval == "6")
                {
                    var amt = SafeValue<string>(SafeValue<int>(firstAmount) * 6);
                    totalAmount = SafeValue<decimal>(amt + ".00");
                }
                if (objPyamentDetails.payInterval == "12")
                {
                    var amt = SafeValue<string>(SafeValue<int>(firstAmount) * 12);
                    totalAmount = SafeValue<decimal>(amt + ".00");
                }
                sub.amount = SafeValue<decimal>(objPyamentDetails.Amount); //SafeValue<decimal>(objPyamentDetails.Amount); //47.00M;
                sub.amountSpecified = true;

                // Create a subscription that is 12 monthly payments starting on Jan 1, 2019
                // with 1 month free (trial period)

                //sub.paymentSchedule = new paymentScheduleType();
                DateTime date = System.DateTime.Now;
                objschedulePayment.startDate = date.AddMonths(0);
                objschedulePayment.startDateSpecified = true;
                if (objPyamentDetails.payInterval == "12")
                {
                    objschedulePayment.totalOccurrences = 3;
                }
                if (objPyamentDetails.payInterval == "6")
                {
                    objschedulePayment.totalOccurrences = 6;
                }
                if (objPyamentDetails.payInterval == "3")
                {
                    objschedulePayment.totalOccurrences = 12;
                }
                if (objPyamentDetails.payInterval == "1")
                {
                    objschedulePayment.totalOccurrences = 36;
                }
                objschedulePayment.totalOccurrencesSpecified = true;

                objschedulePayment.interval = new paymentScheduleTypeInterval();
                if (objPyamentDetails.payInterval == "1")
                {
                    objschedulePayment.interval.length = 1;
                    objschedulePayment.interval.unit = ARBSubscriptionUnitEnum.months;
                }
                if (objPyamentDetails.payInterval == "3")
                {
                    objschedulePayment.interval.length = 3;
                    objschedulePayment.interval.unit = ARBSubscriptionUnitEnum.months;
                }
                if (objPyamentDetails.payInterval == "6")
                {
                    objschedulePayment.interval.length = 6;
                    objschedulePayment.interval.unit = ARBSubscriptionUnitEnum.months;
                }
                if (objPyamentDetails.payInterval == "12")
                {
                    objschedulePayment.interval.length = 12;
                    objschedulePayment.interval.unit = ARBSubscriptionUnitEnum.months;
                }
                sub.paymentSchedule = objschedulePayment;
                //sub.order = new orderType();
                //sub.order.invoiceNumber = "invoice111";

                sub.customer = new customerType();
                sub.customer.email = objPyamentDetails.Email;
                sub.customer.phoneNumber = objPyamentDetails.Phone;


                // Include authentication information
                PopulateMerchantAuthenticationBySystem((ANetApiRequest)request);

                request.subscription = sub;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PaymentBLL-PopulateSubscriptionBySystem", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static string GetPlanAmount(int PlanId)
        {
            try
            {
               return clsColleaguesData.GetPlanAmountByPlanId(PlanId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static bool CheckUserPayment(int userid)
        {
            DataTable dt = new DataTable();
            bool IsValidPayment = false;
            try
            {
                dt = clsColleaguesData.CheckSubscriptionExistingUser(userid, (int)WebhookEventType.SubscriptionEvent);
                if (dt != null && dt.Rows.Count > 0)
                {
                    var strStatus = SafeValue<bool>(dt.Rows[0]["IsPaymentFailed"]);
                    if (!strStatus)
                        IsValidPayment = false;
                    else
                        IsValidPayment = true;
                }
                return IsValidPayment;
            }
            catch (Exception ex)
            {
                new clsCommon().InsertErrorLog("PaymentBLL-CheckUserPayment", ex.Message, ex.StackTrace);
                throw;
            }            
        }

        public static bool AddPaymentBlockingHistory(int userid, bool isSuccess, int? RSid)
        {
            try
            {
                return clsColleaguesData.InsertPaymentBlocking(userid, isSuccess, RSid);
            }
            catch (Exception ex)
            {
                new clsCommon().InsertErrorLog("PaymentBLL-AddPaymentBlockingHistory", ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
