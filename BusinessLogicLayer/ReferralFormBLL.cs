using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataAccessLayer.PatientsData;
using System.IO;
using System.Configuration;
using System.Drawing;
using System.Data;
using BO.Models;
using DataAccessLayer.Common;
using DataAccessLayer.FlipTop;
using DataAccessLayer;
using BO.ViewModel;
using System.Data.SqlClient;
using System.Security;
using RestSharp.Contrib;
using DataAccessLayer.ColleaguesData;
using System.Reflection;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public class ReferralFormBLL
    {
        /// <summary>
        /// Get the All details of Speciality,Services,RefferalForm Settings
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static MainReferralForm GetDentistSpeciality(int UserId)
        {
            try
            {
                // define dal object
                ReferralFormDAL cl = new ReferralFormDAL();

                //define datalist
                List<ReferralFormServices> serviceobj = new List<ReferralFormServices>();
                List<ReferralFormSetting> Plist = new List<ReferralFormSetting>();
                List<ReferralFormSubField> SFlist = new List<ReferralFormSubField>();

                //Define main model object
                MainReferralForm rfobj = new MainReferralForm();
                
                // define datatable object
                DataTable dt = new DataTable();
                DataTable dt1 = new DataTable();
                DataTable dt2 = new DataTable();

                rfobj.UserId = Convert.ToInt32(UserId);
                dt = cl.GetDentistSpeciality(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        dt1 = cl.GetDentistServices(UserId, SafeValue<int>(item["SpecialityId"]));
                        serviceobj = new List<ReferralFormServices>();
                        foreach (DataRow item1 in dt1.Rows)
                        {
                            dt2 = cl.GetServicesSubFieldDetails(UserId, SafeValue<int>(item["SpecialityId"]), SafeValue<int>(item1["FieldId"]));
                            SFlist = new List<ReferralFormSubField>();
                            foreach (DataRow item2 in dt2.Rows)
                            {
                                SFlist.Add(new ReferralFormSubField()
                                {
                                    Id = SafeValue<int>(item2["KeyId"]),
                                    ParentFieldId = SafeValue<int>(item2["ParentFieldId"]),
                                    Name = SafeValue<string>(item2["FieldName"]),
                                    IsEnable = SafeValue<bool>(item2["IsEnable"]),
                                    SpecialityId = SafeValue<int>(item2["SpecialityId"]),
                                    SubFieldId = SafeValue<int>(item2["SubFieldId"]),
                                    OrderBy = SafeValue<int>(item2["SortOrder"])
                                });

                            }

                            serviceobj.Add(new ReferralFormServices()
                            {
                                Id = SafeValue<int>(item1["KeyId"]),
                                ParentFieldId = SafeValue<int>(item1["ParentFieldId"]),
                                FieldId = SafeValue<int>(item1["FieldId"]),
                                Name = SafeValue<string>(item1["Name"]),
                                NameFor = SafeValue<string>(item1["NameFor"]),
                                IsEnable = SafeValue<bool>(item1["IsEnable"]),
                                SpecialityId = SafeValue<int>(item1["SpecialityId"]),
                                OrderBy = SafeValue<int>(item1["SortOrder"]),
                                lstReferralFormSubField = SFlist
                            });

                        }
                        Plist.Add(new ReferralFormSetting()
                        {
                            Id = SafeValue<int>(item["Id"]),
                            SpecialityId = SafeValue<int>(item["SpecialityId"]),
                            Name = SafeValue<string>(item["Name"]),
                            IsEnable = SafeValue<bool>(item["IsEnable"]),
                            RefService = serviceobj
                        });
                    }
                }
                rfobj.RefFormSetting = Plist;
                DataTable dt_referral = new DataTable();
                dt_referral = cl.GetDentistreferral(UserId);
                if (dt_referral != null && dt_referral.Rows.Count > 0)
                {
                    int i = 0;
                    rfobj.IsFirstName = SafeValue<bool>(dt_referral.Rows[i++]["IsEnable"]);
                    rfobj.IsLastName = SafeValue<bool>(dt_referral.Rows[i++]["IsEnable"]);
                    rfobj.IsPatientPhone = SafeValue<bool>(dt_referral.Rows[i++]["IsEnable"]);
                    rfobj.IsPatientEmail = SafeValue<bool>(dt_referral.Rows[i++]["IsEnable"]);
                    rfobj.IsDateofBirth = SafeValue<bool>(dt_referral.Rows[i++]["IsEnable"]);
                    //rfobj.IsInsuranceProvider = SafeValue<bool>(dt_referral.Rows[i++]["IsEnable"]);
                    rfobj.IsDisplayLocation = SafeValue<bool>(dt_referral.Rows[i++]["IsEnable"]);


                    #region Bind DentistReferral
                    List<DentistReferral> DentistReferral = new List<DentistReferral>();
                    foreach (DataRow objData in dt_referral.Rows)
                    {
                        var Obj = new DentistReferral()
                        {
                            Id = SafeValue<int>(objData["Id"]),
                            ControlName = SafeValue<string>(objData["Name"]),
                            MapFieldName = SafeValue<string>(objData["MapFieldName"]),
                            IsEnable = SafeValue<bool>(objData["IsEnable"]),
                            SortOrder = SafeValue<int>(objData["OrderBy"]),
                            UserId = UserId,
                        };
                        DentistReferral.Add(Obj);
                    }
                    #endregion

                    rfobj.DentistReferral = DentistReferral;
                }
                return rfobj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormDAL---GetDentistSpeciality", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// save RefferalForm Settings,Speciality,Services.
        /// </summary>
        /// <param name="rfsobj,spcltyObj,servobj"></param>
        /// <returns></returns>
        public static bool AddRefferalFormSetting(int UserId, List<ReferralFormSetting> rfsobj, List<ReferralFormSetting> spcltyObj, List<ReferralFormServices> servobj, List<ReferralFormSubField> subfieldObj)
        {
            try
            {
                return ReferralFormDAL.AddRefferalFormSetting(UserId, rfsobj, spcltyObj, servobj, subfieldObj);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormDAL---AddRefferalFormSetting", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Get the Doctor service details from Speciality.
        /// </summary>
        /// <param name="UserId,SpecialityId"></param>
        /// <returns></returns>
        public static List<ReferralFormServices> GetDentistServices(int UserId, int SpecialityId)
        {
            try
            {
                ReferralFormDAL cl = new ReferralFormDAL();
                List<ReferralFormServices> Plist = new List<ReferralFormServices>();
                DataTable dt = new DataTable();
                dt = cl.GetDentistServices(UserId, SpecialityId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        Plist.Add(new ReferralFormServices()
                        {
                            FieldId = SafeValue<int>(item["FieldId"]),
                            Name = SafeValue<string>(item["Name"]),
                            IsEnable = SafeValue<bool>(item["IsEnable"])
                        });
                    }
                }
                return Plist;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormDAL---GetDentistServices", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// save Dentist Patient Referral Details.
        /// </summary>
        /// <param name="UserId,smobj,fmObj,subfieldObj"></param>
        /// <returns></returns>
        public static bool AddDentistPatientreferralFormSetting(int UserId, List<PatientSectionMapping> smobj, List<PatientFieldMapping> fmObj, List<PatientSubFieldMapping> subfieldObj)
        {
            try
            {
                return ReferralFormDAL.AddDentistPatientreferralFormSetting(UserId, smobj, fmObj, subfieldObj);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormDAL---AddDentistPatientreferralFormSetting", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Get the Patient service and referal setting details.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static List<PatientSectionMapping> GetDentistPatientmappingDetail(int UserId)
        {
            try
            {
                ReferralFormDAL cl = new ReferralFormDAL();
                List<PatientSectionMapping> smobj = new List<PatientSectionMapping>();
                List<PatientFieldMapping> fmobj = new List<PatientFieldMapping>();
                List<PatientSubFieldMapping> subFieldobj = new List<PatientSubFieldMapping>();
                DataSet ds = new DataSet();

                ds = cl.GetDentistPatientSectionDetails(UserId);
                DataTable dt_section = new DataTable();
                DataTable dt_field = new DataTable();
                DataTable dt_subfield = new DataTable();
                dt_section = ds.Tables[0];
                dt_field = ds.Tables[1];
                dt_subfield = ds.Tables[2];

                if (dt_section != null && dt_section.Rows.Count > 0)
                {
                    foreach (DataRow item in dt_section.Rows)
                    {
                        //dt1 = cl.GetDentistPatientFieldDetails(UserId, SafeValue<int>(item["SectionId"]));
                        DataRow[] FieldRow = dt_field.Select("SectionId=" + SafeValue<int>(item["SectionId"]) + "", "OrderBy asc");
                        fmobj = new List<PatientFieldMapping>();
                        foreach (DataRow item1 in FieldRow)
                        {
                            //dt2 = cl.GetDentistPatientSubFieldDetails(UserId, SafeValue<int>(item["SectionId"]), SafeValue<int>(item1["FieldId"]));
                            DataRow[] SubFieldRow = dt_subfield.Select("SectionId=" + SafeValue<int>(item["SectionId"]) + " AND  ParentFieldId=" + SafeValue<int>(item1["FieldId"]) + "", "OrderBy asc");
                            subFieldobj = new List<PatientSubFieldMapping>();
                            foreach (DataRow item2 in SubFieldRow)
                            {
                                subFieldobj.Add(new PatientSubFieldMapping()
                                {
                                    KeyId = SafeValue<int>(item2["KeyId"]),
                                    ParentFieldId = SafeValue<int>(item2["ParentFieldId"]),
                                    FieldName = SafeValue<string>(item2["FieldName"]),
                                    IsEnable = SafeValue<bool>(item2["IsEnable"]),
                                    SectionId = SafeValue<int>(item2["SectionId"]),
                                    FieldId = SafeValue<int>(item2["FieldId"]),
                                    TableName = "dentistpatientfieldmapping",
                                    XMLFieldName = SafeValue<string>(item2["XMLFieldName"])
                                });

                            }
                            fmobj.Add(new PatientFieldMapping()
                            {
                                KeyId = SafeValue<int>(item1["KeyId"]),
                                FieldId = SafeValue<int>(item1["FieldId"]),
                                FieldName = SafeValue<string>(item1["FieldName"]),
                                IsEnable = SafeValue<bool>(item1["IsEnable"]),
                                SectionId = SafeValue<int>(item1["SectionId"]),
                                XMLFieldName = SafeValue<string>(item1["XMLFieldName"]),
                                TableName = "dentistpatientfieldmapping",
                                lstSubFieldMapping = subFieldobj
                            });

                        }
                        smobj.Add(new PatientSectionMapping()
                        {
                            UserId = UserId,
                            SectionId = SafeValue<int>(item["SectionId"]),
                            SectionName = SafeValue<string>(item["SectionName"]),
                            IsEnable = SafeValue<bool>(item["IsEnable"]),
                            lstFieldMapping = fmobj
                        });
                    }
                }

                return smobj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormDAL---GetDentistPatientmappingDetail", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// This Method used for API that will return the Inner services based on User setting.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static List<SpecialityMaster> GetPatientReferralForm(int UserId)
        {
            try
            {
                InnerService PR = new InnerService();
                Compose1Click Compose1Click = new Compose1Click();
                ReferralFormDAL cl = new ReferralFormDAL();
                DataTable dtSpeciality = new DataTable();
                dtSpeciality = cl.GetDentistSpeciality(UserId);
                if (dtSpeciality.Rows.Count > 0)
                {
                    foreach (DataRow item in dtSpeciality.Rows)
                    {
                        SpecialityMaster model = new SpecialityMaster();
                        model.Speciality_Id = SafeValue<int>(item["SpecialityId"]);
                        model.Speciality_Name = SafeValue<string>(item["Name"]);
                        model.Speciality_IsEnable = SafeValue<bool>(item["IsEnable"]);
                        model.Speciality_SortOrder = SafeValue<int>(item["SortOrder"]);
                        DataTable dtServices = new DataTable();
                        dtServices = cl.GetDentistServices(UserId, SafeValue<int>(item["SpecialityId"]));
                        if (dtServices.Rows.Count > 0)
                        {
                            foreach (DataRow row in dtServices.Rows)
                            {
                                ServiceMasterModel obj = new ServiceMasterModel();
                                obj.Service_Id = SafeValue<int>(row["FieldId"]);
                                obj.Service_Name = SafeValue<string>(row["NAME"]);
                                obj.Service_Speciality_Id = SafeValue<int>(row["SpecialityId"]);
                                obj.Service_IsEnable = SafeValue<bool>(row["IsEnable"]);
                                obj.Service_MapFieldName = SafeValue<string>(row["MapFieldName"]);
                                obj.Service_NameFor = SafeValue<string>(row["NameFor"]);
                                obj.Service_SortOrder = SafeValue<int>(item["SortOrder"]);
                                model.lstServices.Add(obj);
                            }
                        }
                        Compose1Click.lstSpecialityMaster.Add(model);
                    }
                }

                //DataTable dt = new DataTable();
                //dt = cl.GetDentistPatientFieldDetailsForAllSpeciality(UserId);
                //if (dt.Rows.Count > 0)
                //{
                //    foreach (PropertyInfo propertyInfo in PR.GetType().GetProperties())
                //    {
                //        foreach (DataRow row in dt.Rows)
                //        {

                //            ServiceMasterModel obj = new ServiceMasterModel();
                //            obj.Service_Id = SafeValue<int>(row["ServicesId"]);
                //            obj.Service_Name = SafeValue<string>(row["ServiceName"]);
                //            obj.Service_Speciality_Id = SafeValue<int>(row["SpecialityId"]);
                //            obj.Service_IsEnable = SafeValue<bool>(row["IsEnable"]);
                //            obj.Service_MapFieldName = SafeValue<string>(row["MapFieldName"]);
                //            obj.Service_NameFor = SafeValue<string>(row["Service_NameFor"]);
                //            model.lstServices.Add(obj);

                //            var ColumnName = SafeValue<string>(row["MapFieldName"]);
                //            if (propertyInfo.Name == ColumnName)
                //            {
                //                propertyInfo.SetValue(PR, Convert.ChangeType(row["IsEnable"], propertyInfo.PropertyType));
                //            }
                //        }
                //    }
                //}
                return Compose1Click.lstSpecialityMaster;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormBLL---GetPatientReferralForm", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        /// <summary>
        /// This Method used for API that will return the Inner services based on User setting.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static InnerService GetPatientInnerService(int UserId)
        {
            try
            {
                InnerService PR = new InnerService();
                Compose1Click Compose1Click = new Compose1Click();
                ReferralFormDAL cl = new ReferralFormDAL();
                DataTable dt = new DataTable();
                List<AddPatient> PRobj = new List<AddPatient>();
                dt = cl.GetDentistPatientFieldDetailsForAllSpeciality(UserId);
                if (dt.Rows.Count > 0)
                {
                    foreach (PropertyInfo propertyInfo in PR.GetType().GetProperties())
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            var ColumnName = SafeValue<string>(row["MapFieldName"]);
                            if (propertyInfo.Name == ColumnName)
                            {
                                propertyInfo.SetValue(PR, Convert.ChangeType(row["IsEnable"], propertyInfo.PropertyType));
                            }
                        }
                    }
                   
                    foreach (DataRow row in dt.Rows)
                    {
                        var newObj = new AddPatient();
                        newObj.Id = SafeValue<int>(row["ServicesId"]);
                        newObj.ControlName = SafeValue<string>(row["ServiceName"]);
                        newObj.ParentFieldId = SafeValue<int>(row["ParentFieldId"]);
                        newObj.MapFieldName = SafeValue<string>(row["MapFieldName"]);
                        newObj.IsEnable = SafeValue<bool>(row["IsEnable"]);
                        newObj.SpecialityId = SafeValue<int>(row["SpecialityId"]);
                        newObj.SortOrder = SafeValue<int>(row["SortOrder"]);
                        
                        PRobj.Add(newObj);
                    }
                }

                PR.lstDentalInsurance = PRobj;
                //foreach (DataRow row in dt.Rows)
                //{
                //    var newObj = new AddPatient();
                //    newObj.Id = SafeValue<int>(row["Id"]);
                //    newObj.ControlName = SafeValue<string>(row["Name"]);
                //    newObj.IsEnable = SafeValue<bool>(row["IsEnable"]);
                //    newObj.SortOrder = SafeValue<int>(row["OrderBy"]);
                //    obj.Add(newObj);
                //}
                //PR.lstPatient = obj;

                return PR;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormBLL---GetPatientInnerService", Ex.Message, Ex.StackTrace);
                throw;
            }

        }


        /// <summary>
        /// Get the patientInfo from Patientid which fiiled from one click referal url.
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public static Compose1Click GetPatientInfo(int PatientId)
        {
            try
            {
                Compose1Click PI = new Compose1Click();
                DataTable dt = new clsPatientsData().GetPatientsDetails(PatientId);
                PI._patientInfo = new PatinentInfo();
                PI._contactInfo = new PatientContactInfo();
                if (dt != null && dt.Rows.Count > 0)
                {
                    PI._patientInfo.PatientId = SafeValue<int>(dt.Rows[0]["PatientId"]);
                    PI._patientInfo.Password = SafeValue<string>(dt.Rows[0]["Password"]);
                    PI._patientInfo.FirstName = SafeValue<string>(dt.Rows[0]["FirstName"]);
                    PI._patientInfo.LastName = SafeValue<string>(dt.Rows[0]["LastName"]);
                    PI._contactInfo.BOD = SafeValue<string>(dt.Rows[0]["DateOfBirth"]);
                    PI._contactInfo.Gender = (SafeValue<int>(dt.Rows[0]["Gender"]) == 1) ? 1 : 0;
                    PI._patientInfo.Phone = SafeValue<string>(dt.Rows[0]["Phone"]);
                    PI._patientInfo.Email = SafeValue<string>(dt.Rows[0]["Email"]);
                    PI._contactInfo.Address = SafeValue<string>(dt.Rows[0]["Address"]);
                    PI._contactInfo.City = SafeValue<string>(dt.Rows[0]["City"]);
                    PI._contactInfo.State = SafeValue<string>(dt.Rows[0]["State"]);
                    PI._contactInfo.ZipCode = SafeValue<string>(dt.Rows[0]["Zip"]);
                    //PI._contactInfo.EMGContactNo = SafeValue<string>(dt.Rows[0]["WorkPhone"]);
                    PI._contactInfo.SecondaryPhoneNo = SafeValue<string>(dt.Rows[0]["SecondaryPhone"]);
                }
                return PI;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormBLL---GetPatientInfo", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// This Method created for Patient Email click using One-Click referral time.
        /// Fill the details of that patients Insurance coverage.
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public static BO.Models.InsuranceCoverage GetInsuranceDetails(int PatientId)
        {
            try
            {
                InsuranceCoverage Regobj = new InsuranceCoverage();
                //Get Data from database.
                DataTable dt = new clsPatientsData().GetInsuranceCoverage(PatientId, "GetRegistrationform");
                if (dt != null && dt.Rows.Count > 0)
                {

                    Regobj.PrimaryInsuranceCompany = SafeValue<string>(dt.Rows[0]["txtEmployeeName1"]);
                    Regobj.PrimaryInsurancePhone = SafeValue<string>(dt.Rows[0]["txtInsurancePhone1"]);
                    Regobj.PrimaryDateOfBirth = SafeValue<string>(dt.Rows[0]["txtEmployeeDob1"]);
                    Regobj.PrimaryNameOfInsured = SafeValue<string>(dt.Rows[0]["txtEmployerName1"]);
                    // Regobj.txtYearsEmployed1 = SafeValue<string>(dt.Rows[0]["txtYearsEmployed1"]);
                    Regobj.PrimaryMemberID = SafeValue<string>(dt.Rows[0]["txtNameofInsurance1"]);
                    // Regobj.txtInsuranceAddress1 = SafeValue<string>(dt.Rows[0]["txtInsuranceAddress1"]);
                    Regobj.PrimaryGroupNumber = SafeValue<string>(dt.Rows[0]["txtInsuranceTelephone1"]);
                    Regobj.SecondaryInsuranceCompany = SafeValue<string>(dt.Rows[0]["txtEmployeeName2"]);
                    Regobj.SecondaryInsurancePhone = SafeValue<string>(dt.Rows[0]["txtInsurancePhone2"]);
                    //  Regobj.txtEmployeeDob2 = SafeValue<string>(dt.Rows[0]["txtEmployeeDob2"]);
                    Regobj.SecondaryNameOfInsured = SafeValue<string>(dt.Rows[0]["txtEmployerName2"]);
                    Regobj.SecondaryDateOfBirth = SafeValue<string>(dt.Rows[0]["txtYearsEmployed2"]);
                    Regobj.SecondaryMemberID = SafeValue<string>(dt.Rows[0]["txtNameofInsurance2"]);
                    //  Regobj.txtInsuranceAddress2 = SafeValue<string>(dt.Rows[0]["txtInsuranceAddress2"]);
                    Regobj.SecondaryGroupNumber = SafeValue<string>(dt.Rows[0]["txtInsuranceTelephone2"]);

                    Regobj.EMGContactName = SafeValue<string>(dt.Rows[0]["txtemergencyname"]);
                    Regobj.EMGContactNo = SafeValue<string>(dt.Rows[0]["txtemergency"]);
                }
                return Regobj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormBLL---GetInsuranceDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// This Method created for Patient Email click using One-Click referral time.
        /// Fill the details of that patients Medical History.
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public static Compose1ClickMedicalHistory GetMedicalHistoryDetails(int PatientId)
        {
            try
            {
                Compose1ClickMedicalHistory objDH = new Compose1ClickMedicalHistory();
                DataTable dt = new clsPatientsData().GetMedicalHistoryForOneClickreferral(PatientId, "GetMedicalHistory");
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQue1"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQue1"]);
                        objDH.MrdQue1 = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.Mtxtphysicians = SafeValue<string>(dt.Rows[0]["Mtxtphysicians"]);

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQue2"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQue2"]);
                        objDH.MrdQue2 = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.Mtxthospitalized = SafeValue<string>(dt.Rows[0]["Mtxthospitalized"]);

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQue3"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQue3"]);
                        objDH.MrdQue3 = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.Mtxtserious = SafeValue<string>(dt.Rows[0]["Mtxtserious"]);

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQue4"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQue4"]);
                        objDH.MrdQue4 = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.Mtxtmedications = SafeValue<string>(dt.Rows[0]["Mtxtmedications"]);

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQue5"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQue5"]);
                        objDH.MrdQue5 = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.MtxtRedux = SafeValue<string>(dt.Rows[0]["MtxtRedux"]);

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQue6"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQue6"]);
                        objDH.MrdQue6 = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.MtxtFosamax = SafeValue<string>(dt.Rows[0]["MtxtFosamax"]);

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQuediet7"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQuediet7"]);
                        objDH.MrdQuediet7 = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.Mtxt7 = SafeValue<string>(dt.Rows[0]["Mtxt7"]);

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mrdotobacco8"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["Mrdotobacco8"]);
                        objDH.Mrdotobacco8 = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.Mtxt8 = SafeValue<string>(dt.Rows[0]["Mtxt8"]);

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mrdosubstances"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["Mrdosubstances"]);
                        objDH.Mrdosubstances = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.Mtxt9 = SafeValue<string>(dt.Rows[0]["Mtxt9"]);
                    //Code for mtxt11,mtxt12,mtxt13 is added here.
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mrdopregnant"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["Mrdopregnant"]);
                        objDH.Mrdopregnant = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.Mtxt10 = SafeValue<string>(dt.Rows[0]["Mtxt10"]);

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mrdocontraceptives"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["Mrdocontraceptives"]);
                        objDH.Mrdocontraceptives = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.Mtxt11 = SafeValue<string>(dt.Rows[0]["Mtxt11"]);
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdoNursing"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdoNursing"]);
                        objDH.MrdoNursing = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.Mtxt12 = SafeValue<string>(dt.Rows[0]["Mtxt12"]);
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mrdotonsils"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["Mrdotonsils"]);
                        objDH.Mrdotonsils = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.Mtxt13 = SafeValue<string>(dt.Rows[0]["Mtxt13"]);

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MchkQue_1"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MchkQue_1"]);
                        objDH.MchkQue_1 = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MchkQue_2"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MchkQue_2"]);
                        objDH.MchkQue_2 = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MchkQue_3"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MchkQue_3"]);
                        objDH.MchkQue_3 = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MchkQue_4"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MchkQue_4"]);
                        objDH.MchkQue_4 = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MchkQue_5"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MchkQue_5"]);
                        objDH.MchkQue_5 = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MchkQue_6"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MchkQue_6"]);
                        objDH.MchkQue_6 = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MchkQue_7"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MchkQue_7"]);
                        objDH.MchkQue_7 = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MchkQue_8"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MchkQue_8"]);
                        objDH.MchkQue_8 = (Values.Contains("Y")) ? true : false;
                    }
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mchk14"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["Mchk14"]);
                        objDH.Mchk14 = (Values.Contains("Y")) ? true : false;
                    }
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mchk15"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["Mchk15"]);
                        objDH.Mchk15 = (Values.Contains("Y")) ? true : false;
                    }

                    //if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MchkQue_9"])))
                    //{
                    //    string Values = SafeValue<string>(dt.Rows[0]["MchkQue_9"]);
                    //    objDH.MchkQue_9 = (Values.Contains("Y")) ? true : false;
                    //}
                    //objDH.MtxtchkQue_9 = SafeValue<string>(dt.Rows[0]["MtxtchkQue_9"]);

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAbnormal"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueAbnormal"]);
                        objDH.MrdQueAbnormal = (Values.Contains("Y")) ? true : false;
                    }
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAIDS_HIV_Positive"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueAIDS_HIV_Positive"]);
                        objDH.MrdQueAIDS_HIV_Positive = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAlzheimer"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueAlzheimer"]);
                        objDH.MrdQueAlzheimer = (Values.Contains("Y")) ? true : false;
                    }



                    //if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAnaphylaxis"])))
                    //{
                    //    string Values = SafeValue<string>(dt.Rows[0]["MrdQueAnaphylaxis"]);
                    //    objDH.MrdQueAnaphylaxis = (Values.Contains("Y")) ? true : false;
                    //}


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAnemia"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueAnemia"]);
                        objDH.MrdQueAnemia = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAngina"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueAngina"]);
                        objDH.MrdQueAngina = (Values.Contains("Y")) ? true : false;
                    }
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueThinners"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueThinners"]);
                        objDH.MrdQueThinners = (Values.Contains("Y")) ? true : false;
                    }
                    //commented code NTD
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueArthritis"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueArthritis"]);
                        objDH.MrdQueArthritis = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueArtificialHeartValve"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueArtificialHeartValve"]);
                        objDH.MrdQueArtificialHeartValve = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueArtificialJoint"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueArtificialJoint"]);
                        objDH.MrdQueArtificialJoint = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAsthma"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueAsthma"]);
                        objDH.MrdQueAsthma = (Values.Contains("Y")) ? true : false;
                    }
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueFibrillation"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueFibrillation"]);
                        objDH.MrdQueFibrillation = (Values.Contains("Y")) ? true : false;
                    }
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueBloodDisease"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueBloodDisease"]);
                        objDH.MrdQueBloodDisease = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueBloodTransfusion"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueBloodTransfusion"]);
                        objDH.MrdQueBloodTransfusion = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueBreathing"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueBreathing"]);
                        objDH.MrdQueBreathing = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueBruise"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueBruise"]);
                        objDH.MrdQueBruise = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueCancer"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueCancer"]);
                        objDH.MrdQueCancer = (Values.Contains("Y")) ? true : false;
                    }


                    //if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueChemicalDependancy"])))
                    //{
                    //    string Values = SafeValue<string>(dt.Rows[0]["MrdQueChemicalDependancy"]);
                    //    objDH.MrdQueChemicalDependancy = (Values.Contains("Y")) ? true : false;
                    //}

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueChemotherapy"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueChemotherapy"]);
                        objDH.MrdQueChemotherapy = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueChest"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueChest"]);
                        objDH.MrdQueChest = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueCigarette"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueCigarette"]);
                        objDH.MrdQueCigarette = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueCirulation"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueCirulation"]);
                        objDH.MrdQueCirulation = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueCortisone"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueCortisone"]);
                        objDH.MrdQueCortisone = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueFrequentCough"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueFrequentCough"]);
                        objDH.MrdQueFrequentCough = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueConvulsions"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueConvulsions"]);
                        objDH.MrdQueConvulsions = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQuefillers"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQuefillers"]);
                        objDH.MrdQuefillers = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueDiabetes"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueDiabetes"]);
                        objDH.MrdQueDiabetes = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueDiarrhhea"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueDiarrhhea"]);
                        objDH.MrdQueDiarrhhea = (Values.Contains("Y")) ? true : false;
                    }
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueDigestive"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueDigestive"]);
                        objDH.MrdQueDigestive = (Values.Contains("Y")) ? true : false;
                    }
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueDizziness"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueDizziness"]);
                        objDH.MrdQueDizziness = (Values.Contains("Y")) ? true : false;
                    }
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAlcoholic"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueAlcoholic"]);
                        objDH.MrdQueAlcoholic = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueSubstances"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueSubstances"]);
                        objDH.MrdQueSubstances = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueEmphysema"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueEmphysema"]);
                        objDH.MrdQueEmphysema = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueEpilepsy"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueEpilepsy"]);
                        objDH.MrdQueEpilepsy = (Values.Contains("Y")) ? true : false;
                    }

                    //commented code NTD
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueExcessiveUrination"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueExcessiveUrination"]);
                        objDH.MrdQueExcessiveurination = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueExcessiveThirst"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueExcessiveThirst"]);
                        objDH.MrdQueExcessiveThirst = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueGlaucoma"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueGlaucoma"]);
                        objDH.MrdQueGlaucoma = (Values.Contains("Y")) ? true : false;
                    }



                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHeadaches"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueHeadaches"]);
                        objDH.MrdQueHeadaches = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAttack"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueAttack"]);
                        objDH.MrdQueAttack = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHeartPacemaker"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueHeartPacemaker"]);
                        objDH.MrdQueHeartPacemaker = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHemophilia"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueHemophilia"]);
                        objDH.MrdQueHemophilia = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHeart_defect"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueHeart_defect"]);
                        objDH.MrdQueHeart_defect = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHeart_murmur"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueHeart_murmur"]);
                        objDH.MrdQueHeart_murmur = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHepatitisA"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueHepatitisA"]);
                        objDH.MrdQueHepatitisA = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHerpes"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueHerpes"]);
                        objDH.MrdQueHerpes = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHiatal"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueHiatal"]);
                        objDH.MrdQueHiatal = (Values.Contains("Y")) ? true : false;
                    }




                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueBlood_pressure"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueBlood_pressure"]);
                        objDH.MrdQueBlood_pressure = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAIDS"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueAIDS"]);
                        objDH.MrdQueAIDS = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHow_much"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueHow_much"]);
                        objDH.MrdQueHow_much = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHow_often"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueHow_often"]);
                        objDH.MrdQueHow_often = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHypoglycemia"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueHypoglycemia"]);
                        objDH.MrdQueHypoglycemia = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueReplacement"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueReplacement"]);
                        objDH.MrdQueReplacement = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueKidney"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueKidney"]);
                        objDH.MrdQueKidney = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueLeukemia"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueLeukemia"]);
                        objDH.MrdQueLeukemia = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueLiver"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueLiver"]);
                        objDH.MrdQueLiver = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueLung"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueLung"]);
                        objDH.MrdQueLung = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueMitral"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueMitral"]);
                        objDH.MrdQueMitral = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueNeck_BackProblem"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueNeck_BackProblem"]);
                        objDH.MrdQueNeck_BackProblem = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueOsteoporosis"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueOsteoporosis"]);
                        objDH.MrdQueOsteoporosis = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQuePacemaker"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQuePacemaker"]);
                        objDH.MrdQuePacemaker = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQuePainJaw_Joints"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQuePainJaw_Joints"]);
                        objDH.MrdQuePainJaw_Joints = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueEndocarditis"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueEndocarditis"]);
                        objDH.MrdQueEndocarditis = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQuePsychiatric"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQuePsychiatric"]);
                        objDH.MrdQuePsychiatric = (Values.Contains("Y")) ? true : false;
                    }



                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueRadiation"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueRadiation"]);
                        objDH.MrdQueRadiation = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueRheumatic"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueRheumatic"]);
                        objDH.MrdQueRheumatic = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueScarlet"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueScarlet"]);
                        objDH.MrdQueScarlet = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueShingles"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueShingles"]);
                        objDH.MrdQueShingles = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueShortness"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueShortness"]);
                        objDH.MrdQueShortness = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueSinus"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueSinus"]);
                        objDH.MrdQueSinus = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueStroke"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueStroke"]);
                        objDH.MrdQueStroke = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueSjorgren"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueSjorgren"]);
                        objDH.MrdQueSjorgren = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueStomach"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueStomach"]);
                        objDH.MrdQueStomach = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueSwelling"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueSwelling"]);
                        objDH.MrdQueSwelling = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueSwollen"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueSwollen"]);
                        objDH.MrdQueSwollen = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueThyroid"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueThyroid"]);
                        objDH.MrdQueThyroid = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueTuberculosis"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueTuberculosis"]);
                        objDH.MrdQueTuberculosis = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueTumors"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueTumors"]);
                        objDH.MrdQueTumors = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueValve"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueValve"]);
                        objDH.MrdQueValve = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueVision"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueVision"]);
                        objDH.MrdQueVision = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueUnexplained"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdQueUnexplained"]);
                        objDH.MrdQueUnexplained = (Values.Contains("Y")) ? true : false;
                    }
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mrdillness"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["Mrdillness"]);
                        objDH.Mrdillness = (Values.Contains("Y")) ? true : false;
                    }
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdComments"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["MrdComments"]);
                        objDH.MrdComments = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.Mtxtillness = SafeValue<string>(dt.Rows[0]["Mtxtillness"]);
                    objDH.MtxtComments = SafeValue<string>(dt.Rows[0]["MtxtComments"]);
                }
                return objDH;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog($"ReferralFormBLL---GetMedicalHistoryDetails--PatientId:{PatientId}", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static MedicalHistoryMaster GetMedicalHistory(int PatientId)
        {
            clsCommon ObjCommon = new clsCommon();
            clsPatientsData ObjPatientsData = new clsPatientsData();
            MedicalHistoryMaster objDH = new MedicalHistoryMaster();
            DataTable dt = new DataTable();
            dt = ObjPatientsData.GetMedicalHistoryForOneClickreferral(PatientId, "GetMedicalHistory");
            if (dt != null && dt.Rows.Count > 0)
            {

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mtxtphysicians"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["Mtxtphysicians"]);
                    objDH.Mtxtphysicians = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mtxthospitalized"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["Mtxthospitalized"]);
                    objDH.Mtxthospitalized = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mtxtserious"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["Mtxtserious"]);
                    objDH.Mtxtserious = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mtxtmedications"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["Mtxtmedications"]);
                    objDH.Mtxtmedications = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MtxtRedux"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MtxtRedux"]);
                    objDH.MtxtRedux = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MtxtFosamax"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MtxtFosamax"]);
                    objDH.MtxtFosamax = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mtxt7"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["Mtxt7"]);
                    objDH.Mtxt7 = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mtxt8"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["Mtxt8"]);
                    objDH.Mtxt8 = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mtxt9"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["Mtxt9"]);
                    objDH.Mtxt9 = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mtxt10"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["Mtxt10"]);
                    objDH.Mtxt10 = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mtxt11"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["Mtxt11"]);
                    objDH.Mtxt11 = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mtxt12"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["Mtxt12"]);
                    objDH.Mtxt12 = (Values.Contains("Y")) ? true : false;
                }

                //if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mrdotonsils"])))
                //{
                //    string Values = SafeValue<string>(dt.Rows[0]["Mrdotonsils"]);
                //    objDH.Mrdotonsils = (Values.Contains("Y")) ? true : false;
                //}
                objDH.Mtxt13 = SafeValue<string>(dt.Rows[0]["Mtxt13"]);

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MchkQue_1"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MchkQue_1"]);
                    objDH.MchkQue_1 = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MchkQue_2"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MchkQue_2"]);
                    objDH.MchkQue_2 = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MchkQue_3"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MchkQue_3"]);
                    objDH.MchkQue_3 = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MchkQue_4"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MchkQue_4"]);
                    objDH.MchkQue_4 = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MchkQue_5"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MchkQue_5"]);
                    objDH.MchkQue_5 = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MchkQue_6"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MchkQue_6"]);
                    objDH.MchkQue_6 = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MchkQue_7"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MchkQue_7"]);
                    objDH.MchkQue_7 = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MchkQue_8"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MchkQue_8"]);
                    objDH.MchkQue_8 = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MchkQue_9"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MchkQue_9"]);
                    objDH.MchkQue_9 = (Values.Contains("Y")) ? true : false;
                }
                //objDH.MtxtchkQue_9 = SafeValue<string>(dt.Rows[0]["MtxtchkQue_9"]);

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAbnormal"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueAbnormal"]);
                    objDH.MrdQueAbnormal = (Values.Contains("Y")) ? true : false;
                }
                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAIDS_HIV_Positive"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueAIDS_HIV_Positive"]);
                    objDH.MrdQueAIDS_HIV_Positive = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAlzheimer"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueAlzheimer"]);
                    objDH.MrdQueAlzheimer = (Values.Contains("Y")) ? true : false;
                }



                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAnaphylaxis"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueAnaphylaxis"]);
                    objDH.MrdQueAnaphylaxis = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAnemia"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueAnemia"]);
                    objDH.MrdQueAnemia = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAngina"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueAngina"]);
                    objDH.MrdQueAngina = (Values.Contains("Y")) ? true : false;
                }
                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueThinners"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueThinners"]);
                    objDH.MrdQueThinners = (Values.Contains("Y")) ? true : false;
                }
                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueArthritis_Gout"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueArthritis_Gout"]);
                    objDH.MrdQueArthritis_Gout = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueArtificialHeartValve"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueArtificialHeartValve"]);
                    objDH.MrdQueArtificialHeartValve = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueArtificialJoint"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueArtificialJoint"]);
                    objDH.MrdQueArtificialJoint = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAsthma"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueAsthma"]);
                    objDH.MrdQueAsthma = (Values.Contains("Y")) ? true : false;
                }
                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueFibrillation"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueFibrillation"]);
                    objDH.MrdQueFibrillation = (Values.Contains("Y")) ? true : false;
                }
                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueBloodDisease"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueBloodDisease"]);
                    objDH.MrdQueBloodDisease = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueBloodTransfusion"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueBloodTransfusion"]);
                    objDH.MrdQueBloodTransfusion = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueBreathing"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueBreathing"]);
                    objDH.MrdQueBreathing = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueBruise"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueBruise"]);
                    objDH.MrdQueBruise = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueCancer"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueCancer"]);
                    objDH.MrdQueCancer = (Values.Contains("Y")) ? true : false;
                }


                //if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueChemicalDependancy"])))
                //{
                //    string Values = SafeValue<string>(dt.Rows[0]["MrdQueChemicalDependancy"]);
                //    objDH.MrdQueChemicalDependancy = (Values.Contains("Y")) ? true : false;
                //}

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueChemotherapy"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueChemotherapy"]);
                    objDH.MrdQueChemotherapy = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueChest"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueChest"]);
                    objDH.MrdQueChest = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueCigarette"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueCigarette"]);
                    objDH.MrdQueCigarette = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueCirulation"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueCirulation"]);
                    objDH.MrdQueCirulation = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueCortisone"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueCortisone"]);
                    objDH.MrdQueCortisone = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueFrequentCough"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueFrequentCough"]);
                    objDH.MrdQueFrequentCough = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueConvulsions"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueConvulsions"]);
                    objDH.MrdQueConvulsions = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQuefillers"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQuefillers"]);
                    objDH.MrdQuefillers = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueDiabetes"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueDiabetes"]);
                    objDH.MrdQueDiabetes = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueDiarrhhea"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueDiarrhhea"]);
                    objDH.MrdQueDiarrhhea = (Values.Contains("Y")) ? true : false;
                }
                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueDigestive"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueDigestive"]);
                    objDH.MrdQueDigestive = (Values.Contains("Y")) ? true : false;
                }
                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueDizziness"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueDizziness"]);
                    objDH.MrdQueDizziness = (Values.Contains("Y")) ? true : false;
                }
                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAlcoholic"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueAlcoholic"]);
                    objDH.MrdQueAlcoholic = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueSubstances"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueSubstances"]);
                    objDH.MrdQueSubstances = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueEmphysema"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueEmphysema"]);
                    objDH.MrdQueEmphysema = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueEpilepsy"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueEpilepsy"]);
                    objDH.MrdQueEpilepsy = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueExcessiveUrination"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueExcessiveUrination"]);
                    objDH.MrdQueExcessiveUrination = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueExcessiveThirst"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueExcessiveThirst"]);
                    objDH.MrdQueExcessiveThirst = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueGlaucoma"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueGlaucoma"]);
                    objDH.MrdQueGlaucoma = (Values.Contains("Y")) ? true : false;
                }



                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHeadaches"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueHeadaches"]);
                    objDH.MrdQueHeadaches = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAttack"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueAttack"]);
                    objDH.MrdQueAttack = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHeartPacemaker"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueHeartPacemaker"]);
                    objDH.MrdQueHeartPacemaker = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHemophilia"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueHemophilia"]);
                    objDH.MrdQueHemophilia = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHeart_defect"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueHeart_defect"]);
                    objDH.MrdQueHeart_defect = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHeart_murmur"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueHeart_murmur"]);
                    objDH.MrdQueHeart_murmur = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHepatitisA"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueHepatitisA"]);
                    objDH.MrdQueHepatitisA = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHerpes"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueHerpes"]);
                    objDH.MrdQueHerpes = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHiatal"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueHiatal"]);
                    objDH.MrdQueHiatal = (Values.Contains("Y")) ? true : false;
                }




                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueBlood_pressure"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueBlood_pressure"]);
                    objDH.MrdQueBlood_pressure = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueAIDS"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueAIDS"]);
                    objDH.MrdQueAIDS = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHow_much"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueHow_much"]);
                    objDH.MrdQueHow_much = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHow_often"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueHow_often"]);
                    objDH.MrdQueHow_often = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueHypoglycemia"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueHypoglycemia"]);
                    objDH.MrdQueHypoglycemia = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueReplacement"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueReplacement"]);
                    objDH.MrdQueReplacement = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueKidney"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueKidney"]);
                    objDH.MrdQueKidney = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueLeukemia"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueLeukemia"]);
                    objDH.MrdQueLeukemia = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueLiver"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueLiver"]);
                    objDH.MrdQueLiver = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueLung"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueLung"]);
                    objDH.MrdQueLung = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueMitral"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueMitral"]);
                    objDH.MrdQueMitral = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueNeck_BackProblem"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueNeck_BackProblem"]);
                    objDH.MrdQueNeck_BackProblem = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueOsteoporosis"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueOsteoporosis"]);
                    objDH.MrdQueOsteoporosis = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQuePacemaker"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQuePacemaker"]);
                    objDH.MrdQuePacemaker = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQuePainJaw_Joints"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQuePainJaw_Joints"]);
                    objDH.MrdQuePainJaw_Joints = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueEndocarditis"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueEndocarditis"]);
                    objDH.MrdQueEndocarditis = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQuePsychiatric"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQuePsychiatric"]);
                    objDH.MrdQuePsychiatric = (Values.Contains("Y")) ? true : false;
                }



                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueRadiation"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueRadiation"]);
                    objDH.MrdQueRadiation = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueRheumatic"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueRheumatic"]);
                    objDH.MrdQueRheumatic = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueScarlet"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueScarlet"]);
                    objDH.MrdQueScarlet = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueShingles"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueShingles"]);
                    objDH.MrdQueShingles = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueShortness"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueShortness"]);
                    objDH.MrdQueShortness = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueSinus"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueSinus"]);
                    objDH.MrdQueSinus = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueStroke"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueStroke"]);
                    objDH.MrdQueStroke = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueSjorgren"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueSjorgren"]);
                    objDH.MrdQueSjorgren = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueStomach"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueStomach"]);
                    objDH.MrdQueStomach = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueSwelling"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueSwelling"]);
                    objDH.MrdQueSwelling = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueSwollen"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueSwollen"]);
                    objDH.MrdQueSwollen = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueThyroid"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueThyroid"]);
                    objDH.MrdQueThyroid = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueTuberculosis"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueTuberculosis"]);
                    objDH.MrdQueTuberculosis = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueTumors"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueTumors"]);
                    objDH.MrdQueTumors = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueValve"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueValve"]);
                    objDH.MrdQueValve = (Values.Contains("Y")) ? true : false;
                }


                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueVision"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueVision"]);
                    objDH.MrdQueVision = (Values.Contains("Y")) ? true : false;
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["MrdQueUnexplained"])))
                {
                    string Values = SafeValue<string>(dt.Rows[0]["MrdQueUnexplained"]);
                    objDH.MrdQueUnexplained = (Values.Contains("Y")) ? true : false;
                }



                objDH.Mtxtillness = SafeValue<string>(dt.Rows[0]["Mtxtillness"]);
                objDH.MtxtComments = SafeValue<string>(dt.Rows[0]["MtxtComments"]);


            }
            return objDH;
        }
        /// <summary>
        /// This Method created for Patient Email click using One-Click referral time.
        /// Fill the details of that patients Dental History.
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public static Compose1ClickDentalHistory GetDentalHistoryDetails(int PatientId)
        {
            try
            {
                clsCommon ObjCommon = new clsCommon();
                clsPatientsData ObjPatientsData = new clsPatientsData();
                Compose1ClickDentalHistory objDH = new Compose1ClickDentalHistory();
                DataTable dt = new DataTable();
                dt = ObjPatientsData.GetDentalHistoryForOneClickreferral(PatientId, "GetDentalHistory");
                if (dt != null && dt.Rows.Count > 0)
                {


                    objDH.txtQue5 = SafeValue<string>(dt.Rows[0]["txtQue5"]);
                    objDH.txtQue6 = SafeValue<string>(dt.Rows[0]["txtQue6"]);
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["rdQue7a"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["rdQue7a"]);
                        objDH.rdQue7a = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.txtQue7 = SafeValue<string>(dt.Rows[0]["txtQue7"]);
                    objDH.txtQue12 = SafeValue<string>(dt.Rows[0]["txtQue12"]);

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["rdoQue11aFixedbridge"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["rdoQue11aFixedbridge"]);
                        objDH.rdoQue11aFixedbridge = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["rdoQue11bRemoveablebridge"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["rdoQue11bRemoveablebridge"]);
                        objDH.rdoQue11bRemoveablebridge = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["rdoQue11cDenture"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["rdoQue11cDenture"]);
                        objDH.rdoQue11cDenture = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["rdQue11dImplant"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["rdQue11dImplant"]);
                        objDH.rdQue11dImplant = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["rdQue15"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["rdQue15"]);
                        objDH.rdQue15 = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["rdQue16"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["rdQue16"]);
                        objDH.rdQue16 = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["rdQue17"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["rdQue17"]);
                        objDH.rdQue17 = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["rdQue18"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["rdQue18"]);
                        objDH.rdQue18 = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["rdQue19"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["rdQue19"]);
                        objDH.rdQue19 = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["rdQue20"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["rdQue20"]);
                        objDH.rdQue20 = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["chkQue20_1"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["chkQue20_1"]);
                        objDH.chkQue20_1 = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["chkQue20_2"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["chkQue20_2"]);
                        objDH.chkQue20_2 = Values.Contains("Y") ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["chkQue20_3"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["chkQue20_3"]);
                        objDH.chkQue20_3 = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["chkQue20_4"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["chkQue20_4"]);
                        objDH.chkQue20_4 = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["rdQue21"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["rdQue21"]);
                        objDH.rdQue21 = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["rdQue23"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["rdQue23"]);
                        objDH.rdQue23 = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["rdQue24"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["rdQue24"]);
                        objDH.rdQue24 = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["rdQue25"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["rdQue25"]);
                        objDH.rdQue25 = (Values.Contains("Y")) ? true : false;
                    }
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["rdQue28"])))
                    {
                        string Values = SafeValue<string>(dt.Rows[0]["rdQue28"]);
                        objDH.rdQue28 = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.txtQue29 = SafeValue<string>(dt.Rows[0]["txtQue29"]);
                    objDH.txtQue30 = SafeValue<string>(dt.Rows[0]["txtQue30"]);
                    objDH.txtQue31 = SafeValue<string>(dt.Rows[0]["txtQue31"]);
                    objDH.txtComments = SafeValue<string>(dt.Rows[0]["txtComments"]);
                    objDH.firstvistdate = SafeValue<string>(dt.Rows[0]["firstvistdate"]);
                    objDH.Reasonforfirstvisit = SafeValue<string>(dt.Rows[0]["Reasonforfirstvisit"]);
                    objDH.Dateoflastvisit = SafeValue<string>(dt.Rows[0]["Dateoflastvisit"]);
                    objDH.Reasonforlastvisit = SafeValue<string>(dt.Rows[0]["Reasonforlastvisit"]);
                    objDH.Dateofnextvisit = SafeValue<string>(dt.Rows[0]["Dateofnextvisit"]);
                    objDH.Reasonfornextvisit = SafeValue<string>(dt.Rows[0]["Reasonfornextvisit"]);
                }

                return objDH;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }
        /// <summary>
        /// This Method is used for Getting all images of that Patient.
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public static List<PatientImages> GetPatientImagesAll(int PatientId)
        {
            List<PatientImages> lstPatientImages = new List<PatientImages>();
            DataTable dt = new DataTable();
            clsCommon objCommon = new clsCommon();
            dt = new clsPatientsData().GetPatientImages(PatientId, null);
            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstPatientImages.Add(new PatientImages
                        {
                            ImageId = SafeValue<int>(row["ImageId"]),
                            ImageType = SafeValue<int>(row["ImageType"]),
                            CreationDate = SafeValue<string>(row["CreationDate"]),
                            LastModifiedDate = SafeValue<string>(row["LastModifiedDate"]),
                            Description = SafeValue<string>(row["Description"]),
                            MontageId = SafeValue<int>(row["MontageId"]),
                            Name = SafeValue<string>(row["Name"]),
                        });
                    }
                }
                return lstPatientImages;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormDAL---GetPatientImagesAll", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        #region Code For Get Patients Documents
        /// <summary>
        /// This Method used to get all documents of that Patient.
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public static List<PateintDocuments> GetPatientDocumetnsAll(int PatientId)
        {
            List<PateintDocuments> lstPateintDocuments = new List<PateintDocuments>();
            clsCommon objCommon = new clsCommon();
            DataTable dt = new DataTable();
            dt = new clsPatientsData().GetPatientDocumentsAll(PatientId, null);
            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstPateintDocuments.Add(new PateintDocuments
                        {
                            MontageId = SafeValue<int>(row["MontageId"]),
                            DocumentId = SafeValue<int>(row["DocumentId"]),
                            DocumentName = SafeValue<string>(row["DocumentName"]),
                            CreationDate = SafeValue<string>(row["CreationDate"]),
                            LastModifiedDate = SafeValue<string>(row["LastModifiedDate"]),
                            Description = SafeValue<string>(row["Description"]),

                        });
                    }
                }
                return lstPateintDocuments;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormBLL---GetPatientDocumetnsAll", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// This Method use for get the main service like Oral Surgery,Periodontics visibility status.
        /// </summary>
        /// <param name="clgId"></param>
        /// <returns></returns>
        public static SpecialityService GetVisibleSpeciality(int clgId)
        {
            SpecialityService Sp = new SpecialityService();
            DataTable dt = clsColleaguesData.GetUserReferrlaFormSetting(clgId);
            if (dt.Rows.Count > 0)
            {
                foreach (PropertyInfo propertyInfo in Sp.GetType().GetProperties())
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        var ColumnName = SafeValue<string>(row["MapFieldName"]);
                        if (propertyInfo.Name == ColumnName)
                        {
                            propertyInfo.SetValue(Sp, Convert.ChangeType(row["IsEnable"], propertyInfo.PropertyType));
                        }
                    }
                }
                //Sp.OSIsEnable = SafeValue<bool>(dt.Rows[0]["IsEnable"]);
                //Sp.PDIsEnable = SafeValue<bool>(dt.Rows[1]["IsEnable"]);
                //Sp.ODIsEnable = SafeValue<bool>(dt.Rows[2]["IsEnable"]);
                //Sp.PTIsEnable = SafeValue<bool>(dt.Rows[3]["IsEnable"]);
                //Sp.EDIsEnable = SafeValue<bool>(dt.Rows[4]["IsEnable"]);
                //Sp.PRIsEnable = SafeValue<bool>(dt.Rows[5]["IsEnable"]);
                //Sp.INIsEnable = SafeValue<bool>(dt.Rows[6]["IsEnable"]);
                //Sp.GDIsEnable = SafeValue<bool>(dt.Rows[7]["IsEnable"]);
                //Sp.DLIsEnable = SafeValue<bool>(dt.Rows[8]["IsEnable"]);
                //Sp.RDIsEnable = SafeValue<bool>(dt.Rows[9]["IsEnable"]);
                //Sp.OTIsEnable = SafeValue<bool>(dt.Rows[10]["IsEnable"]);
                //if (dt.Rows.Count > 11)
                //{
                //    Sp.PIIsEnable = SafeValue<bool>(dt.Rows[11]["IsEnable"]);
                //    Sp.ICIsEnable = SafeValue<bool>(dt.Rows[12]["IsEnable"]);
                //    Sp.MHIsEnable = SafeValue<bool>(dt.Rows[13]["IsEnable"]);
                //    Sp.DHIsEnable = SafeValue<bool>(dt.Rows[14]["IsEnable"]);
                //}
                //Sp.TPEnable = SafeValue<bool>(dt.Rows[15]["IsEnable"]);
                //Sp.PEDEnable = SafeValue<bool>(dt.Rows[16]["IsEnable"]);
            }
            List<AddPatient> obj = new List<AddPatient>();
            DataTable dt_referral = new DataTable();
            dt_referral = new ReferralFormDAL().GetDentistreferral(clgId);
            foreach (DataRow row in dt_referral.Rows)
            {
                var newObj = new AddPatient();
                newObj.Id = SafeValue<int>(row["Id"]);
                newObj.ControlName = SafeValue<string>(row["Name"]);
                newObj.IsEnable = SafeValue<bool>(row["IsEnable"]);
                newObj.SortOrder = SafeValue<int>(row["OrderBy"]);
                obj.Add(newObj);
            }
            Sp.lstPatient = obj;
            if (dt_referral != null && dt_referral.Rows.Count > 0)
            {
                //Lastname Email Added to Show/Hide on Refer Patient
                Sp.Lastname = SafeValue<bool>(dt_referral.Rows[1]["IsEnable"]);
                Sp.Email = SafeValue<bool>(dt_referral.Rows[3]["IsEnable"]);
                Sp.Location = SafeValue<bool>(dt_referral.Rows[5]["IsEnable"]);
                Sp.DateOfBirth = SafeValue<bool>(dt_referral.Rows[4]["IsEnable"]);
            }
            return Sp;
        }
        #endregion
        /// <summary>
        /// This Method use for save referal detail,Patient detail,Insurance coverage detail,Medical history,Dental History and also send mail and referal to sender and receiver.
        /// </summary>
        /// <param name="Model,IsFromPatientEmail"></param>
        /// <returns></returns>
        public static int ComposeReferral1Click(Compose1Click Model, bool IsFromPatientEmail = false)
        {
            bool Result = false;
            int res = 0;
            try
            {
                TripleDESCryptoHelper obj = new TripleDESCryptoHelper();
                DentistProfileBLL objDp = new DentistProfileBLL();
                clsTemplate template = new clsTemplate();
                //string DecryptedSenderId = obj.decryptText(Model.SenderId);
                string DecryptedSenderId = Model.SenderId;
                string DecryptedReceiverId = Model.ReceiverId;
                //This Method Add new patient on Recordlinc side and return the PatientId.
                if (Model._contactInfo != null)
                {

                    Model._patientInfo.UserId = SafeValue<int>(DecryptedSenderId);
                    Model._contactInfo.PatientId = DentistProfileBLL.AddNewPatient(Model, SafeValue<int>(DecryptedReceiverId));
                    //This Method Save the Patient Contact Details on Address-info table.
                    //XQ1-229 : Email is not updated in Address Info
                    Result = DentistProfileBLL.AddPatientContactInfoFromOneclick(Model._contactInfo, Model._patientInfo.Phone, Model._patientInfo.Email);
                }
                else
                {
                    Model._contactInfo = new PatientContactInfo();
                    Model._patientInfo.UserId = SafeValue<int>(DecryptedSenderId);
                    Model._contactInfo.PatientId = DentistProfileBLL.AddNewPatient(Model, SafeValue<int>(DecryptedReceiverId));
                    Result = DentistProfileBLL.AddPatientContactInfoFromOneclick(Model._contactInfo, Model._patientInfo.Phone, Model._patientInfo.Email);
                }



                //This Method save the xml for Insurance Coverage.

                //Adding workphone to insurace coverage because it erase the secondry phone no of patient.
                if (Model._insuranceCoverage == null)
                {
                    Model._insuranceCoverage = new InsuranceCoverage();
                }

                Model._insuranceCoverage.EMGContactName = Model._contactInfo.EMGContactName;
                Model._insuranceCoverage.EMGContactNo = Model._contactInfo.EMGContactNo;

                Result = objDp._AddInsuranceCoverage(Model._insuranceCoverage, Model._contactInfo.PatientId, Model._contactInfo.EMGContactName, Model._contactInfo.EMGContactNo, Model);

                //This Method save the xml for Medical History.

                Result = objDp._AddMedicalHistory(Model._medicalHistory, Model._contactInfo.PatientId);

                //This Method save the xml for Dental History.

                Result = objDp._AddDentalHistory(Model._dentalhistory, Model._contactInfo.PatientId);

                //This function save the images and documents of the referral.
                int PatientId = Model._contactInfo.PatientId;
                int UserId = SafeValue<int>(DecryptedSenderId);
                SendReferralModel objSendReferral = new SendReferralModel();
                objSendReferral.FileIds = Model.fileIds;
                objSendReferral.UserId = SafeValue<int>(Model.ReceiverId);
                DataSet dsDoctor = new clsColleaguesData().GetDoctorDetailsById(objSendReferral.UserId);
                if (dsDoctor != null && dsDoctor.Tables.Count > 0)
                {
                    //objSendReferral.DoctorFirstName = new clsCommon().CheckNull(SafeValue<string>(dsDoctor.Tables[0].Rows[0]["FirstName"]), string.Empty);
                    //objSendReferral.DoctorLastName = new clsCommon().CheckNull(SafeValue<string>(dsDoctor.Tables[0].Rows[0]["LastName"]), string.Empty);
                    //Passing Account name instead of doctor name in All Emails.
                    objSendReferral.DoctorLastName = SafeValue<string>(dsDoctor.Tables[0].Rows[0]["FullName"]);

                    //Paasing Receiver dentist Phone Referral Setting Email.
                    objSendReferral.AccountName = SafeValue<string>(dsDoctor.Tables[1].Rows[0]["AccountName"]);
                    objSendReferral.DoctorPhone = SafeValue<string>(dsDoctor.Tables[2].Rows[0]["Phone"]);
                    objSendReferral.AccountName = (string.IsNullOrWhiteSpace(objSendReferral.AccountName)) ? "Dr." + objSendReferral.DoctorFirstName + " " + objSendReferral.DoctorLastName : objSendReferral.AccountName;
                }

                IDictionary<string, string> response = SendReferralBLL.UploadImagesAndDocuments(PatientId, UserId, objSendReferral);
                string ImagesId = SafeValue<string>(response.Keys.FirstOrDefault());
                string DocIds = SafeValue<string>(response.Values.FirstOrDefault());

                if (Model.IsReferral == false)
                {
                    if (!IsFromPatientEmail)
                    {
                        if (!string.IsNullOrEmpty(Model._patientInfo.Email))
                        {

                            if (Model._insuranceCoverage.ItHasInsuranceData != 0)
                            {
                                //Verfied Insurace data related changes for XQ1-447
                                //change for XQ1-1059 
                                res = objDp.InsertReferral("Referral sent from One Click Referral", "Referral sent from One Click Referral", 2, Convert.ToInt32(Model.ReceiverId), Model._contactInfo.PatientId, null, null, 0, Convert.ToInt32(DecryptedSenderId), Model.XMLstring, true, Model.LocationId, Model.Comment, ImagesId, DocIds, Model.BoxFolderId, 1);
                            }
                            else
                            {
                                //This Method add referral setting for that dentist and shown in Recordlinkc inbox.
                                //change for XQ1 - 1059
                                 res = objDp.InsertReferral("Referral sent from One Click Referral", "Referral sent from One Click Referral", 2, Convert.ToInt32(Model.ReceiverId), Model._contactInfo.PatientId, null, null, 0, Convert.ToInt32(DecryptedSenderId), Model.XMLstring, true, Model.LocationId, Model.Comment, ImagesId, DocIds, Model.BoxFolderId);
                            }

                            //This Method send Mail to particulate dentist to whom the referral is sent
                            SentReferal(Model.SenderId, Model._patientInfo.Email, Model._contactInfo.PatientId, Model.ReceiverId, Model.XMLstring, Model.Comment, Model.LocationId, objSendReferral.DoctorPhone, objSendReferral.AccountName);
                        }
                    }
                    //This Method send all mail users.
                    SendOneClickreferralMail(Model._contactInfo.PatientId, SafeValue<int>(DecryptedReceiverId), SafeValue<int>(DecryptedSenderId), res ,IsFromPatientEmail);
                }
                else
                {
                    template.SendFormToDoctorEmail(Model._patientInfo.PatientId, SafeValue<int>(Model.ReceiverId), (Model._patientInfo.FirstName + " " + Model._patientInfo.LastName).Trim());
                }
                //change bool to int for XQ1 - 1059 
                return res;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormDAL---ComposeReferral1Click", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// This Method use for send refer.
        /// </summary>
        /// <param name="Model,IsFromPatientEmail"></param>
        /// <returns></returns>
        public static void SentReferal(string SenderId, string Email, int PatientId, string ReceiverId, string XMLstring, string Comment, int LocationId, string Phone, string AccountName)
        {
            string Urllink = string.Empty;
            try
            {

                TripleDESCryptoHelper obj = new TripleDESCryptoHelper();
                clsTemplate template = new clsTemplate();
                DentistProfileBLL objDp = new DentistProfileBLL();
                string DecryptedSenderId = SenderId;
                //string EndRecId = obj.encryptText(ReceiverId);
                int EndRecId = SafeValue<int>(ReceiverId) + 999;
                //Changes made for referral form setting email 
                // string DoctorName = clsPatientsData.GetDoctorNameFormId(SafeValue<int>(DecryptedSenderId));
                string DoctorName = clsPatientsData.GetDoctorNameFormId(SafeValue<int>(ReceiverId));
                //string EncrPatientId = obj.encryptText(SafeValue<string>(PatientId));
                int EncrPatientId = PatientId + 999;
                string EncrptedString = SafeValue<string>(EncrPatientId) + '|' + SafeValue<string>(EndRecId);
                // EncrptedString = HttpUtility.UrlEncode(EncrptedString);
                Urllink = ConfigurationManager.AppSettings["OneClickreferralURL"] + "AccountSettings/GetPatientReferralForm?TYHJNABGA=" + EncrptedString;
                new clsCommon().InsertErrorLog("ReferralFormBLL---SentReferal", Urllink, "");
                //template.SendMailToPatientForRefferalSettings(Email, DoctorName, Urllink, SafeValue<int>(DecryptedSenderId));
                // Account is passed instead of Doc name in All emails 
                template.SendMailToPatientForRefferalSettings(Email, AccountName, Urllink, SafeValue<int>(ReceiverId), Phone);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormBLL---SentReferal" + Urllink, Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This method send a notification to Patient, ReferredBy and ReferredTo users.
        /// SendTo Parameter access to this.
        /// SENDER = 1,
        /// PATIENT = 2,
        /// REFERRED = 3
        /// </summary>
        /// <param name="SenderName"></param>
        /// <param name="ReceiverName"></param>
        /// <param name="PatientName"></param>
        /// <param name="PatientId"></param>
        /// <param name="SenderId"></param>
        /// <param name="ReceiverId"></param>
        /// <param name="PatientEmail"></param>
        /// <param name="ReceiverEmail"></param>
        /// <param name="SenderEmail"></param>
        /// <param name="SendTo"></param>
        /// <returns></returns>
        public static bool SendOneClickreferralMail(int PatientId, int ReceiverId, int SenderId, int messageId, bool IsFromPatientEmail = false)
        {
            try
            {
                #region Local variable
                ReferralDetailsOneClick Obj = new ReferralDetailsOneClick();
                #endregion
                #region Get Sender Details form database.
                Obj.MessageId = messageId;
                if (SenderId > 0)
                {
                    /*Changes made in below code to Pass account name instead of Doctor name in 
                     All emails that are fired while sending one click Referral.
                     */
                    DataTable dt = new clsColleaguesData().GetDoctorDetailsbyId(SenderId);
                    if (dt.Rows.Count > 0 && dt != null)
                    {
                        //Obj.SenderName = SafeValue<string>(dt.Rows[0]["LastName"]) + " " + SafeValue<string>(dt.Rows[0]["FirstName"]);
                        Obj.SenderName = SafeValue<string>(dt.Rows[0]["AccountName"]);
                        Obj.SenderEmail = SafeValue<string>(dt.Rows[0]["Username"]);
                        if (!checkEmailDomain(Obj.SenderEmail))
                        {
                            Obj.SenderEmail = "";
                        }                        
                        Obj.SenderPhone = SafeValue<string>(dt.Rows[0]["Phone"]);
                        Obj.SenderMobile = SafeValue<string>(dt.Rows[0]["Mobile"]);
                        Obj.SenderId = SenderId;
                    }
                }
                #endregion
                #region Get Receiver Details from database.
                if (ReceiverId > 0)
                {
                    DataTable dt = new clsColleaguesData().GetDoctorDetailsbyId(ReceiverId);
                    if (dt.Rows.Count > 0 && dt != null)
                    {
                        //Obj.ReceiverName = SafeValue<string>(dt.Rows[0]["LastName"]) + " " + SafeValue<string>(dt.Rows[0]["FirstName"]);
                        Obj.ReceiverName = SafeValue<string>(dt.Rows[0]["AccountName"]);
                        Obj.ReceiverEmail = SafeValue<string>(dt.Rows[0]["Username"]);
                        if (!checkEmailDomain(Obj.ReceiverEmail))
                        {
                            Obj.ReceiverEmail = "";
                        }
                        Obj.ReceiverPhone = SafeValue<string>(dt.Rows[0]["Phone"]);
                        Obj.ReceiverMobile = SafeValue<string>(dt.Rows[0]["Mobile"]);
                        Obj.Specility = SafeValue<string>(dt.Rows[0]["Speciality"]);
                        //XQ1-197 : Change in Email Templete
                        if (!string.IsNullOrEmpty(Obj.Specility))
                        {
                            Obj.Specility = "(" + Obj.Specility.Trim() + ")";
                        }
                        Obj.ReceiverId = ReceiverId;
                    }
                }
                #endregion
                #region Get Patient Details from database.
                if (PatientId > 0)
                {
                    DataTable dt = new clsPatientsData().GetPatientsDetails(PatientId);
                    if (dt.Rows.Count > 0 && dt != null)
                    {
                        Obj.PatientName = SafeValue<string>(dt.Rows[0]["FirstName"]) + " " + SafeValue<string>(dt.Rows[0]["LastName"]);
                        Obj.PatientEmail = SafeValue<string>(dt.Rows[0]["Email"]);
                        if (!checkEmailDomain(Obj.PatientEmail))
                        {
                            Obj.PatientEmail = "";
                        }
                        Obj.PatientPhone = SafeValue<string>(dt.Rows[0]["Phone"]);
                        Obj.PatientId = PatientId;
                    }
                }
                #endregion
                #region Send mail to all
                if (IsFromPatientEmail)
                {
                    new clsTemplate().SendEmailForOneClickReferral(Obj, BO.Enums.Common.ReferralType.REFERRED);
                }
                else
                {
                    new clsTemplate().SendEmailForOneClickReferral(Obj, BO.Enums.Common.ReferralType.PATIENT);
                    new clsTemplate().SendEmailForOneClickReferral(Obj, BO.Enums.Common.ReferralType.REFERRED);
                    new clsTemplate().SendEmailForOneClickReferral(Obj, BO.Enums.Common.ReferralType.SENDER);
                }
                #endregion

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormBLL---SendOneClickreferralMail", Ex.Message, Ex.StackTrace);
            }
            return true;
        }
        /// <summary>
        /// Get Patient Referral Setting Details.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static InnerService GetSpecialityForPatientReferralFormSetting(int UserId)
        {
            InnerService PR = new InnerService();
            ReferralFormDAL cl = new ReferralFormDAL();
            DataTable dt = new DataTable();

            dt = cl.GetSpecialityForPatientReferralFormSetting(UserId);
            if (dt.Rows.Count > 0)
            {

                PR.txtCity = SafeValue<bool>(dt.Rows[6]["IsEnable"]);
                PR.txtDob = SafeValue<bool>(dt.Rows[3]["IsEnable"]);
                PR.txtEmail = SafeValue<bool>(dt.Rows[2]["IsEnable"]);
                PR.txtemergency = SafeValue<bool>(dt.Rows[12]["IsEnable"]);
                PR.txtemergencyname = SafeValue<bool>(dt.Rows[11]["IsEnable"]);
                PR.txtFirstName = SafeValue<bool>(dt.Rows[0]["IsEnable"]);
                PR.txtGender = SafeValue<bool>(dt.Rows[4]["IsEnable"]);
                PR.txtLastName = SafeValue<bool>(dt.Rows[1]["IsEnable"]);
                PR.txtResidenceStreet = SafeValue<bool>(dt.Rows[5]["IsEnable"]);
                PR.txtResidenceTelephone = SafeValue<bool>(dt.Rows[71]["IsEnable"]);
                PR.txtState = SafeValue<bool>(dt.Rows[7]["IsEnable"]);
                //PR.txtWorkPhone = SafeValue<bool>(dt.Rows[70]["IsEnable"]);
                PR.txtZip = SafeValue<bool>(dt.Rows[8]["IsEnable"]);
                PR.txtWorkPhone = SafeValue<bool>(dt.Rows[9]["IsEnable"]);

                PR.txtResponsiblepartyFname = SafeValue<bool>(dt.Rows[13]["IsEnable"]);
                PR.txtResponsiblepartyLname = SafeValue<bool>(dt.Rows[14]["IsEnable"]);
                PR.txtResponsibleRelationship = SafeValue<bool>(dt.Rows[15]["IsEnable"]);
                PR.txtResponsibleAddress = SafeValue<bool>(dt.Rows[16]["IsEnable"]);
                PR.txtResponsibleCity = SafeValue<bool>(dt.Rows[17]["IsEnable"]);
                PR.txtResponsibleState = SafeValue<bool>(dt.Rows[18]["IsEnable"]);
                PR.txtResponsibleZipCode = SafeValue<bool>(dt.Rows[19]["IsEnable"]);
                PR.txtResponsibleContact = SafeValue<bool>(dt.Rows[20]["IsEnable"]);
                PR.txtemailaddress = SafeValue<bool>(dt.Rows[21]["IsEnable"]);
                PR.MrdEmployeeName1 = SafeValue<bool>(dt.Rows[22]["IsEnable"]);
                PR.MrdInsurancePhone1 = SafeValue<bool>(dt.Rows[23]["IsEnable"]);
                PR.MrdEmployerName1 = SafeValue<bool>(dt.Rows[24]["IsEnable"]);
                PR.MrdEmployeeDob1 = SafeValue<bool>(dt.Rows[25]["IsEnable"]);
                PR.MrdNameofInsurance1 = SafeValue<bool>(dt.Rows[26]["IsEnable"]);
                PR.MrdInsuranceTelephone1 = SafeValue<bool>(dt.Rows[27]["IsEnable"]);
                PR.MrdEmployeeName2 = SafeValue<bool>(dt.Rows[28]["IsEnable"]);
                PR.MrdInsurancePhone2 = SafeValue<bool>(dt.Rows[29]["IsEnable"]);
                PR.MrdEmployerName2 = SafeValue<bool>(dt.Rows[30]["IsEnable"]);
                PR.MrdYearsEmployed2 = SafeValue<bool>(dt.Rows[31]["IsEnable"]);
                PR.MrdNameofInsurance2 = SafeValue<bool>(dt.Rows[32]["IsEnable"]);
                PR.MrdInsuranceTelephone2 = SafeValue<bool>(dt.Rows[33]["IsEnable"]);

                PR.MrdQue1 = SafeValue<bool>(dt.Rows[34]["IsEnable"]);
                PR.MrdQue2 = SafeValue<bool>(dt.Rows[35]["IsEnable"]);
                PR.MrdQue3 = SafeValue<bool>(dt.Rows[36]["IsEnable"]);
                PR.MrdQue4 = SafeValue<bool>(dt.Rows[37]["IsEnable"]);
                PR.MrdQue5 = SafeValue<bool>(dt.Rows[38]["IsEnable"]);
                PR.MrdQue6 = SafeValue<bool>(dt.Rows[39]["IsEnable"]);
                PR.MrdQuediet7 = SafeValue<bool>(dt.Rows[40]["IsEnable"]);
                PR.Mrdotobacco = SafeValue<bool>(dt.Rows[41]["IsEnable"]);
                PR.Mrdosubstances = SafeValue<bool>(dt.Rows[42]["IsEnable"]);
                PR.Mrdopregnant = SafeValue<bool>(dt.Rows[43]["IsEnable"]);
                PR.Mrdocontraceptives = SafeValue<bool>(dt.Rows[44]["IsEnable"]);
                PR.MrdoNursing = SafeValue<bool>(dt.Rows[45]["IsEnable"]);
                PR.Mrdotonsils = SafeValue<bool>(dt.Rows[46]["IsEnable"]);
                PR.Mchk14 = SafeValue<bool>(dt.Rows[47]["IsEnable"]);
                PR.Mchk15 = SafeValue<bool>(dt.Rows[48]["IsEnable"]);
                PR.Mrdillness = SafeValue<bool>(dt.Rows[49]["IsEnable"]);
                PR.MrdComments = SafeValue<bool>(dt.Rows[50]["IsEnable"]);

                PR.txtQue5 = SafeValue<bool>(dt.Rows[51]["IsEnable"]);
                PR.txtQue6 = SafeValue<bool>(dt.Rows[52]["IsEnable"]);
                PR.MrdQue7 = SafeValue<bool>(dt.Rows[53]["IsEnable"]);
                PR.txtQue12 = SafeValue<bool>(dt.Rows[54]["IsEnable"]);
                PR.DhQue5 = SafeValue<bool>(dt.Rows[55]["IsEnable"]);
                PR.rdQue15 = SafeValue<bool>(dt.Rows[56]["IsEnable"]);
                PR.rdQue16 = SafeValue<bool>(dt.Rows[57]["IsEnable"]);
                PR.rdQue17 = SafeValue<bool>(dt.Rows[58]["IsEnable"]);
                PR.rdQue18 = SafeValue<bool>(dt.Rows[59]["IsEnable"]);
                PR.rdQue19 = SafeValue<bool>(dt.Rows[60]["IsEnable"]);
                PR.rdQue20 = SafeValue<bool>(dt.Rows[61]["IsEnable"]);
                PR.rdQue21 = SafeValue<bool>(dt.Rows[62]["IsEnable"]);
                PR.rdQue23 = SafeValue<bool>(dt.Rows[63]["IsEnable"]);
                PR.rdQue24 = SafeValue<bool>(dt.Rows[64]["IsEnable"]);
                PR.rdQue25 = SafeValue<bool>(dt.Rows[65]["IsEnable"]);
                PR.rdQue28 = SafeValue<bool>(dt.Rows[66]["IsEnable"]);
                PR.txtQue29 = SafeValue<bool>(dt.Rows[67]["IsEnable"]);
                PR.txtQue30 = SafeValue<bool>(dt.Rows[68]["IsEnable"]);
                PR.txtQue31 = SafeValue<bool>(dt.Rows[69]["IsEnable"]);
                PR.txtComments = SafeValue<bool>(dt.Rows[70]["IsEnable"]);
                PR.txtDateoffirstvisit = SafeValue<bool>(dt.Rows[71]["IsEnable"]);
                PR.Reasonforfirstvisit = SafeValue<bool>(dt.Rows[72]["IsEnable"]);
                PR.Dateoflastvisit = SafeValue<bool>(dt.Rows[73]["IsEnable"]);
                PR.Reasonforlastvisit = SafeValue<bool>(dt.Rows[74]["IsEnable"]);
                PR.Dateofnextvisit = SafeValue<bool>(dt.Rows[75]["IsEnable"]);
                PR.Reasonfornextvisit = SafeValue<bool>(dt.Rows[76]["IsEnable"]);

            }

            return PR;
        }
        /// <summary>
        /// This Method used for API that will return the Inner services based on User setting.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static InnerService GetSpecialityForPatientForm(int UserId)
        {
            try
            {
                InnerService PR = new InnerService();
                ReferralFormDAL cl = new ReferralFormDAL();
                DataTable dt = new DataTable();
                List<AddPatient> PRobj = new List<AddPatient>();
                dt = cl.GetPatientFieldDetailsForAll(UserId);
                if (dt.Rows.Count > 0)
                {
                    foreach (PropertyInfo propertyInfo in PR.GetType().GetProperties())
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            var ColumnName = SafeValue<string>(row["XMLFieldName"]);
                            if (propertyInfo.Name == ColumnName)
                            {
                                propertyInfo.SetValue(PR, Convert.ChangeType(row["IsEnable"], propertyInfo.PropertyType));
                            }
                        }
                    }

                    foreach (DataRow row in dt.Rows)
                    {
                        var newObj = new AddPatient();
                        newObj.Id = SafeValue<int>(row["ServiceID"]);
                        newObj.ControlName = SafeValue<string>(row["ServiceName"]);
                        newObj.ParentFieldId = SafeValue<int>(row["ParentFieldId"]);
                        newObj.MapFieldName = SafeValue<string>(row["XMLFieldName"]);
                        newObj.IsEnable = SafeValue<bool>(row["IsEnable"]);
                        newObj.SpecialityId = SafeValue<int>(row["SpecialityId"]);
                        newObj.SortOrder = SafeValue<int>(row["SortOrder"]);

                        PRobj.Add(newObj);
                    }

                    PR.lstPatient = PRobj;

                }
                return PR;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormBLL---GetSpecialityForPatientForm", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        /// <summary>
        /// Get the Speciality for Patinet referal Setting Section in RecordLInc.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static SpecialityService GetPatientReferalVisibleSection(int UserId)
        {
            SpecialityService Sp = new SpecialityService();
            ReferralFormDAL cl = new ReferralFormDAL();

            DataTable dt = cl.GetPatientReferalVisibleSection(UserId);

            if (dt.Rows.Count > 0)
            {
                Sp.PIIsEnable = SafeValue<bool>(dt.Rows[0]["IsEnable"]);
                Sp.ICIsEnable = SafeValue<bool>(dt.Rows[1]["IsEnable"]);
                Sp.MHIsEnable = SafeValue<bool>(dt.Rows[2]["IsEnable"]);
                Sp.DHIsEnable = SafeValue<bool>(dt.Rows[3]["IsEnable"]);
            }
            return Sp;
        }

        public static List<TeamMembersOCR> GetTeamMembers(int UserId, BO.Enums.Common.PMSProviderFilterType ProviderType)
        {
            try
            {
                List<TeamMembersOCR> Lst = new List<TeamMembersOCR>();
                TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
                DataTable dt = ReferralFormDAL.GetTeamMemberDetailsOfDoctor(UserId, 1, int.MaxValue, ProviderType);

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        Lst.Add(new TeamMembersOCR()
                        {
                            DoctorId = ObjTripleDESCryptoHelper.encryptText(SafeValue<string>(SafeValue<int>(item["UserId"]))),
                            Name = (SafeValue<string>(item["FirstName"]) + " " + SafeValue<string>(item["LastName"])).Trim(),
                            id = SafeValue<int>(item["UserId"])
                        });
                    }
                }
                return Lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormBLL---GetTeamMembers", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static string GetPatientNameForAutoComplete(string PatientName)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                DataTable dt = new DataTable();
                StringBuilder StrResult = new StringBuilder();
                dt = PatientReport.SearchPatientName(PatientName);
                if (dt.Rows.Count > 0)
                {
                    sb.Append("[");
                    foreach (DataRow item in dt.Rows)
                    {
                        sb.Append("{'label':\"" + SafeValue<string>(item["PatientName"]) + "\",'value':\"" + SafeValue<string>(item["PatientName"]) + "\"},");
                    }
                    StrResult.Append(SafeValue<string>(sb).Substring(0, SafeValue<string>(sb).Length - 1));
                    StrResult.Append("]");
                }

                return SafeValue<string>(StrResult);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormBLL---GetPatientNameForAutoComplete", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static DentistDetail GetDentistDetail(string access_token)
        {
            try
            {
                DentistDetail Obj = new DentistDetail();
                DataTable Dt = clsCommon.GetDentistDetailsByAccessToken(access_token);
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    DataTable dtLogin = clsCommon.GetDentistDetailsByUserId(SafeValue<int>(Dt.Rows[0]["UserId"]));
                    if (dtLogin != null && dtLogin.Rows.Count > 0)
                    {
                        Obj.UserId = SafeValue<int>(Dt.Rows[0]["UserId"]);
                        Obj.FirstName = SafeValue<string>(dtLogin.Rows[0]["FirstName"]);
                        string ImageName = SafeValue<string>(dtLogin.Rows[0]["ImageName"]);
                        if (string.IsNullOrEmpty(ImageName))
                        {
                            Obj.ImageName = SafeValue<string>(ConfigurationManager.AppSettings["DefaultDoctorImage"]);
                        }
                        else
                        {
                            Obj.ImageName = SafeValue<string>(ConfigurationManager.AppSettings["DoctorImage"]) + ImageName;
                        }
                        Obj.ImageWithPath = Obj.ImageName;
                        DataTable dataTable = clsColleaguesData.CheckUserHasEnableOneClick(SafeValue<int>(Dt.Rows[0]["UserId"]));
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            Obj.IsReceiveReferral = SafeValue<bool>(dataTable.Rows[0]["IsReceiveReferral"]);
                            Obj.IsSendReferral = SafeValue<bool>(dataTable.Rows[0]["IsSendReferral"]);
                        }
                        Obj.MembershipDetails = new CommonBLL().GetUserPlanDetails(Convert.ToInt32(Obj.UserId));
                    }
                }
                return Obj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormBLL---GetDentistDetail", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int DeleteMessageFromOCR(DeleteMessage Obj)
        {
            try
            {
                bool Result = false;
                DataTable Dt = clsCommon.GetDentistDetailsByAccessToken(Obj.access_token);
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    if (Obj.MessageFrom == "Inbox")
                    {
                        Result = new clsColleaguesData().RemoveMessageFromInboxOfDoctor(SafeValue<int>(Dt.Rows[0]["UserId"]), Obj.MessageId);
                    }
                    else
                    {
                        Result = new clsColleaguesData().RemoveMessageFromSentBoxOfDoctor(SafeValue<int>(Dt.Rows[0]["UserId"]), Obj.MessageId);
                    }

                    return (Result) ? 1 : 0;
                }
                else
                {
                    return -1;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormBLL---DeleteMessageFromOCR", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int DeleteMessageOfOCRFromRL(DeleteMessage Obj, int UserId)
        {
            try
            {
                bool Result;
                if (Obj.MessageFrom == "Inbox")
                {
                    Result = new clsColleaguesData().RemoveMessageFromInboxOfDoctor(UserId, Obj.MessageId);
                }
                else
                {
                    Result = new clsColleaguesData().RemoveMessageFromSentBoxOfDoctor(UserId, Obj.MessageId);
                }
                return (Result) ? 1 : 0;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormBLL---DeleteMessageOfOCRFromRL", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Save Sequence for Patient Referral Form Settings 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool SaveManageSequence(FieldSequence data)
        {
            try
            {
                ReferralFormDAL obj = new ReferralFormDAL();
                bool Result = obj.SaveManageSequence(data);
                return Result;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormBLL---SaveManageSequence", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        /// <summary>
        /// Save Sequence for Patient Referral Form Settings 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static bool SavePatientManageSequence(FieldSequence data)
        {
            try
            {
                ReferralFormDAL obj = new ReferralFormDAL();
                bool Result = obj.SavePatientManageSequence(data);
                return Result;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ReferralFormBLL---SavePatientManageSequence", Ex.Message, Ex.StackTrace);
                throw;
            }

        }


    }
}

