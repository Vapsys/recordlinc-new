﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using RestSharp.Deserializers;
using BO.Models;
using DataAccessLayer.ColleaguesData;
using static BO.Enums.Common;
using System.Security.Cryptography;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public class RecurringBLL
    {

        private string signatureKey = SafeValue<string>(System.Configuration.ConfigurationManager.AppSettings["signatureKey"]);

        public WebhookEventType GetEventType(string eventType)
        {
            if (String.IsNullOrEmpty(eventType)) return WebhookEventType.UnknownEvent;

            eventType = eventType.Trim();

            // DO NOT change the order of these statements
            if (eventType.StartsWith("net.authorize.payment.fraud", StringComparison.InvariantCultureIgnoreCase))
            {
                if (eventType.Equals("net.authorize.payment.fraud.held", StringComparison.InvariantCultureIgnoreCase)
                    || eventType.Equals("net.authorize.payment.fraud.approved", StringComparison.InvariantCultureIgnoreCase)
                    || eventType.Equals("net.authorize.payment.fraud.declined", StringComparison.InvariantCultureIgnoreCase)
                )
                {
                    return WebhookEventType.FraudEvent;
                }
            }
            else if (eventType.StartsWith("net.authorize.customer.paymentProfile", StringComparison.InvariantCultureIgnoreCase))
            {
                if (eventType.Equals("net.authorize.customer.paymentProfile.created", StringComparison.InvariantCultureIgnoreCase)
                    || eventType.Equals("net.authorize.customer.paymentProfile.updated", StringComparison.InvariantCultureIgnoreCase)
                    || eventType.Equals("net.authorize.customer.paymentProfile.deleted", StringComparison.InvariantCultureIgnoreCase)
                )
                {
                    return WebhookEventType.PaymentProfileEvent;
                }
            }
            else if (eventType.StartsWith("net.authorize.customer.subscription", StringComparison.InvariantCultureIgnoreCase))
            {
                if (eventType.Equals("net.authorize.customer.subscription.created", StringComparison.InvariantCultureIgnoreCase)
                    || eventType.Equals("net.authorize.customer.subscription.updated", StringComparison.InvariantCultureIgnoreCase)
                    || eventType.Equals("net.authorize.customer.subscription.suspended", StringComparison.InvariantCultureIgnoreCase)
                    || eventType.Equals("net.authorize.customer.subscription.terminated", StringComparison.InvariantCultureIgnoreCase)
                    || eventType.Equals("net.authorize.customer.subscription.cancelled", StringComparison.InvariantCultureIgnoreCase)
                    || eventType.Equals("net.authorize.customer.subscription.expiring", StringComparison.InvariantCultureIgnoreCase)
                )
                {
                    return WebhookEventType.SubscriptionEvent;
                }
            }
            else if (eventType.StartsWith("net.authorize.customer", StringComparison.InvariantCultureIgnoreCase))
            {
                if (eventType.Equals("net.authorize.customer.created", StringComparison.InvariantCultureIgnoreCase)
                    || eventType.Equals("net.authorize.customer.updated", StringComparison.InvariantCultureIgnoreCase)
                    || eventType.Equals("net.authorize.customer.deleted", StringComparison.InvariantCultureIgnoreCase)
                )
                {
                    return WebhookEventType.CustomerEvent;
                }
            }

            else if (eventType.StartsWith("net.authorize.payment", StringComparison.InvariantCultureIgnoreCase))
            {
                if (eventType.Equals("net.authorize.payment.authorization.created", StringComparison.InvariantCultureIgnoreCase)
                    || eventType.Equals("net.authorize.payment.authcapture.created", StringComparison.InvariantCultureIgnoreCase)
                    || eventType.Equals("net.authorize.payment.capture.created", StringComparison.InvariantCultureIgnoreCase)
                    || eventType.Equals("net.authorize.payment.refund.created", StringComparison.InvariantCultureIgnoreCase)
                    || eventType.Equals("net.authorize.payment.priorAuthCapture.created", StringComparison.InvariantCultureIgnoreCase)
                    || eventType.Equals("net.authorize.payment.void.created", StringComparison.InvariantCultureIgnoreCase)
                )
                {
                    return WebhookEventType.PaymentEvent;
                }
            }
            else
                return WebhookEventType.UnknownEvent;


            // could not find a match
            return WebhookEventType.UnknownEvent;

        }

        public DataTable GetmemberExists(string jsonRawBody, string signatureKey)
        {
            customerEvent CustObj = new customerEvent();
            fraudEvent FEObj = new fraudEvent();
            paymentEvent PEObj = new paymentEvent();
            paymentProfileEvent PFObj = new paymentProfileEvent();
            subscriptionEvent SEObj = new subscriptionEvent();

            var jsonEventType = Evaluate(jsonRawBody, signatureKey);
            DataTable dt = new DataTable();
            switch (jsonEventType)
            {
                case WebhookEventType.SubscriptionEvent:
                    SEObj = (subscriptionEvent)this.Deserialize<subscriptionEvent>(jsonRawBody);
                    if (SEObj != null)
                    {
                        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
                        string CustomerId = SEObj.payload.profile.customerProfileId;
                        dt = ObjColleaguesData.GetMemberExists(CustomerId);
                    }
                    break;

                case WebhookEventType.CustomerEvent:
                    CustObj = (customerEvent)this.Deserialize<customerEvent>(jsonRawBody);
                    if (CustObj != null)
                    {
                        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
                        string CustomerId = CustObj.payload.id;
                        dt = ObjColleaguesData.GetMemberExists(CustomerId);
                    }
                    break;
            }
            return dt;
        }

        public string InsertRecurringData(string jsonRawBody, string X_Anet_Signature)
        {
            subscriptionEvent SEObj = new subscriptionEvent();
            customerEvent custObj = new customerEvent();
            paymentEvent payObj = new paymentEvent();
            paymentProfileEvent payProObj = new paymentProfileEvent();
            fraudEvent fruObj = new fraudEvent();

            string addData = "";
            var jsonEventType = Evaluate(jsonRawBody, X_Anet_Signature);
            switch (jsonEventType)
            {
                case WebhookEventType.SubscriptionEvent:
                    SEObj = (subscriptionEvent)this.Deserialize<subscriptionEvent>(jsonRawBody);
                    if (SEObj != null)
                    {
                        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
                        addData = ObjColleaguesData.InsertRecurringData(SEObj);
                        if (addData != "-1")
                        {
                            if (SEObj.eventType.Equals("net.authorize.customer.subscription.terminated", StringComparison.InvariantCultureIgnoreCase)
                   || SEObj.eventType.Equals("net.authorize.customer.subscription.cancelled", StringComparison.InvariantCultureIgnoreCase)
                   || SEObj.eventType.Equals("net.authorize.customer.subscription.expiring", StringComparison.InvariantCultureIgnoreCase)
               )
                            {
                                PaymentBLL.AddPaymentBlockingHistory(0, true, SafeValue<int>(addData));
                            }
                        }
                    }
                    break;
                case WebhookEventType.CustomerEvent:
                    custObj = (customerEvent)this.Deserialize<customerEvent>(jsonRawBody);
                    if (custObj != null)
                    {
                        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
                        addData = ObjColleaguesData.InsertRecurringData(custObj);
                    }
                    break;
                case WebhookEventType.PaymentEvent:
                    payObj = (paymentEvent)this.Deserialize<paymentEvent>(jsonRawBody);
                    if (payObj != null)
                    {
                        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
                        addData = ObjColleaguesData.InsertRecurringData(payObj);
                        if (addData != "-1")
                        {
                            if (payObj.eventType.Equals("net.authorize.payment.void.created", StringComparison.InvariantCultureIgnoreCase))
                            {
                                PaymentBLL.AddPaymentBlockingHistory(0, true, SafeValue<int>(addData));
                            }
                        }
                    }
                    break;
                case WebhookEventType.PaymentProfileEvent:
                    payProObj = (paymentProfileEvent)this.Deserialize<paymentProfileEvent>(jsonRawBody);
                    if (payProObj != null)
                    {
                        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
                        addData = ObjColleaguesData.InsertRecurringData(payProObj);
                    }
                    break;
                case WebhookEventType.FraudEvent:
                    fruObj = (fraudEvent)this.Deserialize<fraudEvent>(jsonRawBody);
                    if (fruObj != null)
                    {
                        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
                        addData = ObjColleaguesData.InsertRecurringData(fruObj);
                    }
                    break;
            }
            return addData;
        }

        public WebhookEventType Evaluate(string jsonRawBody, string AnetSignature)
        {


            // evaluate the input parameters
            //if (String.IsNullOrEmpty(jsonRawBody)) return WebhookEventType.UnknownEvent;
            //if (String.IsNullOrEmpty(AnetSignature)) return WebhookEventType.UnknownEvent;

            // make sure the tokens match
            //if (!CheckTokens(jsonRawBody, AnetSignature)) return WebhookEventType.InvalidEvent;

            // use RestSharp to deserialize
            var eventResponse = this.Deserialize<WebHookEventResponse>(jsonRawBody);

            if (eventResponse == null) return WebhookEventType.UnknownEvent;
            if (String.IsNullOrEmpty(eventResponse.eventType)) return WebhookEventType.UnknownEvent;

            var eventType = GetEventType(eventResponse.eventType);

            return eventType;

        }

        private bool CheckTokens(string data, string AnetSignature)
        {
            if (String.IsNullOrEmpty(data)) return false;
            if (String.IsNullOrEmpty(AnetSignature)) return false;
            if (String.IsNullOrEmpty(signatureKey)) return false;

            // generate the shaw token
            var token = GetSHAToken(data, signatureKey);
            System.IO.File.AppendAllText("C:\\Authroize\\log.txt", "AnetSignature ============== " + AnetSignature + Environment.NewLine);
            System.IO.File.AppendAllText("C:\\Authroize\\log.txt", "token ============== " + token + Environment.NewLine);
            if (String.IsNullOrEmpty(token)) return false;

            return token.Equals(AnetSignature, StringComparison.InvariantCultureIgnoreCase);

        }

        private string GetSHAToken(string data, string key)
        {
            // use Encoding.ASCII.GetBytes or Encoding.UTF8.GetBytes

            byte[] _key = Encoding.ASCII.GetBytes(key);
            using (var myhmacsha1 = new HMACSHA1(_key))
            {
                var hashArray = new HMACSHA512(_key).ComputeHash(Encoding.ASCII.GetBytes(data));

                return hashArray.Aggregate("", (s, e) => s + String.Format("{0:x2}", e), s => s);
            }

        }

        private T Deserialize<T>(string jsonRawBody) where T : WebHookEventResponse
        {
            RestResponse response = new RestResponse();
            response.Content = jsonRawBody;
            var deserial = new JsonDeserializer();
            T result = deserial.Deserialize<T>(response);
            var temp = (WebHookEventResponse)result;
            temp.responseBody = jsonRawBody;
            return result;

        }
    }
}
