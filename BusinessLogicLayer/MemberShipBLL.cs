﻿using BO.Models;
using DataAccessLayer.Admin;
using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BO.ViewModel;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public class MemberShipBLL
    {
        MembershipDLL objDAL = new MembershipDLL();
        clsCommon ObjCommon = new clsCommon();
        #region MemberShip
        public bool InsertUpdate(Membership objMembership)
        {
            return objDAL.InsertUpdate(objMembership);
        }

        public List<Membership> GetAllMembership(int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext)
        {
            DataTable dt = new DataTable();
            List<Membership> objlst = new List<Membership>();
            try
            {
                dt = objDAL.GetAllMembershipList(PageIndex, PageSize, SortColumn, SortDirection, Searchtext);
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objlst.Add(new Membership
                        {
                            Membership_Id = SafeValue<int>(dt.Rows[i]["Membership_Id"]),
                            Title = SafeValue<string>(dt.Rows[i]["Title"]), 
                            Description = Regex.Replace(System.Uri.UnescapeDataString(SafeValue<string>(dt.Rows[i]["Description"])), @"<[^>]+>|&nbsp;", "").Trim(),
                            MonthlyPrice = SafeValue<decimal>(dt.Rows[i]["MonthlyPrice"]),
                            QuaterlyPrice = SafeValue<decimal>(dt.Rows[i]["QuaterlyPrice"]),
                            HalfYearlyPrice = SafeValue<decimal>(dt.Rows[i]["HalfYearlyPrice"]),
                            AnnualPrice = SafeValue<decimal>(dt.Rows[i]["AnnualPrice"]),
                            SortOrder = SafeValue<int>(dt.Rows[i]["SortOrder"]),
                            Status = SafeValue<int>(dt.Rows[i]["Status"]),

                        });
                    }
                }
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("MembershipBLL---GetAllMembership", Ex.Message, Ex.StackTrace);
                throw;
            }
            return objlst;
        }

        public Membership GetById(int Id)
        {
            DataTable dt = new DataTable();
            Membership objcreate = new Membership();
            try
            {
                dt = objDAL.GetById(Id);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objcreate.Membership_Id = SafeValue<int>(dt.Rows[0]["Membership_Id"]);
                    objcreate.Title = SafeValue<string>(dt.Rows[0]["Title"]);
                    objcreate.Description = SafeValue<string>(dt.Rows[0]["Description"]);
                    objcreate.MonthlyPrice = SafeValue<decimal>(dt.Rows[0]["MonthlyPrice"]);
                    objcreate.QuaterlyPrice = SafeValue<decimal>(dt.Rows[0]["QuaterlyPrice"]);
                    objcreate.HalfYearlyPrice = SafeValue<decimal>(dt.Rows[0]["HalfYearlyPrice"]);
                    objcreate.AnnualPrice = SafeValue<decimal>(dt.Rows[0]["AnnualPrice"]);
                    objcreate.SortOrder = SafeValue<int>(dt.Rows[0]["SortOrder"]);
                    objcreate.Status = SafeValue<int>(dt.Rows[0]["Status"]);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objcreate;
        }

        public bool Remove(int Id)
        {
            return objDAL.RemoveById(Id);
        }
        public int GetMaxSortOrder()
        {

            return ObjCommon.GetMaxSortOrder("Membership");
        }
        public List<object> GetStatusList()
        {
            var enumVals = new List<object>();
            foreach (var item in Enum.GetValues(typeof(BO.Enums.Common.StatusType)))
            {
                enumVals.Add(new
                {
                    id = (int)item,
                    name = SafeValue<string>(item)
                });
            }
            return enumVals;
        }
        public MemberByMemberShip GetMemberListByMembership(int UserId,int MembershipId,string SearchText,int PageSize,int PageIndex,int SortDirection,int SortColumn)
        {
            try
            {
                DataTable Dt = new DataTable();
                
                MemberByMemberShip Obj = new MemberByMemberShip();
                Dt = objDAL.GetMemberDetialsByMembership(UserId,MembershipId, SortDirection,SortColumn,PageSize,PageIndex,SearchText);
                foreach (DataRow item in Dt.Rows)
                {
                    Obj.lstMemberofMembership.Add(new MembershipOfMember()
                    {
                        UserId = SafeValue<int>(item["UserId"]),
                        FirstName = SafeValue<string>(item["DoctorName"]),
                        MembershipName = SafeValue<string>(item["Plans"]),
                        Price = SafeValue<string>(item["Price"]),
                        Interval = SafeValue<string>(item["Interval"]),
                        PaymentMode = SafeValue<string>(item["PaymentMode"]),
                        TotalCount = SafeValue<string>(item["TotalRecord"]),
                        MembershipId = SafeValue<string>(item["Membership_Id"])                        
                    });
                    Obj.TotalRecord = SafeValue<int>(item["TotalRecord"]);
                }
                return Obj;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("MembershipBLL--GetMemberListByMembership", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /* Devansh: Not needed as of now.
        public MemberByMemberShip GetMemberListByForFreeMember(int UserId, int MembershipId, string SearchText, int PageSize, int PageIndex, int SortDirection, int SortColumn)
        {
            try
            {
                DataTable Dt = new DataTable();

                MemberByMemberShip Obj = new MemberByMemberShip();
                Dt = objDAL.GetMemberDetialsByMembershipForFree(UserId, MembershipId, SortDirection, SortColumn, PageSize, PageIndex, SearchText);
                foreach (DataRow item in Dt.Rows)
                {
                    Obj.lstMemberofMembership.Add(new MembershipOfMember()
                    {
                        UserId = SafeValue<int>(item["UserId"]),
                        FirstName = SafeValue<string>(item["DoctorName"]),
                        MembershipName = SafeValue<string>(item["Plans"]),
                        Price = SafeValue<string>(item["Price"]),
                        Interval = "",
                        PaymentMode = "",
                        TotalCount = SafeValue<string>(item["TotalRecord"]),
                        MembershipId = SafeValue<string>(item["Membership_Id"])
                    });
                    Obj.TotalRecord = SafeValue<int>(item["TotalRecord"]);
                }                
                return Obj;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("MembershipBLL--GetMemberListByMembership", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        */

        public MemberByMemberShip GetAllMemberListByMembership(int UserId, int MembershipId, string SearchText, int PageSize, int PageIndex, int SortDirection, int SortColumn)
        {
            try
            {
                DataTable dt = new DataTable();
                MemberByMemberShip Obj = new MemberByMemberShip();
                dt = objDAL.GetAllMemberListByMembership(UserId, MembershipId, SortDirection, SortColumn, PageSize, PageIndex, SearchText);
                foreach (DataRow item in dt.Rows)
                {
                    Obj.lstMemberofMembership.Add(new MembershipOfMember()
                    {
                        UserId = SafeValue<int>(item["UserId"]),
                        FirstName = SafeValue<string>(item["DoctorName"]),
                        MembershipName = SafeValue<string>(item["Plans"]),
                        Price = SafeValue<string>(item["Price"]),
                        Interval = SafeValue<string>(item["Interval"]),
                        PaymentMode = SafeValue<string>(item["PaymentMode"]),
                        TotalCount = SafeValue<string>(item["TotalRecord"]),
                        MembershipId = SafeValue<string>(item["Membership_Id"])
                    });
                    Obj.TotalRecord = SafeValue<int>(item["TotalRecord"]);
                }
                return Obj;
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("MembershipBLL--GetAllMemberListByMembership", ex.Message, ex.StackTrace);
                throw;
            }
        }
        #endregion
    }
}
