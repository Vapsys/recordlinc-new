﻿using BO.Models;
using BO.ViewModel;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.PatientsData;
using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using DataAccessLayer;
using System.Text;
using static DataAccessLayer.Common.clsCommon;
using System.Web;
using BO.Enums;
using System.IO;
using Newtonsoft.Json;
using System.Web.Mvc;

namespace BusinessLogicLayer
{
    public class DentistProfileBLL
    {
        string VcardFilePath = System.Configuration.ConfigurationManager.AppSettings["VcardFilePath"];
        public DentistProfileBLL() { }
        clsCommon objCommon = new clsCommon();
        clsTemplate ObjTemplate = new clsTemplate();
        clsColleaguesData objColleaguesData = new clsColleaguesData();
        clsPatientsData ObjPatientData = new clsPatientsData();
        string DoctorImage = SafeValue<string>(System.Configuration.ConfigurationManager.AppSettings.Get("DoctorImage"));
        string DefaultDoctorImage = SafeValue<string>(System.Configuration.ConfigurationManager.AppSettings.Get("DefaultDoctorImage"));
        CommonBLL objcommonBLL = new CommonBLL();
        public DentistProfileDetail DoctorPublicProfile(int UserId)
        {
            DentistProfileDetail objProfileDetails = new DentistProfileDetail();
            TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
            clsCompany objcompany = new clsCompany();
            DataSet ds = new DataSet();
            try
            {
                objcompany.GetActiveCompany();
                ds = objColleaguesData.GetDoctorDetailsById(UserId);
                if (ds != null && ds.Tables.Count > 0)
                {
                    objProfileDetails = GetDoctorDetials(UserId, ds);
                    objProfileDetails.lstBannerDetails = GetBannersByUserId(objProfileDetails.UserId);
                    objProfileDetails.lstratingList = GetReviewsByUserId(objProfileDetails.UserId);
                    DentistRatingDAL objRatingDAL = new DentistRatingDAL();
                    objProfileDetails.RatingCount = objRatingDAL.GetDentistRatingsCount(objProfileDetails.UserId, 1);
                    objProfileDetails.Phone = objCommon.ConvertPhoneNumber(objProfileDetails.lstDoctorAddressDetails.Where(p => (int)p.AddressInfoID == (int)objProfileDetails.LocationId).FirstOrDefault() != null ? objProfileDetails.lstDoctorAddressDetails.Where(p => (int)p.AddressInfoID == (int)objProfileDetails.LocationId).FirstOrDefault().Phone : null);
                    objProfileDetails.ObjLDJSON = BindLdJsonObject(objProfileDetails.RatingCount, objProfileDetails.lstratingList, objProfileDetails);
                    JsonConvert.SerializeObject(objProfileDetails.ObjLDJSON);
                }
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("DentistProfileBLL---DoctorPublicProfile", ex.Message, ex.StackTrace);
                throw ex;
            }
            finally
            {
                ObjTripleDESCryptoHelper = null; objcompany = null; ds.Dispose(); ds.Clear();
            }
            objProfileDetails.PlanDetails = objcommonBLL.GetUserPlanDetails(UserId);
            return objProfileDetails;
        }
        //Ankit Changes for I Luv My Dentist API separate out the function.
        public DentistProfileDetail GetDoctorDetials(int UserId, DataSet ds = null)
        {
            DentistProfileDetail objProfileDetails = new DentistProfileDetail();
            TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
            clsCompany objcompany = new clsCompany();
            int ParentUserId = 0;
            try
            {
                if (ds == null)
                {
                    ds = objColleaguesData.GetDoctorDetailsById(UserId);
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            objProfileDetails.UserId = SafeValue<int>(ds.Tables[0].Rows[0]["UserId"]);
                            objProfileDetails.PublicProfileUrl = SafeValue<string>(ds.Tables[0].Rows[0]["PublicProfileUrl"]);
                            ParentUserId = (ds.Tables[0].Rows[0]["ParentUserId"] as int?).GetValueOrDefault(0) == 0 ? SafeValue<int>(ds.Tables[0].Rows[0]["UserId"]) : SafeValue<int>(ds.Tables[0].Rows[0]["ParentUserId"]);
                            objProfileDetails.EncryptUserId = ObjTripleDESCryptoHelper.encryptText(SafeValue<string>(ds.Tables[0].Rows[0]["UserId"]));
                            objProfileDetails.FirstName = SafeValue<string>(ds.Tables[0].Rows[0]["FirstName"]);
                            objProfileDetails.MiddleName = SafeValue<string>(ds.Tables[0].Rows[0]["MiddleName"]);
                            objProfileDetails.LastName = SafeValue<string>(ds.Tables[0].Rows[0]["LastName"]);
                            objProfileDetails.EmailId = SafeValue<string>(ds.Tables[0].Rows[0]["Username"]);
                            objProfileDetails.Salutation = SafeValue<string>(ds.Tables[0].Rows[0]["Salutation"]);
                            string ImageName = SafeValue<string>(ds.Tables[0].Rows[0]["ImageName"]);
                            if (objProfileDetails.lstDoctorAddressDetails.Where(p => !string.IsNullOrWhiteSpace(p.Mobile)).Any())
                            {
                                objProfileDetails.Mobile = objProfileDetails.lstDoctorAddressDetails.Where(p => !string.IsNullOrWhiteSpace(p.Mobile)).FirstOrDefault().Mobile;
                            }
                            if (string.IsNullOrEmpty(ImageName))
                            {
                                objProfileDetails.ImageName = ConfigurationManager.AppSettings.Get("DoctorProfileImage");
                            }
                            else
                            {
                                objProfileDetails.ImageName = DoctorImage + ImageName;

                            }
                            objProfileDetails.Title = SafeValue<string>(ds.Tables[0].Rows[0]["Title"]);
                            objProfileDetails.Description = SafeValue<string>(ds.Tables[0].Rows[0]["Description"]);
                            objProfileDetails.WebsiteURL = SafeValue<string>(ds.Tables[0].Rows[0]["WebsiteURL"]);
                            objProfileDetails.LocationId = SafeValue<int>(ds.Tables[0].Rows[0]["LocationId"]);
                            objProfileDetails.RatingValue = SafeValue<double>(ds.Tables[0].Rows[0]["RatingValue"]);
                        }

                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            objProfileDetails.OfficeName =SafeValue<string>(ds.Tables[1].Rows[0]["AccountName"]);
                        }
                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            objProfileDetails.lstDoctorAddressDetails = GetDoctorAddressDetailsById(ds.Tables[2]);
                        }
                        if (objProfileDetails.lstDoctorAddressDetails.Count > 0)
                        {
                            AddressDetails address = objProfileDetails.lstDoctorAddressDetails.First(x => x.ContactType == 1);
                            objProfileDetails.City = address.City;
                            objProfileDetails.State = address.State;
                            objProfileDetails.ZipCode = address.ZipCode;
                            objProfileDetails.Country = address.Country;
                            objProfileDetails.Address = address.ExactAddress + (!string.IsNullOrEmpty(address.Address2) ? ", " + address.Address2 : "");

                        }
                        if (ds.Tables[3].Rows.Count > 0)
                        {
                            objProfileDetails.lstGetSocialMediaDetailByUserId = GetSocialMediaDetailByUserId(ds.Tables[3]);
                        }
                        if (ds.Tables[5].Rows.Count > 0)
                        {
                            objProfileDetails.lstProfessionalMemberships = GetProfessionalMembershipsByUserId(ds.Tables[5]);
                        }
                        if (ds.Tables[6].Rows.Count > 0)
                        {
                            objProfileDetails.lstSpeacilitiesOfDoctor = GetSpeacilitiesOfDoctorById(ds.Tables[6]);
                        }
                        if (ds.Tables[11].Rows.Count > 0)
                        {
                            objProfileDetails.lstsecondarywebsitelist = GetSecondaryWebsitelistByUserId(ds.Tables[11]);

                        }
                        objProfileDetails.lstratingList = GetReviewsByUserId(objProfileDetails.UserId);
                        DentistRatingDAL objRatingDAL = new DentistRatingDAL();
                        objProfileDetails.RatingCount = objRatingDAL.GetDentistRatingsCount(objProfileDetails.UserId, 1);
                        objProfileDetails.Phone = objCommon.ConvertPhoneNumber(objProfileDetails.lstDoctorAddressDetails.Where(p => (int)p.AddressInfoID == (int)objProfileDetails.LocationId).FirstOrDefault() != null ? objProfileDetails.lstDoctorAddressDetails.Where(p => (int)p.AddressInfoID == (int)objProfileDetails.LocationId).FirstOrDefault().Phone : null);
                        objProfileDetails.Mobile = objCommon.ConvertPhoneNumber(objProfileDetails.lstDoctorAddressDetails.Where(p => (int)p.AddressInfoID == (int)objProfileDetails.LocationId).FirstOrDefault() != null ? objProfileDetails.lstDoctorAddressDetails.Where(p => (int)p.AddressInfoID == (int)objProfileDetails.LocationId).FirstOrDefault().Mobile : null);
                    }
                }
                else
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        objProfileDetails.UserId = SafeValue<int>(ds.Tables[0].Rows[0]["UserId"]);
                        objProfileDetails.PublicProfileUrl = SafeValue<string>(ds.Tables[0].Rows[0]["PublicProfileUrl"]);
                        ParentUserId = (ds.Tables[0].Rows[0]["ParentUserId"] as int?).GetValueOrDefault(0) == 0 ? SafeValue<int>(ds.Tables[0].Rows[0]["UserId"]) : SafeValue<int>(ds.Tables[0].Rows[0]["ParentUserId"]);
                        objProfileDetails.EncryptUserId = ObjTripleDESCryptoHelper.encryptText(SafeValue<string>(ds.Tables[0].Rows[0]["UserId"]));
                        objProfileDetails.FirstName = SafeValue<string>(ds.Tables[0].Rows[0]["FirstName"]);
                        objProfileDetails.MiddleName = SafeValue<string>(ds.Tables[0].Rows[0]["MiddleName"]);
                        objProfileDetails.LastName = SafeValue<string>(ds.Tables[0].Rows[0]["LastName"]);
                        string ImageName = SafeValue<string>(ds.Tables[0].Rows[0]["ImageName"]);

                        if (string.IsNullOrEmpty(ImageName))
                        {
                            objProfileDetails.ImageName = ConfigurationManager.AppSettings.Get("DoctorProfileImage");
                        }
                        else
                        {
                            objProfileDetails.ImageName = DoctorImage + ImageName;

                        }
                        objProfileDetails.Title = SafeValue<string>(ds.Tables[0].Rows[0]["Title"]);
                        objProfileDetails.Description = SafeValue<string>(ds.Tables[0].Rows[0]["Description"]);
                        objProfileDetails.WebsiteURL = SafeValue<string>(ds.Tables[0].Rows[0]["WebsiteURL"]);
                        objProfileDetails.LocationId = SafeValue<int>(ds.Tables[0].Rows[0]["LocationId"]);
                        objProfileDetails.RatingValue = SafeValue<double>(ds.Tables[0].Rows[0]["RatingValue"]);
                    }

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        objProfileDetails.OfficeName = SafeValue<string>(ds.Tables[1].Rows[0]["AccountName"]);
                    }
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        objProfileDetails.lstDoctorAddressDetails = GetDoctorAddressDetailsById(ds.Tables[2]);
                    }
                    if (objProfileDetails.lstDoctorAddressDetails.Count > 0)
                    {
                        AddressDetails address = objProfileDetails.lstDoctorAddressDetails.First(x => x.ContactType == 1);
                        objProfileDetails.City = address.City;
                        objProfileDetails.State = address.State;
                        objProfileDetails.ZipCode = address.ZipCode;
                        objProfileDetails.Country = address.Country;
                        objProfileDetails.Address = address.ExactAddress + (!string.IsNullOrEmpty(address.Address2) ? ", " + address.Address2 : "");

                    }

                    //Display Primary Address First on Public Profile.
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        objProfileDetails.lstDoctorSortAddressDetails = objProfileDetails.lstDoctorAddressDetails;
                        objProfileDetails.lstDoctorAddressDetails = objProfileDetails.lstDoctorSortAddressDetails.Where(x => x.ContactType == 1).ToList();
                        objProfileDetails.lstDoctorAddressDetails.AddRange(objProfileDetails.lstDoctorSortAddressDetails.Where(x => x.ContactType != 1).ToList());
                    }

                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        objProfileDetails.lstGetSocialMediaDetailByUserId = GetSocialMediaDetailByUserId(ds.Tables[3]);
                    }
                    if (ds.Tables[4].Rows.Count > 0)
                    {
                        objProfileDetails.lstEducationandTraining = GetEducationandTrainingDetailsForDoctor(ds.Tables[4]);
                    }
                    if (ds.Tables[5].Rows.Count > 0)
                    {
                        objProfileDetails.lstProfessionalMemberships = GetProfessionalMembershipsByUserId(ds.Tables[5]);
                    }
                    objProfileDetails.lstTeamMemberDetailsForDoctor = GetTeamMemberDetailsOfDoctor(objProfileDetails.UserId, 1, 15);
                    if (ds.Tables[6].Rows.Count > 0)
                    {
                        objProfileDetails.lstSpeacilitiesOfDoctor = GetSpeacilitiesOfDoctorById(ds.Tables[6]);
                    }
                    if (ds.Tables[11].Rows.Count > 0)
                    {
                        objProfileDetails.lstsecondarywebsitelist = GetSecondaryWebsitelistByUserId(ds.Tables[11]);

                    }
                    if (ds.Tables[12].Rows.Count > 0)
                    {
                        objProfileDetails.lstInsuranceDetails = GetInsuranceDetailsOfDoctor(ds.Tables[12]);

                    }
                    if (ds.Tables[13].Rows.Count > 0)
                    {
                        objProfileDetails.lstGallaryDetails = GetGallaryDetailsOfDoctor(ds.Tables[13]);

                    }
                    if (ds.Tables[14].Rows != null && ds.Tables[14].Rows.Count > 0)
                    {
                        objProfileDetails.lstOffers = GetSpecialOfferDetails(ds.Tables[14]);

                    }
                }
                return objProfileDetails;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("DentistProfileBLL---GetDoctorDetials", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        #region Code For WebSite Details
        public List<WebsiteDetails> GetSecondaryWebsitelistByUserId(DataTable dt)
        {
            clsCommon objCommon = new clsCommon();
            List<WebsiteDetails> lstsecondarywebsitelist = new List<WebsiteDetails>();
            try
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstsecondarywebsitelist.Add(new WebsiteDetails()
                        {
                            SecondaryWebsiteId = SafeValue<int>(row["SecondaryId"]),
                            SecondaryWebsiteurl = SafeValue<string>(row["SecondaryWebsite"]),
                        });
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return lstsecondarywebsitelist;

        }
        #endregion
        #region Code For Social Media Details
        public List<SocialMediaDetails> GetSocialMediaDetailByUserId(DataTable dt)
        {

            List<SocialMediaDetails> lstGetSocialMediaDetailByUserId = new List<SocialMediaDetails>();
            try
            {
                #region Start New

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstGetSocialMediaDetailByUserId.Add(new SocialMediaDetails()
                        {
                            FacebookUrl = SafeValue<string>(row["FacebookUrl"]) != string.Empty ? (SafeValue<string>(row["FacebookUrl"]).Contains("http://") || SafeValue<string>(row["FacebookUrl"]).Contains("https://") ? SafeValue<string>(row["FacebookUrl"]) : "http://" + SafeValue<string>(row["FacebookUrl"])) : SafeValue<string>(row["FacebookUrl"]),
                            LinkedinUrl = SafeValue<string>(row["LinkedinUrl"]) != string.Empty ? (SafeValue<string>(row["LinkedinUrl"]).Contains("http://") || SafeValue<string>(row["LinkedinUrl"]).Contains("https://") ? SafeValue<string>(row["LinkedinUrl"]) : "http://" + SafeValue<string>(row["LinkedinUrl"])) : SafeValue<string>(row["LinkedinUrl"]),
                            TwitterUrl = SafeValue<string>(row["TwitterUrl"]) != string.Empty ? (SafeValue<string>(row["TwitterUrl"]).Contains("http://") || SafeValue<string>(row["TwitterUrl"]).Contains("https://") ? SafeValue<string>(row["TwitterUrl"]) : "http://" + SafeValue<string>(row["TwitterUrl"])) : SafeValue<string>(row["TwitterUrl"]),
                            GoogleplusUrl = SafeValue<string>(row["GoogleplusUrl"]) != string.Empty ? (SafeValue<string>(row["GoogleplusUrl"]).Contains("http://") || SafeValue<string>(row["GoogleplusUrl"]).Contains("https://") ? SafeValue<string>(row["GoogleplusUrl"]) : "http://" + SafeValue<string>(row["GoogleplusUrl"])) : SafeValue<string>(row["GoogleplusUrl"]),
                            YoutubeUrl = SafeValue<string>(row["YoutubeUrl"]) != string.Empty ? (SafeValue<string>(row["YoutubeUrl"]).Contains("http://") || SafeValue<string>(row["YoutubeUrl"]).Contains("https://") ? SafeValue<string>(row["YoutubeUrl"]) : "http://" + SafeValue<string>(row["YoutubeUrl"])) : SafeValue<string>(row["YoutubeUrl"]),
                            PinterestUrl = SafeValue<string>(row["PinterestUrl"]) != string.Empty ? (SafeValue<string>(row["PinterestUrl"]).Contains("http://") || SafeValue<string>(row["PinterestUrl"]).Contains("https://") ? SafeValue<string>(row["PinterestUrl"]) : "http://" + SafeValue<string>(row["PinterestUrl"])) : SafeValue<string>(row["PinterestUrl"]),
                            BlogUrl = SafeValue<string>(row["BlogUrl"]) != string.Empty ? (SafeValue<string>(row["BlogUrl"]).Contains("http://") || SafeValue<string>(row["BlogUrl"]).Contains("https://") ? SafeValue<string>(row["BlogUrl"]) : "http://" + SafeValue<string>(row["BlogUrl"])) : SafeValue<string>(row["BlogUrl"]),
                            YelpUrl = SafeValue<string>(row["YelpUrl"]) != string.Empty ? (SafeValue<string>(row["YelpUrl"]).Contains("http://") || SafeValue<string>(row["YelpUrl"]).Contains("https://") ? SafeValue<string>(row["YelpUrl"]) : "http://" + SafeValue<string>(row["YelpUrl"])) : SafeValue<string>(row["YelpUrl"]),
                        });
                    }
                }
                #endregion
            }
            catch (Exception)
            {
                throw;
            }

            return lstGetSocialMediaDetailByUserId;
        }

        //public void InsertInsuranceCoverage(BO.ViewModel.InsuranceCoverage objInsuranceCoverage, int v)
        //{
        //    throw new NotImplementedException();
        //}
        #endregion
        #region Code For EducationandTraining Details
        public List<EducationandTraining> GetEducationandTrainingDetailsForDoctor(DataTable dt)
        {
            List<EducationandTraining> lstEducationandTraining = new List<EducationandTraining>();
            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(SafeValue<string>(dt.Rows[i]["Institute"])))
                        {
                            lstEducationandTraining.Add(new EducationandTraining { Id = SafeValue<int>(dt.Rows[i]["Id"]), Institute = SafeValue<string>(dt.Rows[i]["Institute"]), Specialisation = SafeValue<string>(dt.Rows[i]["Specialisation"]), YearAttended = SafeValue<string>(dt.Rows[i]["YearAttended"]) });
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                dt.Dispose(); dt.Clear();
            }
            return lstEducationandTraining;
        }
        #endregion
        #region Code For SpeacilitiesOfDoctor Details
        public List<SpeacilitiesOfDoctor> GetSpeacilitiesOfDoctorById(DataTable dt)
        {
            List<SpeacilitiesOfDoctor> lstSpeacilities = new List<SpeacilitiesOfDoctor>();
            try
            {
                if (dt.Rows.Count > 0)
                {
                    lstSpeacilities = (from p in dt.AsEnumerable()
                                       select new SpeacilitiesOfDoctor
                                       {
                                           SpecialtyId = int.Parse(string.IsNullOrWhiteSpace(SafeValue<string>(p["SpecialityId"])) ? "0" : SafeValue<string>(p["SpecialityId"])),
                                           SpecialtyDescription = SafeValue<string>(p["Specialities"]),
                                       }).ToList();


                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                dt.Dispose(); dt.Clear();
            }
            return lstSpeacilities;

        }
        #endregion
        #region Code For GetInsuranceDetails 
        public List<InsuranceDetails> GetInsuranceDetailsOfDoctor(DataTable dt)
        {
            List<InsuranceDetails> lstInsurance = new List<InsuranceDetails>();
            try
            {
                if (dt.Rows.Count > 0)
                {
                    lstInsurance = (from p in dt.AsEnumerable()
                                    select new InsuranceDetails
                                    {
                                        Name = SafeValue<string>(p["Name"]),
                                        Logo = SafeValue<string>(p["Logo"]),
                                        Description = SafeValue<string>(p["Description"]),
                                        Link = SafeValue<string>(p["Link"]),
                                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                dt.Dispose(); dt.Clear();
            }
            return lstInsurance;

        }
        #endregion
        #region Code For TeamMemberDetailsOfDoctor Details
        public List<TeamMemberDetailsForDoctor> GetTeamMemberDetailsOfDoctor(int ParentUserId, int PageIndex, int PageSize)
        {
            DataTable dt = new DataTable();
            List<TeamMemberDetailsForDoctor> lstTeamMemberDetailsForDoctor = new List<TeamMemberDetailsForDoctor>();
            try
            {
                int UserId = SafeValue<int>(!string.IsNullOrWhiteSpace(SafeValue<string>(HttpContext.Current.Session["UserId"])) ? HttpContext.Current.Session["UserId"] : 0);
                dt = objColleaguesData.GetTeamMemberDetailsOfDoctor(ParentUserId, PageIndex, PageSize);
                if (dt != null && dt.Rows.Count > 0)
                {
                    lstTeamMemberDetailsForDoctor = (from p in dt.AsEnumerable()
                                                     select new TeamMemberDetailsForDoctor
                                                     {
                                                         UserId = SafeValue<int>(p["UserId"]),
                                                         FirstName = SafeValue<string>(p["FirstName"]),
                                                         LastName = SafeValue<string>(p["LastName"]),
                                                         SpecialtyDescription = SafeValue<string>(p["Speciality"]),
                                                         PublicProfileUrl = SafeValue<string>(p["PublicProfileUrl"]),
                                                         AccountName = SafeValue<string>(p["AccountName"]),
                                                         Image = SafeValue<string>(p["ImageName"]),
                                                         Gender = SafeValue<string>(p["Gender"]),
                                                         RatingValue = SafeValue<double>(p["RatingValue"]),
                                                     }).Where(x => x.UserId != UserId).ToList();
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                dt.Dispose(); dt.Clear();
            }
            return lstTeamMemberDetailsForDoctor;
        }
        #endregion
        #region Code For ProfessionalMemberships Details
        public List<ProfessionalMemberships> GetProfessionalMembershipsByUserId(DataTable dt)
        {
            List<ProfessionalMemberships> lstProfessionalMemberships = new List<ProfessionalMemberships>();
            try
            {

                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(SafeValue<string>(dt.Rows[i]["Membership"])))
                        {
                            lstProfessionalMemberships.Add(new ProfessionalMemberships { Id = SafeValue<int>(dt.Rows[i]["Id"]), Membership = SafeValue<string>(dt.Rows[i]["Membership"]) });
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                dt.Dispose(); dt.Clear();
            }

            return lstProfessionalMemberships;
        }
        #endregion
        #region Code For DoctorAddressDetails Details
        public List<AddressDetails> GetDoctorAddressDetailsById(DataTable dt)
        {
            List<AddressDetails> lstDoctorAddressDetails = new List<AddressDetails>();
            try
            {

                objColleaguesData = new clsColleaguesData();
                if (dt != null && dt.Rows.Count > 0)
                {
                    //For Address details of Doctor

                    lstDoctorAddressDetails = (from p in dt.AsEnumerable()
                                               select new AddressDetails
                                               {
                                                   AddressInfoID = SafeValue<int>(p["AddressInfoID"]),
                                                   Location = SafeValue<string>(p["LocationName"]),
                                                   ExactAddress = SafeValue<string>(p["ExactAddress"]),
                                                   Address2 = SafeValue<string>(p["Address2"]),
                                                   City = SafeValue<string>(p["City"]),
                                                   State = SafeValue<string>(p["State"]),
                                                   Country = SafeValue<string>(p["Country"]),
                                                   ZipCode = SafeValue<string>(p["ZipCode"]),
                                                   Phone = objCommon.ConvertPhoneNumber(SafeValue<string>(p["Phone"])),
                                                   Fax = SafeValue<string>(p["Fax"]),
                                                   EmailAddress = SafeValue<string>(p["EmailAddress"]),
                                                   ContactType = SafeValue<int>(p["ContactType"]),
                                                   Mobile = SafeValue<string>(p["Mobile"]),
                                               }).ToList();

                }
            }
            catch (Exception)
            {

                throw;
            }

            return lstDoctorAddressDetails;
        }
        #endregion
        public int GetIdOnPublicProfileURL(string url)
        {
            DataTable dt = new DataTable();
            dt = objCommon.GetUserIDFromProfilePath(url);
            if (dt != null && dt.Rows.Count > 0)
            {
                return SafeValue<int>(dt.Rows[0]["UserID"]);

            }
            else
            {
                return -1;
            }
        }
        //public DataTable GetDoctorPersonalDetials(int UserId)
        //{
        //    DataTable dt = new DataTable();
        //    dt = 
        //}
        #region Code For Banner Details
        public List<Banner> GetBannersByUserId(int UserId)
        {
            List<Banner> lstBanner = new List<Banner>();
            try
            {
                #region Start New
                DataTable dt = new DataTable();

                dt = objColleaguesData.GetBannerDetailByUserId(UserId);
                if (dt.Rows.Count > 0)
                {
                    lstBanner = (from p in dt.AsEnumerable()
                                 select new Banner
                                 {
                                     BannerId = SafeValue<int>(p["BannerId"]),
                                     UserId = SafeValue<int>((p["UserId"])),
                                     Title = SafeValue<string>(p["Title"]),
                                     Path = SafeValue<string>(p["Path"]) == string.Empty ? SafeValue<string>(ConfigurationManager.AppSettings["DefaultBannerImage"]) : SafeValue<string>(ConfigurationManager.AppSettings["BannerImage"]) + UserId + "/" + SafeValue<string>(p["Path"]),
                                     ColorCode = SafeValue<string>(p["ColorCode"])
                                 }).ToList();

                }

                #endregion
            }
            catch (Exception)
            {

                throw;
            }
            return lstBanner;
        }
        #endregion
        #region Code For GetGallaryDetails 
        public List<GallaryDetail> GetGallaryDetailsOfDoctor(DataTable dt)
        {
            List<GallaryDetail> lstGallary = new List<GallaryDetail>();
            try
            {
                if (dt.Rows.Count > 0)
                {
                    lstGallary = (from p in dt.AsEnumerable()
                                  select new GallaryDetail
                                  {
                                      MediaType = SafeValue<string>(p["MediaType"]),
                                      MediaURL = SafeValue<string>(p["MediaURL"]),
                                      GallaryId = SafeValue<int>(SafeValue<string>(p["GallaryId"])),
                                      UserId = SafeValue<int>(SafeValue<string>(p["UserId"])),
                                  }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return lstGallary;

        }
        public List<SpecialOffers> GetSpecialOfferDetails(DataTable dt)
        {
            List<SpecialOffers> lstOffer = new List<SpecialOffers>();
            try
            {
                if (dt.Rows.Count > 0)
                {
                    lstOffer = (from p in dt.AsEnumerable()
                                select new SpecialOffers
                                {

                                    OfferId = SafeValue<int>(p["OfferId"]),
                                    Title = SafeValue<string>(p["Title"]),
                                    Description = SafeValue<string>(p["Description"]),
                                    Code = SafeValue<string>(p["Code"]),
                                    SortOrder = SafeValue<int>(p["SortOrder"]),
                                    Status = SafeValue<bool>(p["Status"]),
                                    OfferStatus = (SafeValue<string>(p["Status"]) == "True" ? "Active" : "Inactive")
                                }).ToList();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return lstOffer;

        }
        #endregion
        #region Code For Reviews Details
        public List<Ratings> GetReviewsByUserId(int UserId)
        {
            List<Ratings> lstreviews = new List<Ratings>();
            try
            {
                #region Start New
                DataTable dt = new DataTable();
                DentistRatingDAL objDAL = new DentistRatingDAL();
                dt = objDAL.getDentistRatings(UserId, 0, (int)Common.RatingStatus.APPROVED, 1, (int)Common.PublicReviewPageSize.PageSize);
                if (dt.Rows.Count > 0 && dt != null)
                {
                    lstreviews = (from p in dt.AsEnumerable()
                                  select new Ratings
                                  {
                                      RatingId = SafeValue<int>(p["RatingId"]),
                                      RatingValue = SafeValue<double>(p["RatingValue"]),
                                      SenderId = SafeValue<int>(string.IsNullOrWhiteSpace(SafeValue<string>(p["SenderId"])) ? "0" : SafeValue<string>(p["SenderId"])),
                                      SenderName = SafeValue<string>(p["SenderName"]),
                                      ReceiverId = SafeValue<int>(p["ReceiverId"]),
                                      CreatedDate = SafeValue<DateTime>(p["CreatedDate"]),
                                      RatingMessage = SafeValue<string>(p["RatingMessage"])
                                  }).ToList();
                }

                #endregion
            }
            catch (Exception)
            {

                throw;
            }
            return lstreviews;
        }
        #endregion
        #region Insert Patient From Public Profile Side
        public int InsertPatient(MDLPatientSignUp mdlPatient)
        {
            try
            {
                int NewPatientId = 0;
                ObjPatientData = new clsPatientsData();
                mdlPatient.Password = objCommon.CreateRandomPassword(8);
                return NewPatientId = ObjPatientData.InsertPatientFromPublicProfile(mdlPatient);
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("BusinessLogicLayer--InsertPatient", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public int InsertNewPatient(MDLPatientSignUpDetail mdlPatient)
        {
            try
            {
                int NewPatientId = 0;
                string Password = objCommon.CreateRandomPassword(8);
                ObjPatientData = new clsPatientsData();
                MDLPatientSignUp obj = new MDLPatientSignUp();
                obj.FirstName = mdlPatient.FirstName;
                obj.LastName = mdlPatient.LastName;
                obj.Phone = mdlPatient.Phone;
                obj.Email = mdlPatient.Email;
                obj.PublicProfileUserId = mdlPatient.PublicProfileUserId;
                obj.OfferID = mdlPatient.OfferID;
                obj.Password = Password;
                NewPatientId = ObjPatientData.InsertPatientFromPublicProfile(obj);
                ObjTemplate.PatientAddedNotification(mdlPatient.PublicProfileUserId, NewPatientId, mdlPatient.Email, mdlPatient.FirstName, mdlPatient.LastName, mdlPatient.Phone, "", "", "", "", "", "");
                ObjTemplate.PatientSignUpEMail(mdlPatient.Email, Password, mdlPatient.PublicProfileUserId);
                int id = ObjPatientData.Insert_PatientMessages(mdlPatient.FirstName, mdlPatient.PublicProfileUserName, "Patient sign-up", "<a href=\"javascript:;\" onclick=\"getAttachedPatientHistory(" + NewPatientId + ")\">" + mdlPatient.FirstName + " " + mdlPatient.LastName + "</a> recently signed up as patient.", SafeValue<int>(mdlPatient.PublicProfileUserId), NewPatientId, false, false, true);
                return NewPatientId;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("BusinessLogicLayer--InsertPatient", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool InsertPatientAddress(PatientContactInfo ObjPatientContactInfo)
        {
            try
            {
                bool result = ObjPatientData.InsertPatientContactInfoFromPublicProfile(ObjPatientContactInfo);
                return result;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("BusinessLogicLayer--InsertPatientAddress", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        #endregion

        #region Insert Insurance Coverage
        public int InsertInsuranceCoverage(BO.Models.InsuranceCoverage objInsuranceCoverage, int PatientId)
        {
            try
            {
                clsPatientsData ObjPatientsData = new clsPatientsData();
                DataSet dsp = new DataSet();
                DataTable dtPatient = new DataTable();
                dsp = ObjPatientsData.GetPateintDetailsByPatientId(PatientId);
                dtPatient = dsp.Tables[0];
                string Gender = string.Empty;
                string Status = string.Empty;
                string PatientAddress = string.Empty;
                string DateOfBirth = string.Empty;

                string PatientCity = string.Empty;
                string PatientState = string.Empty;
                string PatientZipcode = string.Empty;
                string PatientPrimaryPhone = string.Empty;
                string MobilePhone = string.Empty;
                string PatiantFax = string.Empty;

                string EmergencyContactName = string.Empty;
                string EmergencyContactPhoneNumber = string.Empty;
                string SecondaryPhone = string.Empty;
                if (dtPatient != null && dtPatient.Rows.Count > 0)
                {
                    DataTable dtRegi = new DataTable();
                    dtRegi = ObjPatientsData.GetRegistration(PatientId, "GetRegistrationform");
                    Gender = SafeValue<string>(dtPatient.Rows[0]["Gender"]) == "0" ? "0" : "1";
                    Status = string.Empty;
                    PatientAddress = SafeValue<string>(dtPatient.Rows[0]["ExactAddress"]);
                    DateOfBirth = SafeValue<string>(dtPatient.Rows[0]["DateOfBirth"]);
                    PatientCity = SafeValue<string>(dtPatient.Rows[0]["City"]);
                    PatientState = SafeValue<string>(dtPatient.Rows[0]["State"]);
                    PatientZipcode = SafeValue<string>(dtPatient.Rows[0]["ZipCode"]);
                    PatientPrimaryPhone = SafeValue<string>(dtPatient.Rows[0]["Phone"]);
                    MobilePhone = string.Empty;
                    PatiantFax = string.Empty;
                    if (dtRegi != null && dtRegi.Rows.Count > 0)
                    {
                        EmergencyContactName = SafeValue<string>(dtRegi.Rows[0]["txtemergencyname"]);
                        EmergencyContactPhoneNumber = SafeValue<string>(dtRegi.Rows[0]["txtemergency"]);
                        SecondaryPhone = SafeValue<string>(dtRegi.Rows[0]["txtemergency"]);
                    }
                }
                string Dob = DateOfBirth; string ddlgenderreg = SafeValue<string>(Gender); string drpStatus = string.Empty;
                string ResidenceStreet = PatientAddress; string City = PatientCity; string State = PatientState;
                string Zip = PatientZipcode; string ResidenceTelephone = PatientPrimaryPhone;

                // string emergencyname = EmergencyContactName; string emergency = EmergencyContactPhoneNumber; 
                string WorkPhone = SecondaryPhone;
                EmergencyContactName = objInsuranceCoverage.EMGContactName;
                EmergencyContactPhoneNumber = objInsuranceCoverage.EMGContactNo;

                string Fax = string.Empty;
                string ResponsibleDOB = string.Empty;
                string ResponsibleContact = string.Empty;
                string Whommay = string.Empty;
                string chkInsurance = string.Empty; string chkCash = string.Empty; string chkCrediteCard = string.Empty;
                DataTable tblRegistration = new DataTable("Registration");
                //tblRegistration.Columns.Add("txtResponsiblepartyFname");
                //tblRegistration.Columns.Add("txtResponsiblepartyLname");
                //tblRegistration.Columns.Add("txtResponsibleRelationship");
                //tblRegistration.Columns.Add("txtResponsibleAddress");
                //tblRegistration.Columns.Add("txtResponsibleCity");
                //tblRegistration.Columns.Add("txtResponsibleState");
                //tblRegistration.Columns.Add("txtResponsibleZipCode");
                //tblRegistration.Columns.Add("txtResponsibleDOB");
                //tblRegistration.Columns.Add("txtResponsibleContact");
                tblRegistration.Columns.Add("txtPatientPresentPosition");
                tblRegistration.Columns.Add("txtPatientHowLongHeld");
                tblRegistration.Columns.Add("txtParentPresentPosition");
                tblRegistration.Columns.Add("txtParentHowLongHeld");
                tblRegistration.Columns.Add("txtDriversLicense");
                tblRegistration.Columns.Add("chkMethodOfPayment");
                tblRegistration.Columns.Add("txtPurposeCall");
                tblRegistration.Columns.Add("txtOtherFamily");
                tblRegistration.Columns.Add("txtWhommay");
                tblRegistration.Columns.Add("txtSomeonetonotify");
                tblRegistration.Columns.Add("txtemergency");
                tblRegistration.Columns.Add("txtemergencyname");
                tblRegistration.Columns.Add("txtEmployeeName1");
                tblRegistration.Columns.Add("txtInsurancePhone1");
                tblRegistration.Columns.Add("txtEmployeeDob1");
                tblRegistration.Columns.Add("txtEmployerName1");
                tblRegistration.Columns.Add("txtYearsEmployed1");
                tblRegistration.Columns.Add("txtNameofInsurance1");
                tblRegistration.Columns.Add("txtInsuranceAddress1");
                tblRegistration.Columns.Add("txtInsuranceTelephone1");
                tblRegistration.Columns.Add("txtProgramorpolicy1");
                tblRegistration.Columns.Add("txtSocialSecurity1");
                tblRegistration.Columns.Add("txtUnionLocal1");
                tblRegistration.Columns.Add("txtEmployeeName2");
                tblRegistration.Columns.Add("txtInsurancePhone2");
                tblRegistration.Columns.Add("txtEmployeeDob2");
                tblRegistration.Columns.Add("txtEmployerName2");
                tblRegistration.Columns.Add("txtYearsEmployed2");
                tblRegistration.Columns.Add("txtNameofInsurance2");
                tblRegistration.Columns.Add("txtInsuranceAddress2");
                tblRegistration.Columns.Add("txtInsuranceTelephone2");
                tblRegistration.Columns.Add("txtEmployeeName3");
                tblRegistration.Columns.Add("txtInsurancePhone3");
                tblRegistration.Columns.Add("MedicalNameOfInsured");
                tblRegistration.Columns.Add("MedicalDOB");
                tblRegistration.Columns.Add("MedicalMemberID");
                tblRegistration.Columns.Add("MedicalGroupNumber");
                tblRegistration.Columns.Add("txtProgramorpolicy2");
                tblRegistration.Columns.Add("txtSocialSecurity2");
                tblRegistration.Columns.Add("txtUnionLocal2");
                tblRegistration.Columns.Add("txtDigiSign");
                StringBuilder chkMethodOfPayment = new StringBuilder();
                if (chkCash == "1")
                {
                    if (!string.IsNullOrEmpty(SafeValue<string>(chkMethodOfPayment)))
                    {
                        chkMethodOfPayment.Append("," + "Cash");
                    }
                    else
                    {
                        chkMethodOfPayment.Append("Cash");
                    }
                }
                if (chkInsurance == "1")
                {
                    if (!string.IsNullOrEmpty(SafeValue<string>(chkMethodOfPayment)))
                    {
                        chkMethodOfPayment.Append("," + "Insurance");
                    }
                    else
                    {
                        chkMethodOfPayment.Append("Insurance");
                    }
                }
                if (chkCrediteCard == "1")
                {
                    if (!string.IsNullOrEmpty(SafeValue<string>(chkMethodOfPayment)))
                    {
                        chkMethodOfPayment.Append("," + "Credit Card");
                    }
                    else
                    {
                        chkMethodOfPayment.Append("Credit Card");
                    }
                }

                tblRegistration.Columns.Add("CreatedDate");
                tblRegistration.Columns.Add("ModifiedDate");
                tblRegistration.Rows.Add("", "", "", "", "", SafeValue<string>(chkMethodOfPayment), "", "", Whommay, "", EmergencyContactPhoneNumber, EmergencyContactName, objInsuranceCoverage.PrimaryInsuranceCompany, objInsuranceCoverage.PrimaryInsurancePhone,
           objInsuranceCoverage.PrimaryDateOfBirth, objInsuranceCoverage.PrimaryNameOfInsured, "", objInsuranceCoverage.PrimaryMemberID, "", objInsuranceCoverage.PrimaryGroupNumber, "", "", "", objInsuranceCoverage.SecondaryInsuranceCompany, objInsuranceCoverage.SecondaryInsurancePhone, "",
                  objInsuranceCoverage.SecondaryNameOfInsured, objInsuranceCoverage.SecondaryDateOfBirth, objInsuranceCoverage.SecondaryMemberID, "", objInsuranceCoverage.SecondaryGroupNumber, objInsuranceCoverage.MedicalInsuranceCompany, objInsuranceCoverage.MedicalInsurancePhone, objInsuranceCoverage.MedicalNameOfInsured, objInsuranceCoverage.MedicalDateOfBirth, objInsuranceCoverage.MedicalMemberID, objInsuranceCoverage.MedicalGroupNumber, "", "", "", "");
                if (Dob == null || Dob == "")
                {
                    Dob = "1900-01-01 00:00:00.000";
                }
                DataSet dsRegistration = new DataSet("Registration");
                dsRegistration.Tables.Add(tblRegistration);
                DataSet ds = dsRegistration.Copy();
                dsRegistration.Clear();
                string Registration = ds.GetXml();
                bool status1 = ObjPatientsData.UpdateRegistrationForm(PatientId, "UpdatePatient", SafeValue<string>(Dob), ddlgenderreg,
                    drpStatus, ResidenceStreet, City, State, Zip,
                     ResidenceTelephone, WorkPhone, Fax, Registration);
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("BusinessLogicLayer--InsertInsuranceCoverage", Ex.Message, Ex.StackTrace);
                throw;
            }
            return 1;
        }
        #endregion


        #region Insert Medical History
        public int InsertMediacalHistory(MediacalHisotry objHistory, int PatientId)
        {
            try
            {
                clsPatientsData ObjPatientsData = new clsPatientsData();
                // Dental History Table
                DataTable tblMedicalHistory = new DataTable("MedicalHistory");
                tblMedicalHistory.Columns.Add("MrdQue1");
                tblMedicalHistory.Columns.Add("Mtxtphysicians");
                tblMedicalHistory.Columns.Add("MrdQue2");
                tblMedicalHistory.Columns.Add("Mtxthospitalized");
                tblMedicalHistory.Columns.Add("MrdQue3");
                tblMedicalHistory.Columns.Add("Mtxtserious");
                tblMedicalHistory.Columns.Add("MrdQue4");
                tblMedicalHistory.Columns.Add("Mtxtmedications");
                tblMedicalHistory.Columns.Add("MrdQue5");
                tblMedicalHistory.Columns.Add("MtxtRedux");
                tblMedicalHistory.Columns.Add("MrdQue6");
                tblMedicalHistory.Columns.Add("MtxtFosamax");

                tblMedicalHistory.Columns.Add("MrdQuediet7");
                tblMedicalHistory.Columns.Add("Mtxt7");
                tblMedicalHistory.Columns.Add("Mrdotobacco8");
                tblMedicalHistory.Columns.Add("Mtxt8");
                tblMedicalHistory.Columns.Add("Mrdosubstances");
                tblMedicalHistory.Columns.Add("Mtxt9");
                tblMedicalHistory.Columns.Add("Mrdopregnant");
                tblMedicalHistory.Columns.Add("Mtxt10");
                tblMedicalHistory.Columns.Add("Mrdocontraceptives");
                tblMedicalHistory.Columns.Add("MrdoNursing");
                tblMedicalHistory.Columns.Add("MchkQue_1");
                tblMedicalHistory.Columns.Add("MchkQue_2");
                tblMedicalHistory.Columns.Add("MchkQue_3");
                tblMedicalHistory.Columns.Add("MchkQue_4");
                tblMedicalHistory.Columns.Add("MchkQue_5");
                tblMedicalHistory.Columns.Add("MchkQue_6");
                tblMedicalHistory.Columns.Add("MchkQue_7");
                tblMedicalHistory.Columns.Add("MchkQue_8");
                tblMedicalHistory.Columns.Add("MchkQue_9");
                tblMedicalHistory.Columns.Add("MtxtchkQue_9");


                tblMedicalHistory.Columns.Add("MrdQueAIDS_HIV_Positive");
                tblMedicalHistory.Columns.Add("MrdQueAlzheimer");
                //  tblMedicalHistory.Columns.Add("MrdQueAnaphylaxis");
                tblMedicalHistory.Columns.Add("MrdQueAnemia");
                tblMedicalHistory.Columns.Add("MrdQueAngina");

                //  tblMedicalHistory.Columns.Add("MrdQueArthritis_Gout");
                tblMedicalHistory.Columns.Add("MrdQueArtificialHeartValve");
                tblMedicalHistory.Columns.Add("MrdQueArtificialJoint");
                tblMedicalHistory.Columns.Add("MrdQueAsthma");
                tblMedicalHistory.Columns.Add("MrdQueBloodDisease");
                tblMedicalHistory.Columns.Add("MrdQueBloodTransfusion");
                tblMedicalHistory.Columns.Add("MrdQueBreathing");
                tblMedicalHistory.Columns.Add("MrdQueBruise");
                tblMedicalHistory.Columns.Add("MrdQueCancer");
                tblMedicalHistory.Columns.Add("MrdQueChemotherapy");
                tblMedicalHistory.Columns.Add("MrdQueChest");
                //   tblMedicalHistory.Columns.Add("MrdQueCold_Sores_Fever");
                //tblMedicalHistory.Columns.Add("MrdQueCongenital");
                tblMedicalHistory.Columns.Add("MrdQueConvulsions");
                tblMedicalHistory.Columns.Add("MrdQueCortisone");
                //  tblMedicalHistory.Columns.Add("MrdQueDiabetes");
                //  tblMedicalHistory.Columns.Add("MrdQueDrug");
                //  tblMedicalHistory.Columns.Add("MrdQueEasily");
                tblMedicalHistory.Columns.Add("MrdQueEmphysema");
                tblMedicalHistory.Columns.Add("MrdQueEpilepsy");
                //   tblMedicalHistory.Columns.Add("MrdQueExcessiveBleeding");
                tblMedicalHistory.Columns.Add("MrdQueExcessiveThirst");
                //  tblMedicalHistory.Columns.Add("MrdQueFainting");
                // tblMedicalHistory.Columns.Add("MrdQueFrequentCough");
                // tblMedicalHistory.Columns.Add("MrdQueFrequentDiarrhea");
                //  tblMedicalHistory.Columns.Add("MrdQueFrequentHeadaches");
                //  tblMedicalHistory.Columns.Add("MrdQueGenital");
                tblMedicalHistory.Columns.Add("MrdQueGlaucoma");
                //  tblMedicalHistory.Columns.Add("MrdQueHay");
                //  tblMedicalHistory.Columns.Add("MrdQueHeartAttack_Failure");
                //  tblMedicalHistory.Columns.Add("MrdQueHeartMurmur");
                tblMedicalHistory.Columns.Add("MrdQueHeartPacemaker");
                //  tblMedicalHistory.Columns.Add("MrdQueHeartTrouble_Disease");
                tblMedicalHistory.Columns.Add("MrdQueHemophilia");
                //  tblMedicalHistory.Columns.Add("MrdQueHepatitisA");
                //  tblMedicalHistory.Columns.Add("MrdQueHepatitisBorC");
                tblMedicalHistory.Columns.Add("MrdQueHerpes");
                //   tblMedicalHistory.Columns.Add("MrdQueHighBloodPressure");
                //   tblMedicalHistory.Columns.Add("MrdQueHighCholesterol");
                //   tblMedicalHistory.Columns.Add("MrdQueHives");
                tblMedicalHistory.Columns.Add("MrdQueHypoglycemia");
                //  tblMedicalHistory.Columns.Add("MrdQueIrregular");
                tblMedicalHistory.Columns.Add("MrdQueKidney");
                tblMedicalHistory.Columns.Add("MrdQueLeukemia");
                tblMedicalHistory.Columns.Add("MrdQueLiver");
                //  tblMedicalHistory.Columns.Add("MrdQueLow");
                tblMedicalHistory.Columns.Add("MrdQueLung");
                tblMedicalHistory.Columns.Add("MrdQueMitral");
                tblMedicalHistory.Columns.Add("MrdQueOsteoporosis");
                tblMedicalHistory.Columns.Add("MrdQuePain");
                //   tblMedicalHistory.Columns.Add("MrdQueParathyroid");
                tblMedicalHistory.Columns.Add("MrdQuePsychiatric");
                tblMedicalHistory.Columns.Add("MrdQueRadiation");
                //   tblMedicalHistory.Columns.Add("MrdQueRecent");
                //   tblMedicalHistory.Columns.Add("MrdQueRenal");
                tblMedicalHistory.Columns.Add("MrdQueRheumatic");
                //   tblMedicalHistory.Columns.Add("MrdQueRheumatism");
                tblMedicalHistory.Columns.Add("MrdQueScarlet");
                tblMedicalHistory.Columns.Add("MrdQueShingles");
                //   tblMedicalHistory.Columns.Add("MrdQueSickle");
                tblMedicalHistory.Columns.Add("MrdQueSinus");
                //   tblMedicalHistory.Columns.Add("MrdQueSpina");
                tblMedicalHistory.Columns.Add("MrdQueStomach");
                tblMedicalHistory.Columns.Add("MrdQueStroke");
                tblMedicalHistory.Columns.Add("MrdQueSwelling");
                tblMedicalHistory.Columns.Add("MrdQueThyroid");
                //   tblMedicalHistory.Columns.Add("MrdQueTonsillitis");
                tblMedicalHistory.Columns.Add("MrdQueTuberculosis");
                tblMedicalHistory.Columns.Add("MrdQueTumors");
                //  tblMedicalHistory.Columns.Add("MrdQueUlcers");
                //  tblMedicalHistory.Columns.Add("MrdQueVenereal");
                //  tblMedicalHistory.Columns.Add("MrdQueYellow");

                tblMedicalHistory.Columns.Add("Mtxtillness");
                tblMedicalHistory.Columns.Add("MtxtComments");
                tblMedicalHistory.Columns.Add("CreatedDate");
                tblMedicalHistory.Columns.Add("ModifiedDate");

                //Changes on 06/28/2017 
                tblMedicalHistory.Columns.Add("MrdQueAbnormal");
                tblMedicalHistory.Columns.Add("MrdQueThinners");
                tblMedicalHistory.Columns.Add("MrdQueFibrillation");
                tblMedicalHistory.Columns.Add("MrdQueCigarette");
                tblMedicalHistory.Columns.Add("MrdQueCirulation");
                tblMedicalHistory.Columns.Add("MrdQuePersistent");
                tblMedicalHistory.Columns.Add("MrdQuefillers");
                tblMedicalHistory.Columns.Add("MrdQueDiabletes");
                tblMedicalHistory.Columns.Add("MrdQueDiarrhhea");
                tblMedicalHistory.Columns.Add("MrdQueDigestive");
                tblMedicalHistory.Columns.Add("MrdQueDizziness");
                tblMedicalHistory.Columns.Add("MrdQueAlcoholic");
                tblMedicalHistory.Columns.Add("MrdQueSubstances");
                tblMedicalHistory.Columns.Add("MrdQueHeadaches");
                tblMedicalHistory.Columns.Add("MrdQueAttack");
                tblMedicalHistory.Columns.Add("MrdQueHeart_defect");
                tblMedicalHistory.Columns.Add("MrdQueHeart_murmur");
                tblMedicalHistory.Columns.Add("MrdQueHiatal");
                tblMedicalHistory.Columns.Add("MrdQueBlood_pressure");
                tblMedicalHistory.Columns.Add("MrdQueAIDS");
                tblMedicalHistory.Columns.Add("MrdQueHow_much");
                tblMedicalHistory.Columns.Add("MrdQueHow_often");
                tblMedicalHistory.Columns.Add("MrdQueReplacement");
                tblMedicalHistory.Columns.Add("MrdQueNeck_BackProblem");
                tblMedicalHistory.Columns.Add("MrdQuePacemaker");
                tblMedicalHistory.Columns.Add("MrdQuePainJaw_Joints");
                tblMedicalHistory.Columns.Add("MrdQueEndocarditis");
                tblMedicalHistory.Columns.Add("MrdQueSjorgren");
                tblMedicalHistory.Columns.Add("MrdQueSwollen");
                tblMedicalHistory.Columns.Add("MrdQueValve");
                tblMedicalHistory.Columns.Add("MrdQueVision");

                tblMedicalHistory.Rows.Add(objHistory.MrdQue1, objHistory.Mtxtphysicians, objHistory.MrdQue2, objHistory.Mtxthospitalized, objHistory.MrdQue3, objHistory.Mtxtserious, objHistory.MrdQue4, objHistory.Mtxtmedications, objHistory.MrdQue5, objHistory.MtxtRedux, objHistory.MrdQue6,
                objHistory.MtxtFosamax, objHistory.MrdQuediet7, objHistory.Mtxt7, objHistory.Mrdotobacco8, objHistory.Mtxt8, objHistory.Mrdosubstances, objHistory.Mtxt9, objHistory.Mrdopregnant, objHistory.Mtxt10, objHistory.Mrdocontraceptives, objHistory.MrdoNursing, objHistory.MchkQue_1, objHistory.MchkQue_2, objHistory.MchkQue_3, objHistory.MchkQue_4, objHistory.MchkQue_5,
                objHistory.MchkQue_6, objHistory.MchkQue_7, objHistory.MchkQue_8, objHistory.MchkQue_9, objHistory.MtxtchkQue_9, objHistory.MrdQueAIDS_HIV_Positive, objHistory.MrdQueAlzheimer, objHistory.MrdQueAnemia, objHistory.MrdQueAngina, objHistory.MrdQueArtificialHeartValve, objHistory.MrdQueArtificialJoint, objHistory.MrdQueAsthma, objHistory.MrdQueBloodDisease,
                objHistory.MrdQueBloodTransfusion, objHistory.MrdQueBreathing, objHistory.MrdQueBruise, objHistory.MrdQueCancer, objHistory.MrdQueChemotherapy, objHistory.MrdQueChest, objHistory.MrdQueConvulsions, objHistory.MrdQueCortisone, objHistory.MrdQueEmphysema, objHistory.MrdQueEpilepsy, objHistory.MrdQueExcessiveThirst, objHistory.MrdQueGlaucoma, objHistory.MrdQueHeartPacemaker, objHistory.MrdQueHemophilia, objHistory.MrdQueHerpes, objHistory.MrdQueHypoglycemia,
                objHistory.MrdQueKidney, objHistory.MrdQueLeukemia, objHistory.MrdQueLiver, objHistory.MrdQueLung, objHistory.MrdQueMitral, objHistory.MrdQueOsteoporosis, objHistory.MrdQuePain, objHistory.MrdQuePsychiatric, objHistory.MrdQueAbnormal, objHistory.MrdQueThinners, objHistory.MrdQueFibrillation, objHistory.MrdQueCigarette, objHistory.MrdQueCirulation, objHistory.MrdQuePersistent, objHistory.MrdQuefillers, objHistory.MrdQueDiabletes, objHistory.MrdQueDiarrhhea, objHistory.MrdQueDigestive, objHistory.MrdQueDizziness,
                objHistory.MrdQueAlcoholic, objHistory.MrdQueSubstances, objHistory.MrdQueHeadaches, objHistory.MrdQueAttack, objHistory.MrdQueHeart_defect, objHistory.MrdQueHeart_murmur, objHistory.MrdQueHiatal, objHistory.MrdQueBlood_pressure, objHistory.MrdQueAIDS, objHistory.MrdQueHow_much, objHistory.MrdQueHow_often, objHistory.MrdQueReplacement, objHistory.MrdQueNeck_BackProblem, objHistory.MrdQuePacemaker, objHistory.MrdQuePainJaw_Joints, objHistory.MrdQueEndocarditis, objHistory.MrdQueSjorgren, objHistory.MrdQueSwollen, objHistory.MrdQueValve, objHistory.MrdQueVision,
                objHistory.MrdQueRadiation, objHistory.MrdQueRheumatic, objHistory.MrdQueScarlet, objHistory.MrdQueShingles, objHistory.MrdQueSinus, objHistory.MrdQueStomach, objHistory.MrdQueStroke, objHistory.MrdQueSwelling,
                objHistory.MrdQueThyroid, objHistory.MrdQueTuberculosis, objHistory.MrdQueTumors, objHistory.Mtxtillness, objHistory.MtxtComments);
                DataSet dsMedicalHistory = new DataSet("MedicalHistory");
                dsMedicalHistory.Tables.Add(tblMedicalHistory);

                DataSet ds = dsMedicalHistory.Copy();
                dsMedicalHistory.Clear();

                string MedicalHistory = ds.GetXml();

                bool status = ObjPatientsData.UpdateMedicalHistory(PatientId, "UpdateMedicalHistory", MedicalHistory);
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("BusinessLogicLayer--InsertMedicalHistory", Ex.Message, Ex.StackTrace);
                throw;
            }
            return 1;
        }
        #endregion

        #region Insert Dental History
        public int InsertDentalHistory(DentalHistory objHistory, int PatientId)
        {
            try
            {
                clsPatientsData ObjPatientsData = new clsPatientsData();
                DataTable tblDentalHistory = new DataTable("DentalHistory");
                tblDentalHistory.Columns.Add("txtQue1");
                tblDentalHistory.Columns.Add("txtQue2");
                tblDentalHistory.Columns.Add("txtQue3");
                tblDentalHistory.Columns.Add("txtQue4");
                tblDentalHistory.Columns.Add("txtQue5");
                tblDentalHistory.Columns.Add("txtQue5a");
                tblDentalHistory.Columns.Add("txtQue5b");
                tblDentalHistory.Columns.Add("txtQue5c");
                tblDentalHistory.Columns.Add("txtQue6");
                tblDentalHistory.Columns.Add("txtQue7");
                tblDentalHistory.Columns.Add("rdQue7a");
                tblDentalHistory.Columns.Add("rdQue8");
                tblDentalHistory.Columns.Add("rdQue9");
                tblDentalHistory.Columns.Add("txtQue9a");
                tblDentalHistory.Columns.Add("rdQue10");
                tblDentalHistory.Columns.Add("rdoQue11aFixedbridge");
                tblDentalHistory.Columns.Add("rdoQue11bRemoveablebridge");
                tblDentalHistory.Columns.Add("rdoQue11cDenture");
                tblDentalHistory.Columns.Add("rdQue11dImplant");
                tblDentalHistory.Columns.Add("txtQue12");
                tblDentalHistory.Columns.Add("rdQue15");
                tblDentalHistory.Columns.Add("rdQue16");
                tblDentalHistory.Columns.Add("rdQue17");
                tblDentalHistory.Columns.Add("rdQue18");
                tblDentalHistory.Columns.Add("rdQue19");
                tblDentalHistory.Columns.Add("chkQue20");
                StringBuilder chkQue20 = new StringBuilder();
                if (objHistory.chkQue20_1 == "Y")
                {
                    if (!string.IsNullOrEmpty(SafeValue<string>(chkQue20)))
                    {
                        chkQue20.Append("," + "1");
                    }
                    else
                    {
                        chkQue20.Append("1");
                    }
                }
                if (objHistory.chkQue20_2 == "Y")
                {
                    if (!string.IsNullOrEmpty(SafeValue<string>(chkQue20)))
                    {
                        chkQue20.Append("," + "2");
                    }
                    else
                    {
                        chkQue20.Append("2");
                    }
                }
                if (objHistory.chkQue20_3 == "Y")
                {
                    if (!string.IsNullOrEmpty(SafeValue<string>(chkQue20)))
                    {
                        chkQue20.Append("," + "3");
                    }
                    else
                    {
                        chkQue20.Append("3");
                    }
                }
                if (objHistory.chkQue20_4 == "Y")
                {
                    if (!String.IsNullOrEmpty(SafeValue<string>(chkQue20)))
                    {
                        chkQue20.Append("," + "4");
                    }
                    else
                    {
                        chkQue20.Append("4");
                    }
                }
                tblDentalHistory.Columns.Add("rdQue21");
                tblDentalHistory.Columns.Add("txtQue21a");
                tblDentalHistory.Columns.Add("txtQue22");
                tblDentalHistory.Columns.Add("txtQue23");
                tblDentalHistory.Columns.Add("rdQue24");
                tblDentalHistory.Columns.Add("txtQue26");
                tblDentalHistory.Columns.Add("rdQue27");
                tblDentalHistory.Columns.Add("rdQue28");
                tblDentalHistory.Columns.Add("txtQue28a");
                tblDentalHistory.Columns.Add("txtQue28b");
                tblDentalHistory.Columns.Add("txtQue28c");
                tblDentalHistory.Columns.Add("txtQue29");
                tblDentalHistory.Columns.Add("txtQue29a");
                tblDentalHistory.Columns.Add("rdQue30");
                tblDentalHistory.Columns.Add("txtComments");
                tblDentalHistory.Columns.Add("txtDigiSign");
                tblDentalHistory.Columns.Add("CreatedDate");
                tblDentalHistory.Columns.Add("ModifiedDate");

                tblDentalHistory.Rows.Add(objHistory.txtQue1, objHistory.txtQue2, objHistory.txtQue3, objHistory.txtQue4, objHistory.txtQue5, objHistory.txtQue5a, objHistory.txtQue5b, objHistory.txtQue5c,
                    objHistory.txtQue6, objHistory.txtQue7, objHistory.rdQue7a, objHistory.rdQue8, objHistory.rdQue9, objHistory.txtQue9a, objHistory.rdQue10,
                    objHistory.rdoQue11aFixedbridge, objHistory.rdoQue11bRemoveablebridge, objHistory.rdoQue11cDenture,
                    objHistory.rdQue11dImplant, objHistory.txtQue12, objHistory.rdQue15, objHistory.rdQue16, objHistory.rdQue17, objHistory.rdQue18,
                    objHistory.rdQue19, SafeValue<string>(chkQue20), objHistory.rdQue21, objHistory.txtQue21a, objHistory.txtQue22,
                    objHistory.txtQue23, objHistory.rdQue24, objHistory.txtQue26, objHistory.rdQue27, objHistory.rdQue28,
                    objHistory.txtQue28a, objHistory.txtQue28b, objHistory.txtQue28c, objHistory.txtQue29, objHistory.txtQue29a, objHistory.rdQue30, objHistory.txtComments, objHistory.txtDigiSign, System.DateTime.Now, System.DateTime.Now);
                //Join Two Table
                DataSet dsDentalHistory = new DataSet("DentalHistory");
                dsDentalHistory.Tables.Add(tblDentalHistory);
                DataSet ds = dsDentalHistory.Copy();
                dsDentalHistory.Clear();
                string DentalHistory = ds.GetXml();
                bool status = ObjPatientsData.UpdateDentalHistory(SafeValue<int>(PatientId), "UpdateDentalHistory", DentalHistory);
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("BusinessLogicLayer--InsertDentalHistory", Ex.Message, Ex.StackTrace);
                throw;
            }
            return 1;
        }
        #endregion

        #region PublicProfileSection
        public PublicProfileSection GetPublicProfilesectionDetails(int Userid)
        {
            PublicProfileSection ObjProfileSection = new PublicProfileSection();
            try
            {
                DataTable dt = new DataTable();
                dt = objColleaguesData.GetPublicProfileSectionDetials(Userid);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        ObjProfileSection.SectionId = SafeValue<int>(item["SectionID"]);
                        ObjProfileSection.UserId = SafeValue<int>(item["UserId"]);
                        ObjProfileSection.PatientForms = SafeValue<bool>(item["PatientForms"]);
                        ObjProfileSection.SpecialOffers = SafeValue<bool>(item["SpecialOffers"]);
                        ObjProfileSection.ReferPatient = SafeValue<bool>(item["ReferPatient"]);
                        ObjProfileSection.Reviews = SafeValue<bool>(item["Reviews"]);
                        ObjProfileSection.AppointmentBooking = SafeValue<bool>(item["AppointmentBooking"]);
                        ObjProfileSection.PatientLogin = SafeValue<bool>(item["PatientLogin"]);
                    }
                }
                return ObjProfileSection;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("DentistProfileBLL--GetPublicProfilesectionDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool InsertUpdatePublicProfileSectionDetials(PublicProfileSection obj)
        {
            try
            {
                return objColleaguesData.InsertUpdatePublicProfileDetails(obj);
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("DentistProfileBLL--InsertUpdatePublicProfileSectionDetials", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        #endregion
        #region
        public VCardForDoctor GetVCFfileforRLAPI(int UserId)
        {
            try
            {
                VCardBLL ObjVcardBLL = new VCardBLL();
                VCardForDoctor ObjVcard = new VCardForDoctor();
                ObjVcard = ObjVcardBLL.GetVCardDetialsOfDoctor(UserId);
                return ObjVcard;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("DentistProfileBLL--GetVCFfileforRLAPI", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        #endregion

        #region LD-Json Binding
        public LDJSON BindLdJsonObject(int RatingCount, List<Ratings> lst, DentistProfileDetail objProfileDetails)
        {
            try
            {
                LDJSON obj = new LDJSON();
                List<Review> lstreview = new List<Review>();
                AgregateRating ObjAgg = new AgregateRating();
                ObjAgg.ReviewCount = RatingCount;
                ObjAgg.type = "AggregateRating";
                ObjAgg.RatingValue = objProfileDetails.RatingValue;
                obj.aggregateRating = ObjAgg;
                foreach (var item in lst)
                {
                    reviewRating reviewRating = new reviewRating();
                    reviewRating.ratingValue = item.RatingValue;
                    lstreview.Add(new Review()
                    {
                        reviewRating = reviewRating,
                        author = item.SenderName,
                        description = item.RatingMessage
                    });
                }
                obj.review = lstreview;
                obj.PriceRange = null;
                obj.Name = objProfileDetails.FirstName + " " + objProfileDetails.LastName + " " + "Review";
                address add = new address();
                add.addressLocality = objProfileDetails.City;
                add.addressRegion = objProfileDetails.State;
                add.streetAddress = objProfileDetails.Address;
                add.postalCode = objProfileDetails.ZipCode;
                add.addressCountry = objProfileDetails.Country;
                obj.address = add;
                obj.context = "http://schema.org/";
                obj.type = "LocalBusiness";
                string Imagelink = objProfileDetails.ImageName.Contains("../../") ? objProfileDetails.ImageName.Replace("../../", "") : objProfileDetails.ImageName;
                obj.image = SafeValue<string>(HttpContext.Current.Session["CompanyWebsite"]) + "/" + Imagelink;
                obj.telephone = objProfileDetails.Phone;
                return obj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        // Add method for Insert and Update Petient Details from One Click referral
        public static int AddNewPatient(Compose1Click mdlPatient, int ReferBy)
        {
            try
            {
                int NewPatientId = 0;
                string Password = "";
                if (mdlPatient._patientInfo.PatientId <= 0)
                {
                    Password = new clsCommon().CreateRandomPassword(8);
                }
                mdlPatient._patientInfo.Password = Password;
                string SecondaryPhnNo = string.Empty;
                if (mdlPatient._contactInfo != null)
                {
                    SecondaryPhnNo = mdlPatient._contactInfo.SecondaryPhoneNo;
                }
                // SBI:38
                if (string.IsNullOrEmpty(mdlPatient._patientInfo.Email))
                {
                    mdlPatient._patientInfo.Email = "email" + DateTime.Now.Ticks + "@domain.com";
                }
                // SBI:38
                //XQ1-167 : Last name sent as null while removing it as mandaotry.
                mdlPatient._patientInfo.LastName = string.IsNullOrEmpty(mdlPatient._patientInfo.LastName) ? "" : mdlPatient._patientInfo.LastName;
                int ValLocation = mdlPatient.LocationId;
                NewPatientId = new clsPatientsData().AddPatientFromOneClickRefferal(mdlPatient._patientInfo, ReferBy, SecondaryPhnNo, ValLocation);
                return NewPatientId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("BusinessLogicLayer--InsertPatient", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool AddPatientContactInfoFromOneclick(PatientContactInfo ObjPatientContactInfo, string Phone, string Email)
        {
            try
            {
                bool result = new clsPatientsData().AddPatientContactInfoFromOneclick(ObjPatientContactInfo, Phone, Email);
                return result;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("BusinessLogicLayer--AddPatientContactInfoFromOneclick", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        //public static bool AddMedicalHistory1Clickreferral(InsuranceCoverage Obj)
        //{
        //    try
        //    {

        //    }
        //    catch (Exception Ex)
        //    {
        //        new clsCommon().InsertErrorLog("BusinessLogicLayer--AddInsuranceCoverage1Clickreferral", Ex.Message, Ex.StackTrace);
        //        throw;
        //    }
        //}
        public int AddInsuranceCoverage(PatientDentist_Referral objInsuranceCoverage, int PatientId)
        {
            try
            {
                clsPatientsData ObjPatientsData = new clsPatientsData();
                DataSet dsp = new DataSet();
                DataTable dtPatient = new DataTable();
                dsp = ObjPatientsData.GetPateintDetailsByPatientId(PatientId);
                dtPatient = dsp.Tables[0];
                string Gender = string.Empty;
                string Status = string.Empty;
                string PatientAddress = string.Empty;
                string DateOfBirth = string.Empty;

                string PatientCity = string.Empty;
                string PatientState = string.Empty;
                string PatientZipcode = string.Empty;
                string PatientPrimaryPhone = string.Empty;
                string MobilePhone = string.Empty;
                string PatiantFax = string.Empty;

                string EmergencyContactName = string.Empty;
                string EmergencyContactPhoneNumber = string.Empty;
                string SecondaryPhone = string.Empty;
                if (dtPatient != null && dtPatient.Rows.Count > 0)
                {
                    DataTable dtRegi = new DataTable();
                    dtRegi = ObjPatientsData.GetRegistration(PatientId, "GetRegistrationform");
                    Gender = SafeValue<string>(dtPatient.Rows[0]["Gender"]) == "0" ? "0" : "1";
                    Status = string.Empty;
                    PatientAddress = SafeValue<string>(dtPatient.Rows[0]["ExactAddress"]);
                    DateOfBirth = SafeValue<string>(dtPatient.Rows[0]["DateOfBirth"]);
                    PatientCity = SafeValue<string>(dtPatient.Rows[0]["City"]);
                    PatientState = SafeValue<string>(dtPatient.Rows[0]["State"]);
                    PatientZipcode = SafeValue<string>(dtPatient.Rows[0]["ZipCode"]);
                    PatientPrimaryPhone = SafeValue<string>(dtPatient.Rows[0]["Phone"]);
                    MobilePhone = string.Empty;
                    PatiantFax = string.Empty;
                    if (dtRegi != null && dtRegi.Rows.Count > 0)
                    {
                        EmergencyContactName = SafeValue<string>(dtRegi.Rows[0]["txtemergencyname"]);
                        EmergencyContactPhoneNumber = SafeValue<string>(dtRegi.Rows[0]["txtemergency"]);
                        SecondaryPhone = SafeValue<string>(dtRegi.Rows[0]["txtemergency"]);
                    }
                }
                string Dob = DateOfBirth; string ddlgenderreg = SafeValue<string>(Gender); string drpStatus = string.Empty;
                string ResidenceStreet = PatientAddress; string City = PatientCity; string State = PatientState;
                string Zip = PatientZipcode; string ResidenceTelephone = PatientPrimaryPhone;
                string emergencyname = EmergencyContactName; string emergency = EmergencyContactPhoneNumber; string WorkPhone = SecondaryPhone;
                string Fax = string.Empty;
                string ResponsibleDOB = string.Empty;
                string ResponsibleContact = string.Empty;
                string Whommay = string.Empty;
                string chkInsurance = string.Empty; string chkCash = string.Empty; string chkCrediteCard = string.Empty;
                DataTable tblRegistration = new DataTable("Registration");
                tblRegistration.Columns.Add("txtEmployeeName1");
                tblRegistration.Columns.Add("txtInsurancePhone1");
                tblRegistration.Columns.Add("txtEmployerName1");
                tblRegistration.Columns.Add("txtEmployeeDob1");
                tblRegistration.Columns.Add("txtNameofInsurance1");
                tblRegistration.Columns.Add("txtInsuranceTelephone1");
                tblRegistration.Columns.Add("txtEmployeeName2");
                tblRegistration.Columns.Add("txtInsurancePhone2");
                tblRegistration.Columns.Add("txtEmployerName2");
                tblRegistration.Columns.Add("txtYearsEmployed2");
                tblRegistration.Columns.Add("txtNameofInsurance2");
                tblRegistration.Columns.Add("txtInsuranceTelephone2");
                StringBuilder chkMethodOfPayment = new StringBuilder();
                if (chkCash == "1")
                {
                    if (!string.IsNullOrEmpty(SafeValue<string>(chkMethodOfPayment)))
                    {
                        chkMethodOfPayment.Append("," + "Cash");
                    }
                    else
                    {
                        chkMethodOfPayment.Append("Cash");
                    }
                }
                if (chkInsurance == "1")
                {
                    if (!string.IsNullOrEmpty(SafeValue<string>(chkMethodOfPayment)))
                    {
                        chkMethodOfPayment.Append("," + "Insurance");
                    }
                    else
                    {
                        chkMethodOfPayment.Append("Insurance");
                    }
                }
                if (chkCrediteCard == "1")
                {
                    if (!string.IsNullOrEmpty(SafeValue<string>(chkMethodOfPayment)))
                    {
                        chkMethodOfPayment.Append("," + "Credit Card");
                    }
                    else
                    {
                        chkMethodOfPayment.Append("Credit Card");
                    }
                }

                tblRegistration.Rows.Add(objInsuranceCoverage.txtEmployeeName1, objInsuranceCoverage.txtInsurancePhone1
                                        , objInsuranceCoverage.txtEmployerName1, objInsuranceCoverage.txtEmployeeDob1,
                                        objInsuranceCoverage.txtNameofInsurance1, objInsuranceCoverage.txtInsuranceTelephone1
                                       , objInsuranceCoverage.txtEmployeeName2, objInsuranceCoverage.txtInsurancePhone2,
                                        objInsuranceCoverage.txtEmployerName2, objInsuranceCoverage.txtYearsEmployed2
                                       , objInsuranceCoverage.txtNameofInsurance2, objInsuranceCoverage.txtInsuranceTelephone2);
                if (Dob == null || Dob == "")
                {
                    Dob = "1900-01-01 00:00:00.000";
                }
                DataSet dsRegistration = new DataSet("Registration");
                dsRegistration.Tables.Add(tblRegistration);
                DataSet ds = dsRegistration.Copy();
                dsRegistration.Clear();
                string Registration = ds.GetXml();
                bool status1 = ObjPatientsData.UpdateRegistrationForm(PatientId, "UpdatePatient", SafeValue<string>(Dob), ddlgenderreg,
                    drpStatus, ResidenceStreet, City, State, Zip,
                     ResidenceTelephone, WorkPhone, Fax, Registration);
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("BusinessLogicLayer--AddInsuranceCoverage", Ex.Message, Ex.StackTrace);
                throw;
            }
            return 1;
        }
        public int AddMediacalHistory(PatientDentist_Referral objHistory, int PatientId)
        {
            try
            {
                clsPatientsData ObjPatientsData = new clsPatientsData();
                // Dental History Table
                DataTable tblMedicalHistory = new DataTable("MedicalHistory");
                tblMedicalHistory.Columns.Add("Mtxtphysicians");
                tblMedicalHistory.Columns.Add("Mtxthospitalized");
                tblMedicalHistory.Columns.Add("Mtxtserious");
                tblMedicalHistory.Columns.Add("Mtxtmedications");
                tblMedicalHistory.Columns.Add("MtxtRedux");
                tblMedicalHistory.Columns.Add("MtxtFosamax");

                tblMedicalHistory.Columns.Add("Mtxt7");
                tblMedicalHistory.Columns.Add("Mtxt8");
                tblMedicalHistory.Columns.Add("Mtxt9");
                tblMedicalHistory.Columns.Add("Mtxt10");
                tblMedicalHistory.Columns.Add("Mtxt11");
                tblMedicalHistory.Columns.Add("Mtxt12");
                tblMedicalHistory.Columns.Add("Mtxt13");
                tblMedicalHistory.Columns.Add("QueFollowing14");
                tblMedicalHistory.Columns.Add("MchkQue_1");
                tblMedicalHistory.Columns.Add("MchkQue_2");
                tblMedicalHistory.Columns.Add("MchkQue_3");
                tblMedicalHistory.Columns.Add("MchkQue_4");
                tblMedicalHistory.Columns.Add("MchkQue_5");
                tblMedicalHistory.Columns.Add("MchkQue_6");
                tblMedicalHistory.Columns.Add("MchkQue_7");
                tblMedicalHistory.Columns.Add("MchkQue_8");
                tblMedicalHistory.Columns.Add("QueFollowing15");
                tblMedicalHistory.Columns.Add("MrdQueAbnormal");
                tblMedicalHistory.Columns.Add("MrdQueAIDS_HIV_Positive");
                tblMedicalHistory.Columns.Add("MrdQueAlzheimer");
                tblMedicalHistory.Columns.Add("MrdQueAnaphylaxis");
                tblMedicalHistory.Columns.Add("MrdQueAnemia");
                tblMedicalHistory.Columns.Add("MrdQueAngina");

                tblMedicalHistory.Columns.Add("MrdQueThinners");
                tblMedicalHistory.Columns.Add("MrdQueArthritis_Gout");
                tblMedicalHistory.Columns.Add("MrdQueArtificialHeartValve");
                tblMedicalHistory.Columns.Add("MrdQueArtificialJoint");
                tblMedicalHistory.Columns.Add("MrdQueAsthma");
                tblMedicalHistory.Columns.Add("MrdQueFibrillation");
                tblMedicalHistory.Columns.Add("MrdQueBloodDisease");
                tblMedicalHistory.Columns.Add("MrdQueBloodTransfusion");
                tblMedicalHistory.Columns.Add("MrdQueBreathing");
                tblMedicalHistory.Columns.Add("MrdQueBruise");
                tblMedicalHistory.Columns.Add("MrdQueCancer");
                tblMedicalHistory.Columns.Add("MrdQueChemicalDependancy");
                tblMedicalHistory.Columns.Add("MrdQueChemotherapy");
                tblMedicalHistory.Columns.Add("MrdQueChest");
                tblMedicalHistory.Columns.Add("MrdQueCigarette");
                tblMedicalHistory.Columns.Add("MrdQueCirulation");
                tblMedicalHistory.Columns.Add("MrdQueCortisone");
                tblMedicalHistory.Columns.Add("MrdQueFrequentCough");
                tblMedicalHistory.Columns.Add("MrdQueConvulsions");
                tblMedicalHistory.Columns.Add("MrdQuefillers");
                tblMedicalHistory.Columns.Add("MrdQueDiabetes");
                tblMedicalHistory.Columns.Add("MrdQueDiarrhhea");
                tblMedicalHistory.Columns.Add("MrdQueDigestive");
                tblMedicalHistory.Columns.Add("MrdQueDizziness");
                tblMedicalHistory.Columns.Add("MrdQueAlcoholic");
                tblMedicalHistory.Columns.Add("MrdQueSubstances");
                tblMedicalHistory.Columns.Add("MrdQueEmphysema");
                tblMedicalHistory.Columns.Add("MrdQueEpilepsy");
                tblMedicalHistory.Columns.Add("MrdQueExcessiveUrination");
                tblMedicalHistory.Columns.Add("MrdQueExcessiveThirst");
                tblMedicalHistory.Columns.Add("MrdQueGlaucoma");
                tblMedicalHistory.Columns.Add("MrdQueHeadaches");
                tblMedicalHistory.Columns.Add("MrdQueAttack");
                tblMedicalHistory.Columns.Add("MrdQueHeartPacemaker");
                tblMedicalHistory.Columns.Add("MrdQueHemophilia");
                tblMedicalHistory.Columns.Add("MrdQueHeart_defect");
                tblMedicalHistory.Columns.Add("MrdQueHeart_murmur");
                tblMedicalHistory.Columns.Add("MrdQueHepatitisA");
                tblMedicalHistory.Columns.Add("MrdQueHerpes");
                tblMedicalHistory.Columns.Add("MrdQueHiatal");
                tblMedicalHistory.Columns.Add("MrdQueBlood_pressure");
                tblMedicalHistory.Columns.Add("MrdQueAIDS");
                tblMedicalHistory.Columns.Add("MrdQueHow_much");
                tblMedicalHistory.Columns.Add("MrdQueHow_often");
                tblMedicalHistory.Columns.Add("MrdQueHypoglycemia");
                tblMedicalHistory.Columns.Add("MrdQueReplacement");
                tblMedicalHistory.Columns.Add("MrdQueKidney");
                tblMedicalHistory.Columns.Add("MrdQueLeukemia");
                tblMedicalHistory.Columns.Add("MrdQueLiver");
                tblMedicalHistory.Columns.Add("MrdQueLung");
                tblMedicalHistory.Columns.Add("MrdQueMitral");
                tblMedicalHistory.Columns.Add("MrdQueNeck_BackProblem");
                tblMedicalHistory.Columns.Add("MrdQueOsteoporosis");
                tblMedicalHistory.Columns.Add("MrdQuePacemaker");
                tblMedicalHistory.Columns.Add("MrdQuePainJaw_Joints");
                tblMedicalHistory.Columns.Add("MrdQueEndocarditis");
                tblMedicalHistory.Columns.Add("MrdQuePsychiatric");
                tblMedicalHistory.Columns.Add("MrdQueRadiation");
                tblMedicalHistory.Columns.Add("MrdQueRheumatic");
                tblMedicalHistory.Columns.Add("MrdQueScarlet");
                tblMedicalHistory.Columns.Add("MrdQueShingles");
                tblMedicalHistory.Columns.Add("MrdQueShortness");
                tblMedicalHistory.Columns.Add("MrdQueSinus");
                tblMedicalHistory.Columns.Add("MrdQueStroke");
                tblMedicalHistory.Columns.Add("MrdQueSjorgren");
                tblMedicalHistory.Columns.Add("MrdQueStomach");
                tblMedicalHistory.Columns.Add("MrdQueSwelling");
                tblMedicalHistory.Columns.Add("MrdQueSwollen");
                tblMedicalHistory.Columns.Add("MrdQueThyroid");
                tblMedicalHistory.Columns.Add("MrdQueTuberculosis");
                tblMedicalHistory.Columns.Add("MrdQueTumors");
                tblMedicalHistory.Columns.Add("MrdQueValve");
                tblMedicalHistory.Columns.Add("MrdQueVision");
                tblMedicalHistory.Columns.Add("MrdQueUnexplained");
                tblMedicalHistory.Columns.Add("Mtxtillness");
                tblMedicalHistory.Columns.Add("MtxtComments");

                tblMedicalHistory.Rows.Add(objHistory.Mtxtphysicians, objHistory.Mtxthospitalized, objHistory.Mtxtserious, objHistory.Mtxtmedications, objHistory.MtxtRedux,
                                            objHistory.MtxtFosamax, objHistory.Mtxt7, objHistory.Mtxt8, objHistory.Mtxt9, objHistory.Mtxt10, objHistory.Mtxt11, objHistory.Mtxt12, objHistory.Mtxt13
                                             , objHistory.QueFollowing14, objHistory.MchkQue_1, objHistory.MchkQue_2, objHistory.MchkQue_3, objHistory.MchkQue_4, objHistory.MchkQue_5,
                                            objHistory.MchkQue_6, objHistory.MchkQue_7, objHistory.MchkQue_8
                                           , objHistory.QueFollowing15
                                            , objHistory.MrdQueAbnormal, objHistory.MrdQueAIDS_HIV_Positive, objHistory.MrdQueAlzheimer, objHistory.MrdQueAnaphylaxis,
                                            objHistory.MrdQueAnemia, objHistory.MrdQueAngina, objHistory.MrdQueThinners, objHistory.MrdQueArthritis_Gout, objHistory.MrdQueArtificialHeartValve,
                                            objHistory.MrdQueArtificialJoint, objHistory.MrdQueAsthma, objHistory.MrdQueFibrillation, objHistory.MrdQueBloodDisease,
                                            objHistory.MrdQueBloodTransfusion, objHistory.MrdQueBreathing, objHistory.MrdQueBruise, objHistory.MrdQueCancer, objHistory.MrdQueChemicalDependancy,
                                            objHistory.MrdQueChemotherapy, objHistory.MrdQueChest, objHistory.MrdQueCigarette, objHistory.MrdQueCirulation, objHistory.MrdQueCortisone,
                                            objHistory.MrdQueFrequentCough, objHistory.MrdQueConvulsions, objHistory.MrdQuefillers, objHistory.MrdQueDiabetes, objHistory.MrdQueDiarrhhea,
                                            objHistory.MrdQueDigestive, objHistory.MrdQueDizziness, objHistory.MrdQueAlcoholic, objHistory.MrdQueSubstances, objHistory.MrdQueEmphysema,
                                            objHistory.MrdQueEpilepsy, objHistory.MrdQueExcessiveUrination, objHistory.MrdQueExcessiveThirst, objHistory.MrdQueGlaucoma,
                                            objHistory.MrdQueHeadaches, objHistory.MrdQueAttack, objHistory.MrdQueHeartPacemaker, objHistory.MrdQueHemophilia,
                                            objHistory.MrdQueHeart_defect, objHistory.MrdQueHeart_murmur, objHistory.MrdQueHepatitisA, objHistory.MrdQueHerpes, objHistory.MrdQueHiatal,
                                            objHistory.MrdQueBlood_pressure, objHistory.MrdQueAIDS, objHistory.MrdQueHow_much, objHistory.MrdQueHow_often, objHistory.MrdQueHypoglycemia,
                                            objHistory.MrdQueReplacement, objHistory.MrdQueKidney, objHistory.MrdQueLeukemia, objHistory.MrdQueLiver, objHistory.MrdQueLung,
                                            objHistory.MrdQueMitral, objHistory.MrdQueNeck_BackProblem, objHistory.MrdQueOsteoporosis, objHistory.MrdQuePacemaker, objHistory.MrdQuePainJaw_Joints,


                                            objHistory.MrdQueEndocarditis, objHistory.MrdQuePsychiatric, objHistory.MrdQueRadiation,
                                            objHistory.MrdQueRheumatic, objHistory.MrdQueScarlet, objHistory.MrdQueShingles, objHistory.MrdQueShortness,
                                            objHistory.MrdQueSinus, objHistory.MrdQueStroke, objHistory.MrdQueSjorgren, objHistory.MrdQueStomach, objHistory.MrdQueSwelling,
                                            objHistory.MrdQueSwollen, objHistory.MrdQueThyroid, objHistory.MrdQueTuberculosis, objHistory.MrdQueTumors, objHistory.MrdQueValve, objHistory.MrdQueVision,
                                            objHistory.MrdQueUnexplained, objHistory.Mtxtillness, objHistory.MtxtComments);

                DataSet dsMedicalHistory = new DataSet("MedicalHistory");
                dsMedicalHistory.Tables.Add(tblMedicalHistory);

                DataSet ds = dsMedicalHistory.Copy();
                dsMedicalHistory.Clear();

                string MedicalHistory = ds.GetXml();

                bool status = ObjPatientsData.UpdateMedicalHistory(PatientId, "UpdateMedicalHistory", MedicalHistory);
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("BusinessLogicLayer--AddMediacalHistory", Ex.Message, Ex.StackTrace);
                throw;
            }
            return 1;
        }
        public int AddDentalHistory(PatientDentist_Referral objHistory, int PatientId)
        {
            try
            {
                clsPatientsData ObjPatientsData = new clsPatientsData();
                DataTable tblDentalHistory = new DataTable("DentalHistory");
                tblDentalHistory.Columns.Add("txtQue5");
                tblDentalHistory.Columns.Add("txtQue6");
                tblDentalHistory.Columns.Add("MrdQue7");
                tblDentalHistory.Columns.Add("txtQue12");
                tblDentalHistory.Columns.Add("DhQue5");
                tblDentalHistory.Columns.Add("MchkFixed");
                tblDentalHistory.Columns.Add("MchkRemoveable");
                tblDentalHistory.Columns.Add("MchkDenture");
                tblDentalHistory.Columns.Add("MchkImplant");
                tblDentalHistory.Columns.Add("rdQue15");
                tblDentalHistory.Columns.Add("rdQue16");
                tblDentalHistory.Columns.Add("rdQue17");
                tblDentalHistory.Columns.Add("rdQue18");
                tblDentalHistory.Columns.Add("rdQue19");
                tblDentalHistory.Columns.Add("rdQue21");
                tblDentalHistory.Columns.Add("MchkHot");
                tblDentalHistory.Columns.Add("MchkCold");
                tblDentalHistory.Columns.Add("MchkSweets");
                tblDentalHistory.Columns.Add("MchkPressure");
                tblDentalHistory.Columns.Add("txtQue22");
                tblDentalHistory.Columns.Add("txtQue23");
                tblDentalHistory.Columns.Add("txtQue24");
                tblDentalHistory.Columns.Add("rdQue24");
                tblDentalHistory.Columns.Add("txtQue29");
                tblDentalHistory.Columns.Add("txtQue29a");
                tblDentalHistory.Columns.Add("txtQue26");
                tblDentalHistory.Columns.Add("txtComments");
                tblDentalHistory.Rows.Add(objHistory.txtQue5, objHistory.txtQue6, objHistory.MrdQue7, objHistory.txtQue12, objHistory.DhQue5, objHistory.MchkFixed, objHistory.MchkRemoveable, objHistory.MchkDenture,
                    objHistory.MchkImplant, objHistory.rdQue15, objHistory.rdQue16, objHistory.rdQue17, objHistory.rdQue18, objHistory.rdQue19, objHistory.rdQue21,
                    objHistory.MchkHot, objHistory.MchkCold, objHistory.MchkSweets,
                    objHistory.MchkPressure, objHistory.txtQue22, objHistory.txtQue23, objHistory.txtQue24, objHistory.rdQue24, objHistory.txtQue29, objHistory.txtQue29a, objHistory.txtQue26,
                    objHistory.txtComments);
                //Join Two Table
                DataSet dsDentalHistory = new DataSet("DentalHistory");
                dsDentalHistory.Tables.Add(tblDentalHistory);
                DataSet ds = dsDentalHistory.Copy();
                dsDentalHistory.Clear();
                string DentalHistory = ds.GetXml();
                bool status = ObjPatientsData.UpdateDentalHistory(SafeValue<int>(PatientId), "UpdateDentalHistory", DentalHistory);
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("BusinessLogicLayer--AddDentalHistory", Ex.Message, Ex.StackTrace);
                throw;
            }
            return 1;
        }

        public int InsertReferral(string Subject, string Body, int MessageType, int ReceiverId, int PatientId, string RegardOption, string RequestingOption, int ReferStatus, int SenderId, string XMLstring, bool IsOneClickRefferal, int LocationId, string Comment, string imageid, string docid, int ItHasInsuranceData = 0)
        {
            try
            {
                clsPatientsData ObjPatientsData = new clsPatientsData();
                int status = ObjPatientsData.InsertReferral(Subject, Body, MessageType, ReceiverId, PatientId, RegardOption, RequestingOption, ReferStatus, SenderId, XMLstring, IsOneClickRefferal, LocationId, Comment, imageid, docid,ItHasInsuranceData);
                return status;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("BusinessLogicLayer--InsertReferral", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        //overloaded for box folder mapping
        public int InsertReferral(string Subject, string Body, int MessageType, int ReceiverId, int PatientId, string RegardOption, string RequestingOption, int ReferStatus, int SenderId, string XMLstring, bool IsOneClickRefferal, int LocationId, string Comment, string imageid, string docid, string boxFolderId,int ItHasInsuranceData = 0)
        {
            try
            {
                clsPatientsData ObjPatientsData = new clsPatientsData();
                ObjPatientsData.BoxFolderId = boxFolderId;
                int status = ObjPatientsData.InsertReferral(Subject, Body, MessageType, ReceiverId, PatientId, RegardOption, RequestingOption, ReferStatus, SenderId, XMLstring, IsOneClickRefferal, LocationId, Comment, imageid, docid, ItHasInsuranceData);
                return status;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("BusinessLogicLayer--InsertReferral", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        // Method For insert data by Form Method Post Create When Save logic done for Doctor Field
        //public int FormMethodPostAddNewPatient(FormCollection frmc,int UserId)
        //{
        //    try
        //    {
        //        ObjPatientData = new clsPatientsData();
        //        PatientDentist_Referral mdlPatient = new PatientDentist_Referral();

        //        mdlPatient.txtFirstName = frmc.Get("txtFirstName");
        //        mdlPatient.txtLastName = frmc.Get("txtLastName");
        //        mdlPatient.txtEmail = frmc.Get("txtEmail");
        //        mdlPatient.txtResidenceTelephone = frmc.Get("txtResidenceTelephone");
        //        mdlPatient.txtWorkPhone = frmc.Get("txtWorkPhone");
        //        mdlPatient.txtemergencyname = frmc.Get("txtemergencyname");
        //        mdlPatient.txtemergency = frmc.Get("txtemergency");
        //        mdlPatient.UserId = UserId;
        //        mdlPatient.PatientId = 0;

        //        int NewPatientId = 0;
        //        string Password = "";
        //        if (mdlPatient.PatientId <= 0)
        //        {
        //            Password = objCommon.CreateRandomPassword(8);
        //        }

        //        mdlPatient.Password = Password;
        //        NewPatientId = ObjPatientData.AddPatientFromOneClickRefferal(mdlPatient);

        //        return NewPatientId;
        //    }
        //    catch (Exception Ex)
        //    {
        //        objCommon.InsertErrorLog("BusinessLogicLayer--FormMethodPostAddNewPatient", Ex.Message, Ex.StackTrace);
        //        throw;
        //    }
        //}
        //public bool FormMethodPostAddPatientContactInfoFromOneclick(FormCollection frmc,int PatientId)
        //{
        //    try
        //    {
        //        PatientDentist_Referral ObjPatientContactInfo = new PatientDentist_Referral();
        //        ObjPatientContactInfo.PatientId = PatientId;
        //        ObjPatientContactInfo.txtResidenceStreet = frmc.Get("txtResidenceStreet");
        //        ObjPatientContactInfo.txtCity = frmc.Get("txtCity");
        //        ObjPatientContactInfo.txtState = frmc.Get("txtState");
        //        ObjPatientContactInfo.txtZip = frmc.Get("txtZip");
        //        ObjPatientContactInfo.txtDob = frmc.Get("txtDob");
        //        if (frmc.Get("txtGender") == "Male")
        //            ObjPatientContactInfo.txtGender =false;
        //        else
        //            ObjPatientContactInfo.txtGender = true;
        //        ObjPatientContactInfo.txtemergency = frmc.Get("txtemergency");

        //        bool result = ObjPatientData.AddPatientContactInfoFromOneclick(ObjPatientContactInfo);
        //        return result;
        //    }
        //    catch (Exception Ex)
        //    {
        //        objCommon.InsertErrorLog("BusinessLogicLayer--AddPatientContactInfoFromOneclick", Ex.Message, Ex.StackTrace);
        //        throw;
        //    }
        //}
        public bool FormMethodAddInsuranceCoverage(FormCollection frmc, int PatientId)
        {
            try
            {
                clsPatientsData ObjPatientsData = new clsPatientsData();
                DataSet dsp = new DataSet();
                DataTable dtPatient = new DataTable();
                dsp = ObjPatientsData.GetPateintDetailsByPatientId(PatientId);
                dtPatient = dsp.Tables[0];
                string Gender = string.Empty;
                string Status = string.Empty;
                string PatientAddress = string.Empty;
                string DateOfBirth = string.Empty;

                string PatientCity = string.Empty;
                string PatientState = string.Empty;
                string PatientZipcode = string.Empty;
                string PatientPrimaryPhone = string.Empty;
                string MobilePhone = string.Empty;
                string PatiantFax = string.Empty;

                string EmergencyContactName = string.Empty;
                string EmergencyContactPhoneNumber = string.Empty;
                string SecondaryPhone = string.Empty;

                if (dtPatient != null && dtPatient.Rows.Count > 0)
                {
                    DataTable dtRegi = new DataTable();
                    dtRegi = ObjPatientsData.GetRegistration(PatientId, "GetRegistrationform");
                    Gender = SafeValue<string>(dtPatient.Rows[0]["Gender"]) == "0" ? "0" : "1";
                    Status = string.Empty;
                    PatientAddress = SafeValue<string>(dtPatient.Rows[0]["ExactAddress"]);
                    DateOfBirth = SafeValue<string>(dtPatient.Rows[0]["DateOfBirth"]);
                    PatientCity = SafeValue<string>(dtPatient.Rows[0]["City"]);
                    PatientState = SafeValue<string>(dtPatient.Rows[0]["State"]);
                    PatientZipcode = SafeValue<string>(dtPatient.Rows[0]["ZipCode"]);
                    PatientPrimaryPhone = SafeValue<string>(dtPatient.Rows[0]["Phone"]);
                    MobilePhone = string.Empty;
                    PatiantFax = string.Empty;
                    if (dtRegi != null && dtRegi.Rows.Count > 0)
                    {
                        EmergencyContactName = SafeValue<string>(dtRegi.Rows[0]["txtemergencyname"]);
                        EmergencyContactPhoneNumber = SafeValue<string>(dtRegi.Rows[0]["txtemergency"]);
                        SecondaryPhone = SafeValue<string>(dtRegi.Rows[0]["txtemergency"]);
                    }
                }
                string Dob = DateOfBirth; string ddlgenderreg = SafeValue<string>(Gender); string drpStatus = string.Empty;
                string ResidenceStreet = PatientAddress; string City = PatientCity; string State = PatientState;
                string Zip = PatientZipcode; string ResidenceTelephone = PatientPrimaryPhone;
                string emergencyname = EmergencyContactName; string emergency = EmergencyContactPhoneNumber; string WorkPhone = SecondaryPhone;
                string Fax = string.Empty;
                string ResponsibleDOB = string.Empty;
                string ResponsibleContact = string.Empty;
                string Whommay = string.Empty;
                string chkInsurance = string.Empty; string chkCash = string.Empty; string chkCrediteCard = string.Empty;
                DataTable tblRegistration = new DataTable("Registration");
                //tblRegistration.Columns.Add("txtResponsiblepartyFname");
                //tblRegistration.Columns.Add("txtResponsiblepartyLname");
                //tblRegistration.Columns.Add("txtResponsibleRelationship");
                //tblRegistration.Columns.Add("txtResponsibleAddress");
                //tblRegistration.Columns.Add("txtResponsibleCity");
                //tblRegistration.Columns.Add("txtResponsibleState");
                //tblRegistration.Columns.Add("txtResponsibleZipCode");
                //tblRegistration.Columns.Add("txtResponsibleDOB");
                //tblRegistration.Columns.Add("txtResponsibleContact");
                //tblRegistration.Columns.Add("txtemailaddress");
                tblRegistration.Columns.Add("txtEmployeeName1");
                tblRegistration.Columns.Add("txtInsurancePhone1");
                tblRegistration.Columns.Add("txtEmployerName1");
                tblRegistration.Columns.Add("txtEmployeeDob1");
                tblRegistration.Columns.Add("txtNameofInsurance1");
                tblRegistration.Columns.Add("txtInsuranceTelephone1");
                tblRegistration.Columns.Add("txtEmployeeName2");
                tblRegistration.Columns.Add("txtInsurancePhone2");
                tblRegistration.Columns.Add("txtEmployerName2");
                tblRegistration.Columns.Add("txtYearsEmployed2");
                tblRegistration.Columns.Add("txtNameofInsurance2");
                tblRegistration.Columns.Add("txtInsuranceTelephone2");
                StringBuilder chkMethodOfPayment = new StringBuilder();
                if (chkCash == "1")
                {
                    if (!string.IsNullOrEmpty(SafeValue<string>(chkMethodOfPayment)))
                    {
                        chkMethodOfPayment.Append("," + "Cash");
                    }
                    else
                    {
                        chkMethodOfPayment.Append("Cash");
                    }
                }
                if (chkInsurance == "1")
                {
                    if (!string.IsNullOrEmpty(SafeValue<string>(chkMethodOfPayment)))
                    {
                        chkMethodOfPayment.Append("," + "Insurance");
                    }
                    else
                    {
                        chkMethodOfPayment.Append("Insurance");
                    }
                }
                if (chkCrediteCard == "1")
                {
                    if (!string.IsNullOrEmpty(SafeValue<string>(chkMethodOfPayment)))
                    {
                        chkMethodOfPayment.Append("," + "Credit Card");
                    }
                    else
                    {
                        chkMethodOfPayment.Append("Credit Card");
                    }
                }
                PatientDentist_Referral objInsuranceCoverage = new PatientDentist_Referral();
                objInsuranceCoverage.txtEmployeeName1 = frmc.Get("txtEmployeeName1");
                objInsuranceCoverage.txtInsurancePhone1 = frmc.Get("txtInsurancePhone1");
                objInsuranceCoverage.txtEmployerName1 = frmc.Get("txtEmployerName1");
                objInsuranceCoverage.txtEmployeeDob1 = frmc.Get("txtEmployeeDob1");
                objInsuranceCoverage.txtNameofInsurance1 = frmc.Get("txtNameofInsurance1");
                objInsuranceCoverage.txtInsuranceTelephone1 = frmc.Get("txtInsuranceTelephone1");
                objInsuranceCoverage.txtEmployeeName2 = frmc.Get("txtEmployeeName2");
                objInsuranceCoverage.txtInsurancePhone2 = frmc.Get("txtInsurancePhone2");
                objInsuranceCoverage.txtEmployerName2 = frmc.Get("txtEmployerName2");
                objInsuranceCoverage.txtYearsEmployed2 = frmc.Get("txtYearsEmployed2");
                objInsuranceCoverage.txtNameofInsurance2 = frmc.Get("txtNameofInsurance2");
                objInsuranceCoverage.txtInsuranceTelephone2 = frmc.Get("txtInsuranceTelephone2");

                tblRegistration.Rows.Add(objInsuranceCoverage.txtEmployeeName1, objInsuranceCoverage.txtInsurancePhone1
                                        , objInsuranceCoverage.txtEmployerName1, objInsuranceCoverage.txtEmployeeDob1,
                                        objInsuranceCoverage.txtNameofInsurance1, objInsuranceCoverage.txtInsuranceTelephone1
                                       , objInsuranceCoverage.txtEmployeeName2, objInsuranceCoverage.txtInsurancePhone2,
                                        objInsuranceCoverage.txtEmployerName2, objInsuranceCoverage.txtYearsEmployed2
                                       , objInsuranceCoverage.txtNameofInsurance2, objInsuranceCoverage.txtInsuranceTelephone2);
                if (Dob == null || Dob == "")
                {
                    Dob = "1900-01-01 00:00:00.000";
                }
                DataSet dsRegistration = new DataSet("Registration");
                dsRegistration.Tables.Add(tblRegistration);
                DataSet ds = dsRegistration.Copy();
                dsRegistration.Clear();
                string Registration = ds.GetXml();
                bool status1 = ObjPatientsData.UpdateRegistrationForm(PatientId, "UpdatePatient", SafeValue<string>(Dob), ddlgenderreg,
                    drpStatus, ResidenceStreet, City, State, Zip,
                     ResidenceTelephone, WorkPhone, Fax, Registration);
                return true;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("BusinessLogicLayer--AddInsuranceCoverage", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        public bool FormMethodAddMediacalHistory(FormCollection frmc, int PatientId)
        {
            try
            {
                clsPatientsData ObjPatientsData = new clsPatientsData();
                // Dental History Table
                PatientDentist_Referral objHistory = new PatientDentist_Referral();
                objHistory.MrdQue1 = frmc.Get("MrdQue1") == "on" ? "Y" : "N";
                objHistory.Mtxtphysicians = frmc.Get("Mtxtphysicians");
                objHistory.MrdQue2 = frmc.Get("MrdQue2") == "on" ? "Y" : "N";
                objHistory.Mtxthospitalized = frmc.Get("Mtxthospitalized");
                objHistory.MrdQue3 = frmc.Get("MrdQue3") == "on" ? "Y" : "N";
                objHistory.Mtxtserious = frmc.Get("Mtxtserious");
                objHistory.MrdQue4 = frmc.Get("MrdQue4") == "on" ? "Y" : "N";
                objHistory.Mtxtmedications = frmc.Get("Mtxtmedications");
                objHistory.MrdQue5 = frmc.Get("MrdQue5") == "on" ? "Y" : "N";
                objHistory.MtxtRedux = frmc.Get("MtxtRedux");
                objHistory.MrdQue6 = frmc.Get("MrdQue6") == "on" ? "Y" : "N";
                objHistory.MtxtFosamax = frmc.Get("MtxtFosamax");
                objHistory.MrdQuediet7 = frmc.Get("MrdQuediet7") == "on" ? "Y" : "N";
                objHistory.Mtxt7 = frmc.Get("Mtxt7");
                objHistory.Mrdotobacco = frmc.Get("Mrdotobacco") == "on" ? "Y" : "N";
                objHistory.Mtxt8 = frmc.Get("Mtxt8");
                objHistory.Mrdosubstances = frmc.Get("Mrdosubstances") == "on" ? "Y" : "N";
                objHistory.Mtxt9 = frmc.Get("Mtxt9");
                objHistory.Mrdopregnant = frmc.Get("Mrdopregnant") == "on" ? "Y" : "N";
                objHistory.Mtxt10 = frmc.Get("Mtxt10");
                objHistory.Mtxt11 = frmc.Get("Mtxt11") == "on" ? "Y" : "N";
                objHistory.Mtxt12 = frmc.Get("Mtxt12") == "on" ? "Y" : "N";
                objHistory.Mtxt13 = frmc.Get("Mtxt13") == "on" ? "Y" : "N";

                objHistory.QueFollowing14 = frmc.Get("QueFollowing14") == "on" ? "Y" : "N";
                objHistory.MchkQue_1 = frmc.Get("MchkQue_1") == "on" ? "Y" : "N";
                objHistory.MchkQue_2 = frmc.Get("MchkQue_2") == "on" ? "Y" : "N";
                objHistory.MchkQue_3 = frmc.Get("MchkQue_3") == "on" ? "Y" : "N";
                objHistory.MchkQue_4 = frmc.Get("MchkQue_4") == "on" ? "Y" : "N";
                objHistory.MchkQue_5 = frmc.Get("MchkQue_5") == "on" ? "Y" : "N";

                objHistory.MchkQue_6 = frmc.Get("MchkQue_6") == "on" ? "Y" : "N";
                objHistory.MchkQue_7 = frmc.Get("MchkQue_7") == "on" ? "Y" : "N";
                objHistory.MchkQue_8 = frmc.Get("MchkQue_8") == "on" ? "Y" : "N";

                objHistory.QueFollowing15 = frmc.Get("QueFollowing15") == "on" ? "Y" : "N";

                objHistory.MrdQueAbnormal = frmc.Get("MrdQueAbnormal") == "on" ? "Y" : "N";
                objHistory.MrdQueAIDS_HIV_Positive = frmc.Get("MrdQueAIDS_HIV_Positive") == "on" ? "Y" : "N";
                objHistory.MrdQueAlzheimer = frmc.Get("MrdQueAlzheimer") == "on" ? "Y" : "N";
                objHistory.MrdQueAnaphylaxis = frmc.Get("MrdQueAnaphylaxis") == "on" ? "Y" : "N";

                objHistory.MrdQueAnemia = frmc.Get("MrdQueAnemia") == "on" ? "Y" : "N";
                objHistory.MrdQueAngina = frmc.Get("MrdQueAngina") == "on" ? "Y" : "N";
                objHistory.MrdQueThinners = frmc.Get("MrdQueThinners") == "on" ? "Y" : "N";
                objHistory.MrdQueArthritis_Gout = frmc.Get("MrdQueArthritis_Gout") == "on" ? "Y" : "N";
                objHistory.MrdQueArtificialHeartValve = frmc.Get("MrdQueArtificialHeartValve") == "on" ? "Y" : "N";
                objHistory.MrdQueArtificialJoint = frmc.Get("MrdQueArtificialJoint") == "on" ? "Y" : "N";
                objHistory.MrdQueAsthma = frmc.Get("MrdQueAsthma") == "on" ? "Y" : "N";
                objHistory.MrdQueFibrillation = frmc.Get("MrdQueFibrillation") == "on" ? "Y" : "N";
                objHistory.MrdQueBloodDisease = frmc.Get("MrdQueBloodDisease") == "on" ? "Y" : "N";
                objHistory.MrdQueBloodTransfusion = frmc.Get("MrdQueBloodTransfusion") == "on" ? "Y" : "N";
                objHistory.MrdQueBreathing = frmc.Get("MrdQueBreathing") == "on" ? "Y" : "N";
                objHistory.MrdQueBruise = frmc.Get("MrdQueBruise") == "on" ? "Y" : "N";
                objHistory.MrdQueCancer = frmc.Get("MrdQueCancer") == "on" ? "Y" : "N";
                objHistory.MrdQueChemicalDependancy = frmc.Get("MrdQueChemicalDependancy") == "on" ? "Y" : "N";
                objHistory.MrdQueChemotherapy = frmc.Get("MrdQueChemotherapy") == "on" ? "Y" : "N";
                objHistory.MrdQueChest = frmc.Get("MrdQueChest") == "on" ? "Y" : "N";
                objHistory.MrdQueCigarette = frmc.Get("MrdQueCigarette") == "on" ? "Y" : "N";
                objHistory.MrdQueCirulation = frmc.Get("MrdQueCirulation") == "on" ? "Y" : "N";
                objHistory.MrdQueCortisone = frmc.Get("MrdQueCortisone") == "on" ? "Y" : "N";
                objHistory.MrdQueFrequentCough = frmc.Get("MrdQueFrequentCough") == "on" ? "Y" : "N";
                objHistory.MrdQueConvulsions = frmc.Get("MrdQueConvulsions") == "on" ? "Y" : "N";
                objHistory.MrdQuefillers = frmc.Get("MrdQuefillers") == "on" ? "Y" : "N";
                objHistory.MrdQueDiabetes = frmc.Get("MrdQueDiabetes") == "on" ? "Y" : "N";
                objHistory.MrdQueDiarrhhea = frmc.Get("MrdQueDiarrhhea") == "on" ? "Y" : "N";
                objHistory.MrdQueDigestive = frmc.Get("MrdQueDigestive") == "on" ? "Y" : "N";
                objHistory.MrdQueDizziness = frmc.Get("MrdQueDizziness") == "on" ? "Y" : "N";
                objHistory.MrdQueAlcoholic = frmc.Get("MrdQueAlcoholic") == "on" ? "Y" : "N";
                objHistory.MrdQueSubstances = frmc.Get("MrdQueSubstances") == "on" ? "Y" : "N";
                objHistory.MrdQueEmphysema = frmc.Get("MrdQueEmphysema") == "on" ? "Y" : "N";

                objHistory.MrdQueEpilepsy = frmc.Get("MrdQueEpilepsy") == "on" ? "Y" : "N";
                objHistory.MrdQueExcessiveUrination = frmc.Get("MrdQueExcessiveUrination") == "on" ? "Y" : "N";
                objHistory.MrdQueExcessiveThirst = frmc.Get("MrdQueExcessiveThirst") == "on" ? "Y" : "N";
                objHistory.MrdQueGlaucoma = frmc.Get("MrdQueGlaucoma") == "on" ? "Y" : "N";

                objHistory.MrdQueHeadaches = frmc.Get("MrdQueHeadaches") == "on" ? "Y" : "N";
                objHistory.MrdQueAttack = frmc.Get("MrdQueAttack") == "on" ? "Y" : "N";
                objHistory.MrdQueHeartPacemaker = frmc.Get("MrdQueHeartPacemaker") == "on" ? "Y" : "N";
                objHistory.MrdQueHemophilia = frmc.Get("MrdQueHemophilia") == "on" ? "Y" : "N";

                objHistory.MrdQueHeart_defect = frmc.Get("MrdQueHeart_defect") == "on" ? "Y" : "N";
                objHistory.MrdQueHeart_murmur = frmc.Get("MrdQueHeart_murmur") == "on" ? "Y" : "N";
                objHistory.MrdQueHepatitisA = frmc.Get("MrdQueHepatitisA") == "on" ? "Y" : "N";
                objHistory.MrdQueHerpes = frmc.Get("MrdQueHerpes") == "on" ? "Y" : "N";
                objHistory.MrdQueHiatal = frmc.Get("MrdQueHiatal") == "on" ? "Y" : "N";

                objHistory.MrdQueBlood_pressure = frmc.Get("MrdQueBlood_pressure") == "on" ? "Y" : "N";
                objHistory.MrdQueAIDS = frmc.Get("MrdQueAIDS") == "on" ? "Y" : "N";
                objHistory.MrdQueHow_much = frmc.Get("MrdQueHow_much") == "on" ? "Y" : "N";
                objHistory.MrdQueHow_often = frmc.Get("MrdQueHow_often") == "on" ? "Y" : "N";
                objHistory.MrdQueHypoglycemia = frmc.Get("MrdQueHypoglycemia") == "on" ? "Y" : "N";

                objHistory.MrdQueReplacement = frmc.Get("MrdQueReplacement") == "on" ? "Y" : "N";
                objHistory.MrdQueKidney = frmc.Get("MrdQueKidney") == "on" ? "Y" : "N";
                objHistory.MrdQueLeukemia = frmc.Get("MrdQueLeukemia") == "on" ? "Y" : "N";
                objHistory.MrdQueLiver = frmc.Get("MrdQueLiver") == "on" ? "Y" : "N";
                objHistory.MrdQueLung = frmc.Get("MrdQueLung") == "on" ? "Y" : "N";

                objHistory.MrdQueMitral = frmc.Get("MrdQueMitral") == "on" ? "Y" : "N";
                objHistory.MrdQueNeck_BackProblem = frmc.Get("MrdQueNeck_BackProblem") == "on" ? "Y" : "N";
                objHistory.MrdQueOsteoporosis = frmc.Get("MrdQueOsteoporosis") == "on" ? "Y" : "N";
                objHistory.MrdQuePacemaker = frmc.Get("MrdQuePacemaker") == "on" ? "Y" : "N";
                objHistory.MrdQuePainJaw_Joints = frmc.Get("MrdQuePainJaw_Joints") == "on" ? "Y" : "N";



                objHistory.MrdQueEndocarditis = frmc.Get("MrdQueEndocarditis") == "on" ? "Y" : "N";
                objHistory.MrdQuePsychiatric = frmc.Get("MrdQuePsychiatric") == "on" ? "Y" : "N";
                objHistory.MrdQueRadiation = frmc.Get("MrdQueRadiation") == "on" ? "Y" : "N";

                objHistory.MrdQueRheumatic = frmc.Get("MrdQueRheumatic") == "on" ? "Y" : "N";
                objHistory.MrdQueScarlet = frmc.Get("MrdQueScarlet") == "on" ? "Y" : "N";
                objHistory.MrdQueShingles = frmc.Get("MrdQueShingles") == "on" ? "Y" : "N";
                objHistory.MrdQueShortness = frmc.Get("MrdQueShortness") == "on" ? "Y" : "N";

                objHistory.MrdQueSinus = frmc.Get("MrdQueSinus") == "on" ? "Y" : "N";
                objHistory.MrdQueStroke = frmc.Get("MrdQueStroke") == "on" ? "Y" : "N";
                objHistory.MrdQueSjorgren = frmc.Get("MrdQueSjorgren") == "on" ? "Y" : "N";
                objHistory.MrdQueStomach = frmc.Get("MrdQueStomach") == "on" ? "Y" : "N";
                objHistory.MrdQueSwelling = frmc.Get("MrdQueSwelling") == "on" ? "Y" : "N";

                objHistory.MrdQueSwollen = frmc.Get("MrdQueSwollen") == "on" ? "Y" : "N";
                objHistory.MrdQueThyroid = frmc.Get("MrdQueThyroid") == "on" ? "Y" : "N";
                objHistory.MrdQueTuberculosis = frmc.Get("MrdQueTuberculosis") == "on" ? "Y" : "N";
                objHistory.MrdQueTumors = frmc.Get("MrdQueTumors") == "on" ? "Y" : "N";
                objHistory.MrdQueValve = frmc.Get("MrdQueValve") == "on" ? "Y" : "N";
                objHistory.MrdQueVision = frmc.Get("MrdQueVision") == "on" ? "Y" : "N";

                objHistory.MrdQueUnexplained = frmc.Get("MrdQueUnexplained") == "on" ? "Y" : "N";
                objHistory.Mrdillness = frmc.Get("Mrdillness") == "on" ? "Y" : "N";
                objHistory.Mtxtillness = frmc.Get("Mtxtillness");
                objHistory.MrdComments = frmc.Get("MrdComments") == "on" ? "Y" : "N";
                objHistory.MtxtComments = frmc.Get("MtxtComments");




                DataTable tblMedicalHistory = new DataTable("MedicalHistory");
                tblMedicalHistory.Columns.Add("MrdQue1");
                tblMedicalHistory.Columns.Add("Mtxtphysicians");
                tblMedicalHistory.Columns.Add("MrdQue2");
                tblMedicalHistory.Columns.Add("Mtxthospitalized");
                tblMedicalHistory.Columns.Add("MrdQue3");
                tblMedicalHistory.Columns.Add("Mtxtserious");
                tblMedicalHistory.Columns.Add("MrdQue4");
                tblMedicalHistory.Columns.Add("Mtxtmedications");
                tblMedicalHistory.Columns.Add("MrdQue5");
                tblMedicalHistory.Columns.Add("MtxtRedux");
                tblMedicalHistory.Columns.Add("MrdQue6");
                tblMedicalHistory.Columns.Add("MtxtFosamax");

                tblMedicalHistory.Columns.Add("MrdQuediet7");
                tblMedicalHistory.Columns.Add("Mtxt7");
                tblMedicalHistory.Columns.Add("Mrdotobacco");
                tblMedicalHistory.Columns.Add("Mtxt8");
                tblMedicalHistory.Columns.Add("Mrdosubstances");
                tblMedicalHistory.Columns.Add("Mtxt9");
                tblMedicalHistory.Columns.Add("Mrdopregnant");
                tblMedicalHistory.Columns.Add("Mtxt10");
                tblMedicalHistory.Columns.Add("Mtxt11");
                tblMedicalHistory.Columns.Add("Mtxt12");
                tblMedicalHistory.Columns.Add("Mtxt13");
                tblMedicalHistory.Columns.Add("QueFollowing14");
                tblMedicalHistory.Columns.Add("MchkQue_1");
                tblMedicalHistory.Columns.Add("MchkQue_2");
                tblMedicalHistory.Columns.Add("MchkQue_3");
                tblMedicalHistory.Columns.Add("MchkQue_4");
                tblMedicalHistory.Columns.Add("MchkQue_5");
                tblMedicalHistory.Columns.Add("MchkQue_6");
                tblMedicalHistory.Columns.Add("MchkQue_7");
                tblMedicalHistory.Columns.Add("MchkQue_8");
                tblMedicalHistory.Columns.Add("QueFollowing15");
                tblMedicalHistory.Columns.Add("MrdQueAbnormal");
                tblMedicalHistory.Columns.Add("MrdQueAIDS_HIV_Positive");
                tblMedicalHistory.Columns.Add("MrdQueAlzheimer");
                tblMedicalHistory.Columns.Add("MrdQueAnaphylaxis");
                tblMedicalHistory.Columns.Add("MrdQueAnemia");
                tblMedicalHistory.Columns.Add("MrdQueAngina");

                tblMedicalHistory.Columns.Add("MrdQueThinners");
                tblMedicalHistory.Columns.Add("MrdQueArthritis_Gout");
                tblMedicalHistory.Columns.Add("MrdQueArtificialHeartValve");
                tblMedicalHistory.Columns.Add("MrdQueArtificialJoint");
                tblMedicalHistory.Columns.Add("MrdQueAsthma");
                tblMedicalHistory.Columns.Add("MrdQueFibrillation");
                tblMedicalHistory.Columns.Add("MrdQueBloodDisease");
                tblMedicalHistory.Columns.Add("MrdQueBloodTransfusion");
                tblMedicalHistory.Columns.Add("MrdQueBreathing");
                tblMedicalHistory.Columns.Add("MrdQueBruise");
                tblMedicalHistory.Columns.Add("MrdQueCancer");
                tblMedicalHistory.Columns.Add("MrdQueChemicalDependancy");
                tblMedicalHistory.Columns.Add("MrdQueChemotherapy");
                tblMedicalHistory.Columns.Add("MrdQueChest");
                tblMedicalHistory.Columns.Add("MrdQueCigarette");
                tblMedicalHistory.Columns.Add("MrdQueCirulation");
                tblMedicalHistory.Columns.Add("MrdQueCortisone");
                tblMedicalHistory.Columns.Add("MrdQueFrequentCough");
                tblMedicalHistory.Columns.Add("MrdQueConvulsions");
                tblMedicalHistory.Columns.Add("MrdQuefillers");
                tblMedicalHistory.Columns.Add("MrdQueDiabetes");
                tblMedicalHistory.Columns.Add("MrdQueDiarrhhea");
                tblMedicalHistory.Columns.Add("MrdQueDigestive");
                tblMedicalHistory.Columns.Add("MrdQueDizziness");
                tblMedicalHistory.Columns.Add("MrdQueAlcoholic");
                tblMedicalHistory.Columns.Add("MrdQueSubstances");
                tblMedicalHistory.Columns.Add("MrdQueEmphysema");
                tblMedicalHistory.Columns.Add("MrdQueEpilepsy");
                tblMedicalHistory.Columns.Add("MrdQueExcessiveUrination");
                tblMedicalHistory.Columns.Add("MrdQueExcessiveThirst");
                tblMedicalHistory.Columns.Add("MrdQueGlaucoma");
                tblMedicalHistory.Columns.Add("MrdQueHeadaches");
                tblMedicalHistory.Columns.Add("MrdQueAttack");
                tblMedicalHistory.Columns.Add("MrdQueHeartPacemaker");
                tblMedicalHistory.Columns.Add("MrdQueHemophilia");
                tblMedicalHistory.Columns.Add("MrdQueHeart_defect");
                tblMedicalHistory.Columns.Add("MrdQueHeart_murmur");
                tblMedicalHistory.Columns.Add("MrdQueHepatitisA");
                tblMedicalHistory.Columns.Add("MrdQueHerpes");
                tblMedicalHistory.Columns.Add("MrdQueHiatal");
                tblMedicalHistory.Columns.Add("MrdQueBlood_pressure");
                tblMedicalHistory.Columns.Add("MrdQueAIDS");
                tblMedicalHistory.Columns.Add("MrdQueHow_much");
                tblMedicalHistory.Columns.Add("MrdQueHow_often");
                tblMedicalHistory.Columns.Add("MrdQueHypoglycemia");
                tblMedicalHistory.Columns.Add("MrdQueReplacement");
                tblMedicalHistory.Columns.Add("MrdQueKidney");
                tblMedicalHistory.Columns.Add("MrdQueLeukemia");
                tblMedicalHistory.Columns.Add("MrdQueLiver");
                tblMedicalHistory.Columns.Add("MrdQueLung");
                tblMedicalHistory.Columns.Add("MrdQueMitral");
                tblMedicalHistory.Columns.Add("MrdQueNeck_BackProblem");
                tblMedicalHistory.Columns.Add("MrdQueOsteoporosis");
                tblMedicalHistory.Columns.Add("MrdQuePacemaker");
                tblMedicalHistory.Columns.Add("MrdQuePainJaw_Joints");
                tblMedicalHistory.Columns.Add("MrdQueEndocarditis");
                tblMedicalHistory.Columns.Add("MrdQuePsychiatric");
                tblMedicalHistory.Columns.Add("MrdQueRadiation");
                tblMedicalHistory.Columns.Add("MrdQueRheumatic");
                tblMedicalHistory.Columns.Add("MrdQueScarlet");
                tblMedicalHistory.Columns.Add("MrdQueShingles");
                tblMedicalHistory.Columns.Add("MrdQueShortness");
                tblMedicalHistory.Columns.Add("MrdQueSinus");
                tblMedicalHistory.Columns.Add("MrdQueStroke");
                tblMedicalHistory.Columns.Add("MrdQueSjorgren");
                tblMedicalHistory.Columns.Add("MrdQueStomach");
                tblMedicalHistory.Columns.Add("MrdQueSwelling");
                tblMedicalHistory.Columns.Add("MrdQueSwollen");
                tblMedicalHistory.Columns.Add("MrdQueThyroid");
                tblMedicalHistory.Columns.Add("MrdQueTuberculosis");
                tblMedicalHistory.Columns.Add("MrdQueTumors");
                tblMedicalHistory.Columns.Add("MrdQueValve");
                tblMedicalHistory.Columns.Add("MrdQueVision");
                tblMedicalHistory.Columns.Add("MrdQueUnexplained");
                tblMedicalHistory.Columns.Add("Mrdillness");
                tblMedicalHistory.Columns.Add("Mtxtillness");
                tblMedicalHistory.Columns.Add("MrdComments");
                tblMedicalHistory.Columns.Add("MtxtComments");

                tblMedicalHistory.Rows.Add(objHistory.MrdQue1, objHistory.Mtxtphysicians, objHistory.MrdQue2, objHistory.Mtxthospitalized, objHistory.MrdQue3, objHistory.Mtxtserious, objHistory.MrdQue4,
                                           objHistory.Mtxtmedications, objHistory.MrdQue5, objHistory.MtxtRedux,
                                            objHistory.MrdQue6, objHistory.MtxtFosamax, objHistory.MrdQuediet7, objHistory.Mtxt7, objHistory.Mrdotobacco, objHistory.Mtxt8,
                                            objHistory.Mrdosubstances, objHistory.Mtxt9, objHistory.Mrdopregnant, objHistory.Mtxt10, objHistory.Mtxt11, objHistory.Mtxt12, objHistory.Mtxt13
                                             , objHistory.QueFollowing14, objHistory.MchkQue_1, objHistory.MchkQue_2, objHistory.MchkQue_3, objHistory.MchkQue_4, objHistory.MchkQue_5,
                                            objHistory.MchkQue_6, objHistory.MchkQue_7, objHistory.MchkQue_8
                                           , objHistory.QueFollowing15
                                            , objHistory.MrdQueAbnormal, objHistory.MrdQueAIDS_HIV_Positive, objHistory.MrdQueAlzheimer, objHistory.MrdQueAnaphylaxis,
                                            objHistory.MrdQueAnemia, objHistory.MrdQueAngina, objHistory.MrdQueThinners, objHistory.MrdQueArthritis_Gout, objHistory.MrdQueArtificialHeartValve,
                                            objHistory.MrdQueArtificialJoint, objHistory.MrdQueAsthma, objHistory.MrdQueFibrillation, objHistory.MrdQueBloodDisease,
                                            objHistory.MrdQueBloodTransfusion, objHistory.MrdQueBreathing, objHistory.MrdQueBruise, objHistory.MrdQueCancer, objHistory.MrdQueChemicalDependancy,
                                            objHistory.MrdQueChemotherapy, objHistory.MrdQueChest, objHistory.MrdQueCigarette, objHistory.MrdQueCirulation, objHistory.MrdQueCortisone,
                                            objHistory.MrdQueFrequentCough, objHistory.MrdQueConvulsions, objHistory.MrdQuefillers, objHistory.MrdQueDiabetes, objHistory.MrdQueDiarrhhea,
                                            objHistory.MrdQueDigestive, objHistory.MrdQueDizziness, objHistory.MrdQueAlcoholic, objHistory.MrdQueSubstances, objHistory.MrdQueEmphysema,
                                            objHistory.MrdQueEpilepsy, objHistory.MrdQueExcessiveUrination, objHistory.MrdQueExcessiveThirst, objHistory.MrdQueGlaucoma,
                                            objHistory.MrdQueHeadaches, objHistory.MrdQueAttack, objHistory.MrdQueHeartPacemaker, objHistory.MrdQueHemophilia,
                                            objHistory.MrdQueHeart_defect, objHistory.MrdQueHeart_murmur, objHistory.MrdQueHepatitisA, objHistory.MrdQueHerpes, objHistory.MrdQueHiatal,
                                            objHistory.MrdQueBlood_pressure, objHistory.MrdQueAIDS, objHistory.MrdQueHow_much, objHistory.MrdQueHow_often, objHistory.MrdQueHypoglycemia,
                                            objHistory.MrdQueReplacement, objHistory.MrdQueKidney, objHistory.MrdQueLeukemia, objHistory.MrdQueLiver, objHistory.MrdQueLung,
                                            objHistory.MrdQueMitral, objHistory.MrdQueNeck_BackProblem, objHistory.MrdQueOsteoporosis, objHistory.MrdQuePacemaker, objHistory.MrdQuePainJaw_Joints,


                                            objHistory.MrdQueEndocarditis, objHistory.MrdQuePsychiatric, objHistory.MrdQueRadiation,
                                            objHistory.MrdQueRheumatic, objHistory.MrdQueScarlet, objHistory.MrdQueShingles, objHistory.MrdQueShortness,
                                            objHistory.MrdQueSinus, objHistory.MrdQueStroke, objHistory.MrdQueSjorgren, objHistory.MrdQueStomach, objHistory.MrdQueSwelling,
                                            objHistory.MrdQueSwollen, objHistory.MrdQueThyroid, objHistory.MrdQueTuberculosis, objHistory.MrdQueTumors, objHistory.MrdQueValve, objHistory.MrdQueVision,
                                            objHistory.MrdQueUnexplained, objHistory.Mrdillness, objHistory.Mtxtillness, objHistory.MrdComments, objHistory.MtxtComments);

                DataSet dsMedicalHistory = new DataSet("MedicalHistory");
                dsMedicalHistory.Tables.Add(tblMedicalHistory);

                DataSet ds = dsMedicalHistory.Copy();
                dsMedicalHistory.Clear();

                string MedicalHistory = ds.GetXml();

                bool status = ObjPatientsData.UpdateMedicalHistory(PatientId, "UpdateMedicalHistory", MedicalHistory);
                return true;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("BusinessLogicLayer--AddMediacalHistory", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        public bool FormMethodAddDentalHistory(FormCollection frmc, int PatientId)
        {
            try
            {
                clsPatientsData ObjPatientsData = new clsPatientsData();
                PatientDentist_Referral objHistory = new PatientDentist_Referral();
                objHistory.txtQue5 = frmc.Get("txtQue5");
                objHistory.txtQue6 = frmc.Get("txtQue6");
                objHistory.MrdQue7 = (frmc.Get("MrdQue7") == "on") ? "Y" : "N";
                objHistory.MtxtHowoften = frmc.Get("MtxtHowoften");
                objHistory.txtQue12 = frmc.Get("txtQue12");
                objHistory.DhQue5 = frmc.Get("DhQue5") == "on" ? "Y" : "N";
                objHistory.MchkFixed = frmc.Get("MchkFixed") == "on" ? "Y" : "N";
                objHistory.MchkRemoveable = frmc.Get("MchkRemoveable") == "on" ? "Y" : "N";
                objHistory.MchkDenture = frmc.Get("MchkDenture") == "on" ? "Y" : "N";
                objHistory.MchkImplant = frmc.Get("MchkImplant") == "on" ? "Y" : "N";
                objHistory.rdQue15 = frmc.Get("rdQue15") == "on" ? "Y" : "N";
                objHistory.rdQue16 = frmc.Get("rdQue16") == "on" ? "Y" : "N";
                objHistory.rdQue17 = frmc.Get("rdQue17") == "on" ? "Y" : "N";
                objHistory.rdQue18 = frmc.Get("rdQue18") == "on" ? "Y" : "N";
                objHistory.rdQue19 = frmc.Get("rdQue19") == "on" ? "Y" : "N";
                objHistory.rdQue21 = frmc.Get("rdQue21") == "on" ? "Y" : "N";
                objHistory.MchkHot = frmc.Get("MchkHot") == "on" ? "Y" : "N";
                objHistory.MchkCold = frmc.Get("MchkCold") == "on" ? "Y" : "N";
                objHistory.MchkSweets = frmc.Get("MchkSweets") == "on" ? "Y" : "N";
                objHistory.MchkPressure = frmc.Get("MchkPressure") == "on" ? "Y" : "N";
                objHistory.txtQue22 = frmc.Get("txtQue22") == "on" ? "Y" : "N";
                objHistory.MtxtWhen = frmc.Get("MtxtWhen");
                objHistory.txtQue23 = frmc.Get("txtQue23");
                objHistory.txtQue24 = frmc.Get("txtQue24");
                objHistory.rdQue24 = frmc.Get("rdQue24") == "on" ? "Y" : "N";
                objHistory.txtQue29 = frmc.Get("txtQue29");
                objHistory.txtQue29a = frmc.Get("txtQue29a");
                objHistory.txtQue26 = frmc.Get("txtQue26");
                objHistory.txtComments = frmc.Get("txtComments");

                DataTable tblDentalHistory = new DataTable("DentalHistory");
                tblDentalHistory.Columns.Add("txtQue5");
                tblDentalHistory.Columns.Add("txtQue6");
                tblDentalHistory.Columns.Add("MrdQue7");
                tblDentalHistory.Columns.Add("MtxtHowoften");
                tblDentalHistory.Columns.Add("txtQue12");
                tblDentalHistory.Columns.Add("DhQue5");
                tblDentalHistory.Columns.Add("MchkFixed");
                tblDentalHistory.Columns.Add("MchkRemoveable");
                tblDentalHistory.Columns.Add("MchkDenture");
                tblDentalHistory.Columns.Add("MchkImplant");
                tblDentalHistory.Columns.Add("rdQue15");
                tblDentalHistory.Columns.Add("rdQue16");
                tblDentalHistory.Columns.Add("rdQue17");
                tblDentalHistory.Columns.Add("rdQue18");
                tblDentalHistory.Columns.Add("rdQue19");
                tblDentalHistory.Columns.Add("rdQue21");
                tblDentalHistory.Columns.Add("MchkHot");
                tblDentalHistory.Columns.Add("MchkCold");
                tblDentalHistory.Columns.Add("MchkSweets");
                tblDentalHistory.Columns.Add("MchkPressure");
                tblDentalHistory.Columns.Add("txtQue22");
                tblDentalHistory.Columns.Add("MtxtWhen");
                tblDentalHistory.Columns.Add("txtQue23");
                tblDentalHistory.Columns.Add("txtQue24");
                tblDentalHistory.Columns.Add("rdQue24");
                tblDentalHistory.Columns.Add("txtQue29");
                tblDentalHistory.Columns.Add("txtQue29a");
                tblDentalHistory.Columns.Add("txtQue26");
                tblDentalHistory.Columns.Add("txtComments");
                tblDentalHistory.Rows.Add(objHistory.txtQue5, objHistory.txtQue6, objHistory.MrdQue7, objHistory.MtxtHowoften, objHistory.txtQue12, objHistory.DhQue5, objHistory.MchkFixed, objHistory.MchkRemoveable, objHistory.MchkDenture,
                    objHistory.MchkImplant, objHistory.rdQue15, objHistory.rdQue16, objHistory.rdQue17, objHistory.rdQue18, objHistory.rdQue19, objHistory.rdQue21,
                    objHistory.MchkHot, objHistory.MchkCold, objHistory.MchkSweets,
                    objHistory.MchkPressure, objHistory.txtQue22, objHistory.MtxtWhen, objHistory.txtQue23, objHistory.txtQue24, objHistory.rdQue24, objHistory.txtQue29, objHistory.txtQue29a, objHistory.txtQue26,
                    objHistory.txtComments);
                //Join Two Table
                DataSet dsDentalHistory = new DataSet("DentalHistory");
                dsDentalHistory.Tables.Add(tblDentalHistory);
                DataSet ds = dsDentalHistory.Copy();
                dsDentalHistory.Clear();
                string DentalHistory = ds.GetXml();
                bool status = ObjPatientsData.UpdateDentalHistory(SafeValue<int>(PatientId), "UpdateDentalHistory", DentalHistory);
                return true;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("BusinessLogicLayer--AddDentalHistory", Ex.Message, Ex.StackTrace);
                throw;
            }

        }


        public bool _AddInsuranceCoverage(BO.Models.InsuranceCoverage objInsuranceCoverage, int PatientId, string EmgName, string EmgPhone, Compose1Click Model)
        {
            try
            {
                if (objInsuranceCoverage != null)
                {
                    clsPatientsData ObjPatientsData = new clsPatientsData();
                    DataSet dsp = new DataSet();
                    DataTable dtPatient = new DataTable();
                    dsp = ObjPatientsData.GetPateintDetailsByPatientId(PatientId, Model._patientInfo.UserId);
                    dtPatient = dsp.Tables[0];
                    string Gender = string.Empty;
                    string Status = string.Empty;
                    string PatientAddress = string.Empty;
                    string DateOfBirth = string.Empty;

                    string PatientCity = string.Empty;
                    string PatientState = string.Empty;
                    string PatientZipcode = string.Empty;
                    string PatientPrimaryPhone = string.Empty;
                    string MobilePhone = string.Empty;
                    string PatiantFax = string.Empty;

                    string EmergencyContactName = string.Empty;
                    string EmergencyContactPhoneNumber = string.Empty;
                    string SecondaryPhone = string.Empty;
                    if (dtPatient != null && dtPatient.Rows.Count > 0)
                    {
                        DataTable dtRegi = new DataTable();
                        dtRegi = ObjPatientsData.GetRegistration(PatientId, "GetRegistrationform");
                        Gender = SafeValue<string>(dtPatient.Rows[0]["Gender"]) == "0" ? "0" : "1";
                        Status = string.Empty;
                        PatientAddress = SafeValue<string>(dtPatient.Rows[0]["ExactAddress"]);
                        DateOfBirth = SafeValue<string>(dtPatient.Rows[0]["DateOfBirth"]);
                        PatientCity = SafeValue<string>(dtPatient.Rows[0]["City"]);
                        PatientState = SafeValue<string>(dtPatient.Rows[0]["State"]);
                        PatientZipcode = SafeValue<string>(dtPatient.Rows[0]["ZipCode"]);
                        PatientPrimaryPhone = SafeValue<string>(dtPatient.Rows[0]["Phone"]);
                        SecondaryPhone = SafeValue<string>(dtPatient.Rows[0]["WorkPhone"]);
                        MobilePhone = string.Empty;
                        PatiantFax = string.Empty;
                        if (dtRegi != null && dtRegi.Rows.Count > 0)
                        {
                            EmergencyContactName = SafeValue<string>(dtRegi.Rows[0]["txtemergencyname"]);
                            EmergencyContactPhoneNumber = SafeValue<string>(dtRegi.Rows[0]["txtemergency"]);
                            SecondaryPhone = SafeValue<string>(dtRegi.Rows[0]["txtemergency"]);
                        }
                    }
                    string Dob = DateOfBirth; string ddlgenderreg = SafeValue<string>(Gender); string drpStatus = string.Empty;
                    string ResidenceStreet = PatientAddress; string City = PatientCity; string State = PatientState;
                    string Zip = PatientZipcode; string ResidenceTelephone = PatientPrimaryPhone;
                    string emergencyname = EmergencyContactName; string emergency = EmergencyContactPhoneNumber; string WorkPhone = SecondaryPhone;
                    string Fax = string.Empty;
                    string ResponsibleDOB = string.Empty;
                    string ResponsibleContact = string.Empty;
                    string Whommay = string.Empty;
                    string chkInsurance = string.Empty; string chkCash = string.Empty; string chkCrediteCard = string.Empty;
                    DataTable tblRegistration = new DataTable("Registration");
                    tblRegistration.Columns.Add("txtEmployeeName1");
                    tblRegistration.Columns.Add("txtInsurancePhone1");
                    tblRegistration.Columns.Add("txtEmployerName1");
                    tblRegistration.Columns.Add("txtEmployeeDob1");
                    tblRegistration.Columns.Add("txtNameofInsurance1");
                    tblRegistration.Columns.Add("txtInsuranceTelephone1");
                    tblRegistration.Columns.Add("txtEmployeeName2");
                    tblRegistration.Columns.Add("txtInsurancePhone2");
                    tblRegistration.Columns.Add("txtEmployerName2");
                    tblRegistration.Columns.Add("txtYearsEmployed2");
                    tblRegistration.Columns.Add("txtNameofInsurance2");
                    tblRegistration.Columns.Add("txtInsuranceTelephone2");
                    tblRegistration.Columns.Add("txtemergencyname");
                    tblRegistration.Columns.Add("txtemergency");

                    tblRegistration.Rows.Add(objInsuranceCoverage.PrimaryInsuranceCompany, objInsuranceCoverage.PrimaryInsurancePhone
                                            , objInsuranceCoverage.PrimaryNameOfInsured, objInsuranceCoverage.PrimaryDateOfBirth,
                                            objInsuranceCoverage.PrimaryMemberID, objInsuranceCoverage.PrimaryGroupNumber
                                           , objInsuranceCoverage.SecondaryInsuranceCompany, objInsuranceCoverage.SecondaryInsurancePhone,
                                            objInsuranceCoverage.SecondaryNameOfInsured, objInsuranceCoverage.SecondaryDateOfBirth
                                           , objInsuranceCoverage.SecondaryMemberID, objInsuranceCoverage.SecondaryGroupNumber, EmgName, EmgPhone);
                    //List<BO.Models.InsuranceCoverage> lstobj = new List<BO.Models.InsuranceCoverage>();


                    //lstobj.Add(objInsuranceCoverage);

                    //DataTable dt = clsCommon.ToDataTable(lstobj, true);
                    //dt.TableName = "Registration";
                    if (Dob == null || Dob == "")
                    {
                        Dob = "1900-01-01 00:00:00.000";
                    }
                    DataSet dsRegistration = new DataSet("Registration");
                    dsRegistration.Tables.Add(tblRegistration);
                    DataSet ds = dsRegistration.Copy();
                    dsRegistration.Clear();
                    string Registration = ds.GetXml();

                    //Code for issue inSecondry Phone and Gender 

                    if (dtPatient != null && dtPatient.Rows.Count > 0)
                    {
                        WorkPhone = SecondaryPhone;
                        ddlgenderreg = Gender;
                        ResidenceStreet = PatientAddress;
                        City = PatientCity;
                        State = PatientState;
                        Zip = PatientZipcode;
                        ResidenceTelephone = PatientPrimaryPhone;
                        Dob = DateOfBirth;
                    }
                    else
                    {
                        WorkPhone = Model._contactInfo.SecondaryPhoneNo;
                        ddlgenderreg = (Model._contactInfo.Gender == 1) ? "1" : "0";
                        ResidenceStreet = Model._contactInfo.Address;
                        City = Model._contactInfo.City;
                        State = Model._contactInfo.State;
                        Zip = Model._contactInfo.ZipCode;
                        ResidenceTelephone = Model._patientInfo.Phone;
                        Dob = Model._contactInfo.BOD;
                    }

                    //if (dtPatient != null && dtPatient.Rows.Count > 0)
                    //{
                    //    ddlgenderreg = Gender;
                    //}
                    //else
                    //{
                    //    ddlgenderreg = (_Gender == true) ? "1" : "0";
                    //}

                    bool status1 = ObjPatientsData.UpdateRegistrationForm(PatientId, "UpdatePatient", SafeValue<string>(Dob), ddlgenderreg,
                        drpStatus, ResidenceStreet, City, State, Zip,
                         ResidenceTelephone, WorkPhone, Fax, Registration);
                }

            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("BusinessLogicLayer--_AddInsuranceCoverage", Ex.Message, Ex.StackTrace);
                throw;
            }
            return true;
        }

        public bool _AddMedicalHistory(Compose1ClickMedicalHistory objHistory, int PatientId)
        {
            try
            {
                if (objHistory != null)
                {
                    clsPatientsData ObjPatientsData = new clsPatientsData();
                    // Dental History Table
                    DataTable tblMedicalHistory = new DataTable("MedicalHistory");

                    List<Compose1ClickMedicalHistory> lstobj = new List<Compose1ClickMedicalHistory>();
                    lstobj.Add(objHistory);
                    DataTable dt = clsCommon.ToDataTable(lstobj, true);
                    dt.TableName = "MedicalHistory";
                    DataSet dsMedicalHistory = new DataSet("MedicalHistory");
                    dsMedicalHistory.Tables.Add(dt);

                    DataSet ds = dsMedicalHistory.Copy();
                    dsMedicalHistory.Clear();

                    string MedicalHistory = ds.GetXml();

                    bool status = ObjPatientsData.UpdateMedicalHistory(PatientId, "UpdateMedicalHistory", MedicalHistory);
                }
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("BusinessLogicLayer--AddMediacalHistory", Ex.Message, Ex.StackTrace);
                throw;
            }
            return true;
        }
        public bool _AddDentalHistory(Compose1ClickDentalHistory objHistory, int PatientId)
        {
            try
            {
                if (objHistory != null)
                {
                    clsPatientsData ObjPatientsData = new clsPatientsData();
                    DataTable tblDentalHistory = new DataTable("DentalHistory");

                    //Join Two Table
                    List<Compose1ClickDentalHistory> lstobj = new List<Compose1ClickDentalHistory>();
                    lstobj.Add(objHistory);
                    if (lstobj[0].chkQue20_1 == true)
                    {
                        if (!String.IsNullOrEmpty(lstobj[0].chkQue20))
                        {
                            lstobj[0].chkQue20 += "," + "1";
                        }
                        else
                        {
                            lstobj[0].chkQue20 += "1";
                        }
                    }
                    if (lstobj[0].chkQue20_2 == true)
                    {
                        if (!String.IsNullOrEmpty(lstobj[0].chkQue20))
                        {
                            lstobj[0].chkQue20 += "," + "2";
                        }
                        else
                        {
                            lstobj[0].chkQue20 += "2";
                        }
                    }
                    if (lstobj[0].chkQue20_3 == true)
                    {
                        if (!String.IsNullOrEmpty(lstobj[0].chkQue20))
                        {
                            lstobj[0].chkQue20 += "," + "3";
                        }
                        else
                        {
                            lstobj[0].chkQue20 += "3";
                        }
                    }
                    if (lstobj[0].chkQue20_4 == true)
                    {
                        if (!String.IsNullOrEmpty(lstobj[0].chkQue20))
                        {
                            lstobj[0].chkQue20 += "," + "4";
                        }
                        else
                        {
                            lstobj[0].chkQue20 += "4";
                        }
                    }
                    DataTable dt = clsCommon.ToDataTable(lstobj, true);
                    DataSet dsDentalHistory = new DataSet("DentalHistory");
                    dt.TableName = "DentalHistory";
                    dsDentalHistory.Tables.Add(dt);
                    DataSet ds = dsDentalHistory.Copy();
                    dsDentalHistory.Clear();
                    string DentalHistory = ds.GetXml();
                    bool status = ObjPatientsData.UpdateDentalHistory(SafeValue<int>(PatientId), "UpdateDentalHistory", DentalHistory);
                }
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("BusinessLogicLayer--AddDentalHistory", Ex.Message, Ex.StackTrace);
                throw;
            }
            return true;
        }
        #endregion

        /// <summary>
        /// This Method get Location List for Location Drop-down for 1-Click referral.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static List<BO.Models.LocationDetail> GetColleaguesLocationList(int UserId)
        {
            try
            {
                DataTable dt = new clsColleaguesData().GetDoctorLocaitonById(UserId);
                List<BO.Models.LocationDetail> lst = new List<BO.Models.LocationDetail>();
                if (dt != null && dt.Rows.Count > 0)
                {
                    //if(dt.Rows.Count > 1)
                    //{
                    //    foreach (DataRow item in dt.Rows)
                    //    {
                    //        lst.Add(new BO.Models.LocationDetail()
                    //        {
                    //            LocationName = SafeValue<string>(item["Location"]),
                    //            LocationId = SafeValue<string>(item["AddressInfoId"]),
                    //            ContactType = SafeValue<int>(item["ContactType"]),
                    //        });
                    //    }
                    //}
                    foreach (DataRow item in dt.Rows)
                    {
                        lst.Add(new BO.Models.LocationDetail()
                        {
                            LocationName = SafeValue<string>(item["Location"]),
                            LocationId = SafeValue<string>(item["AddressInfoId"]),
                            ContactType = SafeValue<int>(item["ContactType"]),
                        });
                    }
                }
                return lst.ToList();
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("DentistProfileBLL---GetColleaguesLocationList", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        public static bool AddInsuranceCoverage(InsuranceCoverage objInsuranceCoverage, int PatientId, int UserId)
        {
            try
            {
                bool status1 = false;
                if (objInsuranceCoverage != null)
                {
                    clsPatientsData ObjPatientsData = new clsPatientsData();
                    DataSet dsp = new DataSet(); clsCommon objCommon = new clsCommon();
                    DataTable dtPatient = new DataTable();
                    dsp = ObjPatientsData.GetPateintDetailsByPatientId(PatientId, UserId);
                    dtPatient = dsp.Tables[0];
                    string Gender = string.Empty;
                    string Status = string.Empty;
                    string PatientAddress = string.Empty;
                    string DateOfBirth = string.Empty;

                    string PatientCity = string.Empty;
                    string PatientState = string.Empty;
                    string PatientZipcode = string.Empty;
                    string PatientPrimaryPhone = string.Empty;
                    string MobilePhone = string.Empty;
                    string PatiantFax = string.Empty;

                    string EmergencyContactName = string.Empty;
                    string EmergencyContactPhoneNumber = string.Empty;
                    string SecondaryPhone = string.Empty;
                    if (dtPatient != null && dtPatient.Rows.Count > 0)
                    {
                        DataTable dtRegi = new DataTable();
                        dtRegi = ObjPatientsData.GetRegistration(PatientId, "GetRegistrationform");
                        Gender = SafeValue<string>(dtPatient.Rows[0]["Gender"]) == "0" ? "0" : "1";
                        Status = string.Empty;
                        PatientAddress = SafeValue<string>(dtPatient.Rows[0]["ExactAddress"]);
                        DateOfBirth = SafeValue<string>(dtPatient.Rows[0]["DateOfBirth"]);
                        PatientCity = SafeValue<string>(dtPatient.Rows[0]["City"]);
                        PatientState = SafeValue<string>(dtPatient.Rows[0]["State"]);
                        PatientZipcode = SafeValue<string>(dtPatient.Rows[0]["ZipCode"]);
                        PatientPrimaryPhone = SafeValue<string>(dtPatient.Rows[0]["Phone"]);
                        SecondaryPhone = SafeValue<string>(dtPatient.Rows[0]["WorkPhone"]);
                        MobilePhone = string.Empty;
                        PatiantFax = string.Empty;
                        if (dtRegi != null && dtRegi.Rows.Count > 0)
                        {
                            EmergencyContactName = SafeValue<string>(dtRegi.Rows[0]["txtemergencyname"]);
                            EmergencyContactPhoneNumber = SafeValue<string>(dtRegi.Rows[0]["txtemergency"]);
                            SecondaryPhone = SafeValue<string>(dtRegi.Rows[0]["txtemergency"]);
                        }
                    }
                    string Dob = DateOfBirth; string ddlgenderreg = SafeValue<string>(Gender); string drpStatus = string.Empty;
                    string ResidenceStreet = PatientAddress; string City = PatientCity; string State = PatientState;
                    string Zip = PatientZipcode; string ResidenceTelephone = PatientPrimaryPhone;
                    string emergencyname = EmergencyContactName; string emergency = EmergencyContactPhoneNumber; string WorkPhone = SecondaryPhone;
                    string Fax = string.Empty;
                    string ResponsibleDOB = string.Empty;
                    string ResponsibleContact = string.Empty;
                    string Whommay = string.Empty;
                    string chkInsurance = string.Empty; string chkCash = string.Empty; string chkCrediteCard = string.Empty;
                    DataTable tblRegistration = new DataTable("Registration");
                    tblRegistration.Columns.Add("txtEmployeeName1");
                    tblRegistration.Columns.Add("txtInsurancePhone1");
                    tblRegistration.Columns.Add("txtEmployerName1");
                    tblRegistration.Columns.Add("txtEmployeeDob1");
                    tblRegistration.Columns.Add("txtNameofInsurance1");
                    tblRegistration.Columns.Add("txtInsuranceTelephone1");
                    tblRegistration.Columns.Add("txtEmployeeName2");
                    tblRegistration.Columns.Add("txtInsurancePhone2");
                    tblRegistration.Columns.Add("txtEmployerName2");
                    tblRegistration.Columns.Add("txtYearsEmployed2");
                    tblRegistration.Columns.Add("txtNameofInsurance2");
                    tblRegistration.Columns.Add("txtInsuranceTelephone2");
                    tblRegistration.Columns.Add("txtemergencyname");
                    tblRegistration.Columns.Add("txtemergency");

                    tblRegistration.Rows.Add(objInsuranceCoverage.PrimaryInsuranceCompany, objInsuranceCoverage.PrimaryInsurancePhone
                                            , objInsuranceCoverage.PrimaryNameOfInsured, objInsuranceCoverage.PrimaryDateOfBirth,
                                            objInsuranceCoverage.PrimaryMemberID, objInsuranceCoverage.PrimaryGroupNumber
                                           , objInsuranceCoverage.SecondaryInsuranceCompany, objInsuranceCoverage.SecondaryInsurancePhone,
                                            objInsuranceCoverage.SecondaryNameOfInsured, objInsuranceCoverage.SecondaryDateOfBirth
                                           , objInsuranceCoverage.SecondaryMemberID, objInsuranceCoverage.SecondaryGroupNumber, objInsuranceCoverage.EMGContactName, objInsuranceCoverage.EMGContactNo);

                    if (Dob == null || Dob == "")
                    {
                        Dob = "1900-01-01 00:00:00.000";
                    }
                    DataSet dsRegistration = new DataSet("Registration");
                    dsRegistration.Tables.Add(tblRegistration);
                    DataSet ds = dsRegistration.Copy();
                    dsRegistration.Clear();
                    string Registration = ds.GetXml();

                    //Code for issue inSecondry Phone and Gender 

                    if (dtPatient != null && dtPatient.Rows.Count > 0)
                    {
                        WorkPhone = SecondaryPhone;
                        ddlgenderreg = Gender;
                        ResidenceStreet = PatientAddress;
                        City = PatientCity;
                        State = PatientState;
                        Zip = PatientZipcode;
                        ResidenceTelephone = PatientPrimaryPhone;
                        Dob = DateOfBirth;
                    }
                    //else
                    //{
                    //    WorkPhone = Model._contactInfo.SecondaryPhoneNo;
                    //    ddlgenderreg = (Model._contactInfo.Gender == 1) ? "1" : "0";
                    //    ResidenceStreet = Model._contactInfo.Address;
                    //    City = Model._contactInfo.City;
                    //    State = Model._contactInfo.State;
                    //    Zip = Model._contactInfo.ZipCode;
                    //    ResidenceTelephone = Model._patientInfo.Phone;
                    //    Dob = Model._contactInfo.BOD;
                    //}
                    status1 = ObjPatientsData.UpdateRegistrationForm(PatientId, "UpdatePatient", SafeValue<string>(Dob), ddlgenderreg,
                        drpStatus, ResidenceStreet, City, State, Zip,
                         ResidenceTelephone, WorkPhone, Fax, Registration);
                }
                return status1;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("DentistProfileBLL--AddInsuranceCoverage", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }

}

