﻿using BO.Models;
using DataAccessLayer;
using DataAccessLayer.Common;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using System.Configuration;
using System;
using BO.Enums;
using System.Collections.Generic;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public class TwilioSMSBLL
    {

        public static bool CostEstimatorSMS(string SendToPhone, decimal Cost, string DentalServices, int UserId)
        {
            clsCommon common = new clsCommon();
            var AccountSid = SafeValue<string>(ConfigurationManager.AppSettings["TwillioAccountSid"]);
            var AuthToken = SafeValue<string>(ConfigurationManager.AppSettings["TwillioAuthToken"]);
            var CountryCode = SafeValue<string>(ConfigurationManager.AppSettings["CountryCode"]);
            var TwillioPhone = SafeValue<string>(ConfigurationManager.AppSettings["TwillioPhone"]);


            string dynamicMessage = "Hello, Your estimated cost for " + DentalServices + "is $" + Cost + ".One of our executive will shortly contact you to book an appointment.";
            //Message message = twilio.SendMessage(TwillioPhone, CountryCode + SendToPhone, dynamicMessage);

            TwilioClient.Init(AccountSid, AuthToken);

            var to = new PhoneNumber(CountryCode + SendToPhone);
            var message = MessageResource.Create(
                to,
                from: new PhoneNumber(TwillioPhone),
                body: dynamicMessage
            );

            InsertSMSLog(CountryCode + SendToPhone, dynamicMessage, Common.EnumCommunication.InsuranceCostEstimatorSMS, message, UserId);

            return !string.IsNullOrWhiteSpace(message.Sid);

        }


        private static void InsertSMSLog(string SendToPhone, string Content, Common.EnumCommunication CallType, MessageResource message, int UserId)
        {
            CallHistoryModel model = null;
            if (!string.IsNullOrWhiteSpace(message.Sid))
            {
                model = new CallHistoryModel()
                {
                    Content = Content,
                    PhoneNumber = SendToPhone,
                    CallType = CallType,
                    STATUS = Common.EnumCallStatus.Success,
                    APIMessage = null,
                    APIStatus = 0,
                    APICode = 0,
                    APIMoreInfo = null,
                    APISId = message.Sid,
                    UserId = UserId
                };
            }
            else
            {
                model = new CallHistoryModel()
                {
                    Content = Content,
                    PhoneNumber = SendToPhone,
                    CallType = Common.EnumCommunication.InsuranceCostEstimatorCall,
                    STATUS = Common.EnumCallStatus.failed,
                    APIMessage = message.ErrorMessage,
                    APIStatus = SafeValue<int>(message.Status),
                    APICode = SafeValue<int>(message.ErrorCode ?? 0),
                    APIMoreInfo = message.Uri,
                    APISId = message.Sid,
                    UserId = UserId
                };
            }
            clsCallHistoryLog.InsertCostEstimatorCallLog(model);
        }
    }
}
