﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Models;
using BO.ViewModel;
using RestSharp;
using System.Net;
using Newtonsoft.Json;
using DataAccessLayer;
using System.Web;
using DataAccessLayer.Common;

namespace BusinessLogicLayer
{
    public class Zipwhip
    {
        /// <summary>
        /// call api
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="model"></param>
        /// <returns></returns>
        public APIResponseBase<T> CallZipwhipApi<T>(ZipwhipModel model) where T : new()
        {
            //try
            //{
            var client = new RestClient(model.zipwhip_url);
            var request = new RestRequest(model.api_method == "get" ? Method.GET : Method.POST);
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            var propertyList = GetNotNullPropertiesFromObject(model.parameter);
            if (model.api_method == "get")
            {
                foreach (var item in propertyList)
                {
                    request.AddParameter(item.property, item.value);
                }

            }
            else
            {
                string param = string.Empty;
                foreach (var item in propertyList)
                {
                    param += item.property + "=" + item.value + "&";
                }
                param = param.Substring(0, param.LastIndexOf("&"));
                request.AddParameter("application/x-www-form-urlencoded", param, ParameterType.RequestBody);
            }

            IRestResponse<T> response = client.Execute<T>(request);
            return new APIResponseBase<T>()
            {
                StatusCode = (int)response.StatusCode,
                IsSuccess = response.StatusCode == HttpStatusCode.OK,
                Result = JsonConvert.DeserializeObject<T>(response.Content),
                Message = response.ErrorMessage
            };
            //}
            //catch (Exception ex)
            //{
            //    return new APIResponseBase<T>()
            //    {
            //        StatusCode = 500,
            //        IsSuccess = false,
            //       // Result = null,
            //        Message = "Some internal error occurred"
            //    };
            //}
        }

        /// <summary>
        /// Remove null value property in a model
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public dynamic GetNotNullPropertiesFromObject(object obj)
        {
            var result = obj.GetType().GetProperties()
                    .Select(x => new { property = x.Name, value = x.GetValue(obj, null) })
                    .Where(x => x.value != null)
                    .ToList();

            return result;
        }

        /// <summary>
        /// Assign zipwhip token 
        /// </summary>
        public static bool AssignZipwhipToken()
        {
            ZipwhipBLL bll = new ZipwhipBLL();
            ZipwhipDAL dal = new ZipwhipDAL();
            List<ZipwhipAccount> objList = bll.AssignZipwhipToken();

            if (objList.Count() > 0)
            {
                foreach (var data in objList)
                {
                    try
                    {
                        #region Call Zipwhip Login Services
                        string username = data.ProviderConfig.username;
                        string password = data.ProviderConfig.password;

                        if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                            continue;

                        ZipwhipModel callapi = new ZipwhipModel();
                        callapi.zipwhip_url = "https://api.zipwhip.com/user/login";
                        callapi.api_method = "post";
                        callapi.api_key = data.ProviderConfig.api_key;
                        callapi.parameter.username = username;
                        callapi.parameter.password = password;

                        UserLogin result = new Zipwhip().CallZipwhipApi<UserLogin>(callapi).Result;
                        if (result.success == true)
                        {
                            string token = string.Empty;
                            if (!string.IsNullOrEmpty(result.response))
                            {
                                token = result.response;
                            }

                            if (!string.IsNullOrEmpty(token))
                            {
                                ProviderConfig modal = JsonConvert.DeserializeObject<ProviderConfig>(data.ProviderConfigJson);
                                modal.session = token;

                                try
                                {
                                    ZipwhipModel addwebhook = new ZipwhipModel();
                                    addwebhook.zipwhip_url = "https://api.zipwhip.com/webhook/add";
                                    addwebhook.api_method = "post";
                                    addwebhook.api_key = data.ProviderConfig.api_key;
                                    addwebhook.parameter.session = token;
                                    addwebhook.parameter.@event = "receive";
                                    addwebhook.parameter.url = ConfigurationManager.AppSettings["ZipwhipWebhookUrl"].ToString();

                                    Webhook webhookRspn = new Zipwhip().CallZipwhipApi<Webhook>(addwebhook).Result;
                                    modal.webhookId = webhookRspn.response[0].webhookId;

                                }
                                catch (Exception ex)
                                {
                                    new clsCommon().InsertErrorLog("AddWebhook(/BusinessLogicLayer/zipwhip.cs/)--AssignZipwhipToken", ex.Message, ex.StackTrace);
                                }
                                string modalJson = JsonConvert.SerializeObject(modal);
                                dal.UpdateZipwhipToken(modalJson, data.RefId);
                            }
                        }
                        #endregion
                    }
                    catch (Exception ex)
                    {
                        new clsCommon().InsertErrorLog("AddToken(/BusinessLogicLayer/zipwhip.cs/)--AssignZipwhipToken", ex.Message, ex.StackTrace);
                    }
                }

            }

            return true;
        }


    }
}
