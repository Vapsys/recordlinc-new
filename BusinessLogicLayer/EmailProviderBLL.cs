﻿using BO.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.EmailProviderDAL;
using DataAccessLayer.Common;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public class EmailProviderBLL
    {
        public static clsCommon objclsCommon = new clsCommon();
                
        public static List<EmailProvider> GetAllEmailProvider()
        {
            DataTable dt = new DataTable();
            try
            {
                List<EmailProvider> lst = new List<EmailProvider>();
                dt = EmailProviderDAL.GetAllEmailProviderList();
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        lst.Add(new EmailProvider
                        {
                            providerId = SafeValue<int>(dt.Rows[i]["Provider_Id"]),
                            providerName = SafeValue<string>(dt.Rows[i]["Provider_Name"]),
                            createdAt = SafeValue<DateTime>(dt.Rows[i]["Created_At"]),                            
                            Is_Deleted = SafeValue<bool>(dt.Rows[i]["Is_Deleted"]),
                        });
                    }
                    return lst;
                }
                else
                {
                    return lst;
                }                
            }
            catch (Exception ex)
            {
                throw;
            }                        
        }

        public static bool InsertEmailProviderDetails(EmailProviderDetails _emailProviderDetails)
        {            
            try
            {
                return EmailProviderDAL.InsertEmailProviderDetails(_emailProviderDetails);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static bool DeleteEmailProviderDetailsById(int id, int userid)
        {
            try
            {
                return EmailProviderDAL.DeleteEmailProviderDetailsById(id, userid);                
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static bool UpdateEmailProviderDetails(EmailProviderDetails _epd)
        {
            try
            {
                return EmailProviderDAL.UpdateEmailProviderDetails(_epd);
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static bool UpdateEmailProviderDetailsForDefault(string epdId, string userId)
        {
            try
            {
                return EmailProviderDAL.UpdateEmailProviderDetailsForDefault(epdId, userId);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static List<EmailProviderDetails> GetAllEmailProviderDetails(int userId)
        {
            DataTable dt = new DataTable();
            try
            {
                List<EmailProviderDetails> lstEPD = new List<EmailProviderDetails>();
                dt = EmailProviderDAL.GetAllEmailProviderDetailsList(userId);

                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        lstEPD.Add(new EmailProviderDetails
                        {
                            providerDetailsId = SafeValue<int>(dt.Rows[i]["Epd_Id"]),
                            providerId = SafeValue<int>(dt.Rows[i]["Provider_Id"]),
                            accessToken = SafeValue<string>(dt.Rows[i]["Accesstoken"]),
                            emailAddress = SafeValue<string>(dt.Rows[i]["EmailAddress"]),
                            isDefault = SafeValue<bool>(dt.Rows[i]["IsDefault"]),
                            memberId = SafeValue<int>(dt.Rows[i]["MemberId"]),
                            password = SafeValue<string>(dt.Rows[i]["Password"]),
                            refreshToken = SafeValue<string>(dt.Rows[i]["Refreshtoken"]),
                            requireSSL = SafeValue<bool>(dt.Rows[i]["RequiresSSL"]),
                            sendEnabled = SafeValue<bool>(dt.Rows[i]["SendEnabled"]),
                            smtpPortNumber = SafeValue<int>(dt.Rows[i]["SmtpPortNumber"]),
                            smtpServerName = SafeValue<string>(dt.Rows[i]["SmtpServerName"]),                            
                            createdAt = SafeValue<DateTime>(dt.Rows[i]["Created_At"]),
                            syncEnabled = SafeValue<bool>(dt.Rows[i]["SyncEnabled"])
                        });
                    }
                    return lstEPD;
                }
                else
                {
                    return lstEPD;
                }               
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public static EmailProviderDetails GetAllEmailProviderDetailsById(string _epdId, string UserId)
        {
            DataTable dt = new DataTable();
            try
            {
                dt = EmailProviderDAL.GetAllEmailProviderDetailsById(SafeValue<int>(_epdId), SafeValue<int>(UserId));
                EmailProviderDetails objEPD = new EmailProviderDetails();
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objEPD.providerDetailsId = SafeValue<int>(dt.Rows[i]["Epd_Id"]);
                        objEPD.providerId = SafeValue<int>(dt.Rows[i]["Provider_Id"]);
                        objEPD.accessToken = SafeValue<string>(dt.Rows[i]["Accesstoken"]);
                        objEPD.emailAddress = SafeValue<string>(dt.Rows[i]["EmailAddress"]);
                        objEPD.isDefault = SafeValue<bool>(dt.Rows[i]["IsDefault"]);
                        objEPD.memberId = SafeValue<int>(dt.Rows[i]["MemberId"]);
                        objEPD.password = SafeValue<string>(dt.Rows[i]["Password"]);
                        objEPD.refreshToken = SafeValue<string>(dt.Rows[i]["Refreshtoken"]);
                        objEPD.requireSSL = SafeValue<bool>(dt.Rows[i]["RequiresSSL"]);
                        objEPD.sendEnabled = SafeValue<bool>(dt.Rows[i]["SendEnabled"]);
                        objEPD.smtpPortNumber = SafeValue<int>(dt.Rows[i]["SmtpPortNumber"]);
                        objEPD.smtpServerName = SafeValue<string>(dt.Rows[i]["SmtpServerName"]);
                        objEPD.createdAt = SafeValue<DateTime>(dt.Rows[i]["Created_At"]);
                        objEPD.syncEnabled = SafeValue<bool>(dt.Rows[i]["SyncEnabled"]);
                    }
                }
                return objEPD;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static bool CheckEmailAlreadyExists(string user, int pid, string userid)
        {
            try
            {
                return EmailProviderDAL.CheckEmailAlreadyExists(user, pid, userid);
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
