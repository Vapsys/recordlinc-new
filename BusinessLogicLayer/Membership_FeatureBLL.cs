﻿using BO.Models;
using BO.ViewModel;
using DataAccessLayer.Admin;
using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public class Membership_FeatureBLL
    {
        clsCommon ObjCommonData = new clsCommon();
        Membership_FeatureDLL ObjMemberShip = new Membership_FeatureDLL();
        clsRestriction objRestriction = new clsRestriction();
        public List<Membership> GetMemberShipDetails(int ID)
        {
            try
            {
                DataTable ds = new DataTable();
                ObjMemberShip = new Membership_FeatureDLL();
                List<Membership> LstMember = new List<Membership>();
                ds = ObjMemberShip.GetMembershipDetails(ID);
                if (ds != null && ds.Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Rows)
                    {
                        LstMember.Add(new Membership
                        {
                            Membership_Id = SafeValue<int>(item["Membership_Id"]),
                            Title = SafeValue<string>(item["Title"]),
                            Description = SafeValue<string>(item["Description"]),
                            SortOrder = SafeValue<int>(item["SortOrder"]),
                            MonthlyPrice = SafeValue<int>(item["MonthlyPrice"]),
                            AnnualPrice = SafeValue<int>(item["AnnualPrice"]),
                            QuaterlyPrice = SafeValue<int>(item["QuaterlyPrice"]),
                            HalfYearlyPrice = SafeValue<int>(item["HalfYearlyPrice"]),
                        });

                    }
                }
                return LstMember;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("Membership_FeatureBLL--GetMemberShipFeathersDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public List<Features> GetFeaturesDetails(int Id)
        {
            try
            {
                DataTable dt = new DataTable();
                ObjMemberShip = new Membership_FeatureDLL();
                List<Features> lstFeatures = new List<Features>();
                dt = ObjMemberShip.GetFeaturesDetails(Id);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lstFeatures.Add(new Features()
                        {
                            Feature_Id = SafeValue<int>(item["Feature_Id"]),
                            Title = SafeValue<string>(item["Title"]),
                            SortOrder = SafeValue<int>(item["SortOrder"]),
                            Status = SafeValue<int>(item["Status"]),
                        });

                    }
                }
                return lstFeatures;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("Membership_FeatureBLL--GetFeaturesDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public List<Features> GetFeaturesDetailsByMF(int Id)
        {
            try
            {
                DataTable dt = new DataTable();
                ObjMemberShip = new Membership_FeatureDLL();
                List<Features> lstFeatures = new List<Features>();
                dt = ObjMemberShip.GetFeaturesDetails(Id);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lstFeatures.Add(new Features()
                        {
                            Feature_Id = SafeValue<int>(item["Feature_Id"]),
                            Title = SafeValue<string>(item["Title"]),
                            CanSee = false,
                            MaxCount = "",
                        });

                    }
                }
                return lstFeatures;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("Membership_FeatureBLL--GetFeaturesDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool InsertUpdateMembershipfeaturebll(MembershipFeatures ObjMembershipFeatures)
        {
            bool result = false;
            bool results = false;
            try
            {
                Membership_FeatureDLL objMemFeature = new Membership_FeatureDLL();
                results = objMemFeature.DeleteExistMembershipdata(ObjMembershipFeatures.MembershipId);
                string FeatureIds = ObjMembershipFeatures.FeatureId.TrimEnd(',');
                string[] splits = FeatureIds.Split(',');
                foreach (var item in splits)
                {
                    string[] separate = item.Split('_');
                    int FeatureId = SafeValue<int>(separate[0]);
                    string MaxCount = string.Empty;
                    if (!string.IsNullOrWhiteSpace(separate[1]))
                    {
                        MaxCount = SafeValue<string>(separate[1]);
                    }
                    result = objMemFeature.InsertUpdateMemberShipFeatures(ObjMembershipFeatures.MembershipFeaturesId, FeatureId, ObjMembershipFeatures.MembershipId, true, MaxCount, ObjMembershipFeatures.CreatedBy);
                }
                if (results)
                    return false;
                else
                    return result;                   

            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("Membership_FeatureBLL--InsertUpdateMembershipfeaturebll", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public List<MembershipFeatures> GetDetialsofMembershipFeature()
        {
            List<MembershipFeatures> lstMemberlist = new List<MembershipFeatures>();
            try
            {
                DataTable dt = new DataTable();
                dt = ObjMemberShip.GetMemberShipANDFeaturesDetails();
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lstMemberlist.Add(new MembershipFeatures()
                        {
                            MembershipId = SafeValue<int>(item["Membership_Id"]),
                            MembershipFeaturesId = SafeValue<int>(item["Id"]),
                            FeatureId = SafeValue<string>(item["Feature_Id"]),
                            MembershipName = SafeValue<string>(item["MembershipName"]),
                            FeatureName = SafeValue<string>(item["FeaturesName"]),
                        });
                    }
                }
                return lstMemberlist;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("Membership_FeatureBLL--GetDetialsofMembershipFeature", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool DeleteMembershipfeaturesById(int id)
        {
            bool Result = false;
            try
            {
                return Result = ObjMemberShip.DeleteMembershipFeaturesDelete(id);
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("Membership_FeatureBLL--DeleteMembershipfeaturesById", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public MembershipFeatures GetMembershipFeaturesDetails(int Id)
        {
            MembershipFeatures ObjMemberShipFeatures = new MembershipFeatures();
            List<Features> lstFeaturelist = new List<Features>();
            List<Features> NewlstFeaturelist = new List<Features>();
            try
            {
                DataTable Dt = new DataTable();
                Dt = ObjMemberShip.GetMembershipFeatures(Id);
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    foreach (DataRow item in Dt.Rows)
                    {
                        ObjMemberShipFeatures.MembershipFeaturesId = SafeValue<int>(item["Id"]);
                        ObjMemberShipFeatures.MembershipId = SafeValue<int>(item["MembershipId"]);
                        ObjMemberShipFeatures.MembershipName = SafeValue<string>(item["Title"]);
                        ObjMemberShipFeatures.FeatureId = SafeValue<string>(item["Feature_Id"]);
                        ObjMemberShipFeatures.FeatureName = SafeValue<string>(item["Title1"]);
                        ObjMemberShipFeatures.MaxCount = SafeValue<string>(item["MaxCount"]);
                        ObjMemberShipFeatures.cansee = SafeValue<bool>(item["cansee"]);

                        NewlstFeaturelist.Add(new Features()
                        {
                            Feature_Id = SafeValue<int>(item["Feature_Id"]),
                            Title = SafeValue<string>(item["Title1"]),
                            CanSee = SafeValue<bool>(item["cansee"]),
                            MaxCount = SafeValue<string>(item["MaxCount"])
                        });
                       
                    }
                    ObjMemberShipFeatures.MembershipName = SafeValue<string>(Dt.Rows[0]["Title"]);
                    ObjMemberShipFeatures.MemberShip.Add(new Membership()
                    {
                        Membership_Id = SafeValue<int>(Dt.Rows[0]["MembershipId"]),
                        Title = SafeValue<string>(Dt.Rows[0]["Title"]),
                    });
                    ObjMemberShipFeatures.MemberShip.Take(0);
                    
                    lstFeaturelist = GetFeaturesDetailsByMF(0);
                    if (NewlstFeaturelist.Count > 0)
                    {
                        List<Features> distinctItems = NewlstFeaturelist.ToList().Union(lstFeaturelist.ToList()).ToList();
                        var combined = distinctItems.GroupBy(x => x.Feature_Id).Select(y => y.First());
                        ObjMemberShipFeatures.lstFeatures = combined.ToList();
                    }
                    else
                    {
                        ObjMemberShipFeatures.lstFeatures = GetFeaturesDetailsByMF(0);
                    }
                    ObjMemberShipFeatures.MemberShip.Take(0);
                    return ObjMemberShipFeatures;
                }
                else
                {
                    ObjMemberShipFeatures.MemberShip = GetMemberShipDetails(Id);
                    var obj = ObjMemberShipFeatures.MemberShip.FirstOrDefault();
                    ObjMemberShipFeatures.MembershipName = SafeValue<string>(obj.Title);
                    ObjMemberShipFeatures.MembershipId = obj.Membership_Id;
                    ObjMemberShipFeatures.lstFeatures = GetFeaturesDetailsByMF(0);
                    return ObjMemberShipFeatures;
                }
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("Membership_FeatureBLL--GetMembershipFeaturesDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool UpdateMemberMemberShip(string[] UserId, int MembershipId, string OldValue, string NewValue, string MembershipName, int CreatedBy)
        {
            try
            {
                bool Result = false;
                if (UserId != null)
                {
                    foreach (var item in UserId)
                    {
                        Result = ObjMemberShip.UpdateMemberMembership(SafeValue<int>(item), MembershipId);
                        Membership_History Obj = new Membership_History();
                        Obj.UserId = SafeValue<int>(item);
                        Obj.OldValues = OldValue;
                        Obj.NewValues = NewValue;
                        Obj.MembershipName = MembershipName;
                        Obj.CreatedBy = CreatedBy;
                        Result = InsertMembershipHistory(Obj);
                    }
                }
                return Result;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("Membership_FeatureBLL--UpdateMemberMemberShip", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool InsertMembershipHistory(Membership_History ObjMemberhistory)
        {
            try
            {
                bool Result = false;
                return Result = ObjMemberShip.InsertMembershipHistoryofUser(ObjMemberhistory);
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("Membership_FeatureBLL--InsertMembershipHistory", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public List<Membership_History> GetMemberShipHistory(int UserId)
        {
            try
            {
                List<Membership_History> Lst = new List<Membership_History>();
                DataTable dt = new DataTable();
                dt = ObjMemberShip.GetMemberShipHistoryOfUser(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        Lst.Add(new Membership_History()
                        {
                            UserId = SafeValue<int>(item["UserId"]),
                            OldValues = SafeValue<string>(item["OldValue"]),
                            NewValues = SafeValue<string>(item["NewValues"]),
                            MembershipName = SafeValue<string>(item["FieldName"]),
                            CreatedBy = SafeValue<int>(item["CreatedBy"]),
                            UserName = SafeValue<string>(item["FirstName"]) + " " + SafeValue<string>(item["LastName"]),
                            CreatedDate = SafeValue<string>(item["CreatedDate"]),
                            CreatedUserName = SafeValue<string>(item["CreatedByName"])
                        });
                    }
                }
                return Lst;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("Membership_FeatureBLL--InsertMembershipHistory", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public List<Membership> GetMembershipvaluebyPrice(int MonthlyPrice)
        {
            try
            {
                DataTable dt = new DataTable();
                List<Membership> lstmember = new List<Membership>();
                dt = ObjMemberShip.GetMembershipvaluebyPrice(MonthlyPrice);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lstmember.Add(new Membership()
                        {
                            Membership_Id = SafeValue<int>(item["Membership_Id"]),
                            Title = SafeValue<string>(item["Title"]),
                            Description = SafeValue<string>(item["Description"]),
                            SortOrder = SafeValue<int>(item["SortOrder"]),
                            MonthlyPrice = SafeValue<int>(item["MonthlyPrice"]),
                            AnnualPrice = SafeValue<int>(item["AnnualPrice"]),
                            QuaterlyPrice = SafeValue<int>(item["QuaterlyPrice"]),
                            HalfYearlyPrice = SafeValue<int>(item["HalfYearlyPrice"]),
                        });
                    }
                }
                return lstmember;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("Membership_FeatureBLL--GetMembershipvaluebyPrice", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public List<Membership> GetMembershipPlan()
        {
            try
            {
                DataTable dt = new DataTable();
                List<Membership> lstmember = new List<Membership>();

                MemberShipBLL objShip = new MemberShipBLL();
                lstmember = objShip.GetAllMembership(1, 50, 2, 1, null).Where(t => t.MonthlyPrice != SafeValue<decimal>("0.00")).ToList();
                
                ////dt = ObjMemberShip.GetUpgreadeMembership(MembershipId);
                //if (dt != null && dt.Rows.Count > 0)
                //{
                //    foreach (DataRow item in dt.Rows)
                //    {
                //        lstmember.Add(new Membership()
                //        {
                //            Membership_Id = SafeValue<int>(item["Membership_Id"]),
                //            Title = SafeValue<string>(item["Title"]),
                //            Description = SafeValue<string>(item["Description"]),
                //            SortOrder = SafeValue<int>(item["SortOrder"]),
                //            MonthlyPrice = SafeValue<int>(item["MonthlyPrice"]),
                //            AnnualPrice = SafeValue<int>(item["AnnualPrice"]),
                //            QuaterlyPrice = SafeValue<int>(item["QuaterlyPrice"]),
                //            HalfYearlyPrice = SafeValue<int>(item["HalfYearlyPrice"]),
                //        });
                //    }
                //}
                return lstmember;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }

        //public bool IsColleaguesLimit(int UserId, int FeaturesId, List<MemberPlanDetail> objplandetail)
        //{
        //    try
        //    {
        //        MemberPlanDetail Obj = new MemberPlanDetail();
        //        int Count = objRestriction.GetCountOfColleaguesByUserId(UserId);
        //        Obj = objplandetail.Where(p => p.FeatureId == FeaturesId).FirstOrDefault();
        //        if (Obj.MaxCount > Count)
        //            return true;
        //        else if (Obj.MaxCount == 0)
        //            return true;
        //        else
        //            return false;
        //    }
        //    catch (Exception Ex)
        //    {
        //        ObjCommonData.InsertErrorLog("Membership_FeatureBLL--IsColleaguesLimite", Ex.Message, Ex.StackTrace);
        //        throw;
        //    }
        //}

       

        //public bool IsPatientLimit(int UserId, int FeaturesId, List<MemberPlanDetail> objplandetail)
        //{
        //    try
        //    {
        //        MemberPlanDetail Obj = new MemberPlanDetail();
        //        int Count = objRestriction.GetCountOfPatientByUserId(UserId);
        //        Obj = objplandetail.Where(p => p.FeatureId == FeaturesId).FirstOrDefault();
        //        if (Obj.MaxCount > Count)
        //            return true;
        //        else if (Obj.MaxCount == 0)
        //            return true;
        //        else
        //            return false;
        //    }
        //    catch (Exception Ex)
        //    {
        //        ObjCommonData.InsertErrorLog("Membership_FeatureBLL--IsColleaguesLimite", Ex.Message, Ex.StackTrace);
        //        throw;
        //    }
        //}
    }
}
