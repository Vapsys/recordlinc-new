﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Models;
using System.Data;
using DataAccessLayer.ColleaguesData;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public class ProcedureBLL
    {
        public static List<Procedure> GetProcedureList(int ProcedureId = 0,int UserId=0)
        {
            try
            {
                DataTable dt = new DataTable();
                List<Procedure> lst = new List<Procedure>();
                clsColleaguesData clsColleageData = new clsColleaguesData();
                dt = clsColleaguesData.GetProcedures(ProcedureId,UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lst.Add(new Procedure()
                        {
                            ProcedureId = SafeValue<int>(item["ProcedureId"]),
                            ProcedureName = SafeValue<string>(item["ProcedureName"]),
                            StandardFees = SafeValue<decimal>(item["StandardRate"]),
                            CostPercentage = SafeValue<decimal>(item["InsuranceCostRatio"]),
                            
                        });
                    }
                }
                return lst;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public static bool CheckProcedureAlreadyExistornot(Procedure Obj)
        {
            return clsColleaguesData.CheckProcedureAlreadyExist(Obj);
        }
        public static bool InsertProcedure(Procedure Obj)
        {
            return clsColleaguesData.InsertProcedure(Obj);
        }
        public static bool DeleteProcedure(Procedure Obj,int UserId=0)
        {
            return clsColleaguesData.DeleteProcedure(Obj,UserId);
        }
        public static bool InsertProcedureForUser(Procedure Obj,int UserId)
        {
            return clsColleaguesData.InsertProcedureForUser(Obj, UserId);
        }
        public static bool InsertUpdateProcedureofUser(List<Procedure> lst,int UserId)
        {
            if(lst.Count > 0)
            {
                foreach (var item in lst)
                {
                    if (item.Ischeck)
                    {
                        clsColleaguesData.InsertUpdateUserProcedureDetails(item, UserId);
                    }
                    else
                    {
                        DeleteProcedure(item, UserId);
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool UpdateProcedureDetails(Procedure Obj)
        {
            return clsColleaguesData.UpdateProcedureDetails(Obj);
        }
    }
}
