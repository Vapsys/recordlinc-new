﻿using BO.Models;
using BO.ViewModel;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using static DataAccessLayer.Common.clsCommon;

namespace BusinessLogicLayer
{
    public class ChromeExtentionBLL
    {
        /// <summary>
        /// Used for Get Dentist details by UserId for Chrome Extension
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static DentistDetail DentistDetailsByUserId(int UserId)
        {
            try
            {
                DentistDetail Obj = new DentistDetail();
                DataTable dtLogin = GetDentistDetailsByUserId(SafeValue<int>(UserId));
                if (dtLogin != null && dtLogin.Rows.Count > 0)
                {
                    Obj.UserId = SafeValue<int>(dtLogin.Rows[0]["UserId"]);
                    Obj.FirstName = SafeValue<string>(dtLogin.Rows[0]["FirstName"]);
                    Obj.LastName = SafeValue<string>(dtLogin.Rows[0]["LastName"]);
                    string ImageName = SafeValue<string>(dtLogin.Rows[0]["ImageName"]);
                    if (string.IsNullOrEmpty(ImageName))
                    {
                        Obj.ImageName =SafeValue<string>(ConfigurationManager.AppSettings["DefaultDoctorImage"]);
                    }
                    else
                    {
                        Obj.ImageName = SafeValue<string>(ConfigurationManager.AppSettings["DoctorImage"]) + ImageName;
                    }
                    Obj.ImageWithPath = Obj.ImageName;
                    DataTable dataTable = clsColleaguesData.CheckUserHasEnableOneClick(SafeValue<int>(dtLogin.Rows[0]["UserId"]));
                    if (dataTable != null && dataTable.Rows.Count > 0)
                    {
                        Obj.IsReceiveReferral = SafeValue<bool>(dataTable.Rows[0]["IsReceiveReferral"]);
                        Obj.IsSendReferral = SafeValue<bool>(dataTable.Rows[0]["IsSendReferral"]);
                    }
                }
                return Obj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ChromeExtentionBLL---DentistDetailsByUserId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Get Un-Read Referral Count for Chrome Extension
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static int GetUnReadReferralCount(FilterMessageConversation Filter,int UserId)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(Filter.TimeZoneSystemName))
                    Filter.TimeZoneSystemName = clsColleaguesData.GetTimeZoneOfDoctor(UserId);

                if (Filter.FromDate != null)
                {
                    Filter.FromDate = clsHelper.ConvertToUTC(SafeValue<DateTime>(Filter.FromDate), Filter.TimeZoneSystemName);
                }
                if (Filter.Todate != null)
                {
                    Filter.Todate = clsHelper.ConvertToUTC(SafeValue<DateTime>(Filter.Todate), Filter.TimeZoneSystemName);
                }
                int Count = 0;
                return Count;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ChromeExtentionBLL---GetUnReadReferralCount", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
