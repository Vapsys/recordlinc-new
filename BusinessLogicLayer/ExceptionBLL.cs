﻿using BO.Models;
using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class ExceptionBLL
    {
        public static bool Insert(ExceptionRequestModel Ex)
        {
            return new clsCommon().InsertErrorLog(Ex.URL, Ex.Message, Ex.StackTrace);
        }
    }
}
