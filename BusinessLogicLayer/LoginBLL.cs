﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.ViewModel;
using System.Data;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using System.Configuration;
using BO.Models;
using static DataAccessLayer.Common.clsCommon;
using System.IO;

namespace BusinessLogicLayer
{
    public class LoginBLL
    {
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        clsCommon objCommon = new clsCommon();
        clsTemplate ObjTemplate = new clsTemplate();
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        clsAdmin ObjAdmin = new clsAdmin();
        public IDictionary<bool, string> LoginonRecordlinc(PatientLogin Obj)
        {
            var result = new Dictionary<bool, string>();
            string UserName = string.Empty;
            string Password = string.Empty;
            if (Obj.Email != null && Obj.Email != "")
            {
                int valid = 0; int AttemptsCount = 0; int UserId = 0;
                UserName = SafeValue<string>(Obj.Email).ToLower();
                if (Obj.Password != null && Obj.Password != "")
                {
                    Password = SafeValue<string>(Obj.Password);
                }
                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Password))
                {
                    DataTable dtLogin = new DataTable();
                
                    TripleDESCryptoHelper cryptoHelper = new TripleDESCryptoHelper();
                    string encryptedPassword = cryptoHelper.encryptText(Password);
                    DataTable dtCheck = ObjColleaguesData.CheckEmailInSystem(UserName);
                    if (dtCheck != null && dtCheck.Rows.Count > 0)
                    {
                        int UserStatus = SafeValue<int>(dtCheck.Rows[0]["Status"]);
                        int IsInSystem = SafeValue<int>(dtCheck.Rows[0]["IsInSystem"]);
                        if (IsInSystem == 1)
                        {
                            int ValidUserId = SafeValue<int>(dtCheck.Rows[0]["UserId"]);
                            dtLogin = ObjColleaguesData.LoginUserBasedOnUsernameandPassword(UserName, encryptedPassword);
                            if (UserName == "admin")
                            {
                                if (dtLogin != null && dtLogin.Rows.Count > 0)
                                {
                                    //Redirect to admin module
                                    result.Add(false, "Admin Account");
                                    return result;
                                }
                            }
                            else
                            {
                                if (UserStatus == 1)
                                {
                                    if (AttemptsCount < 5)
                                    {
                                        #region Login As Doctor
                                        if (dtLogin != null && dtLogin.Rows.Count > 0)
                                        {
                                            valid = SafeValue<int>(dtLogin.Rows[0]["UserId"]);
                                            bool Result = false;
                                            Result = objCommon.InsertLogedInfo(SafeValue<int>(dtLogin.Rows[0]["UserId"]));
                                            result.Add(true, string.Empty);
                                            return result;
                                        }
                                        else
                                        {
                                            int CheckAttemps = 1;
                                            if (UserId == ValidUserId)
                                            {
                                                CheckAttemps = (AttemptsCount + 1);
                                            }
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        result.Add(false, "Your account has been blocked by Admin");
                                        return result;
                                    }
                                }
                                if (UserStatus == 2)
                                {
                                    result.Add(false, "Invalid Username OR Password");
                                    return result;
                                }
                                if (UserStatus == 3)
                                {
                                    result.Add(false, "Your account has been blocked by Admin");
                                    return result;
                                    //TempData["IsValid"] = "InActive";
                                    //TempData["InActiveMsg"] = "Your account has been blocked by Admin";
                                    //return RedirectToAction("Index", "Login");
                                }
                            }
                        }
                    }
                }
                result.Add(false, "Invalid User name and Password");
                return result;
            }
            else
            {
                result.Add(false, "Invalid User name and Password");
                return result;
            }
        }

        public IDictionary<bool, string> GetAuthenticationTocken(PatientLogin login)
        {
            string GUId = Guid.NewGuid().ToString();
            clsColleaguesData dalColleages = new clsColleaguesData();
            IDictionary<bool, string> returnValue = new Dictionary<bool, string>();
            int UserId = 0;
            login.Password = new TripleDESCryptoHelper().encryptText(login.Password);
            DataTable dt = dalColleages.LoginUserBasedOnUsernameandPassword(login.Email, login.Password);
            if (dt != null && dt.Rows.Count > 0)
            {
                UserId = SafeValue<int>(dt.Rows[0]["UserId"]);
                //DataTable dtToken = new DataTable();
                //dtToken = dalColleages.GetAccessTokenForPMS(UserId);

                //if (dtToken != null && dtToken.Rows.Count > 0)
                //{
                //    GUId = SafeValue<string>(dtToken.Rows[0]["AccessToken"]);
                //    returnValue.Add(true, GUId);
                //}
                //else
                //{
                    if (dalColleages.CreateAccessTokenForPMS(UserId, GUId))
                    {
                        returnValue.Add(true, GUId);
                    }
                    else
                    {
                        returnValue.Add(false, "Unexpected internal server occurred to get Access Token.");
                    }
               // }
            }
            else
            {
                returnValue.Add(false, "Invalid Credentials");
            }
            return returnValue;
        }
        public IDictionary<bool, string> ValidateToken(string Token)
        {
            string GUId = string.Empty;
            clsColleaguesData dalColleages = new clsColleaguesData();
            DataTable dt = new DataTable();
            dt = dalColleages.ValidateAccessTokenForPMS(Token);
            if (dt != null && dt.Rows.Count > 0)
            {
                GUId = Token;
            }
            string msg = string.Empty;
            msg = GUId == Token ? string.Empty : "Invalid Token";
            IDictionary<bool, string> returnValue = new Dictionary<bool, string>();
            returnValue.Add(GUId == Token, msg);
            return returnValue;
        }
        public DoctorLoginForPMS GetUserCredentials(string Token)
        {
            clsColleaguesData dalColleages = new clsColleaguesData();
            DoctorLoginForPMS ReturnValue;
            string GUId = string.Empty;
            IDictionary<bool, string> response = ValidateToken(Token);
            if (response.Keys.FirstOrDefault())
            {
                DataTable dtToken = dalColleages.GetUserCredentilasBasedOnAccessTokenForPMS(Token);
                if (dtToken != null && dtToken.Rows.Count > 0)
                {
                    ReturnValue = new DoctorLoginForPMS()
                    {
                        Email = SafeValue<string>(dtToken.Rows[0]["Username"]),
                        Password = SafeValue<string>(dtToken.Rows[0]["Password"]),
                        UserId = SafeValue<int>(dtToken.Rows[0]["UserId"])
                    };
                }
                else
                {
                    ReturnValue = null;
                }
            }
            else
            {
                ReturnValue = null;
            }

            return ReturnValue;
        }

        public IDictionary<bool, string> SendForgetPassword(string username)
        {
            var result = new Dictionary<bool, string>();
            object obj = string.Empty; string FullName = string.Empty; string EncPassword = string.Empty;
            try
            {
                DataTable dtCheck = ObjColleaguesData.CheckEmailInSystem(username);// Check in system email is there or not
                if (dtCheck.Rows.Count > 0)
                {
                    string companywebsite = SafeValue<string>("");
                    if (SafeValue<int>(dtCheck.Rows[0]["IsInSystem"]) == 1 && SafeValue<int>(dtCheck.Rows[0]["Status"]) == 1)
                    {
                        FullName = SafeValue<string>(dtCheck.Rows[0]["FirstName"]) + " " + SafeValue<string>(dtCheck.Rows[0]["LastName"]);
                        EncPassword = SafeValue<string>(dtCheck.Rows[0]["Password"]);
                        DataTable dtCheckFirst = new DataTable();
                        dtCheckFirst = ObjColleaguesData.GetFirstLoginCount(SafeValue<int>(dtCheck.Rows[0]["UserId"]));
                        if (dtCheckFirst.Rows.Count == 0)
                        {
                            ObjTemplate.NewUserEmailFormat(username, companywebsite + "/User/Index?UserName=" + username + "&Password=" + EncPassword, ObjTripleDESCryptoHelper.decryptText(EncPassword), companywebsite);
                        }
                        else
                        {
                            ObjTemplate.SendingForgetPassword(FullName, username, EncPassword);
                        }

                        obj = "Password reset link has been sent to your email address.";
                        result.Add(true, SafeValue<string>(obj));

                    }
                    else if (SafeValue<int>(dtCheck.Rows[0]["Status"]) == 2)
                    {
                        FullName =SafeValue<string>(dtCheck.Rows[0]["FirstName"]) + " " +SafeValue<string>(dtCheck.Rows[0]["LastName"]);
                        EncPassword =SafeValue<string>(dtCheck.Rows[0]["Password"]);
                        ObjTemplate.NewUserEmailFormat(username, companywebsite + "/User/Index?UserName=" + username + "&Password=" + EncPassword + "&Status=2", ObjTripleDESCryptoHelper.decryptText(EncPassword), companywebsite);
                        obj = "2";
                    }
                    else if (SafeValue<int>(dtCheck.Rows[0]["Status"]) == 3)
                    {
                        obj = "3";
                    }
                    else
                    {
                        #region Re Send sign up tempalte to user of temp table
                        DataTable dttemp = new DataTable();
                        string EncPasswordTemp = string.Empty;

                        dttemp = ObjAdmin.GetTempSignupByEmail(username);
                        if (dttemp.Rows.Count > 0)
                        {
                            EncPasswordTemp = SafeValue<string>(dttemp.Rows[0]["password"]);
                            ObjTemplate.NewUserEmailFormat(username, companywebsite + "/User/Index?UserName=" + username + "&Password=" + EncPasswordTemp, ObjTripleDESCryptoHelper.decryptText(EncPasswordTemp), companywebsite); // send tempate to admin if doctor sing up in system
                        }
                        #endregion
                        obj = "Password reset link has been sent to your email address.";
                        result.Add(false, SafeValue<string>(obj));
                    }
                }
                else
                {
                    obj = "This Email Address Does Not Exist In Our System.";
                    result.Add(false, SafeValue<string>(obj));
                }
                return result;
            }
            catch (Exception)
            {
                throw;
            }            
        }
        public static List<DentrixConnectors> GetUserDentrixConnector(string UserName)
        {
            DataTable dt = new DataTable();
            int UserId = 0;
            try
            {
                List<DentrixConnectors> lst = new List<DentrixConnectors>();
                DataTable dts = new DataTable();
                dts = new clsColleaguesData().GetDoctorDetailsByUsername(UserName);
                
                if(dts != null && dts.Rows.Count > 0)
                {
                    UserId = SafeValue<int>(dts.Rows[0]["UserId"]);
                }
                dt = new clsColleaguesData().GetDentrixConnectorValuesofUser(UserId);
                if(dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lst.Add(new DentrixConnectors()
                        {
                            DentrixConnectorId = SafeValue<int>(item["DentrixConnectorId"]),
                            DentrixConnectorKey = SafeValue<string>(item["AccountKey"]),
                            LocationId = SafeValue<int>(item["LocationId"]),
                            LocationName = SafeValue<string>(item["Location"]),
                            AccountId = SafeValue<int>(item["AccountId"]),
                        });
                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                string result;
                using (StringWriter sw = new StringWriter())
                {
                    dt.WriteXml(sw);
                    result = sw.ToString();
                }
                new clsCommon().InsertErrorLog("LoginBll- GetDentrixConnectorValuesofUser", $"UserId: {UserId}  dt: {result}", $"StackTrace: {Ex.StackTrace} Message: {Ex.Message}");
                throw;
            }
        }
    }
}
