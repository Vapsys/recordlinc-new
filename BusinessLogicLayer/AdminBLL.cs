﻿using BO.Models;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public static class AdminBLL
    {
        public static clsAdmin objclsAdmin = new clsAdmin();

        public static DataTable GetAllFeatureMaster()
        {
            try
            {
                return objclsAdmin.GetAllFeatureMaster();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static int InsertUpdateFeatureMaster(FeatureMaster obj)
        {
            try
            {
                return objclsAdmin.InsertUpdateFeatureMaster(obj);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static FeatureMaster GetFeatureMasterById(int FeatureId)
        {
            try
            {
                FeatureMaster obj = new FeatureMaster();
                DataTable dt = objclsAdmin.GetFeatureMasterById(FeatureId);
                if(dt != null)
                {
                    obj.Id = SafeValue<int>(dt.Rows[0]["Id"]);
                    obj.Feature = SafeValue<string>(dt.Rows[0]["Feature"]);
                    obj.Status = SafeValue<int>(dt.Rows[0]["IsActive"]) == 1 ? 1 : 2;
                }
                return obj;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static bool CheckFeatureMasterExist(string Feature)
        {
            try
            {
                return objclsAdmin.CheckFeatureMasterExist(Feature);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static List<SelectListItem> GetDoctorAutocompleteList(int Index, int Size, string Searchtext)
        {
            try
            {
                DataTable dt = new DataTable();
                List<SelectListItem> lstdoctor = new List<SelectListItem>();
                dt = objclsAdmin.GetAccountList(Index, Size, Searchtext);
                if(dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count ; i++)
                    {
                        lstdoctor.Add(new SelectListItem()
                        {
                            Text = SafeValue<string>(dt.Rows[i]["AccountName"]).Trim(),
                            Value = SafeValue<string>(dt.Rows[i]["AccountID"])
                        }); 
                    }
                }
                return lstdoctor;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static List<chkFeatureSetting> GetAccountFeatureSettingByAccountId(int AccountId)
        {
            try
            {
                DataTable dt = new DataTable();
                List<chkFeatureSetting> lstFeature = new List<chkFeatureSetting>();
                dt = objclsAdmin.GetAccountFeatureSettingByAccountId(AccountId);
                if (dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        lstFeature.Add(new chkFeatureSetting()
                        {
                            Id = SafeValue<int>(dt.Rows[i]["Id"]),
                            AccountId = SafeValue<int>(dt.Rows[i]["AccountId"]),
                            FeatureId = SafeValue<int>(dt.Rows[i]["FeatureMasterId"]),
                            AdminStatus = SafeValue<bool>(dt.Rows[i]["AdminStatus"]),
                            DentistStatus = SafeValue<bool>(dt.Rows[i]["DentistStatus"]),
                            Feature = SafeValue<string>(dt.Rows[i]["Feature"]),
                        });
                    }
                }
                return lstFeature;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static int InsertUpdateAccountFeatureSetting(AccountFeatureSetting obj, bool IsUpdateByAdmin)
        {
            try
            {
                return objclsAdmin.InsertUpdateAccountFeatureSetting(obj,IsUpdateByAdmin);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public static List<DentistSyncModel> DentistLastSync()
        {
            try
            {
                DataTable dt = new DataTable();
                List<DentistSyncModel> syncLst = new List<DentistSyncModel>();
                dt =  objclsAdmin.DentistLastSync();
                if (dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        syncLst.Add(new DentistSyncModel()
                        {
                            Id = SafeValue<int>(dt.Rows[i]["Id"]),
                            DentrixConnectorId = SafeValue<int>(dt.Rows[i]["DentrixConnectorId"]),
                            AccountId = SafeValue<int>(dt.Rows[i]["AccountId"]),
                            AccountKey = SafeValue<string>(dt.Rows[i]["AccountKey"]),
                            AccountName = SafeValue<string>(dt.Rows[i]["AccountName"]),
                            LocationId = SafeValue<int>(dt.Rows[i]["LocationId"]),
                            UserId = SafeValue<int>(dt.Rows[i]["UserId"]),
                            Email = SafeValue<string>(dt.Rows[i]["Email"]),
                            FirstName = SafeValue<string>(dt.Rows[i]["FirstName"]),
                            LastName = SafeValue<string>(dt.Rows[i]["LastName"]),
                            Location = SafeValue<string>(dt.Rows[i]["Location"]),
                            Address = SafeValue<string>(dt.Rows[i]["Address"]),
                            Phone = SafeValue<string>(dt.Rows[i]["Phone"]),
                            SyncDate = SafeValue<DateTime>(dt.Rows[i]["SyncDate"]),
                            ProcessDate = SafeValue<DateTime>(dt.Rows[i]["ProcessDate"])
                        });
                    }
                }
                return syncLst;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static  List<EmailVerification> GetEmailVerificationListReport(EmailFilter objEmailFilter , string TimeZoneSystemName)
        {
            try
            {
                DataTable dt = new DataTable();
                List<EmailVerification> ObjEmailVerificationList = new List<EmailVerification>();
                dt = objclsAdmin.GetEmailVerificationListing(objEmailFilter);
                string strProcessdate;
                if (dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        strProcessdate = new CommonBLL().ConvertToDate(clsHelper.ConvertFromUTC(SafeValue<DateTime>(dt.Rows[i]["ProcessDate"]), TimeZoneSystemName),(int)BO.Enums.Common.DateFormat.GENERAL);

                        ObjEmailVerificationList.Add(new EmailVerification()
                        {                            
                            UserId = SafeValue<int>(dt.Rows[i]["UserId"]),
                            Emailtype = SafeValue<string>(dt.Rows[i]["Emailtype"]),
                            Email = SafeValue<string>(dt.Rows[i]["Email"]),
                            FullName = SafeValue<string>(dt.Rows[i]["FullName"]),
                            Phone = SafeValue<string>(dt.Rows[i]["Phone"]),
                            ProcessDate = strProcessdate,
                            Status = SafeValue<int>(dt.Rows[i]["Status"])
                        });
                    }
                }
                return ObjEmailVerificationList;
            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}
