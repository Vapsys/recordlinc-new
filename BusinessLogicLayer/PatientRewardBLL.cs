﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Models;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using System.Web.Helpers;
using DataAccessLayer.PatientsData;
using System.Drawing;
using System.IO;
using System.Web;
using System.Data;
using BO.ViewModel;
using System.Configuration;
using System.Data.SqlTypes;
using System.Xml.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using DataAccessLayer;
using System.Data.SqlClient;
using BO;
using BO.Enums;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public class PatientRewardBLL
    {
        public static clsHelper clshelper = new clsHelper();
        /// <summary>
        /// This Method will be Used to Insert Member On Recordlinc from Dentrix using Martin's Json
        /// </summary>
        /// <param name="Obj"></param>
        public static RewardResponse InsertDoctor(List<InsertMember> Obj)
        {
            clsCommon ObjCommon = new clsCommon();

            int UserId = 0;
            RewardResponse Response = new RewardResponse();
            List<InsertedRecord> lst = new List<InsertedRecord>();
            List<NotInsertedRecord> Lst = new List<NotInsertedRecord>();
            Response.TotalInsertedRecord = 0;
            Response.TotalNotInsertedRecord = 0;
            Response.ReceivedRecords = Obj.Count;
            if(CheckDentrixConnectorKeyExists(Obj[0].dentrixConnectorID))
            {
                for (int i = 0; i < Obj.Count; i++)
                {
                    try
                    {
                        if (string.IsNullOrWhiteSpace(Obj[i].Specialty))
                        {
                            Obj[i].Specialty = "General Dentist";
                        }
                        if (Obj[i].EmailAdresses == null || Obj[i].EmailAdresses.Count == 0 || !ObjCommon.IsEmail(SafeValue<string>(Obj[i].EmailAdresses.FirstOrDefault())))
                        {
                            string Email = "email" + DateTime.Now.Ticks + "@domain.com";
                            Obj[i].EmailAdresses = Email.Split(new char[] { ',' }).ToList();
                        }
                        if (string.IsNullOrWhiteSpace(Obj[i].FirstName))
                        {
                            Obj[i].FirstName = "Unknown";
                        }
                        if (string.IsNullOrWhiteSpace(Obj[i].LastName))
                        {
                            Obj[i].LastName = "Unknown";
                        }
                        NotInsertedRecord UnInserted = new NotInsertedRecord();
                        if (Obj[i].EmailAdresses != null)
                        {
                            if (!ObjCommon.IsEmail(SafeValue<string>(Obj[i].EmailAdresses.FirstOrDefault())))
                            {
                                UnInserted.DentrixId = Obj[i].DentrixId;
                                UnInserted.ErrorMessage = "Email address is not valid.";
                                Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                                Lst.Add(UnInserted);
                                Response.NotInsertedList = Lst;
                            }
                            else
                            {
                                if (!string.IsNullOrWhiteSpace(Obj[i].dentrixConnectorID))
                                {
                                    DataTable dt = clsPatientsData.CheckDentrixConnectorExistOrNot(Obj[i].dentrixConnectorID);
                                    if (dt != null && dt.Rows.Count > 0)
                                    {
                                        UserId = clsColleaguesData.CheckProviderExists(SafeValue<int>(Obj[i].DentrixId), Obj[i].dentrixConnectorID, Obj[i].ProviderType, SafeValue<string>(Obj[i].ProviderId));
                                        if (UserId > 0)
                                        {
                                            int result = clsColleaguesData.UpdateProviderDetailsForDentrixLoader(Obj[i], UserId);
                                            bool results = clsColleaguesData.InsertProvierOtherDentrixDetials(Obj[i], UserId);
                                            if (result > 0)
                                            {
                                                InsertedRecord Inserted = new InsertedRecord();
                                                Response.TotalInsertedRecord = Response.TotalInsertedRecord + 1;
                                                Inserted.RLId = UserId;
                                                Inserted.DentrixId = Obj[i].DentrixId;
                                                lst.Add(Inserted);
                                                Response.InsertedList = lst;
                                            }
                                            else
                                            {
                                                UnInserted.DentrixId = Obj[i].DentrixId;
                                                UnInserted.ErrorMessage = "DentrixId is required on Addresses Details!";
                                                Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                                                Lst.Add(UnInserted);
                                                Response.NotInsertedList = Lst;
                                            }
                                        }
                                        else
                                        {
                                            string Password = ObjCommon.CreateRandomPassword(16);
                                            string AccountKey = Crypto.SHA1(SafeValue<string>(DateTime.Now.Ticks));
                                            IDictionary<int, int> response = clsColleaguesData.SignUpDoctor(Obj[i], AccountKey, Password);
                                            UserId = response.Keys.FirstOrDefault();
                                            int Address = response.Values.FirstOrDefault();
                                            InsertedRecord Inserted = new InsertedRecord();
                                            if (Address == 0)
                                            {
                                                UnInserted.DentrixId = Obj[i].DentrixId;
                                                UnInserted.ErrorMessage = "DentrixId is required on Addresses Details!";
                                                Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                                                Lst.Add(UnInserted);
                                                Response.NotInsertedList = Lst;
                                            }
                                            else if (UserId > 0)
                                            {
                                                bool result = clsColleaguesData.InsertProvierOtherDentrixDetials(Obj[i], UserId);
                                                Response.TotalInsertedRecord = Response.TotalInsertedRecord + 1;
                                                Inserted.RLId = UserId;
                                                Inserted.DentrixId = Obj[i].DentrixId;
                                                lst.Add(Inserted);
                                                Response.InsertedList = lst;
                                            }
                                            else
                                            {
                                                UnInserted.DentrixId = Obj[i].DentrixId;
                                                UnInserted.ErrorMessage = "Invalid Data";
                                                Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                                                Lst.Add(UnInserted);
                                                Response.NotInsertedList = Lst;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        UnInserted.DentrixId = Obj[i].DentrixId;
                                        UnInserted.ErrorMessage = "Invalid ConnectorId";
                                        Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                                        Lst.Add(UnInserted);
                                        Response.NotInsertedList = Lst;
                                    }

                                }
                                else
                                {
                                    UnInserted.DentrixId = Obj[i].DentrixId;
                                    UnInserted.ErrorMessage = "Invalid ConnectorId";
                                    Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                                    Lst.Add(UnInserted);
                                    Response.NotInsertedList = Lst;
                                }
                            }
                        }
                        else
                        {
                            UnInserted.DentrixId = Obj[i].DentrixId;
                            UnInserted.ErrorMessage = "Email field is required!";
                            Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                            Lst.Add(UnInserted);
                            Response.NotInsertedList = Lst;
                        }
                    }
                    catch (Exception Ex)
                    {
                        NotInsertedRecord UnInserted = new NotInsertedRecord();
                        UnInserted.DentrixId = Obj[i].DentrixId;
                        UnInserted.ErrorMessage = Ex.Message;
                        Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                        new clsCommon().InsertErrorLog("DentrixSyncProviderInsert---InsertDoctor--PatientReward", Ex.Message, Ex.StackTrace);
                        Lst.Add(UnInserted);
                        Response.NotInsertedList = Lst;
                    }
                }
            }
            else
            {
                Response.TotalNotInsertedRecord = Obj.Count;
                Response.NotInsertedList = new List<NotInsertedRecord>();
                foreach (var item in Obj)
                {
                    Response.NotInsertedList.Add(new NotInsertedRecord()
                    {
                        DentrixId = item.ProviderId,
                        ErrorMessage = "Invalid Dentrix ConnectorID"
                    });
                }
            }
            return Response;
        }

        /// <summary>
        /// This method used for inserting patient on Recordlinc from Dentrix using martin's code.
        /// </summary>
        /// <param name="newPatientObj"></param>
        /// <returns></returns>
        public static RewardResponse PatientInsertUpdateNew(List<DentrixPatient> newPatientObj)
        {
            int PatientId = 0;
            int AddressInfoId = 0;
            clsCommon objCommon = new clsCommon();
            RewardResponse Response = new RewardResponse();
            List<InsertedRecord> lst = new List<InsertedRecord>();
            List<NotInsertedRecord> Lst = new List<NotInsertedRecord>();
            Response.TotalInsertedRecord = 0;
            Response.TotalNotInsertedRecord = 0;
            Response.ReceivedRecords = newPatientObj.Count;


            if (CheckDentrixConnectorKeyExists(newPatientObj[0].dentrixConnectorID))
            {
                for (int i = 0; i < newPatientObj.Count; i++)
                {
                    try
                    {
                        if (newPatientObj[i].Emails == null || newPatientObj[i].Emails.Count == 0)
                        {
                            string Email = "email" + DateTime.Now.Ticks + "@domain.com";
                            newPatientObj[i].Emails = Email.Split(new char[] { ',' }).ToList();
                        }

                        if (newPatientObj[i].Emails != null)
                        {
                            if (SafeValue<DateTime>(newPatientObj[i].birthdate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                            {
                                newPatientObj[i].birthdate = null;
                            }
                            newPatientObj[i].Password = objCommon.CreateRandomPassword(8);
                            //Insert Patient
                            IDictionary<int, int> response = null;
                            if (newPatientObj[i].automodifiedtimestamp != null)
                            {
                                if (SafeValue<DateTime>(newPatientObj[i].automodifiedtimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                                {
                                    newPatientObj[i].automodifiedtimestamp = SafeValue<DateTime>(SqlDateTime.MinValue.ToString());
                                }
                            }
                            else
                            {
                                newPatientObj[i].automodifiedtimestamp = SafeValue<DateTime>(SqlDateTime.MinValue.ToString());
                            }                          

                            response = clsPatientsData.PatientInsertUpdateForDentrix(newPatientObj[i]);
                            PatientId = response.Keys.FirstOrDefault();
                            AddressInfoId = response.Values.FirstOrDefault();
                            InsertedRecord Inserted = new InsertedRecord();
                            if (AddressInfoId == 1)
                            {
                                NotInsertedRecord UnInserted = new NotInsertedRecord();
                                UnInserted.DentrixId = newPatientObj[i].patid;
                                UnInserted.ErrorMessage = "DentrixId is required on Addresses Details!";
                                Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                                Lst.Add(UnInserted);
                                Response.NotInsertedList = Lst;
                            }
                            else if (PatientId == 2)
                            {
                                NotInsertedRecord UnInserted = new NotInsertedRecord();
                                UnInserted.DentrixId = newPatientObj[i].patid;
                                UnInserted.ErrorMessage = "Invalid Dentrix ConnectorID";
                                Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                                Lst.Add(UnInserted);
                                Response.NotInsertedList = Lst;
                            }
                            else
                            {
                                if (PatientId > 0)
                                {
                                    if (SafeValue<DateTime>(newPatientObj[i].FirstVisitDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                                    {
                                        newPatientObj[i].FirstVisitDate = null;
                                    }
                                    if (SafeValue<DateTime>(newPatientObj[i].LastVisitDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                                    {
                                        newPatientObj[i].LastVisitDate = null;
                                    }
                                    if (SafeValue<DateTime>(newPatientObj[i].automodifiedtimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                                    {
                                        newPatientObj[i].automodifiedtimestamp = null;
                                    }
                                    newPatientObj[i].RL_Id = PatientId;
                                    bool result = clsPatientsData.InsertupdateDentrixPatientDetails(newPatientObj[i]);
                                }
                                Response.TotalInsertedRecord = Response.TotalInsertedRecord + 1;
                                Inserted.RLId = PatientId;
                                Inserted.DentrixId = newPatientObj[i].patid;
                                newPatientObj[i].RL_Id = PatientId;
                                lst.Add(Inserted);
                                Response.InsertedList = lst;
                            }
                        }
                        else
                        {
                            NotInsertedRecord UnInserted = new NotInsertedRecord();
                            UnInserted.DentrixId = newPatientObj[i].patid;
                            UnInserted.ErrorMessage = "Emails field is required!";
                            Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                            Lst.Add(UnInserted);
                            Response.NotInsertedList = Lst;
                        }
                    }
                    catch (Exception Ex)
                    {
                        NotInsertedRecord UnInserted = new NotInsertedRecord();
                        UnInserted.DentrixId = newPatientObj[i].patid;
                        UnInserted.ErrorMessage = Ex.Message;
                        Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                        new clsCommon().InsertErrorLog("DentrixSyncPatientInsert---InsertPatient_DentrixInsuranceDetails--PatientReward", Ex.Message, Ex.StackTrace);
                        Lst.Add(UnInserted);
                        Response.NotInsertedList = Lst;
                    }

                }
            }
            else
            {

                Response.TotalNotInsertedRecord = newPatientObj.Count;
                Response.NotInsertedList = new List<NotInsertedRecord>();
                foreach (var item in newPatientObj)
                {
                    Response.NotInsertedList.Add(new NotInsertedRecord()
                    {
                        DentrixId = item.patid,
                        ErrorMessage = "Invalid Dentrix ConnectorID"
                    });
                }
            }
            return Response;
        }

        public static RewardResponse InsertPatient_DentrixAjustmentDetails(List<Patient_DentrixAdjustments> obj)
        {
            RewardResponse Response = new RewardResponse();
            List<InsertedRecord> lst = new List<InsertedRecord>();
            List<NotInsertedRecord> Lst = new List<NotInsertedRecord>();
            Response.TotalInsertedRecord = 0;
            Response.TotalNotInsertedRecord = 0;
            Response.ReceivedRecords = obj.Count;
            foreach (var item in obj)
            {
                try
                {

                    if (SafeValue<DateTime>(item.procdate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                    {
                        item.procdate = null;
                    }
                    if (SafeValue<DateTime>(item.automodifiedtimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                    {
                        item.automodifiedtimestamp = null;
                    }
                    InsertedRecord Inserted = new InsertedRecord();
                    int result = clsPatientsData.InsertUpdatePatient_DentrixAdjustmentDetails(item);
                    if (result == 2)
                    {
                        NotInsertedRecord UnInserted = new NotInsertedRecord();
                        UnInserted.DentrixId = item.adjustid;
                        UnInserted.ErrorMessage = "Invalid Dentrix ConnectorID";
                        Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                        Lst.Add(UnInserted);
                        Response.NotInsertedList = Lst;
                    }
                    else
                    {
                        if (result > 0)
                        {
                            Response.TotalInsertedRecord = Response.TotalInsertedRecord + 1;
                            Inserted.RLId = result;
                            Inserted.DentrixId = item.adjustid;
                            lst.Add(Inserted);
                            Response.InsertedList = lst;
                        }
                    }
                }
                catch (Exception Ex)
                {
                    NotInsertedRecord UnInserted = new NotInsertedRecord();
                    UnInserted.DentrixId = item.adjustid;
                    UnInserted.ErrorMessage = Ex.Message;
                    Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                    new clsCommon().InsertErrorLog("PatientRewardBLL---InsertPatient_DentrixAjustmentDetails", Ex.Message, Ex.StackTrace);
                    Lst.Add(UnInserted);
                    Response.NotInsertedList = Lst;
                }
            }
            return Response;
        }

        public static RewardResponse InsertPatient_DentrixInsurancePaymentDetails(List<Patient_DentrixInsurancePayment> Obj)
        {
            RewardResponse Response = new RewardResponse();
            List<InsertedRecord> lst = new List<InsertedRecord>();
            List<NotInsertedRecord> Lst = new List<NotInsertedRecord>();
            Response.TotalInsertedRecord = 0;
            Response.TotalNotInsertedRecord = 0;
            Response.ReceivedRecords = Obj.Count;
            foreach (var item in Obj)
            {

                try
                {
                    if (CheckDentrixConnectorKeyExists(item.dentrixConnectorID))
                    {
                        if (SafeValue<DateTime>(item.procdate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            item.procdate = null;
                        }
                        if (SafeValue<DateTime>(item.createdate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            item.createdate = null;
                        }
                        if (SafeValue<DateTime>(item.automodifiedtimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            item.automodifiedtimestamp = null;
                        }
                        InsertedRecord Inserted = new InsertedRecord();
                        int PaymentId = clsPatientsData.InsertUpdatePatient_DentrixInsurancePaymentDetails(item);
                        if (PaymentId > 0)
                        {
                            Response.TotalInsertedRecord = Response.TotalInsertedRecord + 1;
                            Inserted.RLId = PaymentId;
                            Inserted.DentrixId = item.paymentId;
                            lst.Add(Inserted);
                            Response.InsertedList = lst;
                        }
                    }
                    else
                    {
                        NotInsertedRecord UnInserted = new NotInsertedRecord();
                        UnInserted.DentrixId = item.paymentId;
                        UnInserted.ErrorMessage = "Invalid Dentrix ConnectorID";
                        Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                        Lst.Add(UnInserted);
                        Response.NotInsertedList = Lst;
                    }
                }
                catch (Exception Ex)
                {
                    new clsCommon().InsertErrorLog("PatientRewardBLL---InsertPatient_DentrixInsuranceDetails", Ex.Message, Ex.StackTrace);
                    NotInsertedRecord UnInserted = new NotInsertedRecord();
                    UnInserted.DentrixId = item.paymentId;
                    UnInserted.ErrorMessage = Ex.Message;
                    Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                    Lst.Add(UnInserted);
                    Response.NotInsertedList = Lst;
                }
            }
            return Response;
        }

        public static RewardResponse InsertPatient_DentrixStandardPayment(List<Patient_DentrixStandardPayment> Obj)
        {
            RewardResponse Response = new RewardResponse();
            List<InsertedRecord> lst = new List<InsertedRecord>();
            List<NotInsertedRecord> Lst = new List<NotInsertedRecord>();
            Response.TotalInsertedRecord = 0;
            Response.TotalNotInsertedRecord = 0;
            Response.ReceivedRecords = Obj.Count;
            foreach (var item in Obj)
            {
                try
                {
                    if (CheckDentrixConnectorKeyExists(item.dentrixConnectorID))
                    {
                        if (SafeValue<DateTime>(item.procdate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            item.procdate = null;
                        }
                        if (SafeValue<DateTime>(item.createdate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            item.createdate = null;
                        }
                        if (SafeValue<DateTime>(item.automodifiedtimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            item.automodifiedtimestamp = null;
                        }
                        int PaymentId = clsPatientsData.InsertUpdatePatient_DentrixStandarsPayment(item);
                        if (PaymentId > 0)
                        {
                            InsertedRecord Inserted = new InsertedRecord();
                            Response.TotalInsertedRecord = Response.TotalInsertedRecord + 1;
                            Inserted.RLId = PaymentId;
                            Inserted.DentrixId = item.paymentId;
                            lst.Add(Inserted);
                            Response.InsertedList = lst;
                        }
                    }
                    else
                    {
                        NotInsertedRecord UnInserted = new NotInsertedRecord();
                        UnInserted.DentrixId = item.paymentId;
                        UnInserted.ErrorMessage = "Invalid Dentrix ConnectorID";
                        Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                        Lst.Add(UnInserted);
                        Response.NotInsertedList = Lst;
                    }
                }
                catch (Exception Ex)
                {
                    new clsCommon().InsertErrorLog("PatientRewardBLL---InsertPatient_DentrixStandardPayment", Ex.Message, Ex.StackTrace);
                    NotInsertedRecord UnInserted = new NotInsertedRecord();
                    UnInserted.DentrixId = item.paymentId;
                    UnInserted.ErrorMessage = Ex.Message;
                    Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                    Lst.Add(UnInserted);
                    Response.NotInsertedList = Lst;
                }
            }
            return Response;
        }

        public static RewardResponse InsertPatient_DentrixFinanceCharges(List<Patient_DentrixFinanceCharges> Obj)
        {
            RewardResponse Response = new RewardResponse();
            List<InsertedRecord> lst = new List<InsertedRecord>();
            List<NotInsertedRecord> Lst = new List<NotInsertedRecord>();
            Response.TotalInsertedRecord = 0;
            Response.TotalNotInsertedRecord = 0;
            Response.ReceivedRecords = Obj.Count;
            foreach (var item in Obj)
            {
                try
                {
                    if (CheckDentrixConnectorKeyExists(item.dentrixConnectorID))
                    {
                        if (SafeValue<DateTime>(item.procdate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            item.procdate = null;
                        }
                        if (SafeValue<DateTime>(item.automodifiedtimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            item.automodifiedtimestamp = null;
                        }
                        int PaymentId = clsPatientsData.InsertUpdatePatient_DentrixFinanceCharges(item);
                        if (PaymentId > 0)
                        {
                            InsertedRecord Inserted = new InsertedRecord();
                            Response.TotalInsertedRecord = Response.TotalInsertedRecord + 1;
                            Inserted.RLId = PaymentId;
                            Inserted.DentrixId = item.chargeId;
                            lst.Add(Inserted);
                            Response.InsertedList = lst;
                        }
                    }
                    else
                    {
                        NotInsertedRecord UnInserted = new NotInsertedRecord();
                        UnInserted.DentrixId = item.chargeId;
                        UnInserted.ErrorMessage = "Invalid Dentrix ConnectorID";
                        Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                        Lst.Add(UnInserted);
                        Response.NotInsertedList = Lst;
                    }
                }
                catch (Exception Ex)
                {
                    new clsCommon().InsertErrorLog("PatientRewardBLL---InsertPatient_DentrixFinanceCharges", Ex.Message, Ex.StackTrace);
                    NotInsertedRecord UnInserted = new NotInsertedRecord();
                    UnInserted.DentrixId = item.chargeId;
                    UnInserted.ErrorMessage = Ex.Message;
                    Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                    Lst.Add(UnInserted);
                    Response.NotInsertedList = Lst;
                }
            }
            return Response;
        }

        //public static RewardResponse InsertPatient_DentrixProcedure(List<Patient_DentrixProcedure> Obj)
        //{
        //    int Total = 0;
        //    RewardResponse Response = new RewardResponse();
        //    Response.TotalInsertedRecord = 0;
        //    Response.TotalNotInsertedRecord = 0;
        //    Response.ReceivedRecords = Obj.Count;
        //    List<InsertedRecord> lst = new List<InsertedRecord>();
        //    List<NotInsertedRecord> Lst = new List<NotInsertedRecord>();
        //    // string DentrixConnectorId = Obj.Where(x => x.dentrixConnectorID == null).OrderBy(x => x.dentrixConnectorID).FirstOrDefault().dentrixConnectorID;
        //    IDictionary<int, string> Result = InsertDPMS_SyncDetails(Obj.FirstOrDefault().dentrixConnectorID, "AddPatientProcedure");
        //    for (int i = 0; i < Obj.Count; i++)
        //    {
        //        try
        //        {
        //            DataTable dtcheck = clsPatientsData.CheckDentrixConnectorExistOrNot(Obj[i].dentrixConnectorID);
        //            if (dtcheck != null && dtcheck.Rows.Count > 0)
        //            {
        //                //var newtd = SafeValue<string>(Obj[i].txplanneddate).Replace("/Date(", "").Replace(")/", "");
        //                //DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
        //                //dateTime = dateTime.AddMilliseconds(SafeValue<double>(newtd)).ToLocalTime();
        //                //Obj[i].txplanneddate = dateTime.ToString();
        //                if (SafeValue<DateTime>(Obj[i].txplanneddate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
        //                {
        //                    Obj[i].txplanneddate = null;
        //                }
        //                if (SafeValue<DateTime>(Obj[i].startdate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
        //                {
        //                    Obj[i].startdate = null;
        //                }
        //                if (SafeValue<DateTime>(Obj[i].completiondate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
        //                {
        //                    Obj[i].completiondate = null;
        //                }
        //                Total = clsPatientsData.InsertIntoPatient_DentrixProcedure(Obj[i]);
        //                if (Total > 0)
        //                {
        //                    Response.TotalInsertedRecord = Response.TotalInsertedRecord + 1;
        //                    InsertedRecord Inserted = new InsertedRecord();
        //                    Inserted.DentrixId = Obj[i].procid;
        //                    Inserted.RLId = Total;
        //                    lst.Add(Inserted);
        //                    Response.InsertedList = lst;
        //                }
        //                else
        //                {
        //                    Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
        //                    NotInsertedRecord UnInserted = new NotInsertedRecord();
        //                    UnInserted.DentrixId = Obj[i].procid;
        //                    Lst.Add(UnInserted);
        //                    Response.NotInsertedList = Lst;
        //                }
        //            }
        //            else
        //            {
        //                NotInsertedRecord UnInserted = new NotInsertedRecord();
        //                UnInserted.DentrixId = Obj[i].procid;
        //                UnInserted.ErrorMessage = "Invalid Dentrix ConnectorId.";
        //                Lst.Add(UnInserted);
        //                Response.NotInsertedList = Lst;
        //            }

        //        }
        //        catch (Exception Ex)
        //        {
        //            new clsCommon().InsertErrorLog("PatientRewardBLL---InsertPatient_DentrixFinanceCharges", Ex.Message, Ex.StackTrace);
        //            Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
        //            NotInsertedRecord UnInserted = new NotInsertedRecord();
        //            UnInserted.DentrixId = Obj[i].procid;
        //            UnInserted.ErrorMessage = Ex.Message;
        //            Lst.Add(UnInserted);
        //            Response.NotInsertedList = Lst;
        //        }
        //    }
        //    return Response;
        //}
        public static string UpdatePatientImage(Reward_UploadProfileImage Obj)
        {
            try
            {
                string result = string.Empty;
                if (!string.IsNullOrWhiteSpace(Obj.ImageString))
                {
                    string Filename = "$" + DateTime.Now.Ticks + "$" + Obj.ImageNameWithExtension;
                    result = CommonBLL.SaveBase64ImageonImageBank(Obj.ImageString, Filename, Obj.path);
                    int PatientId = clsPatientsData.GetPatientIdFromRewardPartnerId(Obj);
                    if (PatientId > 0)
                    {
                        if (!string.IsNullOrWhiteSpace(result))
                        {
                            clsPatientsData.UpdatePatientProfileImg(PatientId, Filename);
                        }
                        else
                        {
                            return "Image Base 64 string is empty";
                        }
                    }
                    else
                    {
                        return "This RewardPartnerId has not Recordlinc PatientId";
                    }
                }
                return result;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---UpdatePatientProfileImage", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static UpdateResponse<PatientDetails> UpdatePatientInfo(Reward_PatientUpdate Obj)
        {
            try
            {
                UpdateResponse<PatientDetails> ResObj = new UpdateResponse<PatientDetails>();
                PatientDetails obj = new PatientDetails();
                DataTable dt = clsPatientsData.UpdatePatientDentails(Obj);

                if (dt != null && dt.Rows.Count > 0)
                {
                    var Patientids = new List<string>();
                    foreach (DataRow item in dt.Rows)
                    {
                        string Result = string.Empty;
                        int PatientId = SafeValue<int>(item["PatientIds"]);
                        #region This Group of Method save Medical History and Dental History as well Insurance Coverages
                        bool IsPersonalInformation = PatientInformation(SafeValue<string>(PatientId), Obj);
                        bool IsInsuranceCoverage = InsuranceCoverage(SafeValue<string>(PatientId), Obj.InsuranceCoverage, Obj.PrimaryDentalInsurance, Obj.SecondaryDentalInsurance);
                        bool IsMedicalHistory = MedicalHistoryForm(SafeValue<string>(PatientId), Obj.MedicalHistory);
                        bool IsDentalHistory = DentalHistoryForm(SafeValue<string>(PatientId), Obj.DentalHistory);
                        #endregion
                        if (!string.IsNullOrWhiteSpace(Obj.ImageString))
                        {
                            string Filename = "$" + DateTime.Now.Ticks + "$" + Obj.ImageNameWithExtension;
                            Result = CommonBLL.SaveBase64ImageonImageBank(Obj.ImageString, Filename, Obj.path);
                            if (PatientId > 0)
                            {
                                if (!string.IsNullOrWhiteSpace(Result))
                                {
                                    clsPatientsData.UpdatePatientProfileImg(PatientId, Filename);
                                }
                            }
                        }
                        Patientids.Add(SafeValue<string>(item["PatientIds"]));
                    }
                    obj.PatientId = Patientids;

                    ResObj.IsSuccess = true;
                    ResObj.Message = "";
                    ResObj.StatusCode = 200;
                    ResObj.Result = obj;
                }
                return ResObj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---UpdatePatientInfo", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool checkPatientExtist(string Email)
        {
            try
            {
                return new clsPatientsData().CheckPatientEmail(Email);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---checkPatientExtist", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool AddDentistForReward(Reward_AddDentist Obj)
        {
            try
            {
                return clsPatientsData.AddUserForRewardPlatForm(Obj);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---AddDentistForReward", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static List<Reward_UserList> GetDentistListForUser(Reward_DentistList Obj)
        {
            try
            {
                List<Reward_UserList> Lst = new List<Reward_UserList>();
                DataTable dt = new DataTable();
                dt = clsPatientsData.GetDentistListForRewardUser(Obj);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        Lst.Add(new Reward_UserList()
                        {
                            AccountId = SafeValue<int>(item["AccountID"]),
                            DentistFirstName = SafeValue<string>(item["FirstName"]),
                            DentistLastName = SafeValue<string>(item["LastName"]),
                            AccountName = SafeValue<string>(item["AccountName"]),
                            Location = SafeValue<string>(item["Location"]),
                            ProviderType = (item["provtype"] is DBNull) ? BO.Enums.Common.PMSProviderType.Provider : (SafeValue<int>(item["provtype"]) == 0 || SafeValue<int>(item["provtype"]) == 1) ? BO.Enums.Common.PMSProviderType.Provider : BO.Enums.Common.PMSProviderType.Staff
                        });
                    }
                }
                return Lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---AddDentistForReward", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static Reward_ListOfPotential GetPotentialRewardList(Reward_RewardsCall Obj)
        {
            try
            {
                Reward_ListOfPotential PotentialReward = new Reward_ListOfPotential();
                List<RewardList> Lst = new List<RewardList>();
                DataSet ds = new DataSet();
                ds = clsPatientsData.GetRewardsList(Obj);
                if (ds != null && ds.Tables.Count > 0)
                {
                    //PotentialReward.RewardPartnerId = 
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(ds.Tables[0].Rows[0]["TotalPotentialReward"])))
                    {
                        PotentialReward.TotalPotentialRewards = SafeValue<double>(ds.Tables[0].Rows[0]["TotalPotentialReward"]);
                    }
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(ds.Tables[1].Rows[0]["TotalActualReward"])))
                    {
                        PotentialReward.TotalActualRewards = SafeValue<double>(ds.Tables[1].Rows[0]["TotalActualReward"]);
                    }
                    PotentialReward.RewardPartnerId = Obj.RewardPartnerId;
                    if (ds.Tables[2] != null && ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow item in ds.Tables[2].Rows)
                        {
                            Lst.Add(new RewardList()
                            {
                                RewardCode = SafeValue<string>(item["Code"]),
                                RewardText = SafeValue<string>(item["RewardText"]),
                                RewardType = SafeValue<int>(item["RewardType"]),
                                Point = SafeValue<double>(item["Point"]),
                                Date = SafeValue<DateTime>(item["CreatedDate"]),
                                Id = SafeValue<int>(item["PatientRewardId"]),
                                RewardPatnerRewardId = SafeValue<int>(item["RewardPlatformRewardId"])
                            });
                        }
                    }
                }
                PotentialReward.RewardsList = Lst;
                return PotentialReward;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---GetPotentialRewardList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static RewardsJson CreateRewardsForPatient(CreateRewards obj, int Source)
        {
            try
            {
                int type;
                if (obj.RewardTypeCode == "RR")
                    type = (int)BO.Enums.Common.DentistRewardsType.Actual;
                else
                    type = (int)BO.Enums.Common.DentistRewardsType.Potential;

                int RewardId = clsPatientsData.CreateRewardsForPatientSolutionOne(obj, Source, type);
                RewardsJson RJ = new RewardsJson();
                if (RewardId == -1)
                {
                    RJ.RewardId = RewardId;
                    RJ.ErrorMessage = "The Dentist of this Patient hasn't set up his Reward details on the system.";
                }
                if (RewardId == -2)
                {
                    RJ.RewardId = RewardId;
                    string MasterCode = clsColleaguesData.GetRegisterRewardMasterCode(obj.RewardPartnerId);
                    RJ.ErrorMessage = $"Master Reward code {MasterCode} is not available at Reward Platform.";
                }
                if (RewardId > 0)
                {
                    DataTable dt = clsPatientsData.GetRewardDetailsByRewardId(RewardId, (int)obj.sourcetype);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            RJ.RewardId = SafeValue<int>(item["RewardId"]);
                            RJ.RewardAmount = SafeValue<double>(item["RewardAmount"]);
                            RJ.RewardType = SafeValue<int>(item["RewardType"]) == 2 ? "Potential" : "Active";
                            RJ.RewardStatus = "Active";
                        }
                    }
                }
                return RJ;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---CreateRewardsForPatient", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool CheckRewardtype(CreateRewards obj)
        {
            try
            {
                return clsPatientsData.CheckRewardtype(obj);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---CheckRewardtype", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool CheckDentrixConnectorKeyExists(string DentrixConnectorKey)
        {
            try
            {
                DataTable dt = clsPatientsData.CheckDentrixConnectorExistOrNot(DentrixConnectorKey);
                if (dt != null && dt.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {

                throw;
            }
        }

        public static int GetRewardAmountBalance(RewardBalanceJson obj)
        {
            try
            {
                if (SafeValue<string>(obj.RewardType.ToLower()) == "potential" || SafeValue<string>(obj.RewardType.ToLower()) == "2")
                {
                    obj.RewardType = "2";
                }
                else if (SafeValue<string>(obj.RewardType.ToLower()) == "actual" || SafeValue<string>(obj.RewardType.ToLower()) == "1")
                {
                    obj.RewardType = "1";
                }
                var RewardAmount = clsPatientsData.GetRewardAmountBalance(obj);
                return RewardAmount;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---GetRewardAmountBalance", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static dynamic GetDentistDetailsById(int DoctorId)
        {
            try
            {
                List<DoctorDetails> DoctorDetails = new List<DoctorDetails>();
                clsColleaguesData objColleaguesData = new clsColleaguesData();
                DataSet ds = objColleaguesData.GetDoctorDetailsById(DoctorId);
                dynamic Result;
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    DoctorDetails.Add(GetDoctorDetails(DoctorId, ds));
                    Result = new { DoctorDetails };
                }
                else
                {
                    Result = null;
                }
                return Result;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---GetDentistDetailsById", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static DoctorDetails GetDoctorDetails(int DoctorId, DataSet ds = null, bool Hasaccount = false)
        {
            try
            {
                clsColleaguesData objColleaguesData = new clsColleaguesData();
                clsCommon objCommon = new clsCommon();
                DoctorDetails objDoctorDetails = new DoctorDetails();
                int ParentUserId = 0;
                string DoctorImage = SafeValue<string>(System.Configuration.ConfigurationManager.AppSettings.Get("DoctorImage"));
                if (ds == null)
                {
                    ds = objColleaguesData.GetDoctorDetailsById(DoctorId);
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            objDoctorDetails.DoctorId = SafeValue<int>(ds.Tables[0].Rows[0]["UserId"]);
                            objDoctorDetails.RecordlincLocationId = SafeValue<int>(ds.Tables[0].Rows[0]["LocationId"]);
                            var FirstName = SafeValue<string>(ds.Tables[0].Rows[0]["FirstName"]);
                            var MiddleName = SafeValue<string>(ds.Tables[0].Rows[0]["MiddleName"]);
                            var LastName = SafeValue<string>(ds.Tables[0].Rows[0]["LastName"]);
                            objDoctorDetails.Email = SafeValue<string>(ds.Tables[0].Rows[0]["Username"]);
                            objDoctorDetails.DoctorFullName = FirstName + " " + MiddleName + " " + LastName;
                            ParentUserId = (ds.Tables[0].Rows[0]["ParentUserId"] as int?).GetValueOrDefault(0) == 0 ? SafeValue<int>(ds.Tables[0].Rows[0]["UserId"]) : SafeValue<int>(ds.Tables[0].Rows[0]["ParentUserId"]);
                            string ImageName = SafeValue<string>(ds.Tables[0].Rows[0]["ImageName"]);
                            objDoctorDetails.Dentrixprovid = SafeValue<string>(ds.Tables[0].Rows[0]["DentrixProviderId"]);
                            objDoctorDetails.ProviderType = (ds.Tables[0].Rows[0]["provtype"] is DBNull) ? BO.Enums.Common.PMSProviderType.Provider : ((SafeValue<int>(ds.Tables[0].Rows[0]["provtype"]) == 0 || SafeValue<int>(ds.Tables[0].Rows[0]["provtype"]) == 1) ? BO.Enums.Common.PMSProviderType.Provider : BO.Enums.Common.PMSProviderType.Staff);
                            if (string.IsNullOrEmpty(ImageName))
                            {
                                objDoctorDetails.ImageUrl = ConfigurationManager.AppSettings.Get("DoctorProfileImage");
                            }
                            else
                            {
                                objDoctorDetails.ImageUrl = DoctorImage + ImageName;

                            }
                            objDoctorDetails.Description = SafeValue<string>(ds.Tables[0].Rows[0]["Description"]);
                            objDoctorDetails.Education = SafeValue<string>(ds.Tables[0].Rows[0]["WebsiteURL"]);
                            objDoctorDetails.Specialties = SafeValue<string>(ds.Tables[7].Rows[0]["Speciality"]);

                        }
                        if (ds.Tables[8].Rows.Count > 0)
                        {
                            objDoctorDetails.AccountId = SafeValue<int>(ds.Tables[8].Rows[0]["AccountID"]);
                        }
                        //PRCSP-411 for Account Name
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            objDoctorDetails.AccountName = SafeValue<string>(ds.Tables[1].Rows[0]["AccountName"]);
                        }
                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            objDoctorDetails.Addresses = GetDoctorAddressDetailsById(ds.Tables[2]);
                            objDoctorDetails.Phone =SafeValue<string>(ds.Tables[2].Rows[0]["Phone"]);
                        }
                        if (objDoctorDetails.Addresses.Count > 0)
                        {
                            //BO.ViewModel.Addresses address = objDoctorDetails.Addresses.First(x => x.AddressType == "1");
                            if (ds.Tables[19].Rows.Count > 0)
                            {
                                foreach (DataRow item in ds.Tables[19].Rows)
                                {
                                    var myobj = objDoctorDetails.Addresses.Where(t => t.AddressId == SafeValue<int>(item["LocationId"])).FirstOrDefault();
                                    if (myobj != null)
                                    {
                                        objDoctorDetails.Addresses.Where(t => t.AddressId == SafeValue<int>(item["LocationId"])).FirstOrDefault().LocationId = SafeValue<int>(item["RewardLocationId"]);
                                    }
                                }
                            }
                            //address.AddressType = "Primary";
                            //objDoctorDetails.Addresses.Clear();
                            //objDoctorDetails.Addresses.Add(address);
                        }
                        if (ds.Tables[3].Rows.Count > 0)
                        {
                            objDoctorDetails.SocialMediaWebsites = GetSocialMediaDetailByUserId(ds.Tables[3]);
                        }
                        if (ds.Tables[5].Rows.Count > 0)
                        {
                            objDoctorDetails.ProfessionalMemberships = GetProfessionalMembershipsByUserId(ds.Tables[5]);
                        }
                        if (ds.Tables[6].Rows.Count > 0)
                        {
                            objDoctorDetails.SpecialitesDetails = GetSpeacilitiesOfDoctorById(ds.Tables[6]);
                        }
                        if (ds.Tables[11].Rows.Count > 0)
                        {
                            objDoctorDetails.PracticeWebsites = GetSecondaryWebsitelistByUserId(ds.Tables[11]);
                        }
                        if (ds.Tables[4].Rows.Count > 0)
                        {
                            objDoctorDetails.EducationDetails = GetEducationandTrainingDetailsForDoctor(ds.Tables[11]);
                        }
                        objDoctorDetails.TeamMembers = GetTeamMemberDetailsOfDoctor(objDoctorDetails.DoctorId, 1, 15);
                        if (ds.Tables[16].Rows.Count > 0)
                        {
                            objDoctorDetails.OrganizationId = SafeValue<int>(ds.Tables[7].Rows[0]["RewardCompanyID"]);
                        }
                        if (ds.Tables[19].Rows.Count > 0)
                        {
                            objDoctorDetails.OrganizationLocationId = SafeValue<int>(ds.Tables[19].Rows[0]["RewardLocationId"]);
                            objDoctorDetails.RecordlincLocationId = 0;
                            objDoctorDetails.RecordlincLocationId = SafeValue<int>(ds.Tables[19].Rows[0]["LocationId"]);
                        }

                        //PRCSP-511
                        DataSet ds1 = new DataSet();
                        ds1 = objColleaguesData.GetSecondaryWebsitelistByParentUserId(DoctorId);
                        if (ds1.Tables.Count > 0)
                        {
                            if (ds1 != null && ds1.Tables[0].Rows.Count > 0)
                            {
                                objDoctorDetails.PracticeWebsites = GetSecondaryWebsitelistByParentUserId(ds1.Tables[0], objDoctorDetails.PracticeWebsites);
                            }
                        }
                    }
                }
                else
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        objDoctorDetails.DoctorId = SafeValue<int>(ds.Tables[0].Rows[0]["UserId"]);
                        objDoctorDetails.RecordlincLocationId = SafeValue<int>(ds.Tables[0].Rows[0]["LocationId"]);
                        var FirstName = SafeValue<string>(ds.Tables[0].Rows[0]["FirstName"]);
                        var MiddleName = SafeValue<string>(ds.Tables[0].Rows[0]["MiddleName"]);
                        var LastName = SafeValue<string>(ds.Tables[0].Rows[0]["LastName"]);
                        objDoctorDetails.Email = SafeValue<string>(ds.Tables[0].Rows[0]["Username"]);
                        objDoctorDetails.DoctorFullName = FirstName + " " + MiddleName + " " + LastName;
                        ParentUserId = (ds.Tables[0].Rows[0]["ParentUserId"] as int?).GetValueOrDefault(0) == 0 ? SafeValue<int>(ds.Tables[0].Rows[0]["UserId"]) : SafeValue<int>(ds.Tables[0].Rows[0]["ParentUserId"]);
                        string ImageName = SafeValue<string>(ds.Tables[0].Rows[0]["ImageName"]);
                        objDoctorDetails.Dentrixprovid = SafeValue<string>(ds.Tables[0].Rows[0]["DentrixProviderId"]);
                        objDoctorDetails.ProviderType = (ds.Tables[0].Rows[0]["provtype"] is DBNull) ? BO.Enums.Common.PMSProviderType.Provider : ((SafeValue<int>(ds.Tables[0].Rows[0]["provtype"]) == 0 || SafeValue<int>(ds.Tables[0].Rows[0]["provtype"]) == 1) ? BO.Enums.Common.PMSProviderType.Provider : BO.Enums.Common.PMSProviderType.Staff);
                        if (string.IsNullOrEmpty(ImageName))
                        {
                            objDoctorDetails.ImageUrl = ConfigurationManager.AppSettings.Get("DoctorProfileImage");
                        }
                        else
                        {
                            objDoctorDetails.ImageUrl = DoctorImage + ImageName;

                        }
                        objDoctorDetails.Description = SafeValue<string>(ds.Tables[0].Rows[0]["Description"]);
                        objDoctorDetails.Education = string.Join(", ", ds.Tables[4].Rows.OfType<DataRow>().Select(r => r[1].ToString()));
                        objDoctorDetails.Specialties = SafeValue<string>(ds.Tables[7].Rows[0]["Speciality"]);
                    }
                    if (ds.Tables[8].Rows.Count > 0)
                    {
                        objDoctorDetails.AccountId = SafeValue<int>(ds.Tables[8].Rows[0]["AccountID"]);
                    }
                    //PRCSP-411 for Account Name
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        objDoctorDetails.AccountName = SafeValue<string>(ds.Tables[1].Rows[0]["AccountName"]);
                    }
                    //PRCSP-485 Locations - as shown in the profile as an array
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        objDoctorDetails.Addresses = GetDoctorAddressDetailsById(ds.Tables[2]);
                        objDoctorDetails.Phone =SafeValue<string>(ds.Tables[2].Rows[0]["Phone"]);
                    }
                    if (objDoctorDetails.Addresses.Count > 0)
                    {
                        // BO.ViewModel.Addresses address = objDoctorDetails.Addresses.First(x => x.AddressType == "1");
                        //Ankit Here: 05-09-2018 PRCSP-390
                        //Add Reward Platform LocationId on Output JSON 
                        if (ds.Tables[19].Rows.Count > 0)
                        {
                            foreach (DataRow item in ds.Tables[19].Rows)
                            {
                                var myobj = objDoctorDetails.Addresses.Where(t => t.AddressId == SafeValue<int>(item["LocationId"])).FirstOrDefault();
                                if (myobj != null)
                                {
                                    objDoctorDetails.Addresses.Where(t => t.AddressId == SafeValue<int>(item["LocationId"])).FirstOrDefault().LocationId = SafeValue<int>(item["RewardLocationId"]);
                                }
                            }
                        }
                        // address.AddressType = "Primary";
                        // objDoctorDetails.Addresses.Clear();
                        // objDoctorDetails.Addresses.Add(address);
                    }
                    if (ds.Tables[3].Rows.Count > 0)
                    {
                        objDoctorDetails.SocialMediaWebsites = GetSocialMediaDetailByUserId(ds.Tables[3]);
                    }
                    if (ds.Tables[5].Rows.Count > 0)
                    {
                        objDoctorDetails.ProfessionalMemberships = GetProfessionalMembershipsByUserId(ds.Tables[5]);
                    }
                    if (ds.Tables[6].Rows.Count > 0)
                    {
                        objDoctorDetails.SpecialitesDetails = GetSpeacilitiesOfDoctorById(ds.Tables[6]);
                    }
                    if (ds.Tables[11].Rows.Count > 0)
                    {
                        objDoctorDetails.PracticeWebsites = GetSecondaryWebsitelistByUserId(ds.Tables[11]);
                    }
                    if (ds.Tables[4].Rows.Count > 0)
                    {
                        objDoctorDetails.EducationDetails = GetEducationandTrainingDetailsForDoctor(ds.Tables[4]);
                    }
                    if (!Hasaccount)
                    {
                        objDoctorDetails.TeamMembers = GetTeamMemberDetailsOfDoctor(objDoctorDetails.DoctorId, 1, 15);
                    }
                    if (Hasaccount)
                    {
                        if (ds.Tables[18].Rows.Count > 0)
                        {
                            objDoctorDetails.DentixDetails = GetDentrixDetailsofDoctor(ds.Tables[18]);
                        }
                    }
                    if (ds.Tables[16].Rows.Count > 0)
                    {
                        objDoctorDetails.OrganizationId = SafeValue<int>(ds.Tables[16].Rows[0]["RewardCompanyID"]);
                    }
                    if (ds.Tables[19].Rows.Count > 0)
                    {
                        objDoctorDetails.OrganizationLocationId = SafeValue<int>(ds.Tables[19].Rows[0]["RewardLocationId"]);
                        objDoctorDetails.RecordlincLocationId = 0;
                        objDoctorDetails.RecordlincLocationId = SafeValue<int>(ds.Tables[19].Rows[0]["LocationId"]);
                    }

                    //PRCSP-511
                    DataSet ds1 = new DataSet();
                    ds1 = objColleaguesData.GetSecondaryWebsitelistByParentUserId(DoctorId);
                    if (ds1.Tables.Count > 0)
                    {
                        if (ds1 != null && ds1.Tables[0].Rows.Count > 0)
                        {
                            objDoctorDetails.PracticeWebsites = GetSecondaryWebsitelistByParentUserId(ds1.Tables[0], objDoctorDetails.PracticeWebsites);
                        }
                    }
                }
                return objDoctorDetails;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---GetDoctorDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        #region Code For DoctorAddressDetails Details
        //ANkit Here : 05-09-2018
        //Get Solution one Location Ids of Dentist location.
        public static int GetRewardPartnerLocationId(AddressesModel Obj, DataTable dt)
        {
            try
            {
                int LocationId = 0;
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        if (Obj.AddressId == SafeValue<int>(item["LocationId"]))
                        {
                            LocationId = SafeValue<int>(item["RewardLocationId"]);
                        }
                    }
                }
                return LocationId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---GetRewardPartnerLocationId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static List<AddressesModel> GetDoctorAddressDetailsById(DataTable dt)
        {
            List<BO.ViewModel.AddressesModel> lstDoctorAddressDetails = new List<BO.ViewModel.AddressesModel>();
            clsCommon objCommon = new clsCommon();
            try
            {
                clsColleaguesData objColleaguesData = new clsColleaguesData();
                if (dt != null && dt.Rows.Count > 0)
                {
                    lstDoctorAddressDetails = (from p in dt.AsEnumerable()
                                               select new BO.ViewModel.AddressesModel
                                               {
                                                   AddressId = SafeValue<int>(p["AddressInfoID"]),
                                                   StreetAddress = SafeValue<string>(p["ExactAddress"]),
                                                   City = SafeValue<string>(p["City"]),
                                                   State = SafeValue<string>(p["State"]),
                                                   Country = SafeValue<string>(p["Country"]),
                                                   Zipcode = SafeValue<string>(p["ZipCode"]), 
                                                   Phone = objCommon.ConvertPhoneNumber(SafeValue<string>(p["Phone"])),
                                                   AddressType = SafeValue<string>(p["ContactType"]),
                                                   Email = SafeValue<string>(p["EmailAddress"]), 
                                               }).ToList();
                }
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("PatientRewardBLL---GetDoctorAddressDetailsById", ex.Message, ex.StackTrace);
                throw;
            }

            return lstDoctorAddressDetails;
        }
        #endregion

        #region Code For Social Media Details
        public static List<SocialMediaWebsites> GetSocialMediaDetailByUserId(DataTable dt)
        {

            List<SocialMediaWebsites> lstGetSocialMediaDetailByUserId = new List<SocialMediaWebsites>();
            clsCommon objCommon = new clsCommon();
            try
            {
                #region Start New

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataColumn col in dt.Columns)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            if (!string.IsNullOrWhiteSpace(SafeValue<string>(row[col.ColumnName])))
                            {
                                lstGetSocialMediaDetailByUserId.Add(new SocialMediaWebsites()
                                {
                                    SocialMediaWebsiteId = SafeValue<string>(col.ColumnName),
                                    SocialMediaWebsiteUrl = SafeValue<string>(row[col.ColumnName]) != string.Empty ? (SafeValue<string>(row[col.ColumnName]).Contains("http://") || SafeValue<string>(row[col.ColumnName]).Contains("https://") ? SafeValue<string>(row[col.ColumnName]) : "http://" + SafeValue<string>(row[col.ColumnName])) : SafeValue<string>(row[col.ColumnName]),
                                });
                            }
                        }
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("PatientRewardBLL---GetSocialMediaDetailByUserId", ex.Message, ex.StackTrace);
                throw;
            }

            return lstGetSocialMediaDetailByUserId;
        }
        #endregion

        #region Code For ProfessionalMemberships Details
        public static List<ProfessionalMembershipsDetails> GetProfessionalMembershipsByUserId(DataTable dt)
        {
            List<ProfessionalMembershipsDetails> lstProfessionalMemberships = new List<ProfessionalMembershipsDetails>();
            clsCommon objCommon = new clsCommon();
            try
            {

                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(SafeValue<string>(dt.Rows[i]["Membership"])))
                        {
                            if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[i]["Membership"])))
                            {
                                lstProfessionalMemberships.Add(new ProfessionalMembershipsDetails { MembershipId = SafeValue<int>(dt.Rows[i]["Id"]), Membership = SafeValue<string>(dt.Rows[i]["Membership"]), });
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("PatientRewardBLL---GetProfessionalMembershipsByUserId", ex.Message, ex.StackTrace);
                throw;
            }
            finally
            {
                dt.Dispose(); dt.Clear();
            }

            return lstProfessionalMemberships;
        }
        #endregion

        #region Code For SpeacilitiesOfDoctor Details
        public static List<SpecialitesDetails> GetSpeacilitiesOfDoctorById(DataTable dt)
        {
            List<SpecialitesDetails> lstSpeacilities = new List<SpecialitesDetails>();
            clsCommon objCommon = new clsCommon();
            try
            {
                if (dt.Rows.Count > 0)
                {
                    lstSpeacilities = (from p in dt.AsEnumerable()
                                       select new SpecialitesDetails
                                       {
                                           SpecialtyId = int.Parse(!string.IsNullOrWhiteSpace(SafeValue<string>(p["SpecialityId"])) ? SafeValue<string>(p["SpecialityId"]) : "0" ),
                                           SpecialtyDescription = SafeValue<string>(p["Specialities"])
                                       }).ToList();


                }
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("PatientRewardBLL---GetSpeacilitiesOfDoctorById", ex.Message, ex.StackTrace);
                throw;
            }
            finally
            {
                dt.Dispose(); dt.Clear();
            }
            return lstSpeacilities;

        }

        #endregion

        #region Code For WebSite Details
        public static List<PracticeWebsites> GetSecondaryWebsitelistByUserId(DataTable dt)
        {
            clsCommon objCommon = new clsCommon();
            List<PracticeWebsites> lstsecondarywebsitelist = new List<PracticeWebsites>();
            try
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["SecondaryWebsite"])))
                        {
                            lstsecondarywebsitelist.Add(new PracticeWebsites()
                            {
                                PracticeWebsiteId = SafeValue<int>(row["SecondaryId"]),
                                PracticeWebsiteUrl = SafeValue<string>(row["SecondaryWebsite"]),
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("PatientRewardBLL---GetSecondaryWebsitelistByUserId", ex.Message, ex.StackTrace);
                throw;
            }
            return lstsecondarywebsitelist;

        }

        public static List<PracticeWebsites> GetSecondaryWebsitelistByParentUserId(DataTable dt, List<PracticeWebsites> lstPrac)
        {
            clsCommon objCommon = new clsCommon();
            try
            {
                foreach (DataRow row in dt.Rows)
                {
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["SecondaryWebsite"])))
                    {
                        lstPrac.Add(new PracticeWebsites()
                        {
                            PracticeWebsiteId = SafeValue<int>(row["SecondaryId"]),
                            PracticeWebsiteUrl =SafeValue<string>(row["SecondaryWebsite"]),
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("PatientRewardBLL---GetSecondaryWebsitelistByParentUserId", ex.Message, ex.StackTrace);
                throw;
            }
            return lstPrac;

        }
        #endregion

        #region Code For EducationandTraining Details
        public static List<EducationDetails> GetEducationandTrainingDetailsForDoctor(DataTable dt)
        {
            clsCommon objCommon = new clsCommon();
            List<EducationDetails> lstEducationandTraining = new List<EducationDetails>();
            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(SafeValue<string>(dt.Rows[i]["Institute"])))
                        {
                            lstEducationandTraining.Add(new EducationDetails { EducationId = SafeValue<int>(dt.Rows[i]["Id"]), Institute = SafeValue<string>(dt.Rows[i]["Institute"]),Degree = SafeValue<string>(dt.Rows[i]["Specialisation"]), StartYear = SafeValue<string>(dt.Rows[i]["YearAttended"]) });
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("PatientRewardBLL---GetEducationandTrainingDetailsForDoctor", ex.Message, ex.StackTrace);
                throw;
            }
            finally
            {
                dt.Dispose(); dt.Clear();
            }
            return lstEducationandTraining;
        }
        #endregion

        #region Code For TeamMemberDetailsOfDoctor Details
        public static List<TeamMembers> GetTeamMemberDetailsOfDoctor(int ParentUserId, int PageIndex, int PageSize)
        {
            DataTable dt = new DataTable();
            clsCommon objCommon = new clsCommon();
            clsColleaguesData objColleaguesData = new clsColleaguesData();
            List<TeamMembers> lstTeamMemberDetailsForDoctor = new List<TeamMembers>();
            try
            {
                int UserId = SafeValue<int>(!string.IsNullOrWhiteSpace(SafeValue<string>(HttpContext.Current.Session["UserId"])) ? HttpContext.Current.Session["UserId"] : 0);
                dt = objColleaguesData.GetTeamMemberDetailsOfDoctor(ParentUserId, PageIndex, PageSize);
                if (dt != null && dt.Rows.Count > 0)
                {
                    lstTeamMemberDetailsForDoctor = (from p in dt.AsEnumerable()
                                                     select new TeamMembers
                                                     {
                                                         DoctorId = SafeValue<int>(p["UserId"]),
                                                         Name = SafeValue<string>(p["FirstName"]) + " " + SafeValue<string>(p["LastName"]),
                                                     }).Where(x => x.DoctorId != UserId).ToList();
                }
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("PatientRewardBLL---GetEducationandTrainingDetailsForDoctor", ex.Message, ex.StackTrace);
                throw;
            }
            finally
            {
                dt.Dispose(); dt.Clear();
            }
            return lstTeamMemberDetailsForDoctor;
        }
        #endregion

        public static Reward PatientReferral(PatientReferral Obj)
        {
            try
            {
                Reward Objs = new Reward();
                Objs.RewardId = clsPatientsData.InsertPatientReferral(Obj);
                return Objs;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---PatientReferral", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool RecordlincPMSAssociation(PatientAssociation Obj)
        {
            try
            {
                return clsPatientsData.RecordlincPMSAssociation(Obj);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---RecordlincPMSAssociation", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static int CheckRewardPartnerAssociatedId(PatientAssociation Obj)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = clsPatientsData.CheckRewardPartnerIdassociation(Obj);
                int PatientId = 0;
                if (dt != null && dt.Rows.Count > 0)
                {
                    PatientId = SafeValue<int>(dt.Rows[0]["PatientId"]);
                }
                return PatientId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---CheckRewardPartnerAssociatedId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static List<DeletedProc> DeleteDentrixPatientProcedure(List<RemoveProcedure> Lst)
        {
            try
            {
                List<DeletedProc> DeletedList = new List<DeletedProc>();
                if(CheckDentrixConnectorKeyExists(Lst[0].DentrixConnectorId))
                {
                    foreach (var item in Lst)
                    {
                        DeletedList.Add(new DeletedProc()
                        {
                            DentrixId = item.DentrixId,
                            IsDeleted = clsPatientsData.DeletePatientProcedure(item)
                        });
                    }
                }
                return DeletedList;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---DeleteDentrixPatientProcedure", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool CheckRegistration(CreateRewards obj)
        {
            try
            {
                if (obj.RewardTypeCode == "RR")
                {
                    return clsPatientsData.checkPatientPartnerID(obj);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---CheckRegistration", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static RewardResponse InsertAppointmentsPatient(List<Appointments> Obj)
        {
            RewardResponse Response = new RewardResponse();
            Response.TotalInsertedRecord = 0;
            Response.TotalNotInsertedRecord = 0;
            Response.ReceivedRecords = Obj.Count;
            List<InsertedRecord> lst = new List<InsertedRecord>();
            List<NotInsertedRecord> Lst = new List<NotInsertedRecord>();
            if(CheckDentrixConnectorKeyExists(Obj[0].dentrixConnectorID))
            {
                string DentrixConnectorKey = string.Empty;
                for (int i = 0; i < Obj.Count; i++)
                {
                    try
                    {
                        if (SafeValue<DateTime>(Obj[i].BrokenDate).ToShortDateString() == DateTime.MinValue.ToShortDateString() && SafeValue<DateTime>(Obj[i].AppointmentDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            RemoveAppointment Obj1 = new RemoveAppointment();
                            Obj1.DentrixId = Obj[i].AppointmentId;
                            Obj1.DentrixConnectorId = Obj[i].dentrixConnectorID;
                            bool result = clsPatientsData.DeletePatientAppointment(Obj1);
                            clsPatientsData.DeleteDentrixAppointmentDetails(Obj1);
                            NotInsertedRecord UnInserted = new NotInsertedRecord();
                            UnInserted.DentrixId = Obj[i].AppointmentId;
                            UnInserted.ErrorMessage = "This Appointment is deleted as it has null values on Schedule date & Broken date";
                            Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                            Lst.Add(UnInserted);
                            Response.NotInsertedList = Lst;
                        }
                        else
                        {
                            int DentrixApptId = clsPatientsData.InsertAppointment_DentrixDetails(Obj[i]);
                            int NewAppointmentId = clsPatientsData.InsertUpdatePatientAppointment(Obj[i]);
                            if (NewAppointmentId > 0)
                            {
                                bool IsMapping = clsPatientsData.InsertDataAppointMentMapping(NewAppointmentId, Obj[i].TxProcs);
                                InsertedRecord Inserted = new InsertedRecord();
                                Response.TotalInsertedRecord = Response.TotalInsertedRecord + 1;
                                Inserted.RLId = NewAppointmentId;
                                Inserted.DentrixId = Obj[i].AppointmentId;
                                lst.Add(Inserted);
                                Response.InsertedList = lst;
                            }
                            else if (NewAppointmentId == -1)
                            {
                                NotInsertedRecord UnInserted = new NotInsertedRecord();
                                UnInserted.DentrixId = Obj[i].AppointmentId;
                                UnInserted.ErrorMessage = "Provider doesn't exist on the system!";
                                Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                                Lst.Add(UnInserted);
                                Response.NotInsertedList = Lst;
                            }
                            else
                            {
                                NotInsertedRecord UnInserted = new NotInsertedRecord();
                                UnInserted.DentrixId = Obj[i].AppointmentId;
                                UnInserted.ErrorMessage = "Invalid Dentrix ConnectorID";
                                Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                                Lst.Add(UnInserted);
                                Response.NotInsertedList = Lst;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        new clsCommon().InsertErrorLog("PatientRewardBLL---InsertAppointmentsPatient", ex.Message, ex.StackTrace);
                        Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                        NotInsertedRecord UnInserted = new NotInsertedRecord();
                        UnInserted.DentrixId = Obj[i].AppointmentId;
                        UnInserted.ErrorMessage = ex.Message;
                        Lst.Add(UnInserted);
                        Response.NotInsertedList = Lst;
                    }
                }
            }
            else
            {

                Response.TotalNotInsertedRecord = Obj.Count;
                Response.NotInsertedList = new List<NotInsertedRecord>();
                foreach (var item in Obj)
                {
                    Response.NotInsertedList.Add(new NotInsertedRecord()
                    {
                        DentrixId = item.AppointmentId,
                        ErrorMessage = "Invalid Dentrix ConnectorID"
                    });
                }
            }


            return Response;
        }

        public static List<DeletedRecord> DeletePatientAppointment(List<RemoveAppointment> Obj)
        {
            try
            {
                List<DeletedRecord> DeletedList = new List<DeletedRecord>();
                if(CheckDentrixConnectorKeyExists(Obj[0].DentrixConnectorId))
                {
                    foreach (var item in Obj)
                    {
                        DeletedList.Add(new DeletedRecord()
                        {
                            DentrixId = item.DentrixId,
                            IsDeleted = clsPatientsData.DeletePatientAppointment(item)
                        });
                    }
                }
                return DeletedList;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---DeletePatientAppointment", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static List<DeletePatient> DeletePatientFromDentrix(List<RemovePatient> Obj)
        {
            try
            {
                List<DeletePatient> DeletedList = new List<DeletePatient>();
                if(CheckDentrixConnectorKeyExists(Obj[0].dentrixConnectorId))
                {
                    foreach (var item in Obj)
                    {
                        int x = clsPatientsData.DeletePatientFromDentrix(item);
                        if (x > 0 && x != 2)
                        {
                            DeletedList.Add(new DeletePatient()
                            {
                                DentrixId = item.DentrixId,
                                IsDeleted = true,
                            });
                        }
                        else
                        {
                            DeletedList.Add(new DeletePatient()
                            {
                                DentrixId = item.DentrixId,
                                IsDeleted = false,
                            });
                        }
                    }
                }
                return DeletedList;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---DeletePatientAppointment", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// This Method Get Patient List of particular DentrixConnectorId..
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public static RetriveResponse<List<DentrixPatient>> GetPatientList(RetrievePatient Obj)
        {
            try
            {
                RetriveResponse<List<DentrixPatient>> Retrieve = new RetriveResponse<List<DentrixPatient>>();
                //Check Dentrix Connector Id is Available on database or not.
                if (CheckDentrixConnectorKeyExists(Obj.PMSConnectorId))
                {
                    DataTable dt = new DataTable();
                    int RequestId = 0;
                    int TotalRecord = 0;
                    if (!string.IsNullOrWhiteSpace(Obj.RequestId))
                    {
                        //Get Request Id from DPMS_Sync Timing table as Request id pass over here.
                        RequestId = clsColleaguesData.GetPMSSyncDetailsByGUID(Obj.RequestId);
                        Retrieve.RequestId = Obj.RequestId;
                    }
                    else
                    {
                        //Insert Request on DPMS_SyncTiming table as Request Id Doesn't pass over here.
                        IDictionary<int, string> Response = InsertDPMS_SyncDetails(Obj.PMSConnectorId, "PATIENT");
                        RequestId = Response.Keys.FirstOrDefault();
                        Retrieve.RequestId = Response.Values.FirstOrDefault();
                        //1) if RequestId Doesn't exist on DPMSQueueProcess table then Insert All Patient of RL and Last Modified Dentrix Patient on it. 
                        //2) if Request Id Exist then check Max(Request Time) on DPMS_Synctiming and check greater Patient modified.
                        //3) If Patient Has Filed on any request with in 3 time and Past 12 hour then it will also added on that List.
                        bool result = clsColleaguesData.GetPatientListForDPMS(Obj.PMSConnectorId, RequestId, Obj.SendAll);
                    }
                    if (RequestId > 0)
                    {

                        //Get Patient List of Patient table using process Queue table.
                        dt = clsColleaguesData.GetPatientList(Obj, RequestId);
                        List<DentrixPatient> Lst = new List<DentrixPatient>();
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            foreach (DataRow item in dt.Rows)
                            {
                                try
                                {
                                    List<PhoneNumbers> ObjPhoneNumbers = new List<PhoneNumbers>();
                                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(item["WorkPhone"])))
                                    {
                                        ObjPhoneNumbers.Add(new PhoneNumbers() { PhoneType = 0, PhoneNumber = SafeValue<string>(item["WorkPhone"]).Trim() });
                                    }
                                    List<BO.Models.Addresses> Address = new List<BO.Models.Addresses>();
                                    string Email = SafeValue<string>(item["Email"]);

                                    //Getting Patient Address on the XML Query if XMLQUERY is not null. 
                                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(item["XMLQUERY"])))
                                    {
                                        DataTable dtss = stam(SafeValue<string>(item["XMLQUERY"]));
                                        Address.Add(new BO.Models.Addresses()
                                        {
                                            DentrixId = (string.IsNullOrWhiteSpace(SafeValue<string>(dtss.Rows[0]["DentrixAddressInfoId"]))) ? string.Empty : SafeValue<string>(dtss.Rows[0]["DentrixAddressInfoId"]).Trim(),
                                            Street1 = (string.IsNullOrWhiteSpace(SafeValue<string>(dtss.Rows[0]["ExactAddress"]))) ? string.Empty : SafeValue<string>(dtss.Rows[0]["ExactAddress"]).Trim(),
                                            Street2 = (string.IsNullOrWhiteSpace(SafeValue<string>(dtss.Rows[0]["Address2"]))) ? string.Empty : SafeValue<string>(dtss.Rows[0]["Address2"]).Trim(),
                                            City = (string.IsNullOrWhiteSpace(SafeValue<string>(dtss.Rows[0]["City"]))) ? string.Empty : SafeValue<string>(dtss.Rows[0]["City"]).Trim(),
                                            State = (string.IsNullOrWhiteSpace(SafeValue<string>(dtss.Rows[0]["State"]))) ? string.Empty : SafeValue<string>(dtss.Rows[0]["State"]).Trim(),
                                            ZipCode = (string.IsNullOrWhiteSpace(SafeValue<string>(dtss.Rows[0]["ZipCode"]))) ? string.Empty : SafeValue<string>(dtss.Rows[0]["ZipCode"]).Trim()
                                        });
                                        if (!string.IsNullOrWhiteSpace(SafeValue<string>(dtss.Rows[0]["Phone"])))
                                        {
                                            ObjPhoneNumbers.Add(new PhoneNumbers() { PhoneType = 2, PhoneNumber = SafeValue<string>(dtss.Rows[0]["Phone"]).Trim() });
                                        }
                                        if (!string.IsNullOrWhiteSpace(SafeValue<string>(dtss.Rows[0]["Phone2"])))
                                        {
                                            ObjPhoneNumbers.Add(new PhoneNumbers() { PhoneType = 3, PhoneNumber = SafeValue<string>(dtss.Rows[0]["Phone2"]).Trim() });
                                        }
                                    }
                                    Lst.Add(new DentrixPatient()
                                    {
                                        RL_Id = (string.IsNullOrWhiteSpace(SafeValue<string>(item["PatientId"]))) ? 0 : SafeValue<int>(item["PatientId"]),
                                        FirstName = (string.IsNullOrWhiteSpace(SafeValue<string>(item["FirstName"]))) ? string.Empty : SafeValue<string>(item["FirstName"]).Trim(),
                                        LastName = (string.IsNullOrWhiteSpace(SafeValue<string>(item["LastName"]))) ? string.Empty : SafeValue<string>(item["LastName"]).Trim(),
                                        MiddleName = (string.IsNullOrWhiteSpace(SafeValue<string>(item["MiddelName"]))) ? string.Empty : SafeValue<string>(item["MiddelName"]).Trim(),
                                        gender = (!string.IsNullOrWhiteSpace(SafeValue<string>(item["Gender"]))) ? SafeValue<int>(item["Gender"]) : 0,
                                        isguar = (string.IsNullOrWhiteSpace(SafeValue<string>(item["isguar"]))) ? false : SafeValue<bool>(item["isguar"]),
                                        patid = (string.IsNullOrWhiteSpace(SafeValue<string>(item["AssignedPatientId"]))) ? 0 : SafeValue<int>(item["AssignedPatientId"]),
                                        patguid = (!string.IsNullOrWhiteSpace(SafeValue<string>(item["patguid"]))) ? SafeValue<string>(item["patguid"]) : SafeValue<string>(item["patguid"]),
                                        isinssubscriber = (string.IsNullOrWhiteSpace(SafeValue<string>(item["isinssubscriber"]))) ? false : SafeValue<bool>(item["isinssubscriber"]),
                                        status = (string.IsNullOrWhiteSpace(SafeValue<string>(item["Status"]))) ? 0 : SafeValue<int>(item["Status"]),
                                        primprovid = SafeValue<string>(item["DentrixProviderId"]),
                                        automodifiedtimestamp = (!string.IsNullOrWhiteSpace(SafeValue<string>(item["automodifiedtimestamp"]))) ? SafeValue<DateTime>(item["automodifiedtimestamp"]) : (DateTime?)null,
                                        FirstVisitDate = (!string.IsNullOrWhiteSpace(SafeValue<string>(item["firstvisitdate"]))) ? SafeValue<DateTime>(item["firstvisitdate"]) : (DateTime?)null,
                                        LastVisitDate = (!string.IsNullOrWhiteSpace(SafeValue<string>(item["lastvisitdate"]))) ? SafeValue<DateTime>(item["lastvisitdate"]) : (DateTime?)null,
                                        secprovid = SafeValue<string>(item["secprovid(OwnerId)"]),
                                        birthdate = (!string.IsNullOrWhiteSpace(SafeValue<string>(item["DateOfBirth"]))) ? SafeValue<DateTime>(item["DateOfBirth"]) : (DateTime?)null,
                                        Emails = (!string.IsNullOrWhiteSpace(Email)) ? SafeValue<string>(item["Email"]).Split(new char[] { ',' }).ToList() : new List<String>(),
                                        PhoneNumbers = (ObjPhoneNumbers != null) ? ObjPhoneNumbers : null,
                                        Addresses = (Address != null) ? Address : null,
                                        BillingType = (string.IsNullOrWhiteSpace(SafeValue<string>(item["BillingType"]))) ? string.Empty : SafeValue<string>(item["BillingType"]).Trim(),
                                        dentrixConnectorID = (string.IsNullOrWhiteSpace(SafeValue<string>(item["DentrixConnectorId"]))) ? string.Empty : SafeValue<string>(item["DentrixConnectorId"]).Trim(),
                                        Userid = SafeValue<int>(item["UserId"]),
                                        RowNumber = SafeValue<int>(item["RowNumber"])
                                    });
                                    TotalRecord = SafeValue<int>(item["TotalRecord"]);
                                }
                                catch (Exception Ex)
                                {
                                    new clsCommon().InsertErrorLog("PatientRewardBLL---GetPatientList--Inner for each loop time error.", Ex.Message, Ex.StackTrace);
                                }
                            }
                        }
                        clsColleaguesData.UpdateDPMS_SyncDetails(RequestId);
                        Retrieve.Message = "";
                        Retrieve.Result = Lst;
                        Retrieve.StatusCode = 200;
                        Retrieve.IsSuccess = true;
                        Retrieve.TotalRecord = TotalRecord;
                        return Retrieve;
                    }
                    else
                    {
                        Retrieve.Message = "Invalid RequestId.";
                        Retrieve.IsSuccess = false;
                        Retrieve.StatusCode = 404;
                        return Retrieve;
                    }
                }
                else
                {
                    Retrieve.Message = "Invalid PMSConnectorId.";
                    Retrieve.IsSuccess = false;
                    Retrieve.StatusCode = 400;
                    return Retrieve;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---GetPatientList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        //Ankit Here 08-07-2018 : Changes for SBI-35 related.
        public static IDictionary<int, string> InsertDPMS_SyncDetails(string DentrixConnectorId, string RequestType, string GUID = null)
        {
            try
            {
                Dictionary<int, string> Response = new Dictionary<int, string>();
                DPMS_SyncDetails DPMS = new DPMS_SyncDetails();
                DPMS.DentrixConnectorId = DentrixConnectorId;
                DPMS.RequestType = RequestType;
                DPMS.RequestTime = SafeValue<DateTime>(SqlDateTime.MinValue.ToString());
                DPMS.GUID = (string.IsNullOrWhiteSpace(GUID)) ? SafeValue<string>(Guid.NewGuid()) : GUID;
                Response.Add(clsColleaguesData.InsertDPMS_SyncDetails(DPMS), DPMS.GUID);
                return Response;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---InsertDPMS_SyncDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// This Method Update the Patient Record which are inserted on Dentrix and update that Dentrix Id into Recordlinc.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public static RetriveResponse<bool> CheckPatientResponseData(DPMS_Response Obj)
        {
            try
            {
                RetriveResponse<bool> Retrieve = new RetriveResponse<bool>();
                int RequestId = 0;
                if (PatientRewardBLL.CheckDentrixConnectorKeyExists(Obj.PMSConnectorId))
                {
                    if (!string.IsNullOrWhiteSpace(Obj.RequestId))
                    {
                        //Get Request Id from DPMS_Sync Timing table as Request id pass over here.
                        RequestId = clsColleaguesData.GetPMSSyncDetailsByGUID(Obj.RequestId);
                        bool Result = clsPatientsData.DPMSResponseUpdate(Obj.Result, RequestId);
                        Retrieve.Message = "";
                        Retrieve.IsSuccess = true;
                        Retrieve.Result = true;
                        Retrieve.StatusCode = 200;
                        return Retrieve;
                    }
                    else
                    {
                        Retrieve.Message = "Invalid RequestId.";
                        Retrieve.IsSuccess = false;
                        Retrieve.StatusCode = 404;
                        return Retrieve;
                    }
                }
                else
                {
                    Retrieve.Message = "Invalid PMSConnectorId.";
                    Retrieve.IsSuccess = false;
                    Retrieve.StatusCode = 400;
                    return Retrieve;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---CheckPatientResponseData", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This Method Convert the XML column to Datatable.
        /// </summary>
        /// <param name="xmlData"></param>
        /// <returns></returns>
        public static DataTable stam(string xmlData)
        {
            XElement x = XElement.Parse(xmlData);

            DataTable dt = new DataTable();

            XElement setup = (from p in x.Descendants() select p).First();
            foreach (XElement xe in setup.Descendants()) // build your DataTable
                dt.Columns.Add(new DataColumn(xe.Name.ToString(), typeof(string))); // add columns to your dt

            var all = from p in x.Descendants(setup.Name.ToString()) select p;
            foreach (XElement xe in all)
            {
                DataRow dr = dt.NewRow();
                foreach (XElement xe2 in xe.Descendants())
                    dr[xe2.Name.ToString()] = xe2.Value; //add in the values
                dt.Rows.Add(dr);
            }
            return dt;

        }

        //public static RetriveResponse<List<Appointments>> GetAppointmentData(RetrieveAppointment Obj)
        //{
        //    try
        //    {

        //    }
        //    catch (Exception Ex)
        //    {
        //        new clsCommon().InsertErrorLog("PatientRewardBLL---GetAppointmentData", Ex.Message, Ex.StackTrace);
        //        throw;
        //    }
        //}


        #region This region has Insert Dental Forms related details we can save using Dentrix side.

        /// <summary>
        /// This Method Used for Insert Patient Personal Information 
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public static bool PatientInformation(string PatientId, Reward_PatientUpdate Obj)
        {
            clsCommon objCommon = new clsCommon();
            object obj = string.Empty;
            try
            {
                DataTable dtRegi = new DataTable();
                dtRegi = new clsPatientsData().GetRegistration(SafeValue<int>(PatientId), "GetRegistrationform");
                string ResponsiblepartyFname = string.Empty; string ResponsiblepartyLname = string.Empty;
                string ResponsibleRelationship = string.Empty; string ResponsibleAddress = string.Empty; string ResponsibleCity = string.Empty;
                string ResponsibleState = string.Empty; string ResponsibleZipCode = string.Empty; string ResponsibleDOB = string.Empty;
                string ResponsibleContact = string.Empty; string Whommay = string.Empty; string Emailaddress = string.Empty;
                string EmployeeName1 = string.Empty; string EmployeeDob1 = string.Empty; string EmployerName1 = string.Empty;
                string YearsEmployed1 = string.Empty; string NameofInsurance1 = string.Empty; string InsuranceAddress1 = string.Empty;
                string InsuranceTelephone1 = string.Empty; string EmployeeName2 = string.Empty; string EmployeeDob2 = string.Empty;
                string EmployerName2 = string.Empty; string YearsEmployed2 = string.Empty; string NameofInsurance2 = string.Empty;
                string InsuranceAddress2 = string.Empty; string InsuranceTelephone2 = string.Empty; string GroupNumber1 = string.Empty;
                string emergencyname = string.Empty; string emergency = string.Empty;
                string GroupNumber2 = string.Empty;
                emergencyname = (!string.IsNullOrWhiteSpace(Obj.EmergencyContactName)) ? Obj.EmergencyContactName : "";
                emergency = (!string.IsNullOrWhiteSpace(Obj.EmergencyContactPhoneNumber)) ? Obj.EmergencyContactPhoneNumber : "";
                if (dtRegi != null && dtRegi.Rows.Count > 0)
                {
                    ResponsiblepartyFname = SafeValue<string>(dtRegi.Rows[0]["txtResponsiblepartyFname"]);
                    ResponsiblepartyLname = SafeValue<string>(dtRegi.Rows[0]["txtResponsiblepartyLname"]);
                    ResponsibleRelationship = SafeValue<string>(dtRegi.Rows[0]["txtResponsibleRelationship"]);
                    ResponsibleAddress = SafeValue<string>(dtRegi.Rows[0]["txtResponsibleAddress"]);
                    ResponsibleZipCode = SafeValue<string>(dtRegi.Rows[0]["txtResponsibleZipCode"]);
                    ResponsibleCity = SafeValue<string>(dtRegi.Rows[0]["txtResponsibleCity"]);
                    ResponsibleState = SafeValue<string>(dtRegi.Rows[0]["txtResponsibleState"]);
                    ResponsibleContact = SafeValue<string>(dtRegi.Rows[0]["txtResponsibleContact"]);
                    EmployeeName1 = SafeValue<string>(dtRegi.Rows[0]["txtEmployeeName1"]);
                    EmployeeDob1 = SafeValue<string>(dtRegi.Rows[0]["txtEmployeeDob1"]);
                    EmployerName1 = SafeValue<string>(dtRegi.Rows[0]["txtEmployerName1"]);
                    NameofInsurance1 = SafeValue<string>(dtRegi.Rows[0]["txtNameofInsurance1"]);
                    InsuranceAddress1 = SafeValue<string>(dtRegi.Rows[0]["txtInsuranceAddress1"]);
                    InsuranceTelephone1 = SafeValue<string>(dtRegi.Rows[0]["txtInsurancePhone1"]);
                    EmployeeName2 = SafeValue<string>(dtRegi.Rows[0]["txtEmployeeName2"]);
                    EmployeeDob2 = SafeValue<string>(dtRegi.Rows[0]["txtEmployeeDob2"]);
                    EmployerName2 = SafeValue<string>(dtRegi.Rows[0]["txtEmployerName2"]);
                    NameofInsurance2 = SafeValue<string>(dtRegi.Rows[0]["txtNameofInsurance2"]);
                    YearsEmployed2 = SafeValue<string>(dtRegi.Rows[0]["txtYearsEmployed2"]);
                    InsuranceAddress2 = SafeValue<string>(dtRegi.Rows[0]["txtInsuranceAddress2"]);
                    InsuranceTelephone2 = SafeValue<string>(dtRegi.Rows[0]["txtInsurancePhone2"]);
                    GroupNumber1 = SafeValue<string>(dtRegi.Rows[0]["txtInsuranceTelephone1"]);
                    GroupNumber2 = SafeValue<string>(dtRegi.Rows[0]["txtInsuranceTelephone2"]);
                    Emailaddress = SafeValue<string>(dtRegi.Rows[0]["txtemailaddress"]);
                    emergencyname = (!string.IsNullOrWhiteSpace(Obj.EmergencyContactName)) ? Obj.EmergencyContactName : SafeValue<string>(dtRegi.Rows[0]["txtemergencyname"]);
                    emergency = (!string.IsNullOrWhiteSpace(Obj.EmergencyContactPhoneNumber)) ? Obj.EmergencyContactPhoneNumber : SafeValue<string>(dtRegi.Rows[0]["txtemergency"]);
                }
                DataTable tblRegistration = new DataTable("Registration");
                tblRegistration.Columns.Add("txtResponsiblepartyFname");
                tblRegistration.Columns.Add("txtResponsiblepartyLname");
                tblRegistration.Columns.Add("txtResponsibleRelationship");
                tblRegistration.Columns.Add("txtResponsibleAddress");
                tblRegistration.Columns.Add("txtResponsibleCity");
                tblRegistration.Columns.Add("txtResponsibleState");
                tblRegistration.Columns.Add("txtResponsibleZipCode");
                tblRegistration.Columns.Add("txtResponsibleDOB");
                tblRegistration.Columns.Add("txtResponsibleContact");
                tblRegistration.Columns.Add("txtPatientPresentPosition");
                tblRegistration.Columns.Add("txtPatientHowLongHeld");
                tblRegistration.Columns.Add("txtParentPresentPosition");
                tblRegistration.Columns.Add("txtParentHowLongHeld");
                tblRegistration.Columns.Add("txtDriversLicense");
                tblRegistration.Columns.Add("chkMethodOfPayment");
                tblRegistration.Columns.Add("txtPurposeCall");
                tblRegistration.Columns.Add("txtOtherFamily");
                tblRegistration.Columns.Add("txtWhommay");
                tblRegistration.Columns.Add("txtSomeonetonotify");
                tblRegistration.Columns.Add("txtemergency");
                tblRegistration.Columns.Add("txtemergencyname");
                tblRegistration.Columns.Add("txtEmployeeName1");
                tblRegistration.Columns.Add("txtEmployeeDob1");
                tblRegistration.Columns.Add("txtEmployerName1");
                tblRegistration.Columns.Add("txtYearsEmployed1");
                tblRegistration.Columns.Add("txtNameofInsurance1");
                tblRegistration.Columns.Add("txtInsuranceAddress1");
                tblRegistration.Columns.Add("txtInsuranceTelephone1");
                tblRegistration.Columns.Add("txtProgramorpolicy1");
                tblRegistration.Columns.Add("txtSocialSecurity1");
                tblRegistration.Columns.Add("txtUnionLocal1");
                tblRegistration.Columns.Add("txtEmployeeName2");
                tblRegistration.Columns.Add("txtEmployeeDob2");
                tblRegistration.Columns.Add("txtEmployerName2");
                tblRegistration.Columns.Add("txtYearsEmployed2");
                tblRegistration.Columns.Add("txtNameofInsurance2");
                tblRegistration.Columns.Add("txtInsuranceAddress2");
                tblRegistration.Columns.Add("txtInsuranceTelephone2");
                tblRegistration.Columns.Add("txtProgramorpolicy2");
                tblRegistration.Columns.Add("txtSocialSecurity2");
                tblRegistration.Columns.Add("txtUnionLocal2");
                tblRegistration.Columns.Add("txtDigiSign");
                tblRegistration.Columns.Add("txtInsurancePhone1");
                tblRegistration.Columns.Add("txtInsurancePhone2");
                tblRegistration.Columns.Add("txtemailaddress");
                tblRegistration.Columns.Add("CreatedDate");
                tblRegistration.Columns.Add("ModifiedDate");

                //tblRegistration.Rows.Add("","","","","","", "", "","", "", "", "", "", "", "", "", "", "", "", Obj.EmergencyContactPhoneNumber, Obj.EmergencyContactName,"",
                //        "","","","","","", "", "", "","","",
                //        "","","","","", "", "", "", "");

                tblRegistration.Rows.Add(ResponsiblepartyFname, ResponsiblepartyLname, ResponsibleRelationship, ResponsibleAddress, ResponsibleCity, ResponsibleState,
                   ResponsibleZipCode, ResponsibleDOB, ResponsibleContact, "", "", "", "", "", "", "", "", Whommay, "", emergency, emergencyname, EmployeeName1,
                   EmployeeDob1, EmployerName1, YearsEmployed1, NameofInsurance1, InsuranceAddress1, GroupNumber1, "", "", "", EmployeeName2, EmployeeDob2,
                   EmployerName2, YearsEmployed2, NameofInsurance2, InsuranceAddress2, GroupNumber2, "", "", "", "", InsuranceTelephone1, InsuranceTelephone2, Emailaddress);

                DataSet dsRegistration = new DataSet("Registration");
                dsRegistration.Tables.Add(tblRegistration);

                DataSet ds = dsRegistration.Copy();
                dsRegistration.Clear();

                string Registration = ds.GetXml();

                bool status1 = new clsPatientsData().UpdateRegistrationForm(SafeValue<int>(PatientId), "UpdatePatient", SafeValue<string>(Obj.DateOfBirth), Obj.Gender,
                    "", Obj.Address, Obj.City, Obj.State, Obj.Zipcode,
                     Obj.WorkPhone, Obj.SecondaryPhoneNumber, "", Registration);

                obj = "2";


            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---PatientPersonalInformation", Ex.Message, Ex.StackTrace);
            }
            return true;
        }

        //Done
        /// <summary>
        /// This Method is Used for Insert Patient Insurance Coverage Details...
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public static bool InsuranceCoverage(string PatientId, BO.ViewModel.InsuranceCoverageModel InsuranceObj, PDIC PrimaryObj, SDIC SecondaryObj)
        {
            object obj = string.Empty;
            //string strUpdateInsert ="Update "
            clsCommon objCommon = new clsCommon();         

            try
            {
                if (!string.IsNullOrEmpty(PatientId))
                {
                    DataSet dsp = new DataSet();
                    DataTable dtPatient = new DataTable();
                    dsp = new clsPatientsData().GetPateintDetailsByPatientId(SafeValue<int>(PatientId));
                    dtPatient = dsp.Tables[0];
                    string Gender = string.Empty;
                    string Status = string.Empty;
                    string PatientAddress = string.Empty;
                    string DateOfBirth = string.Empty;

                    string PatientCity = string.Empty;
                    string PatientState = string.Empty;
                    string PatientZipcode = string.Empty;
                    string PatientPrimaryPhone = string.Empty;
                    string MobilePhone = string.Empty;
                    string PatiantFax = string.Empty;

                    string EmergencyContactName = string.Empty;
                    string EmergencyContactPhoneNumber = string.Empty;
                    string SecondaryPhone = string.Empty;

                    if (dtPatient != null && dtPatient.Rows.Count > 0)
                    {
                        DataTable dtRegi = new DataTable();
                        dtRegi = new clsPatientsData().GetRegistration(SafeValue<int>(PatientId), "GetRegistrationform");
                        //Changes for SUP-55
                        Gender = SafeValue<string>(dtPatient.Rows[0]["Gender"]) == "0" ? "0" : "1";
                        Status = string.Empty;
                        PatientAddress = SafeValue<string>(dtPatient.Rows[0]["ExactAddress"]);
                        DateOfBirth = SafeValue<string>(dtPatient.Rows[0]["DateOfBirth"]);

                        PatientCity = SafeValue<string>(dtPatient.Rows[0]["City"]);
                        PatientState = SafeValue<string>(dtPatient.Rows[0]["State"]);
                        PatientZipcode = SafeValue<string>(dtPatient.Rows[0]["ZipCode"]);
                        PatientPrimaryPhone = SafeValue<string>(dtPatient.Rows[0]["Phone"]);
                        MobilePhone = string.Empty;
                        PatiantFax = string.Empty;

                        if (dtRegi != null && dtRegi.Rows.Count > 0)
                        {
                            EmergencyContactName = SafeValue<string>(dtRegi.Rows[0]["txtemergencyname"]);
                            EmergencyContactPhoneNumber = SafeValue<string>(dtRegi.Rows[0]["txtemergency"]);
                            //SecondaryPhone = objCommon.CheckNull(SafeValue<string>(dtRegi.Rows[0]["txtemergency"]), string.Empty);
                        }
                    }
                    DataTable dtt = new clsPatientsData().GetPatientsDetails(SafeValue<int>(PatientId));
                    if (dtt != null && dtt.Rows.Count > 0)
                    {
                        SecondaryPhone = SafeValue<string>(dtt.Rows[0]["SecondaryPhone"]);
                    }
                    if (DateOfBirth == null || DateOfBirth == "")
                    {
                        DateOfBirth = "1900-01-01 00:00:00.000";
                    }
                    string Dob = DateOfBirth; string ddlgenderreg = SafeValue<string>(Gender); string drpStatus = string.Empty;
                    string ResidenceStreet = PatientAddress; string City = PatientCity; string State = PatientState;
                    string Zip = PatientZipcode; string ResidenceTelephone = PatientPrimaryPhone;

                    string emergencyname = EmergencyContactName; string emergency = EmergencyContactPhoneNumber; string WorkPhone = SecondaryPhone;

                    string Fax = string.Empty;
                    string ResponsiblepartyFname = InsuranceObj.ResponsiblepartyFname;
                    string ResponsiblepartyLname = InsuranceObj.ResponsiblepartyLname;
                    string ResponsibleRelationship = InsuranceObj.Relationship;
                    string ResponsibleAddress = InsuranceObj.Address;
                    string ResponsibleCity = InsuranceObj.City;
                    string ResponsibleState = InsuranceObj.State;
                    string ResponsibleZipCode = InsuranceObj.ZipCode;
                    string ResponsibleDOB = string.Empty;
                    string ResponsibleContact = InsuranceObj.PhoneNumber;
                    string Whommay = string.Empty;
                    string chkInsurance = string.Empty; string chkCash = string.Empty; string chkCrediteCard = string.Empty;
                    
                    DataTable tblRegistration = new DataTable("Registration");
                    tblRegistration.Columns.Add("txtResponsiblepartyFname");
                    tblRegistration.Columns.Add("txtResponsiblepartyLname");
                    tblRegistration.Columns.Add("txtResponsibleRelationship");
                    tblRegistration.Columns.Add("txtResponsibleAddress");
                    tblRegistration.Columns.Add("txtResponsibleCity");
                    tblRegistration.Columns.Add("txtResponsibleState");
                    tblRegistration.Columns.Add("txtResponsibleZipCode");
                    tblRegistration.Columns.Add("txtResponsibleDOB");
                    tblRegistration.Columns.Add("txtResponsibleContact");
                    tblRegistration.Columns.Add("txtPatientPresentPosition");
                    tblRegistration.Columns.Add("txtPatientHowLongHeld");
                    tblRegistration.Columns.Add("txtParentPresentPosition");
                    tblRegistration.Columns.Add("txtParentHowLongHeld");
                    tblRegistration.Columns.Add("txtDriversLicense");
                    tblRegistration.Columns.Add("chkMethodOfPayment");
                    tblRegistration.Columns.Add("txtPurposeCall");
                    tblRegistration.Columns.Add("txtOtherFamily");
                    tblRegistration.Columns.Add("txtWhommay");
                    tblRegistration.Columns.Add("txtSomeonetonotify");
                    tblRegistration.Columns.Add("txtemergency");
                    tblRegistration.Columns.Add("txtemergencyname");
                    tblRegistration.Columns.Add("txtEmployeeName1");
                    tblRegistration.Columns.Add("txtInsurancePhone1");
                    tblRegistration.Columns.Add("txtEmployeeDob1");
                    tblRegistration.Columns.Add("txtEmployerName1");
                    tblRegistration.Columns.Add("txtYearsEmployed1");
                    tblRegistration.Columns.Add("txtNameofInsurance1");
                    tblRegistration.Columns.Add("txtInsuranceAddress1");
                    tblRegistration.Columns.Add("txtInsuranceTelephone1");
                    tblRegistration.Columns.Add("txtProgramorpolicy1");
                    tblRegistration.Columns.Add("txtSocialSecurity1");
                    tblRegistration.Columns.Add("txtUnionLocal1");
                    tblRegistration.Columns.Add("txtEmployeeName2");
                    tblRegistration.Columns.Add("txtInsurancePhone2");
                    tblRegistration.Columns.Add("txtEmployeeDob2");
                    tblRegistration.Columns.Add("txtEmployerName2");
                    tblRegistration.Columns.Add("txtYearsEmployed2");
                    tblRegistration.Columns.Add("txtNameofInsurance2");
                    tblRegistration.Columns.Add("txtInsuranceAddress2");
                    tblRegistration.Columns.Add("txtInsuranceTelephone2");
                    tblRegistration.Columns.Add("txtProgramorpolicy2");
                    tblRegistration.Columns.Add("txtSocialSecurity2");
                    tblRegistration.Columns.Add("txtUnionLocal2");
                    tblRegistration.Columns.Add("txtDigiSign");

                    StringBuilder chkMethodOfPayment = new StringBuilder();

                    if (chkCash == "1")
                    {
                        if (!String.IsNullOrEmpty(SafeValue<string>(chkMethodOfPayment)))
                        {
                            chkMethodOfPayment.Append("," + "Cash");
                        }
                        else
                        {
                            chkMethodOfPayment.Append("Cash");
                        }
                    }
                    if (chkInsurance == "1")
                    {
                        if (!String.IsNullOrEmpty(SafeValue<string>(chkMethodOfPayment)))
                        {
                            chkMethodOfPayment.Append("," + "Insurance");
                        }
                        else
                        {
                            chkMethodOfPayment.Append("Insurance");
                        }
                    }
                    if (chkCrediteCard == "1")
                    {
                        if (!String.IsNullOrEmpty(SafeValue<string>(chkMethodOfPayment)))
                        {
                            chkMethodOfPayment.Append("," + "Credit Card");
                        }
                        else
                        {
                            chkMethodOfPayment.Append("Credit Card");
                        }
                    }

                    tblRegistration.Columns.Add("CreatedDate");
                    tblRegistration.Columns.Add("ModifiedDate");
                    tblRegistration.Columns.Add("txtemailaddress");


                    DateTime dateValuep;
                    DateTime dateValues;
                    PrimaryObj.DateOfBirth = (DateTime.TryParse(PrimaryObj.DateOfBirth, out dateValuep)) ? PrimaryObj.DateOfBirth : string.Empty;
                    SecondaryObj.DateOfBirth = (DateTime.TryParse(SecondaryObj.DateOfBirth, out dateValues)) ? SecondaryObj.DateOfBirth : string.Empty;
                   

                    tblRegistration.Rows.Add(ResponsiblepartyFname, ResponsiblepartyLname, ResponsibleRelationship, ResponsibleAddress, ResponsibleCity, ResponsibleState,
                     ResponsibleZipCode, ResponsibleDOB, ResponsibleContact, "", "", "", "", "", SafeValue<string>(chkMethodOfPayment), "", "", Whommay, "", EmergencyContactPhoneNumber, EmergencyContactName, PrimaryObj.InsuranceCompany, PrimaryObj.InsuranceCompanyPhone,
                     PrimaryObj.DateOfBirth, PrimaryObj.NameOfInsured, "", PrimaryObj.MemberId, "", PrimaryObj.GroupNumber, "", "", "", SecondaryObj.InsuranceCompany, SecondaryObj.InsuranceCompanyPhone, "",
                     SecondaryObj.NameOfInsured, SecondaryObj.DateOfBirth, SecondaryObj.MemberId, "", SecondaryObj.GroupNumber, "", "", "", "", "", "", InsuranceObj.Emailaddress);

                    DataSet dsRegistration = new DataSet("Registration");
                    dsRegistration.Tables.Add(tblRegistration);

                    DataSet ds = dsRegistration.Copy();
                    dsRegistration.Clear();

                    string Registration = ds.GetXml();

                    bool status1 = new clsPatientsData().UpdateRegistrationForm(SafeValue<int>(PatientId), "UpdatePatient", SafeValue<string>(Dob), ddlgenderreg,
                        drpStatus, ResidenceStreet, City, State, Zip,
                         ResidenceTelephone, WorkPhone, Fax, Registration);
                }

                obj = "1";
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---InsuranceCoverage", Ex.Message, Ex.StackTrace);
            }
            return true;
        }

        //Done
        /// <summary>
        /// This Method Used for Insert Patient Medical History Details.
        /// </summary>
        /// <param name="PatientId"></param>
        /// <param name="objHistory"></param>
        /// <returns></returns>
        public static bool MedicalHistoryForm(string PatientId, MedicalHistory objMedHistory)
        {
            object obj = string.Empty;
            if (!string.IsNullOrEmpty(PatientId))
            {
                try
                {
                    #region Dental History Table
                    DataTable tblMedicalHistory = new DataTable("MedicalHistory");
                    tblMedicalHistory.Columns.Add("MrdQue1");
                    tblMedicalHistory.Columns.Add("MrnQue1");
                    tblMedicalHistory.Columns.Add("Mtxtphysicians");
                    tblMedicalHistory.Columns.Add("MrdQue2");
                    tblMedicalHistory.Columns.Add("MrnQue2");
                    tblMedicalHistory.Columns.Add("Mtxthospitalized");
                    tblMedicalHistory.Columns.Add("MrdQue3");
                    tblMedicalHistory.Columns.Add("MrnQue3");
                    tblMedicalHistory.Columns.Add("Mtxtserious");
                    tblMedicalHistory.Columns.Add("MrdQue4");
                    tblMedicalHistory.Columns.Add("MrnQue4");
                    tblMedicalHistory.Columns.Add("Mtxtmedications");
                    tblMedicalHistory.Columns.Add("MrdQue5");
                    tblMedicalHistory.Columns.Add("MrnQue5");
                    tblMedicalHistory.Columns.Add("MtxtRedux");
                    tblMedicalHistory.Columns.Add("MrdQue6");
                    tblMedicalHistory.Columns.Add("MrnQue6");
                    tblMedicalHistory.Columns.Add("MtxtFosamax");
                    tblMedicalHistory.Columns.Add("MrdQuediet7");
                    tblMedicalHistory.Columns.Add("MrnQuediet7");
                    tblMedicalHistory.Columns.Add("Mtxt7");
                    tblMedicalHistory.Columns.Add("Mrdotobacco8");
                    tblMedicalHistory.Columns.Add("Mrnotobacco8");
                    tblMedicalHistory.Columns.Add("Mtxt8");
                    tblMedicalHistory.Columns.Add("Mrdosubstances");
                    tblMedicalHistory.Columns.Add("Mrnosubstances");
                    tblMedicalHistory.Columns.Add("Mtxt9");
                    tblMedicalHistory.Columns.Add("Mrdopregnant");
                    tblMedicalHistory.Columns.Add("Mrnopregnant");
                    tblMedicalHistory.Columns.Add("Mtxt10");
                    tblMedicalHistory.Columns.Add("Mrdocontraceptives");
                    tblMedicalHistory.Columns.Add("Mrnocontraceptives");
                    tblMedicalHistory.Columns.Add("Mtxt11");
                    tblMedicalHistory.Columns.Add("MrdoNursing");
                    tblMedicalHistory.Columns.Add("MrnoNursing");
                    tblMedicalHistory.Columns.Add("Mtxt12");
                    tblMedicalHistory.Columns.Add("Mrdotonsils");
                    tblMedicalHistory.Columns.Add("Mrnotonsils");
                    tblMedicalHistory.Columns.Add("Mtxt13");
                    tblMedicalHistory.Columns.Add("MchkQue_1");
                    tblMedicalHistory.Columns.Add("MchkQueN_1");
                    tblMedicalHistory.Columns.Add("MchkQue_2");
                    tblMedicalHistory.Columns.Add("MchkQueN_2");
                    tblMedicalHistory.Columns.Add("MchkQue_3");
                    tblMedicalHistory.Columns.Add("MchkQueN_3");
                    tblMedicalHistory.Columns.Add("MchkQue_4");
                    tblMedicalHistory.Columns.Add("MchkQueN_4");
                    tblMedicalHistory.Columns.Add("MchkQue_5");
                    tblMedicalHistory.Columns.Add("MchkQueN_5");
                    tblMedicalHistory.Columns.Add("MchkQue_6");
                    tblMedicalHistory.Columns.Add("MchkQueN_6");
                    tblMedicalHistory.Columns.Add("MchkQue_7");
                    tblMedicalHistory.Columns.Add("MchkQueN_7");
                    tblMedicalHistory.Columns.Add("MchkQue_8");
                    tblMedicalHistory.Columns.Add("MchkQueN_8");
                    tblMedicalHistory.Columns.Add("MchkQue_9");
                    tblMedicalHistory.Columns.Add("MchkQueN_9");
                    tblMedicalHistory.Columns.Add("MtxtchkQue_9");

                    tblMedicalHistory.Columns.Add("MrdQueAIDS_HIV_Positive");
                    tblMedicalHistory.Columns.Add("MrdQueAlzheimer");
                    tblMedicalHistory.Columns.Add("MrdQueAnaphylaxis");

                    tblMedicalHistory.Columns.Add("MrdQueAnemia");
                    tblMedicalHistory.Columns.Add("MrdQueAngina");
                    tblMedicalHistory.Columns.Add("MrdQueArthritis_Gout");

                    tblMedicalHistory.Columns.Add("MrdQueArtificialHeartValve");
                    tblMedicalHistory.Columns.Add("MrdQueArtificialJoint");
                    tblMedicalHistory.Columns.Add("MrdQueAsthma");

                    tblMedicalHistory.Columns.Add("MrdQueBloodDisease");
                    tblMedicalHistory.Columns.Add("MrdQueBloodTransfusion");
                    tblMedicalHistory.Columns.Add("MrdQueBoneDisorders");

                    tblMedicalHistory.Columns.Add("MrdQueBreathing");
                    tblMedicalHistory.Columns.Add("MrdQueBruise");
                    tblMedicalHistory.Columns.Add("MrdQueCancer");

                    tblMedicalHistory.Columns.Add("MrdQueChemicalDependancy");
                    tblMedicalHistory.Columns.Add("MrdQueChemotherapy");
                    tblMedicalHistory.Columns.Add("MrdQueChest");

                    tblMedicalHistory.Columns.Add("MrdQueCold_Sores_Fever");
                    tblMedicalHistory.Columns.Add("MrdQueCongenital");
                    tblMedicalHistory.Columns.Add("MrdQueConvulsions");

                    tblMedicalHistory.Columns.Add("MrdQueCortisone");
                    tblMedicalHistory.Columns.Add("MrdQueCortisoneTretments");
                    tblMedicalHistory.Columns.Add("MrdQueDiabetes");

                    tblMedicalHistory.Columns.Add("MrdQueDrug");
                    tblMedicalHistory.Columns.Add("MrdQueEasily");
                    tblMedicalHistory.Columns.Add("MrdQueEmphysema");

                    tblMedicalHistory.Columns.Add("MrdQueEndocrineProblems");
                    tblMedicalHistory.Columns.Add("MrdQueEpilepsy");
                    tblMedicalHistory.Columns.Add("MrdQueExcessiveBleeding");

                    tblMedicalHistory.Columns.Add("MrdQueExcessiveThirst");
                    tblMedicalHistory.Columns.Add("MrdQueExcessiveUrination");
                    tblMedicalHistory.Columns.Add("MrdQueFainting");

                    tblMedicalHistory.Columns.Add("MrdQueFrequentCough");
                    tblMedicalHistory.Columns.Add("MrdQueFrequentDiarrhea");
                    tblMedicalHistory.Columns.Add("MrdQueFrequentHeadaches");

                    tblMedicalHistory.Columns.Add("MrdQueGenital");
                    tblMedicalHistory.Columns.Add("MrdQueGlaucoma");
                    tblMedicalHistory.Columns.Add("MrdQueHay");

                    tblMedicalHistory.Columns.Add("MrdQueHeartAttack_Failure");
                    tblMedicalHistory.Columns.Add("MrdQueHeartMurmur");
                    tblMedicalHistory.Columns.Add("MrdQueHeartPacemaker");

                    tblMedicalHistory.Columns.Add("MrdQueHeartTrouble_Disease");
                    tblMedicalHistory.Columns.Add("MrdQueHemophilia");
                    tblMedicalHistory.Columns.Add("MrdQueHepatitisA");

                    tblMedicalHistory.Columns.Add("MrdQueHepatitisBorC");
                    tblMedicalHistory.Columns.Add("MrdQueHerpes");
                    tblMedicalHistory.Columns.Add("MrdQueHighBloodPressure");

                    tblMedicalHistory.Columns.Add("MrdQueHighCholesterol");
                    tblMedicalHistory.Columns.Add("MrdQueHives");
                    tblMedicalHistory.Columns.Add("MrdQueHypoglycemia");

                    tblMedicalHistory.Columns.Add("MrdQueIrregular");
                    tblMedicalHistory.Columns.Add("MrdQueKidney");
                    tblMedicalHistory.Columns.Add("MrdQueLeukemia");

                    tblMedicalHistory.Columns.Add("MrdQueLiver");
                    tblMedicalHistory.Columns.Add("MrdQueLow");
                    tblMedicalHistory.Columns.Add("MrdQueLung");

                    tblMedicalHistory.Columns.Add("MrdQueMitral");
                    tblMedicalHistory.Columns.Add("MrdQueNervousDisorder");
                    tblMedicalHistory.Columns.Add("MrdQueOsteoporosis");

                    tblMedicalHistory.Columns.Add("MrdQuePain");
                    tblMedicalHistory.Columns.Add("MrdQueParathyroid");
                    tblMedicalHistory.Columns.Add("MrdQuePsychiatric");

                    tblMedicalHistory.Columns.Add("MrdQueRadiation");
                    tblMedicalHistory.Columns.Add("MrdQueRecent");
                    tblMedicalHistory.Columns.Add("MrdQueRenal");

                    tblMedicalHistory.Columns.Add("MrdQueRheumatic");
                    tblMedicalHistory.Columns.Add("MrdQueRheumatism");
                    tblMedicalHistory.Columns.Add("MrdQueScarlet");

                    tblMedicalHistory.Columns.Add("MrdQueShingles");
                    tblMedicalHistory.Columns.Add("MrdQueSickle");
                    tblMedicalHistory.Columns.Add("MrdQueSinus");

                    tblMedicalHistory.Columns.Add("MrdQueSpina");
                    tblMedicalHistory.Columns.Add("MrdQueStomach");
                    tblMedicalHistory.Columns.Add("MrdQueStroke");

                    tblMedicalHistory.Columns.Add("MrdQueSwelling");
                    tblMedicalHistory.Columns.Add("MrdQueThyroid");
                    tblMedicalHistory.Columns.Add("MrdQueTonsillitis");

                    tblMedicalHistory.Columns.Add("MrdQueTuberculosis");
                    tblMedicalHistory.Columns.Add("MrdQueTumors");
                    tblMedicalHistory.Columns.Add("MrdQueUlcers");

                    tblMedicalHistory.Columns.Add("MrdQueVenereal");
                    tblMedicalHistory.Columns.Add("MrdQueYellow");
                    tblMedicalHistory.Columns.Add("Mtxtillness");

                    tblMedicalHistory.Columns.Add("MtxtComments");
                    tblMedicalHistory.Columns.Add("CreatedDate");
                    tblMedicalHistory.Columns.Add("ModifiedDate");
                    #endregion
                    PatientMedicalHistroy objHistory = new PatientMedicalHistroy();
                    objHistory = MappedMedicalHistory(objMedHistory);
                    tblMedicalHistory.Rows.Add(objHistory.MrdQue1,
                        objHistory.MrNQue1,
                        objHistory.Mtxtphysicians,
                        objHistory.MrdQue2,
                        objHistory.MrnQue2,
                        objHistory.Mtxthospitalized,
                        objHistory.MrdQue3,
                        objHistory.MrnQue3,
                        objHistory.Mtxtserious,
                        objHistory.MrdQue4,
                        objHistory.MrnQue4,
                        objHistory.Mtxtmedications,
                        objHistory.MrdQue5,
                        objHistory.MrnQue5,
                        objHistory.MtxtRedux,
                        objHistory.MrdQue6,
                        objHistory.MrnQue6,
                        objHistory.MtxtFosamax,
                        objHistory.MrdQuediet7,
                        objHistory.MrnQuediet7,
                        objHistory.Mtxt7,
                        objHistory.Mrdotobacco8,
                        objHistory.Mrnotobacco8,
                        objHistory.Mtxt8,
                        objHistory.Mrdosubstances,
                        objHistory.Mrnosubstances,
                        objHistory.Mtxt9,
                        objHistory.Mrdopregnant,
                        objHistory.Mrnopregnant,
                        objHistory.Mtxt10,
                        objHistory.Mrdocontraceptives,
                        objHistory.Mrnocontraceptives,
                        objHistory.Mtxt11,
                        objHistory.MrdoNursing,
                        objHistory.MrnoNursing,
                        objHistory.Mtxt12,
                        objHistory.Mrdotonsils,
                        objHistory.Mrnotonsils,
                        objHistory.Mtxt13,
                        objHistory.MchkQue_1,
                        objHistory.MchkQueN_1,
                        objHistory.MchkQue_2,
                        objHistory.MchkQueN_2,
                        objHistory.MchkQue_3,
                        objHistory.MchkQueN_3,
                        objHistory.MchkQue_4,
                        objHistory.MchkQueN_4,
                        objHistory.MchkQue_5,
                        objHistory.MchkQueN_5,
                        objHistory.MchkQue_6,
                        objHistory.MchkQueN_6,
                        objHistory.MchkQue_7,
                        objHistory.MchkQueN_7,
                        objHistory.MchkQue_8,
                        objHistory.MchkQueN_8,
                        objHistory.MchkQue_9,
                        objHistory.MchkQueN_9,
                        objHistory.MtxtchkQue_9,
                        objHistory.MrdQueAIDS_HIV_Positive,
                        objHistory.MrdQueAlzheimer,
                        objHistory.MrdQueAnaphylaxis,
                        objHistory.MrdQueAnemia,
                        objHistory.MrdQueAngina,
                        objHistory.MrdQueArthritis_Gout,
                        objHistory.MrdQueArtificialHeartValve,
                        objHistory.MrdQueArtificialJoint,
                        objHistory.MrdQueAsthma,
                        objHistory.MrdQueBloodDisease,
                        objHistory.MrdQueBloodTransfusion,
                        objHistory.MrdQueBoneDisorder,
                        objHistory.MrdQueBreathing,
                        objHistory.MrdQueBruise,
                        objHistory.MrdQueCancer,
                        objHistory.MrdQueChemicalDepandancy,
                        objHistory.MrdQueChemotherapy,
                        objHistory.MrdQueChest,
                        objHistory.MrdQueCold_Sores_Fever,
                        objHistory.MrdQueCongenital,
                        objHistory.MrdQueConvulsions,
                        objHistory.MrdQueCortisone,
                        objHistory.MrdQueCortisoneTreatMents,
                        objHistory.MrdQueDiabetes,
                        objHistory.MrdQueDrug,
                        objHistory.MrdQueEasily,
                        objHistory.MrdQueEmphysema,
                        objHistory.MrdQueEndorcrineProblems,
                        objHistory.MrdQueEpilepsy,
                        objHistory.MrdQueExcessiveBleeding,
                        objHistory.MrdQueExcessiveThirst,
                        objHistory.MrdQueExcessiveUrination,
                        objHistory.MrdQueFainting,
                        objHistory.MrdQueFrequentCough,
                        objHistory.MrdQueFrequentDiarrhea,
                        objHistory.MrdQueFrequentHeadaches,
                        objHistory.MrdQueGenital,
                        objHistory.MrdQueGlaucoma,
                        objHistory.MrdQueHay,
                        objHistory.MrdQueHeartAttack_Failure,
                        objHistory.MrdQueHeartMurmur,
                        objHistory.MrdQueHeartPacemaker,
                        objHistory.MrdQueHeartTrouble_Disease,
                        objHistory.MrdQueHemophilia,
                        objHistory.MrdQueHepatitisA,
                        objHistory.MrdQueHepatitisBorC,
                        objHistory.MrdQueHerpes,
                        objHistory.MrdQueHighBloodPressure,
                        objHistory.MrdQueHighCholesterol,
                        objHistory.MrdQueHives,
                        objHistory.MrdQueHypoglycemia,
                        objHistory.MrdQueIrregular,
                        objHistory.MrdQueKidney,
                        objHistory.MrdQueLeukemia,
                        objHistory.MrdQueLiver,
                        objHistory.MrdQueLow,
                        objHistory.MrdQueLung,
                        objHistory.MrdQueMitral,
                        objHistory.MrdQueNervousDisorder,
                        objHistory.MrdQueOsteoporosis,
                        objHistory.MrdQuePain,
                        objHistory.MrdQueParathyroid,
                        objHistory.MrdQuePsychiatric,
                        objHistory.MrdQueRadiation,
                        objHistory.MrdQueRecent,
                        objHistory.MrdQueRenal,
                        objHistory.MrdQueRheumatic,
                        objHistory.MrdQueRheumatism,
                        objHistory.MrdQueScarlet,
                        objHistory.MrdQueShingles,
                        objHistory.MrdQueSickle,
                        objHistory.MrdQueSinus,
                        objHistory.MrdQueSpina,
                        objHistory.MrdQueStomach,
                        objHistory.MrdQueStroke,
                        objHistory.MrdQueSwelling,
                        objHistory.MrdQueThyroid,
                        objHistory.MrdQueTonsillitis,
                        objHistory.MrdQueTuberculosis,
                        objHistory.MrdQueTumors,
                        objHistory.MrdQueUlcers,
                        objHistory.MrdQueVenereal,
                        objHistory.MrdQueYellow,
                        objHistory.Mtxtillness,
                        objHistory.MtxtComments);

                    DataSet dsMedicalHistory = new DataSet("MedicalHistory");
                    dsMedicalHistory.Tables.Add(tblMedicalHistory);

                    DataSet ds = dsMedicalHistory.Copy();
                    dsMedicalHistory.Clear();

                    string MedicalHistory = ds.GetXml();

                    bool status = new clsPatientsData().UpdateMedicalHistory(SafeValue<int>(PatientId), "UpdateMedicalHistory", MedicalHistory);

                    obj = "1";
                }
                catch (Exception Ex)
                {
                    new clsCommon().InsertErrorLog("PatientRewardBLL---MedicalHistoryForm", Ex.Message, Ex.StackTrace);
                }
            }
            return true;

        }

        //DONE
        /// <summary>
        /// This Method Used for Insert Patient Dental History.
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public static bool DentalHistoryForm(string PatientId, DentalHistorys Obj)
        {
            object obj = string.Empty;
            if (!string.IsNullOrEmpty(PatientId))
            {
                try
                {
                    DataTable tblDentalHistory = new DataTable("DentalHistory");
                    tblDentalHistory.Columns.Add("txtQue1");
                    tblDentalHistory.Columns.Add("txtQue2");
                    tblDentalHistory.Columns.Add("txtQue3");
                    tblDentalHistory.Columns.Add("txtQue4");
                    tblDentalHistory.Columns.Add("txtQue5");
                    tblDentalHistory.Columns.Add("txtQue5a");
                    tblDentalHistory.Columns.Add("txtQue5b");
                    tblDentalHistory.Columns.Add("txtQue5c");
                    tblDentalHistory.Columns.Add("txtQue6");
                    tblDentalHistory.Columns.Add("txtQue7");
                    tblDentalHistory.Columns.Add("rdQue7a");
                    tblDentalHistory.Columns.Add("rdQue8");
                    tblDentalHistory.Columns.Add("rdQue9");
                    tblDentalHistory.Columns.Add("txtQue9a");
                    tblDentalHistory.Columns.Add("rdQue10");
                    tblDentalHistory.Columns.Add("rdoQue11aFixedbridge");
                    tblDentalHistory.Columns.Add("rdoQue11bRemoveablebridge");
                    tblDentalHistory.Columns.Add("rdoQue11cDenture");
                    tblDentalHistory.Columns.Add("rdQue11dImplant");
                    tblDentalHistory.Columns.Add("txtQue12");
                    tblDentalHistory.Columns.Add("rdQue15");
                    tblDentalHistory.Columns.Add("rdQue16");
                    tblDentalHistory.Columns.Add("rdQue17");
                    tblDentalHistory.Columns.Add("rdQue18");
                    tblDentalHistory.Columns.Add("rdQue19");
                    tblDentalHistory.Columns.Add("chkQue20");

                    StringBuilder chkQue20 = new StringBuilder();
                    PatientDentalHistory PDH = new PatientDentalHistory();
                    PDH = MappedDentalHistory(Obj);

                    if (PDH.chkQue20_1 == "Y")
                    {
                        if (!string.IsNullOrEmpty(SafeValue<string>(chkQue20)))
                        {
                            chkQue20.Append("," + "1");
                        }
                        else
                        {
                            chkQue20.Append("1");
                        }
                    }
                    if (PDH.chkQue20_2 == "Y")
                    {
                        if (!string.IsNullOrEmpty(SafeValue<string>(chkQue20)))
                        {
                            chkQue20.Append("," + "2");
                        }
                        else
                        {
                            chkQue20.Append("2");
                        }
                    }
                    if (PDH.chkQue20_3 == "Y")
                    {
                        if (!string.IsNullOrEmpty(SafeValue<string>(chkQue20)))
                        {
                            chkQue20.Append("," + "3");
                        }
                        else
                        {
                            chkQue20.Append("3");
                        }
                    }
                    if (PDH.chkQue20_4 == "Y")
                    {
                        if (!string.IsNullOrEmpty(SafeValue<string>(chkQue20)))
                        {
                            chkQue20.Append("," + "4");
                        }
                        else
                        {
                            chkQue20.Append("4");
                        }
                    }
                    tblDentalHistory.Columns.Add("rdQue21");
                    tblDentalHistory.Columns.Add("txtQue21a");
                    tblDentalHistory.Columns.Add("txtQue22");

                    tblDentalHistory.Columns.Add("txtQue23");
                    tblDentalHistory.Columns.Add("rdQue24");

                    tblDentalHistory.Columns.Add("txtQue26");
                    tblDentalHistory.Columns.Add("rdQue27");
                    tblDentalHistory.Columns.Add("rdQue28");
                    tblDentalHistory.Columns.Add("txtQue28a");
                    tblDentalHistory.Columns.Add("txtQue28b");
                    tblDentalHistory.Columns.Add("txtQue28c");
                    tblDentalHistory.Columns.Add("txtQue29");
                    tblDentalHistory.Columns.Add("txtQue29a");
                    tblDentalHistory.Columns.Add("rdQue30");

                    tblDentalHistory.Columns.Add("txtComments");
                    tblDentalHistory.Columns.Add("txtDigiSign");
                    tblDentalHistory.Columns.Add("CreatedDate");
                    tblDentalHistory.Columns.Add("ModifiedDate");

                    tblDentalHistory.Columns.Add("Reasonforfirstvisit");
                    tblDentalHistory.Columns.Add("Dateoflastvisit");
                    tblDentalHistory.Columns.Add("Reasonforlastvisit");
                    tblDentalHistory.Columns.Add("Dateofnextvisit");
                    tblDentalHistory.Columns.Add("Reasonfornextvisit");
                    tblDentalHistory.Columns.Add("firstvistdate");
                    tblDentalHistory.Rows.Add(PDH.txtQue1, PDH.txtQue2, PDH.txtQue3, PDH.txtQue4, PDH.txtQue5, PDH.txtQue5a, PDH.txtQue5b, PDH.txtQue5c,
                        PDH.txtQue6, PDH.txtQue7, PDH.rdQue7a, PDH.rdQue8, PDH.rdQue9, PDH.txtQue9a, PDH.rdQue10,
                        PDH.rdoQue11aFixedbridge, PDH.rdoQue11bRemoveablebridge, PDH.rdoQue11cDenture,
                        PDH.rdQue11dImplant, PDH.txtQue12, PDH.rdQue15, PDH.rdQue16, PDH.rdQue17, PDH.rdQue18,
                        PDH.rdQue19, SafeValue<string>(chkQue20), PDH.rdQue21, PDH.txtQue21a, PDH.txtQue22,
                        PDH.txtQue23, PDH.rdQue24, PDH.txtQue26, PDH.rdQue27, PDH.rdQue28,
                        PDH.txtQue28a, PDH.txtQue28b, PDH.txtQue28c, PDH.txtQue29, PDH.txtQue29a, PDH.rdQue30, PDH.txtComments, null, System.DateTime.Now, System.DateTime.Now,
                        PDH.Reasonforfirstvisit, PDH.Dateoflastvisit, PDH.Reasonforlastvisit, PDH.Dateofnextvisit, PDH.Reasonfornextvisit, PDH.txtDateoffirstvisit);


                    //Join Two Table                                                                                                                            
                    DataSet dsDentalHistory = new DataSet("DentalHistory");
                    dsDentalHistory.Tables.Add(tblDentalHistory);

                    DataSet ds = dsDentalHistory.Copy();
                    dsDentalHistory.Clear();

                    string DentalHistory = ds.GetXml();

                    bool status = new clsPatientsData().UpdateDentalHistory(SafeValue<int>(PatientId), "UpdateDentalHistory", DentalHistory);
                    obj = "1";
                }
                catch (Exception Ex)
                {
                    new clsCommon().InsertErrorLog("PatientRewardBLL---DentalHistoryForm", Ex.Message, Ex.StackTrace);
                }
            }
            return true;
        }
        #endregion

        public static bool Checkstringvalues(string Values)
        {
            return (Values == "True") ? true : false;
        }

        /// <summary>
        /// THis Method Mapped Old Medical History Property to new Medical History Property.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public static PatientMedicalHistroy MappedMedicalHistory(MedicalHistory Obj)
        {
            try
            {
                PatientMedicalHistroy PMH = new PatientMedicalHistroy();
                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.under_a_physicians)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.under_a_physicians)))
                        PMH.MrdQue1 = "Y";
                    else
                        PMH.MrNQue1 = "Y";
                }
                PMH.Mtxtphysicians = SafeValue<string>(Obj.str_physicians);

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.hospitalized_or_majoroperation)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.hospitalized_or_majoroperation)))
                        PMH.MrdQue2 = "Y";
                    else
                        PMH.MrnQue2 = "Y";
                }
                PMH.Mtxthospitalized = SafeValue<string>(Obj.str_hospitalized_or_majoroperation);

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.head_or_neck_injury)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.head_or_neck_injury)))
                        PMH.MrdQue3 = "Y";
                    else
                        PMH.MrnQue3 = "Y";
                }
                PMH.Mtxtserious = SafeValue<string>(Obj.str_head_or_neck_injury);

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.medications_pills_drugs)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.medications_pills_drugs)))
                        PMH.MrdQue4 = "Y";
                    else
                        PMH.MrnQue4 = "Y";
                }
                PMH.Mtxtmedications = SafeValue<string>(Obj.str_medications_pills_drugs);

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.PhenFen_or_Redux)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.PhenFen_or_Redux)))
                        PMH.MrdQue5 = "Y";
                    else
                        PMH.MrnQue5 = "Y";
                }
                PMH.MtxtRedux = SafeValue<string>(Obj.str_PhenFen_or_Redux);

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.Isbiphosphonates)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.Isbiphosphonates)))
                        PMH.MrdQue6 = "Y";
                    else
                        PMH.MrnQue6 = "Y";
                }
                PMH.MtxtFosamax = SafeValue<string>(Obj.str_biphosphonates);

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.Specialdiet)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.Specialdiet)))
                        PMH.MrdQuediet7 = "Y";
                    else
                        PMH.MrnQuediet7 = "Y";
                }
                PMH.Mtxt7 = SafeValue<string>(Obj.str_Specialdiet);

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.tobacco)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.tobacco)))
                        PMH.Mrdotobacco8 = "Y";
                    else
                        PMH.Mrnotobacco8 = "Y";
                }
                PMH.Mtxt8 = SafeValue<string>(Obj.str_tobacco);

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.substances)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.substances)))
                        PMH.Mrdosubstances = "Y";
                    else
                        PMH.Mrnosubstances = "Y";
                }
                PMH.Mtxt9 = SafeValue<string>(Obj.str_substances);

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.pregnant)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.pregnant)))
                        PMH.Mrdopregnant = "Y";
                    else
                        PMH.Mrnopregnant = "Y";
                }
                PMH.Mtxt10 = SafeValue<string>(Obj.str_pregnant);

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.contraceptives)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.contraceptives)))
                        PMH.Mrdocontraceptives = "Y";
                    else
                        PMH.Mrnocontraceptives = "Y";
                }
                PMH.Mtxt11 = SafeValue<string>(Obj.str_contraceptives);

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.Nursing)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.Nursing)))
                        PMH.MrdoNursing = "Y";
                    else
                        PMH.MrnoNursing = "Y";
                }
                PMH.Mtxt12 = SafeValue<string>(Obj.str_Nursing);

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.tonsils)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.tonsils)))
                        PMH.Mrdotonsils = "Y";
                    else
                        PMH.Mrnotonsils = "Y";
                }
                PMH.Mtxt13 = SafeValue<string>(Obj.str_tonsils);

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.allergic.Aspirin)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.allergic.Aspirin)))
                        PMH.MchkQue_1 = "Y";
                    else
                        PMH.MchkQueN_1 = "Y";
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.allergic.Penicillin)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.allergic.Penicillin)))
                        PMH.MchkQue_2 = "Y";
                    else
                        PMH.MchkQueN_2 = "Y";
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.allergic.Codeine)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.allergic.Codeine)))
                        PMH.MchkQue_3 = "Y";
                    else
                        PMH.MchkQueN_3 = "Y";
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.allergic.LocalAnesthetics)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.allergic.LocalAnesthetics)))
                        PMH.MchkQue_4 = "Y";
                    else
                        PMH.MchkQueN_4 = "Y";
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.allergic.Acrylic)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.allergic.Acrylic)))
                        PMH.MchkQue_5 = "Y";
                    else
                        PMH.MchkQueN_5 = "Y";
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.allergic.Metal)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.allergic.Metal)))
                        PMH.MchkQue_6 = "Y";
                    else
                        PMH.MchkQueN_6 = "Y";
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.allergic.Latex)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.allergic.Latex)))
                        PMH.MchkQue_7 = "Y";
                    else
                        PMH.MchkQueN_7 = "Y";
                }

                if (!string.IsNullOrWhiteSpace(SafeValue<string>(Obj.allergic.SulfaDrugs)))
                {
                    if (Checkstringvalues(SafeValue<string>(Obj.allergic.SulfaDrugs)))
                        PMH.MchkQue_8 = "Y";
                    else
                        PMH.MchkQueN_8 = "Y";
                }
                //PMH.MrdQue1 = (Obj.under_a_physicians != null) ? "Y" : "N";
                //PMH.MrdQue2 = (Obj.hospitalized_or_majoroperation) ? "Y" : "N";
                //PMH.MrdQue3 = (Obj.head_or_neck_injury) ? "Y" : "N";
                //PMH.MrdQue4 = (Obj.medications_pills_drugs) ? "Y" : "N";
                //PMH.MrdQue5 = (Obj.PhenFen_or_Redux) ? "Y" : "N";
                //PMH.MrdQue6 = (Obj.Isbiphosphonates) ? "Y" : "N";
                //PMH.MrdQuediet7 = (Obj.Specialdiet) ? "Y" : "N";
                //PMH.Mrdotobacco8 = (Obj.tobacco) ? "Y" : "N";
                //PMH.Mrdosubstances = (Obj.substances) ? "Y" : "N";
                //PMH.Mrdopregnant = (Obj.pregnant) ? "Y" : "N";
                //PMH.Mrdocontraceptives = (Obj.contraceptives) ? "Y" : "N";
                //PMH.MrdoNursing = (Obj.Nursing) ? "Y" : "N";
                // PMH.Mrdotonsils = (Obj.tonsils) ? "Y" : "N";
                //PMH.MchkQue_1 = (Obj.allergic.Aspirin) ? "Y" : "N";
                //PMH.MchkQue_2 = (Obj.allergic.Penicillin) ? "Y" : "N";
                //PMH.MchkQue_3 = (Obj.allergic.Codeine) ? "Y" : "N";
                //PMH.MchkQue_4 = (Obj.allergic.LocalAnesthetics) ? "Y" : "N";
                //PMH.MchkQue_5 = (Obj.allergic.Acrylic) ? "Y" : "N";
                //PMH.MchkQue_6 = (Obj.allergic.Metal) ? "Y" : "N";
                //PMH.MchkQue_7 = (Obj.allergic.Latex) ? "Y" : "N";
                //PMH.MchkQue_8 = (Obj.allergic.SulfaDrugs) ? "Y" : "N";
                PMH.MtxtchkQue_9 = Obj.allergic.Other;
                PMH.Mtxtillness = Obj.illness;
                PMH.MtxtComments = Obj.Additional_info;

                PMH.MrdQueAIDS_HIV_Positive = (Obj.following.AIDS_HIV_Positive) ? "Y" : "N";
                PMH.MrdQueAlzheimer = (Obj.following.Alzheimer_Disease) ? "Y" : "N";
                PMH.MrdQueAnaphylaxis = (Obj.following.Anaphylaxis) ? "Y" : "N";

                PMH.MrdQueAnemia = (Obj.following.Anemia) ? "Y" : "N";
                PMH.MrdQueAngina = (Obj.following.Angina) ? "Y" : "N";
                PMH.MrdQueArthritis_Gout = (Obj.following.Arthritis_Gout) ? "Y" : "N";

                PMH.MrdQueArtificialHeartValve = (Obj.following.Artificial_Heart_Valve) ? "Y" : "N";
                PMH.MrdQueArtificialJoint = (Obj.following.Artificial_Joint) ? "Y" : "N";
                PMH.MrdQueAsthma = (Obj.following.Asthma) ? "Y" : "N";

                PMH.MrdQueBloodDisease = (Obj.following.Blood_Disease) ? "Y" : "N";
                PMH.MrdQueBloodTransfusion = (Obj.following.Blood_Transfusion) ? "Y" : "N";
                PMH.MrdQueBoneDisorder = (Obj.following.Bone_Disorders) ? "Y" : "N";

                PMH.MrdQueBreathing = (Obj.following.Breathing_Problem) ? "Y" : "N";
                PMH.MrdQueBruise = (Obj.following.Bruise_Easily) ? "Y" : "N";
                PMH.MrdQueCancer = (Obj.following.Cancer) ? "Y" : "N";


                PMH.MrdQueChemicalDepandancy = (Obj.following.Chemical_Dependancy) ? "Y" : "N";
                PMH.MrdQueChemotherapy = (Obj.following.Chemotherapy) ? "Y" : "N";
                PMH.MrdQueChest = (Obj.following.Chest_Pains) ? "Y" : "N";

                PMH.MrdQueCold_Sores_Fever = (Obj.following.Cold_Sores_Fever_Blisters) ? "Y" : "N";
                PMH.MrdQueCongenital = (Obj.following.Congenital_Heart_Disorder) ? "Y" : "N";
                PMH.MrdQueConvulsions = (Obj.following.Convulsions) ? "Y" : "N";

                PMH.MrdQueCortisone = (Obj.following.Cortisone_Medicine) ? "Y" : "N";
                PMH.MrdQueCortisoneTreatMents = (Obj.following.Cortisone_Tretments) ? "Y" : "N";
                PMH.MrdQueDiabetes = (Obj.following.Diabetes) ? "Y" : "N";

                PMH.MrdQueDrug = (Obj.following.Drug_Addiction) ? "Y" : "N";
                PMH.MrdQueEasily = (Obj.following.Easily_Winded) ? "Y" : "N";
                PMH.MrdQueEmphysema = (Obj.following.Emphysema) ? "Y" : "N";

                PMH.MrdQueEndorcrineProblems = (Obj.following.Endorcrine_Problems) ? "Y" : "N";
                PMH.MrdQueEpilepsy = (Obj.following.Epilepsy_or_Seizures) ? "Y" : "N";
                PMH.MrdQueExcessiveBleeding = (Obj.following.Excessive_Bleeding) ? "Y" : "N";

                PMH.MrdQueExcessiveThirst = (Obj.following.Excessive_Thirst) ? "Y" : "N";
                PMH.MrdQueExcessiveUrination = (Obj.following.Excessive_Urination) ? "Y" : "N";
                PMH.MrdQueFainting = (Obj.following.Fainting_Spells_Dizziness) ? "Y" : "N";

                PMH.MrdQueFrequentCough = (Obj.following.Frequent_Cough) ? "Y" : "N";
                PMH.MrdQueFrequentDiarrhea = (Obj.following.Frequent_Diarrhea) ? "Y" : "N";
                PMH.MrdQueFrequentHeadaches = (Obj.following.Frequent_Headaches) ? "Y" : "N";

                PMH.MrdQueGenital = (Obj.following.Genital_Herpes) ? "Y" : "N";
                PMH.MrdQueGlaucoma = (Obj.following.Glaucoma) ? "Y" : "N";
                PMH.MrdQueHay = (Obj.following.Hay_Fever) ? "Y" : "N";

                PMH.MrdQueHeartAttack_Failure = (Obj.following.Heart_Attack_Failure) ? "Y" : "N";
                PMH.MrdQueHeartMurmur = (Obj.following.Heart_Murmur) ? "Y" : "N";
                PMH.MrdQueHeartPacemaker = (Obj.following.Heart_Pacemaker) ? "Y" : "N";

                PMH.MrdQueHeartTrouble_Disease = (Obj.following.Heart_Trouble_Disease) ? "Y" : "N";
                PMH.MrdQueHemophilia = (Obj.following.Hemophilia) ? "Y" : "N";
                PMH.MrdQueHepatitisA = (Obj.following.Hepatitis_A) ? "Y" : "N";

                PMH.MrdQueHepatitisBorC = (Obj.following.Hepatitis_B_or_C) ? "Y" : "N";
                PMH.MrdQueHerpes = (Obj.following.Herpes) ? "Y" : "N";
                PMH.MrdQueHighBloodPressure = (Obj.following.High_Blood_Pressure) ? "Y" : "N";

                PMH.MrdQueHighCholesterol = (Obj.following.High_Cholesterol) ? "Y" : "N";
                PMH.MrdQueHives = (Obj.following.Hives_or_Rash) ? "Y" : "N";
                PMH.MrdQueHypoglycemia = (Obj.following.Hypoglycemia) ? "Y" : "N";

                PMH.MrdQueIrregular = (Obj.following.Irregular_Heartbeat) ? "Y" : "N";
                PMH.MrdQueKidney = (Obj.following.Kidney_Problems) ? "Y" : "N";
                PMH.MrdQueLeukemia = (Obj.following.Leukemia) ? "Y" : "N";

                PMH.MrdQueLiver = (Obj.following.Liver_Disease) ? "Y" : "N";
                PMH.MrdQueLow = (Obj.following.Low_Blood_Pressure) ? "Y" : "N";
                PMH.MrdQueLung = (Obj.following.Lung_Disease) ? "Y" : "N";


                PMH.MrdQueMitral = (Obj.following.Mitral_Valve_Prolapse) ? "Y" : "N";
                PMH.MrdQueNervousDisorder = (Obj.following.Nervous_Disorder) ? "Y" : "N";
                PMH.MrdQueOsteoporosis = (Obj.following.Osteoporosis) ? "Y" : "N";

                PMH.MrdQuePain = (Obj.following.Pain_in_Jaw_Joints) ? "Y" : "N";
                PMH.MrdQueParathyroid = (Obj.following.Parathyroid_Disease) ? "Y" : "N";
                PMH.MrdQuePsychiatric = (Obj.following.Psychiatric_Care) ? "Y" : "N";

                PMH.MrdQueRadiation = (Obj.following.Radiation_Treatments) ? "Y" : "N";
                PMH.MrdQueRecent = (Obj.following.Recent_Weight_Loss) ? "Y" : "N";
                PMH.MrdQueRenal = (Obj.following.Renal_Dialysis) ? "Y" : "N";

                PMH.MrdQueRheumatic = (Obj.following.Rheumatic_Fever) ? "Y" : "N";
                PMH.MrdQueRheumatism = (Obj.following.Rheumatism) ? "Y" : "N";
                PMH.MrdQueScarlet = (Obj.following.Scarlet_Fever) ? "Y" : "N";

                PMH.MrdQueShingles = (Obj.following.Shingles) ? "Y" : "N";
                PMH.MrdQueSickle = (Obj.following.Sickle_Cell_Disease) ? "Y" : "N";
                PMH.MrdQueSinus = (Obj.following.Sinus_Trouble) ? "Y" : "N";

                PMH.MrdQueSpina = (Obj.following.Spina_Bifida) ? "Y" : "N";
                PMH.MrdQueStomach = (Obj.following.Stomach_Intestinal_Disease) ? "Y" : "N";
                PMH.MrdQueStroke = (Obj.following.Stroke) ? "Y" : "N";

                PMH.MrdQueSwelling = (Obj.following.Swelling_of_Limbs) ? "Y" : "N";
                PMH.MrdQueThyroid = (Obj.following.Thyroid_Disease) ? "Y" : "N";
                PMH.MrdQueTonsillitis = (Obj.following.Tonsillitis) ? "Y" : "N";

                PMH.MrdQueTuberculosis = (Obj.following.Tuberculosis) ? "Y" : "N";
                PMH.MrdQueTumors = (Obj.following.Tumors_or_Growths) ? "Y" : "N";
                PMH.MrdQueUlcers = (Obj.following.Ulcers) ? "Y" : "N";

                PMH.MrdQueVenereal = (Obj.following.Venereal_Disease) ? "Y" : "N";
                PMH.MrdQueYellow = (Obj.following.Yellow_Jaundice) ? "Y" : "N";
                return PMH;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL--MappedMedicalHistory", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This Method Mapped Old DentalHistory Property to New DentalHistory Property.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public static PatientDentalHistory MappedDentalHistory(DentalHistorys Obj)
        {
            try
            {
                PatientDentalHistory PDH = new PatientDentalHistory();
                PDH.txtQue5 = Obj.DentistName;
                PDH.txtQue5a = Obj.DentistAddress;
                PDH.txtQue5b = Obj.DentistPhone;
                PDH.txtQue5c = Obj.DentistEmail;
                PDH.txtQue6 = Obj.Last_Teeth_Cleening_Date;
                PDH.txtQue7 = Obj.Regural_Visit_Dentist;
                PDH.rdQue7a = (Obj.IsRegural_visit) ? "Y" : "N";
                PDH.txtQue12 = Obj.Prob_Complain;
                if (Obj.Teeth_been_Replaced != null)
                {
                    foreach (var item in Obj.Teeth_been_Replaced)
                    {
                        if (SafeValue<string>(item) == "Fixed Bridge")
                        {
                            PDH.rdoQue11aFixedbridge = "Y";
                        }
                        if (SafeValue<string>(item) == "Removeable Bridge")
                        {
                            PDH.rdoQue11bRemoveablebridge = "Y";
                        }
                        if (SafeValue<string>(item) == "Denture")
                        {
                            PDH.rdoQue11cDenture = "Y";
                        }
                        if (SafeValue<string>(item) == "Implant")
                        {
                            PDH.rdQue11dImplant = "Y";
                        }
                    }
                }
                PDH.rdQue15 = (Obj.clench_your_teeth) ? "Y" : "N";
                PDH.rdQue16 = (Obj.jaw_click_or_pop) ? "Y" : "N";
                PDH.rdQue17 = (Obj.soreness_in_the_muscles) ? "Y" : "N";
                PDH.rdQue18 = (Obj.frequent_headaches) ? "Y" : "N";
                PDH.rdQue19 = (Obj.food_get_caught) ? "Y" : "N";
                if (Obj.teeth_sensitive != null)
                {
                    foreach (var item in Obj.teeth_sensitive)
                    {
                        if (SafeValue<string>(item) == "Hot")
                        {
                            PDH.chkQue20_1 = "Y";
                        }
                        if (SafeValue<string>(item) == "Cold")
                        {
                            PDH.chkQue20_2 = "Y";
                        }
                        if (SafeValue<string>(item) == "Sweets")
                        {
                            PDH.chkQue20_3 = "Y";
                        }
                        if (SafeValue<string>(item) == "Pressure")
                        {
                            PDH.chkQue20_4 = "Y";
                        }
                    }
                }
                PDH.rdQue21 = (Obj.Gums_Bleed) ? "Y" : "N";
                PDH.txtQue21a = SafeValue<string>(Obj.str_Gums_Bleed);
                PDH.txtQue23 = Obj.Dental_floss;
                PDH.txtQue22 = Obj.Brush_your_teeth;
                PDH.rdQue24 = (Obj.Loose_tipped_shifted) ? "Y" : "N";
                PDH.rdQue28 = (Obj.isoften_offensive) ? "Y" : "N";
                PDH.txtQue28c = SafeValue<string>(Obj.str_offensive);
                PDH.txtQue29 = Obj.orthodontic_work_you_done;
                PDH.txtQue29a = Obj.Dental_work_you_done;
                PDH.txtQue26 = Obj.Your_Teeth_in_general;
                PDH.txtComments = Obj.Additional_info;
                PDH.txtDateoffirstvisit = Obj.Dateoffirstvisit;
                PDH.Reasonforfirstvisit = Obj.Reasonforfirstvisit;
                PDH.Dateoflastvisit = Obj.Dateoflastvisit;
                PDH.Reasonforlastvisit = Obj.Reasonforlastvisit;
                PDH.Dateofnextvisit = Obj.Dateofnextvisit;
                PDH.Reasonfornextvisit = Obj.Reasonfornextvisit;
                return PDH;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL--MappedDentalHistory", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool AddUserAsRewardPartner(int UserId)
        {
            bool res = clsColleaguesData.AddUserAsRewardPartner(UserId);
            return res;
        }

        public static PatientRewardMDL GetMemberFullDetails(int UserId)
        {
            try
            {
                PatientRewardMDL Obj = new PatientRewardMDL();
                DataTable dt = clsColleaguesData.GetUserFullDetailsById(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    //I have Added Ternary Operator on each condition because it's a Required fields and need to supply those parameter any how.
                    Obj.contact_first_name = SafeValue<string>(dt.Rows[0]["FirstName"]);
                    Obj.contact_last_name = SafeValue<string>(dt.Rows[0]["LastName"]);
                    Obj.address_1 = (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["ExactAddress"]))) ? SafeValue<string>(dt.Rows[0]["ExactAddress"]) : "Street 1";
                    Obj.address_2 = SafeValue<string>(dt.Rows[0]["Address2"]);
                    Obj.name = (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["AccountName"]))) ? SafeValue<string>(dt.Rows[0]["AccountName"]) : SafeValue<string>(dt.Rows[0]["FirstName"]) + ' ' + SafeValue<string>(dt.Rows[0]["LastName"]);
                    Obj.support_email = SafeValue<string>(dt.Rows[0]["Username"]);
                    Obj.city = (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["City"]))) ? SafeValue<string>(dt.Rows[0]["City"]) : "Carson City";
                    Obj.state = (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["State"]))) ? SafeValue<string>(dt.Rows[0]["State"]) : "Nevada";
                    //+1 added because the Reward Partner will accept only those number it has contain +1 on any phone fields.
                    Obj.support_phone = (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Phone"]))) ? SafeValue<string>(dt.Rows[0]["Phone"]) : "";
                    Obj.zip_code = (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["ZipCode"]))) ? SafeValue<string>(dt.Rows[0]["ZipCode"]) : "89701";
                    Obj.external_company_id = SafeValue<int>(dt.Rows[0]["AccountId"]);
                    //+1 added because the Reward Partner will accept only those number it has contain +1 on any phone fields.
                    Obj.contact_phone = (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Mobile"]))) ? SafeValue<string>(dt.Rows[0]["Mobile"]) : "";
                    string ImageName = SafeValue<string>(dt.Rows[0]["ImageName"]);
                    //if (!string.IsNullOrWhiteSpace(ImageName))
                    //{
                    //    string Path = ConfigurationManager.AppSettings.Get("DoctorImage");
                    //    string ImageWithPath = Path + ImageName;
                    //    string Base64Image = clsHelper.GetImageBase64(ImageWithPath);
                    //    Obj.logo = Base64Image;
                    //}
                    //Unmask support phone
                    string Uphone = UnmaskPhone(Obj.support_phone);
                    Obj.support_phone = (string.IsNullOrWhiteSpace(Uphone)) ? "" : "+1 " + Uphone;

                    string cphone = UnmaskPhone(Obj.contact_phone);
                    Obj.contact_phone = (string.IsNullOrWhiteSpace(cphone)) ? "" : "+1 " + cphone;
                }
                return Obj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---GetMemberFullDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static IDictionary<responsemdl, responseerror> InsertCompnyonRewardPartner(PatientRewardMDL Obj, int UserId)
        {
            string Res = "";
            try
            {
                responsemdl obj = new responsemdl();
                responseerror errobj = new responseerror();
                Dictionary<responsemdl, responseerror> ResType = new Dictionary<responsemdl, responseerror>();
                string Token = string.Empty;

                //-- Hardipsinh Jadeja : putting contact_phone and support_phone empty for now.
                HttpResponseMessage Response = new HttpResponseMessage();
                Response = AuthenticationPR.CallApi("dentalApi/v1/companies", new FormUrlEncodedContent(new[] {
                        new KeyValuePair<string,string>("external_company_id",SafeValue<string>(Obj.external_company_id)),
                        new KeyValuePair<string, string>("name",Obj.name),
                        new KeyValuePair<string, string>("support_email",Obj.support_email),
                        new KeyValuePair<string, string>("support_phone",string.Empty),
                        new KeyValuePair<string, string>("contact_first_name",Obj.contact_first_name),
                        new KeyValuePair<string, string>("contact_last_name",Obj.contact_last_name),
                        new KeyValuePair<string, string>("contact_phone",string.Empty),
                        new KeyValuePair<string, string>("logo",Obj.logo),
                        new KeyValuePair<string, string>("status",SafeValue<string>("1")),
                        new KeyValuePair<string, string>("address_1",Obj.address_1),
                        new KeyValuePair<string, string>("address_2",Obj.address_2),
                        new KeyValuePair<string, string>("city",Obj.city),
                        new KeyValuePair<string, string>("state",Obj.state),
                        new KeyValuePair<string, string>("zip_code",Obj.zip_code),
                        //new KeyValuePair<string, string>("latitude","33.2400016784668"),
                        //new KeyValuePair<string, string>("longitude","-111.96"),
                    }), Token);
                if (Response.IsSuccessStatusCode)
                {
                    var Result = Response.Content.ReadAsStringAsync().Result;
                    obj = JsonConvert.DeserializeObject<responsemdl>(Result);
                    ResType.Add(obj, errobj);
                    clsColleaguesData.InsertCompanyonRewardPatform(UserId, Obj.external_company_id, obj.tran_id, obj.company.company_id);
                }
                else
                {
                    var Results = Response.Content.ReadAsStringAsync().Result;
                    Res = SafeValue<string>(Results);
                    errobj = JsonConvert.DeserializeObject<responseerror>(Results);
                    ResType.Add(obj, errobj);
                }
                return ResType;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---InsertCompnyonRewardPartner", Ex.Message, Res);
                throw;
            }
        }

        public static Location GetMemberLocationDetails(int UserId, int LocationId)
        {
            try
            {
                Location locaction = new Location();
                //Location locobj = new Location();
                DataTable dt = clsColleaguesData.GetMemberLocationDetails(UserId, LocationId);
                if (dt != null && dt.Rows.Count > 0)
                {

                    string cphone = (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["Phone"]))) ? SafeValue<string>(dt.Rows[0]["Phone"]) : "";
                    string uphone = UnmaskPhone(cphone);
                    string UnmaskedPhone = (!string.IsNullOrWhiteSpace(uphone)) ? "+1" + UnmaskPhone(uphone) : "";
                    if (UnmaskedPhone.Length == 10)
                    {
                        UnmaskedPhone = "(" + UnmaskedPhone.Substring(0, 3) + ") "
                                            + UnmaskedPhone.Substring(3, 3) + "-"
                                                + UnmaskedPhone.Substring(6, 4);
                    }
                    else
                    {
                        UnmaskedPhone = "";
                    }
                    //string UnmaskedPhone = (string.IsNullOrWhiteSpace(uphone)) ? "" : "+1 " + uphone;
                    locaction.company_id = SafeValue<int>(dt.Rows[0]["RewardCompanyID"]);
                    locaction.external_location_id = SafeValue<int>(dt.Rows[0]["LocationId"]);
                    locaction.name = SafeValue<string>(dt.Rows[0]["Location"]);
                    locaction.contact_first_name = SafeValue<string>(dt.Rows[0]["FirstName"]);
                    locaction.contact_last_name = SafeValue<string>(dt.Rows[0]["LastName"]);
                    locaction.contact_phone = UnmaskedPhone;
                    locaction.address_1 = (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["ExactAddress"]))) ? SafeValue<string>(dt.Rows[0]["ExactAddress"]) : SafeValue<string>(dt.Rows[0]["Address2"]);
                    locaction.address_2 = SafeValue<string>(dt.Rows[0]["Address2"]);
                    locaction.city = (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["City"]))) ? SafeValue<string>(dt.Rows[0]["City"]) : "Carson City";
                    locaction.state = (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["State"]))) ? SafeValue<string>(dt.Rows[0]["State"]) : "Nevada";
                    locaction.zip_code = (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["ZipCode"]))) ? SafeValue<string>(dt.Rows[0]["ZipCode"]) : "89701";


                    //-- Assign dummy content for required fields.
                    locaction.address_1 = "NO_ADDRESS_GIVEN";
                }
                return locaction;
            }
            catch (Exception ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL--GetMemberLocationDetails", ex.Message, ex.StackTrace);
                throw;
            }

        }

        public static IDictionary<responseLoc, responseerror> InsertMemberLocationDetails(BO.ViewModel.Location locobj, int UserId)
        {
            string Res = "";
            try
            {
                responseerror errobj = new responseerror();
                responseLoc obj = new responseLoc();
                string Token = string.Empty;

                HttpResponseMessage Response = new HttpResponseMessage();
                Dictionary<responseLoc, responseerror> ResType = new Dictionary<responseLoc, responseerror>();

                Response = AuthenticationPR.CallApi("dentalApi/v1/locations", new FormUrlEncodedContent(new[] {
                        new KeyValuePair<string,string>("company_id",SafeValue<string>(locobj.company_id)),
                        new KeyValuePair<string,string>("external_location_id",SafeValue<string>(locobj.external_location_id)),
                        new KeyValuePair<string, string>("name",locobj.name),
                        new KeyValuePair<string, string>("contact_first_name",locobj.contact_first_name),
                        new KeyValuePair<string, string>("contact_last_name",locobj.contact_last_name),
                        new KeyValuePair<string, string>("contact_phone",locobj.contact_phone),
                        new KeyValuePair<string, string>("address_1",locobj.address_1),
                        new KeyValuePair<string, string>("address_2",locobj.address_2),
                        new KeyValuePair<string, string>("city",locobj.city),
                        new KeyValuePair<string, string>("state",locobj.state),
                        new KeyValuePair<string, string>("zip_code",locobj.zip_code),
                        //new KeyValuePair<string, string>("latitude","33.2400016784668"),
                        //new KeyValuePair<string, string>("longitude","-111.96"),
                    }), Token);

                if (Response.IsSuccessStatusCode)
                {
                    var Result = Response.Content.ReadAsStringAsync().Result;
                    obj = JsonConvert.DeserializeObject<responseLoc>(Result);
                    ResType.Add(obj, errobj);
                    clsColleaguesData.InsertLocationDetailsOfUser(UserId, obj.location.external_location_id, locobj.company_id, obj.tran_id, obj.location.location_id);
                }
                else
                {
                    var Results = Response.Content.ReadAsStringAsync().Result;
                    Res = SafeValue<string>(Results);
                    errobj = JsonConvert.DeserializeObject<responseerror>(Results);
                    ResType.Add(obj, errobj);
                }

                return ResType;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---InsertCompnyonRewardPartner", Ex.Message, Res);
                throw;
            }
        }
        public static string UnmaskPhone(string Phone)
        {
            //string x = Phone.Trim();
            //if (x == "" || x == null)
            //{
            //    Phone = "";
            //}
            //else if (x.Contains('(') || x.Contains(')') || x.Contains('-'))
            //{
            //    string z = x.Replace("-", "").Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
            //    Phone = z;
            //}
            //return Phone;
            if (!string.IsNullOrWhiteSpace(Phone))
                return System.Text.RegularExpressions.Regex.Replace(Phone, @"[^\d]", "");
            else
                return string.Empty;
        }
        public static bool RegisterSuperDentist(int Userid)
        {
            try
            {
                return true;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---RegisterSuperDentist", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static SuperResponse<List<PatientRewardList>> GetSupPotentialRewardList(Reward_PatientRewardCall Obj)
        {
            try
            {
                SuperResponse<List<PatientRewardList>> ListOfPotential = new SuperResponse<List<PatientRewardList>>();
                List<PatientRewardList> Lst = new List<PatientRewardList>();
                DataSet ds = new DataSet();
                ds = clsPatientsData.GetSupPatientRewardsList(Obj);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows[0][0].ToString() == "-1")
                    {
                        ListOfPotential.Message = "Start date must be smaller then end date";
                        ListOfPotential.Result = null;
                        ListOfPotential.StatusCode = 500;
                        ListOfPotential.IsSuccess = false;
                        return ListOfPotential;
                    }
                }
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(ds.Tables[0].Rows[0]["TotalPotentialReward"])))
                    {
                        ListOfPotential.TotalPotentialRewards = SafeValue<double>(ds.Tables[0].Rows[0]["TotalPotentialReward"]);
                    }
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(ds.Tables[1].Rows[0]["TotalActualReward"])))
                    {
                        ListOfPotential.TotalActualRewards = SafeValue<double>(ds.Tables[1].Rows[0]["TotalActualReward"]);
                    }
                    if (ds.Tables[2].Rows.Count != 0)
                    {
                        ListOfPotential.SupPatientId = SafeValue<int>(ds.Tables[2].Rows[0]["RewardPartnerId"]);
                    }
                    if (ds.Tables[2] != null && ds.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow item in ds.Tables[2].Rows)
                        {
                            Lst.Add(new PatientRewardList()
                            {
                                RewardCode = SafeValue<string>(item["Code"]),
                                RewardText = SafeValue<string>(item["RewardText"]),
                                RewardType = SafeValue<int>(item["RewardType"]),
                                Point = SafeValue<double>(item["Point"]),
                                Date = SafeValue<DateTime>(item["CreatedDate"]),
                                Id = SafeValue<int>(item["PatientRewardId"]),
                                //RewardPatnerRewardId = SafeValue<int>(item["RewardPlatformRewardId"])

                                //RewardPatnerRewardId = SafeValue<int>(item["RewardPlatformRewardId"])
                            });
                        }
                    }
                    ListOfPotential.Result = Lst;
                }
                ListOfPotential.IsSuccess = true;
                ListOfPotential.Message = "Success";
                ListOfPotential.StatusCode = 200;
                return ListOfPotential;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---GetSupPotentialRewardList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int CheckPatientHasExists(int PatientID)
        {
            try
            {
                int RewardType = 0;
                DataTable dt = clsPatientsData.checkPatientExists(PatientID);
                if (dt != null && dt.Rows.Count > 0)
                {
                    RewardType = SafeValue<int>(dt.Rows[0]["RewardType"]);
                }
                return RewardType;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---CheckPatientHasExists", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static OrganizationResponse<List<DoctorDetails>> GetDentistListOfAccount(Account Obj)
        {
            try
            {
                List<DoctorDetails> LstDentistDetails = new List<DoctorDetails>();
                OrganizationResponse<List<DoctorDetails>> Main = new OrganizationResponse<List<DoctorDetails>>();
                DataTable dt = clsPatientsData.GetDentistsListOfAccount(Obj.AccountId, SafeValue<int>(Obj.LocationId));

                foreach (DataRow item in dt.Rows)
                {
                    DataSet ds = new clsColleaguesData().GetDoctorDetailsById(SafeValue<int>(item["OwnerId"]));
                    if (ds != null && ds.Tables[0].Rows.Count > 0)
                    {
                        LstDentistDetails.Add(GetDoctorDetails(SafeValue<int>(item["OwnerId"]), ds, true));
                    }
                }

                Main.TotalRecord = LstDentistDetails.Count;
                Main.IsSuccess = true;
                Main.StatusCode = 200;
                Main.Message = "Success";
                Main.Result = LstDentistDetails;
                return Main;

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---GetDentistListOfAccount", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This method used for getting dentrix details form database and filed up on the model.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DentrixDetail GetDentrixDetailsofDoctor(DataTable dt)
        {
            try
            {
                DentrixDetail Obj = new DentrixDetail();
                if (dt != null && dt.Rows.Count > 0)
                {
                    Obj.IdNumber = SafeValue<int>(dt.Rows[0]["idnum"]);
                    Obj.ssn = SafeValue<int>(dt.Rows[0]["ssn"]);
                    Obj.Provnum = SafeValue<int>(dt.Rows[0]["provnum"]);
                    Obj.ProviderType = SafeValue<int>(dt.Rows[0]["provtype"]);
                    Obj.phoneNext = SafeValue<int>(dt.Rows[0]["phoneext"]);
                    Obj.Isnonperson = SafeValue<int>(dt.Rows[0]["isnonperson"]);
                    Obj.feeSched = SafeValue<int>(dt.Rows[0]["feesched"]);
                    Obj.ProviderClass = SafeValue<int>(dt.Rows[0]["provclass"]);
                    Obj.ProviderTypeString = SafeValue<string>(dt.Rows[0]["providertypestring"]);
                }
                return Obj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---GetDentrixDetailsofDoctor", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static int CheckAccountHasLinked(int AccountId, int UserId = 0)
        {
            try
            {
                int RewardType = 0;
                DataTable dt = clsPatientsData.checkAcountLinked(AccountId, UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    RewardType = SafeValue<int>(dt.Rows[0]["RewardType"]);
                }
                return RewardType;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---CheckPatientHasExists", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This method is use for get the plan detail for user and filled up on the model
        /// </summary>
        /// <param name="AccountId"></param>
        /// <returns></returns>
        public static OrganizationResponse<List<Plan>> GetPlanListForUser(int AccountId)
        {
            try
            {
                List<Plan> Lst = new List<Plan>();
                DataTable dt = new DataTable();
                OrganizationResponse<List<Plan>> Main = new OrganizationResponse<List<Plan>>();
                dt = clsColleaguesData.GetPlanListForUser(AccountId);
                Main.TotalRecord = dt.Rows.Count;
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        Lst.Add(new Plan()
                        {
                            PlanId = SafeValue<int>(item["PlanId"]),
                            PlanName = SafeValue<string>(item["PlanName"]),
                            Amount = SafeValue<decimal>(item["Amount"]),
                            RegisterReward = SafeValue<decimal>(item["RegisterReward"]),
                            IsPrimary = SafeValue<bool>(item["IsPrimary"]),
                            Points = SafeValue<decimal>(item["Points"]),
                            ExamCodes = SafeValue<int>(item["ExamCodes"])
                        });
                    }
                }
                Main.IsSuccess = true;
                Main.StatusCode = 200;
                Main.Message = "Success";
                Main.Result = Lst;
                return Main;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---GetPlanList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool CheckPlanIdExistsornot(int RewardPartnerId, int PlanId)
        {
            try
            {
                int HasExists = clsPatientsData.CheckPlanExistsOfUser(RewardPartnerId, PlanId);
                return (HasExists > 0) ? true : false;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---CheckPlanIdExistsornot", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static List<Patient_DentrixAccountAgingModel> ValidationPatientAccountAging(List<Patient_DentrixAccountAgingModel> lstPatient_DentrixAccountAging)
        {
            List<Patient_DentrixAccountAgingModel> lstPatientDentrixAccountAging = new List<Patient_DentrixAccountAgingModel>();

            foreach (var item in lstPatient_DentrixAccountAging)
            {
                if (SafeValue<DateTime>(item.AgingDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    item.AgingDate = null;
                }
                if (SafeValue<DateTime>(item.AutoModifiedTimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    item.AutoModifiedTimestamp = null;
                }
                if (SafeValue<DateTime>(item.LastBillingDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    item.LastBillingDate = null;
                }
                if (SafeValue<DateTime>(item.LastInsurancePayDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    item.LastInsurancePayDate = null;
                }
                if (SafeValue<DateTime>(item.LastPaymentDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    item.LastPaymentDate = null;
                }
                lstPatientDentrixAccountAging.Add(item);
            }

            return lstPatientDentrixAccountAging;
        }

        /// <summary>
        /// This Method used for inserting the guarantor account aging records on RL database.
        /// </summary>
        /// <param name="lstPatient_DentrixAccountAging"></param>
        /// <returns></returns>
        public static RewardResponse InsertPatient_DentrixAccountAgingDetails(List<Patient_DentrixAccountAgingModel> lstPatient_DentrixAccountAging)
        {
            List<Patient_DentrixAccountAgingModel> lstPatientAccountAging = new List<Patient_DentrixAccountAgingModel>();
            RewardResponse Response = new RewardResponse();
            List<InsertedRecord> lst = new List<InsertedRecord>();
            List<NotInsertedRecord> Lst = new List<NotInsertedRecord>();
            try
            {
                Response.TotalInsertedRecord = 0;
                Response.TotalNotInsertedRecord = 0;
                Response.ReceivedRecords = lstPatient_DentrixAccountAging.Count;

                lstPatientAccountAging = ValidationPatientAccountAging(lstPatient_DentrixAccountAging);

                if(CheckDentrixConnectorKeyExists(lstPatientAccountAging[0].dentrixConnectorId))
                {
                    DataSet dsResult = clsPatientsData.InsertPatient_DentrixAccountAgingDetails(lstPatientAccountAging);

                    if (dsResult != null && dsResult.Tables.Count > 1)
                    {
                        Response.TotalInsertedRecord = dsResult.Tables[1].Rows.Count;

                        foreach (DataRow row in dsResult.Tables[1].Rows)
                        {
                            lst.Add(new InsertedRecord()
                            {
                                RLId = SafeValue<int>(row["Id"]),
                                DentrixId = SafeValue<int>(row["GuarId"]),
                                ProviderId = SafeValue<int>(row["CreatedBy"])
                            });
                        }

                        Response.InsertedList = lst;

                        Response.TotalNotInsertedRecord = dsResult.Tables[0].Rows.Count;

                        foreach (DataRow row1 in dsResult.Tables[0].Rows)
                        {
                            Lst.Add(new NotInsertedRecord()
                            {
                                DentrixId = SafeValue<int>(row1["GuarId"]),
                                ErrorMessage = "An errors occurs while inserting records to RL"
                            });
                        }

                        Response.NotInsertedList = Lst;
                    }
                    else
                    {
                        //NotInsertedRecord UnInserted = new NotInsertedRecord();
                        //UnInserted.ErrorMessage = "Invalid Dentrix ConnectorID";
                        //UnInserted.DentrixId = 
                        //Lst.Add(UnInserted);
                        //Response.NotInsertedList = Lst;
                        Response.TotalNotInsertedRecord = lstPatient_DentrixAccountAging.Count;
                        Response.NotInsertedList = new List<NotInsertedRecord>();
                        foreach (var item in lstPatient_DentrixAccountAging)
                        {
                            Response.NotInsertedList.Add(new NotInsertedRecord()
                            {
                                ErrorMessage = "Invalid Dentrix ConnectorID",
                                DentrixId = item.GuarId
                            });
                        }

                    }
                }
                else
                {

                    Response.TotalNotInsertedRecord = lstPatient_DentrixAccountAging.Count;
                    Response.NotInsertedList = new List<NotInsertedRecord>();
                    foreach (var item in lstPatient_DentrixAccountAging)
                    {
                        Response.NotInsertedList.Add(new NotInsertedRecord()
                        {
                            DentrixId = item.GuarId,
                            ErrorMessage = "Invalid Dentrix ConnectorID"
                        });
                    }
                }

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---InsertPatient_DentrixFinanceCharges", Ex.Message, Ex.StackTrace);
                NotInsertedRecord UnInserted = new NotInsertedRecord();
                UnInserted.ErrorMessage = Ex.Message;
                Response.TotalNotInsertedRecord = Response.TotalNotInsertedRecord + 1;
                Lst.Add(UnInserted);
                Response.NotInsertedList = Lst;
            }

            return Response;
        }

        /// <summary>
        /// This Method used for fetching account aging records on RL database.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public static DentrixAging GetAgingRecords(DentrixAgingInputModel Obj, int UserId)
        {
            try
            {
                DentrixAging objDentrixAgingModel = new DentrixAging();
                DataTable dt = clsPatientsData.GetAgingRecords(Obj.PatientId, UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        objDentrixAgingModel.Aging0to30 = Math.Round(SafeValue<double>(item["Aging0to30"]), 2);
                        objDentrixAgingModel.Aging31to60 = Math.Round(SafeValue<double>(item["Aging31to60"]), 2);
                        objDentrixAgingModel.Aging61to90 = Math.Round(SafeValue<double>(item["Aging61to90"]), 2);
                        objDentrixAgingModel.Aging91plus = Math.Round(SafeValue<double>(item["Aging91plus"]), 2);
                       
                        objDentrixAgingModel.AgingDate = SafeValue<DateTime>(item["AgingDate"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(item["AgingDate"]);
                        objDentrixAgingModel.AutoModifiedTimestamp = SafeValue<DateTime>(item["AutoModifiedTimestamp"]);
                        objDentrixAgingModel.DentrixConnectorId = SafeValue<string>(item["DentrixConnectorId"]);
                        objDentrixAgingModel.LastBillingDate = SafeValue<DateTime>(item["LastBillingDate"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(item["LastBillingDate"]);
                        objDentrixAgingModel.LastInsurancePayDate = SafeValue<DateTime>(item["LastInsurancePayDate"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(item["LastInsurancePayDate"]); 
                        objDentrixAgingModel.LastPaymentDate = SafeValue<DateTime>(item["LastPaymentDate"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(item["LastPaymentDate"]); 
                        objDentrixAgingModel.GuarId = SafeValue<int>(item["GuarId"]);
                        objDentrixAgingModel.DentrixGuarId = SafeValue<int>(item["GuarantorPatientId"]);
                        objDentrixAgingModel.PatientId = SafeValue<int>(item["PatientId"]);
                        objDentrixAgingModel.DentrixPatientId = SafeValue<int>(item["DentrixPatientId"]);
                    }
                }
                else
                {
                    objDentrixAgingModel = null;
                }
                return objDentrixAgingModel;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("SmileBrand---GetAgingRecords", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This Method is used for Edit Patient Reward for SuperBucks.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <param name="AccountId"></param>
        /// <returns></returns>
        public static RetirveReward<bool> EditPatientRewards(EditSupPatientReward model, int UserId, int AccountId)
        {
            try
            {
                RetirveReward<bool> retrive = new RetirveReward<bool>();
                int Result = clsPatientsData.EditPatientReward(model, UserId, AccountId);
                switch (Result)
                {
                    case 1:
                        retrive.StatusCode = 200;
                        retrive.IsSuccess = true;
                        retrive.Message = "Reward edited successfully!";
                        retrive.Result = true;
                        return retrive;
                    case 0:
                        retrive.StatusCode = 500;
                        retrive.IsSuccess = false;
                        retrive.Message = "Something want to wrong while editing Reward";
                        retrive.Result = false;
                        return retrive;
                    default:
                        retrive.StatusCode = 404;
                        retrive.IsSuccess = false;
                        retrive.Message = "RewardId doesn't exists on the system!";
                        retrive.Result = false;
                        return retrive;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---EditPatientRewards", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This Method is used for Add Patient Reward for SuperBucks.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <param name="AccountId"></param>
        /// <returns></returns>
        public static RetirveReward<Reward> AddPatientReward(AddPatientReward model, int UserId, int AccountId)
        {
            try
            {
                RetirveReward<Reward> retrive = new RetirveReward<Reward>();
                int Result = clsPatientsData.AddPatientReward(model, UserId, AccountId);
                Reward Obj = new Reward();
                Obj.RewardId = Result;
                switch (Result)
                {
                    case -1:
                        retrive.StatusCode = 404;
                        retrive.IsSuccess = false;
                        retrive.Message = "Patient doesn't associate with the user!";
                        retrive.Result = null;
                        return retrive;
                    case -2:
                        retrive.StatusCode = 404;
                        retrive.IsSuccess = false;
                        retrive.Message = "Procedure code doesn't associated with the user!";
                        retrive.Result = null;
                        return retrive;
                    default:
                        retrive.StatusCode = 200;
                        retrive.IsSuccess = true;
                        retrive.Message = "Reward added successfully!";
                        retrive.Result = Obj;
                        return retrive;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---AddPatientReward", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This Method used for Delete Patient Reward for SuperBucks.
        /// </summary>
        /// <param name="RewardId"></param>
        /// <param name="AccountId"></param>
        /// <returns></returns>
        public static RetirveReward<bool> DeletePatientReward(int RewardId, int AccountId)
        {
            try
            {
                RetirveReward<bool> retrive = new RetirveReward<bool>();
                int Result = clsPatientsData.DeletePatientReward(RewardId, AccountId);
                switch (Result)
                {
                    case -1:
                        retrive.StatusCode = 500;
                        retrive.IsSuccess = false;
                        retrive.Message = "This reward is grant as Actual, So you can't delete this reward!";
                        retrive.Result = false;
                        return retrive;
                    case -2:
                        retrive.StatusCode = 404;
                        retrive.IsSuccess = false;
                        retrive.Message = "The Reward-id doesn't associate with this user!";
                        retrive.Result = false;
                        return retrive;
                    case -3:
                        retrive.StatusCode = 200;
                        retrive.IsSuccess = true;
                        retrive.Message = "Success";
                        retrive.Result = true;
                        return retrive;
                    default:
                        retrive.StatusCode = 500;
                        retrive.IsSuccess = false;
                        retrive.Message = "Something want to wrong while deleting Reward";
                        retrive.Result = false;
                        return retrive;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---DeletePatientReward", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static RewardResponse BulkInsertProcedures(List<Patient_DentrixProcedure> Obj)
        {
            try
            {
                RewardResponse rwRes = new RewardResponse();
                rwRes.ReceivedRecords = Obj.Count;
                if (CheckDentrixConnectorKeyExists(Obj[0].dentrixConnectorID))
                {
                    BulkResponse bulk = new BulkResponse();
                    for (int i = 0; i < Obj.Count; i++)
                    {
                        if (SafeValue<DateTime>(Obj[i].txplanneddate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].txplanneddate = null;
                        }
                        if (SafeValue<DateTime>(Obj[i].startdate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].startdate = null;
                        }
                        if (SafeValue<DateTime>(Obj[i].completiondate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].completiondate = null;
                        }
                        if (SafeValue<DateTime>(Obj[i].createdate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].createdate = null;
                        }
                    }

                    DataTable Dt = clsCommon.ToDataTable(Obj);
                    DateTime Cdate = DateTime.Now;
                    DataColumn Column = new DataColumn("RLCreationDate", typeof(SqlDateTime));
                    Column.DefaultValue = Cdate;
                    Dt.Columns.Add(Column);


                    bulk = clsPatientsData.BulkInsertOfPatientProcedure(Dt);

                    //SP get Inserted or not inserted Data from Temp Table

                    rwRes = clsPatientsData.GetNotInsertedRecordsListofProcedure(Dt);
                    rwRes.TotalInsertedRecord = rwRes.InsertedList != null ? rwRes.InsertedList.Count() : 0;
                    rwRes.TotalNotInsertedRecord = rwRes.NotInsertedList != null ? rwRes.NotInsertedList.Count() : 0;
                    return rwRes;
                }
                else
                {
                    rwRes.TotalNotInsertedRecord = Obj.Count;
                    rwRes.NotInsertedList = new List<NotInsertedRecord>();
                    foreach (var item in Obj)
                    {
                        rwRes.NotInsertedList.Add(new NotInsertedRecord()
                        {
                            DentrixId = item.DentrixId,
                            ErrorMessage = "Invalid Dentrix ConnectorID"
                        });
                    }
                    rwRes.TotalInsertedRecord = 0;
                    rwRes.InsertedList = null;
                    return rwRes;
                }

                //return bulk;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---BulkInsertProcedures", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static RewardResponse BulkInsertAdjustment(List<Patient_DentrixAdjustments> Obj)
        {
            try
            {
                RewardResponse rwRes = new RewardResponse();
                rwRes.ReceivedRecords = Obj.Count;

                if (CheckDentrixConnectorKeyExists(Obj[0].dentrixConnectorID))
                {
                    for (int i = 0; i < Obj.Count; i++)
                    {
                        if (SafeValue<DateTime>(Obj[i].procdate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].procdate = null;
                        }
                        if (SafeValue<DateTime>(Obj[i].automodifiedtimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].automodifiedtimestamp = null;
                        }
                    }
                    DataTable Dt = clsCommon.ToDataTable(Obj);
                    DateTime Cdate = DateTime.Now;
                    DataColumn Column = new DataColumn("CreationDate", typeof(SqlDateTime));
                    Column.DefaultValue = Cdate;
                    Dt.Columns.Add(Column);

                    clsPatientsData.BulkInsertOfPatientAdjustment(Dt);
                    //SP get Inserted or not inserted Data from Temp Table
                   
                    rwRes = clsPatientsData.GetAdjustmentsRewardResponse(Dt);
                    rwRes.ReceivedRecords = Dt.Rows.Count;
                    rwRes.TotalInsertedRecord = rwRes.InsertedList != null ? rwRes.InsertedList.Count() : 0;
                    rwRes.TotalNotInsertedRecord = rwRes.NotInsertedList != null ? rwRes.NotInsertedList.Count() : 0;
                    return rwRes;
                }
                else
                {
                    rwRes.TotalNotInsertedRecord = Obj.Count;
                    rwRes.NotInsertedList = new List<NotInsertedRecord>();
                    foreach (var item in Obj)
                    {
                        rwRes.NotInsertedList.Add(new NotInsertedRecord()
                        {
                            DentrixId = item.adjustid,
                            ErrorMessage = "Invalid Dentrix ConnectorID"
                        });
                    }
                    rwRes.TotalInsertedRecord = 0;
                    rwRes.InsertedList = null;
                    return rwRes;
                }

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---BulkInsertAdjustment", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static RewardResponse BulkInsertFinance(List<Patient_DentrixFinanceCharges> Obj)
        {
            try
            {
                RewardResponse rwRes = new RewardResponse();
                rwRes.ReceivedRecords = Obj.Count;
                if(CheckDentrixConnectorKeyExists(Obj[0].dentrixConnectorID))
                {
                    for (int i = 0; i < Obj.Count; i++)
                    {
                        if (SafeValue<DateTime>(Obj[i].procdate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].procdate = null;
                        }
                        if (SafeValue<DateTime>(Obj[i].automodifiedtimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].automodifiedtimestamp = null;
                        }
                    }
                    DataTable Dt = clsCommon.ToDataTable(Obj);
                    DateTime Cdate = DateTime.Now;
                    DataColumn Column = new DataColumn("CreationDate", typeof(SqlDateTime));
                    Column.DefaultValue = Cdate;
                    Dt.Columns.Add(Column);

                    clsPatientsData.BulkInsertOfPatientFinance(Dt);
                    //SP get Inserted or not inserted Data from Temp Table

                    rwRes = clsPatientsData.GetFinanceRewardResponse(Dt);
                    rwRes.TotalInsertedRecord = rwRes.InsertedList != null ? rwRes.InsertedList.Count() : 0;
                    rwRes.TotalNotInsertedRecord = rwRes.NotInsertedList != null ? rwRes.NotInsertedList.Count() : 0;
                    return rwRes;
                }
                else
                {
                    rwRes.TotalNotInsertedRecord = Obj.Count;
                    rwRes.NotInsertedList = new List<NotInsertedRecord>();
                    foreach (var item in Obj)
                    {
                        rwRes.NotInsertedList.Add(new NotInsertedRecord()
                        {
                            DentrixId = item.chargeId,
                            ErrorMessage = "Invalid Dentrix ConnectorID"
                        });
                    }
                    rwRes.TotalInsertedRecord = 0;
                    rwRes.InsertedList = null;
                    return rwRes;
                }

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---BulkInsertFinance", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static RewardResponse BulkInsertOfInsurance(List<Patient_DentrixInsurancePayment> Obj)
        {
            try
            {
                RewardResponse rwRes = new RewardResponse();
                rwRes.ReceivedRecords = Obj.Count;
                if(CheckDentrixConnectorKeyExists(Obj[0].dentrixConnectorID))
                {
                    for (int i = 0; i < Obj.Count; i++)
                    {
                        if (SafeValue<DateTime>(Obj[i].createdate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].createdate = null;
                        }
                        if (SafeValue<DateTime>(Obj[i].procdate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].procdate = null;
                        }
                        if (SafeValue<DateTime>(Obj[i].automodifiedtimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].automodifiedtimestamp = null;
                        }
                    }
                    DataTable Dt = clsCommon.ToDataTable(Obj);
                    DateTime Cdate = DateTime.Now;
                    DataColumn Column = new DataColumn("CreationDate", typeof(SqlDateTime));
                    Column.DefaultValue = Cdate;
                    Dt.Columns.Add(Column);

                    clsPatientsData.BulkInsertOfPatientInsurance(Dt);
                    //SP get Inserted or not inserted Data from Temp Table
                    rwRes = clsPatientsData.GetInsuranceRewardResponse(Dt);
                    rwRes.ReceivedRecords = Dt.Rows.Count;

                    rwRes.TotalInsertedRecord = rwRes.InsertedList != null ? rwRes.InsertedList.Count() : 0;
                    rwRes.TotalNotInsertedRecord = rwRes.NotInsertedList != null ? rwRes.NotInsertedList.Count() : 0;
                    return rwRes;
                }
                else
                {
                    rwRes.TotalNotInsertedRecord = Obj.Count;
                    rwRes.NotInsertedList = new List<NotInsertedRecord>();
                    foreach (var item in Obj)
                    {
                        rwRes.NotInsertedList.Add(new NotInsertedRecord()
                        {
                            DentrixId = item.paymentId,
                            ErrorMessage = "Invalid Dentrix ConnectorID"
                        });
                    }
                    rwRes.TotalInsertedRecord = 0;
                    rwRes.InsertedList = null;
                    return rwRes;
                }

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---BulkInsertOfInsurance", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static RewardResponse BulkInsertOfPatientStandard(List<Patient_DentrixStandardPayment> Obj)
        {
            try
            {
                RewardResponse rwRes = new RewardResponse();
                rwRes.ReceivedRecords = Obj.Count;
                if(CheckDentrixConnectorKeyExists(Obj[0].dentrixConnectorID))
                {
                    for (int i = 0; i < Obj.Count; i++)
                    {
                        if (SafeValue<DateTime>(Obj[i].createdate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].createdate = null;
                        }
                        if (SafeValue<DateTime>(Obj[i].procdate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].procdate = null;
                        }
                        if (SafeValue<DateTime>(Obj[i].automodifiedtimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].automodifiedtimestamp = null;
                        }
                        if (SafeValue<DateTime>(Obj[i].createdatedatetime).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].createdatedatetime = null;
                        }
                        if (SafeValue<DateTime>(Obj[i].procdatedatetime).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].procdatedatetime = null;
                        }
                        Obj[i].paymentdesc = SafeValue<string>(Obj[i].paymentdesc).Trim();
                    }
                    DataTable Dt = clsCommon.ToDataTable(Obj);
                    DateTime Cdate = DateTime.Now;
                    DataColumn Column = new DataColumn("CreationDate", typeof(SqlDateTime));
                    Column.DefaultValue = Cdate;
                    Dt.Columns.Add(Column);

                    clsPatientsData.BulkInsertOfPatientStandard(Dt);
                    //SP get Inserted or not inserted Data from Temp Table
                    rwRes = clsPatientsData.GetStandardRewardResponse(Dt);
                    rwRes.TotalInsertedRecord = rwRes.InsertedList != null ? rwRes.InsertedList.Count() : 0;
                    rwRes.TotalNotInsertedRecord = rwRes.NotInsertedList != null ? rwRes.NotInsertedList.Count() : 0;
                    return rwRes;
                }
                else
                {
                    rwRes.TotalNotInsertedRecord = Obj.Count;
                    rwRes.NotInsertedList = new List<NotInsertedRecord>();
                    foreach (var item in Obj)
                    {
                        rwRes.NotInsertedList.Add(new NotInsertedRecord()
                        {
                            DentrixId = item.paymentId,
                            ErrorMessage = "Invalid Dentrix ConnectorID"
                        });
                    }
                    rwRes.TotalInsertedRecord = 0;
                    rwRes.InsertedList = null;
                    return rwRes;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---BulkInsertOfPatientStandard", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static RedimRewardsResponseModel<bool> RedimRewards(RedimRewardsRequestModel model)
        {
            try
            {
                model.AmountToRedim = Math.Round(model.AmountToRedim, 2);
                Common.RedimRewardStatus Response = clsPatientsData.RedimRewards(model);
                string ErrorMsg;
                int StatusCode = 0;
                switch (Response)
                {
                    case Common.RedimRewardStatus.NotEnoughPoints:
                        ErrorMsg = $"RewardPartnerId:{model.RewardPartnerId} does not have enough points to redim.";
                        StatusCode = 400;
                        break;
                    case Common.RedimRewardStatus.NotFound:
                        ErrorMsg = $"RewardPartnerId:{model.RewardPartnerId} not found.";
                        StatusCode = 404;
                        break;
                    case Common.RedimRewardStatus.Failed:
                        ErrorMsg = $"Unexpected error occured while redim rewards for RewardPartnerId:{model.RewardPartnerId}";
                        StatusCode = 500;
                        break;
                    case Common.RedimRewardStatus.AmountDoesNotMatch:
                        ErrorMsg = $"Entered amount ${model.AmountToRedim} does not match for RewardPartnerId:{model.RewardPartnerId}";
                        StatusCode = 400;
                        break;
                    default:
                        ErrorMsg = string.Empty;
                        StatusCode = 200;
                        break;
                }

                var result = new RedimRewardsResponseModel<bool>()
                {
                    IsSuccess = (Response == Common.RedimRewardStatus.Success),
                    Message = ErrorMsg,
                    Result = (Response == Common.RedimRewardStatus.Success),
                    StatusCode = StatusCode,
                    TransactionId = model.TransactionId
                };
                return result;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---RedimRewards", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static RewardResponse BulkInsertPatientDentrixAccountAging(List<Patient_DentrixAccountAgingModel> Obj)
        {
            try
            {
                RewardResponse rwRes = new RewardResponse();
                rwRes.ReceivedRecords = Obj.Count;
                if (CheckDentrixConnectorKeyExists(Obj[0].dentrixConnectorId))
                {
                    BulkResponse bulk = new BulkResponse();
                    for (int i = 0; i < Obj.Count; i++)
                    {
                        if (SafeValue<DateTime>(Obj[i].AutoModifiedTimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].AutoModifiedTimestamp = null;
                        }
                        if (SafeValue<DateTime>(Obj[i].AgingDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].AgingDate = null;
                        }
                        if (SafeValue<DateTime>(Obj[i].LastPaymentDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].LastPaymentDate = null;
                        }
                        if (SafeValue<DateTime>(Obj[i].LastInsurancePayDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].LastInsurancePayDate = null;
                        }
                        if (SafeValue<DateTime>(Obj[i].LastBillingDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].LastBillingDate = null;
                        }
                    }

                    DataTable Dt = clsCommon.ToDataTable(Obj);
                    DateTime Cdate = DateTime.Now;
                    DataColumn Column = new DataColumn("RLCreatedDate", typeof(SqlDateTime));
                    Column.DefaultValue = Cdate;
                    Dt.Columns.Add(Column);


                    bulk = clsPatientsData.BulkInsertOfPatientDentrixAccountAging(Dt);

                    //SP get Inserted or not inserted Data from Temp Table

                    rwRes = clsPatientsData.GetNotInsertedRecordsListofAccountAging(Dt);
                    rwRes.TotalInsertedRecord = rwRes.InsertedList != null ? rwRes.InsertedList.Count() : 0;
                    rwRes.TotalNotInsertedRecord = rwRes.NotInsertedList != null ? rwRes.NotInsertedList.Count() : 0;
                    return rwRes;
                }
                else
                {
                    rwRes.TotalNotInsertedRecord = Obj.Count;
                    rwRes.NotInsertedList = new List<NotInsertedRecord>();
                    foreach (var item in Obj)
                    {
                        rwRes.NotInsertedList.Add(new NotInsertedRecord()
                        {
                            DentrixId = item.GuarId,
                            ErrorMessage = "Invalid Dentrix ConnectorID"
                        });
                    }
                    rwRes.TotalInsertedRecord = 0;
                    rwRes.InsertedList = null;
                    return rwRes;
                }

                //return bulk;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---BulkInsertPatientDentrixAccountAging", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static RewardResponse BulkInsertAppointments_DentrixDetails(List<Appointments> Obj)
        {
            try
            {
                RewardResponse rwRes = new RewardResponse();
                rwRes.ReceivedRecords = Obj.Count;
                if (CheckDentrixConnectorKeyExists(Obj[0].dentrixConnectorID))
                {
                    BulkResponse bulk = new BulkResponse();
                    for (int i = 0; i < Obj.Count; i++)
                    {
                        string txproc = string.Empty; string adacode = string.Empty;
                        if (Convert.ToDateTime(Obj[i].automodifiedtimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].automodifiedtimestamp = null;
                        }
                        if (Convert.ToDateTime(Obj[i].AppointmentDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            DateTime sqlMinDateAsNetDateTime = (DateTime)SqlDateTime.MinValue;
                            Obj[i].AppointmentDate = sqlMinDateAsNetDateTime.AddYears(1);
                        }
                        if (Convert.ToDateTime(Obj[i].BrokenDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].BrokenDate = null;
                        }
                        if (Convert.ToDateTime(Obj[i].AppointmentStart).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            DateTime sqlMinDate = SqlDateTime.MinValue.Value;
                            Obj[i].AppointmentStart = new DateTime(sqlMinDate.Year, sqlMinDate.Month, sqlMinDate.Day, Obj[i].AppointmentStart.Hour, Obj[i].AppointmentStart.Minute, Obj[i].AppointmentStart.Second);
                        }
                        //if (Obj[i].TxProcs != null && Obj[i].TxProcs.Count > 0)
                        //{
                        //    txproc = string.Join(",", Obj[i].TxProcs.ToArray());
                        //}
                        //if (Obj[i].AdaCodes != null && Obj[i].AdaCodes.Count > 0)
                        //{
                        //    adacode = string.Join(",", Obj[i].AdaCodes.ToArray());
                        //}
                    }

                    DataTable Dt = clsCommon.ToDataTableAppointments(Obj);
                    DateTime Cdate = DateTime.Now;
                    DataColumn Column = new DataColumn("RLCreatedDate", typeof(SqlDateTime));
                    Column.DefaultValue = Cdate;
                    Dt.Columns.Add(Column);


                    bulk = clsPatientsData.BulkInsertOfAppointments_DentrixDetails(Dt);


                    //SP get Inserted or not inserted Data from Temp Table

                    rwRes = clsPatientsData.GetNotInsertedRecordsListofAppointmentsDentrixDetails(Dt);
                    rwRes.TotalInsertedRecord = rwRes.InsertedList != null ? rwRes.InsertedList.Count() : 0;
                    rwRes.TotalNotInsertedRecord = rwRes.NotInsertedList != null ? rwRes.NotInsertedList.Count() : 0;
                    return rwRes;
                }
                else
                {
                    rwRes.TotalNotInsertedRecord = Obj.Count;
                    rwRes.NotInsertedList = new List<NotInsertedRecord>();
                    foreach (var item in Obj)
                    {
                        rwRes.NotInsertedList.Add(new NotInsertedRecord()
                        {
                            DentrixId = item.AppointmentId,
                            ErrorMessage = "Invalid Dentrix ConnectorID"
                        });
                    }
                    rwRes.TotalInsertedRecord = 0;
                    rwRes.InsertedList = null;
                    return rwRes;
                }

                //return bulk;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---BulkInsertAppointments_DentrixDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }


        public static RewardResponse BulkInsertPatients(List<DentrixPatient> Obj)
        {
            try
            {
                clsCommon objCommon = new clsCommon();
                RewardResponse rwRes = new RewardResponse();
                rwRes.ReceivedRecords = Obj.Count;
                if (CheckDentrixConnectorKeyExists(Obj[0].dentrixConnectorID))
                {
                    BulkResponse bulk = new BulkResponse();
                    for (int i = 0; i < Obj.Count; i++)
                    {
                         Appointments obj = new Appointments();
                        if (SafeValue<DateTime>(Obj[i].FirstVisitDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].FirstVisitDate = null;
                        }
                        if (SafeValue<DateTime>(Obj[i].LastVisitDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].LastVisitDate = null;
                        }
                        if (SafeValue<DateTime>(Obj[i].birthdate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].birthdate = null;
                        }                       
                        if (SafeValue<DateTime>(Obj[i].automodifiedtimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            Obj[i].automodifiedtimestamp = null;
                        }
                        Obj[i].Password = objCommon.CreateRandomPassword(8);
                        if (Obj[i].Emails == null || Obj[i].Emails.Count == 0)
                        {
                            string Email = "email" + DateTime.Now.Ticks + "@domain.com";
                            Obj[i].Emails = Email.Split(new char[] { ',' }).ToList();
                        }

                    }

                    DataTable Dt = clsCommon.ToDataTablePatients(Obj);
                    DateTime Cdate = DateTime.Now;
                    DataColumn Column = new DataColumn("RLCreatedDate", typeof(SqlDateTime));
                    Column.DefaultValue = Cdate;
                    Dt.Columns.Add(Column);


                    bulk = clsPatientsData.BulkInsertOfPatient(Dt);

                    //SP get Inserted or not inserted Data from Temp Table

                    rwRes = clsPatientsData.GetNotInsertedRecordsListofPatients(Dt);
                    rwRes.TotalInsertedRecord = rwRes.InsertedList != null ? rwRes.InsertedList.Count() : 0;
                    rwRes.TotalNotInsertedRecord = rwRes.NotInsertedList != null ? rwRes.NotInsertedList.Count() : 0;
                    return rwRes;
                }
                else
                {
                    rwRes.TotalNotInsertedRecord = Obj.Count;
                    rwRes.NotInsertedList = new List<NotInsertedRecord>();
                    foreach (var item in Obj)
                    {
                        rwRes.NotInsertedList.Add(new NotInsertedRecord()
                        {
                            DentrixId = item.dentrixConnectorID,
                            ErrorMessage = "Invalid Dentrix ConnectorID"
                        });
                    }
                    rwRes.TotalInsertedRecord = 0;
                    rwRes.InsertedList = null;
                    return rwRes;
                }

                //return bulk;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---BulkInsertProcedures", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static RewardResponse BulkInsertProviderSchedule(List<AddSchedule> schedules)
        {
            try
            {
                clsCommon objCommon = new clsCommon();
                RewardResponse rwRes = new RewardResponse();
                rwRes.ReceivedRecords = schedules.Count;
                if (CheckDentrixConnectorKeyExists(schedules[0].connectorID))
                {
                    BulkResponse bulk = new BulkResponse();
                    DataTable dtSource = new DataTable();
                    DataColumn Cols = new DataColumn("JsonText", typeof(string));
                    Cols.DefaultValue = string.Empty;
                    dtSource.Columns.Add(Cols);
                    for (int i = 0; i < schedules.Count; i++)
                    {
                        if (SafeValue<DateTime>(schedules[i].automodifiedtimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            schedules[i].automodifiedtimestamp = null;
                        }
                        if (SafeValue<DateTime>(schedules[i].date).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            schedules[i].date = null;
                        }
                        dtSource.Rows.Add(JsonConvert.SerializeObject(schedules[i]));
                    }
                    DataTable Dt = ToDataTableProviderSchedule(schedules);
                    DateTime Cdate = DateTime.Now;
                    DataColumn Column = new DataColumn("RLCreationDate", typeof(SqlDateTime));
                    Column.DefaultValue = Cdate;
                    Dt.Columns.Add(Column);
                    DataColumn Col = new DataColumn("JsonText", typeof(string));
                    Col.DefaultValue = string.Empty;
                    Dt.Columns.Add(Col);
                    int rowIdx = 0;
                    Dt.AsEnumerable().All(row => { row["JsonText"] = dtSource.Rows[rowIdx++]["JsonText"]; return true; });
                    bulk = clsPatientsData.BulkInsertProviderSchedule(Dt);

                    rwRes = clsPatientsData.GetNotInsertedRecordsOfProviderSchedule(Dt);
                    rwRes.TotalInsertedRecord = rwRes.InsertedList != null ? rwRes.InsertedList.Count() : 0;
                    rwRes.TotalNotInsertedRecord = rwRes.NotInsertedList != null ? rwRes.NotInsertedList.Count() : 0;
                    return rwRes;
                }
                else
                {
                    rwRes.TotalNotInsertedRecord = schedules.Count;
                    rwRes.NotInsertedList = new List<NotInsertedRecord>();
                    foreach (var item in schedules)
                    {
                        rwRes.NotInsertedList.Add(new NotInsertedRecord()
                        {
                            DentrixId = item.connectorID,
                            ErrorMessage = "Invalid Dentrix ConnectorID"
                        });
                    }
                    rwRes.TotalInsertedRecord = 0;
                    rwRes.InsertedList = null;
                    return rwRes;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---BulkInsertProviderSchedule", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static RewardResponse BulkInsertOperatorySchedule(List<AddSchedule> schedules)
        {
            try
            {
                clsCommon objCommon = new clsCommon();
                RewardResponse rwRes = new RewardResponse();
                rwRes.ReceivedRecords = schedules.Count;
                if (CheckDentrixConnectorKeyExists(schedules[0].connectorID))
                {
                    BulkResponse bulk = new BulkResponse();
                    DataTable dtSource = new DataTable();
                    DataColumn Cols = new DataColumn("JsonText", typeof(string));
                    Cols.DefaultValue = string.Empty;
                    dtSource.Columns.Add(Cols);
                    for (int i = 0; i < schedules.Count; i++)
                    {
                        if (SafeValue<DateTime>(schedules[i].automodifiedtimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            schedules[i].automodifiedtimestamp = null;
                        }
                        if (SafeValue<DateTime>(schedules[i].date).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            schedules[i].date = null;
                        }
                        dtSource.Rows.Add(JsonConvert.SerializeObject(schedules[i]));
                    }
                    DataTable Dt = ToDataTableProviderSchedule(schedules);
                    DateTime Cdate = DateTime.Now;
                    DataColumn Column = new DataColumn("RLCreationDate", typeof(SqlDateTime));
                    Column.DefaultValue = Cdate;
                    Dt.Columns.Add(Column);
                    DataColumn Col = new DataColumn("JsonText", typeof(string));
                    Col.DefaultValue = string.Empty;
                    Dt.Columns.Add(Col);
                    int rowIdx = 0;
                    Dt.AsEnumerable().All(row => { row["JsonText"] = dtSource.Rows[rowIdx++]["JsonText"]; return true; });
                    bulk = clsPatientsData.BulkInsertOperatorySchedule(Dt);

                    rwRes = clsPatientsData.GetNotInsertedRecordsOfOperatorySchedule(Dt);
                    rwRes.TotalInsertedRecord = rwRes.InsertedList != null ? rwRes.InsertedList.Count() : 0;
                    rwRes.TotalNotInsertedRecord = rwRes.NotInsertedList != null ? rwRes.NotInsertedList.Count() : 0;
                    return rwRes;
                }
                else
                {
                    rwRes.TotalNotInsertedRecord = schedules.Count;
                    rwRes.NotInsertedList = new List<NotInsertedRecord>();
                    foreach (var item in schedules)
                    {
                        rwRes.NotInsertedList.Add(new NotInsertedRecord()
                        {
                            DentrixId = item.connectorID,
                            ErrorMessage = "Invalid Dentrix ConnectorID"
                        });
                    }
                    rwRes.TotalInsertedRecord = 0;
                    rwRes.InsertedList = null;
                    return rwRes;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---BulkInsertOperatorySchedule", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static RewardResponse BulkInsertPracticeSchedule(List<AddSchedule> schedules)
        {
            try
            {
                clsCommon objCommon = new clsCommon();
                RewardResponse rwRes = new RewardResponse();
                rwRes.ReceivedRecords = schedules.Count;
                if (CheckDentrixConnectorKeyExists(schedules[0].connectorID))
                {
                    BulkResponse bulk = new BulkResponse();
                    DataTable dtSource = new DataTable();
                    DataColumn Cols = new DataColumn("JsonText", typeof(string));
                    Cols.DefaultValue = string.Empty;
                    dtSource.Columns.Add(Cols);
                    for (int i = 0; i < schedules.Count; i++)
                    {
                        if (SafeValue<DateTime>(schedules[i].automodifiedtimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            schedules[i].automodifiedtimestamp = null;
                        }
                        if (SafeValue<DateTime>(schedules[i].date).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            schedules[i].date = null;
                        }
                        dtSource.Rows.Add(JsonConvert.SerializeObject(schedules[i]));
                    }
                    DataTable Dt = ToDataTableProviderSchedule(schedules);
                    DateTime Cdate = DateTime.Now;
                    DataColumn Column = new DataColumn("RLCreationDate", typeof(SqlDateTime));
                    Column.DefaultValue = Cdate;
                    Dt.Columns.Add(Column);
                    DataColumn Col = new DataColumn("JsonText", typeof(string));
                    Col.DefaultValue = string.Empty;
                    Dt.Columns.Add(Col);
                    int rowIdx = 0;
                    Dt.AsEnumerable().All(row => { row["JsonText"] = dtSource.Rows[rowIdx++]["JsonText"]; return true; });
                    bulk = clsPatientsData.BulkInsertPracticeSchedule(Dt);

                    rwRes = clsPatientsData.GetNotInsertedRecordsOfPracticeSchedule(Dt);
                    rwRes.TotalInsertedRecord = rwRes.InsertedList != null ? rwRes.InsertedList.Count() : 0;
                    rwRes.TotalNotInsertedRecord = rwRes.NotInsertedList != null ? rwRes.NotInsertedList.Count() : 0;
                    return rwRes;
                }
                else
                {
                    rwRes.TotalNotInsertedRecord = schedules.Count;
                    rwRes.NotInsertedList = new List<NotInsertedRecord>();
                    foreach (var item in schedules)
                    {
                        rwRes.NotInsertedList.Add(new NotInsertedRecord()
                        {
                            DentrixId = item.connectorID,
                            ErrorMessage = "Invalid Dentrix ConnectorID"
                        });
                    }
                    rwRes.TotalInsertedRecord = 0;
                    rwRes.InsertedList = null;
                    return rwRes;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---BulkInsertPracticeSchedule", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static List<DeletedRecord> DeleteProviderSchedule(List<RemoveProcedure> Obj)
        {
            try
            {
                List<DeletedRecord> DeletedList = new List<DeletedRecord>();
                if (CheckDentrixConnectorKeyExists(Obj[0].DentrixConnectorId))
                {
                    foreach (var item in Obj)
                    {
                        DeletedList.Add(new DeletedRecord()
                        {
                            DentrixId = item.DentrixId,
                            IsDeleted = clsPatientsData.DeleteProviderSchedule(item)
                        });
                    }
                }
                return DeletedList;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---DeleteProviderSchedule", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static List<DeletedRecord> DeletePracticeSchedule(List<RemoveProcedure> Obj)
        {
            try
            {
                List<DeletedRecord> DeletedList = new List<DeletedRecord>();
                if (CheckDentrixConnectorKeyExists(Obj[0].DentrixConnectorId))
                {
                    foreach (var item in Obj)
                    {
                        DeletedList.Add(new DeletedRecord()
                        {
                            DentrixId = item.DentrixId,
                            IsDeleted = clsPatientsData.DeletePracticeSchedule(item)
                        });
                    }
                }
                return DeletedList;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---DeletePracticeSchedule", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static List<DeletedRecord> DeleteOperatorySchedule(List<RemoveProcedure> Obj)
        {
            try
            {
                List<DeletedRecord> DeletedList = new List<DeletedRecord>();
                if (CheckDentrixConnectorKeyExists(Obj[0].DentrixConnectorId))
                {
                    foreach (var item in Obj)
                    {
                        DeletedList.Add(new DeletedRecord()
                        {
                            DentrixId = item.DentrixId,
                            IsDeleted = clsPatientsData.DeleteOperatorySchedule(item)
                        });
                    }
                }
                return DeletedList;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---DeleteOperatorySchedule", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}

