﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using DotNetOpenAuth.OAuth2;
using MailBee;
using MailBee.SmtpMail;
using System.Net.Http;

namespace BusinessLogicLayer
{
    public class GetAccessTokenBLL
    {
        #region get access token from google
        public static List<string> GoogleAccessToken(string authorizationCode, string gclientID, string gclientSecret,string redirecturi)
        {
            string actionUrl = "https://accounts.google.com/o/oauth2/token";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(actionUrl);
            request.CookieContainer = new CookieContainer();
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            string encodedParameters = string.Format(
                "client_id={1}&code={0}&client_secret={2}&redirect_uri={3}&grant_type={4}",
                HttpUtility.UrlEncode(authorizationCode),
                HttpUtility.UrlEncode(gclientID),
                HttpUtility.UrlEncode(gclientSecret),
                HttpUtility.UrlEncode(redirecturi),
                HttpUtility.UrlEncode("authorization_code")
                );
            byte[] requestData = Encoding.UTF8.GetBytes(encodedParameters);
            request.ContentLength = requestData.Length;
            if (requestData.Length > 0)
                using (Stream stream = request.GetRequestStream())
                    stream.Write(requestData, 0, requestData.Length);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string responseText = null;
            using (TextReader reader = new StreamReader(response.GetResponseStream(), Encoding.ASCII))
                responseText = reader.ReadToEnd();
            List<string> strToken = new List<string>();
            foreach (string sPair in responseText.
                Replace("{", "").
                Replace("}", "").
                Replace("\"", "").
                Split(new string[] { ",\n" }, StringSplitOptions.None))
            {
                string[] pair = sPair.Split(':');
                if ("access_token" == pair[0].Trim())
                {
                    strToken.Add(pair[1].Trim());                    
                }
                if ("refresh_token" == pair[0].Trim())
                {
                    strToken.Add(pair[1].Trim());
                    break;
                }                
            }

            return strToken;
        }
        #endregion

        #region get access token from outlook 
        public static List<string> OutlookAccessToken(string clientid, string clientSecret, string key, string redirectUri)
        {
            UserAgentClient consumer;
            IAuthorizationState grantedAccess;

            StringDictionary p2 = new StringDictionary();
            p2.Add(OAuth2.RedirectUriKey, redirectUri);

            AuthorizationServerDescription server = new AuthorizationServerDescription();
            server.AuthorizationEndpoint = new Uri("https://login.live.com/oauth20_authorize.srf");
            server.TokenEndpoint = new Uri("https://login.live.com/oauth20_token.srf");
            server.ProtocolVersion = ProtocolVersion.V20;

            consumer = new UserAgentClient(server, clientid, clientSecret);

            consumer.ClientCredentialApplicator = ClientCredentialApplicator.PostParameter(clientSecret);

            IAuthorizationState authorizationState = new AuthorizationState(null);

            if (p2.ContainsKey(OAuth2.RedirectUriKey))
            {
                authorizationState.Callback = new Uri(p2[OAuth2.RedirectUriKey]);
            }

            try
            {
                grantedAccess = consumer.ProcessUserAuthorization(
                    new Uri("http://example.com/?code=" + key), authorizationState);
            }
            catch (DotNetOpenAuth.Messaging.ProtocolException e)
            {

                return null;
            }

            var lst = grantedAccess.AccessToken;
            List<string> strToken = new List<string>();
            strToken.Add(grantedAccess.AccessToken);
            if (!string.IsNullOrEmpty(grantedAccess.RefreshToken))
            {
                strToken.Add(grantedAccess.RefreshToken);
            }            
            return strToken;
        }
        #endregion


    }
}
