using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.ViewModel;
using BO.Models;
using DataAccessLayer.Common;
using DataAccessLayer.ColleaguesData;
using System.Data;
using System.Configuration;
using System.IO;
using Excel;
using static DataAccessLayer.Common.clsCommon;

namespace BusinessLogicLayer
{
    public class ColleaguesBLL
    {
        public List<ColleagueDetails> lstColleague = null;

        public static string MaskPhone(string Phone)
        {
            try
            {
                string x = Phone.Trim();
                double y = 0.0d;
                if (x == "" || x == null)
                {
                    Phone = "";
                }
                else if (x.Contains('.') || x.Contains('+'))
                {
                    Phone = x;
                }
                else if (!x.Contains('(') || !x.Contains(')') || !x.Contains('-'))
                {
                    if (x.Contains("-"))
                    {
                        string z = x.Replace("-", "").Replace(" ", "");
                        y = SafeValue<double>(z);
                    }
                    else if (x.Contains(" "))
                    {
                        string z = x.Replace(" ", "");
                        y = SafeValue<double>(z);
                    }
                    else
                    {
                        y = SafeValue<double>(x);
                    }
                    Phone = String.Format("{0:(###)-###-####}", y);
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ColleagueBLL-MaskPhone", Ex.Message, Ex.StackTrace);
                Phone = string.Empty;
                return Phone;
            }
            return Phone;
        }
        public static bool Inserthasbuttonval(List<ColleagueDetails> sp, int UserId)
        {
            bool res = true;
            try
            {
                if (sp.Count > 0)
                {
                    foreach (var item in sp)
                    {
                        res = clsColleaguesData.Inserthasbuttonval(item, UserId);
                    }
                }
                return res;

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-Inserthasbuttonval", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool EnableDisablehasbuttonval(int ColleagueId, int GivenUserId)
        {
            bool res = true;
            try
            {
                res = clsColleaguesData.EnableDisablehasbuttonval(ColleagueId, GivenUserId);
                return res;

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-EnableDisablehasbuttonval", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static int GetAccountIdbyUserId(int UserId, string opr)
        {
            int AccountId = 0;
            DataTable dt = new DataTable();
            try
            {
                dt = clsColleaguesData.GetAccountIdByUserId(UserId, opr);
                if (dt != null && dt.Rows.Count > 0)
                {
                    AccountId = SafeValue<int>(dt.Rows[0]["AccountId"]);
                }
                return AccountId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("Colleagues-GetAccountIdbyUserId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }


        //Colleague
        //public static List<ColleagueDetails> DashboardColleagueDetails(Sorting Obj)
        //{
        //    List<ColleagueDetails> lst = new List<ColleagueDetails>();
        //    Obj.PageIndex = 1;
        //    Obj.Id = Obj.Id;
        //    Obj.PageSize = 10;
        //    Obj.SearchText = null;
        //    Obj.SortColumn = 13;
        //    Obj.SortDirection = 1;
        //    Obj.NewSearchText = null;
        //    Obj.FilterBy = 0;
        //    lst = ColleaguesDetailsForDoctor(Obj);
        //    return lst;
        //}

        public List<ColleagueDetails> ColleaguesDetailsForDoctor(int UserId, int StartIndex, int EndSize, string Searchtext, int SortColumn, int SortDirection, string NewSearchText, int FilterBy)
        {
            clsCommon objCommon = new clsCommon();

            List<ColleagueDetails> lst = new List<ColleagueDetails>();
            clsColleaguesData objColleaguesData = new clsColleaguesData();
            DataSet ds = new DataSet();
            ds = objColleaguesData.GetColleaguesDetailsForDoctorWithSpeacilities(UserId, StartIndex, EndSize, Searchtext, SortColumn, SortDirection, NewSearchText, FilterBy);

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                string ImageName = "";
                ColleagueDetails ObjColleaguesDetails = new ColleagueDetails();


                ObjColleaguesDetails.ColleagueId = SafeValue<int>(item["ColleagueId"]);
                ObjColleaguesDetails.FirstName = SafeValue<string>(item["FirstName"]);
                ObjColleaguesDetails.LastName = SafeValue<string>(item["LastName"]);
                ObjColleaguesDetails.MiddleName = SafeValue<string>(item["MiddleName"]);
                ImageName = SafeValue<string>(item["ImageName"]);
                if (string.IsNullOrEmpty(ImageName))
                {
                    ObjColleaguesDetails.ImageName = SafeValue<string>(ConfigurationManager.AppSettings["DefaultDoctorImage"]);
                }
                else
                {
                    ObjColleaguesDetails.ImageName = SafeValue<string>(ConfigurationManager.AppSettings["DoctorImage"]) + ImageName;
                }

                ObjColleaguesDetails.ExactAddress = SafeValue<string>(item["ExactAddress"]);
                ObjColleaguesDetails.SecondaryEmail = SafeValue<string>(item["SecondaryEmail"]);

                ObjColleaguesDetails.Address2 = SafeValue<string>(item["Address2"]);
                ObjColleaguesDetails.City = SafeValue<string>(item["City"]);
                ObjColleaguesDetails.State = SafeValue<string>(item["State"]);
                ObjColleaguesDetails.Country = SafeValue<string>(item["Country"]);
                ObjColleaguesDetails.ZipCode = SafeValue<string>(item["ZipCode"]);
                ObjColleaguesDetails.EmailAddress = SafeValue<string>(item["EmailAddress"]);
                ObjColleaguesDetails.Phone = SafeValue<string>(item["Phone"]);
                ObjColleaguesDetails.Fax = SafeValue<string>(item["fax"]);
                ObjColleaguesDetails.Officename = SafeValue<string>(item["OfficeName"]);
                ObjColleaguesDetails.TotalRecord = SafeValue<int>(item["TotalRecord"]);
                //  ObjColleaguesDetails.ColleageReferralCount = SafeValue<int>(item["ColleageReferralCount"]);
                ObjColleaguesDetails.Location = SafeValue<string>(item["Location"]);
                ObjColleaguesDetails.LocationId = SafeValue<int>(item["LocationId"]);


                if (ds.Tables.Count > 1)
                {

                    DataTable dtSpeacilities = new DataTable();
                    var rows = ds.Tables[1].AsEnumerable()
                        .Where(x => ((int)x["Userid"]) == SafeValue<int>(item["ColleagueId"]));

                    if (rows.Any())
                        dtSpeacilities = rows.CopyToDataTable();


                    // Datatable 
                    //dtSpeacilities = objColleaguesData.GetMemberSpecialities(int.Parse(objCommon.CheckNull(item["ColleagueId"].ToString(), "0")));
                    List<SpeacilitiesOfDoctor> lstSpeacilities = new List<SpeacilitiesOfDoctor>();
                    foreach (DataRow Specialities in dtSpeacilities.Rows)
                    {
                        SpeacilitiesOfDoctor clsSpe = new SpeacilitiesOfDoctor();
                        clsSpe.SpecialtyId = int.Parse(string.IsNullOrWhiteSpace(SafeValue<string>(Specialities["Id"])) ? "0" : SafeValue<string>(Specialities["Id"]));
                        clsSpe.SpecialtyDescription = SafeValue<string>(Specialities["Specialities"]);
                        lstSpeacilities.Add(clsSpe);
                    }
                    ObjColleaguesDetails.lstSpeacilitiesOfDoctor = lstSpeacilities;
                }

                lst.Add(ObjColleaguesDetails);
            }


            return lst;

        }
        public static List<ColleagueDetails> DashboardColleaguePatientName(string SearchName)
        {
            List<ColleagueDetails> lst = new List<ColleagueDetails>();
            DataSet ds = new DataSet();
            clsColleaguesData objColleaguesData = new clsColleaguesData();
            ds = objColleaguesData.DashboardColleaguePatientName(SearchName);
            foreach (DataRow obj in ds.Tables[0].Rows)
            {
                ColleagueDetails clg = new ColleagueDetails();
                clg.FirstName = SafeValue<string>(obj["Name"]);
                clg.ColleagueId = SafeValue<int>(obj["Id"]);
                clg.EmailAddress = SafeValue<string>(obj["Email"]);
                clg.Type = SafeValue<string>(obj["Type"]);
                lst.Add(clg);
            }
            return lst;
        }

        public static List<AddressDetails> GetDentistLocationsById(string UserId)
        {
            DataTable dt = new DataTable();
            clsCommon objCommon = new clsCommon();
            clsColleaguesData ObjColleaguesData = new clsColleaguesData();
            dt = ObjColleaguesData.GetDoctorLocaitonById(SafeValue<int>(UserId));
            List<AddressDetails> lst = new List<AddressDetails>();
            List<AddressDetails> lstSort = new List<AddressDetails>();
            foreach (DataRow item in dt.Rows)
            {
                lst.Add(new AddressDetails
                {
                    AddressInfoID = SafeValue<int>(item["AddressInfoID"]),
                    ExactAddress = SafeValue<string>(item["ExactAddress"]),
                    Location = SafeValue<string>(item["Location"]),
                    City = SafeValue<string>(item["City"]),
                    State = SafeValue<string>(item["State"]),
                    ZipCode = SafeValue<string>(item["ZipCode"]),
                    ContactType = SafeValue<int>(item["ContactType"])
                });
            }
            lstSort = lst;
            lst = lstSort.Where(x => x.ContactType == 1).ToList();
            lst.AddRange(lstSort.Where(x => x.ContactType != 1).ToList());

            //lst = lst.Where(x => x.ContactType == 1).ToList();
            //lst.AddRange(lst.Where(x => x.ContactType != 1).ToList());
            return lst;
        }

        public static bool UpdateHasButton(int[] ColleaguesId, int GivenUserId, bool IsEnable)
        {
            try
            {
                foreach (var item in ColleaguesId)
                {
                    clsColleaguesData.DisableHasButton(item, GivenUserId);
                    if (IsEnable)
                    {
                        clsColleaguesData.EnableDisablehasbuttonval(item, GivenUserId);
                    }
                }
                return true;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ColleaguesBLL---UpdateHasButton", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static List<ColleagueDetails> GetColleaguesForOCR(FilterColleagues Obj, int UserId)
        {
            try
            {
                clsCommon objCommon = new clsCommon();
                List<ColleagueDetails> lst = new List<ColleagueDetails>();
                DataSet ds = new DataSet();
                ds = new clsColleaguesData().GetColleaguesDetailsForDoctorWithSpeacilities(UserId, Obj.PageIndex, Obj.PageSize, Obj.SearchText, Obj.SortColumn, Obj.SortDirection, Obj.NewSearchText, Obj.FilterBy);
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    string ImageName = "";
                    ColleagueDetails ObjColleaguesDetails = new ColleagueDetails();

                    ObjColleaguesDetails.ColleagueId = SafeValue<int>(item["ColleagueId"]);
                    ObjColleaguesDetails.FirstName = SafeValue<string>(item["FirstName"]);
                    ObjColleaguesDetails.LastName = SafeValue<string>(item["LastName"]);
                    ObjColleaguesDetails.MiddleName = SafeValue<string>(item["MiddleName"]);
                    ImageName = SafeValue<string>(item["ImageName"]);
                    if (string.IsNullOrEmpty(ImageName))
                    {
                        ObjColleaguesDetails.ImageName = SafeValue<string>(ConfigurationManager.AppSettings["DefaultDoctorImage"]);
                    }
                    else
                    {
                        ObjColleaguesDetails.ImageName = SafeValue<string>(ConfigurationManager.AppSettings["DoctorImage"]) + ImageName;
                    }

                    ObjColleaguesDetails.ExactAddress = SafeValue<string>(item["ExactAddress"]);
                    ObjColleaguesDetails.SecondaryEmail = SafeValue<string>(item["SecondaryEmail"]);
                    ObjColleaguesDetails.Address2 = SafeValue<string>(item["Address2"]);
                    ObjColleaguesDetails.City = SafeValue<string>(item["City"]);
                    ObjColleaguesDetails.State = SafeValue<string>(item["State"]);
                    ObjColleaguesDetails.Country = SafeValue<string>(item["Country"]);
                    ObjColleaguesDetails.ZipCode = SafeValue<string>(item["ZipCode"]);
                    ObjColleaguesDetails.EmailAddress = SafeValue<string>(item["EmailAddress"]);
                    ObjColleaguesDetails.Fax = SafeValue<string>(item["fax"]);
                    ObjColleaguesDetails.Officename = SafeValue<string>(item["OfficeName"]);
                    ObjColleaguesDetails.TotalRecord = SafeValue<int>(item["TotalRecord"]);
                    // ObjColleaguesDetails.ColleageReferralCount = SafeValue<int>(item["ColleageReferralCount"]);
                    ObjColleaguesDetails.Location = SafeValue<string>(item["Location"]);
                    ObjColleaguesDetails.LocationId = SafeValue<int>(item["LocationId"]);
                    ObjColleaguesDetails.Phone = SafeValue<string>(item["Phone"]);
                    ObjColleaguesDetails.Phone = MaskPhone(ObjColleaguesDetails.Phone);
                    ObjColleaguesDetails.HasButton = SafeValue<bool>(item["HasButton"]);
                    if (ds.Tables.Count > 1)
                    {
                        DataTable dtSpeacilities = new DataTable();
                        var rows = ds.Tables[1].AsEnumerable()
                            .Where(x => ((int)x["Userid"]) == SafeValue<int>(SafeValue<string>(item["ColleagueId"])));
                        if (rows.Any())
                            dtSpeacilities = rows.CopyToDataTable();
                        List<SpeacilitiesOfDoctor> lstSpeacilities = new List<SpeacilitiesOfDoctor>();
                        foreach (DataRow Specialities in dtSpeacilities.Rows)
                        {
                            SpeacilitiesOfDoctor clsSpe = new SpeacilitiesOfDoctor();
                            // clsSpe.SpecialtyId = int.Parse(objCommon.CheckNull(SafeValue<string>(Specialities["Id"]), "0"));
                            clsSpe.SpecialtyId = int.Parse(string.IsNullOrWhiteSpace(SafeValue<string>(Specialities["Id"])) ? "0" : SafeValue<string>(Specialities["Id"]));
                            clsSpe.SpecialtyDescription = SafeValue<string>(Specialities["Specialities"]);
                            lstSpeacilities.Add(clsSpe);
                        }
                        ObjColleaguesDetails.lstSpeacilitiesOfDoctor = lstSpeacilities;
                    }
                    ObjColleaguesDetails.referralsent = SafeValue<int>(item["SentCount"]);
                    ObjColleaguesDetails.refferralreceived = SafeValue<int>(item["ReceivedCount"]);
                    lst.Add(ObjColleaguesDetails);
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ColleaguesBLL---GetColleaguesForOCR", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static ResponseMeassge InviteColleagues(InviteColleague Obj, int UserId)
        {
            try
            {
                DataTable Dt = new DataTable(); string HasDoctorEmail = string.Empty; string Objs = string.Empty;
                ColleagueDetail colleagueDetail = ProfileDetail(UserId);
                StringBuilder EmailCount = new StringBuilder();
                ResponseMeassge objResponseMeassge = new ResponseMeassge();
                StringBuilder EmailCountAsStaffMember = new StringBuilder();
                List<ResponseEmail> objListResponseEmail = new List<ResponseEmail>();
                if (!string.IsNullOrEmpty(Obj.FilePath))
                {
                    string FileExtension = Path.GetExtension(Obj.FilePath);
                    if (FileExtension.ToLower() == ".xls" || FileExtension.ToLower() == ".xlsx")
                    {
                        string fileName = Path.GetFileName(Obj.FilePath);
                        string fileLocation = SafeValue<string>(ConfigurationManager.AppSettings["CSVUpload"]) + fileName;
                        DataSet ds = new DataSet();
                        DataTable dtExcelRecords = new DataTable();
                        FileInfo fi = new FileInfo(fileLocation);
                        ds = ReadExcel(fi.FullName, fi.Extension, true);
                        dtExcelRecords = ds.Tables[0];
                        if (dtExcelRecords != null && dtExcelRecords.Rows.Count > 0)
                        {
                            string ColumnName = string.Empty;
                            foreach (DataColumn item in dtExcelRecords.Columns)
                            {
                                var dtcolumn = Convert.ToString(item);
                                Dt.Columns.Add(dtcolumn);
                                if (string.IsNullOrEmpty(ColumnName))
                                {
                                    ColumnName =Convert.ToString(item);
                                }
                                else
                                {
                                    ColumnName = ColumnName + "," + Convert.ToString(item);
                                }
                            }

                            // Uploaded File Must Contain one Column Name :Email
                            if (ColumnName.Contains("Email"))
                            {
                                // Call for File Uploaded 
                                DataTable chek = new DataTable();
                                foreach (DataRow item in ds.Tables[0].Rows)
                                {
                                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(item["Email"])))
                                    {
                                        chek = new clsColleaguesData().CheckEmailInSystem(SafeValue<string>(item["Email"]));

                                    }
                                    if (chek == null || chek.Rows.Count == 0)
                                    {
                                        Dt.Rows.Add(item.ItemArray);
                                    }
                                }
                                if (Dt.Rows.Count > 0)
                                {
                                    bool done = CheckUserExistorNot(Dt, UserId);
                                }
                                foreach (DataRow item in ds.Tables[0].Rows)
                                {
                                    ResponseEmail objResponseEmail = new ResponseEmail();

                                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(item["Email"])))
                                    {
                                        if (colleagueDetail.EmailAddress != SafeValue<string>(item["Email"]))
                                        {
                                            bool checkEmailStaff = new clsColleaguesData().CheckEmailAsStaffMember(SafeValue<string>(item["Email"]));
                                            if (!checkEmailStaff)
                                            {
                                                
                                                if (!string.IsNullOrWhiteSpace(colleagueDetail.SecondaryEmail))
                                                {
                                                    if (colleagueDetail.SecondaryEmail != SafeValue<string>(item["Email"]))
                                                    {
                                                       // EmailCount = EmailCounts(UserId, SafeValue<string>(item["Email"]), Obj.Message);
                                                        int status = GetExistingEmail(UserId, SafeValue<string>(item["Email"]), Obj.Message);
                                                        if (status ==0)
                                                        {
                                                            objResponseEmail.EmailId = SafeValue<string>(item["Email"]);
                                                            objResponseEmail.Id = 2;
                                                        }
                                                        else if(status == 1)
                                                        {
                                                            objResponseEmail.EmailId = SafeValue<string>(item["Email"]);
                                                            objResponseEmail.Id = 1;
                                                        }
                                                        else if (status == 2)
                                                        {
                                                            objResponseEmail.EmailId = SafeValue<string>(item["Email"]);
                                                            objResponseEmail.Id = 5;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    //EmailCount = EmailCounts(UserId, SafeValue<string>(item["Email"]), Obj.Message);
                                                    int status = GetExistingEmail(UserId, SafeValue<string>(item["Email"]), Obj.Message);
                                                    if (status == 0)
                                                    {
                                                        objResponseEmail.EmailId = SafeValue<string>(item["Email"]);
                                                        objResponseEmail.Id = 2;
                                                    }
                                                    else if(status == 1)
                                                    {
                                                        objResponseEmail.EmailId = SafeValue<string>(item["Email"]);
                                                        objResponseEmail.Id = 1;
                                                    }
                                                    else if (status == 2)
                                                    {
                                                        objResponseEmail.EmailId = SafeValue<string>(item["Email"]);
                                                        objResponseEmail.Id = 5;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                EmailCountAsStaffMember = EmailCountAsStaffMembers(SafeValue<string>(item["Email"]));
                                                objResponseEmail.EmailId = SafeValue<string>(item["Email"]);
                                                objResponseEmail.Id = 3; 
                                            }
                                        }
                                        else
                                        {
                                            HasDoctorEmail = SafeValue<string>(item["Email"]);
                                            objResponseEmail.EmailId = SafeValue<string>(item["Email"]);
                                            objResponseEmail.Id = 4;
                                        }
                                        // Objs = "1"; 
                                    }
                                    else
                                    {
                                        //Objs = "4";
                                        objResponseMeassge.EmailRequired = "Please check your file. Email field is required!";

                                    }
                                    objListResponseEmail.Add(objResponseEmail);
                                }
                            }
                            else
                            {
                                objResponseMeassge.XlsError = "Your file must be in .xlsx or .xls format and at least one column name should be named Email.";
                               // Objs = "2";
                            }
                        }
                    }
                }
                else
                {
                    //CODE FOR SEND INVITATION USING EMAIL ADDRESS
                    if (!string.IsNullOrWhiteSpace(Obj.Email))
                    {
                        string[] Email = Obj.Email.Split(',');

                        DataTable Check = new DataTable();

                        Dt.Columns.Add("Email");
                        foreach (var item in Email)
                        {
                            Check = new clsColleaguesData().CheckEmailInSystem(SafeValue<string>(item));
                            if (Check.Rows.Count == 0)
                            {
                                Dt.Rows.Add(item);
                            }
                        }
                        if (Dt.Rows.Count > 0)
                        {
                            bool done = CheckUserExistorNot(Dt, UserId);
                        }
                        foreach (var item in Email)
                        {

                           ResponseEmail objResponseEmail = new ResponseEmail();

                            if (colleagueDetail.EmailAddress != item)
                            {
                                bool checkEmailStaff = new clsColleaguesData().CheckEmailAsStaffMember(SafeValue<string>(item));
                                if (!checkEmailStaff)
                                {
                                    if (!string.IsNullOrWhiteSpace(colleagueDetail.SecondaryEmail))
                                    {
                                        if (colleagueDetail.SecondaryEmail != item)
                                        {
                                            int  status = GetExistingEmail(UserId, item, Obj.Message);
                                            if (status==0)
                                            {
                                                objResponseEmail.EmailId = SafeValue<string>(item);
                                                objResponseEmail.Id = 2;
                                            }
                                            else if(status == 1)
                                            {
                                                objResponseEmail.EmailId = SafeValue<string>(item);
                                                objResponseEmail.Id = 1;
                                            }else if(status == 0)
                                            {
                                                objResponseEmail.EmailId = SafeValue<string>(item);
                                                objResponseEmail.Id = 5;
                                            }
                                        }
                                    }
                                    else
                                    {
                                       
                                        int status = GetExistingEmail(UserId, item, Obj.Message);
                                        if (status == 0)
                                        {
                                            objResponseEmail.EmailId = SafeValue<string>(item);
                                            objResponseEmail.Id = 2;
                                        }
                                        else if(status == 1)
                                        {
                                            objResponseEmail.EmailId = SafeValue<string>(item);
                                            objResponseEmail.Id = 1;
                                        }
                                        else if (status == 2)
                                        {
                                            objResponseEmail.EmailId = SafeValue<string>(item);
                                            objResponseEmail.Id = 5;
                                        }                                        
                                    }
                                }
                                else
                                {
                                    EmailCountAsStaffMember = EmailCountAsStaffMembers(SafeValue<string>(item));
                                    objResponseEmail.EmailId = SafeValue<string>(item);
                                    objResponseEmail.Id = 3;
                                }
                            }
                            else
                            {
                                HasDoctorEmail = SafeValue<string>(item);
                                objResponseEmail.EmailId = SafeValue<string>(item);
                                objResponseEmail.Id = 4;
                            }

                            objListResponseEmail.Add(objResponseEmail);
                        }
                        Objs = "1";
                       
                    }
                    if (Objs != null)
                    {
                        if (!string.IsNullOrEmpty(HasDoctorEmail))
                        {
                            if (SafeValue<string>(Objs) == "1")
                            {
                                //  Objs = "3";
                                objResponseMeassge.YourSelfInvite = "You can not invite yourself.";
                            }
                        }
                    }
                    //if (!string.IsNullOrWhiteSpace(SafeValue<string>(EmailCount)))
                    //{
                    //    var numberList = SafeValue<string>(EmailCount).Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();                   

                    //    //if (numberList.Count > 1)
                    //    //{
                    //    //    Objs = "Email id " + SafeValue<string>(EmailCount) + " already exist in colleague list. So, cannot send request to these colleagues.<br/>";
                    //    //}
                    //    //else
                    //    //{
                    //    //    Objs = "Email id " + SafeValue<string>(EmailCount) + " already exists in colleague list. So, cannot send request to this colleague.<br/>";
                    //    //}
                    //}

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(EmailCountAsStaffMember)))
                    {
                        // send Email to travis when someone try to add staff member as colleagues
                        //  new clsTemplate().StaffMemberInviteDoctor(UserId, SafeValue<string>(EmailCountAsStaffMember));

                        var numberList = SafeValue<string>(EmailCountAsStaffMember).Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                        if (numberList.Count > 0)
                        {
                            new clsTemplate().StaffMemberInviteDoctor(UserId,numberList);
                        }
                        //if (numberList.Count > 1)
                        //{

                        //    Objs += "Email id " + SafeValue<string>(EmailCountAsStaffMember) + "is register as a staff. So, cannot send request to this colleague.";
                        //} 
                        //else
                        //{
                        //    Objs += "Email id " + SafeValue<string>(EmailCountAsStaffMember) + "is register as a staff. So, cannot send request to this colleague.";
                        //}
                    }
                }

                //  return Objs;
                //   objResponseMeassge.objListResponseEmail = objResponseEmail;3
                objResponseMeassge.objListResponseEmail = objListResponseEmail;
                return objResponseMeassge;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ColleaguesBLL---GetColleaguesForOCR", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static StringBuilder EmailCounts(int UserId, string EmailAddress, string Message)
        {
            StringBuilder EmailCount = new StringBuilder();
            int status = GetExistingEmail(UserId, EmailAddress, Message);
            if (status ==0)
            {
                if (!string.IsNullOrWhiteSpace(SafeValue<string>(EmailCount)))
                {
                    EmailCount.Append(" , " + EmailAddress);
                }
                else
                {
                    EmailCount.Append(EmailAddress);
                }
            }
            return EmailCount;
        }


        public static StringBuilder EmailCountAsStaffMembers(string EmailAddress)
        {
            StringBuilder EmailCountAsStaffMembers = new StringBuilder();

         //   new clsTemplate().StaffMemberInviteDoctor(UserId, EmailAddress);

            if (!string.IsNullOrWhiteSpace(SafeValue<string>(EmailCountAsStaffMembers)))
                {
                EmailCountAsStaffMembers.Append(" , " + EmailAddress);
                }
                else
                {
                EmailCountAsStaffMembers.Append(EmailAddress);
                }
           
            return EmailCountAsStaffMembers;
        }



        private static DataSet ReadExcel(string filePath, string Extension, bool isHDR)
        {
            FileStream stream = System.IO.File.Open(filePath, FileMode.Open, FileAccess.Read);
            IExcelDataReader excelReader = null;

            switch (Extension)
            {
                case ".xls":
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                    break;
                case ".xlsx": //Excel 07
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    break;
            }
            excelReader.IsFirstRowAsColumnNames = isHDR;
            DataSet result = excelReader.AsDataSet();
            excelReader.Close();
            return result;
        }

        public static bool CheckUserExistorNot(DataTable dt, int Userid)
        {
            var html = GetHTMLofPatinetRecordw(dt);
            new clsTemplate().InviteColleaguesMailForSupport(html, Userid);
            return true;
        }

        public static string GetHTMLofPatinetRecordw(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"<table><thead><tr style='border: 1px solid #ddd;'>");
            foreach (DataColumn item in dt.Columns)
            {
                if (Convert.ToString(item) == "Email" || Convert.ToString(item) == "First Name" || Convert.ToString(item) == "Last Name" || Convert.ToString(item) == "Phone")
                {
                    sb.Append(@"<td><strong>" + Convert.ToString(item) + "</strong></td>");
                }
            }
            sb.Append(@"</tr></thead><tbody>");
            foreach (DataRow item in dt.Rows)
            {
                sb.Append(@"<tr style='border: 1px solid #ddd;'>");
                foreach (DataColumn item1 in dt.Columns)
                {
                    if (Convert.ToString(item1) == "Email" || Convert.ToString(item1) == "First Name" || Convert.ToString(item1) == "Last Name" || Convert.ToString(item1) == "Phone")
                    {
                        sb.Append(@"<td>" + Convert.ToString(item[item1]) + "</td>");
                    }
                }
                sb.Append(@"</tr>");
            }
            sb.Append(@"</tbody></table>");
            return Convert.ToString(sb);
        }

        public static ColleagueDetail ProfileDetail(int UserId)
        {
            ColleagueDetail obj = new ColleagueDetail();
            DataSet dsUser = new DataSet();
            dsUser = new clsColleaguesData().GetDoctorDetailsById(UserId);
            if (dsUser.Tables.Count > 0)
            {
                obj.EmailAddress = SafeValue<string>(dsUser.Tables[0].Rows[0]["Username"]);
                obj.SecondaryEmail = SafeValue<string>(dsUser.Tables[0].Rows[0]["SecondaryEmail"]);
            }
            return obj;
        }

        public static int GetExistingEmail(int UserID, string Email, string PersonalNotes)
        {
            try
            {
                clsColleaguesData objColleaguesData = new clsColleaguesData();
                string[] arryEmail = Email.Split(',');
                for (int i = 0; i < arryEmail.Count(); i++)
                {
                    DataTable dt = new DataTable();
                    dt = objColleaguesData.CheckEmailInSystemForColleguesInvite(arryEmail[i]);
                    DataTable dtCheckAsColleague = objColleaguesData.GetColleaguesDetailsForDoctor(UserID, 1, int.MaxValue, Email, 1, 2);

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        if (dtCheckAsColleague.Rows.Count == 0)
                        {
                            objColleaguesData.AddAsColleague(UserID, SafeValue<int>(dt.Rows[i]["UserId"]));
                            new clsTemplate().ColleagueInviteDoctor(UserID, SafeValue<int>(dt.Rows[i]["UserId"]), PersonalNotes);
                        }
                        else
                        {
                            return 0;
                        }
                    }
                    else
                    {
                        return  2;
                    }
                }
            }
            catch (Exception)
            {
                return 0;
            }

            return 1;
        }



    }
}
