﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using BO.Models;
using System.Configuration;
using System.Web;
using System.Web.Script.Serialization;
using DataAccessLayer.ZifyPay;
using Newtonsoft.Json;
using System.Data;
using DataAccessLayer.Common;
using DataAccessLayer.ColleaguesData;
using BO.ViewModel;
using System.Web.Mvc;
using System.Data.SqlClient;
using BO.Constatnt;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public class ZPPaymentBAL
    {
        /// <summary>
        /// This method is Generic method to send POST request to Zift pay.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string SendPOST(string url, string data)
        {
            int TIMEOUT = 1 * 60 * 1000;
            WebRequest request = WebRequest.Create(url);

            byte[] byteArray = Encoding.UTF8.GetBytes(data);
            request.ContentType = "application/x-www-form-urlencoded";
            request.Method = "POST";
            request.Timeout = TIMEOUT;
            request.ContentLength = byteArray.Length;
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            try
            {
                WebResponse response = request.GetResponse();
                dataStream = response.GetResponseStream();

                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();

                return responseFromServer;
            }
            catch (Exception ex)
            {
                new clsCommon().InsertErrorLog("ZiftPayBAL--USP_GetZiftPayDetails", ex.Message, ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Ankit here 06-20-2018: didn't changes on it because if we miss some thing on this then make issue for us.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="TransactionId"></param>
        /// <param name="strMessage"></param>
        /// <returns></returns>
        public static bool SaleTransaction(ZPTransactionRequest obj, out string TransactionId, out string strMessage)
        {
            try
            {
                strMessage = string.Empty;
                //We need to get the accountid later on from DoctorMerchantSettings_table as we didn't explore merchant side yet.
                string AccountId = obj.accountId;

                string postUrl = SafeValue<string>(ConfigurationManager.AppSettings["transactionURL"]);
                string Username, password, GatewayId;
                GetZiftSettings(out Username, out password, out GatewayId);

                #region post data to ZiftPay
                string straccount, stradminaccountid = string.Empty;
                Decimal strAmount = 0, stradminAmount = 0, FixPrice = 0, Percentage = 0, TotalAmount;

                // Get the data from RLAccountZiftPayAssociation
                DataTable dtZiftPay = new DataTable();
                DataTable dtZiftPayAdmin = new DataTable();
                ZiftPayDAL ObjZiftPayData = new ZiftPayDAL();
                ZPTransactionResponse objResponse = new ZPTransactionResponse();
                bool IsSaveTransaction;
                dtZiftPay = ObjZiftPayData.GetAccountZiftPayDetailsByZPAccountId(obj.DoctorId);
                dtZiftPayAdmin = ObjZiftPayData.GetAccountZiftPayDetailsByUserId(Common.AdminId);
                if (dtZiftPay != null)
                {
                    FixPrice = SafeValue<decimal>(dtZiftPay.Rows[0]["FixPrice"]);
                    Percentage = SafeValue<decimal>(dtZiftPay.Rows[0]["Percentage"]);
                }
                TotalAmount = SafeValue<decimal>(obj.amount);
                if (FixPrice == 0 && Percentage == 0)
                {
                    straccount = "&accountId=" + AccountId;
                    strAmount = TotalAmount;
                }
                else
                {
                    if (dtZiftPayAdmin != null)
                    {
                        stradminaccountid = SafeValue<string>(dtZiftPayAdmin.Rows[0]["ZPAccountId"]);
                    }
                    if (TotalAmount <= FixPrice)
                    {
                        straccount = "&accountId=" + stradminaccountid;
                        strAmount = TotalAmount;
                    }
                    else
                    {
                        if (FixPrice != 0)
                        {
                            strAmount = TotalAmount - FixPrice;
                            stradminAmount = FixPrice;
                        }
                        else if (Percentage != 0)
                        {
                            Decimal PercentAmt = (TotalAmount * Percentage) / 100;
                            stradminAmount = PercentAmt;
                            strAmount = TotalAmount - PercentAmt;
                        }
                        straccount = "&accountId=" + AccountId + "" + "&splits=(accountId=" + stradminaccountid + ";amount=" + SafeValue<int>(stradminAmount * 100) + ")";
                    }
                }
                if(!string.IsNullOrEmpty(obj.expdate))
                {
                    obj.accountAccessoryMonth = obj.expdate.Split('/')[0];
                    obj.accountAccessoryYear = obj.expdate.Split('/')[1];
                }
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                int? PPTId = null;
                string RequestParam = string.Empty;
                if (obj.isProfileSave == true)
                {
                    string RequestParamToken = "requestType=" + Common.requestTypeToken + ""
                   + "&userName=" + Username + ""
                   + "&password=" + password + ""
                   + "&accountId=" + AccountId + ""
                   + "&accountType=" + obj.accountType + ""
                   + "&accountNumber=" + obj.accountNumber + ""
                   + "&accountAccessory=" + obj.accountAccessoryMonth + obj.accountAccessoryYear + ""
                   + "&customerAccountCode=" + obj.customerAccountCode + ""
                   + "&transactionCode=" + obj.transactionCode + ""
                   + "&street=" + obj.street + ""
                   + "&city=" + obj.city + ""
                   + "&state=" + obj.state + ""
                   + "&zipCode=" + obj.zipCode + "";

                    string returnToken = SendPOST(postUrl, RequestParamToken);

                    var Tokendict = HttpUtility.ParseQueryString(returnToken.TrimEnd('&'));
                    string Tokenjson = new JavaScriptSerializer().Serialize(
                                        Tokendict.AllKeys.ToDictionary(k => k, k => Tokendict[k]));
                    var Tokenlst = JsonConvert.DeserializeObject<ZPTokenResponse>(Tokenjson);
                    if (Tokenlst.responseType.ToLower() == "exception")
                    {
                        IsSaveTransaction = false;
                        ErrorResponse objErrorResponse = new JavaScriptSerializer().Deserialize<ErrorResponse>(Tokenjson);
                        objResponse.responseType = objErrorResponse.responseType;
                        objResponse.responseMessage = objErrorResponse.failureMessage;
                        objResponse.responseCode = objErrorResponse.failureCode;
                        //objResponse.responseType = objErrorResponse.failedRequestType;
                        objResponse.accountId = AccountId;
                        objResponse.transactionDate = SafeValue<string>(DateTime.Now.ToString("yyyyMMdd"));
                        strMessage = objErrorResponse.failureMessage;
                    }
                    else
                    {
                        PatientPaymentToken objToken = new PatientPaymentToken();
                        objToken.PatientId = SafeValue<int>(obj.PatientId);
                        objToken.PaymentGatewayId = SafeValue<int>(GatewayId);
                        objToken.PatientId = SafeValue<int>(obj.PatientId);
                        objToken.Token = Tokenlst.token;

                        // Get client ip address
                        IPHostEntry Host = default(IPHostEntry);
                        string Hostname = null;
                        Hostname = System.Environment.MachineName;
                        Host = Dns.GetHostEntry(Hostname);
                        foreach (IPAddress IP in Host.AddressList)
                        {
                            if (IP.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                            {
                                objToken.SourceIP = SafeValue<string>(IP);
                            }
                        }
                        objToken.CreatedBy = SafeValue<int>(obj.PatientId);
                        objToken.IsActive = true;
                        objToken.TokenResponse = Tokenjson;
                        objToken.AccountAccessory = Tokenlst.accountAccessory;
                        string mask = obj.accountNumber;
                        objToken.AccountNumberMasked = mask.Substring(0, 1) + "***********" + mask.Substring(12);
                        PPTId = ZiftPayDAL.AddPatientPaymentToken(objToken);
                        if (PPTId != 0)
                        {
                            RequestParam = "requestType=" + obj.requestType + ""
                           + "&userName=" + Username + ""
                           + "&password=" + password + ""
                           + straccount + ""
                           + "&accountType=" + obj.accountType + ""
                           + "&accountAccessory=" + obj.accountAccessoryMonth + obj.accountAccessoryYear + ""
                           + "&token=" + Tokenlst.token + ""
                           + "&amount=" + SafeValue<int>(strAmount * 100) + ""
                           + "&transactionIndustryType=" + obj.transactionIndustryType + ""
                           + "&holderName=" + obj.holderName + ""
                           + "&street=" + obj.street + ""
                           + "&city=" + obj.city + ""
                           + "&state=" + obj.state + ""
                           + "&zipCode=" + obj.zipCode + "";
                        }
                    }
                }
                else
                {
                    if(obj.SelectedCard != 0 && string.IsNullOrEmpty(obj.accountNumber))
                    {
                        var carddetail = obj.lstCard.Where(t => t.Id == obj.SelectedCard).FirstOrDefault();
                        PPTId = obj.SelectedCard;
                        RequestParam = "requestType=" + obj.requestType + ""
                          + "&userName=" + Username + ""
                          + "&password=" + password + ""
                          + straccount + ""
                          + "&accountType=" + obj.accountType + ""
                          + "&accountAccessory=" + carddetail.accountAccessory + ""
                          + "&token=" + carddetail.Token + ""
                          + "&amount=" + SafeValue<int>(strAmount * 100) + ""
                          + "&transactionIndustryType=" + obj.transactionIndustryType + ""
                          + "&holderName=" + obj.holderName + ""
                          + "&street=" + obj.street + ""
                          + "&city=" + obj.city + ""
                          + "&state=" + obj.state + ""
                          + "&zipCode=" + obj.zipCode + "";
                    }
                    else
                    {
                        RequestParam = "requestType=" + obj.requestType + ""
                    + "&userName=" + Username + ""
                    + "&password=" + password + ""
                    + straccount + ""
                    + "&amount=" + SafeValue<int>(strAmount * 100) + ""
                    + "&accountType=" + obj.accountType + ""
                    + "&transactionIndustryType=" + obj.transactionIndustryType + ""
                    + "&holderType=" + obj.holderType + ""
                    + "&holderName=" + obj.holderName + ""
                    + "&accountNumber=" + obj.accountNumber + ""
                    + "&accountAccessory=" + obj.accountAccessoryMonth + obj.accountAccessoryYear + ""
                    + "&street=" + obj.street + ""
                    + "&city=" + obj.city + ""
                    + "&state=" + obj.state + ""
                    + "&zipCode=" + obj.zipCode + ""
                    + "&customerAccountCode=" + obj.customerAccountCode + ""
                    + "&transactionCode=" + obj.transactionCode + "";
                    }
                    
                }
                string returnData = SendPOST(postUrl, RequestParam);

                #endregion

                var dict = HttpUtility.ParseQueryString(returnData.TrimEnd('&'));
                string json = new JavaScriptSerializer().Serialize(
                                    dict.AllKeys.ToDictionary(k => k, k => dict[k]));
                var lst = JsonConvert.DeserializeObject<ZPTransactionResponse>(json);

                
               
                if (lst.responseType.ToLower() == "exception")
                {
                    IsSaveTransaction = false;
                    ErrorResponse objErrorResponse = new JavaScriptSerializer().Deserialize<ErrorResponse>(json);
                    objResponse.responseType = objErrorResponse.responseType;
                    objResponse.responseMessage = objErrorResponse.failureMessage;
                    objResponse.responseCode = objErrorResponse.failureCode;
                    //objResponse.responseType = objErrorResponse.failedRequestType;
                    objResponse.accountId = AccountId;
                    objResponse.transactionDate = SafeValue<string>(DateTime.Now.ToString("yyyyMMdd"));
                    strMessage = objErrorResponse.failureMessage;
                }
                else
                {
                    if (lst.responseCode.ToLower() == "a01")
                    {
                        IsSaveTransaction = true;
                        objResponse = lst;
                        objResponse.originalAmount = objResponse.originalAmount / 100;
                    }
                    else
                    {
                        IsSaveTransaction = false;
                        objResponse = lst;
                        objResponse.originalAmount = objResponse.originalAmount / 100;
                        strMessage = lst.responseMessage;
                    }
                }
                TransactionId = objResponse.transactionId;
                //Save Patient Payment card details and also track the number of payment request.
                PatientPaymentRequest objPatientPaymentRequest = new PatientPaymentRequest();
                if (lst.responseType.ToLower() == "exception")
                {
                    objPatientPaymentRequest.Status = false;
                    if (string.IsNullOrEmpty(strMessage))
                        strMessage = lst.responseMessage;
                }
                else
                {
                    if (lst.responseCode.ToLower() == "a01")
                    {
                        objPatientPaymentRequest.Status = true;
                    }
                    else
                    {
                        objPatientPaymentRequest.Status = false;
                        strMessage = lst.responseMessage;
                    }

                }
                objPatientPaymentRequest.Amount = SafeValue<decimal>(TotalAmount);
                objPatientPaymentRequest.GatewayId = SafeValue<int>(GatewayId);
                objPatientPaymentRequest.Patientid = SafeValue<int>(obj.PatientId);
                objPatientPaymentRequest.PaymentDate = DateTime.Now;
                objPatientPaymentRequest.PPTId = PPTId;
                int ZPAccountID = SafeValue<int>(obj.accountId);
                int MasterId = ZiftPayDAL.AddPatientCardDetails(objPatientPaymentRequest, ZPAccountID);
                if (MasterId > 0)
                {
                    objResponse.requestParam = RequestParam;
                    bool IsSubDetails = false;
                    IsSubDetails = ZiftPayDAL.AddTransaction(objResponse, obj.PatientId, IsSaveTransaction, MasterId, SafeValue<int>(GatewayId));
                    if (lst.splits != null)
                    {
                        PatientSplitPayment objPatientSplitPayment = new PatientSplitPayment();
                        objPatientSplitPayment.PPMId = MasterId;
                        objPatientSplitPayment.Split_AccountId = SafeValue<int>(lst.splits.Split(';')[0].Split('=')[1]);
                        objPatientSplitPayment.Split_TransactionId = lst.splits.Split(';')[1].Split('=')[1].Trim(')');
                        objPatientSplitPayment.Split_Amount = stradminAmount;
                        bool IsSplitDetail = ZiftPayDAL.AddPatientSplitPaymentDetail(objPatientSplitPayment);
                    }
                    if (IsSubDetails)
                    {
                        if (IsSaveTransaction)
                        {
                            new clsTemplate().SendZiftPayEmail(objResponse, SafeValue<int>(obj.PatientId), ZPAccountID, obj.DoctorId);
                        }
                    }
                }
                return IsSaveTransaction;
            }
            catch (Exception ex)
            {
                clsCommon objclsCommon = new clsCommon();
                objclsCommon.InsertErrorLog("ZiftPayBAL--USP_GetZiftPayDetails", ex.Message, ex.StackTrace);
                throw;
            }

        }

        /// <summary>
        /// This function get the zift pay default values form database.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="paymentgatewayid"></param>
        public static void GetZiftSettings(out string username, out string password, out string paymentgatewayid)
        {
            DataTable dt = new DataTable();
            clsCommon objclsCommon = new clsCommon();
            try
            {
                List<EmailProvider> lst = new List<EmailProvider>();
                dt = ZiftPayDAL.GetZiftDetails();
                if (dt != null && dt.Rows.Count > 0)
                {
                    username = SafeValue<string>(dt.Rows[0]["username"]);
                    password =SafeValue<string>(dt.Rows[0]["password"]);
                    paymentgatewayid = SafeValue<string>(dt.Rows[0]["Id"]);
                }
                else
                {
                    username = "";
                    password = "";
                    paymentgatewayid = "";
                }
            }
            catch (Exception ex)
            {
                objclsCommon.InsertErrorLog("ZiftPayBAL--USP_GetZiftPayDetails", ex.Message, ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This method will request the merchant creation by passing doctor/user id.
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public static string CreateMerchant(string userid)
        {
            string returnData = string.Empty;
            if (userid != null)
            {
                clsColleaguesData objColleaguesData = new clsColleaguesData();
                clsCommon objCommon = new clsCommon();
                ZPMerchantRequest objRequest = new ZPMerchantRequest();

                //Get Doctor Details to pass in the merchant creation request.
                DataSet ds = objColleaguesData.GetDoctorDetailsById(SafeValue<int>(userid));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string Desciption = SafeValue<string>(ds.Tables[0].Rows[0]["Description"]);
                    objRequest.applicationCode = SafeValue<string>(ds.Tables[0].Rows[0]["UserId"]);

                    //Owner Information
                    objRequest.Owner = new MerchantOwner();
                    objRequest.Owner.firstName = SafeValue<string>(ds.Tables[0].Rows[0]["FirstName"]);
                    objRequest.Owner.lastName = SafeValue<string>(ds.Tables[0].Rows[0]["LastName"]);
                    objRequest.Owner.email = SafeValue<string>(ds.Tables[0].Rows[0]["Username"]);
                    objRequest.Owner.birthDate = string.Empty;
                    objRequest.Owner.socialSecurity = string.Empty;
                    objRequest.Owner.countryCode = "US";
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        objRequest.Owner.city = SafeValue<string>(ds.Tables[2].Rows[0]["City"]);
                        objRequest.Owner.state = SafeValue<string>(ds.Tables[2].Rows[0]["State"]);
                        objRequest.Owner.phone = new string(SafeValue<string>(ds.Tables[2].Rows[0]["Phone"]).Where(char.IsDigit).ToArray());
                        objRequest.Owner.street1 = SafeValue<string>(ds.Tables[2].Rows[0]["ExactAddress"]);
                        objRequest.Owner.street2 = SafeValue<string>(ds.Tables[2].Rows[0]["Address2"]);
                        objRequest.Owner.zipCode = SafeValue<string>(ds.Tables[2].Rows[0]["ZipCode"]);
                    }

                    //Business Information
                    objRequest.Business = new MerchantBusiness();
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        objRequest.Business.businessName = SafeValue<string>(ds.Tables[1].Rows[0]["AccountName"]);
                    }

                    objRequest.Business.city = string.Empty;
                    objRequest.Business.countryCode = "US";
                    objRequest.Business.currencyCode = "@!USD";
                    objRequest.Business.descriptorPhone = string.Empty;
                    objRequest.Business.directDebitDescriptor = string.Empty;
                    objRequest.Business.email = string.Empty;
                    objRequest.Business.legalName = string.Empty;
                    objRequest.Business.merchantCategoryCode = "@!8021";
                    objRequest.Business.ownershipStructureType = "C";
                    objRequest.Business.paymentCardDescriptor = string.Empty;
                    objRequest.Business.state = string.Empty;
                    objRequest.Business.street1 = string.Empty;
                    objRequest.Business.street2 = string.Empty;
                    objRequest.Business.taxId = string.Empty;
                    objRequest.Business.webSite = SafeValue<string>(ds.Tables[0].Rows[0]["WebsiteURL"]);
                    objRequest.Business.zipCode = string.Empty;
                    objRequest.Business.description = string.Empty;
                    objRequest.Business.driverLicenseState = "@!";
                    objRequest.Business.driverLicenseCountryCode = "@!";
                    objRequest.Business.driverLicense = "@!";
                    objRequest.Business.relationshipBeginDate = "@!";
                    objRequest.Business.registrationCountryCode = "@!";
                    objRequest.Business.registrationState = "@!";
                    objRequest.Business.registrationYear = "@!";
                }

                //Account Information
                objRequest.Account = new MerchantAccount();
                objRequest.Account.accountNumber = string.Empty;
                objRequest.Account.accountType = string.Empty;
                objRequest.Account.annualCardsVolume = string.Empty;
                objRequest.Account.annualDirectDebitVolume = string.Empty;
                objRequest.Account.avgCardsTransactionAmount = "@!30000";
                objRequest.Account.avgDirectDebitTransactionAmount = "@!30000";
                objRequest.Account.bankName = string.Empty;
                objRequest.Account.holderName = string.Empty;
                objRequest.Account.routingNumber = string.Empty;
                objRequest.Account.maxTransactionAmount= "@!500000";

                //Regulatory Information
                objRequest.merchantType = Common.merchantType;
                objRequest.merchantProfile = Common.merchantProfile;
                objRequest.merchantCreationPolicy = Common.merchantCreationPolicy;
                objRequest.requestType = Common.requestType;
                objRequest.profileId = Common.profileId;
                objRequest.resellerId = Common.resellerId;
                objRequest.clientHost = Common.clientHost;
                objRequest.portfolioId = Common.portfolioId;
                objRequest.feeTemplateId = Common.feeTemplateId;
                objRequest.processingConfigurationScript = Common.processingConfigurationScript;
                objRequest.applicationCode = Common.applicationCode;
                objRequest.isEmbedded = Common.isEmbedded;
                objRequest.pageFormat = Common.pageFormat;
                objRequest.notifyURL = SafeValue<string>(ConfigurationManager.AppSettings["NotifyUrl"]);
                objRequest.cancelURL = SafeValue<string>(ConfigurationManager.AppSettings["CancelUrl"]);
                objRequest.returnURL = SafeValue<string>(ConfigurationManager.AppSettings["ReturnUrl"]);
                objRequest.returnURLPolicy = Common.returnURLPolicy;
                objRequest.notificationPolicy = Common.notificationPolicy;
                string postUrl = SafeValue<string>(ConfigurationManager.AppSettings["MerchantUrl"]);

                //Get ZiftPay Credentials
                string Username, password, gatewayId;
                GetZiftSettings(out Username, out password, out gatewayId);
                objRequest.userName = Username;
                objRequest.password = password;
                returnData = SendPOST(postUrl,
                 "requestType=create"
                + "&userName=" + objRequest.userName + ""
                + "&password=" + objRequest.password + ""
                + "&merchantType=" + objRequest.merchantType + ""
                + "&merchantProfile=" + objRequest.merchantProfile + ""
                + "&merchantCreationPolicy=" + objRequest.merchantCreationPolicy + ""
                + "&profileId=" + objRequest.profileId + ""
                + "&resellerId=" + objRequest.resellerId + ""
                + "&clientHost=" + objRequest.clientHost + ""
                + "&portfolioId=" + objRequest.portfolioId + ""
                + "&merchantId="
                + "&feeTemplateId=" + objRequest.feeTemplateId + ""
                + "&distributionSchemaCode="
                + "&processingConfigurationScript=" + objRequest.processingConfigurationScript + ""
                + "&applicationCode=" + objRequest.applicationCode + ""
                + "&isEmbedded=" + objRequest.isEmbedded + ""
                + "&pageFormat=" + objRequest.pageFormat + ""
                + "&notifyURL=" + objRequest.notifyURL + ""
                + "&cancelURL=" + objRequest.cancelURL + ""
                + "&returnURL=" + objRequest.returnURL + ""
                + "&returnURLPolicy=" + objRequest.returnURLPolicy + ""
                + "&notificationPolicy=" + objRequest.notificationPolicy + ""
                + "&owner.firstName=" + objRequest.Owner.firstName + ""
                + "&owner.lastName=" + objRequest.Owner.lastName + ""
                + "&owner.email=" + objRequest.Owner.email + ""
                + "&owner.street1=" + objRequest.Owner.street1 + ""
                + "&owner.street2=" + objRequest.Owner.street2 + ""
                + "&owner.city=" + objRequest.Owner.city + ""
                + "&owner.state=" + objRequest.Owner.state + ""
                + "&owner.zipCode=" + objRequest.Owner.zipCode + ""
                + "&owner.countryCode=" + objRequest.Owner.countryCode + ""
                + "&owner.birthDate=" + objRequest.Owner.birthDate + ""
                + "&owner.socialSecurity=" + objRequest.Owner.socialSecurity + ""
                + "&owner.phone=" + objRequest.Owner.phone + ""
                + "&business.businessName=" + objRequest.Business.businessName + ""
                + "&business.legalName=" + objRequest.Business.legalName + ""
                + "&business.ownershipStructureType=" + objRequest.Business.ownershipStructureType + ""
                + "&business.street1=" + objRequest.Business.street1 + ""
                + "&business.street2=" + objRequest.Business.street2 + ""
                + "&business.city=" + objRequest.Business.city + ""
                + "&business.state=" + objRequest.Business.state + ""
                + "&business.zipCode=" + objRequest.Business.zipCode + ""
                + "&business.countryCode=" + objRequest.Business.countryCode + ""
                + "&business.descriptorPhone=" + objRequest.Business.descriptorPhone + ""
                + "&business.paymentCardDescriptor=" + objRequest.Business.paymentCardDescriptor + ""
                + "&business.directDebitDescriptor=" + objRequest.Business.directDebitDescriptor + ""
                + "&business.taxId=" + objRequest.Business.taxId + ""
                + "&business.webSite=" + objRequest.Business.webSite + ""
                + "&business.email=" + objRequest.Business.email + ""
                + "&business.description=" + objRequest.Business.description + ""
                + "&business.merchantCategoryCode=" + objRequest.Business.merchantCategoryCode + ""
                + "&business.currencyCode=" + objRequest.Business.currencyCode + ""
                + "&business.contactPhone="
                + "&business.driverLicense=" + objRequest.Business.driverLicense + ""
                + "&business.driverLicenseCountryCode=" + objRequest.Business.driverLicenseCountryCode + ""
                + "&business.driverLicenseState=" + objRequest.Business.driverLicenseState + ""
                + "&business.registrationCountryCode=" + objRequest.Business.registrationCountryCode + ""
                + "&business.registrationState=" + objRequest.Business.registrationState + ""
                + "&business.registrationYear=" + objRequest.Business.registrationYear + ""
                + "&business.workHours="
                + "&business.timeZoneCode="
                + "&business.relationshipBeginDate=" + objRequest.Business.relationshipBeginDate + ""
                + "&estimates.annualDirectDebitVolume=" + objRequest.Account.annualDirectDebitVolume + ""
                + "&estimates.annualCardsVolume=" + objRequest.Account.annualCardsVolume + ""
                + "&estimates.avgDirectDebitTransactionAmount=" + objRequest.Account.avgDirectDebitTransactionAmount + ""
                + "&estimates.avgCardsTransactionAmount=" + objRequest.Account.avgCardsTransactionAmount + ""
                + "&estimates.maxTransactionAmount=" + objRequest.Account.maxTransactionAmount + ""
                + "&deposit.bankName=" + objRequest.Account.bankName + ""
                + "&deposit.holderName=" + objRequest.Account.holderName + ""
                + "&deposit.routingNumber=" + objRequest.Account.routingNumber + ""
                + "&deposit.accountNumber=" + objRequest.Account.accountNumber + ""
                + "&deposit.accountType=" + objRequest.Account.accountType + ""
                    );
            }
            else
            {
                returnData = "We are not able to process your process at this time. Please try again!";
            }
            return returnData;
        }

        /// <summary>
        /// This method added merchant on recordlinc database.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public static bool AddMerchant(MerchantResponse obj, string userid)
        {
            if (obj != null)
            {
                string Username, password, gatewayId;
                GetZiftSettings(out Username, out password, out gatewayId);
                var isInserted = ZiftPayDAL.AddDoctorMerchant(obj, SafeValue<int>(userid), SafeValue<int>(gatewayId));
                if (isInserted > 0)
                {
                    clsTemplate objtemplate = new clsTemplate();
                    objtemplate.SendMerchantEmail(obj, userid);
                }
            }
            return true;
        }

        /// <summary>
        /// This method return list of Merchant for Dropdownlist.
        /// </summary>
        /// <returns></returns>
        public static List<SelectListItem> GetDoctorList(int PatientId)
        {
            List<SelectListItem> lst = new List<SelectListItem>();
            DataTable dt = new DataTable();
            try
            {
                clsHelper ObjHelper = new clsHelper();
                dt = ZiftPayDAL.GetMerchantListOfTreatingDoctor(PatientId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        SelectListItem selectitm = new SelectListItem();
                        selectitm.Value = SafeValue<string>(item["ZPAccountId"]);
                        selectitm.Text = item["FirstName"] + " " + item["LastName"];
                        lst.Add(selectitm);
                    }
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ZPPaymentBAL---GetDoctorList", Ex.Message, Ex.StackTrace);
                throw;
            }
            return lst;
        }

        /// <summary>
        /// This method get Zift pay account id of the doctor.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static int GetAccountZiftPayDetailsByUserId(int UserId)
        {
            try
            {
                int ZiftPayAccountId = 0;
                DataTable dtZiftPay = new DataTable();
                dtZiftPay = new ZiftPayDAL().GetAccountZiftPayDetailsByUserId(UserId);
                if (dtZiftPay != null && dtZiftPay.Rows.Count > 0)
                {
                    ZiftPayAccountId = SafeValue<int>(dtZiftPay.Rows[0]["ZPAccountId"]);
                }
                return ZiftPayAccountId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ZPPaymentBAL---GetAccountZiftPayDetailsByUserId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This method get the merchant details form database.
        /// </summary>
        /// <param name="AccountId"></param>
        /// <param name="AccountName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        public static List<MerchantOwner> GetMerchantDetailList(int AccountId, string AccountName, DateTime? FromDate, DateTime? ToDate)
        {
            List<MerchantOwner> lst = new List<MerchantOwner>();
            DataTable dt = new DataTable();
            try
            {
                dt = ZiftPayDAL.GetMerchantDetails(AccountId, AccountName, FromDate, ToDate);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        try
                        {
                            MerchantOwner MerchantOwneritm = new MerchantOwner();
                            if (SafeValue<string>(item["AccountName"]) != "")
                                MerchantOwneritm.AccountName = SafeValue<string>(item["AccountName"]);
                            else
                                MerchantOwneritm.AccountName = SafeValue<string>(item["FirstName"]) + ' ' + SafeValue<string>(item["LastName"]);
                            MerchantOwneritm.lastName = SafeValue<string>(item["LastName"]);
                            MerchantOwneritm.ZPAccountId = SafeValue<int>(item["ZPAccountId"]);
                            //MerchantOwneritm.firstName = SafeValue<string>(item["FirstName"]);
                            MerchantOwneritm.AccountId = SafeValue<int>(item["AccountId"]);
                            MerchantOwneritm.ActualAmount = SafeValue<decimal>(item["ActualAmount"]);
                            MerchantOwneritm.ReceivedAmount = SafeValue<decimal>(item["ReceivedAmount"]);
                            MerchantOwneritm.split_Amount = SafeValue<decimal>(item["split_Amount"]);
                            MerchantOwneritm.DoctorId = SafeValue<int>(item["DoctorId"]);
                            lst.Add(MerchantOwneritm);
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                clsCommon objclsCommon = new clsCommon();
                objclsCommon.InsertErrorLog("ZiftPayBAL--GetMerchantDetailList", ex.Message, ex.StackTrace);
                throw;
            }
            return lst;
        }

        /// <summary>
        /// This method get doctor payment details form database.
        /// </summary>
        /// <param name="FilterObj"></param>
        /// <returns></returns>
        public static List<MerchantOwner> GetDoctorPaymentDetailList(ZiftPayFilter FilterObj)
        {
            List<MerchantOwner> lst = new List<MerchantOwner>();
            try
            {
                DataTable dt = ZiftPayDAL.GetDentistPaymentListOfZiftPay(FilterObj);
                lst = MappedDataToModelObject(dt);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ZPPaymentBAL--GetDoctorPaymentDetailList", Ex.Message, Ex.StackTrace);
                throw;

            }
            return lst;
        }

        /// <summary>
        /// This function used to mapped data with data table.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static List<MerchantOwner> MappedDataToModelObject(DataTable dt)
        {
            List<MerchantOwner> lst = new List<MerchantOwner>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    try
                    {
                        MerchantOwner MerchantOwneritm = new MerchantOwner();
                        MerchantOwneritm.AccountName = SafeValue<string>(item["DoctorFullName"]);
                        MerchantOwneritm.ZPAccountId = SafeValue<int>(item["ZPAccountId"]);
                        MerchantOwneritm.PatientName = SafeValue<string>(item["PatientFullName"]);
                        MerchantOwneritm.PaymentDate = SafeValue<string>(item["PaymentDate"]);
                        MerchantOwneritm.Amount = SafeValue<decimal>(item["ActualAmount"]);
                        MerchantOwneritm.Status = SafeValue<string>(item["Status"]);
                        MerchantOwneritm.split_Amount = SafeValue<decimal>(item["split_Amount"]);
                        MerchantOwneritm.birthDate = SafeValue<string>(item["DateOfBirth"]);
                        MerchantOwneritm.email = SafeValue<string>(item["Email"]);
                        MerchantOwneritm.ReceivedAmount = SafeValue<decimal>(item["ReceivedAmount"]);
                        MerchantOwneritm.ActualAmount = SafeValue<decimal>(item["ActualAmount"]);
                        MerchantOwneritm.phone = SafeValue<string>(item["WorkPhone"]);
                        MerchantOwneritm.DentrixPatId = (SafeValue<string>(item["AssignedPatientId"])) != "0" ? SafeValue<string>(item["AssignedPatientId"]) : string.Empty;
                        MerchantOwneritm.PatientId = SafeValue<int>(item["PatientId"]);
                        MerchantOwneritm.PaymentDate = SafeValue<string>(item["PaymentDate"]);
                        MerchantOwneritm.PrimaryPhone = SafeValue<string>(item["PrimaryPhone"]);
                        lst.Add(MerchantOwneritm);
                    }
                    catch (Exception Ex)
                    {

                    }
                }
            }
            return lst;
        }

        /// <summary>
        /// Used for check user is registered with Zift pay or not.
        /// </summary>
        /// <param name="DoctorId"></param>
        /// <returns></returns>
        public static int CheckDoctoreExistsOrNot(int DoctorId)
        {
            try
            {
                return ZiftPayDAL.CheckDoctorRegisteredWithZiftpay(DoctorId);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ZPPaymentBAL--CheckDoctoreExistsOrNot", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This function used for getting patient payment details.
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public static List<MerchantOwner> PartialMerchantDetail(Int32 PatientId)
        {
            List<MerchantOwner> lst = new List<MerchantOwner>();
            try
            {
                DataTable dt = ZiftPayDAL.GetPatientPaymentDetails(PatientId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        MerchantOwner MerchantOwneritm = new MerchantOwner();
                        MerchantOwneritm.AccountName = SafeValue<string>(item["DoctorName"]);
                        MerchantOwneritm.ZPAccountId = SafeValue<int>(item["ZPAccountId"]);
                        MerchantOwneritm.ActualAmount = SafeValue<decimal>(item["ActualAmount"]);
                        MerchantOwneritm.ReceivedAmount = SafeValue<decimal>(item["ReceivedAmount"]);
                        MerchantOwneritm.PaymentDate = SafeValue<string>(item["PaymentDate"]);
                        lst.Add(MerchantOwneritm);
                    }
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ZPPaymentBAL--PartialMerchantDetail", Ex.Message, Ex.StackTrace);
                throw;
            }
            return lst;
        }

        /// <summary>
        /// This method used for getting accounts name which is registered on zift pay.
        /// </summary>
        /// <param name="AccountName"></param>
        /// <returns></returns>
        public static DataTable SearchAccountName(string AccountName)
        {
            clsHelper ObjHelper = new clsHelper();
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] sp = new SqlParameter[1];
                sp[0] = new SqlParameter("@AccountName", SqlDbType.VarChar);
                sp[0].Value = AccountName;
                dt = ObjHelper.DataTable("Usp_SearchAccountName", sp);

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ZPPaymentBAL--SearchAccountName", Ex.Message, Ex.StackTrace);
                throw;
            }
            return dt;
        }

        /// <summary>
        /// This method add Patient payment profile token on database.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static int AddPatientPaymentToken(PatientPaymentToken obj)
        {
            try
            {
                int id = ZiftPayDAL.AddPatientPaymentToken(obj);
                return id;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ZPPaymentBAL--AddPatientPaymentToken", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This method get patient profile token form database.
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public static List<CheckList> GetPatientPaymentTokenByPatientId(int PatientId)
        {
            try
            {
                List<CheckList> model = new List<CheckList>();
                DataTable dtZiftPay = new DataTable();
                dtZiftPay = new ZiftPayDAL().GetPatientPaymentTokenByPatientId(PatientId);
                if (dtZiftPay != null && dtZiftPay.Rows.Count > 0)
                {
                    for (int i = 0; i < dtZiftPay.Rows.Count; i++)
                    {
                        model.Add(new CheckList
                        {
                            IsCheck = false,
                            AccountNumberMasked = SafeValue<string>(dtZiftPay.Rows[i]["AccountNumberMasked"]),
                            Id = SafeValue<int>(dtZiftPay.Rows[i]["Id"]),
                            accountAccessory = SafeValue<string>(dtZiftPay.Rows[i]["AccountAccessory"]),
                            Token = SafeValue<string>(dtZiftPay.Rows[i]["Token"])
                        });
                        if (i == 0)
                        {
                            model.First().IsCheck = true;
                        }
                    }
                }
                return model;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ZPPaymentBAL--GetPatientPaymentTokenByPatientId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Remove patient payment card detail
        /// </summary>
        /// <param name="PPTId"></param>
        /// <returns></returns>
        public static bool RemovePatientPaymentToken(int PPTId)
        {
            try
            {
                return ZiftPayDAL.RemovePatientPaymentToken(PPTId);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ZPPaymentBAL--RemovePatientPaymentToken", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This Function mapped object of 2 different objects values.
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public static ZPTransactionRequest MappObjectOfZpTransactionReqeust(int PatientId, ZPTransactionRequest obj)
        {
            try
            {
                var objPatient = PatientBLL.GetPatientDetailsById(PatientId,null);
                obj.accountId = SafeValue<string>(obj.DoctorId);
                obj.city = objPatient.City;
                obj.customerAccountCode = Common.customerAccountCode;
                obj.transactionCode = Common.transactionCode;
                obj.state = objPatient.State;
                obj.zipCode = objPatient.ZipCode;
                obj.street = objPatient.ExactAddress;
                obj.requestType = Common.requestTypePayment;
                obj.holderType = Common.holderType;
                obj.transactionIndustryType = Common.transactionIndustryType;
                obj.accountType = Common.accountType;
                obj.PatientId = SafeValue<string>(PatientId);
                obj.amount = SafeValue<string>((SafeValue<int>(obj.amount)));
               
                return obj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ZPPaymentBAL--GetPatientPaymentTokenByPatientId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool GetZiftPayDoctorForPatient(int PatientId,int IsForZiftPay=1)
        {
            bool IsZift;
            DataTable dt = new DataTable();
            dt= ZiftPayDAL.GetMerchantListOfTreatingDoctor(PatientId);
            IsZift =dt.Rows.Count > 0 ? true : false;
            return IsZift;
        }
    }
}
