﻿using BO.Models;
using DataAccessLayer;
using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public class SpecialOffersBLL
    {
        SpecialOffersDAL objOfferDAL = new SpecialOffersDAL();
        clsCommon ObjCommon = new clsCommon();

        public bool InsertUpdateSpecialOffers(SpecialOffers specialoffers)
        {
            return objOfferDAL.InsertUpdateSpecialOffers(specialoffers);
        }

        public List<SpecialOffers> GetSpecialOfferByUserId(int UserId)
        {
            DataTable dt = new DataTable();
            List<SpecialOffers> lstOffer = new List<SpecialOffers>();
            try
            {
                dt = objOfferDAL.GetSpecialOfferByUserId(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        lstOffer.Add(new SpecialOffers
                        {
                            OfferId = SafeValue<int>(dt.Rows[i]["OfferId"]),
                            Title = SafeValue<string>(dt.Rows[i]["Title"]),
                            Description = SafeValue<string>(dt.Rows[i]["Description"]),
                            Code = SafeValue<string>(dt.Rows[i]["Code"]),
                            SortOrder = SafeValue<int>(dt.Rows[i]["SortOrder"]),
                            Status = SafeValue<bool>(dt.Rows[i]["Status"]),
                            OfferStatus = (SafeValue<string>(dt.Rows[i]["Status"]) == "True" ? "Active" : "Inactive")
                        });
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstOffer;
        }

        public SpecialOffers GetSpecialOffersById(int OfferId)
        {
            DataTable dt = new DataTable();
            SpecialOffers specialoffers = new SpecialOffers();
            try
            {
                dt = objOfferDAL.GetSpecialOfferById(OfferId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    specialoffers.OfferId = SafeValue<int>(dt.Rows[0]["OfferId"]);
                    specialoffers.Title = SafeValue<string>(dt.Rows[0]["Title"]);
                    specialoffers.Description = SafeValue<string>(dt.Rows[0]["Description"]);
                    specialoffers.Code = SafeValue<string>(dt.Rows[0]["Code"]);
                    specialoffers.SortOrder = SafeValue<int>(dt.Rows[0]["SortOrder"]);
                    specialoffers.Status = SafeValue<bool>(dt.Rows[0]["Status"]);
                    specialoffers.OfferStatus = SafeValue<string>((SafeValue<string>(dt.Rows[0]["Status"]) == "1") ? "Active" : "Inactive");
                }
            }
            catch (Exception)
            {
                throw;
            }
            return specialoffers;
        }

        public bool RemoveSpecialOffer(int OfferId)
        {
            return objOfferDAL.RemoveSpecialOffer(OfferId);
        }

        public int CheckSpecialOfferById(SpecialOffers specialoffers)
        {
            return objOfferDAL.CheckSpecialOfferById(specialoffers);
        }
    }
}
