﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Models;
using DataAccessLayer.Common;
using DataAccessLayer.ProfileData;
using DataAccessLayer;
using System.Data;
using static DataAccessLayer.Common.clsCommon;

namespace BusinessLogicLayer
{
    public class LandingBLL
    {
        clsLandingDAL objclsLandingDAL = new clsLandingDAL();
        clsCommon objclsCommon = new clsCommon();
        #region Landing_History
        public bool InsertLandingHistory(LandingHistory landinghistory)
        {
            return objclsLandingDAL.InsertLandingHistory(landinghistory);
        }
        #endregion

        #region Get all the landing history details
        public List<LandingHistory> GetLandingHistoryDetails(int PageIndex, int PageSize,int SortColumn,int SortDirection,string SearchText)
        {
            DataTable dt = new DataTable();
            List<LandingHistory> lstLandingHistory = new List<LandingHistory>();
            try
            {
                dt = objclsLandingDAL.GetLandingHistoryDetails(PageIndex,PageSize,SortColumn,SortDirection,SearchText);
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        lstLandingHistory.Add(new LandingHistory
                        {
                            LanHistoryId = SafeValue<int>(dt.Rows[i]["Id"]),
                            LandingPageName = SafeValue<string>(dt.Rows[i]["LandingPageName"]),
                            Email =SafeValue<string>(dt.Rows[i]["Email"]),
                            CreatedDate = SafeValue<DateTime>(dt.Rows[i]["CreatedDate"])                            
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return lstLandingHistory;
        }
        #endregion
    }
}
