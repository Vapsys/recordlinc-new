﻿using System;
using System.Collections.Generic;
using System.Linq;
using BO.Models;
using DataAccessLayer.PatientsData;
using DataAccessLayer.Common;
using System.Data;
using BO.ViewModel;
using System.Configuration;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.SmileBrand;
using static BO.Enums.Common;
using System.Web;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.IO;
using System.Security.Principal;
using System.Security.AccessControl;
using System.Drawing;
using static DataAccessLayer.Common.clsCommon;
using System.Web.Helpers;
using DataAccessLayer.Appointment;
using ICSharpCode.SharpZipLib.Zip;
using System.Web.Script.Serialization;
using DataAccessLayer;

namespace BusinessLogicLayer
{
    public class VeridentBLL
    {
        #region Global Veriable
        static clsCommon ObjCommon = new clsCommon();
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        clsAdmin ObjAdmin = new clsAdmin();
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        CommonBLL objcommonBll = new CommonBLL();
        clsTemplate ObjTemplate = new clsTemplate();
        VeridentDAL objVeridentDAL = new VeridentDAL();
        #endregion

        //public void Index(string UserName, string Password)
        //{
        //    if (!string.IsNullOrEmpty(UserName))
        //    {
        //        if (!string.IsNullOrEmpty(Password))
        //        {
        //            Password = Password.Replace(" ", "+");
        //            DataTable dtLogin = new DataTable();
        //            DataTable dtFirstLogin = new DataTable();
        //            DataTable dtCheckFirst = new DataTable();
        //            int valid = 0;
        //            DataTable dtCheck = ObjColleaguesData.CheckEmailInSystem(UserName);
        //            if (dtCheck != null && dtCheck.Rows.Count > 0)
        //            {
        //                int IsInSystem = Convert.ToInt32(dtCheck.Rows[0]["IsInSystem"]);
        //                if (IsInSystem == 1)
        //                {

        //                }else
        //                {
        //                    dtFirstLogin = ObjAdmin.GetTempSignupByEmail(UserName);

        //                }


        //            }


        //        }
        //    }

        //}

        public PatientList GetPatients(PatientFilterForVeri FilterObj)
        {
            DataTable dt = new DataTable();
            clsCommon objCommon = new clsCommon();
            List<PatientDetailsforVer> lstPatient = new List<PatientDetailsforVer>();
            clsPatientsData objPatientsData = new clsPatientsData();
            PatientList objPatientList = new PatientList();
            int TotalRecord = 0;
            string TimeZoneName = new clsAppointmentData().GetTimeZoneOfDoctor(FilterObj.UserId);
            try
            {
                
                dt = objVeridentDAL.GetPatientsWithApp(FilterObj);               
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow p in dt.Rows)
                    {
                        string strDOB = string.Empty;
                        string strLastVerified = string.Empty;
                        DateTime DOB = SafeValue<DateTime>(p["DateOfBirth"]);
                        string CreationDate = string.Empty;
                        string strAppointmentDate = string.Empty;
                        if (!string.IsNullOrEmpty(SafeValue<string>(p["AppointmentDate"])))
                        {
                            strAppointmentDate = SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(p["AppointmentDate"]), TimeZoneName));
                        }
                        if (!string.IsNullOrEmpty(SafeValue<string>(p["LastVerified"])))
                        {
                            strLastVerified = SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(p["LastVerified"]), TimeZoneName));
                        }
                        if (DOB != DateTime.MinValue)
                        {
                            strDOB = new CommonBLL().ConvertToDate(SafeValue<DateTime>(DOB), SafeValue<int>(DateFormat.GENERAL));
                        }

                        TotalRecord = SafeValue<int>(p["TotalRecord"]);
                        lstPatient.Add(new PatientDetailsforVer()
                        {
                            PatientId = SafeValue<int>(p["PatientId"]),
                            FirstName = SafeValue<string>(p["FirstName"]),
                            LastName = SafeValue<string>(p["LastName"]),
                            Email = SafeValue<string>(p["Email"]),
                            Phone = SafeValue<string>(p["Phone"]),
                            DateOfBirth = strDOB,
                            AppointmentDate = strAppointmentDate,
                            DentalId = SafeValue<int>(p["DoctorId"]),
                            MemberId = SafeValue<string>(p["MemberId"]),
                            InsuranceCompanyName = SafeValue<string>(p["InsuranceCompanyName"]),
                            HasInsurance = (SafeValue<int>(p["HasInsurance"]) > 0 ? true : false),
                            IsStar = SafeValue<bool>(p["IsStar"]),
                            AttachedLabel = SafeValue<string>(p["AttachedLabel"]),
                            LastVerified = strLastVerified,
                            PayerId = SafeValue<string>(p["PayerId"]),
                            GroupNumber = SafeValue<string>(p["GroupNumber"]),
                        });
                    }
                }
                foreach (var items in lstPatient)
                {
                    try
                    {
                        string x = items.Phone.Trim();
                        double y = 0.0d;
                        if (x == "" || x == null)
                        {
                            items.Phone = "";
                        }
                        else if (!x.Contains('(') || !x.Contains(')') || !x.Contains('-'))
                        {
                            if (x.Contains("-"))
                            {
                                string z = x.Replace("-", "").Replace(" ", "");
                                y = SafeValue<double>(z);
                            }
                            else if (x.Contains(" "))
                            {
                                string z = x.Replace(" ", "");
                                y = SafeValue<double>(z);
                            }
                            else
                            {
                                y = SafeValue<double>(x);
                            }
                            items.Phone = string.Format("{0:(###)-###-####}", y);
                        }
                    }
                    catch (Exception Ex)
                    {
                        new clsCommon().InsertErrorLog("VeridentBLL-MaskPhone", Ex.Message, Ex.StackTrace);
                        items.Phone = string.Empty;
                    }
                }
                objPatientList.lstPatient = lstPatient;
                objPatientList.TotalRecord = TotalRecord;
            }
            catch (Exception EX)
            {
                new clsCommon().InsertErrorLog("GetPatientsWithApp", EX.Message, EX.StackTrace);
                throw;
            }
            return objPatientList;
        }

        public  PatientDetailsForVeri GetPatientDetails(int PatientId)
        {
            DataTable dt = new DataTable();            
            PatientDetailsForVeri objPatientDetailsForVeri = new PatientDetailsForVeri();
            try
            {
                dt = objVeridentDAL.GetPatientDetails(PatientId);
                if (dt != null && dt.Rows.Count > 0)
                {

                    string strDOB = string.Empty;
                    DateTime DOB = SafeValue<DateTime>(dt.Rows[0]["DateOfBirth"]);

                    if (DOB != DateTime.MinValue)
                    {
                        strDOB = new CommonBLL().ConvertToDate(SafeValue<DateTime>(DOB), SafeValue<int>(DateFormat.GENERAL));
                    }
                    objPatientDetailsForVeri.PatientId = SafeValue<int>(dt.Rows[0]["PatientId"]);
                    objPatientDetailsForVeri.FirstName = SafeValue<string>(dt.Rows[0]["FirstName"]);
                    objPatientDetailsForVeri.LastName = SafeValue<string>(dt.Rows[0]["LastName"]);
                    objPatientDetailsForVeri.Email = SafeValue<string>(dt.Rows[0]["Email"]);
                    objPatientDetailsForVeri.Phone = SafeValue<string>(dt.Rows[0]["Phone"]);
                    objPatientDetailsForVeri.DateOfBirth = strDOB;
                    objPatientDetailsForVeri.InsuranceCompanyName = SafeValue<string>(dt.Rows[0]["InsuranceCompany"]);
                    objPatientDetailsForVeri.InsuranceCompanyPhone = SafeValue<string>(dt.Rows[0]["InsurancePhone"]);
                    objPatientDetailsForVeri.NameofInsured = SafeValue<string>(dt.Rows[0]["InsuredName"]);
                    objPatientDetailsForVeri.MemberId = SafeValue<string>(dt.Rows[0]["MemberID"]);
                    objPatientDetailsForVeri.GroupNumber = SafeValue<string>(dt.Rows[0]["GroupNumber"]);
                }

                try
                {
                    string x = objPatientDetailsForVeri.Phone.Trim();
                    double y = 0.0d;
                    if (x == "" || x == null)
                    {
                        objPatientDetailsForVeri.Phone = "";
                    }
                    else if (!x.Contains('(') || !x.Contains(')') || !x.Contains('-'))
                    {
                        if (x.Contains("-"))
                        {
                            string z = x.Replace("-", "").Replace(" ", "");
                            y = SafeValue<double>(z);
                        }
                        else if (x.Contains(" "))
                        {
                            string z = x.Replace(" ", "");
                            y = SafeValue<double>(z);
                        }
                        else
                        {
                            y = SafeValue<double>(x);
                        }
                        objPatientDetailsForVeri.Phone = string.Format("{0:(###)-###-####}", y);
                    }
                }
                catch (Exception Ex)
                {
                    new clsCommon().InsertErrorLog("PatientBLL-MaskPhone", Ex.Message, Ex.StackTrace);
                    objPatientDetailsForVeri.Phone = string.Empty;
                }
            }
            catch (Exception EX)
            {
                new clsCommon().InsertErrorLog("GetPatientDetails", EX.Message, EX.StackTrace);
                throw;
            }
            return objPatientDetailsForVeri;
        }

        /// <summary>
        /// Check Email Exists
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        public int CheckEmailExists(string Email)
        {
            int ReStatus =-1;
            try
            {
                DataTable dt = new DataTable();
                dt = objVeridentDAL.CheckEmailExists(Email);
                if (dt != null && dt.Rows.Count > 0)
                {
                  ReStatus = SafeValue<int>(dt.Rows[0]["IsInSystem"]);                    
                }
            }
            catch (Exception EX)
            {
                new clsCommon().InsertErrorLog("CheckEmailExists", EX.Message, EX.StackTrace);
                throw;                
            }
            return ReStatus;
        }

        public int AddEditLable(Labels objLabel,int UserId,int AccountId)
        {
            
            int LabelId = 0;
            try
            {
                LabelId = objVeridentDAL.AddEditLabel(objLabel, UserId, AccountId);                
            }
            catch (Exception EX)
            {
                new clsCommon().InsertErrorLog("AddEditLable", EX.Message, EX.StackTrace);
                throw;
            }
            return LabelId;
        }


        public int DeleteLabel(int LabelId, int UserId,int AccountId)
        {
            int LabelStatus = 0;
            try
            {
                LabelStatus = objVeridentDAL.DeleteLabel(LabelId, UserId, AccountId);
            }
            catch (Exception EX)
            {
                new clsCommon().InsertErrorLog("DeleteLabel", EX.Message, EX.StackTrace);
                throw;
            }
            return LabelStatus;
        }
        
        /// <summary>
        /// Get Labels List 
        /// </summary>
        /// <param name="LabelId"></param>
        /// <param name="AccountId"></param>
        /// <returns></returns>
        public List<Labels> GetLabels(int LabelId,  int AccountId)
        {
            DataTable dt = new DataTable();
            List<Labels> lstLabels = new List<Labels>();
            try
            {              
                dt = objVeridentDAL.GetLabels(LabelId, AccountId);
                if (dt != null && dt.Rows != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow p in dt.Rows)
                    {
                        lstLabels.Add(new Labels()
                        {
                            Id = SafeValue<int>(p["Id"]),
                            Name = SafeValue<string>(p["Name"])
                        });
                    }
                }
            }
            catch (Exception EX)
            {
                new clsCommon().InsertErrorLog("GetLabels", EX.Message, EX.StackTrace);
                throw;
            }
            return lstLabels;
        }
        public static bool AddPatientLabel(PatientLabelAssociation labelAssociation,int UserId,int AccountId)
        {
            try
            {
                return VeridentDAL.AddPatientLabel(labelAssociation, UserId, AccountId);
            }
            catch (Exception EX)
            {
                new clsCommon().InsertErrorLog("VeridentBLL--AddPatientLabel", EX.Message, EX.StackTrace);
                throw;
            }
        }
        public static bool RemovePatientLabel(PatientLabelAssociation labelAssociation, int UserId, int AccountId)
        {
            try
            {
                return VeridentDAL.RemovePatientLabel(labelAssociation, UserId, AccountId);
            }
            catch (Exception EX)
            {
                new clsCommon().InsertErrorLog("VeridentBLL--RemovePatientLabel", EX.Message, EX.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Verify / Re-verify Patient insurance details
        /// </summary>
        /// <param name="verify"></param>
        /// <returns></returns>
        public static List<One_ResponseInsu> VerifyReverifyPatientInsruance(List<VerifyInsu> VerifyInsu, int UserId,int AccountId)
        {
            try
            {
                List<One_ResponseInsu> lstOne_ResponseInsu = new List<One_ResponseInsu>();
                string Token = OnederfulBLL.GetAccessToken();

                for (int i = 0; i < VerifyInsu.Count; i++)
                {
                    One_ResponseInsu One_ResponseInsu = new One_ResponseInsu();
                    One_ResponseInsu.PatientId = VerifyInsu[i].PatientId;
                    One_ResponseInsu .One_Response= OnederfulBLL.CallOnederfulAPI<One_Response, Verify>(VerifyInsu[i].PatientId, ConfigurationManager.AppSettings.Get("Onederful_Eligibility_URL"), Token, "POST", VerifyInsu[i].Verify, UserId, AccountId).Result;
                    lstOne_ResponseInsu.Add(One_ResponseInsu);
                }
                return lstOne_ResponseInsu;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentBLL--VerifyReverifyPatientInsruance", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public VeridentUser VeridentUserLogin(VeridentLogin objVeridentLogin)
        {
            VeridentUser objVeridentUser = new VeridentUser();
            try
            {
                DataTable dtValidMessage = new DataTable();
                DataTable dtUserDetails = new DataTable();
                MessageDetais objMessageDetais = new MessageDetais();
                VeridentUserDetails objVeridentUserDetails = new VeridentUserDetails();
                
                TripleDESCryptoHelper cryptoHelper = new TripleDESCryptoHelper();                
                objVeridentLogin.Password = cryptoHelper.encryptText(objVeridentLogin.Password);
                DataSet ds = objVeridentDAL.GetUserLoginDetails(objVeridentLogin);
                string RecordlincUrlForImage = ConfigurationManager.AppSettings["DoctorImage"];
                string companywebsite = ConfigurationManager.AppSettings["VeridentWebLink"];

                if (ds !=null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0] !=null && ds.Tables[0].Rows.Count > 0)
                    {
                        dtValidMessage = ds.Tables[0];                        
                        objMessageDetais.Id = SafeValue<int>(dtValidMessage.Rows[0]["ID"]);
                        objMessageDetais.Message = SafeValue<string>(dtValidMessage.Rows[0]["Message"]);
                        
                        if (objMessageDetais.Id == 1)
                        {
                            if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                            {
                                    dtUserDetails = ds.Tables[1];                                
                                    objVeridentUserDetails.AccountId = SafeValue<int>(dtUserDetails.Rows[0]["AccountID"]);
                                    objVeridentUserDetails.UserId = SafeValue<int>(dtUserDetails.Rows[0]["UserId"]);
                                    objVeridentUserDetails.Token = SafeValue<string>(dtUserDetails.Rows[0]["AccessToken"]);
                                    objVeridentUserDetails.FirstName = SafeValue<string>(dtUserDetails.Rows[0]["FirstName"]);
                                    objVeridentUserDetails.LastName = SafeValue<string>(dtUserDetails.Rows[0]["LastName"]);
                                    objVeridentUserDetails.ImageUrl = (!string.IsNullOrWhiteSpace(SafeValue<string>(dtUserDetails.Rows[0]["ImageName"])) ? (RecordlincUrlForImage + SafeValue<string>(dtUserDetails.Rows[0]["ImageName"])): string.Empty);
                                    objVeridentUserDetails.AccountName = SafeValue<string>(dtUserDetails.Rows[0]["AccountName"]);
                                    objVeridentUserDetails.NPINumber = SafeValue<string>(dtUserDetails.Rows[0]["NpiNumber"]);
                            }
                        }
                        else if (objMessageDetais.Id == 5)
                        {
                            string Email = "", EncPassword = "", Password = "";
                            if (ds.Tables[1] != null && ds.Tables[1].Rows.Count > 0)
                            {
                                dtUserDetails = ds.Tables[1];
                                Email = SafeValue<string>(dtUserDetails.Rows[0]["email"]);
                                EncPassword = SafeValue<string>(dtUserDetails.Rows[0]["password"]);
                                Password = cryptoHelper.decryptText(EncPassword);
                             
                                ObjTemplate.NewUserEmailFormat(Email, companywebsite + "/User/Index?UserName=" + Email + "&Password=" + EncPassword, Password, companywebsite); // Resend tempate to admin if doctor sing up in system
                            }
                        }
                    }
                }
                objVeridentUser.messageDetais = objMessageDetais;
                objVeridentUser.veridentUserDetails = objVeridentUserDetails;
            }
            catch (Exception EX)
            {
                new clsCommon().InsertErrorLog("VeridentBLL--VeridentUserLogin", EX.Message, EX.StackTrace);
                throw;
            }
            return objVeridentUser;
        }


        public int AddEditPatientVeri(PatientDetailsForVeri objPatientDetailsForVeri,int UserId)
        {
            int nPatientId = 0;
            try
            {
                    DataTable tblRegistration = new DataTable("Registration");
                    tblRegistration.Columns.Add("txtResponsiblepartyFname");
                    tblRegistration.Columns.Add("txtResponsiblepartyLname");
                    tblRegistration.Columns.Add("txtResponsibleRelationship");
                    tblRegistration.Columns.Add("txtResponsibleAddress");
                    tblRegistration.Columns.Add("txtResponsibleCity");
                    tblRegistration.Columns.Add("txtResponsibleState");
                    tblRegistration.Columns.Add("txtResponsibleZipCode");
                    tblRegistration.Columns.Add("txtResponsibleDOB");
                    tblRegistration.Columns.Add("txtResponsibleContact");
                    tblRegistration.Columns.Add("txtPatientPresentPosition");
                    tblRegistration.Columns.Add("txtPatientHowLongHeld");
                    tblRegistration.Columns.Add("txtParentPresentPosition");
                    tblRegistration.Columns.Add("txtParentHowLongHeld");
                    tblRegistration.Columns.Add("txtDriversLicense");
                    tblRegistration.Columns.Add("chkMethodOfPayment");
                    tblRegistration.Columns.Add("txtPurposeCall");
                    tblRegistration.Columns.Add("txtOtherFamily");
                    tblRegistration.Columns.Add("txtWhommay");
                    tblRegistration.Columns.Add("txtSomeonetonotify");
                    tblRegistration.Columns.Add("txtemergency");
                    tblRegistration.Columns.Add("txtemergencyname");
                    tblRegistration.Columns.Add("txtEmployeeName1");
                    tblRegistration.Columns.Add("txtInsurancePhone1");
                    tblRegistration.Columns.Add("txtEmployeeDob1");
                    tblRegistration.Columns.Add("txtEmployerName1");
                    tblRegistration.Columns.Add("txtYearsEmployed1");
                    tblRegistration.Columns.Add("txtNameofInsurance1");
                    tblRegistration.Columns.Add("txtInsuranceAddress1");
                    tblRegistration.Columns.Add("txtInsuranceTelephone1");
                    tblRegistration.Columns.Add("txtProgramorpolicy1");
                    tblRegistration.Columns.Add("txtSocialSecurity1");
                    tblRegistration.Columns.Add("txtUnionLocal1");
                    tblRegistration.Columns.Add("txtEmployeeName2");
                    tblRegistration.Columns.Add("txtInsurancePhone2");
                    tblRegistration.Columns.Add("txtEmployeeDob2");
                    tblRegistration.Columns.Add("txtEmployerName2");
                    tblRegistration.Columns.Add("txtYearsEmployed2");
                    tblRegistration.Columns.Add("txtNameofInsurance2");
                    tblRegistration.Columns.Add("txtInsuranceAddress2");
                    tblRegistration.Columns.Add("txtInsuranceTelephone2");
                    tblRegistration.Columns.Add("txtProgramorpolicy2");
                    tblRegistration.Columns.Add("txtSocialSecurity2");
                    tblRegistration.Columns.Add("txtUnionLocal2");
                    tblRegistration.Columns.Add("txtDigiSign");
                    tblRegistration.Columns.Add("CreatedDate");
                    tblRegistration.Columns.Add("ModifiedDate");
                    tblRegistration.Columns.Add("txtemailaddress");

                tblRegistration.Rows.Add("", "", "", "", "", "",
                    "", "", "", "", "", "", "", "","", "", "", "", "", "", "", objPatientDetailsForVeri.InsuranceCompanyName, objPatientDetailsForVeri.InsuranceCompanyPhone,
                    "", objPatientDetailsForVeri.NameofInsured, "", objPatientDetailsForVeri.MemberId, "", objPatientDetailsForVeri.GroupNumber, "", "", "", "", "", "",
                   "", "", "", "", "", "", "", "", "", "", "", "");

                DataSet dsRegistration = new DataSet("Registration");
                dsRegistration.Tables.Add(tblRegistration);
                DataSet ds = dsRegistration.Copy();
                dsRegistration.Clear();
                string Registration = ds.GetXml();
                nPatientId = objVeridentDAL.AddEditPatientVeri(objPatientDetailsForVeri, UserId, Registration);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentBLL- AddEditPatientVeri", Ex.Message, Ex.StackTrace);
                throw;
            }
            return nPatientId;
        }


        public bool ForgotPassord(string Email)
        {
            string FullName = string.Empty; string EncPassword = string.Empty;
            try
            {                
                string companywebsite = ConfigurationManager.AppSettings["VeridentWebLink"];
                string ResetPasswordLink = ConfigurationManager.AppSettings["VeridentResetPassorwLink"];
                bool rResult = false;
                DataTable dtCheck = ObjColleaguesData.CheckEmailInSystem(Email);// Check in system email is there or not
                if (dtCheck !=null && dtCheck.Rows.Count > 0)
                {
                    //if (SafeValue<int>(dtCheck.Rows[0]["IsInSystem"]) == 1 && SafeValue<int>(dtCheck.Rows[0]["Status"]) == 1)
                    if (SafeValue<int>(dtCheck.Rows[0]["IsInSystem"]) == 1)
                    {
                        FullName = SafeValue<string>(dtCheck.Rows[0]["FirstName"]) + " " + SafeValue<string>(dtCheck.Rows[0]["LastName"]);                        
                        ObjTemplate.SendForgetPassword(FullName, ResetPasswordLink, Email, companywebsite);
                        rResult = true; 
                    }
                    else 
                    {
                        rResult = false;
                    }
                }
                return rResult;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentBLL--ForgotPassord", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public int ResetPassord(UserAccountPro objResetPassword)
        {            
            try
            {   
                int rResult = 0;
                objResetPassword.Email = objResetPassword.Email.Replace(" ", "+");
                objResetPassword.Email = ObjTripleDESCryptoHelper.decryptText(objResetPassword.Email);
                objResetPassword.Password = ObjTripleDESCryptoHelper.encryptText(objResetPassword.Password);
                DataTable dtCheck = ObjColleaguesData.CheckEmailInSystem(objResetPassword.Email);// Check in system email is there or not
                if (dtCheck != null && dtCheck.Rows.Count > 0)
                {
                    //if (SafeValue<int>(dtCheck.Rows[0]["IsInSystem"]) == 1 && SafeValue<int>(dtCheck.Rows[0]["Status"]) == 1)
                    if (SafeValue<int>(dtCheck.Rows[0]["IsInSystem"]) == 1)
                    {
                        bool Status =objVeridentDAL.ResetPasswordOfDoctor(objResetPassword);
                        if (Status)
                        {
                            rResult = 1;
                        }                       
                    }
                    else
                    {
                        rResult = 2;
                    }
                }
                return rResult;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentBLL--ResetPassord", Ex.Message, Ex.StackTrace);
                throw;
            }
        }


        public int AccountActivation(UserAccountPro objUserAccountPro)
        {
            try
            {
                int rReturn = 0;
                objUserAccountPro.Email = objUserAccountPro.Email.Replace(" ", "+");
                objUserAccountPro.Email = ObjTripleDESCryptoHelper.decryptText(objUserAccountPro.Email);
                DataTable dtLogin = new DataTable();
                DataTable dtFirstLogin = new DataTable();
                DataTable dtCheckFirst = new DataTable();
                string FullName = string.Empty, Email = string.Empty, Password = string.Empty, VeridentLink = string.Empty;
                VeridentLink = ConfigurationManager.AppSettings["VeridentWebLink"];
                DataTable dtCheck = ObjColleaguesData.CheckEmailInSystem(objUserAccountPro.Email);
                if (dtCheck != null && dtCheck.Rows.Count > 0)
                {
                    int IsInSystem = Convert.ToInt32(dtCheck.Rows[0]["IsInSystem"]);
                    if (IsInSystem == 0)
                    {
                        dtFirstLogin = ObjAdmin.GetTempSignupByEmail(objUserAccountPro.Email);

                        FullName = SafeValue<string>(dtFirstLogin.Rows[0]["first_name"]) + " " + SafeValue<string>(dtFirstLogin.Rows[0]["last_name"]);
                        Password = ObjTripleDESCryptoHelper.decryptText(SafeValue<string>(dtFirstLogin.Rows[0]["password"]));
                        Email = objUserAccountPro.Email;


                        if (dtFirstLogin != null && dtFirstLogin.Rows.Count > 0)
                        {
                            int NewUserId = 0;
                            dtCheckFirst = ObjColleaguesData.GetFirstLoginCount(Convert.ToInt32(dtFirstLogin.Rows[0]["id"]));
                            if (dtCheckFirst !=null && dtCheckFirst.Rows.Count == 0)
                            {
                                DataTable transactioHistory = new DataTable();
                                bool isPaidStatus = false;
                                // Move data from temp table to main member table
                                transactioHistory = ObjColleaguesData.GetTransactionHistoryByUserId(Convert.ToInt32(dtFirstLogin.Rows[0]["id"]));
                                if (transactioHistory.Rows.Count > 0)
                                {
                                    isPaidStatus = true;
                                }
                                string EncPassword = SafeValue<string>(Convert.ToString(dtFirstLogin.Rows[0]["password"]));

                                string AccountKey = Crypto.SHA1(Convert.ToString(DateTime.Now.Ticks));
                                NewUserId = ObjColleaguesData.SingUpDoctor(objUserAccountPro.Email, EncPassword,SafeValue<string>(dtFirstLogin.Rows[0]["first_name"]), SafeValue<string>(dtFirstLogin.Rows[0]["last_name"]), null, null, SafeValue<string>(dtFirstLogin.Rows[0]["PracticeName"]), AccountKey, null, null, null, null, null, null, SafeValue<string>(dtFirstLogin.Rows[0]["phoneno"]), null, isPaidStatus, SafeValue<string>(dtFirstLogin.Rows[0]["facebook"]), SafeValue<string>(dtFirstLogin.Rows[0]["linkedin"]), SafeValue<string>(dtFirstLogin.Rows[0]["twitter"]), null, null, SafeValue<string>(dtFirstLogin.Rows[0]["description"]), null, null, null, null, null, null, null, 0, null, null, null, 0,null,null,SafeValue<string>(dtFirstLogin.Rows[0]["NpiNumber"]));
                                if (!string.IsNullOrEmpty(Convert.ToString(dtFirstLogin.Rows[0]["image_url"])))
                                {
                                    string makefilename = "$" + System.DateTime.Now.Ticks + "$" + (SafeValue<string>(dtFirstLogin.Rows[0]["first_name"])) + ".png";
                                    string rootPath = "~/DentistImages/";
                                    string fileName = System.IO.Path.Combine(HttpContext.Current.Server.MapPath(rootPath), makefilename);
                                    System.Drawing.Image image = ObjCommon.DownloadImageFromUrl(SafeValue<string>(dtFirstLogin.Rows[0]["image_url"]));
                                    bool IsUploadImage = ObjColleaguesData.UpdateDoctorProfileImageById(Convert.ToInt32(NewUserId), makefilename);
                                    image.Save(fileName);
                                }
                                if (NewUserId != 0)
                                {
                                    bool Result2 = ObjColleaguesData.UpdateSocialMediaDetailsByUserId(NewUserId,SafeValue<string>(dtFirstLogin.Rows[0]["facebook"]), SafeValue<string>(dtFirstLogin.Rows[0]["linkedin"]), SafeValue<string>(dtFirstLogin.Rows[0]["twitter"]), null, null, null, null, null, null);
                                }

                                if (isPaidStatus)
                                {
                                    ObjColleaguesData.UpdateNewUserID(Convert.ToInt32(transactioHistory.Rows[0]["TransactionId"]), NewUserId, true);
                                }
                                // update temp table status here 
                                
                                bool result = ObjAdmin.UpdateStatusInTempTableUserById(Convert.ToInt32(dtFirstLogin.Rows[0]["id"]), 1);// status 1 for that we move records from temp table to our system
                               
                                ObjTemplate.NewUserSendEmailOrPassword(FullName,Email,Password, VeridentLink);
                                rReturn = 1;
                            }
                        }
                        
                     }
                    else
                    {
                        rReturn = 0;
                    }
                }else
                {
                    rReturn = 2;
                }
                return rReturn;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentBLL--AccountActivation", Ex.Message, Ex.StackTrace);
                throw;
            }
        }



        #region DentalRegistrationDetails
        //public DentalRegistarionDetails GetPatientRegistarionDetails(int PatientId)
        //{
        //    DentalRegistarionDetails objDRD = new DentalRegistarionDetails();
        //    try
        //    {
        //        DataTable dt = objVeridentDAL.GetPatientRegistarionDetails(PatientId);
        //        if (dt !=null && dt.Rows.Count !=0 && dt.Rows.Count > 0)
        //        {                    
        //            objDRD.txtResponsiblepartyFname = SafeValue<string>(dt.Rows[0]["txtResponsiblepartyFname"]);
        //            objDRD.txtResponsiblepartyLname = SafeValue<string>(dt.Rows[0]["txtResponsiblepartyLname"]);
        //            objDRD.txtResponsiblepartyFname = SafeValue<string>(dt.Rows[0]["txtResponsiblepartyFname"]);
        //            objDRD.txtResponsiblepartyFname = SafeValue<string>(dt.Rows[0]["txtResponsiblepartyFname"]);

        //        }
        //    }
        //    catch (Exception Ex)
        //    {
        //        new clsCommon().InsertErrorLog("VeridentBLL GetPatientRegistarionDetails", Ex.Message, Ex.StackTrace);
        //        throw;
        //    }
        //}
        #endregion
    }
}
