﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.ViewModel;
using DataAccessLayer.Common;
using DataAccessLayer.AppointmentData;
using System.Data;
using BO.Models;
using DataAccessLayer.Appointment;
using DataAccessLayer.PatientsData;
using static DataAccessLayer.Common.clsCommon;
using DataAccessLayer.ColleaguesData;
using System.Configuration;
using Newtonsoft.Json;

namespace BusinessLogicLayer
{
    public class AppointmentBLL
    {
        clsCommon ObjCommon = new clsCommon();

        public List<InsuredAppointment> GetInsuredAppointmentlist(int UserId, int SortColumn, int SortDirection, int PageIndex, int PageSize, string SearchText, string TimeZoneSystemName)
        {
            try
            {
                DataTable Dt = new DataTable();
                clsAppointment ObjApp = new clsAppointment();
                List<InsuredAppointment> lst = new List<InsuredAppointment>();
                Dt = ObjApp.GetInsuredAppointmentListDLL(UserId, SortColumn, SortDirection, PageIndex, PageSize, SearchText);
                string DOB, strDOB;

                DateTime Appointdate;
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    foreach (DataRow item in Dt.Rows)
                    {
                        Appointdate = SafeValue<DateTime>(SafeValue<DateTime>(SafeValue<string>(item["AppointmentDate"])).ToString("yyyy-MM-dd") + " " + SafeValue<string>(item["AppointmentTime"]));

                        if (Appointdate >= DateTime.Now) //-- Add in list if appointment date time is grater then current.
                        {
                            Appointdate = clsHelper.ConvertFromUTC(Appointdate, TimeZoneSystemName);
                            DOB = SafeValue<string>(item["DateOfBirth"]);
                            strDOB = !string.IsNullOrEmpty(DOB) ? new CommonBLL().ConvertToDate(clsHelper.ConvertFromUTC(SafeValue<DateTime>(DOB), TimeZoneSystemName), (int)BO.Enums.Common.DateFormat.GENERAL) : string.Empty;
                            lst.Add(new InsuredAppointment()
                            {
                                AppointmentDate = new CommonBLL().ConvertToDate(Appointdate, (int)BO.Enums.Common.DateFormat.GENERAL),
                                UserName = SafeValue<string>(item["DoctorName"]),
                                UserId = SafeValue<int>(item["UserId"]),
                                PatientName = SafeValue<string>(item["FirstName"]) + ' ' + SafeValue<string>(item["LastName"]),
                                PatientId = SafeValue<int>(item["PatientId"]),
                                PrimaryInsuranceCompanyName = SafeValue<string>(item["Primary_Insurance_Company"]),
                                SecondaryInsuranceCompanyName = SafeValue<string>(item["Secondary_Insurance_Company"]),
                                MedicalInsuranceCompanyName = SafeValue<string>(item["Medical_Insurance_Company"]),
                                PrimaryInsuredDOB = SafeValue<string>(item["Primary_Insured_DOB"]),
                                SecondaryInsuredDOB = SafeValue<string>(item["Secondary_Insured_DOB"]),
                                MedicalInsuredDOB = SafeValue<string>(item["Medical_Insured_DOB"]),
                                PrimaryInsuredName = SafeValue<string>(item["Primary_Insured_Name"]),
                                SecondaryInsuredName = SafeValue<string>(item["Secondary_Insured_Name"]),
                                MedicalInsuredName = SafeValue<string>(item["Medical_Insured_Name"]),
                                PrimaryMemberID = SafeValue<string>(item["Primary_MemberID"]),
                                SecondaryMemberID = SafeValue<string>(item["Secondary_MemberID"]),
                                MedicalMemberID = SafeValue<string>(item["Medical_MemberID"]),
                                PatientDOB = strDOB,
                                PrimaryCompanyPhone = SafeValue<string>(item["Primary_Insurance_Phone"]),
                                SecondaryCompanyPhone = SafeValue<string>(item["Secondary_Insurance_Phone"]),
                                MedicalCompanyPhone = SafeValue<string>(item["Medical_Insurance_Phone"])
                            });
                        }

                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("AppointmentBLL--GetInsuredAppointmentlist", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        //To insert Operatory
        public static OperatoryResponse InsertOperatory(List<Operatory> obj)
        {
            int RlAptId = 0;
            OperatoryResponse Response = new OperatoryResponse();
            List<InsertedOperRecords> lst = new List<InsertedOperRecords>();
            List<NotInsertedOperRecords> Lst = new List<NotInsertedOperRecords>();
            Response.TotalInsertedRecords = 0;
            Response.TotalNotInsertedRecords = 0;
            Response.ReceivedRecord = obj.Count;
            if (PatientRewardBLL.CheckDentrixConnectorKeyExists(obj[0].DentrixConnectorId))
            {
                clsAppointmentData objclsAppointmentData = new clsAppointmentData();
                foreach (var item in obj)
                {
                    DataTable dt = clsPatientsData.CheckDentrixConnectorExistOrNot(item.DentrixConnectorId);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        if (SafeValue<DateTime>(item.automodifiedtimestamp).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                        {
                            item.automodifiedtimestamp = null;
                        }
                        RlAptId = objclsAppointmentData.InsertAPIAppointmentResources(item);
                        if (RlAptId > 0)
                        {
                            InsertedOperRecords Inserted = new InsertedOperRecords();
                            Inserted.DentrixId = item.DentrixId;
                            Inserted.RLIdNumber = RlAptId;
                            Response.TotalInsertedRecords = Response.TotalInsertedRecords + 1;
                            lst.Add(Inserted);
                            Response.InsertedList = lst;
                        }
                        else
                        {
                            NotInsertedOperRecords UnInserted = new NotInsertedOperRecords();
                            UnInserted.DentrixId = item.DentrixId;
                            UnInserted.OperatoryId = item.OperatoryId;
                            Response.TotalNotInsertedRecords = Response.TotalNotInsertedRecords + 1;
                            UnInserted.ErrorMessage = "Operatory name already exists.";
                            Lst.Add(UnInserted);
                            Response.NotInsertedList = Lst;
                        }
                    }
                    else
                    {
                        NotInsertedOperRecords UnInserted = new NotInsertedOperRecords();
                        UnInserted.DentrixId = item.DentrixId;
                        UnInserted.OperatoryId = item.OperatoryId;
                        Response.TotalNotInsertedRecords = Response.TotalNotInsertedRecords + 1;
                        UnInserted.ErrorMessage = "DentrixConnectorID not exists.";
                        Lst.Add(UnInserted);
                        Response.NotInsertedList = Lst;
                    }

                }
            }
            else
            {

                Response.TotalNotInsertedRecords = obj.Count;
                Response.NotInsertedList = new List<NotInsertedOperRecords>();
                foreach (var item in obj)
                {
                    Response.NotInsertedList.Add(new NotInsertedOperRecords()
                    {
                        DentrixId = item.DentrixId,
                        ErrorMessage = "Invalid Dentrix ConnectorID"
                    });
                }
            }

            return Response;
        }

        /// <summary>
        /// List of Operatory (OperatoryList) of Perticulare dentist
        /// </summary>
        /// <param name="LocationId"></param>
        /// <returns>List of Opration Room</returns>
        public static List<DentistOperatory> OperatoryList(int LocationId)
        {
            try
            {
                clsAppointment ObjApp = new clsAppointment();
                List<DentistOperatory> Response = new List<DentistOperatory>();
                DataTable dt = ObjApp.OperatoryList(LocationId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        DentistOperatory listObj = new DentistOperatory();

                        listObj.Id = SafeValue<int>(item["ApptResourceId"]);
                        listObj.Name = SafeValue<string>(item["ResourceName"]);
                        Response.Add(listObj);
                    }
                }
                return Response;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("AppointmentBLL--OperatoryList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// List of doctor services
        /// </summary>
        /// <param name="LocationId"></param>
        /// <returns>services</returns>
        public static List<DoctorServices> DoctorServices(int LocationId)
        {
            try
            {
                clsAppointment ObjApp = new clsAppointment();
                List<DoctorServices> Response = new List<DoctorServices>();
                DataTable dt = ObjApp.DoctorServices(LocationId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        DoctorServices listObj = new DoctorServices();
                        listObj.Id = SafeValue<int>(item["AppointmentServiceId"]);
                        listObj.Name = SafeValue<string>(item["ServiceType"]);
                        listObj.DurationInMinutes = SafeValue<int>(item["TimeDuration"]);
                        listObj.Price = SafeValue<int>(item["Price"]);
                        Response.Add(listObj);
                    }
                }
                return Response;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("AppointmentBLL--DoctorServices", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        // To Delete Operatory
        public static List<DeletedOperatory> DeleteOperatory(List<RemoveOperatory> obj)
        {
            try
            {
                List<DeletedOperatory> DeletedOperList = new List<DeletedOperatory>();
                if (PatientRewardBLL.CheckDentrixConnectorKeyExists(obj[0].DentrixConnecoterKey))
                {
                    clsAppointmentData objclsAppointmentData = new clsAppointmentData();
                    foreach (var item in obj)
                    {
                        DeletedOperList.Add(new DeletedOperatory()
                        {
                            DentrixId = item.DentrixId,
                            IsDeleted = objclsAppointmentData.DeleteAPIAppointmentResource(item)
                        });
                    }
                }
                return DeletedOperList;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("AppointmentBLL--DeleteOperatory", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static DataTable GetDentrixSyncDetail(string DentrixConnectorID)
        {
            DataTable Dt = new DataTable();
            if (PatientRewardBLL.CheckDentrixConnectorKeyExists(DentrixConnectorID))
            {
                clsAppointmentData objclsAppointmentData = new clsAppointmentData();
                Dt = objclsAppointmentData.GetAPIDentrixSyncDetail(DentrixConnectorID);
            }
            return Dt;
        }

        public static List<AppointmentReportModel> GetScheduleDataList(GetScheduleDataModel model)
        {
            clsCommon objCommon = new clsCommon();
            try
            {
                int UserId = model.UserId.All(char.IsNumber) ? SafeValue<int>(model.UserId) : 0;
                //Code for default param
                model.PageIndex = (model.PageIndex == 0) ? 1 : model.PageIndex;
                model.PageSize = (model.PageSize == 0) ? 10 : model.PageSize;
                model.SortColumn = (model.SortColumn == "0" || string.IsNullOrEmpty(model.SortColumn)) ? "1" : model.SortColumn;
                model.SortDirection = (model.SortDirection == "0" || string.IsNullOrEmpty(model.SortDirection)) ? "1" : model.SortDirection;

                clsPatientsData cls = new clsPatientsData();
                DataTable dt = new DataTable();
                string TimeZoneSystemName = new clsAppointmentData().GetTimeZoneOfDoctor(UserId);
                //model.FromDate = clsHelper.ConvertFromUTC(SafeValue<DateTime>(model.FromDate), TimeZoneSystemName);
                //model.ToDate = clsHelper.ConvertFromUTC(SafeValue<DateTime>(model.ToDate), TimeZoneSystemName);
                List<AppointmentReportModel> AppReport = null;

                dt = cls.GetAppointmentReportofdoctorForOneClick(UserId, model.SortColumn, model.SortDirection, model.PageIndex, model.PageSize, model.FirstName, model.LastName, model.DOB, model.Email, model.Phone);
                if (dt != null && dt.Rows.Count > 0)
                {
                    AppReport = new List<AppointmentReportModel>();
                    foreach (DataRow item in dt.Rows)
                    {


                        //DateTime AppintmentDateTime = DateTime.MinValue;
                        //if(!string.IsNullOrWhiteSpace(SafeValue<string>(item["AppointmentDateTime"])))
                        //{
                        //    AppintmentDateTime = SafeValue<DateTime>(item["AppointmentDateTime"]);
                        //    AppintmentDateTime = SafeValue<DateTime>(AppintmentDateTime.ToString("yyyy-MM-dd") + " " +
                        //                           AppintmentDateTime.ToString("hh:mm tt"));
                        //    AppintmentDateTime = clsHelper.ConvertFromUTC(AppintmentDateTime, TimeZoneSystemName);

                        //}


                        AppReport.Add(new AppointmentReportModel()
                        {

                            //AppointmentId = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(item["AppointmentId"])) ? 0 : item["AppointmentId"]),
                            //DoctorName = SafeValue<string>(item["DoctorName"]),
                            PatientFirstName = SafeValue<string>(item["FirstName"]),
                            PatientLastName = SafeValue<string>(item["LastName"]),
                            Phone = SafeValue<string>(item["Phone"]),
                            PatientId = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(item["PatientId"])) ? 0 : item["PatientId"]),
                            // DoctorId = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(item["DoctorId"])) ? 0 : item["DoctorId"]),
                            // PatientAge = SafeValue<string>(item["Age"]),
                            // AppointmentReason = SafeValue<string>(item["Age"]),
                            //AppointmentDate = new CommonBLL().ConvertToDate(AppintmentDateTime, (int)BO.Enums.Common.DateFormat.GENERAL),
                            //AppointmentTime = new CommonBLL().ConvertToTime(AppintmentDateTime, (int)BO.Enums.Common.DateFormat.GENERAL),
                            Email = SafeValue<string>(item["Email"]),
                            Mobile = SafeValue<string>(item["Mobile"]),
                            PatientDOB = SafeValue<string>(item["DateOfBirth"]),
                        });
                    }
                }
                return AppReport;
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public static List<Appointmentslist> AppointmentlistforPatientHistory(int PatientId, int AppointmentType, string TimeZone)
        {
            try
            {
                List<Appointmentslist> lst = new List<Appointmentslist>();
                DataTable dataTable = new clsAppointmentData().GetTop3PatientAppointment(PatientId, AppointmentType);
                foreach (DataRow item in dataTable.Rows)
                {
                    DateTime AppointmentDate = Convert.ToDateTime(Convert.ToDateTime(Convert.ToString(item["AppointmentDate"])).ToString("yyyy/MM/dd") + " " + Convert.ToString(item["AppointmentTime"]));
                    AppointmentDate = clsHelper.ConvertFromUTC(AppointmentDate, TimeZone);
                    string AppointmentTime = "";
                    if (!string.IsNullOrWhiteSpace(Convert.ToString(AppointmentDate.TimeOfDay)))
                    {
                        AppointmentTime = DateTime.Today.Add(Convert.ToDateTime(Convert.ToString(AppointmentDate.TimeOfDay)).TimeOfDay).ToString("hh:mm tt");
                    }
                    lst.Add(new Appointmentslist()
                    {
                        AppointmentDate = AppointmentDate,
                        AppointmentTime = AppointmentTime,
                        Doctorname = Convert.ToString(item["Doctorname"]),
                        ServiceType = Convert.ToString(item["ServiceType"]),
                        Status = Convert.ToString(item["Status"]),
                        AppointmentId = Convert.ToInt32(item["AppointmentId"]),
                    });
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get List of available appointment listing
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>List of available appointment</returns>
        public static List<AppointmentListing> AppointmentListing(int UserId, DentrixAppointment param)
        {
            clsAppointment ObjApp = new clsAppointment();
            List<AppointmentListing> Response = new List<AppointmentListing>();

            DataTable dt = ObjApp.AppointmentListing(UserId, param);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    #region Appointment 
                    AppointmentListing obj = new AppointmentListing();
                    obj.Id = SafeValue<int>(item["AppointmentId"]);
                    obj.Date = SafeValue<DateTime>(item["AppointmentDate"]);
                    obj.Time = SafeValue<string>(Convert.ToString(item["AppointmentTime"]));
                    obj.DurationInMinutes = SafeValue<string>(item["AppointmentLength"]);
                    obj.Note = SafeValue<string>(item["Note"]);
                    obj.CancellationNote = SafeValue<string>(item["CancelNote"]);
                    //obj.Reason = SafeValue<string>(item["AppointmentReason"]);
                    obj.Status = SafeValue<string>(item["AppointmentStatus"]);
                    #endregion

                    #region Doctor
                    DoctorDetials doc = new DoctorDetials();
                    doc.Id = SafeValue<int>(item["UserId"]);
                    doc.FirstName = SafeValue<string>(item["FirstName"]);
                    doc.LastName = SafeValue<string>(item["LastName"]);
                    obj.Doctor = doc;
                    #endregion

                    #region Doctor Services
                    Services services = new Services();
                    services.Id = SafeValue<int>(item["AppointmentServiceId"]);
                    services.Type = SafeValue<string>(item["ServiceType"]);
                    services.Price = SafeValue<double>(item["Price"]);
                    services.DurationInMinutes = SafeValue<int>(item["TimeDuration"]);
                    obj.Services = services;
                    #endregion

                    #region Resource
                    Resource resource = new Resource();
                    resource.Id = SafeValue<int>(item["ApptResourceId"]);
                    resource.Name = SafeValue<string>(item["ResourceName"]);
                    obj.Operatory = resource;
                    #endregion

                    #region Patient
                    AppointmentPatientDetail patient = new AppointmentPatientDetail();
                    patient.Id = SafeValue<int>(item["PatientId"]);
                    patient.FirstName = SafeValue<string>(item["PatientFirstName"]);
                    patient.LastName = SafeValue<string>(item["PatientLastName"]);
                    //patient.MiddelName = SafeValue<string>(item["MiddelName"]);
                    if (item["DateOfBirth"] != null && SafeValue<DateTime>(item["DateOfBirth"]) != DateTime.MinValue)
                        patient.DateOfBirth = SafeValue<DateTime>(item["DateOfBirth"]);
                    patient.Email = SafeValue<string>(item["Email"]);
                    patient.Phone = SafeValue<string>(item["WorkPhone"]);
                    patient.Insurance = SafeValue<string>(item["HasInsurance"]);
                    patient.InsuranceCompanyName = SafeValue<string>(item["InsuranceCompanyName"]);
                    patient.Gender = SafeValue<string>(item["Gender"]);
                    obj.Patient = patient;
                    #endregion

                    Response.Add(obj);

                }
            }


            return Response;
        }

        /// <summary>
        /// get all patient base on doctor
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="param"></param>
        /// <returns></returns>
        public static List<GetPatientList> PatientListing(int UserId, PostPatient param)
        {
            clsAppointment ObjApp = new clsAppointment();
            List<GetPatientList> Response = new List<GetPatientList>();

            DataTable dt = ObjApp.PatientListing(UserId, param);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    #region Patients 
                    GetPatientList patient = new GetPatientList();
                    patient.Id = SafeValue<int>(item["PatientId"]);
                    patient.FirstName = SafeValue<string>(item["FirstName"]);
                    patient.LastName = SafeValue<string>(item["LastName"]);
                    patient.Gender = SafeValue<string>(item["Gender"]);
                    if (item["DateOfBirth"] != null && SafeValue<DateTime>(item["DateOfBirth"]) != DateTime.MinValue)
                        patient.DateOfBirth = SafeValue<DateTime>(item["DateOfBirth"]);
                    patient.Email = SafeValue<string>(item["EmailAddress"]);
                    patient.Phone = SafeValue<string>(item["Phone"]);
                    patient.Address = SafeValue<string>(item["Address"]);
                    patient.City = SafeValue<string>(item["City"]);
                    patient.State = SafeValue<string>(item["STATE"]);
                    patient.Country = SafeValue<string>(item["Country"]);
                    patient.PostalCode = SafeValue<string>(item["ZipCode"]);
                    #endregion

                    #region Doctor
                    if (!string.IsNullOrEmpty(Convert.ToString(item["UserId"])))
                    {
                        DoctorDetials doc = new DoctorDetials();
                        doc.Id = SafeValue<int>(item["UserId"]);
                        doc.FirstName = SafeValue<string>(item["FirstName"]);
                        doc.LastName = SafeValue<string>(item["LastName"]);
                        patient.Doctor = doc;
                    }
                    #endregion


                    #region Patient ReferBy
                    if (!string.IsNullOrEmpty(Convert.ToString(item["ReferBy"])))
                    {
                        PatientReferBy referby = new PatientReferBy();
                        referby.Id = SafeValue<int>(item["ReferBy"]);
                        referby.FirstName = SafeValue<string>(item["ReferByFirstName"]);
                        referby.LastName = SafeValue<string>(item["ReferByLastName"]);
                        patient.ReferBy = referby;
                    }
                    #endregion

                    #region Resource
                    if (!string.IsNullOrEmpty(Convert.ToString(item["AddressInfoId"])))
                    {
                        DoctorLocation location = new DoctorLocation();
                        location.Id = SafeValue<int>(item["AddressInfoId"]);
                        location.Name = SafeValue<string>(item["Location"]);
                        patient.Location = location;
                    }
                    #endregion

                    Response.Add(patient);

                }
            }


            return Response;
        }

        /// <summary>
        /// Get team member list
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="param"></param>
        /// <returns>Team member list</returns>
        public static List<DoctorTeamMemberList> TeamMembersListing(int UserId, PostTeamMember param)
        {
            string DoctorImagePath = ConfigurationManager.AppSettings["DoctorImage"];
            clsAppointment ObjApp = new clsAppointment();
            List<DoctorTeamMemberList> Response = new List<DoctorTeamMemberList>();
            DataSet ds = ObjApp.TeamMembersListing(UserId, param);
            DataTable dt = ds.Tables[0];
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    #region TeamMember 
                    DoctorTeamMemberList teammember = new DoctorTeamMemberList();
                    teammember.Id = SafeValue<int>(item["UserId"]);
                    teammember.FirstName = SafeValue<string>(item["FirstName"]);
                    teammember.LastName = SafeValue<string>(item["LastName"]);
                    teammember.Gender = SafeValue<string>(item["Gender"]);
                    if (!string.IsNullOrEmpty(SafeValue<string>(item["ImageName"])))
                        teammember.Image = DoctorImagePath + SafeValue<string>(item["ImageName"]);
                    teammember.Email = SafeValue<string>(item["Email"]);
                    //teammember.Speciality = SafeValue<string>(item["Speciality"]);
                    #endregion

                    #region Speciality
                    DataTable selectedTable = ds.Tables[1].AsEnumerable()
                            .Where(r => r.Field<int>("UserId") == teammember.Id)
                            .CopyToDataTable();
                    if (selectedTable != null && selectedTable.Rows.Count > 0)
                    {
                        List<DoctorLocation> Specialitylst = new List<DoctorLocation>();
                        foreach (DataRow SpecialityItem in selectedTable.Rows)
                        {
                            DoctorLocation Speciality = new DoctorLocation();
                            Speciality.Id = SafeValue<int>(SpecialityItem["SpecialityId"]);
                            Speciality.Name = SafeValue<string>(SpecialityItem["Name"]);
                            Specialitylst.Add(Speciality);
                        }
                        teammember.Specialitys = Specialitylst;
                    }

                    #endregion

                    #region Location
                    if (!string.IsNullOrEmpty(Convert.ToString(item["AddressInfoId"])))
                    {
                        DoctorLocation location = new DoctorLocation();
                        location.Id = SafeValue<int>(item["AddressInfoId"]);
                        location.Name = SafeValue<string>(item["Location"]);
                        teammember.Location = location;
                    }
                    #endregion

                    Response.Add(teammember);

                }
            }


            return Response;
        }

        /// <summary>
        /// This is used for Dentist Time Slot 
        /// </summary>
        /// <param name="filterAppt"></param>
        /// <param name="TimeZone"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static List<ApptDayView> GetDentistTimeSlot(FilterSchedule filterAppt, string TimeZone, int UserId)
        {
            try
            {
                CreateAppt createAppt = new CreateAppt();
                if (string.IsNullOrWhiteSpace(TimeZone))
                {
                    TimeZone = clsColleaguesData.GetTimeZoneOfDoctor(UserId);
                }
                clsAppointment objAppointment = new clsAppointment();
                //var Service = objAppointment.GetDoctorServicesByDoctorId(0, filterAppt.ServiceId);
                //var TimeSlot = CommonAppt.GetAppointmentDataIntoList(objAppointment.GetCreateAppointmentTimeSlotViewData(UserId, filterAppt.OperatoryId, 0), TimeZone);
                //createAppt = CommonAppt.TimeSlotAllocation(TimeSlot, Service, filterAppt.StartDate, TimeZone);
                return createAppt.AppointmentDayViewList;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("AppointmentBLL--GetDentistTimeSlot", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This method used for Appointment write back for Denial request. 
        /// </summary>
        /// <returns></returns>
        public static List<Appointments> GetWriteBackAppointments(int UserId, string DentrixConnectorId)
        {
            try
            {
                List<Appointments> appt = new List<Appointments>();
                DataTable Dt = clsAppointment.GetWriteBackAPpointmentList(UserId, DentrixConnectorId);
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    foreach (DataRow item in Dt.Rows)
                    {
                        appt.Add(new Appointments()
                        {
                            AppointmentDate = SafeValue<DateTime>(item["AppointmentDate"]),
                            AppointmentLength = SafeValue<int>(item["DurationInMinutes"]),
                            AppointmentId = SafeValue<int>(item["AppointmentId"]),
                            AppointmentStart = SafeValue<DateTime>(item["AppointmentTime"]),
                            AppointmentReason = SafeValue<string>(item["AppointmentReason"]),
                            PatientId = SafeValue<int>(item["PatientId"]),
                            OperatoryId = SafeValue<string>(item["ResourceName"]),
                            ProviderId = SafeValue<string>(item["DentrixProviderId"]),
                            automodifiedtimestamp = SafeValue<DateTime>(item["AutoModifiedTimeStamp"]),
                            dentrixConnectorID = SafeValue<string>(item["DentrixConnectorKey"]),
                        });
                    }
                }
                return appt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("AppointmentBLL--GetWriteBackAppointments", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Book appointment 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>Inserted appointmentid</returns>
        public static int AppointmentInsert(int UserId, BookAppointment param)
        {
            clsAppointment ObjApp = new clsAppointment();
            int Id = ObjApp.AppointmentInsert(UserId, param);
            return Id;
        }

        /// <summary>
        /// Appointment writeback for PMS
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns>booked appointment list</returns>
        public static List<AppointmentsWriteback> AppointmentWriteback(WritebackAppointment Obj)
        {
            clsAppointment ObjApp = new clsAppointment();
            List<AppointmentsWriteback> Response = new List<AppointmentsWriteback>();

            DataTable dt = ObjApp.AppointmentWriteback(Obj);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    AppointmentsWriteback obj = new AppointmentsWriteback();
                    obj.AppointmentId = SafeValue<int>(item["DentrixAppointmentId"]);
                    obj.RLAppointmentId = SafeValue<int>(item["RlAppointmentId"]);
                    obj.RLPendingAppointmentId = SafeValue<int>(item["RLPendingAppointmentId"]);
                    obj.AppointmentDate = SafeValue<DateTime>(item["AppointmentDate"]);
                    obj.AppointmentStart = SafeValue<DateTime>(item["AppointmentStart"]);
                    obj.AppointmentLength = SafeValue<int>(item["AppointmentLength"]);
                    obj.OperatoryId = SafeValue<string>(item["OperatoryId"]);
                    obj.ProviderId = SafeValue<string>(item["ProviderId"]);
                    obj.StaffId = SafeValue<string>(item["StaffId"]);
                    obj.AmountOverriden = SafeValue<bool>(item["AmountOverriden"]); ;
                    obj.Amount = SafeValue<decimal>(item["Amount"]); ;
                    obj.PatientId = SafeValue<int>(item["AssignedPatientId"]);
                    //obj.Identifier = SafeValue<string>(item["Identifier"]);
                    if (item["AutoModifiedTimeStamp"] != null && SafeValue<DateTime>(item["AutoModifiedTimeStamp"]) != DateTime.MinValue)
                        obj.automodifiedtimestamp = SafeValue<DateTime>(item["AutoModifiedTimeStamp"]);
                    obj.DentrixId = SafeValue<int>(item["DentrixAppointmentId"]);
                    //obj.PracticeID = SafeValue<string>(item["PracticeID"]);
                    //obj.ConnectorID = SafeValue<string>(item["ConnectorID"]);
                    obj.dentrixConnectorID = Obj.PMSConnectorId;

                    #region TxProcs
                    string txprocs = SafeValue<string>(item["TxProcs"]);
                    if (!string.IsNullOrEmpty(txprocs))
                    {
                        List<int> txtpro = new List<int>();
                        string[] arraproc = txprocs.Split(',');
                        foreach (var proc in arraproc)
                        {
                            txtpro.Add(Convert.ToInt32(proc));
                        }
                        obj.TxProcs = txtpro;
                    }
                    #endregion

                    #region AdaCodes
                    string adacodes = SafeValue<string>(item["AdaCodes"]);
                    if (!string.IsNullOrEmpty(adacodes))
                    {
                        List<string> lstadacode = new List<string>();
                        string[] arraadacode = adacodes.Split(',');
                        foreach (var adocd in arraadacode)
                        {
                            lstadacode.Add(adocd);
                        }
                        obj.AdaCodes = lstadacode;
                    }
                    #endregion

                    #region NewPatientInfo


                    if (obj.PatientId == 0)
                    {
                        NewPatientInfo npi = new NewPatientInfo();
                        npi.PatientName = SafeValue<string>(item["PatientFullName"]);

                        #region Address
                        List<Addresses> adslst = new List<Addresses>();
                        Addresses ad = new Addresses();
                        ad.DentrixId = SafeValue<string>(item["DentrixAddressInfoId"]);
                        ad.Street1 = SafeValue<string>(item["ExactAddress"]);
                        ad.Street2 = SafeValue<string>(item["Address2"]);
                        ad.City = SafeValue<string>(item["City"]);
                        ad.State = SafeValue<string>(item["State"]);
                        ad.ZipCode = SafeValue<string>(item["ZipCode"]);
                        adslst.Add(ad);
                        npi.Addresses = adslst;
                        #endregion

                        #region PhoneNumber
                        List<PhoneNumbers> pnlst = new List<PhoneNumbers>();

                        #region Work
                        if (!string.IsNullOrEmpty(SafeValue<string>(item["WorkPhone"])))
                        {
                            pnlst.Add(new PhoneNumbers
                            {
                                PhoneType = 0,
                                PhoneNumber = SafeValue<string>(item["WorkPhone"])
                            });

                        }
                        #endregion

                        #region Home
                        if (!string.IsNullOrEmpty(SafeValue<string>(item["Phone"])))
                        {
                            pnlst.Add(new PhoneNumbers
                            {
                                PhoneType = 1,
                                PhoneNumber = SafeValue<string>(item["Phone"])
                            });

                        }
                        #endregion

                        #region Mobile
                        if (!string.IsNullOrEmpty(SafeValue<string>(item["Mobile"])))
                        {
                            pnlst.Add(new PhoneNumbers
                            {
                                PhoneType = 2,
                                PhoneNumber = SafeValue<string>(item["Mobile"])
                            });

                        }
                        #endregion

                        npi.phonenumbers = pnlst;
                        #endregion

                        #region EmailAdresses
                        List<string> ea = new List<string>();
                        string email = SafeValue<string>(item["Email"]);
                        if (!email.Contains("@domain.com") && !string.IsNullOrEmpty(email))
                        {
                            ea.Add(SafeValue<string>(item["Email"]));
                        }
                        npi.EmailAdresses = ea;
                        #endregion

                        obj.NewPatientInfo = npi;
                    }
                    #endregion

                    Response.Add(obj);

                }
            }


            return Response;
        }

        /// <summary>
        /// Appointment writeback response
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public static List<DMPSAppointmentResponsedData> PMSAppointmentWritebackResponse(AppointmentResponsed Obj)
        {
            clsAppointment ObjApp = new clsAppointment();
            List<DMPSAppointmentResponsedData> ResponseObj = new List<DMPSAppointmentResponsedData>();
            foreach (var item in Obj.Result)
            {
                DMPSAppointmentResponsedData update = new DMPSAppointmentResponsedData();
                update = item;
                try
                {
                    int isUpdated = ObjApp.PMSAppointmentWritebackResponse(Obj.PMSConnectorId,item);
                    if (isUpdated == 1)
                    {
                        update.IsSuccess = true;
                        update.Message = "Sucessfully updated";
                    }
                    else
                    {
                        update.IsSuccess = false;
                        update.Message = "Not Updated";
                    }
                    ResponseObj.Add(update);
                }
                catch (Exception ex)
                {
                    update.IsSuccess = false;
                    update.Message = ex.Message;
                    ResponseObj.Add(update);
                }
                
            }
            return ResponseObj;
        }

        /// <summary>
        /// Return list of Schedules for Denial request.
        /// </summary>
        /// <param name="filterAppt"></param>
        /// <returns></returns>
        public static List<Schedules> GetSchedules(FilterSchedule filterAppt)
        {
            try
            {
                List<Schedules> schedules = new List<Schedules>();
                
                DataTable table = new clsAppointment().GetSchedules(filterAppt);
                if (table != null && table.Rows.Count > 0)
                {
                    foreach (DataRow item in table.Rows)
                    {
                        Schedules sd = new Schedules();
                        sd = JsonConvert.DeserializeObject<Schedules>(SafeValue<string>(item["JsonText"]));
                        schedules.Add(sd);
                    } 
                }
                return schedules;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("AppointmentBLL--GetSchedules", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
