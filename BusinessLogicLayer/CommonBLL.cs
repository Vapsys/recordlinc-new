﻿using BO.Models;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using DataAccessLayer.FlipTop;
using System;
using System.Data;
using System.Web;
using BO.ViewModel;
using System.Collections.Generic;
using System.Linq;
using DataAccessLayer.PatientsData;
using System.IO;
using System.Configuration;
using System.Drawing;
using System.Text;
using System.Web.Mvc;
using System.Web.Helpers;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public class CommonBLL
    {
        clsCommon objCommon;
        clsColleaguesData ObjColleaguesData;
        string[] ApprovedImageFileExtension;
        TripleDESCryptoHelper cryptoHelper123 = new TripleDESCryptoHelper();
        string DoctorImage = SafeValue<string>(System.Configuration.ConfigurationManager.AppSettings.Get("DoctorImage"));
        public CommonBLL()
        {
            objCommon = new clsCommon();
            ObjColleaguesData = new clsColleaguesData();
            ApprovedImageFileExtension = new string[] { ".jpg", ".jpeg", ".gif", ".png", ".bmp", ".psd" };
        }

        public void FillSessions(int UserId, bool isWidget, string AppointmentDefaultResourceId, string TimezoneSystemName)
        {
            HttpContext.Current.Session["isWidget"] = isWidget;
            int ParentUserId = 0;
            AppointmentDefaultResourceId = string.IsNullOrEmpty(AppointmentDefaultResourceId) ? "0" : AppointmentDefaultResourceId;
            DataSet dsuser = new DataSet();
            dsuser = ObjColleaguesData.GetDoctorDetailsById(UserId);
            if (dsuser != null && dsuser.Tables.Count > 0)
            {
                HttpContext.Current.Session["AppointmentDefaultResourceId"] = AppointmentDefaultResourceId;
                HttpContext.Current.Session["TimezoneSystemName"] = TimezoneSystemName;
                if (dsuser.Tables[0].Rows.Count > 0)
                {
                    HttpContext.Current.Session["PublicProfileUserId"] = SafeValue<int>(dsuser.Tables[0].Rows[0]["UserId"]);
                    HttpContext.Current.Session["PublicProfileUrl"] = SafeValue<string>(dsuser.Tables[0].Rows[0]["PublicProfileUrl"]);
                    HttpContext.Current.Session["MemberProfile"] = SafeValue<string>(dsuser.Tables[0].Rows[0]["FullName"]);
                    HttpContext.Current.Session["PublicProfileUserEmail"] = SafeValue<string>(dsuser.Tables[0].Rows[0]["Username"]);
                    HttpContext.Current.Session["LocationId"] = SafeValue<string>(dsuser.Tables[0].Rows[0]["LocationId"]);
                    HttpContext.Current.Session["MemberDescription"] = SafeValue<string>(dsuser.Tables[0].Rows[0]["Description"]);
                    string Image = SafeValue<string>(dsuser.Tables[0].Rows[0]["ImageName"]);
                    if (string.IsNullOrWhiteSpace(Image))
                    {
                        string DefaultImage = ConfigurationManager.AppSettings.Get("DoctorProfileImage");
                        DefaultImage = DefaultImage.Contains("../../") ? DefaultImage.Replace("../../", "") : DefaultImage;
                        HttpContext.Current.Session["UserProfileImages"] = SafeValue<string>(HttpContext.Current.Session["CompanyWebSite"]) + DefaultImage;
                    }
                    else
                    {
                        string DefaultImage = DoctorImage;
                        DefaultImage = DefaultImage.Contains("../../") ? DefaultImage.Replace("../../", "/") : DefaultImage;
                        HttpContext.Current.Session["UserProfileImages"] = SafeValue<string>(HttpContext.Current.Session["CompanyWebSite"]) + DefaultImage + Image;
                    }
                    ParentUserId = (dsuser.Tables[0].Rows[0]["ParentUserId"] as int?).GetValueOrDefault(0) == 0 ? SafeValue<int>(dsuser.Tables[0].Rows[0]["UserId"]) : SafeValue<int>(dsuser.Tables[0].Rows[0]["ParentUserId"]);
                }
                if (dsuser.Tables[1].Rows.Count > 0)
                {
                    HttpContext.Current.Session["PublicProfileOfficeName"] = SafeValue<string>(dsuser.Tables[1].Rows[0]["AccountName"]);
                }
                if (dsuser.Tables[2].Rows.Count > 0)
                {
                    HttpContext.Current.Session["PublicProfileUserCity"] = (SafeValue<string>(dsuser.Tables[2].Rows[0]["City"]) != string.Empty ? SafeValue<string>(dsuser.Tables[2].Rows[0]["City"]) : string.Empty) + (SafeValue<string>(HttpContext.Current.Session["MemberProfile"]) != string.Empty ? " > " + SafeValue<string>(HttpContext.Current.Session["MemberProfile"]) : string.Empty);
                    HttpContext.Current.Session["PublicProfileUserState"] = SafeValue<string>(dsuser.Tables[2].Rows[0]["StateName"]);
                    HttpContext.Current.Session["PublicProfileUserCountry"] = SafeValue<string>(dsuser.Tables[2].Rows[0]["CountryName"]);
                    string CityStateZipCode = null;
                    if (!string.IsNullOrEmpty(SafeValue<string>(dsuser.Tables[2].Rows[0]["City"])))
                    {
                        CityStateZipCode = SafeValue<string>(dsuser.Tables[2].Rows[0]["City"]);
                    }
                    if (!string.IsNullOrEmpty(SafeValue<string>(dsuser.Tables[2].Rows[0]["State"])))
                    {
                        if (CityStateZipCode != null)
                            CityStateZipCode = CityStateZipCode + "," + SafeValue<string>(dsuser.Tables[2].Rows[0]["State"]);
                    }
                    if (!string.IsNullOrEmpty(SafeValue<string>(dsuser.Tables[2].Rows[0]["ZipCode"])))
                    {
                        if (CityStateZipCode != null)
                            CityStateZipCode = CityStateZipCode + "," + SafeValue<string>(dsuser.Tables[2].Rows[0]["ZipCode"]);
                    }
                    HttpContext.Current.Session["MemberCityStateZipcode"] = CityStateZipCode;
                    //HttpContext.Current.Session["MemberCityStateZipcode"] = objCommon.CheckNull(SafeValue<string>(dsuser.Tables[2].Rows[0]["City"]), string.Empty) + " ," + objCommon.CheckNull(SafeValue<string>(dsuser.Tables[2].Rows[0]["State"]), string.Empty) + " ," + objCommon.CheckNull(SafeValue<string>(dsuser.Tables[2].Rows[0]["ZipCode"]), string.Empty);

                    //HttpContext.Current.Session["MemberCityStateZipcode"] = objCommon.CheckNull(SafeValue<string>(dsuser.Tables[2].Rows[0]["City"]), string.Empty) + " ," + objCommon.CheckNull(SafeValue<string>(dsuser.Tables[2].Rows[0]["State"]), string.Empty);

                    HttpContext.Current.Session["MemberCity"] = SafeValue<string>(dsuser.Tables[2].Rows[0]["City"]);
                    HttpContext.Current.Session["MemberState"] = SafeValue<string>(dsuser.Tables[2].Rows[0]["State"]);
                    HttpContext.Current.Session["MemberCountry"] = SafeValue<string>(dsuser.Tables[2].Rows[0]["Country"]);

                }

                if (dsuser.Tables[7].Rows.Count > 0)
                {
                    HttpContext.Current.Session["MemberSpeciality"] = SafeValue<string>(dsuser.Tables[7].Rows[0]["Speciality"]);
                }
                else
                {
                    HttpContext.Current.Session["MemberSpeciality"] = null;
                }

                if (dsuser.Tables[15].Rows.Count > 0)
                {
                    HttpContext.Current.Session["Show_PatientForms"] = SafeValue<bool>(dsuser.Tables[15].Rows[0]["PatientForms"]);
                    HttpContext.Current.Session["Show_SpecialOffers"] = SafeValue<bool>(dsuser.Tables[15].Rows[0]["SpecialOffers"]);
                    HttpContext.Current.Session["Show_ReferPatient"] = SafeValue<bool>(dsuser.Tables[15].Rows[0]["ReferPatient"]);
                    HttpContext.Current.Session["Show_Reviews"] = SafeValue<bool>(dsuser.Tables[15].Rows[0]["Reviews"]);
                    HttpContext.Current.Session["Show_AppointmentBooking"] = SafeValue<bool>(dsuser.Tables[15].Rows[0]["AppointmentBooking"]);
                    HttpContext.Current.Session["Show_PatientLogin"] = SafeValue<bool>(dsuser.Tables[15].Rows[0]["PatientLogin"]);

                }

                DataTable dt = new DataAccessLayer.Appointment.clsAppointmentData().GetLocationForAppointment(UserId);
                if (dt != null)
                {
                    HttpContext.Current.Session["LocationList"] = (from p in dt.AsEnumerable()
                                                                   select new LocationForAppointment
                                                                   {
                                                                       locationId = SafeValue<int>(p["AddressInfoId"]),
                                                                       locationName = SafeValue<string>(p["Location"])
                                                                   }
                                    ).ToList();
                }
                var dtAppService = new DataAccessLayer.Appointment.clsAppointmentData().GetAppointmentServices(ParentUserId, 0);
                if (dtAppService != null)
                {
                    HttpContext.Current.Session["AppointmentServiceList"] = (from p in dtAppService.AsEnumerable()
                                                                             select new AppointmentService()
                                                                             {
                                                                                 AppointmentServiceId = SafeValue<int>(p["AppointmentServiceId"]),
                                                                                 ServiceType = SafeValue<string>(p["ServiceType"]),
                                                                                 TimeDuration = SafeValue<string>(p["TimeDuration"]),
                                                                                 Price = SafeValue<string>(p["Price"]),
                                                                                 DoctorId = SafeValue<int>(p["DoctorId"])

                                                                             }).ToList();
                }
                dt = new DataAccessLayer.Appointment.clsAppointmentData().GetInuranceNamesOfDoctorForAppointment(UserId);
                if (dt != null)
                {
                    HttpContext.Current.Session["InsuranceList"] = (from p in dt.AsEnumerable()
                                                                    select new InsuranceListForAppointment
                                                                    {
                                                                        InsuranceId = SafeValue<int>(p["InsuranceId"]),
                                                                        InsuranceName = SafeValue<string>(p["Name"])
                                                                    }
                                    ).ToList();
                }
                HttpContext.Current.Session["CompanyWebsite"] = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            }
        }

        public static bool IsFileOwnedByDoctor(int FileId, int UserId, BO.Enums.Common.FileFromFolder FileType, string FileName)
        {
            DataTable dt = null;

            dt = clsColleaguesData.IsFileOwnedByDoctor(FileId, UserId, FileType, FileName);

            return dt != null && dt.Rows.Count > 0;
        }

        public Person GetSocialMediaPerson(string email)
        {
            try
            {
                //connect to fliptop and pull the information down
                SocialMedia sm = new SocialMedia();
                Person person = sm.LookUpPersonClearBit(email);
                Person fullperson = sm.LookUpPersonfullcontact(email);
                person.FirstName = !string.IsNullOrEmpty(person.FirstName) ? person.FirstName : fullperson.FirstName;
                person.LastName = !string.IsNullOrEmpty(person.LastName) ? person.LastName : fullperson.LastName;
                person.FullName = !string.IsNullOrEmpty(person.FullName) ? person.FullName : fullperson.FullName;
                person.FacebookURL = !string.IsNullOrEmpty(person.FacebookURL) ? person.FacebookURL : fullperson.FacebookURL;
                person.LinkedInURL = !string.IsNullOrEmpty(person.LinkedInURL) ? person.LinkedInURL : fullperson.LinkedInURL;
                person.TwitterURL = !string.IsNullOrEmpty(person.TwitterURL) ? person.TwitterURL : fullperson.TwitterURL;
                person.ImageUrl = !string.IsNullOrEmpty(person.ImageUrl) ? person.ImageUrl : fullperson.ImageUrl;
                person.Description = !string.IsNullOrEmpty(fullperson.Description) ? fullperson.Description : person.Description;
                return person;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SaveFileFromBase64(string Base64Content, string filePath)
        {
            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(@"data:(.*?)\;base64,", System.Text.RegularExpressions.RegexOptions.IgnoreCase);

            Base64Content = reg.Replace(Base64Content, "");

            System.IO.File.WriteAllBytes(filePath, Convert.FromBase64String(Base64Content));

        }

        public int Insert_Temp_UploadedImages(int UserId, string Filename, string SessionId)
        {
            try
            {
                return SafeValue<int>(objCommon.Insert_Temp_UploadedImages(UserId, Filename, SessionId));
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("CommonBLL--Insert_Temp_UploadedImages", ex.Message, ex.StackTrace);
                throw;
            }

        }
        public int Insert_Temp_UploadedFiles(int UserId, string Filename, string SessionId)
        {
            try
            {
                return SafeValue<int>(objCommon.Insert_Temp_UploadedFiles(UserId, Filename, SessionId));
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("CommonBLL--Insert_Temp_UploadedFiles", ex.Message, ex.StackTrace);
                throw;
            }

        }

        public string GetUniquenumberForAttachments()
        {
            if (HttpContext.Current.Session["UniquenumberForAttachments"] == null || String.IsNullOrWhiteSpace(SafeValue<string>(HttpContext.Current.Session["UniquenumberForAttachments"])))
                HttpContext.Current.Session["UniquenumberForAttachments"] = SafeValue<string>(DateTime.Now.Ticks);

            return SafeValue<string>(HttpContext.Current.Session["UniquenumberForAttachments"]);

        }

        public int GetLocationIdOfDoctor(int UserId)
        {
            DataTable dt = new DataTable();
            dt = objCommon.GetLocationOfDoctor(UserId);
            if (dt != null && dt.Rows.Count > 0)
            {
                return SafeValue<int>(dt.Rows[0]["LocationId"]);

            }
            else
            {
                return 0;
            }
        }

        //public List<TempFileAttacheMents> GetTempAttachments(string FileIds, int MessageId = 0)
        //{
        //    DataTable dt = new DataTable();
        //    List<TempFileAttacheMents> lstattachments = new List<TempFileAttacheMents>();
        //    try
        //    {
        //        List<string> objImageId = new List<string>();
        //        List<string> objFileId = new List<string>();
        //        if (!string.IsNullOrWhiteSpace(FileIds) && MessageId == 0)
        //        {
        //            var Files = FileIds.Split(',');
        //            foreach (var item in Files)
        //            {
        //                string[] Attachments = item.Split('_');
        //                if (Attachments.Length > 0)
        //                {
        //                    switch (Attachments[0])
        //                    {
        //                        case "F":
        //                            objFileId.Add(Attachments[1]);
        //                            break;
        //                        case "I":
        //                            objImageId.Add(Attachments[1]);
        //                            break;

        //                    }
        //                }

        //            }
        //        }
        //        dt = objCommon.GetTempAttachments(string.Join(",", objImageId), string.Join(",", objFileId), MessageId);
        //        if (dt != null && dt.Rows.Count > 0)
        //        {
        //            lstattachments = (from p in dt.AsEnumerable()
        //                              select new TempFileAttacheMents
        //                              {
        //                                  FileId = SafeValue<int>(p["Id"]),
        //                                  FileFrom = SafeValue<int>(p["FileFrom"]),
        //                                  FileName = objCommon.CheckNull(SafeValue<string>(p["UserFileName"]), string.Empty) != string.Empty ? SafeValue<string>(p["UserFileName"]).Split('$').Last() : "",
        //                                  FilePath = objCommon.CheckNull(SafeValue<string>(p["UserFileName"]), string.Empty) != string.Empty ? SafeValue<string>(p["UserFileName"]) : "",
        //                                  FileExtension = SafeValue<int>(p["FileExtension"]),
        //                              }).ToList();

        //        }
        //        return lstattachments;
        //    }
        //    catch (Exception ex)
        //    {
        //        objCommon.InsertErrorLog("CommonBLL--GetTempAttachments", ex.Message, ex.StackTrace);
        //        throw;
        //    }
        //}

        public bool CheckFileIsValidImage(string FileName)
        {
            string FileExtension = Path.GetExtension(FileName).ToLower();
            return ApprovedImageFileExtension.Contains(FileExtension);
        }
        public bool Temp_AttachedFileDeleteById(int FileId, int FileFrom)
        {
            try
            {
                return SafeValue<bool>(objCommon.Temp_AttachedFileDeleteById(FileId, FileFrom));
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("CommonBLL--Temp_AttachedFileDeleteById", ex.Message, ex.StackTrace);
                throw;
            }

        }

        //public int Insert_Temp_UploadedImages(int UserId, string Filename, string SessionId)
        //{
        //    try
        //    {
        //        return SafeValue<int>(objCommon.Insert_Temp_UploadedImages(UserId, Filename, SessionId));
        //    }
        //    catch (Exception ex)
        //    {
        //        objCommon.InsertErrorLog("CommonBLL--Insert_Temp_UploadedImages", ex.Message, ex.StackTrace);
        //        throw;
        //    }

        //}
        //public int Insert_Temp_UploadedFiles(int UserId, string Filename, string SessionId)
        //{
        //    try
        //    {
        //        return SafeValue<int>(objCommon.Insert_Temp_UploadedFiles(UserId, Filename, SessionId));
        //    }
        //    catch (Exception ex)
        //    {
        //        objCommon.InsertErrorLog("CommonBLL--Insert_Temp_UploadedFiles", ex.Message, ex.StackTrace);
        //        throw;
        //    }

        //}
        public bool Temp_AttachedFileDeleteById(int FileId, int FileFrom, int ComposeType = 0)
        {
            try
            {
                return SafeValue<bool>(objCommon.Temp_AttachedFileDeleteById(FileId, FileFrom, ComposeType));
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("CommonBLL--Temp_AttachedFileDeleteById", ex.Message, ex.StackTrace);
                throw;
            }

        }


        //public string GetUniquenumberForAttachments()
        //{
        //    if (HttpContext.Current.Session["UniquenumberForAttachments"] == null || String.IsNullOrWhiteSpace(SafeValue<string>(HttpContext.Current.Session["UniquenumberForAttachments"])))
        //        HttpContext.Current.Session["UniquenumberForAttachments"] = SafeValue<string>(DateTime.Now.Ticks);

        //    return SafeValue<string>(HttpContext.Current.Session["UniquenumberForAttachments"]);

        //}

        public List<TempFileAttacheMents> GetTempAttachments(string FileIds, int MessageId = 0, int ComposeType = 0)
        {
            DataTable dt = new DataTable();
            List<TempFileAttacheMents> lstattachments = new List<TempFileAttacheMents>();
            try
            {
                List<string> objImageId = new List<string>();
                List<string> objFileId = new List<string>();
                if (!string.IsNullOrWhiteSpace(FileIds))
                {
                    var Files = FileIds.Split(',');
                    foreach (var item in Files)
                    {
                        string[] Attachments = item.Split('_');
                        if (Attachments.Length > 0)
                        {
                            switch (Attachments[0])
                            {
                                case "F":
                                    objFileId.Add(Attachments[1]);
                                    break;
                                case "I":
                                    objImageId.Add(Attachments[1]);
                                    break;

                            }
                        }

                    }
                }
                dt = objCommon.GetTempAttachments(string.Join(",", objImageId), string.Join(",", objFileId), MessageId, ComposeType);
                if (dt != null && dt.Rows.Count > 0)
                {
                    lstattachments = (from p in dt.AsEnumerable()
                                      select new TempFileAttacheMents
                                      {
                                          FileId = SafeValue<int>(p["Id"]),
                                          FileFrom = SafeValue<int>(p["FileFrom"]),
                                          FileName = SafeValue<string>(p["UserFileName"]) != string.Empty ? SafeValue<string>(p["UserFileName"]).Split('$').Last() : "",
                                          FilePath = SafeValue<string>(p["UserFileName"]) != string.Empty ? SafeValue<string>(p["UserFileName"]) : "",
                                          FileExtension = SafeValue<int>(p["FileExtension"]),
                                          ComposeType = ComposeType
                                      }).ToList();

                }
                return lstattachments;
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("CommonBLL--GetTempAttachments", ex.Message, ex.StackTrace);
                throw;
            }
        }
        public List<TempFileAttacheMents> GetForwardAttachMents(string FileIds, int MessageId = 0, int ComposeType = 0)
        {
            DataTable dt = new DataTable();
            List<TempFileAttacheMents> lstattachments = new List<TempFileAttacheMents>();
            try
            {
                List<string> objImageId = new List<string>();
                List<string> objFileId = new List<string>();
                if (!string.IsNullOrWhiteSpace(FileIds) && MessageId == 0)
                {
                    var Files = FileIds.Split(',');
                    foreach (var item in Files)
                    {
                        string[] Attachments = item.Split('_');
                        if (Attachments.Length > 0)
                        {
                            switch (Attachments[0])
                            {
                                case "F":
                                    objFileId.Add(Attachments[1]);
                                    break;
                                case "I":
                                    objImageId.Add(Attachments[1]);
                                    break;

                            }
                        }

                    }
                }
                dt = objCommon.GetForwardAttachments(string.Join(",", objImageId), string.Join(",", objFileId), MessageId, ComposeType);
                if (dt != null && dt.Rows.Count > 0)
                {
                    lstattachments = (from p in dt.AsEnumerable()
                                      select new TempFileAttacheMents
                                      {
                                          FileId = SafeValue<int>(p["Id"]),
                                          FileFrom = SafeValue<int>(p["FileFrom"]),
                                          FileName = SafeValue<string>(p["UserFileName"]) != string.Empty ? SafeValue<string>(p["UserFileName"]).Split('$').Last() : "",
                                          FilePath = SafeValue<string>(p["UserFileName"]) != string.Empty ? SafeValue<string>(p["UserFileName"]) : "",
                                          FileExtension = SafeValue<int>(p["FileExtension"]),
                                          ComposeType = ComposeType
                                      }).ToList();

                }
                return lstattachments;
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("CommonBLL--GetTempAttachments", ex.Message, ex.StackTrace);
                throw;
            }
        }
        public List<TempFileAttacheMents> GetForwardAttachMentsById(string FileIds = "", int ComposeType = 0)
        {
            DataTable dt = new DataTable();
            List<TempFileAttacheMents> lstattachments = new List<TempFileAttacheMents>();
            try
            {
                dt = objCommon.GetForwardAttachementDetailsById(FileIds, ComposeType);
                if (dt != null && dt.Rows.Count > 0)
                {
                    lstattachments = (from p in dt.AsEnumerable()
                                      select new TempFileAttacheMents
                                      {
                                          FileId = SafeValue<int>(p["Id"]),
                                          FileName = SafeValue<string>(p["AttachmentName"]) != string.Empty ? SafeValue<string>(p["AttachmentName"]).Split('$').Last() : "",
                                          FilePath = SafeValue<string>(p["AttachmentName"]) != string.Empty ? SafeValue<string>(p["AttachmentName"]) : "",
                                          ComposeType = ComposeType
                                      }).ToList();

                }
                return lstattachments;
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("CommonBLL--GetForwardAttachMentsById", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public List<TempFileAttacheMents> GetDraftAttachmentsByMultiMessage(string ColleagueMessageId, string PatientMessageId)
        {

            DataTable dt = new DataTable();
            List<TempFileAttacheMents> lstattachments = new List<TempFileAttacheMents>();
            try
            {
                dt = objCommon.GetDraftAttachmentsByMultiMessage(ColleagueMessageId, PatientMessageId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    lstattachments = (from p in dt.AsEnumerable()
                                      select new TempFileAttacheMents
                                      {
                                          FileId = SafeValue<int>(p["Id"]),
                                          FileFrom = SafeValue<int>(p["FileFrom"]),
                                          FileName = SafeValue<string>(p["UserFileName"]) != string.Empty ? SafeValue<string>(p["UserFileName"]).Split('$').Last() : "",
                                          FilePath = SafeValue<string>(p["UserFileName"]) != string.Empty ? SafeValue<string>(p["UserFileName"]) : "",
                                          FileExtension = SafeValue<int>(p["FileExtension"]),
                                          ComposeType = SafeValue<int>(p["ComposeType"]),
                                      }).ToList();

                }
                return lstattachments;
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("CommonBLL--GetDraftAttachmentsByMultiMessage", ex.Message, ex.StackTrace);
                throw;
            }


        }
        public TypesofMessage GetColleaguePAtientList(string MessageId)
        {

            TypesofMessage obj = new TypesofMessage();
            obj.ColleagueMessageId = new List<string>();
            obj.PatientMessageId = new List<string>();
            if (!string.IsNullOrWhiteSpace(MessageId))
            {
                var Files = MessageId.Split(',');
                foreach (var item in Files)
                {
                    string[] Attachments = item.Split('_');
                    if (Attachments.Length > 0)
                    {
                        switch (Attachments[1])
                        {
                            case "0":
                                obj.PatientMessageId.Add(Attachments[0]);
                                break;
                            case "1":
                                obj.ColleagueMessageId.Add(Attachments[0]);
                                break;

                        }
                    }

                }
            }
            return obj;
        }

        public bool DeleteAllAttachements(string FileIds, int MessageId = 0)
        {
            DataTable dt = new DataTable();
            List<TempFileAttacheMents> lstattachments = new List<TempFileAttacheMents>();
            bool Result = false;
            try
            {
                List<string> objImageId = new List<string>();
                List<string> objFileId = new List<string>();
                if (!string.IsNullOrWhiteSpace(FileIds) && MessageId == 0)
                {
                    var Files = FileIds.Split(',');
                    foreach (var item in Files)
                    {
                        string[] Attachments = item.Split('_');
                        if (Attachments.Length > 0)
                        {
                            switch (Attachments[0])
                            {
                                case "F":
                                    objFileId.Add(Attachments[1]);
                                    break;
                                case "I":
                                    objImageId.Add(Attachments[1]);
                                    break;

                            }
                        }

                    }
                }
                return Result = objCommon.DeleteAllAttachements(string.Join(",", objImageId), string.Join(",", objFileId), MessageId);

            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("CommonBLL--DeleteAllAttachements", ex.Message, ex.StackTrace);
                throw;
            }
        }

        public string ConvertToDate(DateTime date, int flag)
        {
            string dateformat = string.Empty;
            switch (flag)
            {
                case (int)BO.Enums.Common.DateFormat.GENERAL:
                    dateformat = "M/d/yyyy";
                    break;
                default:
                    dateformat = "M/d/yyyy";
                    break;
            }
            string Date = date.ToString(dateformat).Replace('-', '/');
            return Date;
        }

        //RM-294 below method added to add leading zero in date at AddPatient
        public string ConvertToDateNew(DateTime date, int flag)
        {
            string dateformat = string.Empty;
            switch (flag)
            {
                case (int)BO.Enums.Common.DateFormat.GENERAL:
                    dateformat = "MM/dd/yyyy";
                    break;
                default:
                    dateformat = "MM/dd/yyyy";
                    break;
            }
            string Date = date.ToString(dateformat).Replace('-', '/');
            return Date;
        }


        public string ConvertToDateTime(DateTime date, int flag)
        {
            string dateformat = string.Empty;
            switch (flag)
            {
                case (int)BO.Enums.Common.DateFormat.GENERAL:
                    dateformat = "M/d/yyyy h:mm tt";
                    break;
                default:
                    dateformat = "M/d/yyyy h:mm tt";
                    break;
            }
            string Date = date.ToString(dateformat).Replace('-', '/');
            return Date;
        }

        public string ConvertToTime(DateTime date, int flag)
        {
            string dateformat = string.Empty;
            switch (flag)
            {
                case (int)BO.Enums.Common.DateFormat.GENERAL:
                    dateformat = "h:mm tt";
                    break;
                default:
                    dateformat = "h:mm tt";
                    break;
            }
            string Date = date.ToString(dateformat);
            return Date;
        }

        public List<LocationHistory> GetLocationHistory()
        {
            DataTable dt = new DataTable();
            List<LocationHistory> lstLocationHistory = new List<LocationHistory>();
            try
            {
                dt = objCommon.GetLocationHistory();
                if (dt != null && dt.Rows.Count > 0)
                {
                    lstLocationHistory = (from p in dt.AsEnumerable()
                                          select new LocationHistory
                                          {
                                              City = SafeValue<string>(p["City"]),
                                              Counting = SafeValue<int>(p["Counting"]),
                                          }).ToList();

                }
                return lstLocationHistory;
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("CommonBLL--GetLocationHistory", ex.Message, ex.StackTrace);
                throw;
            }
        }
        public List<SpecialityHistory> GetSpecialityHistory()
        {
            DataTable dt = new DataTable();
            List<SpecialityHistory> lstSpecialityHistory = new List<SpecialityHistory>();
            try
            {
                dt = objCommon.GetSpecialityHistory();
                if (dt != null && dt.Rows.Count > 0)
                {
                    lstSpecialityHistory = (from p in dt.AsEnumerable()
                                            select new SpecialityHistory
                                            {
                                                Specialitity = SafeValue<string>(p["Specialities"]),
                                                Counting = SafeValue<int>(p["Counting"]),
                                                SpecialityId = SafeValue<int>(p["SpecialitiesId"])
                                            }).ToList();

                }
                return lstSpecialityHistory;
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("CommonBLL--GetSpecialityHistory", ex.Message, ex.StackTrace);
                throw;
            }
        }
        public List<MemberPlanDetail> GetUserPlanDetails(int UserId)
        {
            try
            {
                DataTable Dt = new DataTable();
                List<MemberPlanDetail> lst = new List<MemberPlanDetail>();
                Dt = objCommon.GetUserPlanDetails(UserId);
                foreach (DataRow item in Dt.Rows)
                {
                    lst.Add(new MemberPlanDetail()
                    {
                        FeatureId = SafeValue<int>(item["Feature_Id"]),
                        CanSee = SafeValue<string>(item["CanSee"]) == string.Empty ? false : SafeValue<bool>(item["CanSee"]),
                        MaxCount = SafeValue<string>(item["MaxCount"]),
                        MemberShipId = SafeValue<int>(item["MemberShipId"])
                    });
                }
                return lst;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("MembershipBLL--GetUserPlanDetails", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        public List<FooterCity> GetFooterCity()
        {
            DataTable dt = new DataTable();
            List<FooterCity> lstFooterCity = new List<FooterCity>();
            try
            {
                dt = objCommon.GetFooterCity();
                foreach (DataRow item in dt.Rows)
                {
                    lstFooterCity.Add(new FooterCity()
                    {
                        City = SafeValue<string>(item["CityName"]),
                        SortOrder = SafeValue<int>(item["SortOrder"])
                    });
                }
                return lstFooterCity;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("MembershipBLL--GetFooterCity", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool SendRequestForIluvMyDentist(int UserId, int PatientId, string PatientName, string PatientEmail)
        {
            try
            {
                clsTemplate objtemplate = new clsTemplate();
                bool Result = objtemplate.SendRequstMailtoPatient(UserId, PatientId, PatientName, PatientEmail);
                return Result;
            }
            catch (Exception Ex)
            {
                throw;
            }
        }
        public static bool SendFileUploadNotification(DataTable dt, int PatientId, int Userid)
        {
            bool Result = false;
            string PatientName = string.Empty;
            string PatientEmail = string.Empty;
            string DoctorEmail = string.Empty;
            string ToDoctorName = string.Empty;
            string DoctorName = string.Empty;
            string Password = string.Empty;
            TripleDESCryptoHelper cryptoHelper123 = new TripleDESCryptoHelper();
            clsPatientsData CLSPATIENTDATA = new clsPatientsData();
            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    DataSet ds = CLSPATIENTDATA.GetPateintDetailsByPatientId(PatientId);
                    if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
                    {
                        PatientName = SafeValue<string>(ds.Tables[0].Rows[0]["FirstName"] + " " + ds.Tables[0].Rows[0]["LastName"]);
                        PatientEmail =SafeValue<string>(ds.Tables[0].Rows[0]["Email"]);
                    }
                    foreach (DataRow item in dt.Rows)
                    {

                        DoctorEmail = SafeValue<string>(item["Username"]);
                        if (!string.IsNullOrEmpty(DoctorEmail))
                        {
                            DataTable dtcheck = new DataTable();
                            dtcheck = new clsColleaguesData().CheckEmailInSystem(DoctorEmail);
                            if (dtcheck != null && dtcheck.Rows.Count > 0)
                            {
                                Password = cryptoHelper123.encryptText(SafeValue<string>(dtcheck.Rows[0]["Password"]));
                            }
                        }
                        ToDoctorName = SafeValue<string>(item["ColleaguesName"]);
                        int UserId = SafeValue<int>(item["ColleaguesId"]);
                        string link = DoctorEmail + "||" + Password + "||" + UserId + "||" + PatientId + "||" + (int)BO.Enums.Common.RedirectFrom.PatientHistory;
                        string Encriptedstring = cryptoHelper123.encryptText(link);
                        if (UserId != Userid)
                        {
                            DataTable dts = new DataTable();
                            dts = new clsColleaguesData().GetDoctorDetailsbyId(Userid);
                            if (dts != null && dts.Rows.Count > 0)
                            {
                                DoctorName = SafeValue<string>(dts.Rows[0]["FirstName"] + " " + dts.Rows[0]["LastName"]);
                            }
                            clsTemplate.SendFileuploadNotification(ToDoctorName, PatientName, Encriptedstring, DoctorEmail, DoctorName);
                        }
                    }
                }
                return Result;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static string SaveBase64ImageonImageBank(string Base64string, string FileName, string path)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(Base64string))
                {
                    int width = 480;
                    var height = UInt16.MaxValue - 36;
                    byte[] bytes = Convert.FromBase64String(Base64string);
                    Image image;
                    image = new Bitmap(width, height);
                    using (MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length))
                    {
                        image = Image.FromStream(ms, true);
                    }
                    using (var imageFile = new FileStream(Path.Combine(path, FileName), FileMode.Create))
                    {
                        imageFile.Write(bytes, 0, bytes.Length);
                        imageFile.Flush();
                    }
                    return "https://qa.recordlinc.com/Imagebank/" + FileName;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        //public static bool AddPatientImageUsingBase64(PatientProfileImage profileImage)
        //{
        //    try
        //    {
        //        if (!string.IsNullOrWhiteSpace(profileImage.Base64URL))
        //        {
        //            Image image;
        //            byte[] bytes = Convert.FromBase64String(profileImage.Base64URL);
        //            using (MemoryStream ms = new MemoryStream(bytes))
        //            {
        //                image = Image.FromStream(ms);
        //            }
        //            if (!Directory.Exists(ConfigurationManager.AppSettings.Get("APITempFolder") + "/TempFolder"))
        //                System.IO.Directory.CreateDirectory(ConfigurationManager.AppSettings.Get("APITempFolder") + "/TempFolder");
        //            var imagecrop = new WebImage(bytes);
        //        }
                

        //    }
        //    catch (Exception Ex)
        //    {
        //        new clsCommon().InsertErrorLog("CommonBLL--AddPatientImageUsingBase64", Ex.Message, Ex.StackTrace);
        //        throw;
        //    }
        //}
        public static bool AddPointsAndExamCodes(int UserId, decimal points, int ExamCodes)
        {
            try
            {
                return clsColleaguesData.AddPointsAndExamCodes(UserId, points, ExamCodes);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("CommonBLL---AddPointsAndExamCodes", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static string GetPointsAndExamExists(int UserId)
        {
            try
            {
                return clsColleaguesData.GetPointsAndExamExists(UserId);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("CommonBLL---GetPointsAndExamExists", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool RegisterSuperBuck(int UserId)
        {
            try
            {
                return clsColleaguesData.RegisterSuperBuck(UserId);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("CommonBLL---AddPointsAndExamCodes", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static string StateScript(string countryId, string stateId)
        {
            if (countryId == "")
            {
                countryId = "US";
            }
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            clsCommon objCommon = new clsCommon();

            DataTable dt = objCommon.GetAllStateByCountryCode(countryId);
            foreach (DataRow item in dt.Rows)
            {
                lst.Add(new SelectListItem { Text = SafeValue<string>(item["StateName"]), Value = SafeValue<string>(item["StateCode"]) });
            }
            sb.Append("<select id=\"drpState\" name=\"drpState\" style=\"width:100%;\">");
            foreach (var item in lst)
            {
                if (stateId == item.Value)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
                else
                {
                    sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
            }
            sb.Append("</select>");
            return SafeValue<string>(sb);
        }

    }
}
