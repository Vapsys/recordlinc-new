﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Models;
using DataAccessLayer.Common;
using System.Data;
using System.Data.SqlClient;

namespace BusinessLogicLayer
{

   public class DemoBll
    {
        clsHelper helperobj = new clsHelper();
        clsCommon commonobj = new clsCommon();

        public DataSet Getmembers(int Value, DateTime? Date = null)
        {
            DataSet ds = new DataSet();
            List<Members> mlist = new List<Members>();
            ds = commonobj.getMembers(Value, Date);         
            return ds;
        }

        public DataSet getRewardAssocation(int AccountId)
        {
            DataSet ds = new DataSet();
            ds = commonobj.getRewardAssocation(AccountId);
            return ds;
        }

        public DataSet getLocationAssociation(int AccountId)
        {
            DataSet ds = new DataSet();
            ds = commonobj.getLocationAssociation(AccountId);
            return ds;
        }
        //Sup Account association
        public DataSet getSUPAccountAssociation(int AccountId,int? patientid)
        {
            DataSet ds = new DataSet();
            ds = commonobj.getSUPAccountAssociation(AccountId, patientid);
            return ds;
        }


    }
}
