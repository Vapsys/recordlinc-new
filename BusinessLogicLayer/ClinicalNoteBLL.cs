﻿using BO.Models;
using BO.ViewModel;
using DataAccessLayer.Common;
using DataAccessLayer.PatientsData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class ClinicalNotesBLL
    {
        static clsCommon Common = new clsCommon();
        clsClinicalNotesData data = new clsClinicalNotesData();
        clsCommon common = new clsCommon();
        public static bool CheckDentrixConnectorKeyExists(string DentrixConnectorKey)
        {
            try
            {
                DataTable dt = clsPatientsData.CheckDentrixConnectorExistOrNot(DentrixConnectorKey);
                if (dt != null && dt.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {

                throw;
            }
        }


        public List<ClinicalNote> Delete(List<ClinicalNote> model)
        {
            var data = new clsClinicalNotesData();
            try
            {
                List<ClinicalNote> DeletedList = new List<ClinicalNote>();
                if (CheckDentrixConnectorKeyExists(model[0].dentrixConnectorID))
                {
                    foreach (var item in model)
                    {
                        DeletedList.Add(new ClinicalNote()
                        {
                            DentrixId = item.DentrixId,
                            IsDeleted = data.Delete(item)
                        });
                    }
                }
                return DeletedList;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ClinicalNoteBLL---Delete", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        public List<ClinicalNote> List(ClinicalNoteFilter model)
        {
            var data = new clsClinicalNotesData();
            try
            {
                return data.List(model);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ClinicalNoteBLL---List", Ex.Message, Ex.StackTrace);
                throw;
            }

        }


        public static APIResponseBase<bool> Upload(List<ClinicalNote> models)
        {
            APIResponseBase<bool> response = new APIResponseBase<bool>();
            response.StatusCode = (int)HttpStatusCode.OK;
            response.IsSuccess = true;
            response.Message = "All patient insurance records uploaded successfully";
            clsClinicalNotesData data = new clsClinicalNotesData();
            if (CheckDentrixConnectorKeyExists(models[0].dentrixConnectorID))
            {
                models.ForEach(model =>
                {
                    Dictionary<ClinicalNote, string> failed = new Dictionary<ClinicalNote, string>();
                    try
                    {
                        data.Save(model);
                    }
                    catch (Exception Ex)
                    {
                        failed.Add(model, Ex.Message);
                        response.IsSuccess = false;
                        response.StatusCode = (int)HttpStatusCode.BadRequest;
                        response.Message = "Some of the clincal note records failed to save. please check the result for a list of failed clinical notes records";
                        // response.Result = failed;
                        Common.InsertErrorLog("ClinicalNoteBLL---Upload", Ex.Message, Ex.StackTrace);
                        throw;
                    }
                });
            }
            else
            {
                response.IsSuccess = false;
                response.StatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "invalid dentrix connector id";
            }

            return response;
        }
    }
}
