﻿using System;
using BO.ViewModel;
using System.Data;
using DataAccessLayer.Common;
using DataAccessLayer.ColleaguesData;
using BO.Models;
using DataAccessLayer.FlipTop;
using System.Web.Helpers;
using System.Web;
using DataAccessLayer.PatientsData;
using System.Text;
using System.Configuration;
using System.IO;
using System.Collections.Generic;
using BO.Enums;
using System.Linq;
using System.Drawing;
using static DataAccessLayer.Common.clsCommon;

namespace BusinessLogicLayer
{
    public class SendReferralBLL
    {
        clsCommon objCommon = new clsCommon();
        clsTemplate ObjTemplate = new clsTemplate();
        clsAdmin ObjAdmin = new clsAdmin();
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        clsPatientsData ObjPatientsData;
        int[] ReferralId = new int[2];
        int FromPublicProfilePatientId = 0;// we use this bcz first check in system if exsits then assign them other wise create new patient

        int FromPublicProfileDoctorId = 0;// we use this bcz first check in system if exsits then assign them other wise create new patient

        string FromPublicProfileDoctorFullName = string.Empty;
        string ToPublicProfileDoctorEmail = string.Empty;// here is email id of doctor whom public prfile is 
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();

        StringBuilder UploadedImagesIds = new StringBuilder();
        StringBuilder UploadedDocumentsIds = new StringBuilder();
        public static string TempFile = System.Configuration.ConfigurationManager.AppSettings["TempFileUpload"];
        public static string ImageBankFiles = System.Configuration.ConfigurationManager.AppSettings["ImageBankFileUpload"];
        public static string ThumbFiles = System.Configuration.ConfigurationManager.AppSettings["ThumbFileUpload"];
        public static string DraftFiles = System.Configuration.ConfigurationManager.AppSettings["DraftFileUpload"];


        public bool ProcessSendReferral(SendReferralModel model, int userid)
        {
            bool result = false;

            ToPublicProfileDoctorEmail = GetPublicProfileDoctorEmail(userid);
            GetSourceDoctorDetails(model, userid);
            return result;
        }
        public static bool ComposeReferral(Compose1Click model, int UserId)
        {
            try
            {
                bool Result = false;
                bool IsFromPatientEmail= false;
                TripleDESCryptoHelper obj = new TripleDESCryptoHelper();
                DentistProfileBLL objDp = new DentistProfileBLL();
                //This Function check the Doctor Already exists if yes then added as colleague if not then add on RL and added as colleague.
                IDictionary<int, string> data = ReferralTimeAddReferedBy(model._sendReferralModel.DoctorEmail, UserId, model._sendReferralModel.DoctorFirstName, model._sendReferralModel.DoctorLastName, model._sendReferralModel.DoctorPhone);
                int NewUserId = data.Keys.FirstOrDefault();
                string DoctorName = data.Values.FirstOrDefault();
                model._patientInfo.UserId = NewUserId;
                string DecryptedUserId = SafeValue<string>(UserId);//ReceiverId
                string EncUserId = obj.encryptText(DecryptedUserId);
                string DecryptedSenderId = SafeValue<string>(NewUserId);//SenderId                
                string EnSenderId = SafeValue<string>(NewUserId);                
                string EncReceiverId = SafeValue<string>(DecryptedUserId);
                //This Method Add new patient on Recordlinc side and return the PatientId.
                int PatientId = DentistProfileBLL.AddNewPatient(model, SafeValue<int>(DecryptedUserId));

                //Adding address detail of patient to addressinfo table.
                //XQ1-67
                model._contactInfo.PatientId = PatientId;
                //XQ1-229 : Adding code for updating email in addressinfo
                Result = DentistProfileBLL.AddPatientContactInfoFromOneclick(model._contactInfo, model._patientInfo.Phone,model._patientInfo.Email);

                //This Method Save the Patient Contact Details on Address-info table.
                if(model._contactInfo != null)
                {
                    //XQ1-229 : Adding code for updating email in addressinfo
                    Result = DentistProfileBLL.AddPatientContactInfoFromOneclick(model._contactInfo,model._patientInfo.Phone, model._patientInfo.Email);
                }

                //This Method save the xml for Insurance Coverage.
                if(model._insuranceCoverage != null)
                {
                    Result = objDp._AddInsuranceCoverage(model._insuranceCoverage, PatientId,model._contactInfo.EMGContactName,model._contactInfo.EMGContactNo,model);
                }

                //This Method save the xml for Medical History.
                if(model._medicalHistory != null)
                {
                    Result = objDp._AddMedicalHistory(model._medicalHistory, PatientId);
                }

                //This Method save the xml for Dental History.
                if(model._dentalhistory != null)
                {
                    Result = objDp._AddDentalHistory(model._dentalhistory, PatientId);
                }

                //This function save the images and documents of the referral.
                  IDictionary<string, string> response = UploadImagesAndDocuments(PatientId, UserId, model._sendReferralModel);
                string ImagesId = SafeValue<string>(response.Keys.FirstOrDefault());
                string DocIds = SafeValue<string>(response.Values.FirstOrDefault());

                //This Method add referral setting for that dentist and shown in Recordlinkc inbox.

                string PatientName = model._patientInfo.FirstName + " " + model._patientInfo.LastName;
                //bool res = objDp.InsertReferral("Patient Referral For"+ PatientName, "Patient Referral For"+ PatientName, 2, SafeValue<int>(model.ReceiverId), model._contactInfo.PatientId, null, null, 0, SafeValue<int>(DecryptedSenderId), model.XMLstring, true, model.LocationId);
                int intRes = objDp.InsertReferral("Patient Referral For " + PatientName, "Patient Referral For" + PatientName, 2, UserId, PatientId, null, null, 0, SafeValue<int>(DecryptedSenderId), model.XMLstring, false, model.LocationId,model.Comment, ImagesId, DocIds);
                //This Method send all mail users.
                ReferralFormBLL.SendOneClickreferralMail(PatientId, SafeValue<int>(DecryptedUserId), SafeValue<int>(NewUserId), intRes, IsFromPatientEmail);
                if (!IsFromPatientEmail)
                {
                    //This Method send Mail to perticuler dentist to whom the referral is sent
                    DataSet dsDoctor = new clsColleaguesData().GetDoctorDetailsById(SafeValue<int>(DecryptedUserId));
                    string DoctorPhone="";
                    string AccountName = "";
                    string Fullname = "";
                    if (dsDoctor != null && dsDoctor.Tables.Count > 0)
                    {
                        Fullname = SafeValue<string>(dsDoctor.Tables[0].Rows[0]["FullName"]);
                        DoctorPhone = SafeValue<string>(dsDoctor.Tables[2].Rows[0]["Phone"]);
                        AccountName = SafeValue<string>(dsDoctor.Tables[1].Rows[0]["AccountName"]);
                     AccountName = (string.IsNullOrWhiteSpace(AccountName)) ? "Dr." + Fullname : AccountName;
                    }

                    ReferralFormBLL.SentReferal(EnSenderId, model._patientInfo.Email, PatientId, EncReceiverId, model.XMLstring,model.Comment,model.LocationId, DoctorPhone, AccountName);
                }
                //Change for XQ1 - 1059
                bool res =false; 
                if (intRes > 0)
                {
                    res = true;
                }
                return res;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("SendReferralBLL--ComposeReferral", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        private void GetSourceDoctorDetails(SendReferralModel model, int userid)
        {
            DataTable dtDoctorCheck = new DataTable();
            string DoctorEmail = model.DoctorEmail;
            dtDoctorCheck = ObjColleaguesData.CheckEmailExistsAsDoctor(model.DoctorEmail);
            if (dtDoctorCheck != null && dtDoctorCheck.Rows.Count > 0)
            {
                FromPublicProfileDoctorId = SafeValue<int>(dtDoctorCheck.Rows[0]["UserId"]);
                FromPublicProfileDoctorFullName = SafeValue<string>(dtDoctorCheck.Rows[0]["FirstName"]) + " " + SafeValue<string>(dtDoctorCheck.Rows[0]["LastName"]);

                // Need to Check doctor which is exsits in system is collegaue of doctor whoes public profile is if not then we add them as collegaue because doctor can show only colleague doctor's referral
                bool result = false;
                result = CheckAddAsColleague(userid, FromPublicProfileDoctorId, DoctorEmail);
            }
            else
            {
                // code for signup doctor and set FromPublicProfileDoctorId 
                DataTable dt = new DataTable();
                dt = ObjAdmin.GetTempSignupByEmail(DoctorEmail);
                if (dt.Rows.Count > 0)
                {
                    if (SafeValue<string>(dt.Rows[0]["status"]) == "0")
                    {
                        string[] name = DoctorEmail.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
                        string Password = objCommon.CreateRandomPassword(8);
                        string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                        string Accountkey = Crypto.SHA1(SafeValue<string>(DateTime.Now.Ticks));
                        Person person = GetSocialMediaPerson(DoctorEmail);
                        int NewUserId = ObjColleaguesData.SingUpDoctor(DoctorEmail, EncPassword, model.DoctorFirstName, model.DoctorLastName, null, null, null, Accountkey, null, null, null, null, null, null, model.DoctorPhone, null, false, person.FacebookURL, person.LinkedInURL, person.TwitterURL, null, null, person.Description, null, null, null, null, null, null, null, 0, null, null, null, 0);
                        bool result1 = CheckAddAsColleague(userid, FromPublicProfileDoctorId, DoctorEmail);
                        if (NewUserId != 0)
                        {
                            bool result = ObjAdmin.UpdateStatusInTempTableUserById(NewUserId, 1);// update status 2 in temp table 
                            ObjTemplate.NewUserEmailFormat(DoctorEmail, SafeValue<string>(HttpContext.Current.Session["CompanyWebsite"]) + "/User/Index?UserName=" + DoctorEmail + "&Password=" + EncPassword, Password, SafeValue<string>(HttpContext.Current.Session["CompanyWebsite"]));
                            FromPublicProfileDoctorId = NewUserId;
                            FromPublicProfileDoctorFullName = model.DoctorFirstName + " " + model.DoctorLastName;
                            if (!string.IsNullOrEmpty(SafeValue<string>(person.ImageUrl)))
                            {
                                string makefilename = "$" + System.DateTime.Now.Ticks + "$" + (SafeValue<string>(FromPublicProfileDoctorFullName)) + ".png";
                                string rootPath = "~/DentistImages/";
                                string fileName = System.IO.Path.Combine(HttpContext.Current.Server.MapPath(rootPath), makefilename);
                                System.Drawing.Image image = objCommon.DownloadImageFromUrl(SafeValue<string>(person.ImageUrl));
                                bool IsUploadImage = ObjColleaguesData.UpdateDoctorProfileImageById(SafeValue<int>(NewUserId), makefilename);
                                image.Save(fileName);

                            }

                        }

                    }

                }
                else
                {
                    string[] name = DoctorEmail.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
                    string Password = objCommon.CreateRandomPassword(8);
                    string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                    string Accountkey = Crypto.SHA1(SafeValue<string>(DateTime.Now.Ticks));
                    Person person = GetSocialMediaPerson(DoctorEmail);
                    int NewUserId = ObjColleaguesData.SingUpDoctor(DoctorEmail, EncPassword, model.DoctorFirstName, model.DoctorLastName, null, null, null, Accountkey, null, null, null, null, null, null, model.DoctorPhone, null, false, person.FacebookURL, person.LinkedInURL, person.TwitterURL, null, null, person.Description, null, null, null, null, null, null, null, 0, null, null, null, 0);
                    if (NewUserId != 0)
                    {
                        bool result = ObjAdmin.UpdateStatusInTempTableUserById(NewUserId, 1);// update status 2 in temp table 
                        ObjTemplate.NewUserEmailFormat(DoctorEmail, SafeValue<string>(HttpContext.Current.Session["CompanyWebsite"]) + "/User/Index?UserName=" + DoctorEmail + "&Password=" + EncPassword, Password, SafeValue<string>(HttpContext.Current.Session["CompanyWebsite"]));
                        FromPublicProfileDoctorId = NewUserId;
                        bool result1 = CheckAddAsColleague(userid, FromPublicProfileDoctorId, DoctorEmail);
                        FromPublicProfileDoctorFullName = model.DoctorFirstName + " " + model.DoctorLastName;
                        if (!string.IsNullOrEmpty(SafeValue<string>(person.ImageUrl)))
                        {
                            string makefilename = "$" + System.DateTime.Now.Ticks + "$" + (SafeValue<string>(FromPublicProfileDoctorFullName)) + ".png";
                            string rootPath = "~/DentistImages/";
                            string fileName = System.IO.Path.Combine(HttpContext.Current.Server.MapPath(rootPath), makefilename);
                            System.Drawing.Image image = objCommon.DownloadImageFromUrl(SafeValue<string>(person.ImageUrl));
                            bool IsUploadImage = ObjColleaguesData.UpdateDoctorProfileImageById(SafeValue<int>(NewUserId), makefilename);
                            image.Save(fileName);
                        }
                    }
                }

            }

            #region Get Details of Patient

            ObjPatientsData = new clsPatientsData();
            DataTable dtPatient = new DataTable();
            bool EmailCheck = false;
            int InsertPatient = 0;
            EmailCheck = ObjPatientsData.CheckPatientEmail(model.PatientEmail);
            if (EmailCheck)
            {
                dtPatient = ObjPatientsData.GetPatientPassword(model.PatientEmail);
                if (dtPatient != null && dtPatient.Rows.Count > 0)
                {
                    FromPublicProfilePatientId = SafeValue<int>(dtPatient.Rows[0]["PatientId"]);
                }
            }
            else
            {
                // Code for create new patient
                string Password = objCommon.CreateRandomPassword(8);

                InsertPatient = ObjPatientsData.PatientInsert(0, null, model.PatientFirstName, "", model.PatientLastName, "", 0, null, model.PatientPhone, model.PatientEmail, null, null, null, null, null, userid, 0, null, Password, null);
                if (InsertPatient != 0)
                {
                    FromPublicProfilePatientId = InsertPatient;
                    //Session["PublicProfilePatientId"] = InsertPatient;
                    // Send Patient added notification to Doctor
                    ObjTemplate.PatientAddedNotification(userid,InsertPatient, model.PatientEmail, model.PatientFirstName, model.PatientLastName, model.PatientPhone, "", "", "", "", "", "");
                    int id = ObjPatientsData.Insert_PatientMessages(model.PatientFirstName, model.username, "Patient sign-up", "<a href=\"javascript:;\" onclick=\"getAttachedPatientHistory(" + InsertPatient + ")\">" + model.PatientFirstName + " " + model.PatientLastName + "</a> recently signed up as patient.", SafeValue<int>(userid), InsertPatient, false, false, true);
                    ObjTemplate.PatientSignUpEMail(model.PatientEmail, Password,userid);
                }
            }

            #endregion

            #region Get Uploaded Images and Document,First Move them temp folder to ImageBank and insert into database

            int newPatientId = FromPublicProfilePatientId;
            int RecordId = 0;
            int DoctorId = userid;

            int MontageId = 0;
            int Monatgetype = 0;
            int imageId = 0;
            int UploadImageId = 0;

            int ownerId = DoctorId;
            int ownertype = 0;

            List<string> objImageId = new List<string>();
            List<string> objFileId = new List<string>();
            if (!string.IsNullOrWhiteSpace(model.FileIds))
            {
                var Files = model.FileIds.Split(',');
                foreach (var item in Files)
                {
                    string[] Attachments = item.Split('_');
                    if (Attachments.Length > 0)
                    {
                        switch (Attachments[0])
                        {
                            case "F":
                                objFileId.Add(Attachments[1]);
                                break;
                            case "I":
                                objImageId.Add(Attachments[1]);
                                break;

                        }
                    }

                }
            }

            DataTable dtimageupload = objCommon.Get_UploadedFilesListByDoctor("", string.Join(",", objImageId));
            DataTable dtfileupload = objCommon.Get_UploadedFilesListByDoctor(string.Join(",", objFileId), "");
            DataTable dtPatientImages = ObjPatientsData.GetPatientImages(newPatientId, null);

            if (dtPatientImages.Rows.Count != 0)
            {
                MontageId = SafeValue<int>(dtPatientImages.Rows[0]["MontageId"]);

            }


            if (MontageId == 0)
            {

                string MontageName = newPatientId + "Montage";
                int montageid = SafeValue<int>(ObjPatientsData.AddMontage(MontageName, Monatgetype, newPatientId, ownerId, ownertype));

                foreach (DataRow item in dtimageupload.Rows)
                {

                    string pathfull = HttpContext.Current.Server.MapPath("~/" + SafeValue<string>(ConfigurationManager.AppSettings.Get("ImageBank")).Replace("../", ""));
                    pathfull = pathfull.Replace("\\Patients\\", "\\");
                    string fullpath = pathfull;
                    string filename = SafeValue<string>(item["UserFileName"]).Replace(ConfigurationManager.AppSettings.Get("ImageBank"), "");
                    int Imagetype;

                    Imagetype = 0;

                    int imageid = ObjPatientsData.AddImagetoPatient(newPatientId, 1, filename, 1000, 1000, 0, Imagetype, fullpath, SafeValue<string>(item["UserFileName"]), ownerId, ownertype, 1, "Doctor");
                    if (imageid > 0)
                    {

                        bool resmonimg = ObjPatientsData.AddImagetoMontage(imageid, Imagetype, montageid);
                    }

                    if (string.IsNullOrEmpty(SafeValue<string>(UploadedImagesIds)))
                    {
                        UploadedImagesIds.Append(SafeValue<string>(imageid));

                    }
                    else
                    {
                        UploadedImagesIds.Append("," + SafeValue<string>(imageid));
                    }

                }

                foreach (DataRow item in dtfileupload.Rows)
                {

                    RecordId = ObjPatientsData.UploadPatientDocument(montageid, SafeValue<string>(item["UserFileName"]), "Doctor",0,0);
                    if (string.IsNullOrEmpty(SafeValue<string>(UploadedDocumentsIds)))
                    {
                        UploadedDocumentsIds.Append(SafeValue<string>(RecordId));

                    }
                    else
                    {
                        UploadedDocumentsIds.Append("," + SafeValue<string>(RecordId));
                    }
                }
            }
            else if (MontageId != 0)
            {
                foreach (DataRow item in dtimageupload.Rows)
                {

                    if (UploadImageId == 0)
                    {

                        string pathfull = HttpContext.Current.Server.MapPath("~/" + SafeValue<string>(ConfigurationManager.AppSettings.Get("ImageBank")).Replace("../", ""));
                        pathfull = pathfull.Replace("\\Patients\\", "\\");
                        string fullpath = pathfull;
                        string imagename = SafeValue<string>(item["UserFileName"]).Replace(ConfigurationManager.AppSettings.Get("ImageBank"), "");
                        int Imagetype;

                        Imagetype = 0;

                        imageId = ObjPatientsData.AddImagetoPatient(newPatientId, 1, imagename, 1000, 1000, 0, Imagetype, fullpath, SafeValue<string>(item["UserFileName"]), ownerId, ownertype, 1, "Doctor");
                        if (imageId > 0)
                        {
                            bool resmonimg = ObjPatientsData.AddImagetoMontage(imageId, Imagetype, MontageId);

                        }

                        if (string.IsNullOrEmpty(SafeValue<string>(UploadedImagesIds)))
                        {
                            UploadedImagesIds.Append(SafeValue<string>(imageId));

                        }
                        else
                        {
                            UploadedImagesIds.Append("," + SafeValue<string>(imageId));
                        }

                    }


                }

                foreach (DataRow item in dtfileupload.Rows)
                {

                    RecordId = ObjPatientsData.UploadPatientDocument(MontageId, SafeValue<string>(item["UserFileName"]), "Doctor",0,0);
                    if (string.IsNullOrEmpty(SafeValue<string>(UploadedDocumentsIds)))
                    {
                        UploadedDocumentsIds.Append(SafeValue<string>(RecordId));

                    }
                    else
                    {
                        UploadedDocumentsIds.Append("," + SafeValue<string>(RecordId));
                    }
                }

            }




            DataTable dtTempFiles = objCommon.Get_UploadedFilesListByDoctor(string.Join(",", objFileId), string.Join(",", objImageId));


            if (dtTempFiles != null && dtTempFiles.Rows.Count > 0)
            {
                foreach (DataRow item in dtTempFiles.Rows)
                {

                    string DocName = SafeValue<string>(item["UserFileName"]);


                    //string Extension = Path.GetExtension(DocName);
                    DocName = SafeValue<string>(item["UserFileName"]);
                    DocName = DocName.Replace("../TempUploads/", "");

                    string sourceFile = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings.Get("TempUploadsFolderPath")) + DocName;

                    sourceFile = sourceFile.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");
                    string tempPath = ConfigurationManager.AppSettings["ImageBank"];
                    string destinationFile = HttpContext.Current.Server.MapPath("~/" + tempPath.Replace("../", ""));
                    destinationFile = destinationFile.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");
                    string DestinationFilePath = destinationFile;
                    destinationFile = destinationFile + DocName;

                    if (System.IO.File.Exists(sourceFile))
                    {
                        System.IO.File.Copy(sourceFile, destinationFile);

                        if (new CommonBLL().CheckFileIsValidImage(DocName))
                        {
                            string ThubimagePath = HttpContext.Current.Server.MapPath("~/" + ConfigurationManager.AppSettings["FolderPathThumb"]);

                            objCommon.GenerateThumbnailImage(DestinationFilePath, DocName, ThubimagePath);
                        }

                    }

                }





                // Delete from temp folder

                foreach (DataRow item in dtTempFiles.Rows)
                {
                    string DocName = SafeValue<string>(item["UserFileName"]);

                    // string Extension = Path.GetExtension(DocName);
                    DocName = SafeValue<string>(item["UserFileName"]);
                    DocName = DocName.Replace("../TempUploads/", "");

                    string sourceFile = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings.Get("TempUploadsFolderPath")) + DocName;

                    sourceFile = sourceFile.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");
                    string tempPath = ConfigurationManager.AppSettings["ImageBank"];
                    string destinationFile = HttpContext.Current.Server.MapPath("~/" + tempPath.Replace("../", ""));
                    //string tempPath = System.Configuration.ConfigurationManager.AppSettings["ImageBank"];
                    //string destinationFile = HttpContext.Current.Server.MapPath(tempPath);
                    destinationFile = destinationFile.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");
                    string DestinationFilePath = destinationFile;
                    destinationFile = destinationFile + DocName;

                    if (File.Exists(sourceFile))
                    {
                        File.Delete(sourceFile);
                    }
                }




            }

            // objCommon.Temp_RemoveAllByUserId(ownerId, clsCommon.GetUniquenumberForAttachments());
            #endregion

            #region Send Referral
            ReferralId = ObjColleaguesData.InsertReferral(FromPublicProfileDoctorFullName, model.PatientFirstName + " " + model.PatientLastName, "Referral Card", "Refer Patient", 2, FromPublicProfileDoctorId, FromPublicProfilePatientId, null, null, 0, userid, model.Comments, null, null, null, SafeValue<string>(UploadedImagesIds), SafeValue<string>(UploadedDocumentsIds), null, string.Empty);
            ObjColleaguesData.AddPatientToReferredDoctor(userid, InsertPatient);
            if (ReferralId[0] > 0)
            {
                ObjTemplate.SendPatientReferral(FromPublicProfileDoctorId, ToPublicProfileDoctorEmail, model.DoctorFirstName + " " + model.DoctorLastName, model.Comments, ReferralId[0], ReferralId[1], 2);
            }
            #endregion

        }

        /// <summary>
        /// Ankit here 06-13-2018 Added this function for sending the notification to the patient and insert a data on database of referrals.
        /// </summary>
        /// <param name="ReferredToUserId"></param>
        /// <param name="ReferredByUserId"></param>
        /// <param name="model"></param>
        /// <param name="ImagesId"></param>
        /// <param name="DocIds"></param>
        /// <returns></returns>
        public static int[] InsertReferralWithNotification(string ReferredToUserId,string ReferredByUserId,Compose1Click model,string ImagesId, string DocIds,string DoctorsName)
        {
            try
            {
                #region Local Variable Declaration
                TripleDESCryptoHelper obj = new TripleDESCryptoHelper();
                clsTemplate template = new clsTemplate();
                DentistProfileBLL objDp = new DentistProfileBLL();
                #endregion
                string EncrUserId = obj.encryptText(SafeValue<string>(ReferredToUserId));
                string DecryptedUserId =ReferredToUserId;
                string DoctorName = clsPatientsData.GetDoctorNameFormId(SafeValue<int>(DecryptedUserId));
                string EncrPatientId = obj.encryptText(SafeValue<string>(model._contactInfo.PatientId));
                string EncrptedString = HttpUtility.UrlEncode(EncrPatientId + '|' + EncrUserId);
                //string DecryptedSenderId = ReferredByUserId;
                string Urllink = ConfigurationManager.AppSettings["OneClickreferralURL"] + "AccountSettings/GetPatientReferralForm?TYHJNABGA=" + EncrptedString;
                
                //Changes made to pass phone number to Save Referral Form Setting Mail.
                DataSet dsDoctor = new clsColleaguesData().GetDoctorDetailsById(SafeValue<int>(DecryptedUserId));
                string DoctorPhone = "";
                if (dsDoctor != null && dsDoctor.Tables.Count > 0)
                {
                    DoctorPhone = SafeValue<string>(dsDoctor.Tables[2].Rows[0]["Phone"]);
                }
                
                template.SendMailToPatientForRefferalSettings(model._patientInfo.Email, DoctorName, Urllink,0,DoctorPhone);
                int[] ReferralId = new clsColleaguesData().InsertReferral(DoctorsName, model._patientInfo.FirstName+ " " + model._patientInfo.LastName, "Referral Card", "Refer Patient", 2, SafeValue<int>(ReferredByUserId), model._contactInfo.PatientId, null, null, 0, SafeValue<int>(ReferredToUserId), model._sendReferralModel.Comments, null, null, null, ImagesId,DocIds, null,model.XMLstring);
                return ReferralId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("SendREferralBLL---InsertReferralWithNotification", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Ankit here 06-13-2018: This Function works for mainly 2 purpose
        /// 1) Insert an Images and save it on folder.
        /// 2) Same as a document Insert and save.
        /// </summary>
        /// <param name="newPatientId"></param>
        /// <param name="DoctorId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public static IDictionary<string,string> UploadImagesAndDocuments(int newPatientId, int DoctorId,SendReferralModel model)
        {
            try
            {
                #region Get Uploaded Images and Document,First Move them temp folder to ImageBank and insert into database
                List<string> objImageId = new List<string>();
                List<string> objFileId = new List<string>();
                var Obj = new Dictionary<string, string>();
                string NewFileName = string.Empty;
                clsCommon objCommon = new clsCommon();
                StringBuilder UploadedImagesIds = new StringBuilder();
                StringBuilder UploadedDocumentsIds = new StringBuilder();
                if (!string.IsNullOrWhiteSpace(model.FileIds))
                {
                    var Files = model.FileIds.Split(',');
                    foreach (var item in Files)
                    {
                        string[] Attachments = item.Split('_');
                        if (Attachments.Length > 0)
                        {
                            switch (Attachments[0])
                            {
                                case "F":
                                    objFileId.Add(Attachments[1]);
                                    break;
                                case "I":
                                    objImageId.Add(Attachments[1]);
                                    break;

                            }
                        }

                    }
                }
                DataTable dtTempUploadedDocs = objCommon.Get_UploadedFilesListByDoctor(string.Join(",", objFileId), string.Join(",", objImageId));
                if (dtTempUploadedDocs != null && dtTempUploadedDocs.Rows.Count > 0)
                {

                    foreach (DataRow itemTempUploadedDocs in dtTempUploadedDocs.Rows)
                    {
                        clsPatientsData ObjPatientsData = new clsPatientsData();
                        string DocName = SafeValue<string>(itemTempUploadedDocs["UserFileName"]);
                        NewFileName = SafeValue<string>("$" + System.DateTime.Now.Ticks + "$" + DocName.Substring(SafeValue<string>(DocName).LastIndexOf("$") + 1));
                        string Extension = Path.GetExtension(NewFileName);
                        string sourceFile = TempFile + DocName;
                        string destinationFile = ImageBankFiles + NewFileName;
                        System.IO.File.Copy(sourceFile, destinationFile);
                        if (Extension.ToLower() == ".jpg" || Extension.ToLower() == ".jpeg" || Extension.ToLower() == ".gif" || Extension.ToLower() == ".png" || Extension.ToLower() == ".bmp" || Extension.ToLower() == ".psd" || Extension.ToLower() == ".pspimage" || Extension.ToLower() == ".thm" || Extension.ToLower() == ".tif")
                        {
                            if (!Directory.Exists(ThumbFiles))
                            {
                                Directory.CreateDirectory(ThumbFiles);
                                //SetPermissions(ThumbFiles);
                            }

                            // Load image.
                            Image image = Image.FromFile(destinationFile);

                            // Compute thumbnail size.
                            Size thumbnailSize = objCommon.GetThumbnailSize(image);

                            // Get thumbnail.
                            Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                                thumbnailSize.Height, null, IntPtr.Zero);

                            // Save thumbnail.
                            thumbnail.Save(ThumbFiles + NewFileName);

                            // Add the data in image table
                            // In referral Result : 0- Image, 1- Document
                            int imageid = ObjPatientsData.AddImagetoPatient(newPatientId, 1, NewFileName, 1000, 1000, 0, 0, ImageBankFiles, NewFileName, DoctorId, 0, 1, "Doctor");
                            if (string.IsNullOrEmpty(SafeValue<string>(UploadedImagesIds)))
                            {
                                UploadedImagesIds.Append(SafeValue<string>(imageid));

                            }
                            else
                            {
                                UploadedImagesIds.Append("," + SafeValue<string>(imageid));
                            }
                        }
                        else
                        {
                            int RecordId = ObjPatientsData.UploadPatientDocument(0, NewFileName, "Doctor", newPatientId, DoctorId);
                            if (string.IsNullOrEmpty(SafeValue<string>(UploadedDocumentsIds)))
                            {
                                UploadedDocumentsIds.Append(SafeValue<string>(RecordId));

                            }
                            else
                            {
                                UploadedDocumentsIds.Append("," + SafeValue<string>(RecordId));
                            }
                        }
                        if (System.IO.File.Exists(sourceFile))
                        {
                            System.IO.File.Delete(sourceFile);
                        }
                    }
                }

                bool Result = objCommon.Temp_RemoveAllByUserId(DoctorId, clsCommon.GetUniquenumberForAttachments()); // For Remove All Uplaoded Temp Images and Files
                Obj.Add(SafeValue<string>(UploadedImagesIds),SafeValue<string>(UploadedDocumentsIds));
                return Obj;

                //#region Local Variable Declaration
                //int RecordId = 0;
                //int MontageId = 0;
                //int Monatgetype = 0;
                //int imageId = 0;
                //int UploadImageId = 0;
                //int ownerId = DoctorId;
                //int ownertype = 0;
                //clsCommon objCommon = new clsCommon();
                //clsPatientsData ObjPatientsData = new clsPatientsData();
                //StringBuilder UploadedImagesIds = new StringBuilder();
                //StringBuilder UploadedDocumentsIds = new StringBuilder();
                //List<string> objImageId = new List<string>();
                //List<string> objFileId = new List<string>();
                //var Obj = new Dictionary<string, string>();
                //#endregion

                //if (!string.IsNullOrWhiteSpace(model.FileIds))
                //{
                //    var Files = model.FileIds.Split(',');
                //    foreach (var item in Files)
                //    {
                //        string[] Attachments = item.Split('_');
                //        if (Attachments.Length > 0)
                //        {
                //            switch (Attachments[0])
                //            {
                //                case "F":
                //                    objFileId.Add(Attachments[1]);
                //                    break;
                //                case "I":
                //                    objImageId.Add(Attachments[1]);
                //                    break;

                //            }
                //        }

                //    }
                //}

                //DataTable dtimageupload = objCommon.Get_UploadedFilesListByDoctor("", string.Join(",", objImageId));
                //DataTable dtfileupload = objCommon.Get_UploadedFilesListByDoctor(string.Join(",", objFileId), "");
                //DataTable dtPatientImages = ObjPatientsData.GetPatientImages(newPatientId, null);

                //if (dtPatientImages.Rows.Count != 0)
                //{
                //    MontageId = SafeValue<int>(dtPatientImages.Rows[0]["MontageId"]);

                //}


                //if (MontageId == 0)
                //{

                //    string MontageName = newPatientId + "Montage";
                //    int montageid = SafeValue<int>(ObjPatientsData.AddMontage(MontageName, Monatgetype, newPatientId, ownerId, ownertype));

                //    foreach (DataRow item in dtimageupload.Rows)
                //    {
                //        string pathfull = ConfigurationManager.AppSettings.Get("ImageBankFullPath");                        
                //        DataAccessLayer.AuthenticationPR objAuth = new DataAccessLayer.AuthenticationPR();
                //        objAuth.WriteLog("Upload file pathfull - 1:- " + pathfull, true);
                //        //string pathfull = HttpContext.Current.Server.MapPath("~/" + ConfigurationManager.AppSettings.Get("ImageBank").ToString().Replace("../", ""));                        
                //        pathfull = pathfull.Replace("\\Patients\\", "\\");
                //        string fullpath = pathfull;
                //        string filename = SafeValue<string>(item["UserFileName"]).Replace(ConfigurationManager.AppSettings.Get("ImageBank"), "");
                //        int Imagetype;

                //        Imagetype = 0;

                //        int imageid = ObjPatientsData.AddImagetoPatient(newPatientId, 1, filename, 1000, 1000, 0, Imagetype, fullpath, item["UserFileName"].ToString(), ownerId, ownertype, 1, "Doctor");
                //        //if (imageid > 0)
                //        //{

                //        //    bool resmonimg = ObjPatientsData.AddImagetoMontage(imageid, Imagetype, montageid);
                //        //}

                //        if (string.IsNullOrEmpty(UploadedImagesIds.ToString()))
                //        {
                //            UploadedImagesIds.Append(SafeValue<string>(imageid));

                //        }
                //        else
                //        {
                //            UploadedImagesIds.Append("," + SafeValue<string>(imageid));
                //        }

                //    }

                //    foreach (DataRow item in dtfileupload.Rows)
                //    {

                //        RecordId = ObjPatientsData.UploadPatientDocument(montageid, item["UserFileName"].ToString(), "Doctor", 0, 0);
                //        if (string.IsNullOrEmpty(UploadedDocumentsIds.ToString()))
                //        {
                //            UploadedDocumentsIds.Append(SafeValue<string>(RecordId));

                //        }
                //        else
                //        {
                //            UploadedDocumentsIds.Append("," + SafeValue<string>(RecordId));
                //        }
                //    }
                //}
                //else if (MontageId != 0)
                //{
                //    foreach (DataRow item in dtimageupload.Rows)
                //    {

                //        if (UploadImageId == 0)
                //        {

                //            string pathfull = ConfigurationManager.AppSettings.Get("ImageBankFullPath");
                //            DataAccessLayer.AuthenticationPR objAuth = new DataAccessLayer.AuthenticationPR();
                //            objAuth.WriteLog("Upload file pathfull - 2(Images):- " + pathfull, true);
                //            //string pathfull = HttpContext.Current.Server.MapPath("~/" + ConfigurationManager.AppSettings.Get("ImageBank").ToString().Replace("../", ""));                            
                //            pathfull = pathfull.Replace("\\Patients\\", "\\");
                //            string fullpath = pathfull;
                //            string imagename = SafeValue<string>(item["UserFileName"]).Replace(ConfigurationManager.AppSettings.Get("ImageBank"), "");
                //            int Imagetype;

                //            Imagetype = 0;

                //            imageId = ObjPatientsData.AddImagetoPatient(newPatientId, 1, imagename, 1000, 1000, 0, Imagetype, fullpath, item["UserFileName"].ToString(), ownerId, ownertype, 1, "Doctor");
                //            if (imageId > 0)
                //            {
                //                bool resmonimg = ObjPatientsData.AddImagetoMontage(imageId, Imagetype, MontageId);

                //            }

                //            if (string.IsNullOrEmpty(UploadedImagesIds.ToString()))
                //            {
                //                UploadedImagesIds.Append(SafeValue<string>(imageId));

                //            }
                //            else
                //            {
                //                UploadedImagesIds.Append("," + SafeValue<string>(imageId));
                //            }

                //        }


                //    }

                //    foreach (DataRow item in dtfileupload.Rows)
                //    {

                //        RecordId = ObjPatientsData.UploadPatientDocument(MontageId, item["UserFileName"].ToString(), "Doctor", 0, 0);
                //        if (string.IsNullOrEmpty(UploadedDocumentsIds.ToString()))
                //        {
                //            UploadedDocumentsIds.Append(SafeValue<string>(RecordId));

                //        }
                //        else
                //        {
                //            UploadedDocumentsIds.Append("," + SafeValue<string>(RecordId));
                //        }
                //    }

                //}

                //DataTable dtTempFiles = objCommon.Get_UploadedFilesListByDoctor(string.Join(",", objFileId), string.Join(",", objImageId));

                //if (dtTempFiles != null && dtTempFiles.Rows.Count > 0)
                //{
                //    foreach (DataRow item in dtTempFiles.Rows)
                //    {

                //        string DocName = SafeValue<string>(item["UserFileName"]);


                //        //string Extension = Path.GetExtension(DocName);
                //        DocName = SafeValue<string>(item["UserFileName"]);
                //        DocName = DocName.Replace("../TempUploads/", "");

                //        string sourceFile = Path.Combine(ConfigurationManager.AppSettings.Get("TempFolderFullPath"), DocName);
                //        DataAccessLayer.AuthenticationPR objAuth = new DataAccessLayer.AuthenticationPR();
                //        objAuth.WriteLog("Upload file sourceFile - 1:- " + sourceFile, true);
                //        //string sourceFile = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings.Get("TempUploadsFolderPath")) + DocName;                        

                //        sourceFile = sourceFile.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");

                //        //string tempPath = ConfigurationManager.AppSettings["ImageBank"];
                //        string destinationFile = ConfigurationManager.AppSettings.Get("ImageBankFullPath");                        
                //        objAuth.WriteLog("Upload file destinationFile - 1:- " + destinationFile, true);
                //        //string destinationFile = HttpContext.Current.Server.MapPath("~/" + tempPath.Replace("../", ""));                        

                //        destinationFile = destinationFile.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");

                //        string DestinationFilePath = destinationFile;
                //        destinationFile = Path.Combine(destinationFile,DocName);

                //        if (System.IO.File.Exists(sourceFile))
                //        {
                //            System.IO.File.Copy(sourceFile, destinationFile);

                //            if (new CommonBLL().CheckFileIsValidImage(DocName))
                //            {
                //                string ThumbimagePath = ConfigurationManager.AppSettings.Get("ThumbFolderFullPath");
                //                objAuth.WriteLog("Upload file ThumbimagePath - 1:- " + ThumbimagePath, true);
                //                //string FolderThumbPath = ConfigurationManager.AppSettings["FolderPathThumb"];// "ImageBank/Thumb";
                //                //string ThubimagePath = HttpContext.Current.Server.MapPath("~/" + FolderThumbPath);                                
                //                objCommon.GenerateThumbnailImage(DestinationFilePath, DocName, ThumbimagePath);
                //            }
                //        }
                //    }
                //    // Delete from temp folder
                //    foreach (DataRow item in dtTempFiles.Rows)
                //    {
                //        string DocName = SafeValue<string>(item["UserFileName"]);

                //        // string Extension = Path.GetExtension(DocName);
                //        DocName = SafeValue<string>(item["UserFileName"]);
                //        DocName = DocName.Replace("../TempUploads/", "");

                //        string sourceFile = ConfigurationManager.AppSettings.Get("TempFolderFullPath") + DocName;
                //        DataAccessLayer.AuthenticationPR objAuth = new DataAccessLayer.AuthenticationPR();
                //        objAuth.WriteLog("Upload file sourceFile - 2:- " + sourceFile, true);

                //        sourceFile = sourceFile.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");

                //        //string tempPath = ConfigurationManager.AppSettings["ImageBank"];
                //        //string destinationFile = HttpContext.Current.Server.MapPath("~/" + tempPath.Replace("../", ""));
                //        string destinationFile = ConfigurationManager.AppSettings.Get("ImageBankFullPath");
                //        objAuth.WriteLog("Upload file destinationFile - 2:- " + destinationFile, true);

                //        destinationFile = destinationFile.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");

                //        string DestinationFilePath = destinationFile;
                //        destinationFile = destinationFile + DocName;

                //        if (File.Exists(sourceFile))
                //        {
                //            File.Delete(sourceFile);
                //        }
                //    }
                //}
                //Obj.Add(UploadedImagesIds.ToString(), UploadedDocumentsIds.ToString());
                //return Obj;
                // objCommon.Temp_RemoveAllByUserId(ownerId, clsCommon.GetUniquenumberForAttachments());
                #endregion
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("SendREferralBLL---UploadImagesAndDocuments", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Ankit Here 06-13-2018: This Function works For mainly 2 purpose.
        /// 1) Referred By Is already on the system then added as colleagues
        /// 2) If Already not Exists then Add this dentist as sign up and sending mail of it as well added as colleagues of that public profile doctor
        /// 3) This method return that referred by userids.
        /// </summary>
        /// <param name="EmailAddress"></param>
        /// <returns></returns>
        public static IDictionary<int,string> ReferralTimeAddReferedBy(string EmailAddress, int Userid, string DentistFirstName, string DentistLastName, string Phone)
        {
            try
            {
                #region Local variable Declaration 
                int FromPublicProfileDoctorId = 0;// we use this bcz first check in system if exsits then assign them other wise create new patient
                int NewUserId = 0;
                clsCommon objCommon = new clsCommon();
                clsColleaguesData ObjColleaguesData = new clsColleaguesData();
                clsAdmin ObjAdmin = new clsAdmin();
                clsTemplate ObjTemplate = new clsTemplate();
                TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
                string FromPublicProfileDoctorFullName = string.Empty;
                string ToPublicProfileDoctorEmail = string.Empty;
                DataTable dtDoctorCheck = new DataTable();
                var Obj = new Dictionary<int, string>();
                #endregion

                string DoctorEmail = EmailAddress;
                dtDoctorCheck = new clsColleaguesData().CheckEmailExistsAsDoctor(EmailAddress);
                if (dtDoctorCheck != null && dtDoctorCheck.Rows.Count > 0)
                {
                    NewUserId = SafeValue<int>(dtDoctorCheck.Rows[0]["UserId"]);
                    FromPublicProfileDoctorFullName = SafeValue<string>(dtDoctorCheck.Rows[0]["FirstName"]) + " " + SafeValue<string>(dtDoctorCheck.Rows[0]["LastName"]);

                    // Need to Check doctor which is exsits in system is collegaue of doctor whoes public profile is if not then we add them as collegaue because doctor can show only colleague doctor's referral
                    bool result = false;
                    result = new SendReferralBLL().CheckAddAsColleague(Userid, FromPublicProfileDoctorId, DoctorEmail);
                }
                else
                {
                    // code for signup doctor and set FromPublicProfileDoctorId 
                    DataTable dt = new DataTable();
                    dt = new clsAdmin().GetTempSignupByEmail(DoctorEmail);
                    if (dt.Rows.Count > 0)
                    {
                        if (SafeValue<string>(dt.Rows[0]["status"]) == "0")
                        {
                            string[] name = DoctorEmail.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
                            string Password = objCommon.CreateRandomPassword(8);
                            string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                            string Accountkey = Crypto.SHA1(SafeValue<string>(DateTime.Now.Ticks));
                            Person person = new SendReferralBLL().GetSocialMediaPerson(DoctorEmail);
                            NewUserId = ObjColleaguesData.SingUpDoctor(DoctorEmail, EncPassword, DentistFirstName, DentistLastName, null, null, null, Accountkey, null, null, null, null, null, null, Phone, null, false, person.FacebookURL, person.LinkedInURL, person.TwitterURL, null, null, person.Description, null, null, null, null, null, null, null, 0, null, null, null, 0);
                            bool result1 = new SendReferralBLL().CheckAddAsColleague(Userid, FromPublicProfileDoctorId, DoctorEmail);
                            if (NewUserId != 0)
                            {
                                bool result = ObjAdmin.UpdateStatusInTempTableUserById(NewUserId, 1);// update status 2 in temp table 
                                ObjTemplate.NewUserEmailFormat(DoctorEmail, SafeValue<string>(HttpContext.Current.Session["CompanyWebsite"]) + "/User/Index?UserName=" + DoctorEmail + "&Password=" + EncPassword, Password, SafeValue<string>(HttpContext.Current.Session["CompanyWebsite"]));
                                FromPublicProfileDoctorId = NewUserId;
                                FromPublicProfileDoctorFullName = DentistFirstName + " " + DentistLastName;
                                if (!string.IsNullOrEmpty(SafeValue<string>(person.ImageUrl)))
                                {
                                    string makefilename = "$" + System.DateTime.Now.Ticks + "$" + SafeValue<string>(FromPublicProfileDoctorFullName) + ".png";
                                    string rootPath = "~/DentistImages/";
                                    string fileName = System.IO.Path.Combine(HttpContext.Current.Server.MapPath(rootPath), makefilename);
                                    System.Drawing.Image image = objCommon.DownloadImageFromUrl(SafeValue<string>(person.ImageUrl));
                                    bool IsUploadImage = ObjColleaguesData.UpdateDoctorProfileImageById(SafeValue<int>(NewUserId), makefilename);
                                    image.Save(fileName);

                                }
                            }
                        }
                    }
                    else
                    {
                        string[] name = DoctorEmail.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
                        string Password = objCommon.CreateRandomPassword(8);
                        string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                        string Accountkey = Crypto.SHA1(SafeValue<string>(DateTime.Now.Ticks));
                        Person person = new SendReferralBLL().GetSocialMediaPerson(DoctorEmail);
                        NewUserId = ObjColleaguesData.SingUpDoctor(DoctorEmail, EncPassword, DentistFirstName, DentistLastName, null, null, null, Accountkey, null, null, null, null, null, null, Phone, null, false, person.FacebookURL, person.LinkedInURL, person.TwitterURL, null, null, person.Description, null, null, null, null, null, null, null, 0, null, null, null, 0);
                        if (NewUserId != 0)
                        {
                            bool result = ObjAdmin.UpdateStatusInTempTableUserById(NewUserId, 1);// update status 2 in temp table 
                            ObjTemplate.NewUserEmailFormat(DoctorEmail, SafeValue<string>(HttpContext.Current.Session["CompanyWebsite"]) + "/User/Index?UserName=" + DoctorEmail + "&Password=" + EncPassword, Password, SafeValue<string>(HttpContext.Current.Session["CompanyWebsite"]));
                            FromPublicProfileDoctorId = NewUserId;
                            bool result1 = new SendReferralBLL().CheckAddAsColleague(Userid, FromPublicProfileDoctorId, DoctorEmail);
                            FromPublicProfileDoctorFullName = DentistFirstName + " " + DentistLastName;
                            if (!string.IsNullOrEmpty(SafeValue<string>(person.ImageUrl)))
                            {
                                string makefilename = "$" + System.DateTime.Now.Ticks + "$" + (SafeValue<string>(FromPublicProfileDoctorFullName)) + ".png";
                                string rootPath = "~/DentistImages/";
                                string fileName = System.IO.Path.Combine(HttpContext.Current.Server.MapPath(rootPath), makefilename);
                                System.Drawing.Image image = objCommon.DownloadImageFromUrl(SafeValue<string>(person.ImageUrl));
                                bool IsUploadImage = ObjColleaguesData.UpdateDoctorProfileImageById(SafeValue<int>(NewUserId), makefilename);
                                image.Save(fileName);
                            }
                        }
                    }
                }
                Obj.Add(NewUserId, FromPublicProfileDoctorFullName);
                return Obj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("SendREferralBLL---ReferralTimeAddReferedBy", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        private string GetPublicProfileDoctorEmail(int userID)
        {
            string DoctroEmail = string.Empty;
            DataSet ds = new DataSet();
            ds = ObjColleaguesData.GetDoctorDetailsById(userID);
            if (ds != null && ds.Tables.Count > 0)
            {
                DoctroEmail = SafeValue<string>(ds.Tables[0].Rows[0]["Username"]);

            }
            return DoctroEmail;
        }

        protected Person GetSocialMediaPerson(string email)
        {
            try
            {
                //connect to fliptop and pull the information down
                SocialMedia sm = new SocialMedia();
                Person person = sm.LookUpPersonClearBit(email);
                Person fullperson = sm.LookUpPersonfullcontact(email);
                person.FirstName = !string.IsNullOrEmpty(person.FirstName) ? person.FirstName : fullperson.FirstName;
                person.LastName = !string.IsNullOrEmpty(person.LastName) ? person.LastName : fullperson.LastName;
                person.FullName = !string.IsNullOrEmpty(person.FullName) ? person.FullName : fullperson.FullName;
                person.FacebookURL = !string.IsNullOrEmpty(person.FacebookURL) ? person.FacebookURL : fullperson.FacebookURL;
                person.LinkedInURL = !string.IsNullOrEmpty(person.LinkedInURL) ? person.LinkedInURL : fullperson.LinkedInURL;
                person.TwitterURL = !string.IsNullOrEmpty(person.TwitterURL) ? person.TwitterURL : fullperson.TwitterURL;
                person.ImageUrl = !string.IsNullOrEmpty(person.ImageUrl) ? person.ImageUrl : fullperson.ImageUrl;
                person.Description = !string.IsNullOrEmpty(fullperson.Description) ? fullperson.Description : person.Description;
                return person;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CheckAddAsColleague(int UserId, int DoctorId, string DoctorEmail)
        {
            bool result = false;
            try
            {
                // Need to Check doctor which is exsits in system is collegaue of doctor whoes public profile is if not then we add them as collegaue because doctor can show only colleague doctor's referral
                DataTable dtColleagues = new DataTable();
                dtColleagues = ObjColleaguesData.GetColleaguesDetailsForDoctor(UserId, 1, int.MaxValue, DoctorEmail, 1, 2);
                if (dtColleagues == null || dtColleagues.Rows.Count == 0)
                {
                    result = ObjColleaguesData.AddAsColleague(UserId, DoctorId);
                    if (result)
                    {
                        //Send Added Colleague Templet
                        ObjTemplate.ColleagueInviteDoctor(UserId, DoctorId, null);
                    }
                    else
                    {
                        //Send Invite Colleague Templet
                        ObjTemplate.InviteDoctor(DoctorEmail, UserId, null);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return result;
        }

        public ValidationResponse ValidateInputsForPMSSendReferral(PMSPatientDetails patient, PMSProviderDetails provider)
        {
            ValidationResponse result = new ValidationResponse(); ;
            result.Status = true;
            List<string> MessageList = new List<string>();
            if(patient != null)
            {
                if(string.IsNullOrWhiteSpace(patient.PatientId))
                {
                    result.Status = false;
                    MessageList.Add("Please give us patient id.");
                }
                if(string.IsNullOrWhiteSpace(patient.FirstName))
                {
                    result.Status = false;
                    MessageList.Add("Please give us patient first name.");
                }

                if(patient.Gender != 0 && patient.Gender != 1 )
                {
                    result.Status = false;
                    MessageList.Add("Please give us proper gender.");
                }
                if(!string.IsNullOrWhiteSpace(patient.Email) && !new clsCommon().IsEmail(patient.Email))
                {
                    result.Status = false;
                    MessageList.Add("Please give us proper patient email address.");
                }
            }
            else
            {
                result = new ValidationResponse() { Status = false, Message = "Patient details not found." };
                return result;
            }
            
            if(provider != null)
            {
                if(string.IsNullOrWhiteSpace(provider.ProviderId))
                {
                    result.Status = false;
                    MessageList.Add("Please give us provider ID.");
                }
                if(string.IsNullOrWhiteSpace(provider.FirstName))
                {
                    result.Status = false;
                    MessageList.Add("Please give us provider first name");
                }
                if(string.IsNullOrWhiteSpace(provider.Email))
                {
                    result.Status = false;
                    MessageList.Add("Please give us provider email address");
                }
                else if(!new clsCommon().IsEmail(provider.Email))
                {
                    result.Status = false;
                    MessageList.Add("Please give us provider valid email address");
                }

            }
            else
            {
                result = new ValidationResponse() { Status = false, Message = "Provider details not found." };
                return result;
            }
            result.Message = string.Join("--||--", MessageList.ToArray());
            return result;
        }
        public PMSSendReferralDetails SendReferralFromPMS(int CurrentUserId, PMSPatientDetails patient, PMSProviderDetails provider)
        {
            PMSSendReferralDetails ReturnValue = null;
            DataTable dtDC = new clsColleaguesData().GetDentrixConnectorOfDoctor(CurrentUserId);
            int DentrixConnectorId = 0;
            if (dtDC != null && dtDC.Rows.Count > 0)
            {
                DentrixConnectorId = SafeValue<int>(dtDC.Rows[0]["DentrixConnectorId"]);

                ReturnValue = new PMSSendReferralDetails();
                ReturnValue.ColleagueIds = CreateDoctorForSendReferral(CurrentUserId, provider);
                ReturnValue.PatientId = CreatePatientForSendReferral(CurrentUserId, patient, ReturnValue.ColleagueIds, DentrixConnectorId);
            }
            

            return ReturnValue;
        }

        private int CreateDoctorForSendReferral(int CurrentUserId, PMSProviderDetails provider)
        {
            int DoctorId = 0;
            DataTable dtDoctorCheck = new DataTable();
            string DoctorEmail = provider.Email;
            dtDoctorCheck = ObjColleaguesData.CheckEmailExistsAsDoctor(provider.Email);
            if (dtDoctorCheck != null && dtDoctorCheck.Rows.Count > 0)
            {
                FromPublicProfileDoctorId = SafeValue<int>(dtDoctorCheck.Rows[0]["UserId"]);
                DoctorId = FromPublicProfileDoctorId;
                FromPublicProfileDoctorFullName = SafeValue<string>(dtDoctorCheck.Rows[0]["FirstName"]) + " " + SafeValue<string>(dtDoctorCheck.Rows[0]["LastName"]);

                // Need to Check doctor which is exsits in system is collegaue of doctor whoes public profile is if not then we add them as collegaue because doctor can show only colleague doctor's referral
                bool result = false;
                result = CheckAddAsColleague(CurrentUserId, FromPublicProfileDoctorId, DoctorEmail);
            }
            else
            {
                // code for signup doctor and set FromPublicProfileDoctorId 
                //DataTable dt = new DataTable();
                //dt = ObjAdmin.GetTempSignupByEmail(DoctorEmail);
                //if (dt.Rows.Count > 0)
                //{
                //    if (dt.Rows[0]["status"].ToString() == "0")
                //    {
                //        string[] name = DoctorEmail.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
                //        string Password = objCommon.CreateRandomPassword(8);
                //        string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                //        string Accountkey = Crypto.SHA1(SafeValue<string>(DateTime.Now.Ticks));
                //        Person person = GetSocialMediaPerson(DoctorEmail);
                //        int NewUserId = ObjColleaguesData.SingUpDoctor(DoctorEmail, EncPassword, provider.FirstName, provider.LastName, null, null, null, Accountkey, null, null, null, null, null, null, provider.Phone, null, false, person.FacebookURL, person.LinkedInURL, person.TwitterURL, null, null, person.Description, null, null, null, null, null, null, null, 0, null, null, null, 0, provider.ProviderId);
                //        bool result1 = CheckAddAsColleague(CurrentUserId, FromPublicProfileDoctorId, DoctorEmail);
                //        if (NewUserId != 0)
                //        {
                //            bool result = ObjAdmin.UpdateStatusInTempTableUserById(NewUserId, 1);// update status 2 in temp table 
                //            ObjTemplate.NewUserEmailFormat(DoctorEmail, HttpContext.Current.Session["CompanyWebsite"].ToString() + "/User/Index?UserName=" + DoctorEmail + "&Password=" + EncPassword, Password, HttpContext.Current.Session["CompanyWebsite"].ToString());
                //            FromPublicProfileDoctorId = NewUserId;
                //            FromPublicProfileDoctorFullName = provider.FirstName + " " + provider.LastName;
                //            if (!string.IsNullOrEmpty(SafeValue<string>(person.ImageUrl)))
                //            {
                //                string makefilename = "$" + System.DateTime.Now.Ticks + "$" + (objCommon.CheckNull(SafeValue<string>(FromPublicProfileDoctorFullName), string.Empty)) + ".png";
                //                string rootPath = "~/DentistImages/";
                //                string fileName = System.IO.Path.Combine(HttpContext.Current.Server.MapPath(rootPath), makefilename);
                //                System.Drawing.Image image = objCommon.DownloadImageFromUrl(objCommon.CheckNull(SafeValue<string>(person.ImageUrl), string.Empty));
                //                bool IsUploadImage = ObjColleaguesData.UpdateDoctorProfileImageById(SafeValue<int>(NewUserId), makefilename);
                //                image.Save(fileName);

                //            }

                //        }

                //    }

                //}
                //else
                //{
                string[] name = DoctorEmail.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
                string Password = objCommon.CreateRandomPassword(8);
                string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                string Accountkey = Crypto.SHA1(SafeValue<string>(DateTime.Now.Ticks));
                Person person = GetSocialMediaPerson(DoctorEmail);
                provider.Mobile = string.IsNullOrWhiteSpace(provider.Mobile) ? null : provider.Mobile;
                int NewUserId = ObjColleaguesData.SingUpDoctor(DoctorEmail, EncPassword, provider.FirstName, provider.LastName, null, null, null, Accountkey, null, null, null, null, null, null, provider.Phone, null, false, person.FacebookURL, person.LinkedInURL, person.TwitterURL, null, null, person.Description, null, null, null, null, null, null, provider.Speciality, 0, null, null, null, 0, provider.ProviderId, provider.Mobile);
                if (NewUserId != 0)
                {
                    bool result = ObjAdmin.UpdateStatusInTempTableUserById(NewUserId, 1);// update status 2 in temp table 
                    ObjTemplate.NewUserEmailFormat(DoctorEmail, SafeValue<string>(HttpContext.Current.Session["CompanyWebsite"]) + "/User/Index?UserName=" + DoctorEmail + "&Password=" + EncPassword, Password, SafeValue<string>(HttpContext.Current.Session["CompanyWebsite"]));
                    FromPublicProfileDoctorId = NewUserId;
                    bool result1 = CheckAddAsColleague(CurrentUserId, FromPublicProfileDoctorId, DoctorEmail);
                    FromPublicProfileDoctorFullName = provider.FirstName + " " + provider.LastName;
                    if (!string.IsNullOrEmpty(SafeValue<string>(person.ImageUrl)))
                    {
                        string makefilename = "$" + System.DateTime.Now.Ticks + "$" + (SafeValue<string>(FromPublicProfileDoctorFullName)) + ".png";
                        string rootPath = "~/DentistImages/";
                        string fileName = System.IO.Path.Combine(HttpContext.Current.Server.MapPath(rootPath), makefilename);
                        System.Drawing.Image image = objCommon.DownloadImageFromUrl(SafeValue<string>(person.ImageUrl));
                        bool IsUploadImage = ObjColleaguesData.UpdateDoctorProfileImageById(SafeValue<int>(NewUserId), makefilename);
                        image.Save(fileName);
                    }
                    DoctorId = NewUserId;
                }
                //  }

            }
            return DoctorId;
        }
        private int CreatePatientForSendReferral(int CurrentUserId, PMSPatientDetails patient, int ToUserId, int DentrixConnectorId)
        {
            int PatientId = 0;

            ObjPatientsData = new clsPatientsData();
            DataTable dtPatient = new DataTable();

            dtPatient = ObjPatientsData.GetPatientBasedOnAssignedPatientId(patient.PatientId, CurrentUserId);
            if (dtPatient != null && dtPatient.Rows.Count > 0)
            {
                PatientId = SafeValue<int>(dtPatient.Rows[0]["PatientId"]);

            }
            else
            {
                // Code for create new patient
                int InsertPatient = 0;
                string Password = objCommon.CreateRandomPassword(8);
                patient.Mobile = string.IsNullOrWhiteSpace(patient.Mobile) ? null : patient.Mobile;
                InsertPatient = ObjPatientsData.PatientInsert(0, patient.PatientId, patient.FirstName, "", patient.LastName, "", 0, null, patient.Phone, patient.Email, null, null, null, null, null, CurrentUserId, 0, null, Password, null, DentrixConnectorId, patient.Mobile);
                if (InsertPatient != 0)
                {
                    PatientId = InsertPatient;
                    //Session["PublicProfilePatientId"] = InsertPatient;
                    // Send Patient added notification to Doctor
                    string Username = GetPublicProfileDoctorEmail(ToUserId);
                    ObjTemplate.PatientAddedNotification(CurrentUserId,InsertPatient, patient.Email, patient.FirstName, patient.LastName, patient.Phone, "", "", "", "", "", "");
                    int id = ObjPatientsData.Insert_PatientMessages(patient.FirstName, Username , "Patient sign-up", "<a href=\"javascript:;\" onclick=\"getAttachedPatientHistory(" + InsertPatient + ")\">" + patient.FirstName + " " + patient.LastName + "</a> recently signed up as patient.", CurrentUserId, InsertPatient, false, false, true);
                    ObjTemplate.PatientSignUpEMail(patient.Email, Password, CurrentUserId);
                }
            }

            return PatientId;
        }

        public static void GetReferralForOneClick()
        {
            try
            {
                DataTable dt = clsColleaguesData.GetOneclickReferralDetails();
                List<ReferralDetailsOneClick> lstReferralClick = new List<ReferralDetailsOneClick>();
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        ReferralDetailsOneClick _objReferral = new ReferralDetailsOneClick();
                        _objReferral.MessageId = SafeValue<int>(dt.Rows[i]["MessageId"]);
                        _objReferral.SenderId = SafeValue<int>(dt.Rows[i]["SenderId"]);
                        _objReferral.PatientId = SafeValue<int>(dt.Rows[i]["PatientId"]);
                        _objReferral.ReceiverId = SafeValue<int>(dt.Rows[i]["ReferedUserId"]);
                        _objReferral.SenderEmail = SafeValue<string>(dt.Rows[i]["SenderEmail"]);
                        _objReferral.PatientEmail = SafeValue<string>(dt.Rows[i]["PatientEmail"]);
                        _objReferral.ReceiverEmail = SafeValue<string>(dt.Rows[i]["ReferredEmail"]);
                        _objReferral.SenderPhone = SafeValue<string>(dt.Rows[i]["SenderPhone"]);
                        _objReferral.PatientPhone = SafeValue<string>(dt.Rows[i]["PatientPhone"]);
                        _objReferral.ReceiverPhone = SafeValue<string>(dt.Rows[i]["ReferredPhone"]);
                        _objReferral.SenderName = SafeValue<string>(dt.Rows[i]["SenderName"]);
                        _objReferral.PatientName = SafeValue<string>(dt.Rows[i]["PatientName"]);
                        _objReferral.ReceiverName = SafeValue<string>(dt.Rows[i]["ReferredName"]);
                        lstReferralClick.Add(_objReferral);
                    }
                }
                foreach (var item in lstReferralClick)
                {
                    foreach (Common.ReferralType itm in Enum.GetValues(typeof(Common.ReferralType)))
                    {
                        new clsTemplate().SendEmailForOneClickReferral(item, itm);
                    }                                    
                }                
            }
            catch (Exception ex)
            {
                new clsCommon().InsertErrorLog("SendReferralBll-GetReferralForOneClick", ex.Message, ex.StackTrace);
                throw;
            }
        }
    }
}
