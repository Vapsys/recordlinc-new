﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using DataAccessLayer.Common;
using BO.Models;
using System.Configuration;

namespace BusinessLogicLayer
{
    public class APILogBLL
    {
        public static bool InsertAPILogDetials(ApiLogEntry Obj)
        {
            try
            {
                return (Convert.ToString(ConfigurationManager.AppSettings["IsWebAPIEventLog"]) == "true") ? ApiLogDAL.InsertLog(Obj) : true;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("APILogBLL--InsertAPILogDetials", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
