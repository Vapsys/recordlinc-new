﻿using BO.Models;
using DataAccessLayer;
using DataAccessLayer.Common;
using Twilio;
using Twilio.Types;
using Twilio.Rest.Api.V2010.Account;
using System.Configuration;
using System;
using BO.Enums;
using Twilio.Exceptions;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public class TwilioCallBLL
    {

        public static bool CostEstimatorCall(string SendToPhone, decimal Cost, string Phone, int UserId)
        {
            clsCommon common = new clsCommon();
            var AccountSid = SafeValue<string>(ConfigurationManager.AppSettings["TwillioAccountSid"]);
            var AuthToken = SafeValue<string>(ConfigurationManager.AppSettings["TwillioAuthToken"]);
            var CountryCode = SafeValue<string>(ConfigurationManager.AppSettings["CountryCode"]);
            var TwillioPhone = SafeValue<string>(ConfigurationManager.AppSettings["TwillioPhone"]);

            //var twilio = new TwilioRestClient(AccountSid, AuthToken);
            //var options = new CallOptions();
            //twilio = MessageResource.Create(
            string DynamicURL = SafeValue<string>(ConfigurationManager.AppSettings["TwmlCostEstimator"]) + "?Cost=" + Cost + "&Phone=" + Phone + "";
            //options.Url = DynamicURL;
            //options.To = CountryCode + common.ConvertPhoneNumber(SendToPhone);
            //options.From = TwillioPhone;
            //Call call = twilio.InitiateOutboundCall(options);

            TwilioClient.Init(AccountSid, AuthToken);
            var to = new PhoneNumber(CountryCode + common.ConvertPhoneNumber(SendToPhone));
            var from = new PhoneNumber(TwillioPhone);

            try
            {
                var call = CallResource.Create(to, from, url: new Uri(DynamicURL));
                InsertCallLog(CountryCode + SendToPhone, Common.EnumCommunication.InsuranceCostEstimatorCall, UserId, call.Sid);
                return true;
            }
            catch(TwilioException ex)
            {
                InsertCallLog(CountryCode + SendToPhone, Common.EnumCommunication.InsuranceCostEstimatorCall, UserId, null,ex);
                return false;
            }

        }

        private static void InsertCallLog(string SendToPhone, Common.EnumCommunication CallType,int UserId, string callSid = null, TwilioException  ex = null)
        {
            CallHistoryModel model = null;
            if (!string.IsNullOrWhiteSpace(callSid))
            {
                model = new CallHistoryModel()
                {
                    Content = null,
                    PhoneNumber = SendToPhone,
                    CallType = CallType,
                    STATUS = Common.EnumCallStatus.Success,
                    APIMessage = null,
                    APIStatus = 0,
                    APICode = 0,
                    APIMoreInfo = null,
                    APISId = callSid,
                    UserId = UserId
                };
            }
            else
            {
                model = new CallHistoryModel()
                {
                    Content = null,
                    PhoneNumber = SendToPhone,
                    CallType = Common.EnumCommunication.InsuranceCostEstimatorCall,
                    STATUS = Common.EnumCallStatus.failed,
                    APIMessage = ex.Message,
                    APIStatus = 0,
                    APICode = SafeValue<int>(ex.HResult),
                    APIMoreInfo = ex.StackTrace + "  Help URL : " + ex.HelpLink,
                    APISId = null,
                    UserId = UserId
                };
            }
            clsCallHistoryLog.InsertCostEstimatorCallLog(model);
        }
    }
}
