﻿using BO.Models;
using DataAccessLayer.Common;
using DataAccessLayer.ProfileData;
using DataAccessLayer.ColleaguesData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using static DataAccessLayer.Common.clsCommon;
using BO.ViewModel;
using System.Configuration;
using System.Web.Mvc;
using System.IO;
using System.Web.Helpers;

namespace BusinessLogicLayer
{
    public class ProfileBLL
    {
        clsProfileData objProfileData = new clsProfileData();
        clsCommon ObjCommon = new clsCommon();
        #region Insurance_Member

        public bool InsertInsuranceMember(Insurance insurance)
        {
            return objProfileData.InsertInsuranceMember(insurance);
        }

        public bool RemoveInsuranceMember(int UserId)
        {
            return objProfileData.RemoveInsuranceMember(UserId);
        }

        public List<Insurance> GetInsuranceDetail()
        {
            DataTable dt = new DataTable();
            List<Insurance> lstInsurance = new List<Insurance>();
            try
            {
                dt = objProfileData.GetInsuranceDetail();
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        lstInsurance.Add(new Insurance
                        {
                            InsuranceId = SafeValue<int>(dt.Rows[i]["InsuranceId"]),
                            Name = SafeValue<string>(dt.Rows[i]["Name"]),
                            LogoPath = SafeValue<string>(dt.Rows[i]["Logo"]),
                            Link = SafeValue<string>(dt.Rows[i]["Link"]),
                            Description = SafeValue<string>(dt.Rows[i]["Description"]),
                        });
                    }
                }
                return lstInsurance;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public List<Insurance> GetMemberInsuranceByUserId(int UserId, int LocationId = 0)
        {
            DataTable dt = new DataTable();
            List<Insurance> lstInsurance = new List<Insurance>();
            try
            {
                dt = objProfileData.GetMemberInsuranceByUserId(UserId, LocationId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        lstInsurance.Add(new Insurance
                        {
                            Member_InsuranceId = SafeValue<int>(dt.Rows[i]["Id"]),
                            UserId = SafeValue<int>(dt.Rows[i]["UserId"]),
                            InsuranceId = SafeValue<int>(dt.Rows[i]["InsuranceId"]),
                            Name = SafeValue<string>(dt.Rows[i]["Name"]),
                           // LocationId = SafeValue<int>(dt.Rows[i]["locationId"]),
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return lstInsurance;
        }




        public bool RemoveInsuranceMemberById(int Id, int UserId)
        {
            return objProfileData.RemoveInsuranceMemberById(Id, UserId);
        }

        #endregion

        public ClaimProfile GetProfileDetailsOfDoctorByID(int UserId)
        {
            DataSet ds = new DataSet();
            clsColleaguesData clscolleaguedata = new clsColleaguesData();
            ds = clscolleaguedata.GetDoctorDetailsById(UserId);
            ClaimProfile clmprofile = new ClaimProfile();
            if (ds.Tables.Count > 0)
            {
                clmprofile.Firstname = SafeValue<string>(ds.Tables[0].Rows[0]["FirstName"]);
                clmprofile.LastName = SafeValue<string>(ds.Tables[0].Rows[0]["LastName"]);
                clmprofile.Phone = SafeValue<string>(ds.Tables[2].Rows[0]["Phone"]);
                clmprofile.Email = SafeValue<string>(ds.Tables[2].Rows[0]["EmailAddress"]);
            }
            return clmprofile;
        }

        public bool GetEPMSIdbyAccountId(int UserId, string EMPSId, int LocationId)
        {
            bool result = false;
            DataTable dt = new DataTable();
            dt = clsColleaguesData.GetExtrnalPMSIdByAccountId(UserId, EMPSId, LocationId);
            result = (dt.Rows.Count > 0) ? true : false;
            return result;
        }

        /// <summary>
        /// Get Dentist Details by Id Used on OCR side Profile page.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static DentistProfileViewModel GetDentistDetails(int UserId)
        {
            try
            {
                DentistProfileViewModel MdlMember = new DentistProfileViewModel();
                clsCommon objCommon = new clsCommon(); string ImageName = null;
                DataSet ds = new clsColleaguesData().GetDoctorDetailsById(UserId);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string Desciption = SafeValue<string>(ds.Tables[0].Rows[0]["Description"]);
                    MdlMember.UserId = SafeValue<int>(ds.Tables[0].Rows[0]["UserId"]);
                    MdlMember.FirstName = SafeValue<string>(ds.Tables[0].Rows[0]["FirstName"]);
                    MdlMember.LastName = SafeValue<string>(ds.Tables[0].Rows[0]["LastName"]);
                    MdlMember.Salutation = SafeValue<string>(ds.Tables[0].Rows[0]["Salutation"]);
                    if (!string.IsNullOrEmpty(Desciption))
                    {
                        MdlMember.Description = Desciption.Replace("\n", "<br/>");
                    }

                    MdlMember.Title = SafeValue<string>(ds.Tables[0].Rows[0]["Title"]);
                    MdlMember.MiddleName = SafeValue<string>(ds.Tables[0].Rows[0]["MiddleName"]);
                    MdlMember.PublicProfile = SafeValue<string>(ds.Tables[0].Rows[0]["PublicProfileUrl"]);
                    MdlMember.WebsiteURL = SafeValue<string>(ds.Tables[0].Rows[0]["WebsiteURL"]);

                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        MdlMember.City = SafeValue<string>(ds.Tables[2].Rows[0]["City"]);
                        MdlMember.State = SafeValue<string>(ds.Tables[2].Rows[0]["State"]);
                        MdlMember.ZipCode = SafeValue<string>(ds.Tables[2].Rows[0]["ZipCode"]);

                        MdlMember.lstDoctorAddressDetails = (from p in ds.Tables[2].AsEnumerable()
                                                             select new AddressDetails
                                                             {
                                                                 AddressInfoID = SafeValue<int>(p["AddressInfoID"]),
                                                                 Location = SafeValue<string>(p["LocationName"]),
                                                                 ExactAddress = SafeValue<string>(p["ExactAddress"]),
                                                                 Address2 = SafeValue<string>(p["Address2"]),
                                                                 City = SafeValue<string>(p["City"]),
                                                                 State = SafeValue<string>(p["State"]),
                                                                 Country = SafeValue<string>(p["Country"]),
                                                                 ZipCode = SafeValue<string>(p["ZipCode"]),
                                                                 Phone = SafeValue<string>(p["Phone"]),
                                                                 Fax = SafeValue<string>(p["Fax"]),
                                                                 TimeZoneId = SafeValue<int>(p["TimeZoneId"]),
                                                                 EmailAddress = SafeValue<string>(p["EmailAddress"]),
                                                                 ContactType = SafeValue<int>(p["ContactType"]),
                                                                 Mobile = SafeValue<string>(p["Mobile"])
                                                             }).ToList();

                    }
                    MdlMember.lstDoctorSortedAddressDetails = MdlMember.lstDoctorAddressDetails;
                    MdlMember.lstDoctorAddressDetails = MdlMember.lstDoctorSortedAddressDetails.Where(x => x.ContactType == 1).ToList();
                    MdlMember.lstDoctorAddressDetails.AddRange(MdlMember.lstDoctorSortedAddressDetails.Where(x => x.ContactType != 1).ToList());
                    ImageName = SafeValue<string>(ds.Tables[0].Rows[0]["ImageName"]);

                    if (string.IsNullOrEmpty(ImageName))
                    {
                        MdlMember.ImageName = SafeValue<string>(ConfigurationManager.AppSettings["DefaultDoctorImage"]);
                    }
                    else
                    {
                        MdlMember.ImageName = SafeValue<string>(ConfigurationManager.AppSettings["DoctorImage"]) + ImageName;
                    }

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        MdlMember.OfficeName = SafeValue<string>(ds.Tables[1].Rows[0]["AccountName"]);
                    }
                    SectionPublicProfileViewModel objprofiles = new SectionPublicProfileViewModel();
                    if (ds.Tables[15].Rows.Count > 0)
                    {
                        objprofiles.PatientForms = SafeValue<bool>(ds.Tables[15].Rows[0]["PatientForms"]);
                        objprofiles.ReferPatient = SafeValue<bool>(ds.Tables[15].Rows[0]["ReferPatient"]);
                        objprofiles.Reviews = SafeValue<bool>(ds.Tables[15].Rows[0]["Reviews"]);
                        objprofiles.SpecialOffers = SafeValue<bool>(ds.Tables[15].Rows[0]["SpecialOffers"]);
                        objprofiles.AppointmentBooking = SafeValue<bool>(ds.Tables[15].Rows[0]["AppointmentBooking"]);
                        MdlMember.ObjProfileSection = objprofiles;
                    }
                    else
                    {
                        objprofiles.PatientForms = true;
                        objprofiles.ReferPatient = true;
                        objprofiles.Reviews = true;
                        objprofiles.SpecialOffers = true;
                        objprofiles.AppointmentBooking = true;
                        MdlMember.ObjProfileSection = objprofiles;
                    }
                    MdlMember.lstsecondarywebsitelist = GetSecondaryWebsitelistByUserId(MdlMember.UserId);
                    MdlMember.lstGetSocialMediaDetailByUserId = GetSocialMediaDetailByUserId(MdlMember.UserId);
                    MdlMember.lstEducationandTraining = GetEducationandTrainingDetailsForDoctor(MdlMember.UserId);
                    MdlMember.lstProfessionalMemberships = GetProfessionalMembershipsByUserId(MdlMember.UserId);
                    MdlMember.lstTeamMemberDetailsForDoctor = GetTeamMemberDetailsOfDoctor(MdlMember.UserId, 0, 1, 50);
                    MdlMember.lstSpeacilitiesOfDoctor = GetSpeacilitiesOfDoctorById(MdlMember.UserId);
                    MdlMember.lstBanner = GetBannerDetailByUserId(MdlMember.UserId);
                    MdlMember.lstProcedure = ProcedureBLL.GetProcedureList(0, MdlMember.UserId);

                    string filePath = ConfigurationManager.AppSettings.Get("GallaryFilePath") + MdlMember.UserId + "/";
                    MdlMember.lstGallary = getGallaryList(MdlMember.UserId, 0);
                    MdlMember.GallaryPath = filePath;
                    ProfileBLL objProfile = new ProfileBLL();
                    MdlMember.lstInsurance = objProfile.GetMemberInsuranceByUserId(MdlMember.UserId);
                }
                return MdlMember;

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--GetDentistDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// SEcondary WEbsite details
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static List<WebsiteDetails> GetSecondaryWebsitelistByUserId(int UserId)
        {

            try
            {
                DataTable dt = new DataTable(); clsCommon objCommon = new clsCommon();
                List<WebsiteDetails> lstsecondarywebsitelist = new List<WebsiteDetails>();
                dt = new clsColleaguesData().GetSecondaryWebsiteDetailsbyId(UserId);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstsecondarywebsitelist.Add(new WebsiteDetails()
                        {
                            SecondaryWebsiteId = SafeValue<int>(row["SecondaryId"]),
                            SecondaryWebsiteurl = SafeValue<string>(row["WebSite"]),
                        });
                    }
                }
                return lstsecondarywebsitelist;

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--GetSecondaryWebsitelistByUserId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Social media details
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static List<SocialMediaForDoctorViewModel> GetSocialMediaDetailByUserId(int UserId)
        {
            try
            {
                #region Start New
                DataTable dt = new DataTable(); clsCommon objCommon = new clsCommon();
                List<SocialMediaForDoctorViewModel> lstGetSocialMediaDetailByUserId = new List<SocialMediaForDoctorViewModel>();
                dt = new clsColleaguesData().GetSocialMediaDetailByUserId(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {

                    foreach (DataRow row in dt.Rows)
                    {
                        lstGetSocialMediaDetailByUserId.Add(new SocialMediaForDoctorViewModel()
                        {
                            FacebookUrl = SafeValue<string>(row["FacebookUrl"]) != string.Empty ? (SafeValue<string>(row["FacebookUrl"]).Contains("http://") || SafeValue<string>(row["FacebookUrl"]).Contains("https://") ? SafeValue<string>(row["FacebookUrl"]) : "http://" + SafeValue<string>(row["FacebookUrl"])) : SafeValue<string>(row["FacebookUrl"]),
                            LinkedinUrl = SafeValue<string>(row["LinkedinUrl"]) != string.Empty ? (SafeValue<string>(row["LinkedinUrl"]).Contains("http://") || SafeValue<string>(row["LinkedinUrl"]).Contains("https://") ? SafeValue<string>(row["LinkedinUrl"]) : "http://" + SafeValue<string>(row["LinkedinUrl"])) : SafeValue<string>(row["LinkedinUrl"]),
                            TwitterUrl = SafeValue<string>(row["TwitterUrl"]) != string.Empty ? (SafeValue<string>(row["TwitterUrl"]).Contains("http://") || SafeValue<string>(row["TwitterUrl"]).Contains("https://") ? SafeValue<string>(row["TwitterUrl"]) : "http://" + SafeValue<string>(row["TwitterUrl"])) : SafeValue<string>(row["TwitterUrl"]),
                            GoogleplusUrl = SafeValue<string>(row["GoogleplusUrl"]) != string.Empty ? (SafeValue<string>(row["GoogleplusUrl"]).Contains("http://") || SafeValue<string>(row["GoogleplusUrl"]).Contains("https://") ? SafeValue<string>(row["GoogleplusUrl"]) : "http://" + SafeValue<string>(row["GoogleplusUrl"])) : SafeValue<string>(row["GoogleplusUrl"]),
                            YoutubeUrl = SafeValue<string>(row["YoutubeUrl"]) != string.Empty ? (SafeValue<string>(row["YoutubeUrl"]).Contains("http://") || SafeValue<string>(row["YoutubeUrl"]).Contains("https://") ? SafeValue<string>(row["YoutubeUrl"]) : "http://" + SafeValue<string>(row["YoutubeUrl"])) : SafeValue<string>(row["YoutubeUrl"]),
                            PinterestUrl = SafeValue<string>(row["PinterestUrl"]) != string.Empty ? (SafeValue<string>(row["PinterestUrl"]).Contains("http://") || SafeValue<string>(row["PinterestUrl"]).Contains("https://") ? SafeValue<string>(row["PinterestUrl"]) : "http://" + SafeValue<string>(row["PinterestUrl"])) : SafeValue<string>(row["PinterestUrl"]),
                            BlogUrl = SafeValue<string>(row["BlogUrl"]) != string.Empty ? (SafeValue<string>(row["BlogUrl"]).Contains("http://") || SafeValue<string>(row["BlogUrl"]).Contains("https://") ? SafeValue<string>(row["BlogUrl"]) : "http://" + SafeValue<string>(row["BlogUrl"])) : SafeValue<string>(row["BlogUrl"]),
                            YelpUrl = SafeValue<string>(row["YelpUrl"]) != string.Empty ? (SafeValue<string>(row["YelpUrl"]).Contains("http://") || SafeValue<string>(row["YelpUrl"]).Contains("https://") ? SafeValue<string>(row["YelpUrl"]) : "http://" + SafeValue<string>(row["YelpUrl"])) : SafeValue<string>(row["YelpUrl"]),
                            InstagramUrl = SafeValue<string>(row["InstagramUrl"]) != string.Empty ? (SafeValue<string>(row["InstagramUrl"]).Contains("http://") || SafeValue<string>(row["InstagramUrl"]).Contains("https://") ? SafeValue<string>(row["InstagramUrl"]) : "http://" + SafeValue<string>(row["InstagramUrl"])) : SafeValue<string>(row["InstagramUrl"]),//RM-355
                        });
                    }
                }
                return lstGetSocialMediaDetailByUserId;
                #endregion
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--GetSocialMediaDetailByUserId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Education and Training
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static List<EducationandTraining> GetEducationandTrainingDetailsForDoctor(int UserId)
        {
            try
            {
                clsCommon objCommon = new clsCommon();
                DataTable dt = new DataTable();
                List<EducationandTraining> lstEducationandTraining = new List<EducationandTraining>();
                dt = new clsColleaguesData().GetEducationTrainingByUserId(UserId);

                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(SafeValue<string>(dt.Rows[i]["Institute"])))
                        {
                            lstEducationandTraining.Add(new EducationandTraining { Id = SafeValue<int>(dt.Rows[i]["Id"]), Institute = SafeValue<string>(dt.Rows[i]["Institute"]), Specialisation = SafeValue<string>(dt.Rows[i]["Specialisation"]), YearAttended = SafeValue<string>(dt.Rows[i]["YearAttended"]) });
                        }
                    }
                }
                return lstEducationandTraining;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--GetEducationandTrainingDetailsForDoctor", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Professional Membership
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static List<ProfessionalMemberships> GetProfessionalMembershipsByUserId(int UserId)
        {
            try
            {
                DataTable dt = new DataTable(); clsCommon objCommon = new clsCommon();
                List<ProfessionalMemberships> lstProfessionalMemberships = new List<ProfessionalMemberships>();
                dt = new clsColleaguesData().GetProfessionalMembershipsByUserId(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(SafeValue<string>(dt.Rows[i]["Membership"])))
                        {
                            lstProfessionalMemberships.Add(new ProfessionalMemberships { Id = SafeValue<int>(dt.Rows[i]["Id"]), Membership = SafeValue<string>(dt.Rows[i]["Membership"]), });
                        }
                    }
                }
                return lstProfessionalMemberships;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--GetProfessionalMembershipsByUserId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// TeamMember list
        /// </summary>
        /// <param name="ParentUserId"></param>
        /// <param name="UserId"></param>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <returns></returns>
        public static List<TeamMemberDetailsForDoctorViewModel> GetTeamMemberDetailsOfDoctor(int ParentUserId, int UserId, int PageIndex, int PageSize)
        {
            try
            {
                DataTable dt = new DataTable(); clsCommon objCommon = new clsCommon();
                List<TeamMemberDetailsForDoctorViewModel> lstTeamMemberDetailsForDoctor = new List<TeamMemberDetailsForDoctorViewModel>();
                dt = new clsColleaguesData().GetTeamMemberDetailsOfDoctor(ParentUserId, PageIndex, PageSize);
                if (dt != null && dt.Rows.Count > 0)
                {
                    lstTeamMemberDetailsForDoctor = (from p in dt.AsEnumerable()
                                                     select new TeamMemberDetailsForDoctorViewModel
                                                     {
                                                         UserId = SafeValue<int>(p["UserId"]),
                                                         FirstName = SafeValue<string>(p["FirstName"]),
                                                         LastName = SafeValue<string>(p["LastName"]),
                                                     }).Where(x => x.UserId != UserId).ToList();
                }
                return lstTeamMemberDetailsForDoctor;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--GetTeamMemberDetailsOfDoctor", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        /// <summary>
        /// Specialties of doctor
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static List<SpeacilitiesOfDoctor> GetSpeacilitiesOfDoctorById(int UserId)
        {
            DataTable dtSpeacilities = new DataTable(); clsCommon objCommon = new clsCommon();
            dtSpeacilities = new clsColleaguesData().GetMemberSpecialities(UserId);
            List<SpeacilitiesOfDoctor> lstSpeacilities = new List<SpeacilitiesOfDoctor>();
            if (dtSpeacilities.Rows.Count > 0)
            {
                lstSpeacilities = (from p in dtSpeacilities.AsEnumerable()
                                   select new SpeacilitiesOfDoctor
                                   {
                                       SpecialtyId = int.Parse(!string.IsNullOrWhiteSpace(SafeValue<string>(p["SpecialityId"])) ? SafeValue<string>(p["SpecialityId"]) : "0"),
                                       SpecialtyDescription = SafeValue<string>(p["Specialities"])
                                   }).ToList();


            }
            return lstSpeacilities;
        }

        /// <summary>
        /// Get List of Banners of Doctor
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static List<Banner> GetBannerDetailByUserId(int UserId)
        {
            try
            {
                DataTable dt = new DataTable(); clsCommon objCommon = new clsCommon();
                List<Banner> lstBanner = new List<Banner>();
                dt = new clsColleaguesData().GetBannerDetailByUserId(UserId);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        Banner banner = new Banner();
                        banner.BannerId = SafeValue<int>(row["BannerId"]);
                        banner.UserId = SafeValue<int>(row["UserId"]);
                        banner.Title = SafeValue<string>(row["Title"]);
                        banner.Path = SafeValue<string>(row["Path"]);
                        banner.ColorCode = SafeValue<string>(row["ColorCode"]);
                        banner.ImagePath = SafeValue<string>(ConfigurationManager.AppSettings["BannerImage"]).Replace('~', ' ');
                        lstBanner.Add(banner);
                    }
                }
                return lstBanner;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--GetBannerDetailByUserId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Gallery of Doctor
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="GallaryId"></param>
        /// <returns></returns>
        public static List<Gallery> getGallaryList(int UserId, int GallaryId)
        {

            var objProfileData = new clsProfileData();
            List<Gallery> list = new List<Gallery>();
            DataTable dt = new DataTable();
            dt = objProfileData.getGallaryById(UserId, GallaryId);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = (from p in dt.AsEnumerable()
                        select new Gallery
                        {
                            FileName = SafeValue<int>(SafeValue<string>(p["MediaType"])) == (int)BO.Enums.Common.GalleryConstants.IMAGE_URL ? SafeValue<string>(p["MediaUrl"]) : null,
                            VideoURL = SafeValue<int>(SafeValue<string>(p["MediaType"])) == (int)BO.Enums.Common.GalleryConstants.VIDEO_URL ? SafeValue<string>(p["MediaUrl"]) : null,
                            GallaryId = SafeValue<int>(p["GallaryId"]),
                            UserId = SafeValue<int>(p["UserId"])
                        }).ToList();
            }
            return list;
        }

        /// <summary>
        /// Get Address Details By Address Info ID
        /// </summary>
        /// <param name="AddressInfoID"></param>
        /// <returns></returns>
        public static AddressDetails GetDoctorAddressDetailsByAddressInfoID(int AddressInfoID)
        {
            AddressDetails lst = new AddressDetails();
            DataTable dt = new DataTable(); clsCommon objCommon = new clsCommon();
            dt = new clsColleaguesData().GetAddressDetailsByAddressInfoID(AddressInfoID);
            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    lst.AddressInfoID = SafeValue<int>(dt.Rows[0]["AddressInfoID"]);
                    lst.ExactAddress = SafeValue<string>(dt.Rows[0]["ExactAddress"]);
                    lst.Address2 = SafeValue<string>(dt.Rows[0]["Address2"]);
                    lst.City = SafeValue<string>(dt.Rows[0]["City"]);
                    lst.State = SafeValue<string>(dt.Rows[0]["State"]);
                    lst.Country = SafeValue<string>(dt.Rows[0]["Country"]);
                    lst.ZipCode = SafeValue<string>(dt.Rows[0]["ZipCode"]);
                    lst.EmailAddress = SafeValue<string>(dt.Rows[0]["EmailAddress"]);
                    lst.Phone = SafeValue<string>(dt.Rows[0]["Phone"]);
                    lst.Fax = SafeValue<string>(dt.Rows[0]["Fax"]);
                    lst.ContactType = SafeValue<int>(dt.Rows[0]["ContactType"]);
                    lst.Location = SafeValue<string>(dt.Rows[0]["Location"]);
                    lst.TimeZoneId = SafeValue<int>(dt.Rows[0]["TimeZoneId"]);
                    lst.Website = SafeValue<string>(dt.Rows[0]["Website"]);
                    lst.Mobile = SafeValue<string>(dt.Rows[0]["Mobile"]);
                    lst.ExternalPMSId = SafeValue<string>(dt.Rows[0]["ExternalPMSId"]);
                    lst.SchedulingLink = SafeValue<string>(dt.Rows[0]["SchedulingLink"]);
                }
                lst.StateList = PatientBLL.GetStateList("US");
                lst.CountryList = CountryList();
                lst.TimeZoneList = GetTimeZoneList();
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--GetDoctorAddressDetailsByAddressInfoID", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Country List
        /// </summary>
        /// <returns></returns>
        public static List<SelectListItem> CountryList()
        {
            try
            {
                DataTable Dt = new clsCommon().GetAllCountry(); List<SelectListItem> lst = new List<SelectListItem>();
                if (Dt.Rows.Count > 0)
                {
                    foreach (DataRow item in Dt.Rows)
                    {
                        if (SafeValue<string>(item["CountryCode"]) == "US")
                        {
                            lst.Add(new SelectListItem { Text = SafeValue<string>(item["CountryName"]), Value = SafeValue<string>(item["CountryCode"]), Selected = true });
                        }
                        else
                        {
                            lst.Add(new SelectListItem { Text = SafeValue<string>(item["CountryName"]), Value = SafeValue<string>(item["CountryCode"]) });
                        }
                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--CountryList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Country code
        /// </summary>
        /// <param name="CountryCode"></param>
        /// <returns></returns>
        public static List<SelectListItem> StateList(string CountryCode)
        {
            try
            {
                DataTable dt = new clsCommon().GetAllStateByCountryCode(CountryCode);
                List<SelectListItem> lst = new List<SelectListItem>();
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lst.Add(new SelectListItem { Text = SafeValue<string>(item["StateName"]), Value = SafeValue<string>(item["StateCode"]) });
                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--StateList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Insert Update Address Details of User.
        /// </summary>
        /// <param name="Obj"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static int InsertUpdateAddressDetails(AddressDetails Obj, int UserId)
        {
            try
            {
                int? intLocationId;
                intLocationId = clsColleaguesData.InsertUpdateAddressDetails(Obj, UserId);
                if (intLocationId != 0)
                {
                    new clsColleaguesData().UpdateDoctorWebsiteUrlById(Convert.ToInt32(UserId), Obj.Website, 0, intLocationId);
                }
                return (intLocationId != null && intLocationId != 0) ? 1 : 0;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--UpdateAddressDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// First Check the Location is Used in Integrations 
        /// # IF Yes then Return 0.
        /// # If no then Delete the Location and Retun 1.
        /// # If Some Error occured while delete then Return 2.
        /// # If Some Team Member are associate with this location then retun 3.
        /// Remove Address details.
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static int RemoveAddress(int Id)
        {
            try
            {
                //CHECK LOCATION IS USED IN ANY INTEGRATION OR NOT
                //XQ1-689 'Guide key' detail does not disappear after deleting Location, on 'Integrations' page.
                DataTable LocationUsed = clsColleaguesData.IsLocationUsedinIntegration(Id);
                if (LocationUsed != null && LocationUsed.Rows.Count > 0)
                {
                    return 0;
                }
                else
                {
                    //Check Location is associate with any team member or not.
                    DataTable Dt = new clsColleaguesData().GetDoctorsByLocaiton(Id);
                    if (Dt != null && Dt.Rows.Count > 0)
                    {
                        return 3;
                    }
                    else
                    {
                        //DELETE THE LOCATION BAISED ON LOCATION ID
                        if (new clsColleaguesData().RemoveDoctorAddressDetailsByAddressInfoID(Id))
                        {
                            return 1;
                        }
                        else
                        {
                            return 2;
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--RemoveAddress", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Public profile URL Details of Doctor
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static PublicProfileDetail GetPublicProfileDetail(int UserId)
        {
            try
            {
                PublicProfileDetail profileDetail = new PublicProfileDetail();
                DataTable dt = new clsColleaguesData().GetColleagueDetailById(UserId);
                if (dt.Rows.Count > 0)
                {
                    profileDetail.PublicProfileUrl = SafeValue<string>(dt.Rows[0]["PublicProfileUrl"]);
                }
                return profileDetail;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--GetPublicProfileDetail", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Education and Training details by EduId.
        /// </summary>
        /// <param name="Id">Unique Id of Education/Training</param>
        /// <returns>Single List of Education and Training Object.</returns>
        public static EducationandTraining GetEducationandTrainingDetailsById(int Id)
        {
            try
            {

                DataTable dt = new DataTable(); clsCommon objCommon = new clsCommon();
                EducationandTraining Obj = new EducationandTraining();
                dt = new clsColleaguesData().GetEducationTrainingByUniqueId(Id);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        Obj.Id = SafeValue<int>(row["id"]);
                        Obj.Institute = SafeValue<string>(row["Institute"]);
                        Obj.Specialisation = SafeValue<string>(row["Specialisation"]);
                        Obj.YearAttended = SafeValue<string>(row["YearAttended"]);
                    }
                }
                return Obj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--GetEducationandTrainingDetailsForDoctorById", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Insert / Updated Education and Training section
        /// </summary>
        /// <param name="Obj">Education and Training Object</param>
        /// <param name="UserId">UserId of Dentist</param>
        /// <returns></returns>
        public static bool InsertUpdateEducationTrainingDetail(EducationandTraining Obj, int UserId)
        {
            try
            {
                return new clsColleaguesData().EducationAndTrainingInsertAndUpdate(Obj.Id, UserId, Obj.Institute, Obj.Specialisation, Obj.YearAttended);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--InsertUpdateEducationTrainingDetail", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Remove Education Training Details by Unique Id
        /// </summary>
        /// <param name="Id">UniqueId of Education and Training</param>
        /// <returns></returns>
        public static bool RemoveEducationTrainingDetails(int Id)
        {
            try
            {
                return new clsColleaguesData().RemoveEducationTrainingByUniqueId(Id);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--RemoveEducationTrainingDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Professional Membership By Unique Id
        /// </summary>
        /// <param name="Id">Unique Id of Professional Membership Details</param>
        /// <returns></returns>
        public static ProfessionalMemberships GetProfessionalMembershipsByUniqueId(int Id)
        {
            try
            {
                DataTable dt = new DataTable();
                ProfessionalMemberships Obj = new ProfessionalMemberships();
                dt = new clsColleaguesData().GetProfessionalMembershipsByUniqueId(Id);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        Obj.Id = SafeValue<int>(row["Id"]);
                        Obj.Membership = SafeValue<string>(row["Membership"]);
                    }
                }
                return Obj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--GetProfessionalMembershipsByUniqueId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Search Professional Membership.
        /// </summary>
        /// <param name="MembershipName">Search Text</param>
        /// <returns></returns>
        public static List<SelectListItem> SearchProfessionalMemberships(string MembershipName)
        {
            try
            {
                clsCommon objCommon = new clsCommon(); List<SelectListItem> listItems = new List<SelectListItem>();
                DataTable dt = objCommon.SearchProMembership(MembershipName);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        listItems.Add(new SelectListItem()
                        {
                            Text = SafeValue<string>(item["Membership"]),
                            Value = SafeValue<string>(item["Membership"])
                        });
                    }
                }
                return listItems;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--SearchProfessionalMemberships", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Insert Update Professional Membership
        /// </summary>
        /// <param name="Obj">Single Object of Professional Membership</param>
        /// <param name="UserId">Dentist Id</param>
        /// <returns></returns>
        public static bool InsertUpdateProfiessionalMemberShip(ProfessionalMemberships Obj, int UserId)
        {
            try
            {
                return new clsColleaguesData().ProfessionalMembershipsInsertAndUpdate(Obj.Id, UserId, Obj.Membership);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--InsertUpdateProfiessionalMemberShip", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Remove Professional membership of User by Unique id
        /// </summary>
        /// <param name="Id">Unique id</param>
        /// <returns></returns>
        public static bool RemoveProfessionalMembership(int Id)
        {
            try
            {
                return new clsColleaguesData().RemoveProfessionalMembershipsByUniqueId(Id);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--RemoveProfessionalMembership", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Insert/Update TeamMember of User
        /// </summary>
        /// <param name="Obj">Team Member Detail Object</param>
        /// <param name="ParentUserId">Parent userId</param>
        /// <returns>2 = Already Exists team member email address, 1 = successfully added, 0 = failed to insert team member.</returns>
        public static int InsertUpdateTeamMember(InsertTeamMember Obj, int ParentUserId)
        {
            try
            {
                DataTable data = new clsColleaguesData().CheckEmailExistsAsDoctor(Obj.Email);
                if (data != null && data.Rows.Count > 0)
                {
                    return 2;
                }
                else
                {
                    Obj.Password = new TripleDESCryptoHelper().encryptText(new clsCommon().CreateRandomPassword(8));

                    return (clsColleaguesData.InsertUpdateTeamMember(Obj, ParentUserId)) ? 1 : 0;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--InsertUpdateTeamMember", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Team Member Details 
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="ParentUserId"></param>
        /// <returns></returns>
        public static TeamMemberDetails GetTeamMemberDetails(int UserId, int ParentUserId)
        {
            try
            {
                TeamMemberDetails Obj = new TeamMemberDetails();
                DataTable Dt = clsColleaguesData.GetUserFullDetailsById(UserId);
                Obj.LocationList = GetLocationDetailsByDoctorId(ParentUserId);
                if (Dt.Rows.Count > 0)
                {
                    Obj.UserId = SafeValue<int>(Dt.Rows[0]["UserId"]);
                    Obj.FirstName = SafeValue<string>(Dt.Rows[0]["FirstName"]);
                    Obj.LastName = SafeValue<string>(Dt.Rows[0]["LastName"]);
                    Obj.LocationId = SafeValue<int>(Dt.Rows[0]["LocationId"]);
                    Obj.Email = SafeValue<string>(Dt.Rows[0]["Username"]);
                    Obj.SpecialtyList = DoctorSpecialities("");
                    List<SpeacilitiesOfDoctor> lst = GetSpeacilitiesOfDoctorById(Obj.UserId);
                    if (lst != null && lst.Count > 0)
                    {
                        Obj.SpecialtyId = SafeValue<int>(lst[0].SpecialtyId);
                    }
                }
                else
                {
                    Obj.SpecialtyList = DoctorSpecialities("");
                }
                return Obj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--GetTeamMemberDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Location list while getting team member details
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static List<LocationDetails> GetLocationDetailsByDoctorId(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = new clsColleaguesData().GetDoctorLocaitonById(UserId);
                List<LocationDetails> lst = new List<LocationDetails>();
                if (dt.Rows.Count > 0)
                {
                    lst = (from p in dt.AsEnumerable()
                           select new LocationDetails
                           {
                               LocationName = SafeValue<string>(p["Location"]),
                               AddressInfoId = SafeValue<int>(p["AddressInfoId"])

                           }).ToList();
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--GetTeamMemberDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Bind Specialty list with selected.
        /// </summary>
        /// <param name="selected"></param>
        /// <returns></returns>
        public static List<SelectListItem> DoctorSpecialities(string selected = "")
        {
            DataTable dt = new DataTable();
            clsCommon objCommon = new clsCommon();
            dt = objCommon.GetAllSpeciality();
            List<SelectListItem> lst = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    string Id = SafeValue<string>(item["SpecialityId"]);
                    if (selected.Split(',').Contains(Id))
                    {
                        lst.Add(new SelectListItem { Text = SafeValue<string>(item["Description"]), Value = SafeValue<string>(item["SpecialityId"]), Selected = true });
                    }
                    else
                    {
                        lst.Add(new SelectListItem { Text = SafeValue<string>(item["Description"]), Value = SafeValue<string>(item["SpecialityId"]) });
                    }
                }
            }
            return lst;
        }

        /// <summary>
        /// Remove Team Member
        /// </summary>
        /// <param name="UserId">Team MemberId</param>
        /// <param name="ParentUserId">Main DoctorId</param>
        /// <returns></returns>
        public static bool RemoveTeamMember(int UserId, int ParentUserId)
        {
            try
            {
                return new clsColleaguesData().RemoveTeamMemberOfDoctor(UserId, ParentUserId);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--RemoveTeamMember", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        /// <summary>
        /// Get Banner Details By BannerId
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public static Banner GetBannerDetailsByBannerId(int Id)
        {
            try
            {
                Banner banner = new Banner(); clsCommon objCommon = new clsCommon();
                DataTable dt = new clsColleaguesData().GetBannerDetailById(Id);
                if (dt.Rows.Count > 0)
                {
                    banner.BannerId = SafeValue<int>(dt.Rows[0]["BannerId"]);
                    banner.UserId = SafeValue<int>(dt.Rows[0]["UserId"]);
                    banner.Title = SafeValue<string>(dt.Rows[0]["Title"]);
                    banner.Path = SafeValue<string>(dt.Rows[0]["Path"]);
                    banner.ColorCode = SafeValue<string>(dt.Rows[0]["ColorCode"]);
                }
                return banner;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--GetBannerDetailsByBannerId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Remove Banner By Banner Id 
        /// </summary>
        /// <param name="Id"> Banner Id</param>
        /// <param name="UserId">Doctor Id</param>
        /// <returns></returns>
        public static bool RemoveBannerByBannerId(int Id, int UserId)
        {
            try
            {
                Banner banner = GetBannerDetailsByBannerId(Id);
                bool Result = new clsColleaguesData().RemoveBanner(Id);
                if (Result)
                {
                    String path = SafeValue<string>(ConfigurationManager.AppSettings["BannerImage"]) + UserId + "/" + banner.Path;
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                }
                return Result;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--RemoveBannerByBannerId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Dentist Profile Details
        /// </summary>
        /// <param name="UserId">UserId of Dentist</param>
        /// <returns></returns>
        public static DentistProfileViewModel GetDentistProfileInfo(int UserId)
        {
            try
            {
                DentistProfileViewModel Obj = new DentistProfileViewModel(); clsCommon objCommon = new clsCommon();
                DataTable Dt = clsColleaguesData.GetUserFullDetailsById(UserId);
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    Obj.UserId = SafeValue<int>(Dt.Rows[0]["UserId"]);
                    Obj.FirstName = SafeValue<string>(Dt.Rows[0]["FirstName"]);
                    Obj.MiddleName = SafeValue<string>(Dt.Rows[0]["MiddleName"]);
                    Obj.LastName = SafeValue<string>(Dt.Rows[0]["LastName"]);
                    Obj.Title = SafeValue<string>(Dt.Rows[0]["Title"]);
                    Obj.Salutation = SafeValue<string>(Dt.Rows[0]["Salutation"]);
                    Obj.lstSpeacilitiesOfDoctor = GetSpeacilitiesOfDoctorById(Obj.UserId);
                    Obj.specialtyIds = String.Join(",", Obj.lstSpeacilitiesOfDoctor.Select(t => t.SpecialtyId).ToArray());
                    Obj.OfficeName = SafeValue<string>(Dt.Rows[0]["AccountName"]);
                }
                return Obj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--GetDentistProfileInfo", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool InertUpdateDescription(int UserId, string Descitption)
        {
            try
            {
                Descitption = (string.IsNullOrEmpty(Descitption)) ? "" : Descitption;
                return new clsColleaguesData().UpdateMemberDetailsByUserId(UserId, "Description", Descitption);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--InertUpdateDescription", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool InsertUpdateSocialMedia(int UserId, SocialMediaForDoctorViewModel socialMedia)
        {
            try
            {
                return new clsColleaguesData().UpdateSocialMediaDetailsByUserId(UserId, socialMedia.FacebookUrl, socialMedia.LinkedinUrl, socialMedia.TwitterUrl, socialMedia.InstagramUrl, socialMedia.GoogleplusUrl, socialMedia.YoutubeUrl, socialMedia.PinterestUrl, socialMedia.BlogUrl, socialMedia.YelpUrl);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--InsertUpdateSocialMedia", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool InsertUpdateWebSiteURL(int UserId, WebsiteDetails website, int? LocationId)
        {
            try
            {
                return new clsColleaguesData().UpdateDoctorWebsiteUrlById(SafeValue<int>(UserId), website.SecondaryWebsiteurl, website.SecondaryWebsiteId, LocationId);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--InsertUpdateWebSiteURL", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static List<TimeZones> GetTimeZoneList()
        {
            try
            {
                List<TimeZones> list = new List<TimeZones>();
                DataTable dt = new clsColleaguesData().GetTimeZones();
                if (dt != null && dt.Rows.Count > 0)
                {
                    list = (from p in dt.AsEnumerable()
                            select new TimeZones
                            {
                                TimeZoneId = SafeValue<int>(p["TimeZoneId"]),
                                TimeZoneText = SafeValue<string>(p["TimeZoneText"])
                            }).ToList();
                }
                return list;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--GetTimeZoneList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool UpdatePersonalInfo(DentistProfileViewModel Obj)
        {
            try
            {
                return new clsColleaguesData().UpdateDoctorPersonalInformationFromProfile(Obj.UserId, Obj.FirstName, Obj.MiddleName, Obj.LastName, Obj.OfficeName, Obj.Title, Obj.specialtyIds, Obj.Salutation);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--UpdatePersonalInfo", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static List<University> SearchUniversityNames(string UniversityNames)
        {
            try
            {
                List<University> lst = new List<University>();
                DataTable dt = new DataTable();
                dt = new clsCommon().SearchUniversityNames(UniversityNames);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lst.Add(new University { text = clsCommon.SafeValue<string>(item["UniversityName"]), id = clsCommon.SafeValue<string>(item["UniversityName"]) });
                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--UpdatePersonalInfo", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static string AddProfileImage(DentistProfileImage dentistProfile, int UserId)
        {
            try
            {
                byte[] bytes = Convert.FromBase64String(dentistProfile.Base64URL);
                if (!Directory.Exists(ConfigurationManager.AppSettings.Get("APITempFolder")))
                    Directory.CreateDirectory(ConfigurationManager.AppSettings.Get("APITempFolder"));
                string imgName = $"$" + DateTime.Now.Ticks + "$" + UserId + ".jpg";
                string FullPathImage = ConfigurationManager.AppSettings.Get("APITempFolder");
                using (var imageFile = new FileStream(Path.Combine(FullPathImage, imgName), FileMode.Create))
                {
                    imageFile.Write(bytes, 0, bytes.Length);
                    imageFile.Flush();
                }
                var Byts = File.ReadAllBytes(ConfigurationManager.AppSettings.Get("APITempFolder") + "\\" + imgName);
                string thambnailimage = imgName; string extension = Path.GetExtension(imgName);
                if (extension.ToLower() == ".jpg" || extension.ToLower() == ".jpeg" || extension.ToLower() == ".gif" || extension.ToLower() == ".png" || extension.ToLower() == ".bmp" || extension.ToLower() == ".psd" || extension.ToLower() == ".pspimage" || extension.ToLower() == ".thm" || extension.ToLower() == ".tif")
                {
                    int Bottom = SafeValue<int>(Math.Floor(dentistProfile.Bottom));
                    int Right = SafeValue<int>(Math.Floor(dentistProfile.Right));
                    int Top = SafeValue<int>(Math.Floor(dentistProfile.Top));
                    int Left = SafeValue<int>(Math.Floor(dentistProfile.Left));
                    var imagecrop = new WebImage(Byts);// tempPath + filename1);
                    var height = imagecrop.Height;
                    var width = imagecrop.Width;
                    imagecrop.Crop(Top, Left, height - Bottom, width - Right);
                    int maxPixels = SafeValue<int>(ConfigurationManager.AppSettings["Thumbimagediamention"]);
                    imagecrop.Resize(maxPixels, maxPixels, true, false);
                    imagecrop.Save(ConfigurationManager.AppSettings.Get("DentistImage") + @"\" + thambnailimage, null, false);
                    new clsColleaguesData().UpdateDoctorProfileImageById(UserId, imgName);
                }
                //return ConfigurationManager.AppSettings.Get("DoctorImage")+imgName+".png"; - session image issues solve
                return ConfigurationManager.AppSettings.Get("DoctorImage") + imgName;

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--AddProfileImage", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static WebsiteDetails GetWebSiteDetailById(int Id, int UserId)
        {
            try
            {
                WebsiteDetails Obj = new WebsiteDetails();
                DataTable Dt = clsColleaguesData.GetWebsiteDetail(Id, UserId);
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    Obj.SecondaryWebsiteurl = clsCommon.SafeValue<string>(Dt.Rows[0]["Website"]);
                    Obj.SecondaryWebsiteId = clsCommon.SafeValue<int>(Dt.Rows[0]["WebsiteId"]);
                }
                return Obj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--GetWebSiteDetailById", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool RemoveWebsite(int Id, int UserId)
        {
            try
            {
                return (Id == 0) ? new clsColleaguesData().RemovePrimaryWebSite(UserId) : new clsColleaguesData().RemoveSecondaryWebSite(Id, UserId);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--RemoveWebsite", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool RemoveProfileImage(int UserId)
        {
            try
            {
                //  return  new clsColleaguesData().RemovePrimaryWebSite(UserId) : new clsColleaguesData().RemoveSecondaryWebSite(Id, UserId);
                return clsColleaguesData.RemoveProfileImage(UserId);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--RemoveWebsite", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static string UpdatePrimarySecondaryWebsite(WebSiteUrl Obj)
        {
            try
            {
                string Result = string.Empty;
                if (Obj.WebsiteId == 0)
                {
                    DataTable dt = new DataTable();
                    string qry = "Update Member set WebsiteURL='" + new clsCommon().CheckNull(Obj.WebsiteUrl, string.Empty) + "',LastModifiedDate = '" + DateTime.Now + "',ProfilesLastUpdated = '" + DateTime.Now + "' where userid='" + new clsCommon().CheckNull(Convert.ToString(Obj.UserId), string.Empty) + "'";
                    dt = new clsCommon().DataTable(qry);
                    if (dt == null)
                    {
                        return "1";
                    }
                    else
                    {
                        return "0";
                    }

                }
                else
                {
                    DataTable dt = new DataTable();
                    string qry = "IF NOT EXISTS(Select * from SecondaryDetails where userid = " + Obj.UserId +
                                    " and WebSite = '" + Obj.WebsiteUrl + "' and secondaryid != '" + Obj.WebsiteId +
                                    "' ) update SecondaryDetails set WebSite = '" + new clsCommon().CheckNull(Obj.WebsiteUrl, string.Empty) +
                                    "' where UserId = '" + new clsCommon().CheckNull(Convert.ToString(Obj.UserId), string.Empty) + "' and SecondaryId = '" +
                                    new clsCommon().CheckNull(Convert.ToString(Obj.WebsiteId), string.Empty) + "'"
                                    + " else (Select * from SecondaryDetails where userid = " + Convert.ToString(Obj.UserId) +
                                    " and WebSite = '" + Obj.WebsiteUrl + "' and secondaryid != '" + Obj.WebsiteId +
                                    "' ) ";
                    // string qry = "update SecondaryDetails set WebSite = '" + objCommon.CheckNull(Websiteurl, string.Empty) + "' where UserId = '" + objCommon.CheckNull(UserId, string.Empty) + "' and SecondaryId = '" + objCommon.CheckNull(WebsiteId, string.Empty) + "'";
                    dt = new clsCommon().DataTable(qry);
                    if (dt == null)
                    {
                        return "1";
                    }
                    else
                    {
                        return "0";
                    }
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ProfileBLL--UpdatePrimarySecondaryWebsite", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static Insurance GetInsuranceProviderDetail(int userId, int LocationId = 0)
        {
            Insurance model = new Insurance();
            // objProfile = new ProfileBLL();
            model.lstInsurance = new ProfileBLL().GetInsuranceDetail();
            List<Insurance> UserData = new List<Insurance>();
            UserData = new ProfileBLL().GetMemberInsuranceByUserId(userId, LocationId);
            foreach (var item in UserData)
            {
                //var InsuranceId = model.lstInsurance.Where(t => t.InsuranceId == item.InsuranceId).FirstOrDefault().InsuranceId;
                model.lstInsurance.Where(t => t.InsuranceId == item.InsuranceId).FirstOrDefault().Ischeck = true;
            }
            model.LocationId = LocationId;
            return model;
        }

        public static bool InsertUpdateInsuranceProviderListByLocation(Insurance insurance)
        {

            //ProfileBLL objProfile = new ProfileBLL();
            bool result;

            //if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || insurance.UserId == 0)
            //    insurance.UserId = Convert.ToInt32(SessionManagement.UserId);


            result = new clsProfileData().RemoveInsuranceMember(insurance.UserId, insurance.LocationId);
            foreach (var item in insurance.lstInsurance)
            {
                if (item.Ischeck == true)
                {
                    item.Member_InsuranceId = 0;
                    item.UserId = insurance.UserId;
                    item.LocationId = insurance.LocationId;
                    result = new clsProfileData().InsertInsuranceMember(item);
                }
            }
            return true;
        }

        public static List<InsuranceLocation> GetAllProviderListByUserId(int UserId)

        {
            List<InsuranceLocation> objInsuranceLocationList = new List<InsuranceLocation>();
            List<AddressDetails> objAddressDetailsList = new List<AddressDetails>();
            objAddressDetailsList = ColleaguesBLL.GetDentistLocationsById(Convert.ToString(UserId));
            for (int i = 0; i < objAddressDetailsList.Count; i++)
            {
                InsuranceLocation objInsuranceLocation = new InsuranceLocation();
                objInsuranceLocation.LocationId = objAddressDetailsList[i].AddressInfoID;
                objInsuranceLocation.LocationName = objAddressDetailsList[i].Location;

                DataTable dt = new DataTable();
                List<InsuranceProvider> lstInsuranceProvider = new List<InsuranceProvider>();
                try
                {
                    dt = new clsProfileData().GetAllProviderListByUserId(UserId, objInsuranceLocation.LocationId);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            lstInsuranceProvider.Add(new InsuranceProvider
                            {
                                InsuranceId = SafeValue<int>(dt.Rows[j]["Id"]),
                                InsuranceName = SafeValue<string>(dt.Rows[j]["Name"])
                            });
                        }
                    }

                    objInsuranceLocation.objInsuranceProvider = lstInsuranceProvider;

                }
                catch (Exception ex)
                {
                    throw;
                }
                objInsuranceLocationList.Add(objInsuranceLocation);
            }
            return objInsuranceLocationList;
        }
        public static bool RemoveInsuranceProvider(int UserId, int InsuranceId)
        {

            try
            {
                return new clsProfileData().RemoveInsuranceProvider(UserId, InsuranceId);

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Get Member Public Profile Settings base on User Id
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns>Member Public Profile Settings</returns>
        public static ProfileSection GetMemberPublicProfileSettings(int UserId)
        {
            ProfileSection ProfileSection = new ProfileSection();
            try
            {
                DataTable dt = new DataTable();
                dt = new clsProfileData().GetMemberPublicProfileSettings(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ProfileSection.SectionId = SafeValue<int>(dt.Rows[0]["SectionID"]);
                    ProfileSection.UserId = SafeValue<int>(dt.Rows[0]["UserId"]);
                    ProfileSection.PatientForms = SafeValue<bool>(dt.Rows[0]["PatientForms"]);
                    ProfileSection.SpecialOffers = SafeValue<bool>(dt.Rows[0]["SpecialOffers"]);
                    ProfileSection.ReferPatient = SafeValue<bool>(dt.Rows[0]["ReferPatient"]);
                    ProfileSection.Reviews = SafeValue<bool>(dt.Rows[0]["Reviews"]);
                    ProfileSection.AppointmentBooking = SafeValue<bool>(dt.Rows[0]["AppointmentBooking"]);
                    ProfileSection.PatientLogin = SafeValue<bool>(dt.Rows[0]["PatientLogin"]);
                    ProfileSection.PublicProfileUrl = SafeValue<string>(dt.Rows[0]["PublicProfileUrl"]);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return ProfileSection;
        }

        /// <summary>
        /// update public profile url base on userid
        /// </summary>
        /// <returns>bool value</returns>
        public static bool UpdatePublicProfileURL(ProfileSection profileSection)
        {
            bool result;
            result = new clsProfileData().UpdatePublicProfileURL(profileSection);
            
            return true;
        }

    }
}
