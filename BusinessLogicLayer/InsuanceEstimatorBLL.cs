﻿using DataAccessLayer.PatientsData;
using System;
using System.Collections.Generic;
using BO.ViewModel;
using System.Data;
using DataAccessLayer.ColleaguesData;
using BO.Models;
using DataAccessLayer.ProfileData;
using System.Web.Mvc;
using DataAccessLayer.Common;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public class InsuanceEstimatorBLL
    {
        clsCommon objcommon = new clsCommon();
        public static List<InsurnaceCompaniesModel> GetInsuranceCompaniesList(int userid)
        {
            List<InsurnaceCompaniesModel> list = new List<InsurnaceCompaniesModel>();
            DataTable dt = clsColleaguesData.GetInsuranceCompaniesOfUser(userid);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    list.Add(new InsurnaceCompaniesModel()
                    {
                        InsuranceId = SafeValue<int>(row["InsuranceId"]),
                        InsuranceCompanyName = SafeValue<string>(row["InsuranceCompanyName"])
                    });
                }
            }

            return list;
        }
        //Olivia Changes on 10-04-2018 for Dr. Lan Allan's website. - DLADCETK-1
        public static string GetAppointmentLink(int userid)
        {
            string link = clsColleaguesData.GetAppointmentLink(userid);
            link = string.IsNullOrEmpty(link) ? string.Empty : link;
            return link;
        }

        public static List<DentalProceduresModel> GetDentalProceduresList(int userid)
        {
            List<DentalProceduresModel> list = new List<DentalProceduresModel>();
            DataTable dt = clsColleaguesData.GetDentalProceduresListOfUser(userid);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {

                    list.Add(new DentalProceduresModel()
                    {
                        ProcedureId = SafeValue<int>(row["ProcedureId"]),
                        ProcedureName = SafeValue<string>(row["ProcedureName"]),
                        InsuranceCostRatio = SafeValue<decimal>(row["InsuranceCostRatio"])

                    });
                }
            }

            return list;
        }

        // RM-904
        public static List<StateMaster> GetStateList()
        {
            List<StateMaster> list = new List<StateMaster>();
            DataTable dt = clsColleaguesData.GetstateListOfUser();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    list.Add(new StateMaster()
                    {
                        StateName = Convert.ToString(row[0]),

                    });
                }
            }
            return list;
        }


        public static CalculateInsuranceModel CalculateInsurance(CalculateInsuranceModel model, int UserId)
        {
            clsProfileData ObjprofileData = new clsProfileData();
            DataSet ds = new DataSet();
            ds = ObjprofileData.GetTotalCostEstimation(model, UserId);
            string proceduresname = string.Empty;

            if (ds != null && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    model.TotalCost = SafeValue<decimal>(ds.Tables[0].Rows[0]["TotalCost"]);
                    model.ServiceCost = SafeValue<decimal>(ds.Tables[0].Rows[0]["ServiceCost"]);
                    model.ThreePayments = SafeValue<decimal>(ds.Tables[0].Rows[0]["ThreePayments"]);
                    model.PaymentEMI = SafeValue<decimal>(ds.Tables[0].Rows[0]["PaymentEMI"]);
                    model.HistoryId = SafeValue<int>(ds.Tables[0].Rows[0]["HistoryId"]);
                    model.InsuranceCompanyName = SafeValue<string>(ds.Tables[0].Rows[0]["CompanyName"]);

                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[1].Rows)
                    {
                        proceduresname += SafeValue<string>(item["ProcedureName"]) + ",";
                    }
                    proceduresname = proceduresname.TrimEnd(',');
                }
                model.ProceduresName = proceduresname;
            }

            clsCommon common = new clsCommon();
            //#region Logic to call via Twillio
            //try
            //{
            //    DataTable dt = new clsColleaguesData().GetDoctorDetailsbyId(UserId);
            //    if (dt != null && dt.Rows.Count > 0 && (!string.IsNullOrWhiteSpace(model.EstimateViaCall) && common.IsValidPhone(model.EstimateViaCall)))
            //    {
            //        string DoctorPhone = SafeValue<string>(dt.Rows[0]["Phone"]);
            //        DoctorPhone = !string.IsNullOrEmpty(DoctorPhone) ? DoctorPhone : "Not Available";
            //        TwilioCallBLL.CostEstimatorCall(model.EstimateViaCall, model.TotalCost, DoctorPhone, UserId);
            //    }
            //}
            //catch (Exception)
            //{

            //}
            //#endregion

            //#region Logic to SMS via Twillio
            //try
            //{
            //    if (!string.IsNullOrEmpty(model.EstimateViaText) && common.IsValidPhone(model.EstimateViaText))
            //    {
            //        TwilioSMSBLL.CostEstimatorSMS(model.EstimateViaText, model.TotalCost, proceduresname, UserId);
            //    }
            //}
            //catch(Exception)
            //{

            //}
            //#endregion

            #region Logic to Send Email 
            //try
            //{
            //    if (!string.IsNullOrEmpty(model.EstimateViaEmail) && common.IsEmail(model.EstimateViaEmail))
            //    {
            new clsTemplate().SendCostEstimationToUser(model, UserId);
            //    }
            //}
            //catch(Exception)
            //{

            //}
            #endregion
            return model;
        }

        public static bool SendCostEstimationRequest(SendInsuranceRequestBO model, int userId)
        {
            bool result = false;
            if (clsColleaguesData.UpdateDentalInsurananceHistory(model))
            {
                clsTemplate obj = new clsTemplate();
                result = obj.SendCostEstimationRequest(model, userId);
            }

            return result;
        }
        public static List<EstimateReport> GetPatientEstimateReport(int UserId)
        {
            DataTable dt = new DataTable();
            clsCommon objcommon = new clsCommon();
            List<EstimateReport> lst = new List<EstimateReport>();
            dt = new clsPatientsData().GetPatientCostEstimation(UserId);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    string[] data = SafeValue<string>(item["CreationDate"]).Split(' ');
                    lst.Add(new EstimateReport()
                    {
                        Name = SafeValue<string>(item["PatientName"]),
                        ProcedureName = SafeValue<string>(item["ProcedureName"]).Replace("||", ","), 
                        EstimateViaCall = SafeValue<string>(item["EstimateViaCall"]), 
                        EstimateViaEmail = SafeValue<string>(item["EstimateViaEmail"]), 
                        EstimateViaText = SafeValue<string>(item["EstimateViaText"]),
                        InsuranceName = SafeValue<string>(item["InsuranceName"]).Replace("/", ","), 
                        HistoryId = SafeValue<int>(item["InsuranceHistoryId"]),
                        ServiceCost = SafeValue<decimal>(item["ServiceCost"]),
                        TotalCost = SafeValue<decimal>(item["TotalCost"]),
                        Date = SafeValue<string>(data[0]),
                    });
                }
            }
            return lst;
        }
        public static InsuranceProcedureMapping GetInsuranceCostEstimatorPivot(int UserId)
        {
            InsuranceProcedureMapping Obj = new InsuranceProcedureMapping();
            DataTable dt = new DataTable();
            dt = new clsColleaguesData().GetPivotQueryforInsuranceCostEstimator(UserId);
            if(dt != null && dt.Rows.Count > 0)
            {
                Obj.ColumnName = dt;
            }
            return Obj;
        }
        public static Insurance GetInsuraceDetialsById(int InsuranceId)
        {
            Insurance Obj = new Insurance();
            DataTable dt = new DataTable();
            dt = new clsColleaguesData().GetInsuranceCompanyNamebyId(InsuranceId);
            if(dt != null && dt.Rows.Count > 0)
            {
                Obj.InsuranceId = SafeValue<int>(dt.Rows[0]["InsuranceId"]);
                Obj.Name = SafeValue<string>(dt.Rows[0]["Name"]);
            }
            return Obj;
        }
        public static Procedure GetProcedureDetialsById(int ProcedureId)
        {
            Procedure obj = new Procedure();
            DataTable dt = new DataTable();
            dt = new clsColleaguesData().GetProcedureDetials(ProcedureId);
            if(dt != null && dt.Rows.Count > 0)
            {
                obj.ProcedureId = SafeValue<int>(dt.Rows[0]["ProcedureId"]);
                obj.ProcedureName = SafeValue<string>(dt.Rows[0]["ProcedureName"]);
            }
            return obj;
        }
        public static bool InsertProcedureInsurenceData(List<InsuranceProcedureMapp> Lst,int UserId)
        {
            return new clsColleaguesData().InsertProcedureandInsurenceMappingData(Lst, UserId);
        }
    }
}
