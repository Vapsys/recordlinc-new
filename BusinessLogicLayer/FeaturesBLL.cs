﻿using BO.Models;
using DataAccessLayer.Admin;
using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
   public class FeaturesBLL
    {
        FeaturesDLL objDAL = new FeaturesDLL();
        clsCommon ObjCommon = new clsCommon();
        #region Features
        public bool InsertUpdate(Features obj)
        {
            return objDAL.InsertUpdate(obj);
        }

        public List<Features> GetAll(int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext)
        {
            DataTable dt = new DataTable();
            List<Features> objlst = new List<Features>();
            try
            {
                dt = objDAL.GetAll(PageIndex, PageSize, SortColumn, SortDirection, Searchtext);
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        objlst.Add(new Features
                        {
                            Feature_Id = SafeValue<int>(dt.Rows[i]["Feature_Id"]),
                            Title = SafeValue<string>(dt.Rows[i]["Title"]), 
                            SortOrder = SafeValue<int>(dt.Rows[i]["SortOrder"]),
                            Status = SafeValue<int>(dt.Rows[i]["Status"]),
                            CreatedBy = SafeValue<int>(dt.Rows[i]["CreatedBy"]),
                            ModifiedBy = SafeValue<int>(dt.Rows[i]["ModifiedBy"]),
                        });
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objlst;
        }

        public Features GetById(int Id)
        {
            DataTable dt = new DataTable();
            Features objcreate = new Features();
            try
            {
                dt = objDAL.GetById(Id);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objcreate.Feature_Id = SafeValue<int>(dt.Rows[0]["Feature_Id"]);
                    objcreate.Title = SafeValue<string>(dt.Rows[0]["Title"]);
                    objcreate.SortOrder = SafeValue<int>(dt.Rows[0]["SortOrder"]);
                    objcreate.Status = SafeValue<int>(dt.Rows[0]["Status"]);
                    objcreate.CreatedBy = SafeValue<int>(dt.Rows[0]["CreatedBy"]);
                    objcreate.ModifiedBy = SafeValue<int>(dt.Rows[0]["ModifiedBy"]);

                }
            }
            catch (Exception)
            {
                throw;
            }
            return objcreate;
        }

        public bool Remove(int Id)
        {
            return objDAL.RemoveById(Id);
        }
        public int GetMaxSortOrder()
        {

            return ObjCommon.GetMaxSortOrder("Features");
        }
        public List<object> GetStatusList()
        {
            var enumVals = new List<object>();
            foreach (var item in Enum.GetValues(typeof(BO.Enums.Common.StatusType)))
            {
                enumVals.Add(new
                {
                    id = (int)item,
                    name = SafeValue<string>(item)
                });
            }
            return enumVals;
        }


        #endregion
    }
}
