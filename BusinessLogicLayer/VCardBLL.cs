﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.ProfileData;
using DataAccessLayer.Common;
using System.Data;
using BO.Models;
using System.IO;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public class VCardBLL
    {
        clsProfileData ObjProfile = new clsProfileData();
        clsCommon ObjCommon = new clsCommon();

        public VCardForDoctor GetVCardDetialsOfDoctor(int UserId)
        {
            VCardForDoctor ObjVcard = new VCardForDoctor();
            try
            {
                DataTable dt = new DataTable();
                dt = ObjProfile.GetVcardDetialsofDoctor(UserId);
                if(dt!=null &&dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        ObjVcard.Userid = SafeValue<int>(item["UserId"]);
                        ObjVcard.FName = SafeValue<string>(item["FirstName"]);
                        ObjVcard.LName = SafeValue<string>(item["LastName"]);
                        ObjVcard.Company = SafeValue<string>(item["AccountName"]);
                        ObjVcard.JobTitle = SafeValue<string>(item["Title"]);
                        ObjVcard.Address = SafeValue<string>(item["ExactAddress"])+SafeValue<string>(item["Address2"]);
                        ObjVcard.City = SafeValue<string>(item["City"]);
                        ObjVcard.State = SafeValue<string>(item["State"]);
                        ObjVcard.Mobile = SafeValue<string>(item["Phone"]);
                        ObjVcard.Phone = SafeValue<string>(item["Phone2"]);
                        ObjVcard.Email = SafeValue<string>(item["Username"]);
                        ObjVcard.ImageName = SafeValue<string>(item["ImageName"]);
                        ObjVcard.FacebookUrl = SafeValue<string>(item["FacebookUrl"]);
                        ObjVcard.GoogleplusUrl = SafeValue<string>(item["GoogleplusUrl"]);
                        ObjVcard.LinkedinUrl = SafeValue<string>(item["LinkedinUrl"]);
                        ObjVcard.TwitterUrl = SafeValue<string>(item["TwitterUrl"]);
                        ObjVcard.PinterestUrl = SafeValue<string>(item["PinterestUrl"]);
                        ObjVcard.YoutubeUrl = SafeValue<string>(item["YoutubeUrl"]);
                        ObjVcard.BlogUrl = SafeValue<string>(item["BlogUrl"]);
                        ObjVcard.YelpUrl = SafeValue<string>(item["YelpUrl"]);
                        ObjVcard.WebSite = SafeValue<string>(item["WebsiteURL"]);
                    }
                }
                return ObjVcard;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("VCardBLL--GetVCardDetialsOfDoctor", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public string BulidVCard(VCardForDoctor ObjVcard)
        {
            try
            {
                StringBuilder strvCardBuilder = new StringBuilder();
                strvCardBuilder.AppendLine("BEGIN:VCARD");
                strvCardBuilder.AppendLine("VERSION:2.1");
                strvCardBuilder.AppendLine("N:" + ObjVcard.LName + ";" + ObjVcard.FName);
                strvCardBuilder.AppendLine("FN:" + ObjVcard.FName + " " + ObjVcard.LName);
                strvCardBuilder.Append("ADR;HOME;PREF:;;");
                strvCardBuilder.Append(ObjVcard.Address + ";");
                strvCardBuilder.Append(ObjVcard.City + ";;");
                strvCardBuilder.AppendLine(ObjVcard.Country);
                if(!string.IsNullOrWhiteSpace(ObjVcard.Company))
                    strvCardBuilder.AppendLine("ORG:" + ObjVcard.Company);
                if(!string.IsNullOrWhiteSpace(ObjVcard.JobTitle))
                    strvCardBuilder.AppendLine("TITLE:" + ObjVcard.JobTitle);
                if(!string.IsNullOrWhiteSpace(ObjVcard.Phone))
                    strvCardBuilder.AppendLine("TEL;HOME;VOICE:" + ObjVcard.Phone);
                strvCardBuilder.AppendLine("TEL;CELL;VOICE:" + ObjVcard.Mobile);
                strvCardBuilder.AppendLine("EMAIL;type=INTERNET;type=WORK;type=pref:" + ObjVcard.Email);
                strvCardBuilder.AppendLine("END:VCARD");
                return SafeValue<string>(strvCardBuilder);
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("VCardBLL--GetVCardDetialsOfDoctor", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
