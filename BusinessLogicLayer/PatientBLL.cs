using System;
using System.Collections.Generic;
using System.Linq;
using BO.Models;
using DataAccessLayer.PatientsData;
using DataAccessLayer.Common;
using System.Data;
using BO.ViewModel;
using System.Configuration;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.SmileBrand;
using static BO.Enums.Common;
using System.Web;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.IO;
using System.Security.Principal;
using System.Security.AccessControl;
using System.Drawing;
using static DataAccessLayer.Common.clsCommon;
using System.Web.Helpers;
using DataAccessLayer.Appointment;
using ICSharpCode.SharpZipLib.Zip;
using System.Web.Script.Serialization;

namespace BusinessLogicLayer
{
    public class PatientBLL
    {
        #region Global Declaration
        public static string TempFile = ConfigurationManager.AppSettings["TempFileUpload"];
        public static string ImageBankFiles = ConfigurationManager.AppSettings["ImageBankFileUpload"];
        public static string ThumbFiles = ConfigurationManager.AppSettings["ThumbFileUpload"];
        public static string DraftFiles = ConfigurationManager.AppSettings["DraftFileUpload"];
        static clsCommon ObjCommon = new clsCommon();
        public List<NewPatientDetailsOfDoctor> lstPatientDetailsOfDoctor = new List<NewPatientDetailsOfDoctor>();
        clsPatientsData objPatientsData = new clsPatientsData();
        clsCommon objCommon = new clsCommon();
        static List<DentalHistorys> PatientListDentalHistoryForm = new List<DentalHistorys>();
        static List<MedicalHistory> PatientListMedicalHistoryForm = new List<MedicalHistory>();
        #endregion

        public static int PatientInsertUpdateNew(Patients newPatientObj)
        {
            int PatientId = 0;
            clsPatientsData objPatientsData = new clsPatientsData();
            try
            {
                DateTime? dt_DateOfBirth = null;
                if (!string.IsNullOrEmpty(newPatientObj.DateOfBirth))
                {
                    dt_DateOfBirth = SafeValue<DateTime>(newPatientObj.DateOfBirth);
                    newPatientObj.DateOfBirth = SafeValue<string>(dt_DateOfBirth);
                }
                newPatientObj.Password = ObjCommon.CreateRandomPassword(8);
                if (!string.IsNullOrWhiteSpace(newPatientObj.Notes))
                {
                    newPatientObj.Notes = newPatientObj.Notes.Replace("[^A-Za-z0-9]", "");
                    if (newPatientObj.Notes.Length > 200)
                    {
                        newPatientObj.Notes = newPatientObj.Notes.Substring(0, Math.Min(200, newPatientObj.Notes.Length));
                    }
                }
                return PatientId = objPatientsData.PatientInsertAndUpdateNew(newPatientObj);
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("PatientBLL---PatientInsertUpdateNew", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static Patients GetPatientDetailsById(int PatientId, string DoctorId)
        {
            try
            {
                Patients mdlNewPateint = new Patients();
                clsPatientsData ObjPatientsData = new clsPatientsData();
                DataSet ds = new DataSet();
                int? intDoc = null;
                if (!string.IsNullOrEmpty(DoctorId))
                {
                    intDoc = SafeValue<int>(DoctorId);
                }
                ds = ObjPatientsData.GetPateintDetailsByPatientId(PatientId, intDoc);
                if (ds != null && ds.Tables.Count > 0)
                {
                    DataTable dt = ds.Tables[0];
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        mdlNewPateint.PatientId = SafeValue<int>(dt.Rows[0]["PatientId"]);
                        mdlNewPateint.AssignedPatientId = SafeValue<string>(dt.Rows[0]["AssignedPatientId"]);
                        if (mdlNewPateint.AssignedPatientId == "0")
                        {
                            mdlNewPateint.AssignedPatientId = "";
                        }
                        mdlNewPateint.FirstName = SafeValue<string>(dt.Rows[0]["FirstName"]);
                        mdlNewPateint.LastName = SafeValue<string>(dt.Rows[0]["LastName"]);
                        if (SafeValue<string>(dt.Rows[0]["DateOfBirth"]) != null && SafeValue<string>(dt.Rows[0]["DateOfBirth"]) != "")
                        {
                            //RM-294 Changes for adding a leading zero in mdlNewPateint.DateOfBirth
                            DateTime DOB = SafeValue<DateTime>(dt.Rows[0]["DateOfBirth"]);
                            mdlNewPateint.DateOfBirth = new CommonBLL().ConvertToDateNew(SafeValue<DateTime>(DOB), SafeValue<int>(BO.Enums.Common.DateFormat.GENERAL));
                        }
                        mdlNewPateint.Email = SafeValue<string>(dt.Rows[0]["Email"]);
                        mdlNewPateint.Age = SafeValue<string>(dt.Rows[0]["Age"]);
                        mdlNewPateint.Gender = SafeValue<string>(dt.Rows[0]["Gender"]);
                        mdlNewPateint.ExactAddress = SafeValue<string>(dt.Rows[0]["ExactAddress"]);
                        mdlNewPateint.Address2 = SafeValue<string>(dt.Rows[0]["Address2"]);
                        mdlNewPateint.City = SafeValue<string>(dt.Rows[0]["City"]);
                        mdlNewPateint.State = SafeValue<string>(dt.Rows[0]["State"]);
                        mdlNewPateint.ZipCode = SafeValue<string>(dt.Rows[0]["ZipCode"]);
                        if (SafeValue<string>(dt.Rows[0]["Mobile"]) != null && SafeValue<string>(dt.Rows[0]["Mobile"]) != "")
                        {
                            mdlNewPateint.Phone = SafeValue<string>(dt.Rows[0]["Mobile"]);
                        }
                        else
                        {
                            mdlNewPateint.Phone = SafeValue<string>(dt.Rows[0]["Phone"]);
                        }
                        mdlNewPateint.Notes = SafeValue<string>(dt.Rows[0]["Notes"]);
                        mdlNewPateint.Country = SafeValue<string>(dt.Rows[0]["Country"]);
                        mdlNewPateint.ReferredBy = SafeValue<string>(dt.Rows[0]["ReferredById"]);
                        if (SafeValue<string>(dt.Rows[0]["FirstExamDate"]) != null && SafeValue<string>(dt.Rows[0]["FirstExamDate"]) != "")
                        {
                            //RM-294 Changes for adding a leading zero in mdlNewPateint.DateOfBirth
                            mdlNewPateint.FirstExamDate = new CommonBLL().ConvertToDateNew(SafeValue<DateTime>(dt.Rows[0]["FirstExamDate"]), SafeValue<int>(BO.Enums.Common.DateFormat.GENERAL));
                        }
                        // mdlNewPateint.ReferredByName = SafeValue<string>(dt.Rows[0]["ReferredBy"]), string.Empty);
                        //  ViewBag.country = CountryScriptNew(SafeValue<string>(dt.Rows[0]["Country"]), string.Empty));
                        //  ViewBag.state = StateScriptNew("", SafeValue<string>(dt.Rows[0]["State"]), "0"));
                        mdlNewPateint.ColleagueId = SafeValue<string>(dt.Rows[0]["ReferredById"]);
                        mdlNewPateint.LocationId = SafeValue<string>(dt.Rows[0]["Location"]);
                        mdlNewPateint.StateId = SafeValue<string>(dt.Rows[0]["State"]);
                        mdlNewPateint.SecondaryPhone = SafeValue<string>(dt.Rows[0]["SecondaryPhone"]);
                    }
                }
             //   DataTable dtInsurance = clsPatientsData.GetPatientInsuranceDetails(PatientId);
                mdlNewPateint.ItHasInsuranceData = false;
                mdlNewPateint._insuranceCoverage = ReferralFormBLL.GetInsuranceDetails(PatientId);
                mdlNewPateint._medicalHistory = ReferralFormBLL.GetMedicalHistoryDetails(PatientId);
                mdlNewPateint._dentalhistory = ReferralFormBLL.GetDentalHistoryDetails(PatientId);
                return mdlNewPateint;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("PatientBLL---GetPatientDetailsById", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        public static bool UpdatePatientDetails(PatientEditableVM obj)
        {
            try
            {
                bool IsUpdated = false;
                if (clsPatientsData.UpdatePatientDetails(obj))
                {
                    IsUpdated = true;
                    return IsUpdated;
                }
                else
                {
                    return IsUpdated;
                }
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("PatientBLL--UpdatePatientDetails", ex.Message, ex.StackTrace);
                throw ex;
            }
        }

        public static bool CheckAssignPatientId(int AssignedPatientId, int PatientId, int UserId)
        {
            DataTable dt = new DataTable();
            bool result = false;
            dt = clsPatientsData.CheckAssignPatientId(AssignedPatientId, PatientId, UserId);
            result = (dt.Rows.Count > 0) ? true : false;
            return result;
        }

        //New GetPatientDetailsOf Docotr.
        public static List<NewPatientDetailsOfDoctor> NewGetPatientDetailsOfDoctor(PatientFilter FilterObj)
        {
            DataTable dt = new DataTable();
            clsCommon objCommon = new clsCommon();
            List<NewPatientDetailsOfDoctor> lstPatientDetailsOfDoctor = new List<NewPatientDetailsOfDoctor>();
            clsPatientsData objPatientsData = new clsPatientsData();
            bool ItPatientHasInsuranceData = false;
            try
            {
                dt = objPatientsData.NewGetPatientDetailsOfDoctor(FilterObj);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow p in dt.Rows)
                    {
                        //string DOB = SafeValue<string>(p["DateOfBirth"]);
                        //if (!string.IsNullOrEmpty(DOB))
                        //{
                        //    DateTime? data = SafeValue<DateTime>(DOB);
                        //    TimeSpan timeofDay = data.Value.TimeOfDay;
                        //    if (timeofDay != TimeSpan.Zero)
                        //    {
                        //        DOB = new CommonBLL().ConvertToDate(SafeValue<DateTime>(DOB), SafeValue<int>(BO.Enums.Common.DateFormat.GENERAL));
                        //    }
                        //}


                        ItPatientHasInsuranceData = objPatientsData.PatientHasInsuranceData(SafeValue<int>(p["PatientId"]));
                        string strDOB = string.Empty;
                        DateTime DOB = SafeValue<DateTime>(p["DateOfBirth"]);
                        if (DOB != DateTime.MinValue)
                        {
                            strDOB = new CommonBLL().ConvertToDate(SafeValue<DateTime>(DOB), SafeValue<int>(DateFormat.GENERAL));
                        }

                        lstPatientDetailsOfDoctor.Add(new NewPatientDetailsOfDoctor()
                        {
                            PatientId = SafeValue<int>(p["PatientId"]),
                            AssignedPatientId = SafeValue<string>(p["AssignedPatientId"]),
                            ItHasInsuranceData = ItPatientHasInsuranceData,
                            FirstName = SafeValue<string>(p["FirstName"]).Trim(),
                            LastName = SafeValue<string>(p["LastName"]).Trim(),
                            Email = SafeValue<string>(p["Email"]).Trim(),
                            Gender = SafeValue<string>(p["Gender"]),
                            Age = SafeValue<string>(p["Age"]),
                            Phone = SafeValue<string>(p["Phone"]).Trim(),
                            DateOfBirth = strDOB,
                            ExactAddress = SafeValue<string>(p["ExactAddress"]).Trim(),
                            Address2 = SafeValue<string>(p["Address2"]).Trim(),
                            City = SafeValue<string>(p["City"]).Trim(),
                            State = SafeValue<string>(p["State"]),
                            ZipCode = SafeValue<string>(p["ZipCode"]).Trim(),
                            ImageName = SafeValue<string>(p["ProfileImage"]) != string.Empty ? SafeValue<string>(ConfigurationManager.AppSettings["ImageBank"]) + SafeValue<string>(p["ProfileImage"]) : "",
                            TotalRecord = SafeValue<int>(p["TotalRecord"]),
                            OwnerId = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(p["OwnerId"])) ? 0 : p["OwnerId"]),
                            ReferredBy = SafeValue<string>(p["ReferredBy"]),
                            ReferredById = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(p["ReferredById"])) ? 0 : p["ReferredById"]),
                            Location = SafeValue<string>(p["Location"]),
                        });
                    }
                }
                foreach (var items in lstPatientDetailsOfDoctor)
                {
                    if (string.IsNullOrEmpty(items.ImageName))
                    {
                        items.ImageName = SafeValue<string>(ConfigurationManager.AppSettings["DefaultDoctorImage"]);
                    }
                    else
                    {
                        items.ImageName = SafeValue<string>(ConfigurationManager.AppSettings["DoctorImage"]) + items.ImageName;
                    }
                }
                foreach (var items in lstPatientDetailsOfDoctor)
                {
                    try
                    {
                        string x = items.Phone.Trim();
                        double y = 0.0d;
                        if (x == "" || x == null)
                        {
                            items.Phone = "";
                        }
                        else if (!x.Contains('(') || !x.Contains(')') || !x.Contains('-'))
                        {
                            if (x.Contains("-"))
                            {
                                string z = x.Replace("-", "").Replace(" ", "");
                                y = SafeValue<double>(z);
                            }
                            else if (x.Contains(" "))
                            {
                                string z = x.Replace(" ", "");
                                y = SafeValue<double>(z);
                            }
                            else
                            {
                                y = SafeValue<double>(x);
                            }
                            items.Phone = String.Format("{0:(###)-###-####}", y);
                        }


                    }
                    catch (Exception Ex)
                    {
                        new clsCommon().InsertErrorLog("PatientBLL-MaskPhone", Ex.Message, Ex.StackTrace);
                        items.Phone = string.Empty;
                    }
                }

            }
            catch (Exception EX)
            {
                new clsCommon().InsertErrorLog("NewGetPatientDetailsOfDoctor", EX.Message, EX.StackTrace);
                throw;
            }
            return lstPatientDetailsOfDoctor;
        }
        
      

        //patinet Count
        public int NewGetPatientCountOfDoctor(int DoctorId, string SearchText = null, string NewSearchText = null, int FilterBy = 0, int Location = 0)
        {
            try
            {
                DataTable dt = new DataTable();
                int Count = 0;
                dt = objPatientsData.NewGetPatientCountByDoctorId(DoctorId, SearchText, NewSearchText, FilterBy, Location);
                if (dt != null && dt.Rows.Count > 0)
                {
                    Count = SafeValue<int>(dt.Rows[0]["Counting"]);
                }
                return Count;
            }
            catch (Exception EX)
            {
                objCommon.InsertErrorLog("GetPatientCountByDoctorId", EX.Message, EX.StackTrace);
                throw;
            }
        }

        // New Remove Patient method 
        public bool RemovePatient(int PatientId, int UserId)
        {
            clsPatientsData objPatientsData = new clsPatientsData();
            return objPatientsData.RemovePatientnew(PatientId, UserId);
        }

        /// <summary>
        /// This Method Used for PatientReward API or WebUI related not for Existing Recordlinc Patient Listing.
        /// </summary>
        /// <param name="FilterObj"></param>
        /// <returns></returns>
        public static List<PatientDetialsForPatientreward> GetPatientDetails(PatientFilter FilterObj)
        {
            DataTable dt = new DataTable();
            List<PatientDetialsForPatientreward> lst = new List<PatientDetialsForPatientreward>();
            clsPatientsData objPatientsData = new clsPatientsData();
            clsCommon objCommon = new clsCommon();
            dt = objPatientsData.NewGetPatientDetailsOfDoctor(FilterObj);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow p in dt.Rows)
                {
                    string DOB = SafeValue<string>(p["DateOfBirth"]);
                    if (!string.IsNullOrEmpty(DOB))
                    {
                        DateTime? data = SafeValue<DateTime>(DOB);
                        TimeSpan timeofDay = data.Value.TimeOfDay;
                        if (timeofDay != TimeSpan.Zero)
                        {
                            DOB = new CommonBLL().ConvertToDate(SafeValue<DateTime>(DOB), SafeValue<int>(BO.Enums.Common.DateFormat.GENERAL));
                        }
                    }
                    if (!clsPatientsData.PatientAssociationwithRewardPlatform(SafeValue<int>(p["PatientId"]), (int)FilterObj.sourcetype))
                    {
                        lst.Add(new PatientDetialsForPatientreward()
                        {
                            PatientId = SafeValue<int>(p["PatientId"]),
                            FirstName = SafeValue<string>(p["FirstName"]).Trim(),
                            LastName = SafeValue<string>(p["LastName"]).Trim(),
                            Email = SafeValue<string>(p["Email"]).Trim(),
                            Gender = SafeValue<string>(p["Gender"]),
                            Age = SafeValue<string>(p["Age"]),
                            WorkPhone = SafeValue<string>(p["Phone"]).Trim(),
                            MobilePhone = SafeValue<string>(p["MobilePhone"]).Trim(),
                            HomePhone = SafeValue<string>(p["HomePhone"]).Trim(),
                            DateOfBirth = DOB,
                            Address = SafeValue<string>(p["ExactAddress"]).Trim(),
                            Address1 = SafeValue<string>(p["Address2"]).Trim(),
                            City = SafeValue<string>(p["City"]).Trim(),
                            State = SafeValue<string>(p["State"]).Trim(),
                            PostalCode = SafeValue<string>(p["ZipCode"]).Trim(),
                            RewardPartnerId = SafeValue<string>(p["RewardPartnerId"])
                        });
                    }
                }
            }
            return lst;
        }

        public static PateintSolOneDetailsModel GetPatientBySolutionOne(PatientSolOne Obj)
        {
            DataTable Dt = new DataTable();
            clsCommon objCommon = new clsCommon();
            PateintSolOneDetailsModel psobj = new PateintSolOneDetailsModel();
            clsPatientsData objPatientsData = new clsPatientsData();
            Dt = objPatientsData.GetPateintBySolutionOneId(Obj);
            DataTable dtt = new DataTable();

            string url = SafeValue<string>(ConfigurationManager.AppSettings["RLURLImage"]);
            if (Dt != null && Dt.Rows.Count > 0)
            {
                psobj.IsSendPushNotification = SafeValue<bool>(Dt.Rows[0]["IsSendPushNotification"]);
                psobj.FirstName = SafeValue<string>(Dt.Rows[0]["FirstName"]).Trim();
                psobj.LastName = SafeValue<string>(Dt.Rows[0]["LastName"]).Trim();
                psobj.Email = SafeValue<string>(Dt.Rows[0]["Email"]).Trim();
                psobj.ProfileImage = url + SafeValue<string>(Dt.Rows[0]["ProfileImage"]).Trim();
                psobj.ExactAddress = SafeValue<string>(Dt.Rows[0]["ExactAddress"]).Trim();
                psobj.City = SafeValue<string>(Dt.Rows[0]["City"]).Trim();
                psobj.State = SafeValue<string>(Dt.Rows[0]["State"]).Trim();
                psobj.ZipCode = SafeValue<string>(Dt.Rows[0]["ZipCode"]).Trim();
                psobj.Country = SafeValue<string>(Dt.Rows[0]["Country"]).Trim();
                psobj.Phone = SafeValue<string>(Dt.Rows[0]["Phone"]).Trim();
                psobj.Phone2 = SafeValue<string>(Dt.Rows[0]["Phone2"]).Trim();
                psobj.Mobile = SafeValue<string>(Dt.Rows[0]["Mobile"]).Trim();
                psobj.DOB = SafeValue<string>(Dt.Rows[0]["DateOfBirth"]);
                psobj.DOB = (!string.IsNullOrWhiteSpace(psobj.DOB)) ? SafeValue<string>(new CommonBLL().ConvertToDate(SafeValue<DateTime>(psobj.DOB), SafeValue<int>(BO.Enums.Common.DateFormat.GENERAL))) : "";
                psobj.FirstExamDate = SafeValue<string>(Dt.Rows[0]["FirstExamDate"]).Trim();
                psobj.Email2 = SafeValue<string>(Dt.Rows[0]["Email2"]).Trim();
                psobj.PatientStatus = SafeValue<string>(Dt.Rows[0]["PatientStatus"]).Trim();
                int PatientId = SafeValue<int>(Dt.Rows[0]["PatientId"]);
                psobj.PrimaryPhoneNumber = SafeValue<string>(Dt.Rows[0]["Phone"]).Trim();
                if (Obj.sourcetype == APIFilter.FilterFlag.IsSuperBucks)
                {
                    PlanDetails Plans = new PlanDetails();
                    psobj.Plan.PlanId = SafeValue<int>(Dt.Rows[0]["PlanId"]);
                    psobj.Plan.PlanName = SafeValue<string>(Dt.Rows[0]["PlanName"]).Trim();
                    psobj.Plan.Amount = SafeValue<decimal>(Dt.Rows[0]["Amount"]);
                    psobj.Plan.RegisterReward = SafeValue<decimal>(Dt.Rows[0]["RegisterReward"]);
                    psobj.Plan.Point = SafeValue<decimal>(Dt.Rows[0]["Points"]);
                    //psobj.Plan = Plans;
                }
                if (SafeValue<string>(Dt.Rows[0]["Gender"]).Trim() == "0")
                {
                    psobj.Gender = "Male";
                }
                else if (SafeValue<string>(Dt.Rows[0]["Gender"]).Trim() == "1")
                {
                    psobj.Gender = "Female";
                }

                dtt = objPatientsData.GetPatientsDetails(PatientId);
                if (dtt != null && dtt.Rows.Count > 0)
                {
                    psobj.SecondaryPhoneNumber = SafeValue<string>(dtt.Rows[0]["SecondaryPhone"]);
                }

                DataTable dtRegi = new DataTable();
                dtRegi = new clsPatientsData().GetRegistration(SafeValue<int>(PatientId), "GetRegistrationform");
                if (dtRegi != null && dtRegi.Rows.Count > 0)
                {
                    psobj.EmergencyContactName = SafeValue<string>(dtRegi.Rows[0]["txtemergencyname"]);
                    psobj.EmergencyContactPhoneNumber = SafeValue<string>(dtRegi.Rows[0]["txtemergency"]);


                }
                //Get Dental History of that Patient.
                //Ankit Here: PRCSP -292 as per Bruce Change Request we have done this.
                psobj.DentalHistory = GetDentalHistory(PatientId);
                psobj.MedicalHistory = GetMedicalHistory(PatientId);
                psobj.InsuranceCoverage = GetInsuranceCoverage(PatientId);
                psobj.PrimaryDentalInsurance = GetPrimaryInsuranceDetails(PatientId);
                psobj.SecondaryDentalInsurance = GetSecondaryInsuranceDetails(PatientId);
                return psobj;
            }
            else
            {
                psobj = null;
                return psobj;
            }
        }
        public static List<ColleagueDetail> GetColleaguesListForReferral(int UserId, int StartIndex, int EndSize, string Searchtext, int SortColumn, int SortDirection, string NewSearchText, int FilterBy)
        {
            try
            {
                string ImageName = "";
                List<ColleagueDetail> Lst = new List<ColleagueDetail>();
                DataSet ds = new clsColleaguesData().GetColleaguesDetailsForDoctorWithSpeacilities(UserId, StartIndex, EndSize, Searchtext, SortColumn, SortDirection, NewSearchText, FilterBy);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        ImageName = SafeValue<string>(item["ImageName"]);
                        if (string.IsNullOrEmpty(ImageName))
                        {
                            ImageName = SafeValue<string>(ConfigurationManager.AppSettings["DefaultDoctorImage"]);
                        }
                        else
                        {
                            ImageName = SafeValue<string>(ConfigurationManager.AppSettings["DoctorImage"]) + ImageName;
                        }
                        DataTable dtSpeacilities = new DataTable();
                        var rows = ds.Tables[1].AsEnumerable()
                       .Where(x => ((int)x["Userid"]) == SafeValue<int>(SafeValue<string>(item["ColleagueId"])));

                        if (rows.Any())
                            dtSpeacilities = rows.CopyToDataTable();


                        // Datatable 
                        //dtSpeacilities = objColleaguesData.GetMemberSpecialities(int.Parse(objCommon.CheckNull(item["ColleagueId"].ToString(), "0")));
                        List<SpeacilitiesOfDoctor> lstSpeacilities = new List<SpeacilitiesOfDoctor>();
                        foreach (DataRow Specialities in dtSpeacilities.Rows)
                        {
                            SpeacilitiesOfDoctor clsSpe = new SpeacilitiesOfDoctor();
                            clsSpe.SpecialtyId = int.Parse(!string.IsNullOrWhiteSpace(SafeValue<string>(Specialities["Id"])) ? SafeValue<string>(Specialities["Id"]) : "0");
                            clsSpe.SpecialtyDescription = SafeValue<string>(Specialities["Specialities"]);
                            lstSpeacilities.Add(clsSpe);
                        }
                        Lst.Add(new ColleagueDetail()
                        {
                            ColleagueId = SafeValue<int>(item["ColleagueId"]),
                            FirstName = SafeValue<string>(item["FirstName"]),
                            LastName = SafeValue<string>(item["LastName"]),
                            MiddleName = SafeValue<string>(item["MiddleName"]),
                            ImageName = ImageName,
                            ExactAddress = SafeValue<string>(item["ExactAddress"]),
                            SecondaryEmail = SafeValue<string>(item["SecondaryEmail"]),
                            Address2 = SafeValue<string>(item["Address2"]),
                            City = SafeValue<string>(item["City"]),
                            State = SafeValue<string>(item["State"]),
                            Country = SafeValue<string>(item["Country"]),
                            ZipCode = SafeValue<string>(item["ZipCode"]),
                            EmailAddress = SafeValue<string>(item["EmailAddress"]),
                            Phone = SafeValue<string>(item["Phone"]),
                            Fax = SafeValue<string>(item["fax"]),
                            Officename = SafeValue<string>(item["OfficeName"]),
                            TotalRecord = SafeValue<int>(item["TotalRecord"]),
                            Location = SafeValue<string>(item["Location"]),
                            LocationId = SafeValue<int>(item["LocationId"]),
                            lstSpeacilitiesOfDoctor = lstSpeacilities
                        });
                    }
                }
                return Lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("GetColleaguesListForReferral", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static DentalHistorys GetDentalHistory(int PatientId)
        {
            DentalHistorys objDH = new DentalHistorys();
            DataTable dt = new DataTable();
            dt = new clsPatientsData().GetDentalHistory(PatientId, "GetDentalHistory");
            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    //objDH.txtQue1 = SafeValue<string>(row["txtQue1"]); //
                    //objDH.txtQue2 = SafeValue<string>(row["txtQue2"]);
                    //objDH.txtQue3 = SafeValue<string>(row["txtQue3"]);
                    //objDH.txtQue4 = SafeValue<string>(row["txtQue4"]);
                    objDH.DentistName = SafeValue<string>(row["txtQue5"]); //Dentist Name
                    objDH.DentistAddress = SafeValue<string>(row["txtQue5a"]);   //Dentist Address
                    objDH.DentistPhone = SafeValue<string>(row["txtQue5b"]);   //Dentist Phone
                    objDH.DentistEmail = SafeValue<string>(row["txtQue5c"]);   //Dentist Email
                    objDH.Last_Teeth_Cleening_Date = SafeValue<string>(row["txtQue6"]);   //Last Teeth Cleaning Date
                    objDH.Regural_Visit_Dentist = SafeValue<string>(row["txtQue7"]);   //make regular visits to dentist

                    if (row["rdQue7a"] != null && SafeValue<string>(row["rdQue7a"]) != "")
                    {
                        string Values = SafeValue<string>(row["rdQue7a"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.IsRegural_visit = true;
                        }
                        else
                        {
                            objDH.IsRegural_visit = false;
                        }
                    }

                    var teethReplace = new List<string>();
                    if (row["rdoQue11aFixedbridge"] != null && SafeValue<string>(row["rdoQue11aFixedbridge"]) != "") //Have any of your teeth been replaced with the following?
                    {
                        string Values = SafeValue<string>(row["rdoQue11aFixedbridge"]);
                        if (Values.Contains("Y"))
                        {
                            teethReplace.Add("Fixed Bridge");
                        }
                    }


                    if (row["rdoQue11bRemoveablebridge"] != null && SafeValue<string>(row["rdoQue11bRemoveablebridge"]) != "") //Have any of your teeth been replaced with the following?
                    {
                        string Values = SafeValue<string>(row["rdoQue11bRemoveablebridge"]);
                        if (Values.Contains("Y"))
                        {
                            teethReplace.Add("Removeable Bridge");
                        }
                    }


                    if (row["rdoQue11cDenture"] != null && SafeValue<string>(row["rdoQue11cDenture"]) != "") //Have any of your teeth been replaced with the following?
                    {
                        string Values = SafeValue<string>(row["rdoQue11cDenture"]);
                        if (Values.Contains("Y"))
                        {
                            teethReplace.Add("Denture");
                        }
                    }


                    if (row["rdQue11dImplant"] != null && SafeValue<string>(row["rdQue11dImplant"]) != "") //Have any of your teeth been replaced with the following?
                    {
                        string Values = SafeValue<string>(row["rdQue11dImplant"]);
                        if (Values.Contains("Y"))
                        {
                            teethReplace.Add("Implant");
                        }
                    }
                    objDH.Teeth_been_Replaced = teethReplace;
                    objDH.Prob_Complain = SafeValue<string>(row["txtQue12"]); // any problems or complications with previous dental treatment?

                    if (row["rdQue15"] != null && SafeValue<string>(row["rdQue15"]) != "")
                    {
                        string Values = SafeValue<string>(row["rdQue15"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.clench_your_teeth = true;
                        }
                        else
                        {
                            objDH.clench_your_teeth = false;
                        }
                    }


                    if (row["rdQue16"] != null && SafeValue<string>(row["rdQue16"]) != "")
                    {
                        string Values = SafeValue<string>(row["rdQue16"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.jaw_click_or_pop = true;
                        }
                        else
                        {
                            objDH.jaw_click_or_pop = false;
                        }
                    }

                    if (row["rdQue17"] != null && SafeValue<string>(row["rdQue17"]) != "")
                    {
                        string Values = SafeValue<string>(row["rdQue17"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.soreness_in_the_muscles = true;
                        }
                        else
                        {
                            objDH.soreness_in_the_muscles = false;
                        }
                    }

                    if (row["rdQue18"] != null && SafeValue<string>(row["rdQue18"]) != "")
                    {
                        string Values = SafeValue<string>(row["rdQue18"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.frequent_headaches = true;
                        }
                        else
                        {
                            objDH.frequent_headaches = false;
                        }
                    }

                    if (row["rdQue19"] != null && SafeValue<string>(row["rdQue19"]) != "")
                    {
                        string Values = SafeValue<string>(row["rdQue19"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.food_get_caught = true;
                        }
                        else
                        {
                            objDH.food_get_caught = false;
                        }
                    }
                    //11. Are any of your teeth sensitive to
                    var TeehtSensitive = new List<string>();

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["chkQue20"])))
                    {
                        string Values = SafeValue<string>(row["chkQue20"]);
                        if (Values.Contains("1"))
                        {
                            TeehtSensitive.Add("Hot");
                        }
                        if (Values.Contains("2"))
                        {
                            TeehtSensitive.Add("Cold");
                        }
                        if (Values.Contains("3"))
                        {
                            TeehtSensitive.Add("Sweets");
                        }
                        if (Values.Contains("4"))
                        {
                            TeehtSensitive.Add("Pressure");
                        }
                    }
                    objDH.teeth_sensitive = TeehtSensitive;

                    if (row["rdQue21"] != null && SafeValue<string>(row["rdQue21"]) != "") //12. Do your gums bleed or hurt?
                    {
                        string Values = SafeValue<string>(row["rdQue21"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.Gums_Bleed = true;
                        }
                        else
                        {
                            objDH.Gums_Bleed = false;
                        }
                    }
                    objDH.str_Gums_Bleed = SafeValue<string>(row["txtQue21a"]);
                    objDH.Brush_your_teeth = SafeValue<string>(row["txtQue22"]); //13. How often do you brush your teeth?

                    objDH.Dental_floss = SafeValue<string>(row["txtQue23"]); //14. How often do you use dental floss?

                    if (row["rdQue24"] != null && SafeValue<string>(row["rdQue24"]) != "") //15. Are any of your teeth loose, tipped, shifted or chipped?
                    {
                        string Values = SafeValue<string>(row["rdQue24"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.Loose_tipped_shifted = true;
                        }
                        else
                        {
                            objDH.Loose_tipped_shifted = false;
                        }
                    }


                    objDH.Your_Teeth_in_general = SafeValue<string>(row["txtQue26"]); //19. How do you feel about your teeth in general?


                    if (row["rdQue28"] != null && SafeValue<string>(row["rdQue28"]) != "") //16. Doyou_feelyour_breath_isoften_offensive?
                    {
                        string Values = SafeValue<string>(row["rdQue28"]);
                        if (Values.Contains("Y"))
                        {
                            objDH.isoften_offensive = true;
                        }
                        else
                        {
                            objDH.isoften_offensive = false;
                        }
                    }
                    //objDH.txtQue28a = SafeValue<string>(row["txtQue28a"]);
                    //objDH.txtQue28b = SafeValue<string>(row["txtQue28b"]);
                    objDH.str_offensive = SafeValue<string>(row["txtQue28c"]);
                    objDH.orthodontic_work_you_done = SafeValue<string>(row["txtQue29"]); //17. Explain any orthodontic work you have had done?
                    objDH.Dental_work_you_done = SafeValue<string>(row["txtQue29a"]); //18. Explain any dental work you have had done
                    //if (row["rdQue30"] != null && SafeValue<string>(row["rdQue30"]) != "")
                    //{
                    //    string Values = SafeValue<string>(row["rdQue30"]);
                    //    if (Values.Contains("Y"))
                    //    {
                    //        objDH.rdQue30 = true;
                    //    }
                    //    else
                    //    {
                    //        objDH.rdQue30 = false;
                    //    }
                    //}

                    objDH.Additional_info = SafeValue<string>(row["txtComments"]); //20. Additional Information
                    objDH.Reasonforfirstvisit = SafeValue<string>(row["Reasonforfirstvisit"]); //22. Reason for first visit
                    objDH.Dateoflastvisit = SafeValue<string>(row["Dateoflastvisit"]); //23. Date of last (latest) visit
                    objDH.Reasonforlastvisit = SafeValue<string>(row["Reasonforlastvisit"]); //24. Reason for last visit
                    objDH.Dateofnextvisit = SafeValue<string>(row["Dateofnextvisit"]); //25. Date of next (scheduled visit)
                    objDH.Reasonfornextvisit = SafeValue<string>(row["Reasonfornextvisit"]); //26. Reason for next visit
                    objDH.Dateoffirstvisit = SafeValue<string>(row["firstvistdate"]); //21. Date of first visit
                }
            }
            //PatientListDentalHistoryForm.Add(objDH);
            return objDH;
        }
        public static MedicalHistory GetMedicalHistory(int PatientId)
        {
            MedicalHistory objDH = new MedicalHistory();
            DataTable dt = new DataTable();
            dt = new clsPatientsData().GetMedicalHistory(PatientId, "GetMedicalHistory");
            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    //1. Are you under a physicians care right now?
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQue1"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["MrnQue1"])))
                    {
                        objDH.under_a_physicians = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["MrdQue1"]), SafeValue<string>(row["MrnQue1"]));
                    }
                    objDH.str_physicians = SafeValue<string>(row["Mtxtphysicians"]);

                    // 2.Have you ever been hospitalized or had a major operation ?
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQue2"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["MrnQue2"])))
                    {
                        objDH.hospitalized_or_majoroperation = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["MrdQue2"]), SafeValue<string>(row["MrnQue2"]));
                    }
                    objDH.str_hospitalized_or_majoroperation = SafeValue<string>(row["Mtxthospitalized"]);

                    //3. Have you ever had a serious head or neck injury?
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQue3"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["MrnQue3"])))
                    {
                        objDH.head_or_neck_injury = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["MrdQue3"]), SafeValue<string>(row["MrnQue3"]));
                    }
                    objDH.str_head_or_neck_injury = SafeValue<string>(row["Mtxtserious"]);

                    //4. Are taking any medications, pills, or drugs?
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQue4"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["MrnQue4"])))
                    {
                        objDH.medications_pills_drugs = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["MrdQue4"]), SafeValue<string>(row["MrnQue4"]));
                    }
                    objDH.str_medications_pills_drugs = SafeValue<string>(row["Mtxtmedications"]);

                    //5. Do you take, or have you taken, Phen-Fen or Redux?
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQue5"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["MrnQue5"])))
                    {
                        objDH.PhenFen_or_Redux = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["MrdQue5"]), SafeValue<string>(row["MrnQue5"]));
                    }
                    objDH.str_PhenFen_or_Redux = SafeValue<string>(row["MtxtRedux"]);

                    // 6. Have you ever taken Fosamax, Boniva, Actonel or any other medications containing biphosphonates?
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQue6"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["MrnQue6"])))
                    {
                        objDH.Isbiphosphonates = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["MrdQue6"]), SafeValue<string>(row["MrnQue6"]));
                    }
                    objDH.str_biphosphonates = SafeValue<string>(row["MtxtFosamax"]);

                    //7. Are you on a special diet?
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQuediet7"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["MrnQuediet7"])))
                    {
                        objDH.Specialdiet = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["MrdQuediet7"]), SafeValue<string>(row["MrnQuediet7"]));
                    }
                    objDH.str_Specialdiet = SafeValue<string>(row["Mtxt7"]);

                    //8. Do you use tobacco?
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["Mrdotobacco8"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["Mrnotobacco8"])))
                    {
                        objDH.tobacco = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["Mrdotobacco8"]), SafeValue<string>(row["Mrnotobacco8"]));
                    }
                    objDH.str_tobacco = SafeValue<string>(row["Mtxt8"]);

                    //9. Do you use controlled substances?
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["Mrdosubstances"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["Mrnosubstances"])))
                    {
                        objDH.substances = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["Mrdosubstances"]), SafeValue<string>(row["Mrnosubstances"]));
                    }
                    objDH.str_substances = SafeValue<string>(row["Mtxt9"]);

                    //10. Are you pregnant/Trying to get pregnant(women)?
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["Mrdopregnant"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["Mrnopregnant"])))
                    {
                        objDH.pregnant = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["Mrdopregnant"]), SafeValue<string>(row["Mrnopregnant"]));
                    }
                    objDH.str_pregnant = SafeValue<string>(row["Mtxt10"]);

                    //11. Taking oral contraceptives(women)?
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["Mrdocontraceptives"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["Mrnocontraceptives"])))
                    {
                        objDH.contraceptives = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["Mrdocontraceptives"]), SafeValue<string>(row["Mrnocontraceptives"]));
                    }
                    objDH.str_contraceptives = SafeValue<string>(row["Mtxt11"]);
                    //12. Nursing(women)?
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdoNursing"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["MrnoNursing"])))
                    {
                        objDH.Nursing = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["MrdoNursing"]), SafeValue<string>(row["MrnoNursing"]));
                    }
                    objDH.str_Nursing = SafeValue<string>(row["Mtxt12"]);

                    //13 Have tonsils and or adenoids been removed?
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["Mrdotonsils"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["Mrnotonsils"])))
                    {
                        objDH.tonsils = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["Mrdotonsils"]), SafeValue<string>(row["Mrnotonsils"]));
                    }
                    objDH.str_tonsils = SafeValue<string>(row["Mtxt13"]);
                    Allergic Obj = new Allergic();
                    //14. Are you allergic to any of the following?
                    //Asprin
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MchkQue_1"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["MchkQueN_1"])))
                    {
                        Obj.Aspirin = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["MchkQue_1"]), SafeValue<string>(row["MchkQueN_1"]));
                    }

                    //14. Are you allergic to any of the following?
                    //Penicillin
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MchkQue_2"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["MchkQueN_2"])))
                    {
                        Obj.Penicillin = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["MchkQue_2"]), SafeValue<string>(row["MchkQueN_2"]));
                    }

                    //14. Are you allergic to any of the following?
                    //Codeine
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MchkQue_3"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["MchkQueN_3"])))
                    {
                        Obj.Codeine = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["MchkQue_3"]), SafeValue<string>(row["MchkQueN_3"]));
                    }

                    //14. Are you allergic to any of the following?
                    //Acrylic
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MchkQue_4"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["MchkQueN_4"])))
                    {
                        Obj.Acrylic = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["MchkQue_3"]), SafeValue<string>(row["MchkQueN_4"]));
                    }

                    //14. Are you allergic to any of the following?
                    //Local Anesthetics
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MchkQue_5"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["MchkQueN_5"])))
                    {
                        Obj.LocalAnesthetics = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["MchkQue_5"]), SafeValue<string>(row["MchkQueN_5"]));
                    }

                    //14. Are you allergic to any of the following?
                    //Metal
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MchkQue_6"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["MchkQueN_6"])))
                    {
                        Obj.Metal = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["MchkQue_6"]), SafeValue<string>(row["MchkQueN_6"]));
                    }

                    //14. Are you allergic to any of the following?
                    //Latex
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MchkQue_7"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["MchkQueN_7"])))
                    {
                        Obj.Latex = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["MchkQue_7"]), SafeValue<string>(row["MchkQueN_7"]));
                    }

                    //14. Are you allergic to any of the following?
                    //Sulfa Drugs
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MchkQue_8"])) || !string.IsNullOrWhiteSpace(SafeValue<string>(row["MchkQueN_8"])))
                    {
                        Obj.SulfaDrugs = CheckIsMedicalHistorycheckbox(SafeValue<string>(row["MchkQue_8"]), SafeValue<string>(row["MchkQueN_8"]));
                    }

                    Obj.Other = SafeValue<string>(row["MtxtchkQue_9"]); //
                    objDH.allergic = Obj;

                    #region Commented code of not used on Dentrix JSON
                    Following flw = new Following();
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueAIDS_HIV_Positive"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueAIDS_HIV_Positive"]);
                        flw.AIDS_HIV_Positive = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueAlzheimer"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueAlzheimer"]);
                        flw.Alzheimer_Disease = (Values.Contains("Y")) ? true : false;
                    }



                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueAnaphylaxis"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueAnaphylaxis"]);
                        flw.Anaphylaxis = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueAnemia"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueAnemia"]);
                        flw.Anemia = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueAngina"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueAngina"]);
                        flw.Angina = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueArthritis_Gout"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueArthritis_Gout"]);
                        flw.Arthritis_Gout = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueArtificialHeartValve"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueArtificialHeartValve"]);
                        flw.Artificial_Heart_Valve = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueArtificialJoint"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueArtificialJoint"]);
                        flw.Artificial_Joint = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueAsthma"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueAsthma"]);
                        flw.Asthma = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueBloodDisease"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueBloodDisease"]);
                        flw.Blood_Disease = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueBloodTransfusion"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueBloodTransfusion"]);
                        flw.Blood_Transfusion = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueBoneDisorders"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueBoneDisorders"]);
                        flw.Bone_Disorders = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueBreathing"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueBreathing"]);
                        flw.Breathing_Problem = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueBruise"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueBruise"]);
                        flw.Bruise_Easily = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueCancer"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueCancer"]);
                        flw.Cancer = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueChemicalDependancy"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueChemicalDependancy"]);
                        flw.Chemical_Dependancy = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueChemotherapy"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueChemotherapy"]);
                        flw.Chemotherapy = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueChest"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueChest"]);
                        flw.Chest_Pains = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueCold_Sores_Fever"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueCold_Sores_Fever"]);
                        flw.Cold_Sores_Fever_Blisters = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueCongenital"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueCongenital"]);
                        flw.Congenital_Heart_Disorder = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueConvulsions"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueConvulsions"]);
                        flw.Convulsions = (Values.Contains("Y")) ? true : false;
                    }

                    //Cortisone_Medicine
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueCortisone"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueCortisone"]);
                        flw.Cortisone_Medicine = (Values.Contains("Y")) ? true : false;
                    }

                    //Cortisone Tretments
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueCortisoneTretments"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueCortisoneTretments"]);
                        flw.Cortisone_Tretments = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueDiabetes"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueDiabetes"]);
                        flw.Diabetes = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueDrug"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueDrug"]);
                        flw.Drug_Addiction = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueEasily"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueEasily"]);
                        flw.Easily_Winded = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueEmphysema"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueEmphysema"]);
                        flw.Emphysema = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueEndocrineProblems"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueEndocrineProblems"]);
                        flw.Endorcrine_Problems = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueEpilepsy"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueEpilepsy"]);
                        flw.Epilepsy_or_Seizures = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueExcessiveBleeding"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueExcessiveBleeding"]);
                        flw.Excessive_Bleeding = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueExcessiveThirst"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueExcessiveThirst"]);
                        flw.Excessive_Thirst = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueExcessiveUrination"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueExcessiveUrination"]);
                        flw.Excessive_Urination = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueFainting"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueFainting"]);
                        flw.Fainting_Spells_Dizziness = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueFrequentCough"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueFrequentCough"]);
                        flw.Frequent_Cough = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueFrequentDiarrhea"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueFrequentDiarrhea"]);
                        flw.Frequent_Diarrhea = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueFrequentHeadaches"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueFrequentHeadaches"]);
                        flw.Frequent_Headaches = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueGenital"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueGenital"]);
                        flw.Genital_Herpes = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueGlaucoma"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueGlaucoma"]);
                        flw.Glaucoma = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueHay"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueHay"]);
                        flw.Hay_Fever = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueHeartAttack_Failure"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueHeartAttack_Failure"]);
                        flw.Heart_Attack_Failure = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueHeartMurmur"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueHeartMurmur"]);
                        flw.Heart_Murmur = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueHeartPacemaker"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueHeartPacemaker"]);
                        flw.Heart_Pacemaker = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueHeartTrouble_Disease"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueHeartTrouble_Disease"]);
                        flw.Heart_Trouble_Disease = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueHemophilia"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueHemophilia"]);
                        flw.Hemophilia = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueHepatitisA"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueHepatitisA"]);
                        flw.Hepatitis_A = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueHepatitisBorC"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueHepatitisBorC"]);
                        flw.Hepatitis_B_or_C = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueHerpes"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueHerpes"]);
                        flw.Herpes = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueHighBloodPressure"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueHighBloodPressure"]);
                        flw.High_Blood_Pressure = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueHighCholesterol"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueHighCholesterol"]);
                        flw.High_Cholesterol = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueHives"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueHives"]);
                        flw.Hives_or_Rash = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueHypoglycemia"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueHypoglycemia"]);
                        flw.Hypoglycemia = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueIrregular"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueIrregular"]);
                        flw.Irregular_Heartbeat = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueKidney"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueKidney"]);
                        flw.Kidney_Problems = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueLeukemia"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueLeukemia"]);
                        flw.Leukemia = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueLiver"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueLiver"]);
                        flw.Liver_Disease = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueLow"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueLow"]);
                        flw.Low_Blood_Pressure = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueLung"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueLung"]);
                        flw.Lung_Disease = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueMitral"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueMitral"]);
                        flw.Mitral_Valve_Prolapse = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueNervousDisorder"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueNervousDisorder"]);
                        flw.Nervous_Disorder = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueOsteoporosis"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueOsteoporosis"]);
                        flw.Osteoporosis = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQuePain"])))
                    {
                        string Values = SafeValue<string>(row["MrdQuePain"]);
                        flw.Pain_in_Jaw_Joints = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueParathyroid"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueParathyroid"]);
                        flw.Parathyroid_Disease = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQuePsychiatric"])))
                    {
                        string Values = SafeValue<string>(row["MrdQuePsychiatric"]);
                        flw.Psychiatric_Care = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueRadiation"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueRadiation"]);
                        flw.Radiation_Treatments = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueRecent"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueRecent"]);
                        flw.Recent_Weight_Loss = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueRenal"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueRenal"]);
                        flw.Renal_Dialysis = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueRheumatic"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueRheumatic"]);
                        flw.Rheumatic_Fever = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueRheumatism"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueRheumatism"]);
                        flw.Rheumatism = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueScarlet"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueScarlet"]);
                        flw.Scarlet_Fever = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueShingles"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueShingles"]);
                        flw.Shingles = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueSickle"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueSickle"]);
                        flw.Sickle_Cell_Disease = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueSinus"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueSinus"]);
                        flw.Sinus_Trouble = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueSpina"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueSpina"]);
                        flw.Spina_Bifida = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueStomach"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueStomach"]);
                        flw.Stomach_Intestinal_Disease = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueStroke"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueStroke"]);
                        flw.Stroke = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueSwelling"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueSwelling"]);
                        flw.Swelling_of_Limbs = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueThyroid"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueThyroid"]);
                        flw.Thyroid_Disease = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueTonsillitis"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueTonsillitis"]);
                        flw.Tonsillitis = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueTuberculosis"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueTuberculosis"]);
                        flw.Tuberculosis = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueTumors"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueTumors"]);
                        flw.Tumors_or_Growths = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueUlcers"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueUlcers"]);
                        flw.Ulcers = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueVenereal"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueVenereal"]);
                        flw.Venereal_Disease = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(row["MrdQueYellow"])))
                    {
                        string Values = SafeValue<string>(row["MrdQueYellow"]);
                        flw.Yellow_Jaundice = (Values.Contains("Y")) ? true : false;
                    }

                    #endregion
                    objDH.following = flw;
                    objDH.illness = SafeValue<string>(row["Mtxtillness"]);//15. List any serious illness not listed above
                    objDH.Additional_info = SafeValue<string>(row["MtxtComments"]); //16. Additional Information

                }
            }
            PatientListMedicalHistoryForm.Add(objDH);
            return objDH;
        }

        public static InsuranceCoverageModel GetInsuranceCoverage(int PatientId)
        {
            BO.ViewModel.InsuranceCoverageModel Obj = new BO.ViewModel.InsuranceCoverageModel();
            DataTable dt = new DataTable();
            dt = new clsPatientsData().GetRegistration(PatientId, "GetRegistrationform");
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Obj.ResponsiblepartyFname = SafeValue<string>(row["txtResponsiblepartyFname"]);
                    Obj.ResponsiblepartyLname = SafeValue<string>(row["txtResponsiblepartyLname"]);
                    Obj.Relationship = SafeValue<string>(row["txtResponsibleRelationship"]);
                    Obj.Address = SafeValue<string>(row["txtResponsibleAddress"]);
                    Obj.City = SafeValue<string>(row["txtResponsibleCity"]);
                    Obj.State = SafeValue<string>(row["txtResponsibleState"]);
                    Obj.ZipCode = SafeValue<string>(row["txtResponsibleZipCode"]);
                    //Obj.DOB = SafeValue<string>(row["txtResponsibleDOB"]);
                    Obj.PhoneNumber = SafeValue<string>(row["txtResponsibleContact"]);
                    Obj.Emailaddress = SafeValue<string>(row["txtemailaddress"]);
                }
            }
            return Obj;
        }
        public static PDIC GetPrimaryInsuranceDetails(int Patientid)
        {
            PDIC Obj = new PDIC();
            DataTable dt = new DataTable();
            dt = new clsPatientsData().GetRegistration(Patientid, "GetRegistrationform");
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Obj.InsuranceCompany = SafeValue<string>(row["txtEmployeeName1"]);
                    Obj.InsuranceCompanyPhone = SafeValue<string>(row["txtInsurancePhone1"]);
                    Obj.DateOfBirth = SafeValue<string>(row["txtEmployeeDob1"]);
                    Obj.NameOfInsured = SafeValue<string>(row["txtEmployerName1"]);
                    Obj.MemberId = SafeValue<string>(row["txtNameofInsurance1"]);
                    Obj.GroupNumber = SafeValue<string>(row["txtInsuranceTelephone1"]);
                }
            }
            return Obj;
        }

        public static SDIC GetSecondaryInsuranceDetails(int Patientid)
        {
            SDIC Obj = new SDIC();
            DataTable dt = new DataTable();
            dt = new clsPatientsData().GetRegistration(Patientid, "GetRegistrationform");
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    Obj.InsuranceCompany = SafeValue<string>(row["txtEmployeeName2"]);
                    Obj.InsuranceCompanyPhone = SafeValue<string>(row["txtInsurancePhone2"]);
                    Obj.DateOfBirth = SafeValue<string>(row["txtYearsEmployed2"]);
                    Obj.NameOfInsured = SafeValue<string>(row["txtEmployerName2"]);
                    Obj.MemberId = SafeValue<string>(row["txtNameofInsurance2"]);
                    Obj.GroupNumber = SafeValue<string>(row["txtInsuranceTelephone2"]);
                }
            }
            return Obj;
        }
        public static bool? CheckIsMedicalHistorycheckbox(string Mrd, string Mrn)
        {
            bool? result = null;
            if (!string.IsNullOrWhiteSpace(Mrd) && Mrd != "N")
            {
                string Values = SafeValue<string>(Mrd);
                result = (Values.Contains("Y")) ? true : (bool?)null;
            }
            else if (!string.IsNullOrWhiteSpace(Mrn) && Mrn != "N")
            {
                string Values = SafeValue<string>(Mrn);
                result = (Values.Contains("Y")) ? false : (bool?)null;
            }
            else
            {
                result = null;
            }
            return result;
        }

        public static List<PatientDetialsForPatientreward> SUP_GetPatientDetails(PatientFilter FilterObj)
        {
            DataTable dt = new DataTable();
            List<PatientDetialsForPatientreward> lst = new List<PatientDetialsForPatientreward>();
            clsPatientsData objPatientsData = new clsPatientsData();
            clsCommon objCommon = new clsCommon();
            dt = objPatientsData.SUP_GetPatientDetailsOfDoctor(FilterObj);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow p in dt.Rows)
                {
                    string DOB = SafeValue<string>(p["DateOfBirth"]);
                    if (!string.IsNullOrEmpty(DOB))
                    {
                        DateTime? data = SafeValue<DateTime>(DOB);
                        TimeSpan timeofDay = data.Value.TimeOfDay;
                        if (timeofDay != TimeSpan.Zero)
                        {
                            DOB = new CommonBLL().ConvertToDate(SafeValue<DateTime>(DOB), SafeValue<int>(BO.Enums.Common.DateFormat.GENERAL));
                        }
                    }
                    if (!clsPatientsData.PatientAssociationwithRewardPlatform(SafeValue<int>(p["PatientId"]), (int)FilterObj.sourcetype))
                    {
                        lst.Add(new PatientDetialsForPatientreward()
                        {
                            PatientId = SafeValue<int>(p["PatientId"]),
                            FirstName = SafeValue<string>(p["FirstName"]).Trim(),
                            LastName = SafeValue<string>(p["LastName"]).Trim(),
                            Email = SafeValue<string>(p["Email"]).Trim(),
                            Gender = SafeValue<string>(p["Gender"]),
                            Age = SafeValue<string>(p["Age"]),
                            WorkPhone = SafeValue<string>(p["Phone"]).Trim(),
                            MobilePhone = SafeValue<string>(p["MobilePhone"]).Trim(),
                            HomePhone = SafeValue<string>(p["HomePhone"]).Trim(),
                            DateOfBirth = DOB,
                            Address = SafeValue<string>(p["ExactAddress"]).Trim(),
                            Address1 = SafeValue<string>(p["Address2"]).Trim(),
                            City = SafeValue<string>(p["City"]).Trim(),
                            State = SafeValue<string>(p["State"]).Trim(),
                            PostalCode = SafeValue<string>(p["ZipCode"]).Trim(),
                            RewardPartnerId = SafeValue<string>(p["RewardPartnerId"])
                        });
                    }
                }
            }
            return lst;
        }

        /// <summary>
        /// Method use to search the patient data in appointment : Ref : XQ1-114
        /// </summary>
        /// <param name="FilterObj"></param>
        /// <returns></returns>
        public static List<PatientDetialsForPatientreward> Appointment_GetPatientDetails(PatientFilter FilterObj)
        {
            DataTable dt = new DataTable();
            List<PatientDetialsForPatientreward> lst = new List<PatientDetialsForPatientreward>();
            clsPatientsData objPatientsData = new clsPatientsData();
            clsCommon objCommon = new clsCommon();
            dt = objPatientsData.SUP_GetPatientDetailsOfDoctor(FilterObj);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow p in dt.Rows)
                {
                    string DOB = SafeValue<string>(p["DateOfBirth"]);
                    if (!string.IsNullOrEmpty(DOB))
                    {
                        DateTime? data = SafeValue<DateTime>(DOB);
                        TimeSpan timeofDay = data.Value.TimeOfDay;
                        if (timeofDay != TimeSpan.Zero)
                        {
                            DOB = new CommonBLL().ConvertToDate(SafeValue<DateTime>(DOB), SafeValue<int>(BO.Enums.Common.DateFormat.GENERAL));
                        }
                    }
                    lst.Add(new PatientDetialsForPatientreward()
                    {
                        PatientId = SafeValue<int>(p["PatientId"]),
                        FirstName = SafeValue<string>(p["FirstName"]).Trim(),
                        LastName = SafeValue<string>(p["LastName"]).Trim(),
                        Email = SafeValue<string>(p["Email"]).Trim(),
                        Gender = SafeValue<string>(p["Gender"]),
                        Age = SafeValue<string>(p["Age"]),
                        WorkPhone = SafeValue<string>(p["Phone"]).Trim(),
                        MobilePhone = SafeValue<string>(p["MobilePhone"]).Trim(),
                        HomePhone = SafeValue<string>(p["HomePhone"]).Trim(),
                        DateOfBirth = DOB,
                        Address = SafeValue<string>(p["ExactAddress"]).Trim(),
                        Address1 = SafeValue<string>(p["Address2"]).Trim(),
                        City = SafeValue<string>(p["City"]).Trim(),
                        State = SafeValue<string>(p["State"]).Trim(),
                        PostalCode = SafeValue<string>(p["ZipCode"]).Trim(),
                        RewardPartnerId = SafeValue<string>(p["RewardPartnerId"])
                    });
                }
            }
            return lst;
        }

        //Patients
        public static List<NewPatientDetailsOfDoctor> PatientListOfDoctor(PatientFilter FilterObj)
        {
            List<NewPatientDetailsOfDoctor> lstPatients = new List<NewPatientDetailsOfDoctor>();
            FilterObj.PageSize = 10;
            FilterObj.PageIndex = 1;
            FilterObj.SearchText = null;
            FilterObj.SortColumn = 2;
            FilterObj.SortDirection = 1;
            FilterObj.NewSearchtext = null;
            FilterObj.FilterBy = "2";
            FilterObj.LocationBy = "0";
            lstPatients = NewGetPatientDetailsOfDoctor(FilterObj);
            return lstPatients;
        }

        /// <summary>
        /// This Method get patient all procedure list.
        /// Updated date: 08-07-2018
        /// Updated by: Ankit Solanki
        /// JIRA NO SBI-35
        /// </summary>
        /// <param name="Obj"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static RetriveResponse<List<PatientDetailsModel>> GetPatientList(PatientFilter Obj, int UserId)
        {
            try
            {
                clsCommon objCommon = new clsCommon();
                RetriveResponse<List<PatientDetailsModel>> Retrieve = new RetriveResponse<List<PatientDetailsModel>>();
                string RequestGUID = string.Empty;
                int PatientRequestId = 0;
                int TotalRecord = 0;
                List<PatientDetailsModel> Lst = new List<PatientDetailsModel>();
                if (SafeValue<DateTime>(Obj.FromDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    Obj.FromDate = null;
                }
                if (SafeValue<DateTime>(Obj.ToDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    Obj.ToDate = null;
                }
                if (SafeValue<DateTime>(Obj.DateOfBirth).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    Obj.DateOfBirth = null;
                }

                if (Obj.PageSize > 1500)
                {
                    return new RetriveResponse<List<PatientDetailsModel>>()
                    {
                        IsSuccess = false,
                        Message = "Maximum page size exceeded. Maximum PageSize is 1500.",
                        Result = null,
                        StatusCode = 413
                    };
                }
                if (string.IsNullOrEmpty(Obj.RequestId))
                {
                    if (Obj.PageIndex > 1)
                    {
                        return new RetriveResponse<List<PatientDetailsModel>>()
                        {
                            IsSuccess = false,
                            Message = "In order to use pagination, RequestId parameter is mandatory",
                            Result = null,
                            StatusCode = 413
                        };
                    }
                    int GetCountOfRecords = clsSmileBrand.GetCountofPatients(Obj, UserId);
                    if (GetCountOfRecords > BO.Constatnt.Common.CountOverLoad)
                    {
                        Retrieve.IsSuccess = false;
                        Retrieve.Message = $"The maximum number of records that can be returned by this API call is {BO.Constatnt.Common.CountOverLoad} rows. Your request has created a result set of {GetCountOfRecords} rows. Please modify your query to return a result set of less than {BO.Constatnt.Common.CountOverLoad} rows";
                        Retrieve.RequestId = RequestGUID;
                        Retrieve.TotalRecord = GetCountOfRecords;
                        Retrieve.Result = null;
                        Retrieve.StatusCode = 413;
                        return Retrieve;
                    }

                    IDictionary<int, string> Response = SmileBrandBLL.InsertDPMS_SyncDetails("PATIENTS_SMILEBRAND");
                    PatientRequestId = Response.Keys.FirstOrDefault();
                    RequestGUID = Response.Values.FirstOrDefault();

                    bool Result = clsSmileBrand.InsertDMPSPatientProcessQueue(Obj, UserId, PatientRequestId);
                }
                else
                {
                    PatientRequestId = clsColleaguesData.GetPMSSyncDetailsByGUID(Obj.RequestId);
                }

                if (PatientRequestId > 0)
                {
                    DataTable dt = clsSmileBrand.GetPatientDetails(PatientRequestId, Obj.PageIndex, Obj.PageSize);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow p in dt.Rows)
                        {
                            int AssignedPatientId = SafeValue<string>(p["AssignedPatientId"]).Length > 0 ? SafeValue<int>(p["AssignedPatientId"]) : 0;

                            Lst.Add(new PatientDetailsModel()
                            {
                                RLPatientId = SafeValue<int>(p["PatientId"]),
                                DentrixPatientId = AssignedPatientId,
                                FirstName = SafeValue<string>(p["FirstName"]).Trim(),
                                LastName = SafeValue<string>(p["LastName"]).Trim(),
                                Email = SafeValue<string>(p["Email"]).Trim(),
                                Gender = SafeValue<string>(p["Gender"]),
                                Age = SafeValue<string>(p["Age"]),
                                WorkPhone = SafeValue<string>(p["Phone"]).Trim(),
                                MobilePhone = SafeValue<string>(p["MobilePhone"]).Trim(),
                                HomePhone = SafeValue<string>(p["HomePhone"]).Trim(),
                                DateOfBirth = SafeValue<string>(p["DateOfBirth"]).Trim(),
                                Address = SafeValue<string>(p["ExactAddress"]).Trim(),
                                Address1 = SafeValue<string>(p["Address2"]).Trim(),
                                City = SafeValue<string>(p["City"]).Trim(),
                                State = SafeValue<string>(p["State"]).Trim(),
                                PostalCode = SafeValue<string>(p["ZipCode"]).Trim()
                            });
                            TotalRecord = SafeValue<int>(p["TotalRecord"]);
                        }
                        Retrieve.IsSuccess = true;
                        Retrieve.Message = "Success!";
                        Retrieve.RequestId = RequestGUID;
                        Retrieve.TotalRecord = TotalRecord;
                        Retrieve.Result = Lst;
                        Retrieve.StatusCode = 200;
                    }
                    else
                    {
                        Retrieve.IsSuccess = false;
                        Retrieve.Message = "Not Found!";
                        Retrieve.RequestId = RequestGUID;
                        Retrieve.TotalRecord = TotalRecord;
                        Retrieve.Result = Lst;
                        Retrieve.StatusCode = 404;
                    }
                }
                return Retrieve;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("SmileBrandBLL--GetPatientDetailsModel", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This Method used for fetching account aging records on RL database.
        /// Ankit Solanki here Updated date: 08-09-2018
        /// SBI-36 related change request as perform like Retrieve Patient.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public static UpdateResponse<PatientPaymentModel> GetPatientPayments(PatientPayments Obj, int UserId)
        {
            try
            {
                Request RequestIds = new Request();
                string RequestGUID = string.Empty;
                UpdateResponse<PatientPaymentModel> retrives = new UpdateResponse<PatientPaymentModel>();
                List<FinanceCharges> lstPatient_DentrixFinanceCharges = new List<FinanceCharges>();
                List<AdjutmentDetails> lstPatient_DentrixAdjustments = new List<AdjutmentDetails>();
                List<InsurancePayment> lstPatient_DentrixInsurancePayment = new List<InsurancePayment>();
                List<StandardPayment> lstPatient_DentrixStandardPayment = new List<StandardPayment>();
                if (SafeValue<DateTime>(Obj.FromDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    Obj.FromDate = null;
                }
                if (SafeValue<DateTime>(Obj.ToDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    Obj.ToDate = null;
                }

                // Prevent to count of records if request Id exists
                if (string.IsNullOrEmpty(Obj.RequestId))
                {
                    if (Obj.PageIndex > 1)
                    {
                        return new UpdateResponse<PatientPaymentModel>()
                        {
                            IsSuccess = false,
                            Message = "In order to use pagination, RequestId parameter is mandatory",
                            Result = null,
                            StatusCode = 400
                        };
                    }
                    DataTable dtCounts = clsSmileBrand.GetCountofPatientPayments(Obj, UserId);
                    if (dtCounts != null && dtCounts.Rows.Count > 0)
                    {
                        string ADjustmentCount = (!string.IsNullOrWhiteSpace(SafeValue<string>(dtCounts.Rows[0]["ADjustmentCount"]))) ? SafeValue<string>(dtCounts.Rows[0]["ADjustmentCount"]) : "0";
                        string InsuranceCount = (!string.IsNullOrWhiteSpace(SafeValue<string>(dtCounts.Rows[0]["InsuranceCount"]))) ? SafeValue<string>(dtCounts.Rows[0]["InsuranceCount"]) : "0";
                        string StandardPayment = (!string.IsNullOrWhiteSpace(SafeValue<string>(dtCounts.Rows[0]["StandardPayment"]))) ? SafeValue<string>(dtCounts.Rows[0]["StandardPayment"]) : "0";
                        string FinanceCharges = (!string.IsNullOrWhiteSpace(SafeValue<string>(dtCounts.Rows[0]["FinanceCharges"]))) ? SafeValue<string>(dtCounts.Rows[0]["FinanceCharges"]) : "0";

                        if (SafeValue<int>(ADjustmentCount) > BO.Constatnt.Common.CountOverLoad || SafeValue<int>(InsuranceCount) > BO.Constatnt.Common.CountOverLoad || SafeValue<int>(StandardPayment) > BO.Constatnt.Common.CountOverLoad || SafeValue<int>(FinanceCharges) > BO.Constatnt.Common.CountOverLoad)
                        {
                            PatientPaymentModel objPatientPaymentModels = new PatientPaymentModel()
                            {
                                FinanceCharges_TotalRecord = SafeValue<int>(FinanceCharges),
                                Adjustments_TotalRecord = SafeValue<int>(ADjustmentCount),
                                InsurancePayment_TotalRecord = SafeValue<int>(InsuranceCount),
                                StandardPayment_TotalRecord = SafeValue<int>(StandardPayment)
                            };
                            retrives.IsSuccess = false;
                            retrives.Message = $"The maximum number of records that can be returned by this API call is 50,000 rows. Your request has created a result set of Finance changes has : {SafeValue<int>(FinanceCharges)} rows."
                                + $" Adjustment Details has : {SafeValue<int>(ADjustmentCount)} rows."
                                + $" Insurance Payment has : {SafeValue<int>(InsuranceCount)} rows."
                                + $" Standard Payment has : {SafeValue<int>(StandardPayment)} rows."
                                + " Please modify your query to return a result set of less than 50,000 rows";
                            retrives.Result = objPatientPaymentModels;
                            retrives.StatusCode = 413;
                            return retrives;
                        }
                    }
                }

                if (string.IsNullOrWhiteSpace(Obj.RequestId))
                {
                    IDictionary<int, string> FinanceResponse = SmileBrandBLL.InsertDPMS_SyncDetails("FinanceChanges_SBI");
                    RequestIds.FinanceRequestId = FinanceResponse.Keys.FirstOrDefault();
                    RequestIds.FinanceRequestKey = RequestGUID = FinanceResponse.Values.FirstOrDefault();

                    IDictionary<int, string> InsuranceResponse = SmileBrandBLL.InsertDPMS_SyncDetails("InsurancePayment_SBI", RequestGUID);
                    RequestIds.InsuranceRequestId = InsuranceResponse.Keys.FirstOrDefault();
                    RequestIds.InsuranceRequestKey = InsuranceResponse.Values.FirstOrDefault();

                    IDictionary<int, string> StandardResponse = SmileBrandBLL.InsertDPMS_SyncDetails("StandardPayment_SBI", RequestGUID);
                    RequestIds.StandardRequestId = StandardResponse.Keys.FirstOrDefault();
                    RequestIds.StandardRequestKey = StandardResponse.Values.FirstOrDefault();

                    IDictionary<int, string> AdjustmentResponse = SmileBrandBLL.InsertDPMS_SyncDetails("AdjustmentPayment_SBI", RequestGUID);
                    RequestIds.AdjustmentRequestId = AdjustmentResponse.Keys.FirstOrDefault();
                    RequestIds.AdjustmentRequestKey = AdjustmentResponse.Values.FirstOrDefault();

                    bool Result = clsSmileBrand.InsertDPMSProcessQueueForPayment(RequestIds.FinanceRequestId, RequestIds.StandardRequestId, RequestIds.InsuranceRequestId, RequestIds.AdjustmentRequestId, Obj, UserId);
                }
                else
                {
                    RequestIds = clsSmileBrand.GetPMSSyncDetailsByGUID(Obj.RequestId);
                }
                switch (Obj.Type)
                {
                    case PatientPaymentType.DentrixFinanceCharges:
                        lstPatient_DentrixFinanceCharges = GetPatientFinance(Obj, RequestIds.FinanceRequestId);

                        break;
                    case PatientPaymentType.DentrixAdjustments:
                        lstPatient_DentrixAdjustments = GetPatientAdjustment(Obj, RequestIds.AdjustmentRequestId);
                        break;
                    case PatientPaymentType.DentrixInsurancePayment:
                        lstPatient_DentrixInsurancePayment = GetPatientInsurance(Obj, RequestIds.InsuranceRequestId);
                        break;
                    case PatientPaymentType.DentrixStandardPayment:
                        lstPatient_DentrixStandardPayment = GetPatientStandard(Obj, RequestIds.StandardRequestId);
                        break;
                    case PatientPaymentType.All:
                        lstPatient_DentrixFinanceCharges = GetPatientFinance(Obj, RequestIds.FinanceRequestId);
                        lstPatient_DentrixAdjustments = GetPatientAdjustment(Obj, RequestIds.AdjustmentRequestId);
                        lstPatient_DentrixInsurancePayment = GetPatientInsurance(Obj, RequestIds.InsuranceRequestId);
                        lstPatient_DentrixStandardPayment = GetPatientStandard(Obj, RequestIds.StandardRequestId);
                        break;
                }
                PatientPaymentModel objPatientPaymentModel = new PatientPaymentModel()
                {
                    lstPatient_DentrixFinanceCharges = (lstPatient_DentrixFinanceCharges.Count > 0) ? lstPatient_DentrixFinanceCharges : null,
                    lstPatient_DentrixAdjustments = (lstPatient_DentrixAdjustments.Count > 0) ? lstPatient_DentrixAdjustments : null,
                    lstPatient_DentrixInsurancePayment = (lstPatient_DentrixInsurancePayment.Count > 0) ? lstPatient_DentrixInsurancePayment : null,
                    lstPatient_DentrixStandardPayment = (lstPatient_DentrixStandardPayment.Count > 0) ? lstPatient_DentrixStandardPayment : null,

                    FinanceCharges_RequestId = (lstPatient_DentrixFinanceCharges.Count > 0) ? RequestIds.FinanceRequestKey : null,
                    Adjustments_RequestId = (lstPatient_DentrixAdjustments.Count > 0) ? RequestIds.AdjustmentRequestKey : null,
                    InsurancePayment_RequestId = (lstPatient_DentrixInsurancePayment.Count > 0) ? RequestIds.InsuranceRequestKey : null,
                    StandardPayment_RequestId = (lstPatient_DentrixStandardPayment.Count > 0) ? RequestIds.StandardRequestKey : null,

                    FinanceCharges_TotalRecord = GetCountByRequestId(RequestIds.FinanceRequestId),
                    Adjustments_TotalRecord = GetCountByRequestId(RequestIds.AdjustmentRequestId),
                    InsurancePayment_TotalRecord = GetCountByRequestId(RequestIds.InsuranceRequestId),
                    StandardPayment_TotalRecord = GetCountByRequestId(RequestIds.StandardRequestId)
                };
                retrives.IsSuccess = true;
                retrives.Message = "Success";
                retrives.Result = objPatientPaymentModel;
                retrives.StatusCode = 200;
                return retrives;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("SmileBrand---GetPatientPayments", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Send Patient from into Patient listing page.
        /// </summary>
        /// <param name="PatientId"></param>
        /// <param name="DoctorId"></param>
        /// <param name="Email"></param>
        /// <param name="Name"></param>
        /// <param name="PatientName"></param>
        /// <returns></returns>
        public static bool SendForm(int PatientId, int DoctorId, string Email, string Name, string PatientName)
        {
            try
            {
                clsTemplate obj = new clsTemplate();
                return obj.SendFormToPatientEmail(PatientId, DoctorId, Email, Name, PatientName);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---SendForm", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Request a Payment to Patient from Patient Listing page.
        /// </summary>
        /// <param name="PatientId"></param>
        /// <param name="DoctorId"></param>
        /// <param name="Amount"></param>
        /// <returns></returns>
        public static bool RequestPayment(int PatientId, int DoctorId, string Amount)
        {
            try
            {
                clsTemplate obj = new clsTemplate();
                return obj.SendRequestPaymentEmail(PatientId, DoctorId, Amount);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---SendForm", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static List<AdjutmentDetails> GetPatientAdjustment(PatientPayments Obj, int AdjustmentRequestId)
        {
            try
            {
                List<AdjutmentDetails> lst = new List<AdjutmentDetails>();
                DataTable dt = clsSmileBrand.GetAdjustmentDetails(Obj, AdjustmentRequestId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        lst.Add(new AdjutmentDetails()
                        {
                            RL_ID = SafeValue<int>(dr["AdjustmentId"]),
                            adjustid = SafeValue<int>(dr["adjustId"]),
                            adjusttype = SafeValue<int>(dr["AdjustType"]),
                            patid = SafeValue<int>(dr["PatientId"]),
                            guarid = SafeValue<int>(dr["guarid"]),
                            procdate = SafeValue<DateTime>(dr["procdate"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(dr["procdate"]),
                            Dentrixprovid = SafeValue<string>(dr["provid"]),
                            RLprovid = SafeValue<string>(dr["RLProviderId"]),
                            amount = Math.Round(SafeValue<double>(dr["amount"]), 2),
                            history = SafeValue<bool>(dr["history"]),
                            automodifiedtimestamp = SafeValue<DateTime>(dr["automodifiedtimestamp"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(dr["automodifiedtimestamp"]),
                            adjustdescript = SafeValue<string>(dr["Adjustdescript"]),
                            adjusttypestring = SafeValue<string>(dr["AdjustTypeString"]),
                            createdate = SafeValue<DateTime>(dr["CreateDate"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(dr["CreateDate"]),
                            DentrixPatientId = SafeValue<int>(dr["DtxPatientid"]),
                            DentrixGuarId = SafeValue<int>(dr["DtxGuarId"]),
                            adjustdefid = SafeValue<int>(dr["Adjustdefid"]),
                            appliedtopayagreement = SafeValue<bool>(dr["appliedtopayagreement"]),
                            claimid = SafeValue<int>(dr["ClaimId"])

                        });
                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetPatient_Adjustment", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static List<FinanceCharges> GetPatientFinance(PatientPayments Obj, int FinanceRequestId)
        {
            try
            {
                List<FinanceCharges> lst = new List<FinanceCharges>();
                DataTable Dt = clsSmileBrand.GetFinanceChanges(Obj, FinanceRequestId);
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in Dt.Rows)
                    {
                        lst.Add(new FinanceCharges()
                        {
                            RL_ID = SafeValue<int>(dr["ChargeId"]),
                            chargeId = SafeValue<int>(dr["DtxchargeId"]),
                            patid = SafeValue<int>(dr["PatientId"]),
                            guarid = SafeValue<int>(dr["guarid"]),
                            procdate = SafeValue<DateTime>(dr["procdate"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(dr["procdate"]),
                            Dentrixprovid = SafeValue<string>(dr["provid"]),
                            RLprovid = SafeValue<string>(dr["RLProviderId"]),
                            amount = Math.Round(SafeValue<decimal>(dr["amount"]), 2),
                            history = SafeValue<bool>(dr["history"]),
                            automodifiedtimestamp = SafeValue<DateTime>(dr["automodifiedtimestamp"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(dr["automodifiedtimestamp"]),
                            chargetype = SafeValue<int>(dr["chargetype"]),
                            chargetypestring = SafeValue<string>(dr["chargetypestring"]),
                            DentrixGuarId = SafeValue<int>(dr["DentrixGarId"]),
                            DentrixPatientId = SafeValue<int>(dr["DtxPatientId"]),
                            createdate = SafeValue<DateTime>(dr["CreateDate"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(dr["CreateDate"])
                        });
                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetPatientFinance", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Get Patient Insurance
        /// </summary>
        /// <param name="Obj"></param>
        /// <param name="RequestId"></param>
        /// <returns></returns>
        public static List<InsurancePayment> GetPatientInsurance(PatientPayments Obj, int RequestId)
        {
            try
            {
                List<InsurancePayment> lst = new List<InsurancePayment>();
                DataTable dt = clsSmileBrand.GetInsuranceDetails(Obj, RequestId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        lst.Add(new InsurancePayment()
                        {
                            RL_ID = SafeValue<int>(dr["PaymentId"]),
                            paymentId = SafeValue<int>(dr["DtxpaymentId"]),
                            inspaytype = SafeValue<int>(dr["InsurancePayType"]),
                            patid = SafeValue<int>(dr["PatientId"]),
                            guarid = SafeValue<int>(dr["guarid"]),
                            procdate = SafeValue<DateTime>(dr["procdate"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(dr["procdate"]),
                            Dentrixprovid = SafeValue<string>(dr["provid"]),
                            RLprovid = SafeValue<string>(dr["RLProviderId"]),
                            amount = Math.Round(SafeValue<decimal>(dr["amount"]), 2),
                            history = SafeValue<bool>(dr["history"]),
                            automodifiedtimestamp = SafeValue<DateTime>(dr["automodifiedtimestamp"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(dr["automodifiedtimestamp"]),
                            claimid = SafeValue<int>(dr["ClaimId"]),
                            createdate = SafeValue<DateTime>(dr["CreatedDate"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(dr["CreatedDate"]),
                            inspaydescript = SafeValue<string>(dr["InsurancePayDescription"]),
                            DentrixGuarId = SafeValue<int>(dr["DentrixGarId"]),
                            DentrixPatientId = SafeValue<int>(dr["DtxPatientId"])

                        });
                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetPatientInsurance", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static int GetCountByRequestId(int RequestId)
        {
            try
            {
                return clsSmileBrand.GetCountByRequestId(RequestId);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetCountByRequestId", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static List<StandardPayment> GetPatientStandard(PatientPayments Obj, int RequestId)
        {
            try
            {
                List<StandardPayment> lst = new List<StandardPayment>();
                DataTable dt = clsSmileBrand.GetStandardDetails(Obj, RequestId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        lst.Add(new StandardPayment()
                        {
                            RL_ID = SafeValue<int>(dr["StdPaymentId"]),
                            paymentId = SafeValue<int>(dr["paymentId"]),
                            patid = SafeValue<int>(dr["PatientId"]),
                            guarid = SafeValue<int>(dr["guarid"]),
                            procdate = SafeValue<DateTime>(dr["procdate"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(dr["procdate"]),
                            Dentrixprovid = SafeValue<string>(dr["provid"]),
                            RLprovid = SafeValue<string>(dr["RLProviderId"]),
                            amount = Math.Round(SafeValue<decimal>(dr["amount"]), 2),
                            history = SafeValue<bool>(dr["history"]),
                            automodifiedtimestamp = SafeValue<DateTime>(dr["automodifiedtimestamp"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(dr["automodifiedtimestamp"]),
                            createdate = SafeValue<DateTime>(dr["CreatedDate"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(dr["CreatedDate"]),
                            paymentdefid = SafeValue<int>(dr["PaymentDefId"]),
                            appliedtopayagreement = SafeValue<bool>(dr["AppliedToPayAgreement"]),
                            paymentdesc = SafeValue<string>(dr["Description"]),
                            DentrixGuarId = SafeValue<int>(dr["DentrixGarId"]),
                            DentrixPatientId = SafeValue<int>(dr["DtxPatientId"])

                        });
                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetPatientStandard", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool RequestReviews(int PatientId, string Email, string PatientFirstName, string PatientLastName, string PublicProfile, string UserFirstName, string UserLastName)
        {
            try
            {
                clsTemplate obj = new clsTemplate();
                return obj.RequestReviewsToPatientEmail(PatientId, Email, PatientFirstName, PatientLastName, PublicProfile, UserFirstName, UserLastName);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---RequestReviews", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static DataTable GetPatientDetailsBy_Id(int PatientId)
        {
            DataTable dt = new DataTable();

            try
            {
                clsPatientsData objclsPatientsData = new clsPatientsData();
                dt = objclsPatientsData.GetPatientsDetails(SafeValue<int>(PatientId));

                return dt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetPatientDetailsById", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Delete Patient from Patient Listing
        /// </summary>
        /// <param name="delete"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static PatientDelete DeletePatient(DeleteMultiplePatient delete, int UserId)
        {
            try
            {
                PatientDelete pdcnt = new PatientDelete(); bool status = false;

                string[] values = delete.MultiplePatientId[0].Split(',');
                foreach (var item in values)
                {
                    bool result = new clsColleaguesData().GetCommunicationHistoryForPatientByDocID(UserId, SafeValue<int>(item));
                    if (result)
                    {
                        DataTable dt = new clsPatientsData().GetPatientsDetails(SafeValue<int>(item));
                        string fname = SafeValue<string>(dt.Rows[0]["FirstName"]);
                        string lname = SafeValue<string>(dt.Rows[0]["LastName"]);
                        string fullname = lname + " " + fname;
                        pdcnt.UnRemovedCount++;
                    }
                    else
                    {
                        status = new PatientBLL().RemovePatient(SafeValue<int>(item), UserId);
                        pdcnt.RemovedCount++;
                    }
                }
                return pdcnt;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---DeletePatient", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Get Patient Details for Patient History.
        /// </summary>
        /// <param name="PatientId"></param>
        /// <param name="DoctorId"></param>
        /// <param name="TimeZoneSystemName"></param>
        /// <returns></returns>
        public static PatientHistoryViewModel GetPatientDetails(int PatientId, int DoctorId, string TimeZoneSystemName)
        {
            try
            {

                TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
                PatientHistoryViewModel objPatientHistory = new PatientHistoryViewModel();
                DataSet ds = new clsPatientsData().GetPateintDetailsByPatientId(PatientId, DoctorId);
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    DataTable dtTable = ds.Tables[0];
                    if (dtTable != null && dtTable.Rows.Count > 0)
                    {
                        string strGender = SafeValue<string>(dtTable.Rows[0]["Gender"]);
                        if (!string.IsNullOrWhiteSpace(strGender) && strGender == "0")
                        {
                            strGender = "Male";
                        }
                        else if (!string.IsNullOrWhiteSpace(strGender) && strGender == "1")
                        {
                            strGender = "Female";
                        }
                        objPatientHistory.PatientId = SafeValue<int>(dtTable.Rows[0]["PatientId"]);
                        objPatientHistory.Gender = strGender;
                        objPatientHistory.Age = SafeValue<string>(dtTable.Rows[0]["Age"]);
                        objPatientHistory.FirstName = SafeValue<string>(dtTable.Rows[0]["FirstName"]);
                        objPatientHistory.LastName = SafeValue<string>(dtTable.Rows[0]["LastName"]);
                        if (SafeValue<string>(dtTable.Rows[0]["DateOfBirth"]) != null && SafeValue<string>(dtTable.Rows[0]["DateOfBirth"]) != "")
                        {
                            //RM-294 Changes for adding a leading zero in mdlNewPateint.DateOfBirth
                            DateTime DOB = SafeValue<DateTime>(dtTable.Rows[0]["DateOfBirth"]);
                            objPatientHistory.DateOfBirth = new CommonBLL().ConvertToDateNew(SafeValue<DateTime>(DOB), SafeValue<int>(BO.Enums.Common.DateFormat.GENERAL));
                        }
                        //DateOfBirth = SafeValue<string>(dtTable.Rows[0]["DateOfBirth"]) != string.Empty ? SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(dtTable.Rows[0]["DateOfBirth"]); TimeZoneSystemName)) : string.Empty;//SafeValue<string>(dtTable.Rows[0]["DateOfBirth"]);
                        //objPatientHistory.DateOfBirth = SafeValue<string>(dtTable.Rows[0]["DateOfBirth"]); // SUP-119 Removing UTC for DOB 
                        //DateOfBirth = SafeValue<string>(dtTable.Rows[0]["DateOfBirth"]) != string.Empty ? SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(dtTable.Rows[0]["DateOfBirth"]); TimeZoneSystemName)) : string.Empty;//SafeValue<string>(dtTable.Rows[0]["DateOfBirth"]);
                        //objPatientHistory.DateOfBirth = SafeValue<string>(dtTable.Rows[0]["DateOfBirth"]); // SUP-119 Removing UTC for DOB 
                        objPatientHistory.AssignedPatientId = SafeValue<string>(dtTable.Rows[0]["AssignedPatientId"]);
                        objPatientHistory.OwnerId = SafeValue<int>(dtTable.Rows[0]["OwnerId"]);
                        objPatientHistory.Email = SafeValue<string>(dtTable.Rows[0]["Email"]);
                        objPatientHistory.SecondryEmail = SafeValue<string>(dtTable.Rows[0]["SecondaryEmail"]);
                        objPatientHistory.AddressInfoEmail = SafeValue<string>(dtTable.Rows[0]["EmailAddress"]);
                        objPatientHistory.ProfileImage = SafeValue<string>(dtTable.Rows[0]["ProfileImage"]);
                        objPatientHistory.Notes = SafeValue<string>(dtTable.Rows[0]["Notes"]);
                        objPatientHistory.ExactAddress = SafeValue<string>(dtTable.Rows[0]["ExactAddress"]);
                        objPatientHistory.Address2 = SafeValue<string>(dtTable.Rows[0]["Address2"]);
                        objPatientHistory.City = SafeValue<string>(dtTable.Rows[0]["City"]);
                        objPatientHistory.State = SafeValue<string>(dtTable.Rows[0]["STATE"]);
                        objPatientHistory.Country = SafeValue<string>(dtTable.Rows[0]["Country"]);
                        objPatientHistory.ZipCode = SafeValue<string>(dtTable.Rows[0]["ZipCode"]);
                        objPatientHistory.Phone = SafeValue<string>(dtTable.Rows[0]["Phone"]);
                        objPatientHistory.Workphone = SafeValue<string>(dtTable.Rows[0]["WorkPhone"]);
                        objPatientHistory.Mobile = SafeValue<string>(dtTable.Rows[0]["Mobile"]);
                        objPatientHistory.Fax = SafeValue<string>(dtTable.Rows[0]["Fax"]);
                        objPatientHistory.FullName = SafeValue<string>(dtTable.Rows[0]["FullName"]);
                        // objPatientHistory.MiddelName = SafeValue<string>(dtTable.Rows[0]["MiddelName"]);
                        //ImageName = SafeValue<string>(dtTable.Rows[0]["ImageName"]);
                        objPatientHistory.ExactPassword = SafeValue<string>(dtTable.Rows[0]["ExactPassword"]);
                        objPatientHistory.ReferredById = SafeValue<string>(dtTable.Rows[0]["ReferredById"]);
                        objPatientHistory.ReferredBy = SafeValue<string>(dtTable.Rows[0]["ReferredBy"]);
                        objPatientHistory.Location = SafeValue<string>(dtTable.Rows[0]["Location"]);
                        objPatientHistory.FirstExamDate = SafeValue<string>(dtTable.Rows[0]["FirstExamDate"]) != string.Empty ? SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(dtTable.Rows[0]["FirstExamDate"]), TimeZoneSystemName)) : string.Empty;//SafeValue<string>(dtTable.Rows[0]["FirstExamDate"]);
                        objPatientHistory.EncryptesPId = string.Empty;
                        objPatientHistory.DoctorFullName = string.Empty;
                        objPatientHistory.Password = SafeValue<string>(dtTable.Rows[0]["Password"]);
                    }
                    //string DOB = objPatientHistory.DateOfBirth;
                    //if (!string.IsNullOrEmpty(DOB))
                    //{
                    //    DateTime? data = SafeValue<DateTime>(DOB);
                    //    TimeSpan timeofDay = data.Value.TimeOfDay;
                    //    if (timeofDay != TimeSpan.Zero)
                    //    {
                    //        DOB = DOB != string.Empty ? SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(DOB), TimeZoneSystemName)) : string.Empty;
                    //    }
                    //}
                    //objPatientHistory.DateOfBirth = DOB;
                    objPatientHistory.EncryptesPId = ObjTripleDESCryptoHelper.encryptText(SafeValue<string>(PatientId));
                    if (objPatientHistory.Gender != null && objPatientHistory.Gender == "Female")
                    {
                        objPatientHistory.ImageName =
                            objPatientHistory.ProfileImage == "" ?
                          SafeValue<string>(ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"]) :
                          SafeValue<string>(ConfigurationManager.AppSettings["imagebankpathPatient"]) + objPatientHistory.ProfileImage;

                    }
                    else
                    {
                        objPatientHistory.ImageName =
                             objPatientHistory.ProfileImage == "" ?
                          SafeValue<string>(ConfigurationManager.AppSettings["DefaultDoctorImage"]) :
                          //XQ1-511 User is not able to upload patient profile picture, under 'Patient History' page.
                          SafeValue<string>(ConfigurationManager.AppSettings["imagebankpathPatient"]) +
                            objPatientHistory.ProfileImage;
                    }
                    objPatientHistory.lstColleaguesMessagesDoctor = GetCommunicationHistoryForPatient(DoctorId, PatientId, 1, 20, 1, 2, TimeZoneSystemName);
                }
                objPatientHistory.LstFamily = GetFamilyHistoryDetails(PatientId);
                objPatientHistory._insuranceCoverage = ReferralFormBLL.GetInsuranceDetails(PatientId);
                objPatientHistory._medicalHistory = ReferralFormBLL.GetMedicalHistoryDetails(PatientId);
                objPatientHistory._dentalhistory = ReferralFormBLL.GetDentalHistoryDetails(PatientId);
                objPatientHistory.stateLists = GetStateList("US");
                // Get colleagues count for autocomplate drop down
                Colleaguelist objColleaguelist = new Colleaguelist();
                objColleaguelist = new SearchProfileOfDoctor().GetColleagueListById(DoctorId, BO.Enums.Common.PMSProviderFilterType.Provider);
                objPatientHistory.ColleaguesCount = objColleaguelist.RecordCount;

                return objPatientHistory;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetPatientDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static List<ColleaguesMessagesOfDoctorViewModel> GetCommunicationHistoryForPatient(int UserId, int PatientId, int PageIndex, int PageSize, int SortColumn, int SortDirection, string TimeZoneSystemName)
        {
            try
            {
                DataTable dt = new clsColleaguesData().GetCommunicationHistoryForPatient(UserId, PatientId, PageIndex, PageSize, SortColumn, SortDirection);
                List<ColleaguesMessagesOfDoctorViewModel> lstColleaguesMessagesDoctor = new List<ColleaguesMessagesOfDoctorViewModel>();
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        ColleaguesMessagesOfDoctorViewModel objcolleaguesdata = new ColleaguesMessagesOfDoctorViewModel();
                        objcolleaguesdata.lstImages = new List<MessageAttachmentViewModel>();
                        objcolleaguesdata.MessageId = SafeValue<int>(item["MessageId"]);
                        objcolleaguesdata.MessageAttachments = ColleagueMessageAttachments(objcolleaguesdata.MessageId);
                        objcolleaguesdata.MessageTypeId = SafeValue<int>(SafeValue<string>(item["MessageTypeId"]));
                        objcolleaguesdata.ColleagueName = SafeValue<string>(item["ColleagueName"]);
                        string Body = HttpUtility.UrlDecode(SafeValue<string>(item["Messages"]));
                        HtmlDocument htmlDoc = new HtmlDocument();
                        htmlDoc.LoadHtml(Body);
                        string BOdy = System.Uri.UnescapeDataString(SafeValue<string>(item["Messages"]));
                        objcolleaguesdata.Message = Regex.Replace(BOdy, @"<[^>]+>|&nbsp;", "").Trim();
                        objcolleaguesdata.CreationDate = SafeValue<string>(item["CreationDate"]);
                        if (!string.IsNullOrEmpty(SafeValue<string>(item["ColleagueId"])))
                        {
                            objcolleaguesdata.ColleagueId = SafeValue<int>(item["ColleagueId"]);
                        }
                        int PatientId1 = PatientId > 0 ? PatientId : 0;
                        objcolleaguesdata.PatientId = PatientId1;

                        if (!string.IsNullOrEmpty(objcolleaguesdata.CreationDate))
                        {
                            DateTime dtCreationDate = clsHelper.ConvertFromUTC(SafeValue<DateTime>(objcolleaguesdata.CreationDate), TimeZoneSystemName);
                            objcolleaguesdata.CreationDate = new CommonBLL().ConvertToDateTime(SafeValue<DateTime>(dtCreationDate), SafeValue<int>(DateFormat.GENERAL));
                        }
                        lstColleaguesMessagesDoctor.Add(objcolleaguesdata);
                    }
                }
                return lstColleaguesMessagesDoctor;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetCommunicationHistoryForPatient", Ex.Message, Ex.StackTrace);
                throw;
            }
        }



        /// <summary>
        ///  Jira XQ1-550 - Point 3  - Get File name in Communications History List 
        /// </summary>
        /// <param name="MessageId"></param>
        /// <returns></returns>
        public static List<MessageAttachmentsViewModel> ColleagueMessageAttachments(int MessageId)
        {
            DataTable dt = new DataTable();
            List<MessageAttachmentsViewModel> _listMessageAttechment = new List<MessageAttachmentsViewModel>();
            string qry = "select * from ColleagueMessageAttachments where MessageId =" + MessageId;
            dt = new clsCommon().DataTable(qry);
            if (dt.Rows.Count > 0)
            {
                _listMessageAttechment = dt.DataTableToList<MessageAttachmentsViewModel>();
            }
            return _listMessageAttechment;
        }



        /// <summary>
        /// Get Communication Colleague list 
        /// </summary>
        /// <param name="PatientId"></param>
        /// <param name="UserId"></param>
        /// <param name="selected"></param>
        /// <returns></returns>
        public static List<SelectListItem> GetCommunicationColleagueList(int PatientId, int UserId, string selected)
        {
            try
            {
                List<SelectListItem> lst = new List<SelectListItem>();
                DataTable dt = new clsPatientsData().GetColleagueTeamMemberandTreatingDoctorList(PatientId, UserId);
                selected = selected ?? string.Empty;
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        string Id = SafeValue<string>(item["ColleaguesId"]);
                        if (selected.Split(',').Contains(Id))
                        {
                            lst.Add(new SelectListItem()
                            {
                                Text = SafeValue<string>(item["ColleaguesId"]),
                                Value = SafeValue<string>(item["ColleaguesName"]),
                                Selected = true
                            });
                        }
                        else
                        {
                            lst.Add(new SelectListItem()
                            {
                                Text = SafeValue<string>(item["ColleaguesId"]),
                                Value = SafeValue<string>(item["ColleaguesName"]),
                            });
                        }
                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetCommunicationColleagueList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Selected List for Compose Communication section
        /// </summary>
        /// <param name="PatientId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static string GetSelectedListforComposeComm(int PatientId, int UserId)
        {
            try
            {
                DataTable dt = new clsPatientsData().GetSelectedTeamMemberForPatientHistory(PatientId, UserId);
                var TreatingDoctors = dt.AsEnumerable().ToList().AsEnumerable().Select(e1 => new { Id = SafeValue<string>(e1["ColleaguesId"]) });
                string strList = "";
                foreach (var itm in TreatingDoctors)
                {
                    if (strList.Length > 0)
                    {
                        strList += ",";
                    }
                    strList += SafeValue<string>(itm.Id);
                }
                return strList;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetSelectedListforComposeComm", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get All selected colleague List
        /// </summary>
        /// <param name="PatientId"></param>
        /// <param name="UserId"></param>
        /// <param name="selected"></param>
        /// <returns></returns>
        public static List<SelectListItem> GetSelectedListforcollegues(int PatientId, int UserId, string selected)
        {
            try
            {
                List<SelectListItem> lst = new List<SelectListItem>();
                DataTable dt = new clsPatientsData().GetColleagueTeamMemberandTreatingDoctorList(PatientId, UserId);
                selected = selected ?? string.Empty;
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        string Id = SafeValue<string>(item["ColleaguesId"]);
                        if (selected.Split(',').Contains(Id))
                        {
                            lst.Add(new SelectListItem()
                            {
                                Value = SafeValue<string>(item["ColleaguesId"]),
                                Text = SafeValue<string>(item["ColleaguesName"]),                              
                            });
                        }                      
                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetSelectedListforcollegues", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Patient Images
        /// </summary>
        /// <param name="PatientId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static List<UplodedImagesViewModel> GetPatientImages(int PatientId, int UserId)
        {
            try
            {
                List<UplodedImagesViewModel> objpatientMessage = new List<UplodedImagesViewModel>();
                DataTable dt = new clsPatientsData().GetPatientImages(PatientId, UserId);
                string TimeZoneSystemName = new clsAppointmentData().GetTimeZoneOfDoctor(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objpatientMessage = (from p in dt.AsEnumerable()
                                         select new UplodedImagesViewModel
                                         {
                                             ImageId = SafeValue<int>(p["ImageId"]),
                                             PatientId = SafeValue<int>(p["PatientId"]),
                                             Name = SafeValue<string>(p["Name"]),
                                             ImagePath = SafeValue<string>(p["FullPath"]),
                                             RelativePath = SafeValue<string>(p["RelativePath"]),
                                             CreationDate = SafeValue<DateTime>(p["CreationDate"]) != DateTime.MinValue ? new CommonBLL().ConvertToDateTime(clsHelper.ConvertFromUTC(SafeValue<DateTime>(p["CreationDate"]), TimeZoneSystemName), (int)DateFormat.GENERAL) : string.Empty,
                                             LastModifiedDate = SafeValue<DateTime>(p["LastModifiedDate"]) != DateTime.MinValue ? new CommonBLL().ConvertToDateTime(clsHelper.ConvertFromUTC(SafeValue<DateTime>(p["LastModifiedDate"]), TimeZoneSystemName), (int)DateFormat.GENERAL) : string.Empty,
                                             // MontageId = SafeValue<int>(p["MontageId"]),
                                             Timepoint = SafeValue<int>(p["Timepoint"]),
                                             FullPath = SafeValue<string>(p["FullPath"]),
                                             Height = SafeValue<int>(p["Height"]),
                                             Weight = SafeValue<int>(p["Width"]),
                                             ImageTypeId = SafeValue<int>(p["ImageType"]),
                                             ImageFormat = SafeValue<string>(p["ImageFormat"]),
                                             Description = SafeValue<string>(p["Description"]),
                                             Uploadby = SafeValue<string>(p["Uploadby"]),
                                             ImageStatus = SafeValue<bool>(p["ImageStatus"]),
                                             //ImagePosition = SafeValue<int>(p["ImagePosition"]),
                                             ImageTypeName = SafeValue<string>(p["ImageTypeName"]),
                                             //DateForApi = SafeValue<string>(p["DateForApi"]) != string.Empty ? SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(p["DateForApi"]), TimeZoneSystemName)) : string.Empty,
                                             DocumentName = string.Empty,
                                             DocumentId = 0,
                                         }).ToList();
                }
                return objpatientMessage;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetPatientImages", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Patient Documents
        /// </summary>
        /// <param name="PatientId"></param>
        /// <param name="UserId"></param>
        /// <param name="TimeZone"></param>
        /// <returns></returns>
        public static List<UplodedImagesViewModel> GetPatientDocuments(int PatientId, int UserId, string TimeZone)
        {
            try
            {
                List<UplodedImagesViewModel> objuplodedDocumentList = new List<UplodedImagesViewModel>();
                DataTable dt = new clsPatientsData().GetPatientDocumentsAll(PatientId, UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objuplodedDocumentList = (from p in dt.AsEnumerable()
                                              select new UplodedImagesViewModel
                                              {
                                                  ImageId = 0,
                                                  PatientId = SafeValue<int>(p["PatientId"]),
                                                  CreationDate = SafeValue<DateTime>(p["CreationDate"]) != DateTime.MinValue ? new CommonBLL().ConvertToDateTime(clsHelper.ConvertFromUTC(SafeValue<DateTime>(p["CreationDate"]), TimeZone), (int)DateFormat.GENERAL) : string.Empty,
                                                  LastModifiedDate = SafeValue<DateTime>(p["LastModifiedDate"]) != DateTime.MinValue ? new CommonBLL().ConvertToDateTime(clsHelper.ConvertFromUTC(SafeValue<DateTime>(p["LastModifiedDate"]), TimeZone), (int)DateFormat.GENERAL) : string.Empty,
                                                  MontageId = SafeValue<int>(p["MontageId"]),
                                                  Description = SafeValue<string>(p["Description"]),
                                                  Uploadby = SafeValue<string>(p["Uploadby"]),
                                                  DateForApi = SafeValue<DateTime>(p["DateForApi"]) != DateTime.MinValue ? new CommonBLL().ConvertToDateTime(clsHelper.ConvertFromUTC(SafeValue<DateTime>(p["DateForApi"]), TimeZone), (int)DateFormat.GENERAL) : string.Empty,
                                                  DocumentName = SafeValue<string>(p["DocumentName"]),
                                                  DocumentId = SafeValue<int>(p["DocumentId"])
                                              }).ToList();
                }
                return objuplodedDocumentList;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetPatientDocuments", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Delete Patient Images
        /// </summary>
        /// <param name="ImageId"></param>
        /// <param name="MontageId"></param>
        /// <returns></returns>
        public static bool DeletePatientImage(int ImageId, int MontageId)
        {
            try
            {
                return new clsPatientsData().RemovePatientImage(ImageId, MontageId);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---DeletePatientImage", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Delete Patient Documents
        /// </summary>
        /// <param name="MontageId"></param>
        /// <returns></returns>
        public static bool DeletePatientDocument(int MontageId)
        {
            try
            {
                return new clsPatientsData().RemovePatientDocumentById(MontageId);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---DeletePatientDocument", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get File Details of Patient
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="Type"></param>
        /// <param name="PatientId"></param>
        /// <param name="TimeZone"></param>
        /// <returns></returns>
        public static EditFilesViewModel FileDetails(int Id, int Type, int PatientId, string TimeZone)
        {
            try
            {
                DataTable Dt = new DataTable(); EditFilesViewModel edit = new EditFilesViewModel();
                //For Images we are using type = 1 only for this code.
                Dt = (Type == 1) ? new clsPatientsData().GetDetailByImageId(Id) : new clsPatientsData().GetDescriptionofDocumentById(PatientId, Id);
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    if (Type == 1)
                    {
                        edit.ImageId = SafeValue<int>(Dt.Rows[0]["ImageId"]);
                        edit.PatientId = SafeValue<int>(Dt.Rows[0]["PatientId"]);
                        edit.Name = SafeValue<string>(Dt.Rows[0]["Name"]);
                        edit.CreationDate = SafeValue<string>(Dt.Rows[0]["CreationDate"]) != string.Empty ? SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(Dt.Rows[0]["CreationDate"]), TimeZone)) : string.Empty;
                        edit.LastModifiedDate = SafeValue<string>(Dt.Rows[0]["LastModifiedDate"]) != string.Empty ? SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(Dt.Rows[0]["LastModifiedDate"]), TimeZone)) : string.Empty;
                        // MontageId = SafeValue<int>(Dt.Rows[0]["MontageId"]);
                        edit.Timepoint = SafeValue<int>(Dt.Rows[0]["Timepoint"]);
                        edit.Height = SafeValue<int>(Dt.Rows[0]["Height"]);
                        edit.Weight = SafeValue<int>(Dt.Rows[0]["Width"]);
                        edit.Description = SafeValue<string>(Dt.Rows[0]["Description"]);
                        edit.Uploadby = SafeValue<string>(Dt.Rows[0]["Uploadby"]);
                        edit.ImageStatus = SafeValue<bool>(Dt.Rows[0]["ImageStatus"]);
                        edit.FullPathToImage = SafeValue<string>(Dt.Rows[0]["FullPathToImage"]);
                        edit.RelativePathToImage = SafeValue<string>(Dt.Rows[0]["RelativePathToImage"]);
                        edit.LastModifiedBy = SafeValue<int>(Dt.Rows[0]["LastModifiedBy"]);
                        edit.ImageTypeId = SafeValue<int>(Dt.Rows[0]["ImageTypeId"]);
                        edit.OwnerId = SafeValue<int>(Dt.Rows[0]["OwnerId"]);
                        edit.ImageFormatTypeId = SafeValue<int>(Dt.Rows[0]["ImageFormatTypeId"]);
                        // PositionInMontage = SafeValue<int>(Dt.Rows[0]["PositionInMontage"]);
                        edit.CreationDateForApi = SafeValue<string>(Dt.Rows[0]["CreationDateForApi"]) != string.Empty ? SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(Dt.Rows[0]["CreationDateForApi"]), TimeZone)) : string.Empty;
                        edit.LastModifiedDateForApi = SafeValue<string>(Dt.Rows[0]["LastModifiedDateForApi"]) != string.Empty ? SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(Dt.Rows[0]["LastModifiedDateForApi"]), TimeZone)) : string.Empty;
                        edit.CreationDateNew = SafeValue<string>(Dt.Rows[0]["CreationDateNew"]) != string.Empty ? SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(Dt.Rows[0]["CreationDateNew"]), TimeZone)) : string.Empty;
                        edit.LastModifiedDateNew = SafeValue<string>(Dt.Rows[0]["LastModifiedDateNew"]) != string.Empty ? SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(Dt.Rows[0]["LastModifiedDateNew"]), TimeZone)) : string.Empty;
                        edit.DocumentName = string.Empty;
                        edit.DocumentId = 0;
                    }
                    else
                    {
                        edit.MontageId = SafeValue<int>(Dt.Rows[0]["MontageId"]);
                        edit.PatientId = SafeValue<int>(Dt.Rows[0]["PatientId"]);
                        edit.DocumentName = SafeValue<string>(Dt.Rows[0]["DocumentName"]);
                        edit.CreationDate = SafeValue<string>(Dt.Rows[0]["CreationDate"]) != string.Empty ? SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(Dt.Rows[0]["CreationDate"]), TimeZone)) : string.Empty;
                        edit.LastModifiedDate = SafeValue<string>(Dt.Rows[0]["LastModifiedDate"]) != string.Empty ? SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(Dt.Rows[0]["LastModifiedDate"]), TimeZone)) : string.Empty;
                        edit.CreationDateNew = SafeValue<string>(Dt.Rows[0]["CreationDateNew"]) != string.Empty ? SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(Dt.Rows[0]["CreationDateNew"]), TimeZone)) : string.Empty;
                        edit.LastModifiedDateNew = SafeValue<string>(Dt.Rows[0]["LastModifiedDateNew"]) != string.Empty ? SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(Dt.Rows[0]["LastModifiedDateNew"]), TimeZone)) : string.Empty;
                        edit.Description = SafeValue<string>(Dt.Rows[0]["Description"]);
                        edit.DocumentId = SafeValue<int>(Dt.Rows[0]["DocumentId"]);
                        edit.CreationDateForApi = SafeValue<string>(Dt.Rows[0]["CreationDateForApi"]) != string.Empty ? SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(Dt.Rows[0]["CreationDateForApi"]), TimeZone)) : string.Empty;
                        edit.LastModifiedDateForApi = SafeValue<string>(Dt.Rows[0]["LastModifiedDateForApi"]) != string.Empty ? SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(Dt.Rows[0]["LastModifiedDateForApi"]), TimeZone)) : string.Empty;
                    }
                }
                return edit;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---EditFile", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Edit file details form Patient history
        /// </summary>
        /// <param name="alterFile"></param>
        /// <returns></returns>
        public static bool EditFileDetails(AlterFile alterFile)
        {
            try
            {
                if (alterFile.FileType == 1)
                {
                    return new clsPatientsData().UpdateDescriptionofImage(SafeValue<int>(alterFile.Id), alterFile.CreationDate, alterFile.ModifiedDate, alterFile.Description);
                }
                else
                {
                    return new clsPatientsData().UpdateDescriptionofDocument(SafeValue<int>(alterFile.Id), alterFile.CreationDate, alterFile.ModifiedDate, alterFile.Description);
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---EditFileDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get State list
        /// </summary>
        /// <param name="CountryCode"></param>
        /// <returns></returns>
        public static List<StateList> GetStateList(string CountryCode)
        {
            try
            {
                CountryCode = (!string.IsNullOrWhiteSpace(CountryCode)) ? CountryCode : "US";
                List<StateList> lst = new List<StateList>();
                DataTable dt = new clsCommon().GetAllStateByCountryCode(CountryCode);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        if (SafeValue<string>(item["StateName"]) != "[Select State]")
                        {
                            lst.Add(new StateList()
                            {
                                StateCode = clsCommon.SafeValue<string>(item["StateCode"]),
                                StateName = clsCommon.SafeValue<string>(item["StateName"])
                            });
                        }
                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetStateList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This is used for add/edit patient details
        /// </summary>
        /// <param name="patients"></param>
        /// <returns></returns>
        public static int AddEditPatientDetails(Patients patients)
        {
            try
            {
                return new clsPatientsData().PatientInsertAndUpdateNew(patients);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---AddEditPatientDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }


        public static int AddEditPatientDetailsForVeri(Reward_PatientUpdate objReward_PatientUpdate)
        {
            try
            {
                int intPatientId = 0;
                Patients objPatient = new Patients();
                objPatient.FirstName = objReward_PatientUpdate.FirstName;
                objPatient.LastName = objReward_PatientUpdate.LastName;
                objPatient.Email = objReward_PatientUpdate.Email;
                objPatient.Phone = objReward_PatientUpdate.WorkPhone;
                objPatient.DateOfBirth = SafeValue<string>(objReward_PatientUpdate.DateOfBirth);
                intPatientId = new clsPatientsData().PatientInsertAndUpdateNew(objPatient);
                if (intPatientId > 0)
                {
                    bool IsInsuranceCoverage = PatientRewardBLL.InsuranceCoverage(SafeValue<string>(intPatientId), objReward_PatientUpdate.InsuranceCoverage, objReward_PatientUpdate.PrimaryDentalInsurance, objReward_PatientUpdate.SecondaryDentalInsurance);
                }
                return intPatientId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---AddEditPatientDetailsForVeri", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Add/Edit Patient for verident
        /// </summary>
        /// <param name="objReward_PatientUpdate"></param>
        /// <param name="intOwnerID"></param>
        /// <returns></returns>
        public static int AddEditPatientDetailsForVeri(Reward_PatientUpdate objReward_PatientUpdate,int intOwnerID)
        {
            try
            {
                int intPatientId = 0;
                Patients objPatient = new Patients();
                objPatient.FirstName = objReward_PatientUpdate.FirstName;
                objPatient.LastName = objReward_PatientUpdate.LastName;
                objPatient.Email = objReward_PatientUpdate.Email;
                objPatient.Phone = objReward_PatientUpdate.WorkPhone;
                objPatient.DateOfBirth = SafeValue<string>(objReward_PatientUpdate.DateOfBirth);
                objPatient.OwnerId = intOwnerID;
                objPatient.PatientId = objReward_PatientUpdate.RewardPartnerId;
                intPatientId = new clsPatientsData().PatientInsertAndUpdateNew(objPatient);
                SDIC objSDIC = new SDIC();                
                if (intPatientId > 0)
                {
                    
                    bool IsInsuranceCoverage = PatientRewardBLL.InsuranceCoverage(SafeValue<string>(intPatientId), objReward_PatientUpdate.InsuranceCoverage, objReward_PatientUpdate.PrimaryDentalInsurance, objSDIC);
                }
                return intPatientId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---AddEditPatientDetailsForVeri", Ex.Message, Ex.StackTrace);
                throw;
            }
        }



        /// <summary>
        /// Send Message from Patient history using Communication section.
        /// </summary>
        /// <returns></returns>
        public static bool SendMessageFromPatientHistory(ComposeCommunication compose)
        {
            try
            {
                int NewMessageId = 0;
                #region This condition added because when user forgot to add colleagues into To list then all colleagues will received this message
                if (string.IsNullOrWhiteSpace(compose.ReciverId))
                {
                    string separator = "";
                    DataTable dt = new DataTable();
                    dt = new clsColleaguesData().SendPatientHistoryMessageforall(compose.PatientId, compose.UserId);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            compose.ReciverId += separator + SafeValue<string>(item["ColleaguesId"]);
                            separator = ",";
                        }
                    }
                }
                #endregion

                if (!string.IsNullOrEmpty(compose.ReciverId))
                {
                    NewMessageId = new clsColleaguesData().SentMessageFromPatientHistory("", compose.Message, compose.UserId, 1, SafeValue<string>(compose.PatientId), compose.ReciverId);

                }
                string[] ColleaguesId = SafeValue<string>(compose.ReciverId).Split(new char[] { ',' });
                if (ColleaguesId.Length != 0)
                {
                    foreach (var item in ColleaguesId)
                    {
                        if (!string.IsNullOrEmpty(item))
                        {
                            new clsTemplate().SendMessageToColleague(compose.UserId, SafeValue<int>(item), NewMessageId, 1);
                        }
                    }
                }
                // Solved Jira XQ1-1143 
                if (compose.Files != null)
                {
                    IDictionary<string, bool> response = UploadFilesFromTemp(compose.UserId, compose.PatientId, false, compose.Files);
                    foreach (var item in response)
                    {
                        if (!string.IsNullOrEmpty(item.Key))
                        {
                            bool Attach = new clsColleaguesData().InsertMessageAttachemntsForColleague(NewMessageId, item.Key);
                        }
                    }
                }
                //Changes made for Display all files while sending Messages.
               
                return true;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---SendMessageFromPatientHistory", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Upload File From Temp folder to Image Bank
        /// </summary>
        /// <param name="DoctorId"></param>
        /// <param name="PatientId"></param>
        /// <param name="IsReferralForm"></param>
        /// <returns></returns>
        public static Dictionary<string, bool> UploadFilesFromTemp(int DoctorId = 0, int PatientId = 0, bool IsReferralForm = false, string[] Files = null)
        {
            try
            {
                Dictionary<string, bool> return_result = new Dictionary<string, bool>();
                string NewFileName = string.Empty;
                Dictionary<string, bool> referral_result = new Dictionary<string, bool>();
                List<string> objImageId = new List<string>();
                List<string> objFileId = new List<string>();
                foreach (var item in Files)
                {
                    string[] Attachments = SafeValue<string>(item).Split('_');
                    if (Attachments.Length > 0)
                    {
                        switch (Attachments[0])
                        {
                            case "F":
                                objFileId.Add(Attachments[1]);
                                break;
                            case "I":
                                objImageId.Add(Attachments[1]);
                                break;

                        }
                    }
                }
                DataTable dt = ObjCommon.GetTempAttachments(string.Join(",", objImageId), string.Join(",", objFileId), 0, 0);

                // DataTable dtTempUploadedDocs = ObjCommon.GetRecordsFromTempTablesForDoctor(DoctorId, null);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow itemTempUploadedDocs in dt.Rows)
                    {
                        clsPatientsData ObjPatientsData = new clsPatientsData();
                        string DocName = SafeValue<string>(itemTempUploadedDocs["UserFileName"]);
                        NewFileName = SafeValue<string>("$" + System.DateTime.Now.Ticks + "$" + DocName.Substring(SafeValue<string>(DocName).LastIndexOf("$") + 1));
                        string Extension = Path.GetExtension(NewFileName);
                        string sourceFile = TempFile + DocName;
                        string destinationFile = ImageBankFiles + NewFileName;
                        File.Copy(sourceFile, destinationFile);
                        if (Extension.ToLower() == ".jpg" || Extension.ToLower() == ".jpeg" || Extension.ToLower() == ".gif" || Extension.ToLower() == ".png" || Extension.ToLower() == ".bmp" || Extension.ToLower() == ".psd" || Extension.ToLower() == ".pspimage" || Extension.ToLower() == ".thm" || Extension.ToLower() == ".tif")
                        {
                            if (!Directory.Exists(ThumbFiles))
                            {
                                Directory.CreateDirectory(ThumbFiles);
                                SetPermissions(ThumbFiles);
                            }

                            // Load image.
                            Image image = Image.FromFile(destinationFile);

                            // Compute thumbnail size.
                            Size thumbnailSize = ObjCommon.GetThumbnailSize(image);

                            // Get thumbnail.
                            Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                                thumbnailSize.Height, null, IntPtr.Zero);

                            // Save thumbnail.
                            thumbnail.Save(ThumbFiles + NewFileName);

                            // Add the data in image table
                            // In referral Result : 0- Image, 1- Document
                            int imageid = ObjPatientsData.AddImagetoPatient(PatientId, 1, NewFileName, 1000, 1000, 0, 0, ImageBankFiles, NewFileName, DoctorId, 0, 1, "Doctor");
                            referral_result.Add(SafeValue<string>(imageid), true);
                            return_result.Add(NewFileName, true);

                        }
                        else
                        {
                            int RecordId = ObjPatientsData.UploadPatientDocument(0, NewFileName, "Doctor", PatientId, DoctorId);
                            referral_result.Add(SafeValue<string>(RecordId), true);
                            return_result.Add(NewFileName, true);

                        }
                        if (File.Exists(sourceFile))
                        {
                            File.Delete(sourceFile);
                        }
                    }
                }

                bool Result = ObjCommon.Temp_RemoveAllByUserId(DoctorId, clsCommon.GetUniquenumberForAttachments()); // For Remove All Uplaoded Temp Images and Files
                                                                                                                     // return_result.Add(NewFileName, Result);
                if (IsReferralForm)
                {
                    return referral_result;
                }
                else
                {
                    return return_result;
                    //return referral_result;

                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---SendMessageFromPatientHistory", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Set Permission if Directory hasn't permission to Copy/Move time.
        /// </summary>
        /// <param name="savepath"></param>
        public static void SetPermissions(string savepath)
        {
            try
            {
                DirectoryInfo info = new DirectoryInfo(savepath);
                WindowsIdentity self = WindowsIdentity.GetCurrent();
                DirectorySecurity ds = info.GetAccessControl();
                ds.AddAccessRule(new FileSystemAccessRule(self.Name,
                FileSystemRights.FullControl,
                InheritanceFlags.ObjectInherit |
                InheritanceFlags.ContainerInherit,
                PropagationFlags.None,
                AccessControlType.Allow));
                info.SetAccessControl(ds);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---SetPermissions", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static List<SelectListItem> GetColleague(int UserId)
        {
            List<SelectListItem> lst = new List<SelectListItem>();
            DataTable dt = new DataTable();
            try
            {
                dt = new clsColleaguesData().GetColleaguesDetailsForDoctor(UserId, 1, int.MaxValue, null, 11, 2);
                //sb.Append("<select id=\"drpState\" name=\"drpState\" style=\"width:100%;\">");
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        SelectListItem selectitm = new SelectListItem();
                        selectitm.Value = SafeValue<string>(item["ColleagueId"]);
                        selectitm.Text = item["FirstName"] + " " + item["LastName"];
                        lst.Add(selectitm);
                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetColleague", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static string AddPatientImage(PatientProfileImage profileImage)
        {
            try
            {
                byte[] bytes = Convert.FromBase64String(profileImage.Base64URL);
                if (!Directory.Exists(ConfigurationManager.AppSettings.Get("APITempFolder")))
                    Directory.CreateDirectory(ConfigurationManager.AppSettings.Get("APITempFolder"));

                string imgName = $"$" + DateTime.Now.Ticks + "$" + profileImage.PatientId + ".jpg";
                string FullPathImage = ConfigurationManager.AppSettings.Get("APITempFolder");
                using (var imageFile = new FileStream(Path.Combine(FullPathImage, imgName), FileMode.Create))
                {
                    imageFile.Write(bytes, 0, bytes.Length);
                    imageFile.Flush();
                }

                var Byts = File.ReadAllBytes(ConfigurationManager.AppSettings.Get("APITempFolder") + "\\" + imgName);

                string FileName = string.Empty;
                string ImagePath = imgName;
                string thambnailimage = imgName;
                string extension;
                extension = Path.GetExtension(imgName);
                if (extension.ToLower() == ".jpg" || extension.ToLower() == ".jpeg" || extension.ToLower() == ".gif" || extension.ToLower() == ".png" || extension.ToLower() == ".bmp" || extension.ToLower() == ".psd" || extension.ToLower() == ".pspimage" || extension.ToLower() == ".thm" || extension.ToLower() == ".tif")
                {
                    string tempPath = ConfigurationManager.AppSettings["FolderPath"];

                    int Bottom = SafeValue<int>(Math.Floor(profileImage.Bottom));
                    int Right = SafeValue<int>(Math.Floor(profileImage.Right));
                    int Top = SafeValue<int>(Math.Floor(profileImage.Top));
                    int Left = SafeValue<int>(Math.Floor(profileImage.Left));

                    HttpPostedFileBase hpf;
                    var imagecrop = new WebImage(Byts);// tempPath + filename1);
                    var height = imagecrop.Height;
                    var width = imagecrop.Width;
                    imagecrop.Crop(Top, Left, height - Bottom, width - Right);
                    int maxPixels = SafeValue<int>(ConfigurationManager.AppSettings["Thumbimagediamention"]);
                    imagecrop.Resize(maxPixels, maxPixels, true, false);
                    imagecrop.Save(tempPath + @"\" + thambnailimage, null, false);
                    clsPatientsData.UpdatePatientProfileImg(profileImage.PatientId, imgName);
                }
                return ConfigurationManager.AppSettings["FolderPath"] + "/" + imgName;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---AddPatientImage", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int AddPatient(PatientHistoryViewModel add)
        {
            try
            {
                //Commented because we have already converted the gender as 0 and 1.
                //if (add.Gender == "Female")
                //{
                //    add.Gender = "1";
                //}
                //else if (add.Gender == "Male")
                //{
                //    add.Gender = "0";
                //}
                //else
                //{
                //    add.Gender = "1";
                //}
                DateTime? dt_DateOfBirth = null;
                if (!string.IsNullOrEmpty(add.DateOfBirth))
                {
                    dt_DateOfBirth = SafeValue<DateTime>(add.DateOfBirth);
                    //dt_DateOfBirth = clsHelper.ConvertToUTC(SafeValue<DateTime>(dt_DateOfBirth), TimeZoneSystemName);
                }
                if (!string.IsNullOrWhiteSpace(add.Notes))
                {
                    add.Notes = add.Notes.Replace("[^A-Za-z0-9]", "");
                    if (add.Notes.Length > 200)
                    {
                        add.Notes = add.Notes.Substring(0, Math.Min(200, add.Notes.Length));
                    }
                }
                int Gender = (!string.IsNullOrWhiteSpace(add.Gender)) ? SafeValue<int>(add.Gender) : 0;
                return new clsPatientsData().PatientInsertAndUpdate(add.PatientId, add.AssignedPatientId, add.FirstName, "", add.LastName, dt_DateOfBirth, Gender, add.ProfileImage, add.Phone, add.Email,
                                           add.ExactAddress, add.City, add.State, add.ZipCode, add.Country,
                                           add.OwnerId, 0, add.Address2, Guid.NewGuid().ToString("N").Substring(0, 8), add.Notes, (!string.IsNullOrWhiteSpace(add.ReferredBy)) ? Convert.ToInt32(add.ReferredBy) : (int?)null, add.Location, add.FirstExamDate);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---AddPatient", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static object checkPatient(CheckPatients check, int UserId)
        {
            try
            {
                string email = "1", assignedId = "1", strPatientName = string.Empty;
                DataTable dtPatient = new clsPatientsData().CheckAssignedPatientId(check.AssignedPatientId, UserId, check.PatientId);
                if (dtPatient != null && dtPatient.Rows.Count > 0)
                {
                    strPatientName = SafeValue<string>(dtPatient.Rows[0]["FirstName"]).Trim() + " " + SafeValue<string>(dtPatient.Rows[0]["LastName"]).Trim();
                    assignedId = "0";
                }
                var result = new { PatientEmail = email, AssignedPatientId = assignedId, PatientName = strPatientName };
                return result;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---checkPatient", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static ColleaguesMessagesOfDoctorViewModel GetMessageDetials(int MessageId, string TimeZone)
        {
            try
            {
                ColleaguesMessagesOfDoctorViewModel CMD = new ColleaguesMessagesOfDoctorViewModel();
                clsAppointmentData objclsAppointmentData = new clsAppointmentData();
                clsColleaguesData ObjColleaguesData = new clsColleaguesData();
                clsCommon objCommon = new clsCommon();
                DataTable dt = new DataTable();
                string Message = string.Empty;
                DataSet dsMessage = ObjColleaguesData.GetColleagueMessageFormDoctorbyMessageId(MessageId);
                if (dsMessage.Tables.Count > 0)
                    dt = dsMessage.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    string ReceverIds = string.Empty;
                    CMD.lstReceiverDetails = new List<ReceiversDetailsViewModel>();
                    for (int i = 0; i < dsMessage.Tables[0].Rows.Count; i++)
                    {
                        ReceverIds += SafeValue<string>(SafeValue<string>(dsMessage.Tables[0].Rows[i]["reciveruserid"])) + ',';
                    }
                    ReceverIds = ReceverIds.TrimEnd(',');
                    for (int i = 0; i < dsMessage.Tables[0].Rows.Count; i++)
                    {
                        CMD.lstReceiverDetails.Add(new ReceiversDetailsViewModel()
                        {
                            ReceiverEmail = SafeValue<string>(dsMessage.Tables[0].Rows[i]["ReciverEmail"]),
                            ReceiverId = SafeValue<string>(ReceverIds),
                            ReceiverName = SafeValue<string>(dsMessage.Tables[0].Rows[i]["ReciverName"])
                        });
                    }
                    string NewMessage = Uri.UnescapeDataString(SafeValue<string>(dt.Rows[0]["Messages"]));
                    CMD.CreationDate = string.Empty;
                    DateTime dtCreationDate = SafeValue<DateTime>(dt.Rows[0]["CreationDate"]);
                    //-- convert date as per timezone
                    if (dtCreationDate != DateTime.MinValue)
                    {
                        dtCreationDate = clsHelper.ConvertFromUTC(dtCreationDate, TimeZone);
                        CMD.CreationDate = new CommonBLL().ConvertToDateTime(dtCreationDate, (int)DateFormat.GENERAL);
                    }
                    CMD.Message = NewMessage;
                    CMD.SenderName = SafeValue<string>(dt.Rows[0]["SenderName"]);
                    CMD.ReciverName = SafeValue<string>(dt.Rows[0]["ReciverName"]);
                    CMD.SenderEmail = SafeValue<string>(dt.Rows[0]["SenderEmail"]);
                    CMD.ReciverEmail = SafeValue<string>(dt.Rows[0]["ReciverEmail"]);
                    CMD.SenderId = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(dt.Rows[0]["senderuserid"])) ? 0 : dt.Rows[0]["senderuserid"]);
                    CMD.ReciverId = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(dt.Rows[0]["reciveruserid"])) ? 0 : dt.Rows[0]["reciveruserid"]);
                    List<AttachedPatientDetailsViewModel> lst = new List<AttachedPatientDetailsViewModel>();
                    if (!string.IsNullOrEmpty(SafeValue<string>(dt.Rows[0]["AttachedPatientIds"])))
                    {
                        CMD.AttachedPatientsId = SafeValue<string>(dt.Rows[0]["AttachedPatientIds"]);
                        string[] attch = CMD.AttachedPatientsId.Split(',');
                        foreach (var item in attch)
                        {

                            DataTable dtpatient = new DataTable();
                            dtpatient = objclsAppointmentData.GetPateintDetailsByPatientId(SafeValue<long>(item), null);
                            if (dtpatient.Rows.Count > 0)
                            {
                                foreach (DataRow pat in dtpatient.Rows)
                                {
                                    lst.Add(new AttachedPatientDetailsViewModel()
                                    {
                                        AttachedPatientId = SafeValue<int>(pat["PatientId"]),
                                        AttachedPatientFirstName = SafeValue<string>(pat["FirstName"]),
                                        AttachedPatientLastName = SafeValue<string>(pat["LastName"])
                                    });
                                }
                            }
                        }
                    }
                    CMD.LstPatient = lst;
                    CMD.lstImages = new List<MessageAttachmentViewModel>();
                }
                if (dsMessage.Tables.Count > 1)
                {
                    foreach (DataRow item in dsMessage.Tables[1].Rows)
                    {

                        CMD.lstImages.Add(new MessageAttachmentViewModel()
                        {
                            AttachementFullName = SafeValue<string>(item["AttachmentName"]),
                            AttachmentId = SafeValue<int>(item["Id"]),
                            AttachmentName = SafeValue<string>(item["AttachmentName"]).Substring(SafeValue<string>(item["AttachmentName"]).LastIndexOf("$") + 1)
                        });
                    }
                }
                return CMD;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetMessageDetials", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        #region Get Message Details by MessageId
        public static OCRPatientMessagesOfDoctor PatientMessage(int MessageId, string TimeZoneSystemName)
        {
            OCRPatientMessagesOfDoctor objOCRPatientMessagesOfDoctor = new OCRPatientMessagesOfDoctor();
            clsPatientsData ObjPatientsData = new clsPatientsData();
            DataTable dt = new DataTable();
            List<MessageAttachmentsViewModel> lstattachement = new List<MessageAttachmentsViewModel>();
            dt = ObjPatientsData.GetPatientMessageofDoctor(MessageId);
            if (dt.Rows.Count > 0)
            {

                string NewMessage = Uri.UnescapeDataString(Convert.ToString(dt.Rows[0]["Message"]));
                HtmlDocument htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(NewMessage);
                string Message = htmlDoc.DocumentNode.InnerText;
                objOCRPatientMessagesOfDoctor.FromField = dt.Rows[0]["PatientName"].ToString();
                objOCRPatientMessagesOfDoctor.Message = Uri.UnescapeDataString(Convert.ToString(dt.Rows[0]["Message"]));
                objOCRPatientMessagesOfDoctor.CreationDate = dt.Rows[0]["CreationDate"].ToString();
                objOCRPatientMessagesOfDoctor.ToField = dt.Rows[0]["ToField"].ToString();
                objOCRPatientMessagesOfDoctor.PatientEmail = dt.Rows[0]["PatientEmail"].ToString();
                objOCRPatientMessagesOfDoctor.DoctorEmail = dt.Rows[0]["DoctorEmail"].ToString();
                objOCRPatientMessagesOfDoctor.PatientId = Convert.ToInt32(dt.Rows[0]["PatientId"]);
                foreach (DataRow item in dt.Rows)
                {
                    objOCRPatientMessagesOfDoctor.lstattachement = new List<MessageAttachmentsViewModel>();
                    objOCRPatientMessagesOfDoctor.lstattachement.Add(new MessageAttachmentsViewModel()
                    {
                        AttachmentName = item["AttachmentName"].ToString(),
                        Id = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(item["Id"])) ? 0 : item["Id"]),
                        // AttachmentName = item["AttachmentName"].ToString().Substring(item["AttachmentName"].ToString().ToString().LastIndexOf("$") + 1)
                    });
                }
                //-- Timezone changes
                if (!string.IsNullOrWhiteSpace(objOCRPatientMessagesOfDoctor.CreationDate))
                {
                    var dtCreationDate = Convert.ToDateTime(objOCRPatientMessagesOfDoctor.CreationDate);
                    dtCreationDate = clsHelper.ConvertFromUTC(dtCreationDate, TimeZoneSystemName);
                    objOCRPatientMessagesOfDoctor.CreationDate = dtCreationDate.ToString();
                }
            }
            return objOCRPatientMessagesOfDoctor;
        }

        #endregion



        public static List<ReferralMessageDetails> GetReferralDetail(int MessageId, string TimeZone)
        {
            try
            {
                List<ReferralMessageDetails> lstRefferalDetails = new List<ReferralMessageDetails>();
                DataTable dt = new DataTable(); clsCommon objCommon = new clsCommon();
                dt = new clsColleaguesData().GetReferralViewByID(MessageId, "Inbox");
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        ReferralMessageDetails ObjRefferalDetails = new ReferralMessageDetails();
                        string ReferralId = SafeValue<string>(item["ReferralCardId"]);
                        string ReferralCardId = ReferralId;
                        if (!string.IsNullOrEmpty(ReferralId))
                        {
                            ObjRefferalDetails.ReferralCardId = SafeValue<int>(item["ReferralCardId"]);
                        }


                        ObjRefferalDetails.MessageId = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(item["MessageId"])) ? 0 : item["MessageId"]);
                        ObjRefferalDetails.SenderId = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(item["SenderID"])) ? 0 : item["SenderID"]);
                        ObjRefferalDetails.ReceiverID = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(item["ReceiverID"])) ? 0 : item["ReceiverID"]);
                        ObjRefferalDetails.PatientId = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(item["PatientId"])) ? 0 : item["PatientId"]);
                        if (!string.IsNullOrWhiteSpace(SafeValue<string>(item["ProfileImage"])))
                        {
                            ObjRefferalDetails.ProfileImage = SafeValue<string>(System.Configuration.ConfigurationManager.AppSettings.Get("ImageBank")) + SafeValue<string>(item["ProfileImage"]);
                        }
                        else
                        {
                            if (SafeValue<string>(item["Gender"]) == "Female")
                            {
                                ObjRefferalDetails.ProfileImage = SafeValue<string>(System.Configuration.ConfigurationManager.AppSettings.Get("DefaultDoctorFemaleImage"));
                            }
                            else
                            {
                                ObjRefferalDetails.ProfileImage = SafeValue<string>(System.Configuration.ConfigurationManager.AppSettings.Get("DefaultDoctorImage"));
                            }
                        }



                        string CreationDate = string.Empty;
                        if (!string.IsNullOrEmpty(SafeValue<string>(item["CreationDate"])))
                            CreationDate = SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(item["CreationDate"]), TimeZone));
                        string strDateOfBirth = string.Empty;

                        ObjRefferalDetails.FullName = SafeValue<string>(item["FullName"]);
                        ObjRefferalDetails.Gender = SafeValue<string>(item["Gender"]);
                        ObjRefferalDetails.DateOfBirth = SafeValue<string>(item["DateOfBirth"]);
                        ObjRefferalDetails.SenderName = SafeValue<string>(item["SenderName"]);
                        ObjRefferalDetails.ReceiverName = SafeValue<string>(item["ReceiverName"]);
                        ObjRefferalDetails.FullAddress = SafeValue<string>(item["FullAddress"]);
                        ObjRefferalDetails.EmailAddress = SafeValue<string>(item["EmailAddress"]);
                        ObjRefferalDetails.Location = SafeValue<string>(item["ReferralLocation"]);
                        ObjRefferalDetails.RegardOption = SafeValue<string>(item["RegardOption"]);
                        ObjRefferalDetails.RequestingOption = SafeValue<string>(item["RequestingOption"]);
                        ObjRefferalDetails.OtherComments = SafeValue<string>(item["OtherComments"]);
                        ObjRefferalDetails.RequestComments = SafeValue<string>(item["RequestComments"]);
                        ObjRefferalDetails.Comments = SafeValue<string>(item["Comments"]);
                        ObjRefferalDetails.CreationDate = CreationDate;


                        if (!string.IsNullOrEmpty(ReferralId))
                        {



                            DataTable dtDocument = new DataTable();
                            // Datatable For Document
                            dtDocument = new clsColleaguesData().GetRefferalDocumentAttachemntsById(SafeValue<int>(item["ReferralCardId"]), ObjRefferalDetails.PatientId);

                            List<MessageAttachment> lstDocument = new List<MessageAttachment>();
                            foreach (DataRow Document in dtDocument.Rows)
                            {
                                MessageAttachment ObjMessageAttachment = new MessageAttachment();
                                ObjMessageAttachment.AttachmentId = int.Parse(!string.IsNullOrWhiteSpace(SafeValue<string>(Document["DocumentId"])) ? SafeValue<string>(Document["DocumentId"]) : "0");
                                ObjMessageAttachment.AttachmentName = SafeValue<string>(Document["DocumentName"]);
                                lstDocument.Add(ObjMessageAttachment);
                            }
                            ObjRefferalDetails.lstDocuments = lstDocument;

                            // Datatable For Image
                            DataTable dtImage = new DataTable();
                            dtImage = new clsColleaguesData().GetRefferalImageById(SafeValue<int>(item["ReferralCardId"]), ObjRefferalDetails.PatientId);
                            List<MessageAttachment> lstImage = new List<MessageAttachment>();
                            foreach (DataRow Image in dtImage.Rows)
                            {
                                MessageAttachment ObjMessageAttachment = new MessageAttachment();
                                ObjMessageAttachment.AttachmentId = int.Parse(!string.IsNullOrWhiteSpace(SafeValue<string>(Image["ImageId"])) ? SafeValue<string>(Image["ImageId"]) : "0");
                                ObjMessageAttachment.AttachmentName = SafeValue<string>(Image["Name"]);
                                ObjMessageAttachment.AttachementFullName = System.Configuration.ConfigurationManager.AppSettings.Get("ImageBank") + SafeValue<string>(Image["Name"]);
                                lstImage.Add(ObjMessageAttachment);
                            }
                            ObjRefferalDetails.lstImages = lstImage;


                            //ToothType
                            DataTable dtToothTest = new clsColleaguesData().GetReferralToothDetailsByReferralCardId(ObjRefferalDetails.ReferralCardId);
                            List<ToothList> lstToothList = new List<ToothList>();
                            ObjRefferalDetails.lstSelectedTeeths = new List<Int32>();
                            ObjRefferalDetails.lstSelectedPermanentTeeths = new List<Int32>();
                            ObjRefferalDetails.lstSelectedPrimaryTeeths = new List<Int32>();
                            foreach (DataRow ToothTest in dtToothTest.Rows)
                            {
                                ToothList ObjToothList = new ToothList();
                                ObjToothList.ID = int.Parse(!string.IsNullOrWhiteSpace(SafeValue<string>(ToothTest["ReferralDetailId"])) ? SafeValue<string>(ToothTest["ReferralDetailId"]) : "0");
                                ObjToothList.ToothType = SafeValue<int>(SafeValue<string>(ToothTest["ToothType"]));
                                lstToothList.Add(ObjToothList);
                                if (SafeValue<int>(ToothTest["ToothType"]) <= 32)
                                {
                                    ObjRefferalDetails.lstSelectedPermanentTeeths.Add(SafeValue<int>(SafeValue<string>(ToothTest["ToothType"])));
                                }
                                else
                                {
                                    ObjRefferalDetails.lstSelectedPrimaryTeeths.Add(SafeValue<int>(SafeValue<string>(ToothTest["ToothType"])));
                                }
                                ObjRefferalDetails.lstSelectedTeeths.Add(SafeValue<int>(SafeValue<string>(ToothTest["ToothType"])));
                            }
                            ObjRefferalDetails.lstToothList = lstToothList;
                        }

                        lstRefferalDetails.Add(ObjRefferalDetails);
                    }
                }
                return lstRefferalDetails;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetReferralDetail", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static ColleaguesMessagesOfDoctorViewModel GetNormalMessage(int MessageId, string TimeZone)
        {
            try
            {
                ColleaguesMessagesOfDoctorViewModel CMD = new ColleaguesMessagesOfDoctorViewModel(); clsCommon objCommon = new clsCommon();
                clsAppointmentData objclsAppointmentData = new clsAppointmentData();
                clsColleaguesData ObjColleaguesData = new clsColleaguesData();
                DataTable dt = new DataTable();
                string Message = string.Empty;
                DataSet dsMessage = ObjColleaguesData.GetColleagueMessageFormDoctorbyMessageId(MessageId);
                if (dsMessage.Tables.Count > 0)
                    dt = dsMessage.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    string ReceverIds = string.Empty;
                    CMD.lstReceiverDetails = new List<ReceiversDetailsViewModel>();
                    for (int i = 0; i < dsMessage.Tables[0].Rows.Count; i++)
                    {
                        ReceverIds += SafeValue<string>(SafeValue<string>(dsMessage.Tables[0].Rows[i]["reciveruserid"])) + ',';
                    }
                    ReceverIds = ReceverIds.TrimEnd(',');
                    for (int i = 0; i < dsMessage.Tables[0].Rows.Count; i++)
                    {
                        CMD.lstReceiverDetails.Add(new ReceiversDetailsViewModel()
                        {
                            ReceiverEmail = SafeValue<string>(dsMessage.Tables[0].Rows[i]["ReciverEmail"]),
                            ReceiverId = SafeValue<string>(ReceverIds),
                            ReceiverName = SafeValue<string>(dsMessage.Tables[0].Rows[i]["ReciverName"])
                        });
                    }
                    string NewMessage = Uri.UnescapeDataString(SafeValue<string>(dt.Rows[0]["Messages"]));
                    CMD.CreationDate = SafeValue<string>(dt.Rows[0]["CreationDate"]);
                    //-- convert date as per timezone
                    if (!string.IsNullOrEmpty(CMD.CreationDate))
                    {
                        DateTime dtCreationDate = SafeValue<DateTime>(CMD.CreationDate);
                        dtCreationDate = clsHelper.ConvertFromUTC(dtCreationDate, TimeZone);
                        CMD.CreationDate = SafeValue<string>(dtCreationDate);
                    }
                    CMD.Message = NewMessage;
                    CMD.SenderName = SafeValue<string>(dt.Rows[0]["SenderName"]);
                    CMD.ReciverName = SafeValue<string>(dt.Rows[0]["ReciverName"]);
                    CMD.SenderEmail = SafeValue<string>(dt.Rows[0]["SenderEmail"]);
                    CMD.ReciverEmail = SafeValue<string>(dt.Rows[0]["ReciverEmail"]);
                    CMD.SenderId = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(dt.Rows[0]["senderuserid"])) ? 0 : dt.Rows[0]["senderuserid"]);
                    CMD.ReciverId = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(dt.Rows[0]["reciveruserid"])) ? 0 : dt.Rows[0]["reciveruserid"]);
                    List<AttachedPatientDetailsViewModel> lst = new List<AttachedPatientDetailsViewModel>();
                    if (!string.IsNullOrEmpty(SafeValue<string>(dt.Rows[0]["AttachedPatientIds"])))
                    {
                        CMD.AttachedPatientsId = SafeValue<string>(dt.Rows[0]["AttachedPatientIds"]);
                        string[] attch = CMD.AttachedPatientsId.Split(',');
                        foreach (var item in attch)
                        {

                            DataTable dtpatient = new DataTable();
                            dtpatient = objclsAppointmentData.GetPateintDetailsByPatientId(SafeValue<long>(item), null);
                            if (dtpatient.Rows.Count > 0)
                            {
                                foreach (DataRow pat in dtpatient.Rows)
                                {
                                    lst.Add(new AttachedPatientDetailsViewModel()
                                    {
                                        AttachedPatientId = SafeValue<int>(pat["PatientId"]),
                                        AttachedPatientFirstName = SafeValue<string>(pat["FirstName"]),
                                        AttachedPatientLastName = SafeValue<string>(pat["LastName"])
                                    });
                                }
                            }
                        }
                    }
                    CMD.LstPatient = lst;
                    CMD.lstImages = new List<MessageAttachmentViewModel>();
                }

                if (dsMessage.Tables.Count > 1)
                {
                    foreach (DataRow item in dsMessage.Tables[1].Rows)
                    {

                        CMD.lstImages.Add(new MessageAttachmentViewModel()
                        {
                            AttachementFullName = SafeValue<string>(item["AttachmentName"]),
                            AttachmentId = SafeValue<int>(item["Id"]),
                            AttachmentName = SafeValue<string>(item["AttachmentName"]).Substring(SafeValue<string>(item["AttachmentName"]).LastIndexOf("$") + 1)
                        });
                    }
                }
                return CMD;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetNormalMessage", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static PatientMessagesOfDoctorViewModel GetPatientMessage(int MessageId, string TimeZone)
        {
            try
            {
                PatientMessagesOfDoctorViewModel PMD = new PatientMessagesOfDoctorViewModel();
                clsPatientsData ObjPatientsData = new clsPatientsData();
                DataTable dt = new DataTable();
                List<MessageAttachmentViewModel> lstattachement = new List<MessageAttachmentViewModel>();
                dt = ObjPatientsData.GetPatientMessageofDoctor(MessageId);
                if (dt.Rows.Count > 0)
                {

                    string NewMessage = Uri.UnescapeDataString(SafeValue<string>(dt.Rows[0]["Message"]));
                    HtmlDocument htmlDoc = new HtmlDocument();
                    htmlDoc.LoadHtml(NewMessage);
                    string Message = htmlDoc.DocumentNode.InnerText;
                    PMD.FromField = SafeValue<string>(dt.Rows[0]["PatientName"]);
                    PMD.Message = Uri.UnescapeDataString(SafeValue<string>(dt.Rows[0]["Message"]));
                    PMD.CreationDate = SafeValue<string>(dt.Rows[0]["CreationDate"]);
                    PMD.ToField = SafeValue<string>(dt.Rows[0]["ToField"]);
                    PMD.PatientEmail = SafeValue<string>(dt.Rows[0]["PatientEmail"]);
                    PMD.DoctorEmail = SafeValue<string>(dt.Rows[0]["DoctorEmail"]);
                    PMD.PatientId = SafeValue<int>(dt.Rows[0]["PatientId"]);
                    foreach (DataRow item in dt.Rows)
                    {
                        PMD.lstattachement = new List<MessageAttachmentViewModel>();
                        PMD.lstattachement.Add(new MessageAttachmentViewModel()
                        {
                            AttachementFullName = SafeValue<string>(item["AttachmentName"]),
                            AttachmentId = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(item["Id"])) ? 0 : item["Id"]),
                            AttachmentName = SafeValue<string>(item["AttachmentName"]).Substring(SafeValue<string>(item["AttachmentName"]).LastIndexOf("$") + 1)
                        });
                    }
                    //-- Timezone changes
                    if (!string.IsNullOrWhiteSpace(PMD.CreationDate))
                    {
                        var dtCreationDate = SafeValue<DateTime>(PMD.CreationDate);
                        dtCreationDate = clsHelper.ConvertFromUTC(dtCreationDate, TimeZone);
                        PMD.CreationDate = SafeValue<string>(dtCreationDate);
                    }
                }
                return PMD;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetPatientMessage", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static List<FamilyReleationHistoryViewModel> GetFamilyHistoryDetails(int PatientId)
        {
            List<FamilyReleationHistoryViewModel> lstfamilyrelation = new List<FamilyReleationHistoryViewModel>();
            try
            {
                DataTable dt = new DataTable();
                clsPatientsData objPatientsData = new clsPatientsData();
                dt = objPatientsData.GetFamilyHistoryDetails(PatientId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    lstfamilyrelation = (from p in dt.AsEnumerable()
                                         select new FamilyReleationHistoryViewModel
                                         {
                                             ReletaiveId = SafeValue<int>(p["ReleativeId"]),
                                             PatientId = SafeValue<int>(p["PatientId"]),
                                             PatientName = SafeValue<string>(p["PatientName"]),
                                             ReleationName = SafeValue<string>(p["ReleationName"]),
                                             ToPatientId = SafeValue<int>(p["ToPatientId"])
                                         }).ToList();
                }

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetFamilyHistoryDetails", Ex.Message, Ex.StackTrace);
                throw;
            }

            return lstfamilyrelation;

        }

        public static bool DeleteRelation(int FromPatientId, int ToPatientId)
        {

            try
            {
                clsPatientsData objPatientsData = new clsPatientsData();
                int status = objPatientsData.DeleteRelation(FromPatientId, ToPatientId) ? (int)FamilyStatus.IsSuccess : (int)FamilyStatus.IsFailed;
                if (status > 0)
                {
                    status = 0;
                    status = objPatientsData.DeleteRelation(ToPatientId, FromPatientId) ? (int)FamilyStatus.IsSuccess : (int)FamilyStatus.IsFailed;

                }
                return status > 0;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---DeleteRelation", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static PatientInsuranceDetails GetPatientInsuranceDetails(int intPatientId)
        {
            try
            {
                PatientInsuranceDetails objPatientInsuranceDetails = new PatientInsuranceDetails();
                PatientSubscriber objPatientSubscriber = new PatientSubscriber();
                InsuranceCompany objInsuranceCompany = new InsuranceCompany();
                InsuranceInformation objInsuranceInformation = new InsuranceInformation();
                DeductiblesMaximums objDeductiblesMaximums = new DeductiblesMaximums();

                DataTable dt = new DataTable();
                dt = clsPatientsData.GetPatientInsuranceDetails(intPatientId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objPatientInsuranceDetails.PatientId = intPatientId;
                    // Change
                    objPatientInsuranceDetails.Office = SafeValue<string>(dt.Rows[0]["OfficeName"]);
                    //objPatientInsuranceDetails.Date = SafeValue<DateTime>(dt.Rows[0]["date"]);

                    // Patient and Subscriber
                    objPatientSubscriber.PatientFirstName = SafeValue<string>(dt.Rows[0]["MemberFirstName"]);
                    objPatientSubscriber.PatientMiddleName = SafeValue<string>(dt.Rows[0]["MemberMiddleName"]);
                    objPatientSubscriber.PatientLastName = SafeValue<string>(dt.Rows[0]["MemberLastName"]);

                    // Change
                    //objPatientInsuranceDetails.objPatientSubscriber.PatientSuffix = SafeValue<string>(dt.Rows[0]["Suffix"]);

                    objPatientSubscriber.PatientDateOfBirth = SafeValue<DateTime>(dt.Rows[0]["MemberDOB"]);


                    objPatientSubscriber.SubscriberFirstName = SafeValue<string>(dt.Rows[0]["SubscriberFirstName"]);
                    objPatientSubscriber.SubscriberLastName = SafeValue<string>(dt.Rows[0]["SubscriberLastName"]);
                    //objPatientInsuranceDetails.objPatientSubscriber.SubscriberSuffix = SafeValue<string>(dt.Rows[0]["Suffix"]);
                    objPatientSubscriber.SubscriberDateOfBirth = SafeValue<DateTime>(dt.Rows[0]["SubscriberDOB"]);

                    // Insurance Company
                    objInsuranceCompany.InsuranceCompanyName = SafeValue<string>(dt.Rows[0]["CarrierName"]);
                    objInsuranceCompany.City = SafeValue<string>(dt.Rows[0]["CarrierCity"]);
                    objInsuranceCompany.ClaimsMailingAddress = SafeValue<string>(dt.Rows[0]["CarrierStreet1"]);
                    objInsuranceCompany.State = SafeValue<string>(dt.Rows[0]["CarrierState"]);
                    objInsuranceCompany.Zip = SafeValue<string>(dt.Rows[0]["CarrierZipcode"]);
                    objInsuranceCompany.PhoneNumber = SafeValue<string>(dt.Rows[0]["CarrierPhone"]);
                    objInsuranceCompany.Address2 = SafeValue<string>(dt.Rows[0]["CarrierStreet2"]);
                    //Insurance Information


                    objInsuranceInformation.PayerID = SafeValue<string>(dt.Rows[0]["CarrierPayOrId"]);
                    // discuss
                    //  objPatientInsuranceDetails.objInsuranceInformation.EffectiveDate= SafeValue<DateTime>(dt.Rows[0]["EffectiveDate"]);

                    objInsuranceInformation.GroupNumber = SafeValue<string>(dt.Rows[0]["CarrierGroupNumber"]);

                    // discuss
                    // objPatientInsuranceDetails.objInsuranceInformation.TerminationDate= SafeValue<DateTime>(dt.Rows[0]["TerminationDate"]);

                    objInsuranceInformation.PlanName = SafeValue<string>(dt.Rows[0]["CarrierGroupName"]);

                    // objPatientInsuranceDetails.objInsuranceInformation.FeeSchedule = SafeValue<string>(dt.Rows[0]["FeeSchedule"]);

                    // Deductibles and Maximums
                    // discuss
                    //objPatientInsuranceDetails.objDeductiblesMaximums.BenefitPeriod = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);
                    objDeductiblesMaximums.BenefitYearStarts = SafeValue<string>(dt.Rows[0]["CarrierRenewalMonth"]);
                    objDeductiblesMaximums.YearlyMaximum = SafeValue<string>(dt.Rows[0]["CarrierYearlyMax"]);
                    objDeductiblesMaximums.RemainingBenefits = SafeValue<string>(dt.Rows[0]["MemberBenefitsUsed"]);

                    objDeductiblesMaximums.IndividualDeductible = SafeValue<string>(dt.Rows[0]["CarrierDeductible"]);
                    objDeductiblesMaximums.IndividualDeductibleMet = SafeValue<string>(dt.Rows[0]["MemberStandard"]);
                    objDeductiblesMaximums.FamilyDeductible = SafeValue<string>(dt.Rows[0]["CarrierDeductible"]);
                    objDeductiblesMaximums.FamilyDeductibleMet = SafeValue<string>(dt.Rows[0]["RecordStandard"]);

                    //objPatientInsuranceDetails.objDeductiblesMaximums.DeductibleAppliestoPreventive = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);
                    //objPatientInsuranceDetails.objDeductiblesMaximums.Basic = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);
                    //objPatientInsuranceDetails.objDeductiblesMaximums.Major = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);


                    //// Coverage
                    //objPatientInsuranceDetails.objCoverage.Diagnostic = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);
                    //objPatientInsuranceDetails.objCoverage.Endo = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);
                    //objPatientInsuranceDetails.objCoverage.Preventive = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);
                    //objPatientInsuranceDetails.objCoverage.Perio = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);
                    //objPatientInsuranceDetails.objCoverage.Basic = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);
                    //objPatientInsuranceDetails.objCoverage.OralSurgery = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);
                    //objPatientInsuranceDetails.objCoverage.Major = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);
                    //objPatientInsuranceDetails.objCoverage.Implant = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);
                    //objPatientInsuranceDetails.objCoverage.Ortho = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);
                    //objPatientInsuranceDetails.objCoverage.OrthoMaximum = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);
                    //objPatientInsuranceDetails.objCoverage.OrthoBillingRequirements = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);

                    //// Frequency

                    //objPatientInsuranceDetails.objFrequency.Exam = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);
                    //objPatientInsuranceDetails.objFrequency.FullMouthXRaysPano = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);
                    //objPatientInsuranceDetails.objFrequency.SharesBenefits = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);
                    //objPatientInsuranceDetails.objFrequency.Prophy = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);
                    //objPatientInsuranceDetails.objFrequency.Fluoride = SafeValue<string>(dt.Rows[0]["BenefitPeriod"]);
                    //objPatientInsuranceDetails.objFrequency.AgeLimitlessthan = SafeValue<int>(dt.Rows[0]["BenefitPeriod"]);
                    objPatientInsuranceDetails.objPatientSubscriber = objPatientSubscriber;
                    objPatientInsuranceDetails.objInsuranceCompany = objInsuranceCompany;
                    objPatientInsuranceDetails.objInsuranceInformation = objInsuranceInformation;
                    objPatientInsuranceDetails.objDeductiblesMaximums = objDeductiblesMaximums;
                }

                return objPatientInsuranceDetails;

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetPatientInsuranceDetails", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        public static PatientInsuranceViewModel GetPatientInsuranceDataDetails(int intPatientId)
        {
            try
            {
                PatientInsuranceViewModel objPatientInsuranceViewModel = new PatientInsuranceViewModel();
                DataTable dt = new DataTable();
                dt = clsPatientsData.GetPatientInsuranceDataDetails(intPatientId);
                JavaScriptSerializer objJavaScriptSerializer = new JavaScriptSerializer();
                objPatientInsuranceViewModel = (PatientInsuranceViewModel)objJavaScriptSerializer.Deserialize(SafeValue<string>(dt.Rows[0]["JSON"]), typeof(PatientInsuranceViewModel));
                objPatientInsuranceViewModel.PatientName = SafeValue<string>(dt.Rows[0]["PatientName"]);
                objPatientInsuranceViewModel.PatientId = SafeValue<int>(dt.Rows[0]["PatientId"]);
                return objPatientInsuranceViewModel;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientBLL---GetPatientInsuranceDataDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
