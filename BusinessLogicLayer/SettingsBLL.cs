﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using BO.Models;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using static DataAccessLayer.Common.clsCommon;
using DataAccessLayer.Appointment;

namespace BusinessLogicLayer
{
    public class SettingsBLL
    {
        static clsCommon ObjCommon = new clsCommon();
        static clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        static TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        //static cls

        public static AccountSettings GetAccountDetails(int UserId)
        {
            try
            {
                AccountSettings Obj = new AccountSettings();
                DataSet ds = new DataSet();
                ds = ObjColleaguesData.GetDoctorDetailsById(UserId);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        Obj.PrimaryEmail = SafeValue<string>(ds.Tables[0].Rows[0]["Username"]);
                        Obj.SecondaryEmail = SafeValue<string>(ds.Tables[0].Rows[0]["SecondaryEmail"]);
                        Obj.StayLogin = SafeValue<int>(ds.Tables[0].Rows[0]["StayloggedMins"]);
                        Obj.Username = SafeValue<string>(ds.Tables[0].Rows[0]["LoginUserName"]);
                        Obj.LocationId = SafeValue<int>(ds.Tables[0].Rows[0]["LocationId"]);
                        // DentrixProviderId = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["DentrixProviderId"]), string.Empty);
                    }
                }
                Obj.StayLoginList = GetStayloggedMins();
                DataTable dt = new DataTable();
                dt = ObjColleaguesData.GetDoctorLocaitonById(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    List<SelectListItem> lst = new List<SelectListItem>();
                    foreach (DataRow item in dt.Rows)
                    {
                        lst.Add(new SelectListItem()
                        {
                            Text = SafeValue<string>(item["Location"]),
                            Value = SafeValue<string>(item["AddressInfoId"])
                        });
                    }
                    Obj.LocationList = lst;
                }
                return Obj;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SettingsBLL--GetAccountDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Get Stay Logged-in Time static value.
        /// </summary>
        /// <returns></returns>
        public static List<SelectListItem> GetStayloggedMins()
        {
            List<SelectListItem> _StayloggedMins = new List<SelectListItem>();
            _StayloggedMins.Add(new SelectListItem { Text = "1 Hour", Value = "60" });
            _StayloggedMins.Add(new SelectListItem { Text = "2 Hour", Value = "120" });
            _StayloggedMins.Add(new SelectListItem { Text = "3 Hour", Value = "180" });
            _StayloggedMins.Add(new SelectListItem { Text = "4 Hour", Value = "240" });
            _StayloggedMins.Add(new SelectListItem { Text = "5 Hour", Value = "300" });
            _StayloggedMins.Add(new SelectListItem { Text = "6 Hour", Value = "360" });
            _StayloggedMins.Add(new SelectListItem { Text = "7 Hour", Value = "420" });
            _StayloggedMins.Add(new SelectListItem { Text = "8 Hour", Value = "480" });
            _StayloggedMins.Add(new SelectListItem { Text = "1 Day", Value = "1440" });
            _StayloggedMins.Add(new SelectListItem { Text = "1 Week", Value = "10080" });
            return _StayloggedMins;
        }

        /// <summary>
        /// Update Account settings details.
        /// </summary>
        /// <param name="accountsettings"></param>
        /// <returns></returns>
        public static string SaveAccountSettings(SaveAccountSetting accountsettings, int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                string obj = string.Empty;

                #region Check Username
                DataTable dttbl = new DataTable();
                if (accountsettings.OldUsername.ToLower() != accountsettings.Username.ToLower())
                {
                    dttbl = ObjColleaguesData.CheckEmailInSystem(accountsettings.Username);
                    if (dttbl != null && dttbl.Rows.Count > 0)
                    {
                        obj = "User Name already exists in system.";
                        return obj;
                    }
                    else
                    {
                        obj = (ObjColleaguesData.UpdateUserName(UserId, accountsettings.Username)) ? "Security settings updated successfully." : "Failed to update User Name.";
                    }
                }
                #endregion

                #region Check and Update Primary Email
                if (accountsettings.PrimaryEmail.ToLower() != accountsettings.OldPrimaryEmail)
                {
                    dt = new DataTable();
                    dt = ObjColleaguesData.CheckEmailInSystem(accountsettings.PrimaryEmail);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        obj = "This primary email address is already in use.";
                        return obj;
                    }
                    else
                    {
                        obj = (ObjColleaguesData.UpdatePrimaryEmail(UserId, accountsettings.PrimaryEmail)) ? "Security settings updated successfully." : "Failed to update primary email";
                    }
                }
                #endregion

                #region Update Secondary Email
                obj = (ObjColleaguesData.UpdateSecondaryEmail(UserId, accountsettings.SecondaryEmail)) ? "Security settings updated successfully." : "Failed to update secondary email";
                #endregion

                #region Update Password
                if (!string.IsNullOrWhiteSpace(accountsettings.NewPassword))
                {
                    obj = (ObjColleaguesData.UpdatePasswordOfDoctor(UserId, ObjTripleDESCryptoHelper.encryptText(accountsettings.NewPassword))) ? "Security settings updated successfully." : "Failed to update password";
                }
                #endregion

                #region Update Staylogged time
                //obj = (ObjColleaguesData.UpdateStayloggedMinsOfDoctor(UserId, Convert.ToString(accountsettings.StayLogin))) ? "Account settings updated successfully." : "Failed to update loggedintime";
                #endregion

                #region Update Location
                //if(accountsettings.OldLocationId != accountsettings.LocationId)
                //{
                //    obj = (ObjColleaguesData.UpdateLocationIdOfDoctor(UserId, accountsettings.LocationId)) ? "Account settings updated successfully." : "Failed to update Location";
                //}
                #endregion


                return obj;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SettingsBLL--SaveAccountSettings", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Integration list for Integration settings page.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static List<Integration> GetIntegration(int UserId)
        {
            try
            {
                List<Integration> IntegrationList = new List<Integration>();
                DataTable Dt = clsColleaguesData.GetIntegrationMasterDetails();
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    foreach (DataRow item in Dt.Rows)
                    {
                        Integration integrtion = new Integration();
                        integrtion.IntegrationId = SafeValue<int>(item["Id"]);
                        integrtion.IntegrationName = SafeValue<string>(item["Name"]);
                        DataTable DtIntegrat = ObjColleaguesData.GetDentrixConnectorValuesofUser(UserId, integrtion.IntegrationId);
                        if (DtIntegrat != null && DtIntegrat.Rows.Count > 0)
                        {
                            List<CompanyIntegrationDetail> lst = new List<CompanyIntegrationDetail>();
                            foreach (DataRow items in DtIntegrat.Rows)
                            {
                                CompanyIntegrationDetail Obj = new CompanyIntegrationDetail();
                                Obj.IntegrationId = integrtion.IntegrationId;
                                Obj.ConnectorId = SafeValue<int>(items["DentrixConnectorId"]);
                                Obj.ConnectorKey = SafeValue<string>(items["AccountKey"]);
                                Obj.LocationId = SafeValue<int>(items["LocationId"]);
                                Obj.LocationList = GetDentistLocationList(UserId);
                                Obj.IsDisable = SafeValue<bool>(items["IsDeleted"]);
                                lst.Add(Obj);
                            }
                            integrtion.ConnectorList = lst;
                        }
                        IntegrationList.Add(integrtion);
                    }
                }
                IntegrationList.ForEach(x => x.LocationList = GetDentistLocationList(UserId));
                return IntegrationList;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SettingsBLL--GetIntegration", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Dentist Location List
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static List<SelectListItem> GetDentistLocationList(int UserId)
        {
            try
            {
                List<SelectListItem> List = new List<SelectListItem>();                
                DataTable Dt = ObjColleaguesData.GetDoctorLocaitonById(UserId);
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    foreach (DataRow item in Dt.Rows)
                    {
                        List.Add(new SelectListItem()
                        {
                            Text = SafeValue<string>(item["Location"]),
                            Value = SafeValue<string>(item["AddressInfoId"])
                        });
                    }
                }              
                return List;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SettingsBLL--GetDentistLocationList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Save Integration Settings
        /// </summary>
        /// <param name="Save"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static bool SaveIntegrationRecord(SaveIntegration Save, int UserId)
        {
            try
            {
                foreach (var item in Save.Integration)
                {
                    string[] Splited = item.Split('_');
                    int ConnectorId = ObjColleaguesData.InsertDentrixConnector(UserId, SafeValue<string>(Splited[1]), SafeValue<string>(Splited[2]), SafeValue<int>(Splited[0]));
                }
                //if (!string.IsNullOrWhiteSpace(dentrixProviderId))
                //{
                //    ObjColleaguesData.UpdateDentrixProvideId(UserId, dentrixProviderId);
                //}
                return true;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SettingsBLL--SaveIntegrationRecord", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get UnSync Locations for Integrations
        /// </summary>
        /// <param name="IntegrationId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static List<SelectListItem> GetUnSyncLocations(int IntegrationId, int UserId)
        {
            try
            {
                List<SelectListItem> DentrixLocations = new List<SelectListItem>();
                DataTable dt = new DataTable();
                dt = ObjColleaguesData.GetDentrixConnectorValuesofUser(UserId, IntegrationId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        DentrixLocations.Add(new SelectListItem
                        {
                            Text = SafeValue<string>(item["Location"]),
                            Value = SafeValue<string>(item["LocationId"]),
                        });
                    }
                }
                List<SelectListItem> AllLocation = GetDentistLocationList(UserId);
                AllLocation = AllLocation.Where(p => !DentrixLocations.Any(p2 => p2.Text == p.Text)).ToList();
                return AllLocation;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SettingsBLL--GetUnSyncLocations", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Team Member List
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public static List<TeamMember> GetTeamMemberList(FilterTeamMember Obj)
        {
            try
            {
                List<TeamMember> lst = new List<TeamMember>();
                DataTable dt = new DataTable();
                dt = clsColleaguesData.GetTeamMemberList(Obj);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lst.Add(new TeamMember()
                        {
                            FirstName = SafeValue<string>(item["FirstName"]),
                            LastName = SafeValue<string>(item["LastName"]),
                            TeamMemberId = SafeValue<int>(item["UserId"]),
                            Email = SafeValue<string>(item["Username"]),
                            Location = SafeValue<string>(item["Location"]),
                            Specialty = SafeValue<string>(item["Speciality"]),
                            ProviderId = SafeValue<string>(item["DentrixProviderId"]),
                            FullName = SafeValue<string>(item["LastName"]) + " " +SafeValue<string>(item["FirstName"])
                        });
                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SettingsBLL--GetTeamMemberList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Get Team Member Details of Doctor
        /// </summary>
        /// <param name="TeamMemberId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static TeamMember GetTeamMemberDetails(int TeamMemberId, int userId)
        {
            try
            {
                TeamMember Obj = new TeamMember();
                DataTable Dt = clsColleaguesData.GetUserFullDetailsById(TeamMemberId);
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    Obj.FirstName = SafeValue<string>(Dt.Rows[0]["FirstName"]);
                    Obj.LastName = SafeValue<string>(Dt.Rows[0]["LastName"]);
                    Obj.Email = SafeValue<string>(Dt.Rows[0]["Username"]);
                    Obj.Location = SafeValue<string>(Dt.Rows[0]["LocationId"]);
                    Obj.Specialty = SafeValue<string>(Dt.Rows[0]["Speciality"]);
                    Obj.TeamMemberId = SafeValue<int>(Dt.Rows[0]["UserId"]);
                    Obj.ProviderId = SafeValue<string>(Dt.Rows[0]["DentrixProviderId"]);
                    Obj.provtype = SafeValue<int>(Dt.Rows[0]["provtype"]) == 2 ? true : false;
                }
                Obj.Locationlist = GetDentistLocationList(userId);
                Obj.Specialtylist = DentistBLL.GetSpeacilities();
                return Obj;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SettingsBLL--GetTeamMemberDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Add/Edit Team Member Details of Doctor
        /// </summary>
        /// <param name="_team"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static bool AddEditTeamMember(TeamMember _team, int UserId)
        {
            try
            {
                var doctorDetails = new clsAppointmentData().GetDoctorDetails(UserId);
                int ParentId = 0;
                if (doctorDetails.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(SafeValue<string>(doctorDetails.Rows[0]["ParentUserId"])))
                    {
                        ParentId = SafeValue<int>(doctorDetails.Rows[0]["ParentUserId"]);
                        if (ParentId == 0)
                        {
                            ParentId = UserId;
                        }
                    }
                }
                string Password = string.Empty;
                if (_team.TeamMemberId == 0)
                {
                    Password = new clsCommon().CreateRandomPassword(8);
                    Password = new TripleDESCryptoHelper().encryptText(Password);
                }
                bool Result = ObjColleaguesData.TeamMemberInsertAndUpdate(_team.TeamMemberId, ParentId, _team.Email, _team.FirstName, _team.LastName, null, null, null, null, null, "US", null, _team.Specialty, Password, _team.ProviderId, SafeValue<int>(_team.Location),_team.provtype);
                if (Result)
                {
                    if (_team.TeamMemberId == 0)
                    {
                        string CompanyWebsite = string.Empty;
                        DataTable dtCheck = ObjColleaguesData.CheckEmailInSystem(_team.Email);
                        if (dtCheck.Rows.Count > 0)
                        {
                            DataSet ds = new clsCompany().ChechActiveCompany();
                            if (ds != null && ds.Tables[0].Rows.Count > 0)
                            {
                                CompanyWebsite = SafeValue<string>(ds.Tables[0].Rows[0]["CompanyWebsite"]);
                            }
                            string EncPassword = SafeValue<string>(dtCheck.Rows[0]["Password"]);
                            new clsTemplate().NewUserEmailFormat(_team.Email, CompanyWebsite + "/User/Index?UserName=" + _team.Email + "&Password=" + ObjTripleDESCryptoHelper.decryptText(EncPassword), ObjTripleDESCryptoHelper.decryptText(EncPassword), CompanyWebsite);
                            new clsTemplate().TemplateForTeamMember(UserId, _team.FirstName + " " + _team.LastName, _team.Email, ObjTripleDESCryptoHelper.decryptText(EncPassword), "OldDoctor",_team.provtype);
                        }
                        else
                        {
                            new clsTemplate().TemplateForTeamMember(UserId, _team.FirstName + " " + _team.LastName, _team.Email, "", "OldDoctor",_team.provtype);
                        }
                    }
                }
                return Result;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SettingsBLL--AddEditTeamMember", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Remove Team Member
        /// </summary>
        /// <param name="TeamMemberId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static bool RemoveTeamMember(RemoveTeam TeamMemberId, int UserId)
        {
            try
            {
                string[] splited = TeamMemberId.TeamMemberId.FirstOrDefault().Split(',').Select(sValue => sValue.Trim()).ToArray();
                foreach (var item in splited)
                {
                    ObjColleaguesData.RemoveTeamMemberOfDoctor(SafeValue<int>(item), UserId);
                }
                return true;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SettingsBLL--RemoveTeamMember", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Check Dentist Email exists or not.
        /// </summary>
        /// <param name="Email"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static bool CheckDentistEmailExists(string Email, int UserId)
        {
            try
            {
                DataTable dtEmailChek = ObjColleaguesData.CheckEmailForDoctorSetting(Email, UserId);
                return (dtEmailChek.Rows.Count > 0) ? true : false;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SettingsBLL--CheckDentistEmailExists", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Disable Integrations
        /// </summary>
        /// <param name="DentrixConnectorIds"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static bool DisableIntegration(string DentrixConnectorIds, int UserId)
        {
            try
            {
                return clsColleaguesData.DisableIntegrations(DentrixConnectorIds, UserId);
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SettingsBLL--DisableIntegration", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Enable Integrations
        /// </summary>
        /// <param name="IntegrationId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static bool EnableIntegration(int IntegrationId, int UserId)
        {
            try
            {
                return clsColleaguesData.EnableIntegrations(IntegrationId, UserId);
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SettingsBLL--EnableIntegration", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Get Integration Company Details By Integration Id
        /// </summary>
        /// <param name="IntegrationId"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static List<CompanyIntegrationDetail> GetIntegrationDetails(int IntegrationId, int UserId)
        {
            try
            {
                DataTable DtIntegrat = ObjColleaguesData.GetDentrixConnectorValuesofUser(UserId, IntegrationId);
                List<CompanyIntegrationDetail> Obj = new List<CompanyIntegrationDetail>();
                if (DtIntegrat != null && DtIntegrat.Rows.Count > 0)
                {
                    foreach (DataRow items in DtIntegrat.Rows)
                    {
                        Obj.Add(new CompanyIntegrationDetail()
                        {
                            IntegrationId = IntegrationId,
                            ConnectorId = SafeValue<int>(items["DentrixConnectorId"]),
                            ConnectorKey = SafeValue<string>(items["AccountKey"]),
                            LocationId = SafeValue<int>(items["LocationId"]),
                            LocationList = GetDentistLocationList(UserId),
                            IntegrationName = SafeValue<string>(items["Name"])
                        });
                    }
                }
                return Obj;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SettingsBLL--GetIntegrationDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Enable Integration Connector
        /// </summary>
        /// <param name="id">Connector Id</param>
        /// <param name="AccountId">Account Id</param>
        /// <param name="UserId">User Id</param>
        /// <returns></returns>
        public static bool EnableConnector(int id,int AccountId,int UserId)
        {
            try
            {
                return clsColleaguesData.EnableConnector(id, AccountId, UserId);
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SettingsBLL--EnableConnector", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Disable Integration Connector
        /// </summary>
        /// <param name="id">Connector Id</param>
        /// <param name="AccountId">Account Id</param>
        /// <param name="UserId">User Id</param>
        /// <returns></returns>
        public static bool DisableConnector(int id,int AccountId,int UserId)
        {
            try
            {
                return clsColleaguesData.DisableConnector(id, AccountId, UserId);
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SettingsBLL--DisableConnector", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
