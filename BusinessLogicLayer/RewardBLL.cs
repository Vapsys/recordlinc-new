﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.ViewModel;
using DataAccessLayer.Common;
using System.Data;
using DataAccessLayer.ColleaguesData;
using System.Web.Mvc;
using BO.Models;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public class RewardBLL
    {
        /// <summary>
        /// This Method Used for Getting Dentist Reward Setting List.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static List<DentistReward> GetDentistRewardSettingDetails(int UserId)
        {
            try
            {
                List<DentistReward> lst = new List<DentistReward>();
                DataTable dt = new DataTable();
                dt = clsColleaguesData.GetDentistRewardSettingList(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lst.Add(new DentistReward()
                        {
                            SettingId = SafeValue<int>(item["SettingId"]),
                            RewardText = SafeValue<string>(item["RewardText"]),
                            RewardCode = SafeValue<string>(item["Code"]),
                            StartRange = SafeValue<string>(item["StartRange"]),
                            EndRange = SafeValue<string>(item["EndRange"]),
                            OptionId = SafeValue<string>(item["OptionId"]),
                            RewardMasterId = SafeValue<int>(item["RewardMasterId"])

                        });

                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-GetDentistRewardSettingDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        //public static ManageSuperBuck GetManageSuperBuck(int UserId)
        //{
        //    try
        //    {
        //        DataSet ds = new DataSet();
        //        clsColleaguesData clsColleaguesdata = new clsColleaguesData();
        //        ManageSuperBuck msbobj = new ManageSuperBuck();
        //        ds = clsColleaguesdata.GetManageSuperBucks(UserId);
        //        //Procedure Code
        //        if (ds != null)
        //        {
        //            if (ds.Tables[0].Rows.Count > 0)
        //            {
        //                foreach (DataRow item in ds.Tables[0].Rows)
        //                {
        //                    msbobj.lstProcedureCode.Add(new ProcedureCode() {
        //                        ProcedureCodeId = SafeValue<int>(item["id"]),
        //                        Procedurecode = SafeValue<string>(item["ProcedureCode"]),
        //                        ProcedureName = SafeValue<string>(item["ProcedureName"]),
        //                        SuperBucksReward =SafeValue<int>(item["SuperBucksRewards"])
        //                    });
        //                }
        //            }
        //            //Exams Code
        //            if (ds.Tables[1].Rows.Count > 0)
        //            {
        //                foreach (DataRow item in ds.Tables[1].Rows)
        //                {
        //                    msbobj.lstProcedureExamCodes.Add(new ProcedureExamCodes() {
        //                        ExamId = SafeValue<int>(item["ExamId"]),
        //                        ProcedureName = SafeValue<string>(item["ProcedureName"]),
        //                        ExamCode = SafeValue<int>(item["ExamCode"])
        //                    });
        //                }
        //            }
        //            //Longevity Multiplier
        //            if (ds.Tables[2].Rows.Count > 0)
        //            {
        //                foreach (DataRow item in ds.Tables[2].Rows)
        //                {
        //                    msbobj.lstRewardsLongevity.Add(new RewardsLongevity()
        //                    {
        //                        LM_Id= SafeValue<int>(item["LM_Id"]),
        //                        MonthsofLongevity =SafeValue<int>(item["MonthOfLongevity"]),
        //                        Multiplier = SafeValue<decimal>(item["Multiplier"])
        //                    });
        //                }

        //            }
        //            //Point balances
        //            if (ds.Tables[3].Rows.Count > 0)
        //            {
        //                foreach (DataRow item in ds.Tables[3].Rows)
        //                {
        //                    msbobj.lstPointBalances.Add(new PointsBalances()
        //                    {
        //                        PM_Id = SafeValue<int>(item["PM_Id"]),
        //                        PointLevel = SafeValue<int>(item["PointLevel"]),
        //                        Multiplier = SafeValue<decimal>(item["Multiplier"])
        //                    });
        //                }
        //            }
        //        }
        //        return msbobj;
        //    }
        //    catch (Exception Ex)
        //    {
        //        new clsCommon().InsertErrorLog("RewardBLL-GetManageSuperBuck", Ex.Message, Ex.StackTrace);
        //        throw;
        //    }
        //}

        public static List<ProcedureCode> GetProcedureCode(int Userid)
        {
            try
            {
                List<ProcedureCode> lst = new List<ProcedureCode>();
                DataTable dt = new DataTable();
                dt = clsColleaguesData.GetManageSuperBucks(Userid, 1,0);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lst.Add(new ProcedureCode()
                        {
                            ProcedureCodeId = SafeValue<int>(item["id"]),
                            Procedurecode = SafeValue<string>(item["ProcedureCode"]),
                            ProcedureName = SafeValue<string>(item["ProcedureName"]),
                            SuperBucksReward = SafeValue<decimal>(item["SuperBucksRewards"])
                        });
                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-GetProcedureCode", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// This Method Get List of Point multiplier from db.
        /// </summary>
        /// <param name="Userid"></param>
        /// <returns></returns>
        public static List<PointsBalances> GetPointMultiplier(int Userid,int PlanId)
        {
            try
            {
                List<PointsBalances> lst = new List<PointsBalances>();
                DataTable dt = new DataTable();
                dt = clsColleaguesData.GetManageSuperBucks(Userid, 3, PlanId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lst.Add(new PointsBalances()
                        {
                            PM_Id = SafeValue<int>(item["PM_Id"]),
                            PointLevel = SafeValue<int>(item["PointLevel"]),
                            Multiplier = SafeValue<decimal>(item["Multiplier"]),
                            PlanName = SafeValue<string>(item["PlanName"])
                        });
                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-GetPointMultiplier", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static List<RewardsLongevity> GetLongevityMultiplier(int Userid)
        {
            try
            {
                List<RewardsLongevity> lst = new List<RewardsLongevity>();
                DataTable dt = new DataTable();
                dt = clsColleaguesData.GetManageSuperBucks(Userid, 2,0);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lst.Add(new RewardsLongevity()
                        {
                            LM_Id = SafeValue<int>(item["LM_Id"]),
                            MonthsofLongevity = SafeValue<int>(item["MonthOfLongevity"]),
                            Multiplier = SafeValue<decimal>(item["Multiplier"])
                        });
                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-GetLongevityMultiplier", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static List<SelectListItem> GetDentistRewardCodeList()
        {
            try
            {
                List<SelectListItem> RewardCodeList = new List<SelectListItem>();
                DataTable dt = new DataTable();
                dt = clsColleaguesData.GetRewardCodes();

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        SelectListItem selitem = new SelectListItem();
                        selitem.Text = SafeValue<string>(item["Code"]);
                        selitem.Value = SafeValue<string>(item["RewardMasterId"]);
                        RewardCodeList.Add(selitem);
                    }
                }
                return RewardCodeList;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-GetDentistRewardCodeList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool UpdateRewardListOfUser(List<DentistReward> lst, int UserId)
        {
            try
            {
                if (lst.Count > 0)
                {
                    foreach (var item in lst)
                    {
                        clsColleaguesData.UpdateRewardListOfUser(item, UserId);
                    }
                }
                return true;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-UpdateRewardListOfUser", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        public static string InsertRewardPointForProcedureCode(BO.ViewModel.ProcedureCode rc)
        {
            try
            {
                string res = "";
                res = clsColleaguesData.InsertRewardPointForProcedureCode(rc);
                return res;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-InsertRewardPointForProcedureCode", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool InsertProcedureCodesForExams(ProcedureExamCode ecobj)
        {
            try
            {
                clsColleaguesData.InsertProcedureCodesForExams(ecobj);
                return true;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-InsertProcedureCodesForExams", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static string InsertRewardMultiplierForLongevity(RewardLongevity relobj)
        {
            try
            {
                string res = "";
                res = clsColleaguesData.InsertRewardMultiplierForLongevity(relobj);
                return res;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-InsertRewardMultiplierForLongevity", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool InsertPointMultiplierForPointBalance(BO.Models.PointBalance pbobj)
        {
            bool res = false;
            try
            {
                res = clsColleaguesData.InsertPointMultiplierForPointBalance(pbobj);
                return res;

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-InsertPointMultiplierForPointBalance", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool DeleteRewardPointForProcedureCode(ProcedureCodes rc)
        {
            bool res = false;
            try
            {
                res = clsColleaguesData.DeleteRewardPointForProcedureCode(rc);
                return res;

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-DeleteRewardPointForProcedureCode", Ex.Message, Ex.StackTrace);
                throw;
            }
        }


        public static bool DeleteProcedureCodesForExams(ProcedureExamCode ecobj)
        {
            bool res = false;
            try
            {
                res = clsColleaguesData.DeleteProcedureCodesForExams(ecobj);
                return res;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-DeleteProcedureCodesForExams", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static string DeleteRewardMultiplierForLongevity(int LMId)
        {
            string res = "";
            try
            {
                res = clsColleaguesData.DeleteRewardMultiplierForLongevity(LMId);
                return res;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-DeleteRewardMultiplierForLongevity", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool DeletePointMultiplierForPointBalance(PointBalance pbobj)
        {
            bool res = false;
            try
            {
                res = clsColleaguesData.DeletePointMultiplierForPointBalance(pbobj);
                return res;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-DeletePointMultiplierForPointBalance", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static ProcedureCodes AddEditRewardCode(ProcedureCodes pc)
        {
            DataTable dt = new DataTable();
            ProcedureCodes pcobj = new ProcedureCodes();
            dt = clsColleaguesData.AddEditRewardCode(pc);
            if (dt != null && dt.Rows.Count > 0)
            {
                pc.ProcedureCode = SafeValue<string>(dt.Rows[0]["ProcedureCode"]);
                pc.ProcedureName = SafeValue<string>(dt.Rows[0]["ProcedureName"]);
                pc.SuperBucksReward = SafeValue<decimal>(dt.Rows[0]["SuperBucksRewards"]);
            }
            return pc;
        }

        //For Super Plan
        public static SuperPlan AddEditSuperPlan(SuperPlan sp)
        {
            DataTable dt = new DataTable();
            SuperPlan pcobj = new SuperPlan();
            dt = clsColleaguesData.AddEditSuperPlan(sp);
            if (dt != null && dt.Rows.Count > 0)
            {
                sp.PlanId = SafeValue<int>(dt.Rows[0]["PlanId"]);
                sp.PlanName = SafeValue<string>(dt.Rows[0]["PlanName"]);
                sp.Amount = SafeValue<decimal>(dt.Rows[0]["Amount"]);
                sp.RewardPoint = SafeValue<decimal>(dt.Rows[0]["RewardPoint"]);
                sp.Points = SafeValue<decimal>(dt.Rows[0]["Points"]);
                sp.ExamCodes = SafeValue<int>(dt.Rows[0]["ExamCodes"]);
            }
            return sp;
        }
        public static List<SuperPlan> GetSuperPlan(int Userid)
        {
            try
            {
                List<SuperPlan> lst = new List<SuperPlan>();
                DataTable dt = new DataTable();
                dt = clsColleaguesData.GetSuperPlan(Userid);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lst.Add(new SuperPlan()
                        {
                            PlanId = SafeValue<int>(item["PlanId"]),
                            PlanName = SafeValue<string>(item["PlanName"]),
                            Amount = SafeValue<decimal>(item["Amount"]),
                            IsPrimary = SafeValue<bool>(item["IsPrimary"]),
                            RewardPoint = SafeValue<decimal>(item["RewardPoint"]),
                            Points = SafeValue<decimal>(item["Points"]),
                            ExamCodes= SafeValue<int>(item["ExamCodes"])
                        });
                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-GetSuperPlan", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        public static string AddSuperPlan(SuperPlan sp)
        {
            string res = "";
            try
            {
                res = clsColleaguesData.AddSuperPlan(sp);
                return res;

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-AddSuperPlan", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static string DeleteSuperPlan(SuperPlan sp)
        {
            string res = "";
            try
            {
                res = clsColleaguesData.DeleteSuperPlan(sp);
                return res;

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-DeleteSuperPlan", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static List<ProcedureCode> GetProcedureCodeData(int UserId, int PlanId)
        {
            List<ProcedureCode> Plist = new List<ProcedureCode>();
            DataTable dt = new DataTable();
            dt = clsColleaguesData.GetProcedureCodeData(UserId, PlanId);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    Plist.Add(new ProcedureCode()
                    {
                        ProcedureCodeId = SafeValue<int>(item["ProcedureCodeId"]),
                        Procedurecode = SafeValue<string>(item["ProcedureCode"]),
                        ProcedureName = SafeValue<string>(item["ProcedureName"]),
                        SuperBucksReward = SafeValue<int>(item["SuperBucksRewards"]),
                        IsExam = SafeValue<bool>(item["IsExam"]),
                        PlanName = SafeValue<string>(item["PlanName"])
                    });
                }
            }
            return Plist;
        }
        public static string InsertProcedureExamAssociation(List<ProcedureCode> sp, int UserId)
        {
            string res = "";
            try
            {
                if (sp.Count > 0)
                {
                    foreach (var item in sp)
                    {
                        res = clsColleaguesData.InsertProcedureExamAssociation(item, UserId);

                    }
                }
                return res;

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-InsertProcedureExamAssociation", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static string InsertProcedureForPlan(List<ProcedureCode> sp, int UserId, int PlanId)
        {
            string res = "";
            try
            {
                if (sp.Count > 0)
                {
                    foreach (var item in sp)
                    {
                        if (item.IsChecked == true)
                            res = clsColleaguesData.InsertProcedureForPlan(item, UserId, PlanId);
                        else
                            res = clsColleaguesData.DeleteProcedureForPlan(item.ProcedureCodeId, PlanId);
                    }
                }
                return res;

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-InsertProcedureForPlan", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static string DeleteProcedureForPlan(int ProcedureCodeId,int PlanId)
        {
            string res = "";
            try
            {
                res = clsColleaguesData.DeleteProcedureForPlan(ProcedureCodeId,PlanId);

                return res;

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-DeleteProcedureForPlan", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        
        //For LongevityMultiplier
        public static List<RewardsLongevity> Get_LongevityMultiplier(int Userid, int PlanId)
        {
            try
            {
                List<RewardsLongevity> lst = new List<RewardsLongevity>();
                DataTable dt = new DataTable();
                dt = clsColleaguesData.Get_LongevityMultiplier(Userid, PlanId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lst.Add(new RewardsLongevity()
                        {
                            LM_Id = SafeValue<int>(item["LM_Id"]),
                            MonthsofLongevity = SafeValue<int>(item["MonthOfLongevity"]),
                            Multiplier = SafeValue<decimal>(item["Multiplier"]),
                            PlanName = SafeValue<string>(item["PlanName"])
                        });
                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-Get_LongevityMultiplier", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static RewardsLongevity GetLongitivtyPopUp(RewardsLongevity rl)
        {
            DataTable dt = new DataTable();
            RewardsLongevity pcobj = new RewardsLongevity();
            dt = clsColleaguesData.GetLongitivtyPopUp(rl);
            if (dt != null && dt.Rows.Count > 0)
            {
                rl.PlanId = SafeValue<int>(dt.Rows[0]["PlanId"]);
                rl.MonthsofLongevity = SafeValue<int>(dt.Rows[0]["MonthOfLongevity"]);
                rl.Multiplier = SafeValue<decimal>(dt.Rows[0]["Multiplier"]);
                rl.LM_Id = SafeValue<int>(dt.Rows[0]["LM_Id"]);
            }
            return rl;
        }

        public static List<ProcedureCode> GetAllProcedure(int UserId, int PlanId)
        {
            List<ProcedureCode> Plist = new List<ProcedureCode>();
            DataTable dt = new DataTable();
            dt = clsColleaguesData.GetAllProcedure(UserId,PlanId);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    Plist.Add(new ProcedureCode()
                    {
                        ProcedureCodeId = SafeValue<int>(item["ProcedureCodeId"]),
                        Procedurecode = SafeValue<string>(item["ProcedureCode"]),
                        ProcedureName = SafeValue<string>(item["ProcedureName"]),
                        IsChecked = SafeValue<bool>(item["IsChecked"])
                    });
                }
            }
            return Plist;
        }
        /// <summary>
        /// This Method Get Point Multiplier Details form database for Edit time.
        /// </summary>
        /// <param name="PointId"></param>
        /// <returns></returns>
        public static PointsBalances GetPointMuliplierDetails(int PointId, int Userid)
        {
            try
            {
                PointsBalances Obj = new PointsBalances();
                //Get Particular Point Multiplier Details using PointId.
                DataTable dt = clsColleaguesData.GetPointMultiplier(PointId, Userid);
                if(dt != null && dt.Rows.Count > 0)
                {
                    Obj.PlanId = SafeValue<int>(dt.Rows[0]["PM_Id"]);
                    Obj.PointLevel = SafeValue<int>(dt.Rows[0]["PointLevel"]);
                    Obj.Multiplier = SafeValue<decimal>(dt.Rows[0]["Multiplier"]);
                    Obj.PlanId = SafeValue<int>(dt.Rows[0]["SupPlanId"]);
                    Obj.PM_Id = SafeValue<int>(dt.Rows[0]["PM_Id"]);
                }
                List<SuperPlan> lst = new List<SuperPlan>();
                //Get All Active Plans from database.
                dt = clsColleaguesData.GetSuperPlan(Userid);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lst.Add(new SuperPlan()
                        {
                            PlanId = SafeValue<int>(item["PlanId"]),
                            PlanName = SafeValue<string>(item["PlanName"]),
                            Amount = SafeValue<decimal>(item["Amount"]),
                        });
                    }
                }
                Obj.Plans = lst;
                return Obj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-GetPointMuliplierDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// This Method Insert Point Multiplier on DB
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public static int InsertPointMultiplier(PointsBalances Obj,int UserId)
        {
            try
            {
                return clsColleaguesData.InsertPointMultiplierOfUser(Obj, UserId);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-InsertPointMultiplier", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// This Method is used for delete the Point Multiplier.
        /// </summary>
        /// <param name="PointId"></param>
        /// <returns></returns>
        public static bool DeletePointMultiplier(int PMId, int UserID)
        {
            try
            {
                return clsColleaguesData.DeletePointMulitplier(PMId, UserID);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("RewardBLL-DeletePointMultiplier", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
