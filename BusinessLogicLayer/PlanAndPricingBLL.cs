﻿using System;
using System.Collections.Generic;
using BO.Models;
using System.Data;
using DataAccessLayer.Common;
using DataAccessLayer.ProfileData;
using DataAccessLayer;
using DataAccessLayer.ColleaguesData;
using BO.ViewModel;
using System.Web;
using System.Web.Helpers;
using System.Globalization;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public class PlanAndPricingBLL
    {
        clsPlanPricingDAL objPlanPricing = new clsPlanPricingDAL();
        clsCommon ObjCommon;
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        clsAdmin ObjAdmin = new clsAdmin();
       public static Dictionary<bool,string>  Signup(Members model)
        {
            var result = new Dictionary<bool, string>();
            DataTable dtCheckEmail = new DataTable();
            clsColleaguesData ObjColleaguesData = new clsColleaguesData();
            clsCommon ObjCommon = new clsCommon(); ;
            clsTemplate ObjTemplate = new clsTemplate();
            dtCheckEmail = ObjColleaguesData.CheckEmailInSystem(model.Email);
            if(dtCheckEmail != null && dtCheckEmail.Rows.Count > 0)
            {
                int AllowLogin = SafeValue<int>(dtCheckEmail.Rows[0]["Status"]);
                if (AllowLogin == 1)
                {
                    string FullName = string.Empty; string EncPassword = string.Empty;
                    #region Send Password to user if already in system
                    // send password tempalte
                    FullName = SafeValue<string>(dtCheckEmail.Rows[0]["FirstName"]) + " " + SafeValue<string>(dtCheckEmail.Rows[0]["LastName"]);
                    EncPassword = SafeValue<string>(dtCheckEmail.Rows[0]["Password"]);
                    ObjTemplate.SendingForgetPassword(FullName, model.Email, EncPassword);
                    result.Add(false, "1");
                    return result;
                    #endregion

                }
                else
                {
                    result.Add(false, "1");
                }
            }
            else
            {
                result.Add(true, "0");
            }
            return result;
        }

        public MainPlanPricing GetPlanPricing()
        {
            DataTable dt = new DataTable();
            MainPlanPricing lstPlanPricing = new MainPlanPricing();
            try
            {
                dt = objPlanPricing.GetPlanPricingDetail();
                if (dt != null && dt.Rows.Count > 0)
                {
                    lstPlanPricing.dsPackages = dt;
                }
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("GetPlanPricing--PlanAndPricingBLL", ex.Message, ex.StackTrace);
                throw;
            }
            return lstPlanPricing;
        }
        public static Dictionary<bool,string> SaveBillingInfo(PlanAndPricing ObjPlanAndPrice, string requestUrl)
        {
            TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
            clsCommon ObjCommon = new clsCommon();
            clsColleaguesData ObjColleaguesData = new clsColleaguesData();
            clsAdmin ObjAdmin = new clsAdmin();
            clsTemplate ObjTemplate = new clsTemplate();
            int UserId = 0;
            try
            {
                var Result = new Dictionary<bool, string>();
                DataTable dtCheckEmail = new DataTable();
                dtCheckEmail = ObjColleaguesData.CheckEmailInSystem(ObjPlanAndPrice.Signup.Email);
                if (dtCheckEmail != null && dtCheckEmail.Rows.Count > 0)
                {
                    DataTable dtFirstLogin = ObjAdmin.GetTempSignupByEmail(ObjPlanAndPrice.Signup.Email);
                    if (dtFirstLogin != null && dtFirstLogin.Rows.Count > 0)
                    {
                        //If User on Temp Table Then move to member table and do billing for user.
                        string Password = ObjCommon.CreateRandomPassword(8);
                        string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                        string Accountkey = Crypto.SHA1(SafeValue<string>(DateTime.Now.Ticks));
                        if (dtFirstLogin.Rows.Count > 0)
                        {
                            UserId = ObjColleaguesData.SingUpDoctor(ObjPlanAndPrice.Signup.Email, EncPassword,
                                (!string.IsNullOrWhiteSpace(SafeValue<string>(dtFirstLogin.Rows[0]["first_name"])) ? SafeValue<string>(dtFirstLogin.Rows[0]["first_name"]) : ObjPlanAndPrice.Signup.FirstName),                                
                                (!string.IsNullOrWhiteSpace(SafeValue<string>(dtFirstLogin.Rows[0]["last_name"])) ? SafeValue<string>(dtFirstLogin.Rows[0]["last_name"]) : ObjPlanAndPrice.Signup.LastName), null, null, null, Accountkey, null, null, null, null, null, null, (SafeValue<string>(dtFirstLogin.Rows[0]["phoneno"])), null, false, SafeValue<string>(dtFirstLogin.Rows[0]["facebook"]),SafeValue<string>(dtFirstLogin.Rows[0]["linkedin"]),SafeValue<string>(dtFirstLogin.Rows[0]["twitter"]), null, null, SafeValue<string>(dtFirstLogin.Rows[0]["description"]), null, null, null, null, null, null, null, 0, null, null, null, 0);
                            bool result = ObjAdmin.UpdateStatusInTempTableUserById(SafeValue<int>(dtFirstLogin.Rows[0]["id"]), 1);// update status 1 in temp table 
                            ObjTemplate.NewUserEmailFormat(ObjPlanAndPrice.Signup.Email, SafeValue<string>(HttpContext.Current.Session["CompanyWebsite"]) + "/User/Index?UserName=" + ObjPlanAndPrice.Signup.Email + "&Password=" + EncPassword, Password,SafeValue<string>(HttpContext.Current.Session["CompanyWebsite"]));
                            
                        }
                        //Here is the billing code for the user.
                        //.....

                        Result.Add(true, "0");//0 = User move to temp table to Member table and send activation mail.
                        return Result;
                    }
                    else
                    {
                        //If user already exist in the system and not used then resend user credentials of it.
                        string FullName = string.Empty; string EncPassword = string.Empty;
                        int AllowLogin = SafeValue<int>(dtCheckEmail.Rows[0]["Status"]);
                        if (AllowLogin == 1)
                        {
                            #region Send Password to user if already in system
                            // send password tempalte
                            FullName = SafeValue<string>(dtCheckEmail.Rows[0]["FirstName"]) + " " + SafeValue<string>(dtCheckEmail.Rows[0]["LastName"]);
                            EncPassword = SafeValue<string>(dtCheckEmail.Rows[0]["Password"]);
                            ObjTemplate.SendingForgetPassword(FullName, ObjPlanAndPrice.Signup.Email, EncPassword);
                            Result.Add(false, "You are already signed up, Please active your account.");
                            return Result;
                            #endregion

                        }
                        else
                        {
                            //If Email is De Actvie by Admin then send this link as below
                            if (AllowLogin == 2)
                            {
                                string companywebsite = "";
                                if (HttpContext.Current.Session["CompanyWebsite"] != null)
                                {
                                    companywebsite = SafeValue<string>(HttpContext.Current.Session["CompanyWebsite"]);
                                    //companywebsite = "http://localhost:2298/";
                                }
                                FullName = SafeValue<string>(dtCheckEmail.Rows[0]["FirstName"]) + " " + SafeValue<string>(dtCheckEmail.Rows[0]["LastName"]);
                                EncPassword = SafeValue<string>(dtCheckEmail.Rows[0]["Password"]);
                                ObjTemplate.NewUserEmailFormat(ObjPlanAndPrice.Signup.Email, companywebsite + "/User/Index?UserName=" + ObjPlanAndPrice.Signup.Email + "&Password=" + EncPassword + "&Status=2", ObjTripleDESCryptoHelper.decryptText(EncPassword), companywebsite);

                            }
                            Result.Add(false, "You are already signed up, Please active your account.");
                            return Result;
                        }
                    }

                }
                else
                {
                    //If User not Exist on System then insert into Member Table.
                    //Send him to activation mail for Insert activation date on Member table.
                    string Password = ObjCommon.CreateRandomPassword(8);
                    string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                    string Accountkey = Crypto.SHA1(SafeValue<string>(DateTime.Now.Ticks));
                    UserId = ObjColleaguesData.SingUpDoctor(ObjPlanAndPrice.Signup.Email, EncPassword, ObjPlanAndPrice.Signup.FirstName, ObjPlanAndPrice.Signup.LastName, null, null, null, Accountkey, null, null, null, null, null, null, ObjPlanAndPrice.Signup.Phone, null, true, null, null, null, null, null, null, null, null, null, null, null, null, null, 0, null, null, null, 0);
                    
                    #region Payment code
                    //BillingInfo objbill = ObjPlanAndPrice.ObjBillingInfo;
                    //Members objmem = ObjPlanAndPrice.Signup;

                    //PayamentDetails objPyamentDetails = new PayamentDetails();
                    //objPyamentDetails.Planes = SafeValue<string>(ObjPlanAndPrice.MembershipId);
                    //objPyamentDetails.Firstname = objmem.FirstName;
                    //objPyamentDetails.Lastname = objmem.LastName;
                    //objPyamentDetails.Email = objmem.Email;
                    //objPyamentDetails.Phone = objmem.Phone;
                    //objPyamentDetails.Zip = objbill.BillingZipCode;
                    //objPyamentDetails.Cvv = objbill.CVV;
                    //objPyamentDetails.CardNumber = objbill.CardNumber;
                    //objPyamentDetails.expdate = SafeValue<string>(DateTime.ParseExact(objbill.ExpirationMonth, "MMMM", CultureInfo.CurrentCulture).Month + "/" + objbill.ExpirationYear);
                    //objPyamentDetails.payInterval = objbill.BillPayInterval;
                    //objPyamentDetails.Amount = SafeValue<string>(objbill.BillingAmount);
                    //bool IsPlanPrice = true;
                    //RecurringSubscription objSubs = new RecurringSubscription();
                    //string strResult = objSubs.MakePayment(objPyamentDetails, IsPlanPrice, requestUrl);
                    #endregion
                    Result.Add(true, "Done");
                    return Result;
                }
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SaveBillingInfo--PlanAndPricingBLL", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public List<PlanAndPrice> GetPlanDetialsForTablet()
        {
            try
            {
                DataTable dt = new DataTable();
                List<PlanAndPrice> Lst = new List<PlanAndPrice>();
                dt = ObjColleaguesData.GetPlanDetialsForTabletView();
                if(dt !=null && dt.Rows.Count >0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        Lst.Add(new PlanAndPrice()
                        {
                            MembershipId = SafeValue<int>(item["Membership_Id"]),
                            FeatureId = SafeValue<int>(item["Feature_Id"]),
                            FeatureName = SafeValue<string>(item["Title"]),
                            CanSee = SafeValue<bool>(item["CanSee"] == System.DBNull.Value ? false : item["CanSee"]),
                            MaxCount = item["MaxCount"] == System.DBNull.Value ? null:SafeValue<string>(item["MaxCount"])
                        });
                    }
                }
                return Lst;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("PlanAndPricingBLL--GetPlanDetialsForTablet", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
