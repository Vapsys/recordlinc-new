﻿using DataAccessLayer.ColleaguesData;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class AccountSettingsBLL
    {
        public static DataTable GetDentrixConnectorByIntegrationId(int IntegrationId,int AccountId)
        {
            try
            {
                return clsColleaguesData.GetDentrixConnectorByIntegrationId(IntegrationId,AccountId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static DataTable GetAllIntegrationMaster()
        {
            try
            {
                return clsColleaguesData.GetAllIntegrationMaster();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static DataTable GetLocationOfUserForIntegration(int UserId, int IntegrationId)
        {
            try
            {
                return clsColleaguesData.GetLocationOfUserForIntegration(UserId, IntegrationId);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
