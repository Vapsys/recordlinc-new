﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.ViewModel;
using DataAccessLayer.Common;
using DataAccessLayer.ColleaguesData;
using System.Data;
using System.Web.Helpers;
using BO.Models;
using DataAccessLayer.FlipTop;
using System.Web;
using DataAccessLayer.PatientsData;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{

    public class SignUpBLL
    {
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        clsCommon objCommon = new clsCommon();
        clsTemplate ObjTemplate = new clsTemplate();
        string _Password_onSignUp = string.Empty;
        public IDictionary<bool, string> SignuponRecordlinc(DentistSignup Obj)
        {
            try
            {
                string companywebsite = "";
                if (!string.IsNullOrWhiteSpace(Obj.CompanyWebSite))
                {
                    companywebsite = Obj.CompanyWebSite;
                }
                else
                {
                    string url = HttpContext.Current.Request.Url.ToString();
                    Uri originalUrl = new Uri(url); // Request.Url
                    string domain = originalUrl.Host;
                    companywebsite = domain;
                }
                var Result = new Dictionary<bool, string>();
                DataTable dtCheckEmail = new DataTable();
                dtCheckEmail = ObjColleaguesData.CheckEmailInSystem(Obj.Email);
                if (dtCheckEmail.Rows.Count > 0)
                {
                    string FullName = string.Empty; string EncPassword = string.Empty;

                    // send template for forgot password
                    int AllowLogin = SafeValue<int>(dtCheckEmail.Rows[0]["Status"]);
                    if (AllowLogin == 1)
                    {
                        #region Send Password to user if already in system

                        FullName = SafeValue<string>(dtCheckEmail.Rows[0]["FirstName"]) + " " + SafeValue<string>(dtCheckEmail.Rows[0]["LastName"]);
                        EncPassword = SafeValue<string>(dtCheckEmail.Rows[0]["Password"]);
                        ObjTemplate.SendingForgetPassword(FullName, Obj.Email, EncPassword);
                        Result.Add(false, "User already exists. Please check your email to reset your password.");
                        return Result;
                        #endregion
                    }
                    else
                    {
                        //If Email is De Actvie by Admin then send this link as below
                        if (AllowLogin == 2)
                        {
                            FullName = SafeValue<string>(dtCheckEmail.Rows[0]["FirstName"]) + " " + SafeValue<string>(dtCheckEmail.Rows[0]["LastName"]);
                            EncPassword = SafeValue<string>(dtCheckEmail.Rows[0]["Password"]);
                            ObjTemplate.NewUserEmailFormat(Obj.Email, companywebsite + "/User/Index?UserName=" + Obj.Email + "&Password=" + EncPassword + "&Status=2", ObjTripleDESCryptoHelper.decryptText(EncPassword), companywebsite);
                            Result.Add(false, "Your account has been De-activated by Admin");
                            return Result;
                        }
                        else
                        {
                            FullName = SafeValue<string>(dtCheckEmail.Rows[0]["FirstName"]) + " " + SafeValue<string>(dtCheckEmail.Rows[0]["LastName"]);
                            EncPassword = SafeValue<string>(dtCheckEmail.Rows[0]["Password"]);
                            ObjTemplate.SendingForgetPassword(FullName, Obj.Email, EncPassword);
                            Result.Add(false, "User already exists. Please check your email to reset your password.");
                            return Result;
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(Obj.Password))
                    {
                        #region Doctor sign up in system
                        string Password = objCommon.CreateRandomPassword(8);
                        string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                        string Accountkey = Crypto.SHA1(Convert.ToString(DateTime.Now.Ticks));
                        //Doctor sign up
                        Person person = GetSocialMediaPerson(Obj.Email); //Get Social Media detail
                        int NewUserId = 0;
                        // Here we put code for sign up doctor in sytem or Temp table
                        NewUserId = ObjColleaguesData.Insert_Temp_Signup(Obj.Email, EncPassword, false, person.ImageUrl, Obj.FirstName, Obj.LastName, person.Age, person.Gender, person.Location, person.Company, person.Title, person.FacebookURL, person.TwitterURL, person.LinkedInURL, person.InfluenceScore, Obj.Phone, person.Description);
                        if (NewUserId > 0)
                        {
                            clsTemplate ObjTemplate = new clsTemplate();
                            string Email = Obj.Email;
                            string fullName = Obj.FirstName + " " + Obj.LastName;
                            string Phone = Obj.Phone;

                            ObjTemplate.NewUserEmailFormat(Email, companywebsite + "/User/Index?UserName=" + Email + "&Password=" + EncPassword, Password, companywebsite); // send tempate to admin if doctor sing up in system
                            ObjTemplate.SignUpEMail(Email, fullName, Phone, person.FacebookURL, person.TwitterURL, person.LinkedInURL);// send tempate to admin if doctor sing up in system
                            Result.Add(true, string.Empty);
                            return Result;
                        }
                        else
                        {
                            Result.Add(false, "Insert time some error throw.");
                            return Result;
                        }
                        #endregion
                    }
                    // Bhupesh : Make only Else part Here with Status True and Commented on NewUserEmail Format and SignUpEmail Code
                    else
                    {
                        #region Doctor sign up in system
                        // string Password = objCommon.CreateRandomPassword(8);
                        string EncPassword = ObjTripleDESCryptoHelper.encryptText(Obj.Password);
                        string Accountkey = Crypto.SHA1(Convert.ToString(DateTime.Now.Ticks));
                        //Doctor sign up
                        Person person = GetSocialMediaPerson(Obj.Email); //Get Social Media detail
                        int NewUserId = 0;
                        // Here we put code for sign up doctor in sytem or Temp table
                        NewUserId = ObjColleaguesData.Insert_Temp_Signup(Obj.Email, EncPassword, true, person.ImageUrl, Obj.FirstName, Obj.LastName, person.Age, person.Gender, person.Location, person.Company, person.Title, person.FacebookURL, person.TwitterURL, person.LinkedInURL, person.InfluenceScore, Obj.Phone, person.Description);
                        if (NewUserId > 0)
                        {
                            clsTemplate ObjTemplate = new clsTemplate();
                            string Email = Obj.Email;
                            string fullName = Obj.FirstName + " " + Obj.LastName;
                            string Phone = Obj.Phone;
                            //Ankit Here 02-28-2019 10:32PM: Added code for Active account 
                            NewUserId = ObjColleaguesData.SingUpDoctor(Obj.Email, EncPassword, SafeValue<string>(Obj.FirstName), SafeValue<string>(Obj.LastName), null, null, null, Accountkey, null, null, null, null, null, null, SafeValue<string>(Obj.Phone), null, false, SafeValue<string>(person.FacebookURL), SafeValue<string>(person.LinkedInURL), SafeValue<string>(person.TwitterURL), null, null, SafeValue<string>(person.Description), null, null, null, null, null, null, null, 0, null, null, null, 0);
                            // ObjTemplate.NewUserEmailFormat(Email, companywebsite + "/User/Index?UserName=" + Email + "&Password=" + _Password_onSignUp, Password, companywebsite); // send tempate to admin if doctor sing up in system
                            // ObjTemplate.SignUpEMail(Email, fullName, Phone, person.FacebookURL, person.TwitterURL, person.LinkedInURL);// send tempate to admin if doctor sing up in system
                            Result.Add(true, string.Empty);
                            return Result;
                        }
                        else
                        {
                            Result.Add(false, "Insert time some error throw.");
                            return Result;
                        }
                        #endregion
                    }
                }
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        protected Person GetSocialMediaPerson(string email)
        {
            try
            {
                //connect to fliptop and pull the information down
                SocialMedia sm = new SocialMedia();
                Person person = sm.LookUpPersonClearBit(email);
                Person fullperson = sm.LookUpPersonfullcontact(email);
                person.FirstName = !string.IsNullOrEmpty(person.FirstName) ? person.FirstName : fullperson.FirstName;
                person.LastName = !string.IsNullOrEmpty(person.LastName) ? person.LastName : fullperson.LastName;
                person.FullName = !string.IsNullOrEmpty(person.FullName) ? person.FullName : fullperson.FullName;
                person.FacebookURL = !string.IsNullOrEmpty(person.FacebookURL) ? person.FacebookURL : fullperson.FacebookURL;
                person.LinkedInURL = !string.IsNullOrEmpty(person.LinkedInURL) ? person.LinkedInURL : fullperson.LinkedInURL;
                person.TwitterURL = !string.IsNullOrEmpty(person.TwitterURL) ? person.TwitterURL : fullperson.TwitterURL;
                person.ImageUrl = !string.IsNullOrEmpty(person.ImageUrl) ? person.ImageUrl : fullperson.ImageUrl;
                person.Description = !string.IsNullOrEmpty(fullperson.Description) ? fullperson.Description : person.Description;
                return person;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int SignupPatientOnRecordlinc(PatientSignup model,int userid)
        {
            try
            {
                clsPatientsData ObjPatientsData = new clsPatientsData();
                DataTable dtPatient = new DataTable();
                bool EmailCheck = false;
                int FromPublicProfilePatientId = 0;
                EmailCheck = ObjPatientsData.CheckPatientEmail(model.Email);
                if (EmailCheck)
                {
                    dtPatient = ObjPatientsData.GetPatientPassword(model.Email);
                    if (dtPatient != null && dtPatient.Rows.Count > 0)
                    {
                        FromPublicProfilePatientId = SafeValue<int>(dtPatient.Rows[0]["PatientId"]);
                    }
                }
                else
                {
                    // Code for create new patient
                    int InsertPatient = 0;
                    string Password = objCommon.CreateRandomPassword(8);

                    InsertPatient = ObjPatientsData.PatientInsert(0, null, model.FirstName, "", model.LastName,  "", 0, null, model.Phone, model.Email,  null, null, null, null, null, userid, 0, null, Password, null);
                    if (InsertPatient != 0)
                    {
                        FromPublicProfilePatientId = InsertPatient;
                        //Session["PublicProfilePatientId"] = InsertPatient;
                        // Send Patient added notification to Doctor
                        ObjTemplate.PatientAddedNotification(userid, InsertPatient, model.Email, model.FirstName, model.LastName, model.Phone, "", "", "", "", "", "");
                        //int id = ObjPatientsData.Insert_PatientMessages(model.FirstName, model.username, "Patient sign-up", "<a href=\"javascript:;\" onclick=\"getAttachedPatientHistory(" + InsertPatient + ")\">" + model.FirstName + " " + model.LastName + "</a> recently signed up as patient.", SafeValue<int>(userid), InsertPatient, false, false, true);
                        ObjTemplate.PatientSignUpEMail(model.Email, Password, userid);
                    }
                }
                return FromPublicProfilePatientId;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }

        public int SignupForVeri(UserSignUpForVeri objUserSignUpForVeri)
        {
            string companywebsite = System.Configuration.ConfigurationManager.AppSettings["VeridentWebLink"];
            int NewUserId = 0;
            try
            {
                DataTable dtCheckEmail = new DataTable();
                dtCheckEmail = ObjColleaguesData.CheckEmailInSystem(objUserSignUpForVeri.Email);
                if (dtCheckEmail.Rows.Count == 0)
                {
                    string Password = objCommon.CreateRandomPassword(8);
                    string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                    string EncEmail = ObjTripleDESCryptoHelper.encryptText(objUserSignUpForVeri.Email);

                    Person person = GetSocialMediaPerson(objUserSignUpForVeri.Email);
                    NewUserId = ObjColleaguesData.Insert_Temp_Signup(objUserSignUpForVeri.Email, EncPassword, false, person.ImageUrl, objUserSignUpForVeri.FirstName, objUserSignUpForVeri.LastName, person.Age, person.Gender, person.Location, person.Company, person.Title, person.FacebookURL, person.TwitterURL, person.LinkedInURL, person.InfluenceScore, objUserSignUpForVeri.Phone, person.Description, objUserSignUpForVeri.Speciality, objUserSignUpForVeri.PracticeName, objUserSignUpForVeri.NpiNumber);
                    if (NewUserId > 0)
                    {
                        clsTemplate ObjTemplate = new clsTemplate();
                        string Email = objUserSignUpForVeri.Email;
                        string fullName = objUserSignUpForVeri.FirstName + " " + objUserSignUpForVeri.LastName;
                        string Phone = objUserSignUpForVeri.Phone;
                        ObjTemplate.NewUserEmail(Email, companywebsite + "activation?Email=" + EncEmail + "&Password=" + EncPassword, Password, companywebsite); // send tempate to admin if doctor sing up in system
                        ObjTemplate.SignUpEMail(Email, fullName, Phone, person.FacebookURL, person.TwitterURL, person.LinkedInURL);// send tempate to admin if doctor sing up in system                   
                    }
                }else
                {
                    NewUserId = -1;
                }
                
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("SignupForVeri -", Ex.Message, Ex.StackTrace);
                throw Ex;
            }
            return NewUserId;
        }
    }
}
