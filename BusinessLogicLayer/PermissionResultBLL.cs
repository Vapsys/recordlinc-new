﻿using BO.Models;
using BO.ViewModel;
using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DataAccessLayer.Common.clsCommon;

namespace BusinessLogicLayer
{

    public class PermissionResultBLL
    {


        public PermissionResult CheckPermissionAndMemberShip(int UserId, int FeaturesId, List<MemberPlanDetail> objPlanDetails, int PatientId = 0)
        {
            PermissionResult objpermission = new PermissionResult();
            MemberPlanDetail objPlanDetail = new MemberPlanDetail();
            objPlanDetail = objPlanDetails.Where(p => p.FeatureId == FeaturesId).FirstOrDefault();
            objpermission = GetPermissionResult(UserId, FeaturesId, objPlanDetail, PatientId);
            return objpermission;

        }
        public static PermissionResult GetPermissionResult(int UserId, int FeaturesId, MemberPlanDetail objplandetail, int PatientId = 0)
        {
            clsRestriction objRestriction = new clsRestriction();
            PermissionResult objpermission = new PermissionResult();
            objpermission.HasPermission = false;
            objpermission.DoesMembershipAllow = false;
            objpermission.Message = string.Empty;
            objpermission.FeatureId = FeaturesId;
            int Count = 0;
            switch (FeaturesId)
            {
                case (int)BO.Enums.Common.Features.Colleagues:
                    Count = objRestriction.GetCountOfColleaguesByUserId(UserId);
                    break;
                case (int)BO.Enums.Common.Features.New_Patient_Leads:
                    Count = objRestriction.GetCountOfPatientByUserId(UserId);
                    break;
                case (int)BO.Enums.Common.Features.Attachments:
                    Count = objRestriction.GetCountOfPatientAttachments(PatientId, UserId);
                    break;
                case (int)BO.Enums.Common.Features.OCR:
                    Count = objRestriction.CheckUserHasAccessOCR(UserId);
                    break;
            }
            objpermission.UsedCount = SafeValue<int>(Count);
            // For Unlimited Access
            if (objplandetail.CanSee == true && string.IsNullOrWhiteSpace(objplandetail.MaxCount))
            {
                objpermission.DoesMembershipAllow = true;
            }
            //Limited Access
            else if (objplandetail.CanSee == true && !string.IsNullOrWhiteSpace(objplandetail.MaxCount))
            {
                if (SafeValue<int>(objplandetail.MaxCount) > Count)
                {
                    objpermission.MaxCount = SafeValue<int>(objplandetail.MaxCount);
                    objpermission.DoesMembershipAllow = true;
                }
                else {
                    objpermission.MaxCount = SafeValue<int>(objplandetail.MaxCount);
                    objpermission.DoesMembershipAllow = false;

                }
            }
            // Can See Logic Only
            else if (objplandetail.CanSee == false)
                objpermission.DoesMembershipAllow = false;
            return objpermission;
        }
        public static string GetPermissionMessage(int FeaturesId)
        {
            string Message = string.Empty;
            switch (FeaturesId)
            {
                case (int)BO.Enums.Common.Features.Colleagues:
                    Message = BO.Constatnt.PermissionMesssage.Colleagues;
                    break;
                case (int)BO.Enums.Common.Features.New_Patient_Leads:
                    Message = BO.Constatnt.PermissionMesssage.New_Patient_Leads;
                    break;
                case (int)BO.Enums.Common.Features.Attachments:
                    Message = BO.Constatnt.PermissionMesssage.Attachments;
                    break;
                case (int)BO.Enums.Common.Features.OCR:
                    Message = BO.Constatnt.PermissionMesssage.OCR;
                    break;
            }
            return Message;
        }
        public static string GetDynamicPermissionMessage(int FeaturesId, int MaxCount, int UsedCount)
        {
            string Message = string.Empty;
            switch (FeaturesId)
            {
                case (int)BO.Enums.Common.Features.Colleagues:
                    if (UsedCount == MaxCount || UsedCount > MaxCount)
                    {
                        Message = "You have reached the maximum number of colleagues allowed with your plan. Please <a href=\"/FreeToPaid/UpgradeAccount\">click here</a> to upgrade.";
                    }
                    else {
                        Message = "You have exceeded maximum limit of " + MaxCount+ " for adding colleagues.";

                       // Message = "You have used " + UsedCount + " of " + MaxCount + " add colleague functionality.";
                    }
                    break;
                case (int)BO.Enums.Common.Features.New_Patient_Leads:
                    if (UsedCount == MaxCount || UsedCount > MaxCount)
                    {
                        Message = "You have reached the maximum number of patients for your plan. Please <a href=\"/FreeToPaid/UpgradeAccount\">click here</a> to upgrade.";
                    }
                    else { Message = "You have used " + UsedCount + " of " + MaxCount + " add patient functionality."; }
                    break;
                case (int)BO.Enums.Common.Features.Attachments:
                    if (UsedCount == MaxCount || UsedCount > MaxCount)
                    {
                        Message = "You have reached the maximum number of patient documents allowed with your plan. Please <a href=\"/FreeToPaid/UpgradeAccount\">click here</a> to upgrade.";
                    }
                    else { Message = "You have used " + UsedCount + " of " + MaxCount + " upload patient document functionality."; }
                    break;
                case (int)BO.Enums.Common.Features.OCR:
                    Message = BO.Constatnt.PermissionMesssage.OCR;
                    break;
            }
            return Message;
        }

    }
}
