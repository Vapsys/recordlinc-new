﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer;
using DataAccessLayer.Common;

namespace BusinessLogicLayer
{
    public class ErrorLogBLL
    {
        public bool LogError(Exception exc)
        {
            clsCommon obj = new clsCommon();
            bool isLogged = obj.InsertErrorLog(exc.Source, exc.Message, exc.StackTrace);
            return isLogged;
        }

    }
}
