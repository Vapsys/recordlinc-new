﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Models;
using BO.ViewModel;
using System.Data;
using System.Configuration;
using DataAccessLayer.Common;
using DataAccessLayer.PatientsData;
using System.Data.SqlClient;
using  static DataAccessLayer.Common.clsCommon;
using DataAccessLayer.ColleaguesData;

namespace BusinessLogicLayer
{
    public class PatientReport
    {
        public List<PatientReward> GetPatientList(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirection)
        {
            try
            {
                DataTable dt = new DataTable();
                clsPatientsData dal = new clsPatientsData();
                List<PatientReward> Lst = new List<PatientReward>();
                dt = dal.GetPatientRewardsReport(UserId, PageIndex, PageSize, null, SortColumn, SortDirection, null, 0, 0);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        Lst.Add(new PatientReward()
                        {
                            PatientId = (SafeValue<int>(item["PatientId"]) > 0) ? SafeValue<int>(item["PatientId"]) : 0,
                            PatientName = SafeValue<string>(item["PatientName"]),
                            OwnerId = (SafeValue<int>(item["OwnerId"]) > 0) ? SafeValue<int>(item["OwnerId"]) : 0,
                            DOB = SafeValue<string>(item["DOB"]),
                            CountOfReferral = (SafeValue<int>(item["TotalMessages"]) > 0) ? SafeValue<int>(item["TotalMessages"]) : 0,
                            CountOfPlan = (SafeValue<int>(item["TotalPlan"]) > 0) ? SafeValue<int>(item["TotalPlan"]) : 0
                        });
                    }
                }
                return Lst;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public List<OneClickReferralDetails> GetOneClickReferralHistoryById(ReferralFilter FilterObj)
        {
            List<OneClickReferralDetails> lst = new List<OneClickReferralDetails>();
            DataTable dt = new DataTable();
            clsCommon objCommon = new clsCommon();
            clsPatientsData dal = new clsPatientsData();
            dt = dal.GetOneClickReferralHistory(FilterObj, FilterObj.UserId);
            List<Disposition> DispositionList = new List<Disposition>();
            DispositionList = GetDispositionlist();
            string ImageName = string.Empty;
            foreach (DataRow item in dt.Rows)
            {
                ImageName = SafeValue<string>(item["ProfileImage"]);
                string CreationDate = SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(item["ReferralDate"]), ""));
                lst.Add(new OneClickReferralDetails()
                {
                    ReferralDate = !string.IsNullOrEmpty(CreationDate) ? new CommonBLL().ConvertToDateNew(SafeValue<DateTime>(SafeValue<DateTime>(CreationDate)), SafeValue<int>(BO.Enums.Common.DateFormat.GENERAL)) : string.Empty,
                    PatientName = SafeValue<string>(item["PatientName"]),
                    DoctorName = SafeValue<string>(item["DoctorName"]),
                    //Added for OCR dashboard
                    ReferringDoctorPhone = SafeValue<string>(item["ReferringDoctorPhone"]),
                    ReferringDoctorEmail = SafeValue<string>(item["ReferringDoctorEmail"]),
                    MessageBody = SafeValue<string>(item["Body"]),
                    //Added for OCR dashboard
                    PatientId = SafeValue<int>(item["PatientId"]),
                    ReceiverId = SafeValue<int>(item["ReceiverId"]),
                    MessageId = SafeValue<int>(item["MessageId"]),
                    Email = SafeValue<string>(item["Email"]),
                    PhoneNumber = SafeValue<string>(item["PhoneNumber"]),
                    DOB = SafeValue<string>(item["DateOfBirth"]),
                    DentrixId = SafeValue<string>(item["DentrixId"]),
                    DispositionId = SafeValue<int>(item["Dispositionstatus"]),
                    //ProfileImage =  SafeValue<string>(item["ProfileImage"]),
                    ProfileImage =SafeValue<string>(item["ProfileImage"]) != string.Empty ? SafeValue<string>(ConfigurationManager.AppSettings["ImageBank"]) + SafeValue<string>(item["ProfileImage"]) : "",
                    DispositionList = DispositionList,
                    DoctorId = SafeValue<int>(item["DoctorId"]),
                    Gender = SafeValue<string>(item["Gender"]) == "0" ? "Male" : "Female",
                    MessageStatus = SafeValue<string>(item["MessageStatus"]),
                    MessageFrom = SafeValue<string>(item["MessageFrom"])
                });
            }
            foreach (var items in lst)
            {

                if (SafeValue<string>(items.Gender) == "Female")
                {
                    items.ProfileImage =SafeValue<string>(System.Configuration.ConfigurationManager.AppSettings.Get("DefaultDoctorFemaleImage"));
                }
                else
                {
                    items.ProfileImage =SafeValue<string>(System.Configuration.ConfigurationManager.AppSettings.Get("DefaultDoctorImage"));
                }
            }

            return lst;
        }
        public List<Disposition> GetDispositionlist()
        {
            List<Disposition> lst = new List<Disposition>();
            DataTable dt = new DataTable();
            clsPatientsData dal = new clsPatientsData();
            dt = dal.GetDispositionlist();
            foreach (DataRow item in dt.Rows)
            {
                lst.Add(new Disposition()
                {
                    Id = SafeValue<int>(item["Id"]),
                    Status = SafeValue<string>(item["Status"])
                });
            }
            return lst;
        }

        public static bool UpdateDispositionStatus(int Disposition, int MessageId, int UserId, string URL=null, string Note = null, bool VisibleToPatient =false, bool VisibleToDoctor =true,string ScheduledDate ="",string TimeZoneOfDentist ="")
        {
            try
            {  // set note values for Email "change-disposition-status-to-scheduled-patient"

                string strNote = Note;

                DateTime? Scheduled_Date = null;
                // jira 887 change
                if (Disposition == 7)
                {
                    if (!string.IsNullOrWhiteSpace(ScheduledDate))
                    {
                        Scheduled_Date = clsHelper.ConvertToUTC(Convert.ToDateTime(ScheduledDate), TimeZoneOfDentist);
                        Note += " Scheduled Date : [Scheduled_Date]";
                    }
                }
                clsPatientsData dal = new clsPatientsData();
                //Unread functionality
                if (MessageId > 0)
                {
                    clsColleaguesData.UpdateMessageStatus(MessageId, UserId);
                }
                int Result = dal.UpdateDispositionStatus(Disposition, MessageId, UserId, Note, VisibleToPatient, VisibleToDoctor, Scheduled_Date);
                if (Result > 0 && (Disposition == 7 || Disposition == 8 || Disposition == 9))
                {
                    MailContain Obj = SendSMSandMail(Result, URL);
                    if (Disposition != 7)
                    {
                        new clsTemplate().SendEmailForDispositionStatusChange(Obj);
                    }else
                    {
                        new clsTemplate().SendEmailForDispositionStatusToScheduled(Obj, strNote, ScheduledDate,  VisibleToPatient);
                    }

                }
                return Result > 0 ? true : false;
            }
            catch (Exception exc)
            {
                new clsCommon().InsertErrorLog("PatientReport in BLL -- UpdateDispositionStatus", "RLAPI: " + exc.Message, exc.StackTrace);
                throw;
            }
            
        }
        public List<OneClickReferralDetails> SearchbyFilter(string DoctorName, DateTime? FromDate, DateTime? ToDate, int? Disposition, int UserId)
        {
            List<OneClickReferralDetails> lst = new List<OneClickReferralDetails>();
            DataTable dt = new DataTable();
            clsCommon objCommon = new clsCommon();
            clsPatientsData dal = new clsPatientsData();
            dt = dal.GetSearchByFilter(DoctorName, FromDate, ToDate, Disposition, UserId);
            List<Disposition> DispositionList = new List<Disposition>();
            DispositionList = GetDispositionlist();
            string ImageName = string.Empty;
            foreach (DataRow item in dt.Rows)
            {
                ImageName = SafeValue<string>(item["ProfileImage"]);
                lst.Add(new OneClickReferralDetails()
                {
                    ReferralDate = !string.IsNullOrEmpty(SafeValue<string>(item["ReferralDate"])) ? new CommonBLL().ConvertToDateTime(SafeValue<DateTime>(SafeValue<DateTime>(item["ReferralDate"])), SafeValue<int>(BO.Enums.Common.DateFormat.GENERAL)) : string.Empty,
                    PatientName = SafeValue<string>(item["PatientName"]),
                    DoctorName = SafeValue<string>(item["DoctorName"]),
                    PatientId = SafeValue<int>(item["PatientId"]),
                    ReceiverId = SafeValue<int>(item["ReceiverId"]),
                    MessageId = SafeValue<int>(item["MessageId"]),
                    Email = SafeValue<string>(item["Email"]),
                    PhoneNumber = SafeValue<string>(item["PhoneNumber"]),
                    DOB = SafeValue<string>(item["DateOfBirth"]),
                    DentrixId = SafeValue<string>(item["DentrixId"]),
                    DispositionId = SafeValue<int>(item["Dispositionstatus"]),
                    ProfileImage = SafeValue<string>(item["ProfileImage"]) != string.Empty ? SafeValue<string>(ConfigurationManager.AppSettings["ImageBank"]) + SafeValue<string>(item["ProfileImage"]) : "",
                    DispositionList = DispositionList
                });
            }
            foreach (var items in lst)
            {
                if (string.IsNullOrEmpty(items.ProfileImage))
                {
                    items.ProfileImage = SafeValue<string>(ConfigurationManager.AppSettings["DefaultDoctorImage"]);
                }
                else
                {
                    items.ProfileImage =SafeValue<string>(ConfigurationManager.AppSettings["DoctorImage"]) + items.ProfileImage;
                }
            }

            return lst;
        }
        public static DataTable SearchdoctorName(string doctorName)
        {
            clsHelper ObjHelper = new clsHelper();
            DataTable dt = new DataTable();
            try
            {
                SqlParameter[] sp = new SqlParameter[1];
                sp[0] = new SqlParameter("@doctorName", SqlDbType.VarChar);
                sp[0].Value = doctorName;
                dt = ObjHelper.DataTable("USP_SearchDoctorName", sp);

            }
            catch (Exception)
            {
                throw;
            }
            return dt;
        }
        public static DataTable SearchPatientName(string PatietName)
        {
            try
            {
                SqlParameter[] sp = new SqlParameter[1];
                sp[0] = new SqlParameter("@PatientName", SqlDbType.VarChar);
                sp[0].Value = PatietName;
                return new clsHelper().DataTable("Usp_GetPatientNameForOCR", sp);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientReport--SearchPatientName", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public List<OneClickReferralDetails> GetOneClickReferralHistory(ReferralFilter FilterObj)
        {
            List<OneClickReferralDetails> lst = new List<OneClickReferralDetails>();
            DataTable dt = new DataTable();
            clsCommon objCommon = new clsCommon();
            clsPatientsData dal = new clsPatientsData();
            DataTable Dt = clsCommon.GetDentistDetailsByAccessToken(FilterObj.access_token);
            if (Dt != null && Dt.Rows.Count > 0)
            {
                dt = dal.GetOneClickReferralHistory(FilterObj, SafeValue<int>(Dt.Rows[0]["UserId"]));
                string ImageName = string.Empty;
                foreach (DataRow item in dt.Rows)
                {
                    ImageName = SafeValue<string>(item["ProfileImage"]);
                    string CreationDate = SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(item["ReferralDate"]), ""));
                    lst.Add(new OneClickReferralDetails()
                    {
                        ReferralDate = !string.IsNullOrEmpty(CreationDate) ? new CommonBLL().ConvertToDateNew(SafeValue<DateTime>(SafeValue<DateTime>(CreationDate)), SafeValue<int>(BO.Enums.Common.DateFormat.GENERAL)) : string.Empty,
                        PatientName = SafeValue<string>(item["PatientName"]),
                        DoctorName = SafeValue<string>(item["DoctorName"]),
                        //Added for OCR dashboard
                        ReferringDoctorPhone = SafeValue<string>(item["ReferringDoctorPhone"]),
                        ReferringDoctorEmail = SafeValue<string>(item["ReferringDoctorEmail"]),
                        MessageBody = SafeValue<string>(item["Body"]),
                        //Added for OCR dashboard
                        PatientId = SafeValue<int>(item["PatientId"]),
                        ReceiverId = SafeValue<int>(item["ReceiverId"]),
                        MessageId = SafeValue<int>(item["MessageId"]),
                        Email = SafeValue<string>(item["Email"]),
                        PhoneNumber = SafeValue<string>(item["PhoneNumber"]),
                        DOB = SafeValue<string>(item["DateOfBirth"]),
                        DentrixId = SafeValue<string>(item["DentrixId"]),
                        DispositionId = SafeValue<int>(item["Dispositionstatus"]),
                        //ProfileImage =  SafeValue<string>(item["ProfileImage"]),
                        ProfileImage =SafeValue<string>(item["ProfileImage"])!= string.Empty ? SafeValue<string>(ConfigurationManager.AppSettings["ImageBank"]) + SafeValue<string>(item["ProfileImage"]) : "",
                        DispositionList = GetDispositionlist(),
                        DoctorId = SafeValue<int>(item["DoctorId"]),
                        Gender = SafeValue<string>(item["Gender"]) == "0" ? "Male" : "Female",
                        MessageStatus = SafeValue<string>(item["MessageStatus"]),
                        MessageFrom = SafeValue<string>(item["MessageFrom"])
                    });
                }
                foreach (var items in lst)
                {
                    if (SafeValue<string>(items.Gender) == "Female")
                    {
                        items.ProfileImage =SafeValue<string>(System.Configuration.ConfigurationManager.AppSettings.Get("DefaultDoctorFemaleImage"));
                    }
                    else
                    {
                        items.ProfileImage = SafeValue<string>(System.Configuration.ConfigurationManager.AppSettings.Get("DefaultDoctorImage"));
                    }
                }
            }
            else
            {
                lst = null;
            }
            return lst;
        }

        public static MailContain SendSMSandMail(int MessageId,string URL)
        {
            try
            {
                MailContain Obj = new MailContain();
                DataTable Dt = clsPatientsData.GetMessageDetailsForSMSandEmail(MessageId);
                if(Dt != null && Dt.Rows.Count > 0)
                {
                    Obj.SendarId = SafeValue<int>(Dt.Rows[0]["SenderId"]);
                    Obj.SendarName = SafeValue<string>(Dt.Rows[0]["SendarFullName"]);
                    Obj.SendarEmail = SafeValue<string>(Dt.Rows[0]["sendaremail"]);
                    Obj.SendarPhone = SafeValue<string>(Dt.Rows[0]["SenderPhone"]);

                    Obj.ReceiverId = SafeValue<int>(Dt.Rows[0]["ReferedUserId"]);
                    Obj.ReceiverName = SafeValue<string>(Dt.Rows[0]["ReceiverFullName"]);
                    Obj.ReceiverEmail = SafeValue<string>(Dt.Rows[0]["recevieremail"]);
                    Obj.ReceiverPhone = SafeValue<string>(Dt.Rows[0]["ReceiverPhone"]);

                    Obj.PatientId = SafeValue<int>(Dt.Rows[0]["PatientId"]);
                    Obj.PatientName = SafeValue<string>(Dt.Rows[0]["PatientFullName"]);
                    Obj.PatientEmail = SafeValue<string>(Dt.Rows[0]["Email"]);
                    Obj.PatientPhone = SafeValue<string>(Dt.Rows[0]["WorkPhone"]);

                    Obj.Body = SafeValue<string>(Dt.Rows[0]["Body"]);
                    Obj.Status = SafeValue<string>(Dt.Rows[0]["Status"]);
                    Obj.Address = SafeValue<string>(Dt.Rows[0]["Addresses"]);
                    Obj.FacebookUrl  = SafeValue<string>(Dt.Rows[0]["FacebookUrl"]);
                    Obj.LinkedinUrl = SafeValue<string>(Dt.Rows[0]["LinkedinUrl"]);
                    Obj.TwitterUrl = SafeValue<string>(Dt.Rows[0]["TwitterUrl"]);
                    Obj.GoogleplusUrl = SafeValue<string>(Dt.Rows[0]["GoogleplusUrl"]);
                    Obj.YoutubeUrl = SafeValue<string>(Dt.Rows[0]["YoutubeUrl"]);
                    Obj.PinterestUrl = SafeValue<string>(Dt.Rows[0]["PinterestUrl"]);
                    Obj.BlogUrl = SafeValue<string>(Dt.Rows[0]["BlogUrl"]);
                    Obj.YelpUrl = SafeValue<string>(Dt.Rows[0]["YelpUrl"]);
                    Obj.InstagramUrl = SafeValue<string>(Dt.Rows[0]["InstagramUrl"]);
                    Obj.URL = URL;
                }
                return Obj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientReport--SendSMSandMail", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static bool ChangeCoordinator(Coordinator objCoordinate)
        {
            try
            {
                clsPatientsData dal = new clsPatientsData();
                bool Result = dal.ChangeCoordinator(objCoordinate);

                return Result;
            }
            catch (Exception exc)
            {
                new clsCommon().InsertErrorLog("PatientReport in BLL -- ChangeCoordinator", "RLAPI: " + exc.Message, exc.StackTrace);
                throw;
            }

        }
    }
}
