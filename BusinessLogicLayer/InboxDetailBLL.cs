﻿using BO.ViewModel;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using static DataAccessLayer.Common.clsCommon;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class InboxDetailBLL
    {
        public InboxDetailBLL() { }
        clsColleaguesData objColleaguesData = new clsColleaguesData();
        clsCommon objCommon = new clsCommon();
        public List<InboxMessages> GetDraftofDoctor(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext, bool IsReferral, string TimeZone)
        {
            try
            {
                List<InboxMessages> lstDraftsOfDoctor = new List<InboxMessages>();
                DataTable dt = new DataTable();
                dt = objColleaguesData.GetDraftMessagesofDoctor(UserId, PageIndex, PageSize, SortColumn, SortDirection, Searchtext, IsReferral);
               

                if (dt != null && dt.Rows.Count > 0)
                {
                    DateTime CurrentDate = clsHelper.ConvertFromUTC(SafeValue<DateTime>(DateTime.Now.ToString()), TimeZone);
                    foreach (DataRow row in dt.Rows)
                    {
                        string BOdy = System.Uri.UnescapeDataString(SafeValue<string>(row["Body"]));
                        lstDraftsOfDoctor.Add(new InboxMessages()
                        {
                            MessageId = SafeValue<int>(row["MessageId"]),
                            Field =SafeValue<string>(row["ToField"]),
                            Body = Regex.Replace(Regex.Replace(objCommon.RemoveHTML(SafeValue<string>(BOdy)), @"<[^>]+>|&nbsp;", string.Empty), @"[\r\n\t]+", string.Empty),
                            CreationDate = clsHelper.ConvertFromUTC(SafeValue<DateTime>(row["CreationDate"]), TimeZone),
                            MessageTypeId = SafeValue<int>(row["MessageTypeId"]),
                            AttachmentId = SafeValue<string>(row["AttachmentId"]),
                            PatientName =SafeValue<string>(row["PatientName"]), 
                            TotalRecord = SafeValue<int>(row["TotalRecord"]),
                            MessageDisplayType = SafeValue<int>(row["MessageDisplayType"]),
                            IsColleagueMesage = SafeValue<int>(row["IsColleagueMesage"]),
                            CurrentDate = CurrentDate
                        });
                    }

                }
                return lstDraftsOfDoctor;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("DraftDetailBLL--GetDraftofDoctor", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool DeleteDraftMessage(string ColleagueMessageId, string PatientMessageId)
        {
            return objColleaguesData.DeleteDraftAttachments(ColleagueMessageId, PatientMessageId);
        }
        public bool DeleteInboxMessage(string PatientMessageId, string ColleagueMessageId, int UserId)
        {
            return objColleaguesData.DeleteInboxMessages(PatientMessageId, ColleagueMessageId, UserId);
        }
        public bool DeleteSentBoxMessage(string PatientMessageId, string ColleagueMessageId, int UserId)
        {
            return objColleaguesData.DeleteSentBoxMessages(PatientMessageId, ColleagueMessageId, UserId);
        }
        public List<InboxMessages> GetInboxOfDoctor(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirction, string SearchText, bool IsReferral, string TimeZone)
        {
            try
            {
                List<InboxMessages> lstInboxOfDoctor = new List<InboxMessages>();
                DataTable dt = new DataTable();
                string isreferral = string.Empty;
                if(!IsReferral)
                {
                    isreferral = null;
                }
                dt = objColleaguesData.InboxOfDoctor(UserId, PageIndex, PageSize, SortColumn, SortDirction, SearchText, SafeValue<string>(isreferral));
                if (dt != null && dt.Rows.Count > 0)
                {
                    DateTime CurrentDate = clsHelper.ConvertFromUTC(SafeValue<DateTime>(DateTime.Now.ToString()), TimeZone);
                    foreach (DataRow row in dt.Rows)
                    {
                        string Body = Uri.UnescapeDataString(SafeValue<string>(row["Body"]));
                        Body = Regex.Replace(Body, @"<[^>]+>|&nbsp;", "").Trim();
                        lstInboxOfDoctor.Add(new InboxMessages()
                        {
                            MessageId = SafeValue<int>(row["MessageId"]),
                            PatientName = SafeValue<string>(row["PatientName"]),
                            Field =SafeValue<string>(row["FromField"]), 
                            Body = ((SafeValue<string>(row["MessageTypeId"]) == "2") ? "Patient Referral For " + (SafeValue<string>(row["PatientName"])) : Body),//Regex.Replace(Regex.Replace(objCommon.RemoveHTML(objCommon.CheckNull(Body, string.Empty)), @"<[^>]+>|&nbsp;", string.Empty), @"[\r\n\t]+", string.Empty),
                            CreationDate = clsHelper.ConvertFromUTC(SafeValue<DateTime>(row["CreationDate"]), TimeZone),
                            TotalRecord = SafeValue<int>(row["TotalRecord"]),
                            MessageTypeId = SafeValue<int>(row["MessageTypeId"]),
                            AttachmentId = SafeValue<string>(row["AttachmentId"]),
                            IsRead = SafeValue<bool>(SafeValue<string>(row["IsRead"])),
                            MessageDisplayType = SafeValue<int>(row["MessageDisplayType"]),
                            IsColleagueMesage = SafeValue<int>(row["IsColleagueMesage"]),
                            CurrentDate = CurrentDate
                        });
                    }
                }
                #region Resolve Ticket - NSM-27
                if (SortColumn == 3)
                {
                    if(SortDirction == 1)
                    lstInboxOfDoctor = lstInboxOfDoctor.OrderBy(m => m.Body).ToList();
                    else
                        lstInboxOfDoctor = lstInboxOfDoctor.OrderByDescending(m => m.Body).ToList();
                }
                #endregion
                return lstInboxOfDoctor;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("InboxDetailBLL--GetInboxOfDoctor", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public List<InboxMessages> GetSentBoxOfDoctor(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirction, string SearchText, bool IsReferral, string TimeZone)
        {
            try
            {
                string GetSentboxofDoctor = "Start is :-" + DateTime.Now;
                List<InboxMessages> lstInboxOfDoctor = new List<InboxMessages>();
                DataTable dt = new DataTable();
                dt = objColleaguesData.SentboxOfDoctor(UserId, PageIndex, PageSize, SortColumn, SortDirction, SearchText, SafeValue<string>(IsReferral));
                if (dt != null && dt.Rows.Count > 0)
                {
                    DateTime CurrentDate = clsHelper.ConvertFromUTC(SafeValue<DateTime>(DateTime.Now.ToString()), TimeZone);
                    foreach (DataRow row in dt.Rows)
                    {
                        //HtmlDocument htmlDoc = new HtmlDocument();
                        //htmlDoc.LoadHtml(SafeValue<string>(row["Body"]));
                        //string Message = htmlDoc.DocumentNode.InnerText;
                        string BOdy = System.Uri.UnescapeDataString(SafeValue<string>(row["Body"]));
                        BOdy = Regex.Replace(BOdy, @"<[^>]+>|&nbsp;", "").Trim();
                        //Html.fromHtml(htmltext).toString().replaceAll("\n", "").trim()
                        DateTime CreationDate = DateTime.MinValue;
                        if (!string.IsNullOrEmpty(SafeValue<string>(row["CreationDate"])))
                            CreationDate = clsHelper.ConvertFromUTC(SafeValue<DateTime>(row["CreationDate"]), TimeZone);
                        lstInboxOfDoctor.Add(new InboxMessages()
                        {
                            StartTime = SafeValue<string>(DateTime.Now),
                            MessageId = SafeValue<int>(row["MessageId"]),
                            MessageTypeId = SafeValue<int>(row["MessageTypeId"]),
                            Field = SafeValue<string>(row["ToField"]), 
                            Body = ((SafeValue<string>(row["MessageTypeId"])=="2")? "Patient Referral For "+ (SafeValue<string>(row["PatientName"])) : BOdy),
                            CreationDate = CreationDate,
                            TotalRecord = SafeValue<int>(row["TotalRecord"]),
                            AttachmentId =SafeValue<string>(row["AttachmentId"]),
                            PatientName =SafeValue<string>(row["PatientName"]), 
                            MessageDisplayType = SafeValue<int>(row["MessageDisplayType"]),
                            IsColleagueMesage = SafeValue<int>(row["IsColleagueMesage"]),
                            IsRead = SafeValue<bool>(SafeValue<string>(row["IsRead"])),
                            EndTime = SafeValue<string>(DateTime.Now),
                            CurrentDate = CurrentDate
                        });
                    }
                }
                return lstInboxOfDoctor;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("InboxDetailBLL--GetSentBoxOfDoctor", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public List<InboxMessages> GetReceiveReferralOfDoctor(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirction, string SearchText, string TimeZone)
        {
            try
            {
                List<InboxMessages> lstInboxOfDoctor = new List<InboxMessages>();
                DataTable dt = new DataTable();
                dt = objColleaguesData.GetReceivedReferredPatientList(UserId, PageIndex, PageSize, SortColumn, SortDirction, SearchText);
                if (dt != null && dt.Rows.Count > 0)
                {
                    DateTime CurrentDate = clsHelper.ConvertFromUTC(SafeValue<DateTime>(DateTime.Now.ToString()), TimeZone);
                    foreach (DataRow item in dt.Rows)
                    {
                        DateTime CreationDate = DateTime.MinValue;
                        if (!string.IsNullOrEmpty(SafeValue<string>(item["CreationDate"])))
                            CreationDate = clsHelper.ConvertFromUTC(SafeValue<DateTime>(item["CreationDate"]), TimeZone);

                        lstInboxOfDoctor.Add(new InboxMessages()
                        {
                            MessageId = SafeValue<int>(item["MessageId"]),
                            PatientId = SafeValue<int>(item["PatientId"]),
                            SenderId = SafeValue<int>(item["SenderId"]),
                            ReceiveId = SafeValue<int>(item["ReferedUserId"]),
                            Field = SafeValue<string>(item["SenderName"]),
                            ReceiverName = SafeValue<string>(item["ReceiverName"]),
                            PatientName = SafeValue<string>(item["PatientName"]),
                            Body = "Patient Referral For " + SafeValue<string>(item["PatientName"]),
                            CreationDate = CreationDate,
                            MessageDisplayType = SafeValue<int>(item["MessageDisplayType"]),
                            TotalRecord = SafeValue<int>(item["TotalRecord"]),
                            MessageTypeId = 2,
                            IsColleagueMesage = SafeValue<int>(item["IsColleagueMesage"]),
                            IsRead = SafeValue<bool>(item["IsRead"]),
                            CurrentDate = CurrentDate
                        });
                    }
                }
                return lstInboxOfDoctor;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("InboxDetailBLL--GetReceiveReferralOfDoctor", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public List<InboxMessages> GetSentReferralOfDoctor(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirction, string SearchText, string TimeZone)
        {
            try
            {
                List<InboxMessages> lstInboxOfDoctor = new List<InboxMessages>();
                DataTable dt = new DataTable();
                dt = objColleaguesData.GetSendReferredPatientList(UserId, PageIndex, PageSize, SortColumn, SortDirction, SearchText);
                if (dt != null && dt.Rows.Count > 0)
                {
                    DateTime CurrentDate = clsHelper.ConvertFromUTC(SafeValue<DateTime>(DateTime.Now.ToString()), TimeZone);
                    foreach (DataRow item in dt.Rows)
                    {
                        DateTime CreationDate = DateTime.MinValue;
                        if (!string.IsNullOrEmpty(SafeValue<string>(item["CreationDate"])))
                            CreationDate = clsHelper.ConvertFromUTC(SafeValue<DateTime>(item["CreationDate"]), TimeZone);

                        lstInboxOfDoctor.Add(new InboxMessages()
                        {
                            MessageId = SafeValue<int>(item["MessageId"]),
                            PatientId = SafeValue<int>(item["PatientId"]),
                            SenderId = SafeValue<int>(item["SenderId"]),
                            ReceiveId = SafeValue<int>(item["ReferedUserId"]),
                            SenderName = SafeValue<string>(item["SenderName"]),
                            Field = SafeValue<string>(item["ReceiverName"]),
                            PatientName = SafeValue<string>(item["PatientName"]),
                            Body = "Patient Referral For " + SafeValue<string>(item["PatientName"]),
                            CreationDate = CreationDate,
                            MessageDisplayType = SafeValue<int>(item["MessageDisplayType"]),
                            TotalRecord = SafeValue<int>(item["TotalRecord"]),
                            MessageTypeId = 2,
                            IsColleagueMesage = SafeValue<int>(item["IsColleagueMesage"]),
                            IsRead = SafeValue<bool>(item["IsRead"]),
                            CurrentDate = CurrentDate
                        });
                    }
                }
                return lstInboxOfDoctor;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("InboxDetailBLL--GetSentReferralOfDoctor", Ex.Message, Ex.StackTrace);
                throw;
            }
        }


        //Inbox
        public  static List<InboxMessages> NewGetInboxOfDoctorGrid(int UserId, int PageIndex, int SortColumn, int SortDirection, string Searchtext, string IsRefarral, string timezone)
        {
            List<InboxMessages> lst = new List<InboxMessages>();
            InboxDetailBLL objinbox = new InboxDetailBLL();
            lst = objinbox.GetInboxOfDoctor(UserId, PageIndex, 10, SortColumn, SortDirection, Searchtext, false, timezone);
            return lst;
        }

    }
}
