﻿using BO.Models;
using DataAccessLayer;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using System;
using System.Data;
using BO.Models;
using System;
using static DataAccessLayer.Common.clsCommon;
using System.Collections.Generic;
using BO.ViewModel;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;

namespace BusinessLogicLayer
{
    public class DentistBLL
    {
        public static bool IsDentistIntegrationPartner(int UserId)
        {
            return DentistDAL.IsDentistIntegrationPartner(UserId);
        }

        public static DataTable GetDentistDetailByUserId(int UserId)
        {
            return clsColleaguesData.GetUserFullDetailsById(UserId);
        }
        public static bool SaveReferralStatusOfUser(OCR_ReferralStatus _ReferralStatus)
        {
            try
            {
                return DentistDAL.SaveReferralStatusOfUser(_ReferralStatus);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("DentistBLL--SaveReferralStatusOfUser", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static int GetDentistIdByAccessToken(string Token)
        {
            try
            {
                int UserId = 0;
                DataTable Dts = clsCommon.GetDentistDetailsByAccessToken(Token);
                if (Dts.Rows.Count > 0)
                {
                    UserId = SafeValue<int>(Dts.Rows[0]["UserId"]);
                }
                return UserId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("DentistBLL--SaveReferralStatusOfUser", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool CheckUserHasEnableOCR(int UserID)
        {
            try
            {
                DataTable Dt = clsColleaguesData.CheckUserHasEnableOneClick(UserID);
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    bool sent, receive;
                    sent = SafeValue<bool>(Dt.Rows[0]["IsSendReferral"]);
                    receive = SafeValue<bool>(Dt.Rows[0]["IsReceiveReferral"]);
                    return (sent || receive) ? true : false;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("DentistBLL--CheckUserHasEnableOCR", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool DeleteColleagues(DeleteColleagues Obj, int UserId)
        {
            try
            {
                bool Result = false;
                object obj = "";
                string[] ColleagueIds = Obj.ColleaguesId.Split(',');
                foreach (string ColleagueId in ColleagueIds)
                {
                    Result = new clsColleaguesData().RemoveColleagues(UserId, SafeValue<int>(ColleagueId));
                }
                return Result;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("DentistBLL--DeleteColleagues", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static List<SearchColleagueViewModel> SearchColleagues(FilterSearchColleagues Obj, int UserId)
        {
            try
            {
                List<SearchColleagueViewModel> lst1 = new List<SearchColleagueViewModel>();
                DataTable dt = new DataTable();
                dt = new clsColleaguesData().GetSearchColleagueForDoctor(UserId, Obj.PageIndex, Obj.PageSize, clsCommon.SafeSearch(Obj.Name), clsCommon.SafeSearch(Obj.Email), clsCommon.SafeSearch(Obj.Phone), clsCommon.SafeSearch(Obj.City), Obj.State, Obj.Zipcode, Obj.Institute, clsCommon.SafeSearch(Obj.SpecialityList), clsCommon.SafeSearch(Obj.Keywords));
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        string ImagPath = string.Empty;
                        if (string.IsNullOrWhiteSpace(SafeValue<string>(item["ImageName"])))
                        {
                            ImagPath = SafeValue<string>(ConfigurationManager.AppSettings["DefaultDoctorImage"]);
                        }
                        else
                        {
                            ImagPath = SafeValue<string>(ConfigurationManager.AppSettings["DoctorImage"]) + SafeValue<string>(item["ImageName"]);
                        }
                        decimal Miles = 0;
                        if (string.IsNullOrWhiteSpace(SafeValue<string>(item["Miles"])))
                        {
                            Miles = 0;
                        }
                        else
                        {
                            Miles = SafeValue<decimal>(item["Miles"]);
                        }
                        lst1.Add(new SearchColleagueViewModel()
                        {
                            UserId = SafeValue<int>(item["UserId"]),
                            FirstName = SafeValue<string>(item["FirstName"]),
                            LastName = SafeValue<string>(item["LastName"]),
                            ImageName = ImagPath,
                            ExactAddress = SafeValue<string>(item["ExactAddress"]),
                            Address2 = SafeValue<string>(item["Address2"]),
                            City = SafeValue<string>(item["City"]),
                            State = SafeValue<string>(item["State"]),
                            Miles = Miles,
                            Specialities = SafeValue<string>(item["Specialities"]),
                            OfficeName = SafeValue<string>(item["OfficeName"]),
                            TotalRecord = SafeValue<int>(item["TotalRecord"]),
                            ZipCode = SafeValue<string>(item["ZipCode"]),
                            Email = SafeValue<string>(item["Username"]),
                            Phone = SafeValue<string>(item["Phone"])
                        });
                    }
                }
                return lst1;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("DentistBLL--SearchColleagues", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static bool AddColleagues(AddColleagues Obj,int UserId)
        {
            try
            {
                 bool status =  new clsColleaguesData().AddAsColleague(UserId, SafeValue<int>(Obj.ColleaguesId));
                 new clsTemplate().ColleagueInviteDoctor(UserId, SafeValue<int>(Obj.ColleaguesId), "");
                 return status;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("DentistBLL--AddColleagues", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static List<SelectListItem> GetSpeacilities(string SpecialtyId =null)
        {
            try
            {
                List<SelectListItem> lst = new List<SelectListItem>();
                DataTable dataTable = new clsCommon().GetAllSpeciality();
                if (!string.IsNullOrWhiteSpace(SpecialtyId))
                {
                    string[] arrSpecialitiId = SpecialtyId.Split(',');
                    foreach (DataRow item in dataTable.Rows)
                    {
                        lst.Add(new SelectListItem { Text = SafeValue<string>(item["Description"]), Value = SafeValue<string>(item["SpecialityId"]),Selected = arrSpecialitiId.Contains(SafeValue<string>(item["SpecialityId"]))?true:false });
                    }
                }
                else
                {
                    foreach (DataRow item in dataTable.Rows)
                    {
                        lst.Add(new SelectListItem { Text = SafeValue<string>(item["Description"]), Value = SafeValue<string>(item["SpecialityId"]) });
                    }
                }
                
                return lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("DentistBLL--GetSpeacilities", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
