﻿using BO.Models;
using DataAccessLayer.PatientsData;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class PatientFileBLL
    {
        private readonly clsPatientFileData _data;
        public PatientFileBLL(clsPatientFileData data)
        {
            _data = data;
        }
        public void CreateMatrix(FileModel model)
        {
            _data.CreateMatrix(model);
        }

        public string FileUpload(FileModel model)
        {
            return _data.UploadFile(model);
        }
        public string FileUploadForConversation(FileModel model)
        {
            return _data.UploadFileForConversation(model);
        }

        public FileListModel Files(int recordId, string folderId, int patientId)
        {
            return _data.Files(recordId, folderId, patientId);
        }

        public void Delete(string fileId)
        {
            _data.Delete(fileId);
        }

        public FileModel Download(string fileId)
        {
            return _data.Download(fileId);
        }
    }
}
