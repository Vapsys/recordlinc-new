﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Models;
using DataAccessLayer.PlanTreatment;
using DataAccessLayer.Common;
using DataAccessLayer.PatientsData;
using BO.ViewModel;
using DataAccessLayer.ColleaguesData;
using System.Web;
using System.Web.Mvc;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public class PlanTreatmentBLL
    {

        public static clsCommon objclsCommon = new clsCommon();
        public static clsPatientsData ObjPatientData = new clsPatientsData();
        public static clsColleaguesData ObjColleagueData = new clsColleaguesData();
        public static clsPatientsData ObjPatientsData = new clsPatientsData();

        public static List<CaseSteps> GetAllCaseDefaultSteps(int UserId)
        {
            DataTable dt = new DataTable();
            try
            {
                List<CaseSteps> lst = new List<CaseSteps>();
                dt = PlanTreatmentDAL.GetAllCaseDefalutStepsList();
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        lst.Add(new CaseSteps
                        {
                            CaseId = SafeValue<int>(dt.Rows[i]["CaseDefaultId"]),
                            StepOrder = SafeValue<int>(dt.Rows[i]["StepOrderNo"]),
                            Description = SafeValue<string>(dt.Rows[i]["Description"]),
                            UserId = 0,
                        });
                    }
                    return lst;
                }
                else
                {
                    return lst;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static List<SelectListItem> GetColleagueList(string selected)
        {
            try
            {
                if (selected == "0")
                    selected = "";
                List<SelectListItem> objcolleagueList = new List<SelectListItem>();
                DataTable dt = new DataTable();
                dt = ObjColleagueData.GetColleaguesListForAutoComplete(SafeValue<int>(HttpContext.Current.Session["UserId"]),null,null);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                                        {
                        string Id = SafeValue<string>(item["ColleagueId"]);
                        SelectListItem selectitm = new SelectListItem();
                        if (selected.Split(',').Contains(Id))
                        {
                            selectitm.Value = Id;
                            selectitm.Text = SafeValue<string>(item["doctorname"]) + (!string.IsNullOrEmpty(SafeValue<string>(item["AccountName"])) || !string.IsNullOrEmpty(SafeValue<string>(item["LocationName"])) ? " (" : "") + (!string.IsNullOrEmpty(SafeValue<string>(item["AccountName"])) ? (SafeValue<string>(item["AccountName"])) : "") + (!string.IsNullOrEmpty(SafeValue<string>(item["AccountName"])) && !string.IsNullOrEmpty(SafeValue<string>(item["LocationName"])) ? " - " : "") + (!string.IsNullOrEmpty(SafeValue<string>(item["LocationName"])) ? (SafeValue<string>(item["LocationName"])) : "") + (!string.IsNullOrEmpty(SafeValue<string>(item["AccountName"])) || !string.IsNullOrEmpty(SafeValue<string>(item["LocationName"])) ? ")" : "");
                            selectitm.Selected = true;
                            objcolleagueList.Add(selectitm);
                        }
                        else
                        {
                            selectitm.Value = Id;
                            selectitm.Text = SafeValue<string>(item["doctorname"]) + (!string.IsNullOrEmpty(SafeValue<string>(item["AccountName"])) || !string.IsNullOrEmpty(SafeValue<string>(item["LocationName"])) ? " (" : "") + (!string.IsNullOrEmpty(SafeValue<string>(item["AccountName"])) ? (SafeValue<string>(item["AccountName"])) : "") + (!string.IsNullOrEmpty(SafeValue<string>(item["AccountName"])) && !string.IsNullOrEmpty(SafeValue<string>(item["LocationName"])) ? " - " : "") + (!string.IsNullOrEmpty(SafeValue<string>(item["LocationName"])) ? (SafeValue<string>(item["LocationName"])) : "") + (!string.IsNullOrEmpty(SafeValue<string>(item["AccountName"])) || !string.IsNullOrEmpty(SafeValue<string>(item["LocationName"])) ? ")" : "");
                            selectitm.Selected = false;
                            objcolleagueList.Add(selectitm);
                        }
                    }

                }
                return objcolleagueList;
            }
            catch (Exception Ex)
            {
                objclsCommon.InsertErrorLog("MessagesBLL--GetColleagueList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }




        public static List<PatientsDatalist> GetPatientList(int UserId, string SearchText, string PatientId = null)
        {
            try
            {
                List<PatientsDatalist> objpatientList = new List<PatientsDatalist>();
                DataTable dt = new DataTable();
                dt = ObjPatientData.GetPatientListForAutoComplete(UserId, SearchText, PatientId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objpatientList = (from p in dt.AsEnumerable()
                                      select new PatientsDatalist
                                      {
                                          id = SafeValue<string>(p["PatientId"]),
                                          text = SafeValue<string>(p["patientName"])
                                      }).ToList();

                    //PatientList.Where(t => t.PatientId == Selected).Any();
                }
                return objpatientList;
            }
            catch (Exception Ex)
            {
                objclsCommon.InsertErrorLog("PlanTreatmentBLL--GetPatientList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static CaseDefaultStep GetPatientDetials(int PatientId, string TimeZoneSystemName)
        {
            try
            {
                CaseDefaultStep objpatientdetails = new CaseDefaultStep();
                string strDateOfBirth = null;
                DateTime TempDateOfBirth;
                DataTable dt = new DataTable();
                dt = ObjPatientsData.GetPatientsDetails(PatientId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        if (!string.IsNullOrEmpty(SafeValue<string>(row["DateOfBirth"])))
                        {
                            TempDateOfBirth = SafeValue<DateTime>(row["DateOfBirth"]);
                            TempDateOfBirth = clsHelper.ConvertFromUTC(TempDateOfBirth, TimeZoneSystemName);

                            strDateOfBirth = new CommonBLL().ConvertToDate(TempDateOfBirth, (int)BO.Enums.Common.DateFormat.GENERAL);
                        }
                        else
                        {
                            strDateOfBirth = null;
                        }
                        objpatientdetails.objPatientdetail = new Patient
                        {
                            PatientId = SafeValue<int>(row["PatientId"]),
                            FirstName = SafeValue<string>(row["FirstName"]),
                            LastName = SafeValue<string>(row["LastName"]),
                            FullName = SafeValue<string>(row["FirstName"]) + " " + SafeValue<string>(row["LastName"]),
                            DateOfBirth = strDateOfBirth,
                        };
                    }
                }
                return objpatientdetails;
            }
            catch (Exception Ex)
            {
                objclsCommon.InsertErrorLog("PlanTreatmentBLL--GetPatientDetials", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static Cases GetPlanDetailsByCaseId(int _caseId, int _patientId, string TimeZoneSystemName)
        {
            try
            {
                Cases obj = new Cases();
                List<CaseSteps> list = new List<CaseSteps>();
                DataSet ds = PlanTreatmentDAL.GetPlanDetailsByCaseId(_caseId, _patientId);
                if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
                {

                    obj.CaseId = SafeValue<int>(ds.Tables[0].Rows[0]["CaseId"]);
                    obj.CaseName = SafeValue<string>(ds.Tables[0].Rows[0]["CaseName"]);
                    obj.CreatedBy = SafeValue<string>(ds.Tables[0].Rows[0]["CreatedBy"]);
                    obj.CreatedDate = SafeValue<DateTime>(ds.Tables[0].Rows[0]["CreatedDate"]);
                    obj.PatientId = SafeValue<int>(ds.Tables[0].Rows[0]["PatientId"]);
                    obj.Status = SafeValue<int>(ds.Tables[0].Rows[0]["Status"]);
                    obj.TreatmentAccepted = SafeValue<bool>(ds.Tables[0].Rows[0]["TreatmentAccepted"]);
                    if (!string.IsNullOrWhiteSpace(SafeValue<string>(ds.Tables[0].Rows[0]["TreatmentAcceptedDate"])))
                    {
                        obj.TreatmentAcceptedDate = SafeValue<DateTime>(ds.Tables[0].Rows[0]["TreatmentAcceptedDate"]);
                    }
                    string strDateOfBirth = null;
                    DateTime TempDateOfBirth;
                    var dt = ObjPatientsData.GetPatientsDetails(SafeValue<int>(_patientId));
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            if (!string.IsNullOrEmpty(SafeValue<string>(row["DateOfBirth"])))
                            {
                                TempDateOfBirth = SafeValue<DateTime>(row["DateOfBirth"]);
                                TempDateOfBirth = clsHelper.ConvertFromUTC(TempDateOfBirth, TimeZoneSystemName);

                                strDateOfBirth = new CommonBLL().ConvertToDate(TempDateOfBirth, (int)BO.Enums.Common.DateFormat.GENERAL);
                            }
                            else
                            {
                                strDateOfBirth = null;
                            }
                            obj.objPatientdetail = new Patient
                            {
                                PatientId = SafeValue<int>(row["PatientId"]),
                                FirstName = SafeValue<string>(row["FirstName"]),
                                LastName = SafeValue<string>(row["LastName"]),
                                FullName = SafeValue<string>(row["FirstName"]) + " " + SafeValue<string>(row["LastName"]),
                                DateOfBirth = strDateOfBirth,
                            };
                        }
                    }
                    //Changes mad for Issue when Patient Not Exist in Patient Table
                    if (dt.Rows.Count == 0)
                    {
                        obj.objPatientdetail = new Patient
                        {
                            PatientId = 0,
                            FirstName ="       ",
                            LastName = "        ",
                            FullName = "        ",
                            DateOfBirth = "dd/MM/yyyy"
                        };
                    }

                    if (ds.Tables[1].Rows != null && ds.Tables[1].Rows.Count > 0)
                    {
                        foreach (DataRow row in ds.Tables[1].Rows)
                        {
                            list.Add(new CaseSteps()
                            {
                                CaseStepId = SafeValue<int>(row["CaseStepId"]),
                                Description = SafeValue<string>(row["Description"]), 
                                CreatedAt = SafeValue<DateTime>(row["EndDate"]),
                                StartDate = SafeValue<DateTime>(row["StartDate"]),
                                EndDate = SafeValue<DateTime>(row["EndDate"]),
                                UserId = SafeValue<int>(row["UserId"]),
                                Status = SafeValue<bool>(row["Status"]),
                                CaseId = SafeValue<int>(row["CaseId"]),
                                CreatedBy = SafeValue<string>(row["CreatedBy"]),
                                ModifiedAt = SafeValue<DateTime>(row["ModifiedDate"]),
                                ModifiedBy = SafeValue<string>(row["ModifiedBy"]), 
                                StepOrder = SafeValue<int>(row["StepOrder"]),
                            });
                        }
                    }
                    obj._caseStepList = list;
                }
                return obj;
            }
            catch (Exception ex)
            {
                objclsCommon.InsertErrorLog("PlanTreatmentBLL--GetPlanDetailsByCaseId", ex.Message, ex.StackTrace);
                throw ex;
            }


        }

        public static bool InsertPlanDetails(PlanTreatmentVM _planTreatmentVM)
        {
            try
            {
                bool IsInserted = false;
                var _objCases = MapVMToCases(_planTreatmentVM);
                var _isCaseId = PlanTreatmentDAL.InsertPlanDetails(_objCases);
                if (_isCaseId > 0)
                {
                    //foreach (var item in _objCases._caseStepList)
                    //{
                    //    item.CaseId = _isCaseId;
                    //    PlanTreatmentDAL.InsertUpdatePlanStepDetails(item);
                    //    IsInserted = true;
                    //}
                    IsInserted = true;
                    return IsInserted;
                }
                else
                {
                    return IsInserted;
                }
            }
            catch (Exception ex)
            {
                objclsCommon.InsertErrorLog("PlanTreatmentBLL--InsertPlanDetails", ex.Message, ex.StackTrace);
                throw ex;
            }
        }

        public static bool CheckPatientPlanByPatientId(int _patientId)
        {
            try
            {
                return PlanTreatmentDAL.CheckPatientPlanByPatientId(_patientId);
            }
            catch (Exception ex)
            {
                objclsCommon.InsertErrorLog("PlanTreatmentBLL--CheckPatientPlanByPatientId", ex.Message, ex.StackTrace);
                throw ex;
            }
        }

        public static Cases MapVMToCases(PlanTreatmentVM _planTreatmentVM)
        {
            Cases objCases = new Cases();
            objCases.CaseId = SafeValue<int>(_planTreatmentVM.id);
            objCases.PatientId = SafeValue<int>(_planTreatmentVM.PatientId);
            //RM-364 :Plan name is not appears
            objCases.CaseName = _planTreatmentVM.fullname.Trim();
            objCases.CreatedBy = SafeValue<string>(HttpContext.Current.Session["UserId"]);
            objCases.TreatmentAcceptedDate = SafeValue<DateTime>(_planTreatmentVM.acceptdate);
            if (_planTreatmentVM.isAccepted.ToLower() == "on")
            {
                objCases.TreatmentAccepted = true;
            }
            else
            {
                objCases.TreatmentAccepted = false;
            }
            objCases.Status = SafeValue<int>(_planTreatmentVM.Status);
            objCases.CreatedDate = DateTime.Now;
            List<CaseSteps> lstCaseSteps = new List<CaseSteps>();
            foreach (var item in _planTreatmentVM.jsonObj)
            {
                CaseSteps objCaseSteps = new CaseSteps();
                objCaseSteps.Description = item.desc;
                objCaseSteps.UserId = SafeValue<int>(item.provider);
                objCaseSteps.StepOrder = SafeValue<int>(item.stepOrder);
                objCaseSteps.StartDate = DateTime.Now;
                objCaseSteps.Status = true;
                objCaseSteps.EndDate = SafeValue<DateTime>(item.date);
                objCaseSteps.CaseId = SafeValue<int>(item.caseid);
                objCaseSteps.CaseStepId = SafeValue<int>(item.casestepid);
                objCaseSteps.CreatedAt = DateTime.Now;
                if (objCaseSteps.CaseStepId <= 0)
                {
                    objCaseSteps.CreatedBy = SafeValue<string>(HttpContext.Current.Session["UserId"]);
                }
                objCaseSteps.ModifiedAt = DateTime.Now;
                objCaseSteps.ModifiedBy = SafeValue<string>(HttpContext.Current.Session["UserId"]);
                lstCaseSteps.Add(objCaseSteps);
            }
            objCases._caseStepList = lstCaseSteps;
            return objCases;
        }

        public static bool UpdatePlanDetails(PlanTreatmentVM _updateplanTreatmentVM)
        {
            try
            {
                bool IsUpdated = false;
                var _objCases = MapVMToCases(_updateplanTreatmentVM);
                var _isCaseId = PlanTreatmentDAL.InsertPlanDetails(_objCases);
                if (_isCaseId > 0)
                {
                    //foreach (var item in _objCases._caseStepList)
                    //{
                    //    PlanTreatmentDAL.InsertUpdatePlanStepDetails(item);
                    //    IsUpdated = true;
                    //}
                    IsUpdated = true;
                    return IsUpdated;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                objclsCommon.InsertErrorLog("PlanTreatmentBLL--UpdatePlanDetails", ex.Message, ex.StackTrace);
                throw ex;
            }
        }
        public static bool DeleteCaseStep(int CaseStepId,int UserId)
        {
            return PlanTreatmentDAL.DeletePlanStepbyId(CaseStepId, UserId);
        }

        public static List<PlanTreatmentReport> GetCaseReportList(int UserId, string SortColumn, string SortDirection, int PageIndex, int PageSize, string SearchText, string TimeZoneSystemName, string PatientName, string DoctorName, int PatientId, string TreatmentStatus, DateTime? StartDate, DateTime? EndDate,string PlanStatus)
        {
            
            try
            {
                Cases obj = new Cases();
                PlanTreatmentReport objtreatment = new PlanTreatmentReport();
                List<PlanTreatmentReport> treatmentlist = new List<PlanTreatmentReport>();
                
                DataTable dt = PlanTreatmentDAL.GetCaseReport(PatientId, UserId, SortColumn, SortDirection, PageIndex, PageSize, SearchText, TreatmentStatus, PatientName, DoctorName, StartDate, EndDate, PlanStatus);
               
                if (dt != null && dt.Rows.Count > 0)
                    {
                    foreach (DataRow row in dt.Rows)
                    {
                       // var UserName = string.IsNullOrEmpty(SafeValue<string>(row["DoctorName"])) ? "N/A" : SafeValue<string>(row["DoctorName"]);
                      
                        var TreatmentAccepted = SafeValue<string>(row["TreatmentAccepted"]) != "True" ? "No" : "Yes";
                        

                        DateTime? ConvertedTreatmentAcceptedDate = null;
                        if (!string.IsNullOrEmpty(SafeValue<string>(row["TreatmentAcceptedDate"])))
                        {
                            ConvertedTreatmentAcceptedDate = SafeValue<DateTime>
                                                    (SafeValue<DateTime>(SafeValue<string>(row["TreatmentAcceptedDate"])).ToString("MM-dd-yyyy")
                                                    );                            
                        }

                        //DateTime? ConvertedStartDate = null;
                        //if (!string.IsNullOrEmpty(SafeValue<string>(row["StartDate"])))
                        //{
                        //    ConvertedStartDate = SafeValue<DateTime>
                        //                            (SafeValue<DateTime>(SafeValue<string>(row["StartDate"])).ToString("MM-dd-yyyy") 
                        //                            );
                        //    ConvertedStartDate = clsHelper.ConvertFromUTC(SafeValue<DateTime>(ConvertedStartDate), TimeZoneSystemName);
                        //}

                        //DateTime? ConvertedEndDate = null;
                        //if (!string.IsNullOrEmpty(SafeValue<string>(row["EndDate"])))
                        //{
                        //    ConvertedEndDate = SafeValue<DateTime>
                        //                            (SafeValue<DateTime>(SafeValue<string>(row["EndDate"])).ToString("MM-dd-yyyy")
                        //                            );
                        //    ConvertedEndDate = clsHelper.ConvertFromUTC(SafeValue<DateTime>(ConvertedEndDate), TimeZoneSystemName);
                        //}

                        objtreatment = new PlanTreatmentReport
                        {
                            CaseId = SafeValue<string>(row["CaseId"]),
                            PatientId = SafeValue<string>(row["PatientId"]),
                            PatientName = SafeValue<string>(row["PatientName"]),
                            CaseName = SafeValue<string>(row["CaseName"]),                            
                            IsTreatmentAccepted = TreatmentAccepted,
                            TreatmentAcceptedDate = (ConvertedTreatmentAcceptedDate.HasValue ? SafeValue<string>(ConvertedTreatmentAcceptedDate.Value.ToString("MM/dd/yyyy")) : ""),
                            DoctorId = SafeValue<string>(row["UserIds"]),
                            DoctorName = SafeValue<string>(row["DoctorName"]),
                            AccountMember = SafeValue<string>(row["AccountMember"]),
                            //StartDate = (ConvertedStartDate.HasValue ? SafeValue<string>(ConvertedStartDate.Value.ToString("MM/dd/yyyy")) : ""),
                            // EndDate = (ConvertedEndDate.HasValue ? SafeValue<string>(ConvertedEndDate.Value.ToString("MM/dd/yyyy")) : ""),

                        };
                        treatmentlist.Add(objtreatment);
                    }                        
                }
                return treatmentlist;
            }
            catch (Exception ex)
            {
                objclsCommon.InsertErrorLog("PlanTreatmentBLL--GetPlanDetailsByCaseId", ex.Message, ex.StackTrace);
                throw ex;
            }


        }

        public static List<PlanTreatmentReport> GetCaseByPatientId(int _patientId,string UserId)
        {
            try
            {                
                List<PlanTreatmentReport> lsttreatment = new List<PlanTreatmentReport>();
                DataSet ds = PlanTreatmentDAL.GetCaseByPatientId(_patientId, UserId);
                if (ds.Tables[0].Rows != null && ds.Tables[0].Rows.Count > 0)
                {                    
                    foreach (DataRow rows in ds.Tables[0].Rows)
                    {
                        PlanTreatmentReport objtreatment = new PlanTreatmentReport();
                        objtreatment.CaseId = SafeValue<string>(rows["CaseId"]);
                        objtreatment.PatientId = SafeValue<string>(rows["PatientId"]);
                        objtreatment.Status = SafeValue<string>(rows["Status"]);
                        objtreatment.IsTreatmentAccepted = SafeValue<string>(rows["TreatmentAccepted"]);
                        objtreatment.TreatmentAcceptedDate = SafeValue<string>(rows["TreatmentAcceptedDate"]);
                        objtreatment.StartDate = SafeValue<string>(rows["CreatedDate"]);
                        objtreatment.DoctorId = SafeValue<string>(rows["CreatedBy"]);
                        objtreatment.CaseName = SafeValue<string>(rows["CaseName"]);
                        objtreatment.AccountMember = SafeValue<string>(rows["AccountMember"]);
                        if (ds.Tables[1].Rows != null && ds.Tables[1].Rows.Count > 0)
                        {
                            var lstCasestep = new List<CaseSteps>();
                            foreach (DataRow row in ds.Tables[1].Rows)
                            {
                                CaseSteps objCaseSteps = new CaseSteps();
                                if (objtreatment.CaseId == SafeValue<string>(row["CaseId"]))
                                {                                    
                                    objCaseSteps.CaseId = SafeValue<int>(row["CaseId"]);
                                    objCaseSteps.CaseStepId = SafeValue<int>(row["CaseStepId"]);
                                    objCaseSteps.StepOrder = SafeValue<int>(row["StepOrder"]);
                                    objCaseSteps.Description = SafeValue<string>(row["Description"]);
                                    objCaseSteps.StartDate = SafeValue<DateTime>(row["StartDate"]);
                                    objCaseSteps.EndDate = SafeValue<DateTime>(row["EndDate"]);
                                    objCaseSteps.UserId = SafeValue<int>(row["UserId"]);
                                    objCaseSteps.Status = SafeValue<bool>(row["Status"]);
                                    objCaseSteps.CreatedAt = SafeValue<DateTime>(row["CreatedDate"]);
                                    objCaseSteps.CreatedBy = SafeValue<string>(row["CreatedBy"]);
                                    objCaseSteps.DoctorName = SafeValue<string>(row["DoctorName"]);
                                    lstCasestep.Add(objCaseSteps);
                                }                                                                
                            }
                            objtreatment.lstPlanSteps = lstCasestep;
                        }
                        lsttreatment.Add(objtreatment);
                    }                    
                }
                return lsttreatment;
            }
            catch (Exception ex)
            {
                objclsCommon.InsertErrorLog("PlanTreatmentBLL--GetCaseByPatientId", ex.Message, ex.StackTrace);
                throw ex;
            }
        }
    }
}


