﻿using BO.Models;
using BO.ViewModel;
using DataAccessLayer;
using DataAccessLayer.Common;
using System;
using System.Configuration;
using System.Net;
using System.Net.Http;

namespace BusinessLogicLayer
{
    public class OnederfulBLL
    {
        /// <summary>
        /// Get Access Token form Onederful.
        /// </summary>
        /// <returns>Token</returns>
        public static string GetAccessToken()
        {
            try
            {
                OnederfulCredentials Objs = new OnederfulCredentials();
                AccessToken Obj = CallOnederfulAPI<AccessToken, OnederfulCredentials>(0,ConfigurationManager.AppSettings.Get("Onederful_Token_URL"), "", "POST", Objs).Result;
                return (!string.IsNullOrWhiteSpace(Obj.access_token)) ? Obj.access_token : string.Empty;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("OnederfulBLL-GetAccessToken", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        /// <summary>
        /// Create Generic functions for Call the Onederful API.
        /// </summary>
        /// <typeparam name="T">Response parameters</typeparam>
        /// <typeparam name="I">Request Parameters</typeparam>
        /// <param name="URL">Onederful API URL</param>
        /// <param name="Token">Bearer Token</param>
        /// <param name="Method">Method Type POST/GET</param>
        /// <param name="parameter">Parameters</param>
        /// <returns></returns>
        public static APIResponseBase<T> CallOnederfulAPI<T,I>(int PatientId, string URL,string Token, string Method = "POST",I parameter = default(I), int UserId = 0, int AccountId = 0)
        {
            try
            {
                HttpResponseMessage response = null;
                HttpClient Client = new HttpClient();
                Client.DefaultRequestHeaders.Clear();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                Client.DefaultRequestHeaders.Add("Authorization", "Bearer " + Token);
                if (Method == "POST")
                    response = Client.PostAsJsonAsync(URL, parameter).Result;
                else
                    response = Client.GetAsync(URL).Result;
                if (!string.IsNullOrWhiteSpace(Token))
                {
                    string ReceivedJSON = response.Content.ReadAsStringAsync().Result;
                    OnederfulDAL.InsertPatientInsuranceDetails(PatientId, ReceivedJSON,UserId,AccountId, (response.StatusCode == HttpStatusCode.OK) ? false : true);
                }
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    
                    var result = response.Content.ReadAsAsync<T>().Result;

                    return new APIResponseBase<T>()
                    {
                        IsSuccess = true,
                        Message = string.Empty,
                        Result = result,
                        StatusCode = (int)response.StatusCode
                    };
                }
                else
                {
                    return new APIResponseBase<T>()
                    {
                        IsSuccess = false,
                        Message = getErrorMessage(URL, response),
                        Result = default(T),
                        StatusCode = (int)response.StatusCode
                    };
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("OnederfulBLL-CallOnederfulAPI", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Error Message into the Onederful API method.
        /// </summary>
        /// <param name="url"></param>
        /// <param name="response"></param>
        /// <returns></returns>
        private static string getErrorMessage(string url, HttpResponseMessage response)
        {
            return $"Request Url : {url} Status code : {response.StatusCode} Error : {response.ReasonPhrase}";
        }

        public static void GetPatientInsuranceInfo()
        {
            try
            {
                InputModel obj = new InputModel();
                GetSubscriber subscriber = new GetSubscriber();
                GetProvider provider = new GetProvider();
                GetPayer payer = new GetPayer();

                subscriber.firstName = "Abhiram";
                subscriber.lastName = "Ravi";
                subscriber.dob = "08/02/1992";
                subscriber.memberId = "398537";
                subscriber.groupNumber = "2460-0001";
                provider.npi = "1659896827";
                payer.id = "DD_CALIFORNIA";

                obj.subscriber = subscriber;
                obj.provider = provider;
                obj.payer = payer;
                One_Response Objs = CallOnederfulAPI<One_Response, InputModel>(1,ConfigurationManager.AppSettings.Get("Onederful_Eligibility_URL"), GetAccessToken(), "POST",obj).Result;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("OnederfulBLL-GetPatientInsuranceInfo", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
