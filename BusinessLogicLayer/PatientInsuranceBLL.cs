﻿using BO.Models;
using BO.ViewModel;
using DataAccessLayer.Common;
using DataAccessLayer.PatientsData;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer
{
    public class PatientInsuranceBLL
    {
        static clsCommon Common = new clsCommon();
        clsPatientInsuranceData data = new clsPatientInsuranceData();
        clsCommon common = new clsCommon();


        public List<DataAccessLayer.PatientsData.Schema.InsuredRecordSchema> GetPatientInsuranceDetail(PatientInsuranceFilter model)
        {
            return data.GetInsuranceRecordList(model);
        }


        /// <summary>
        /// This Method will save insurance carrier data from outside systems.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static APIResponseBase<bool> NewUploadCarrier(List<BO.Models.InsuranceCarrier> models)
        {
            APIResponseBase<bool> response = new APIResponseBase<bool>();
            try
            {
                if (CheckDentrixConnectorKeyExists(models[0].dentrixConnectorID))
                //if (true)
                {
                    DataTable PhoneTable = new DataTable();

                    #region Declare DataTable


                    // Adding Columns  
                    DataColumn COLUMN = new DataColumn();
                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "Id";
                    COLUMN.DataType = typeof(int);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "Identifier";
                    COLUMN.DataType = typeof(int);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "CarrierID";
                    COLUMN.DataType = typeof(int);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "InsuranceType";
                    COLUMN.DataType = typeof(int);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "Name";
                    COLUMN.DataType = typeof(string);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "CarrierName";
                    COLUMN.DataType = typeof(string);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "GroupName";
                    COLUMN.DataType = typeof(string);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "GroupNumber";
                    COLUMN.DataType = typeof(string);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "CoverageTableId";
                    COLUMN.DataType = typeof(int);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "RenewalMonth";
                    COLUMN.DataType = typeof(int);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "FeeScheduleId";
                    COLUMN.DataType = typeof(int);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "PayorId";
                    COLUMN.DataType = typeof(string);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "Person";
                    COLUMN.DataType = typeof(decimal);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "Family";
                    COLUMN.DataType = typeof(decimal);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "Lifetime";
                    COLUMN.DataType = typeof(decimal);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "Person_Standard";
                    COLUMN.DataType = typeof(decimal);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "Person_Preventive";
                    COLUMN.DataType = typeof(decimal);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "Person_Other";
                    COLUMN.DataType = typeof(decimal);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "Family_Standard";
                    COLUMN.DataType = typeof(decimal);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "Family_Preventive";
                    COLUMN.DataType = typeof(decimal);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "Family_Other";
                    COLUMN.DataType = typeof(decimal);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "Lifetime_Standard";
                    COLUMN.DataType = typeof(decimal);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "Lifetime_Preventive";
                    COLUMN.DataType = typeof(decimal);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "Lifetime_Other";
                    COLUMN.DataType = typeof(decimal);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "Emails";
                    COLUMN.DataType = typeof(string);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "PhoneNumbers";
                    COLUMN.DataType = typeof(string);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "Addresses";
                    COLUMN.DataType = typeof(string);
                    PhoneTable.Columns.Add(COLUMN);


                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "automodifiedtimestamp";
                    COLUMN.DataType = typeof(DateTime);
                    PhoneTable.Columns.Add(COLUMN);


                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "DentrixId";
                    COLUMN.DataType = typeof(int);
                    PhoneTable.Columns.Add(COLUMN);


                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "PracticeID";
                    COLUMN.DataType = typeof(int);
                    PhoneTable.Columns.Add(COLUMN);


                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "ConnectorID";
                    COLUMN.DataType = typeof(string);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "dentrixConnectorID";
                    COLUMN.DataType = typeof(string);
                    PhoneTable.Columns.Add(COLUMN);

                    

                    #endregion

                    int Id = 0;
                    foreach (var UPB in models)
                    {
                        #region Assign value into datatable


                        Id = Id + 1;
                        DataRow DR = PhoneTable.NewRow();
                        DR[0] = Id;
                        DR[1] = UPB.Identifier;
                        DR[2] = UPB.CarrierID;
                        DR[3] = UPB.InsuranceType;
                        DR[4] = UPB.Name;
                        DR[5] = UPB.CarrierName;
                        DR[6] = UPB.GroupName;
                        DR[7] = UPB.GroupNumber;
                        DR[8] = UPB.CoverageTableId;
                        DR[9] = UPB.RenewalMonth;
                        DR[10] = UPB.FeeScheduleId;
                        DR[11] = UPB.PayorId;
                        DR[12] = UPB.MaxBenfits.Person;
                        DR[13] = UPB.MaxBenfits.Family;
                        DR[14] = UPB.MaxBenfits.Lifetime;
                        DR[15] = UPB.DeductibleInfo.Person.Standard == null ? 0 : UPB.DeductibleInfo.Person.Standard;
                        DR[16] = UPB.DeductibleInfo.Person.Preventive == null ? 0 : UPB.DeductibleInfo.Person.Preventive;
                        DR[17] = UPB.DeductibleInfo.Person.Other == null ? 0 : UPB.DeductibleInfo.Person.Other;
                        DR[18] = UPB.DeductibleInfo.Family.Standard == null ? 0 : UPB.DeductibleInfo.Family.Standard;
                        DR[19] = UPB.DeductibleInfo.Family.Preventive == null ? 0 : UPB.DeductibleInfo.Family.Preventive;
                        DR[20] = UPB.DeductibleInfo.Family.Other == null ? 0 : UPB.DeductibleInfo.Family.Other;
                        DR[21] = UPB.DeductibleInfo.Lifetime.Standard == null ? 0 : UPB.DeductibleInfo.Lifetime.Standard;
                        DR[22] = UPB.DeductibleInfo.Lifetime.Preventive == null ? 0 : UPB.DeductibleInfo.Lifetime.Preventive;
                        DR[23] = UPB.DeductibleInfo.Lifetime.Other == null ? 0 : UPB.DeductibleInfo.Person.Other;
                        if (UPB.Emails.Count > 0)
                        {
                            DR[24] = JsonConvert.SerializeObject(UPB.Emails);
                        }

                        DR[25] = JsonConvert.SerializeObject(UPB.PhoneNumbers);
                        DR[26] = JsonConvert.SerializeObject(UPB.Addresses);
                        if (UPB.automodifiedtimestamp != null)
                        {
                            DR[27] = UPB.automodifiedtimestamp;
                        }

                        DR[28] = UPB.DentrixId;
                        DR[29] = UPB.PracticeID;
                        DR[30] = UPB.ConnectorID;
                        DR[31] = UPB.dentrixConnectorID;

                        PhoneTable.Rows.Add(DR);
                    }
                    #endregion

                    clsPatientInsuranceData data = new clsPatientInsuranceData();
                    data.SaveJsonCarrierInfo(PhoneTable);

                    response.StatusCode = (int)HttpStatusCode.OK;
                    response.IsSuccess = true;
                    response.Message = "All patient insurance records uploaded successfully";

                }
                else
                {
                    response.IsSuccess = false;
                    response.StatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "invalid dentrix connector id";
                }
            }
            catch (Exception ex)
            {

                response.StatusCode = (int)HttpStatusCode.InternalServerError;
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;

        }

        public static APIResponseBase<bool> UploadCarrier(List<BO.Models.InsuranceCarrier> models)
        {
            APIResponseBase<bool> response = new APIResponseBase<bool>();

            if (true)
            {


                DataTable PhoneTable = new DataTable();

                #region MyRegion


                // Adding Columns  
                DataColumn COLUMN = new DataColumn();
                COLUMN = new DataColumn();
                COLUMN.ColumnName = "Id";
                COLUMN.DataType = typeof(int);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "Identifier";
                COLUMN.DataType = typeof(int);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "CarrierID";
                COLUMN.DataType = typeof(int);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "InsuranceType";
                COLUMN.DataType = typeof(int);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "Name";
                COLUMN.DataType = typeof(string);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "CarrierName";
                COLUMN.DataType = typeof(string);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "GroupName";
                COLUMN.DataType = typeof(string);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "GroupNumber";
                COLUMN.DataType = typeof(string);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "CoverageTableId";
                COLUMN.DataType = typeof(int);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "RenewalMonth";
                COLUMN.DataType = typeof(int);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "FeeScheduleId";
                COLUMN.DataType = typeof(int);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "PayorId";
                COLUMN.DataType = typeof(string);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "Person";
                COLUMN.DataType = typeof(decimal);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "Family";
                COLUMN.DataType = typeof(decimal);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "Lifetime";
                COLUMN.DataType = typeof(decimal);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "Person_Standard";
                COLUMN.DataType = typeof(decimal);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "Person_Preventive";
                COLUMN.DataType = typeof(decimal);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "Person_Other";
                COLUMN.DataType = typeof(decimal);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "Family_Standard";
                COLUMN.DataType = typeof(decimal);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "Family_Preventive";
                COLUMN.DataType = typeof(decimal);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "Family_Other";
                COLUMN.DataType = typeof(decimal);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "Lifetime_Standard";
                COLUMN.DataType = typeof(decimal);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "Lifetime_Preventive";
                COLUMN.DataType = typeof(decimal);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "Lifetime_Other";
                COLUMN.DataType = typeof(decimal);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "Emails";
                COLUMN.DataType = typeof(string);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "PhoneNumbers";
                COLUMN.DataType = typeof(string);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "Addresses";
                COLUMN.DataType = typeof(string);
                PhoneTable.Columns.Add(COLUMN);


                COLUMN = new DataColumn();
                COLUMN.ColumnName = "automodifiedtimestamp";
                COLUMN.DataType = typeof(DateTime);
                PhoneTable.Columns.Add(COLUMN);


                COLUMN = new DataColumn();
                COLUMN.ColumnName = "DentrixId";
                COLUMN.DataType = typeof(int);
                PhoneTable.Columns.Add(COLUMN);


                COLUMN = new DataColumn();
                COLUMN.ColumnName = "PracticeID";
                COLUMN.DataType = typeof(int);
                PhoneTable.Columns.Add(COLUMN);


                COLUMN = new DataColumn();
                COLUMN.ColumnName = "ConnectorID";
                COLUMN.DataType = typeof(string);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "dentrixConnectorID";
                COLUMN.DataType = typeof(string);
                PhoneTable.Columns.Add(COLUMN);

                COLUMN = new DataColumn();
                COLUMN.ColumnName = "dentrixConnectorID";
                COLUMN.DataType = typeof(string);
                PhoneTable.Columns.Add(COLUMN);

                #endregion

                int Id = 0;
                foreach (var UPB in models)
                {
                    Id = Id + 1;
                    DataRow DR = PhoneTable.NewRow();
                    DR[0] = Id;
                    DR[1] = UPB.Identifier;
                    DR[2] = UPB.CarrierID;
                    DR[3] = UPB.InsuranceType;
                    DR[4] = UPB.Name;
                    DR[5] = UPB.CarrierName;
                    DR[6] = UPB.GroupName;
                    DR[7] = UPB.GroupNumber;
                    DR[8] = UPB.CoverageTableId;
                    DR[9] = UPB.RenewalMonth;
                    DR[10] = UPB.FeeScheduleId;
                    DR[11] = UPB.PayorId;
                    DR[12] = UPB.MaxBenfits.Person;
                    DR[13] = UPB.MaxBenfits.Family;
                    DR[14] = UPB.MaxBenfits.Lifetime;
                    DR[15] = UPB.DeductibleInfo.Person.Standard == null ? 0 : UPB.DeductibleInfo.Person.Standard;
                    DR[16] = UPB.DeductibleInfo.Person.Preventive == null ? 0 : UPB.DeductibleInfo.Person.Preventive;
                    DR[17] = UPB.DeductibleInfo.Person.Other == null ? 0 : UPB.DeductibleInfo.Person.Other;
                    DR[18] = UPB.DeductibleInfo.Family.Standard == null ? 0 : UPB.DeductibleInfo.Family.Standard;
                    DR[19] = UPB.DeductibleInfo.Family.Preventive == null ? 0 : UPB.DeductibleInfo.Family.Preventive;
                    DR[20] = UPB.DeductibleInfo.Family.Other == null ? 0 : UPB.DeductibleInfo.Family.Other;
                    DR[21] = UPB.DeductibleInfo.Lifetime.Standard == null ? 0 : UPB.DeductibleInfo.Lifetime.Standard;
                    DR[22] = UPB.DeductibleInfo.Lifetime.Preventive == null ? 0 : UPB.DeductibleInfo.Lifetime.Preventive;
                    DR[23] = UPB.DeductibleInfo.Lifetime.Other == null ? 0 : UPB.DeductibleInfo.Person.Other;
                    if (UPB.Emails.Count > 0)
                    {
                        DR[24] = JsonConvert.SerializeObject(UPB.Emails);
                    }

                    DR[25] = JsonConvert.SerializeObject(UPB.PhoneNumbers);
                    DR[26] = JsonConvert.SerializeObject(UPB.Addresses);
                    if (UPB.automodifiedtimestamp != null)
                    {
                        DR[27] = UPB.automodifiedtimestamp;
                    }

                    DR[28] = UPB.DentrixId;
                    DR[29] = UPB.PracticeID;
                    DR[30] = UPB.ConnectorID;
                    DR[31] = UPB.dentrixConnectorID;

                    PhoneTable.Rows.Add(DR);
                }


                clsPatientInsuranceData data = new clsPatientInsuranceData();

                //string g = JsonConvert.SerializeObject(models);
                data.SaveJsonCarrierInfo(PhoneTable);
            }
            else
            {

                response.StatusCode = (int)HttpStatusCode.OK;
                response.IsSuccess = true;
                response.Message = "All patient insurance records uploaded successfully";




                clsPatientInsuranceData data = new clsPatientInsuranceData();
                if (CheckDentrixConnectorKeyExists(models[0].dentrixConnectorID))
                {
                    models.ForEach(model =>
                    {
                        Dictionary<BO.Models.InsuranceCarrier, string> failed = new Dictionary<BO.Models.InsuranceCarrier, string>();
                        try
                        {
                            data.SaveCarrier(model);
                        }
                        catch (Exception Ex)
                        {
                            failed.Add(model, Ex.Message);
                            response.IsSuccess = false;
                            response.StatusCode = (int)HttpStatusCode.BadRequest;
                            response.Message = "Some of the patient insurance records failed to save. please check the result for a list of failed patient insurance records";
                            // response.Result = failed;
                            Common.InsertErrorLog("PatientInsuranceBLL---UploadCarrier", Ex.Message, Ex.StackTrace);
                            throw;
                        }
                    });
                }
                else
                {
                    response.IsSuccess = false;
                    response.StatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "invalid dentrix connector id";
                }
            }

            return response;

        }

        /// <summary>
        /// This Method will save insured records from outside systems.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static APIResponseBase<bool> NewUploadInsuranceRecord(List<InsuredRecord> models)
        {
            APIResponseBase<bool> response = new APIResponseBase<bool>();
            try
            {
                //if (true)
                if (CheckDentrixConnectorKeyExists(models[0].dentrixConnectorID))
                {
                    DataTable PhoneTable = new DataTable();

                    #region Declare DataTable


                    // Adding Columns  
                    DataColumn COLUMN = new DataColumn();
                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "Id";
                    COLUMN.DataType = typeof(int);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "Identifier";
                    COLUMN.DataType = typeof(int);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "InsuredId";
                    COLUMN.DataType = typeof(int);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "SubscriberID";
                    COLUMN.DataType = typeof(string);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "SubscriberDetail";
                    COLUMN.DataType = typeof(string);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "CarrierId";
                    COLUMN.DataType = typeof(int);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "GroupName";
                    COLUMN.DataType = typeof(string);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "GroupNumber";
                    COLUMN.DataType = typeof(string);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "BenefitsUsed";
                    COLUMN.DataType = typeof(decimal);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "DeductibleUsed";
                    COLUMN.DataType = typeof(string);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "InsuredMembers";
                    COLUMN.DataType = typeof(string);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "automodifiedtimestamp";
                    COLUMN.DataType = typeof(DateTime);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "DentrixId";
                    COLUMN.DataType = typeof(int);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "PracticeID";
                    COLUMN.DataType = typeof(int);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "ConnectorID";
                    COLUMN.DataType = typeof(string);
                    PhoneTable.Columns.Add(COLUMN);

                    COLUMN = new DataColumn();
                    COLUMN.ColumnName = "dentrixConnectorID";
                    COLUMN.DataType = typeof(string);
                    PhoneTable.Columns.Add(COLUMN);

                    #endregion

                    int Id = 0;
                    foreach (var UPB in models)
                    {
                        #region Assign value into datatable


                        Id = Id + 1;
                        DataRow DR = PhoneTable.NewRow();
                        DR[0] = Id;
                        DR[1] = UPB.Identifier;
                        DR[2] = UPB.InsuredId;
                        DR[3] = UPB.SubscriberID;
                        if (UPB.SubscriberDetail != null)
                        {
                            
                            DR[4] = JsonConvert.SerializeObject(UPB.SubscriberDetail); 
                        }
                        DR[5] = UPB.CarrierId;
                        DR[6] = UPB.GroupName;
                        DR[7] = UPB.GroupNumber;
                        DR[8] = UPB.BenefitsUsed;
                        if (UPB.DeductibleUsed != null)
                        {
                            DR[9] = JsonConvert.SerializeObject(UPB.DeductibleUsed);  
                        }
                        if (UPB.InsuredMembers != null)
                        {
                            DR[10] = JsonConvert.SerializeObject(UPB.InsuredMembers);  
                        }
                        if (UPB.automodifiedtimestamp != null)
                        {
                            DR[11] = UPB.automodifiedtimestamp;
                        }
                        DR[12] = UPB.DentrixId;
                        DR[13] = UPB.PracticeID;
                        DR[14] = UPB.ConnectorID;
                        DR[15] = UPB.dentrixConnectorID;
                         
                        PhoneTable.Rows.Add(DR);
                    }
                    #endregion

                    clsPatientInsuranceData data = new clsPatientInsuranceData();
                    data.SaveJsonInsuredRecord(PhoneTable);

                    response.StatusCode = (int)HttpStatusCode.OK;
                    response.IsSuccess = true;
                    response.Message = "All patient insurance records uploaded successfully";

                }
                else
                {
                    response.IsSuccess = false;
                    response.StatusCode = (int)HttpStatusCode.BadRequest;
                    response.Message = "invalid dentrix connector id";
                }
            }
            catch (Exception ex)
            {

                response.StatusCode = (int)HttpStatusCode.InternalServerError;
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;

        }

        public static APIResponseBase<bool> UploadInsuranceRecord(List<InsuredRecord> models)
        {
            APIResponseBase<bool> response = new APIResponseBase<bool>();
            response.StatusCode = (int)HttpStatusCode.OK;
            response.IsSuccess = true;
            response.Message = "All insured records uploaded successfully";

            clsPatientInsuranceData data = new clsPatientInsuranceData();
            if (CheckDentrixConnectorKeyExists(models[0].dentrixConnectorID))
            {
                models.ForEach(model =>
                {
                    Dictionary<InsuredRecord, string> failed = new Dictionary<InsuredRecord, string>();
                    try
                    {
                        data.SaveInsuredRecord(model);
                    }
                    catch (Exception Ex)
                    {
                        response.IsSuccess = false;
                        response.StatusCode = (int)HttpStatusCode.BadRequest;
                        response.Message = "Some of the patient insured records failed to save. please check the result for a list of failed patient insurance records";
                        // response.Result = failed;
                        Common.InsertErrorLog("PatientInsuranceBLL---UploadInsuranceRecord", Ex.Message, Ex.StackTrace);
                        throw;
                    }
                });
            }
            else
            {
                response.IsSuccess = false;
                response.StatusCode = (int)HttpStatusCode.BadRequest;
                response.Message = "invalid dentrix connector id";
            }

            return response;

        }

        public static bool CheckDentrixConnectorKeyExists(string DentrixConnectorKey)
        {
            try
            {
                DataTable dt = clsPatientsData.CheckDentrixConnectorExistOrNot(DentrixConnectorKey);
                if (dt != null && dt.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception Ex)
            {

                throw;
            }
        }

    }
}

