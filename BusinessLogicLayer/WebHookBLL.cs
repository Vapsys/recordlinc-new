﻿using BO.Models;
using DataAccessLayer.Common;
using System;

namespace BusinessLogicLayer
{
    public class WebHookBLL
    {
        public static SMSDetails ReceivedSMSHistory(ZipwhipReceiver Receiver)
        {
            try
            {
                SMSDetails Obj = new SMSDetails();

                return Obj;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("WebHookBLL--ReceivedSMSHistory", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
