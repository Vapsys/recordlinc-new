﻿using BO.Models;
using BO.ViewModel;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using System;
using System.Data;
using DataAccessLayer.SmileBrand;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using DataAccessLayer.PatientsData;
using static BO.Enums.Common;
using static DataAccessLayer.Common.clsCommon;
using BO.Enums;

namespace BusinessLogicLayer
{
    public class SmileBrandBLL
    {
        /// <summary>
        /// This Method generate a token when user will request for token.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public static SmileBrandResponse<TokenModel> GetAccessToken(Login Obj)
        {
            SmileBrandResponse<TokenModel> response = new SmileBrandResponse<TokenModel>();
            try
            {
                DataTable dtCheck = new clsColleaguesData().CheckEmailInSystem(Obj.UserName);
                if (dtCheck != null && dtCheck.Rows.Count > 0)
                {
                    int IsInSystem = SafeValue<int>(dtCheck.Rows[0]["IsInSystem"]);
                    TripleDESCryptoHelper cryptoHelper = new TripleDESCryptoHelper();
                    DataTable Login = new clsColleaguesData().LoginUserBasedOnUsernameandPassword(Obj.UserName, cryptoHelper.encryptText(Obj.Password));
                    if (Login != null && Login.Rows.Count > 0)
                    {
                        int UserId = SafeValue<int>(Login.Rows[0]["UserId"]);
                        //Create access token based on request.
                        TokenModel token = new TokenModel();
                        token.Token = GenerateToken(UserId);
                        response.IsSuccess = true;
                        response.Message = "Success";
                        response.Result = token;
                        response.StatusCode = 200;
                        return response;
                    }
                    else
                    {
                        response.IsSuccess = false;
                        response.Message = "User-name and Password is Invalid!";
                        response.StatusCode = 401;
                        response.Result = null;
                        return response;
                    }
                }
                else
                {
                    response.IsSuccess = false;
                    response.Message = "User-name and Password is Invalid!";
                    response.StatusCode = 401;
                    response.Result = null;
                    return response;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("SmileBrandBLL--GetAccessToken", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This Method Call the database and save the Token for that User and return that Token.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static string GenerateToken(int UserId)
        {
            try
            {
                string Token = string.Empty;
                DataTable dt = clsSmileBrand.GenerateTokenBasedonRequest(UserId, Guid.NewGuid().ToString());
                if (dt != null && dt.Rows.Count > 0)
                {
                    Token = SafeValue<string>(dt.Rows[0]["TokenNumber"]);
                }
                return Token;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("SmileBrandBLL--GenerateToken", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This method check the token is valid or not.
        /// </summary>
        /// <param name="Token"></param>
        /// <returns></returns>
        public static DataTable CheckTokenIsValid(string Token)
        {
            try
            {
                return clsSmileBrand.CheckTokenIsValid(Token);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("SmileBrandBLL--CheckTokenIsValid", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This Method get patient all procedure list.
        /// Updated date: 08-07-2018
        /// Updated by: Ankit Solanki
        /// JIRA NO SBI-35
        /// </summary>
        /// <param name="Obj"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static RetriveResponse<List<PatientProcedures>> GetPatientProcedures(PatientProcedure<PatientProcedureDateFilter> Obj, int UserId)
        {
            try
            {
                RetriveResponse<List<PatientProcedures>> Retrieve = new RetriveResponse<List<PatientProcedures>>();
                string RequestGUID = string.Empty;
                int ProcedureRequestId = 0;
                int TotalRecord = 0;
                List<PatientProcedures> Lst = new List<PatientProcedures>();
                if (SafeValue<DateTime>(Obj.FromDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    Obj.FromDate = null;
                }
                if (SafeValue<DateTime>(Obj.ToDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    Obj.ToDate = null;
                }
                if (string.IsNullOrEmpty(Obj.RequestId))
                {
                    if (Obj.PageIndex > 1)
                    {
                        return new RetriveResponse<List<PatientProcedures>>()
                        {
                            IsSuccess = false,
                            Message = "In order to use pagination, RequestId parameter is mandatory",
                            Result = null,
                            StatusCode = 400
                        };
                    }
                    int GetCountOfRecords = clsSmileBrand.GetCountofPatientProcedure(Obj, UserId);
                    if (GetCountOfRecords > BO.Constatnt.Common.CountOverLoad)
                    {
                        Retrieve.IsSuccess = false;
                        Retrieve.Message = $"The maximum number of records that can be returned by this API call is 50,000 rows. Your request has created a result set of {GetCountOfRecords} rows. Please modify your query to return a result set of less than 50,000 rows";
                        Retrieve.RequestId = RequestGUID;
                        Retrieve.TotalRecord = GetCountOfRecords;
                        Retrieve.Result = null;
                        Retrieve.StatusCode = 413;
                        return Retrieve;
                    }

                    IDictionary<int, string> Response = InsertDPMS_SyncDetails("PROCEDURE_SBI");
                    ProcedureRequestId = Response.Keys.FirstOrDefault();
                    RequestGUID = Response.Values.FirstOrDefault();

                    bool Result = clsSmileBrand.InsertDMPSProcedureProcessQueue(Obj, UserId, ProcedureRequestId);
                }
                else
                {
                    ProcedureRequestId = clsColleaguesData.GetPMSSyncDetailsByGUID(Obj.RequestId);
                }

                if (ProcedureRequestId > 0)
                {
                    DataTable dt = clsSmileBrand.GetPatientProcedures(ProcedureRequestId, Obj.FetchRecordCount, Obj.PageIndex, UserId);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            Lst.Add(new PatientProcedures()
                            {
                                RL_Id = SafeValue<int>(item["Id"]),
                                procid = SafeValue<int>(item["ProcId"]),
                                adacode = SafeValue<string>(item["ADACode"]),
                                adadescription = SafeValue<string>(item["adadescription"]),
                                amount = SafeValue<decimal>(item["Amount"]),
                                amountpreauth = SafeValue<decimal>(item["AmountPreauth"]),
                                amountpriminspaid = SafeValue<decimal>(item["AmountPrimInsPaid"]),
                                amountsecinspaid = SafeValue<decimal>(item["AmountSecInsPaid"]),
                                amountsecpreauth = SafeValue<decimal>(item["AmountSecPreAuth"]),
                                authstatus = SafeValue<int>(item["Authstatus"]),
                                authstatusstring = SafeValue<string>(item["AuthStatusString"]),
                                automodifiedtimestamp = SafeValue<DateTime>(item["automodifiedtimestamp"]),
                                chartstatus = SafeValue<int>(item["ChartStatus"]),
                                chartstatusstring = SafeValue<string>(item["ChartStatusString"]),
                                claimid = SafeValue<int>(item["ClaimId"]),
                                completiondate = SafeValue<DateTime>(item["completiondate"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(item["completiondate"]),
                                donotbillinsurance = SafeValue<bool>(item["DoNotBillInsurance"]),
                                RLguarid = SafeValue<int>(item["RLGuarId"]),
                                DentrixGuarId = SafeValue<int>(item["DentrixGuarId"]),
                                history = SafeValue<bool>(item["History"]),
                                medproctype = SafeValue<bool>(item["MedProctype"]),
                                DentrixPatientId = SafeValue<int>(item["DtxPatientId"]),
                                RLPatientId = SafeValue<int>(item["RLPatientId"]),
                                preauthid = SafeValue<int>(item["PreAuthId"]),
                                proccodeid = SafeValue<int>(item["ProcCodeId"]),
                                procdate = SafeValue<DateTime>(item["ProcDate"]),
                                RLprovid = SafeValue<string>(item["UserId"]),
                                Dentrixprovid = SafeValue<string>(item["DentrixProviderId"]),
                                refid = SafeValue<int>(item["RefId"]),
                                reftype = SafeValue<int>(item["RefType"]),
                                secauthstatus = SafeValue<int>(item["SecAuthStatus"]),
                                secauthstatusstring = SafeValue<string>(item["SecAuthStatusString"]),
                                startdate = SafeValue<DateTime>(item["startdate"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(item["startdate"]),
                                surfacebytes = SafeValue<string>(item["SurfaceBytes"]),
                                surfacestring = SafeValue<string>(item["SurfaceString"]),
                                toothrangeend = SafeValue<int>(item["ToothRangeEnd"]),
                                toothrangestart = SafeValue<int>(item["ToothRangeStart"]),
                                txcaseid = SafeValue<int>(item["TxCaseId"]),
                                txplanneddate = SafeValue<DateTime>(item["TxPlannedDate"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(item["TxPlannedDate"]),
                                IsDeleted = SafeValue<bool>(item["IsDeleted"]),
                                createdate = SafeValue<DateTime>(item["CreatedDate"])
                            });
                            TotalRecord = SafeValue<int>(item["TotalRecord"]);
                        }
                        Retrieve.IsSuccess = true;
                        Retrieve.Message = "Success!";
                        Retrieve.RequestId = RequestGUID;
                        Retrieve.TotalRecord = TotalRecord;
                        Retrieve.Result = Lst;
                        Retrieve.StatusCode = 200;
                    }
                    else
                    {
                        Retrieve.IsSuccess = false;
                        Retrieve.Message = "Not Found!";
                        Retrieve.RequestId = RequestGUID;
                        Retrieve.TotalRecord = TotalRecord;
                        Retrieve.Result = Lst;
                        Retrieve.StatusCode = 404;
                    }
                }
                return Retrieve;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("SmileBrandBLL--GetPatientProcedures", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This method will return all the locations of that user/dentist.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public static List<AddressInfo> GetLocationList(int UserId)
        {
            try
            {
                List<AddressInfo> Lst = new List<AddressInfo>();
                DataTable dt = clsSmileBrand.GetLocationList(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        Lst.Add(new AddressInfo()
                        {
                            LocationId = SafeValue<int>(item["AddressInfoID"]),
                            LocationName = SafeValue<string>(item["LocationName"]),
                            StreetAddress = SafeValue<string>(item["ExactAddress"]) + SafeValue<string>(item["Address2"]),
                            Email = SafeValue<string>(item["EmailAddress"]),
                            State = SafeValue<string>(item["StateName"]),
                            Country = SafeValue<string>(item["CountryName"]),
                            City = SafeValue<string>(item["City"]),
                            Phone = SafeValue<string>(item["Phone"]),
                            Zipcode = SafeValue<string>(item["ZipCode"]),
                            ExternalPMSId = SafeValue<string>(item["ExternalPMSId"])
                        });
                    }
                }
                return Lst;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("SmileBrandBLL--GetLocationList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Ankit Here 08-07-2018 : Changes for SBI-35 related.
        /// </summary>
        /// <param name="RequestType"></param>
        /// <param name="GUID"></param>
        /// <returns></returns>
        public static IDictionary<int, string> InsertDPMS_SyncDetails(string RequestType, string GUID = null)
        {
            try
            {
                Dictionary<int, string> Response = new Dictionary<int, string>();
                DPMS_SyncDetails DPMS = new DPMS_SyncDetails();
                DPMS.RequestType = RequestType;
                DPMS.RequestTime = SafeValue<DateTime>(SqlDateTime.MinValue.ToString());
                // GUID Convert issue  safeValue Function 
                DPMS.GUID = (string.IsNullOrWhiteSpace(GUID)) ? SafeValue<string>(Guid.NewGuid()) : GUID;
                DPMS.IsFromSBI = true;
                Response.Add(clsSmileBrand.InsertDPMS_SyncDetails(DPMS), DPMS.GUID);
                return Response;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("SimleBrandBLL---InsertDPMS_SyncDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This method will manage the response of SBI once the procedure will inserted on SBI they will send us the response of it.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public static RetriveResponse<bool> ProcedureResponse(DPMS_ProcedureResponse Obj)
        {
            try
            {
                RetriveResponse<bool> Retrieve = new RetriveResponse<bool>();
                int RequestId = 0;
                if (!string.IsNullOrWhiteSpace(Obj.RequestId))
                {
                    RequestId = clsColleaguesData.GetPMSSyncDetailsByGUID(Obj.RequestId);
                    bool Result = clsPatientsData.DPMSResponseUpdate(Obj.Result, RequestId, true);
                    Retrieve.Message = "";
                    Retrieve.IsSuccess = true;
                    Retrieve.Result = true;
                    Retrieve.StatusCode = 200;
                    return Retrieve;
                }
                else
                {
                    Retrieve.Message = "Invalid RequestId.";
                    Retrieve.IsSuccess = false;
                    Retrieve.StatusCode = 404;
                    return Retrieve;
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---ProcedureResponse", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static RetriveResponse<List<PatientAppointments>> GetPatientAppointments(PatientProcedure<PatientAppointmentDateFilter> Obj, int UserId)
        {
            try
            {
                RetriveResponse<List<PatientAppointments>> Retrieve = new RetriveResponse<List<PatientAppointments>>();
                string RequestGUID = string.Empty;
                int ApptRequestId = 0;
                int TotalRecord = 0;
                List<PatientAppointments> Lst = new List<PatientAppointments>();
                if (SafeValue<DateTime>(Obj.FromDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    Obj.FromDate = null;
                }
                if (SafeValue<DateTime>(Obj.ToDate).ToShortDateString() == DateTime.MinValue.ToShortDateString())
                {
                    Obj.ToDate = null;
                }
                if (string.IsNullOrEmpty(Obj.RequestId))
                {
                    if (Obj.PageIndex > 1)
                    {
                        return new RetriveResponse<List<PatientAppointments>>()
                        {
                            IsSuccess = false,
                            Message = "In order to use pagination, RequestId parameter is mandatory",
                            Result = null,
                            StatusCode = 400
                        };
                    }
                    int GetCountOfRecords = clsSmileBrand.GetCountofPatientAppointment(Obj, UserId);
                    if (GetCountOfRecords > BO.Constatnt.Common.CountOverLoad)
                    {
                        Retrieve.IsSuccess = false;
                        Retrieve.Message = $"The maximum number of records that can be returned by this API call is {BO.Constatnt.Common.CountOverLoad} rows. Your request has created a result set of {GetCountOfRecords} rows. Please modify your query to return a result set of less than {BO.Constatnt.Common.CountOverLoad} rows";
                        Retrieve.RequestId = RequestGUID;
                        Retrieve.TotalRecord = GetCountOfRecords;
                        Retrieve.Result = null;
                        Retrieve.StatusCode = 413;
                        return Retrieve;
                    }

                    IDictionary<int, string> Response = InsertDPMS_SyncDetails("APPOINTMENTS_SBI");
                    ApptRequestId = Response.Keys.FirstOrDefault();
                    RequestGUID = Response.Values.FirstOrDefault();

                    bool Result = clsSmileBrand.InsertDMPSAppointmentProcessQueue(Obj, UserId, ApptRequestId);
                }
                else
                {
                    ApptRequestId = clsColleaguesData.GetPMSSyncDetailsByGUID(Obj.RequestId);
                }

                if (ApptRequestId > 0)
                {
                    DataTable dt = clsSmileBrand.GetPatientAppointments(ApptRequestId, Obj.FetchRecordCount, Obj.PageIndex);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            Lst.Add(new PatientAppointments()
                            {
                                RL_Id = SafeValue<int>(item["RL_Id"]),
                                PMSAppointmentId = SafeValue<int>(item["PMSAppointmentId"]),
                                RLPatientId = SafeValue<int>(item["RLPatientId"]),
                                PMSPatientId = SafeValue<int>(item["PMSPatientId"]) == 0 ? (int?)null : SafeValue<int>(item["PMSPatientId"]),
                                RLProviderId = SafeValue<int>(item["RLProviderId"]),
                                PMSProviderId = SafeValue<string>(item["PMSProviderId"]),
                                PMSOperatoryId = SafeValue<string>(item["PMSOperatoryId"]),
                                PMSAdaCode = SafeValue<string>(item["PMSAdaCode"]) ,
                                PMSTxProcedure = SafeValue<string>(item["PMSTxProcedure"]),
                                AppointmentDateTime = SafeValue<DateTime>(item["AppointmentDateTime"]),
                                PMSAppointmentReason = SafeValue<string>(item["PMSAppointmentReason"]),
                                PMSAutoModifiedTimeStamp = SafeValue<DateTime>(item["PMSAutoModifiedTimeStamp"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(item["PMSAutoModifiedTimeStamp"]),
                                PMSAmount = SafeValue<double>(item["PMSAmount"]) == 0 ? (double?)null : SafeValue<double>(item["PMSAmount"]),
                                PMSAmountOverriden = SafeValue<bool>(item["PMSAmountOverriden"]) == false ? (bool?)null : SafeValue<bool>(item["PMSAmountOverriden"]),
                                IsDeleted = SafeValue<bool>(item["IsDeleted"]),
                                PMSStaffId = SafeValue<string>(item["PMSStaffId"]),
                                PMSBrokenDate = SafeValue<DateTime>(item["PMSBrokenDate"]) == DateTime.MinValue ? (DateTime?)null : SafeValue<DateTime>(item["PMSBrokenDate"])
                            });
                            TotalRecord = SafeValue<int>(item["TotalRecord"]);
                        }
                        Retrieve.IsSuccess = true;
                        Retrieve.Message = "Success!";
                        Retrieve.RequestId = RequestGUID;
                        Retrieve.TotalRecord = TotalRecord;
                        Retrieve.Result = Lst;
                        Retrieve.StatusCode = 200;
                    }
                    else
                    {
                        Retrieve.IsSuccess = false;
                        Retrieve.Message = "Not Found!";
                        Retrieve.RequestId = RequestGUID;
                        Retrieve.TotalRecord = TotalRecord;
                        Retrieve.Result = Lst;
                        Retrieve.StatusCode = 404;
                    }
                }
                return Retrieve;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("SmileBrandBLL--GetPatientProcedures", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This Method will write back the response on RL database which is given by SmileBrand
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public static RetriveResponse<bool> PaymentResponse(DMPS_PaymentResponse Obj)
        {
            try
            {
                bool Result = false;
                Request RequestIds = new Request();
                RetriveResponse<bool> Retrieve = new RetriveResponse<bool>();
                if (!string.IsNullOrWhiteSpace(Obj.RequestId))
                {
                    RequestIds = clsSmileBrand.GetPMSSyncDetailsByGUID(Obj.RequestId);
                    switch (Obj.RequestType)
                    {
                        case PatientPaymentType.DentrixFinanceCharges:
                            Result = clsPatientsData.DPMSResponseUpdate(Obj.Result, RequestIds.FinanceRequestId, true);
                            break;
                        case PatientPaymentType.DentrixAdjustments:
                            Result = clsPatientsData.DPMSResponseUpdate(Obj.Result, RequestIds.AdjustmentRequestId, true);
                            break;
                        case PatientPaymentType.DentrixInsurancePayment:
                            Result = clsPatientsData.DPMSResponseUpdate(Obj.Result, RequestIds.InsuranceRequestId, true);
                            break;
                        case PatientPaymentType.DentrixStandardPayment:
                            Result = clsPatientsData.DPMSResponseUpdate(Obj.Result, RequestIds.StandardRequestId, true);
                            break;
                    }
                    Retrieve.Message = "";
                    Retrieve.IsSuccess = true;
                    Retrieve.Result = true;
                    Retrieve.StatusCode = 200;
                    return Retrieve;
                }
                else
                {
                    Retrieve.Message = "Invalid RequestId.";
                    Retrieve.IsSuccess = false;
                    Retrieve.StatusCode = 404;
                    return Retrieve;
                }

            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("PatientRewardBLL---PaymentResponse", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

    }
}
