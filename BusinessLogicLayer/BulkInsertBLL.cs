﻿using System;
using DataAccessLayer;

namespace BusinessLogicLayer
{
    public static class BulkInsertBLL
    {
        public static bool BulkInsertProcedures()
        {
           return BulkInsertDAL.BulkInsertProcedures();
        }

        public static bool BulkInsertPatientAdjustments()
        {
            return BulkInsertDAL.BulkInsertPatientAdjustments();
        }

        public static bool BulkInsertPatientDentrixInsurance()
        {
            return BulkInsertDAL.BulkInsertPatientDentrixInsurance();
        }

        public static bool BulkInsertPatientDentrixStandardPayment()
        {
            return BulkInsertDAL.BulkInsertPatientDentrixStandardPayment();
        }

        public static bool BulkInsertPatientDentrixFinanceCharges()
        {
            return BulkInsertDAL.BulkInsertPatientDentrixFinanceCharges();
        }

        public static bool BulkInsertPatientDentrixAccountAging()
        {
            return BulkInsertDAL.BulkInsertPatientDentrixAccountAging();
        }
        public static bool BulkInsertPatientAppointment()
        {
            return BulkInsertDAL.BulkInsertPatientAppointment();
        }
        public static bool InsuranceData()
        {
            return BulkInsertDAL.InsuranceData();
        }
        
    }
}
