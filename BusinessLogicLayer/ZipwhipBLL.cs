﻿using BO.Models;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Common;
using static DataAccessLayer.Common.clsCommon;
using Newtonsoft.Json;

namespace BusinessLogicLayer
{
    public class ZipwhipBLL
    {
        ZipwhipDAL objDal = new ZipwhipDAL();

        /// <summary>
        /// Insert zipwhip provider config details in SMSIntegration table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int InsertProviderConfig(ZipwhipAccount model)
        {
            return objDal.InsertProviderConfig(model);
        }

        /// <summary>
        /// Insert zipwhip provider config details in SMSIntegration table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<ZipwhipAccount> ListOfProviderConfig(int UserId)
        {
            DataTable dt = new DataTable();
            List<ZipwhipAccount> lst = new List<ZipwhipAccount>();
            dt = objDal.ListOfProviderConfig(UserId);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow p in dt.Rows)
                {
                    ZipwhipAccount obj = new ZipwhipAccount();
                    obj.RefId = SafeValue<int>(p["Id"]);
                    obj.UserId = SafeValue<int>(p["UserId"]);
                    obj.LocationId = SafeValue<int>(p["LocationId"]);
                    obj.Location = SafeValue<string>(p["Location"]);
                    obj.IsActive = SafeValue<bool>(p["IsActive"]);
                    obj.ProviderConfig = JsonConvert.DeserializeObject<ProviderConfig>(SafeValue<string>(p["ProviderConfig"]));
                    lst.Add(obj);
                }
            }
            return lst;

        }

        /// <summary>
        /// Insert zipwhip provider config details in SMSIntegration table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public int SendMessage(SMSDetails model)
        {
            return objDal.SendMessage(model);
        }


        public List<SMSDetails> ListOfSMSHistory(int UserId)
        {
            DataTable dt = new DataTable();
            List<SMSDetails> lst = new List<SMSDetails>();
            dt = objDal.ListOfSMSHistory(UserId);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow p in dt.Rows)
                {
                    SMSDetails obj = new SMSDetails();
                    obj.Id = SafeValue<int>(p["Id"]);
                    obj.UserId = SafeValue<int>(p["UserId"]);
                    obj.SenderType = SafeValue<string>(p["SenderType"]);
                    obj.SMSIntegrationMasterId = SafeValue<int>(p["SMSIntegrationMasterId"]);
                    obj.ReceiverId = SafeValue<int>(p["ReceiverId"]);
                    obj.ReceiverType = SafeValue<string>(p["ReceiverType"]);
                    obj.RecepientNumber = SafeValue<string>(p["RecepientNumber"]);
                    obj.MessageType = SafeValue<int>(p["MessageType"]);
                    obj.MessageBody = SafeValue<string>(p["MessageBody"]);
                    obj.ResponseBody = SafeValue<string>(p["ResponseBody"]);
                    obj.DateCreated = SafeValue<DateTime>(p["DateCreated"]);
                    obj.Status = SafeValue<int>(p["STATUS"]);
                    obj.RetryCount = SafeValue<int>(p["RetryCount"]);
                    obj.DateSent = SafeValue<DateTime>(p["DateSent"]);
                    lst.Add(obj);
                }
            }
            return lst;

        }

        public int FindDentistId(string PhoneNumber, string Type)
        {
            return objDal.FindDentistId(PhoneNumber, Type);
        }

        public List<ZipwhipAccount> AssignZipwhipToken()
        {
            DataTable dt = new DataTable();
            dt = objDal.AssignZipwhipToken();

            List<ZipwhipAccount> lst = new List<ZipwhipAccount>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow p in dt.Rows)
                {
                    ZipwhipAccount obj = new ZipwhipAccount();
                    obj.RefId = SafeValue<int>(p["Id"]);
                    obj.UserId = SafeValue<int>(p["UserId"]);
                    obj.LocationId = SafeValue<int>(p["LocationId"]);
                    obj.ProviderConfigJson = SafeValue<string>(p["ProviderConfig"]);
                    obj.IsActive = SafeValue<bool>(p["IsActive"]);

                    obj.ProviderConfig = JsonConvert.DeserializeObject<ProviderConfig>(SafeValue<string>(p["ProviderConfig"]));
                    lst.Add(obj);
                }
            }
            return lst;
        }

        public List<ZipwhipAccount> CheckZipwhipAccountActivated(int UserId, int LocationId)
        {
            DataTable dt = new DataTable();
            dt = objDal.CheckZipwhipAccountActivated( UserId,  LocationId);

            List<ZipwhipAccount> model = new List<ZipwhipAccount>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow p in dt.Rows)
                {
                     ZipwhipAccount  obj = new ZipwhipAccount();
                    obj.RefId = SafeValue<int>(p["Id"]);
                    obj.UserId = SafeValue<int>(p["UserId"]);
                    obj.LocationId = SafeValue<int>(p["LocationId"]);
                    obj.ProviderConfigJson = SafeValue<string>(p["ProviderConfig"]);
                    obj.IsActive = SafeValue<bool>(p["IsActive"]);
                    obj.ProviderConfig = JsonConvert.DeserializeObject<ProviderConfig>(SafeValue<string>(p["ProviderConfig"]));
                    model.Add(obj);
                }
            }
            return model;
        }

        public List<LocationList> GetDentistLocationList(int UserId)
        {
            try
            {
                List<LocationList> List = new List<LocationList>();
                DataTable Dt = objDal.GetDentistLocationList(UserId);
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    foreach (DataRow item in Dt.Rows)
                    {
                        List.Add(new LocationList()
                        {
                            AddressInfoId = SafeValue<int>(item["AddressInfoId"]),
                            ContactType = SafeValue<int>(item["ContactType"]),
                            ExactAddress = SafeValue<string>(item["ExactAddress"]),
                            Location = SafeValue<string>(item["Location"]),
                            UserId = SafeValue<int>(item["UserId"]),
                            UserType = SafeValue<int>(item["UserType"])
                        });
                    }
                }
                return List;
            }
            catch (Exception Ex)
            {
                //ObjCommon.InsertErrorLog("SettingsBLL--GetDentistLocationList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }



    }
}
