﻿using BO.Models;
using BO.ViewModel;
using DataAccessLayer.Appointment;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using DataAccessLayer.PatientsData;
using DataAccessLayer.PatientsData.Schema;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using static BO.Enums.Common;
using static DataAccessLayer.Common.clsCommon;

namespace BusinessLogicLayer
{

    public class MessagesBLL
    {
        public clsPatientFileData _data = new clsPatientFileData(new DataAccessLayer.PatientsData.Schema.PatientContext(ConfigurationManager.ConnectionStrings["CONNECTIONSTRING"].ConnectionString));
        clsCommon ObjCommonData = new clsCommon();
        clsColleaguesData ObjColleagueData = new clsColleaguesData();
        clsPatientsData ObjPatientData = new clsPatientsData();
        private clsTemplate ObjTemplate = new clsTemplate();
        private clsCommon ObjCommon = new clsCommon();

        /// <summary>
        /// Get Colleagues List of Doctor
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="SearchText"></param>
        /// <param name="ColleagueId"></param>
        /// <returns></returns>
        public List<ColleagueData> GetColleagueList(int UserId, string SearchText, string ColleagueId = null)
        {
            try
            {
                List<ColleagueData> objcolleagueList = new List<ColleagueData>();
                DataTable dt = new DataTable();
                dt = ObjColleagueData.GetColleaguesListForAutoComplete(UserId, SearchText, ColleagueId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objcolleagueList = (from p in dt.AsEnumerable()
                                        select new ColleagueData
                                        {
                                            id = SafeValue<int>(p["ColleagueId"]),
                                            text = SafeValue<string>(p["doctorname"]) + (!string.IsNullOrEmpty(SafeValue<string>(p["AccountName"])) || !string.IsNullOrEmpty(SafeValue<string>(p["LocationName"])) ? " (" : "") + (!string.IsNullOrEmpty(SafeValue<string>(p["AccountName"])) ? (SafeValue<string>(p["AccountName"])) : "") + (!string.IsNullOrEmpty(SafeValue<string>(p["AccountName"])) && !string.IsNullOrEmpty(SafeValue<string>(p["LocationName"])) ? " - " : "") + (!string.IsNullOrEmpty(SafeValue<string>(p["LocationName"])) ? (SafeValue<string>(p["LocationName"])) : "") + (!string.IsNullOrEmpty(SafeValue<string>(p["AccountName"])) || !string.IsNullOrEmpty(SafeValue<string>(p["LocationName"])) ? ")" : "")
                                        }).ToList();
                    // MemberList.Where(t => t.UserId == Selected).Any();

                }
                return objcolleagueList;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--GetColleagueList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Patinet List of Doctor
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="SearchText"></param>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public List<PatientsData> GetPatientList(int UserId, string SearchText, string PatientId = null)
        {
            try
            {
                List<PatientsData> objpatientList = new List<PatientsData>();
                DataTable dt = new DataTable();
                dt = ObjPatientData.GetPatientListForAutoComplete(UserId, SearchText, PatientId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objpatientList = (from p in dt.AsEnumerable()
                                      select new PatientsData
                                      {
                                          id = SafeValue<int>(p["PatientId"]),
                                          text = SafeValue<string>(p["patientName"])
                                      }).ToList();
                    //PatientList.Where(t => t.PatientId == Selected).Any();
                }
                return objpatientList;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--GetPatientList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Compose Message Details
        /// </summary>
        /// <param name="MessageId"></param>
        /// <param name="MessageTypeId"></param>
        /// <param name="UserId"></param>
        /// <param name="Issent"></param>
        /// <returns></returns>
        public ComposeMessage GetCommopseMessgeDetails(int MessageId, int MessageTypeId, int UserId, bool Issent = false)
        {
            ComposeMessage objcompose = new ComposeMessage();
            objcompose.MessageId = MessageId;
            objcompose.MessageTypeId = MessageTypeId;
            objcompose.MessageId = MessageId;
            DataTable dt = new DataTable();
            dt = ObjColleagueData.GetMessageDetails(MessageId);
            if (dt != null && dt.Rows.Count > 0)
            {

                switch (MessageTypeId)
                {
                    case (int)BO.Enums.Common.ComposeMessageType.Draft:
                        objcompose.PatientId = SafeValue<string>(dt.Rows[0]["AttachedPatientIds"]);
                        objcompose.ColleagueId = SafeValue<string>(dt.Rows[0]["DraftUserId"]);
                        objcompose.MessageBody = Uri.UnescapeDataString(SafeValue<string>(dt.Rows[0]["Body"]));
                        objcompose.ColleagueList = GetColleagueList(UserId, null, objcompose.ColleagueId);
                        objcompose.PatientList = GetPatientList(UserId, null, objcompose.PatientId);
                        objcompose.IsColleaguesMessage = 1;
                        break;
                    case (int)BO.Enums.Common.ComposeMessageType.Replay:
                        if (SafeValue<int>(dt.Rows[0]["MessageTypeId"]) == 2)
                        {
                            objcompose.PatientId = SafeValue<string>(dt.Rows[0]["PatientId"]);
                            objcompose.ColleagueId = SafeValue<string>(dt.Rows[0]["ReferedUserId"]);
                        }
                        else
                        {
                            objcompose.PatientId = SafeValue<string>(dt.Rows[0]["AttachedPatientIds"]);
                            objcompose.ColleagueId = SafeValue<string>(dt.Rows[0]["ReferedUserId"]);
                            if (string.IsNullOrWhiteSpace(SafeValue<string>(objcompose.ColleagueId)))
                            {
                                objcompose.ColleagueId = SafeValue<string>(dt.Rows[0]["SenderId"]);
                            }
                            if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["ReferedUserId"])))
                            {
                                objcompose.ColleagueId = SafeValue<string>(dt.Rows[0]["SenderId"]);
                            }
                            else
                            {
                                objcompose.ColleagueId = (!Issent) ? SafeValue<string>(dt.Rows[0]["SenderId"]) : SafeValue<string>(dt.Rows[0]["ReferedUserId"]);
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["ReferedUserId"])))
                        {
                            if (UserId == Convert.ToUInt32(dt.Rows[0]["ReferedUserId"]))
                            {
                                objcompose.ColleagueId = SafeValue<string>(dt.Rows[0]["SenderId"]);
                            }
                            else
                            {
                                objcompose.ColleagueId = (!Issent) ? SafeValue<string>(dt.Rows[0]["SenderId"]) : SafeValue<string>(dt.Rows[0]["ReferedUserId"]);
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(objcompose.PatientId)) { objcompose.PatientList = GetPatientList(UserId, null, objcompose.PatientId); } else { objcompose.PatientList = new List<PatientsData>(); }
                        objcompose.MessageId = MessageId;
                        objcompose.ColleagueList = GetColleagueList(UserId, null, objcompose.ColleagueId);
                        objcompose.IsColleaguesMessage = 1;
                        break;
                    case (int)BO.Enums.Common.ComposeMessageType.Forward:
                        if (SafeValue<int>(dt.Rows[0]["MessageTypeId"]) == 2)
                        {
                            objcompose.PatientId = SafeValue<string>(dt.Rows[0]["PatientId"]);
                            //objcompose.ColleagueId = SafeValue<string>(dt.Rows[0]["SenderId"]);
                        }
                        else
                        {
                            objcompose.PatientId = SafeValue<string>(dt.Rows[0]["AttachedPatientIds"]);
                            // objcompose.ColleagueId = SafeValue<string>(dt.Rows[0]["ReferedUserId"]);
                        }
                        //objcompose.MessageId = 0;
                        objcompose.PatientId = SafeValue<string>(dt.Rows[0]["AttachedPatientIds"]);
                        objcompose.ColleagueList = new List<ColleagueData>();
                        objcompose.PatientList = GetPatientList(UserId, null, objcompose.PatientId);
                        objcompose.MessageBody = Uri.UnescapeDataString(SafeValue<string>(dt.Rows[0]["Body"]));
                        objcompose.ForwardFileId = string.Join(",", GetForwardFileId(MessageId, (int)BO.Enums.Common.ComposeType.ColleagueCompose));
                        objcompose.IsColleaguesMessage = 1;
                        break;

                }
            }
            return objcompose;


        }

        /// <summary>
        /// Get Forwraded Files Basied on Message Id
        /// </summary>
        /// <param name="MessageId"></param>
        /// <param name="ComposeType"></param>
        /// <returns></returns>
        public List<string> GetForwardFileId(int MessageId, int ComposeType)
        {
            List<string> objfileuploadId = new List<string>();
            try
            {
                DataTable dt = new DataTable();
                dt = ObjColleagueData.GetForwardAttachementDetails(MessageId, ComposeType);
                if (dt != null && dt.Rows.Count > 0)
                {

                    foreach (DataRow item in dt.Rows)
                    {
                        objfileuploadId.Add(SafeValue<string>(item["Id"]));
                    }

                }
                return objfileuploadId;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--GetAttachmentFileofMessage", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Plain message Details By MessageId
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="MessageId"></param>
        /// <param name="MessageTypeId"></param>
        /// <param name="TimeZoneSystemName"></param>
        /// <returns></returns>
        public List<NormalMessageDetails> GetPlainMessageBodyByMessageId(int UserId, int MessageId, int MessageTypeId, string TimeZoneSystemName)
        {
            DataSet ds = new DataSet();
            List<NormalMessageDetails> MessageDetailList = new List<NormalMessageDetails>();
            try
            {

                DataTable Dt = new DataTable();
                if (MessageTypeId == 0)
                {
                    Dt = ObjColleagueData.GetInBoxPatientMessageDetailsById(UserId, MessageId);
                    if (Dt != null && Dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in Dt.Rows)
                        {
                            string Body = Uri.UnescapeDataString(SafeValue<string>(item["Body"]));
                            string CreationDate = string.Empty;
                            if (!string.IsNullOrEmpty(SafeValue<string>(item["CreationDate"])))
                            {
                                CreationDate = SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(item["CreationDate"]), TimeZoneSystemName));
                            }

                            NormalMessageDetails ObjMessageDetails = new NormalMessageDetails();
                            DataTable dts = new DataTable();
                            ObjMessageDetails.MessageId = SafeValue<int>(item["MessageId"]);
                            ObjMessageDetails.SenderId = SafeValue<int>(item["Senderid"]);
                            ObjMessageDetails.From = SafeValue<string>(item["FromField"]);
                            ObjMessageDetails.To = SafeValue<string>(item["ToField"]);
                            ObjMessageDetails.CreationDate = CreationDate;
                            ObjMessageDetails.Body = Body;
                            ObjMessageDetails.SenderEmail = SafeValue<string>(item["SenderEmail"]);
                            ObjMessageDetails.ReceiverEmail = SafeValue<string>(item["ReceiverEmail"]);
                            ObjMessageDetails.IsPatientMessage = true;
                            ObjMessageDetails.IsSentMessage = false;
                            DataTable dtAttachments = new DataTable();

                            dtAttachments = ObjColleagueData.GetPatientMessageAttachemntsById(SafeValue<int>(item["MessageId"]));

                            List<MessageAttachment> lstAttachment = new List<MessageAttachment>();
                            foreach (DataRow Attachment in dtAttachments.Rows)
                            {
                                MessageAttachment ObjMessageAttachment = new MessageAttachment();
                                ObjMessageAttachment.MessageId = ObjMessageDetails.MessageId;
                                ObjMessageAttachment.AttachmentId = int.Parse(!string.IsNullOrWhiteSpace(SafeValue<string>(Attachment["Id"])) ? SafeValue<string>(Attachment["Id"]) : "0");
                                ObjMessageAttachment.AttachmentName = SafeValue<string>(Attachment["AttachmentName"]);
                                lstAttachment.Add(ObjMessageAttachment);
                            }

                            ObjMessageDetails.lstMessageAttachment = lstAttachment;

                            MessageDetailList.Add(ObjMessageDetails);
                        }
                    }
                }
                else
                {
                    ds = ObjColleagueData.GetInboxMessageDetailsById(UserId, MessageId);
                }
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        string Body = Uri.UnescapeDataString(SafeValue<string>(item["Body"]));
                        string CreationDate = string.Empty;
                        string ScheduledDate = string.Empty;
                        if (!string.IsNullOrEmpty(SafeValue<string>(item["CreationDate"])))
                        {
                            CreationDate = SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(item["CreationDate"]), TimeZoneSystemName));
                        }

                        if (!string.IsNullOrEmpty(SafeValue<string>(item["ScheduledDate"])))
                        {
                            ScheduledDate = SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(item["ScheduledDate"]), TimeZoneSystemName));
                        }

                        NormalMessageDetails ObjMessageDetails = new NormalMessageDetails();
                        ObjMessageDetails.MessageId = SafeValue<int>(item["MessageId"]);
                        ObjMessageDetails.SenderId = SafeValue<int>(item["Senderid"]);
                        ObjMessageDetails.From = SafeValue<string>(item["FromField"]);
                        ObjMessageDetails.To = SafeValue<string>(item["ToField"]);
                        ObjMessageDetails.CreationDate = CreationDate;
                        ObjMessageDetails.ScheduledDate = ScheduledDate;
                        ObjMessageDetails.Body = Body;
                        ObjMessageDetails.SenderEmail = SafeValue<string>(item["SenderEmail"]);
                        ObjMessageDetails.ReceiverEmail = SafeValue<string>(item["ReceiverEmail"]);
                        ObjMessageDetails.Status = SafeValue<bool>(item["Status"]);
                        ObjMessageDetails.MsgStatus = SafeValue<int>(item["Status"]);
                        ObjMessageDetails.IsPatientMessage = false;
                        ObjMessageDetails.IsSentMessage = false;
                        if (!string.IsNullOrEmpty(SafeValue<string>(item["AttachedPatientIds"])))
                        {
                            ObjMessageDetails.AttachedPatientsId = SafeValue<string>(item["AttachedPatientIds"]);
                            if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                            {
                                ObjMessageDetails.lstAttachedPatient.Add(new AttachedPatientDetails()
                                {
                                    AttachedPatientId = SafeValue<int>(ds.Tables[1].Rows[0]["PatientId"]),
                                    AttachedPatientFirstName = SafeValue<string>(ds.Tables[1].Rows[0]["FirstName"]),
                                    AttachedPatientLastName = SafeValue<string>(ds.Tables[1].Rows[0]["LastName"])
                                });
                            }
                            else
                            {
                                ObjMessageDetails.lstAttachedPatient = new List<AttachedPatientDetails>();
                            }
                        }

                        DataTable dtAttachments = new DataTable();
                        // Datatable 
                        if (MessageTypeId == 0)
                        {
                            dtAttachments = ObjColleagueData.GetPatientMessageAttachemntsById(SafeValue<int>(item["MessageId"]));
                        }
                        else
                        {
                            dtAttachments = ObjColleagueData.GetColleageMessageAttachemntsById(SafeValue<int>(item["MessageId"]));
                        }

                        List<MessageAttachment> lstAttachment = new List<MessageAttachment>();
                        foreach (DataRow Attachment in dtAttachments.Rows)
                        {
                            MessageAttachment ObjMessageAttachment = new MessageAttachment();
                            ObjMessageAttachment.MessageId = ObjMessageDetails.MessageId;
                            ObjMessageAttachment.MessageStatus = ObjMessageDetails.Status;
                            ObjMessageAttachment.AttachmentId = int.Parse(!string.IsNullOrWhiteSpace(SafeValue<string>(Attachment["Id"])) ? SafeValue<string>(Attachment["Id"]) : "0");
                            ObjMessageAttachment.AttachmentName = SafeValue<string>(Attachment["AttachmentName"]);
                            lstAttachment.Add(ObjMessageAttachment);

                        }

                        ObjMessageDetails.lstMessageAttachment = lstAttachment;

                        MessageDetailList.Add(ObjMessageDetails);
                    }
                }
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--GetPlainMessageBodyByMessageId", Ex.Message, Ex.StackTrace);
                throw;
            }
            return MessageDetailList;
        }

        /// <summary>
        /// Get SentBox message Details BY MessageId
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="MessageId"></param>
        /// <param name="MessageTypeId"></param>
        /// <param name="TimeZoneSystemName"></param>
        /// <returns></returns>
        public List<NormalMessageDetails> GetSentBoxMessageDetailsByMessageId(int UserId, int MessageId, int MessageTypeId, string TimeZoneSystemName)
        {
            List<NormalMessageDetails> MessageDetailList = new List<NormalMessageDetails>();
            try
            {
                DataSet dss = new DataSet();
                DataTable dt = new DataTable();
                if (MessageTypeId == 3)
                {
                    dt = ObjColleagueData.GetSentboxPatientMessageDetailsById(UserId, MessageId);
                    foreach (DataRow item in dt.Rows)
                    {
                        string Body = Uri.UnescapeDataString(SafeValue<string>(item["Body"]));
                        NormalMessageDetails ObjMessageDetails = new NormalMessageDetails();
                        DataTable dts = new DataTable();
                        ObjMessageDetails.MessageId = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(item["MessageId"])) ? 0 : item["MessageId"]);
                        ObjMessageDetails.SenderId = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(item["Senderid"])) ? 0 : item["Senderid"]);
                        ObjMessageDetails.From = SafeValue<string>(item["FromField"]);
                        ObjMessageDetails.To = SafeValue<string>(item["ToField"]);
                        ObjMessageDetails.CreationDate = SafeValue<string>(item["CreationDate"]);
                        ObjMessageDetails.Body = Body;
                        ObjMessageDetails.SenderEmail = SafeValue<string>(item["SenderEmail"]);
                        ObjMessageDetails.ReceiverEmail = SafeValue<string>(item["ReceiverEmail"]);
                        ObjMessageDetails.Status = SafeValue<bool>(item["Status"]);
                        ObjMessageDetails.MsgStatus = SafeValue<int>(item["Status"]);
                        ObjMessageDetails.IsPatientMessage = true;
                        ObjMessageDetails.IsSentMessage = true;
                        DataTable dtAttachments = new DataTable();

                        dtAttachments = ObjColleagueData.GetPatientMessageAttachemntsById(SafeValue<int>(item["MessageId"]));

                        List<MessageAttachment> lstAttachment = new List<MessageAttachment>();
                        foreach (DataRow Attachment in dtAttachments.Rows)
                        {
                            MessageAttachment ObjMessageAttachment = new MessageAttachment();
                            ObjMessageAttachment.MessageId = ObjMessageDetails.MessageId;
                            ObjMessageAttachment.AttachmentId = int.Parse(!string.IsNullOrWhiteSpace(SafeValue<string>(Attachment["Id"])) ? SafeValue<string>(Attachment["Id"]) : "0");
                            ObjMessageAttachment.AttachmentName = SafeValue<string>(Attachment["AttachmentName"]);
                            ObjMessageAttachment.AttachementFullName = SafeValue<string>(Attachment["AttachmentName"]);
                            //ObjMessageAttachment.AttachmentName = 
                            lstAttachment.Add(ObjMessageAttachment);
                        }

                        ObjMessageDetails.lstMessageAttachment = lstAttachment;

                        if (!string.IsNullOrEmpty(ObjMessageDetails.CreationDate))
                        {
                            ObjMessageDetails.CreationDate = clsHelper.ConvertFromUTC(SafeValue<DateTime>(ObjMessageDetails.CreationDate), TimeZoneSystemName).ToString("MM/dd/yyy hh:mm tt");
                        }

                        MessageDetailList.Add(ObjMessageDetails);
                    }
                }
                else
                {
                    dss = ObjColleagueData.GetSentboxMessageDetailsById(UserId, MessageId);
                }

                if (dss != null && dss.Tables.Count > 0)
                {
                    foreach (DataRow item in dss.Tables[0].Rows)
                    {
                        string body = Uri.UnescapeDataString(SafeValue<string>(item["Body"]));
                        NormalMessageDetails ObjMessageDetails = new NormalMessageDetails();
                        ObjMessageDetails.MessageId = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(item["MessageId"])) ? 0 : item["MessageId"]);
                        ObjMessageDetails.SenderId = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(item["Senderid"])) ? 0 : item["Senderid"]);
                        ObjMessageDetails.From = SafeValue<string>(item["FromField"]);
                        ObjMessageDetails.To = SafeValue<string>(item["ToField"]);
                        ObjMessageDetails.CreationDate = SafeValue<string>(item["CreationDate"]);
                        ObjMessageDetails.Body = body;
                        ObjMessageDetails.SenderEmail = SafeValue<string>(item["SenderEmail"]);
                        ObjMessageDetails.ReceiverEmail = SafeValue<string>(item["ReceiverEmail"]);
                        ObjMessageDetails.Status = SafeValue<bool>(item["Status"]);
                        ObjMessageDetails.MsgStatus = SafeValue<int>(item["Status"]);
                        ObjMessageDetails.IsPatientMessage = false;
                        ObjMessageDetails.IsSentMessage = true;
                        if (!string.IsNullOrEmpty(SafeValue<string>(item["AttachedPatientIds"])))
                        {
                            ObjMessageDetails.AttachedPatientsId = SafeValue<string>(item["AttachedPatientIds"]);
                            if (dss.Tables.Count > 1 && dss.Tables[1].Rows.Count > 0)
                            {
                                foreach (DataRow item1 in dss.Tables[1].Rows)
                                {
                                    ObjMessageDetails.lstAttachedPatient.Add(new AttachedPatientDetails()
                                    {
                                        AttachedPatientId = SafeValue<int>(item1["PatientId"]),
                                        AttachedPatientFirstName = SafeValue<string>(item1["FirstName"]),
                                        AttachedPatientLastName = SafeValue<string>(item1["LastName"])
                                    });
                                }
                            }
                            else
                            {
                                ObjMessageDetails.lstAttachedPatient = new List<AttachedPatientDetails>();
                            }
                        }

                        DataTable dtAttachments = new DataTable();
                        // Datatable 
                        if (MessageTypeId == 0)
                        {
                            dtAttachments = ObjColleagueData.GetPatientMessageAttachemntsById(SafeValue<int>(item["MessageId"]));
                        }
                        else
                        {
                            dtAttachments = ObjColleagueData.GetColleageMessageAttachemntsById(SafeValue<int>(item["MessageId"]));
                        }

                        List<MessageAttachment> lstAttachment = new List<MessageAttachment>();
                        foreach (DataRow Attachment in dtAttachments.Rows)
                        {
                            MessageAttachment ObjMessageAttachment = new MessageAttachment();
                            ObjMessageAttachment.MessageId = ObjMessageDetails.MessageId;
                            ObjMessageAttachment.AttachmentId = int.Parse(!string.IsNullOrWhiteSpace(SafeValue<string>(Attachment["Id"])) ? SafeValue<string>(Attachment["Id"]) : "0");
                            ObjMessageAttachment.AttachmentName = SafeValue<string>(Attachment["AttachmentName"]);
                            lstAttachment.Add(ObjMessageAttachment);

                        }

                        ObjMessageDetails.lstMessageAttachment = lstAttachment;
                        if (!string.IsNullOrEmpty(ObjMessageDetails.CreationDate))
                        {
                            ObjMessageDetails.CreationDate = clsHelper.ConvertFromUTC(SafeValue<DateTime>(ObjMessageDetails.CreationDate), TimeZoneSystemName).ToString("MM/dd/yyy hh:mm tt");
                        }

                        MessageDetailList.Add(ObjMessageDetails);
                    }
                }
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--GetSentBoxMessageDetailsByMessageId", Ex.Message, Ex.StackTrace);
                throw;
            }
            return MessageDetailList;
        }

        /// <summary>
        /// Download Files as Zip for Message
        /// </summary>
        /// <param name="DownId"></param>
        /// <param name="IsReferral"></param>
        /// <returns></returns>
        public DataTable GetDownloadZipFileDataForMessage(int DownId, bool IsReferral)
        {
            DataTable getFilenames = new DataTable();

            try
            {
                DataTable dt = new DataTable();
                dt = ObjColleagueData.GetColleageMessageAttachemntsById(DownId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    getFilenames.Columns.Add("FileName", System.Type.GetType("System.String"));
                    foreach (DataRow item in dt.Rows)
                    {
                        DataRow dr = getFilenames.NewRow();
                        dr["FileName"] = ConfigurationManager.AppSettings.Get("ImageBank") + SafeValue<string>(item["AttachmentName"]);
                        getFilenames.Rows.Add(dr);
                    }
                }
                else
                {
                    DataTable dts = new DataTable();
                    dts = ObjColleagueData.GetPatientMessageAttachemntsById(DownId);
                    if (dts != null && dts.Rows.Count > 0)
                    {
                        getFilenames.Columns.Add("FileName", System.Type.GetType("System.String"));
                        foreach (DataRow item in dts.Rows)
                        {
                            DataRow dr = getFilenames.NewRow();
                            dr["FileName"] = ConfigurationManager.AppSettings.Get("ImageBank") + SafeValue<string>(item["AttachmentName"]);
                            getFilenames.Rows.Add(dr);
                        }
                    }
                }

                return getFilenames;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--GetDownloadZipFileDataForMessage", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Download Single Attachments basied on MessageId
        /// </summary>
        /// <param name="ReferralCardId"></param>
        /// <param name="DocumentId"></param>
        /// <param name="IsDocument"></param>
        /// <returns></returns>
        public DataTable DownloadParticulareFileForReferral(int ReferralCardId, int DocumentId, bool IsDocument)
        {
            try
            {
                DataTable getFilenames = new DataTable();
                DataRow dr;
                if (IsDocument)
                {
                    DataTable dt = new DataTable();
                    dt = ObjColleagueData.GetPerticulerFileForDownloadReferral(ReferralCardId, DocumentId);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        getFilenames.Columns.Add("FileName", System.Type.GetType("System.String"));
                        foreach (DataRow item in dt.Rows)
                        {
                            dr = getFilenames.NewRow();
                            dr["FileName"] = SafeValue<string>(item["DocumentName"]);
                            getFilenames.Rows.Add(dr);
                        }
                    }
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt = ObjColleagueData.GetPerticulerImagesForDownloadReferral(ReferralCardId, DocumentId);
                    if (dt != null && dt.Rows.Count > 0)
                    {

                        getFilenames.Columns.Add("FileName", System.Type.GetType("System.String"));
                        foreach (DataRow item in dt.Rows)
                        {
                            dr = getFilenames.NewRow();
                            dr["FileName"] = SafeValue<string>(item["RelativePath"]);
                            getFilenames.Rows.Add(dr);
                        }
                    }
                }
                return getFilenames;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--DownloadParticulareFileForReferral", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Download all attachments as ZIP
        /// </summary>
        /// <param name="ReferralCardId"></param>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        public DataTable GetFileForDownloadReferralAllAttachment(int ReferralCardId, int PatientId)
        {
            DataTable getFilenames = new DataTable();
            DataRow dr;
            try
            {
                DataTable dtreferral = new DataTable();
                dtreferral = ObjColleagueData.GetRefferalDocumentAttachemntsById(ReferralCardId, PatientId);
                if (dtreferral != null && dtreferral.Rows.Count > 0)
                {
                    getFilenames.Columns.Add("FileName", System.Type.GetType("System.String"));
                    foreach (DataRow item in dtreferral.Rows)
                    {
                        dr = getFilenames.NewRow();
                        if (!(SafeValue<string>(item["DocumentName"]).Contains("../ImageBank/")))
                        {
                            dr["FileName"] = "../ImageBank/" + SafeValue<string>(item["DocumentName"]);
                        }
                        else
                        {
                            dr["FileName"] = SafeValue<string>(item["DocumentName"]);
                        }
                        getFilenames.Rows.Add(dr);
                    }
                }
                DataTable DTREFERRAL = new DataTable();
                DTREFERRAL = ObjColleagueData.GetRefferalImageById(ReferralCardId, PatientId);
                if (DTREFERRAL != null && DTREFERRAL.Rows.Count > 0)
                {
                    if (dtreferral == null || dtreferral.Rows.Count <= 0)
                    {
                        getFilenames.Columns.Add("FileName", System.Type.GetType("System.String"));
                    }
                    foreach (DataRow item in DTREFERRAL.Rows)
                    {

                        dr = getFilenames.NewRow();
                        if (!(SafeValue<string>(item["RelativePath"]).Contains("../ImageBank/")))
                        {
                            dr["FileName"] = "../ImageBank/" + SafeValue<string>(item["RelativePath"]);
                        }
                        else
                        {
                            dr["FileName"] = SafeValue<string>(item["RelativePath"]);
                        }

                        getFilenames.Rows.Add(dr);

                    }
                }

                return getFilenames;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--GetFileForDownloadReferralAllAttachment", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Attached File of Message
        /// </summary>
        /// <param name="MessageId"></param>
        /// <param name="Attachid"></param>
        /// <returns></returns>
        public DataTable GetAttachmentFileofMessage(int MessageId, int Attachid)
        {
            try
            {
                DataTable DT = new DataTable();
                DataTable dts = new DataTable();
                dts = ObjColleagueData.GetPatientAttachmentforMessage(MessageId, Attachid);
                if (dts != null && dts.Rows.Count > 0)
                {
                    DT.Columns.Add("FileName", System.Type.GetType("System.String"));
                    foreach (DataRow item in dts.Rows)
                    {
                        DataRow dr = DT.NewRow();
                        dr["FileName"] = ConfigurationManager.AppSettings.Get("ImageBank") + SafeValue<string>(item["AttachmentName"]);
                        DT.Rows.Add(dr);
                    }
                }
                else
                {
                    DataTable dt = new DataTable();
                    dt = ObjColleagueData.GetColleagueAttachmentforMessage(MessageId, Attachid);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        DT.Columns.Add("FileName", System.Type.GetType("System.String"));
                        foreach (DataRow item in dt.Rows)
                        {
                            DataRow dr = DT.NewRow();
                            dr["FileName"] = ConfigurationManager.AppSettings.Get("ImageBank") + SafeValue<string>(item["AttachmentName"]);
                            DT.Rows.Add(dr);
                        }
                    }
                }
                return DT;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--GetAttachmentFileofMessage", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public string ReferralCardId { get; set; }
        /// <summary>
        /// Get Referral message Details by Message Id
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="MessageId"></param>
        /// <param name="Type"></param>
        /// <param name="TimeZoneSystemName"></param>
        /// <returns></returns>
        public List<ReferralMessageDetails> GetReferralMessageBodyDetails(int UserId, int MessageId, string Type, string TimeZoneSystemName,bool IsFromPDF = false)
        {
            List<ReferralMessageDetails> lstReferralMessageDetails = new List<ReferralMessageDetails>();
            try
            {
                DataTable dt = new DataTable();
                dt = ObjColleagueData.GetReferralViewByID(MessageId, Type);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        ReferralMessageDetails ObjRefferalDetails = new ReferralMessageDetails();
                        string ReferralId = SafeValue<string>(item["ReferralCardId"]);
                        ReferralCardId = ReferralId;
                        if (!string.IsNullOrEmpty(ReferralId))
                        {
                            ObjRefferalDetails.ReferralCardId = SafeValue<int>(item["ReferralCardId"]);
                        }
                        if (Type == "Inbox")
                        {
                            ObjRefferalDetails.IsSent = false;
                        }
                        else
                        {
                            ObjRefferalDetails.IsSent = true;
                        }

                        ObjRefferalDetails.ServicesCount = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(item["ServicesCount"])) ? 0 : item["ServicesCount"]);
                        ObjRefferalDetails.InsuranceVerified = SafeValue<DateTime>(item["InsuranceVerified"]);
                        ObjRefferalDetails.MessageId = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(item["MessageId"])) ? 0 : item["MessageId"]);
                        ObjRefferalDetails.SenderId = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(item["SenderID"])) ? 0 : item["SenderID"]);
                        ObjRefferalDetails.ReceiverID = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(item["ReceiverID"])) ? 0 : item["ReceiverID"]);
                        ObjRefferalDetails.PatientId = SafeValue<int>(string.IsNullOrEmpty(SafeValue<string>(item["PatientId"])) ? 0 : item["PatientId"]);
                        ObjRefferalDetails.BoxFolderId = SafeValue<string>(string.IsNullOrEmpty(SafeValue<string>(item["BoxFolderId"])) ? "" : item["BoxFolderId"]);
                        if (!string.IsNullOrWhiteSpace(SafeValue<string>(item["SenderImage"])))
                        {
                            ObjRefferalDetails.SenderImage = SafeValue<string>(ConfigurationManager.AppSettings["DoctorImage"]) + SafeValue<string>(item["SenderImage"]);
                        }
                        else
                        {
                            ObjRefferalDetails.SenderImage = SafeValue<string>(ConfigurationManager.AppSettings["DefaultDoctorImage"]);
                        }
                        if (!string.IsNullOrWhiteSpace(SafeValue<string>(item["ReceiverImage"])))
                        {
                            ObjRefferalDetails.ReceiverImage = SafeValue<string>(ConfigurationManager.AppSettings["DoctorImage"]) + SafeValue<string>(item["ReceiverImage"]);
                        }
                        else
                        {
                            ObjRefferalDetails.ReceiverImage = SafeValue<string>(ConfigurationManager.AppSettings["DefaultDoctorImage"]);
                        }
                        if (!string.IsNullOrWhiteSpace(SafeValue<string>(item["ProfileImage"])))
                        {
                            ObjRefferalDetails.ProfileImage = SafeValue<string>(ConfigurationManager.AppSettings.Get("ImageBank")) + SafeValue<string>(item["ProfileImage"]);
                        }
                        else
                        {
                            if (SafeValue<string>(item["Gender"]) == "Female")
                            {
                                ObjRefferalDetails.ProfileImage = SafeValue<string>(ConfigurationManager.AppSettings.Get("DefaultDoctorFemaleImage"));
                            }
                            else
                            {
                                ObjRefferalDetails.ProfileImage = SafeValue<string>(ConfigurationManager.AppSettings.Get("DefaultDoctorImage"));
                            }
                        }



                        string strCreationDate = string.Empty;
                        DateTime CreationDate = SafeValue<DateTime>(item["CreationDate"]);
                        //if (!string.IsNullOrEmpty(SafeValue<string>(item["CreationDate"])))
                        //    CreationDate = SafeValue<string>(clsHelper.ConvertFromUTC(SafeValue<DateTime>(item["CreationDate"]), TimeZoneSystemName));
                        if (CreationDate != DateTime.MinValue)
                        {
                            strCreationDate = new CommonBLL().ConvertToDateTime(clsHelper.ConvertFromUTC(CreationDate, TimeZoneSystemName), (int)DateFormat.GENERAL);
                        }

                        string strUpdatedDate = string.Empty;
                        DateTime UpdatedDate = SafeValue<DateTime>(item["LastModifiedDate"]);
                        if (CreationDate != DateTime.MinValue)
                        {
                            strUpdatedDate = new CommonBLL().ConvertToDateTime(clsHelper.ConvertFromUTC(UpdatedDate, TimeZoneSystemName), (int)DateFormat.GENERAL);
                        }


                        DateTime DateOfBirth = SafeValue<DateTime>(item["DateOfBirth"]);
                        string strDateOfBirth = string.Empty;
                        if (DateOfBirth != DateTime.MinValue)
                        {
                            strDateOfBirth = new CommonBLL().ConvertToDate(DateOfBirth, (int)DateFormat.GENERAL);
                        }
                        ObjRefferalDetails.FullName = SafeValue<string>(item["FullName"]);
                        //Added for XQ1-67
                        ObjRefferalDetails.AssignedPatientId = SafeValue<string>(item["AssignedPatientId"]);
                        ObjRefferalDetails.AssignedPatientId = ObjRefferalDetails.AssignedPatientId == "0" ? "" : ObjRefferalDetails.AssignedPatientId;
                        ObjRefferalDetails.Phone = SafeValue<string>(item["Phone"]);
                        ObjRefferalDetails.City = SafeValue<string>(item["City"]);
                        ObjRefferalDetails.State = SafeValue<string>(item["State"]);
                        ObjRefferalDetails.State = ObjRefferalDetails.State == "0" ? "" : ObjRefferalDetails.State;
                        ObjRefferalDetails.Zipcode = SafeValue<string>(item["Zipcode"]);
                        //Added for XQ1-1021
                        ObjRefferalDetails.PatientStatus = SafeValue<bool>(item["PatientStatus"]);
                        //Added for XQ1-67
                        ObjRefferalDetails.Gender = SafeValue<string>(item["Gender"]);
                        ObjRefferalDetails.DateOfBirth = strDateOfBirth;
                        ObjRefferalDetails.SenderName = SafeValue<string>(item["SenderName"]);
                        ObjRefferalDetails.ReceiverName = SafeValue<string>(item["ReceiverName"]);
                        ObjRefferalDetails.FullAddress = SafeValue<string>(item["FullAddress"]);
                        ObjRefferalDetails.EmailAddress = SafeValue<string>(item["EmailAddress"]);
                        ObjRefferalDetails.Location = SafeValue<string>(item["ReferralLocation"]);
                        ObjRefferalDetails.RegardOption = SafeValue<string>(item["RegardOption"]);
                        ObjRefferalDetails.RequestingOption = SafeValue<string>(item["RequestingOption"]);
                        ObjRefferalDetails.OtherComments = SafeValue<string>(item["OtherComments"]);
                        ObjRefferalDetails.RequestComments = SafeValue<string>(item["RequestComments"]);
                        ObjRefferalDetails.Comments = SafeValue<string>(item["Comments"]);
                        ObjRefferalDetails.CreationDate = strCreationDate;
                        ObjRefferalDetails.PatientLastModifiedDate = strUpdatedDate;

                        if (!string.IsNullOrEmpty(ReferralId))
                        {
                            DataTable dtDocument = new DataTable();
                            // Datatable For Document
                            dtDocument = ObjColleagueData.GetRefferalDocumentAttachemntsById(SafeValue<int>(item["ReferralCardId"]), ObjRefferalDetails.PatientId);

                            List<MessageAttachment> lstDocument = new List<MessageAttachment>();
                            foreach (DataRow Document in dtDocument.Rows)
                            {
                                MessageAttachment ObjMessageAttachment = new MessageAttachment();
                                ObjMessageAttachment.MessageId = ObjRefferalDetails.MessageId;
                                ObjMessageAttachment.AttachmentId = int.Parse(!string.IsNullOrWhiteSpace(SafeValue<string>(Document["DocumentId"])) ? SafeValue<string>(Document["DocumentId"]) : "0");
                                ObjMessageAttachment.AttachmentName = SafeValue<string>(Document["DocumentName"]);
                                ObjMessageAttachment.IsDocument = true;
                                ObjMessageAttachment.ReferralCardId = SafeValue<int>(item["ReferralCardId"]);
                                lstDocument.Add(ObjMessageAttachment);
                            }
                            ObjRefferalDetails.lstDocuments = lstDocument;

                            // Datatable For Image
                            DataTable dtImage = new DataTable();
                            dtImage = ObjColleagueData.GetRefferalImageById(SafeValue<int>(item["ReferralCardId"]), ObjRefferalDetails.PatientId);
                            List<MessageAttachment> lstImage = new List<MessageAttachment>();
                            foreach (DataRow Image in dtImage.Rows)
                            {
                                MessageAttachment ObjMessageAttachment = new MessageAttachment();
                                ObjMessageAttachment.MessageId = ObjRefferalDetails.MessageId;
                                ObjMessageAttachment.AttachmentId = int.Parse(!string.IsNullOrWhiteSpace(SafeValue<string>(Image["ImageId"])) ? SafeValue<string>(Image["ImageId"]) : "0");
                                ObjMessageAttachment.AttachmentName = SafeValue<string>(Image["Name"]);
                                ObjMessageAttachment.AttachementFullName = ConfigurationManager.AppSettings.Get("ImageBank") + SafeValue<string>(Image["Name"]);
                                ObjMessageAttachment.IsDocument = false;
                                ObjMessageAttachment.ReferralCardId = SafeValue<int>(item["ReferralCardId"]);
                                lstImage.Add(ObjMessageAttachment);
                            }
                            ObjRefferalDetails.lstImages = lstImage;


                            //ToothType
                            DataTable dtToothTest = ObjColleagueData.GetReferralToothDetailsByReferralCardId(ObjRefferalDetails.ReferralCardId);
                            List<ToothList> lstToothList = new List<ToothList>();
                            ObjRefferalDetails.lstSelectedTeeths = new List<Int32>();
                            ObjRefferalDetails.lstSelectedPermanentTeeths = new List<Int32>();
                            ObjRefferalDetails.lstSelectedPrimaryTeeths = new List<Int32>();
                            foreach (DataRow ToothTest in dtToothTest.Rows)
                            {
                                ToothList ObjToothList = new ToothList();
                                ObjToothList.ID = int.Parse(!string.IsNullOrWhiteSpace(SafeValue<string>(ToothTest["ReferralDetailId"])) ? SafeValue<string>(ToothTest["ReferralDetailId"]) : "0");
                                ObjToothList.ToothType = SafeValue<int>(ToothTest["ToothType"]);
                                lstToothList.Add(ObjToothList);
                                if (SafeValue<int>(ToothTest["ToothType"]) <= 32)
                                {
                                    ObjRefferalDetails.lstSelectedPermanentTeeths.Add(SafeValue<int>(ToothTest["ToothType"]));
                                }
                                else
                                {
                                    ObjRefferalDetails.lstSelectedPrimaryTeeths.Add(SafeValue<int>(ToothTest["ToothType"]));
                                }
                                ObjRefferalDetails.lstSelectedTeeths.Add(SafeValue<int>(ToothTest["ToothType"]));
                            }
                            ObjRefferalDetails.lstToothList = lstToothList;
                        }
                        if (IsFromPDF)
                        {
                            ObjRefferalDetails.fileListModel = _data.Files(ObjRefferalDetails.MessageId, string.Empty, ObjRefferalDetails.PatientId);
                        }
                        lstReferralMessageDetails.Add(ObjRefferalDetails);
                    }
                }
                return lstReferralMessageDetails;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--GetReferralMessageBodyDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Insert/Update Draft Messages
        /// </summary>
        /// <param name="objcompose"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public int InsertUpdateDraftMessage(ComposeMessage objcompose, int UserId)
        {
            int Result = 0;
            try
            {
                switch (objcompose.ComposeType)
                {
                    case (int)BO.Enums.Common.ComposeType.ColleagueCompose:
                        Result = ObjColleagueData.InsertUpdateDraftMessage(objcompose.MessageId, UserId, objcompose.MessageBody, objcompose.PatientId, objcompose.ColleagueId, 1, UserId);
                        break;
                    case (int)BO.Enums.Common.ComposeType.PatientCompose:
                        Result = ObjPatientData.InsertUpdateDraftMessage(objcompose.MessageId, objcompose.MessageBody, objcompose.PatientId, UserId);
                        break;
                }

                return Result;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--InsertUpdateDraftMessage", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        /// <summary>
        /// Insert Draft Attachments
        /// </summary>
        /// <param name="MessageId"></param>
        /// <param name="FileName"></param>
        /// <param name="UserId"></param>
        /// <param name="AttachmentType"></param>
        /// <param name="ComposeType"></param>
        /// <returns></returns>
        public bool InsertDraftAttachments(int MessageId, string FileName, int UserId, int AttachmentType, int ComposeType)
        {
            bool Result = false;
            try
            {
                Result = ObjColleagueData.InsertDraftAttachments(MessageId, FileName, UserId, AttachmentType, ComposeType);
                return Result;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--InsertDraftAttachments", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        /// <summary>
        /// Get Referral Category Values
        /// </summary>
        /// <param name="ReferralCardId"></param>
        /// <returns></returns>
        public List<ReferralCategory> GetReferralCategoryValue(int ReferralCardId)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = ObjColleagueData.GetNewReferralMessageViewByRefCardId(ReferralCardId);
                List<ReferralCategory> LstNewrefView = new List<ReferralCategory>();
                if (dt != null && dt.Rows.Count != 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (SafeValue<int>(dt.Rows[i]["Sub_Sub_CatId"]) != 0)
                        {
                            LstNewrefView.Add(new ReferralCategory()
                            {
                                CategoryId = SafeValue<int>(SafeValue<string>(dt.Rows[i]["CategoryId"])),
                                CategoryName = SafeValue<string>(dt.Rows[i]["CategoryName"]),
                                SubCategoryId = SafeValue<int>(SafeValue<string>(dt.Rows[i]["Sub_CatId"])),
                                SubCategoryName = SafeValue<string>(dt.Rows[i]["SubCatName"]),
                                SubCategoryHeaderText = SafeValue<string>(dt.Rows[i]["HeaderText"]),
                                SubSubCategoryId = SafeValue<int>(SafeValue<string>(dt.Rows[i]["Sub_Sub_CatId"])),
                                SubSubCategoryName = SafeValue<string>(dt.Rows[i]["Sub_Sub_CatName"]),
                                SubSubCategoryHeaderText = SafeValue<string>(dt.Rows[i]["SubSubCatHeaderText"]),
                                SubSubCategoryValue = SafeValue<string>(dt.Rows[i]["Value"]),
                            });
                        }
                        else
                        {
                            LstNewrefView.Add(new ReferralCategory()
                            {
                                CategoryId = SafeValue<int>(SafeValue<string>(dt.Rows[i]["CategoryId"])),
                                CategoryName = SafeValue<string>(dt.Rows[i]["CategoryName"]),
                                SubCategoryId = SafeValue<int>(SafeValue<string>(dt.Rows[i]["Sub_CatId"])),
                                SubCategoryName = SafeValue<string>(dt.Rows[i]["SubCatName"]),
                                SubCategoryHeaderText = SafeValue<string>(dt.Rows[i]["HeaderText"]),
                                SubCategoryValue = SafeValue<string>(dt.Rows[i]["Value"]),
                                SubSubCategoryId = SafeValue<int>(SafeValue<string>(dt.Rows[i]["Sub_Sub_CatId"])),
                                SubSubCategoryName = SafeValue<string>(dt.Rows[i]["Sub_Sub_CatName"]),
                                SubSubCategoryHeaderText = SafeValue<string>(dt.Rows[i]["SubSubCatHeaderText"]),
                            });
                        }
                    }
                }
                return LstNewrefView;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--GetReferralMessageBodyDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Insert/Update Colleagues Messages
        /// </summary>
        /// <param name="Subject"></param>
        /// <param name="Body"></param>
        /// <param name="SenderId"></param>
        /// <param name="ColleagueId"></param>
        /// <param name="MessageType"></param>
        /// <param name="AttachedPatientIds"></param>
        /// <param name="status"></param>
        /// <param name="CreatedByUserId"></param>
        /// <param name="MessageId"></param>
        /// <returns></returns>
        public int InsertUpdateColleagueMessage(string Subject, string Body, int SenderId, int ColleagueId, int MessageType, string AttachedPatientIds, int status, int CreatedByUserId, int MessageId = 0)
        {
            int Result = 0;
            try
            {
                Result = ObjColleagueData.InsertUpdateColleagueMessage(Subject, Body, SenderId, ColleagueId, MessageType, AttachedPatientIds, status, CreatedByUserId, MessageId);
                return Result;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--InsertUpdateColleagueMessage", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        /// <summary>
        /// Manage Reply/Forwared Messages
        /// </summary>
        /// <param name="MessageId"></param>
        /// <param name="ReplyMessageId"></param>
        /// <param name="MessageTypeId"></param>
        /// <param name="ComposeTypeId"></param>
        /// <returns></returns>
        public bool ManageReplyForwardMessage(int MessageId, int ReplyMessageId, int MessageTypeId, int ComposeTypeId)
        {
            bool Result = false;
            try
            {
                Result = ObjColleagueData.InsertMessageAssociation(MessageId, ReplyMessageId, MessageTypeId, ComposeTypeId);
                return Result;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--ManageReplyForwardMessage", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        /// <summary>
        /// Insert Update Message Attachments of Colleagues Messages
        /// </summary>
        /// <param name="MessageId"></param>
        /// <param name="AttachemntName"></param>
        /// <returns></returns>
        public bool InsertMessageAttachemntsForColleague(int MessageId, string AttachemntName)
        {
            bool Result = false;
            try
            {
                Result = ObjColleagueData.InsertMessageAttachemntsForColleague(MessageId, AttachemntName);
                return Result;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--InsertMessageAttachemntsForColleague", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        /// <summary>
        /// Get Dentist Details
        /// </summary>
        /// <param name="DoctorId"></param>
        /// <returns></returns>
        public DoctorDetail GetDoctorDetail(int DoctorId)
        {
            try
            {
                DoctorDetail objDoctorDetail = new DoctorDetail();
                DataTable dt = new DataTable();
                dt = ObjColleagueData.GetDoctorDetails(DoctorId);
                if (dt.Rows.Count > 0)
                {
                    objDoctorDetail.DoctorName = SafeValue<string>(dt.Rows[0]["DoctorName"]);
                }
                return objDoctorDetail;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--GetDoctorDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Get Patient Details
        /// </summary>
        /// <param name="PatientId"></param>
        /// <param name="DoctorId"></param>
        /// <returns></returns>
        public PatientDetail GetPatientDetail(int PatientId, int DoctorId)
        {
            try
            {
                DataSet ds = new DataSet();
                PatientDetail objpatientdetail = new PatientDetail();
                ds = ObjPatientData.GetPateintDetailsByPatientId(PatientId, DoctorId);
                if (ds.Tables != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        objpatientdetail.PatientName = SafeValue<string>(ds.Tables[0].Rows[0]["FullName"]);
                        objpatientdetail.Email = SafeValue<string>(ds.Tables[0].Rows[0]["Email"]);
                    }
                }
                return objpatientdetail;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--GetDoctorDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Insert/Update Patient Messages
        /// </summary>
        /// <param name="FromField"></param>
        /// <param name="ToField"></param>
        /// <param name="Subject"></param>
        /// <param name="Body"></param>
        /// <param name="DoctorId"></param>
        /// <param name="PatientId"></param>
        /// <param name="IsSave"></param>
        /// <param name="IsDoctor"></param>
        /// <param name="IsPatient"></param>
        /// <param name="MessageId"></param>
        /// <returns></returns>
        public int InsertUpdatePatientMessages(string FromField, string ToField, string Subject, string Body, int DoctorId, int PatientId, bool IsSave, bool IsDoctor, bool IsPatient, int MessageId = 0)
        {
            int Result = 0;
            try
            {
                Result = ObjPatientData.InsertUpdatePatientMessages(FromField, ToField, "Subject", Body, DoctorId, SafeValue<int>(PatientId), false, true, false, MessageId);

            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--Insert_PatientMessages", Ex.Message, Ex.StackTrace);

            }
            return Result;
        }

        /// <summary>
        /// Add Montage Details
        /// </summary>
        /// <param name="MontageName"></param>
        /// <param name="MonatageType"></param>
        /// <param name="PatientId"></param>
        /// <param name="OwnerId"></param>
        /// <param name="OwnerType"></param>
        /// <returns></returns>
        public int AddMontage(string MontageName, int MonatageType, int PatientId, int OwnerId, int OwnerType)
        {
            int Result = 0;
            try
            {
                Result = ObjPatientData.AddMontage(MontageName, 0, SafeValue<int>(PatientId), SafeValue<int>(OwnerId), 0);

            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--AddMontage", Ex.Message, Ex.StackTrace);

            }
            return Result;
        }

        /// <summary>
        /// Add Document of Montage
        /// </summary>
        /// <param name="MontageId"></param>
        /// <param name="DocumentName"></param>
        /// <param name="uploadby"></param>
        /// <returns></returns>
        public int AddDocumenttoMontage(int MontageId, string DocumentName, string uploadby)
        {
            int Result = 0;
            try
            {
                clsCommon objcommon = new clsCommon();
                Result = objcommon.AddDocumenttoMontage(MontageId, DocumentName, uploadby);

            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--AddMontage", Ex.Message, Ex.StackTrace);

            }
            return Result;
        }

        /// <summary>
        /// Insert Attachments of Patient Message
        /// </summary>
        /// <param name="MessageId"></param>
        /// <param name="AttachemntName"></param>
        /// <returns></returns>
        public bool InsertMessageAttachemntsForPatient(int MessageId, string AttachemntName)
        {
            bool Result = false;
            try
            {
                Result = ObjPatientData.InsertMessageAttachemntsForPatient(MessageId, AttachemntName);
                return Result;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--InsertMessageAttachemntsForPatient", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        public int AddImagetoPatient(int PatientId, int TimePoint, string Name, int Height, int Width, int ImageFormat, int ImageType, string ImageFullPath, string RelativePath, int OwnerId, int OwnerType, int ImageStatus, string Uploadby)
        {
            int Result = 0;
            try
            {

                Result = ObjPatientData.AddImagetoPatient(SafeValue<int>(PatientId), 1, Name, 1000, 1000, 0, 500, ImageFullPath, RelativePath, SafeValue<int>(OwnerId), 0, 1, "Patient");

            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--AddImagetoPatient", Ex.Message, Ex.StackTrace);

            }
            return Result;
        }
        public bool AddImagetoMontage(int ImagId, int PositioninMontage, int MontageId)
        {
            bool Result = false;
            try
            {
                Result = ObjPatientData.AddImagetoMontage(ImagId, PositioninMontage, MontageId);
                return Result;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--AddImagetoMontage", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        public ComposeMessage GetPatientCommopseMessgeDetails(int MessageId, int MessageTypeId, int UserId)
        {
            ComposeMessage objcompose = new ComposeMessage();
            objcompose.MessageId = MessageId;
            objcompose.MessageTypeId = MessageTypeId;
            objcompose.MessageId = MessageId;
            DataTable dt = new DataTable();
            dt = ObjColleagueData.GetPatientMessageDetails(MessageId);
            if (dt != null && dt.Rows.Count > 0)
            {

                switch (MessageTypeId)
                {
                    case (int)BO.Enums.Common.ComposeMessageType.Draft:
                        objcompose.PatientId = SafeValue<string>(dt.Rows[0]["DraftPatientId"]);
                        objcompose.MessageBody = Uri.UnescapeDataString(SafeValue<string>(dt.Rows[0]["Body"]));
                        objcompose.PatientList = GetPatientList(UserId, null, objcompose.PatientId);
                        objcompose.IsColleaguesMessage = 0;
                        break;
                    case (int)BO.Enums.Common.ComposeMessageType.Replay:
                        objcompose.PatientId = SafeValue<string>(dt.Rows[0]["PatientId"]);
                        //objcompose.MessageBody = Uri.UnescapeDataString(SafeValue<string>(dt.Rows[0]["Body"]));
                        objcompose.PatientList = GetPatientList(UserId, null, objcompose.PatientId);
                        objcompose.MessageId = MessageId;
                        objcompose.IsColleaguesMessage = 0;
                        break;
                    case (int)BO.Enums.Common.ComposeMessageType.Forward:
                        //objcompose.PatientId = SafeValue<string>(dt.Rows[0]["PatientId"]);
                        //objcompose.PatientList = GetPatientList(UserId, null, objcompose.PatientId);
                        objcompose.PatientList = new List<PatientsData>();
                        objcompose.MessageBody = Uri.UnescapeDataString(SafeValue<string>(dt.Rows[0]["Body"]));
                        objcompose.ForwardFileId = string.Join(",", GetForwardFileId(MessageId, (int)BO.Enums.Common.ComposeType.PatientCompose));
                        objcompose.IsColleaguesMessage = 0;
                        break;
                }
            }
            return objcompose;


        }

        public int GetUnreadMessageCountOfDoctor(int UserId)
        {
            int Count = 0;
            try
            {
                clsColleaguesData clsColleagueData = new clsColleaguesData();
                DataTable dt = new DataTable();
                dt = clsColleagueData.GetUnreadedMessageCountOfDoctor(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    Count = SafeValue<int>(dt.Rows[0]["Counting"]);
                }
                return Count;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--GetUnreadMessageCountOfDoctor", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public bool UpdateMessageReadStatus(int? PatientMessageId, int? ColleagueMessageId)
        {
            bool Result = false;
            try
            {
                clsColleaguesData clsColleagueData = new clsColleaguesData();
                Result = clsColleagueData.UpdateReadMessageStatus(PatientMessageId, ColleagueMessageId);
                return Result;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--UpdateMessageReadStatus", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool AddPatientToReferredDoctor(int ColleagueId, int PatientId)
        {
            bool Result = false;
            try
            {
                clsColleaguesData clsColleagueData = new clsColleaguesData();
                Result = clsColleagueData.AddPatientToReferredDoctor(ColleagueId, PatientId);
                return Result;
            }
            catch (Exception Ex)
            {
                ObjCommonData.InsertErrorLog("MessagesBLL--AddPatientToReferredDoctor", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static List<OneClickReferralDetail> GetMessageConversation(FilterMessageConversation filterMessageConversation)
        {
            try
            {

                List<OneClickReferralDetail> oneClickReferralDetails = new List<OneClickReferralDetail>();
                DataTable Dts = clsCommon.GetDentistDetailsByAccessToken(filterMessageConversation.access_token);
                if (Dts.Rows.Count > 0)
                {
                    //Unread functionality
                    if (filterMessageConversation.MessageId > 0)
                    {
                        clsColleaguesData.UpdateMessageStatus(SafeValue<int>(filterMessageConversation.MessageId), SafeValue<int>(Dts.Rows[0]["UserId"]));
                    }
                    filterMessageConversation.UserId = SafeValue<int>(Dts.Rows[0]["UserId"]);
                    if (string.IsNullOrWhiteSpace(filterMessageConversation.TimeZoneSystemName))
                    {
                        filterMessageConversation.TimeZoneSystemName = clsColleaguesData.GetTimeZoneOfDoctor(filterMessageConversation.UserId);
                    }

                    if (filterMessageConversation.FromDate != null)
                    {
                        filterMessageConversation.FromDate = clsHelper.ConvertToUTC(SafeValue<DateTime>(filterMessageConversation.FromDate), filterMessageConversation.TimeZoneSystemName);
                    }
                    if (filterMessageConversation.Todate != null)
                    {
                        filterMessageConversation.Todate = clsHelper.ConvertToUTC(SafeValue<DateTime>(filterMessageConversation.Todate), filterMessageConversation.TimeZoneSystemName);
                    }
                    DataTable Dt = clsColleaguesData.GetMessageConversationalView(filterMessageConversation);
                    if (Dt != null && Dt.Rows.Count > 0)
                    {
                        string ImageName = string.Empty;
                        foreach (DataRow item in Dt.Rows)
                        {
                            ImageName = SafeValue<string>(item["ProfileImage"]);
                            string CreationDate = string.Empty;
                            if (SafeValue<DateTime>(item["ReferralDate"]) != DateTime.MinValue)
                            {
                                CreationDate = new CommonBLL().ConvertToDateTime(clsHelper.ConvertFromUTC(SafeValue<DateTime>(item["ReferralDate"]), filterMessageConversation.TimeZoneSystemName), (int)DateFormat.GENERAL);
                            }
                            string Body = SafeValue<string>(item["Body"]);
                            if (filterMessageConversation.MessageId != null && filterMessageConversation.MessageId > 0)
                            {
                                Body = Body.Replace("[CREATION_DATE]", CreationDate);
                            }

                            oneClickReferralDetails.Add(new OneClickReferralDetail()
                            {
                                
                                MessageId = SafeValue<int>(item["MessageId"]),
                                ReferralDate = CreationDate,
                                ReceiverDoctorName = SafeValue<string>(item["ReceiverDoctorName"]),
                                SenderDoctorName = SafeValue<string>(item["SenderDoctorName"]),
                                //Added for OCR dashboard
                                ReferringDoctorPhone = SafeValue<string>(item["ReferringDoctorPhone"]),
                                ReferringDoctorEmail = SafeValue<string>(item["ReferringDoctorEmail"]),
                                MessageBody = Body,
                                //Note = SafeValue<string>(item["Note"]),
                                //Added for OCR dashboard
                                MemberId = SafeValue<int>(item["TeamMemberId"]),
                                SenderId = SafeValue<int>(item["SenderId"]),
                                TeamMemberName = SafeValue<string>(item["TeamMemberName"]),
                                PatientName = SafeValue<string>(item["PatientName"]),
                                PatientId = SafeValue<int>(item["PatientId"]),
                                ReceiverId = SafeValue<int>(item["ReceiverId"]),
                                PatientStatus = SafeValue<bool>(item["PatientStatus"]),
                                Email = SafeValue<string>(item["PatientEmail"]),
                                PhoneNumber = SafeValue<string>(item["PatientPhone"]),
                                DOB = SafeValue<string>(item["DateOfBirth"]),
                                DentrixId = SafeValue<string>(item["DentrixId"]),
                                DispositionId = SafeValue<int>(item["Dispositionstatus"]),
                                ProfileImage = SafeValue<string>(item["ProfileImage"]) != string.Empty ? SafeValue<string>(ConfigurationManager.AppSettings["ImageBank"]) + SafeValue<string>(item["ProfileImage"]) : "",
                                DispositionList = new PatientReport().GetDispositionlist(),
                                DoctorId = SafeValue<int>(item["DoctorId"]),
                                Gender = SafeValue<string>(item["Gender"]) == "0" ? "Male" : "Female",
                                MessageStatus = SafeValue<string>(item["MessageStatus"]),
                                MessageFrom = SafeValue<string>(item["MessageFrom"]),
                                TotalCount = SafeValue<int>(item["TotalCount"]),
                                MessageTypeId = SafeValue<int>(item["MessageTypeId"]),
                                Status = SafeValue<int>(item["Status"]),
                                Subject = SafeValue<string>(item["Subject"]),
                                CreatedDate = SafeValue<DateTime>(item["CreationDate"]),
                                ReferralCardId = SafeValue<int>(item["ReferralCardId"]),
                                lstCategory = (SafeValue<int>(item["ReferralCardId"]) != 0) ? new MessagesBLL().GetReferralCategoryValue(SafeValue<int>(item["ReferralCardId"])) : null
                            });
                            foreach (var items in oneClickReferralDetails)
                            {

                                if (SafeValue<string>(items.Gender) == "Female")
                                {
                                    items.ProfileImage = SafeValue<string>(ConfigurationManager.AppSettings.Get("DefaultDoctorFemaleImage"));
                                }
                                else
                                {
                                    items.ProfileImage = SafeValue<string>(ConfigurationManager.AppSettings.Get("DefaultDoctorImage"));
                                }
                                items.CreatedDate = clsHelper.ConvertFromUTC(SafeValue<DateTime>(item["CreationDate"]), filterMessageConversation.TimeZoneSystemName);
                                items.CreationDateString = new CommonBLL().ConvertToDateTime(items.CreatedDate, (int)DateFormat.GENERAL);
                            }
                        }
                    }
                }
                return oneClickReferralDetails;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("MessagesBLL--GetMessageConversation", Ex.Message, Ex.StackTrace);
                throw;
            }
        }


        public static List<OneClickReferralDetail> GetCommunicationList(FilterMessageConversation filterMessageConversation)
        {
            try
            {

                List<OneClickReferralDetail> oneClickReferralDetails = new List<OneClickReferralDetail>();
                DataTable Dts = clsCommon.GetDentistDetailsByAccessToken(filterMessageConversation.access_token);
                if (Dts.Rows.Count > 0)
                {
                    //Unread functionality
                    if (filterMessageConversation.MessageId > 0)
                    {
                        clsColleaguesData.UpdateMessageStatus(SafeValue<int>(filterMessageConversation.MessageId), SafeValue<int>(Dts.Rows[0]["UserId"]));
                    }
                    filterMessageConversation.UserId = SafeValue<int>(Dts.Rows[0]["UserId"]);
                    if (string.IsNullOrWhiteSpace(filterMessageConversation.TimeZoneSystemName))
                    {
                        filterMessageConversation.TimeZoneSystemName = clsColleaguesData.GetTimeZoneOfDoctor(filterMessageConversation.UserId);
                    }

                    if (filterMessageConversation.FromDate != null)
                    {
                        filterMessageConversation.FromDate = clsHelper.ConvertToUTC(SafeValue<DateTime>(filterMessageConversation.FromDate), filterMessageConversation.TimeZoneSystemName);
                    }
                    if (filterMessageConversation.Todate != null)
                    {
                        filterMessageConversation.Todate = clsHelper.ConvertToUTC(SafeValue<DateTime>(filterMessageConversation.Todate), filterMessageConversation.TimeZoneSystemName);
                    }
                    DataTable Dt = clsColleaguesData.GetCommunicationList(filterMessageConversation);
                    if (Dt != null && Dt.Rows.Count > 0)
                    {
                        string ImageName = string.Empty;
                        foreach (DataRow item in Dt.Rows)
                        {
                            ImageName = SafeValue<string>(item["ProfileImage"]);
                            string CreationDate = string.Empty;
                            if (SafeValue<DateTime>(item["ReferralDate"]) != DateTime.MinValue)
                            {
                                CreationDate = new CommonBLL().ConvertToDateTime(clsHelper.ConvertFromUTC(SafeValue<DateTime>(item["ReferralDate"]), filterMessageConversation.TimeZoneSystemName), (int)DateFormat.GENERAL);
                            }
                            string ScheduleDate = string.Empty;
                            if (SafeValue<DateTime>(item["ScheduledDate"]) != DateTime.MinValue)
                            {
                                ScheduleDate = new CommonBLL().ConvertToDate(clsHelper.ConvertFromUTC(SafeValue<DateTime>(item["ScheduledDate"]), filterMessageConversation.TimeZoneSystemName), (int)DateFormat.GENERAL);
                            }
                            string Body = SafeValue<string>(item["Body"]);
                            string Note = SafeValue<string>(item["Note"]);
                            if (filterMessageConversation.MessageId != null && filterMessageConversation.MessageId > 0)
                            {
                                Body = Body.Replace("on [CREATION_DATE]", "");
                            }
                            if (!string.IsNullOrWhiteSpace(Note))
                            {
                                Note = Note.Replace("[Scheduled_Date]", ScheduleDate);
                            }
                            oneClickReferralDetails.Add(new OneClickReferralDetail()
                            {
                                MessageId = SafeValue<int>(item["MessageId"]),
                                ReferralDate = CreationDate,
                                ReceiverDoctorName = SafeValue<string>(item["ReceiverDoctorName"]),
                                SenderDoctorName = SafeValue<string>(item["SenderDoctorName"]),
                                //Added for OCR dashboard
                                ReferringDoctorPhone = SafeValue<string>(item["ReferringDoctorPhone"]),
                                ReferringDoctorEmail = SafeValue<string>(item["ReferringDoctorEmail"]),
                                MessageBody = Body,
                                Note = Note,
                                //Added for OCR dashboard
                                PatientName = SafeValue<string>(item["PatientName"]),
                                PatientId = SafeValue<int>(item["PatientId"]),
                                ReceiverId = SafeValue<int>(item["ReceiverId"]),
                                PatientStatus = SafeValue<bool>(item["PatientStatus"]),
                                Email = SafeValue<string>(item["PatientEmail"]),
                                PhoneNumber = SafeValue<string>(item["PatientPhone"]),
                                DOB = SafeValue<string>(item["DateOfBirth"]),
                                DentrixId = SafeValue<string>(item["DentrixId"]),
                                DispositionId = SafeValue<int>(item["Dispositionstatus"]),
                                ProfileImage = SafeValue<string>(item["ProfileImage"]) != string.Empty ? SafeValue<string>(ConfigurationManager.AppSettings["ImageBank"]) + SafeValue<string>(item["ProfileImage"]) : "",
                                DispositionList = new PatientReport().GetDispositionlist(),
                                DoctorId = SafeValue<int>(item["DoctorId"]),
                                Gender = SafeValue<string>(item["Gender"]) == "0" ? "Male" : "Female",
                                MessageStatus = SafeValue<string>(item["MessageStatus"]),
                                MessageFrom = SafeValue<string>(item["MessageFrom"]),
                                MessageTypeId = SafeValue<int>(item["MessageTypeId"]),
                                Status = SafeValue<int>(item["Status"]),
                                Subject = SafeValue<string>(item["Subject"]),
                                CreatedDate = SafeValue<DateTime>(item["CreationDate"]),
                                ReferralCardId = SafeValue<int>(item["ReferralCardId"]),
                                lstCategory = (SafeValue<int>(item["ReferralCardId"]) != 0) ? new MessagesBLL().GetReferralCategoryValue(SafeValue<int>(item["ReferralCardId"])) : null,
                                BoxFileId = SafeValue<string>(item["BoxFileId"]),
                                BoxFolderId = SafeValue<string>(item["BoxFolderId"]),
                                BoxFileName = SafeValue<string>(item["BoxFileName"])
                            });
                            foreach (var items in oneClickReferralDetails)
                            {

                                if (SafeValue<string>(items.Gender) == "Female")
                                {
                                    items.ProfileImage = SafeValue<string>(ConfigurationManager.AppSettings.Get("DefaultDoctorFemaleImage"));
                                }
                                else
                                {
                                    items.ProfileImage = SafeValue<string>(ConfigurationManager.AppSettings.Get("DefaultDoctorImage"));
                                }
                                items.CreatedDate = clsHelper.ConvertFromUTC(SafeValue<DateTime>(item["CreationDate"]), filterMessageConversation.TimeZoneSystemName);
                                items.CreationDateString = new CommonBLL().ConvertToDateTime(items.CreatedDate, (int)DateFormat.GENERAL);
                            }
                        }
                    }
                }
                return oneClickReferralDetails;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("MessagesBLL--GetMessageConversation", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static ReferralMessage referralMessageDetails(ReferralMessageFilter Obj)
        {
            try
            {
                ReferralMessage refer = new ReferralMessage();
                DataTable Dts = GetDentistDetailsByAccessToken(Obj.access_token);
                if (Dts.Rows.Count > 0)
                {
                    int UserId = SafeValue<int>(Dts.Rows[0]["UserId"]);
                    string TimeZoneName = new clsAppointmentData().GetTimeZoneOfDoctor(UserId);
                    refer.ReferralMessageList = new MessagesBLL().GetReferralMessageBodyDetails(UserId, Obj.MessageId, Obj.MessageType, TimeZoneName,Obj.IsFromPDF);
                    var Id = refer.ReferralMessageList.Select(item => item.ReferralCardId).Take(1).ToList();
                    int ReferralCardId = SafeValue<int>(Id[0]);
                    refer.ReferralCategory = new MessagesBLL().GetReferralCategoryValue(ReferralCardId);
                }
                return refer;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("MessagesBLL--referralMessageDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static NormalMessageDetails NormalMessageDetails(ReferralMessageFilter Obj)
        {
            try
            {
                NormalMessageDetails normal = new NormalMessageDetails();
                DataTable Dts = clsCommon.GetDentistDetailsByAccessToken(Obj.access_token);
                if (Dts.Rows.Count > 0)
                {
                    int UserId = SafeValue<int>(Dts.Rows[0]["UserId"]);
                    string TimeZoneName = new clsAppointmentData().GetTimeZoneOfDoctor(UserId);

                    if (Obj.MessageType == "Inbox")
                    {
                        var MyList = new MessagesBLL().GetPlainMessageBodyByMessageId(UserId, Obj.MessageId, 1, TimeZoneName).Take(1).ToList();
                        if (MyList != null && MyList.Count > 0)
                        {
                            normal = MyList[0];
                        }
                    }
                    else
                    {
                        var MyList = new MessagesBLL().GetSentBoxMessageDetailsByMessageId(UserId, Obj.MessageId, 1, TimeZoneName).Take(1).ToList();
                        if (MyList != null && MyList.Count > 0)
                        {
                            normal = MyList[0];
                        }
                    }
                }
                if (normal.MsgStatus == 2 && (!string.IsNullOrWhiteSpace(normal.Body)))
                {
                    normal.Body = normal.Body.Replace("[CREATION_DATE]", normal.CreationDate);
                    if (normal.Body.Contains("[Scheduled_Date]") && !string.IsNullOrWhiteSpace(normal.ScheduledDate))
                    {
                        normal.Body = normal.Body.Replace("[Scheduled_Date]", Convert.ToDateTime(normal.ScheduledDate).ToShortDateString());
                    }
                }
                return normal;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("MessagesBLL--NormalMessageDetails", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public bool SendMessageToColleagues(SendMessageToColleaguesRequest request)
        {
            bool Result = false;

            MessagesBLL objmessagebll = new MessagesBLL();
            CommonBLL ObjCommonbll = new CommonBLL();
            List<TempFileAttacheMents> objlistofattachements = new List<TempFileAttacheMents>();
            objlistofattachements = ObjCommonbll.GetTempAttachments(request.objcomposedetail.FileIds, request.objcomposedetail.MessageId, (int)ComposeType.ColleagueCompose);
            string[] ColleagueIds = SafeValue<string>(request.objcomposedetail.ColleagueId).Split(new char[] { ',' });
            if (ColleagueIds.Length > 0)
            {
                if (request.objcomposedetail.MessageId > 0)
                {
                    switch (request.objcomposedetail.MessageTypeId)
                    {
                        case (int)ComposeMessageType.Draft:
                            int i = 0;
                            foreach (var item in ColleagueIds)
                            {
                                int RecordId = 0;
                                if (i == 0)
                                {
                                    i++;
                                    RecordId = objmessagebll.InsertUpdateColleagueMessage("", request.objcomposedetail.MessageBody, request.UserId, SafeValue<int>(item), 1, request.objcomposedetail.PatientId, 0, request.UserId, request.objcomposedetail.MessageId);
                                    ManageAttachement(objlistofattachements, request, RecordId);
                                    ObjTemplate.SendMessageToColleague(request.UserId, SafeValue<int>(item), RecordId, 1);
                                    if (!string.IsNullOrEmpty(request.objcomposedetail.PatientId)) { AddPatientToReferredDoctor(SafeValue<int>(item), request.objcomposedetail.PatientId, request); }
                                    Result = true;
                                }
                                else
                                {
                                    RecordId = objmessagebll.InsertUpdateColleagueMessage("", request.objcomposedetail.MessageBody, request.UserId, SafeValue<int>(item), 1, request.objcomposedetail.PatientId, 0, request.UserId);

                                    ManageAttachement(objlistofattachements, request, RecordId);
                                    ObjTemplate.SendMessageToColleague(request.UserId, SafeValue<int>(item), RecordId, 1);
                                    if (!string.IsNullOrEmpty(request.objcomposedetail.PatientId)) { AddPatientToReferredDoctor(SafeValue<int>(item), request.objcomposedetail.PatientId, request); }
                                    Result = true;
                                }

                            }
                            break;
                        case (int)ComposeMessageType.Replay:
                            foreach (var item in ColleagueIds)
                            {
                                int RecordId = 0;
                                RecordId = objmessagebll.InsertUpdateColleagueMessage("", request.objcomposedetail.MessageBody, request.UserId, SafeValue<int>(item), 1, request.objcomposedetail.PatientId, 0, request.UserId);
                                //Ankit Solanki: 06/08-2019
                                //This code is added for Add attachments into the Office communciation section of Conversation view. 
                                if (!string.IsNullOrWhiteSpace(request.objcomposedetail.BoxFileId))
                                {
                                    clsPatientFileData clsPatientFileData = new clsPatientFileData(new PatientContext(ConfigurationManager.ConnectionStrings["CONNECTIONSTRING"].ConnectionString));
                                    clsPatientFileData.CreateMatrix(new FileModel()
                                    {
                                        RecordId = RecordId,
                                        FolderId = request.objcomposedetail.BoxFolderId,
                                        FileName = request.objcomposedetail.BoxFileName,
                                        FileId = request.objcomposedetail.BoxFileId,
                                        CreatedDate = DateTime.Now
                                    });
                                }
                                ManageAttachement(objlistofattachements, request, RecordId);
                                objmessagebll.ManageReplyForwardMessage(request.objcomposedetail.MessageId, RecordId, (int)ComposeMessageType.Replay, (int)ComposeType.ColleagueCompose);
                                ObjTemplate.SendMessageToColleague(request.UserId, SafeValue<int>(item), RecordId, 1);
                                if (!string.IsNullOrEmpty(request.objcomposedetail.PatientId)) { AddPatientToReferredDoctor(SafeValue<int>(item), request.objcomposedetail.PatientId, request); }
                                Result = true;
                            }
                            break;
                    }
                }
                else
                {
                    foreach (var item in ColleagueIds)
                    {
                        int RecordId = 0;
                        RecordId = objmessagebll.InsertUpdateColleagueMessage("", request.objcomposedetail.MessageBody, request.UserId, SafeValue<int>(item), 1, request.objcomposedetail.PatientId, 0, request.UserId);
                        ManageAttachement(objlistofattachements, request, RecordId);
                        ObjTemplate.SendMessageToColleague(request.UserId, SafeValue<int>(item), RecordId, 1);
                        if (!string.IsNullOrEmpty(request.objcomposedetail.PatientId)) { AddPatientToReferredDoctor(SafeValue<int>(item), request.objcomposedetail.PatientId, request); }
                        Result = true;
                    }
                }
                ObjCommonbll.DeleteAllAttachements(request.objcomposedetail.FileIds, request.objcomposedetail.MessageId);
                DeleteAttachmentFiles(objlistofattachements, request);
            }

            return Result;
        }

        public bool ForwardMessageToColleagues(SendMessageToColleaguesRequest request)
        {
            bool Result = false;
            MessagesBLL objmessagebll = new MessagesBLL();
            CommonBLL ObjCommonbll = new CommonBLL();
            List<TempFileAttacheMents> objlistofattachements = new List<TempFileAttacheMents>();
            objlistofattachements = ObjCommonbll.GetTempAttachments(request.objcomposedetail.FileIds, 0, (int)ComposeType.ColleagueCompose);
            List<TempFileAttacheMents> objlistofforwardattachements = new List<TempFileAttacheMents>();
            objlistofforwardattachements = ObjCommonbll.GetForwardAttachMentsById(request.objcomposedetail.ForwardFileId == null ? string.Empty : request.objcomposedetail.ForwardFileId, (int)ComposeType.ColleagueCompose);
            string[] ColleagueIds = SafeValue<string>(request.objcomposedetail.ColleagueId).Split(new char[] { ',' });
            if (ColleagueIds.Length > 0)
            {
                if (request.objcomposedetail.MessageId > 0)
                {
                    switch (request.objcomposedetail.MessageTypeId)
                    {
                        case (int)ComposeMessageType.Forward:
                            foreach (var item in ColleagueIds)
                            {
                                int RecordId = 0;
                                RecordId = objmessagebll.InsertUpdateColleagueMessage("", request.objcomposedetail.MessageBody, request.UserId, SafeValue<int>(item), 1, request.objcomposedetail.PatientId, 0, request.UserId);
                                ManageAttachement(objlistofattachements, request, RecordId);
                                foreach (var obj in objlistofforwardattachements)
                                {
                                    if (System.IO.File.Exists(request.ImageBankFileUpload + obj.FilePath))
                                    {
                                        objmessagebll.InsertMessageAttachemntsForColleague(RecordId, obj.FilePath);

                                    }

                                }
                                objmessagebll.ManageReplyForwardMessage(request.objcomposedetail.MessageId, RecordId, (int)ComposeMessageType.Forward, (int)ComposeType.ColleagueCompose);
                                ObjTemplate.SendMessageToColleague(request.UserId, SafeValue<int>(item), RecordId, 1);
                                if (!string.IsNullOrEmpty(request.objcomposedetail.PatientId)) { AddPatientToReferredDoctor(SafeValue<int>(item), request.objcomposedetail.PatientId, request); }
                                Result = true;
                            }
                            break;
                    }
                }
                ObjCommonbll.DeleteAllAttachements(request.objcomposedetail.FileIds, request.objcomposedetail.MessageId);
                DeleteAttachmentFiles(objlistofattachements, request);
            }
            return Result;
        }

        public bool ForwardSaveAsDraft(SendMessageToColleaguesRequest request)
        {
            MessagesBLL objmessagebll = new MessagesBLL();
            CommonBLL ObjCommonbll = new CommonBLL();
            int RecordId = 0;

            request.objcomposedetail.ComposeType = (int)ComposeType.ColleagueCompose;
            request.objcomposedetail.MessageId = 0;
            RecordId = objmessagebll.InsertUpdateDraftMessage(request.objcomposedetail, request.UserId);
            List<TempFileAttacheMents> objlistofattachements = new List<TempFileAttacheMents>();
            objlistofattachements = ObjCommonbll.GetTempAttachments(request.objcomposedetail.FileIds);
            foreach (var item in objlistofattachements)
            {
                if (System.IO.File.Exists(request.NewTempFileUpload + item.FilePath))
                {
                    System.IO.File.Move(request.NewTempFileUpload + item.FilePath, request.DraftFileUpload + item.FilePath);
                    objmessagebll.InsertDraftAttachments(RecordId, item.FilePath, request.UserId, SafeValue<int>(item.FileExtension), (int)ComposeType.ColleagueCompose);
                }

            }
            List<TempFileAttacheMents> objlistofforwardattachements = new List<TempFileAttacheMents>();
            objlistofforwardattachements = ObjCommonbll.GetForwardAttachMentsById(request.objcomposedetail.ForwardFileId == null ? string.Empty : request.objcomposedetail.ForwardFileId, (int)ComposeType.ColleagueCompose);
            foreach (var item in objlistofforwardattachements)
            {
                string NewFileName = SafeValue<string>(item.FileName);
                NewFileName = NewFileName.Substring(SafeValue<string>(NewFileName).LastIndexOf("$") + 1);
                NewFileName = SafeValue<string>("$" + DateTime.Now.Ticks + "$" + NewFileName);
                if (System.IO.File.Exists(request.ImageBankFileUpload + item.FilePath))
                {
                    if (!new CommonBLL().CheckFileIsValidImage(item.FilePath))
                    {
                        item.FileExtension = 1;
                    }
                    else
                    {
                        item.FileExtension = 0;
                    }
                    System.IO.File.Copy(request.ImageBankFileUpload + item.FilePath, request.DraftFileUpload + NewFileName);
                    objmessagebll.InsertDraftAttachments(RecordId, NewFileName, request.UserId, SafeValue<int>(item.FileExtension), (int)ComposeType.ColleagueCompose);
                }

            }
            ObjCommonbll.DeleteAllAttachements(request.objcomposedetail.FileIds);

            return RecordId > 0;
        }
        #region Private Helper Methods 
        private bool ManageAttachement(List<TempFileAttacheMents> objlistofattachements, SendMessageToColleaguesRequest request, int RecordId = 0)
        {
            CommonBLL ObjCommonbll = new CommonBLL();
            MessagesBLL objmessagebll = new MessagesBLL();
            bool Result = false;
            if (objlistofattachements.Count > 0 && RecordId > 0)
            {
                foreach (var item in objlistofattachements)
                {
                    string NewFileName = SafeValue<string>(item.FilePath);
                    NewFileName = NewFileName.Substring(SafeValue<string>(NewFileName).LastIndexOf("$") + 1);
                    NewFileName = SafeValue<string>("$" + DateTime.Now.Ticks + "$" + NewFileName);
                    switch (item.FileFrom)
                    {
                        case (int)FileFromFolder.Draft_DraftAttachments:
                            switch (item.FileExtension)
                            {
                                case (int)FileTypeExtension.File:
                                    if (System.IO.File.Exists(request.DraftFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Copy(request.DraftFileUpload + item.FilePath, request.ImageBankFileUpload + NewFileName);
                                        objmessagebll.InsertMessageAttachemntsForColleague(RecordId, NewFileName);
                                        Result = true;
                                    }
                                    break;
                                case (int)FileTypeExtension.Image:
                                    if (System.IO.File.Exists(request.DraftFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Copy(request.DraftFileUpload + item.FilePath, request.ImageBankFileUpload + NewFileName);
                                        using (Image image = Image.FromFile(request.DraftFileUpload + item.FilePath))
                                        {
                                            Size thumbnailSize = ObjCommon.GetThumbnailSize(image);
                                            using (Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width, thumbnailSize.Height, null, IntPtr.Zero))
                                            {

                                                thumbnail.Save(request.ThumbFileUpload + NewFileName);
                                            }
                                        }

                                        objmessagebll.InsertMessageAttachemntsForColleague(RecordId, NewFileName);
                                        Result = true;
                                    }
                                    break;
                            }
                            break;
                        case (int)FileFromFolder.TempFile_TempFolder:
                        case (int)FileFromFolder.TempImage_TempFolder:
                            switch (item.FileExtension)
                            {
                                case (int)FileTypeExtension.File:
                                    if (System.IO.File.Exists(request.NewTempFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Copy(request.NewTempFileUpload + item.FilePath, request.ImageBankFileUpload + NewFileName);
                                        objmessagebll.InsertMessageAttachemntsForColleague(RecordId, NewFileName);
                                        Result = true;
                                    }
                                    break;
                                case (int)FileTypeExtension.Image:
                                    if (System.IO.File.Exists(request.NewTempFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Copy(request.NewTempFileUpload + item.FilePath, request.ImageBankFileUpload + NewFileName);
                                        using (Image image = Image.FromFile(request.NewTempFileUpload + item.FilePath))
                                        {
                                            Size thumbnailSize = ObjCommon.GetThumbnailSize(image);
                                            using (Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width, thumbnailSize.Height, null, IntPtr.Zero))
                                            {
                                                thumbnail.Save(request.ThumbFileUpload + NewFileName);
                                            }
                                        }
                                        objmessagebll.InsertMessageAttachemntsForColleague(RecordId, NewFileName);
                                        Result = true;
                                    }
                                    break;
                            }

                            break;
                    }
                }
            }
            return Result;
        }
        private bool AddPatientToReferredDoctor(int ColleagueId, string AttachPatientIds, SendMessageToColleaguesRequest request)
        {
            bool Result = false;
            string[] PatientIds = SafeValue<string>(AttachPatientIds).Split(new char[] { ',' });
            MessagesBLL objmessagebll = new MessagesBLL();
            for (int i = 0; i < PatientIds.Length; i++)
            {
                Result = objmessagebll.AddPatientToReferredDoctor(SafeValue<int>(ColleagueId), SafeValue<int>(PatientIds[i]));
            }
            return Result;
        }

        private bool DeleteAttachmentFiles(List<TempFileAttacheMents> objlistofattachements, SendMessageToColleaguesRequest request)
        {
            bool Result = false;
            if (objlistofattachements.Count > 0)
            {
                foreach (var item in objlistofattachements)
                {
                    switch (item.FileFrom)
                    {
                        case (int)FileFromFolder.Draft_DraftAttachments:
                            switch (item.FileExtension)
                            {
                                case (int)FileTypeExtension.File:
                                    if (System.IO.File.Exists(request.DraftFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Delete(request.DraftFileUpload + item.FilePath);
                                        Result = true;
                                    }
                                    break;
                                case (int)FileTypeExtension.Image:
                                    if (System.IO.File.Exists(request.DraftFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Delete(request.DraftFileUpload + item.FilePath);
                                        Result = true;
                                    }
                                    break;
                            }
                            break;
                        case (int)FileFromFolder.TempFile_TempFolder:
                        case (int)FileFromFolder.TempImage_TempFolder:
                            switch (item.FileExtension)
                            {
                                case (int)FileTypeExtension.File:
                                    if (System.IO.File.Exists(request.NewTempFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Delete(request.NewTempFileUpload + item.FilePath);
                                        Result = true;
                                    }
                                    break;
                                case (int)FileTypeExtension.Image:
                                    if (System.IO.File.Exists(request.NewTempFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Delete(request.NewTempFileUpload + item.FilePath);
                                        Result = true;
                                    }
                                    break;
                            }

                            break;
                    }
                }
            }
            return Result;
        }
        #endregion 
        public static DownloadPDF GetReferralMessageDetailsForDownloadPDF(DownloadPDFFilter filter)
        {
            try
            {
                DownloadPDF downloadPDF = new DownloadPDF();
                #region Referral Message details.
                string TimeZoneName = new clsAppointmentData().GetTimeZoneOfDoctor(filter.UserId);
                downloadPDF.ReferralDetails.ReferralMessageList = new MessagesBLL().GetReferralMessageBodyDetails(filter.UserId, filter.MessageId, filter.MessageType, TimeZoneName);
                var Id = downloadPDF.ReferralDetails.ReferralMessageList.Select(item => item.ReferralCardId).Take(1).ToList();
                int ReferralCardId = SafeValue<int>(Id[0]);
                downloadPDF.ReferralDetails.ReferralCategory = new MessagesBLL().GetReferralCategoryValue(ReferralCardId);
                #endregion

                #region Office communication section
                FilterMessageConversation obj = GetObjectvalues(filter.MessageId, filter.UserId, TimeZoneName);
                downloadPDF.OfficeCommunication = GetCommunicationList(obj);
                #endregion

                #region Text Messenger
                downloadPDF.TextMessenger = new ZipwhipBLL().ListOfSMSHistory(filter.PatientId);
                #endregion

                #region Patient History
                downloadPDF.PatientHistory = PatientBLL.GetPatientDetails(filter.PatientId, filter.UserId, TimeZoneName);
                #endregion

                #region Appointments
                downloadPDF.FutureAppointments = AppointmentBLL.AppointmentlistforPatientHistory(filter.PatientId, 1, TimeZoneName);
                downloadPDF.PastAppointments = AppointmentBLL.AppointmentlistforPatientHistory(filter.PatientId, 0, TimeZoneName);
                #endregion
                return downloadPDF;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("MessagesBLL--GetReferralMessageDetailsForDownloadPDF", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public static FilterMessageConversation GetObjectvalues(int messageid, int UserId,string Timezone)
        {
            FilterMessageConversation Obj = new FilterMessageConversation();
            Obj.MessageId = messageid;
            Obj.UserId = UserId;
            Obj.TimeZoneSystemName = Timezone;
            return Obj;
        }
    }
    
}
