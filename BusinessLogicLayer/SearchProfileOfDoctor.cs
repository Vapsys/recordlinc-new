﻿using System;
using System.Collections.Generic;
using BO.ViewModel;
using BO.Models;
using System.Data;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using System.Web.Mvc;
using System.Configuration;
using System.Data.SqlClient;
using System.Web;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using static DataAccessLayer.Common.clsCommon;
namespace BusinessLogicLayer
{
    public class SearchProfileOfDoctor
    {
        public static clsColleaguesData objColleaguesData = new clsColleaguesData();
        public static clsCommon ObjCommon = new clsCommon();
        string DentistImage = SafeValue<string>(System.Configuration.ConfigurationManager.AppSettings.Get("DentistImages"));
        string DoctorImage = SafeValue<string>(System.Configuration.ConfigurationManager.AppSettings.Get("RLURL"));

        string DefaultDoctorImage = SafeValue<string>(System.Configuration.ConfigurationManager.AppSettings.Get("DefaultDoctorImage"));
        public List<DentistDetial> GetSearchResult(Search Obj)
        {
            try
            {
                DataTable ds = new DataTable();
                List<DentistDetial> Lst = new List<DentistDetial>();
                ds = objColleaguesData.GetDentistListSearch(Obj);
                if (ds != null && ds.Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Rows)
                    {
                        string ImageName = SafeValue<string>(item["ImageName"]);
                        if (string.IsNullOrWhiteSpace(ImageName))
                        {
                            ImageName = ConfigurationManager.AppSettings.Get("DoctorProfileImage");
                        }
                        else
                        {
                            ImageName = DoctorImage + ImageName;
                        }
                        Lst.Add(new DentistDetial()
                        {
                            FirstName = SafeValue<string>(item["FirstName"]),
                            LastName = SafeValue<string>(item["LastName"]),
                            UserId = SafeValue<int>(item["UserId"]),
                            AccountName = SafeValue<string>(item["AccountName"]),
                            Address2 = SafeValue<string>(item["Address2"]),
                            ExactAddress = SafeValue<string>(item["ExactAddress"]),
                            FullAddress = SafeValue<string>(item["FullAddress"]),
                            City = SafeValue<string>(item["City"]),
                            Country = SafeValue<string>(item["Country"]),
                            Email = SafeValue<string>(item["Email"]),
                            FullName = SafeValue<string>(item["name"]),
                            Gender = SafeValue<string>(item["Gender"]),
                            MapAddress = SafeValue<string>(item["MapAddress"]),
                            Image = SafeValue<string>(item["Image"]),
                            ImageName = ImageName,
                            PublicProfileName = SafeValue<string>(item["PublicPath"]),
                            Specialty = SafeValue<string>(item["Type"]),
                            State = SafeValue<string>(item["State"]),
                            UserName = SafeValue<string>(item["Username"]),
                            Zipcode = SafeValue<string>(item["ZipCode"]),
                            Count = SafeValue<int>(item["TotalRecord"])
                        });
                    }
                }
                return Lst;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SearchProfileOfDoctorBLL--GetSearchResult", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public List<SelectListItem> DoctorSpecialities()
        {
            DataTable dt = new DataTable();
            clsCommon objCommon = new clsCommon();
            dt = objCommon.GetAllSpeciality();
            List<SelectListItem> lst = new List<SelectListItem>();

            // lst.Add(new SelectListItem { Text = "Select Specialty", Value = "0" });


            foreach (DataRow item in dt.Rows)
            {
                lst.Add(new SelectListItem { Text = SafeValue<string>(item["Description"]), Value = SafeValue<string>(item["SpecialityId"]) });
            }
            return lst;
        }
        public List<DentistDetailsForPatientReward> GetSearchResultForPatientReward(Search Obj)
        {
            try
            {
                DataTable ds = new DataTable();
                List<DentistDetailsForPatientReward> Lst = new List<DentistDetailsForPatientReward>();
                ds = objColleaguesData.GetDentistListSearch(Obj);
                if (ds != null && ds.Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Rows)
                    {
                        string ImageName = SafeValue<string>(item["ImageName"]);
                        if (string.IsNullOrWhiteSpace(ImageName))
                        {
                            ImageName = ConfigurationManager.AppSettings.Get("DoctorProfileImage");
                        }
                        else
                        {
                            ImageName = DoctorImage + DentistImage + ImageName;
                        }
                        if (!string.IsNullOrWhiteSpace(SafeValue<string>(item["AccountId"])))
                        {
                            Lst.Add(new DentistDetailsForPatientReward()
                            {
                                FirstName = SafeValue<string>(item["FirstName"]),
                                LastName = SafeValue<string>(item["LastName"]),
                                UserId = SafeValue<int>(item["UserId"]),
                                Address2 = SafeValue<string>(item["Address2"]),
                                ExactAddress = SafeValue<string>(item["ExactAddress"]),
                                FullAddress = SafeValue<string>(item["FullAddress"]).Replace("<br />", string.Empty),

                                City = SafeValue<string>(item["City"]),
                                Country = SafeValue<string>(item["Country"]), 
                                Email = SafeValue<string>(item["Email"]),
                                FullName = SafeValue<string>(item["name"]), 
                                //Gender = SafeValue<string>(item["Gender"]), string.Empty),
                                MapAddress = SafeValue<string>(item["MapAddress"]),
                                Image = ImageName,
                                ImageName = SafeValue<string>(item["Image"]),
                                PublicProfileName = SafeValue<string>(item["PublicPath"]), 
                                Specialty = SafeValue<string>(item["Type"]),
                                State = SafeValue<string>(item["State"]),
                                UserName = SafeValue<string>(item["AccountName"]), 
                                Zipcode = SafeValue<string>(item["ZipCode"]),
                                AccountId = SafeValue<int>(item["AccountId"]),
                                Latitude = SafeValue<string>(item["LATITUDE"]), 
                                Longitude = SafeValue<string>(item["LONGITUDE"]),
                                WorkPhone = SafeValue<string>(item["Phone"]), 
                                HomePhone = SafeValue<string>(item["Phone2"]),
                                Mobile = SafeValue<string>(item["Mobile"]), 
                                RewardCompanyId = SafeValue<string>(item["RewardCompanyID"]), 
                                RewardLocationId = SafeValue<string>(item["RewardLocationId"]), 
                                ProviderType = (item["provtype"] is DBNull) ? BO.Enums.Common.PMSProviderType.Provider : (Convert.ToUInt32(item["provtype"]) == 0 || Convert.ToUInt32(item["provtype"]) == 1) ? BO.Enums.Common.PMSProviderType.Provider : BO.Enums.Common.PMSProviderType.Staff
                            });
                        }
                    }
                }
                return Lst;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SearchProfileOfDoctorBLL--GetSearchResult", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public static List<ColleagueData> GetDoctorList(Search Obj)
        {
            try
            {
                DataTable ds = new DataTable();
                List<ColleagueData> Lst = new List<ColleagueData>();
                ds = objColleaguesData.GetDentistListSearch(Obj);
                if (ds != null && ds.Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Rows)
                    {
                        Lst.Add(new ColleagueData()
                        {
                            id = SafeValue<int>(item["UserId"]),
                            text = SafeValue<string>(item["NAME"]) + (!string.IsNullOrEmpty(SafeValue<string>(item["AccountName"])) || !string.IsNullOrEmpty(SafeValue<string>(item["Location"])) ? " (" : "") + (!string.IsNullOrEmpty(SafeValue<string>(item["AccountName"])) ? (SafeValue<string>(item["AccountName"])) : "") + (!string.IsNullOrEmpty(SafeValue<string>(item["AccountName"])) && !string.IsNullOrEmpty(SafeValue<string>(item["Location"])) ? " - " : "") + (!string.IsNullOrEmpty(SafeValue<string>(item["Location"])) ? (SafeValue<string>(item["Location"])) : "") + (!string.IsNullOrEmpty(SafeValue<string>(item["AccountName"])) || !string.IsNullOrEmpty(SafeValue<string>(item["Location"])) ? ")" : ""),
                        });
                    }
                }
                return Lst;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SearchProfileOfDoctorBLL--GetDoctorList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Getting Dentists list based on Zip-code from database.
        /// </summary>
        /// <param name="ZipCode"></param>
        /// <param name="Mile"></param>
        /// <returns></returns>
        public List<DentistDetailsForPatientReward> GetSearchDentistByLocation(string ZipCode, int Mile,  BO.Enums.Common.PMSProviderFilterType provFilter)
        {
            try
            {
                TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
                DataTable ds = new DataTable();
                List<DentistDetailsForPatientReward> Lst = new List<DentistDetailsForPatientReward>();
                ds = objColleaguesData.GetDentistByLocation(ZipCode, Mile, provFilter);
                if (ds != null && ds.Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Rows)
                    {
                        string ImageName = SafeValue<string>(item["ImageName"]);
                        string FullAddress = SafeValue<string>(item["FullAddress"]);
                        if(!string.IsNullOrEmpty(FullAddress))
                        {
                            FullAddress = FullAddress.Replace("<br />", string.Empty);
                        }
                        if (string.IsNullOrWhiteSpace(ImageName))
                        {
                            ImageName = ConfigurationManager.AppSettings.Get("DoctorProfileImageWithMale");

                        }
                        else
                        {
                            ImageName = DoctorImage + HttpUtility.UrlEncode(ImageName);
                            ImageName = ImageName.Replace("+", "%20");
                        }
                        if (!string.IsNullOrWhiteSpace(SafeValue<string>(item["AccountId"])))
                        {
                            if (!string.IsNullOrEmpty(SafeValue<string>(item["AccountName"])))
                            {
                                item["AccountName"] = SafeValue<string>("(" + item["AccountName"] + ")");
                            }
                            Lst.Add(new DentistDetailsForPatientReward()
                            {
                                FirstName = SafeValue<string>(item["FirstName"]),
                                LastName = SafeValue<string>(item["LastName"]),
                                EncrypteUserId = ObjTripleDESCryptoHelper.encryptText(SafeValue<string>(item["UserId"])),
                                UserId = SafeValue<int>(item["UserId"]),
                                Address2 = SafeValue<string>(item["Address2"]),
                                ExactAddress = SafeValue<string>(item["ExactAddress"]),
                                FullAddress = FullAddress,
                                City = SafeValue<string>(item["City"]),
                                Country = SafeValue<string>(item["Country"]),
                                Email = SafeValue<string>(item["Email"]),
                                FullName = SafeValue<string>(item["name"]),
                                //Gender = ObjCommon.CheckNull(SafeValue<string>(item["Gender"]), string.Empty),
                                MapAddress = SafeValue<string>(item["MapAddress"]),
                                Image = ImageName,
                                ImageName = SafeValue<string>(item["Image"]),
                                PublicProfileName = SafeValue<string>(item["PublicPath"]),
                                Specialty = SafeValue<string>(item["Type"]),
                                State = SafeValue<string>(item["State"]),
                                UserName = SafeValue<string>(item["LoginUsername"]),
                                Zipcode = SafeValue<string>(item["ZipCode"]),
                                AccountId = SafeValue<int>(item["AccountId"]),
                                Latitude = SafeValue<string>(item["LATITUDE"]),
                                Longitude = SafeValue<string>(item["LONGITUDE"]),
                                WorkPhone = SafeValue<string>(item["Phone"]),
                                HomePhone = SafeValue<string>(item["Phone2"]),
                                Mobile = SafeValue<string>(item["Mobile"]),
                                //ProviderType = (item["provtype"] is DBNull) ? BO.Enums.Common.PMSProviderType.Provider : (SafeValue<int>(item["provtype"]) == 0 || SafeValue<int>(item["provtype"]) == 1) ? BO.Enums.Common.PMSProviderType.Provider : BO.Enums.Common.PMSProviderType.Staff,
                                ProviderType = SafeValue<BO.Enums.Common.PMSProviderType>(item["provtype"]),
                                AccountName = SafeValue<string>(item["AccountName"])

                            });
                        }
                    }
                }
                return Lst;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SearchProfileOfDoctorBLL--GetSearchResult", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Getting Zip-code based on passing Latitude and Longitude.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public DataTable GetZipCodeByLatitudeLongitude(Searchlatitudelongitude Obj)
        {
            try
            {
                DataTable ds = new DataTable();
                List<Searchlatitudelongitude> Lst = new List<Searchlatitudelongitude>();
                ds = objColleaguesData.GetZipCodeByLatitudeLongitude(Obj);

                return ds;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SearchProfileOfDoctorBLL--GetSearchResult", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Getting Colleagues list which has given #Button to WHO AM I.
        /// Used in One-Click referral side second step.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public Colleaguelist GetColleagueListById(int userId ,BO.Enums.Common.PMSProviderFilterType provFilter,string strSearchText="")
        {
            try
            {
                DataTable ds = new DataTable(); Colleaguelist colleaguelist = new Colleaguelist();
                List<ColleagueData> lst = new List<ColleagueData>();
                ds = objColleaguesData.GetColleagueListById(userId, provFilter, strSearchText);
                if (ds != null && ds.Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Rows)
                    {
                        if (SafeValue<string>(item["AccountName"]) != "")
                        {
                            item["AccountName"] = SafeValue<string>("(" + item["AccountName"] + ")");
                        }
                        lst.Add(new ColleagueData()
                        {
                            text = SafeValue<string>(item["FirstName"]) + item["AccountName"], 
                            id = SafeValue<int>(item["ColleagueId"]),
                        });
                    }
                    colleaguelist.colleagues = lst;
                    if (lst !=null)
                    {
                        colleaguelist.RecordCount = lst.Count;
                    }
                }
                return colleaguelist;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SearchProfileOfDoctorBLL--GetColleagueListById", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

     


        /// <summary>
        /// Get Services of doctor form one-click referral form
        /// </summary>
        /// <param name="clgId"></param>
        /// <returns></returns>
        public List<SpecialityService> GetVisibleSpeciality(int clgId)
        {
            List<SpecialityService> lstSp = new List<SpecialityService>();
            DataTable dt = clsColleaguesData.GetUserReferrlaFormSetting(clgId);
            foreach (DataRow item in dt.Rows)
            {
                lstSp.Add(new SpecialityService()
                {
                    SpecialityName = SafeValue<string>(item["SpecialityName"]),
                    SpecialityId = SafeValue<int>(item["SpecialtyId"])
                });

            }
            return lstSp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="regardId"></param>
        /// <param name="clgId"></param>
        /// <returns></returns>
        public List<SpecialityService> GetVisibleDetails(int regardId, int clgId)
        {
            List<SpecialityService> lstSp = new List<SpecialityService>();
            SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter{ParameterName="@regardId",Value=regardId},
                new SqlParameter{ParameterName="@ColleagueId",Value=clgId}
            };
            DataTable dt = new clsHelper().DataTable("Usp_GetReferralItemVisibility", sqlpara);
            foreach (DataRow item in dt.Rows)
            {
                lstSp.Add(new SpecialityService()
                {
                    ServiceName = SafeValue<string>(item["ServiceName"]),
                    ServicesId = SafeValue<int>(item["ServicesId"]),
                    SpecialityId = SafeValue<int>(item["SpecialityId"]),
                    DentialServiceId = SafeValue<int>(item["DentialServiceId"])
                });

            }
            return lstSp;
        }
        public List<DentistDetailsForPatientReward> GetSpecificSearch(GetSpecificSearchModel model)
        {
            List<DentistDetailsForPatientReward> lstSp = new List<DentistDetailsForPatientReward>();
            TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
            SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter{ParameterName="@Search",Value=model.Keywords},
                new SqlParameter{ParameterName="@ProvTypefilter",Value=model.ProviderfilterType}
            };
            DataTable dt = new clsHelper().DataTable("USP_GetColleaguebyEmail", sqlpara);
            foreach (DataRow item in dt.Rows)
            {
                string ImageName = SafeValue<string>(item["ImageName"]);
                if (string.IsNullOrWhiteSpace(ImageName))
                {
                    ImageName = ConfigurationManager.AppSettings.Get("DoctorProfileImageWithMale");
                }
                else
                {
                    ImageName = DoctorImage + System.Web.HttpUtility.UrlEncode(ImageName);
                    ImageName = ImageName.Replace("+", "%20");
                }
                if (SafeValue<string>(item["AccountName"]) != "")
                {
                    item["AccountName"] = SafeValue<string>("(" + item["AccountName"] + ")");
                }
                lstSp.Add(new DentistDetailsForPatientReward()
                {
                    UserId = SafeValue<int>(item["UserId"]),
                    FirstName = SafeValue<string>(item["FirstName"]),
                    UserName = SafeValue<string>(item["Username"]),
                    EncrypteUserId = ObjTripleDESCryptoHelper.encryptText(SafeValue<string>(item["UserId"])),
                    Image = ImageName,
                    Email = SafeValue<string>(item["Email"]),
                    AccountName = SafeValue<string>(item["AccountName"])

                });

            }
            return lstSp;
        }

        public List<DentistDetailsForPatientReward> GetColleagueDetailById(int userId)
        {
            try
            {
                TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
                DataTable ds = new DataTable();
                List<DentistDetailsForPatientReward> Lst = new List<DentistDetailsForPatientReward>();
                ds = objColleaguesData.GetColleagueDetailById(userId);
                if (ds != null && ds.Rows.Count > 0)
                {
                    foreach (DataRow item in ds.Rows)
                    {
                        if (SafeValue<string>(item["AccountName"]) != "")
                        {
                            item["AccountName"] = SafeValue<string>("(" + item["AccountName"] + ")");
                        }
                        string ImageName = SafeValue<string>(item["ImageName"]);
                        if (string.IsNullOrWhiteSpace(ImageName))
                        {
                            ImageName = ConfigurationManager.AppSettings.Get("DoctorProfileImageWithMale");
                        }
                        else
                        {
                            ImageName = DoctorImage + HttpUtility.UrlEncode(ImageName);
                            ImageName = ImageName.Replace("+", "%20");
                        }
                        Lst.Add(new DentistDetailsForPatientReward()
                        {
                            FirstName = SafeValue<string>(item["Name"]),
                            ImageName = ImageName,
                            Email = SafeValue<string>(item["EmailAddress"]),
                            Description = SafeValue<string>(item["Description"]),
                            State = (!string.IsNullOrWhiteSpace(SafeValue<string>(item["State"]))) ? SafeValue<string>(item["State"]) : "",
                            WorkPhone = SafeValue<string>(item["Phone"]),
                            City = (!string.IsNullOrWhiteSpace(SafeValue<string>(item["City"]))) ? SafeValue<string>(item["City"]) + ", " : "",
                            UserName = SafeValue<string>(item["AccountName"]),
                            RewardLocationId = SafeValue<string>(item["LocationId"]),
                            Salutation = SafeValue<string>(item["Salutation"]),
                            PublicProfileName = SafeValue<string>(item["PublicProfileURL"]),
                        });
                    }
                }
                return Lst;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SearchProfileOfDoctorBLL--GetColleagueDetailById", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public string getZipCodeFromGoogle(Searchlatitudelongitude model,string apiKey)
        {
            try
            {
                string url = $@"https://maps.googleapis.com/maps/api/geocode/json?latlng={model.Latitude},{model.Longitude}&key={apiKey}";

                WebRequest request = WebRequest.Create(url);

                WebResponse webResponse = request.GetResponse();

                Stream data = webResponse.GetResponseStream();

                StreamReader reader = new StreamReader(data);
                var response = JsonConvert.DeserializeObject<dynamic>(reader.ReadToEnd());
                webResponse.Close();
                string zipCode = null;
                if(response.results.Count > 0)
                {
                    foreach (dynamic item in response.results[0].address_components)
                    {
                        if (item.types[0] == "postal_code")
                        {
                            zipCode = item.short_name;
                        }
                    }
                }
                
                return zipCode;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("SearchProfileOfDoctorBLL--getZipCodeFromGoogle", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}
