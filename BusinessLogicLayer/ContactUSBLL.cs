﻿using BO.Models;
using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;

namespace BusinessLogicLayer
{
    public class ContactUSBLL
    {
        public static bool SubmitContactUSRequest(ContactUs Obj)
        {
            try
            {
                CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
                objCustomeMailPropery.ToMailAddress = new List<string>
                {   ConfigurationManager.AppSettings.Get("emailoftravis"),
                    ConfigurationManager.AppSettings.Get("MailNotificationEmail_To")
                };
                objCustomeMailPropery.MailTemplateName = "contact-us-request";
                string CompanyName = "RecordLinc";
                DataTable dt = new DataTable();
                dt.Columns.Add("FieldName");
                dt.Columns.Add("FieldValue");
                dt.Rows.Add("FromMail", Obj.Email);
                dt.Rows.Add("FirstName", Obj.FirstName);
                dt.Rows.Add("LastName", Obj.LastName);
                dt.Rows.Add("Phone", Obj.Phone);
                dt.Rows.Add("Message", Obj.Message);
                dt.Rows.Add("CompanyName", CompanyName);
                objCustomeMailPropery.MergeTags = dt;

                SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
                return true;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }
    }
}
