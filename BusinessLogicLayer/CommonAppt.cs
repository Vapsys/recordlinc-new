﻿using BO.ViewModel;
using DataAccessLayer.AppointmentData;
using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BusinessLogicLayer
{
    public static class CommonAppt
    {
        public static int CONSTTIME = ApptSetting.CONSTTIME;
        public static int CONSTNOOFDAY = ApptSetting.CONSTNOOFDAY;
        public static string CONSTDATEFORMATE = ApptSetting.CONSTDATEFORMATE;
        public static CreateApptDataManagement GetAppointmentDataIntoList(DataSet ds, string TimeZoneSystemName)
        {
            var objCreateAppointmentDataManagement = new CreateApptDataManagement();

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    var tempWeekScheduleData = new WkScheduleData();
                    tempWeekScheduleData.AppointmentWeekScheduleId = Convert.ToInt32(item["AppointmentWeekScheduleId"]);
                    tempWeekScheduleData.Day = Convert.ToInt32(item["Day"]);
                    tempWeekScheduleData.IsOffDay = (Convert.ToString(item["IsOffDay"]) == "N") ? "NO" : "YES";
                    tempWeekScheduleData.AppointmentWorkingTimeAllocationId = Convert.ToInt32(item["AppointmentWorkingTimeAllocationId"]);
                    if (!string.IsNullOrEmpty(Convert.ToString(item["MorningFromTime"])))
                    {
                        tempWeekScheduleData.MorningFromTime = TimeSpan.Parse(Convert.ToString(item["MorningFromTime"]));
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(item["MorningToTime"])))
                    {
                        tempWeekScheduleData.MorningToTime = TimeSpan.Parse(Convert.ToString(item["MorningToTime"]));
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(item["EveningFromTime"])))
                    {
                        tempWeekScheduleData.EveningFromTime = TimeSpan.Parse(Convert.ToString(item["EveningFromTime"]));
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(item["EveningToTime"])))
                    {
                        tempWeekScheduleData.EveningToTime = TimeSpan.Parse(Convert.ToString(item["EveningToTime"]));
                    }
                    objCreateAppointmentDataManagement.WeekSchedulesData.Add(tempWeekScheduleData);
                }
            }

            if (ds.Tables[1].Rows.Count > 0)
            {
                foreach (DataRow item in ds.Tables[1].Rows)
                {
                    var tempSpecialDayCheduleData = new SpDayCheduleData();
                    tempSpecialDayCheduleData.AppointmentSpecailDayScheduleId = Convert.ToInt32(item["AppointmentSpecialDayScheduleId"]);
                    tempSpecialDayCheduleData.Date = Convert.ToDateTime(item["Date"]);
                    tempSpecialDayCheduleData.IsOffDay = (Convert.ToString(item["IsOffDay"]) == "N") ? "NO" : "YES";
                    tempSpecialDayCheduleData.AppointmentWorkingTimeAllocationId = Convert.ToInt32(item["AppointmentWorkingTimeAllocationId"]);

                    if (!string.IsNullOrEmpty(Convert.ToString(item["MorningFromTime"])))
                    {
                        tempSpecialDayCheduleData.MorningFromTime = TimeSpan.Parse(Convert.ToString(item["MorningFromTime"]));
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(item["MorningToTime"])))
                    {
                        tempSpecialDayCheduleData.MorningToTime = TimeSpan.Parse(Convert.ToString(item["MorningToTime"]));
                    }
                    objCreateAppointmentDataManagement.SpecialDayChedulesData.Add(tempSpecialDayCheduleData);
                }
            }

            if (ds.Tables[2].Rows.Count > 0)
            {
                objCreateAppointmentDataManagement.AppointmentsData = (from p in ds.Tables[2].AsEnumerable()
                                                                       select new ApptData
                                                                       {
                                                                           AppointmentId = Convert.ToInt32(p["AppointmentId"]),
                                                                           DoctorId = Convert.ToInt32(p["DoctorId"]),
                                                                           PatientId = Convert.ToInt32(p["PatientId"]),
                                                                           AppointmentDate = Convert.ToDateTime(p["AppointmentDate"]),
                                                                           AppointmentTime = TimeSpan.Parse(Convert.ToString(p["AppointmentTime"])),
                                                                           AppointmentLength = Convert.ToInt32(Convert.ToString(p["AppointmentLength"])),
                                                                           ServiceTypeId = Convert.ToString(p["ServiceTypeId"]),
                                                                           //Status = Convert.ToString(p["Status"])
                                                                       }).ToList();
                //-- convert appointment time to appropriate format
                foreach (var item in objCreateAppointmentDataManagement.AppointmentsData)
                {
                    var AppointmentDateTime = Convert.ToDateTime(item.AppointmentDate.ToString("yyyy-MM-dd") + " " + DateTime.Today.Add(item.AppointmentTime).ToString("HH:mm:ss"));
                    AppointmentDateTime = clsHelper.ConvertFromUTC(AppointmentDateTime, TimeZoneSystemName);
                    item.AppointmentDate = AppointmentDateTime.Date;
                    item.AppointmentTime = AppointmentDateTime.TimeOfDay;
                }
            }
            return objCreateAppointmentDataManagement;
        }
        public static List<CalendarRes> CalendarResourceIntoList(DataTable dt)
        {

            var CalList = new List<CalendarRes>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    var tempCalList = new CalendarRes();
                    tempCalList.id = Convert.ToString(item["ApptResourceId"]);
                    tempCalList.title = Convert.ToString(item["ResourceName"]);
                    CalList.Add(tempCalList);
                }
            }
            return CalList;
        }
        public static string CheckOverLappingAndDoctorAvailability(CreateAppt objCreateAppointment, int DoctorId, string TimeZoneSystemName)
        {

            var objclsAppointmentData = new clsAppointment();
            DateTime OrigionalFromTime = Convert.ToDateTime(objCreateAppointment.FromTime);
            DateTime FromTime = OrigionalFromTime;
            if (!string.IsNullOrEmpty(TimeZoneSystemName))
                FromTime = clsHelper.ConvertToUTC(FromTime, TimeZoneSystemName);

            DateTime OrigionalToTime = Convert.ToDateTime(objCreateAppointment.ToTime);
            DateTime ToTime = OrigionalToTime;
            if (!string.IsNullOrEmpty(TimeZoneSystemName))
                ToTime = clsHelper.ConvertToUTC(ToTime, TimeZoneSystemName);

            if (FromTime < DateTime.Now || ToTime < DateTime.Now)
            {
                return "Can not select past time!";
            }
            if (Convert.ToInt32(objCreateAppointment.AppointmentId) > 0)
            {
                objCreateAppointment.AppointmentId = objCreateAppointment.AppointmentId;
            }
            else
            {
                objCreateAppointment.AppointmentId = 0;
            }

            var timeSlotData = GetAppointmentDataIntoList(objclsAppointmentData.GetCreateAppointmentTimeSlotViewData(DoctorId, objCreateAppointment.ApptResourceId, objCreateAppointment.AppointmentId), TimeZoneSystemName);

            int StartDay = ApptSetting.GetWeekSetting().Where(t => t.DayName == FromTime.DayOfWeek.ToString()).Select(t => t.Day).FirstOrDefault();

            var drSpacialDaySchedule = timeSlotData.SpecialDayChedulesData.Where(t => t.Date.ToString(CONSTDATEFORMATE) == FromTime.ToString(CONSTDATEFORMATE)).FirstOrDefault();
            var drWeekSchedule = timeSlotData.WeekSchedulesData.Where(t => t.Day == StartDay).FirstOrDefault();
            var drBookedAppointment = timeSlotData.AppointmentsData.Where(t => t.AppointmentDate.ToString(CONSTDATEFORMATE) == FromTime.ToString(CONSTDATEFORMATE)).ToList();

            var checkbookin = drBookedAppointment.Where(t => t.AppointmentTime <= FromTime.AddMinutes(+1).TimeOfDay && t.AppointmentTime.Add(TimeSpan.FromMinutes(t.AppointmentLength)) >= FromTime.AddMinutes(+1).TimeOfDay).ToList();
            if (checkbookin.Count > 0)
            {
                return "You have booked appointment on this time";
            }
            else
            {
                var totimecheck = drBookedAppointment.Where(t => t.AppointmentTime <= ToTime.AddMinutes(-1).TimeOfDay && t.AppointmentTime.Add(TimeSpan.FromMinutes(t.AppointmentLength)) >= ToTime.AddMinutes(-1).TimeOfDay).ToList();
                if (totimecheck.Count > 0)
                {
                    return "You have booked appointment on this time";
                }
            }

            var CheckBetn = drBookedAppointment.Where(t => t.AppointmentTime >= FromTime.AddMinutes(+1).TimeOfDay && t.AppointmentTime.Add(TimeSpan.FromMinutes(15)) <= ToTime.AddMinutes(-1).TimeOfDay).ToList();

            if (CheckBetn.Count > 0)
            {
                return "You have booked appointment on this time";
            }


            if (drSpacialDaySchedule?.IsOffDay == "YES")
            {
                return "Off day on special day schedule, can not create appointment";
            }
            else if (drSpacialDaySchedule != null)
            {
                if (!(drSpacialDaySchedule?.MorningFromTime <= FromTime.TimeOfDay && drSpacialDaySchedule?.MorningToTime >= ToTime.TimeOfDay))
                {
                    return "your are not available on that time,Specail day schedule is created for this date";
                }
            }

            if (drWeekSchedule?.IsOffDay == "YES")
            {
                return "Your have off day on this date!";
            }
            else if (drWeekSchedule != null)
            {
                DateTime MorningFromTime = OrigionalFromTime.Date, EveningFromTime = OrigionalFromTime.Date, EveningToTime = OrigionalToTime.Date, MorningToTime = OrigionalToTime.Date;
                MorningFromTime = drWeekSchedule.MorningFromTime.HasValue ? Convert.ToDateTime(OrigionalFromTime.Date.Add(drWeekSchedule.MorningFromTime.GetValueOrDefault())) : MorningFromTime;
                MorningFromTime = clsHelper.ConvertToUTC(MorningFromTime, TimeZoneSystemName);

                EveningFromTime = drWeekSchedule.EveningFromTime.HasValue ? Convert.ToDateTime(OrigionalFromTime.Date.Add(drWeekSchedule.EveningFromTime.GetValueOrDefault())) : EveningFromTime;
                EveningFromTime = clsHelper.ConvertToUTC(EveningFromTime, TimeZoneSystemName);

                EveningToTime = drWeekSchedule.EveningToTime.HasValue ? Convert.ToDateTime(OrigionalToTime.Date.Add(drWeekSchedule.EveningToTime.GetValueOrDefault())) : EveningToTime;
                EveningToTime = clsHelper.ConvertToUTC(EveningToTime, TimeZoneSystemName);

                MorningToTime = drWeekSchedule.MorningToTime.HasValue ? Convert.ToDateTime(OrigionalToTime.Date.Add(drWeekSchedule.MorningToTime.GetValueOrDefault())) : MorningToTime;
                MorningToTime = clsHelper.ConvertToUTC(MorningToTime, TimeZoneSystemName);

                if ((MorningFromTime <= FromTime && MorningToTime >= ToTime) || (EveningFromTime <= FromTime && EveningToTime >= ToTime))
                {
                    return "";
                }
                else
                {
                    return "your are not available on that time!";
                }
            }
            return "";
        }
        public static int CheckAvalibleDoctorByDateTimeTheatreDoctor(CreateAppt objCreateAppointment, int DoctorId, string TimeZoneSystemName)
        {
            DateTime FromTime = Convert.ToDateTime(objCreateAppointment.FromTime);
            var objclsAppointmentData = new clsAppointment();
            DateTime AppointmentDate;
            TimeSpan AppointmentFromTime;
            AppointmentDate = Convert.ToDateTime(objCreateAppointment.FromTime);
            AppointmentDate = clsHelper.ConvertToUTC(AppointmentDate, TimeZoneSystemName);
            AppointmentFromTime = AppointmentDate.TimeOfDay;
            TimeSpan AppointmentToTime = AppointmentFromTime.Add(TimeSpan.FromMinutes(Convert.ToInt32(objCreateAppointment.AppointmentLength)));
            if (Convert.ToInt32(objCreateAppointment.AppointmentId) > 0)
            {
                objCreateAppointment.AppointmentId = objCreateAppointment.AppointmentId;
            }
            else
            {
                objCreateAppointment.AppointmentId = 0;
            }
            int Result = (objclsAppointmentData.CheckAvalibleDoctorByDateTimeTheatreDoctor(objCreateAppointment.DoctorId, AppointmentFromTime, AppointmentToTime, objCreateAppointment.CurrentDate, objCreateAppointment.AppointmentId)).Rows.Count;
            return Result;
        }
        public static int CheckAvalibleDoctorByDateTimeTheatrePatient(CreateAppt objCreateAppointment, int PatientId, string TimeZoneSystemName)
        {
            DateTime FromTime = Convert.ToDateTime(objCreateAppointment.FromTime);
            var objclsAppointmentData = new clsAppointment();
            DateTime AppointmentDate;
            TimeSpan AppointmentFromTime;
            AppointmentDate = Convert.ToDateTime(objCreateAppointment.FromTime);
            AppointmentDate = clsHelper.ConvertToUTC(AppointmentDate, TimeZoneSystemName);
            AppointmentFromTime = AppointmentDate.TimeOfDay;
            TimeSpan AppointmentToTime = AppointmentFromTime.Add(TimeSpan.FromMinutes(Convert.ToInt32(objCreateAppointment.AppointmentLength)));
            if (Convert.ToInt32(objCreateAppointment.AppointmentId) > 0)
            {
                objCreateAppointment.AppointmentId = objCreateAppointment.AppointmentId;
            }
            else
            {
                objCreateAppointment.AppointmentId = 0;
            }
            int Result = (objclsAppointmentData.CheckAvalibleDoctorByDateTimeTheatrePatientId(PatientId, AppointmentFromTime, AppointmentToTime, objCreateAppointment.CurrentDate, objCreateAppointment.AppointmentId)).Rows.Count;
            return Result;
        }
        public static CreateAppt TimeSlotAllocation(CreateApptDataManagement objCreateAppointmentDataManagement, DataTable dtAppService, string StartDate, string TimeZoneSystemName)
        {
            var objCreateAppointment = new CreateAppt();
            //List of one month days
            var listofmonth = ApptSetting.GetOneMonthList(StartDate);
            var TodayDateTime = DateTime.Now;
            TodayDateTime = clsHelper.ConvertFromUTC(Convert.ToDateTime(TodayDateTime.ToString()), TimeZoneSystemName);
            if (!string.IsNullOrEmpty(StartDate))
            {
                if (TodayDateTime.Date == Convert.ToDateTime(StartDate).Date)
                {
                    objCreateAppointment.PrevDate = TodayDateTime.ToString(CONSTDATEFORMATE);
                    objCreateAppointment.NextDate = TodayDateTime.AddDays(CONSTNOOFDAY).ToString(CONSTDATEFORMATE);
                }
                else if (TodayDateTime.AddDays(1).Date == Convert.ToDateTime(StartDate).Date)
                {

                    objCreateAppointment.PrevDate = Convert.ToDateTime(StartDate).AddDays(-1).ToString(CONSTDATEFORMATE);
                    objCreateAppointment.NextDate = Convert.ToDateTime(StartDate).AddDays(CONSTNOOFDAY).ToString(CONSTDATEFORMATE);
                }
                else if (TodayDateTime.AddDays(2).Date == Convert.ToDateTime(StartDate).Date)
                {
                    objCreateAppointment.PrevDate = Convert.ToDateTime(StartDate).AddDays(-2).ToString(CONSTDATEFORMATE);
                    objCreateAppointment.NextDate = Convert.ToDateTime(StartDate).AddDays(CONSTNOOFDAY).ToString(CONSTDATEFORMATE);
                }
                else
                {
                    objCreateAppointment.PrevDate = Convert.ToDateTime(StartDate).AddDays(-CONSTNOOFDAY).ToString(CONSTDATEFORMATE);
                    objCreateAppointment.NextDate = Convert.ToDateTime(StartDate).AddDays(CONSTNOOFDAY).ToString(CONSTDATEFORMATE);
                }

            }
            else
            {
                objCreateAppointment.PrevDate = TodayDateTime.ToString(CONSTDATEFORMATE);
                objCreateAppointment.NextDate = TodayDateTime.AddDays(CONSTNOOFDAY).ToString(CONSTDATEFORMATE);
            }
            //Possible time slot allocation count base on 15 minute for 24 hours
            List<int> TimeDivision = ApptSetting.TimeSlotwithStaticTimeMin();

            //Loop Will run for one month
            foreach (var itemMonthsdays in listofmonth)
            {
                //DayNo. to find day wise data of week schedule
                int StartDay = ApptSetting.GetWeekSetting().Where(t => t.DayName == itemMonthsdays.DayOfWeek.ToString()).Select(t => t.Day).FirstOrDefault();

                //Found week schedule by DayNo.
                var drWeekSchedule = objCreateAppointmentDataManagement.WeekSchedulesData.Where(t => t.Day == StartDay).FirstOrDefault();

                //Found specailday schedule by Date in "MM-dd-yyyy" formate.
                var drSpacialDaySchedule = objCreateAppointmentDataManagement.SpecialDayChedulesData.Where(t => t.Date.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE)).FirstOrDefault();

                //Found Booked appointment by Date in "MM-dd-yyyy" formate.
                var drBookedAppointment = objCreateAppointmentDataManagement.AppointmentsData.Where(t => t.AppointmentDate.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE)).ToList();

                var tempAppointmentDayView = new ApptDayView() { Date = itemMonthsdays.ToString(CONSTDATEFORMATE), IsOffDay = drWeekSchedule.IsOffDay };
                //if (itemMonthsdays.Date == DateTime.Now.Date)
                //{
                //    tempAppointmentDayView.IsToday = true;
                //}

                //Check Specail day shedule is created on Date
                if (drSpacialDaySchedule != null)
                {
                    //If Spcail Day schdule is off day
                    if (drSpacialDaySchedule.IsOffDay.Equals("NO"))
                    {
                        //Get time of spcial day schedule
                        TimeSpan MorFromTime = TimeSpan.Parse(Convert.ToString(drSpacialDaySchedule.MorningFromTime));
                        TimeSpan MorToTime = TimeSpan.Parse(Convert.ToString(drSpacialDaySchedule.MorningToTime));

                        //Start Slot Division with Maximum possiblity for selected service
                        foreach (var itemMinutes in TimeDivision)
                        {
                            //Check Morning From time should greater then To time
                            if (MorFromTime < MorToTime)
                            {
                                //Check is there any booked appointment for the day
                                if (drBookedAppointment.Count > 0)
                                {
                                    //if appointment is booked then start checking one by one because there can me multiple appointment booked on the day
                                    foreach (var itemBookedApp in drBookedAppointment)
                                    {
                                        //Check where booked time is started from
                                        if (itemBookedApp.AppointmentTime == MorFromTime)
                                        {
                                            for (int i = 0; i < ApptSetting.TimeSlotForBookedTimeDivsion(itemBookedApp.AppointmentLength); i++)
                                            {
                                                if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= MorFromTime)
                                                {
                                                    //We Dont send TimeveOver Time Slots for Denial Reuest.
                                                    tempAppointmentDayView.TimeSlots.Add(new ApptTimeSlotView()
                                                    {
                                                        //ActullTime = MorFromTime, Date = itemMonthsdays.ToString(),
                                                        //IsBookedTimeslot = ApptSetting.ApptStatus.TimeveOver.ToString(),
                                                        TimeSlot = MorFromTime.ToString(@"hh\:mm")// string.Format("{0}", DateTime.Today.Add(MorFromTime).ToString(ApptSetting.CONSTTIMEFORMATE))
                                                    });
                                                    MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                                }
                                                else
                                                {
                                                    //We Dont send Booked Time Slots for Denial Reuest.
                                                    tempAppointmentDayView.TimeSlots.Add(new ApptTimeSlotView()
                                                    {
                                                        //ActullTime = MorFromTime, Date = itemMonthsdays.ToString(),
                                                        //IsBookedTimeslot = ApptSetting.ApptStatus.Booked.ToString(),
                                                        TimeSlot = MorFromTime.ToString(@"hh\:mm") //string.Format("{0}", DateTime.Today.Add(MorFromTime).ToString(ApptSetting.CONSTTIMEFORMATE))
                                                    });
                                                    MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                                }
                                            }
                                        }//Check last slote should not excide To time and also check if appointment is booked on the day so rest time will be added there
                                        else if (MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= MorToTime)
                                        {
                                            if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= MorFromTime)
                                            {
                                                tempAppointmentDayView.TimeSlots.Add(new ApptTimeSlotView() {
                                                    //ActullTime = MorFromTime, Date = itemMonthsdays.ToString(),
                                                    //IsBookedTimeslot = ApptSetting.ApptStatus.TimeveOver.ToString(),
                                                    TimeSlot = MorFromTime.ToString(@"hh\:mm")// string.Format("{0} ", DateTime.Today.Add(MorFromTime).ToString(ApptSetting.CONSTTIMEFORMATE)) 
                                                });
                                                MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                            }
                                            else
                                            {
                                                tempAppointmentDayView.TimeSlots.Add(new ApptTimeSlotView() {
                                                    //ActullTime = MorFromTime, Date = itemMonthsdays.ToString(),
                                                    TimeSlot = MorFromTime.ToString(@"hh\:mm") // string.Format("{0}", DateTime.Today.Add(MorFromTime).ToString(ApptSetting.CONSTTIMEFORMATE)) 
                                                });
                                                MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                            }
                                        }
                                    }
                                }//Check last slote should not excide To time
                                else if (MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= MorToTime)
                                {
                                    if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= MorFromTime)
                                    {
                                        tempAppointmentDayView.TimeSlots.Add(new ApptTimeSlotView() {
                                            //ActullTime = MorFromTime, Date = itemMonthsdays.ToString(),
                                            //IsBookedTimeslot = ApptSetting.ApptStatus.TimeveOver.ToString(),
                                            TimeSlot = MorFromTime.ToString(@"hh\:mm") //string.Format("{0}", DateTime.Today.Add(MorFromTime).ToString(ApptSetting.CONSTTIMEFORMATE)) 
                                        });
                                        MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                    }
                                    else
                                    {
                                        tempAppointmentDayView.TimeSlots.Add(new ApptTimeSlotView() {
                                            //ActullTime = MorFromTime, Date = itemMonthsdays.ToString(),
                                            TimeSlot = MorFromTime.ToString(@"hh\:mm") // string.Format("{0}", DateTime.Today.Add(MorFromTime).ToString(ApptSetting.CONSTTIMEFORMATE)) 
                                        });
                                        MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    //Check week schedule day should be not Off day
                    if (drWeekSchedule.IsOffDay.Equals("NO"))
                    {
                        TimeSpan MorFromTime;
                        TimeSpan MorToTime;
                        TimeSpan EveFromTime;
                        TimeSpan EveToTime;
                        //TimeSpan MorFromTime = TimeSpan.Parse(Convert.ToString(drWeekSchedule.MorningFromTime));
                        //TimeSpan MorToTime = TimeSpan.Parse(Convert.ToString(drWeekSchedule.MorningToTime));
                        if (string.IsNullOrEmpty(Convert.ToString(drWeekSchedule.MorningFromTime)) && string.IsNullOrEmpty(Convert.ToString(drWeekSchedule.MorningToTime)))
                        {
                            EveFromTime = Convert.ToDateTime(Convert.ToString(drWeekSchedule.EveningFromTime)).AddMinutes(-10).TimeOfDay;
                            MorFromTime = EveFromTime.Add(Convert.ToDateTime("00:05:00").TimeOfDay);
                            MorToTime = MorFromTime.Add(Convert.ToDateTime("00:05:00").TimeOfDay);
                            EveFromTime = EveFromTime;
                        }
                        else
                        {
                            MorFromTime = TimeSpan.Parse(Convert.ToString(drWeekSchedule.MorningFromTime));
                            MorToTime = TimeSpan.Parse(Convert.ToString(drWeekSchedule.MorningToTime));
                        }


                        if (string.IsNullOrEmpty(Convert.ToString(drWeekSchedule.EveningFromTime)) && string.IsNullOrEmpty(Convert.ToString(drWeekSchedule.EveningFromTime)))
                        {
                            MorToTime = Convert.ToDateTime(Convert.ToString(drWeekSchedule.MorningToTime)).AddMinutes(-10).TimeOfDay;
                            EveFromTime = MorToTime.Add(Convert.ToDateTime("00:05:00").TimeOfDay);
                            EveToTime = EveFromTime.Add(Convert.ToDateTime("00:05:00").TimeOfDay);
                            MorToTime = EveToTime;
                        }
                        else
                        {
                            EveFromTime = TimeSpan.Parse(Convert.ToString(drWeekSchedule.EveningFromTime));
                            EveToTime = TimeSpan.Parse(Convert.ToString(drWeekSchedule.EveningToTime));
                        }

                        TimeSpan TempTime = TimeSpan.Parse("00:00:00");
                        TimeSpan TempEndTime = TimeSpan.Parse("23:45:00");
                        foreach (var itemMinutes in TimeDivision)
                        {
                            if (TempTime < MorFromTime)
                            {
                                // tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new ApptTimeSlotView() { ActullTime = TempTime, Date = itemMonthsdays.ToString(), //IsBookedTimeslot = ApptSetting.ApptStatus.NotAvailable.ToString(), TimeSlot = string.Format("{0}",DateTime.Today.Add(TempTime).ToString(ApptSetting.CONSTTIMEFORMATE)) });
                                TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                            }
                            else if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= MorFromTime)
                            {
                                if (MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= MorToTime)
                                {
                                    // tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new ApptTimeSlotView() { ActullTime = MorFromTime, Date = itemMonthsdays.ToString(), //IsBookedTimeslot = ApptSetting.ApptStatus.TimeveOver.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(MorFromTime).ToString(ApptSetting.CONSTTIMEFORMATE)) });
                                    MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                    TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                }
                                else if (TempTime >= MorToTime && TempTime < EveFromTime)
                                {
                                    // tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new ApptTimeSlotView() { ActullTime = TempTime, Date = itemMonthsdays.ToString(), //IsBookedTimeslot = ApptSetting.ApptStatus.NotAvailable.ToString(), TimeSlot = string.Format("{0}",DateTime.Today.Add(TempTime).ToString(ApptSetting.CONSTTIMEFORMATE)) });
                                    TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                }
                            }
                            else if (drBookedAppointment.Count > 0)
                            {
                                bool flag = false;
                                foreach (var itemBookedApp in drBookedAppointment)
                                {
                                    if (itemBookedApp.AppointmentTime == MorFromTime)
                                    {
                                        flag = true;
                                        //Divide booked slot with 15 min as we are showing booking with 15 of difference
                                        for (int i = 0; i < ApptSetting.TimeSlotForBookedTimeDivsion(itemBookedApp.AppointmentLength); i++)
                                        {
                                            // We Dont send Booked Time Slots for Denial Reuest.
                                            tempAppointmentDayView.TimeSlots.Add(new ApptTimeSlotView()
                                            {
                                                //ActullTime = MorFromTime, Date = itemMonthsdays.ToString(),
                                                //IsBookedTimeslot = ApptSetting.ApptStatus.Booked.ToString(),
                                                TimeSlot = MorFromTime.ToString(@"hh\:mm") //string.Format("{0}", DateTime.Today.Add(MorFromTime).ToString(ApptSetting.CONSTTIMEFORMATE))
                                            });
                                            MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                            TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                        }
                                    }
                                }
                                if (!flag)
                                {
                                    if (MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= MorToTime)
                                    {
                                        tempAppointmentDayView.TimeSlots.Add(new ApptTimeSlotView() {
                                            //ActullTime = MorFromTime, Date = itemMonthsdays.ToString(),
                                            TimeSlot = MorFromTime.ToString(@"hh\:mm") //string.Format("{0}", DateTime.Today.Add(MorFromTime).ToString(ApptSetting.CONSTTIMEFORMATE)) 
                                        });
                                        MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                        TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                    }
                                    else if (TempTime >= MorToTime && TempTime < EveFromTime)
                                    {
                                        // tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new ApptTimeSlotView() { ActullTime = TempTime, Date = itemMonthsdays.ToString(), //IsBookedTimeslot = ApptSetting.ApptStatus.NotAvailable.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(TempTime).ToString(ApptSetting.CONSTTIMEFORMATE)) });
                                        TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                    }
                                }

                            }
                            else if (MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= MorToTime)
                            {
                                tempAppointmentDayView.TimeSlots.Add(new ApptTimeSlotView() {
                                    //ActullTime = MorFromTime, Date = itemMonthsdays.ToString(),
                                    TimeSlot = MorFromTime.ToString(@"hh\:mm")// string.Format("{0}", DateTime.Today.Add(MorFromTime).ToString(ApptSetting.CONSTTIMEFORMATE)) 
                                });
                                MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                            }
                            else if (TempTime >= MorToTime && TempTime < EveFromTime)
                            {
                                //  tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new ApptTimeSlotView() { ActullTime = TempTime, Date = itemMonthsdays.ToString(), //IsBookedTimeslot = ApptSetting.ApptStatus.NotAvailable.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(TempTime).ToString(ApptSetting.CONSTTIMEFORMATE)) });
                                TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                            }
                        }

                        //Evening Time calculation

                        foreach (var itemMinutes in TimeDivision)
                        {
                            if (TempTime < EveFromTime)
                            {
                                //  tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new ApptTimeSlotView() { ActullTime = TempTime, Date = itemMonthsdays.ToString(), //IsBookedTimeslot = ApptSetting.ApptStatus.NotAvailable.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(TempTime).ToString(ApptSetting.CONSTTIMEFORMATE)) });
                                TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                            }
                            else if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= EveFromTime)
                            {
                                if (EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= EveToTime)
                                {
                                    //  tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new ApptTimeSlotView() { ActullTime = EveFromTime, Date = itemMonthsdays.ToString(), //IsBookedTimeslot = ApptSetting.ApptStatus.TimeveOver.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(EveFromTime).ToString(ApptSetting.CONSTTIMEFORMATE)) });
                                    EveFromTime = EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                    TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                }
                                else if (TempTime >= EveToTime && TempTime <= TempEndTime)
                                {
                                    tempAppointmentDayView.TimeSlots.Add(new ApptTimeSlotView() {
                                        //ActullTime = TempTime, Date = itemMonthsdays.ToString(),
                                        //IsBookedTimeslot = ApptSetting.ApptStatus.NotAvailable.ToString(),
                                        TimeSlot = TempTime.ToString(@"hh\:mm")// string.Format("{0}", DateTime.Today.Add(TempTime).ToString(ApptSetting.CONSTTIMEFORMATE)) 
                                    });
                                    TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                }
                            }
                            else if (drBookedAppointment.Count > 0)
                            {
                                bool flag = false;
                                foreach (var itemBookedApp in drBookedAppointment)
                                {
                                    if (itemBookedApp.AppointmentTime == EveFromTime)
                                    {
                                        flag = true;
                                        for (int i = 0; i < ApptSetting.TimeSlotForBookedTimeDivsion(itemBookedApp.AppointmentLength); i++)
                                        {
                                            // We Dont send Booked Time Slots for Denial Reuest.
                                            //tempAppointmentDayView.TimeSlots.Add(new ApptTimeSlotView()
                                            //{
                                            //    //ActullTime = EveFromTime, Date = itemMonthsdays.ToString(),
                                            //    //IsBookedTimeslot = ApptSetting.ApptStatus.Booked.ToString(),
                                            //    TimeSlot = EveFromTime.ToString(@"hh\:mm") //string.Format("{0}", DateTime.Today.Add(EveFromTime).ToString(ApptSetting.CONSTTIMEFORMATE))
                                            //});
                                            EveFromTime = EveFromTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                            TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                        }
                                    }
                                }
                                if (!flag)
                                {
                                    if (EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= EveToTime)
                                    {
                                        tempAppointmentDayView.TimeSlots.Add(new ApptTimeSlotView() {
                                            //ActullTime = EveFromTime, Date = itemMonthsdays.ToString(),
                                            TimeSlot = EveFromTime.ToString(@"hh\:mm") //string.Format("{0}", DateTime.Today.Add(EveFromTime).ToString(ApptSetting.CONSTTIMEFORMATE)) 
                                        });
                                        EveFromTime = EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                        TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                    }
                                    else if (TempTime >= EveToTime && TempTime <= TempEndTime)
                                    {
                                        // tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new ApptTimeSlotView() { ActullTime = TempTime, Date = itemMonthsdays.ToString(), //IsBookedTimeslot = ApptSetting.ApptStatus.NotAvailable.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(TempTime).ToString(ApptSetting.CONSTTIMEFORMATE)) });
                                        TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                    }
                                }

                            }
                            else if (EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= EveToTime)
                            {
                                tempAppointmentDayView.TimeSlots.Add(new ApptTimeSlotView() {
                                    //ActullTime = EveFromTime, Date = itemMonthsdays.ToString(),
                                    TimeSlot = EveFromTime.ToString(@"hh\:mm") //string.Format("{0}", DateTime.Today.Add(EveFromTime).ToString(ApptSetting.CONSTTIMEFORMATE)) 
                                });
                                EveFromTime = EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                            }
                            else if (TempTime >= EveToTime && TempTime <= TempEndTime)
                            {
                                // tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new ApptTimeSlotView() { ActullTime = TempTime, Date = itemMonthsdays.ToString(), //IsBookedTimeslot = ApptSetting.ApptStatus.NotAvailable.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(TempTime).ToString(ApptSetting.CONSTTIMEFORMATE)) });
                                TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                            }
                        }
                    }
                    else
                    {

                        //TimeSpan TempTime = TimeSpan.Parse("00:00:00");
                        //TimeSpan TempEndTime = TimeSpan.Parse("23:45:00");
                        //foreach (var itemMinutes in TimeDivision)
                        //{
                        //    tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new ApptTimeSlotView() { ActullTime = TempTime, Date = itemMonthsdays.ToString(), //IsBookedTimeslot = ApptSetting.ApptStatus.NotAvailable.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(TempTime).ToString(ApptSetting.CONSTTIMEFORMATE)) });
                        //    TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                        //}
                    }
                }
                objCreateAppointment.AppointmentDayViewList.Add(tempAppointmentDayView);
            }
            return objCreateAppointment;
        }
    }
    public static class ApptSetting
    {
        public const int CONSTTIME = 15;
        public const int CONSTNOOFDAY = 5;
        public const string CONSTDATEFORMATE = "MM-dd-yyyy";
        public const string CONSTDATEFORMATE1 = "MM/dd/yyyy";
        public const string CONSTTIMEFORMATE = "hh:mm tt";
        public const string NewCONSTTIMEFORMATE = "h:mm tt";
        public const string CONSTTIMEFORMATE24 = "hh:mm";
        public static List<SelectListItem> GetAllTime()
        {

            List<SelectListItem> lst = new List<SelectListItem>();
            lst.Add(new SelectListItem { Text = "00", Value = "00:00:00" });
            lst.Add(new SelectListItem { Text = "01", Value = "01:00:00" });
            lst.Add(new SelectListItem { Text = "02", Value = "02:00:00" });
            lst.Add(new SelectListItem { Text = "03", Value = "03:00:00" });
            lst.Add(new SelectListItem { Text = "04", Value = "04:00:00" });
            lst.Add(new SelectListItem { Text = "05", Value = "05:00:00" });
            lst.Add(new SelectListItem { Text = "06", Value = "06:00:00" });
            lst.Add(new SelectListItem { Text = "07", Value = "07:00:00" });
            lst.Add(new SelectListItem { Text = "08", Value = "08:00:00" });
            lst.Add(new SelectListItem { Text = "09", Value = "09:00:00" });
            lst.Add(new SelectListItem { Text = "10", Value = "10:00:00" });
            lst.Add(new SelectListItem { Text = "11", Value = "11:00:00" });
            lst.Add(new SelectListItem { Text = "12", Value = "12:00:00" });
            lst.Add(new SelectListItem { Text = "13", Value = "13:00:00" });
            lst.Add(new SelectListItem { Text = "14", Value = "14:00:00" });
            lst.Add(new SelectListItem { Text = "15", Value = "15:00:00" });
            lst.Add(new SelectListItem { Text = "16", Value = "16:00:00" });
            lst.Add(new SelectListItem { Text = "17", Value = "17:00:00" });
            lst.Add(new SelectListItem { Text = "18", Value = "18:00:00" });
            lst.Add(new SelectListItem { Text = "19", Value = "19:00:00" });
            lst.Add(new SelectListItem { Text = "20", Value = "20:00:00" });
            lst.Add(new SelectListItem { Text = "21", Value = "21:00:00" });
            lst.Add(new SelectListItem { Text = "22", Value = "22:00:00" });
            lst.Add(new SelectListItem { Text = "23", Value = "23:00:00" });
            return lst;
        }
        public static List<WkDay> GetWeekSetting()
        {

            return new List<WkDay>()
            {
                new WkDay(){Day=0,DayName="Monday"},
                new WkDay(){Day=1,DayName="Tuesday"},
                new WkDay(){Day=2,DayName="Wednesday"},
                new WkDay(){Day=3,DayName="Thursday"},
                new WkDay(){Day=4,DayName="Friday"},
                new WkDay(){Day=5,DayName="Saturday"},
                new WkDay(){Day=6,DayName="Sunday"},
            };
        }

        public static List<DateTime> GetOneMonthList(string SartDate)
        {
            var dates = new List<DateTime>();
            var onemonth = DateTime.Now.AddDays(CONSTNOOFDAY);
            var startingdate = DateTime.Now;
            if (!string.IsNullOrEmpty(SartDate))
            {
                startingdate = Convert.ToDateTime(SartDate);
                onemonth = startingdate.AddDays(CONSTNOOFDAY);
            }



            for (var date = startingdate; date < onemonth; date = date.AddDays(1))
            {
                dates.Add(date);
            }
            return dates;
        }
        public static List<int> HoursDevision(TimeSpan Servicetime)
        {
            int minutes = (int)Servicetime.TotalMinutes;
            int slotcount = 24 * 60 / minutes;
            List<int> lst = new List<int>();
            for (int i = 0; i < slotcount; i++)
            {
                lst.Add(minutes);
            }
            return lst;
        }
        public static List<int> TimeSlotwithStaticTimeMin()
        {

            int slotcount = 24 * 60 / CONSTTIME;
            List<int> lst = new List<int>();
            for (int i = 0; i < slotcount; i++)
            {
                lst.Add(CONSTTIME);
            }
            return lst;
        }
        public static int TimeSlotForBookedTimeDivsion(int Servicetime)
        {
            return Servicetime / CONSTTIME;
        }
        public enum ApptStatus
        {
            Booked,
            TimeveOver,
            NotAvailable

        }
    }
    public class WkDay
    {
        public int Day { get; set; }

        public string DayName { get; set; }

        public bool IsOffDay { get; set; }

        public int AppointmentWorkingTimeAllocationId { get; set; }

        public int? AppointmentWeekScheduleId { get; set; }

        public int? AppintmentSpecialDayScheduleId { get; set; }

        public string MorningFromTime { get; set; }

        public string MorningToTime { get; set; }

        public string EveningFromTime { get; set; }

        public string EveningToTime { get; set; }

        public List<SelectListItem> Times { get; set; }
    }
}
