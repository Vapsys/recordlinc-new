﻿using BO.ViewModel;
using BO.Enums;
using DataAccessLayer;
using DataAccessLayer.Common;
using System;
using System.Data;
using BO.Models;
using System.Collections.Generic;
using System.Linq;
using static DataAccessLayer.Common.clsCommon;

namespace BusinessLogicLayer
{
    public class DentistRatingBLL
    {
        DentistRatingDAL objDAL;
        clsCommon objCommon;
        clsTemplate obj = new clsTemplate();
        public DentistRatingBLL()
        {
            objDAL = new DentistRatingDAL();
            objCommon = new clsCommon();
        }
        public DentistRatingModel GetDentistRatingById(int UserId, int RatingId)
        {

            DentistRatingModel model = null;
            DataTable dt = new DataTable();
            dt = objDAL.getDentistRatings(UserId, RatingId, (int)Common.RatingStatus.APPROVED, 1);
            if (dt != null && dt.Rows.Count > 0)
            {
                model.ratings = new BO.Models.Ratings()
                {
                    RatingId = SafeValue<int>(dt.Rows[0]["RatingId"]),
                    RatingValue = SafeValue<double>(dt.Rows[0]["RatingValue"]),
                    SenderId = SafeValue<int>(!string.IsNullOrWhiteSpace(SafeValue<string>(dt.Rows[0]["SenderId"])) ? SafeValue<string>(dt.Rows[0]["SenderId"]) : "0"),
                    SenderName = SafeValue<string>(dt.Rows[0]["SenderName"]),
                    ReceiverId = SafeValue<int>(dt.Rows[0]["ReceiverId"]),
                    CreatedDate = SafeValue<DateTime>(dt.Rows[0]),
                };
            }
            return model;
        }

        public bool InsertRatings(DentistRatingModel model)
        {
            return objDAL.InsertRatings(model);
        }

        public List<Ratings> GetDentistRatings(int userid, int page)
        {
            List<Ratings> list = new List<Ratings>();
            DataTable dt = new DataTable();
            dt = objDAL.getDentistRatings(userid, 0, (int)Common.RatingStatus.APPROVED, page);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = (from p in dt.AsEnumerable()
                        select new Ratings
                        {
                            RatingId = SafeValue<int>(p["RatingId"]),
                            RatingValue = SafeValue<double>(p["RatingValue"]),
                            SenderId = SafeValue<int>(!string.IsNullOrWhiteSpace(SafeValue<string>(p["SenderId"])) ? SafeValue<string>(p["SenderId"]) : "0" ),
                            SenderName = SafeValue<string>(p["SenderName"]),
                            ReceiverId = SafeValue<int>(p["ReceiverId"]),
                            CreatedDate = SafeValue<DateTime>(p["CreatedDate"]),
                            RatingMessage = SafeValue<string>(p["RatingMessage"])
                        }).ToList();


            }
            return list;
        }

        public List<Ratings> GetDentistRatingsByStatus(int UserId, int RatingId, int Status, int Page, int PageSize = 10, int SortDirection = 2, int SortColumn = 1)
        {
            List<Ratings> list = new List<Ratings>();
            DataTable dt = new DataTable();
            dt = objDAL.getDentistRatings(UserId,0,Status,Page,PageSize,SortDirection,SortColumn);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = (from p in dt.AsEnumerable()
                        select new Ratings
                        {
                            RatingId = SafeValue<int>(p["RatingId"]),
                            RatingValue = SafeValue<double>(p["RatingValue"]),
                            SenderId = SafeValue<int>(!string.IsNullOrWhiteSpace(SafeValue<string>(p["SenderId"])) ? SafeValue<string>(p["SenderId"]) : "0"),
                            SenderName = SafeValue<string>(p["SenderName"]),
                            ReceiverId = SafeValue<int>(p["ReceiverId"]),
                            CreatedDate = SafeValue<DateTime>(p["CreatedDate"]),
                            RatingMessage = SafeValue<string>(p["RatingMessage"])
                        }).ToList();


            }
            return list;
        }

        public int GetDentistRatingsCount(int userid)
        {
            return objDAL.GetDentistRatingsCount(userid, (int)Common.RatingStatus.APPROVED);
        }

        public int GetDentistRatingsCountByStatus(int userid, int Status)
        {
            return objDAL.GetDentistRatingsCount(userid, Status);
        }

        public bool ApproveDeleteRating(int RatingId, int UserId, int Status)
        {
            return objDAL.ApproveDeleteRating(RatingId, UserId, Status);
        }
        public int GetIdOnPublicProfileURL(string url)
        {
            DataTable dt = new DataTable();
            dt = objCommon.GetUserIDFromProfilePath(url);
            if (dt != null && dt.Rows.Count > 0)
            {
                return SafeValue<int>(dt.Rows[0]["UserID"]);

            }
            else
            {
                return -1;
            }
        }
        public bool SendMailfromIluvmyDentist(MailSendMendrill Model, string _serverPath)
        {
            bool Result = false;
            try
            {
                return Result = obj.SendMailfromIluvmydentist(Model, _serverPath);
            }
            catch (Exception Ex)
            {

                throw;
            }
        }
        public bool SendMailToSupportforRequestDemo(MDLPatientSignUp Obj)
        {
            bool Result = false;
            try
            {
                return Result = obj.SendMailToSupportforRequestDemo(Obj);                
            }
            catch (Exception Ex)
            {

                throw;
            }
        }
    }
}
