﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(OneClickReferral.Startup))]
namespace OneClickReferral
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
