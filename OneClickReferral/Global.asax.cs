﻿using BO.ViewModel;
using Newtonsoft.Json;
using OneClickReferral.Models;
using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace OneClickReferral
{
    public class MvcApplication : HttpApplication
    {
        RepositoryBLL repository = new RepositoryBLL("", ConfigurationManager.AppSettings["apikey"].ToString());
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_BeginRequest(object sender, EventArgs e)
        {

            try
            {

                Response.Cache.SetNoStore();
                //Code for set www by default


                if (!HttpContext.Current.Request.Url.ToString().ToLower().Contains("http://localhost") && !HttpContext.Current.Request.Url.ToString().ToLower().Contains("qa.") && !HttpContext.Current.Request.Url.ToString().ToLower().Contains("http://192.168") && !HttpContext.Current.Request.Url.ToString().ToLower().Contains("variance"))
                {
                    if (HttpContext.Current.Request.Url.Scheme.ToLower() != "https")
                    {
                       // HttpContext.Current.Response.Redirect(HttpContext.Current.Request.Url.ToString().ToLower().Replace("http://", "https://"));
                    }
                }
            }
            catch (Exception)
            {
            }
        }
        void Application_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError().GetBaseException();
            bool isAjaxCall = string.Equals("XMLHttpRequest", Context.Request.Headers["x-requested-with"], StringComparison.OrdinalIgnoreCase);
            Server.ClearError();
            var strRequestUrl = Request.Url.ToString();
            if (strRequestUrl.IndexOf("/images/") < 0
                    && strRequestUrl.IndexOf("/DentistImages/") < 0
                    && strRequestUrl.IndexOf("/ImageBank/") < 0
                    && strRequestUrl.IndexOf("favicon.ico") < 0)
            {

                repository.CallAPI<bool, Exception>("Exception/POST", "POST", exc);
                if (!isAjaxCall)
                {
                    if (exc.Message == "session timeout") {
                        Response.Redirect("/Login/Index");                     
                       }
                    else
                        {
                            Response.Redirect("/Error");
                        }
                }
                else
                {
                    if (exc.Message == "session timeout")
                    {
                        Response.StatusCode = 400;
                        Response.AddHeader("Content-Type", "application/json");
                        //Response.Write(JsonConvert.SerializeObject(new APIResponseBase<string>
                        //{
                        //    IsSuccess = false,
                        //    Message = "Authonication required.",
                        //    Result = null,
                        //    StatusCode = 500
                        //}));
                        Response.Write(JsonConvert.SerializeObject(new
                        {
                            Error = "NotAuthorized",
                            LogOnUrl = "/Login/Index"
                        }));
                    }
                    else
                    {
                        Response.StatusCode = 500;
                        Response.Write(JsonConvert.SerializeObject(new APIResponseBase<string>
                        {
                            IsSuccess = false,
                            Message = "Some internal server occured.",
                            Result = null,
                            StatusCode = 500
                        }));
                    }
                }
            }
        }
    }
}
