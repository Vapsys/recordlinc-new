﻿//-- Dependencis - Jquery, Scripts/Common.js

var fileCount = 0;
var errorMsg = "";
function getApprovedImageType() {
    var imageTypes = new Array
        (
        ".jpg",
        ".jpeg",
        ".gif",
        ".tiff",
        ".png",
        ".bmp",
        ".tif"
        );

    return imageTypes;

}

function getApprovedFileType() {
    var fileType = new Array
        (
        ".jpg",
        ".jpeg",
        ".gif",
        ".tiff",
        ".png",
        ".bmp",
        ".pdf",
        ".doc",
        ".docx",
        ".txt",
        ".xls",
        ".xlsx",
        ".zip",
        ".rar",
        ".tif"
        );

    return fileType;

}

function isImageFile(fileName) {
    var ext = getFileExtension(fileName);
    return $.inArray(ext, getApprovedImageType()) >= 0;
}

function isValidFile(fileName) {
    var ext = getFileExtension(fileName);
    return $.inArray(ext, getApprovedFileType()) >= 0;
}

function getFileExtension(fileName) {
    var extension = fileName.substr((fileName.lastIndexOf('.')));
    return extension.toLowerCase();
}
function getOtherFileShow(fileName) {
    var ext = getFileExtension(fileName);
    switch (ext) {
        case ".doc": return "../../Content/images/word.png";
        case ".docx": return "../../Content/images/word.png";
        case ".xls": return "../../Content/images/excel.png";
        case ".csv": return "../../Content/images/excel.png";
        case ".xlsx": return "../../Content/images/excel.png";
        case ".pdf": return "../../Content/images/pdf.png";
        case ".dex": return "../../Content/images/dex.png";
        case ".dcm": return "../../Content/images/dcm.png";
        default: return "../../Content/images/img_1.jpg";
    }
}


function initFileUpload(ctrl, baseDiv, maxSizeInKB) {

    baseDiv = (baseDiv) ? baseDiv : "imagesList";
    maxSizeInKB = (maxSizeInKB) ? maxSizeInKB : 5 * 1024 * 1024 // 5 MB;
    //$("#" + baseDiv1).empty();
    var selectedFiles = ctrl.files;
    for (var i = 0; i < selectedFiles.length; i++) {
        //var fileName = selectedFiles[i].name;
        validateFile(selectedFiles[i], baseDiv, maxSizeInKB);
    }
    if ($.trim(errorMsg).length > 0) {
        showErrorMsg(errorMsg, -1);
    }
}


function validateFile(file, baseDiv, maxSizeInKB) {
    var fileName = file.name;

    if (file.size > maxSizeInKB) {
        //if (errorFileNames)
        //    errorFileNames = file.name;
        //else
        //    errorFileNames += "," + file.name;
        errorMsg += "<br>File size exceeds in file : " + fileName;
        //        showErrorMsg("File size exceeds in files : " + errorFileNames);
        return;
    }
    if (!isValidFile(fileName)) {
        errorMsg += "<br>Filename is invalid for file : " + fileName;
        errorMsg += ". Allowed types are : " + getApprovedFileType().join(", ");
        return;
    }

    fileCount++;


    //reader.onload = function (event) {

    //    $(result).append("<input type='hidden' name='Files' value='" + event.target.result + "' id='Files" + fileCount + "'>");
    //    $(result).append("<input type='hidden' name='FileNames' value='" + fileName + "' id='FileNames" + fileCount + "'>");
    //};
    //reader.readAsDataURL(file);


    renderFile(file, baseDiv, fileCount);
}

//function renderOtherFile(file) {
//    var fileName = file.name;
//    var fileSize = file.size;
//    //alert("Other file File Name : " + fileName
//    //    + " " + "File Size :" + fileSize
//    //    + " Image to show : " + getOtherFileShow(fileName));
//    var imageURL = getOtherFileShow(fileName);

//        //-- Change HTML as per your requirement.
//        html += ;

//    $("#imagesList").append(html);

//}

function renderFile(file, baseDiv, vfileCount) {
    var fileName = file.name;
    var fileSize = file.size;
    var reader = new FileReader();
    var result = $("#" + baseDiv);

    reader.onload = function (e) {
        var html = '   <input type="hidden" name="Files" value="' + event.target.result + '" id="Files' + vfileCount + '"> ';
        html += "<input type='hidden' name='FileNames' value='" + fileName + "' id='FileNames" + vfileCount + "'>";

        if (isImageFile(fileName)) {
            html += getImageFileHTML(fileName, vfileCount, e);
        }
        else {
            var imageURL = getOtherFileShow(fileName);
            html += getOtherFileHTML(fileName, vfileCount, imageURL);
        }
        $(result).append(html);

    }
    reader.readAsDataURL(file);
}


function deleteSelectedFile(ctrl, id) {

    $(ctrl).parent().hide();
    $("#Files" + id).remove();
    $("#FileNames" + id).remove();
}