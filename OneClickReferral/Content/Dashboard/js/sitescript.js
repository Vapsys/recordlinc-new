$(document).ready(function() {

    $( ".dropdown-submenu" ).click(function(event) {
        // stop bootstrap.js to hide the parents
        event.stopPropagation();
        // hide the open children
        $( this ).find(".dropdown-submenu").removeClass('open');
        // add 'open' class to all parents with class 'dropdown-submenu'
        $( this ).parents(".dropdown-submenu").addClass('open');
        // this is also open (or was)
        $( this ).toggleClass('open');
    });
    
});
$('[data-toggle=offcanvas]').click(function() {
    $('.row-offcanvas').toggleClass('active');
    $('.collapse').toggleClass('in').toggleClass('hidden-xs').toggleClass('visible-xs');
});


/*$(document).ready(function(){
	$(".opener").click(function(){
		$("#content").toggleClass("active");
	});
});*/


/*$("a.opener[data-toggle]").on("click", function(e) {
  e.preventDefault();  // prevent navigating
  var selector = $(this).data("toggle");  // get corresponding element
  $(".widgets-block").hide();
  $(selector).show();
});*/

/*$('.table3 a.opener').on('click', function(){
    $(this).addClass('active');
    $(this).parent().siblings().find('a.opener').removeClass('active');
});*/


/*$('.table3 tr a').click(

function(e) {
    e.preventDefault(); // prevent the default action
    e.stopPropagation; // stop the click from bubbling
    $(this).closest('.opener').find('.opener').removeClass('active');
    $(this).addClass('active');
});*/



$('.input-holder input').focus(function () {
    $(this).parent().addClass('active');
}).blur(function () {
    $(this).parent().removeClass('active');
});

$(document).ready(function () {
    $('.acc-opener').on('click', function () {
        $(this).toggleClass('active');
    });
});


function hideshow(id) {
	//$( id ).toggleClass( "tab" );
	$( id ).toggleClass( "active" );
	//$( this ).addClass( "active" );
}
$(document).ready(function(){
	$('.nav-pills2 li').click(function(){
		$( this ).toggleClass( "active" );
	});
    /*$(".inner-menu a").click(function (e) {
        e.stopPropagation();
        $('.btn-group.btn-holder').toggleClass( "active" );
    });*/
});
$(function () {
    $('.inner-menu a').click(function (evt) {
        evt.stopPropagation(); //stops the document click action
        $('.btn-group.btn-holder').siblings().removeClass('active');
        $('.btn-group.btn-holder').toggleClass('active');
    });


    $(document).click(function () {
        $('.btn-group.btn-holder').removeClass('active'); //make all inactive
    });
});
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});

$('body').on('hidden.bs.modal', function () {
    if($('.modal.in').length > 0)
    {
        $('body').addClass('modal-open');
    }
});
/*  window.onscroll = function() {myFunction()};
   var header = document.getElementById("thead-inverse");
   var sticky = header.offsetTop;
   function myFunction() {
   if (window.pageYOffset > sticky) {
    header.classList.add("stick");
  } else {
    header.classList.remove("stick");
  }
}*/
window.onscroll = function() {myFunction()};
   var header = document.getElementById("top-section");
   var sticky = header.offsetTop;
   function myFunction() {
   if (window.pageYOffset > sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
$('.search.text-right .btn-default').click(function() {
  $('.search.text-right').toggleClass('show-input');
});
