﻿using OneClickReferral.App_Start;
using OneClickReferral.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
namespace OneClickReferral.Controllers
{
    [CustomAuthorize]
    public class PatientsController : BaseController
    {
        // GET: Patients
        public ActionResult Index()
        {
            TempData.Clear();
            return View(CallAPI<List<AddressDetails>, string>("Patients/GetLocations", "GET", string.Empty).Result);
        }
        public ActionResult GetPatients(PatientFilter patientFilter)
        {
            return View(CallAPI<List<PatientDetailsOfDoctor>, PatientFilter>("Patients/Get", "POST", patientFilter).Result);
        }
        public ActionResult DeletePatient(DeletePatient delete)
        {
            return Json(CallAPI<PatientDelete, DeletePatient>("Patients/Delete", "POST", delete).Result);
        }
        public ActionResult SearchPatient(PatientFilter patientFilter)
        {
            return Json(CallAPI<List<PatientDetailsOfDoctor>, PatientFilter>("Patients/Get", "POST", patientFilter).Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SendForm(SendForm Obj)
        {
            Obj.DoctorName = SessionManagement.FirstName;
            return Json(CallAPI<bool, SendForm>("Patients/SendForm", "POST", Obj).Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult History()
        {
            if (Convert.ToInt32(TempData["PatientId"]) > 0)
            {
                ViewBag.EncryptedUserId = CallAPI<string, string>("PMS/GetEncryptedUserId", "GET").Result;
                PatientHistoryViewModel Obj = CallAPI<PatientHistoryViewModel, string>($"Patients/PatientDetails?PatientId={Convert.ToInt32(TempData["PatientId"])}&SystemTimeZoneName={SessionManagement.TimeZoneSystemName}", "GET", string.Empty).Result;
                Obj.IsAuthorize = "true";
                TempData.Keep();
                return View(Obj);
            }
            else
            {
                return RedirectToAction("Index");
            }

        }
        public ActionResult PatientHistory(int PatientId,bool PatientForm = false)
        {
            TempData["PatientId"] = PatientId;
            TempData["PatientForm"] = PatientForm;
            return RedirectToAction("History");
        }
        public ActionResult GetUserListofTeam(int PatientId, string selected)
        {
            return Json(CallAPI<List<SelectListItem>, string>($"Patients/ComposeCommunicationList?PatientId={PatientId}&ColleagueId={selected}", "GET", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }
          public ActionResult GetSelectedColleagueList(int PatientId, string selected)
        {
            return Json(CallAPI<List<SelectListItem>, string>($"Patients/GetSelectedColleagueList?PatientId={PatientId}&ColleagueId={selected}", "GET", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetSelectedUserListforComm(int PatientId)
        {
            return Json(CallAPI<string, string>($"Patients/GetSelectedUserList?PatientId={PatientId}", "GET", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddInsurance(InsuranceCoverage Obj, int PatientId)
        {
            return Json(CallAPI<bool, InsuranceCoverage>($"Patients/AddInsuranceCoverage?PatientId={PatientId}", "POST", Obj).Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddMedicalHistory(BO.Models.Compose1ClickMedicalHistory medicalHistory, int PatientId)
        {
            return Json(CallAPI<bool, BO.Models.Compose1ClickMedicalHistory>($"Patients/AddMedicalHistory?PatientId={PatientId}", "POST", medicalHistory).Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddDentalHistory(BO.Models.Compose1ClickDentalHistory dentalHistory, int PatientId)
        {
            return Json(CallAPI<bool, BO.Models.Compose1ClickDentalHistory>($"Patients/AddDentalHistory?PatientId={PatientId}", "POST", dentalHistory).Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetPatientImages(int PatientId)
        {
            List<UplodedImagesViewModel> lst = CallAPI<List<UplodedImagesViewModel>, string>($"Patients/GetImages?id={PatientId}", "GET", string.Empty).Result;
            return View("_PartialUploadedImagesSection", lst);
        }
        public ActionResult GetPatientDocuments(int PatientId)
        {
            List<UplodedImagesViewModel> lst = CallAPI<List<UplodedImagesViewModel>, string>($"Patients/GetDocuments?id={PatientId}&TimeZone={SessionManagement.TimeZoneSystemName}", "GET", string.Empty).Result;
            return View("_PartialUploadedImagesSection", lst);
        }
        public ActionResult DeleteImage(int ImageId, int MotangeId, int PatientId)
        {
            return Json(CallAPI<bool, string>($"Patients/DeleteImage?ImageId={ImageId}&MontageId={MotangeId}", "POST", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteDocument(int MotangeId, int PatientId)
        {
            return Json(CallAPI<bool, string>($"Patients/DeleteDocument?MontageId={MotangeId}", "POST", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetImageDetail(int ImageId, int PatientId, int Type)
        {
            return View("_PartialFileEdit", CallAPI<EditFilesViewModel, string>($"Patients/FileDetail?id={ImageId}&Type={Type}&PatientId={PatientId}&TimeZone={SessionManagement.TimeZoneSystemName}", "GET", string.Empty).Result);
        }
        public ActionResult EditFileDetail(AlterFile alterFile)
        {
            return Json(CallAPI<bool, AlterFile>($"Patients/EditFileDetail", "POST", alterFile).Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetReferralMessageDetail(ReferralMessageFilter filter)
        {
            BO.ViewModel.APIResponseBase<Models.ReferralMessage> aPIResponse = CallAPI<BO.ViewModel.APIResponseBase<Models.ReferralMessage>, ReferralMessageFilter>("OneClickReferral/ReferralMessageDetails", "POST", filter).Result;
            return PartialView("_PartialReferralMessageDetail", aPIResponse.Result);
        }
        public ActionResult GetReferralCategory(int ReferralCardId)
        {
            return View("_PartialReferralCategory", CallAPI<List<ReferralCategory>, string>($"Patients/GetReferralCategory?id={ReferralCardId}", "GET", string.Empty).Result);
        }
        public ActionResult EditPatientInfo(BO.ViewModel.PatientEditableVM patientEditableVM)
        {
            return Json(CallAPI<bool, BO.ViewModel.PatientEditableVM>($"Patients/EditPatientHistory", "POST", patientEditableVM).Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult CheckAssignPatientID(int AssignedPatientId, int PatientId)
        {
            return Json(CallAPI<bool, string>($"Patients/CheckAssignPatientID?PatientId={PatientId}&AssignPatientid={AssignedPatientId}", "GET", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddPatient(Patients patients)
        {
            return Json(CallAPI<int, Patients>($"Patients/AddPatientInfo", "POST", patients).Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddPatientProfile(PatientProfileImage profileImage)
        {
            Logger log = new Logger();
            log.WriteLog("Add Patient Image Start", true);
            log.WriteLog($"Request.Files.Count: {Request.Files.Count}", true);

            if (Request.Files.Count > 0)
            {
                Stream fs = Request.Files[0].InputStream;
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                profileImage.Base64URL = Convert.ToBase64String(bytes, 0, bytes.Length);
            }
            log.WriteLog($"Base64URL: {profileImage.Base64URL}", true);
            string Result = CallAPI<string, PatientProfileImage>($"Patients/AddPatientProfile", "POST", profileImage).Result;
            return RedirectToAction("History");
        }
        public ActionResult SentMessageFromPatientHistory(BO.Models.ComposeCommunication compose)
        {
            return Json(CallAPI<bool, BO.Models.ComposeCommunication>($"Patients/ComposeCommunication", "POST", compose).Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Add()
        {
            PatientHistoryViewModel Obj = new PatientHistoryViewModel();
            if (Convert.ToInt32(TempData["PatientId"]) > 0)
            {
                Obj = CallAPI<PatientHistoryViewModel, string>($"Patients/PatientDetails?PatientId={Convert.ToInt32(TempData["PatientId"])}&SystemTimeZoneName={SessionManagement.TimeZoneSystemName}", "GET", string.Empty).Result;
                TempData.Keep();
            }
            ViewBag.LocationsId = CallAPI<List<AddressDetails>, string>("Patients/GetLocations", "GET", string.Empty).Result;
            ViewBag.ColleaguesId = CallAPI<List<SelectListItem>, string>("Patients/GetColleagues", "GET", string.Empty).Result;
            ViewBag.State = CallAPI<List<StateList>, string>("Common/GetStates?CountryCode=US", "GET", string.Empty).Result;
            return View(Obj);
        }
        public ActionResult AddPatients(int PatientId = 0)
        {
            TempData["PatientId"] = PatientId;
            return RedirectToAction("Add");
        }
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult AddPatients(PatientHistoryViewModel Obj)
        {
            int resutl = CallAPI<int, PatientHistoryViewModel>("Patients/AddPatient", "POST", Obj).Result;
            return RedirectToAction("PatientHistory", new { PatientId = (resutl == 0) ? Obj.PatientId : resutl });
        }

        public ActionResult AddPatientsData(PatientHistoryViewModel Obj, int PatientId)
        {
            Obj.PatientId = PatientId;
            bool resutl = CallAPI<bool, PatientHistoryViewModel>("Patients/AddPatient", "POST", Obj).Result;
            return RedirectToAction("PatientHistory", new { PatientId = Obj.PatientId });
        }
        public ActionResult CheckPatientsEmailIdandUniqeId(CheckPatient check)
        {
            return Json(CallAPI<object, CheckPatient>("Patients/CheckPatient", "POST", check).Result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ViewColleagueMessage(int Id)
        {
            return PartialView("_PartialMessageDetails", CallAPI<ColleaguesMessagesOfDoctorViewModel, string>($"Patients/GetMessageDetails?id={Id}&TimeZone={SessionManagement.TimeZoneSystemName}", "POST", string.Empty).Result);
        }
        public ActionResult ViewPatientMessage(int Id)
        {
            return PartialView("_PartialPatientMessageDetails", CallAPI<OCRPatientMessagesOfDoctor, string>($"Patients/GetPatientMessageDetails?id={Id}&TimeZone={SessionManagement.TimeZoneSystemName}", "POST", string.Empty).Result);
        }

        public ActionResult PrintCommunication()
        {
            int PatientId = Convert.ToInt32(TempData["PatientId"]);
            TempData.Keep();
            return View("PrintAllCommunicationSection", CallAPI<List<ColleaguesMessagesOfDoctorViewModel>, string>($"Patients/GetCommunicationDetails?id={TempData["PatientId"]}&TimeZone={SessionManagement.TimeZoneSystemName}", "GET", string.Empty).Result);
        }

        public ActionResult PrintReferralMessage(int MessageId)
        {
            return View("_PrintReferralMessage", CallAPI<List<ReferralMessageDetails>, string>($"Patients/GetReferral?id={MessageId}&TimeZone={SessionManagement.TimeZoneSystemName}", "GET", string.Empty).Result);
        }
        public ActionResult PrintColleagueMessage(int MessageId)
        {
            return View("_PrintColleaguesMessage", CallAPI<ColleaguesMessagesOfDoctorViewModel, string>($"Patients/GetMessage?id={MessageId}&TimeZone={SessionManagement.TimeZoneSystemName}", "GET", string.Empty).Result);
        }
        public ActionResult PrintPatientMessage(int MessageId)
        {
            return View("_PrintPatientMessage", CallAPI<PatientMessagesOfDoctorViewModel, string>($"Patients/GetPatientMessage?id={MessageId}&TimeZone={SessionManagement.TimeZoneSystemName}", "GET", string.Empty).Result);
        }
        public ActionResult DeleteRelation(int PatientId, int ToPatientId)
        {
            return Json(CallAPI<bool, BO.ViewModel.DeleteRelationRequest>($"Patients/DeleteRelation", "POST", new BO.ViewModel.DeleteRelationRequest()
            {
                FromPatientId = PatientId,
                ToPatientId = ToPatientId
            }).Result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadPatientscrolling(PatientCommunicationSort objPatientCommunicationSort)
        {
            List<ColleaguesMessagesOfDoctorViewModel> lst = CallAPI<List<ColleaguesMessagesOfDoctorViewModel>, PatientCommunicationSort>($"Patients/GetCommunicationHistoryForPatient", "POST", objPatientCommunicationSort).Result;

            return PartialView("_PartialCommunicationListPortion", lst);
        }

        public ActionResult GetPatientHostoryCommunicationOnSorting(PatientCommunicationSort objPatientCommunicationSort)
        {
            List<ColleaguesMessagesOfDoctorViewModel> lst = CallAPI<List<ColleaguesMessagesOfDoctorViewModel>, PatientCommunicationSort>($"Patients/GetCommunicationHistoryForPatient", "POST", objPatientCommunicationSort).Result;

            return PartialView("_PartialCommunicationListPortion", lst);
        }
        public ActionResult PatientInsuranceDataDetails(int Id)
        {
            PatientInsuranceViewModel objPatientInsuranceViewModel = CallAPI<PatientInsuranceViewModel, string>($"Common/GetPatientInsuranceDataDetails?intPatientId={Id}", "GET", string.Empty).Result;
            return PartialView("_PartialPatientInsuranceDataPopup", objPatientInsuranceViewModel);
        }
      
        public ActionResult PatientInsurancePdfView(int Id)
        {
            PatientInsuranceViewModel objPatientInsuranceViewModel = CallAPI<PatientInsuranceViewModel, string>($"Common/GetPatientInsuranceDataDetails?intPatientId={Id}", "GET", string.Empty).Result;
            //PatientInsuranceViewModel objPatientInsuranceViewModel = new PatientInsuranceViewModel();
            // return new Rotativa.ViewAsPdf("PatientInsurancePdfView", model) { FileName = "TestPartialViewAsPdf.pdf" };
            return new Rotativa.ViewAsPdf("PatientInsurancePdfView", objPatientInsuranceViewModel) {
                FileName = "InsuranceDetails.pdf",
                PageSize = Rotativa.Options.Size.A4,
                PageOrientation = Rotativa.Options.Orientation.Portrait,
                PageMargins = new Rotativa.Options.Margins(10,5,10,5)
                
            }; 
            //return View("PatientInsurancePdfView", objPatientInsuranceViewModel);
        }       
    }
}