﻿using OneClickReferral.App_Start;
using OneClickReferral.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneClickReferral.Controllers
{
    public class PermissionController : BaseController
    {
        // GET: Permission
        public ActionResult Index(int id)
        {
            PermissionResult Permission = CallAPI<PermissionResult, List<MemberPlanDetail>>($"Permission/FeaturePermission?id={id}", "POST", SessionManagement.GetMemberPlanDetails).Result;
            if (Permission.HasPermission || !Permission.DoesMembershipAllow)
            {
                return PartialView("~/Views/Shared/UpgradeMembership.cshtml", Permission);
            }
            return Json(true, JsonRequestBehavior.AllowGet);
        }
    }
}