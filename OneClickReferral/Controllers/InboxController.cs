﻿using BO.ViewModel;
using OneClickReferral.App_Start;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace OneClickReferral.Controllers
{
    [CustomAuthorize]
    public class InboxController : BaseController
    {
        /// <summary>
        /// Get Partial Compose Message View
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ActionResult PartialComposeMessage(ComposeMessageRequest request)
        {
            var response = CallAPI<ComposeMessage, ComposeMessageRequest>("OCRInbox/Compose", "POST", request);
            return View("PartialComposeMessage", response.Result);
        }

        /// <summary>
        /// Get Colleague List 
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        public ActionResult GetColleagueList(string q)
        {
            var response = CallAPI<List<ColleagueData>, string>($"OCRInbox/GetColleagueList?SearchText={HttpUtility.UrlEncode(q)}", "GET");
            return Json(response.Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Patient List for Compose time Patient Attachment
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        public ActionResult GetPatientList(string q)
        {
            var response = CallAPI<List<PatientsData>, string>($"OCRInbox/GetPatientList?SearchText={HttpUtility.UrlEncode(q)}", "GET");
            return Json(response.Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Composed message Save as Draft.
        /// </summary>
        /// <param name="objcomposedetail"></param>
        /// <returns></returns>
        public ActionResult SaveAsDraft(ComposeMessage objcomposedetail)
        {
            var response = CallAPI<bool, ComposeMessage>("OCRInbox/SaveAsDraft", "POST", objcomposedetail);
            return Json(response.Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Send Message To Colleagues
        /// </summary>
        /// <param name="objcomposedetail"></param>
        /// <returns></returns>
        public ActionResult SendMessageToColleagues(ComposeMessage objcomposedetail)
        {
            var response = CallAPI<bool, ComposeMessage>("OCRInbox/SendMessageToColleagues", "POST", objcomposedetail);
            return Json(response.Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Partial Colleagues Forward Message
        /// </summary>
        /// <param name="objcomposedetails"></param>
        /// <returns></returns>
        public ActionResult PartialColleagueForwardMessage(ComposeMessage objcomposedetails)
        {
            var response = CallAPI<ComposeMessage, ComposeMessage>("OCRInbox/Forward", "POST", objcomposedetails);
            ViewBag.Title = "Forward";
            return View("PartialColleagueForwardMessage", response.Result);
        }

        /// <summary>
        /// Forward Time Save Message AS Draft.
        /// </summary>
        /// <param name="objcomposedetail"></param>
        /// <returns></returns>
        public ActionResult ForwardSaveAsDraft(ComposeMessage objcomposedetail)
        {
            var response = CallAPI<bool, ComposeMessage>("OCRInbox/ForwardSaveAsDraft", "POST", objcomposedetail);
            return Json(response.Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Forward time Message To Colleagues.
        /// </summary>
        /// <param name="objcomposedetail"></param>
        /// <returns></returns>
        public ActionResult ForwardMessageToColleagues(ComposeMessage objcomposedetail)
        {
            var response = CallAPI<bool, ComposeMessage>("OCRInbox/ForwardMessageToColleagues", "POST", objcomposedetail);
            return Json(response.Result, JsonRequestBehavior.AllowGet);
        }
    }
}