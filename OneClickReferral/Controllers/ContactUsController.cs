﻿using BO.Models;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Web.Mvc;
using System;

namespace OneClickReferral.Controllers
{
    public class ContactUsController : BaseController
    {
        // GET: ContactUs
        public ActionResult Index()
        {
            ViewBag.Message = null;
            if (Convert.ToBoolean(TempData["Sucess"]))
            {
                ViewBag.Message = true;
                //TempData.Clear();
            }
            TempData.Remove("Sucess");

            return View();
        }
        [HttpPost]
        public ActionResult Submit(ContactUs objContactUs)
        {
            string R =  CallAPI<string, ContactUs>("OneClickReferral/ContactUS", "POST", objContactUs).Result;
            TempData["Sucess"] = true;
            return new JsonResult()
            {
                JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                Data = new { result = "success" }
            };
        }
    }
}