﻿using System.Web.Mvc;
using OneClickReferral.App_Start;

namespace OneClickReferral.Controllers
{
    public class LoginController : BaseController
    {
        public ActionResult Index()
        {
            if (!string.IsNullOrWhiteSpace(SessionManagement.access_token))
            {
                return RedirectToAction("Indexs","OneClick", new { HRVCA = SessionManagement.access_token });
            }
            return View();
        }
        public ActionResult CheckUserHasAccess()
        {
            return Json(CallAPI<bool, string>($"Permission/AccessOfOCR?id=21", "GET", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }
    }
}