﻿using BO.Models;
using BO.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using OneClickReferral.Helpers;
using System.Configuration;
using Newtonsoft.Json;
using OneClickReferral.App_Start;
using System.IO;

namespace OneClickReferral.Controllers
{
    public class ZipwhipController : BaseController
    {

        /// <summary>
        /// Zipwhip configration listing
        /// </summary>
        /// <returns></returns>
        public ActionResult ZipwhipConfig()
        {

            List<ZipwhipAccount> obj = new List<ZipwhipAccount>();
            ViewBag.LocationList = CallAPI<List<LocationList>, string>($"Zipwhip/GetDentistLocations?UserId={SessionManagement.UserId}", "GET", string.Empty).Result;
            obj = CallAPI<APIResponse<List<ZipwhipAccount>>, string>($"Zipwhip/ListOfProviderConfig?UserId={SessionManagement.UserId}", "GET", string.Empty).Result.Result;
            return View(obj);

        }

        /// <summary>
        /// Check phone number is eligible or not
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult CheckEligible(ZipwhipModel model)
        {
            #region Prepare required parameter for this API 
            model.zipwhip_url = ZipwhipApiURL.Provisioning_Eligible;
            model.api_method = "get";
            model.api_key = ConfigurationSettings.AppSettings["ZIPWHIP_APIKEY"];
            model.parameter.api_key = model.api_key;
            #endregion
            var response = CallAPI<APIResponseBase<ProvisionEligible>, ZipwhipModel>("Zipwhip/Eligible", "POST", model);
            if (response.Result == null) { return Json(response, JsonRequestBehavior.AllowGet); } else { return Json(response.Result, JsonRequestBehavior.AllowGet); }
        }

        /// <summary>
        /// Add Provisioning in Zipwhip Account
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult AddProvisioning(ZipwhipAccount model)
        {
            #region Prepare required parameter for this API 
            ZipwhipModel obj = new ZipwhipModel();
            obj.zipwhip_url = ZipwhipApiURL.Provisioning_Add;
            obj.api_method = "get";
            obj.api_key = ConfigurationSettings.AppSettings["ZIPWHIP_APIKEY"];
            obj.parameter.api_key = obj.api_key;
            obj.parameter.phone_number = model.ProviderConfig.username;
            obj.parameter.account_name = model.ProviderConfig.accountname;
            obj.parameter.email = model.ProviderConfig.email;
            obj.parameter.feature_package = ConfigurationSettings.AppSettings["FEATURE_PACKAGE"];
            #endregion
            var response = CallAPI<APIResponseBase<ProvisionEligible>, ZipwhipModel>("Zipwhip/AddProvision", "POST", obj);
            if (response.Result == null) { return Json(response, JsonRequestBehavior.AllowGet); } else { return Json(response.Result, JsonRequestBehavior.AllowGet); }
        }

        /// <summary>
        /// Insert provider configration in out database
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="LocationId"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public JsonResult InsertProviderConfig(int UserId, int LocationId, ProviderConfig obj)
        {
            #region Prepare required parameter for this API 
            obj.feature_package = ConfigurationSettings.AppSettings["FEATURE_PACKAGE"];
            obj.api_key = ConfigurationSettings.AppSettings["ZIPWHIP_APIKEY"];

            ZipwhipAccount ProvAcnt = new ZipwhipAccount();
            ProvAcnt.UserId = UserId;
            ProvAcnt.LocationId = LocationId;
            ProvAcnt.ProviderConfigJson = JsonConvert.SerializeObject(obj);
            ProvAcnt.ProviderConfig = obj;
            #endregion
            var response = CallAPI<APIResponseBase<int>, ZipwhipAccount>("Zipwhip/InsertProviderConfig", "POST", ProvAcnt);
            if (response.Result == null) { return Json(response, JsonRequestBehavior.AllowGet); } else { return Json(response.Result, JsonRequestBehavior.AllowGet); }

        }

        [ValidateInput(false)]
        /// <summary>
        /// Zipwhip message send in sms history table
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="LocationId"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public JsonResult SendMessage(int UserId, int PatientId, string MsgBody, string RecepientNumber)
        {
            #region Prepare required parameter for this API 
            SMSDetails smsdtl = new SMSDetails();
            smsdtl.UserId = UserId;
            smsdtl.SenderId = UserId;
            smsdtl.SenderType = "DENTIST";
            smsdtl.ReceiverId = PatientId;
            smsdtl.ReceiverType = "PATIENT";
            smsdtl.RecepientNumber = RecepientNumber;
            smsdtl.Status = (int)SMSHistoryStatus.Inserting; /* {0:Inserting, 1:Sent, 2:Recevied, 3:Failed, 4:Deliverd, 5:Queue} */
            smsdtl.MessageType = (int)SMSHistoryMessageType.Send;
            smsdtl.MessageBody = MsgBody;
            smsdtl.SMSIntegrationMasterId = 2;
            #endregion
            var response = CallAPI<APIResponseBase<int>, SMSDetails>("Zipwhip/SendMessage", "POST", smsdtl);
            if (response.Result == null) { return Json(response, JsonRequestBehavior.AllowGet); } else { return Json(response.Result, JsonRequestBehavior.AllowGet); }

        }

        /// <summary>
        /// Get Zipwhip token
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public JsonResult GetZipwhipToken(string username, string password)
        {
            //string df = "6308618275";
            //var ObjDentist = CallAPI<APIResponseBase<int>, string>($"Zipwhip/FindDentistId?PhoneNumber={df}&Type=0", "GET", string.Empty);

            ZipwhipModel model = new ZipwhipModel();
            #region Prepare required parameter for this API 
            model.zipwhip_url = ZipwhipApiURL.Messaging_Login;
            model.api_method = "post";
            model.api_key = ConfigurationSettings.AppSettings["ZIPWHIP_APIKEY"];
            model.parameter.api_key = model.api_key;
            model.parameter.username = username;
            model.parameter.password = password;
            #endregion
            var response = CallAPI<APIResponseBase<UserLogin>, ZipwhipModel>("Zipwhip/ZipwhipLogin", "POST", model);
            if (response.Result == null) { return Json(response, JsonRequestBehavior.AllowGet); } else { return Json(response.Result, JsonRequestBehavior.AllowGet); }
        }

        /// <summary>
        /// Webhook for receive sms
        /// </summary>
        /// <returns></returns>
        //public ActionResult WebhookForReceiveSMS()
        //{
        //    try
        //    {
        //        var req = Request.InputStream;
        //        var json = new StreamReader(req).ReadToEnd();
        //        ZipwhipWebhookResponse response = JsonConvert.DeserializeObject<ZipwhipWebhookResponse>(json);
        //        if (!string.IsNullOrEmpty(json) && response != null)
        //        {
        //            var ObjDentist = CallAPI<APIResponseBase<int>, string>($"Zipwhip/FindDentistId?PhoneNumber={response.finalDestination}&Type=0", "GET", string.Empty);
        //            var ObjPatient = CallAPI<APIResponseBase<int>, string>($"Zipwhip/FindDentistId?PhoneNumber={response.finalSource}&Type=1", "GET", string.Empty);

        //            #region Prepare required parameter for this API 
        //            SMSDetails smsdtl = new SMSDetails();
        //            smsdtl.UserId = Convert.ToInt32(ObjPatient.Result);
        //            smsdtl.SenderId = Convert.ToInt32(ObjPatient.Result);
        //            smsdtl.SenderType = "PATIENT";
        //            smsdtl.ReceiverId = Convert.ToInt32(ObjDentist.Result);
        //            smsdtl.ReceiverType = "DENTIST";
        //            smsdtl.RecepientNumber = response.finalDestination;
        //            smsdtl.Status = (int)SMSHistoryStatus.Sent; /* {0:Inserting, 1:Sent, 2:Recevied, 3:Failed, 4:Deliverd, 5:Queue} */
        //            smsdtl.MessageBody = response.body;
        //            #endregion

        //            var obj = CallAPI<APIResponseBase<int>, SMSDetails>("Zipwhip/SendMessage", "POST", smsdtl);
        //        }
        //        return new HttpStatusCodeResult(200);
        //    }
        //    catch (Exception ex)
        //    {

        //        return new HttpStatusCodeResult(200);
        //    }
        //}

        //public static string SafeString(string str)
        //{
        //    string rtn = string.Empty;
        //    string alpha = "0123456789";
        //    if (!string.IsNullOrEmpty(str))
        //    {
        //        str = str.Replace("+", "");
        //        for (int i = 0; i < str.Length; i++)
        //        {
        //            if (str[i] == '1')
        //                continue;

        //            for (int j = 0; j < alpha.Length; j++)
        //            {
        //                if (str[i] == alpha[j])
        //                {
        //                    rtn += str[i];
        //                    break;
        //                }
        //            }
        //        }
        //    }
        //    return rtn;
        //}

        /// <summary>
        /// Assign zipwhip token
        /// </summary>
        /// <returns></returns>
        public ActionResult AssignZipwhipToken()
        {
            try
            {
                List<ZipwhipAccount> ObjDentist = CallAPI<APIResponseBase<List<ZipwhipAccount>>, string>("Zipwhip/AssignZipwhipToken", "GET", string.Empty).Result.Result;
            }
            catch (Exception ex)
            {
            }
            return View();
        }

        /// <summary>
        /// Get Address Details By Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public JsonResult GetAddressDetail(int Id)
        {
            AddressDetails response = CallAPI<AddressDetails, string>($"Profile/GetAddress?id={Id}", "GET", string.Empty).Result;
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetDentistLocations()
        {
            List<LocationList> response = CallAPI<List<LocationList>, string>($"Zipwhip/GetDentistLocations?UserId={SessionManagement.UserId}", "GET", string.Empty).Result;
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}