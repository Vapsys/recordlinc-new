﻿using OneClickReferral.App_Start;
using OneClickReferral.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace OneClickReferral.Controllers
{
    [CustomAuthorize]
    public class ColleaguesController : BaseController
    {
        // GET: Colleagues
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Get Colleague List
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public ActionResult GetColleagueList(FilterColleagues Obj)
        {
            return PartialView("_PartialColleagueListing", CallAPI<List<Models.ColleagueDetails>, FilterColleagues>("Colleagues/Get", "POST", Obj).Result);
        }

        /// <summary>
        /// Delete Colleagues
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public ActionResult DeleteColleagues(DeleteColleagues Obj)
        {
            return Json(CallAPI<bool, DeleteColleagues>("Colleagues/DeleteColleagues", "POST", Obj).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Enable Has Button
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EnableHasButton(EnableHasButton Obj)
        {
            return Json(CallAPI<bool, EnableHasButton>("Colleagues/EnableHasButton", "POST", Obj).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Update Has Button
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateHasButton(UpdateHasButton Obj)
        {
            return Json(CallAPI<bool, UpdateHasButton>("Colleagues/UpdateHasButton", "POST", Obj).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Search Colleagues
        /// </summary>
        /// <returns></returns>
        public ActionResult Search()
        {
            //CallAPI<List<Models.SpeacilitiesOfDoctor>, string>("Colleagues/DentistSpecialty", "GET").Result
            return View("SearchColleague", CallAPI<List<StateList>, string>("Common/GetStates?CountryCode=US", "GET", string.Empty).Result);
        }

        /// <summary>
        /// Search Dentist 
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SearchDentistList(FilterSearchColleagues Obj)
        {
            List<SearchColleagueViewModel> lst= null;
            lst = CallAPI<List<SearchColleagueViewModel>, FilterSearchColleagues>("Colleagues/SearchColleagues", "POST", Obj).Result;
            return PartialView("_PartialSearchColleagues", lst);
        }
        
        /// <summary>
        /// Add Dentist as Colleague
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddColleagues(AddColleagues Obj)
        {
            return Json(CallAPI<bool, AddColleagues>("Colleagues/AddColleague", "POST", Obj).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Main page of Invite colleague
        /// </summary>
        /// <returns></returns>
        public ActionResult Invite()
        {
            return View();
        }

        /// <summary>
        /// Upload Excel sheet for Invite Colleagues
        /// </summary>
        /// <returns></returns>
        public ActionResult UploadExcelSheet()
        {
            string UploadedFileIds = string.Empty;
            var files = System.Web.HttpContext.Current.Request.Files;
            List<string> objId = new List<string>();
            for (int i = 0; i < files.Count; i++)
            {
                HttpPostedFileBase file = new HttpPostedFileWrapper(files[i]);
                string Filename = string.Empty; string FileExtension = string.Empty; string NewFileName = string.Empty;
                if (System.Web.HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || System.Web.HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                {
                    string[] testfiles = file.FileName.Split(new char[] { '\\' });
                    Filename = testfiles[testfiles.Length - 1];
                }
                else
                {
                    Filename = file.FileName;
                }

                Filename = Regex.Replace(Filename, @"[^0-9a-zA-Z\._]", string.Empty);
                NewFileName = "$" + DateTime.Now.Ticks + "$" + Filename;
                FileExtension = Path.GetExtension(Filename).ToLower();

                UploadedFileIds = CallAPI<string, string>("Colleagues/InsertTempFile?FileName="+ NewFileName, "POST", string.Empty).Result;
                NewFileName = Path.Combine(ConfigurationManager.AppSettings["CSVUpload"], NewFileName);
                file.SaveAs(NewFileName);
            }
            //UploadedFileIds = string.Join(",", objId);
            //return Request.CreateResponse(HttpStatusCode.OK, UploadedFileIds);
            return Json(UploadedFileIds, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Excel sheet details for Invite colleagues.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetExcelFileDetails(string FileIds)
        {
            List<TempFileAttachments> Lst = CallAPI<List<TempFileAttachments>, string>("Colleagues/GetInviteColleagueFile?FileIds=" + FileIds, "GET", string.Empty).Result;
            return PartialView("_PartialExcelFileUploadTemp", Lst);
        }

        /// <summary>
        /// Delete Excel sheet from Invite colleagues
        /// </summary>
        /// <param name="FieldId"></param>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public ActionResult DeleteExcel(int FieldId, string FileName)
        {
            bool Result = CallAPI<bool, string>("Colleagues/DeleteExcel?FileId="+FieldId+ "&FileName="+FileName, "POST", string.Empty).Result;
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Send Invitation for add as Colleague into website.
        /// </summary>
        /// <param name="inviteColleague"></param>
        /// <returns></returns>
        public ActionResult InviteColleague(InviteColleague inviteColleague)
        {
            //string Result = CallAPI<string, InviteColleague>("Colleagues/Invite", "POST", inviteColleague).Result;
            //return Json(Result, JsonRequestBehavior.AllowGet);
            ResponseMeassge Lst = CallAPI<ResponseMeassge, InviteColleague>("Colleagues/Invite", "POST", inviteColleague).Result;
            return PartialView("_PartialInviteColleagueMessage", Lst);
        }

        /// <summary>
        /// Get State list for Search colleague field.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetStateList()
        {
            return Json(CallAPI<List<StateList>, string>("Common/GetStates?CountryCode=US", "GET", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }
    }
}