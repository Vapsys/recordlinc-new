﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OneClickReferral.Models;
namespace OneClickReferral.Controllers
{
    public class InsuranceController : BaseController
    {
        // GET: Insurance
        public ActionResult PatientInsuranceDetails(int Id)
        {
            PatientInsuranceDetails objPatientInsuranceDetails = CallAPI<PatientInsuranceDetails, string>($"Common/GetPatientInsuranceDetails?intPatientId={Id}", "GET", string.Empty).Result;
            return PartialView("_PartialInsurancePopup", objPatientInsuranceDetails);
        }

     
    }
}