﻿using OneClickReferral.App_Start;
using OneClickReferral.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System;
using System.IO;

namespace OneClickReferral.Controllers
{
    [CustomAuthorize]
    public class ProfileController : BaseController
    {
        /// <summary>
        /// Profile main page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            int UserId = (!string.IsNullOrWhiteSpace(Convert.ToString(TempData["UserId"]))) ? Convert.ToInt32(TempData["UserId"]) : SessionManagement.UserId;
            TempData["UserId"] = UserId;
            TempData.Keep();
            //Get Dentist Profile Details
            DentistProfileViewModel model = GetDentistProfile(UserId);
            model.specialtyIds = string.Join(",", model.lstSpeacilitiesOfDoctor.Select(c => c.SpecialtyId).ToList());
            ViewBag.Specialty = GetDentistSpeciality(model.specialtyIds);
            ViewBag.hdnViewProfile = (UserId == SessionManagement.UserId) ? false : true;
            if (UserId == SessionManagement.UserId)
            {
                SessionManagement.DoctorImage = model.ImageName;
            }
            return View(model);
        }

        /// <summary>
        /// Temp Action Method for Redirection
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ViewProfile(int id)
        {
            TempData["UserId"] = id;
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Add Address Details
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public ActionResult AddAddress(AddressDetails Obj)
        {
            return Json(CallAPI<bool, AddressDetails>("Profile/AddAddress", "POST", Obj).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Address Details By Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult GetAddressDetail(int Id)
        {
            AddressDetails obj = CallAPI<AddressDetails, string>($"Profile/GetAddress?id={Id}", "GET", string.Empty).Result;
            return PartialView("_PartialAddEditAddress", obj);
        }

        /// <summary>
        /// Get State List based on Country Code.
        /// </summary>
        /// <param name="CountryCode"></param>
        /// <returns></returns>
        public ActionResult GetStateList(string CountryCode)
        {
            return Json(CallAPI<List<StateList>, string>($"Common/GetStates?CountryCode={CountryCode}", "GET", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Remove Address basied on AddressInfo Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveAddress(int id)
        {
            return Json(CallAPI<int, string>($"Profile/RemoveAddress?Id={id}", "POST", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get EPMSId By Address Info Id
        /// </summary>
        /// <param name="ExternalPMSId"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult GetEPMSIdbyAddressId(string ExternalPMSId, int Id)
        {
            return Json(CallAPI<bool, string>($"Profile/CheckPMSId?ExternalPMSId={ExternalPMSId}&id={Id}", "GET", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Update Addresss
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public ActionResult UpdateAddress(AddressDetails Obj)
        {
            return Json(CallAPI<int, AddressDetails>("Profile/AddAddress", "POST", Obj).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Edit Profile Info
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public ActionResult EditPersonalInfo(DentistProfileViewModel Obj)
        {
            bool Response = CallAPI<bool, DentistProfileViewModel>("Profile/EditPersonalInfo", "POST", Obj).Result;
            if (Response)
            {
                SessionManagement.FirstName = Obj.FirstName;
            }
            return Json(Response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Update Description
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public ActionResult UpdateDesciption(Description Obj)
        {
            return Json(CallAPI<bool, Description>($"Profile/AddDescription", "POST", Obj).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Social Medial details of Doctor
        /// </summary>
        /// <returns></returns>
        public ActionResult GetSocialMedia()
        {
            SocialMediaForDoctorViewModel Obj = new SocialMediaForDoctorViewModel();
            List<SocialMediaForDoctorViewModel> lst = CallAPI<List<SocialMediaForDoctorViewModel>, string>("Profile/GetSocialMedia", "GET", string.Empty).Result;
            if (lst.Count > 0)
            {
                Obj = lst.FirstOrDefault();
            }
            return PartialView("_PartialSocialMedia", Obj);
        }

        /// <summary>
        /// Edit Social Media Details
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public ActionResult EditSocialMedia(SocialMediaForDoctorViewModel Obj)
        {
            return Json(CallAPI<bool, SocialMediaForDoctorViewModel>("Profile/AddSocialMedia", "POST", Obj).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Add/Edit Eduction and Training details
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public ActionResult AddEditEducationAndTraining(EducationandTraining Obj)
        {
            return Json(CallAPI<bool, EducationandTraining>("Profile/AddEducationAndTraining", "POST", Obj).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Education Details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult GetEducationDetails(int id)
        {
            EducationandTraining Obj = CallAPI<EducationandTraining, string>($"Profile/GetEducationDetails?id={id}", "GET", string.Empty).Result;
            ViewBag.yearlist = Common.YearList(Convert.ToInt32(Obj.YearAttended));
            return PartialView("_PartialEducationAndTrainingPopUp", Obj);
        }

        /// <summary>
        /// Search University Names for Education details.
        /// </summary>
        /// <param name="UniversityNames"></param>
        /// <returns></returns>
        public ActionResult SearchUniversityNames()
        {
            string SearchText = string.Empty;
            if (Convert.ToString(Request["q"]) != null)
            {
                SearchText = Convert.ToString(Request["q"]);
            }
            return Json(CallAPI<List<University>, string>($"Profile/SearchUniversityNames?UniversityNames={SearchText}", "GET", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Remove Eduction Details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult RemoveEducation(int id)
        {
            return Json(CallAPI<bool, string>($"Profile/RemoveEduction?id={id}", "POST", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Add Profile Images  
        /// </summary>
        /// <param name="profileImage"></param>
        /// <returns></returns>
        public ActionResult AddProfileImage(DentistProfileImage profileImage)
        {
            Logger log = new Logger();
            log.WriteLog("Add Dentist Image Start", true);
            log.WriteLog($"Request.Files.Count: {Request.Files.Count}", true);
            if (Request.Files.Count > 0)
            {
                Stream fs = Request.Files[0].InputStream;
                BinaryReader br = new BinaryReader(fs);
                Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                profileImage.Base64URL = Convert.ToBase64String(bytes, 0, bytes.Length);
            }
            log.WriteLog($"Base64URL: {profileImage.Base64URL}", true);
            string Result = CallAPI<string, DentistProfileImage>($"Profile/AddProfileImage", "POST", profileImage).Result;
            SessionManagement.DoctorImage = Result;
            return RedirectToAction("Index");
        }

        /// <summary>
        /// Get Website based on websiteid
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult GetWebsite(int id)
        {
            return PartialView("_PartialEditPrimarySecondaryWebsite", CallAPI<WebsiteDetails, string>($"Profile/GetWebSiteDetail?Id={id}", "GET", string.Empty).Result);
        }

        /// <summary>
        /// Add Webiste of Popup.
        /// </summary>
        /// <returns></returns>
        public ActionResult Website()
        {
            WebsiteDetails Obj = new WebsiteDetails();
            Obj.UserId = SessionManagement.UserId;
            return PartialView("_PartialInsertWebsitePopUp", Obj);
        }

        /// <summary>
        /// Add Website Details
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public ActionResult AddWebSite(WebsiteDetails Obj)
        {
            return Json(CallAPI<bool, WebsiteDetails>($"Profile/AddWebsite", "POST", Obj).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Removed WEbsite Details
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ActionResult RemoveWebsite(int Id)
        {
            return Json(CallAPI<bool, string>($"Profile/RemoveWebsite?id={Id}", "POST", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Dentist Profile Details
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public DentistProfileViewModel GetDentistProfile(int UserId)
        {
            return CallAPI<DentistProfileViewModel, string>("Profile/Get?UserId=" + UserId, "GET", string.Empty).Result;
        }

        /// <summary>
        /// Get List of Specialty
        /// </summary>
        /// <param name="Specilty"></param>
        /// <returns></returns>
        public List<SelectListItem> GetDentistSpeciality(string Specilty)
        {
            return CallAPI<List<SelectListItem>, string>($"Common/GetSpecialty?Specialty={Specilty}", "GET", string.Empty).Result;
        }

        public JsonResult UpdatePrimary(WebsiteDetails Obj)
        {
            return Json(CallAPI<bool, WebsiteDetails>($"Profile/AddWebsite", "POST", Obj).Result, JsonRequestBehavior.AllowGet);
        }

        public bool RemoveDoctorProfileImage()
        {
            bool status = CallAPI<bool, string>($"Profile/RemoveDoctorProfileImage", "POST", string.Empty).Result;
            if (status)
            {
                SessionManagement.DoctorImage = null;
            }
            return status;
        }
    }
}