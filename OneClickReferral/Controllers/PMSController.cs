﻿using OneClickReferral.App_Start;
using OneClickReferral.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace OneClickReferral.Controllers
{
    public class PMSController : BaseController
    {
        // GET: PMS
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login(string Token)
        {
            if (!string.IsNullOrWhiteSpace(Token))
            {

            }
            return View();
        }
        [HttpPost]
        public ActionResult ReferPatient(string Token, PMSPatientDetails patient, PMSProviderDetails provider)
        {
           
            if ((Token ?? string.Empty).Trim().Length > 0)
            {
                #region Bind model
                ReferPatientModel newModel = new ReferPatientModel();
                newModel.Token = Token;
                newModel.PMSPatientDetails = patient;
                newModel.PMSProviderDetails = provider;
                #endregion

                //Call API
                ResponseReferPatientModel obj = (CallAPI<ResponseReferPatientModel, ReferPatientModel>($"PMS/GetReferPatient", "POST", newModel).Result);

                return RedirectToAction("TempSendReferral", "OneClick", new { Token = obj.Token, UserId = obj.UserId, ColleagueId = obj.ToColleague, PatientId = obj.PatientId, IsIntegration = true, ActionType = "sendrefbybutton" });

            }
            else
            {
                return RedirectToAction("Index", "Login");
            }
        }

        public ActionResult DentrixButton()
        {
            return View();
        }
    }
}