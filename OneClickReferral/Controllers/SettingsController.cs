﻿using System;
using System.Collections.Generic;
using System.Web.Helpers;
using System.Web.Mvc;
using OneClickReferral.App_Start;
using OneClickReferral.Models;

namespace OneClickReferral.Controllers
{
    [CustomAuthorize]
    public class SettingsController : BaseController
    {
        /// <summary>
        /// Get Security Settings Details of Doctor
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View(CallAPI<AccountSettings,string>("Settings/Get", "GET",string.Empty).Result);
        }

        /// <summary>
        /// Save Security Settings Details
        /// </summary>
        /// <param name="accountsettings"></param>
        /// <returns></returns>
        public ActionResult Submit(SaveAccountSetting accountsettings)
        {
            //string Result = CallAPI<string, SaveAccountSetting>("Settings/Post", "POST", accountsettings).Result;
            //XQ1 - 481 change request by Travis for Remove Stay logged in, Location and Confrim password fields.
            //if(Result.Contains("Account settings updated successfully"))
            //{
            //    Session.Timeout = accountsettings.StayLogin;
            //}
            return Json(CallAPI<string, SaveAccountSetting>("Settings/Post", "POST", accountsettings).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Integration Settings main page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Integrations()
        {
            return View();
        }

        /// <summary>
        /// Lazy loading the Integration settings details.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetIntegration()
        {
            List<Integration> Lst = CallAPI<List<Integration>, string>("Settings/GetIntegration", "GET", string.Empty).Result;
            return PartialView("_PartialIntegrationDetails", Lst);
        }

        /// <summary>
        /// Add Integration Company accounts with Rendom Generated GUID key.
        /// </summary>
        /// <returns></returns>
        public ActionResult AddAccount()
        {
            string AccountKey = Crypto.SHA1(Convert.ToString(DateTime.Now.Ticks));
            return Json(AccountKey,JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Save Integration Settings details
        /// </summary>
        /// <param name="save"></param>
        /// <returns></returns>
        public ActionResult SaveIntegrationSetting(SaveIntegration save)
        {
            return Json(CallAPI<bool,SaveIntegration>("Settings/SaveIntegrations", "POST",save).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Un-Sync Locations basied on Integration Id
        /// </summary>
        /// <param name="IntegrationId"></param>
        /// <returns></returns>
        public ActionResult GetUnSyncLocation(int IntegrationId)
        {
            return Json(CallAPI<List<SelectListItem>, string>($"Settings/GetUnSyncLocations?id={IntegrationId}", "GET", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Team Member Main view with getting Dentist Location list.
        /// </summary>
        /// <returns></returns>
        public ActionResult TeamMember()
        {
            //return View(CallAPI<List<SelectListItem>, string>("Settings/GetDentistLocations", "GET",string.Empty).Result);
            // Jira XQ1-995 
            List<AddressDetails> objAddressDetails = CallAPI<List<AddressDetails>, string>("Patients/GetLocations", "GET", string.Empty).Result;
            return View("TeamMember", objAddressDetails);
        }

        /// <summary>
        /// Lazy loaing Team Member list
        /// </summary>
        /// <param name="_filter"></param>
        /// <returns></returns>
        public ActionResult GetTeamMember(PatientFilter _filter)
        {
            return PartialView("_PartialTeamMember", CallAPI<List<TeamMember>, PatientFilter>("Settings/GetTeamMember", "POST", _filter).Result);
        }

        /// <summary>
        /// Get Team Member Details basied on Team Member Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult GetTeamMemberDetails(int id)
        {
            return PartialView("_PartialAddTeamMember", CallAPI<TeamMember, string>($"Settings/GetTeamMemberDetails?id={id}", "GET", string.Empty).Result);
        }

        /// <summary>
        /// Add Team Member
        /// </summary>
        /// <param name="_team"></param>
        /// <returns></returns>
        public ActionResult AddEditTeamMember(TeamMember _team)
        {
            return Json(CallAPI<bool, TeamMember>("Settings/AddEditTeamMemer", "POST", _team).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Remove Team Member
        /// </summary>
        /// <param name="_remove"></param>
        /// <returns></returns>
        public ActionResult RemoveTeamMember(RemoveTeam _remove)
        {
            return Json(CallAPI<bool, RemoveTeam>($"Settings/RemoveTeamMember", "POST", _remove).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Check Email is Exists or not
        /// </summary>
        /// <param name="Email"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult CheckEmailExists(string Email,int id)
        {
            return Json(CallAPI<bool, string>($"Settings/CheckDentistEmailExists?Email={Email}&id={id}", "GET", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Dentist Referral Form Settings Main page.
        /// </summary>
        /// <returns></returns>
        public ActionResult ReferralForm()
        {
            return View();
        }

        /// <summary>
        /// Lazy load Referral Settings of Dentist
        /// </summary>
        /// <returns></returns>
        public ActionResult GetReferralData()
        {
            return PartialView("_PartialReferralFormSettings", CallAPI<DentistReferralForm, string>("Settings/GetReferralForm", "GET", string.Empty).Result);
        }

        /// <summary>
        /// Save Dentist Referral Form Settings
        /// </summary>
        /// <param name="_save"></param>
        /// <returns></returns>
        public ActionResult SaveReferralFormSettings(SaveReferralForm _save)
        {
            return Json(CallAPI<bool, SaveReferralForm>("Settings/SaveReferralForm", "POST", _save).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Patient Referral Form Settings
        /// </summary>
        /// <returns></returns>
        public ActionResult PatientReferralForm()
        {
            return View();
        }

        /// <summary>
        /// Lazy load Patient Referral form settings
        /// </summary>
        /// <returns></returns>
        public ActionResult PatientForm()
        {
            return PartialView("_PartialPatientReferralForm", CallAPI<List<PatientReferralForm>, string>("Settings/GetPatientReferralForm", "GET", string.Empty).Result);
        }

        /// <summary>
        /// Save Patient Referral Form Settings
        /// </summary>
        /// <param name="_save"></param>
        /// <returns></returns>
        public ActionResult SavePatientReferralForm(SavePatientReferralSettings _save)
        {
            return Json(CallAPI<bool, SavePatientReferralSettings>("Settings/SavePatientReferralSettings", "POST", _save).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Disable Integration settings
        /// </summary>
        /// <param name="DentrixConnectorIds"></param>
        /// <returns></returns>
        public ActionResult DisableIntegration(string DentrixConnectorIds)
        {
            return Json(CallAPI<bool, string>($"Settings/DisableIntegration?id={DentrixConnectorIds}", "POST", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Enable Integration Settings
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult EnableIntegration(int id)
        {
            return Json(CallAPI<bool, string>($"Settings/EnableIntegration?id={id}", "POST", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Integration Details By Integration Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult GetIntegrationDetails(int id)
        {
            return PartialView("_PartialIntegration", CallAPI<List<CompanyIntegrationDetail>, string>($"Settings/GetIntegrationDetails?id={id}", "POST", string.Empty).Result);
        }

        /// <summary>
        /// Enable Integration Connector
        /// </summary>
        /// <param name="id">Connector Id</param>
        /// <returns></returns>
        public ActionResult EnableConnector(int id)
        {
            return Json(CallAPI<bool, string>($"Settings/EnableConnector?id={id}", "GET", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        ///  Disable Integration Connector
        /// </summary>
        /// <param name="id">Connector Id</param>
        /// <returns></returns>
        public ActionResult DisableConnector(int id)
        {
            return Json(CallAPI<bool, string>($"Settings/DisableConnector?id={id}", "GET", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Save Manage Order by sequence
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult SaveManageSequence(FieldSequence data)
        {
            return Json(CallAPI<bool, FieldSequence>("Settings/SaveManageSequence", "POST", data).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Save Manage Order by sequence
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult SavePatientManageSequence(FieldSequence data)
        {
            return Json(CallAPI<bool, FieldSequence>("Settings/SavePatientManageSequence", "POST", data).Result, JsonRequestBehavior.AllowGet);
        }

       
    }
}