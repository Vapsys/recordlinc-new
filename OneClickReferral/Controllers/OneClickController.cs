using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Mvc;
using System.Configuration;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OneClickReferral.Models;
using System.Security;
using System.Web;
using System.Collections.Generic;
using BO.ViewModel;
using System.Data;
using OneClickReferral.App_Start;
using System.Net;

namespace OneClickReferral.Controllers
{

    public class OneClickController : BaseController
    {
        // GET: OneClickReferral
        Logger Log = new Logger();
        string apikey = ConfigurationManager.AppSettings["apikey"].ToString();
        //string recordlinc_fileupload = ConfigurationManager.AppSettings["recordlinc_fileupload"].ToString();F
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();

        /// <summary>
        /// This method call on first time choose WHO AM I
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()

        {
            string strAccess_Token = string.Empty;
            HttpCookie access_token = Request.Cookies["access_token"];
            HttpCookie EncryptedUserId = Request.Cookies["EncryptedUserId"];
            if (access_token != null)
            {
                strAccess_Token = access_token["access_token"];
            }         
            if (EncryptedUserId != null)
            {
                SessionManagement.EncryptedUserId = EncryptedUserId["EncryptedUserId"];
            }

            if (!string.IsNullOrWhiteSpace(SessionManagement.access_token) || !string.IsNullOrWhiteSpace(strAccess_Token))
            {
                if (string.IsNullOrWhiteSpace(SessionManagement.access_token))
                {
                    SessionManagement.access_token = strAccess_Token;
                }
                if (!string.IsNullOrWhiteSpace(SessionManagement.access_token))
                {
                    return RedirectToAction("Indexs", new { HRVCA = SessionManagement.access_token });
                }
            }
            return View();
        }

        /// <summary>
        /// This method call one second step when you choose which colleagues you want to sent a referral.
        /// </summary>
        /// <param name="ColleagueId"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Compose1Click> GetSerializedata(string ColleagueId)
        {
            using (var client = new HttpClient())
            {
                Compose1Click data = new Compose1Click();
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                //ColleagueId = HttpUtility.UrlEncode(ColleagueId);
                HttpResponseMessage Res = await client.GetAsync(apikey + "OneClickReferral/GetReferral?VCHDARP=" + HttpUtility.UrlEncode(ColleagueId));

                if (Res.IsSuccessStatusCode)
                {
                    var List = Res.Content.ReadAsStringAsync().Result;
                    data = JsonConvert.DeserializeObject<Compose1Click>(List);
                    TempData["TObj"] = data;
                }
                return data;
            }
        }

        /// <summary>
        /// This method post the Referral form which is fill-up by WHO AM I user. 
        /// </summary>
        /// <param name="CompData"></param>
        /// <returns></returns>
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> ReferralForm(Compose1Click CompData)
        {
            //if (ModelState.IsValid)
            Log.WriteLog("This Form is called now perform the Compose referral send", true);
            try
            {
                {
                    bool Result = false;
                    int res = 0;
                    using (var client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        Log.WriteLog("INSIDE THE TRY BLOCK", true);
                        string TextValue = string.Empty;
                        string XMLstring = "<NewDataSet>";
                        foreach (var item in Request.Form.Keys)
                        {
                            if (!string.IsNullOrEmpty(Convert.ToString(Request.Form[item.ToString()])) && Convert.ToString(Request.Form[item.ToString()]) != ",")
                            {
                                if (item.ToString().StartsWith("sub"))
                                {

                                    string[] SplitString; string CategoryID = string.Empty;
                                    string SubCatId = string.Empty;
                                    string SubSubCatId = string.Empty;
                                    SplitString = item.ToString().Split('_');
                                    Log.WriteLog("Item " + item.ToString(), true);
                                    CategoryID = SplitString[1];
                                    SubCatId = SplitString[2];
                                    if (SplitString[3] == "0")
                                    {
                                        SplitString[3] = null;
                                    }
                                    SubSubCatId = ((SplitString[3] != "" && SplitString[3] != null) ? SplitString[3] : null);
                                    TextValue = Convert.ToString(Request.Form[item.ToString()]);
                                    XMLstring += "<Table><catId>" + SecurityElement.Escape(CategoryID) + "</catId>";
                                    XMLstring += "<SubCatId>" + SecurityElement.Escape(SubCatId) + "</SubCatId>";
                                    XMLstring += "<SubSubCatId>" + SecurityElement.Escape(SubSubCatId) + "</SubSubCatId>";
                                    XMLstring += "<Value>" + SecurityElement.Escape(TextValue) + "</Value>";
                                    XMLstring += "<ReferrelCardId>" + null + "</ReferrelCardId></Table>";
                                }
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(Request.Form["LocationId"]))
                        {
                            Log.WriteLog("LocationId: " + Convert.ToString(Request.Form["LocationId"]), true);
                            CompData.LocationId = Convert.ToInt32(Request.Form["LocationId"]);
                        }
                        XMLstring += "</NewDataSet>";
                        CompData.XMLstring = XMLstring;
                        Log.WriteLog("Before call RLAPI", true);
                        HttpResponseMessage Res = await client.PostAsJsonAsync(apikey + "OneClickReferral/ComposeReferral", CompData);
                        Log.WriteLog("After calling RL API" + Res.Content.ReadAsStringAsync().Result.ToString(), true);
                        if (Res.IsSuccessStatusCode)
                        {
                            //Change for XQ1 - 1059
                            var result = Res.Content.ReadAsStringAsync().Result;
                            //Result = JsonConvert.DeserializeObject<bool>(result);
                            res = JsonConvert.DeserializeObject<int>(result);
                            if (res > 0)
                            {
                                Result = true;
                            }
                        }
                    }
                    TempData["success"] = Result;
                    TempData["messageId"] = res;
                    //return RedirectToAction("ReferralForm");
                }
            }
            catch (Exception ex)
            {
                Log.WriteLog("Exception occurred : " + ex.Message, true);
                throw;
            }
            return Redirect(Request.UrlReferrer.ToString());

        }

        public ActionResult GetLocationList(string ColleagueId)
        {
            //Ankit's changes 12-03-2018 OLD reference code.
            #region OLD CODE
            //List<BO.Models.LocationDetail> lst = new List<BO.Models.LocationDetail>();
            //lst = CallAPI<List<BO.Models.LocationDetail>, string>("OneClickReferral/LocationList?ColleagueId=" + HttpUtility.UrlEncode(ColleagueId), "GET",string.Empty).Result;
            //using (var client = new HttpClient())
            //{
            //    client.DefaultRequestHeaders.Clear();
            //    //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //    ColleagueId = HttpUtility.UrlEncode(ColleagueId);
            //    HttpResponseMessage Res = client.GetAsync(apikey + "OneClickReferral/LocationList?ColleagueId=" + ColleagueId).Result;
            //    if (Res.IsSuccessStatusCode)
            //    {
            //        var List = Res.Content.ReadAsStringAsync().Result;
            //        lst = JsonConvert.DeserializeObject<List<BO.Models.LocationDetail>>(List);
            //    }
            //}
            #endregion
            return Json(CallAPI<List<BO.Models.LocationDetail>, string>("OneClickReferral/LocationList?ColleagueId=" + HttpUtility.UrlEncode(ColleagueId), "GET", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTempFiles(string FileIds, bool IsCommunication = false)
        {
            //Ankit's changes 12-03-2018 OLD reference code.
            #region OLD CODE
            //List<TempFileAttachments> lst = new List<TempFileAttachments>();
            //lst = CallAPI<List<TempFileAttachments>, string>("OneClickReferral/Fetchfiles?FileIds=" + FileIds, "GET", string.Empty).Result;
            //using (var client = new HttpClient())
            //{
            //    client.DefaultRequestHeaders.Clear();
            //    //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));                
            //    HttpResponseMessage Res = client.GetAsync(apikey + "OneClickReferral/Fetchfiles?FileIds=" + FileIds + "&MessageId=0").Result;
            //    if (Res.IsSuccessStatusCode)
            //    {
            //        var List = Res.Content.ReadAsStringAsync().Result;
            //        lst = JsonConvert.DeserializeObject<List<TempFileAttachments>>(List);
            //    }
            //}
            #endregion
            ViewBag.IsCommunication = IsCommunication;
            return PartialView("GetFilesSection", CallAPI<List<TempFileAttachments>, string>("OneClickReferral/Fetchfiles?FileIds=" + HttpUtility.UrlEncode(FileIds), "GET", string.Empty).Result);
        }

        public ActionResult PatientList()
        {
            return View("_PartialPatientListing");
        }

        public ActionResult GetPatient(Appointment obj, string UserId)
        {
            //using (var client = new HttpClient())
            //{

                BO.Models.PatientFilter model = new BO.Models.PatientFilter();
                APIResponseBase<List<PatientDetialsForPatientreward>> response = new APIResponseBase<List<PatientDetialsForPatientreward>>();
                model.PageIndex = 1;
                model.PageSize = BO.Constatnt.Common.AppointmentMaxRecord;
                model.SortColumn = 1;
                model.SortDirection = 1;
                UserId = UserId.Replace(" ", "+");                
                model.UserId = Convert.ToInt32(ObjTripleDESCryptoHelper.decryptText(UserId));
                model.FirstName = obj.Search_FirstName;
                model.Lastname = obj.Search_LastName;
                model.Email = obj.Search_Email;
                model.Phone = obj.Search_Phone;
                model.DateOfBirth = obj.Search_DOB;
                model.Username = "RecordlincPatientReward";
                model.Password = "Recordlinc#1";
                //string F_date = DateTime.Today.ToString("MM/dd/yyyy") + " 00:00:00";
                //string T_date = DateTime.Today.ToString("MM/dd/yyyy") + " 23:59:59";
                //model.FromDate = Convert.ToDateTime(F_date);
                //model.ToDate = Convert.ToDateTime(T_date);
                response = CallAPI<APIResponseBase<List<PatientDetialsForPatientreward>>, BO.Models.PatientFilter>("OneClickReferral/ListOfPatient", "POST", model).Result;
            if (response !=null)
            {
                return PartialView("_PatientList", response.Result);
            }else
            {
                return PartialView("_PatientList", response);
            }
            //HttpResponseMessage Res = client.PostAsJsonAsync(apikey + "OneClickReferral/ListOfPatient", model).Result;
            //if (Res.IsSuccessStatusCode)
            //{
            //    var result = Res.Content.ReadAsStringAsync().Result;
            //    response = JsonConvert.DeserializeObject<APIResponseBase<List<PatientDetialsForPatientreward>>>(result);
            //}
           // return PartialView("_PatientList", response.Result);
           // }
        }

        public ActionResult GetAppointment(string UserId)
        {
            //using (var client = new HttpClient())
            //{
                GetScheduleDataModel model = new GetScheduleDataModel();
                 APIResponseBase<List<AppointmentReportModel>> response = new APIResponseBase<List<AppointmentReportModel>>();
                model.PageIndex = 1;
                model.PageSize = BO.Constatnt.Common.AppointmentMaxRecord;
                model.SortColumn = "1";
                model.SortDirection = "1";
                model.UserId = UserId;
                string F_date = DateTime.UtcNow.ToString("MM/dd/yyyy") + " 00:00:00";
                string T_date = DateTime.UtcNow.ToString("MM/dd/yyyy") + " 23:59:59";
                model.FromDate = Convert.ToDateTime(F_date);
                model.ToDate = Convert.ToDateTime(T_date);
                //List<AppointmentReportModel> objAppointmentReportModel = new List<AppointmentReportModel>();
                //objAppointmentReportModel =
                response = CallAPI< APIResponseBase<List<AppointmentReportModel>>, GetScheduleDataModel>("OneClickReferral/GetScheduleData", "POST", model).Result;

            //HttpResponseMessage Res = client.PostAsJsonAsync(apikey + "OneClickReferral/GetScheduleData", model).Result;
            //if (Res.IsSuccessStatusCode)
            //{
            //    var result = Res.Content.ReadAsStringAsync().Result;
            //    response = JsonConvert.DeserializeObject<APIResponseBase<List<AppointmentReportModel>>>(result);
            //}
                if (response !=null && response.Result != null && response.Result.Count > 0)
                {
                    return PartialView("_AppointmentList", response.Result);
                }         
                else
                {
                    PatientFilter models = new PatientFilter();
                    APIResponseBase<List<PatientDetialsForPatientreward>> responses = new APIResponseBase<List<PatientDetialsForPatientreward>>();
                    models.PageIndex = 1;
                    models.PageSize = 10;
                    models.SortColumn = 1;
                    models.SortDirection = 1;
                    UserId = UserId.Replace(" ", "+");
                    models.UserId = Convert.ToInt32(ObjTripleDESCryptoHelper.decryptText(UserId));
                    models.FirstName = null;
                    models.Lastname = null;
                    models.Email = null;
                    models.Phone = null;
                    models.DateOfBirth = null;
                    models.Username = "RecordlincPatientReward";
                    models.Password = "Recordlinc#1";
                    //List<PatientDetialsForPatientreward> objPatientDetialsForPatientreward = new List<PatientDetialsForPatientreward>();
                    responses = CallAPI<APIResponseBase<List<PatientDetialsForPatientreward>>, PatientFilter>("OneClickReferral/ListOfPatient", "POST", models).Result;
                if (responses != null)
                {
                    return PartialView("_PatientList", responses.Result);
                }else
                {
                    return PartialView("_PatientList", responses);
                }
                //HttpResponseMessage Resp = client.PostAsJsonAsync(apikey + "OneClickReferral/ListOfPatient", models).Result;
                //if (Resp.IsSuccessStatusCode)
                //{
                //    var result = Resp.Content.ReadAsStringAsync().Result;
                //    responses = JsonConvert.DeserializeObject<APIResponseBase<List<PatientDetialsForPatientreward>>>(result);
                //}
                //return PartialView("_PatientList", responses.Result);
            }
            
        }

        [HttpPost]
        public ActionResult IsDentistIntegrationPartner(string UserId)
        {
            try
            {
                bool Result = CheckIntegration(UserId);
                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public JsonResult TempUpload(string DoctorId)
        {
            try
            {
                FileUploadModel model = new FileUploadModel();
                HttpFileCollectionBase files = Request.Files;
                string str = model.TempUpload(ObjTripleDESCryptoHelper.decryptText(DoctorId), files);
                return Json(str, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }
        }


        [HttpPost]
        public ActionResult Login(UserCredentialsModel model)
        {
            try
            {
                if (Request["CheckBoxRemember"] != null)
                {
                    Response.Cookies["UserCredential"].Value = model.Username;
                    Response.Cookies["UserCredential"].Expires = DateTime.Now.AddMonths(1);
                }
                //string token = string.Empty;
                //using (var client = new HttpClient())
                //{
                //    HttpResponseMessage Res = client.PostAsJsonAsync(apikey + "PMS/Login", model).Result;
                //    if (Res.IsSuccessStatusCode)
                //    {
                //        var result = Res.Content.ReadAsStringAsync().Result;
                //        token = JsonConvert.DeserializeObject<APIResponseBase<PMSLoginModel>>(result).Result.Token;
                //    }
                //    SessionManagement.access_token = token;
                //}
                var response = CallAPI<APIResponseBase<PMSLoginModel>, UserCredentialsModel>("PMS/Login", "POST", model);
                string token = response.Result != null ? response.Result.Result.Token : string.Empty;
                if (!string.IsNullOrEmpty(token))
                {
                    SessionManagement.access_token = token;
                    if (token != null)
                    {
                        HttpCookie access_token = new HttpCookie("access_token");
                        access_token["access_token"] = token;                        
                        access_token.Expires = DateTime.Now.AddMonths(1);
                        access_token.Secure = true;
                        Response.Cookies.Add(access_token);
                    }
                    return Json(new APIResponseBase<string>()
                    {
                        IsSuccess = true,
                        StatusCode = (int)HttpStatusCode.OK,
                        Message = "",
                        Result = token
                    }
                    , JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new APIResponseBase<string>()
                    {
                        IsSuccess = false,
                        StatusCode = (int)HttpStatusCode.BadRequest,
                        Message = " Invalid username or password",
                        Result = null
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                throw;
            }
        }
        [HttpGet]
        public ActionResult V2(string UserId)

        {
            TempData["UserId"] = UserId;
            return RedirectToAction("ReferralForm");
        }

        public ActionResult ReferralForm()
        {
            string UserId = Convert.ToString(TempData["UserId"]);
            TempData.Keep();
            bool result = CheckIntegration(UserId);
            ViewBag.IsIntigration = result;
            ViewBag.alert = TempData["success"];
            Colleaguelist colleaguelist = CallAPI<Colleaguelist, string>($"Common/GetColleagueList?SearchText=&ProviderType=0", "GET", string.Empty).Result;
            if (colleaguelist !=null  && colleaguelist.colleagues !=null && colleaguelist.colleagues.Count > 0 && colleaguelist.RecordCount >0)
            {
                ViewBag.Colleaguelist = colleaguelist.colleagues;
                ViewBag.ColleagueCount = colleaguelist.RecordCount;
            }                      

            //Change for XQ1 - 1059 
            ViewBag.messageId = TempData["messageId"];
            TempData["messageId"] = null;
            TempData["success"] = null;
            return View("V2");
        }
        public ActionResult GetReferralFormData(string ColleagueId)
        {
            //Ankit's changes 12-03-2018 OLD reference code.
            #region OLD CODE
            //Compose1Click data = new Compose1Click();
            //data = CallAPI<Compose1Click, string>("OneClickReferral/GetReferral?VCHDARP=" + ColleagueId, "GET", string.Empty).Result;
            //HttpClient client = new HttpClient();
            //client.DefaultRequestHeaders.Clear();
            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //HttpResponseMessage Message = client.GetAsync(apikey + "OneClickReferral/GetReferral?VCHDARP=" + ColleagueId).Result;
            //if (Message.IsSuccessStatusCode)
            //{
            //    var Data = Message.Content.ReadAsStringAsync().Result;
            //    data = JsonConvert.DeserializeObject<Compose1Click>(Data);
            //    TempData["TObj"] = data;
            //}
            #endregion

            return View("_partialMainPage", CallAPI<Compose1Click, string>("OneClickReferral/GetReferral?VCHDARP=" + HttpUtility.UrlEncode(ColleagueId), "GET", string.Empty).Result);
        }

        public ActionResult GetTeamMemberList(ColleagueList objColleagueList)
        {
            //Ankit's changes 12-03-2018 OLD reference code.
            #region OLD CODE
            //Changes for Encryped User Id for tema member list.
            //List<Models.TeamMembers> Lst = new List<Models.TeamMembers>();
            //List<Models.TeamMembersOCR> Lst = new List<Models.TeamMembersOCR>();
            //Lst = CallAPI<List<Models.TeamMembersOCR>, string>("OneClickReferral/GetTeamMember?UserId=" + UserId, "GET", string.Empty).Result;
            //HttpClient client = new HttpClient();
            //client.DefaultRequestHeaders.Clear();
            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //HttpResponseMessage Message = client.GetAsync(apikey + "OneClickReferral/GetTeamMember?UserId=" + UserId).Result;
            //if (Message.IsSuccessStatusCode)
            //{
            //    var Data = Message.Content.ReadAsStringAsync().Result;
            //    Lst = JsonConvert.DeserializeObject<List<Models.TeamMembersOCR>>(Data);
            //}
            #endregion
            // return Json(CallAPI<List<Models.TeamMembersOCR>, string>("OneClickReferral/GetTeamMember?UserId=" + HttpUtility.UrlEncode(objColleagueList.userId) , "GET", string.Empty).Result, JsonRequestBehavior.AllowGet);
            return Json(CallAPI<List<Models.TeamMembersOCR>, ColleagueList>("OneClickReferral/GetTeamMember", "POST", objColleagueList).Result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPatientDetailById(string PatientId, string DoctorId)
        {
            //Ankit's changes 12-03-2018 OLD reference code.
            #region OLD CODE
            //BO.Models.Patients obj = new BO.Models.Patients();
            //obj = CallAPI<BO.Models.Patients, string>("OneClickReferral/GetPatientDetail?PatientId=" + ObjTripleDESCryptoHelper.encryptText(PatientId) + "&DoctorId=" + DoctorId, "GET", string.Empty).Result;
            //HttpClient client = new HttpClient();
            //client.DefaultRequestHeaders.Clear();
            //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            //HttpResponseMessage Message = client.GetAsync(apikey + "OneClickReferral/GetPatientDetail?PatientId=" + ObjTripleDESCryptoHelper.encryptText(PatientId) + "&DoctorId=" + DoctorId).Result;
            //if (Message.IsSuccessStatusCode)
            //{
            //    var Data = Message.Content.ReadAsStringAsync().Result;
            //    obj = JsonConvert.DeserializeObject<BO.Models.Patients>(Data);
            //}
            #endregion
            return Json(CallAPI<BO.Models.Patients, string>("OneClickReferral/GetPatientDetail?PatientId=" + HttpUtility.UrlEncode(ObjTripleDESCryptoHelper.encryptText(PatientId)) + "&DoctorId=" + HttpUtility.UrlEncode(DoctorId), "GET", string.Empty).Result, JsonRequestBehavior.AllowGet);
        }

        public bool CheckIntegration(string UserId)
        {
            TempData["UserId"] = UserId;
            //Ankit's changes 12-03-2018 OLD reference code.
            #region OLD CODE
            //bool Result = false;
            //using (var client = new HttpClient())
            //{
            //    Compose1Click data = new Compose1Click();
            //    client.DefaultRequestHeaders.Clear();
            //    HttpResponseMessage Res = client.GetAsync(apikey + "OneClickReferral/IsDentistIntegrationPartner?UserId=" + UserId).Result;

            //    if (Res.IsSuccessStatusCode)
            //    {
            //        var result = Res.Content.ReadAsStringAsync().Result;
            //        Result = JsonConvert.DeserializeObject<bool>(result);
            //    }
            //}
            #endregion
            return CallAPI<bool, string>("OneClickReferral/IsDentistIntegrationPartner?UserId=" + HttpUtility.UrlEncode(UserId), "GET", string.Empty).Result;
        }

        //public ActionResult DashboardLogin()
        //{
        //    if (!string.IsNullOrWhiteSpace(SessionManagement.access_token))
        //    {
        //        return RedirectToAction("Indexs", new { HRVCA = SessionManagement.access_token });
        //    }
        //    return View();
        //}
        public ActionResult Logout(string fromPage)
        {
            Session.Abandon();
            Session.Clear();
            if (Request.Cookies["access_token"] != null)
            {
                Response.Cookies["access_token"].Expires = DateTime.Now.AddDays(-1);
            }
            if (Request.Cookies["EncryptedUserId"] != null)
            {
                Response.Cookies["EncryptedUserId"].Expires = DateTime.Now.AddDays(-1);
            }
            if (!string.IsNullOrWhiteSpace(fromPage))
                return RedirectToAction("Index");
            else
                return RedirectToAction("Index", "Login");

        }

        public ActionResult Indexs(string HRVCA = null)

        {
            if (string.IsNullOrWhiteSpace(HRVCA))
            {
                return RedirectToAction("Index", "Login");
            }
            else
            {
                SessionManagement.access_token = HRVCA;
                if (!string.IsNullOrWhiteSpace(SessionManagement.EncryptedUserId))
                {
                    return RedirectToAction("V2", new { UserId = SessionManagement.EncryptedUserId });
                }
                else
                {
                    return RedirectToAction("Index", "Dashboard");
                }

            }
        }

        public ActionResult TempSendReferral(string Token, int? UserId, int? ColleagueId, int? PatientId, bool? IsIntegration, string ActionType)
        {
            if (ActionType == "sendrefbybutton") // XQ1-900- Send referral using dentrix button functionality
            {
                SessionManagement.access_token = Token;
                ViewBag.UserId = CallAPI<string, string>("PMS/GetEncryptedUserId", "GET").Result;
                ViewBag.IsIntegration = true;
                ViewBag.Token = Token;
                ViewBag.ToColleague = ColleagueId;
                ViewBag.PatientId = PatientId;

            }
            else
            {
                ViewBag.UserId = CallAPI<string, string>("PMS/GetEncryptedUserId", "GET").Result;
                ViewBag.IsIntegration = CheckIntegration(ViewBag.UserId);
                ViewBag.Token = SessionManagement.access_token;
                ViewBag.ToColleague = ColleagueId;
                ViewBag.PatientId = "";
            }

            return View();
        }

        [HttpGet]
        public ActionResult LargeFileUploadWidget(int? recordId, int? patientId)
        {
            var model = new BO.Models.FileModel();
            model.RecordId = (recordId == null) ? 0 : (int)recordId;
            model.PatientId = (patientId == null) ? 0 : (int)patientId;
            return View(model);
        }

        [HttpGet]
        public ActionResult LargeFileUploadWidgetReferral(int? recordId, int? patientId)
        {
            var model = new BO.Models.FileModel();
            model.RecordId = (recordId == null) ? 0 : (int)recordId;
            model.PatientId = (patientId == null) ? 0 : (int)patientId;
            return View(model);
        }

        [HttpPost]
        public ActionResult DentistList(BO.Models.Searchlatitudelongitude model)
        {
            model.ProviderfilterType = BO.Enums.Common.PMSProviderFilterType.Provider;
            return Json(CallAPI<List<DentistDetailsForPatientReward>, BO.Models.Searchlatitudelongitude>("OneClickReferral/DentistList", "POST", model));
        }

        [HttpPost]
        public ActionResult GetSpecificSearch(string Keywords)
        {
            GetSpecificSearchModel model = new GetSpecificSearchModel()
            {
                Keywords = Keywords,
                ProviderfilterType = BO.Enums.Common.PMSProviderFilterType.Provider
            };
            return Json(CallAPI<List<DentistDetailsForPatientReward>, GetSpecificSearchModel>("OneClickReferral/GetSpecificSearch", "POST", model));

        }

        public JsonResult GetColleagueList()
        {
            string SearchText = string.Empty;
            if (Convert.ToString(Request["q"]) != null)
            {
                SearchText = Convert.ToString(Request["q"]);
            }
            Colleaguelist colleaguelist = CallAPI<Colleaguelist, string>($"Common/GetColleagueList?SearchText={SearchText}&ProviderType=0", "GET", string.Empty).Result;
            return Json(colleaguelist.colleagues, JsonRequestBehavior.AllowGet);
        }      
    }
}

