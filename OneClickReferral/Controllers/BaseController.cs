using BO.Models;
using BO.ViewModel;
using OneClickReferral.App_Start;
using OneClickReferral.Models;
using System.Configuration;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace OneClickReferral.Controllers
{
    public class BaseController : Controller
    {
        // GET: Base
        private RepositoryBLL repository = new RepositoryBLL(SessionManagement.access_token, ConfigurationManager.AppSettings["apikey"].ToString());
        
    public APIResponseBase<T> CallAPI<T, I>(string url, string Method = "POST", I param = default(I))
        {

            if (string.IsNullOrWhiteSpace(repository.Token) && !string.IsNullOrWhiteSpace(SessionManagement.access_token))
            {
                repository.Token = SessionManagement.access_token;
            }                       
            APIResponseBase<T> response = new APIResponseBase<T>();
            response = repository.CallAPI<T, I>(url, Method, param);
            if(response.StatusCode !=(int) HttpStatusCode.OK)
            {
                ExceptionRequestModel exceptionRequestModel = new ExceptionRequestModel();
                exceptionRequestModel.Message =$"OneclickReferral: - {response.Message}";
                exceptionRequestModel.URL = url;
                repository.CallAPI<bool, ExceptionRequestModel>("Exception/POST", "POST", exceptionRequestModel);
                if (!response.IsSuccess && (response.Message.Contains("Invalid") || response.StatusCode == (int)HttpStatusCode.BadRequest))
                {
                   
                        throw new System.Exception("session timeout");
                        //Response.Redirect("Onclick/Logout", true);
                        //Response.End();
                        //Response.Flush();
                }
            }
            //else if(!response.IsSuccess && response.Message.Contains("Invalid"))
            //{
            //    if(!Request.IsAjaxRequest())
            //    {
            //        Response.Redirect("Onclick/Logout", true);
            //    }
            //}
            
            return response;
        }
        public bool CallAPIWithFile<T,I>(string URL,string Method = "POST",I param = default(I))
        {
            var files = Request.Files[0];
            using (HttpClient client = new HttpClient())
            {
                using (var content = new MultipartFormDataContent())
                {
                    byte[] fileBytes = new byte[files.InputStream.Length + 1];
                    files.InputStream.Read(fileBytes, 0, fileBytes.Length);
                    var fileContent = new ByteArrayContent(fileBytes);
                    fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment") { FileName = files.FileName };
                    content.Add(fileContent);
                    if(param != null)
                    {

                    }
                    var result = client.PostAsync(URL, content).Result;
                    if (result.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
    }
}
