using System.Collections.Generic;
using System.Web.Mvc;
using OneClickReferral.Models;
using System;
using OneClickReferral.App_Start;
using BO.ViewModel;
using System.Linq;
using System.Web;

namespace OneClickReferral.Controllers
{
    [CustomAuthorize]
    public class DashboardController : BaseController
    {
        /// <summary>
        /// Dashboard Main page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (string.IsNullOrWhiteSpace(SessionManagement.access_token))
            {
                return Redirect("/Login/Index");
            }
            //Get Dentist Details
            Models.DentistDetail Obj = GetDentistDetails();
            //Get Unread message count
            Models.UnreadMessageCount unreadMessageCount = GetUnreadMessageCount();

            if (string.IsNullOrWhiteSpace(Obj.FirstName) && string.IsNullOrWhiteSpace(Obj.ImageName))
            {
                return RedirectToAction("Logout", "OneClick");
            }
            //Fill the Session based on Get Dentist detail
            FillSession(Obj, unreadMessageCount.Count);

            ViewBag.UserName = Obj.FirstName;
            ViewBag.Image = Obj.ImageWithPath;
            ViewBag.Receiver = Obj.IsReceiveReferral;
            ViewBag.Sent = Obj.IsSendReferral;
            ViewBag.UnreadCount = unreadMessageCount.Count;
            return View();
        }

        /// <summary>
        /// Get Dispostion list for Filtering Dashboard Messages View.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetDispostionList()
        {
            return Json(CallAPI<APIResponseBase<List<DispositionStatus>>, string>("OneClickReferral/GetDispositionListForOCR", "GET", string.Empty).Result.Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Dashboard Referral Message List
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public ActionResult GetReceivedReferral(ReferralFilter Obj)
        {
            return PartialView("_ReceivedReferral", CallAPI<APIResponseBase<List<OneClickReferralDetails>>, ReferralFilter>("OneClickReferral/OneClickReferralHistory", "POST", Obj).Result.Result);
        }

        /// <summary>
        /// Get Referral History Based on Conversation view.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public ActionResult GetReferralHistory(FilterMessageConversation Obj)
        {
            if (Obj.FromDate != null)
            {
                var dtFromDate = Convert.ToDateTime(Obj.FromDate);
                Obj.FromDate = new DateTime(dtFromDate.Year, dtFromDate.Month, dtFromDate.Day, 0, 0, 0);
            }
            if (Obj.Todate != null)
            {
                var dtToDate = Convert.ToDateTime(Obj.Todate);
                Obj.Todate = new DateTime(dtToDate.Year, dtToDate.Month, dtToDate.Day, 23, 59, 59);
            }
            FilterTeamMember objFilterTeamMember = new FilterTeamMember();
            objFilterTeamMember.PageIndex = 1;
            objFilterTeamMember.PageSize = 50;
            objFilterTeamMember.ProviderTypefilter = 1;
            objFilterTeamMember.IncludeUser = true;
            ViewBag.TeamMember = CallAPI<List<TeamMember>, FilterTeamMember>("Settings/GetTeamMember", "POST", objFilterTeamMember).Result;
            return PartialView("_ReceivedReferral", CallAPI<APIResponseBase<List<OneClickReferralDetail>>, FilterMessageConversation>("OneClickReferral/OneClickReferralHistory", "POST", Obj).Result.Result);
        }

        /// <summary>
        /// Converstion view 
        /// </summary>
        /// <returns></returns>
        public ActionResult NewConversationview()
        {
            FilterMessageConversation Obj = new FilterMessageConversation();
            int MessageId = Convert.ToInt32(TempData["MessageId"]);
            bool IsFromDashBoard = Convert.ToBoolean(TempData["IsFromDashboard"]);
            if (MessageId > 0)
            {
                Obj = BindModel(Convert.ToInt32(TempData["MessageId"]), SessionManagement.access_token);
                TempData.Keep();
            }
            APIResponseBase<List<OneClickReferralDetail>> response = new APIResponseBase<List<OneClickReferralDetail>>();
            // To get encrypted userid..
            ViewBag.EncryptedUserId = CallAPI<string, string>("PMS/GetEncryptedUserId", "GET").Result;
            //Get List related Messages for Conversation view.
            response = CallAPI<APIResponseBase<List<OneClickReferralDetail>>, FilterMessageConversation>("OneClickReferral/OneClickReferralHistory", "POST", Obj).Result;
            //Setted Current Message form list of messages.
            ViewBag.CurrentMessage = (response.Result.Any(x => x.MessageId == MessageId)) ? response.Result.Where(x => x.MessageId == MessageId).First() : null;
            return View("ConversationView", response.Result);
        }

        /// <summary>
        /// For new conversationview
        /// </summary>
        /// <returns></returns>
        public ActionResult ConversationView()
        {
            FilterMessageConversation Obj = new FilterMessageConversation();
            int MessageId = Convert.ToInt32(TempData["MessageId"]);
            bool IsFromDashBoard = Convert.ToBoolean(TempData["IsFromDashboard"]);
            if (MessageId > 0)
            {
                Obj = BindModel(Convert.ToInt32(TempData["MessageId"]), SessionManagement.access_token);
                TempData.Keep();
            }
            APIResponseBase<List<OneClickReferralDetail>> response = new APIResponseBase<List<OneClickReferralDetail>>();
            // To get encrypted userid..
            ViewBag.EncryptedUserId = CallAPI<string, string>("PMS/GetEncryptedUserId", "GET").Result;
            //Get List related Messages for Conversation view.
            response = CallAPI<APIResponseBase<List<OneClickReferralDetail>>, FilterMessageConversation>("OneClickReferral/OneClickReferralHistory", "POST", Obj).Result;
            ConversationView conversationView = new ConversationView();
            conversationView.oneClickReferralDetails = response.Result;
            ReferralMessageFilter filter = new ReferralMessageFilter();
            filter.MessageId = Convert.ToInt32(Obj.MessageId);
            filter.MessageType = response.Result.FirstOrDefault(x => x.MessageTypeId == "2").MessageFrom;
            filter.MessageTypeId = 2;
            filter.access_token = SessionManagement.access_token;
            conversationView.referralMessage = CallAPI<APIResponseBase<Models.ReferralMessage>, ReferralMessageFilter>("OneClickReferral/ReferralMessageDetails", "POST", filter).Result.Result;
            var referralMessage = conversationView.referralMessage.ReferralMessageList.FirstOrDefault();
            //int userid = referralMessage.IsSent == true ? referralMessage.ReceiverID : referralMessage.SenderId;
            int patientid = referralMessage.PatientId;
            conversationView.smsHistory = CallAPI<APIResponseBase<List<BO.Models.SMSDetails>>, string>($"Zipwhip/ListOfSMSHistory?UserId={patientid}", "GET", string.Empty).Result.Result;

            #region Account Activated or Not
            var obj = CallAPI<BO.ViewModel.APIResponseBase<List<BO.Models.ZipwhipAccount>>, string>($"Zipwhip/CheckZipwhipAccountActivated?UserId={SessionManagement.UserId}&LocationId=0", "GET", string.Empty).Result;
            bool IsAvailable = false;
            bool IsActive = false;
            if (obj != null && obj.Result != null && obj.Result.Count()>0)
            {
                IsAvailable = true;
                var chk = obj.Result.Where(m => m.IsActive == true).FirstOrDefault();
                if (chk != null)
                {
                    IsActive = true;
                }
            }
            ViewBag.IsAvailable = IsAvailable;
            ViewBag.IsAccountActive = IsActive;
            #endregion

            return View("_ConversationView", conversationView);
        }

        /// <summary>
        /// For Get Communication Data in New Conversation View.
        /// </summary>
        /// <returns></returns>
        public ActionResult GetCommunicationList(int MessageId)
        {
            FilterMessageConversation Obj = new FilterMessageConversation();
            if (MessageId > 0)
            {
                Obj = BindModel(Convert.ToInt32(MessageId), SessionManagement.access_token);
                TempData.Keep();
            }

            APIResponseBase<List<OneClickReferralDetail>> response = new APIResponseBase<List<OneClickReferralDetail>>();
            // To get encrypted userid..
            ViewBag.EncryptedUserId = CallAPI<string, string>("PMS/GetEncryptedUserId", "GET").Result;
            response = CallAPI<APIResponseBase<List<OneClickReferralDetail>>, FilterMessageConversation>("OneClickReferral/GetCommunicationList", "POST", Obj).Result;
            ConversationView conversationView = new ConversationView();
            conversationView.oneClickReferralDetails = response.Result;
            return PartialView("_OfficeCommunication", conversationView);
        }

        /// <summary>
        /// Temp Redirect Action method for not show Message id on URL.
        /// </summary>
        /// <param name="MessageId"></param>
        /// <returns></returns>
        public ActionResult GetConversationView(int MessageId,bool IsFromDashboard = false)
        {
           
            TempData["MessageId"] = MessageId;
            TempData["IsFromDashboard"] = IsFromDashboard;
            return RedirectToAction("ConversationView");
        }

        public ActionResult GetConversationViewNew(int MessageId, bool IsFromDashboard = false)
        {
            TempData["MessageId"] = MessageId;
            TempData["IsFromDashboard"] = IsFromDashboard;
            return RedirectToAction("NewConversationview");
        }

        /// <summary>
        /// Bind Message conversation Model based on Message Id and Access token
        /// </summary>
        /// <param name="MessageId"></param>
        /// <param name="access_token"></param>
        /// <returns></returns>
        public FilterMessageConversation BindModel(int MessageId, string access_token)
        {
            return new FilterMessageConversation()
            {
                MessageId = MessageId,
                access_token = access_token
            };
        }

        /// <summary>
        /// Get Patient name for Search on Dashboard page
        /// </summary>
        /// <param name="PatientName"></param>
        /// <returns></returns>
        public string GetPatientName(string PatientName)
        {
            return CallAPI<string, string>("OneClickReferral/GetPatietName?PatientName=" + PatientName, "POST", string.Empty).Result;
        }

        /// <summary>
        /// Change Dispostion Status for Received referral.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public ActionResult ChangeDisposionStatus(UpdateDisposition Obj)
        {
            Obj.LoginURL = Request.UrlReferrer.AbsoluteUri.ToLower().Substring(0, Request.UrlReferrer.AbsoluteUri.ToLower().LastIndexOf('/')) + "/Login/Index";
            return Json(CallAPI<bool, UpdateDisposition>("OneClickReferral/ChangeDispositionStatus", "POST", Obj).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Delete Referral
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public ActionResult DeleteReferral(DeleteMessage Obj)
        {
            return Json(CallAPI<string, DeleteMessage>("OneClickReferral/DeleteMessage", "POST", Obj).Result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Referral History
        /// </summary>
        /// <returns></returns>
        public ActionResult ReferralHistory()
        {
            return View();
        }

        /// <summary>
        /// Get Message/Referral Details
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ActionResult GetReferralMessageDetails(ReferralMessageFilter filter)
        {
            //GET Normal Message Details
            if (filter.MessageTypeId == 1)
            {
                return PartialView("_GetMessageDetails", CallAPI<APIResponseBase<NormalMessageDetails>, ReferralMessageFilter>("OneClickReferral/NormalMessageDetails", "POST", filter).Result.Result);
            }
            //GET Referral Message Details
            else
            {
                return PartialView("GetReferralMessageDetails", CallAPI<APIResponseBase<Models.ReferralMessage>, ReferralMessageFilter>("OneClickReferral/ReferralMessageDetails", "POST", filter).Result.Result);
            }
        }

        /// <summary>
        /// Get Dentist Details baised on access_token
        /// </summary>
        /// <returns></returns>
        public Models.DentistDetail GetDentistDetails()
        {
            return CallAPI<Models.DentistDetail, string>("OneClickReferral/DentistDetail?access_token=" + SessionManagement.access_token, "GET", string.Empty).Result;
        }

        /// <summary>
        /// Get UnRead Message Count of User
        /// </summary>
        /// <returns></returns>
        public Models.UnreadMessageCount GetUnreadMessageCount()
        {
            return CallAPI<Models.UnreadMessageCount, string>("OneClickReferral/InboxUnreadCount?access_token=" + SessionManagement.access_token, "GET", string.Empty).Result;
        }

        /// <summary>
        /// Fill the Session Based on Get Dentist Details.
        /// </summary>
        /// <param name="Obj"></param>
        /// <param name="UnreadMessageCount"></param>
        public void FillSession(Models.DentistDetail Obj, int UnreadMessageCount)
        {
            SessionManagement.FirstName = Obj.FirstName;
            SessionManagement.UserId = Obj.UserId;
            SessionManagement.DoctorImage = Obj.ImageWithPath;
            SessionManagement.TimeZoneSystemName = Obj.TimeZoneSystemName;
            SessionManagement.MessageCount = UnreadMessageCount;
            SessionManagement.EncryptedUserId = new TripleDESCryptoHelper().encryptText(Convert.ToString(Obj.UserId));
            SessionManagement.GetMemberPlanDetails = Obj.MembershipDetails;

            HttpCookie EncryptedUserId = new HttpCookie("EncryptedUserId");
            EncryptedUserId["EncryptedUserId"] = SessionManagement.EncryptedUserId;
            EncryptedUserId.Expires = DateTime.Now.AddMonths(1);
            Response.Cookies.Add(EncryptedUserId);
        }

        public ActionResult ConversationViewToPdf(int MessageId, string MessageType)
        {
            string FullName = "ConversationView";
            ReferralMessageFilter referralMessage = new ReferralMessageFilter();
            referralMessage.MessageId = MessageId;
            referralMessage.MessageType = MessageType;
            referralMessage.MessageTypeId = 2;
            referralMessage.access_token = SessionManagement.access_token;
            referralMessage.IsFromPDF = true;
            Models.ReferralMessage referral = CallAPI<APIResponseBase<Models.ReferralMessage>, ReferralMessageFilter>("OneClickReferral/ReferralMessageDetails", "POST", referralMessage).Result.Result;
            if (!string.IsNullOrWhiteSpace(referral.ReferralMessageList.FirstOrDefault().FullName))
            {
                FullName = referral.ReferralMessageList.FirstOrDefault().FullName;
            }
            //return View("_ReferralFullDetails", CallAPI<APIResponseBase<Models.ReferralMessage>, ReferralMessageFilter>("OneClickReferral/ReferralMessageDetails", "POST", referralMessage).Result.Result);
            return new Rotativa.ViewAsPdf("_ReferralFullDetails", referral)
            {
                FileName = FullName + ".pdf",
                PageSize = Rotativa.Options.Size.A4,
                PageOrientation = Rotativa.Options.Orientation.Portrait,
                PageMargins = new Rotativa.Options.Margins(10, 5, 10, 5)
            };
        }

        public ActionResult checkPdf()
        {
            return new Rotativa.ViewAsPdf("Download");
        }
        public ActionResult checkPdfview()
        {
            return View("Download");
        }

        //public ActionResult ConversationViewToPdf123(int MessageId, string MessageType)
        //{
        //    string FullName = "ConversationView";
        //    ReferralMessageFilter referralMessage = new ReferralMessageFilter();
        //    referralMessage.MessageId = MessageId;
        //    referralMessage.MessageType = MessageType;
        //    referralMessage.MessageTypeId = 2;
        //    referralMessage.access_token = SessionManagement.access_token;
        //    Models.ReferralMessage referral = CallAPI<APIResponseBase<Models.ReferralMessage>, ReferralMessageFilter>("OneClickReferral/ReferralMessageDetails", "POST", referralMessage).Result.Result;
        //    if (!string.IsNullOrWhiteSpace(referral.ReferralMessageList.FirstOrDefault().FullName))
        //    {
        //        FullName = referral.ReferralMessageList.FirstOrDefault().FullName;
        //    }
        //    //return View("_ReferralFullDetails", CallAPI<APIResponseBase<Models.ReferralMessage>, ReferralMessageFilter>("OneClickReferral/ReferralMessageDetails", "POST", referralMessage).Result.Result);
        //     return View("_ReferralFullDetails", referral);
        //    //return new Rotativa.ViewAsPdf("_ReferralFullDetails12", referral)
        //    //{
        //    //    FileName = FullName + ".pdf",
        //    //    PageSize = Rotativa.Options.Size.A4,
        //    //    PageOrientation = Rotativa.Options.Orientation.Portrait,
        //    //    PageMargins = new Rotativa.Options.Margins(10, 5, 10, 5)
        //    //};
        //}


        public ActionResult GetAttachments(string FolderId)
        {
            var model = new BO.Models.FileModel();
            model.FolderId = FolderId;
            return View("_PartialGetAttachments", model);
        }
        public ActionResult GetReferralCategory(int ReferralCardId)
        {
            return View("_PartialReferralCategoryConversation", CallAPI<List<ReferralCategory>, string>($"Patients/GetReferralCategory?id={ReferralCardId}", "GET", string.Empty).Result);
        }

        public bool ChangeCoordinator(Coordinator objCoordinator)
        {
            return (CallAPI<bool, Coordinator>("OneClickReferral/ChangeCoordinator", "POST", objCoordinator).Result);
        }
        public ActionResult GetBoxAPIFiles(int RecordId, string folderId, int? patientId)
        {
            folderId = folderId.Contains("undefined") ? string.Empty : folderId;
            return View("ViewBoxAPIFiles", CallAPI<BO.Models.FileListModel, string>($"Common/BoxAPIFiles?recordId={RecordId}&folderId={folderId}&patientId={patientId}", "GET", string.Empty).Result);
        }
    }
}
