﻿using OneClickReferral.Models;
using System;
using System.Collections.Generic;
using System.Web;

namespace OneClickReferral.App_Start
{
    public class SessionManagement
    {
        public static int UserId
        {
            get { return Convert.ToInt32(HttpContext.Current.Session["UserId"]); }
            set { HttpContext.Current.Session["UserId"] = value; }
        }
        public static string DoctorImage
        {
            get { return Convert.ToString(HttpContext.Current.Session["DoctorImage"]); }
            set { HttpContext.Current.Session["DoctorImage"] = value; }
        }
        public static string FirstName
        {
            get { return Convert.ToString(HttpContext.Current.Session["FirstName"]); }
            set { HttpContext.Current.Session["FirstName"] = value; }
        }
        public static string access_token
        {
            get { return Convert.ToString(HttpContext.Current.Session["access_token"]); }
            set { HttpContext.Current.Session["access_token"] = value; }
        }
        public static int MessageCount
        {
            get { return Convert.ToInt32(HttpContext.Current.Session["UnreadMessageCount"]); }
            set { HttpContext.Current.Session["UnreadMessageCount"] = value; }
        }
        public static string TimeZoneSystemName
        {
            get { return Convert.ToString(HttpContext.Current.Session["TimeZoneSystemName"]); }
            set { HttpContext.Current.Session["TimeZoneSystemName"] = value; }
        }
        public static string EncryptedUserId
        {
            get { return Convert.ToString(HttpContext.Current.Session["EncryptedUserId"]); }
            set { HttpContext.Current.Session["EncryptedUserId"] = value; }
        }
        public static List<MemberPlanDetail> GetMemberPlanDetails
        {
            get
            {
                if (HttpContext.Current.Session["GetMemberPlanDetails"] == null)
                {
                    HttpContext.Current.Session["GetMemberPlanDetails"] = new List<MemberPlanDetail>();
                }
                return HttpContext.Current.Session["GetMemberPlanDetails"] as List<MemberPlanDetail>;
            }
            set { HttpContext.Current.Session["GetMemberPlanDetails"] = value; }
        }
    }
}