﻿using System.Web;
using System.Web.Optimization;

namespace OneClickReferral
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/OneClickReferral/css").Include(
                "~/Content/OneClickReferral/css/bootstrap.css",
                "~/Content/OneClickReferral/css/bootstrap.min.css",
                "~/Content/OneClickReferral/css/main.css",
                "~/Content/OneClickReferral/css/all.css",
                "~/Content/OneClickReferral/bootstrap-datepicker/Css/bootstrap-datepicker3.css",
                "~/Content/Loader.css"
                ));
            bundles.Add(new ScriptBundle("~/Content/OneClickReferral/js").Include(
                "~/Content/OneClickReferral/js/jquery-1.11.2.min.js",
                "~/Scripts/jquery.validate.min.js",
                "~/Scripts/jquery.validate.unobtrusive.min.js",
                "~/Content/OneClickReferral/js/jquery.main.js"
                ));
            bundles.Add(new ScriptBundle("~/Content/Dashboard/js").Include(
                "~/Content/Dashboard/js/bootstrap.min.js",
                "~/Content/Dashboard/js/jquery.toaster.js",
                "~/Content/Dashboard/js/sitescript.js",
                "~/Scripts/jquery.validate.min.js",
                "~/Content/Dashboard/js/jquery.steps.js",
                "~/Scripts/jquery.validate.unobtrusive.min.js"
                //"~/Content/Dashboard/js/togglebtn.js",
                //"~/Content/Dashboard/js/Datepicker_Bootstrap_v1_4_1.js",
                //"~/Scripts/common.js",
                //"~/Scripts/moment.min.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/ProfileScript").Include(
                   "~/Scripts/select2.min.js",
                   "~/Content/Dashboard/js/jcf.js",
                   "~/Scripts/ControllerJS/Profile/Profile.js",
                   "~/Content/file_upload/file-upload-jquery.js",
                   "~/Content/JCrop/jquery.Jcrop.js"
                   ));
            bundles.Add(new ScriptBundle("~/bundles/PatientHistory").Include(
                    "~/Scripts/select2.min.js",
                    "~/Scripts/Default/jquery.iframe-transport.js",
                    "~/Scripts/Mask.js",
                    "~/Content/ckeditor/ckeditor.js",
                    "~/Content/ckeditor/samples/sample.js",
                    "~/Content/JCrop/jquery.Jcrop.js",
                    "~/Scripts/bootstrap-datepicker.js",
                    "~/Scripts/ControllerJS/PatientHistory/PatientConfiguration.js",
                    "~/Scripts/JS/sitescript.js",
                    "~/Scripts/JS/togglebtn.js",
                    "~/Scripts/commonfileupload.js",
                    "~/Content/Dashboard/js/jcf.js",
                    "~/Scripts/ControllerJS/PatientHistory/PatientHistory.js"
                  ));

        }
    }
}
