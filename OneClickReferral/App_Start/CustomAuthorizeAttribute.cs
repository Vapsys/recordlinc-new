﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
namespace OneClickReferral.App_Start
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (string.IsNullOrEmpty(SessionManagement.access_token))
            {

                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    var urlHelper = new UrlHelper(filterContext.RequestContext);
                    filterContext.HttpContext.Response.StatusCode = 403;

                    filterContext.Result = new JsonResult
                    {
                        Data = new
                        {
                            Error = "NotAuthorized",

                            LogOnUrl = urlHelper.Action("Index", "Login")
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                else
                {
                    if (filterContext.HttpContext.Request.RawUrl.Contains("OneClick"))
                    {
                        filterContext.Result = new RedirectToRouteResult(new
                                           RouteValueDictionary(new { controller = "OneClick", action = "Index" }));
                    }
                    else
                    {
                        filterContext.Result = new RedirectToRouteResult(new
                                           RouteValueDictionary(new { controller = "Login", action = "Index" }));
                    }
                    filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
                }
            }

        }
    }
}