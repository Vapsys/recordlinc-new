﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace OneClickReferral
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
            name: "OneClick",
            url: "OneClick/Index",
            defaults: new { controller = "OneClick", action = "Index" }
        );

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "OneClick", action = "Index", id = UrlParameter.Optional }

            //);

            routes.MapRoute(
             "Default", // Route name
             "{controller}/{action}/{id}", // URL with parameters
             new { controller = "OneClick", action = "Index", id = UrlParameter.Optional }, // Parameter defaults
             new string[] { "OneClickReferral.Controllers" }
            );


        }
    }
}
