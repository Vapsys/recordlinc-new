﻿function validation() {
    $("#pswd_info").css("display", "block");
    var Pass = $("#NewPassword").val();
    if (Pass.length < 8) {
        $('#length').removeClass('valid').addClass('invalid');
    } else {
        $('#length').removeClass('invalid').addClass('valid');
    }
    if (Pass.match(/[a-z]/)) {
        $('#letter').removeClass('invalid').addClass('valid');
    } else {
        $('#letter').removeClass('valid').addClass('invalid');
    }
    //validate capital letter
    if (Pass.match(/[A-Z]/)) {
        $('#capital').removeClass('invalid').addClass('valid');
    } else {
        $('#capital').removeClass('valid').addClass('invalid');
    }
    //validate number
    if (Pass.match(/\d/)) {
        $('#number').removeClass('invalid').addClass('valid');
    } else {
        $('#number').removeClass('valid').addClass('invalid');
    }
    var ankit = Pass.replace(/([0-9a-zA-Z])+/g, "");
    if (/^[!@@#$%^&*()_+{}:\"\\\'?></.,|]+$/.test(ankit)) {

        $('#special').removeClass('invalid').addClass('valid');
    } else {
        $('#special').removeClass('valid').addClass('invalid');
    }
}
function hide() {
    $("#pswd_info").css("display", "none");
}