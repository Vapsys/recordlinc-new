﻿function IsEmail(email) {
    var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email);
}
function isUrl(s) {
    var regexp = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
    return regexp.test(s);
}
// For ZipCode Validation - Start
function ValidateZipCode(data) {
    //var regx = /^([FG]?\d{5}|\d{5}[AB])$/;
    //Change Bcoz XQ1-1150
    var regx = /^[0-9]{5,6}$/;
    return regx.test(data);
}

// For ZipCode Validation - End
//For Text Counter  - Start
function textCounter(field, cnt, maxlimit) {

    var cntfield = document.getElementById(cnt)
    if (field.value.length > maxlimit) // if too long...trim it!
        field.value = field.value.substring(0, maxlimit);
        // otherwise, update 'characters left' counter
    else
        cntfield.value = maxlimit - field.value.length;
}
//For Text Counter  - End
//For CharactersOnly - Start
function IsCharactersOnly(data) {
    //data = data.replace(/'/g, "''");
    // var regex = /^[a-zA-Z]*$/;
    var regex = /^[a-zA-Z\/\s\.'-]*$/;
    //var regex = /^[a-zA-Z*]$/;
    // var regex = /^[A-Za-z\/\s\.-]+$/;
    // var regex = /^[a-z A-Z._]{1,15}$/;
    //var regex =  /^([^']([a-zA-Z])+(\s){0,1})+('){0,1}([a-zA-Z][^'])+$/;
    return regex.test(data);
}
//For CharactersOnly - End


// Just only character accept with space 
function onlyCharacters(data) {
    data = data.replace(/'/g, "");
    let regex = /^[a-zA-Z\s]*$/;
    return regex.test(data);
}

// For Phone Validation - Start
function ValidationPhone(data) {
    var RegExPhone = /^(?:\([0-9]\d{2}\)\ ?|[0-9]\d{2}(?:\-?|\ ?))[0-9]\d{2}[- ]?\d{4}$/;
    return RegExPhone.test(data);

}
function showErrorMsg(msg, delay, idPrefix) {
    //-- idPrefix is useful when one or more divs are present on page with name id like errormsg
    idPrefix = (idPrefix) ? idPrefix : "";
    delay = delay ? delay : 10000;
    $("#" + idPrefix + "errormsg").show();
    $("#" + idPrefix + "errormsg").focus();
    $("#" + idPrefix + "errormsg").html(msg);
    // $("html, body").animate({ scrollTop: $("#errormsg").offset().top }, "slow");
    if (delay >= 0) {
        setTimeout(function () {
            $("#" + idPrefix + "errormsg").fadeOut(700);
            $("#" + idPrefix + "errormsg").hide();
            $("#" + idPrefix + "errormsg").html("");
        }, delay);

    }
}

function showSuccessMsg(msg, delay, idPrefix) {
    //-- idPrefix is useful when one or more divs are present on page with name id like successmsg
    idPrefix = (idPrefix) ? idPrefix : "";
    delay = delay ? delay : 5000;
    $("#" + idPrefix + "successmsg").show();
    setTimeout(function () {
        $("#" + idPrefix + "successmsg").focus();
    }, 0);
    $("#" + idPrefix + "successmsg").html(msg);
    setTimeout(function () {
        $("#" + idPrefix + "successmsg").fadeOut(700);
        $("#" + idPrefix + "successmsg").hide();
        $("#" + idPrefix + "successmsg").html("");
    }, delay);
}

function getQueryStringParam(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        return uri + separator + key + "=" + value;
    }
}
$(document).ready(function () {

    $(".clsnumericOnly").bind("keypress", function (e) {
        var arr = [0, 8, 9, 16, 17, 20, 35, 36, 37, 38, 39, 40, 45, 46];
        // Allow numbers
        for (var i = 48; i <= 57; i++) {
            arr.push(i);
        }

        // Prevent default if not in array
        if (jQuery.inArray(e.which, arr) === -1) {
            e.preventDefault();
            return false;
        }
    });

    $(".clsalphabetsOnly").on("keydown", function (event) {

        // Allow controls such as backspace
        var arr = [8, 9, 16, 17, 20, 35, 36, 37, 38, 39, 40, 45, 46, 32];
        // Allow letters
        for (var i = 65; i <= 90; i++) {
            arr.push(i);
        }
        // Prevent default if not in array
        if (jQuery.inArray(event.which, arr) === -1) {
            event.preventDefault();
        }
    });

    $(".numericOnly").bind("keypress", function (e) {
        var arr = [0, 8, 9, 16, 17, 20, 35, 36, 37, 38, 39, 40, 45, 46];
        // Allow letters
        for (var i = 48; i <= 57; i++) {
            arr.push(i);
        }

        // Prevent default if not in array
        if (jQuery.inArray(e.which, arr) === -1) {
            e.preventDefault();
            return false;
        }
    });
    
    $(".numericOnly").bind("paste", function (e) {
        return false;
    });
    $(".numericOnly").bind("drop", function (e) {
        return false;
    });

    $(".alphaonly").on("keydown", function (event) {
        // Allow controls such as backspace
        var arr = [8, 9, 16, 17, 20, 35, 36, 37, 38, 39, 40, 45, 46, 32];

        // Allow letters
        for (var i = 65; i <= 90; i++) {
            arr.push(i);
        }

        // Prevent default if not in array
        if (jQuery.inArray(event.which, arr) === -1) {
            event.preventDefault();
        }

    });

});

function formatDate(date) {
    var monthNames = [
        "January", "February", "March",
        "April", "May", "June", "July",
        "August", "September", "October",
        "November", "December"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return monthNames[monthIndex] + ' ' + day + ', ' + year;
}

$(document).ready(function () {
    
    //$('input[class="txtdob"]').mask("99/99/9999");
    //our date input has the name "date" 
    if ($.fn.datepicker) {
        //

        $('input[class="txtdob"]').datepicker({
            format: 'm/d/yyyy',
            todayHighlight: true,
            autoclose: true,
            endDate: '+0d',
        }).on('change', function (e) {
            
            if ($(this).val() != "") {
                var date = $(this).val();
                for (var i = 0; i < date.length; i++) {
                    if (isLetter(date.charAt(i))) {
                        alert('Invalid date.');
                        $(this).val('');
                        break;
                    }
                }
            }

            if ($(this).val() != "") {
                if ($(this).val().split('/').length = 3 && $(this).val().split('/')[2].length < 4) {
                    alert('Invalid date.');
                    $(this).val('');
                    $(this).datepicker("update", moment().format('M/D/YYYY'))
                }
                if (!moment($(this).val(), 'M/D/YYYY').isValid()) {
                    alert('Invalid date.');
                    $(this).val('');
                    $(this).datepicker("update", moment().format('M/D/YYYY'))
                }
            }

        });

        //}).on("change", function (e) {
        //    var date = this.value;
        //    var sign = "";
        //    for (var i = 0; i < date.length; i++) {
        //        if (!(parseInt(date.charAt(i)))) {
        //            sign = (date.charAt(i));
        //        }
        //        if (sign != "") {
        //            break;
        //        }
        //    }

        //    var res = date.split(sign);
        //    var m = res[0];
        //    var D = res[1];
        //    var y = res[2];



    }
});

function isLetter(str) {
    return str.length === 1 && str.match(/[a-z]/i);
}
$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (!(this.name in o)) {
            if (o[this.name] !== undefined) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        }
    });
    return o;
};

jQuery.fn.outerHTML = function () {
    return jQuery('<div />').append(this.eq(0).clone()).html();
};
function performAjax(AjaxObj) {

    //var localObj = new CustomLocalStorage();
    //if (AjaxObj.url.indexOf("http") < 0) {
    //    AjaxObj.url = localObj.webapi + AjaxObj.url
    //}

    AjaxObj.type = AjaxObj.type ? AjaxObj.type.toUpperCase() : "POST";
    AjaxObj.url += (AjaxObj.url.indexOf("?") < 0 ? "?" : "&") + "cdate=" + new Date().toString()
    showLoader();
    setTimeout(function () {
        //this set timeout using for loader not show in chrome.
        $.ajax({
            url: encodeURI(AjaxObj.url),
            type: AjaxObj.type,
            //dataType: AjaxObj.dataType ? AjaxObj.dataType : "json",
            //contentType: AjaxObj.contentType ? AjaxObj.contentType : "application/json",
            data: (AjaxObj.type == "GET") ? AjaxObj.data : AjaxObj.data, //{params: params, 'module': 'Emails', token: token},
            cache: AjaxObj.cache ? AjaxObj.cache : false,
            async: AjaxObj.async ? AjaxObj.async : true,
        }).done(function (response) {
            hideLoader();
            try {
                var objJSON = jQuery.parseJSON(response);
                if (objJSON.LogOnUrl) {
                    location.href = objSON.LogOnUrl;
                }
            }
            catch (error) {

            }
            if (typeof (AjaxObj.success) == "function") {
                AjaxObj.success(response);
            }
        }).fail(function (e) {            
            hideLoader();
            if (typeof (AjaxObj.error) == "function") {              
                    if (e.responseJSON) {
                        var resonse = e.resonseJSON;
                        if (e.responseJSON.Error == "NotAuthorized") {
                            location.href = e.responseJSON.LogOnUrl;
                            return;
                        } else if (response.Message) {
                            console.log("Internal server error occured while calling " + AjaxObj.url);
                            return;
                        }
                    }                
                AjaxObj.error();
            }
        }).always(function () {
            hideLoader();          
        });
    });
}
function showLoader() {
    setTimeout(function () {
        $("#divLoading").show();
    }, 10);

}
function hideLoader() {
    setTimeout(function () {
        $("#divLoading").hide();
    }, 10);
}
function select2Dropdown(Select2ID, ph, Controller, listAction, isMultiple) {
    var sid = '#' + Select2ID;
    $(sid).select2({
        placeholder: ph,
        minimumInputLength: 1,
        multiple: isMultiple,
        ajax: {
            url: "/" + Controller + "/" + listAction,
            dataType: 'json',
            delay: 550,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            error: function (response, status, xhr) {
                if (response.status == 403) {
                    SessionExpire(response.responseText)
                }
            }
        }
    })

}
function isDate(txtDate) {
    var currVal = txtDate;
    if (currVal == '')
        return false;

    //Declare Regex
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/;
    var dtArray = currVal.match(rxDatePattern); // is format OK?
    if (dtArray == null)
        return false;

    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[1];
    dtDay = dtArray[3];
    dtYear = dtArray[5];

    if (dtMonth < 1 || dtMonth > 12)
        return false;
    else if (dtDay < 1 || dtDay > 31)
        return false;
    else if ((dtMonth == 4 || dtMonth == 6 || dtMonth == 9 || dtMonth == 11) && dtDay == 31)
        return false;
    else if (dtMonth == 2) {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay > 29 || (dtDay == 29 && !isleap))
            return false;
    }
    return true;
}


function getInsuranceDetails(strId) {
    performAjax({
        url: "/Insurance/PatientInsuranceDetails",
        type: "GET",
        data: { Id: strId },
        async: true,
        success: function (data) {
            $("#dvInsurancePopup").html(data);
            $("#dvInsurancePopup").modal('show');
            //jcf.replaceAll();
            ////$('#Phone').mask("(999) 999-9999");
            ////$('#Mobile').mask("(999) 999-9999");
            //LoadScript();
        }
    });
}



$(document).ajaxError(function (e, jqXHR, settings, err) {    
    var responseType = jqXHR.getResponseHeader("content-type") || "";
    var isJSONResponse = responseType.toLowerCase().indexOf("json") > -1;
    var responseData = jqXHR.responseJSON ? jqXHR.responseJSON : jqXHR.responseText ? JSON.parse(jqXHR.responseText) : '';
    var redirectionURL = responseData.RedirectionURL ? responseData.RedirectionURL : '/Login/Index';
    var errorThrown = responseData.Error ? responseData.Error : 'Sorry, please try again.';
    if (jqXHR.status == 401 || jqXHR.status == 400 || jqXHR.status == 403) {
        window.location.href = redirectionURL;
    }
    //if (isFunction(settings.errorCallBackFunction))
    //    settings.errorCallBackFunction(jqXHR, textStatus, errorThrown);
    //else {
        if (jqXHR.status == 500) {
            // TODO 
            // Each layout page should have one div id="dvJSCommonError" and assign this error as inner html to this DIV
            if ($("#dvJSCommonError").length > 0) {
                $("#dvJSCommonError").text(errorThrown);
            }
            else
                console.log(errorThrown);
        //}
    }
});

//$(document).ajaxStart(function (e) {
//    $(this).find('button').not(".dontdisableonajax").attr('disabled', 'disabled');
//});

//$(document).ajaxStop(function (e) {
//    $(this).find('button').not(".dontdisableonajax").removeAttr('disabled');
//});