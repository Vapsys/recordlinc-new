﻿$(document).ready(function () {
    $(".wizard-inner li").removeClass("active");
    $("#liTeamMember").addClass("active");
    GetList();
   
});

//Get List of Team Member
function GetList() {
    var Obj = {
        'PageIndex': $("#hdnPageIndex").val(),
        'PageSize': $("#hdnPageSize").val(),
        'SearchText': $.trim($("#txtSearchTeam").val()),
        'SortColumn': $("#hdnSortColumn").val(),
        'SortDirection': $("#hdnSortDirection").val(),
        'LocationBy': $("#LocationBy").find(':selected').val(),
        'ProviderTypefilter': $("#ddlProviderfilter").find(':selected').val(),
    };
    performAjax({
        url: "/Settings/GetTeamMember",
        type: "POST",
        data: { _filter: Obj },
        success: function (data) {
            if (data) {
                $("#AppendList").html('');
                $("#AppendList").append(data);
                //XQ1-598 Incorrect information message appears after searching non-existing Team Member, under 'Team Members' page.
                if ($("#AppendList tr").length >= 1) {
                    $("#nomore").text("No record found!");
                }
                if (data.includes("nomore")) {
                    $("#hdnPageIndex").val(-1);
                }
            } else {

            }
            jcf.replaceAll();
        }
    });
}

//While change the Location Dropdown Team Member lsit apper as Location wise.
$("#LocationBy").on('change', function () {
    $("#hdnPageIndex").val(1);
    $('#txtSearchTeam').val('');
    GetList();
})

$("#ddlProviderfilter").on('change', function () {
    $("#hdnPageIndex").val(1);
    $('#txtSearchTeam').val('');
    GetList();
})


//Sort Column and Direction common logic.
function SortColumn(ColId) {
    var SortCol = $("#" + ColId).attr('sort-col');
    var Sortdir = $("#" + ColId).attr('sort-dir');
    $("#hdnSortColumn").val(SortCol);
    $("#hdnSortDirection").val(Sortdir);
    if (Sortdir == 1) {
        $("#" + ColId).attr('sort-dir', 2);
    } else {
        $("#" + ColId).attr('sort-dir', 1);
    }
    var pagesize = $("#hdnPageSize").val();

    var pageindex = $("#hdnPageIndex").val();

    //RM-194 changes for page index.
    if (pageindex == -1) {
        pageindex = 1;
    }
    $("#hdnPageIndex").val(1);

    GetList();
}

//On Enter press search the Team Member list.
function HdrSearchTeam(event) {
    if (event.keyCode == 13) {
        //XQ1-586 All records do not appear on clicking 'Search' icon when 'Search' field is blank, on 'Team Members' page.
        //var message = $('#txtSearchTeam').val();
        //if (message != null && message != "") {
        SearchTeam();
        //}
    }
}

//Search Team 
function SearchTeam() {
    var obj = {
        'PageIndex': 1,
        'PageSize': 50,
        'SearchText': $.trim($("#txtSearchTeam").val()),
        'SortColumn': 2,
        'SortDirection': 1,
        'LocationBy': $("#LocationBy").find(':selected').val(),
    }
    performAjax({
        url: "/Settings/GetTeamMember",
        type: "POST",
        data: { _filter: obj },
        success: function (data) {
            $("#hdnPageSize").val(parseInt(50));
            $("#hdnSortColumn").val(parseInt(2));
            $("#hdnSortDirection").val(parseInt(1));
            $("#AppendList").html('');
            $("#AppendList").html(data);
            //XQ1-598 Incorrect information message appears after searching non-existing Team Member, under 'Team Members' page.
            if ($("#AppendList tr").length >= 1) {
                $("#nomore").text("No record found!");
            }
            if (data.includes("nomore")) {
                $("#hdnPageIndex").val(-1);
            } else {
                $("#hdnPageIndex").val(parseInt(1));
            }
            jcf.replaceAll();
        },
        error: function (err) {
            alert(err);
        }
    });
}

//CheckBox logic to select all check-box
$(".checkedAll").change(function () {
    if (this.checked) {
        $(".checkSingle").each(function () {
            $(this).prop('checked', true);
            $(".all-select span").removeClass('jcf-unchecked').addClass('jcf-checked');
        })
    } else {
        $(".checkSingle").each(function () {
            $(this).prop('checked', false);
            $(".all-select span").removeClass('jcf-checked jcf-focus').addClass('jcf-unchecked');
        })
    }
});

//Un check main checkbox when single checkbox is un check
function UncheckMaincheckBox() {
    $(".checkedAll").parent('span').removeClass('jcf-checked').addClass('jcf-unchecked');
    $(".checkedAll").prop("checked", false);
}

//Bind Scroll
$(function () {
    var bindScrollHandler = function () {
        var win_hg = ($(window).height()) * 10 / 100;
        $(window).scroll(function () {
            //console.log('$(window).scrollTop():- ' + $(window).scrollTop() + '$(document).height():- ' + $(document).height() + ' $(window).height():- ' + $(window).height() + ' win_hg:- ' + win_hg);
            if (parseInt($(window).scrollTop()) >= parseInt($(document).height() - $(window).height())) {
                var PageIndex = $("#hdnPageIndex").val();
                if (parseInt(PageIndex) == -1) {
                    $(window).unbind("scroll");
                    $('#divLoading').hide();
                }
                if (parseInt(PageIndex) != -1 && $("#AppendList #nomore").length == 0) {
                    //$(window).scrollTop($(window).scrollTop() - parseInt($(window).scrollTop() / 2))
                    GetTeamMemberOnScroll();
                }
            } else {

            }
        });
    };
    bindScrollHandler();
});

//Get Team Member list on Scroll
function GetTeamMemberOnScroll() {
    var PageIndex = $("#hdnPageIndex").val();
    if (PageIndex == -1) {
        return false;
    } else {
        PageIndex = parseInt(PageIndex) + parseInt(1);
    }
    var Obj = {
        'PageIndex': PageIndex,
        'PageSize': $("#hdnPageSize").val(),
        'SortColumn': $("#hdnSortColumn").val(),
        'SortDirection': $("#hdnSortDirection").val(),
        'SearchText': $("#txtSearchTeam").val(),
        'LocationBy': $("#LocationBy").find(':selected').val()
    }
    performAjax({
        url: "/Settings/GetTeamMember",
        type: "POST",
        data: { _filter: Obj },
        success: function (data) {
            var html = $("#AppendList").html();
            if (data != null && data != "" && html.indexOf("No more record found") < 0) {
                $("#AppendList").append(data);
                if (data.includes("nomore")) {
                    $("#hdnPageIndex").val(-1);
                } else {
                    $("#hdnPageIndex").val(parseInt(PageIndex));
                }
                jcf.replaceAll();
            } else {
                $("#hdnPageIndex").val(parseInt(-1));
                return false;
            }
        }, error: function () {

        }
    });
}

///Add/Edit Team member popup open.
function AddEditTeamMember(id) {
    $("#add-team-members").html('');
    performAjax({
        url: "/Settings/GetTeamMemberDetails?id=" + id,
        type: "GET",
        success: function (data) {
            if (data) {
                $("#add-team-members").modal('show');
                $("#add-team-members").append(data);
                jcf.replaceAll();
            } else {

            }
        }
    });
}

//Add/Edit Team Member
function AddEditTeamMemberDetails(id) {
    if ($.trim($("#FirstName").val()) == null || $.trim($("#FirstName").val()) == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter First Name!' });
        $("#FirstName").focus();
        ToggleSaveButton('btn_TeamMember');
        return false;
    } else {
        //XQ1-603 There is no limit on number of characters that can be entered in 'First Name' and 'Last Name' text field under 'Add Team/Staff Member' popup on 'Team Members' page.
        let firstNameCheck = onlyCharacters($.trim($("#FirstName").val()));
        if (firstNameCheck == false) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter only character in First Name'});
            $("#FirstName").focus();
            ToggleSaveButton('btn_TeamMember');
            return false;
        }else if ($('#FirstName').val().toString().length > 20) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'First Name allow maximum 20 characters only.' });
            $("#FirstName").focus();
            ToggleSaveButton('btn_TeamMember');
            return false;
        }
    }
    if ($.trim($("#LastName").val()) == null || $.trim($("#LastName").val()) == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter Last Name!' });
        $("#LastName").focus();
        ToggleSaveButton('btn_TeamMember');
        return false;
    } else {
        //XQ1-603 There is no limit on number of characters that can be entered in 'First Name' and 'Last Name' text field under 'Add Team/Staff Member' popup on 'Team Members' page.
        let lastNameCheck = onlyCharacters($.trim($("#LastName").val()));
        if (lastNameCheck == false) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter only character in Last Name' });
            $("#FirstName").focus();
            ToggleSaveButton('btn_TeamMember');
            return false;
        }else if ($('#LastName').val().toString().length > 20) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Last Name allow maximum 20 characters only.' });
            $("#LastName").focus();
            ToggleSaveButton('btn_TeamMember');
            return false;
        }
    }
    //if ($.trim($("#ProviderId").val()) != null || $.trim($("#ProviderId").val()) != "") {
    //    if ($('#ProviderId').val().toString().length > 20) {
    //        $.toaster({ priority: 'warning', title: 'Notice', message: 'PMS Provider Id allow maximun 20 characters only.' });
    //        $("#ProviderId").focus();
    //        ToggleSaveButton('btn_TeamMember');
    //        return false;
    //    }
    //}
    if ($.trim($("#Email").val()) == null || $.trim($("#Email").val()) == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter Email!' });
        $("#Email").focus();
        ToggleSaveButton('btn_TeamMember');
        return false;
    } else {
        var result = IsEmail($("#Email").val());
        if (result != true) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter a valid Email Address.' });
            $("#Email").focus();
            ToggleSaveButton('btn_TeamMember');
            return false;
        }
        else {
            if ($('#Email').val().toString().length > 50) {
                $.toaster({ priority: 'warning', title: 'Notice', message: 'Email allow maximun 50 characters only.' });
                $("#Email").focus();
                ToggleSaveButton('btn_TeamMember');
                return false;
            }
        }
    }
    var Obj = $("#frmTeam").serializeObject();
    performAjax({
        url: "/Settings/CheckEmailExists?Email=" + $("#Email").val() + "&id=" + id,
        type: "POST",
        success: function (data) {
            if (data) {
                $.toaster({ priority: 'info', title: 'Notice', message: 'This Email Address is already in use.' });
                $("#Email").focus();
                return false;
            } else {
                performAjax({
                    url: "/Settings/AddEditTeamMember",
                    type: "POST",
                    data: { _team: Obj },
                    success: function (data) {
                        if (Obj.provtype ==="false") {
                            if (id == 0) {
                                $.toaster({ priority: 'success', title: 'Notice', message: 'Team Member added successfully.' });
                            } else {
                                $.toaster({ priority: 'success', title: 'Notice', message: 'Team Member updated successfully.' });
                            }
                        } else {
                            if (id == 0) {
                                $.toaster({ priority: 'success', title: 'Notice', message: 'Staff Member added successfully.' });
                            } else {
                                $.toaster({ priority: 'success', title: 'Notice', message: 'Staff Member updated successfully.' });
                            }
                        }

                        ToggleSaveButton('btn_TeamMember');
                        setTimeout(function () {
                            window.location.reload();
                        }, 4000);
                    }, error: function () {
                        if (Obj.provtype === "false") {
                            if (id == 0) {
                                $.toaster({ priority: 'warning', title: 'Notice', message: 'Failed to add a Team Member.' });
                            } else {
                                $.toaster({ priority: 'warning', title: 'Notice', message: 'Failed to update a Team Member.' });
                            }
                        } else {
                            if (id == 0) {
                                $.toaster({ priority: 'warning', title: 'Notice', message: 'Failed to add a Staff Member.' });
                            } else {
                                $.toaster({ priority: 'warning', title: 'Notice', message: 'Failed to update a Staff Member.' });
                            }
                        }
                    }
                });
            }
        },
    });

}

//Toggle the button disable when the alert message is show.
function ToggleSaveButton(id) {
    $('#' + id).attr('disabled', true);
    setTimeout(function () {
        $('#' + id).attr('disabled', false);
    }, 4000);
}

//Delete team member with selected and single
function DeleteTeamMember(id) {
    var Obj = {
        'TeamMemberId': id
    }
    performAjax({
        url: "/Settings/RemoveTeamMember?id=" + id,
        type: "POST",
        data: { _remove: Obj },
        success: function (data) {
            if (data) {
                $.toaster({ priority: 'success', title: 'Notice', message: 'Member removed successfully.' });
                ToggleSaveButton('btnyes');
                setTimeout(function () {
                    window.location.reload();
                }, 4000);
            } else {
                $.toaster({ priority: 'success', title: 'Notice', message: 'Failed to remove member.' });
            }
        }
    });
}

//Open Popup for Confirmation about delete the Team Member.
function OpenRemoveTeamMember(UserId) {
    $("#Team_delete").modal('show');
    $('#desc_message').text('Are you sure you want to remove?');
    $('#btnyes').attr('onclick', ' DeleteTeamMember("' + UserId + '")');
}

//Delete Multiple selected team Member.
function DeleteSelected() {
    var userlist = []; var cnt = 0;
    $(".checkSingle").each(function () {
        if ($(this).prop('checked')) {
            cnt++;
            userlist.push($(this).val());
        }
    });
    if (userlist != null && userlist != "") {
        $('#Team_delete').modal('show');
        $('#desc_message').text('Are you sure you want to Delete selected members?');
        $('#btnyes').attr('onclick', ' DeleteTeamMember("' + userlist + '")');
    }
    else {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select at least one member!' });
        ToggleSaveButton('btnMultipleDelete');
        return false;
    }
}
//$(document).keydown(function (e) {
//    var nodeName = e.target.nodeName.toLowerCase();
//    if (e.which === 8) {
//        if ((nodeName === 'input' && e.target.type === 'text') ||
//            nodeName === 'textarea') {
//            // do nothing
//        } else {
//            e.preventDefault();
//        }
//    }
//});