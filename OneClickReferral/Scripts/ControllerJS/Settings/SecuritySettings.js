﻿function SubmitData() {
    var Identifier = null;
    var PrimaryEmail = $('#hdnPrimaryEmail').val();
    var PrimaryEmailNew = $('#PrimaryEmail').val();
    var LocationId = $("#LocationId").val();
    var defaultLocationId = $("#hdnLocationId").val();
    if ($.trim($("#Username").val()) == null || $.trim($("#Username").val()) == "" || $.trim($("#Username").val()) == undefined) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Username field is required!' });
        $("#Username").val('');
        $("#Username").focus();
        return false;
    }
    if ($.trim($("#PrimaryEmail").val()) == null || $.trim($("#PrimaryEmail").val()) == '' || $.trim($("#PrimaryEmail").val()) == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter valid email address in Primary Email field.' });
        $("#PrimaryEmail").focus();
        return false;
    }
    if ($.trim($("#SecondaryEmail").val()) == $.trim($("#PrimaryEmail").val())) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Primary and Secondary emails are same. Please try again with another Email ID.' });
        return false
    }
    if (PrimaryEmailNew != PrimaryEmail) {
        if ($.trim($("#PrimaryEmail").val()) != null && $.trim($("#PrimaryEmail").val()) != "") {
            var result = IsEmail($("#PrimaryEmail").val());
            if (!result) {
                $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter valid primary email' });
                $("PrimaryEmail").focus();
                return false;
            }
        }
    }
    //XQ1 - 481 change request by Travis for Remove Stay logged in, Location and Confrim password fields.
    //if ($("#NewPassword").val() != "") {
    //    if ($("#ConfrimPassword").val() == "") {
    //        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter confirm password' });
    //        return false;
    //    }
    //}
    //XQ1 - 481 change request by Travis for Remove Stay logged in, Location and Confrim password fields.*@
    //if ($("#NewPassword").val() != $("#ConfrimPassword").val()) {
    //    $.toaster({ priority: 'warning', title: 'Notice', message: 'New Password and Confirm Password does not match' });
    //    return false;
    //}
    
    var SecondaryEmail = $('#hdnSecondaryEmail').val();
    var SecondaryEmailNew = $("#SecondaryEmail").val();
    var Pemail = $('#PrimaryEmail').val().split('@@');

    if (Pemail[0].length > 50) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Primary Email should be of valid 50 characters only' });
        $('#PrimaryEmail').focus();
        return false;
    }
    var SEmail = $('#SecondaryEmail').val().split('@@');
    if (SEmail[0].length > 50) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Secondary Email should be of valid 50 characters only' });
        $('#SecondaryEmail').focus();
        return false;
    }
    if (SecondaryEmailNew != SecondaryEmail) {
        if ($("#SecondaryEmail").val() != null && $("#SecondaryEmail").val() != "") {
            var result1 = IsEmail($("#SecondaryEmail").val());
            if (!result1) {
                $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter valid email address in Secondary Email field.' });
                $("SecondaryEmail").focus();
                return false;
            }
        }
    }
    var pass = $.trim($('#NewPassword').val());
    //XQ1 - 481 change request by Travis for Remove Stay logged in, Location and Confrim password fields.
    if (pass != null && pass != "") {
        var re = /^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d])(?=.*[@@#!$%^&+=]).*$/;
        if (!re.test(pass)) {
            validation();
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Password must be a minimum of 8 characters long  and contain \n an upper and lower case letter, a number, and a symbol such as #,!,$,%,&,_..' });
            return false;
        }
    }
    var Obj = {
        'LocationId': $("#LocationId").val(),
        'StayLogin': $("#StayLogin").val(),
        'Username': $("#Username").val(),
        'PrimaryEmail': $("#PrimaryEmail").val(),
        'SecondaryEmail': $("#SecondaryEmail").val(),
        'NewPassword': $("#NewPassword").val(),
        'Email': $("#Email").is(":checked"),
        'Text': $("#Text").is(":checked"),
        'Fax': $("#Fax").is(":checked"),
        'OldPrimaryEmail': $("#hdnPrimaryEmail").val(),
        'OldSecondaryEmail': $("#hdnSecondaryEmail").val(),
        'OldLocationId': $("#hdnLocationId").val(),
        'OldUsername': $("#hdnUsername").val()
    }
    if ($("#PrimaryEmail").val() != null && $("#PrimaryEmail").val() != "") {
        performAjax({
            url: "/Settings/Submit",
            type: "POST",
            data: { accountsettings: Obj },
            success: function (data) {
                $.toaster({ priority: 'success', title: 'Notice', message: data });
                setTimeout(function () {
                    window.location.reload();
                }, 4000);
            }, error: function (err) {
            }
        });
    }
    else {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter primary email' });
        return false;
    }
}
$("#confirm").on('click', function () {
    if ($("#confirm i").hasClass("fa fa-eye")) {
        $("#confirm i").removeClass("fa fa-eye").addClass("fa fa-eye-slash");
        $("#ConfrimPassword").attr('type', 'password');
    } else {
        $("#confirm i").removeClass("fa fa-eye-slash").addClass("fa fa-eye");
        $("#ConfrimPassword").attr('type', 'text');
    }
});
$("#password").on('click', function () {
    if ($("#password i").hasClass("fa fa-eye")) {
        $("#password i").removeClass("fa fa-eye").addClass("fa fa-eye-slash");
        $("#NewPassword").attr('type', 'text');
        $("#password").attr('title', 'Hide').tooltip('fixTitle').tooltip('show');
    } else {
        $("#NewPassword").attr('type', 'password');
        $("#password i").removeClass("fa fa-eye-slash").addClass("fa fa-eye");
        $("#password").attr('title', 'Show').tooltip('fixTitle').tooltip('show');
    }
});