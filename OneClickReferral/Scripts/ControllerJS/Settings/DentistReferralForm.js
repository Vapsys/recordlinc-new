﻿$(document).ready(function () {
    $(".wizard-inner li").removeClass("active");
    $("#liReferralForm").addClass("active");
    GetReferralData();
});
function LoadScript() {
    $(".xyz input[type=hidden]").appendTo("#hdnfields");
    $(".SectionClass").each(function () {
        var name = $(this).find('input[type=checkbox]').data('name');
        if ($("#isSelected_" + name).is(':checked')) {
            $("#Service_" + name).show();
        } else {
            $("#Service_" + name).hide();
        }
    });
    $(".itemDiv").each(function () {
        var name = $(this).find('input[type=checkbox]').data('id');
        if ($(this).find('input[type=checkbox]').is(':checked')) {
            $("#Service_" + name).show();
        } else {
            $("#Service_" + name).hide();
        }
    });
}
function GetReferralData() {
    performAjax({
        url: "/Settings/GetReferralData",
        type: "POST",
        success: function (data) {
            if (data) {
                $("#doc").append(data);
                LoadScript();
            }
        }
    });
}
function ShowRefSettingData(id, divid) {
    if ($('#' + id).is(':checked')) {
        $('#' + divid).show();
    } else {
        $('#' + divid).hide();
    }
    if ($('#' + id).is(':checked') == false) {
        $("#" + divid + " .itemDiv").each(function () {
            $(this).find('input[type=checkbox]').attr("checked", false);
        });
    }
}
function ShowSubItemData(id, divid) {
    if ($('#' + id).is(':checked')) {
        $('#' + divid).show();
    } else {
        $('#' + divid).hide();
    }
    if ($('#' + id).is(':checked') == false) {
        $("#" + divid + " .FinalDiv").each(function () {
            $(this).find('input[type=checkbox]').attr("checked", false);
        });
    }
}
function SaveReferralFormSettings() {

    var obj = [];
    var Serviceobj = [];
    //Detail Object
    $("#DetailId .ref_form_setting").each(function (index) {
        proc = {
            'Name': $(this).find('input[type=checkbox]').data('name'),
            'IsEnable': $(this).find('input[type=checkbox]').is(':checked'),
            'OrderBy': index + 1
        }
        obj.push(proc);
    });

    //speciality obj
    var spObj = [];
    $(".SectionClass").each(function (index) {
        proc = {
            'SpecialityId': $(this).find('input[type=checkbox]').data('id'),
            'IsEnable': $(this).find('input[type=checkbox]').is(':checked'),
            'OrderBy': index + 1
        }
        spObj.push(proc);
    });

    //service obj
    var serviceobj = [];
    $(".itemDiv").each(function (index) {
        proc = {
            'FieldId': $(this).find('input[type=checkbox]').data('id'),
            'IsEnable': $(this).find('input[type=checkbox]').is(':checked'),
            'OrderBy': index + 1,
            'SpecialityId': $(this).find('input[type=checkbox]').data('name')
        }
        serviceobj.push(proc);
    });

    //final Section
    var subsectionObj = [];
    $(".FinalDiv").each(function (index) {
        proc = {
            'SpecialityId': $(this).find('input[type=checkbox]').data('name'),
            'ParentFieldId': $(this).find('input[type=checkbox]').data('id'),
            'SubFieldId': $("#" + $(this).find('input[type=checkbox]').data('hdnid')).val(),
            'IsEnable': $(this).find('input[type=checkbox]').is(':checked'),
            'OrderBy': index + 1
        }
        subsectionObj.push(proc);
    });
    var Obj = {
        'RefFrmSetting': obj,
        'SpecialtySetting': spObj,
        'ServiceSetting': serviceobj,
        'SubFieldSetting': subsectionObj
    }
    performAjax({
        url: "/Settings/SaveReferralFormSettings",
        type: "POST",
        data: { _save: Obj },
        success: function (data) {
            if (data == true) {
                //$.toaster({ priority: 'success', title: 'Notice', message: 'Referral Form Settings saved Successfully.' });
                //ToggleSaveButton('btn-dentist');
            }
        }
    });
}
var isDirty = false;
$(document).on('change', '.new_plan input:checkbox', function () {
    if (!isDirty) {
        isDirty = true;
    }
});



window.setInterval(function () {
    if (isDirty) {
        SaveReferralFormSettings();
        isDirty = false;
    }
}, 5000);
var unloadEvent = function (e) {
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("Firefox");
    if (msie > -1) {
        (e || window.event).returnValue = SaveReferralFormSettings();
    } else {
        return SaveReferralFormSettings();
    }
};
window.addEventListener("beforeunload", unloadEvent);