﻿if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
    var msViewportStyle = document.createElement('style')
    msViewportStyle.appendChild(
        document.createTextNode(
            '@@-ms-viewport{width:auto!important}'
        )
    )
    document.querySelector('head').appendChild(msViewportStyle)
}
$(document).ready(function () {
    $(".wizard-inner li").removeClass("active");
    $("#liIntegration").addClass("active");
    Get();
});
function Get() {
    performAjax({
        url: "/Settings/GetIntegration",
        type: "GET",
        success: function (data) {
            if (data != '' && data != null) {
                $("#appendIntegration").append(data);
                LoadScript();
                Script();
            } else {
                console.log("Somethings is not correct while requesting data");
            }
        }, error: function (err) {
            console.error(err.Text);
        }
    });
}
function Script() {
    $(".integrations").each(function () {
        var IntegrationId = $(this).attr('id');
        var TotalCount = $(".integration #trInt-" + IntegrationId).length;
        var Count = 0;
        //$(".integration #trInt-1").find('input[type="checkbox"]').each(function () {
        //    if (!$(this).is(":checked")) {
        //        Count = Count + 1;
        //    }
        //});
        $(".integration #trInt-"+IntegrationId+"").find('input[type="checkbox"]').each(function () {
            if (!$(this).is(":checked")) {
                Count = Count + 1;
            }
        });
        if (TotalCount == Count) {
            $('.' + IntegrationId + '-show').hide();
            $('#' + IntegrationId).prop("checked", false);
        }
    });
}
function LoadScript() {  
    $("#appendIntegration input[type='checkbox']").each(function () {
        var CheckBoxId = $(this).attr('id');
        if ($('#' + CheckBoxId).is(":checked")) {
            $('.' + CheckBoxId + '-show').show();
        } else {
            $('.' + CheckBoxId + '-show').hide();
        }
    });
}
function toggle(CheckBoxId,integrationName) {
    if ($('#' + CheckBoxId).is(":checked")) {
        EnableIntegration(CheckBoxId,integrationName);
    } else {
        //02-08-2019 As per Discussion with Travis and Chirag sir Travis want disable all Integration section whenever user will change toggle as off.
        Confirmation(integrationName, CheckBoxId);
    }
}


//For Disable Integration section time show the confirmation popup that Are you sure you want to Disable your all Dentrix Integrations?'
function Confirmation(integrationName,integrationId) {
    $("#disableIntegration").modal('show');
    $('#desc_message').text('Are you sure you want to disable all '+integrationName+' Integrations?');
    $('#btn_yes').attr('onclick', ' DisableIntegration("' + integrationId + '","'+integrationName+'")');
    $("#btn_no").attr('onclick', 'reverse("' + integrationId + '")');
}
function reverse(integrationId) {
    $('#' + integrationId).prop("checked", true);
    $('.' + integrationId + '-show').show();
}
function EnableIntegration(integrationId,integrationName) {
    performAjax({
        url: "/Settings/EnableIntegration",
        type: "POST",
        data: { id: integrationId },
        success: function (data) {
            $("#appendIntegration").html('');
            Get();
            setTimeout(function () {
                $('#' + integrationId).prop("checked", true);
                $('.' + integrationId + '-show').show();
            }, 2000)
            //XQ1 - 758 Incorrect success message appears when user disable/enable toggle buttons under 'Integrations' page.
            $.toaster({ priority: 'success', title: 'Success', message: integrationName + ' enabled successfully!' });
        }, error: function (err) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Filed to enable ' + integrationName +'!' });
            $('.' + integrationId + '-show').hide();
        }
    })
    $('#' + integrationId).prop("checked", true);
    $('.' + integrationId + '-show').show();
}
function DisableIntegration(integrationId,integrationName) {
    var DentrixConnectorId = '';
    $("#trInt-" + integrationId + " input[type='hidden']").each(function () {
        DentrixConnectorId += $(this).val() +",";
    });
    performAjax({
        url: "/Settings/DisableIntegration",
        type: "POST",
        data: { DentrixConnectorIds: DentrixConnectorId },
        success: function (data) {
            $('.' + integrationId + '-show').hide();
            $("#disableIntegration").modal('hide');
            //XQ1 - 758 Incorrect success message appears when user disable/enable toggle buttons under 'Integrations' page.
            $.toaster({ priority: 'success', title: 'Success', message: integrationName +' disabled successfully!' });
        }, error: function (err) {
            $('.' + integrationId + '-show').show();
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Filed to disable ' + integrationName +'!' });
        }
    });
}
//Add Account for Perticular integration
function AddAccount(integrationId, integrationName) {
    //Check the Integration has remain location or not.
    //If yes then add new row with the Location otherwise show the Error message.
    performAjax({
        url: "/Settings/GetUnSyncLocation?IntegrationId=" + integrationId,
        type: "GET",
        async: false,
        success: function (dataval) {
            if (dataval != null && dataval != '') {
                //Get Row HTML data which is hidden.
                var HTML = $("#trHidden-" + integrationId).html();
                //Append that data on the perticular Integration section.
                $("#" + integrationId + "-tbody tr:last").before($("<tr id='tr-"+ integrationId + "' class='newadded'></tr>").html(HTML));
                //Generate New row
                AddNewRow(integrationId);
            } else {
                //Show error you have sync all location with the Integration
                $.toaster({ priority: 'info', title: 'Notice', message: 'You are already connected to all locations with the ' + integrationName });
                //ShowError(integrationName);
            }
        }, error: function (err) {
            //Get UnSync Location Error Portion
        }
    });
}

//Add new Row if UnSync location has a Data.
function AddNewRow(integrationId) {
    performAjax({
        url: "/Settings/AddAccount",
        type: "GET",
        async: false,
        success: function (data) {
            if (data != "", data != null) {
                //Add New Connector Key
                $("#" + integrationId + "-tbody tr:last").prev().find("input[type='text']").val(data);
                //Apply that key as readonly
                $("#" + integrationId + "-tbody tr:last").prev().find("input[type='text']").prop('readonly', true);
                //Remove the Location Option on it.
                $("#" + integrationId + "-tbody tr:last").prev().find("#ddlLocation-" + integrationId + " option").remove();
                //Append UnSync Location List
                AppendLocation(integrationId);
                freezAddButton(integrationId);
            }
        }, error: function (err) {
            console.error(err.Text);
            //Add Account Error Portion
        }
    });
}

//Append UnSync Location on the Drop-down list.
function AppendLocation(integrationId) {
    performAjax({
        url: "/Settings/GetUnSyncLocation?IntegrationId=" + integrationId,
        type: "GET",
        async: false,
        success: function (location) {
            if (location) {
                $.each(location, function () {
                    $("#" + integrationId + "-tbody tr:last").prev().find("#ddlLocation-" + integrationId).append($("<option></option>").val(this['Value']).html(this['Text']));
                });
            }
        }
    });
}

//Show the Error Message that U have already all location sync with the Integration
function ShowError(integrationName) {
    //Change text baised on Integration section
    $("#snackbar").html("<b>Notice</b> You are already connected to all locations with the " + integrationName);
    // Add the "show" class to DIV
    $("#snackbar").addClass('show');
    // After 5 seconds, remove the show class from DIV
    setTimeout(function () {
        $("#snackbar").removeClass('show');
    }, 5000);
}

//Submit the form to Save Integrations.
function submitData() {
    var Obj = [];
    $(".newadded").find('td').each(function () {
        var values = {};
        $(this).find('input[type="text"]').each(function () {
            Obj.push($(this).attr('id') + '_' + $(this).val() + '_' + $(this).parent().next('td').find('select').val());
        });
    });
    var savingdata = {
        'Integration': Obj
    };
    //XQ1-759 User is not able to save details on 'Integrations' page.
    if (Obj.length > 0) {
    performAjax({
        url: "/Settings/SaveIntegrationSetting",
        type: "POST",
        data: { save: savingdata },
        success: function (data) {
            if (data) {
                $.toaster({ priority: 'success', title: 'Success', message: 'Integration settings updated successfully!' });
                setTimeout(function () {
                    window.location.reload();
                }, 5000);
                    ToggleSaveButton("btnSubmit");
            } else {
                $.toaster({ priority: 'warning', title: 'Notice', message: 'Filed to update Integration settings!' });
            }
        }, error: function () {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Filed to update Integration settings!' });
        }
    })
    } else {
        $.toaster({ priority: 'success', title: 'Success', message: 'Integration settings updated successfully!' });
        setTimeout(function () {
            window.location.reload();
        }, 5000);
        ToggleSaveButton("btnSubmit");
    }
    console.log(Obj);
}
function deleteTemp(id,integrationId) {
    $(id).closest('tr').remove();
    $("#btn-" + integrationId).attr('disabled', false);
}
function freezAddButton(id) {
    $("#btn-" + id).attr('disabled', true);
}
function ToggleSaveButton(id) {
    $('#' + id).attr('disabled', true);
    setTimeout(function () {
        $('#' + id).attr('disabled', false);
    }, 4000);
}
//XQ1-687 'Delete' and 'Edit' icons do not appear for added Guid Key, under 'Integrations' page.
function EnableDisable(ConnectorId, integrationName) {
    if ($("#int_" + ConnectorId).is(":checked")) {
        SingleEnable(ConnectorId, integrationName);
    } else {
        ConnectorConfirmation(ConnectorId, integrationName);
    }
}
function SingleEnable(ConnectorId, integrationName) {
    var LocationName = $("#int_" + ConnectorId).closest('td').siblings('td').next('td').next('td').find('#items_LocationId option:selected').text();
    performAjax({
        url: "/Settings/EnableConnector",
        type: "POST",
        data: { id: ConnectorId },
        success: function (data) {
            if (data) {
                $('#int_' + ConnectorId).prop("checked", true);
                $.toaster({ priority: 'success', title: 'Success', message: integrationName + ' integration for ' + LocationName +' office is enabled now.' });
                $("#disableIntegration").modal('hide');
            } else {
                $('#int_' + ConnectorId).prop("checked", false);
            }
        }
    });
}
function SingleDisable(ConnectorId, integrationName) {
    var LocationName = $("#int_" + ConnectorId).closest('td').siblings('td').next('td').next('td').find('#items_LocationId option:selected').text();
    performAjax({
        url: "/Settings/DisableConnector",
        type: "POST",
        data: { id: ConnectorId },
        success: function (data) {
            if (data) {
                $('#int_' + ConnectorId).prop("checked", false);
                $.toaster({ priority: 'success', title: 'Success', message: integrationName + ' integration for ' + LocationName +' office is disabled now.' });
                $("#disableIntegration").modal('hide');
            } else {
                $('#int_' + ConnectorId).prop("checked", true);
            }
        }
    });
}
function ConnectorConfirmation(ConnectorId, integrationName) {
    $("#disableIntegration").modal('show');
    var LocationName = $("#int_" + ConnectorId).closest('td').siblings('td').next('td').next('td').find('#items_LocationId option:selected').text();
    $('#desc_message').text('Are you sure you want to disable ' + integrationName + ' for ' + LocationName +' office?');
    $('#btn_yes').attr('onclick', ' SingleDisable("' + ConnectorId + '","' + integrationName +'")');
    $("#btn_no").attr('onclick', 'reverseConnector("' + ConnectorId + '")');
}
function reverseConnector(ConnectorId) {
    $('#int_' + ConnectorId).prop("checked", true);
}