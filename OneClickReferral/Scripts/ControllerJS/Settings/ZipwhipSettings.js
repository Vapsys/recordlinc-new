﻿$(document).ready(function () {
    $(".wizard-inner li").removeClass("active");
    $("#liZipwhipSettings").addClass("active");
    //$('#btncreateaccount').prop('disabled', true);


    //called when key is pressed in textbox
    $("#txtzipwhipnumber").keypress(function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            //display error message
            $("#errmsg").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });


});


function CheckEligible() {
    

    $('#add_sms_step1 .alert-danger').html('');
    $('#add_sms_step1 .alert-success').html('');

    var zipwhipphone = $('#txtzipwhipnumber').val();
    var accountname = $('#txtaccountname').val();
    var email = $('#txtemailaddress').val();
    var location = $('#locationid').val();
   
    if (location == "" || location == "0") {
        $('#add_sms_step1 .alert-danger').html('Please select location.');
        return false;
    }
    if (accountname == "") {
        $('#add_sms_step1 .alert-danger').html('Please enter the account name.');
        return false;
    }

    if (accountname.trim() == "") {
        $('#add_sms_step1 .alert-danger').html('Please enter the account name.');
        return false;
    }

    if (/(.+)@(\w+){2,}\.(\w+){2,}/.test(email)) {
        //alert('valid');
    } else {
        $('#add_sms_step1 .alert-danger').html('Please enter valid email address.');
        return false;
    }

    //if (email == "" || email.indexOf(" ") !== -1) {
    //    $('#add_sms_step1 .alert-danger').html('Please enter valid email address.');
    //    return false;
    //}

    if (zipwhipphone == "" || zipwhipphone.indexOf(" ") !== -1) {
        $('#add_sms_step1 .alert-danger').html('Please enter the zipwhip number without space.');
        return false;
    }

    if (zipwhipphone.length<=3) {
        $('#add_sms_step1 .alert-danger').html('Please enter the proper zipwhip number.');
        return false;
    }

    var model_Param = { phone_number: zipwhipphone }
    var model = { api_method: "get", parameter: model_Param }
    performAjax({
        url: "/Zipwhip/CheckEligible",
        type: "POST",
        data: { model: model },
        success: function (data) {
            if (data != null && data.Result != null) {
                if (data.Result.error == 'false' && data.Result.eligible == 'true') {
                    //$('#add_sms_step1 .alert-success').html('Phone number is eligible.');
                    //$('#add_sms_step1 .alert-danger').html('');
                    AddProvisioning();
                } else {
                    $('#add_sms_step1 .alert-success').html('');
                    $('#add_sms_step1 .alert-danger').html(data.Result.status_desc);
                }
            } else {
                $('#add_sms_step1 .alert-success').html('');
                $('#add_sms_step1 .alert-danger').html('Invalid request parameter.');
            }

        },
        error: function (err) {
            // alert(err);
        }
    });
}

function AddProvisioning() {
    var model_Param = {
        username: $('#txtzipwhipnumber').val(),
        accountname: $('#txtaccountname').val(),
        email: $('#txtemailaddress').val(),
    }
    var model = { LocationId: $('#locationid').val(), ProviderConfig: model_Param }

    performAjax({
        url: "/Zipwhip/AddProvisioning",
        type: "POST",
        data: { model: model },
        success: function (data) {
            if (data != null && data.Result != null) {
                if (data.Result.error == 'false' && data.Result.password != "") {
                    $('#btncreateaccount').prop('disabled', true);
                    InsertProviderConfig(data.Result.password);
                } else {
                    $('#add_sms_step1 .alert-success').html('');
                    $('#add_sms_step1 .alert-danger').html(data.Result.status_desc);
                }
            } else {
                $('#add_sms_step1 .alert-success').html('');
                $('#add_sms_step1 .alert-danger').html(data.Result.status_desc);
            }

        },
        error: function (err) {
            // alert(err);
        }
    });

}


function InsertProviderConfig(password) {
    var model = {
        accountname: $('#txtaccountname').val(),
        email: $('#txtemailaddress').val(),
        username: $('#txtzipwhipnumber').val(),
        password: password
    }

    performAjax({
        url: "/Zipwhip/InsertProviderConfig",
        type: "POST",
        data: {
            UserId: $('#hdnloginuser').val(),
            LocationId: $('#locationid').val(),
            obj: model
        },
        success: function (data) {
            if (data.Message == 'Success' && data.Result > 0) {
               // $('#add_sms_step1 .alert-success').html('Account successfully created.');
                $('#add_sms_step1').modal('toggle');
                $.toaster({ priority: 'success', title: 'success', message: 'Your account successfully created.' });
                setTimeout(function () { window.location.reload(); }, 3000);
                

            }
            else {
                $('#add_sms_step1 .alert-danger').html(data.Message);
            }
        },
        error: function (err) {
            // alert(err);
        }
    });
}



function GetZipwhipToken(username, password) {

    performAjax({
        url: "/Zipwhip/GetZipwhipToken",
        type: "POST",
        data: {
            username: username,
            password: password 
        },
        success: function (data) {

        },
        error: function (err) {
            // alert(err);
        }
    });
}


function GetAddressDetail(id) {
    $('#add_sms_step1 .alert-danger').html("");
    $("#locationid option").each(function () {
        var isprimar = $(this).data("id");
        var selectedVal = $(this).val();
        if (isprimar == "1" && selectedVal!= id) {
            $('#add_sms_step1 .alert-danger').html("Please select first primary location.");
            $("#locationid").val("");
        }
    });

    if (id == '') {
        $('#txtzipwhipnumber').val('');
    } else {
        $.ajax({
            url: "/Zipwhip/GetAddressDetail",
            type: "POST",
            data: { Id: id },
            async: true,
            success: function (data) {
                if (data != null) {
                    $('#txtzipwhipnumber').val(data.Phone.replace(/[^0-9]/g, ''));
                }
                else {
                    $('#txtzipwhipnumber').val('');
                }
            }
        });
    }

}


function ClearPopupValue() {
    $('#locationid').val('');
    $('#txtaccountname').val('');
    $('#txtemailaddress').val('');
    $('#txtzipwhipnumber').val('');
    $('#add_sms_step1 .alert-danger').html('');
    $('#add_sms_step1 .alert-success').html('');
}