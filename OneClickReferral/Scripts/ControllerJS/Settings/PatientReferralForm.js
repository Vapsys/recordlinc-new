﻿$(document).ready(function () {
    $(".wizard-inner li").removeClass("active");
    $("#liPatientReferralForm").addClass("active");
    PatientReferralSetting();

});
function PatientReferralSetting() {
    performAjax({
        url: "/Settings/PatientForm",
        type: "POST",
        success: function (data) {
            if (data) {
                $("#Patient").append(data);
                LaodScript();
            }

        }, error: function () {

        }
    });
}
function LaodScript() {
    $(".FinalDiv_P input[type=hidden]").appendTo("#hdnpatfields");
    $(".SectionClass_P").each(function () {
        var name = $(this).find('input[type=checkbox]').data('name');
        if ($(this).find('input[type=checkbox]').is(':checked')) {
            $("#P_Service_" + name).show();
        } else {
            $("#P_Service_" + name).hide();
        }
    });
    $(".itemDiv_P").each(function () {
        var name = $(this).find('input[type=checkbox]').data('id');
        if ($(this).find('input[type=checkbox]').is(':checked')) {
            $("#P_Service_" + name).show();
        } else {
            $("#P_Service_" + name).hide();
        }
    });
}
function ShowRefSettingDataP(id, divid) {
    if ($('#' + id).is(':checked')) {
        $('#' + divid).show();
    } else {
        $('#' + divid).hide();
    }

    if ($('#' + id).is(':checked') == false) {
        $("#" + divid + " .itemDiv_P").each(function () {
            $(this).find('input[type=checkbox]').attr("checked", false);
        });
    }
}
function ShowSubItemDataP(id, divid) {
    if ($('#' + id).is(':checked')) {
        $('#' + divid).show();
    } else {
        $('#' + divid).hide();
    }
    if ($('#' + id).is(':checked') == false) {
        $("#" + divid + " .FinalDiv_P").each(function () {
            $(this).find('input[type=checkbox]').attr("checked", false);
        });
    }
}
function SavePatientReferralFormSettings() {
    var obj = [];
    //Section Object
    $(".SectionClass_P").each(function (index) {
        proc = {
            'SectionId': $(this).find('input[type=checkbox]').data('id'),
            'IsEnable': $(this).find('input[type=checkbox]').is(':checked'),
            'OrderBy': index + 1
        }
        obj.push(proc);
    });

    //Field Object
    var fieldObj = [];
    $(".itemDiv_P").each(function (index) {
        proc = {
            'SectionId': $(this).find('input[type=checkbox]').data('name'),
            'FieldId': $(this).find('input[type=checkbox]').data('id'),
            'IsEnable': $(this).find('input[type=checkbox]').is(':checked'),
            'OrderBy': index + 1
        }
        fieldObj.push(proc);
    });

    //final Section
    var subsectionObj = [];
    $(".FinalDiv_P").each(function (index) {
        proc = {
            'SectionId': $(this).find('input[type=checkbox]').data('name'),
            'ParentFieldId': $(this).find('input[type=checkbox]').data('id'),
            //'SubFieldId': $("#" + $(this).find('input[type=checkbox]').data('hdnid')).val(),
            'SubFieldId': $(this).find('input[type=checkbox]').data('hdnid'),
            'IsEnable': $(this).find('input[type=checkbox]').is(':checked'),
            'OrderBy': index + 1
        }
 
        subsectionObj.push(proc);
    });
    var Obj = {
        'SectionSetting': obj,
        'FieldSettings': fieldObj,
        'SubFieldSettings': subsectionObj
    }
    $.ajax({
        url: "/Settings/SavePatientReferralForm",
        type: "POST",
        data: { _save: Obj },
        success: function () {
        }
    });
}
var isDirty = false;
//$(':input').change(function () {
$(document).on('change', '.new_plan input:checkbox', function () {
    if (!isDirty) {
        isDirty = true;     
    }
});
window.setInterval(function () {
    if (isDirty) {      
        SavePatientReferralFormSettings();
        isDirty = false;
    }
}, 5000);

var unloadEvent = function (e) {    
    if (isDirty) {
         var ua = window.navigator.userAgent;
    var msie = ua.indexOf("Firefox");
    if (msie > -1) {
        (e || window.event).returnValue = SavePatientReferralFormSettings();
    } else {
        return SavePatientReferralFormSettings();
    }
    isDirty = false;
    }
};
window.addEventListener("beforeunload", unloadEvent);