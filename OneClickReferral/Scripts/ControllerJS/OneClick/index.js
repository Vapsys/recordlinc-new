﻿//Get colleague list from 20 miles location from latitude and longitude.
$(document).ready(function () {
    $(".Signout").hide();
    localStorage.clear();

    if (localStorage.getItem("UserId") === null) {
        //$("#divLoading").show();
        searchDentistByLocation(true);
    }
    else {
        redirectToV2();
    }

    //setup before functions
    var typingTimer;                //timer identifier
    var doneTypingInterval = 500;  //time in ms, 5 second for example
    var $input = $('#SearchText');

    //on keyup, start the countdown
    $input.on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    //on keydown, clear the countdown
    $input.on('keydown', function () {
        clearTimeout(typingTimer);
    });

    //user is "finished typing," do something
    function doneTyping() {
        if ($.trim($input.val()).length == 0 || $.trim($input.val()).length >= 3)
            GetSpecificData();
    }
});
function Getdata(srcObj) {
    var $this = $(srcObj);
    var userId = $this.data("userid")
    localStorage.setItem("UserId", $this.data("userid"));
    localStorage.setItem("DocUsername", $.trim($this.data("username")).length > 0 ? $this.data("username") : $this.data("email"));
    performAjax({
        url: INTEGRATION_PARTNER_URL,
        type: "POST",
        data: { "UserId": userId },
        success: function (data) {
            localStorage.setItem("IsIntegration", data);
            if (data == true) {
                window.location = DENTIST_LOGIN_URL;
            }
            else {
                localStorage.removeItem("token");
                window.location = DENTIST_V2_URL;
            }
        }, error: function (err) {

        }
    });
}
function GetSpecificData() {

    var text = $("#SearchText").val();
    if (text == '') {
        searchDentistByLocation();
    }
    else {
        performAjax({
            url: DENTIST_KEYWORD_SEARCH_URL,
            type: "POST",
            data: { Keywords: text },
            success: function (data) {
                renderDentist(data);
            }
        });
    }
}

function searchDentistByLocation(isWhilePageLoaded) {
    navigator.geolocation.getCurrentPosition(

           function (position) {
               var obj = {
                   Latitude: position.coords.latitude,
                   Longitude: position.coords.longitude,
               };
               performAjax({
                   url: DENTIST_LOCATION_SEARCH_URL,
                   type: "POST",
                   data: obj,
                   success: function (data) {
                       renderDentist(data, isWhilePageLoaded);
                   }
               });
           },
            function (error) {
                console.error("Site is not secure with https");
           },{enableHighAccuracy: true, maximumAge: 10000 }
       );
}
function renderDentist(data, isWhilePageLoaded) {
    var $table = $('#DentistRow');
    $table.html('');
    if (data.Result) {
        if (data.Result.length > 0) {
            for (var i = 0; i < data.Result.length; i++) {
                var item = data.Result[i];
                var dentistName = item.FullName || item.FirstName;
                $table.append('<a  href="javascript:;" onclick="Getdata(this)" data-userid = "' + item.EncrypteUserId + '" data-username="' + item.UserName + '" data-email="' + item.Email + '"> <div class="col-sm-4"><div class="panel"><div class="img-box"><img src=' + item.Image + ' alt=' + dentistName + ' style="height:90px;"/></div><div class="detail-box"><h4>' + dentistName + '<br>' + item.AccountName + '</h4></div></div></div></a>');
            }
            $('#notfound').addClass('hide');
        }
        else {
            if (!isWhilePageLoaded) {
                $('#notfound').removeClass('hide');
            }
        }
    }
    else {
        if (!isWhilePageLoaded) {
            $('#notfound').removeClass('hide');
        }
    }
}
function redirectToV2() {
    if (localStorage.getItem("IsIntegration") == "true" && localStorage.getItem("token") != null) {
        window.location = DENTIST_V2_URL + "?UserId=" + encodeURIComponent(localStorage.getItem("UserId"));
    }
    //else if (localStorage.getItem("IsIntegration") == "true" && localStorage.getItem("token") == null) {
    //    window.location = DENTIST_LOGIN_URL;

    //}
    else if (localStorage.getItem("IsIntegration") == "false") {
        window.location = DENTIST_V2_URL;

    }
}