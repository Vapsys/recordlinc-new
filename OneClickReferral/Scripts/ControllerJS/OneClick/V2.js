﻿//setup before functions
var typingTimer;                //timer identifier
var doneTypingInterval = 2000;  //time in ms, 2 second for example
$Search_FirstName = $('#Search_FirstName').val();
var Search_LastName = $('#Search_LastName').val();
var Search_DOB = $('#Search_DOB').val();
var Search_Phone = $('#Search_Phone').val();
var Search_Email = $('#Search_Email').val();

$(function () {
    $(window).scroll(function () {
        $('#Search_DOB').datepicker('hide');
        $('#Search_DOB').trigger('blur');
    });
});

$(window).scroll(function () {
    $(".aligning-right-text").removeClass("btn_sticky");
    //console.log("$(window).scrollTop() : = " + $(window).scrollTop() + ", $(window).height() : = " + $(window).height() + ", $(document).height() : = " + $(document).height())
    if ($(window).scrollTop() + $(window).height() >= parseInt($(document).height() - 280)) {
        //you are at bottom
        $(".aligning-right-text").addClass("btn_sticky");
    }
});


function GetCollegeList() {
    performAjax({
        url: apikey + "OneClickReferral/ColleagueList",
        type: "POST",
        beforeSend: function () {
            $("#divLoading").show();
        },
        data: { EncrypteUserId: User, ProviderType: 0 },
        success: function (data) {
            $("#divLoading").hide();
            $("#HiddenUserID").val(User);
            $("#ddlColleague").html('');
            $.each(data, function (i, value) {
                //$("#ddlColleague").append($("<option></option>").val(value.EncrypteUserId).html(value.FirstName + ' ' + value.UserName));
                $("#ddlColleague").append($("<option></option>").val(value.Encryptid).html(value.Name + ' ' + value.AccountName));
            });

            if ($.trim(localStorage.getItem("ToColleague")).length > 0) {
                $("#ddlColleague").val(localStorage.getItem("ToColleague"));
            } else if ($.trim(localStorage.getItem("ddlColleagueId")).length > 0) {
                $("#ddlColleague").val(localStorage.getItem("ddlColleagueId"));
                localStorage.removeItem('ddlColleagueId');
            }
            if ($('#ddlColleague > option').length >= 1) {
                //GetTeam();
                GetReferralData();
            } else {
                $("#ddlColleague").css("display", "none");
                $("#divLocation").css("display", "none");
                $("#spColleagueExist").css("display", "block");
                $("#upfile").attr("disabled", true);
                $("#savnbtn").attr("disabled", true);
                $("#upfile").css("cursor", "no-drop");
                $("#savnbtn").css("cursor", "no-drop");
            }

        }
    });
}

function GetReferralData() {
    $('#Search_FirstName').val('');
    $('#Search_LastName').val('');
    $('#Search_DOB').val('');
    $('#Search_Email').val('');
    $('#Search_Phone').val('');

    ResetPatientInfo();
    //  GetLocationForDropDown();
    GetSerializedData();
    //No need to call GetAppointment if Doc is Integration Client
    if (localStorage.getItem("IsIntegration") == "true") {
        GetAppointment();
    }
    //GetTeam();


    //setTimeout(function () {
    //    $("#divLoading").hide();
    //}, 4000);
}
function GetSerializedData() {
    performAjax({
        url: "GetReferralFormData?ColleagueId=" + $("#ddlColleague").val(),
        type: "POST",
        data: { ColleagueId: $("#ddlColleague").val() },
        async: true,
        success: function (data) {
            $("#changeUI").html('');
            $("#changeUI").append(data);
            $('#phone').mask("(999) 999-9999");
            $("#SecondaryPhone").mask("(999) 999-9999");
            $("#EmergencyPhone").mask("(999) 999-9999");
            $("#PrimaryInsurancePhone").mask("(999) 999-9999");
            $("#SecondaryInsurancePhone").mask("(999) 999-9999");
            $("#dateofbirth,#PrimaryDateOfBirth,#SecondaryDateOfBirth").datepicker({
                format: 'mm/dd/yyyy',
                startDate: '-120y',
                endDate: '+0d',
                autoclose: true,
            });
            setDateRangeonDOB();
            LoadScripts();
            //$.validator.unobtrusive.parse("#my_form");
            //re-validating form
            var form = $("#my_form")
                        .removeData("validator") /* added by the raw jquery.validate plugin */
                        .removeData("unobtrusiveValidation");  /* added by the jquery unobtrusive plugin*/

            $.validator.unobtrusive.parse(form);

            $(".form-check input[type=hidden]").insertBefore($("#ButtonDivId"));
            $('#phone').click(function () {
                var input = $('#phone');
                input.focus().val(input.val());
            });
            $('#SecondaryPhone').click(function () {
                var input = $('#SecondaryPhone');
                input.focus().val(input.val());
            });
            $('#EmergencyPhone').click(function () {
                var input = $('#EmergencyPhone');
                input.focus().val(input.val());
            });
            $('#PrimaryInsurancePhone').click(function () {
                var input = $('#PrimaryInsurancePhone');
                input.focus().val(input.val());
            });
            $('#SecondaryInsurancePhone').click(function () {
                var input = $('#SecondaryInsurancePhone');
                input.focus().val(input.val());
            });
            $('input[type="checkbox"]').click(function () {
                $("#div_" + $(this).attr("id")).toggle();
            });
            $.validator.addMethod(".endDate", function (value, element) {
                var startDate = $('.startDate').val();
                return Date.parse(startDate) <= Date.parse(value) || value == "";
            }, "* End date must be after start date");
            $('#my_form').validate();
            showPatientlist();
        }
    });
}

function showPatientlist() {
    try {
        if (localStorage.getItem('IsIntegration') == "true") {
            $('#SecPatientList').css("display", "block");
        }

        var val = localStorage.getItem('ToPatientId');
        if (val != "" && val != "0") {
            SelectPatient(parseInt(val));
        }
    }
    catch (err) {
    }
}

$(document).on('click', '.Extract-Slide', function () {
    var toggleInput = $(this).siblings();
    $(toggleInput).toggleClass("Extract-Slide-show");
    $(this).parent().parent().toggleClass('test');
    $(this).parent().toggleClass('ibr');
    $(this).siblings('div').find('input[type=text]').focus();
});
$(".numericOnly").keypress(function () {
    var arr = [0, 8, 9, 16, 17, 20, 35, 36, 37, 38, 39, 40, 45, 46];
    // Allow letters
    for (var i = 48; i <= 57; i++) {
        arr.push(i);
    }
    // Prevent default if not in array
    if (jQuery.inArray(e.which, arr) === -1) {
        e.preventDefault();
        return false;
    }
})
$(document).on("keypress", ".numericOnly", function (e) {
    var arr = [0, 8, 9, 16, 17, 20, 35, 36, 37, 38, 39, 40, 45, 46];
    // Allow letters
    for (var i = 48; i <= 57; i++) {
        arr.push(i);
    }
    // Prevent default if not in array
    if (jQuery.inArray(e.which, arr) === -1) {
        e.preventDefault();
        return false;
    }
});
$(".numericOnly").bind("paste", function (e) {
    return false;
});
$(".numericOnly").bind("drop", function (e) {

    return false;
});

$(".alphaonly").on("keydown", function (event) {
    // Allow controls such as backspace
    var arr = [8, 9, 16, 17, 20, 35, 36, 37, 38, 39, 40, 45, 46, 32];
    // Allow letters
    for (var i = 65; i <= 90; i++) {
        arr.push(i);
    }
    // Prevent default if not in array
    if (jQuery.inArray(event.which, arr) === -1) {
        event.preventDefault();
    }
});

function GetAppointment() {
    $("#divLoading").show();
    if (User == null || User == '') {
        User = localStorage.getItem("UserId")
    }
    performAjax({
        url: "GetAppointment?UserId=" + User,
        type: "Get",
        success: function (data) {
            if (data != null) {
                $('#lstAppointment').html(data);
            }
            else {
                $('#lstAppointment').html('');
            }
            $("#divLoading").hide();
        }
    });

}

function setDateRangeonDOB() {
    var dtToday = new Date();

    var month = dtToday.getMonth() + 1;
    var day = dtToday.getDate();
    var year = dtToday.getFullYear();

    if (month < 10)
        month = '0' + month.toString();
    if (day < 10)
        day = '0' + day.toString();

    var maxDate = year + '-' + month + '-' + day;
    //$('#dateofbirth').attr('max', maxDate);
    //$('#PrimaryDateOfBirth').attr('max', maxDate);
    //$('#SecondaryDateOfBirth').attr('max', maxDate);
}

function GetLocationForDropDown() {
    performAjax({
        url: "GetLocationList?ColleagueId=" + $("#ddlColleague").val(),
        type: "POST",
        async: false,
        success: function (data) {
            if (data.length > 1) {
                $("#ddlLocation").html('');
                // $("#divLocation").show();
                $("#HiddenLocationId").val(data[0].LocationId);
                $.each(data, function (i, value) {
                    $("#ddlLocation").append($("<option></option>").val(value.LocationId).html(value.LocationName));
                });
            }
            else {
                $("#HiddenLocationId").val(parseInt(data[0].LocationId));
                $("#divLocation").hide();
            }
        }
    })
}

function GetTeam() {
    performAjax({
        url: "GetTeamMemberList",
        //  url: apikey + "OneClickReferral/GetTeamMember",
        type: "POST",
        data: { EncrypteUserId: User, ProviderType: 0 },
        async: true,
        success: function (data) {
            $("#teamMember").html('');
            $("#divTeamMember").show();
            $.each(data, function (i, value) {
                $("#teamMember").append($("<option></option>").val(value.DoctorId).html(value.Name).attr('data-id', value.Id));
            });


            //if (data.length > 0) {
            //    $("#teamMember").html('');
            //    $("#divTeamMember").show();
            //    $.each(data, function (i, value) {
            //        $("#teamMember").append($("<option></option>").val(value.UserId).html(value.Name));
            //    });
            //}
            //else {
            //    $("#divTeamMember").hide();
            //}
        }
    });
}

function LoadScripts() {
    //Commented for RM-558
    //$('#dateofbirth,#PrimaryDateOfBirth,#SecondaryDateOfBirth,#firstvistdate,#Dateoflastvisit,#Search_DOB').datepicker({
    //    format: 'mm/dd/yyyy',
    //    todayHighlight: true,
    //    autoclose: true,
    //    todayBtn: "linked",
    //    endDate: '+0d',
    //    "singleDatePicker": true,
    //    "showDropdowns": true,
    //    "autoApply": true
    //});

    $('#firstvistdate,#Dateoflastvisit,#Search_DOB').datepicker({
        format: 'm/d/yyyy',
        todayHighlight: true,
        autoclose: true,
        todayBtn: "linked",
        endDate: '+0d',
        "singleDatePicker": true,
        "showDropdowns": true,
        "autoApply": true
    });

    //Select future date of visit
    $('#Dateofnextvisit').datepicker({
        format: 'm/d/yyyy',
        todayHighlight: true,
        autoclose: true,
        todayBtn: "linked",
        startDate: new Date(),
        "singleDatePicker": true,
        "showDropdowns": true,
        "autoApply": true
    });
}


//on keyup, start the countdown
$('.aptsearch').on('keyup', function () {
    clearTimeout(typingTimer);
    typingTimer = setTimeout(doneTyping, doneTypingInterval);
});

//on keydown, clear the countdown
$('.aptsearch').on('keydown', function () {
    clearTimeout(typingTimer);
});

$('#Search_DOB').on('change', function () {
    doneTyping();
});

//user is "finished typing," do something
function doneTyping() {
    ResetPatientInfo();
    $('#SelectedPatientId').val('');
    $("#divLoading").show();
    var userid = localStorage.getItem("UserId");
    if ($('#Search_FirstName').val() == '' && $('#Search_LastName').val() == '' && $('#Search_DOB').val() == ''
        && $('#Search_Phone').val() == '' && $('#Search_Email').val() == '') {
        performAjax({
            url: "/OneClick/GetAppointment",
            type: "post",
            data: { "UserId": userid },
            async: true,
            success: function (data) {
                $("#divLoading").hide();
                if (data != null) {
                    $('#lstAppointment').html(data);
                }
                else {
                    $('#lstAppointment').html('');
                }
            }
        });
    }
    else {
        performAjax({
            url: "/OneClick/GetPatient",
            type: "post",
            data: $('#my_form').serialize() + "&UserId=" + localStorage.getItem("UserId"),
            async: true,
            success: function (data) {
                $("#divLoading").hide();
                if (data != null) {
                    $('#lstAppointment').html(data);
                }
                else {
                    $('#lstAppointment').html('');
                }
            }, error: function () {

            }
        });
    }
}

function LoadPatientFileWidget(patientId) {
    $.get("/OneClick/LargeFileUploadWidget?patientId=" + patientId, function (data) {
        $("#divPatientFileUploadContainer").html(data);
    });
}

function SelectPatient(PatientId) {
    //box, large file attachment widget
    // LoadPatientFileWidget(PatientId);

    $("#divLoading").show();
    var old_patient = $('#SelectedPatientId').val();
    ResetPatientInfo();
    $('#tblappointment tr').removeClass("active");
    $('#span_' + old_patient).text('Select');
    if (old_patient == PatientId) {
        $('#Select_' + PatientId).removeClass('active');
        $('#span_' + PatientId).text('Select');
        $('#SelectedPatientId').val('');
        $("#divLoading").hide();
        document.getElementById("my_form").reset();
        //ANKIT HERE INSURANCE VERIFY RELATED CHANGES XQ1-447
        //START CODE
        $("#itHasInsuraceData").css('display', 'none');
        $("#itHasNotInsuranceData").css('display', 'none');
        $("#hasInsuredData").val(0);
        //END CODE
        return false;
    }
    else {
        $('#Select_' + PatientId).addClass('active');
        $('#span_' + PatientId).text('Selected');
        $('#SelectedPatientId').val(PatientId);
    }
    performAjax({
        url: "/OneClick/GetPatientDetailById",
        type: "post",
        data: { 'PatientId': PatientId, 'DoctorId': localStorage.getItem("UserId") },
        async: true,
        success: function (data) {
            $("#divLoading").hide();
            if (data != null) {
                //$.trim($('#SelectedPatientId').val(PatientId));
                $('#txtFirstName').val($.trim(data["FirstName"]));
                $('#txtLastName').val($.trim(data["LastName"]));
                $('#txtEmail').val($.trim(data["Email"]));
                $('#phone').val($.trim(data["Phone"]));
                $('#dateofbirth').val($.trim(data["DateOfBirth"]));
                if (data["Gender"] != null && data["Gender"].length > 0) {
                    $('#Gender').val($.trim(data["Gender"]));
                }
                $('#contactInfo_Address').val($.trim(data["ExactAddress"]));
                $('#City').val($.trim(data["City"]));
                $('#State').val($.trim(data["State"]));
                $('#Zip').val($.trim(data["ZipCode"]));
                $('#SecondaryPhone').val($.trim(data["SecondaryPhone"]));
                $('#EMGContactName').val($.trim(data["EmergencyContactName"]));
                $('#EmergencyPhone').val($.trim(data["EmergencyPhoneNo"]));
                if (data["ItHasInsuranceData"] == true) {
                    $("#itHasInsuraceData").css('display', 'block');
                    $("#hasInsuredData").val(1);
                } else {
                    $("#itHasNotInsuranceData").css('display', 'block');
                    $("#hasInsuredData").val(0);
                }
                var patientData = { _dentalhistory: data._dentalhistory, _medicalHistory: data._medicalHistory, _insuranceCoverage: data._insuranceCoverage };
                for (var parentKey in patientData) {
                    if (patientData[parentKey]) {
                        for (var key in patientData[parentKey]) {
                            if (typeof (patientData[parentKey][key]) == 'boolean' && patientData[parentKey][key]) {
                                $("input[name='" + parentKey + "." + key + "']").prop("checked", true);
                                $("#div_" + $("input[name='" + parentKey + "." + key + "']").attr("id")).toggle();
                            }
                            if (typeof (patientData[parentKey][key]) == 'string' && $.trim(patientData[parentKey][key]).length > 0) {
                                $("input[name='" + parentKey + "." + key + "']").val(patientData[parentKey][key]);
                                $("textarea[name='" + parentKey + "." + key + "']").val(patientData[parentKey][key]);
                            }
                        }
                    }
                }
            }
            else {
                $.toaster({ priority: 'danger', title: 'Notice', message: 'Error to get patient detail' });
            }
        }
    });

}
function initFileUpload(ctrl) {
    // Checking whether FormData is available in browser

    if (window.FormData !== undefined) {
        var fileUpload = $(ctrl).get(0);
        var files = fileUpload.files;
        var fileExtension = ['jpg', 'jpeg', 'bmp', 'gif', 'png', 'psd', 'pspimage', 'thm', 'tif', 'yuv', 'pdf', 'doc', 'docx', 'txt', 'xls', 'xlsx', 'zip', 'rar', 'dex', 'dcm'];
        for (var i = 0; i < files.length; i++) {
            if ($.inArray(files[i].name.split('.').pop().toLowerCase(), fileExtension) == -1) {
                //alert(fileExtension.join(', ') + " formats are to be uploaded.");
                $.toaster({ priority: 'warning', title: 'Notice', message: 'Only file(s) with ' + fileExtension.join(', ') + ' extensions are only allowed.' });
                //showErrorMsg("Only file(s) with " + fileExtension.join(', ') + " extensions are only allowed.");
                //$(window).scrollTop(0);
                $(ctrl).val('');
                return false;
            }
        }
        // Create FormData object
        var fileData = new FormData();
        // Looping over all files and add it to FormData object
        for (var i = 0; i < files.length; i++) {
            //XQ1-209 : Validation for Max file
            if (files[i].size > (50 * 1024 * 1024)) {
                $.toaster({ priority: 'warning', title: 'Notice', message: 'Maximum file size is 50 MB only.' });
                return false;
            }
            fileData.append(files[i].name, files[i]);
        }
        fileData.append("DoctorId", localStorage.getItem("UserId"));
        $("#divLoading").show();

        $.ajax({
            url: apikey + "OneClickReferral/UploadFiles?SessionId=" + localStorage.getItem("UserId"),
            type: "POST",
            contentType: false, // Not to set any content header
            processData: false, // Not to process data
            data: fileData,
            success: function (data) {
                if (data) {
                    if ($("#hduploadfiles").val()) {
                        $("#hduploadfiles").val(($("#hduploadfiles").val() + "," + data))
                    }
                    else {
                        $("#hduploadfiles").val(data)
                    }
                    $("#imagesList .attachment-list").empty();
                    BindTeamFileUpload($("#hduploadfiles").val());
                    $(ctrl).val('');
                    $("input[type='file']").replaceWith($("input[type='file']").clone(true));
                    $("#divLoading").hide();
                }
            },
            error: function (err) {
                //   alert(err.statusText);
                //showErrorMsg(err.statusText);
                $.toaster({ priority: 'warning', title: 'Notice', message: err.statusText });
                $("#divLoading").hide();
            }
        });
    } else {
        //alert("FormData is not supported.");
        //showErrorMsg("FormData is not supported.");
        $.toaster({ priority: 'warning', title: 'Notice', message: "FormData is not supported." });
    }
    $("#divLoading").hide();
}

function DeleteAttachmentPopUp(FileId, FileFrom, FilePath, TempFileId) {
    $('#patient_delete').modal('show');
    $('#btn_yes').show();
    $('#btn_no').text('No');
    $('#desc_message').text('Are you sure you want to remove?');
    $('#btn_yes').attr('onclick', ' DeleteAttachMent("' + FileId + '","' + FileFrom + '","' + FilePath + '","' + TempFileId + '")');
}

function DeleteAttachMent(FileId, FileFrom, FilePath, TempFileId) {
    $("#divLoading").show();
    performAjax({
        url: apikey + "OneClickReferral/DeleteAttachedFile?FileId=" + FileId + "&FileFrom=" + FileFrom + "&FileName=" + FilePath,
        type: "POST",
        contentType: false, // Not to set any content header
        processData: false, // Not to process data
        success: function (data) {
            if (data) {
                $('#patient_delete').modal('hide');
                if (TempFileId.indexOf('F') >= 0 || TempFileId.indexOf('I') >= 0) {
                    if ($("#hduploadfiles").val()) { $("#hduploadfiles").val(RemoveValue($("#hduploadfiles").val(), TempFileId)); }
                    if (!$("#hduploadfiles").val()) { $("#dvshowtitle").hide() }
                }
                $('#patient_delete').modal('hide');
                $("#" + TempFileId).remove();
                $("#divLoading").hide();
            }
        },
        error: function (err) {
            $.toaster({ priority: 'danger', title: 'Notice', message: err.statusText });
            $("#divLoading").hide();
        }
    });
}

function RemoveValue(list, value) {
    return list.replace(new RegExp(",?" + value + ",?"), function (match) {
        var first_comma = match.charAt(0) === ',',
            second_comma;
        if (first_comma &&
            (second_comma = match.charAt(match.length - 1) === ',')) {
            return ',';
        }
        return '';
    });
}

function InsertData() {
    $.validator.unobtrusive.parse("#my_form");
    $.validator.addMethod("endDate", function (value, element) {
        var startDate = $('.startDate').val();
        return Date.parse(startDate) <= Date.parse(value) || value == "";
    }, "* End date must be after start date");
    $('#my_form').validate();
    //$("#divLoading").show();
    $("#hdnReceiverId").val($("#ddlColleague").val());
    if ($("#teamMember").length > 0) {
        $("#hdnsenderId").val($("#teamMember").val());
    } else {
        $("#hdnsenderId").val(localStorage.getItem("UserId"));
    }
    if ($("#divLocation").is(':visible')) {
        $("#hdnLocationId").val($("#ddlLocation").val());
    } else {
        $("#hdnLocationId").val($("#HiddenLocationId").val());
    }

    let temId = $("#teamMember :selected").data("id");
    let colId = $("#ddlColleague").val();
    if (temId == colId) {
        $("#spColFromError").css("display", "block");
        setTimeout(function () {
            $("#spColFromError").css("display", "none");
        }, 5000);
        return false;
    }else if ($("#my_form").valid()) {
        $("#my_form").submit();
    }
    else {
        return false;
    }
}

function ResetPatientInfo() {
    $('#txtFirstName').val('');
    $('#txtLastName').val('');
    $('#txtEmail').val('');
    $('#phone').val('');
    $('#dateofbirth').val('');
    $('#Gender').val('0');
    $('#contactInfo_Address').val('');
    $('#City').val('');
    $('#State').val('');
    $('#Zip').val('');
    $('#SecondaryPhone').val('');
    $('#EMGContactName').val('');
    $('#EmergencyPhone').val('');
    $('#SelectedPatientId').val('');
}

$(document).on('keypress', '.aptsearch', function (e) {
    if (e.which == 13) e.preventDefault();
});
      
select2Dropdown('colleagueList', 'Search Colleague', 'OneClick', 'GetColleagueList', false);
function select2Dropdown(Select2ID, ph, Controller, listAction, isMultiple) {
    var sid = '.' + Select2ID;
    $(sid).select2({
        placeholder: ph,
        minimumInputLength: 1,
        multiple: isMultiple,
        ajax: {
            url: "/" + Controller + "/" + listAction,
            dataType: 'json',
            delay: 550,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            error: function (response, status, xhr) {
                if (response.status === 403) {
                    SessionExpire(response.responseText);
                }
            }
        }
    });    
}


