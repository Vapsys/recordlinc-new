﻿//DOCUMENT READY
$(document).ready(function () {
    PatientList(true);
});


function getInsuranceDataDetails(strId) {        
    performAjax({
        url: "/Patients/PatientInsuranceDataDetails",
        type: "GET",
        data: { Id: strId },
        async: true,
        success: function (data) {
            $("#dvInsurancePopup").html(data);
            $("#dvInsurancePopup").modal('show');            
        }
    });
}


//Get Patient List while page lode, sortColumn, Location filter time.
function PatientList(isFirst) {
    var obj = {
        'PageIndex': $("#hdnPageIndex").val(),
        'PageSize': $("#hdnPageSize").val(),
        'SearchText': $.trim(($("#txtSearchPatients").val() != '' ? $("#txtSearchPatients").val() : $("#txtMobileSearchPatients").val())),
        'SortColumn': $("#hdnSortColumn").val(),
        'SortDirection': $("#hdnSortDirection").val(),
        'FilterBy': 2,
        'LocationBy': $("#LocationBy").find(':selected').attr('data-id'),
    }
    performAjax({
        url: "/Patients/GetPatients",
        type: "POST",
        data: { patientFilter: obj },
        success: function (data) {
            $("#AppendList").html('');
            $("#AppendList").html(data);
            // Move this condition botton bcoz nomore not support in IE  XQ1-818 
            //if (data.includes("nomore")) {
            //    $("#hdnPageIndex").val(-1);
            //}
            if (isFirst) {
                $("#count").html($("#hdnTotalCount").val());
                $("#mobileCount").html($("#hdnTotalCount").val());
                $("#hdnPatientCount").val($("#modelcount").val());
            } else {
                $("#count").html($("#modelcount").val());
                $("#mobileCount").html($("#modelcount").val());
                $("#hdnPatientCount").val($("#modelcount").val());
            }          
            jcf.replaceAll();
            if (data.includes("nomore")) {
                $("#hdnPageIndex").val(-1);
            }
        },
        error: function () {

        }
    });
}

$('body').removeClass('modal-open');
//$('a[data-toggle="tooltip"]').tooltip({
//    container: 'body',
//    trigger: 'hover'
//});

function onChangePatientAction(Id)
{
    var ddlId = "ddlPatient_" + Id;
    if ($("#" + ddlId).val() == 1) {
        SendForms($("#" + ddlId + " :selected").data("patientid"), $("#" + ddlId + " :selected").data("email"), $("#" + ddlId + " :selected").data("name"))
    } else if ($("#" + ddlId).val() == 2) {
        OpenDeletePopUp($("#" + ddlId + " :selected").data("patientid"));
    } else if ($("#" + ddlId).val() == 3) {
        window.location.href = ($("#" + ddlId + " :selected").data("url"));
       // Redirect($("#" + ddlId + " :selected").data("url"));
    } else {
        return false;
    }

    $("#" + ddlId).val("0");
}

//CheckBox logic to select all check-box
$(".checkedAll").change(function () {
    if (this.checked) {
        $(".checkSingle").each(function () {
            $(this).prop('checked', true);
            $(".all-select span").removeClass('jcf-unchecked').addClass('jcf-checked');
        })
    } else {
        $(".checkSingle").each(function () {
            $(this).prop('checked', false);
            $(".all-select span").removeClass('jcf-checked jcf-focus').addClass('jcf-unchecked');
        })
    }
});

//Bind Scroll
$(function () {
    var bindScrollHandler = function () {
        var win_hg = ($(window).height()) * 10 / 100;
        $(window).scroll(function () {
            //console.log('$(window).scrollTop():- ' + $(window).scrollTop() + '$(document).height():- ' + $(document).height() + ' $(window).height():- ' + $(window).height() + ' win_hg:- ' + win_hg);
            if (parseInt($(window).scrollTop()) >= parseInt($(document).height() - $(window).height())) {
                var PageIndex = $("#HdnPageIndex").val();
                if (parseInt(PageIndex) == -1) {
                    $(window).unbind("scroll");
                    $('#divLoading').hide();
                }
                if (parseInt(PageIndex) != -1 && $("#AppendList #nomore").length == 0) {
                    //$(window).scrollTop($(window).scrollTop() - parseInt($(window).scrollTop() / 2))
                    GetPatientListOnScroll();
                }
            }
            else {

            }
        });
    };
    bindScrollHandler();
});

//Get Patient List while scroll the page.
function GetPatientListOnScroll() {
    var pageindex = $("#hdnPageIndex").val();
    if (pageindex == -1) {
        return false;
    } else {
        pageindex = parseInt(pageindex) + parseInt(1);
    }
    var obj = {
        'PageIndex': pageindex,
        'PageSize': $("#hdnPageSize").val(),
        'SearchText': $.trim($("#txtSearchPatients").val()),
        'SortColumn': $("#hdnSortColumn").val(),
        'SortDirection': $("#hdnSortDirection").val(),
        'FilterBy': 2,
        'LocationBy': $("#LocationBy").find(':selected').attr('data-id'),
    }
    performAjax({
        url: "/Patients/GetPatients",
        type: "POST",
        data: { patientFilter: obj },
        success: function (data) {
            var html = $("#AppendList").html();
            var current = data;
            if (data != null && data != "" && html.indexOf("No more record found") < 0) {
                $("#AppendList").append(data);
                $("#hdnPageIndex").val(parseInt(pageindex));
                var PrePatientCount = $("#hdnPatientCount").val();
                var CurrentRecordGet = $("#modelcount").val();
                if (data.indexOf("No more record found") < 0) {
                    $("#count").html(parseInt(PrePatientCount) + parseInt(CurrentRecordGet));
                    $("#hdnPatientCount").val(parseInt(PrePatientCount) + parseInt(CurrentRecordGet));
                }
                jcf.replaceAll();
            } else {
                $("#hdnPageIndex").val(parseInt(-1));
                return false;
            }
        },
        error: function (err) {
            alert(err);
        }
    });
}

//Sort Column and Direction common logic.
function SortColumn(ColId) {
    var SortCol = $("#" + ColId).attr('sort-col');
    var Sortdir = $("#" + ColId).attr('sort-dir');
    $("#hdnSortColumn").val(SortCol);
    $("#hdnSortDirection").val(Sortdir);
    if (Sortdir == 1) {
        $("#" + ColId).attr('sort-dir', 2);
    } else {
        $("#" + ColId).attr('sort-dir', 1);
    }
    var pagesize = $("#hdnPageSize").val();

    var pageindex = $("#hdnPageIndex").val();

    //RM-194 changes for page index.
    if (pageindex == -1) {
        return false;
    }
    else {
        $("#hdnPageIndex").val(1);
    }
    PatientList();
}

//Location Filter Logic
$("#LocationBy").on('change', function () {
    $("#hdnPageIndex").val(1);
    $('#txtSearchPatients').val('');
    PatientList();
})

function DeleteSelectedPatiets() {
    debugger;
    var userlist = []; var cnt = 0;
    $(".checkSingle").each(function () {
        if ($(this).prop('checked')) {
            cnt++;
            userlist.push($(this).val());
        }
    });
    if (userlist != null && userlist != "") {
        $('#patient_delete').modal('show');
        $('#btn_yes').show();
        $('#btn_no').text('No');
        $('#desc_message').text('Are you sure you want to Delete Patients?');
        $('#btn_yes').attr('onclick', ' DeleteSeletedpatient("' + userlist + '","' + cnt + '")');
    }
    else {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select at least one patient' });
        $('#btnMultipleDelete').attr('disabled', true);
        setTimeout(function () {
            $('#btnMultipleDelete').attr('disabled', false);
        }, 4000);
        return false;
    }
}
function UncheckMaincheckBox(id) {
    $(".checkedAll").parent('span').removeClass('jcf-checked').addClass('jcf-unchecked');
    $(".checkedAll").prop("checked", false);
}
function DeleteSeletedpatient(selected, cnt) {
    var Obj = {
        'MultiplePatientId': selected,
    };
    $.ajax({
        url: "/Patients/DeletePatient",
        type: "post",
        data: { delete: Obj },
        success: function (data) {
            var removed = data.RemovedCount;
            var unremoved = data.UnRemovedCount;
            if (removed > 0) {
                $('#desc_message').text('Patient removed successfully');
                $('#btn_yes').hide();
                $('#btn_no').html('Ok');
                $('.modal-footer').css('text-align', 'center');
                for (var i = 0; i < selected.length; i++) {
                    if (cnt == 1) {
                        $('#row_' + selected).remove();
                    } else {
                        $('#row_' + selected[i]).remove();
                    }
                }
                var MyCount = $("#count").text();
                var NewCount = parseInt(MyCount) - parseInt(cnt);
                $("#count").text(NewCount);
                cnt = 0;
                if ($("#MainCheckBox").prop("checked")) {
                    $("#MainCheckBox").checked = false;
                    $("input[type=checkbox]").attr('checked', false);
                }
            }
            else if (unremoved > 0) {
                if (unremoved == 1) {
                    $('#patient_delete').modal('hide');
                    $.toaster({ priority: 'warning', title: 'Notice', message: 'This patient is associated with another dentist.' });
                }
                else {
                    $('#patient_delete').modal('hide');
                    $.toaster({ priority: 'warning', title: 'Notice', message: 'These patients are associated with another dentist.' });
                }
            }
            else {
                $('#desc_message').text('Failed to remove patient.');
            }
        }, error: function (err) {
            alert(err);
        }
    });
}
function OpenDeletePopUp(id) {
    $('#patient_delete').modal('show');
    $('#btn_yes').show();
    $('#btn_no').html('No');
    $('.modal-footer').css('text-align', 'right');
    $('#desc_message').text('Are you sure you want to Delete Patient?');
    //$('#btn_yes').attr('onclick', ' DeletePatient("' + id + '","' + PageIndex + '")');
    $('#btn_yes').attr('onclick', ' DeleteSeletedpatient("' + id + '","' + 1 + '")');
}
function SendForms(PatientId, Email, Name) {
    if (Email != "" && Email != null) {
        var Obj = {
            'PatientId': PatientId,
            'PatientName': Name,
            'PatientEmail': Email
        };
        $.ajax({
            url: '/Patients/SendForm',
            type: 'post',
            data: { Obj: Obj },
            success: function (data) {
                if (data == true) {
                    $.toaster({ priority: 'success', title: 'Success', message: 'Patient Form sent successfully.' });
                }
                else {
                    $.toaster({ priority: 'danger', title: 'Failed', message: 'Failed to sent the Patient form.' });
                }
            }
        });
    } else {
        $.toaster({ priority: 'info', title: 'Notice', message: "Sorry, This Patient have not Email address. So we are unable to send Patient form!" });
    }

}

function HdrSearchPatient(event) {
    if (event.keyCode == 13) {
        var message = $('#txtSearchPatients').val();
        if (message != null && message != "") {
            SearchPatient();
        }
    }
}

//Search Colleague
function SearchPatient() {
    var obj = {
        'PageIndex': 1,
        'PageSize': 50,
        'SearchText': $.trim(($("#txtSearchPatients").val() != '' ? $("#txtSearchPatients").val() : $("#txtMobileSearchPatients").val())),
        'SortColumn': 2,
        'SortDirection': 1,
        'FilterBy': 2,
        'LocationBy': $("#LocationBy").find(':selected').attr('data-id'),
    }
    performAjax({
        url: "/Patients/GetPatients",
        type: "POST",
        data: { patientFilter: obj },
        success: function (data) {
            $("#hdnPageSize").val(parseInt(50));
            $("#hdnSortColumn").val(parseInt(2));
            $("#hdnSortDirection").val(parseInt(1));

            $("#AppendList").html('');
            $("#AppendList").html(data);
            $("#count").html($("#modelcount").val());
            if (data.includes("#nomore")) {
                $("#hdnPageIndex").val(-1);
                $("#count").html(0);
            } else {
                $("#hdnPageIndex").val(parseInt(1));
            }
            jcf.replaceAll();
        },
        error: function (err) {
            alert(err);
        }
    });
}