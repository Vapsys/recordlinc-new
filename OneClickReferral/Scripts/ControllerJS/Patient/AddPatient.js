﻿function GotoBack() {
    if (document.referrer.indexOf("PatientHistory") >= 0) {
        $("#hdnViewPatientHistory").val(PatientId);
        $("#frmViewPatientHistory").submit();
    }
    else {
        history.back();
    }
}
$("#ColleagueId").on('change', function () {
    $('#ReferredBy').val($(this).val());
});
$("#LocationId").on('change', function () {
    $('#Location').val($(this).val());
});
$("#State").on('change', function () {
    $('#sta').val($(this).val());
});
$('#gender li > a').click(function (e) {
    var pId = $(this).attr('data-id')
    $('#genbtn').text(this.innerHTML);
    $('#gen').val(pId);

});
$('body').removeClass('modal-open');
$('#Phone').mask("(999) 999-9999");

function checkDate() {
    var selectedText = $('#FirstExamDate').val();
    var selectedDate = new Date(selectedText);
    var DobText = $('#DateOfBirth').val();
    var DobDate = new Date(DobText);
    if (selectedDate < DobDate) {
        $("#errormsg").show();
        return false;
    }
    else {
        $("#errormsg").hide();

    }
}
function InsertPatient() {
    if ($('#DateOfBirth').val() != null && $('#DateOfBirth').val() != "") {
        if (!isDate($('#DateOfBirth').val())) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter valid Birth Date.' });
            $('#DateOfBirth').focus();
            return false;
        }
    }
    if ($('#FirstExamDate').val() != null && $('#FirstExamDate').val() != "") {
        if (!isDate($('#FirstExamDate').val())) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter valid First Exam Date.' });
            $('#FirstExamDate').focus();
            return false;
        }
    } 

    if ($('#FirstName').val() != null && $('#FirstName').val() != "") {
        if ($('#FirstName').val().toString().length > 20) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'First Name allow maximum 20 characters only.' });
            $('#FirstName').focus();
            return false;
        }
    }
    if ($('#LastName').val() != null && $('#LastName').val() != "") {
        if ($('#LastName').val().toString().length > 20) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Last Name allow maximum 20 characters only.' });
            $('#LastName').focus();
            return false;
        }
    }
    if ($('#Email').val() != null && $('#Email').val() != "") {
        if ($('#Email').val().toString().length > 50) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Email allow maximum 50 characters only.' });
            $('#Email').focus();
            return false;
        }
    }

    if (!$("#frmAddPatient").valid()) {
        $(window).scrollTop(0);
        $('#Gender').removeClass('input-validation-error');
    }
    //if ($("#frmAddPatient").valid()) {
    else {
        var selectedText = $('#FirstExamDate').val();
        var selectedDate = new Date(selectedText);
        var DobText = $('#DateOfBirth').val();
        var DobDate = new Date(DobText);
        if (selectedDate < DobDate) {
            $("#errormsg").show();
            return false;
        }
        else {
            $("#errormsg").hide();
            var Obj = {
                'PatientEmail': $("#Email").val(),
                'AssignedPatientId': $("#AssignedPatientId").val(),
                'OldPatientEmail': $("#hdnOldEmail").val(),
                'oldAssignedPatientId': $("#hdnOldAssignedPatientId").val(),
                'PatientId': PatientId
            }
            $.post("/Patients/CheckPatientsEmailIdandUniqeId", { check: Obj },
                function (data) {
                    //if (data.PatientEmail == "0" && data.AssignedPatientId == "0") {
                    //    $.toaster({ priority: 'warning', title: 'wait', message: 'Email and Patient ID is exists please try another.' });
                    //    return false;
                    //}
                    //else if (data.PatientEmail == "0") {
                    //    if ($("#Email").val() != "") {
                    //        $.toaster({ priority: 'warning', title: 'wait', message: 'Email address is exists please try another.' });
                    //        return false;
                    //    }
                    //}
                    //else if (data.AssignedPatientId == "0") {
                    //    if ($("#AssignedPatientId").val() != "") {
                    //        $.toaster({ priority: 'warning', title: 'wait', message: 'Patient Id is already assigned to ' + data.PatientName + '. Please enter a different Patient Id.' });
                    //        return false;
                    //    }
                    //}
                    //else {

                        var check = PatientId;
                        //if (data.PatientEmail == "1" && data.AssignedPatientId == "1") {
                            if (check == 0) {
                                var Firstname = $('#FirstName').val();
                                var LastName = $('#LastName').val();
                                if (Firstname != "" && LastName != "") {
                                    $.toaster({ priority: 'success', title: 'Success', message: 'Patient added successfully' });
                                }
                            }
                            else {
                                var Firstname = $('#FirstName').val();
                                var LastName = $('#LastName').val();
                                if (Firstname != "" && LastName != "") {
                                    $.toaster({ priority: 'success', title: 'Success', message: 'Patient details updated successfully' });
                                }
                            }
                            setTimeout(function () {
                                $("#frmAddPatient").submit();
                            }, 3000)

                        //}
                    //}
                });

        }
    }
}
//$(function () {
//    if (PatientId == 0) {
//        $.post("/Common/CheckAuthorizationByFeature",
//                                   { FeatureId: FeatureId }, function (data) {
//                                       if (data == true) {

//                                       } else {
//                                           $("#upgradeProfile").html(data)
//                                       }
//                                   });
//        $(document).on('click', '.mfp-close', function () {
//            location.href='/Patients'
//        });
//    }
//});