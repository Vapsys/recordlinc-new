﻿var GLOB_PATIENT_VALUES = {};
function crop_reset() {
    $("#addLogo").modal('show');
    $("#fuImage").val(null);
}
function cropremove_reset() {     
    $("#addLogo").modal('show');
    $("#fuImage").val(null);
}

$("#closelogo").click(function(){
    $("#cropDisplay").text('');
    $("#txtimage").text('');
    $('#addLogo').modal('hide');
});

$("#closelogos").click(function(){
    $("#cropDisplay").text('');
    $("#txtimage").text('');
    $('#addLogo').modal('hide');
});
$('body').removeClass('modal-open');

$(".planing").click(function() {
    $('html,body').animate({
        scrollTop: $("#planes").offset().top}, 2000);
});   
$(".patinet_form").click(function() {
    $('html,body').animate({
        scrollTop: $(".pateint_form_history").offset().top}
    , 2000);
});
$(".book_appiontment").click(function() {
    $('html,body').animate({
        scrollTop: $(".book_appiontment_new").offset().top}
    , 2000);
});
function show() {
    document.getElementById("dThreshold").style.display = "block";
}

$(document).ready(function () {
    if ($("#tblCommunicationListing tr").length == 0) {
        $('#_printCommunication').attr('disabled', true);
        $('#_printCommunication').removeAttr("href");
    }

    GetPatientDocuments();
    GetPatientImages();
    $('#patientHistoryscrollId').scroll(function () {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            var Pageindex = $("#PageIndexCommPatientHistory").val();
            if (Pageindex != 0) {
                var PatientId = $("#hdnPatientId").val();
                var SortCol = $("#CurrentSortColCommPatientHistory").val();
                var SortDir = $("#PatientHCol_" + SortCol).attr('sort-dir');
                $("#PageIndexCommPatientHistory").val(parseInt(Pageindex, 10) + 1);
                LoadPatientscrolling(PatientId, Pageindex, SortCol, SortDir);
            }
        }
    });
    $(".datepick").keypress(function(event) {event.preventDefault();})
    $('.datepick').datepicker({
        format: 'm/d/yyyy',
        todayHighlight: true,
        autoclose: true,
        todayBtn: "linked",
        endDate: '+0d',
        startDate: '01/01/1900',
        "singleDatePicker": true,
        "showDropdowns": true,
        "autoApply": true,
        showOn: 'both'
    }).on("changeDate", function(e) {
        CallEditableMethod();
        $('#dob_text').hide();
    });
    $(".datepickNext").keypress(function(event) {event.preventDefault();})
    $('.datepickNext').datepicker({
        format: 'm/d/yyyy',
        todayHighlight: true,
        autoclose: true,
        todayBtn: "linked",
        //endDate: '',
        startDate: '+0d',
        "singleDatePicker": true,
        "showDropdowns": true,
        "autoApply": true,
        showOn: 'both'
    }).on("changeDate", function(e) {
        CallEditableMethod();
        $('#dob_text').hide();
    });
    $('#update_phoneId').mask("(999) 999-9999");
    $(".patient-history :input").not('.datepick').blur(function (event) {
        if(this.value != this.defaultValue) {
            CallEditableMethod();
        }
    });

    function revertToOriginalURL() {
        var original = window.location.href.substr(0, window.location.href.indexOf('#'))
        history.replaceState({}, document.title, original);
    }

    $('.modal').on('hidden.bs.modal', function () {
        revertToOriginalURL();
    });

    $('.modal').on('shown.bs.modal', function () {
        setTimeout(function () {
            revertToOriginalURL();
        }, 10);
    });

    $('#id').click( function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('#id_text').toggle();
        $("#update_AsspId").addClass("numericOnly");
        if($('#phAssPID').text() == 'Please enter id')
        {
            $('#update_AsspId').val();
            $('#phAssPID').text('');
        }
        else
        {
            $('#update_AsspId').val($('#phAssPID').text());
        }

    });
    $('#id_text').click( function(e) {
        e.stopPropagation();
    });

    $('#gender').click( function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('#gender_text').toggle();
        $('#update_genderId').val($('#phGenderID').text().toLowerCase() == "female" ? 1 : 0);
    });
    $('#gender_text').click( function(e) {
        e.stopPropagation();
    });

    $('#dob').click( function(e) {
        debugger;
        e.preventDefault();
        e.stopPropagation();
        $('#dob_text').toggle();
        if('@Model.DateOfBirth' == '')
        {
            //$('#update_dobId').val('');
            $('input[name="date"]').mask('99/99/9999', { placeholder: "mm/dd/yyyy" });
            $('#phDobID').hide();
        }          
    });
    $('#dob_text').click( function(e) {
        e.stopPropagation();
    });

    $('#phone').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#phone_text').toggle();
        if($('#phPhoneID').text() == 'Please enter phone number')
        {
            $('#update_phoneId').val();
            $('#phPhoneID').text('');
        }
        else
        {
            $('#update_phoneId').val($('#phPhoneID').text());
        }

    });
    $('#phone_text').click(function (e) {
        e.stopPropagation();
    });
    //Editable address  for address field.
    $('#address').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#address_text').toggle();
        if ($('#phAddressId').text() == 'Please enter address')
        {
            $('#update_addressId').val();
            $('#phAddressId').hide();
        }
        else
        {
            $('#update_addressId').val($.trim($('#phAddressId').text()));
            $('#phAddressId').hide();
        }

    });
    $('#address_text').click(function (e) {
        e.stopPropagation();
    });

    //Editable city field

    $('#city').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#city_text').toggle();
        if ($('#phCityId').text() == 'Please enter city') {
            $('#update_cityId').val();
            $('#phCityId').hide();
        }
        else {
            $('#update_cityId').val($.trim($('#phCityId').text()));             
        }

    });
    $('#city_text').click(function (e) {
        e.stopPropagation();
    });


    //Editable zip field
    $('#zip').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#zip_text').toggle();
        if ($('#phZipId').text() == 'Please enter zip') {
            $('#update_zipId').val();
            $('#phZipId').hide();
        }
        else {
            $('#update_zipId').val($.trim($('#phZipId').text()));                
        }

    });
    $('#zip_text').click(function (e) {
        e.stopPropagation();
    });


    //Editable state for state field
    $('#state').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#drpstate').show();
        $('#state_text').toggle();
        if ($('#drpstate :selected').val() == '0') {
            $('#drpstate').val();
            $('#phStateId').hide();
        }           
    });
    $('#state_text').click(function (e) {
        e.stopPropagation();
    });
    $('#email').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#email_text').toggle();
        if($("#phEmailID").text() == 'Please enter email id')
        {
            $('#update_emailId').val();
            $("#phEmailID").hide();
        }
        else
        {
            $('#phEmailID').hide();

        }
    });
    $('#email_text').click(function (e) {
        e.stopPropagation();
    });

    $('body').click(function () {
        $('#id_text').hide();
        $('#gender_text').hide();
        $('#email_text').hide();
        $('#phone_text').hide();
        $('#address_text').hide();
        $('#city_text').hide();
        $('#zip_text').hide();
        $('#drpstate').hide();

        $('#phEmailID').show();
        $('#phDobID').show();
        $('#phAddressId').show();
        $('#phCityId').show();
        $('#phZipId').show();
        $('#phStateId').show();

        if('@Model.DateOfBirth' == "") {
            $('#phDobID').text('Please enter DOB');
        }
        if('@Model.Phone' == "") {
            $('#phPhoneID').text('Please enter phone number');
        }
        if('@Model.Email' == "") {
            $("#phEmailID").text('Please enter email id');
        }
        if('@Model.AssignedPatientId' == "") {
            $('#phAssPID').text('Please enter id');
        }
        if('@Model.ExactAddress' == "") {
            $('#phAddressId').text('Please enter address');
        }
        if('@Model.City' == "") {
            $('#phCityId').text('Please enter city');
        }
        if('@Model.ZipCode' == "") {
            $('#phZipId').text('Please enter zip');
        }

        if('@Model.State' == "") {
            $('#phStateId').text('Please enter state');
        }
    });

    GLOB_PATIENT_VALUES = $("#frmPatientHistory").serializeObject();

    $("input[id^='tab'][id$='_checkbox']").each(function() {
        var $selector = $(this).attr("id");
        $selector = $selector.replace("_checkbox","");
        if($(this).is(":checked"))
            $("#" + $selector + "_link").trigger("click");
    });

    $("input[id^='tab'][id$='_checkbox']").change(function() {
        var $selector = $(this).attr("id");
        $selector = $selector.replace("_checkbox","");
        $("#" + $selector + "_link").trigger("click");
    });
});
function OpenFamilyPopup(pID)
{

    if(pID == '' || pID == undefined)
    {
        pID = '@Model.PatientId';
    }
    var PatientId = pID;
    $("#search_listing_pageindex").val('1');
    var GetSearchText = null;
    if ($("#txtSearchPatient").val() == null) {
        GetSearchText = null;
    } else if ($('#txtSearchPatient').length == 0) {
        GetSearchText = null;
    }
    else {
        if ($("#txtSearchPatient").val() == "Find patient to add as family member..." || $("#txtSearchPatient").val() == "Find colleague to refer to...") {
            GetSearchText = null;
        }
        else {
            GetSearchText = $("#txtSearchPatient").val();
        }
    }
    $.post('@Url.Action("SearchPatientFromPopUp", "Patients")', { SearchText: GetSearchText, PatientId: PatientId }, function (data) {
        $("#AddFamilyMembers").modal('show');
        if (data == 'no_record')
        {
            $("#search_listing").html('');
            $("#search_listing").append('<center><p style="letter-spacing: 0px;">No Record Found.</p></center>');
        }
        else
        {
            $("#AddFamilyMembers").html(data);
        }
        $(".list-holder").on('scroll', function (event) {
            if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                var data = SearchPatientLI().toString();
                $("#search_listing").append(data);
            }
        });
    });
}

function GotoBack() {
    history.back();
}

function isValidEmailAddress(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    return pattern.test(emailAddress);
}

function OpenReferPatientPopup(id) {
    $("#search_listing_pageindex").val('1');
    var GetSearchText = null;
    if ($("#txtSearchPatient").val() == null) {
        GetSearchText = null;
    } else if ($('#txtSearchPatient').length == 0) {
        GetSearchText = null;
    }
    else {
        if ($("#txtSearchPatient").val() == "Find patient to add as family member..." || $("#txtSearchPatient").val() == "Find colleague to refer to...") {
            GetSearchText = null;
        }
        else {
            GetSearchText = $("#txtSearchPatient").val();
        }
    }
    $.post("/Patients/GetColleagueListForReferralPopup", { searchtext: "" }, function (data) {
        if (data != "") {
            $("#colleague_refer_patient").modal('show');
            $("#colleague_refer_patient").html(data);
        }
    })
}
function OpenSendReferralPopup(pid) {
    //var PageIndex = $("#ColleaguPageIndex").val();
    var PatientId = pid;
    $('#hdnPatientId').val(pid);
    var GetSearchText = null;
    if ($("#txtSearchColleague").val() == null) {
        GetSearchText = null;
    } else if ($('#txtSearchColleague').length == 0) {
        GetSearchText = null;
    }
    else {
        if ($('txtSearchColleague').val() == 'Find colleague to refer to...') {
            GetSearchText = null;
        }
        else {
            GetSearchText = $('txtSearchColleague').val();
        }
    }
    $.ajax({
        url: '/Patients/NewGetListSearchColleagueFromPopUp',
        type: 'post',
        data: { SearchText: GetSearchText },
        success: function (data) {

            $("#colleague_refer_patient").modal('show');
            $("#colleague_refer_patient").html(data);
            $("#btn_send_pat").hide();
            $("#btn_send_patient").show();
            $(".list-holder").bind('scroll', function (event) {
                if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                    var data = GetColleaguesByScroll().toString();
                    if (data.indexOf('Colleague not found') > -1) {
                        $('.referral-list').append(data);
                        $("#ColleaguPageIndex").val("-1");
                    }
                    else {
                        $('.referral-list').append(data);
                        $("#ColleaguPageIndex").val(PageIndex);
                    }
                }
            });
        }
    })
}
function GetColleaguesByScroll() {
    var datalist = '';
    $.ajaxSetup({ async: false });
    var PageIndex = $("#ColleaguPageIndex").val();
    var SearchText = $("#txtSearchPatient").val();
    if (PageIndex == "-1") {
        return "";
    } else {
        PageIndex = parseInt(PageIndex) + parseInt(1);
        $.ajax({
            url: '/Patients/NewGetColleagueListForSendReferralPopUpOnScroll',
            type: 'post',
            data: { PageIndex: PageIndex, SearchText: SearchText },
            success: function (data) {
                if (data == '' || data.trim() == '<li><center>No more record found.</center></li>') {
                    $("#ColleaguPageIndex").val("-1");
                }
                else {
                    $("#ColleaguPageIndex").val(PageIndex);
                }
                datalist = data;
            }
        });
    }
    $.ajaxSetup({ async: true });
    return datalist;
}
var timer;
function SearchColleagueOnkeyPress() {
    var ctrl = $("#txtSearchColleague").val();
    clearTimeout(timer);
    var ms = 500; // milliseconds
    timer = setTimeout(function () {
        Onkeypresscolleaguelist(ctrl);
    }, ms);
}
//Bind Colleagues on scroll.
function Onkeypresscolleaguelist(ctrl) {
    var value = ctrl.toLowerCase().trim();
    if ($.trim(value).length == 0) {
        value = "";
    }
    $('#divLoading').show();
    $.ajaxSetup({ async: false });
    $.ajax({
        url: "/Patients/NewSearchColleagueFromPopUp",
        type: 'post',
        data: { SearchText: value },
        success: function (data) {
            $('#divLoading').hide();
            $('.referral-list').html('');
            $('.referral-list').append(data);
        }
    });

    $.ajaxSetup({ async: true });
    $(".list-holder").bind('scroll', function (event) {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            var data = GetColleaguesByScroll().toString();
            $(this).append(data);
        }
    });
}

function SearchColleague() {
    $("#ColleaguPageIndex").val("1");
    var GetSearchText = null;
    if ($('#txtSearchColleague').length == 0) {
        GetSearchText = null;
    } else {
        GetSearchText = document.getElementById('txtSearchColleague').value;
        if (GetSearchText == "Find colleague to refer to...") {
            GetSearchText = null;
        }
    }
    $.ajaxSetup({ async: false });
    $.ajax({
        url: "/Patients/NewGetListSearchColleagueFromPopUp",
        type: 'post',
        data: { SearchText: GetSearchText },
        success: function (data) {
            $('.referral-list').append(data);
        }
    })

    $.ajaxSetup({ async: true });
    $(".list-holder").bind('scroll', function (event) {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            var data = GetColleaguesByScroll().toString();
            $(this).append(data);
        }
    });
}
function SelectColleague(cid) {

    $(".referral-list > li").each(function () { $(this).removeClass('active'); });
    if ($("." + cid).hasClass('active')) {
        $("." + cid).removeClass('active');
    } else {
        $("." + cid).addClass('active');
        $('#btn_send').attr("disabled", false);
        $("#hdnColleagueId").val(cid);
    }
}
function FinalReferralSend() {

    var PatientId = $('#hdnPatientId').val();
    var ColleagueId = $("#hdnColleagueId").val();
    var LocationId = $("#hdnlocationid").val();
    if (ColleagueId == null || ColleagueId == "") {
        $("#NewRefferalPopUp").modal('show');
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select at least one colleague.' });
        //$('#btn_send').attr("disabled", true);
        ToggleSaveButton('btn_send_pat');
    }
    else if (LocationId == null || LocationId == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select at least one location.' });
        ToggleSaveButton('btn_send_pat');
    }
    else {
        window.location.href = '/Referrals/Referral?PatientId=' + PatientId + '&ColleagueIds=' + ColleagueId + '&AddressInfoId=' + LocationId;
    }
}

//Select Location Code
function SelectLocationForReferral() {
    var colleaguid = $("#hdnColleagueId").val();
    if (colleaguid == null || colleaguid == "") {
        //Jquery Error for toster
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select at least one Colleague.' });
        ToggleSaveButton("btn_send_Location");
        return false;
    }
    else {
        $('.referral-list').html('');
        $(".list-holder").unbind('scroll');
    }
    $("#divLoading").show();

    $.ajax({
        url: "/Dashboard/LocationList",
        type: "POST",
        async: true,
        data: { UserId: colleaguid },
        success: function (data) {
            if (data == 1) {
                $("#hdnlocationid").val(0);
                FinalReferralSend();
            }
            else
            {
                $("#divLoading").hide();
                $(".referral-list").html(data);
                $("#btn_send_Location").hide();
                $("#btn_send_patient").hide();
                $('#Colleague_Search_Block').hide();
                $('#clg-heading').hide();
                $('#Patient_Search_Block').hide();
                $('#loc-heading').show();
                $("#btn_send_dashboard").show();
                $("#btn_send_pat").show();
                $('#btn_send_final').hide();
            }
        }
    })
}
function SelectLocation(lid) {
    $(".referral-list > li").each(function () { $(this).removeClass('active'); });
    if ($("." + lid).hasClass('active')) {
        $("." + lid).removeClass('active');
    } else {
        $("." + lid).addClass('active');
        $('#btn_send_patient').attr("disabled", false);
        $("#hdnlocationid").val(lid);
    }
}

function ToggleSaveButton(id)
{
    $('#'+id).attr('disabled', true);
    setTimeout(function () {
        $('#'+id).attr('disabled', false);
    }, 4000);
}
function CheckIsImage() {
    var item = $("#fuImage").val();
    var extension = item.replace(/^.*\./, '');
    if (extension == "jpg" || extension == "jpeg" || extension == "gif" || extension == "png" || extension == "bmp" || extension == "psd" || extension == "pspimage" || extension == "thm" || extension == "tif" || extension == "yuv") {
        return true;
    }
    else if ($("#fuImage").val() == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select an image.' });
        ToggleSaveButton('fuImage');
        //alert('Please select an image');
        return false;
    } else {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please upload only Jpg,Png,Bmp and Gif Image.' });
        $("#fuImage").val('');
        ToggleSaveButton('fuImage');
        // alert('Please upload only Jpg,Png and Gif Image');
        return false;
    }

}
function ViewProfile(id) {
    $("#divLoading").show();
    $('#hdnViewProfile').val(id);
    $("#frmViewProfile").submit();
    $("#divLoading").hide();
}
function AddEditPatientInfo() {
    if ($.trim($("#FirstName").val()) == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter First Name.' });
        //alert('Please select an image');
        return false;
    }
    if ($.trim($("#LastName").val()) == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter Last Name.' });
        //alert('Please select an image');
        return false;
    }
    if ($.trim($("#Email").val()) != "") {
        if (!IsEmail($("#Email").val())) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Invalid email address.' });
            return false;
        }
    }

    if ($.trim($("#ZipCode").val()) != "") {
        if (!ValidateZipCode($("#ZipCode").val())) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Invalid ZipCode.' });
            return false;
        }
    }

    var Obj = $("#PatientForm1").serializeObject();
    performAjax({
        url: "/Patients/AddPatientsData",
        type: "POST",
        data: { Obj: Obj, PatientId: $("#hdnPatientId").val(), ReferredBy: $("#hdnReferredById").val() },
        success: function(s) {
            $.toaster({ priority: 'success', title: 'success', message: 'Details updated successfully' });
            setTimeout(function(){  window.location.reload(); }, 3000);
        }, error: function(err) {
            alert(err);
        }
    });

}
function AddEditPatientInsuranceCoverage() {
    var Obj = $("#PatientForm2").serializeObject();
    Obj["EmgContactName"] = $("#EmgContactName").val();
    Obj["EmergencyPhoneNo"] = $("#EmergencyPhoneNo").val();
    performAjax({
        url: "/Patients/AddInsurance",
        type: "POST",
        data: { Obj: Obj, PatientId: $("#hdnPatientId").val(), ReferredBy: $("#hdnReferredById").val() },
        success: function (Data) {
            $.toaster({ priority: 'success', title: 'success', message: 'Details updated successfully' });
            setTimeout(function(){  window.location.reload(); }, 3000);
        },
        error: function (err) {
            alert(err);
        }
    });
}
function AddMedicalHistory() {
    var Obj = $("#PatientForm3").serializeObject();
    performAjax({
        url: "/Patients/AddMedicalHistory",
        type: "POST",
        data: { medicalHistory: Obj, PatientId: $("#hdnPatientId").val(), ReferredBy: $("#hdnReferredById").val() },
        success: function (Data) {
            $.toaster({ priority: 'success', title: 'success', message: 'Details updated successfully' });
            setTimeout(function(){  window.location.reload(); }, 3000);
        },
        error: function (err) {
            alert(err);
        }
    });
}
function AddDentalHistory() {
    var Obj = $("#PatientForm4").serializeObject();
    performAjax({
        url: "/Patients/AddDentalHistory",
        type: "POST",
        data: { dentalHistory: Obj, PatientId: $("#hdnPatientId").val(), ReferredBy: $("#hdnReferredById").val() },
        success: function (Data) {
            $.toaster({ priority: 'success', title: 'success', message: 'Details updated successfully' });
            setTimeout(function(){  window.location.reload(); }, 3000);
        },
        error: function (err) {
            alert(err);
        }
    });
}
function GetPatientImages() {
    performAjax({
        url: "/Patients/GetPatientImages",
        type: "POST",
        data: { PatientId: $("#hdnPatientId").val() },
        success: function (data) {
            if (data != null && data != '') {
                $("#divImages").append(data);
            }
        }, error: function (err) {
            alert(err);
        }
    })
}
function GetPatientDocuments() {
    performAjax({
        url: "/Patients/GetPatientDocuments",
        type: "POST",
        data: { PatientId: $("#hdnPatientId").val() },
        success: function (data) {
            if (data != null && data != '') {
                $("#divDocuments").append(data);
            }
        }, error: function (err) {
            alert(err);
        }
    })
}

function showPopUp(config) {        
    if(config) {
        config.selector = config.selector || "customDailog";
        config.NoText = config.NoText || "No";
        config.YesText = config.YesText || "Yes";
        config.message = config.message || "Are you sure?";
        config.imagePath = config.imagePath || "/Content/OneClickReferral/images/Delete_Popup.png";


        $("#" + config.selector).modal('show');
        $("#" + config.selector + ' #dialog_yes').show();
        $("#" + config.selector + ' #dialog_no').html(config.NoText);
        $("#" + config.selector + ' #dialog_yes').html(config.YesText);
        $("#" + config.selector + ' #dialog_img').attr("src", config.imagePath);
        $("#" + config.selector + ' .modal-footer').css('text-align', 'center');
        $("#" + config.selector + ' #desc_message').text(config.message);

        $("#" + config.selector + ' #dialog_yes').click(function() {
            config.successCallBack(config.successParams);
            $("#" + config.selector).modal('hide');
        });
    }
}
