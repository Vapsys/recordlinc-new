﻿$(document).ready(function () {   
    if ($('#IsDashboard').val() == 1) {
        $('#btn-rply').trigger('click');
    }
    var x = $('#IsDashboardFoward').val();
    if (x == 1)
    {
        $('#btn-forward').trigger('click');
    }

    //--Temparary comment following code
    //if (PreviousURL) {
    //    if (PreviousURL.indexOf("Sent") > -1) {
    //        $("#lisentmessage").addClass('active');
    //    }
    //    else if (PreviousURL.indexOf("ReceiveReferral") > -1) {
    //        $("#lireceiveReferral").addClass('active');
    //    }
    //    else if (PreviousURL.indexOf("SentReferral") > -1) {
    //        $("#lisentreferral").addClass('active');
    //    }
    //}
})
function GetColleaguesProfile(id) {
    $("#hdnViewProfile").val(id);
    $("#frmViewProfile").submit();
}
function GetPatientProfile(id) {
    $("#hdnViewPatientHistory").val(id);
    $("#frmViewPatientHistory").submit();
}
function getAttachedPatientHistory(id) {
    $("#hdnViewPatientHistory").val(id);
    $("#frmViewPatientHistory").submit();
}
function deletepatientmessage(id) {
    if (id != null || id != '') {
        swal({
            title: "",
            text: MSG_REMOVEDIALOG_TITLE,
            type: "warning",
            showCancelButton: true,
            confirmButtonText: MSG_REMOVEDIALOG_YES,
            cancelButtonText: MSG_REMOVEDIALOG_CANCEL,
            closeOnConfirm: true,
            showLoaderOnConfirm: true,
        },
function (isConfirm) {
    if (isConfirm) {
        $.post("/Inbox/DeletePatientMessage", { MessageId: id }, function (data, status, xhr) {
            if (xhr.status == 403) {
                SessionExpire(xhr.responseText)
            }
            $.toaster({ priority: 'success', title: 'Success', message: 'Message deleted successfully.' });
            //window.location.href = "/Inbox/Index";
            window.location.href = PreviousURL;
        });
    }
});
    } else {
        return false;
    }
}
function deletesentboxmessage(id) {
    if (id != null || id != '') {
        swal({
            title: "",
            text: MSG_REMOVEDIALOG_TITLE,
            type: "warning",
            showCancelButton: true,
            confirmButtonText: MSG_REMOVEDIALOG_YES,
            cancelButtonText: MSG_REMOVEDIALOG_CANCEL,
            closeOnConfirm: true,
            showLoaderOnConfirm: true,
        },
function (isConfirm) {
    if (isConfirm) {
        $.post("/Inbox/DeleteSentBoxMessage", { MessageId: id }, function (data, status, xhr) {
            if (xhr.status == 403) {
                SessionExpire(xhr.responseText)
            }
            $.toaster({ priority: 'success', title: 'Success', message: 'Message deleted successfully.' });
            window.location.reload();
        });
    }
});
    } else {
        return false;
    }
}

function OpenDeletePopUpForMessage(id, check, msgFrom, $selector) {
    debugger;
    $selector = $selector || "patient_delete";
    $("#" + $selector).modal('show');
    $("#" + $selector + ' #btn_yes').show();
    $("#" + $selector + ' #btn_no').html('No');
    $("#" + $selector + ' .modal-footer').css('text-align', 'center');
    $("#" + $selector + ' #desc_message').text('Are you sure you want to archive message?');
    $("#" + $selector + ' #btn_yes').attr('onclick', ' deletemessage("' + id + '","' + check + '","' + msgFrom + '")');
}
function deletemessage(id, check, msgFrom) {
    var Obj = {
        'MessageId': id,
        'MessageDisplayType': check,
        'access_token': localStorage.getItem("token"),
        'MessageFrom': msgFrom
    };
    performAjax({
        url: "/Dashboard/DeleteReferral",
        type: "POST",
        data: { Obj: Obj, },
        success: function (data, status, xhr) {
            if (data == '1') {
                $.toaster({ priority: 'success', title: 'Success', message: 'Message archived successfully.' });
                setTimeout(function () {
                    //window.location = '/Inbox/Draft';
                    // window.location.href = "/Dashboard";
                    // Solve jira  XQ1-805
                    if (check ==="1") {
                        window.location.reload();
                    } else {
                        window.location.href = "/Dashboard";
                    }
                }, 3000);
            } else if (data == '-1') {
                $.toaster({ priority: 'danger', title: 'Success', message: 'Your session is Expired!' });
            } else {
                $.toaster({ priority: 'danger', title: 'Success', message: "Message can't deleted. Something want to wrong!" });
            }
        },
        error: function () {
            $.toaster({ priority: 'danger', title: 'Success', message: "Message can't deleted. Something want to wrong!" });
        }
    });
}
function getcomposeview(MessageId, IsPatientMessage, GetPreviousURL, Issent) {
    $("#divLoading").show();
    var objcomposedetails = {};
    objcomposedetails.MessageTypeId = 1;
    objcomposedetails.MessageId = MessageId;
    objcomposedetails.Issent = Issent;

    $.post("/Inbox/PartialComposeMessage", { request: objcomposedetails }, function (data, status, xhr) {
        if (xhr.status == 403) {
            SessionExpire(xhr.responseText);
        }
        if (data != null) {
            $("#div_" + MessageId + " #composeview").html('');
            $("#div_" + MessageId + " #replayportion").hide();
            $("#div_" + MessageId + " #composeview").show();
            $("#div_" + MessageId + " #composeview").html(data);
            $('html, body').animate({ scrollTop: $("#div_" + MessageId + " #composeview").offset().top }, "slow");
            $("#divLoading").hide();
            $("#btncancel").prop("href", "javascript:location.reload()");
        }
    });
}

function SendAsForward(MessageId, IsPatientMessage, GetPreviousURL) {
    $("#divLoading").show();
    var objcomposedetails = {};
    objcomposedetails.MessageTypeId = 3;
    objcomposedetails.MessageId = MessageId;
    if (IsPatientMessage == 1) {
        $.post("/Inbox/PartialColleagueForwardMessage", { objcomposedetails: objcomposedetails }, function (data, status, xhr) {
            if (xhr.status == 403) {
                SessionExpire(xhr.responseText);
            }
            if (data != null) {
                $("#composeview").html('');
                $("#replayportion").hide();
                $("#composeview").show();
                $("#composeview").append(data);
                $('html, body').animate({ scrollTop: $("#composeview").offset().top }, "slow");
                $("#btncancel").prop("href", "javascript:location.reload()");
                $("#divLoading").hide();
            }
        });
    } else {
        $.post("/Patients/PartialPatientForwardMessage", { objcomposedetails: objcomposedetails }, function (data, status, xhr) {
            if (xhr.status == 403) {
                SessionExpire(xhr.responseText);
            }
            if (data != null) {
                $("#composeview").html('');
                $("#replayportion").hide();
                $("#composeview").show();
                $("#composeview").append(data);
                $('html, body').animate({ scrollTop: $("#composeview").offset().top }, "slow");
                $("#btncancel").prop("href", "javascript:location.reload()");
                $("#divLoading").hide();

            }
        });
    }

}
function DownloadAtthachmentid(messageid, attachmentid, Isstatus) {
    $.post("/Inbox/DownloadAttachedfileofMessage", { DownId: messageid, AttaId: attachmentid, status: Isstatus }, function (data) {
    });
}