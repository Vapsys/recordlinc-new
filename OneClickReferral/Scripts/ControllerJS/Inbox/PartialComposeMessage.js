﻿$(document).ready(function () {
    select2Dropdown('[id^=colleaguelist]', 'Search Colleague', 'Inbox', 'GetColleagueList', true);
    select2Dropdown('[id^=patientlist]', 'Search Patient', 'Inbox', 'GetPatientList', true);

});
function select2Dropdown(Select2ID, ph, Controller, listAction, isMultiple) {
    var sid = Select2ID;
    $(sid).select2({
        placeholder: ph,
        minimumInputLength: 1,
        multiple: isMultiple,
        ajax: {
            url: "/" + Controller + "/" + listAction,
            dataType: 'json',
            delay: 550,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            error: function (response, status, xhr) {
                if (response.status == 403) {
                    SessionExpire(response.responseText)
                }
            }
        }
    })

}
function BindTeamFileUpload(FileIds, isCSV, MessageId) {
    debugger;
    MessageId = MessageId || 0;
    $("#divLoading").show();
    $.post("/OneClick/GetTempFiles", { FileIds: FileIds }, function (data) {
        $("#div_" + MessageId + " #tempfilecontent").append(data);
        $("#divLoading").hide();
        if ($("#div_" + MessageId + " #hduploadfiles").val()) { $("#div_" + MessageId + " #dvshowtitle").show() }
    });
}

function RemoveValue(list, value) {
    return list.replace(new RegExp(",?" + value + ",?"), function (match) {
        var first_comma = match.charAt(0) === ',',
            second_comma;
        if (first_comma &&
            (second_comma = match.charAt(match.length - 1) === ',')) {
            return ',';
        }
        return '';
    });
};
function SaveAsDraft(MessageId, ComposeType) {
    if (!$("#div_" + MessageId + " #btnSaveDraft").attr('disabled')) {
        var Message = CKEDITOR.instances["editor" + MessageId].getData();
        if ($("#div_" + MessageId + " #patientlist").val() == null && Message == '' && $("#div_" + MessageId + " #colleaguelist").val() == null && $("#div_" + MessageId + " #hduploadfiles").val() == '') {
            $("#danger-alert").alert();
            $("#danger-alert").fadeTo(3000, 3000).slideUp(3000, function () {
                $("#danger-alert").slideUp(3000);
            });
            $("html, body").animate({ scrollTop: 0 }, 600);
            return false;
        } else {
            $("#divLoading").show();
            var EncodedMessage = encodeURIComponent(Message);
            var objcomposedetail = {};
            objcomposedetail.MessageId = 0;
            objcomposedetail.PatientId = $("#div_" + MessageId + " #patientlist").val() != null ? $("#div_" + MessageId + " #patientlist").val().toString() : $("#div_" + MessageId + " #patientlist").val();
            objcomposedetail.ColleagueId = $("#div_" + MessageId + " #colleaguelist").val() != null ? $("#div_" + MessageId + " #colleaguelist").val().toString() : $("#div_" + MessageId + " #colleaguelist").val();
            objcomposedetail.MessageBody = EncodedMessage;
            objcomposedetail.FileIds = $("#div_" + MessageId + " #hduploadfiles").val();
            objcomposedetail.ComposeType = ComposeType;
            $("#div_" + MessageId + " #btnSaveDraft").attr("disabled", true);
            $.post("/Inbox/SaveAsDraft", { objcomposedetail: objcomposedetail },
                            function (data, status, xhr) {
                                if (xhr.status == 403) {
                                    var response = $.parseJSON(xhr.responseText);
                                    window.location = response.LogOnUrl;
                                }
                                $("#divLoading").hide();
                                $.toaster({ priority: 'success', title: 'Success', message: 'Draft saved successfully' });
                                setTimeout(function () {
                                    //window.location = '/Inbox/Draft';
                                    location.reload();
                                }, 3000);
                            });
        }
    }
}
function SendMessage(MessageId, MessageTypeId) {
    if (!$("#div_" + MessageId + " #btnSend").attr('disabled')) {
        var ColleagueId = $("#div_" + MessageId + " #colleaguelist").val();
        var Message = CKEDITOR.instances["editor" + MessageId].getData();
        var PatientId = $("#div_" + MessageId + " #patientlist").val();
        var EncodedMessage = encodeURIComponent(Message);
        var Colleagueflag = false, Messageflag = false, Patientflag = false
        if (ColleagueId == null) {
            Colleagueflag = true;
            $("#div_" + MessageId + " #lblColleague").show();
        }
        if (Message.toString() == '') {
            Messageflag = true;
            $("#div_" + MessageId + " #lblMessage").show();
        }

        //RM-412 : blank message issue
        var myContent = Message.toString();
        var x = $(myContent).text().trim();
        if (Message.toString() == '' || x == '') {
            Messageflag = true;
            $("#lblMessage").show();
        }
        //if (PatientId== null) {
        //    Patientflag = true;
        //    $("#lblPatient").show();
        //}
        if (Colleagueflag || Messageflag) {
            setTimeout(function () {
                HideValidation();
            }, 5000);
            return false;
        }
        $("#div_" + MessageId + " #btnSend").attr("disabled", true);
        $("#divLoading").show();
        var objcomposedetail = {};
        objcomposedetail.MessageId = MessageId;
        objcomposedetail.PatientId = $("#div_" + MessageId + " #patientlist").val() != null ? $("#div_" + MessageId + " #patientlist").val().toString() : $("#div_" + MessageId + " #patientlist").val();
        objcomposedetail.ColleagueId = $("#div_" + MessageId + " #colleaguelist").val() != null ? $("#div_" + MessageId + " #colleaguelist").val().toString() : $("#div_" + MessageId + " #colleaguelist").val();
        objcomposedetail.MessageBody = EncodedMessage;
        objcomposedetail.FileIds = $("#div_" + MessageId + " #hduploadfiles").val();
        objcomposedetail.MessageTypeId = MessageTypeId;
        $.post("/Inbox/SendMessageToColleagues", { objcomposedetail: objcomposedetail },
                         function (data, status, xhr) {
                             if (xhr.status == 403) {
                                 var response = $.parseJSON(xhr.responseText);
                                 window.location = response.LogOnUrl;
                             }
                             $("#divLoading").hide();
                             $.toaster({ priority: 'success', title: 'Success', message: 'Your Message sent successfully.' });
                             setTimeout(function () {
                                 window.location.reload();
                             }, 3000);
                         });

    }
}
function HideValidation() {
    $("#lblColleague").hide();
    $("#lblMessage").hide();
    //$("#lblPatient").hide();
}
//Ankit Create This Discard Draft Functionlity
function DeleteMessages(Id) {
    swal({
        title: "",
        text: MSG_REMOVEDIALOG_TITLE,
        type: "warning",
        showCancelButton: true,
        //confirmButtonColor: "#DD6B55",
        confirmButtonText: MSG_REMOVEDIALOG_YES,
        cancelButtonText: MSG_REMOVEDIALOG_CANCEL,
        closeOnConfirm: true,
        showLoaderOnConfirm: true,
    },
function (isConfirm) {
    if (isConfirm) {
        $("#divLoading").show();
        $.ajaxSetup({ async: false });
        $.ajax({
            url: "/Inbox/RemoveDraftMessageByID",
            type: "post",
            data: { MessageId: Id, MessageDisplayType: 1 },
            success: function (data) {
                if (data) {
                    $("#divLoading").hide();
                    $.toaster({ priority: 'success', title: 'Success', message: 'Message deleted successfully.' });
                    window.location.href = '../Inbox/Draft';
                }
            },
            error: function (response, status, xhr) {
                console.log("Error while deleting draft message");
            }
        });
        $.ajaxSetup({ async: true });
    }
});
}