﻿//Apply Select2 on Email text.
$("#ddlEmail").select2({
    minimumInputLength: 1,
    multiple: true,
    tags: true,
    tokenSeparators: [',', ' '],
    placeholder: "Enter the email of the colleague you would like to invite.  To invite multiple colleagues enter their email addresses.",
    createTag: function (term, data) {
        var value = term.term;        
        if (validateEmail(value)) {
            return {
                id: value,
                text: value
            };
        }
        return null;
    },
    language: {
        noResults: function () {
            return null;
        }
    }
});

//Check Email Regex for email.
//function validateEmail(email) {
//    var re = /^([a-zA-Z0-9][a-zA-Z0-9_.+-]+@@[\w-.]+\.[A-Za-z]{2,4},?)+$/;
//    return re.test(email);
//}

//Upload Excel sheet
$("#patientExcelUpload").change(function () {
    // Checking whether FormData is available in browser
    if (window.FormData !== undefined) {
        var fileUpload = $("#patientExcelUpload").get(0);
        var files = fileUpload.files;
        var fileExtension = ['xlsx', 'xls'];
        for (var i = 0; i < files.length; i++) {
            if ($.inArray(files[i].name.split('.').pop().toLowerCase(), fileExtension) == -1) {
                //swal({ title: "", text: 'Upload .xlsx or .xls file' });
                $("#ExcelUploadPopUp").modal('show');
                $('#desc_message').text('Upload .xlsx or .xls file.');
                $('#btn_yes').hide();
                $('#btn_no').css('right', '115px');
                $('#btn_no').text('Ok');
                //$("#lblfileupload").html(fileExtension.join(', ') + " formats are to be uploaded.");
                $("input[type='file']").replaceWith($("input[type='file']").clone(true));
                $("#lblfileupload").show();
                setTimeout(function () {
                    $("#lblfileupload").hide();
                }, 15000);
                $("#patientExcelUpload").val('');
                return false;
            }
        }
        // Create FormData object
        var fileData = new FormData();
        // Looping over all files and add it to FormData object
        for (var i = 0; i < files.length; i++) {
            fileData.append(files[i].name, files[i]);
        }

        // Adding one more key to FormData object
        $("#lblfileupload").hide();
        $("#divLoading").show();
        $.ajax({
            url: '/Colleagues/UploadExcelSheet',
            type: "POST",
            contentType: false, // Not to set any content header
            processData: false, // Not to process data
            data: fileData,
            async: false,
            success: function (data) {
                if (data) {
                    if ($("#hduploadfiles").val()) { $("#hduploadfiles").val(($("#hduploadfiles").val() + "," + data)) } else { $("#hduploadfiles").val(data) }
                    BindExcelFileUpload(data, 0);
                    $("#patientExcelUpload").val('');
                    //  $("input[type='file']").replaceWith($("input[type='file']").clone(true));
                    $("#divLoading").hide();
                    $("#lblfileupload").hide();
                }
            },
            error: function (err) {
                alert(err.statusText);
                $("#divLoading").hide();
            }
        });
    } else {
        alert("FormData is not supported.");
    }
});

//Bind Excel sheet
function BindExcelFileUpload(FileIds, ids) {
    $("#divLoading").show();
    if (ids == 0) {
        performAjax({
            url: "/Colleagues/GetExcelFileDetails",
            type: "POST",
            data: { FileIds: FileIds },
            async: false,
            success: function (data) {
                $("#tempExcelContent").append(data);
                $("#divLoading").hide();
            },
            error: function (err, xhr) {
                alert(xhr);
            }
        });
    }
}
function ExampleFile(RelativePath) {
    //var sitename = window.location.host;
    //if (sitename.indexOf('http://') == -1 || sitename.indexOf('https://')) {
    //    sitename = 'http://' + sitename
    //}
    //var str = RelativePath;
    //var res = sitename + str.replace("/mydentalfiles/", "/").replace("~/", "/");
    //res = res.replace("mydentalfiles", "recordlinc");
    //res = res.replace("../", "/");
    window.open(RelativePath, '_blank');
    window.focus();
}

function OpenDeleteExcelpopup(FileId, FileFrom, FilePath, TempFileId, ComposeType) {
    $('#colleage_delete').modal('show');
    $('#btn_yes_cd').show();
    $('#btn_no_cd').text('No');
    $('#btn-grp').css('margin', '0px 50px');
    $('#desc_message_cd').text('Are you sure you want to remove?');
    $('#btn_yes_cd').attr('onclick', ' DeleteExcel("' + FileId + '","' + FileFrom + '","' + FilePath + '","' + TempFileId + '","' + ComposeType + '")');
}

//Delete Excel sheet
function DeleteExcel(FileId, FileFrom, FilePath, TempFileId, ComposeType) {
    performAjax({
        url: "/Colleagues/DeleteExcel",
        type: "POST",
        data: { FieldId: FileId, FileName: FilePath },
        async: false,
        success: function (data) {
            if (data) {
                if (TempFileId.indexOf('F') >= 0 || TempFileId.indexOf('I') >= 0) {
                    if ($("#hduploadfiles").val()) {
                        $("#hduploadfiles").val(RemoveValue($("#hduploadfiles").val(), TempFileId));
                    }
                }
                $("#" + TempFileId).remove();
            }
            $.toaster({ priority: 'success', title: 'Notice', message: 'Attachment deleted successfully' });
            $("#divLoading").hide();
            $('#colleage_delete').modal('hide');
        },
        error: function (err, xhr) {
            alert(xhr);
        }
    });
}

function RemoveValue(list, value) {
    return list.replace(new RegExp(",?" + value + ",?"), function (match) {
        var first_comma = match.charAt(0) === ',',
            second_comma;
        if (first_comma &&
            (second_comma = match.charAt(match.length - 1) === ',')) {
            return ',';
        }
        return '';
    });
};

function ValidateEmail() {
    debugger;
    var Email = $("#ddlEmail").val() != null ? $("#ddlEmail").val().toString() : $("#ddlEmail").val();
    var FileName = "";
    FileName = $('#hdnfilename').val();
    if ((Email == null || Email == "Enter the email of the colleague you would like to invite.  To invite multiple colleagues enter their email addresses.") && FileName == null) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter email or upload excel file.' });
        ToggleSaveButton('Sendinvitebtn');
        return false;
    }
    else if (Email != null && Email != "Enter the email of the colleague you would like to invite.  To invite multiple colleagues enter their email addresses." && FileName != null) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter either email or upload excel file.' });
        ToggleSaveButton('Sendinvitebtn');
        return false;
    }
    else {
        var Obj = {
            'Email': Email,
            'FilePath': FileName,
            'Message': $("#txtMessage").val()
        };
        //performAjax({
        //    url: "/Colleagues/InviteColleague",
        //    type: "POST",
        //    data: { inviteColleague: Obj },
        //    async: false,
        //    success: function (data) {
        //        if (data == "1") {
        //            if (Email != null) {
        //                var res = Email.split(",");
        //                var cnt = res.length;
        //                if (cnt == 1) {
        //                    $("#msgSuccess").css("display", "block")
        //                    $("#msgSuccess").html("Colleague invited successfully.")

        //                   // $.toaster({ priority: 'success', title: 'Notice', message: 'Colleague invited successfully.' })
        //                }
        //                else {
        //                    $("#msgSuccess").css("display", "block")
        //                    $("#msgSuccess").html("Colleague invited successfully.")
        //                   // $.toaster({ priority: 'success', title: 'Notice', message: 'Colleagues invited successfully.' })
        //                }
        //            } else {
        //                $("#msgSuccess").css("display", "block")
        //                $("#msgSuccess").html("Colleague invited successfully.")
        //               // $.toaster({ priority: 'success', title: 'Notice', message: 'Colleague invited successfully.' })
        //            }
        //            setTimeout(function () {
        //                $('#ddlEmail').html('');
        //            }, 4000);
        //            if ($("#hduploadfiles").val()) {
        //                $("#hduploadfiles").val(RemoveValue($("#hduploadfiles").val(), TempFileId));
        //            }
        //            $('#txtMessage').val('I have added you as a colleague in my professional referral network on RecordLinc.');
        //            setTimeout(function () {
        //                window.location.reload();
        //            }, 4000);
        //        }
        //        else if (data == "2") {
        //            $("#msgWarning").css("display", "block")
        //            $("#msgWarning").html("Your file must be in .xlsx or .xls format and at least one column name should be named Email.")
        //         //   $.toaster({ priority: 'danger', title: 'Notice', message: 'Your file must be in .xlsx or .xls format and at least one column name should be named Email.' });
        //        }
        //        else if (data == "3") {
        //            $("#msgWarning").css("display", "block")
        //            $("#msgWarning").html("You can not invite yourself.")
        //           // $.toaster({ priority: 'danger', title: 'Notice', message: 'You can not invite yourself.' });
        //        }
        //        else if (data == "4") {
        //            $("#msgDanger").css("display", "block")
        //            $("#msgDanger").html("Please check your file. Email field is required!")
        //          //  $.toaster({ priority: 'danger', title: 'Notice', message: 'Please check your file. Email field is required!' });
        //        } else if (data != "") {
        //            $("#msgWarning").css("display", "block")
        //            $("#msgWarning").html(data)
        //           // $.toaster({ priority: 'warning', title: 'Notice', message: data })
        //        }
        //        else {
        //            // $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed Invite Colleague.' });
        //            $("#msgDanger").css("display", "block")
        //            $("#msgDanger").html("'danger', title: 'Notice', message: 'Failed Invite Colleague.")
        //            return false;
        //        }
        //        document.body.scrollTop = 0; // For Safari
        //        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        //        setTimeout(function () { $(".alert").css("display", "none") }, 4000);

        //    },
        //    error: function (err, xhr) {               
        //        alert(xhr);
        //    }
        //});
        $("#dvMessage").html('');
        performAjax({
            url: "/Colleagues/InviteColleague",
            type: "POST",
            data: { inviteColleague: Obj },
            async: false,
            success: function (data) {
                if (data != "") {
                    $("#dvInviteColleagues").css("display", "none");
                    $("#dvMessage").append(data);
                }
                //setTimeout(function () {
                //    $('#dvMessage').html('');
                //}, 10000);
            },error: function (err, xhr) {
                $("#msgDanger").css("display", "block");
                $("#msgDanger").html("'danger', title: 'Notice', message: 'Failed Invite Colleague.")
                setTimeout(function () {
                    $("#msgDanger").css("display", "none");
                    $('#msgDanger').html('');
                }, 4000);
                
            }
        })
    }
}

    function ToggleSaveButton(id) {
        $('#' + id).attr('disabled', true);
        setTimeout(function () {
            $('#' + id).attr('disabled', false);
        }, 4000);
    }


    function InviteMoreColleagues() {
        $('#dvMessage').html('');
        $("#tempExcelContent").empty();
        $("#ddlEmail").html('');
        $("#dvInviteColleagues").css("display", "block");
    }