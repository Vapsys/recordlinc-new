﻿$(document).ready(function () {
    SearchColleague();

});
//Page Load time called function
function SearchColleague() {
    var State = $("#ddlState").val();
    if (State == "Select State") {
        State = '';
    }
    var Obj = {
        'PageIndex': 1,
        'PageSize': 35,
        'Name': $("#txtDoctorName").val(),
        'Email': $("#txtEmail").val(),
        'Phone': $("#txtPhone").val(),
        'City': $("#txtCity").val(),
        'State': State,
        'Zipcode': $("#txtZipcode").val(),
        'SpecialityList': $("#txtSpeciality").val(),
        'Institute': $("#txtInstitute").val(),
        'Keywords': $("#txtKeyword").val(),
        'access_token': localStorage.getItem('token'),
    };
    performAjax({
        url: "/Colleagues/SearchDentistList",
        type: "POST",
        data: { Obj: Obj },
        async: false,
        success: function (data) {
            $("#appendDentist").html('');
            $("#appendDentist").html(data);
            if (data.indexOf("#nomore") == -1) {
                $("#HdnPageIndex").val(-1);
                $("#no_more").text("No record found");
            } else {               
                $("#HdnPageIndex").val(parseInt(1));
            }
            $("#HdnPageSize").val(parseInt(35));
        },
        error: function (err, xhr) {
            alert(xhr);
        }
    });
}

//Bind Scroll
$(function () {
    var bindScrollHandler = function () {
        var win_hg = ($(window).height()) * 30 / 100;
        $(window).scroll(function () {
            //console.log('$(window).scrollTop():- ' + $(window).scrollTop() + '$(document).height():- ' + $(document).height() + ' $(window).height():- ' + $(window).height() + ' win_hg:- ' + win_hg);
            if (parseInt($(window).scrollTop()) >= parseInt($(document).height() - $(window).height())) {
                var PageIndex = $("#HdnPageIndex").val();
                if (parseInt(PageIndex) == -1) {
                    $(window).unbind("scroll");
                    $('#divLoading').hide();
                }
                if (parseInt(PageIndex) != -1 && $("#appColleagues #nomore").length == 0) {
                    //$(window).scrollTop($(window).scrollTop() - parseInt($(window).scrollTop() / 2))
                    SearchListOnScroll();
                }
            }
            else {

            }
        });
    };
    bindScrollHandler();
});

//ScrollTime Called Function
function SearchListOnScroll() {
    var State = $("#ddlState").val();
    if (State == "Select State") {
        State = '';
    }
    var PageIndex = $("#HdnPageIndex").val();
    if (PageIndex == "-1") {
        return "";
    } else {
        PageIndex = parseInt(PageIndex) + parseInt(1);
    }
    var Obj = {
        'PageIndex': PageIndex,
        'PageSize': $("#HdnPageSize").val(),
        'Name': $("#txtDoctorName").val(),
        'Email': $("#txtEmail").val(),
        'Phone': $("#txtPhone").val(),
        'City': $("#txtCity").val(),
        'State': State,
        'Zipcode': $("#txtZipcode").val(),
        'SpecialityList': $("#txtSpeciality").val(),
        'Institute': $("#txtInstitute").val(),
        'Keywords': $("#txtKeyword").val(),
        'access_token': localStorage.getItem('token'),
    };
    performAjax({
        url: "/Colleagues/SearchDentistList",
        type: "POST",
        data: { Obj: Obj },
        async: false,
        success: function (data) {
            var html = $("#appendDentist").html();
            if (data != null && data != "" && html.indexOf("No more record found") < 0) {
                $("#appendDentist").append(data);
                $("#HdnPageIndex").val(parseInt(PageIndex));
            } else {
                $("#HdnPageIndex").val(parseInt(-1));
                return false;
            }
        },
        error: function (err, xhr) {
            alert(xhr);
        }
    });
}

//Add Dentist As Colleagues
function AddColleagues(id) {
    var Obj = {
        'ColleaguesId': id,
        'access_token': localStorage.getItem('token')
    };
    performAjax({
        url: "/Colleagues/AddColleagues",
        type: "POST",
        data: { Obj: Obj },
        async: false,
        success: function (data) {
            if (data) {
                $.toaster({ priority: 'success', title: 'Notice', message: 'Successfully added as colleague.', timeout:10000 });
                $('#div_' + id).remove();
            } else {
                $.toaster({ priority: 'warning', title: 'Notice', message: 'Failed Add As Colleague.' });
            }
        },
        error: function (err, xhr) {
            alert(xhr);
        }
    });
}

function Clear() {
    $("input[type=text]").val('');
}
$("#ddlState").on('change', function () {
    $("#HdnPageIndex").val(1);
    $("#HdnPageSize").val(35);
    SearchColleague();
});