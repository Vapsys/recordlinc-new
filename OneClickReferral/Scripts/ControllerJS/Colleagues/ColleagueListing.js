﻿$(document).ready(function () {
    ColleaguesList();   
});

function showSpecialty(id) {
   // $(".seemore").click(function () {    
    $("#aSeemore_" + id).addClass("remove");
    $("#aSeeless_" + id).removeClass("remove");
        $("#dvSeemore_"+ id).addClass("active");
   // });

}
function hideSpecialty(id) {       
    $("#aSeemore_" + id).removeClass("remove");
    $("#aSeeless_" + id).addClass("remove");
    $("#dvSeemore_" + id).removeClass("active");
}

function ToggleSaveButton(id) {
    $('#' + id).attr('disabled', true);
    setTimeout(function () {
        $('#' + id).attr('disabled', false);
    }, 4000);
}



function onChangeAction(Id) {
    var ddlId = "ddlAction_" + Id;   
    if ($("#" + ddlId).val() == 1) {
        localStorage.setItem("ddlColleagueId", Id);
        window.location.href = ($("#" + ddlId + " :selected").data("url"));        
    } else if ($("#" + ddlId).val() == 2) {
        OpenDeleteForColleague($("#" + ddlId + " :selected").data("colleagueid"));
    } else {
        return false;
    }
  $('#'+ddlId).val(0);
}

//Page load time Get Colleague List.
function ColleaguesList() {
    var Obj = {
        'PageIndex': $("#HdnPageIndex").val(),
        'PageSize': $("#HdnPageSize").val(),
        'SearchText': $("#txtSearchColleagues").val(),
        'SortColumn': $("#HdnSortColumn").val(),
        'SortDirection': $("#HdnSortDirection").val(),
        'FilterBy': 12,
        'NewSearchText': '',
        'access_token': localStorage.getItem('token')
    };
    performAjax({
        url: "/Colleagues/GetColleagueList",
        type: "POST",
        data: { Obj: Obj },
        success: function (data) {
            $("#appColleague").html('');
            $("#appColleague").html(data);
            if (data.includes("nomore")) {
                $("#HdnPageIndex").val(-1);
            }
            jcf.replaceAll();
        },
        error: function () {

        }
    });
}

//Open Popup for Delete Colleague
function OpenDeleteForColleague(ColleagueId) {
    $('#colleage_delete').modal('show');
    $('#btn_yes_cd').show();
    $('#btn_no_cd').text('No');
    $('#btn-grp').css('margin', '0px 50px');
    $('#desc_message_cd').text('Are you sure you want to remove?');
    $('#btn_yes_cd').attr('onclick', ' RemoveColleague("' + ColleagueId + '")');
}

//Bind Scroll
$(function () {
    var bindScrollHandler = function () {
        var win_hg = ($(window).height()) * 30 / 100;
        $(window).scroll(function () {
            //console.log('$(window).scrollTop():- ' + $(window).scrollTop() + '$(document).height():- ' + $(document).height() + ' $(window).height():- ' + $(window).height() + ' win_hg:- ' + win_hg);
            if (parseInt($(window).scrollTop()) >= parseInt($(document).height() - $(window).height())) {
                var PageIndex = $("#HdnPageIndex").val();
                if (parseInt(PageIndex) == -1) {
                    $(window).unbind("scroll");
                    $('#divLoading').hide();
                }
                if (parseInt(PageIndex) != -1 || $("#appColleagues #nomore").length == 0) {
                    //$(window).scrollTop($(window).scrollTop() - parseInt($(window).scrollTop() / 2))
                    GetColleaguesListOnScroll();
                }
            }
            else {

            }
        });
    };
    bindScrollHandler();
});

//Get Colleague list on Scroll
function GetColleaguesListOnScroll() {
    var PageIndex = $("#HdnPageIndex").val();
    if (PageIndex == "-1") {
        return "";
    } else {
        PageIndex = parseInt(PageIndex) + parseInt(1);
    }
    var Obj = {
        'PageIndex': PageIndex,
        'PageSize': $("#HdnPageSize").val(),
        'SearchText': $("#txtSearchColleagues").val(),
        'SortColumn': $("#HdnSortColumn").val(),
        'SortDirection': $("#HdnSortDirection").val(),
        'FilterBy': 12,
        'NewSearchText': '',
        'access_token': localStorage.getItem('token')
    };
    performAjax({
        url: "/Colleagues/GetColleagueList",
        type: "POST",
        data: { Obj: Obj },
        success: function (data) {
            var html = $("#appColleague").html();
            if (data != null && data != "" && html.indexOf("No more record found") < 0 && html.indexOf("No record found") < 0) {
                $("#appColleague").append(data);
                if (data.includes("nomore")) {
                    $("#HdnPageIndex").val(-1);
                } else {
                    $("#HdnPageIndex").val(parseInt(PageIndex));
                }
                if ($(".trcolleague").length == 0) {
                    $("#nomore").text('No record found!');
                }
                jcf.replaceAll();
            } else {
                if ($(".trcolleague").length == 0) {
                    $("#nomore").text('No record found!');
                }
                $("#HdnPageIndex").val(parseInt(-1));
                return false;
            }
        },
        error: function (err) {

        }
    })
}
function UncheckMaincheckBox(id) {
    $("#chkallClg").parent('span').removeClass('jcf-checked').addClass('jcf-unchecked');
    $("#chkallClg").prop("checked", false);
}
//Delete Multiple Colleagues

//$('#btnMultipleDelete').on("click", function () {
//function btnMultipleDelete() {
function DeleteMultipleColleagues() {
    debugger;
    //#XQ1-474 'Delete' tool tip does not get disappear after clicking 'Delete' icon, under 'Colleagues' page.
    $(this).tooltip('hide');
    //var userlist = [];
    var MultipleDelete = null;

    //var c = new Array();
    //c = document.getElementsByTagName('input');
    //for (var i = 0; i < c.length; i++) {
    //    if (c[i].type == 'checkbox' && c[i].id == 'checkcolleague' && c[i].checked == true) {
    //        if (MultipleDelete == null) {
    //            MultipleDelete = c[i].value;
    //        } else {
    //            MultipleDelete = MultipleDelete + ',' + c[i].value;
    //        }
    //    }
    //}
    //var Obj = {};
    // Obj = {
    //    'ColleaguesId': MultipleDelete,
    //    'access_token': localStorage.getItem('token')
    //};

    MultipleDelete = $(".checkSingle:checked").map(function () {
        return $(this).val();
    }).get().join(",");
    //   userlist = MultipleDelete.split(',');


    if ($(".checkSingle:checked").length == 0) {
        //swal({ title: "", text: 'Please select at least one colleague' });
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select at least one colleague' });
        $('#btnMultipleDelete').attr('disabled', true);
        setTimeout(function () {
            $('#btnMultipleDelete').attr('disabled', false);
        }, 4000);
        return false;
    }
    else {
        $('#multiple_delete').modal('show');
        $('#btn_yes').show();
        $('#btn_no').text('No');
        $('#desc_message').text('Are you sure you want to remove?');
        $('#btn_yes').attr('onclick', ' DeleteColleagues("' + MultipleDelete + '")');
    }
};
//});

function DeleteColleagues(MultipleDelete) {
    var Obj = {
        'ColleaguesId': MultipleDelete,
        'access_token': localStorage.getItem('token')
    };
    var userlist = [];
    userlist = MultipleDelete.split(',');
    $.ajax({
        url: "/Colleagues/DeleteColleagues",
        type: "POST",
        data: { Obj: Obj },
        success: function (data) {
            if (data == "1") {
                $('#multiple_delete').modal('hide');
                $.toaster({ priority: 'success', title: 'Notice', message: 'Colleague removed successfully.' });
                for (var i = 0; i < userlist.length; i++) {
                    $('#row_' + userlist[i]).remove();
                }
                $('#btnMultipleDelete').attr('disabled', true);
                setTimeout(function () {
                    $('#btnMultipleDelete').attr('disabled', false);
                }, 4000);
                if ($("#appColleague tr").length == 0) {
                    setTimeout(function () { window.location.reload(); }, 3000);
                }
            } else {
                //swal({ title: "", text: 'Failed to remove  Colleague' });
                $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to remove  Colleague' });
                $('#btnMultipleDelete').attr('disabled', true);
                setTimeout(function () {
                    $('#btnMultipleDelete').attr('disabled', false);
                }, 4000);
            }
        }
    })
}


//Delete Single Colleagues
function RemoveColleague(ColleagueId) {
    var Obj = {
        'ColleaguesId': ColleagueId,
        'access_token': localStorage.getItem('token')
    };    
    $.post("/Colleagues/DeleteColleagues",
        { Obj: Obj }, function (data) {
            if (data) {
                $('#multiple_delete').modal('hide');
                $.toaster({ priority: 'success', title: 'Notice', message: 'Colleague removed successfully.' });
                $('#row_' + ColleagueId).remove();
            } else {
                //swal({ title: "", text: 'Failed to remove  Colleague' });
                $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to remove  Colleague' });
            }
            $('#colleage_delete').modal('hide');
        });
    $.ajaxSetup({ async: true });
}

//Check All Funcationality of Thead.
$("#chkallClg").on('click', function () {
    if (this.checked) {
        $(".checkSingle").each(function () {
            $(this).prop('checked', true);
            $(".all-select span").removeClass('jcf-unchecked').addClass('jcf-checked');
        })
    } else {
        $(".checkSingle").each(function () {
            $(this).prop('checked', false);
            $(".all-select span").removeClass('jcf-checked jcf-focus').addClass('jcf-unchecked');
        })
    }
})

//Search Colleague on Keypress
function HdrSearchColleague(event) {
    if (event.keyCode == 13) {
        var message = $('#txtSearchColleagues').val();
        if (message != null && message != "") {
            SearchColleague();
        }
    }
}

//Search Colleague
function SearchColleague() {
    var Obj = {
        'PageIndex': 1,
        'PageSize': 35,
        'SearchText': $("#txtSearchColleagues").val(),
        'SortColumn': 12,
        'SortDirection': 2,
        'FilterBy': 12,
        'NewSearchText': '',
        'access_token': localStorage.getItem('token')
    };
    performAjax({
        url: "/Colleagues/GetColleagueList",
        type: "POST",
        data: { Obj: Obj },
        success: function (data) {
            $("#HdnPageSize").val(parseInt(35));
            $("#HdnSortColumn").val(parseInt(12));
            $("#HdnSortDirection").val(parseInt(2));
            $("#appColleague").html('');
            $("#appColleague").html(data);
            if ($(".trcolleague").length == 0) {
                $("#nomore").text('No record found!');
            }
            if (data.includes("nomore")) {
                $("#HdnPageIndex").val(-1);
            } else {
                $("#HdnPageIndex").val(parseInt(1));
            }

            jcf.replaceAll();
        },
        error: function (err) {

        }
    });
}

//Open Confrim popup for Has button
function OpenConfirmPopUp(id) {
    $("#EnableDisableitem").modal('show');
    var Ischeck = $("#checkhas_" + id).is(":unchecked");
    if (Ischeck == true) {
        $('#desc_dismessage').show();
        $('#desc_enmessage').hide();
    } else {
        $('#desc_enmessage').show();
        $('#desc_dismessage').hide();
    }
    $('#btnno').attr('onclick', ' UncheckHasButton(' + id + ')');
    $('#btnyes').attr('onclick', ' EnableDisableButton(' + id + ')');
}

//Disable Has button.
function UncheckHasButton(id) {
    var hascheck = $("#checkhas_" + id + "").is(":checked");
    if (hascheck == true) {
        $("#checkhas_" + id + "").prop("checked", false);
        $(".clshas_" + id + " span").removeClass('jcf-checked').addClass('jcf-unchecked');
    }
    else {
        $("#checkhas_" + id + "").prop("checked", true);
        $(".clshas_" + id + " span").removeClass('jcf-unchecked').addClass('jcf-checked');
    }
}

//Enable Single Has button
function EnableDisableButton(id) {
    var Obj = {
        'ReceiverId': id,
        'access_token': localStorage.getItem('token')
    }
    performAjax({
        url: "/Colleagues/EnableHasButton",
        type: "POST",
        data: { Obj: Obj },
        success: function () {
            $.toaster({ priority: 'success', title: 'Notice', message: 'Has Button Updated Successfully' });
        },
        error: function (err) {

        }
    })
}

//Check Multiple Has button 
$("#checkallHas").on('click', function () {
    var colleaguesId = [];
    $("#appColleague .align-middle").find("input[type='checkbox']").each(function () {
        if ($("#checkallHas").prop('checked') == true) {
            $(this).attr('checked', true);
            $(".align-middle span").removeClass('jcf-unchecked').addClass('jcf-checked');
            colleaguesId.push($(this).data('val'));
        }
        else {
            $(this).removeAttr('checked', false);
            $(".align-middle span").removeClass('jcf-checked jcf-focus').addClass('jcf-unchecked');
            colleaguesId.push($(this).data('val'));
        }
    });
    if ($("#checkallHas").prop('checked') == true)
        UpdateHasButton(colleaguesId, true);
    else
        UpdateHasButton(colleaguesId, false);
});

//Enable Multiple Has button
function UpdateHasButton(ColleaguesId, IsEnable) {
    var Obj = {
        'ColleaguesId': ColleaguesId,
        'IsEnable': IsEnable,
        'access_token': localStorage.getItem('token')
    };
    performAjax({
        url: "/Colleagues/UpdateHasButton",
        type: "post",
        data: { Obj: Obj },
        async: false,
        success: function (data) {
            if (data) {
                $.toaster({ priority: 'success', title: 'Notice', message: 'Has Button Updated Successfully.' });
            } else {
                $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to Update Hus Button!' });
            }
        },
        error: function (err) {

        }
    })
}