﻿function ChangeSent() {
    changeReferredBy();
    $("#hdnReferralFrom").val(2);
    _getdataofReceivedReferral(2);
    ApplyActiveClass(2);
}
function ChangeReceive() {
    changeReferredTo();
    $("#hdnReferralFrom").val(1);
    _getdataofReceivedReferral(1);
    ApplyActiveClass(1);
}
function ChangeAll() {
    changeReferredAll();
    $("#hdnReferralFrom").val(0);
    _getdataofReceivedReferral(0);
}
$("#btn_send").on('click', function () {
    $('#cnfSendReceive').modal('hide');
    ChangeSent();
});
$("#btn_Receive").on('click', function () {
    $('#cnfSendReceive').modal('hide');
    ChangeReceive();
});
function ApplyActiveClass(From) {
    if (From == 2) {
        $("#lisentreferral").addClass('active').siblings().removeClass('active');
    } else {
        $("#lireceiveReferral").addClass('active').siblings().removeClass('active');
    }
}
function changeReferredBy() {
    $(".textchange").text('Referred To');
    $("#txtReferredBy").attr('placeholder', 'Referred To');
}
function changeReferredTo() {
    $(".textchange").text('Referred By');
    $("#txtReferredBy").attr('placeholder', 'Referred By');
}
function changeReferredAll() {
    $(".textchange").text('Referred By/To');
    $("#txtReferredBy").attr('placeholder', 'Referred By/To');
}
function disablefields() {
    $(".isDisable").attr('disabled', 'disabled');
}
function OpenDeletePopUpForMessage(id, check, msgFrom, isownreferral) {
    $('#Message_delete').modal('show');
    debugger;
    if (isownreferral === "False") {
        $('#btn_yes').hide();
        $('#desc_message').html('This message can not be archived because it is <br/> assigned to one of your team members. <br/> Please contact them to archive it.');
        $('#btn_no').html('Ok');

    } else {
    $('#btn_yes').show();
    $('#btn_no').html('No');
    $('#desc_message').text('Are you sure you want to archive message?');
    $('#btn_yes').attr('onclick', ' deletemessage("' + id + '","' + check + '","' + msgFrom + '")');
    }
    $('.modal-footer').css('text-align', 'center');

}
function deletemessage(id, check, msgFrom) {
    var Obj = {
        'MessageId': id,
        'MessageDisplayType': check,
        'access_token': localStorage.getItem("token"),
        'MessageFrom': msgFrom
    };
    performAjax({
        url: "/Dashboard/DeleteReferral",
        type: "POST",
        data: { Obj: Obj, },
        success: function (data, status, xhr) {
            if (data == '1') {
                //$('#desc_message').text('Message archive successfully.');
                //$('.modal-footer').css('text-align', 'center');
                $('#msg_' + id).remove();
                $('#Message_delete').modal('hide');
                //$('#btn_yes').hide();
                //$('#btn_no').html('Ok');
            } else if (data == '-1') {
                $('#desc_message').text('Your session is Expired!');
                $('#btn_yes').hide();
                $('#btn_no').html('Ok');
                localStorage.clear();
                setTimeout(function () {
                    window.location = "/Dashboard";
                }, 5000);

            } else {
                $('#desc_message').text("This message can not be archived");
                $('#btn_yes').hide();
                $('#btn_no').html('Ok');
            }
        },
        error: function () {
            $('#desc_message').text("This message can not be archived");
            $('#btn_yes').hide();
            $('#btn_no').html('Ok');
        }
    });
}
function clearSearch() {
    $('#txtReferredBy').val('');
    $('#txtSearchFromDate').val('');
    $('#txtSearchToDate').val('');
    $("#txtPatient").val('');
    $("#txtMessage").val('');
    $("#ddlDisposition").val('').trigger('change');
    $('#ddlStatus').prop('selectedIndex', 0);
    $("#ddlInsurance").prop('selectedIndex', 0);

    _getdataofReceivedReferral($("#hdnReferralFrom").val());
}
function _getdataofReceivedReferral(ReferralFrom) {
    $('#refData').animate({
        scrollTop: 0
    }, 'slow');
    $("#divLoading").show();
    var obj = {
        'MessageId': null,
        'PageSize': 50,
        'PageIndex': 1,
        'SortDirection': $("#SortDirct").val(),
        'SortColumn': $("#currentSortCol").val(),
        'access_token': localStorage.getItem('token'),
        'DoctorName': $.trim($('#txtReferredBy').val()),
        'FromDate': $.trim($('#txtSearchFromDate').val()),
        'ToDate': $.trim($('#txtSearchToDate').val()),
        'Disposition': $("#ddlDisposition").val() != null ? $("#ddlDisposition").val().toString() : $("#ddlDisposition").val(),
        'MessageFrom': ReferralFrom,
        'InsuranceStatus': $("#ddlInsurance").val()
    };
    performAjax({
        url: "/Dashboard/GetReferralHistory",
        type: "POST",
        async: false,
        data: { Obj: obj },
        success: function (data) {
            if (data != null) {
                $("#refData").html('');
                $("#refData").append(data);
                jcf.replaceAll();
                if (ReferralFrom == 2) {
                    disablefields();
                }
                ApplyActiveClass(ReferralFrom);
            }
        },
        error: function () {
            alert("Oops...this pages need to reload.  Please try again.");
        }
    });
    $("#divLoading").hide();
}
$(window).scroll(function () {
    if (parseInt($(window).scrollTop()) >= parseInt($(document).height() - $(window).height())) {
        if (parseInt(My) != -1) {
            var My = $("#PageIndex").val();
            $("#PageIndex").val(parseInt(My) + parseInt($("#PageIndex").val()));
            ScrollData();
        }
    }
});
function SetDispostionStatus() {
    performAjax({
        url: "/Dashboard/GetDispostionList",
        type: "POST",
        async: true,
        success: function (data) {
            if (data != null) {
                $.each(data, function (i, value) {
                    $("#ddlDisposition").append($("<option></option>").val(value.Id).html(value.Status));
                });
            }
        },
        error: function () {
            alert("Oops...this pages need to reload.  Please try again.");
        }
    });
}
function SortColumn(ColId) {

    var SortCol = $("#" + ColId).attr('sort-col');
    var Sortdir = $("#" + ColId).attr('sort-dir');
    $("#currentSortCol").val(SortCol);
    $("#SortDirct").val(Sortdir);
    if (Sortdir == 1) {
        $("#" + ColId).attr('sort-dir', 2);
    } else {
        $("#" + ColId).attr('sort-dir', 1);
    }
    var pagesize = $("#PageSizeSet").val();

    var pageindex = $("#PageIndex").val();

    //RM-194 changes for page index.
     $("#PageIndex").val(1);
    //if (pageindex <= -1) {
    //    return false;
    //}
    //else {
    //    $("#PageIndex").val(1);
    //}
    _searchData();
}
function _searchData() {
    $("#PageIndex").val(1);
    $("#divLoading").show();
    var obj = {
        'MessageId': null,
        'PageSize': $.trim($("#PageSizeSet").val()),
        'PageIndex': $("#PageIndex").val(),
        'SortDirection': $("#SortDirct").val(),
        'SortColumn': $("#currentSortCol").val(),
        'access_token': $.trim(localStorage.getItem('token')),
        'DoctorName': $('#txtReferredBy').val(),
        'FromDate': $.trim($('#txtSearchFromDate').val()),
        'ToDate': $.trim($('#txtSearchToDate').val()),
        'Disposition': $("#ddlDisposition").val() != null ? $("#ddlDisposition").val().toString() : $("#ddlDisposition").val(),
        'MessageStatus': $("#ddlStatus").val(),
        'PatientName': $.trim($("#txtPatient").val()),
        'Message': $.trim($("#txtMessage").val()),
        'MessageFrom': $("#hdnReferralFrom").val(),
        'InsuranceStatus': $("#ddlInsurance").val()
    };
    performAjax({
        url: "/Dashboard/GetReferralHistory",
        type: "POST",
        async: true,
        data: { Obj: obj },
        success: function (data) {
            $("#refData").html('');
            if (data != null) {
                $("#refData").append(data);
            }
            if ($("#hdnReferralFrom").val() == 2) {
                disablefields();
            }
            ApplyActiveClass($("#hdnReferralFrom").val());
        },
        error: function () {
            alert("Oops...this pages need to reload.  Please try again.");
        }
    });
    $("#divLoading").hide();
}
function ScrollData() {
    var pageindex = $("#PageIndex").val();
    if (pageindex <= -1) {
        return false;
    }
    $("#divLoading").show();
    var obj = {
        'MessageId': null,
        'PageSize': $("#PageSizeSet").val(),
        'PageIndex': $("#PageIndex").val(),
        'SortDirection': $("#SortDirct").val(),
        'SortColumn': $("#currentSortCol").val(),
        'access_token': localStorage.getItem('token'),
        'DoctorName': $.trim($('#txtReferredBy').val()),
        'FromDate': $.trim($('#txtSearchFromDate').val()),
        'ToDate': $.trim($('#txtSearchToDate').val()),
        'Disposition': $("#ddlDisposition").val() != null ? $("#ddlDisposition").val().toString() : $("#ddlDisposition").val(),
        'MessageStatus': $("#ddlStatus").val(),
        'PatientName': $.trim($("#txtPatient").val()),
        'Message': $.trim($("#txtMessage").val()),
        'MessageFrom': $("#hdnReferralFrom").val(),
        'InsuranceStatus': $("#ddlInsurance").val()
    };
    performAjax({
        url: "/Dashboard/GetReferralHistory",
        type: "POST",
        async: true,
        data: { Obj: obj },
        success: function (data) {
            if (data != null) {
                if (data.indexOf("Nomore") != -1) {
                    $("#PageIndex").val(-1);
                }
                var my = $("#refData").html();
                if (my.indexOf("Nomore") == -1) {
                    $("#refData").append(data);
                }
                if ($("#hdnReferralFrom").val() == 2) {
                    disablefields();
                }
                ApplyActiveClass($("#hdnReferralFrom").val());
            }
        },
        error: function () {
            alert("Oops...this pages need to reload.  Please try again.");
        }
    });
    $("#divLoading").hide();
}
function GetdoctorName(e) {
    var AccountNamesearch = $("#txtReferredBy").val();
    var Editkeycode;
    if (window.event) Editkeycode = window.event.keyCode;
    else if (e) Editkeycode = e.which;
    if (AccountNamesearch.length > 3 || AccountNamesearch.length == 0) {
        _searchData();
    }
}
function GetPatientName(e) {
    var AccountNamesearch = $("#txtPatient").val();
    var Editkeycode;
    if (window.event) Editkeycode = window.event.keyCode;
    else if (e) Editkeycode = e.which;
    if (AccountNamesearch.length > 3 || AccountNamesearch.length == 0) {
        _searchData();
    }
}
function GetMessageName(e) {
    var AccountNamesearch = $("#txtMessage").val();
    var Editkeycode;
    if (window.event) Editkeycode = window.event.keyCode;
    else if (e) Editkeycode = e.which;
    if (AccountNamesearch.length > 3 || AccountNamesearch.length == 0) {
        _searchData();
    }
}
function OpenNotePopup(MessageId, CurrentDispositionId, objVal) {
    if (objVal != "") {
        $("#add-Note").modal('show');
        $("#add-Note #MessageId").val(MessageId);
       
        $('#btn_yesfor').attr('onclick', 'InsertDispositionstatus("' + MessageId + '")');
        $("#btn_nofor").attr("onclick", "dismissDispositionstatus(" + MessageId + "," + CurrentDispositionId + ")");
        if (objVal == 7) {
            $("#txtScheduledDate").css("display", "inline-block");          
        } else {
            $("#txtScheduledDate").css("display", "none");        
            $('#spScheduledDate').text('');
            $("#spRemoveScheduledDate").css("display", "none");
        }
    }
}
function removeScheduledDate() {
    $('#spScheduledDate').text('');
    $("#spRemoveScheduledDate").css("display", "none");   
}
function dismissDispositionstatus(MessageId, CurrentDispositionId) {
    if (CurrentDispositionId == 0) {
        $('#ddl_' + MessageId).find('option:eq(0)').prop('selected', true);
    }
    else {
        $('#ddl_' + MessageId).val(CurrentDispositionId);
    }
    $("#spScheduledDate").text('');    
    $("#txtNote").val('');
   // $("#chkPat").attr("checked", false);
    $("#dvChkPat").children().removeClass("jcf-checked");
    $("#dvChkDoc").children().addClass("jcf-checked");    
    $('#add-Note').modal('hide');
}
function InsertDispositionstatus(MessageId) {

    //Condition put here because defult text not updated as disposition status.
    if ($("#ddl_" + MessageId).val() == "" || $("#ddl_" + MessageId).data('val') == $("#ddl_" + MessageId).val()) {
        return false;
    }
    var obj = {
        'MessageId': MessageId,
        'DispositionId': $("#ddl_" + MessageId).val(),
        'UserId': localStorage.getItem("Userid"),
        'Note': $("#txtNote").val(),
        'VisibleToPatient': $("#chkPat").prop("checked") ? true : false,
        'VisibleToDoctor': $("#chkDoc").prop("checked") ? true : false,
        'ScheduledDate': $("#spScheduledDate").text()
    };
    performAjax({
        url: "/Dashboard/ChangeDisposionStatus",
        type: "POST",
        data: { Obj: obj },
        success: function (data) {
            if (data == true) {
                $.toaster({ priority: 'success', title: 'success', message: 'Disposition status updated successfully' });
                $("ddl_" + MessageId).attr('data-val', $("#ddl_" + MessageId).val());
                ToggleSaveButton("ddl_" + MessageId);
                $('#add-Note').modal('hide');
                $("#txtNote").val('');
                $("#chkPat").attr("checked", false);
                $("#chkDoc").attr("checked", true);
                _getdataofReceivedReferral($("#ddlMessageFrom").val());
                $("#spScheduledDate").text('');
            }
        },
        error: function () {
            alert("Oops...this pages need to reload.  Please try again.");
        }
    });
}
function ToggleSaveButton(id) {
    $('#' + id).attr('disabled', true);
    setTimeout(function () {
        $('#' + id).attr('disabled', false);
    }, 5000);
}
function GetConversationView(MessageId) {

    //if (MessageId > 0) {
    //    $("#hdnMessageId").val(MessageId);
    //    $("#hdnPageSize").val($("#PageSizeSet").val());
    //    $("#hdnPageIndex").val($("#PageIndex").val());
    //    $("#hdnSortDirection").val(1);
    //    $("#hdnSortColumn").val(7);
    //    $("#hdnaccess_token").val(localStorage.getItem('token'));
    //    $("#hdnDoctorName").val($('#txtReferredBy').val());
    //    $("#hdnFromDate").val($('#txtSearchFromDate').val());
    //    $("#hdnToDate").val($('#txtSearchToDate').val());
    //    $("#hdnDisposition").val($("#ddlDisposition").val());
    //    $("#hdnMessageStatus").val($("#ddlStatus").val());
    //    $("#hdnPatientName").val($("#txtPatient").val());
    //    $("#hdnMessage").val($("#txtMessage").val());
    //    $("#hdnReferralFrom").val($("#hdnReferralFrom").val());
    //    $("#hdnform").submit();
    //}
    var obj = {
        'MessageId': MessageId,
        'PageSize': $("#PageSizeSet").val(),
        'PageIndex': $("#PageIndex").val(),
        'SortDirection': $("#SortDirct").val(),
        'SortColumn': $("#currentSortCol").val(),
        'access_token': localStorage.getItem('token'),
        'DoctorName': $('#txtReferredBy').val(),
        'FromDate': $('#txtSearchFromDate').val(),
        'ToDate': $('#txtSearchToDate').val(),
        'Disposition': $("#ddlDisposition").val(),
        'MessageStatus': $("#ddlStatus").val(),
        'PatientName': $("#txtPatient").val(),
        'Message': $("#txtMessage").val(),
        'MessageFrom': $("#hdnReferralFrom").val()
    };
    performAjax({
        url: "/Dashboard/GetConversationView",
        type: "POST",
        async: true,
        data: { Obj: obj },
        success: function (data) {
            if (data != null) {

            }
        },
        error: function () {
            alert("Oops...this pages need to reload.  Please try again.");
        }
    });
}
$("#ddlMessageFrom").on('change', function () {
    if ($(this).val() == '1') {
        ChangeReceive();
    } else if ($(this).val() == '2') {
        ChangeSent();
    } else {
        ChangeAll();
    }
})

$(document).ready(function () {
    $("#add-Note").on('hidden.bs.modal', function () {
        var MessageId = $(this).find("#MessageId").val();
        var CurrentDispositonStatus = 0;
        if ($.trim($("#ddl_" + MessageId).data("val")).length > 0) {
            CurrentDispositonStatus = parseInt($("#ddl_" + MessageId).data("val"));
        }
        dismissDispositionstatus(MessageId, CurrentDispositonStatus);
    });
});
function ChangeAction(id) {
    
    if ($("#" + id).val() == 1) {
        window.location.href = $("#" + id + " :selected").data("url");
        //Redirect($("#" + id + " :selected").data("url"));
    } else if ($("#" + id).val() == 2) {        
        OpenDeletePopUpForMessage($("#" + id + " :selected").data("messageid"), 2, $("#" + id + " :selected").data("messagefrom"), $("#" + id + " :selected").data("isownreferral"));
    } else if ($("#" + id).val() == 3) {
        //downloadConAsPdf($("#" + id + " :selected").data("messageid"))
        window.location.href = $("#" + id + " :selected").data("url");
    }
    else {
        return false;
    }
    $("#" + id).val(0);
}

function downloadConAsPdf(messageId) {
    if (messageId != null && messageId !=0) {
        performAjax({
            url: "/Dashboard/ConversationViewToPdf",
            type: "GET",
            async: true,
            data: { MessageId: messageId },
            success: function (data) {
                if (data != null) {

                }
            },
            error: function () {
                alert("Oops...this pages need to reload.  Please try again.");
            }
        });
    };
}


function changeCoordinator(messageId, memberId) {
    
    let memberName = $("#ddlCoordinator_" + messageId + " option:selected").text();
    if (memberId != "") {    
        let objCoordinator = { MessageId: messageId, MemberId: memberId }
        performAjax({
            url: "/Dashboard/ChangeCoordinator",
            type: "POST",
            async: true,
            data: objCoordinator,
            success: function (data) {
                if (data != null) {
                    $.toaster({ priority: 'success', title: 'success', message: 'Referral assign to ' + memberName + ' successfully' });
                    $('body select').prop('disabled', true);
                    setTimeout(function () {
                        $('body select').prop('disabled', false);
                    }, 4000);
                }
            },
            error: function () {
                alert("Oops...this pages need to reload.  Please try again.");
            }
        });
    } else {
        $("#ddlCoordinator_" + messageId).val(parseInt($("#ddlCoordinator_" + messageId).data("memberid")))
    }
};