﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace OneClickReferral.Helpers
{

    public enum Specialty
    {
        OralSurgery = 1,        /* -- MapFieldName:OSIsEnable & SortOrder:1 --*/
        Periodontics = 2,       /* -- MapFieldName:PDIsEnable & SortOrder:2 --*/
        Orthodontics = 3,       /* -- MapFieldName:ODIsEnable & SortOrder:3 --*/
        Prosthodontics = 4,     /* -- MapFieldName:PTIsEnable & SortOrder:4 --*/
        Endodontics = 5,        /* -- MapFieldName:EDIsEnable & SortOrder:5 --*/
        Pediatrics = 6,          /* -- MapFieldName:PRIsEnable & SortOrder:6 --*/
        Implants = 10,           /* -- MapFieldName:INIsEnable & SortOrder:7 --*/
        GeneralDentistry = 11,   /* -- MapFieldName:GDIsEnable & SortOrder:8 --*/
        DentalLab = 12,           /* -- MapFieldName:DLIsEnable & SortOrder:9 --*/
        Radiology = 13,         /* -- MapFieldName:RDIsEnable & SortOrder:10 --*/
        Other = 14,             /* -- MapFieldName:OTIsEnable & SortOrder:13 --*/
        PersonalInformation = 15,       /* -- MapFieldName:PIIsEnable & SortOrder:15 --*/
        InsuranceCoverage = 16,     /* -- MapFieldName:ICIsEnable & SortOrder:16 --*/
        MedicalHistory = 17,        /* -- MapFieldName:MHIsEnable & SortOrder:17 --*/
        DentalHistory = 18,     /* -- MapFieldName:DHIsEnable & SortOrder:18 --*/
        TreatmentPerformed = 19,        /* -- MapFieldName:TPEnable & SortOrder:11 --*/
        PediatricDentistry = 20,        /* -- MapFieldName:PEDEnable & SortOrder:12 --*/
        Comment = 21        /* -- MapFieldName:CMTEnable & SortOrder:14 --*/


    }

    public class ZipwhipApiURL
    {
        public const string Webhook_URL = "http://localhost:56697/zipwhip/WebhookForReceiveSMS";

        public const string Messaging_Login = "https://api.zipwhip.com/user/login";
        public const string Provisioning_Eligible = "https://provision.zipwhip.com/api/20140925/provision/eligible";
        public const string Provisioning_Add = "https://provision.zipwhip.com/api/20140925/provision/add";
        public const string Provisioning_Status = "https://provision.zipwhip.com/api/20140925/provision/status";
        public const string Provisioning_Update = "https://provision.zipwhip.com/api/20140925/provision/update";
        public const string Provisioning_Delete = "https://provision.zipwhip.com/api/20140925/provision/delete";
    }

    public enum SMSHistoryStatus
    {
        Inserting = 0,
        Sent = 1,
        Received = 2,
        Failed = 3,
        Delivered = 4,
        Queue = 5,
    }

    public enum SMSHistoryMessageType
    {
        Send = 1,
        Received = 2,
    }
}