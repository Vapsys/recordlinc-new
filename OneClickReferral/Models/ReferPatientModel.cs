﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    /// <summary>
    /// For refer patient model for dentrix button functionality
    /// </summary>
    public class ReferPatientModel
    {
        public string Token { get; set; }
        public PMSPatientDetails PMSPatientDetails { get; set; }
        public PMSProviderDetails PMSProviderDetails { get; set; }
    }

    /// <summary>
    /// For dentrix button functionality api response
    /// </summary>
    public class ResponseReferPatientModel
    {
        public bool IsIntegration { get; set; }
        public int? ToColleague { get; set; }
        public int? UserId { get; set; }
        public string Token { get; set; }
        public int? PatientId { get; set; }
        
    }

    /// <summary>
    /// For doctor login 
    /// </summary>
    public class DoctorLoginForPMS
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }

    /// <summary>
    /// for Validation response
    /// </summary>
    public class ValidationResponse
    {
        public ValidationResponse()
        {
            Status = false;
            Message = string.Empty;
        }
        public bool Status { get; set; }
        public string Message { get; set; }
    }

    /// <summary>
    /// for PMS Send Referral Details
    /// </summary>
    public class PMSSendReferralDetails
    {
        public int PatientId { get; set; }
        public int ColleagueIds { get; set; }
    }
}