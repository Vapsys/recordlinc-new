﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace OneClickReferral.Models
{
    /// <summary>
    /// Dentist Profile for OCR AND Recordlinc Profile page.
    /// </summary>
    public class DentistProfileViewModel
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }
        public string OfficeName { get; set; }
        public string Title { get; set; }
        public string ImageName { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string DentrixProviderId { get; set; }
        public string PublicProfile { get; set; }
        public string WebsiteURL { get; set; }
        public int TeamMemberUserId { get; set; }
        public string Location { get; set; }
        public int ProfileComplete { get; set; }
        public string Profilepercentage { get; set; }
        public string RemainList { get; set; }
        public string Institute { get; set; }
        public string MemberShip { get; set; }
        public string EncryptUserId { get; set; }
        public int LocationId { get; set; }
        public string Salutation { get; set; }
        public string specialtyIds { get; set; }
        public List<Banner> lstBanner { get; set; }
        public List<Gallery> lstGallary { get; set; }
        public List<Insurance> lstInsurance { get; set; }
        public string GallaryPath { get; set; }
        public List<AddressDetails> lstDoctorAddressDetails = new List<AddressDetails>();
        public List<AddressDetails> lstDoctorSortedAddressDetails = new List<AddressDetails>();
        public List<EducationandTraining> lstEducationandTraining = new List<EducationandTraining>();
        public List<ProfessionalMemberships> lstProfessionalMemberships = new List<ProfessionalMemberships>();
        public List<SpeacilitiesOfDoctor> lstSpeacilitiesOfDoctor = new List<SpeacilitiesOfDoctor>();
        public List<Procedure> lstProcedure = new List<Procedure>();
        public List<LicenseDetailsForDoctor> licensedetails = new List<LicenseDetailsForDoctor>();
        public LicenseDetailsForDoctor objLicense { get; set; }
        public SectionPublicProfileViewModel ObjProfileSection { get; set; }
        public List<AddressDetails> lstDoctorAddressDetailsByAddressInfoID = new List<AddressDetails>();
        public List<SocialMediaForDoctorViewModel> lstGetSocialMediaDetailByUserId = new List<SocialMediaForDoctorViewModel>();
        public List<EducationandTraining> lstEducationandTrainingForDoctorById = new List<EducationandTraining>();
        public List<ProfessionalMemberships> lstProfessionalMembershipForDoctorById = new List<ProfessionalMemberships>();
        public List<WebsiteDetails> lstsecondarywebsitelist = new List<WebsiteDetails>();
        public List<TimeZonesViewModel> lstTimeZone { get; set; }
        public List<TeamMemberDetailsForDoctorViewModel> lstTeamMemberDetailsForDoctor { get; set; }
    }
    public class WebsiteDetails
    {
        public int SecondaryWebsiteId { get; set; }
        public int UserId { get; set; }
        public string SecondaryWebsiteurl { get; set; }
    }
    public class WebSiteUrl
    {
        public string WebsiteUrl { get; set; }
        public int UserId { get; set; }
        public int? WebsiteId { get; set; }
    }
    public class LicenseDetailsForDoctor
    {
        public int UserId { get; set; }
        public string LicenseNumber { get; set; }
        public string LicenseState { get; set; }
        public System.DateTime? LicenseExpiration { get; set; }
        public string LicenseExpirationDisplay { get; set; }
    }
    public class Procedure
    {
        public Procedure()
        {
            List<Procedure> lstProcedure = new List<Procedure>();
        }
        public int ProcedureId { get; set; }
        public string ProcedureName { get; set; }
        public decimal CostPercentage { get; set; }
        public decimal StandardFees { get; set; }
        public bool Ischeck { get; set; }
    }
    public class EducationandTraining
    {
        public int Id { get; set; }
        public string Institute { get; set; }
        public string Specialisation { get; set; }
        public string YearAttended { get; set; }
    }
    public class ProfessionalMemberships
    {
        public int Id { get; set; }
        public string Membership { get; set; }
    }
    public class AddressDetails
    {
        public int AddressInfoID { get; set; }
        public string ExactAddress { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string EmailAddress { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public int ContactType { get; set; }
        public string Location { get; set; }
        public int TimeZoneId { get; set; }
        public string Mobile { get; set; }
        public string Website { get; set; }
        public string ExternalPMSId { get; set; }
        public string SchedulingLink { get; set; }
        public List<StateList> StateList { get; set; }
        public List<SelectListItem> CountryList { get; set; }
        public List<TimeZones> TimeZoneList { get; set; }
    }
    public class TimeZones
    {
        /// <summary>
        /// Time Zone Id
        /// </summary>
        public int TimeZoneId { get; set; }
        /// <summary>
        /// Time Zone Text
        /// </summary>
        public string TimeZoneText { get; set; }
    }
    public class Insurance
    {
        public Insurance()
        {
            List<Insurance> lstInsurance = new List<Insurance>();
        }
        public int InsuranceId { get; set; }
        public int UserId { get; set; }
        public bool Ischeck { get; set; }
        public int Member_InsuranceId { get; set; }
        [Display(Name = "Insurance Name")]
        public string Name { get; set; }
        [Display(Name = "Logo")]
        public string LogoPath { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Link")]
        public string Link { get; set; }
        public List<Insurance> lstInsurance { get; set; }
        public int CurrentPageNo { get; set; }
    }
    public class Banner
    {
        public int BannerId { get; set; }
        public int UserId { get; set; }
        [Display(Name = "Title")]
        [Required(ErrorMessage = "Please enter title")]
        public string Title { get; set; }
        [Display(Name = "Banner Image")]
        [Required(ErrorMessage = "Please choose image")]
        public string Path { get; set; }
        public string ImagePath { get; set; }
        public HttpPostedFileBase BannerImage { get; set; }
        [Display(Name = "Color")]
        [Required(ErrorMessage = "Please choose color")]
        public string ColorCode { get; set; }
        public int Position { get; set; }
    }
    public class TeamMemberDetailsForDoctorViewModel
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SpecialtyDescription { get; set; }
        public string PublicProfileUrl { get; set; }
        public string AccountName { get; set; }

    }
    public class Gallery
    {
        public int GallaryId { get; set; }
        public string VideoURL { get; set; }
        public HttpPostedFileBase File { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public int UserId { get; set; }
    }
    public class SectionPublicProfileViewModel
    {
        public int Userid { get; set; }
        public bool PatientForms { get; set; }
        public bool SpecialOffers { get; set; }
        public bool ReferPatient { get; set; }
        public bool Reviews { get; set; }
        public bool AppointmentBooking { get; set; }
        public bool PatientLogin { get; set; }
    }
    public class SocialMediaForDoctorViewModel
    {
        public string FacebookUrl { get; set; }
        public string LinkedinUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string GoogleplusUrl { get; set; }
        public string YoutubeUrl { get; set; }
        public string PinterestUrl { get; set; }
        public string BlogUrl { get; set; }
        public string InstagramUrl { get; set; }
        public string YelpUrl { get; set; }
    }
    public class TimeZonesViewModel
    {
        public int TimeZoneId { get; set; }
        public string TimeZoneText { get; set; }
    }
    /// <summary>
    /// Add Dentist Profile Image
    /// </summary>
    public class DentistProfileImage
    {
        /// <summary>
        /// Image Bottom side crop
        /// </summary>
        public double Bottom { get; set; }
        /// <summary>
        /// Image Right side crop
        /// </summary>
        public double Right { get; set; }
        /// <summary>
        /// Image Top side crop
        /// </summary>
        public double Top { get; set; }
        /// <summary>
        /// Image Left side crop
        /// </summary>
        public double Left { get; set; }
        /// <summary>
        /// File Name
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// Base 64 string of Image
        /// </summary>
        public string Base64URL { get; set; }
    }
}