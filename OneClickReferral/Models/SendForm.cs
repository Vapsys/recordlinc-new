﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class SendForm
    {
        /// <summary>
        /// PatientId of Patient
        /// </summary>
        public int PatientId { get; set; }
        /// <summary>
        /// Logged in Doctor Name
        /// </summary>
        public string DoctorName { get; set; }
        /// <summary>
        /// Patient Name
        /// </summary>
        public string PatientName { get; set; }
        /// <summary>
        /// Patient Email Address.
        /// </summary>
        public string PatientEmail { get; set; }
    }
}