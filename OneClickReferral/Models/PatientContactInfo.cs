﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class PatientContactInfo
    {

        public int PatientId { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }
        [Display(Name = "City")]
        public string City { get; set; }
        [Display(Name = "State")]
        public string State { get; set; }
        /// <summary>
        ///  State table list
        /// </summary>
       
        [Display(Name = "Emergency Contact Name")]
        public string EMGContactName { get; set; }

        [Display(Name = "ZipCode")]
        [RegularExpression(@"\d{5,6}$", ErrorMessage = "Invalid zip code.")]
        public string ZipCode { get; set; }

        [Display(Name = "Gender")]
        public int Gender { get; set; }

        [Display(Name = "Date of Birth")]
        //[DataType(DataType.DateTime)]
       // [DisplayFormat(DataFormatString = "{0:M/dd/yyyy}")]
       // [RegularExpression(@"^((((0[13578])|(1[02]))[\/]?(([0-2][0-9])|(3[01])))|(((0[469])|(11))[\/]?(([0-2][0-9])|(30)))|(02[\/]?[0-2][0-9]))[\/]?\d{4}$", ErrorMessage = "Please enter valid Date of Birth.")]
       // [DataType(DataType.DateTime,ErrorMessage = "Please enter valid Date of Birth.")]
        public string BOD { get; set; }
        //[Display(Name = "Emergency Contact Phone Number")]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Emergency Phone number is invalid.")]
        public string EMGContactNo { get; set; }
        [Display(Name = "Primary Contact Phone Number")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Primary Phone number is invalid.")]
        public string PrimaryPhoneNo { get; set; }
        //[Display(Name = "Secondary Contact Phone Number")]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Secondary Phone number is invalid.")]
        public string SecondaryPhoneNo { get; set; }
    }

    public class TempFileAttachments
    {
        public int FileId { get; set; }
        public string FilePath { get; set; }
        public int FileFrom { get; set; }
        public string FileName { get; set; }
        public int FileExtension { get; set; }
        public int ComposeType { get; set; }
    }
}