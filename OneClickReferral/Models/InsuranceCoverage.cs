﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class InsuranceCoverage
    {
        public int ItHasInsuranceData { get; set; }
        public string PrimaryInsuranceCompany { get; set; }
        [Display(Name = "Primary Insurance Phone Number")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Phone number is invalid.")]
        public string PrimaryInsurancePhone { get; set; }
        public string PrimaryNameOfInsured { get; set; }
        public string PrimaryDateOfBirth { get; set; }
        public string PrimaryMemberID { get; set; }
        public string PrimaryGroupNumber { get; set; }
        public string SecondaryInsuranceCompany { get; set; }
        [Display(Name = "Secondary Insurance Phone Number")]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Phone number is invalid.")]
        public string SecondaryInsurancePhone { get; set; }
        public string SecondaryNameOfInsured { get; set; }
        public string SecondaryDateOfBirth { get; set; }
        public string SecondaryMemberID { get; set; }
        public string SecondaryGroupNumber { get; set; }
        public string EMGContactName { get; set; }
        public string EMGContactNo { get; set; }
    }




}