﻿using BO.ViewModel;
using System.Net.Http;

namespace OneClickReferral.Models
{
    public class RepositoryBLL
    {
        /// <summary>
        /// Cotains root path of API.
        /// </summary>
        private string RootAPI;
        /// <summary>
        /// Object to do network calls.
        /// </summary>
        private HttpClient Client;
        /// <summary>
        /// Token to be passed to API.
        /// </summary>
        public string Token;
        /// <summary>
        /// Constructor that accepts Token.
        /// </summary>
        /// <param name="Token"></param>
        public RepositoryBLL(string Token, string BaseURL)
        {
            RootAPI = BaseURL;
            Client = new HttpClient();
            this.Token = Token;
        }
        /// <summary>
        /// Generic method to call API.
        /// </summary>
        /// <typeparam name="T">Response type.</typeparam>
        /// <typeparam name="I">Input type of last parameter</typeparam>
        /// <param name="url">URL of API to be called.</param>
        /// <param name="token">Token for calling that API</param>
        /// <param name="Method">Request method to call API (i.e. GET OR POST). By default it is POST.</param>
        /// <param name="param"> Input parameters to call API via POST. </param>
        /// <returns></returns>
        public APIResponseBase<T> CallAPI<T, I>(string url, string Method = "POST", I param = default(I))
        {
            try
            {
                HttpResponseMessage response = null;
                Client.DefaultRequestHeaders.Clear();
                Client.DefaultRequestHeaders.Add("access_token", Token);
                if (Method == "POST")
                    response = Client.PostAsJsonAsync(getAPIPath(url), param).Result;
                else
                    response = Client.GetAsync(getAPIPath(url)).Result;
                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var result = response.Content.ReadAsAsync<T>().Result;

                    return new APIResponseBase<T>()
                    {
                        IsSuccess = true,
                        Message = string.Empty,
                        Result = result,
                        StatusCode = (int)response.StatusCode
                    };
                }
                else
                {

                    return new APIResponseBase<T>()
                    {
                        IsSuccess = false,
                        Message = getErrorMessage(url, response),
                        Result = default(T),
                        StatusCode = (int)response.StatusCode
                    };
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        private string getAPIPath(string partialURL)
        {
            return RootAPI + partialURL;
        }

      
        private string getErrorMessage(string url, HttpResponseMessage response)
        {
            return $"Request Url : {url} Status code : {response.StatusCode} Error : {response.ReasonPhrase}";
        }
    }
}