﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class SpecialtyMaster
    {
        public SpecialtyMaster()
        {
            lstServices = new List<ServiceMaster>();
        }
        public int Speciality_Id { get; set; }
        public string Speciality_Name { get; set; }
        public string Speciality_MapFieldName { get; set; }
        public bool Speciality_IsEnable { get; set; }
        public List<ServiceMaster> lstServices { get; set; }
    }
    public class ServiceMaster
    {
        public int Service_Id { get; set; }
        public string Service_Name { get; set; }
        public string Service_MapFieldName { get; set; }
        public bool Service_IsEnable { get; set; }
        public string Service_Value { get; set; }
        public int Service_Speciality_Id { get; set; }
        public string Service_NameFor { get; set; }
    }
}