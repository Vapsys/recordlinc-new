﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class PatientInsuranceViewModel
    {
        public int PatientId { get; set; }
        public string PatientName { get; set; }
        public List<object> metadata { get; set; }
        public Payer payer { get; set; }
        public Provider provider { get; set; }
        public Subscriber subscriber { get; set; }
        public List<ActiveCoverage> activeCoverage { get; set; }
        public List<CoInsurance> coInsurance { get; set; }
        public List<Deductible> deductible { get; set; }
        public List<Limitation> limitations { get; set; }
        public List<Maximum> maximums { get; set; }
        public string onederfulId { get; set; }
    }

    public class Payer
    {
        public string payor_identification { get; set; }
        public string entity { get; set; }
        public string entity_qualifier { get; set; }
        public string name { get; set; }
    }

    //public class Reference
    //{
    //    public string tax_id { get; set; }
    //}

    public class Provider
    {
        public string npi { get; set; }
        public string entity { get; set; }
        public string entity_qualifier { get; set; }
        public string provider_code { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string name { get; set; }
        public List<Reference> reference { get; set; }
    }

    public class Reference
    {
        public string tax_id { get; set; }
        public string plan_network_identification_number { get; set; }
        public string group_number { get; set; }
        public string group_number_description { get; set; }
        public string description { get; set; }
    }

    //public class Reference
    //{
    //    public string plan_network_identification_number { get; set; }
    //}

    public class DateTimePeriod
    {
        public string date_string { get; set; }
        public string date_qualifier { get; set; }
    }

    public class Subscriber
    {
        public string entity { get; set; }
        public string entity_qualifier { get; set; }
        public string InformationReceiverAdditionalAddressLine { get; set; }
        public string dob { get; set; }
        public string gender { get; set; }
        public bool is_subscriber { get; set; }
        public string relationship { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public Reference reference { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip_code { get; set; }
        public List<DateTimePeriod> date_time_periods { get; set; }
        public string member_id { get; set; }
    }

    public class ActiveCoverage
    {
        public string coverage_level { get; set; }
        public string insurance_type { get; set; }
        public string plan_coverage_description { get; set; }
        public List<string> messages { get; set; }
    }





    public class CoInsurance
    {
        public string service_type { get; set; }
        public string in_network_status { get; set; }
        public string percentage_string { get; set; }
        public string percentage_for_provider { get; set; }
        public string percentage { get; set; }
        public string service_or_procedure_code { get; set; }
        public List<Reference> reference { get; set; }
        public string plan_coverage_description { get; set; }
        public string procedure_code { get; set; }
    }


    public class Deductible
    {
        public string in_network_status { get; set; }
        public string amount { get; set; }
        public string procedure_code { get; set; }
        public string service_or_procedure_code { get; set; }
        public List<Reference> reference { get; set; }
        public string plan_coverage_description { get; set; }
        public string service_type { get; set; }
        public string time_qualifier { get; set; }
        public string coverage_level { get; set; }
    }

    public class HealthServicesDescription
    {
        public string description { get; set; }
    }

    public class AgeLimit
    {
        public string qualifier { get; set; }
        public string quantity { get; set; }
    }



    public class Limitation
    {
        public string service_type { get; set; }
        public string service_or_procedure_code { get; set; }
        public List<HealthServicesDescription> health_services_description { get; set; }
        public string quantity_description_string { get; set; }
        public AgeLimit age_limit { get; set; }
        public List<DateTimePeriod> date_time_periods { get; set; }
        public string procedure_code { get; set; }
    }



    public class Maximum
    {
        public string time_qualifier { get; set; }
        public string coverage_level { get; set; }
        public string service_type { get; set; }
        public string in_network_status { get; set; }
        public string amount { get; set; }
        public string service_or_procedure_code { get; set; }
        public List<Reference> reference { get; set; }
        public string plan_coverage_description { get; set; }
    }
}