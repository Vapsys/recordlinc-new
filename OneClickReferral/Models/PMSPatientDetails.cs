﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class PMSPatientDetails
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Gender { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string PatientId { get; set; }
        public string Mobile { get; set; }
    }
    public class PMSProviderDetails
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Speciality { get; set; }
        public string ProviderId { get; set; }
        public string Mobile { get; set; }
    }
}