﻿using BO.Models;
using System.Collections.Generic;

namespace OneClickReferral.Models
{
    /// <summary>
    /// Created this model for the new conversation UI related changes.
    /// This UI has a referral full details as well as on the Office commnication has a disposition related details 
    /// </summary>
    public class ConversationView
    {
        /// <summary>
        /// This property has a referral full details into the record
        /// </summary>
        public ReferralMessage referralMessage { get; set; }
        /// <summary>
        /// This property has a office communication related details.
        /// </summary>
        public List<OneClickReferralDetail> oneClickReferralDetails { get; set; }

        public List<SMSDetails> smsHistory { get; set; }
    }
}
