﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class ReferralFilter
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int SortDirection { get; set; }
        public int SortColumn { get; set; }
        public string access_token { get; set; }
        public string DoctorName { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int Disposition { get; set; }
        public string EncryptedUserId { get; set; }
        public int MessageStatus { get; set; }
        public string Message { get; set; }
        public string PatientName { get; set; }
        public int ReferralFrom { get; set; }
        public string TimeZoneSystemName { get; set; }
    }
    public class UnreadMessageCount
    {
        public int Count { get; set; }
    }
    public class FilterMessageConversation
    {
        /// <summary>
        /// MessageId is Null-able 
        /// </summary>
        public int? MessageId { get; set; }
        /// <summary>
        /// UserId 
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Page Size
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// Page Index
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// Sort Column
        /// </summary>
        public int SortColumn { get; set; }
        /// <summary>
        /// Sort Direction
        /// </summary>
        public int SortDirection { get; set; }
        /// <summary>
        /// Doctor Name
        /// </summary>
        public string DoctorName { get; set; }
        /// <summary>
        /// From Date
        /// </summary>
        public DateTime? FromDate { get; set; }
        /// <summary>
        /// To Date
        /// </summary>
        public DateTime? Todate { get; set; }
        /// <summary>
        /// Disposition
        /// </summary>
        public string Disposition { get; set; }
        /// <summary>
        /// Message
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Message Status
        /// </summary>
        public int MessageStatus { get; set; }
        /// <summary>
        /// Patient Name
        /// </summary>
        public string PatientName { get; set; }
        /// <summary>
        /// Message From
        /// </summary>
        public int MessageFrom { get; set; }
        /// <summary>
        /// Access token of User logged in
        /// </summary>
        public string access_token { get; set; }
        /// <summary>
        /// Added SearchDisposition for Select2.
        /// </summary>
        public string SearchDisposition { get; set; }
        /// <summary>
        ///  Patient Insurance Status 
        /// </summary>
        public int InsuranceStatus { get; set; }
    }

    public class Coordinator{
        public int MessageId { get; set; }
        public int MemberId { get; set; }
    }
}