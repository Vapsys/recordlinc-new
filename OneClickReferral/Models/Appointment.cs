﻿using BO.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class Appointment
    {
        public string Search_FirstName { get; set; }
        public string Search_LastName { get; set; }
        public DateTime? Search_DOB { get; set; }
        public string Search_Email { get; set; }
        public string Search_Phone { get; set; }
    }
}