﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class SpecialityService
    {
        public SpecialityService()
        {
            lstPatient = new List<AddPatient>();
        }
        public int ServicesId { get; set; }
        public string ServiceName { get; set; }
        public int SpecialityId { get; set; }
        public int DentialServiceId { get; set; }
        public string SpecialityName { get; set; }
        public bool OSIsEnable { get; set; }
        public bool PDIsEnable { get; set; }
        public bool ODIsEnable { get; set; }
        public bool PRIsEnable { get; set; }
        public bool EDIsEnable { get; set; }
        public bool PTIsEnable { get; set; }
        public bool INIsEnable { get; set; }
        public bool GDIsEnable { get; set; }
        public bool DLIsEnable { get; set; }
        public bool RDIsEnable { get; set; }
        public bool OTIsEnable { get; set; }
        public bool PIIsEnable { get; set; }
        public bool ICIsEnable { get; set; }
        public bool MHIsEnable { get; set; }
        public bool DHIsEnable { get; set; }
        public bool TPEnable { get; set; }
        public bool PEDEnable { get; set; }
        public bool Location { get; set; }
        public bool DateOfBirth { get; set; }
        public bool CMTEnable { get; set; }
        public bool Lastname { get; set; }
        public bool Email { get; set; }
        public List<AddPatient> lstPatient { get; set; }
    }

    public class AddPatient
    {
        public int Id { get; set; } // Key 
        public string ControlName { get; set; }
        public int? ParentFieldId { get; set; }
        public string MapFieldName { get; set; }
        public bool IsEnable { get; set; }
        public int SpecialityId { get; set; }
        public int SortOrder { get; set; }

    }
}