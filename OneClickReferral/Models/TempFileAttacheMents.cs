﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class TempFileAttacheMents
    {
        public int FileId { get; set; }
        public string FilePath { get; set; }
        public int FileFrom { get; set; }
        public string FileName { get; set; }
        public int FileExtension { get; set; }
        public int ComposeType { get; set; }
    }
}