﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class Logger
    {
        private string _LogfilePath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["LogFilePath"]);
        private string _LogFileName = "OCRLogFile";
        private Object thisLock = new Object();
        private bool _IsLoggingEnabled = true;
        public bool IsLoggingEnabled
        {
            get { return _IsLoggingEnabled; }
            set
            {
                if (value != _IsLoggingEnabled)        //  Only display if it changes.
                    WriteLog("Logging is being set to " + value.ToString(), true);
                _IsLoggingEnabled = value;
            }
        }
        public void WriteLog(string prmData, bool prmForce)
        {
            string fullFileName = ".." + _LogfilePath + "\\" + _LogFileName + ".log";
            try
            {
                if (_IsLoggingEnabled == true || prmForce == true)
                {
                    string Path = HttpContext.Current.Server.MapPath(_LogfilePath + _LogFileName + ".log");
                    lock (thisLock)
                    {
                        // If the log file is way too large, rename it and start over. 

                        if (File.Exists(Path))
                        {
                            FileInfo f = new FileInfo(Path);
                            if (f.Length > 5000000L)        // 5 MB
                            {
                                // Rename the current file.
                                string newFileName = _LogfilePath + " Ending " + BuildTimeWithoutSlashes() + ".oldlog";
                                File.Move(Path, newFileName);
                            }
                        }

                        using (StreamWriter sw = new StreamWriter(Path, true))
                        {
                            sw.WriteLine(DateTime.Now.ToString() + " " + prmData);
                            sw.Flush();
                            sw.Close();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string errMsg = "Logger Failed: Check to see if someone is holding the log file open. " + prmData + " " +
                        ex.GetBaseException().Message + " " +
                        ex.StackTrace;

            }
        }
        private string BuildTimeWithoutSlashes()
        {
            string x = DateTime.Now.Year.ToString() + " " +
                        AddLeadingZero(DateTime.Now.Month.ToString()) + " " +
                        AddLeadingZero(DateTime.Now.Day.ToString()) + " " +
                        AddLeadingZero(DateTime.Now.Hour.ToString()) +
                        AddLeadingZero(DateTime.Now.Minute.ToString()) +
                        AddLeadingZero(DateTime.Now.Second.ToString());
            return x;
        }
        // Need leading zeroes to sort properly in Windows Explorer.
        private string AddLeadingZero(string prmNumber)
        {
            return prmNumber.Length == 1 ? "0" + prmNumber : prmNumber;
        }
    }
}