﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class ReferralCategory
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int SubCategoryId { get; set; }
        public string SubCategoryName { get; set; }
        public string SubCategoryHeaderText { get; set; }
        public string SubCategoryValue { get; set; }
        public int SubSubCategoryId { get; set; }
        public string SubSubCategoryName { get; set; }
        public string SubSubCategoryHeaderText { get; set; }
        public string SubSubCategoryValue { get; set; }
    }
}