﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class MemberPlanDetail
    {
        public int FeatureId { get; set; }
        public int MemberShipId { get; set; }
        public bool? CanSee { get; set; }
        public string MaxCount { get; set; }
    }
    public class PermissionResult
    {
        public bool HasPermission { get; set; }
        public bool DoesMembershipAllow { get; set; }
        public string Message { get; set; }
        public int UsedCount { get; set; }
        public int MaxCount { get; set; }
        public int FeatureId { get; set; }
    }
}