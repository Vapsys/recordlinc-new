﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace OneClickReferral.Models
{
    /// <summary>
    /// Integration settings page for OCR
    /// </summary>
    public class Integration
    {
        /// <summary>
        /// Integration Master Id
        /// </summary>
        public int IntegrationId { get; set; }
        /// <summary>
        /// Integration Company Name
        /// </summary>
        public string IntegrationName { get; set; }
        /// <summary>
        /// List of Connectors for this Integration
        /// </summary>
        public List<CompanyIntegrationDetail> ConnectorList { get; set; }
        /// <summary>
        /// Integration Provider Id
        /// </summary>
        public string IntegrationProviderId { get; set; }
        /// <summary>
        /// List of Locations of Dentist while Adding new connection
        /// </summary>
        public List<SelectListItem> LocationList { get; set; }
    }
    /// <summary>
    /// Company Integration Details
    /// </summary>
    public class CompanyIntegrationDetail
    {
        /// <summary>
        /// Company Connector Id
        /// </summary>
        public int ConnectorId { get; set; }
        /// <summary>
        /// Connector Key / GUID Key
        /// </summary>
        public string ConnectorKey { get; set; }
        /// <summary>
        /// Connector Location Id
        /// </summary>
        public int LocationId { get; set; }
        /// <summary>
        /// Company Integration Id
        /// </summary>
        public int IntegrationId { get; set; }
        /// <summary>
        /// Integration Name
        /// </summary>
        public string IntegrationName { get; set; }
        /// <summary>
        /// List of Locations of Dentist
        /// </summary>
        public List<SelectListItem> LocationList { get; set; }
        /// <summary>
        /// Show the Connector is Enable/Disable
        /// </summary>
        public bool IsDisable { get; set; }
    }

    public class SaveIntegration
    {
        public string[] Integration { get; set; }

    }
}