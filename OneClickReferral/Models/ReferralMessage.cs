﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class ReferralMessage
    {
        public List<ReferralMessageDetails> ReferralMessageList { get; set; }
        public List<ReferralCategory> ReferralCategory { get; set; }
    }
}