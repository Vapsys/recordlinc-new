﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class EnableHasButton
    {
        public int ReceiverId { get; set; }
        public string access_token { get; set; }
    }
    /// <summary>
    /// Enable/Disable Multiple Has button functionality.
    /// </summary>
    public class UpdateHasButton
    {
        /// <summary>
        /// Multiple Colleagues Id
        /// </summary>
        public int[] ColleaguesId { get; set; }
        /// <summary>
        /// IsEnable/Disable functionality.
        /// </summary>
        public bool IsEnable { get; set; }
        /// <summary>
        /// User access_token
        /// </summary>
        public string access_token { get; set; }
    }
}