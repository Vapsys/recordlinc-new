﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class NormalMessageDetails
    {
        public int MessageId { get; set; }
        public int SenderId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string CreationDate { get; set; }
        public bool IsPatientMessage { get; set; }
        public bool Status { get; set; }
        public string Body { get; set; }
        public string SenderEmail { get; set; }
        public bool IsSentMessage { get; set; }
        public bool Isread { get; set; }
        public string ReceiverEmail { get; set; }
        public string AttachedPatientsId { get; set; }
        public List<MessageAttachment> lstMessageAttachment = new List<MessageAttachment>();
        public List<AttachedPatientDetails> lstAttachedPatient = new List<AttachedPatientDetails>();
        public int MsgStatus { get; set; }
    }
    public class MessageAttachment
    {
        public int MessageId { get; set; }
        public int AttachmentId { get; set; }
        public string AttachmentName { get; set; }
        public string AttachementFullName { get; set; }
        public bool MessageStatus { get; set; }
        public int PatientId { get; set; }
        public int ReferralCardId { get; set; }
        public bool IsDocument { get; set; }
    }
    public class AttachedPatientDetails
    {
        public int AttachedPatientId { get; set; }
        public string AttachedPatientFirstName { get; set; }
        public string AttachedPatientLastName { get; set; }
    }
    public static class FileExtension
    {

        public static string CheckFileExtenssion(string DocName)
        {
            try
            {
                string Ext = string.IsNullOrEmpty(DocName) ? "" : Path.GetExtension(DocName);
                switch (Ext.ToLower())
                {
                    case ".doc": return "../../Content/images/word.png";
                    case ".docx": return "../../Content/images/word.png";
                    case ".xls": return "../../Content/images/excel.png";
                    case ".csv": return "../../Content/images/excel.png";
                    case ".xlsx": return "../../Content/images/excel.png";
                    case ".pdf": return "../../Content/images/pdf.png";
                    case ".dex": return "../../Content/images/dex.png";
                    case ".dcm": return "../../Content/images/dcm.png";
                    case ".tif": return "../../Content/images/img_1.jpg";
                    case ".TIF": return "../../Content/images/img_1.jpg";
                    case ".pspimage": return "../../Content/images/img_1.jpg";
                    case ".thm": return "../../Content/images/img_1.jpg";
                    default: return "../../Content/images/img_1.jpg";
                }
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string CheckFileExtenssionOnReferral(string DocName)
        {
            try
            {
                string Ext = string.IsNullOrEmpty(DocName) ? "" : Path.GetExtension(DocName);
                switch (Ext.ToLower())
                {
                    case ".doc": return "../../Content/images/word.png";
                    case ".docx": return "../../Content/images/word.png";
                    case ".xls": return "../../Content/images/excel.png";
                    case ".csv": return "../../Content/images/excel.png";
                    case ".xlsx": return "../../Content/images/excel.png";
                    case ".dex": return "../../Content/images/dex.png";
                    case ".pdf": return "../../Content/images/pdf.png";
                    case ".dcm": return "../../Content/images/dcm.png";
                    case ".yuv": return "../ImageBank/" + DocName + "";
                    default: return "../ImageBank/" + DocName + "";
                }
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string CheckFileExtenssionOnlyThumbName(string DocName)
        {
            try
            {
                string Ext = string.IsNullOrEmpty(DocName) ? "" : Path.GetExtension(DocName);
                switch (Ext.ToLower())
                {
                    case ".doc": return "word.png";
                    case ".docx": return "word.png";
                    case ".xls": return "excel.png";
                    case ".csv": return "excel.png";
                    case ".xlsx": return "excel.png";
                    case ".dex": return "dex.png";
                    case ".pdf": return "pdf.png";
                    case ".dcm": return "dcm.png";
                    case ".yuv": return "img_1.jpg";
                    case ".tif": return "img_1.jpg";
                    case ".TIF": return "img_1.jpg";
                    case ".thm": return "img_1.jpg";
                    case ".pspimage": return "img_1.jpg";
                    default: return "img_1.jpg";
                }
            }
            catch (Exception)
            {
                return "";
            }
        }
        
    }
    public static class DateFuncations
    {
        public static string ConvertToDate(DateTime date, int flag)
        {
            string dateformat = string.Empty;
            switch (flag)
            {
                case (int)BO.Enums.Common.DateFormat.GENERAL:
                    dateformat = "M/d/yyyy";
                    break;
                default:
                    dateformat = "M/d/yyyy";
                    break;
            }
            string Date = date.ToString(dateformat).Replace('-', '/');
            return Date;
        }

        //RM-294 below method added to add leading zero in date at AddPatient
        public static string ConvertToDateNew(DateTime date, int flag)
        {
            string dateformat = string.Empty;
            switch (flag)
            {
                case (int)BO.Enums.Common.DateFormat.GENERAL:
                    dateformat = "MM/dd/yyyy";
                    break;
                default:
                    dateformat = "MM/dd/yyyy";
                    break;
            }
            string Date = date.ToString(dateformat).Replace('-', '/');
            return Date;
        }


        public static string ConvertToDateTime(DateTime date, int flag)
        {
            string dateformat = string.Empty;
            switch (flag)
            {
                case (int)BO.Enums.Common.DateFormat.GENERAL:
                    dateformat = "M/d/yyyy h:mm tt";
                    break;
                default:
                    dateformat = "M/d/yyyy h:mm tt";
                    break;
            }
            string Date = date.ToString(dateformat).Replace('-', '/');
            return Date;
        }

        public static string ConvertToTime(DateTime date, int flag)
        {
            string dateformat = string.Empty;
            switch (flag)
            {
                case (int)BO.Enums.Common.DateFormat.GENERAL:
                    dateformat = "h:mm tt";
                    break;
                default:
                    dateformat = "h:mm tt";
                    break;
            }
            string Date = date.ToString(dateformat);
            return Date;
        }
    }
}