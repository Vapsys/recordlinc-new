﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class DeleteMessage
    {
        /// <summary>
        /// The Message Id which you want to Delete.
        /// </summary>
        public int MessageId { get; set; }
        /// <summary>
        /// The MessageDisplay Type is for Sent or Received Message 2 = Received 3 = Sent
        /// </summary>
        public int MessageDisplayType { get; set; }
        /// <summary>
        /// The UserId which is associate with Message
        /// </summary>
        public string access_token { get; set; }
        /// <summary>
        /// This identify the Message is sent or Received.
        /// </summary>
        public string MessageFrom { get; set; }
    }
}