﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class ReferralMessageFilter
    {
        public int MessageId { get; set; }
        public string MessageType { get; set; }
        public string access_token { get; set; }
        public int MessageTypeId { get; set; }
        public bool IsFromPDF { get; set; }
    }
}