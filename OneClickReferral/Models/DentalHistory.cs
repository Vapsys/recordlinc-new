﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class DentalHistory
    {
        public string txtQue5 { get; set; }
        public string txtQue6 { get; set; }
        public string txtQue7 { get; set; }
        public string txtQue12 { get; set; }

        public string txtQue1 { get; set; }
        public string txtQue2 { get; set; }
        public string txtQue3 { get; set; }
        public string txtQue4 { get; set; }
         
        public string txtQue5a { get; set; }
        
         
        public bool rdQue7a { get; set; }
        public string rdQue8 { get; set; }
        public string txtQue9a { get; set; }
        public string rdQue9 { get; set; }
        public string rdQue10 { get; set; }
        public bool rdoQue11aFixedbridge { get; set; }
        public bool rdoQue11bRemoveablebridge { get; set; }
        public bool rdoQue11cDenture { get; set; }
        public bool rdQue11dImplant { get; set; }
         
        public bool DhQue5 { get; set; }
        public bool rdQue15 { get; set; }
        public bool rdQue16 { get; set; }
        public bool rdQue17 { get; set; }
        public bool rdQue18 { get; set; }
        public bool rdQue19 { get; set; }
        public bool chkQue20_1 { get; set; }
        public bool chkQue20_2 { get; set; }
        public bool chkQue20_3 { get; set; }
        public bool chkQue20_4 { get; set; }
        public bool rdQue21 { get; set; }
        public bool rdQue20 { get; set; }
        public string txtQue21a { get; set; }
        public string txtQue22 { get; set; }
        public string txtQue23 { get; set; }
        public bool rdQue24 { get; set; }
        public string txtQue26 { get; set; }
        public string rdQue27 { get; set; }
        public bool rdQue28 { get; set; }
        public string txtQue28a { get; set; }
        public string txtQue28b { get; set; }
        public string txtQue28c { get; set; }
        public string txtQue29 { get; set; }
        public string txtQue30 { get; set; }
        public string txtQue31 { get; set; }
        public string rdQue30 { get; set; }
        public string txtComments { get; set; }
        public string txtDigiSign { get; set; }
        public string txtQue11dAge { get; set; }
        public string txtQue12a { get; set; }
        public bool rdQue23 { get; set; }
        public bool rdQue25 { get; set; }
        public string firstvistdate { get; set; }
        public string Reasonforfirstvisit { get; set; }
        public string Dateoflastvisit { get; set; }
        public string Reasonforlastvisit { get; set; }
        public string Dateofnextvisit { get; set; }
        public string Reasonfornextvisit { get; set; }
    }
}