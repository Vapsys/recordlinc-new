﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class PatientProfileImage
    {
        /// <summary>
        /// Patient Id
        /// </summary>
        public int PatientId { get; set; }
        /// <summary>
        /// Image Bottom side crop
        /// </summary>
        public double Bottom { get; set; }
        /// <summary>
        /// Image Right side crop
        /// </summary>
        public double Right { get; set; }
        /// <summary>
        /// Image Top side crop
        /// </summary>
        public double Top { get; set; }
        /// <summary>
        /// Image Left side crop
        /// </summary>
        public double Left { get; set; }
        public string FileName { get; set; }
        public string Base64URL { get; set; }
    }
}