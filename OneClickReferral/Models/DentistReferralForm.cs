﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class DentistReferralForm
    {
        public DentistReferralForm()
        {
            RefFormSetting = new List<ReferralFormSetting>();
            DentistReferral = new List<DentistReferral>();
        }
        public bool IsFirstName { get; set; }
        public bool IsLastName { get; set; }
        public bool IsPatientPhone { get; set; }
        public bool IsPatientEmail { get; set; }
        public bool IsDateofBirth { get; set; }
        public bool IsInsuranceProvider { get; set; }
        public bool IsDisplayLocation { get; set; }
        public int UserId { get; set; }
        public List<ReferralFormSetting> RefFormSetting { get; set; }
        public List<DentistReferral> DentistReferral { get; set; }
    }

    /// <summary>
    /// Get Dentist Referral Setting Data
    /// </summary>
    public class DentistReferral
    {
        /// <summary>
        /// Primary key for Dentist Referral setting
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Field name for Dentist Referral setting
        /// </summary>
        public string ControlName { get; set; }

        /// <summary>
        /// Map Field name for Dentist Referral setting
        /// </summary>
        public string MapFieldName { get; set; }

        /// <summary>
        /// IsEnable field for Enable/Disable setting
        /// </summary>
        public bool IsEnable { get; set; }

        /// <summary>
        /// SortOrder field for Dentist Referral setting
        /// </summary>
        public int SortOrder { get; set; }

        /// <summary>
        /// UserId Field For Manage Orderby
        /// </summary>
        public int UserId { get; set; }
    }

    public class ReferralFormSetting
    {
        public ReferralFormSetting()
        {
            RefService = new List<ReferralFormServices>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? IsEnable { get; set; }
        public int OrderBy { get; set; }
        public int SpecialityId { get; set; }
        public List<ReferralFormServices> RefService { get; set; }
    }
    public class ReferralFormServices
    {
        public ReferralFormServices()
        {
            lstReferralFormSubField = new List<ReferralFormSubField>();
        }
        public int Id { get; set; }
        public int ParentFieldId { get; set; }
        public string Name { get; set; }
        public bool? IsEnable { get; set; }
        public int OrderBy { get; set; }
        public int FieldId { get; set; }
        public int SpecialityId { get; set; }
        public List<ReferralFormSubField> lstReferralFormSubField { get; set; }
    }
    public class ReferralFormSubField
    {
        public int ParentFieldId { get; set; }
        public int SubFieldId { get; set; }
        public int SpecialityId { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public bool? IsEnable { get; set; }
        public int OrderBy { get; set; }
        public int Id { get; set; }
    }

    public class SaveReferralForm
    {
        public List<ReferralFormSetting> RefFrmSetting { get; set; }
        public List<ReferralFormSetting> SpecialtySetting { get; set; }
        public List<ReferralFormServices> ServiceSetting { get; set; }
        public List<ReferralFormSubField> SubFieldSetting { get; set; }
    }

    /// <summary>
    /// Get property for save database
    /// </summary>
    public class FieldSequence
    {
        /// <summary>
        /// primary key
        /// </summary>
        public int KeyId { get; set; }
        /// <summary>
        /// parent primary key
        /// </summary>
        public int ParentKeyId { get; set; }
        /// <summary>
        /// get specialityid
        /// </summary>
        public int SpecialityId { get; set; }
        /// <summary>
        /// get servicesid
        /// </summary>
        public int ServicesId { get; set; }
        /// <summary>
        /// new sequence assign by user drag and drop
        /// </summary>
        public int NewSequence { get; set; }
        /// <summary>
        /// login user id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// table name for track update sequence which table 
        /// </summary>
        public string TableName { get; set; }

    }
}