﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class DeletePatient
    {
        /// <summary>
        /// Multiple Patient Ids with Comma Separated.
        /// </summary>
        public string[] MultiplePatientId { get; set; }
    }
    public class PatientDelete
    {
        public int RemovedCount { get; set; }
        public int UnRemovedCount { get; set; }
    }
}