﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class DispositionStatus
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }
}