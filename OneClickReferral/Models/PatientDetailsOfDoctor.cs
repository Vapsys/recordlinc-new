﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class PatientDetailsOfDoctor
    {
        public int PatientId { get; set; }
        public string AssignedPatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Age { get; set; }
        public string Phone { get; set; }
        public string DateOfBirth { get; set; }
        public string ExactAddress { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string ImageName { get; set; }
        public int OwnerId { get; set; }
        public int TotalRecord { get; set; }
        public int ColleaguesId { get; set; }
        public string ColleaguesName { get; set; }
        public string ReferredBy { get; set; }
        public int ReferredById { get; set; }
        public string Location { get; set; }
        public bool Voice { get; set; }
        public bool Text { get; set; }
        public bool ChkEmail { get; set; }
        public int AccountId { get; set; }
        public int MailCount { get; set; }
        public string LastSentDate { get; set; }
        public bool ItHasInsuranceData { get; set; }
    }
}