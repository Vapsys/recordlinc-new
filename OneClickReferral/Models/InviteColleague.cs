﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class InviteColleague
    {
        /// <summary>
        /// Comma Separated Email address.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// File Path of Invite Colleague.
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// Message for Colleague
        /// </summary>
        public string Message { get; set; }
    }

    public class ResponseMeassge
    {
        public string YourSelfInvite { get; set; }
        public string FailInvite { get; set; }
        public string XlsError { get; set; }
        public List<ResponseEmail> objListResponseEmail { get; set; }
        public string EmailRequired { get; set; }
    }
    public class ResponseEmail
    {
        public string EmailId { get; set; }
        public int Id { get; set; }
    }

}