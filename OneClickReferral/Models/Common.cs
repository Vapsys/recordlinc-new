﻿using BO.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace OneClickReferral.Models
{
    public static class Common
    {
        public static string CheckFileExtenssionOnlyThumbName(string DocName)
        {
            try
            {
                string Ext = string.IsNullOrEmpty(DocName) ? "" : Path.GetExtension(DocName);
                switch (Ext.ToLower())
                {
                    case ".doc": return "word.png";
                    case ".docx": return "word.png";
                    case ".xls": return "excel.png";
                    case ".csv": return "excel.png";
                    case ".xlsx": return "excel.png";
                    case ".dex": return "dex.png";
                    case ".pdf": return "pdf.png";
                    case ".dcm": return "dcm.png";
                    case ".yuv": return "img_1.jpg";
                    case ".tif": return "img_1.jpg";
                    case ".TIF": return "img_1.jpg";
                    case ".thm": return "img_1.jpg";
                    case ".pspimage": return "img_1.jpg";
                    default: return "img_1.jpg";
                }
            }
            catch (Exception)
            {
                return "";
            }
        }
        public static string EscapeUriString(string Url)
        {
            string ReturnValue = Url;
            //ReturnValue = ReturnValue.Replace(" ", "%20");
            //ReturnValue = System.Web.HttpContext.Current.Server.UrlEncode(ReturnValue);
            ReturnValue = Uri.EscapeUriString(Url);
            ReturnValue = ReturnValue.Replace("#", "%23");
            ReturnValue = ReturnValue.Replace("&", "%26");
            ReturnValue = ReturnValue.Replace(",", "%2c");
            ReturnValue = ReturnValue.Replace("=", "%3d");
            ReturnValue = ReturnValue.Replace("+", "%2b");
            ReturnValue = ReturnValue.Replace(";", "%3b");
            ReturnValue = ReturnValue.Replace("'", "%27");
            ReturnValue = ReturnValue.Replace("@", "%40");
            ReturnValue = ReturnValue.Replace("$", "%24");

            return ReturnValue;
        }
        public static List<SelectListItem> YearList(int? Year)
        {
            List<SelectListItem> lst = new List<SelectListItem>();

            lst.Add(new SelectListItem { Text = "Select Year", Value = "0" });


            for (int i = DateTime.Now.Year; i > 1970; i--)
            {
                lst.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString(), Selected = (Year == i) ? true : false });
            }

            return lst;
        }
        /// <summary>
        /// Safe Date Time return defult value whenever parameter contains string.empty values. 
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public static DateTime SafeDateTime(object parm)
        {
            if(parm == null)
            {
                return default(DateTime);
            }
            if (parm.GetType() == typeof(string))
            {
                if (!string.IsNullOrWhiteSpace(Convert.ToString(parm)))
                {
                    return Convert.ToDateTime(parm);
                }
                else
                {
                    return default(DateTime);
                }
            }
            else
            {
                return Convert.ToDateTime(parm);
            }
        }

        /// <summary>
        /// SAFE Boolean value return defult value whenever parameter contains empty values.
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public static bool SafeBoolean(object parm)
        {
            if(parm == null)
            {
                return default(bool);
            }
            if (parm.GetType() == typeof(string))
            {
                if (!string.IsNullOrWhiteSpace(Convert.ToString(parm)))
                {
                    return Convert.ToBoolean(parm);
                }
                else
                {
                    return default(bool);
                }
            }
            else
            {
                return Convert.ToBoolean(parm);
            }
        }

        /// <summary>
        /// SAFE Integer value return defult value whenever parameter contains empty values.
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public static int SafeInteger(object parm)
        {
            if(parm == null)
            {
                return default(int);
            }
            if (parm.GetType() == typeof(string))
            {
                if (!string.IsNullOrWhiteSpace(Convert.ToString(parm)))
                {
                    return Convert.ToInt32(parm);
                }
                else
                {
                    return default(int);
                }
            }
            else
            {
                return Convert.ToInt32(parm);
            }
        }

        /// <summary>
        /// SAFE Decimal value return defult value whenever parameter contains empty values.
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public static Decimal SafeDeciaml(object parm)
        {
            if(parm == null)
            {
                return default(decimal);
            }
            if (parm.GetType() == typeof(string))
            {
                if (!string.IsNullOrWhiteSpace(Convert.ToString(parm)))
                {
                    return Convert.ToDecimal(parm);
                }
                else
                {
                    return default(decimal);
                }
            }
            else
            {
                return Convert.ToDecimal(parm);
            }
        }

        /// <summary>
        /// SAFE string value return defult value whenever parameter contains empty values.
        /// </summary>
        /// <param name="parm"></param>
        /// <returns></returns>
        public static string SafeString(object parm)
        {
            if(parm == null)
            {
                return string.Empty;
            }
            if (parm.GetType() == typeof(string))
            {
                if (!string.IsNullOrWhiteSpace(Convert.ToString(parm)))
                {
                    return Convert.ToString(parm);
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                return Convert.ToString(parm);
            }
        }

         
       public static bool checkEmailDomain(string strEmail)
        {
            bool result = false;
            if (!string.IsNullOrWhiteSpace(strEmail) && !strEmail.Contains("@domain.com"))
            {
                result = true;
            }
            return result;
        }


        public static void LogError(Exception Ex)
        {
            RepositoryBLL repository = new RepositoryBLL(App_Start.SessionManagement.access_token, ConfigurationManager.AppSettings["apikey"].ToString());
            ExceptionRequestModel exceptionRequestModel = new ExceptionRequestModel();
            exceptionRequestModel.Message = $"OneclickReferral: - {Ex.Message}";
            exceptionRequestModel.URL = HttpContext.Current.Request.Url.AbsoluteUri;
            exceptionRequestModel.StackTrace = Ex.StackTrace;
            repository.CallAPI<bool, ExceptionRequestModel>("Exception/POST", "POST", exceptionRequestModel);
            new HttpApplication().Server.ClearError();
        }
    }
}