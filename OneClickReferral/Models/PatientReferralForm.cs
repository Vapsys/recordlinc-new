﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class PatientReferralForm
    {
        public int SectionMappingId { get; set; }
        public int SectionId { get; set; }
        public string SectionValue { get; set; }
        public int UserId { get; set; }
        public int OrderBy { get; set; }
        public bool IsEnable { get; set; }
        public string SectionName { get; set; }
        public List<PatientFieldMapping> lstFieldMapping { get; set; }
    }
    public class PatientFieldMapping
    {
        public int KeyId { get; set; }
        public int FieldMappingId { get; set; }
        public int FieldId { get; set; }
        public dynamic FieldValue { get; set; }
        public int SectionId { get; set; }
        public int UserId { get; set; }
        public int OrderBy { get; set; }
        public bool IsEnable { get; set; }
        public string FieldName { get; set; }
        public string XMLFieldName { get; set; }
        public int ParentFieldId { get; set; }
        public string TableName { get; set; }
        public List<PatientSubFieldMapping> lstSubFieldMapping { get; set; }
    }
    public class PatientSubFieldMapping
    {
        public int KeyId { get; set; }
        public int ParentFieldId { get; set; }
        public int SubFieldId { get; set; }
        public int FieldId { get; set; }
        public int SectionId { get; set; }
        public int UserId { get; set; }
        public int OrderBy { get; set; }
        public bool IsEnable { get; set; }
        public string FieldName { get; set; }
        public string XMLFieldName { get; set; }
        public dynamic SubFieldValue { get; set; }
        public string TableName { get; set; }
    }
    public class SavePatientReferralSettings
    {
        public List<PatientReferralForm> SectionSetting { get; set; }
        public List<PatientFieldMapping> FieldSettings { get; set; }
        public List<PatientSubFieldMapping> SubFieldSettings { get; set; }
    }


}