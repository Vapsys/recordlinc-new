﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OneClickReferral.Models
{
    public class AddColleagues
    {
        /// <summary>
        /// Colleagues Id with comma separated.
        /// </summary>
        public string ColleaguesId { get; set; }
        /// <summary>
        /// User access token.
        /// </summary>
        public string access_token { get; set; }
    }
}