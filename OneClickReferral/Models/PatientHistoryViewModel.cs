﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using BO.Models;
using Foolproof;

namespace OneClickReferral.Models
{
    public class PatientHistoryViewModel
    {
        /// <summary>
        /// Patient Id
        /// </summary>
        public int PatientId { get; set; }
        /// <summary>
        /// AssignedPatientId
        /// </summary>
        public string AssignedPatientId { get; set; }
        /// <summary>
        /// Encrypted Patient Id
        /// </summary>
        public string EncryptesPId { get; set; }
        /// <summary>
        /// First Name
        /// </summary>
        [Required(ErrorMessage ="Please enter First Name.")]
        public string FirstName { get; set; }
        /// <summary>
        /// LastName
        /// </summary>
        [Required(ErrorMessage = "Please enter Last Name.")]
        public string LastName { get; set; }
        /// <summary>
        /// DOB
        /// </summary>
        public string DateOfBirth { get; set; }

        [EmailAddress(ErrorMessage = "Invalid email address.")]
        public string Email { get; set; }
        /// <summary>
        /// Email Address
        /// </summary>
        public string SecondryEmail { get; set; }
        /// <summary>
        /// AddressInfo Email Address
        /// </summary>
        public string AddressInfoEmail { get; set; }
        /// <summary>
        /// Gender
        /// </summary>
        /// 
        [Required(ErrorMessage = "Please select gender.")]
        public string Gender { get; set; }
        /// <summary>
        /// Age
        /// </summary>
        public string Age { get; set; }
        /// <summary>
        /// Exact Address
        /// </summary>
        public string ExactAddress { get; set; }
        /// <summary>
        /// Address 2
        /// </summary>
        public string Address2 { get; set; }
        /// <summary>
        /// Phone number
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// Mobile No
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// Work Phone
        /// </summary>
        public string Workphone { get; set; }
        /// <summary>
        /// Fax
        /// </summary>
        public string Fax { get; set; }
        /// <summary>
        /// State
        /// </summary>
        public string State { get; set; }
        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// Image
        /// </summary>
        public string ImageName { get; set; }
        /// <summary>
        /// Zipcode
        /// </summary>
        /// 
       // [RegularExpression(@"^([FG]?\d{5}|\d{5}[AB])$", ErrorMessage = "Invalid ZipCode.")]
        [RegularExpression(@"\d{5,6}$", ErrorMessage = "Invalid zip code.")]
        public string ZipCode { get; set; }
        /// <summary>
        /// Country
        /// </summary>
        public string Country { get; set; }
        /// <summary>
        /// Doctor Full Name
        /// </summary>
        public string DoctorFullName { get; set; } // this field use in send referral
        /// <summary>
        /// Patient Note
        /// </summary>
        public string Notes { get; set; }
        /// <summary>
        /// First Exam Date
        /// </summary>        
        [LessThan("DateOfBirth", DependentPropertyDisplayName = "DateOfBirth", ErrorMessage = "First exam date must be after date of birth.")]
        public string FirstExamDate { get; set; }
        /// <summary>
        /// Location
        /// </summary>
        public string Location { get; set; }
        /// <summary>
        /// Referred by 
        /// </summary>
        public string ReferredBy { get; set; }
        /// <summary>
        /// Referred By Naem
        /// </summary>
        public string ReferredByName { get; set; }
        /// <summary>
        /// Is Authorized
        /// </summary>
        public string IsAuthorize { get; set; }
        /// <summary>
        /// Profile Images
        /// </summary>
        public string ProfileImage { get; set; }
        /// <summary>
        /// Full Name
        /// </summary>
        public string FullName { get; set; }
        /// <summary>
        /// Middle Name
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// Exact Password
        /// </summary>
        public string ExactPassword { get; set; }
        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// ReferredById
        /// </summary>
        public string ReferredById { get; set; }
        /// <summary>
        /// Emergency Contact Name 
        /// </summary>
        public string EmgContactName { get; set; }
        /// <summary>
        /// Emergency Phone No.
        /// </summary>
        public string EmergencyPhoneNo { get; set; }
        /// <summary>
        /// OwnerId
        /// </summary>
        public int OwnerId { get; set; }
        /// <summary>
        /// This Object used for Medical History Dental History and Dental Insurance
        /// </summary>
        public Patients patients { get; set; }
        /// <summary>
        /// Colleagues List for Communication 
        /// </summary>
        public List<ColleaguesMessagesOfDoctorViewModel> lstColleaguesMessagesDoctor { get; set; }
        /// <summary>
        /// Uploaded Images
        /// </summary>
        public List<UplodedImagesViewModel> lstUplodedImages { get; set; }
        /// <summary>
        /// Uploaded Documents
        /// </summary>
        public List<UplodedImagesViewModel> lstUplodedDocuments { get; set; }
        /// <summary>
        /// List of Patient Family Members
        /// </summary>
        public List<FamilyReleationHistoryViewModel> LstFamily { get; set; }
        /// <summary>
        /// Insurance coverage
        /// </summary>
        public InsuranceCoverage _insuranceCoverage { get; set; }
        /// <summary>
        /// Medical History
        /// </summary>
        public MediacalHisotry _medicalHistory { get; set; }
        /// <summary>
        /// DentalHistory
        /// </summary>
        public DentalHistory _dentalhistory { get; set; }
        /// <summary>
        /// State table list
        /// </summary>
        public List<StateList> stateLists { get; set; }
        /// <summary>
        /// Colleagues Count
        /// </summary>
        public int ColleaguesCount { get; set; }
    }
    /// <summary>
    /// State list for drop-down
    /// </summary>
    public class StateList
    {
        /// <summary>
        /// State Id
        /// </summary>
        public string StateCode { get; set; }
        /// <summary>
        /// State Name
        /// </summary>
        public string StateName { get; set; }
    }
    /// <summary>
    /// Uploaded Images/Documents View Model.
    /// </summary>
    public partial class UplodedImagesViewModel
    {
        /// <summary>
        /// ImageId
        /// </summary>
        public int ImageId { get; set; }
        /// <summary>
        /// PatientId
        /// </summary>
        public int PatientId { get; set; }
        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// ImagePath
        /// </summary>
        public string ImagePath { get; set; }
        /// <summary>
        /// RelativePath
        /// </summary>
        public string RelativePath { get; set; }
        /// <summary>
        /// CreationDate
        /// </summary>
        public string CreationDate { get; set; }
        /// <summary>
        /// LastModifiedDate
        /// </summary>
        public string LastModifiedDate { get; set; }
        /// <summary>
        /// MontageId
        /// </summary>
        public int MontageId { get; set; }
        /// <summary>
        /// Timepoint
        /// </summary>
        public int Timepoint { get; set; }
        /// <summary>
        /// FullPath
        /// </summary>
        public string FullPath { get; set; }
        /// <summary>
        /// Height
        /// </summary>
        public int Height { get; set; }
        /// <summary>
        /// Weight
        /// </summary>
        public int Weight { get; set; }
        /// <summary>
        /// ImageTypeId
        /// </summary>
        public int ImageTypeId { get; set; }
        /// <summary>
        /// ImageFormat
        /// </summary>
        public string ImageFormat { get; set; }
        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Uploaded by
        /// </summary>
        public string Uploadby { get; set; }
        /// <summary>
        /// ImageStatus
        /// </summary>
        public bool ImageStatus { get; set; }
        /// <summary>
        /// ImagePosition
        /// </summary>
        public int ImagePosition { get; set; }
        /// <summary>
        /// ImageTypeName
        /// </summary>
        public string ImageTypeName { get; set; }
        /// <summary>
        /// DateForApi
        /// </summary>
        public string DateForApi { get; set; }
        /// <summary>
        /// DocumentName
        /// </summary>
        public string DocumentName { get; set; }
        /// <summary>
        /// DocumentId
        /// </summary>
        public int DocumentId { get; set; }
    }
    /// <summary>
    /// ColleaguesMessagesOfDoctorViewModel
    /// </summary>
    public class ColleaguesMessagesOfDoctorViewModel
    {
        /// <summary>
        /// UserId
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// MessageId
        /// </summary>
        public int MessageId { get; set; }
        /// <summary>
        /// Message
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// MessageTypeId
        /// </summary>
        public int MessageTypeId { get; set; }
        /// <summary>
        /// ColleagueId
        /// </summary>
        public int ColleagueId { get; set; }
        /// <summary>
        /// ColleagueName
        /// </summary>
        public string ColleagueName { get; set; }
        /// <summary>
        /// AttachedPatientsId
        /// </summary>
        public string AttachedPatientsId { get; set; }
        /// <summary>
        /// CreationDate
        /// </summary>
        public string CreationDate { get; set; }
        /// <summary>
        /// PatientName
        /// </summary>
        public string PatientName { get; set; }
        /// <summary>
        /// SenderName
        /// </summary>
        public string SenderName { get; set; }
        /// <summary>
        /// ReciverName
        /// </summary>
        public string ReciverName { get; set; }
        /// <summary>
        /// SenderEmail
        /// </summary>
        public string SenderEmail { get; set; }
        /// <summary>
        /// ReciverEmail
        /// </summary>
        public string ReciverEmail { get; set; }
        /// <summary>
        /// SenderId
        /// </summary>
        public int SenderId { get; set; }
        /// <summary>
        /// ReciverId
        /// </summary>
        public int ReciverId { get; set; }
        /// <summary>
        /// PatientId
        /// </summary>
        public int PatientId { get; set; }
        /// <summary>
        /// List of Attached Images
        /// </summary>
        public List<MessageAttachmentViewModel> lstImages { get; set; }
        /// <summary>
        /// List of Patient Details
        /// </summary>
        public List<AttachedPatientDetailsViewModel> LstPatient { get; set; }
        /// <summary>
        /// List of Receiver Dentist Details
        /// </summary>
        public List<ReceiversDetailsViewModel> lstReceiverDetails { get; set; }
        /// <summary>
        /// Message Attachments
        /// </summary>
        public List<MessageAttachmentsViewModel> MessageAttachments { get; set; }
    }

    public class OCRPatientMessagesOfDoctor
    {
        public int PatientmessageId { get; set; }
     
        public string CreationDate { get; set; }
      
        public string FromField { get; set; }
      
        public string ToField { get; set; }
       
        public int PatientId { get; set; }
    
        public string Message { get; set; }
      
        public int MessageId { get; set; }
        
        public int TotalRecord { get; set; }
        public string PatientEmail { get; set; }
        public string DoctorEmail { get; set; }

        public List<MessageAttachmentsViewModel> lstattachement { get; set; }
    }


    /// <summary>
    /// MessageAttachmentViewModel
    /// </summary>
    public class MessageAttachmentViewModel
    {
        /// <summary>
        /// AttachmentId
        /// </summary>
        public int AttachmentId { get; set; }
        /// <summary>
        /// AttachmentName
        /// </summary>
        public string AttachmentName { get; set; }
        /// <summary>
        /// AttachementFullName
        /// </summary>
        public string AttachementFullName { get; set; }
    }
    /// <summary>
    /// Attached Patient Details
    /// </summary>
    public class AttachedPatientDetailsViewModel
    {
        /// <summary>
        /// AttachedPatientId
        /// </summary>
        public int AttachedPatientId { get; set; }
        /// <summary>
        /// AttachedPatientFirstName
        /// </summary>
        public string AttachedPatientFirstName { get; set; }
        /// <summary>
        /// AttachedPatientLastName
        /// </summary>
        public string AttachedPatientLastName { get; set; }
    }
    /// <summary>
    /// Receiver Dentist View Model
    /// </summary>
    public class ReceiversDetailsViewModel
    {
        /// <summary>
        /// ReceiverId
        /// </summary>
        public string ReceiverId { get; set; }
        /// <summary>
        /// ReceiverName
        /// </summary>
        public string ReceiverName { get; set; }
        /// <summary>
        /// ReceiverEmail
        /// </summary>
        public string ReceiverEmail { get; set; }
    }
    /// <summary>
    /// Message Attachments
    /// </summary>
    public partial class MessageAttachmentsViewModel
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// MessageId
        /// </summary>
        public int MessageId { get; set; }
        /// <summary>
        /// AttachmentName
        /// </summary>
        public string AttachmentName { get; set; }
    }
    /// <summary>
    /// Patient Family Member Details
    /// </summary>
    public class FamilyReleationHistoryViewModel
    {
        /// <summary>
        /// ReletaiveId of Patient.
        /// </summary>
        public int ReletaiveId { get; set; }
        /// <summary>
        /// PatientId
        /// </summary>
        public int PatientId { get; set; }
        /// <summary>
        /// PatientName
        /// </summary>
        public string PatientName { get; set; }
        /// <summary>
        /// ReleationName like daughter, Sister, Dad, Mom. 
        /// </summary>
        public string ReleationName { get; set; }
        /// <summary>
        /// Relative Patient Id.
        /// </summary>
        public int ToPatientId { get; set; }
    }
    public class Patients
    {
        public int PatientId { get; set; }
        public string AssignedPatientId { get; set; }

        [Required(ErrorMessage = "First name is required.")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Last name is required.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email address is required.")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Invalid email address.")]
        public string Email { get; set; }

        //[Display(Name = "Gender")]
        //[Required(ErrorMessage = "Please select Gender")]
        //public bool? Gender { get; set; }

        [Display(Name = "Gender")]
        [Required(ErrorMessage = "Gender is required.")]
        public string Gender { get; set; }
        //[StringLength(10, ErrorMessage = "Phone number must be at least 10 characters long.", MinimumLength = 10)]
        public string Phone { get; set; }
        public string ReferredBy { get; set; }
        public string LocationName { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }

        public string DateOfBirth { get; set; }
        public string FirstExamDate { get; set; }

        public string Notes { get; set; }
        //New 
        public string ExactAddress { get; set; }
        public string Age { get; set; }
        public string Address2 { get; set; }
        public string Country { get; set; }
        public int OwnerId { get; set; }
        public int PatientStatus { get; set; }
        public string Password { get; set; }
        public string ColleagueId { get; set; }
        public string LocationId { get; set; }
        public string StateId { get; set; }
        public string SecondaryPhone { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyPhoneNo { get; set; }

        public InsuranceCoverage _insuranceCoverage { get; set; }
        public MediacalHisotry _medicalHistory { get; set; }
        public DentalHistory _dentalhistory { get; set; }
    }
    public class EditFilesViewModel
    {
        public int ImageId { get; set; }
        public int PatientId { get; set; }
        public string Name { get; set; }
        public string CreationDate { get; set; }
        public string LastModifiedDate { get; set; }
        public int MontageId { get; set; }
        public int Timepoint { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        public int ImageTypeId { get; set; }
        public string ImageFormat { get; set; }
        public string Description { get; set; }
        public string Uploadby { get; set; }
        public bool ImageStatus { get; set; }
        public int PositionInMontage { get; set; }
        public int ImageFormatTypeId { get; set; }
        public string FullPathToImage { get; set; }
        public string RelativePathToImage { get; set; }
        public int LastModifiedBy { get; set; }
        public string DocumentName { get; set; }
        public int DocumentId { get; set; }
        public int OwnerId { get; set; }
        public string CreationDateForApi { get; set; }
        public string LastModifiedDateForApi { get; set; }
        public string CreationDateNew { get; set; }
        public string LastModifiedDateNew { get; set; }

    }
    public class AlterFile
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string Description { get; set; }
        public int FileType { get; set; }
    }
    public class CheckPatient
    {
        public string PatientEmail { get; set; }
        public string AssignedPatientId { get; set; }
        public string OldPatientEmail { get; set; }
        public string oldAssignedPatientId { get; set; }
        public int PatientId { get; set; }
    }
    public class PatientMessagesOfDoctorViewModel
    {
        public int PatientmessageId { get; set; }
        public string CreationDate { get; set; }
        public string FromField { get; set; }
        public string ToField { get; set; }
        public int PatientId { get; set; }
        public string Message { get; set; }
        public int MessageId { get; set; }
        public int TotalRecord { get; set; }
        public string PatientEmail { get; set; }
        public string DoctorEmail { get; set; }
        public List<MessageAttachmentViewModel> lstattachement { get; set; }
    }

    public class PatientCommunicationSort
    {
        public int PatientId { get; set; }
        public int PageIndex { get; set; }
        public int SortColumn { get; set; }
        public int SortDirection { get; set; }

    } 
}
