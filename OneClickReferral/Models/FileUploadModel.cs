﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Web;

namespace OneClickReferral.Models
{
    public class FileUploadModel
    {
        string apikey = ConfigurationManager.AppSettings["apikey"].ToString();
        public string TempUpload(string DoctorId, HttpFileCollectionBase files)
        {
            using (var client = new HttpClient())
            {
                HttpResponseMessage Res = client.PostAsJsonAsync(apikey + "FileUpload/TempUpload?DoctorId=" + DoctorId + "&filelist="+ files,"").Result;
                return Res.Content.ReadAsStringAsync().Result; 
            }
        }
    }
}