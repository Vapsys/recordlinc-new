﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OneClickReferral.Models
{
    public class PatientFilter
    {
        public int UserId { get; set; }
        public int PageIndex { get; set; }
        [Range(1, 1500, ErrorMessage = "Please enter a value bigger than 1 and less than 1500")]
        public int PageSize { get; set; }
        public string SearchText { get; set; }
        public string FirstName { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int SortColumn { get; set; }
        public int SortDirection { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public string NewSearchtext { get; set; }
        public string FilterBy { get; set; }
        public string LocationBy { get; set; }
        public bool ExcludeFamilyMembers { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public int LocationId { get; set; }
        public bool IsDashboard { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int ProviderTypefilter { get; set; }
    }
}