﻿using System;
using System.Web;
using System.Web.Script.Serialization;
using System.IO;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using NewRecordlinc.Models;
using System.Configuration;
using DataAccessLayer.Common;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.PatientsData;
using System.Collections.Generic;
using System.Security.Principal;
using System.Security.AccessControl;

namespace NewRecordlinc
{
    /// <summary>
    /// Summary description for PatientFileUpload
    /// </summary>
    public class PatientFileUpload : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        clsCommon ObjCommon = new clsCommon();
        clsPatientsData ObjPatientsData = new clsPatientsData();
        public void ProcessRequest(HttpContext context)
        {
            if (context.Request.HttpMethod == "OPTIONS")
            {
                ReturnOptions(context);
            }

            var lstFileUpload = new System.Collections.Generic.List<FileUpload>();

            try
            {

                int DeleteId = -1;
                string DeleteFileExtension = string.Empty;
                context.Response.ContentType = "json";
                HttpPostedFile postedFile = context.Request.Files["Filedata"];
                string Sesionid = string.Empty;
                if (context.Session["UserId"] != null && context.Session["UserId"].ToString() != "")
                {
                    Sesionid = context.Session["UserId"].ToString();
                }


                string UploadfromInviteColleague = string.Empty;


                if (context.Request.QueryString["InviteColleague"] != null && context.Request.QueryString["InviteColleague"] != "")
                {
                    UploadfromInviteColleague = Convert.ToString(context.Request.QueryString["InviteColleague"]);
                }


                if (context.Request.QueryString["DeleteId"] != null && context.Request.QueryString["DeleteId"] != "")
                {
                    DeleteId = Convert.ToInt32(context.Request.QueryString["DeleteId"]); // Uploaded RecordId will in Respsoe

                    if (context.Request.QueryString["FileExtension"] != null && context.Request.QueryString["FileExtension"] != "")
                    {
                        DeleteFileExtension = context.Request.QueryString["FileExtension"]; // Uploaded Files type
                    }
                }


                string savepath = "";
                string tempPath = "";
                tempPath = ConfigurationManager.AppSettings["ImageBank"];
                tempPath = tempPath.Replace("../", "");
                savepath = context.Server.MapPath(tempPath);
                if (!Directory.Exists(savepath))
                {
                    Directory.CreateDirectory(savepath);
                    SetPermissions(savepath);


                }
                savepath = savepath.Replace("PublicProfile\\", "");


               
                JavaScriptSerializer js = new JavaScriptSerializer();
                

                if (string.IsNullOrEmpty(UploadfromInviteColleague))
                {

                    if (DeleteId == -1)
                    {

                        #region Code For Upload Images and Documents



                        for (int filecount = 0; filecount < context.Request.Files.Count; filecount++)
                        {
                            HttpPostedFile hpf = context.Request.Files[filecount] as HttpPostedFile;


                            string FileName = string.Empty;
                            int TempRecordId = 0;
                            int RecordId = 0;
                            string NewFileName = string.Empty;
                            if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
                            {
                                string[] files = hpf.FileName.Split(new char[] { '\\' });
                                FileName = files[files.Length - 1];
                            }
                            else
                            {
                                FileName = hpf.FileName;
                                string[] files = hpf.FileName.Split(new char[] { '\\' });
                                FileName = files[files.Length - 1];
                            }
                            if (FileName.Contains("/"))
                            {
                                string[] files = hpf.FileName.Split(new char[] { '/' });
                                FileName = files[files.Length - 1];
                            }
                            NewFileName = "$" + System.DateTime.Now.Ticks + "$" + FileName;
                            string filename1 = System.Configuration.ConfigurationManager.AppSettings.Get("ImageBank") + NewFileName;

                            string thambnailimage = filename1.Replace(System.Configuration.ConfigurationManager.AppSettings.Get("ImageBank"), "/");

                            string extension;
                            if (hpf.ContentLength == 0)
                                continue;

                           //filename1 = clsHelper.EscapeUriString(filename1);
                            extension = Path.GetExtension(FileName);


                            if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd")
                            {
                                TempRecordId = ObjCommon.Insert_Temp_UploadedFiles(Convert.ToInt32(Sesionid.Split('?')[0]), filename1, clsCommon.GetUniquenumberForAttachments());
                            }
                            else
                            {
                                TempRecordId = ObjCommon.Insert_Temp_UploadedImages(Convert.ToInt32(Sesionid.Split('?')[0]), filename1, clsCommon.GetUniquenumberForAttachments());
                            }


                            // For upload Patient's Files



                            int DoctorId = Convert.ToInt32(Sesionid);
                            int PatientId = Convert.ToInt32(context.Request.QueryString["PatientId"]);
                            int MontageId = 0;
                            int Monatgetype = 0;
                            int imageId = 0;
                            int UploadImageId = 0;

                            int ownerId = DoctorId;
                            int ownertype = 0; int images;
                            DataTable dtimageupload = ObjCommon.Get_UploadedImagesById(TempRecordId);
                            DataTable dtfileupload = ObjCommon.Get_UploadedFilesById(TempRecordId);
                            //DataTable dtPatientImages = ObjPatientsData.GetPatientImages(PatientId, null);

                            //if (dtPatientImages.Rows.Count != 0)
                            //{
                            //    MontageId = Convert.ToInt32(dtPatientImages.Rows[0]["MontageId"]);

                            //}

                            DataTable dtGetMontageId = ObjPatientsData.GetMontageID(PatientId, DoctorId);

                            if (dtGetMontageId.Rows.Count != 0)
                            {
                                MontageId = Convert.ToInt32(dtGetMontageId.Rows[0]["MontageId"]);
                            }
                            
                            if (MontageId == 0)
                            {

                                string MontageName = PatientId + "Montage";
                                int montageid = Convert.ToInt32(ObjPatientsData.AddMontage(MontageName, Monatgetype, PatientId, ownerId, ownertype));

                                for (images = 0; images < dtimageupload.Rows.Count; images++)
                                {

                                    string fullpath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings.Get("ImageBank").ToString().Replace("../", ""));
                                    string filename = Convert.ToString(dtimageupload.Rows[images]["DocumentName"]).Replace(ConfigurationManager.AppSettings.Get("ImageBank"), "");
                                    int Imagetype;
                                    if (dtimageupload.Rows[images]["Category"].ToString() != "Get Category" && dtimageupload.Rows[images]["Category"].ToString() != "")
                                    {
                                        Imagetype = Convert.ToInt32(dtimageupload.Rows[images]["Category"]);
                                    }
                                    else
                                    {
                                        Imagetype = 0;
                                    }
                                    int imageid = ObjPatientsData.AddImagetoPatient(PatientId, 1, filename, 1000, 1000, 0, Imagetype, fullpath, dtimageupload.Rows[images]["Name"].ToString(), ownerId, ownertype, 1, "Doctor");
                                    if (imageid > 0)
                                    {

                                        bool resmonimg = ObjPatientsData.AddImagetoMontage(imageid, Imagetype, montageid);
                                    }
                                    RecordId = imageid;

                                }


                                foreach (DataRow dentaldoc in dtfileupload.Rows)
                                {
                                    int Docstatus = ObjPatientsData.UploadPatientDocument(montageid, dentaldoc["DocumentName"].ToString(), "Doctor", 0, 0);
                                    RecordId = Docstatus;
                                }

                            }

                            else if (MontageId != 0)
                            {
                                foreach (DataRow MontageImage in dtimageupload.Rows)
                                {

                                    if (UploadImageId == 0)
                                    {

                                        string fullpath = HttpContext.Current.Server.MapPath(ConfigurationManager.AppSettings.Get("ImageBank").ToString().Replace("../", ""));
                                        string imagename = Convert.ToString(MontageImage["DocumentName"]).Replace(ConfigurationManager.AppSettings.Get("ImageBank"), "");
                                        int Imagetype;
                                        if (MontageImage["Category"].ToString() != "Get Category" && MontageImage["Category"].ToString() != "")
                                        {
                                            Imagetype = Convert.ToInt32(MontageImage["Category"]);
                                        }
                                        else
                                        {
                                            Imagetype = 0;
                                        }
                                        imageId = ObjPatientsData.AddImagetoPatient(PatientId, 1, imagename, 1000, 1000, 0, Imagetype, fullpath, MontageImage["Name"].ToString(), ownerId, ownertype, 1, "Doctor");
                                        if (imageId > 0)
                                        {
                                            bool resmonimg = ObjPatientsData.AddImagetoMontage(imageId, Imagetype, MontageId);

                                        }
                                        RecordId = imageId;
                                    }


                                }
                                

                                foreach (DataRow dentaldoc in dtfileupload.Rows)
                                {
                                    int Docstatus = ObjPatientsData.UploadPatientDocument(MontageId, dentaldoc["DocumentName"].ToString(), "Doctor", 0, 0);
                                    RecordId = Docstatus;
                                }

                                //Here is the code for send file upload notification to doctor which one is associated with that patient.
                                DataTable dt = ObjPatientsData.GetSelectedTeamMemberForPatientHistory(PatientId, ownerId);
                                bool result =  BusinessLogicLayer.CommonBLL.SendFileUploadNotification(dt,PatientId,ownerId);
                            }



                            hpf.SaveAs(savepath + @"\" + filename1);




                            if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd" )
                            { }
                            else
                            {

                                string savepathThumb = "";
                                string tempPathThumb = "";
                                tempPathThumb = ConfigurationManager.AppSettings["FolderPathThumb"];
                                savepathThumb = context.Server.MapPath(tempPathThumb);
                                if (!Directory.Exists(savepathThumb))
                                {
                                    Directory.CreateDirectory(savepathThumb);
                                    SetPermissions(savepathThumb);
                                }


                                // Load image.
                                Image image = Image.FromFile(savepath + @"\" + filename1);

                                // Compute thumbnail size.
                                Size thumbnailSize = ObjCommon.GetThumbnailSize(image);

                                // Get thumbnail.
                                Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                                    thumbnailSize.Height, null, IntPtr.Zero);

                                // Save thumbnail.
                                thumbnail.Save(savepathThumb + @"\" + thambnailimage);

                            }
                            string ThumbUrl = string.Empty;
                            string UplodedFileType = string.Empty;

                            if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd")
                            {
                                ThumbUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/Content/images/" + NewRecordlinc.Common.FileExtension.CheckFileExtenssionOnlyThumbName("unknown" + extension);
                                UplodedFileType = "Document";
                            }
                            else
                            {
                                ThumbUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/" + tempPath + NewFileName;
                                UplodedFileType = "Image";
                            }
                            string ShorFilename = string.Empty;
                            if (FileName.Contains("$"))
                            {
                                ShorFilename = Convert.ToString(FileName.Split('$')[2]);
                            }
                            else
                            {
                                ShorFilename = Convert.ToString(FileName);
                            }
                            lstFileUpload.Add(new FileUpload()
                            {
                                progress = Convert.ToString(RecordId),
                                name = FileName,
                                shortfilename = ShorFilename,
                                size = hpf.ContentLength,
                                type = UplodedFileType,
                                url = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/" + tempPath + NewFileName,
                                thumbnail_url = ThumbUrl,
                                delete_url = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/PatientFileUpload.ashx?DeleteId=" + RecordId + "&FileExtension=" + extension.ToLower() + "&PatientId=" + PatientId,
                                delete_type = "DELETE"



                            });






                            // Remove Uploded Images and Documents from Temp Table

                            bool ResultDoc = ObjCommon.Temp_DeleteUploadedFileById(Convert.ToInt32(Sesionid), TempRecordId);
                            bool ResultImage = ObjCommon.Temp_DeleteUploadedImageById(Convert.ToInt32(Sesionid), TempRecordId);
                        }
                        #endregion
                    }
                    else
                    {
                        #region Code For Delete Uploaded File





                        DataTable dt = new DataTable();
                        if (DeleteFileExtension != ".jpg" && DeleteFileExtension != ".jpeg" && DeleteFileExtension != ".gif" && DeleteFileExtension != ".png" && DeleteFileExtension != ".bmp" && DeleteFileExtension != ".psd")
                        {

                            dt = ObjPatientsData.GetDescriptionofDocumentById(Convert.ToInt32(context.Request.QueryString["PatientId"]), DeleteId);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                string Name = savepath + Convert.ToString(dt.Rows[0]["DocumentName"]);
                                bool result = ObjPatientsData.RemovePatientDocumentById(DeleteId);
                                if (result)
                                {
                                    if (System.IO.File.Exists(Name))
                                    {
                                        System.IO.File.Delete(Name);
                                    }
                                }
                            }

                        }
                        else
                        {
                            dt = ObjPatientsData.GetDetailByImageId(DeleteId);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                string Name = savepath + Convert.ToString(dt.Rows[0]["Name"]);
                                bool result = ObjPatientsData.RemovePatientImage(DeleteId, Convert.ToInt32(dt.Rows[0]["MontageId"]));
                                if (result)
                                {

                                    if (System.IO.File.Exists(Name))
                                    {
                                        System.IO.File.Delete(Name);
                                    }

                                }
                            }
                        }

                        #endregion
                    }

                    context.Response.Write(js.Serialize(lstFileUpload));

                }

                else
                {
                    // Code for upload document

                    for (int filecount = 0; filecount < context.Request.Files.Count; filecount++)
                    {
                        HttpPostedFile hpf = context.Request.Files[filecount] as HttpPostedFile;
                        string FileName = string.Empty;
                        if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
                        {
                            string[] filenames = hpf.FileName.Split(new char[] { '\\' });
                            FileName = filenames[filenames.Length - 1];
                        }
                        else
                        {
                            FileName = hpf.FileName;
                        }
                        if (FileName.Contains("\\"))
                        {
                            string[] files = hpf.FileName.Split(new char[] { '\\' });
                            FileName = files[files.Length - 1];
                        }

                        string filename1 = "$" + System.DateTime.Now.Ticks + "$" + FileName;


                        string extension = Path.GetExtension(FileName);
                        string savepathInviteCoulleague = "";
                        string tempPathInviteCoulleague = "";
                        tempPathInviteCoulleague = System.Configuration.ConfigurationManager.AppSettings["UploadFromInviteCoulleague"].ToString();
                        savepathInviteCoulleague = context.Server.MapPath(tempPathInviteCoulleague);
                        savepathInviteCoulleague = savepathInviteCoulleague.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");
                        string newpath = Path.Combine(savepathInviteCoulleague + filename1);

                        hpf.SaveAs(Path.Combine(savepathInviteCoulleague + filename1));

                        string ThumbUrl = string.Empty;
                        if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd")
                        {
                            ThumbUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/Content/images/" + NewRecordlinc.Common.FileExtension.CheckFileExtenssionOnlyThumbName("unknown" + extension);
                        }
                        else
                        {
                            ThumbUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + tempPathInviteCoulleague + filename1;
                        }

                        lstFileUpload.Add(new FileUpload()
                        {
                            progress = "1.0",
                            name = filename1,
                            shortfilename = filename1.Split('$')[2],
                            size = hpf.ContentLength,
                            type = hpf.ContentType,
                            url = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + tempPathInviteCoulleague + filename1,
                            thumbnail_url = ThumbUrl,
                            delete_url = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/PatientFileUpload.ashx?DeleteId=1",
                            delete_type = "DELETE"



                        });

                    }
                    var uploadedFiles = lstFileUpload.ToArray();

                    var jsonObj = js.Serialize(uploadedFiles);
                    context.Response.Write(jsonObj.ToString());

                }


            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("", ex.Message, ex.StackTrace);
                lstFileUpload.Add(new FileUpload()
                {
                    progress = Convert.ToString(-1),
                    name = ex.Message + "    " + ex.StackTrace,
                    size = 1,
                    type = "1",
                    url = HttpContext.Current.Request.Url.ToString(),
                    thumbnail_url = HttpContext.Current.Request.Url.ToString(),
                    delete_url = HttpContext.Current.Request.Url.ToString(),
                    delete_type = "DELETE"



                });
                
            }
        }

        private static void SetPermissions(string savepath)
        {
            DirectoryInfo info = new DirectoryInfo(savepath);
            WindowsIdentity self = System.Security.Principal.WindowsIdentity.GetCurrent();
            DirectorySecurity ds = info.GetAccessControl();
            ds.AddAccessRule(new FileSystemAccessRule(self.Name,
            FileSystemRights.FullControl,
            InheritanceFlags.ObjectInherit |
            InheritanceFlags.ContainerInherit,
            PropagationFlags.None,
            AccessControlType.Allow));
            info.SetAccessControl(ds);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
        
        private static void ReturnOptions(HttpContext context)
        {
            context.Response.AddHeader("Allow", "DELETE,GET,HEAD,POST,PUT,OPTIONS");
            context.Response.StatusCode = 200;
        }
    }

   

}