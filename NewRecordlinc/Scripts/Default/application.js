﻿/*
* jQuery File Upload Plugin JS Example 5.0.2
* https://github.com/blueimp/jQuery-File-Upload
*
* Copyright 2010, Sebastian Tschan
* https://blueimp.net
*
* Licensed under the MIT license:
* http://creativecommons.org/licenses/MIT/
*/

/*jslint nomen: true */
/*global $ */

$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload();

    // Load existing files:
    $.getJSON($('#fileupload form').prop('action'), function (files) {
        // if condition added by chirag as when first time page loads it was returning null
        if (files != null) {
            var fu = $('#fileupload').data('fileupload');
            fu._adjustMaxNumberOfFiles(-files.length);
            fu._renderDownload(files)
            .appendTo($('#fileupload .files'))
            .fadeIn(function () {
                // Fix for IE7 and lower:
                $(this).show();
            });
        }
    });

    // Open download dialogs via iframes,
    // to prevent aborting current uploads:
    $('#fileupload .files a:not([target^=_blank])').on('click', function (e) {
        e.preventDefault();
        $('<iframe style="display:none;"></iframe>')
            .prop('src', this.href)
            .appendTo('body');
    });

    //File Upload initialize for message attachment in patient history
    $('#fileuploadPatientHistoryMessages').fileupload({ ctrlType : true});

    // Load existing files:
    $.getJSON($('#fileuploadPatientHistoryMessages form').prop('action'), function (files) {
        // if condition added by chirag as when first time page loads it was returning null
        if (files != null) {
            var fu = $('#fileuploadPatientHistoryMessages').data('fileupload');
            fu._adjustMaxNumberOfFiles(-files.length);
            fu._renderDownload(files)
            .appendTo($('#fileuploadPatientHistoryMessages .files'))
            .fadeIn(function () {
                // Fix for IE7 and lower:
                $(this).show();
            });
        }
    });

    // Open download dialogs via iframes,
    // to prevent aborting current uploads:
    $('#fileuploadPatientHistoryMessages .files a:not([target^=_blank])').on('click', function (e) {
        e.preventDefault();
        $('<iframe style="display:none;"></iframe>')
            .prop('src', this.href)
            .appendTo('body');
    });



});