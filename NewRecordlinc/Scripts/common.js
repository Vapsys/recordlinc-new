﻿
function showErrorMsg(msg, delay, idPrefix) {
    //-- idPrefix is useful when one or more divs are present on page with name id like errormsg
    idPrefix = (idPrefix) ? idPrefix : "";
    delay = delay ? delay : 10000;
    $("#" + idPrefix + "errormsg").show();
    $("#" + idPrefix + "errormsg").focus();
    $("#" + idPrefix + "errormsg").html(msg);
    // $("html, body").animate({ scrollTop: $("#errormsg").offset().top }, "slow");
    if (delay >= 0) {
        setTimeout(function () {
            $("#" + idPrefix + "errormsg").fadeOut(700);
            $("#" + idPrefix + "errormsg").hide();
            $("#" + idPrefix + "errormsg").html("");
        }, delay);

    }
}

function showSuccessMsg(msg, delay, idPrefix) {
    //-- idPrefix is useful when one or more divs are present on page with name id like successmsg
    idPrefix = (idPrefix) ? idPrefix : "";
    delay = delay ? delay : 5000;
    $("#" + idPrefix + "successmsg").show();
    setTimeout(function () {
        $("#" + idPrefix + "successmsg").focus();
    }, 0);
    $("#" + idPrefix + "successmsg").html(msg);
    setTimeout(function () {
        $("#" + idPrefix + "successmsg").fadeOut(700);
        $("#" + idPrefix + "successmsg").hide();
        $("#" + idPrefix + "successmsg").html("");
    }, delay);
}

function getQueryStringParam(uri, key, value) {
    var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
    var separator = uri.indexOf('?') !== -1 ? "&" : "?";
    if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
    }
    else {
        return uri + separator + key + "=" + value;
    }
}
$(document).ready(function () {

    $(".clsnumericOnly").bind("keypress", function (e) {
        var arr = [0, 8, 9, 16, 17, 20, 35, 36, 37, 38, 39, 40, 45, 46];
        // Allow numbers
        for (var i = 48; i <= 57; i++) {
            arr.push(i);
        }

        // Prevent default if not in array
        if (jQuery.inArray(e.which, arr) === -1) {
            e.preventDefault();
            return false;
        }
    });

    $(".clsalphabetsOnly").on("keydown", function (event) {

        // Allow controls such as backspace
        var arr = [8, 9, 16, 17, 20, 35, 36, 37, 38, 39, 40, 45, 46, 32];
        // Allow letters
        for (var i = 65; i <= 90; i++) {
            arr.push(i);
        }
        // Prevent default if not in array
        if (jQuery.inArray(event.which, arr) === -1) {
            event.preventDefault();
        }
    });

    $(".numericOnly").bind("keypress", function (e) {
        var arr = [0, 8, 9, 16, 17, 20, 35, 36, 37, 38, 39, 40, 45, 46];
        // Allow letters
        for (var i = 48; i <= 57; i++) {
            arr.push(i);
        }

        // Prevent default if not in array
        if (jQuery.inArray(e.which, arr) === -1) {
            e.preventDefault();
            return false;
        }
    });
    $(".numericOnly").bind("paste", function (e) {
        return false;
    });
    $(".numericOnly").bind("drop", function (e) {
        return false;
    });

    $(".alphaonly").on("keydown", function (event) {
        // Allow controls such as backspace
        var arr = [8, 9, 16, 17, 20, 35, 36, 37, 38, 39, 40, 45, 46, 32];

        // Allow letters
        for (var i = 65; i <= 90; i++) {
            arr.push(i);
        }

        // Prevent default if not in array
        if (jQuery.inArray(event.which, arr) === -1) {
            event.preventDefault();
        }

    });

});

function formatDate(date) {
    var monthNames = [
        "January", "February", "March",
        "April", "May", "June", "July",
        "August", "September", "October",
        "November", "December"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return monthNames[monthIndex] + ' ' + day + ', ' + year;
}

$(document).ready(function () {
    //our date input has the name "date" 
    if ($.fn.datepicker) {
        $('input[class="txtdob"]').datepicker({
            format: 'm/d/yyyy',
            todayHighlight: true,
            autoclose: true,
            endDate: '+0d'
        });
    }
});
jQuery.fn.getJsonData = function () {
    var o = $(this[0]) // It's your element

    var $form = o;
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};

    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });

    return indexed_array;
};

jQuery.fn.outerHTML = function () {
    return jQuery('<div />').append(this.eq(0).clone()).html();
};
function performAjax(AjaxObj) {

    //var localObj = new CustomLocalStorage();
    //if (AjaxObj.url.indexOf("http") < 0) {
    //    AjaxObj.url = localObj.webapi + AjaxObj.url
    //}

    AjaxObj.type = AjaxObj.type ? AjaxObj.type.toUpperCase() : "POST";
    AjaxObj.url += (AjaxObj.url.indexOf("?") < 0 ? "?" : "&") + "cdate=" + new Date().toString()
    showLoader();
    setTimeout(function () {
        //this set timeout using for loader not show in chrome.
        $.ajax({
            url: encodeURI(AjaxObj.url),
            type: AjaxObj.type,
            //dataType: AjaxObj.dataType ? AjaxObj.dataType : "json",
            //contentType: AjaxObj.contentType ? AjaxObj.contentType : "application/json",
            data: (AjaxObj.type == "GET") ? AjaxObj.data : AjaxObj.data, //{params: params, 'module': 'Emails', token: token},
            cache: AjaxObj.cache ? AjaxObj.cache : false,
            async: AjaxObj.async ? AjaxObj.async : false,
        }).done(function (response) {
            hideLoader();
            try {
                var objJSON = jQuery.parseJSON(response);
                if (objJSON.LogOnUrl) {
                    location.href = objSON.LogOnUrl;
                }
            }
            catch (error) {

            }
            if (typeof (AjaxObj.success) == "function") {
                AjaxObj.success(response);
            }
        }).fail(function () {
            hideLoader();
            if (typeof (AjaxObj.error) == "function") {
                AjaxObj.error();
            }
        }).always(function () {
            hideLoader();
        });

    });
    
}
function showLoader() {
    setTimeout(function () {
        $("#divLoading").show();
    }, 10);

}
function hideLoader() {
    setTimeout(function () {
        $("#divLoading").hide();
    }, 10);
}