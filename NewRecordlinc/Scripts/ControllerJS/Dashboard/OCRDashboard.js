﻿function ChangeSent() {
    changeReferredBy();
    $("#hdnReferralFrom").val(2);
    _getdataofReceivedReferral(2);
    ApplyActiveClass(2);
}
function ChangeReceive() {
    changeReferredTo();
    $("#hdnReferralFrom").val(1);
    _getdataofReceivedReferral(1);
    ApplyActiveClass(1);
}
$("#btn_send").on('click', function () {
    $('#cnfSendReceive').modal('hide');
    ChangeSent();
});
$("#btn_Receive").on('click', function () {
    $('#cnfSendReceive').modal('hide');
    ChangeReceive();
});
function ApplyActiveClass(From) {
    if (From == 2) {
        $("#lisentreferral").addClass('active').siblings().removeClass('active');
    } else {
        $("#lireceiveReferral").addClass('active').siblings().removeClass('active');
    }
}
function changeReferredBy() {
    $(".textchange").text('Referred To');
    $("#txtReferredBy").attr('placeholder', 'Referred To');
}
function changeReferredTo() {
    $(".textchange").text('Referred By');
    $("#txtReferredBy").attr('placeholder', 'Referred By');
}
function disablefields() {
    $(".isDisable").attr('disabled', 'disabled');
}
function OpenDeletePopUpForMessage(id, check,msgfrom) {
    $('#Message_delete').modal('show');
    $('#btn_yes').show();
    $('#btn_no').html('No');
    $('.modal-footer').css('text-align', 'center');
    $('#desc_message').text('Are you sure you want to archive message?');
    $('#btn_yes').attr('onclick', ' deletemessage("' + id + '","' + check + '","'+msgfrom+'")');
}
function deletemessage(id, check,msgFrom) {
    var Obj = {
        'MessageId': id,
        'MessageDisplayType': check,
        'MessageFrom':msgFrom
    };
    performAjax({
        url: "/Dashboard/DeleteReferral",
        type: "POST",
        data: { Obj: Obj, },
        success: function (data, status, xhr) {
            if (data == '1') {
                $('#desc_message').text('Message archive successfully.');
                $('.modal-footer').css('text-align', 'center');
                $('#msg_' + id).remove();
                $('#btn_yes').hide();
                $('#btn_no').html('Ok');
            } else {
                $('#desc_message').text("Message can't archive.");
                $('#btn_yes').hide();
                $('#btn_no').html('Ok');
            }
        },
        error: function () {
            $('#desc_message').text("Message can't archive.");
            $('#btn_yes').hide();
            $('#btn_no').html('Ok');
        }
    });
}
function clearSearch() {
    $('#txtReferredBy').val('');
    $('#txtSearchFromDate').val('');
    $('#txtSearchToDate').val('');
    $("#txtPatient").val('');
    $("#txtMessage").val('');
    $('#ddlDisposition').prop('selectedIndex', 0);
    $('#ddlStatus').prop('selectedIndex', 0);
    _getdataofReceivedReferral($("#hdnReferralFrom").val());
}
function _getdataofReceivedReferral(ReferralFrom) {
    $('#refData').animate({
        scrollTop: 0
    }, 'slow');
    $("#divLoading").show();
    var obj = {
        'PageSize': 50,
        'PageIndex': 1,
        'SortDirection': $("#SortDirct").val(),
        'SortColumn': $("#currentSortCol").val(),
        'DoctorName': $('#txtReferredBy').val(),
        'FromDate': $('#txtSearchFromDate').val(),
        'ToDate': $('#txtSearchToDate').val(),
        'Disposition': $("#ddlDisposition").val(),
        'ReferralFrom': ReferralFrom
    };
    performAjax({
        url: "/Dashboard/GetOCRReferrals",
        type: "POST",
        async: false,
        data: { referralFilter: obj },
        success: function (data) {
            if (data != null) {
                $("#refData").html('');
                $("#refData").append(data);
                jcf.replaceAll();
                if (ReferralFrom == 2) {
                    disablefields();
                }
                ApplyActiveClass(ReferralFrom);
            }
        },
        error: function () {
            alert('Oops...this pages need to reload.  Please try again.');
        }
    })
    $("#divLoading").hide();
}
$('#refData').scroll(function () {
    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
        var My = $("#PageIndex").val();
        $("#PageIndex").val(parseInt(My) + parseInt($("#PageIndex").val()));
        if (parseInt(My) != -1) {
            ScrollData();
        }
    }
});
function SetDispostionStatus() {
    performAjax({
        url: "/Dashboard/GetDispostionList",
        type: "POST",
        async: true,
        success: function (data) {
            if (data != null) {
                $.each(data, function (i, value) {
                    $("#ddlDisposition").append($("<option></option>").val(value.Id).html(value.Status));
                });
            }
        },
        error: function () {
            alert('Oops...this pages need to reload.  Please try again.');
        }
    });
}
function SortColumn(ColId) {
    var SortCol = $("#" + ColId).attr('sort-col');
    var Sortdir = $("#" + ColId).attr('sort-dir');
    $("#currentSortCol").val(SortCol);
    $("#SortDirct").val(Sortdir);
    if (Sortdir == 1) {
        $("#" + ColId).attr('sort-dir', 2);
    } else {
        $("#" + ColId).attr('sort-dir', 1);
    }
    var pagesize = $("#PageSizeSet").val();

    var pageindex = $("#PageIndex").val();

    //RM-194 changes for page index.
    if (pageindex == -1) {
        return false;
    }
    else {
        $("#PageIndex").val(1);
    }
    _searchData();
}
function _searchData() {
    $("#PageIndex").val(1);
    $("#divLoading").show();
    var obj = {
        'PageSize': $("#PageSizeSet").val(),
        'PageIndex': $("#PageIndex").val(),
        'SortDirection': $("#SortDirct").val(),
        'SortColumn': $("#currentSortCol").val(),
        'DoctorName': $('#txtReferredBy').val(),
        'FromDate': $('#txtSearchFromDate').val(),
        'ToDate': $('#txtSearchToDate').val(),
        'Disposition': $("#ddlDisposition").val(),
        'MessageStatus': $("#ddlStatus").val(),
        'PatientName': $("#txtPatient").val(),
        'Message': $("#txtMessage").val(),
        'ReferralFrom': $("#hdnReferralFrom").val()
    };
    performAjax({
        url: "/Dashboard/GetOCRReferrals",
        type: "POST",
        async: true,
        data: { referralFilter: obj },
        success: function (data) {
            $("#refData").html('');
            if (data != null) {
                $("#refData").append(data);
            }
            if ($("#hdnReferralFrom").val() == 2) {
                disablefields();
            }
            ApplyActiveClass($("#hdnReferralFrom").val());

            if ($(".countReferral").length == 0) {
                $("#Nomore").text("No record found!");
            }
        },
        error: function () {
            alert('Oops...this pages need to reload.  Please try again.');
        }
    });
    $("#divLoading").hide();
}
function ScrollData() {
    $("#divLoading").show();
    var obj = {
        'PageSize': $("#PageSizeSet").val(),
        'PageIndex': $("#PageIndex").val(),
        'SortDirection': $("#SortDirct").val(),
        'SortColumn': $("#currentSortCol").val(),
        'DoctorName': $('#txtReferredBy').val(),
        'FromDate': $('#txtSearchFromDate').val(),
        'ToDate': $('#txtSearchToDate').val(),
        'Disposition': $("#ddlDisposition").val(),
        'MessageStatus': $("#ddlStatus").val(),
        'PatientName': $("#txtPatient").val(),
        'Message': $("#txtMessage").val(),
        'ReferralFrom': $("#hdnReferralFrom").val()
    };
    performAjax({
        url: "/Dashboard/GetOCRReferrals",
        type: "POST",
        async: true,
        data: { referralFilter: obj },
        success: function (data) {
            if (data != null) {
                if (data.indexOf("No more") != -1) {
                    $("#PageIndex").val(-1);
                }
                var my = $("#refData").html();
                if (my.indexOf("Nomore") == -1) {
                    $("#refData").append(data);
                }
                if ($("#hdnReferralFrom").val() == 2) {
                    disablefields();
                }
                ApplyActiveClass($("#hdnReferralFrom").val());
            }
        },
        error: function () {
            alert('Oops...this pages need to reload.  Please try again.');
        }
    });
    $("#divLoading").hide();
}
function GetdoctorName(e) {
    var AccountNamesearch = $("#txtReferredBy").val();
    var Editkeycode;
    if (window.event) Editkeycode = window.event.keyCode;
    else if (e) Editkeycode = e.which;
    if (AccountNamesearch.length > 3 || AccountNamesearch.length == 0) {
        _searchData();
    }
}
function GetPatientName(e) {
    var AccountNamesearch = $("#txtPatient").val();
    var Editkeycode;
    if (window.event) Editkeycode = window.event.keyCode;
    else if (e) Editkeycode = e.which;
    if (AccountNamesearch.length > 3 || AccountNamesearch.length == 0) {
        _searchData();
    }
}
function GetMessageName(e) {
    var AccountNamesearch = $("#txtMessage").val();
    var Editkeycode;
    if (window.event) Editkeycode = window.event.keyCode;
    else if (e) Editkeycode = e.which;
    if (AccountNamesearch.length > 3 || AccountNamesearch.length == 0) {
        _searchData();
    }
}
function InsertDispositionstatus(MessageId) {
    //Condition put here because defult text not updated as disposition status.
    if ($("#ddl_" + MessageId).val() == "" || $("#ddl_" + MessageId).data('val') == $("#ddl_" + MessageId).val()) {
        return false;
    }
    var obj = {
        'MessageId': MessageId,
        'DispositionId': $("#ddl_" + MessageId).val(),
        'UserId': localStorage.getItem("Userid")
    };
    performAjax({
        url: "/Dashboard/ChangeDisposionStatus",
        type: "POST",
        data: { Obj: obj },
        success: function (data) {
            if (data == true) {
                $.toaster({ priority: 'success', title: 'success', message: 'Disposition status updated successfully' });
                $("ddl_" + MessageId).attr('data-val', $("#ddl_" + MessageId).val());
                ToggleSaveButton("ddl_" + MessageId);
            }
        },
        error: function () {
            alert('Oops...this pages need to reload.  Please try again.');
        }
    });
}
function ToggleSaveButton(id) {
    $('#' + id).attr('disabled', true);
    setTimeout(function () {
        $('#' + id).attr('disabled', false);
    }, 5000);
}