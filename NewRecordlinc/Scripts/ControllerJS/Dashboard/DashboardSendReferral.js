﻿/*Ankit Solanki 10-30-2018 Seprate this JS code for used on both side Recordlinc Dashboard and OCR Dashboard 
  So I have remove this code from Recordlinc Dashboard page and put it on here.
  XQ1-249 related changes. -- As check if User has enable the OCR and then that user has shown the OCR Dashboard
  other wise it will show as it is recordlinc dashboard.
 */
function OpenSendReferralPopup() {
    $('#hdnPatientId').val('');
    $('#hdnColleagueId').val('');
    $('#PopupId').val('dashboard');
    var GetSearchText = null;
    if ($("#txtSearchColleague").val() == null) {
        GetSearchText = null;
    } else if ($('#txtSearchColleague').length == 0) {
        GetSearchText = null;
    }
    else {
        if ($('txtSearchColleague').val() == 'Find colleague to refer to...') {
            GetSearchText = null;
        }
        else {
            GetSearchText = $('txtSearchColleague').val();
        }
    }
    $.ajax({
        url: '/Dashboard/NewGetListSearchColleagueFromPopUp',
        type: 'post',
        data: { SearchText: GetSearchText },
        success: function (data) {

            $("#colleague_refer_patient").modal('show');
            $("#colleague_refer_patient").html(data);

            $("#btn_send_dashboard").hide();
            $("#btn_send").hide();

            $(".list-holder").bind('scroll', function (event) {
                if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                    var data = GetColleaguesByScroll().toString();
                    if (data.indexOf('Colleague not found') > -1) {

                        $('.referral-list').append(data);
                        $("#ColleaguPageIndex").val("-1");
                    }
                    else {
                        $('.referral-list').append(data);
                    }
                }
            });
        }
    })
}
var timer;
function SearchColleagueOnkeyPress() {
    var ctrl = $("#txtSearchColleague").val();
    clearTimeout(timer);
    var ms = 500; // milliseconds
    timer = setTimeout(function () {
        Onkeypresscolleaguelist(ctrl);
    }, ms);
}
//Bind Colleagues on scroll.
function Onkeypresscolleaguelist(ctrl) {
    var value = ctrl.toLowerCase().trim();
    if ($.trim(value).length == 0) {
        value = "";
    }
    $('#divLoading').show();
    $.ajaxSetup({ async: false });
    $.ajax({
        url: "/Dashboard/NewSearchColleagueFromPopUp",
        type: 'post',
        data: { SearchText: value },
        success: function (data) {
            $('#divLoading').hide();
            $('.referral-list').html('');
            $('.referral-list').append(data);
        }
    });

    $.ajaxSetup({ async: true });
    $(".list-holder").bind('scroll', function (event) {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            var data = GetColleaguesByScroll().toString();
            $(this).append(data);
        }
    });
}
function GetColleaguesByScroll() {

    var datalist = '';
    $.ajaxSetup({ async: false });
    var PageIndex = $("#ColleaguPageIndex").val();
    var SearchText = $("#txtSearchColleague").val();
    if (PageIndex == "-1") {
        return "";
    } else {
        PageIndex = parseInt(PageIndex) + parseInt(1);
        $.ajax({
            url: '/Dashboard/NewGetColleagueListForSendReferralPopUpOnScroll',
            type: 'post',
            data: { PageIndex: PageIndex, SearchText: SearchText },
            success: function (data) {
                if (data == '' || data.trim() == '<li><center>No more record found.</center></li>') {
                    $("#ColleaguPageIndex").val("-1");
                }
                else {
                    $("#ColleaguPageIndex").val(PageIndex);

                }
                datalist = data;
            }
        });
    }
    $.ajaxSetup({ async: true });
    return datalist;
}
function SelectColleague(cid) {
    $(".referral-list > li").each(function () { $(this).removeClass('active'); });
    if ($("." + cid).hasClass('active')) {
        $("." + cid).removeClass('active');
    } else {
        $("." + cid).addClass('active');
        $('#btn_send_patient').attr("disabled", false);
        $("#hdnColleagueId").val(cid);
    }
}

function SelectLocation(lid) {
    $(".referral-list > li").each(function () { $(this).removeClass('active'); });
    if ($("." + lid).hasClass('active')) {
        $("." + lid).removeClass('active');
    } else {
        $("." + lid).addClass('active');
        $('#btn_send_patient').attr("disabled", false);
        $("#hdnlocationid").val(lid);
    }
}
//Method to check patient
function SelectPatient(cid) {
    $(".referral-list > li").each(function () { $(this).removeClass('active'); });
    if ($("." + cid).hasClass('active')) {
        $("." + cid).removeClass('active');
    } else {
        $("." + cid).addClass('active');
        $('#btn_send').attr("disabled", false);
        $("#hdnPatientId").val(cid);
    }
}

// Select Location Code
function SelectLocationForReferral() {

    var colleaguid = $("#hdnColleagueId").val();
    if (colleaguid == null || colleaguid == "") {
        //Jquery Error for toster
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select at least one Colleague.' });
        ToggleSaveButton("btn_send_Location");
        return false;
    }
    else {
        $('.referral-list').html('');
        $(".list-holder").unbind('scroll');
    }
    $("#divLoading").show();

    $.ajax({
        url: "/Dashboard/LocationList",
        type: "POST",
        async: true,
        data: { UserId: colleaguid },
        success: function (data) {

            if (data == 1) {
                $("#hdnlocationid").val(0);

                var popupid = $('#PopupId').val();
                if (popupid == 'dashboard') {
                    DashboardReferralSend(); // call Main Button Location 
                }
                else {
                    FinalReferralSend();    // call patient Listing Location
                }
            }
            else {
                $("#divLoading").hide();
                $(".referral-list").html(data);

                $("#btn_send_Location").hide();
                $("#btn_send_patient").hide();
                $('#Colleague_Search_Block').hide();
                $('#clg-heading').hide();
                $('#Patient_Search_Block').hide();
                $('#loc-heading').show();
                $("#btn_send_dashboard").show();
                $("#btn_send_pat").show();
                $('#btn_send_final').hide();

            }
        }
    })
}
//Selecting Patient Code
function DashboardReferralSend() {
    var locationid = $("#hdnlocationid").val();
    //clg-heading
    if (locationid == 0) {
        $('#clg-heading').hide();
        $('#btn_send_Location').hide();
    }

    if (locationid == null || locationid == "") {
        //Jquery Error for toster
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select at least one Location.' });
        ToggleSaveButton("btn_send_dashboard");
        return false;
    }
    else {
        $('.referral-list').html('');
        $(".list-holder").unbind('scroll');
    }
    $("#divLoading").show();
    var SearchText = "";
    var PageIndex = $("#PatientPageIndex").val();
    if ($("#txtSearchPatient").val() == null) {
        GetSearchText = null;
    } else if ($('#txtSearchPatient').length == 0) {
        GetSearchText = null;
    }
    else {
        if ($('txtSearchPatient').val() == 'Find patient to refer...') {
            GetSearchText = null;
        }
        else {
            GetSearchText = $('txtSearchPatient').val();
        }
    }
    $.ajax({
        url: "/Dashboard/NewSearchPatientFromItem",
        type: "POST",
        async: true,
        data: { SearchText: SearchText },
        success: function (data) {
            $("#divLoading").hide();
            $(".referral-list").html(data);
            $('#Colleague_Search_Block').hide();
            $('#loc-heading').hide();
            $('#Patient_Search_Block').show();
            $('#pat-heading').show();
            $("#btn_send_dashboard").hide();
            $('#btn_send_final').show();

            $(".list-holder").bind('scroll', function (event) {
                if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                    var data = GetPatientByScroll().toString();
                    if (data == "" || data.indexOf('Patient not found.') > -1) {
                        $('.referral-list').append(data);
                        $("#PatientPageIndex").val("-1");
                    }
                    else {
                        $('.referral-list').append(data);
                        //$("#PatientPageIndex").val(PageIndex);
                    }
                }
            });
        }
    })
}
function GetPatientByScroll() {
    var datalist = '';
    $.ajaxSetup({ async: false });
    var PageIndex = $("#PatientPageIndex").val();
    var SearchText = $("#txtSearchPatient").val();
    if (PageIndex == "-1") {
        return "";
    } else {
        PageIndex = parseInt(PageIndex) + parseInt(1);
        $.ajax({
            url: '/Dashboard/NewGetPatientListForSendReferralPopUpLI',
            type: 'post',
            data: { pageIndex: PageIndex, SearchText: SearchText, pagesize: '30' },
            success: function (data) {
                if (data == '' || data.trim() == '<div style="letter-spacing: 0px;"><center>No more record found.</center></div>') {
                    $("#PatientPageIndex").val('-1');
                }
                else {
                    $("#PatientPageIndex").val(PageIndex);
                }
                datalist = data;
            }
        });
    }
    $.ajaxSetup({ async: true });
    return datalist;
}

//search Patient on Key Up
var timer;
function SearchPatientOnKeyup() {
    var ctrl = $("#txtSearchPatient");
    clearTimeout(timer);
    var ms = 500; // milliseconds
    timer = setTimeout(function () {
        OnkeypressPatientsearch(ctrl);
    }, ms);
}

function OnkeypressPatientsearch(ctrl) {
    var value = $(ctrl).val().toLowerCase().trim();
    if ($.trim(value).length == 0) {
        value = "";
    }
    $("#divLoading").show();
    $.ajaxSetup({ async: false });
    $.post("/Dashboard/NewSearchPatientFromItem",
        { SearchText: value }, function (data) {
            $("#divLoading").hide();
            $('.referral-list').html('');
            $('.referral-list').append(data);
        });
    $.ajaxSetup({ async: true });
    $(".referral-list").bind('scroll', function (event) {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            var data = SearchPatientLI().toString();
            $(this).append(data);
        }
    });
}
function FinalReferralSend() {

    var PatientId = $('#hdnPatientId').val();
    var ColleagueId = $("#hdnColleagueId").val();
    var LocationId = $("#hdnlocationid").val();
    if (PatientId == null || PatientId == "") {
        //swal({ title: "", text: 'Please select at least one patient.' });
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select at least one patient.' });
        ToggleSaveButton('btn_send_final');
    }
    else if (LocationId == null || LocationId == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select at least one location.' });
        ToggleSaveButton('btn_send_final');
    }
    else {
        window.location.href = '/Referrals/Referral?PatientId=' + PatientId + '&ColleagueIds=' + ColleagueId + '&AddressInfoId=' + LocationId;
    }
}

//Send Referral to Colleague Popup
function ReferralSend() {

    var PatientId = $('#hdnPatientId').val();
    var ColleagueId = $("#hdnColleagueId").val();
    var LocationId = $("#hdnlocationid").val();
    if (PatientId == null || PatientId == "") {
        //$("#ReferPatient").modal('show');
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select at least one Patient.' });
        $('#btn_send').attr("disabled", true);
    }
    else {
        window.location.href = '/Referrals/Referral?PatientId=' + PatientId + '&ColleagueIds=' + ColleagueId.toString() + "&AddressInfoId=" + LocationId;
    }
}
$(function () {
    $("#search_main").css("display", "block");
    $("#search_main").html("<div class='search_main'><div class='searchbar'><input id='txtHeaderSearchtext' PlaceHolder='Search Patients' type='text' ></div></div>");
    $("#txtHeaderSearchtext").autocomplete({
        source: function (request, response) {

            $.post("/Patients/GetPatientAutocompleteList", { text: request.term }, function (data) {
                if (!data.length) {
                    var result = [
                        {
                            label: 'No record found',
                            value: 0
                        }
                    ];
                    response(result);
                }
                else {
                    // normal response
                    response($.map(data, function (item) {
                        return {
                            label: item.FirstName.trim() + ' ' + item.LastName.trim()
                                + ' (' + item.PatientId + ($.trim(item.Email).length > 0 ? ' - ' + item.Email : '') + ')'
                            , value: item.PatientId
                        };
                    }));
                }
            });
        },
        minLength: 3,
        select: function (event, ui) {
            event.preventDefault();
            if (ui.item.label != "No record found") {
                $("#txtHeaderSearchtext").val(ui.item.label);
                ViewPatientHistory(ui.item.value);
            }
            else {
                $("#txtHeaderSearchtext").val(null);
            }
        }
    });
}); 