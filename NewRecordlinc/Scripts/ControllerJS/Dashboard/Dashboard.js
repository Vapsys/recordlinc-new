﻿
function ReferralSendPatient() {

    var PatientId = $('#hdnPatientId').val();
    var ColleagueId = $("#hdnColleagueId").val();
    var LocationId = $("#hdnlocationid").val();
    if (ColleagueId == null || ColleagueId == "") {
        //$("#ReferPatient").modal('show');
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select at least one Colleague.' });
        $('#btn_send_patient').attr("disabled", true);
    }
    else {
        window.location.href = '@Url.Action("Referral", "Referrals")?PatientId=' + PatientId + '&ColleagueIds=' + ColleagueId.toString() + "&AddressInfoId=" + LocationId;
    }
}



function ToggleSaveButton(id) {
    $('#' + id).attr('disabled', true);
    setTimeout(function () {
        $('#' + id).attr('disabled', false);
    }, 4000);
}

function SortColumn(ColId) {

    var PageIndex = $('#PageIndex').val();
    var PageSize = $('#PageSize').val();
    var SortColumn = $("#" + ColId).attr('sort-col');
    $("#Sort_Column").val(SortColumn);
    var SortDirection = $("#" + ColId).attr('sort-dir');
    if (SortDirection == 1) {
        $("#" + ColId).attr('sort-dir', 2);
    } else {
        $("#" + ColId).attr('sort-dir', 1);
    }

    $.ajax({
        url: "/Inbox/PartialInBoxMessages",
        type: "post",
        data: { PageIndex: PageIndex, PageSize: 10, ShortColumn: SortColumn, ShortDirection: SortDirection, SearchText: '', MessageDisplayType: 2, IsDashboard: true },
        async: true, success: function (data) {
            $('#AppendList').html(data);
        }
    });
}

function ViewProfile(id) {
    $("#divLoading").show();
    $('#hdnViewProfile').val(id);
    $("#frmViewProfile").submit();
    $("#divLoading").hide();
}

function ViewPatientHistory(PatientId) {
    $("#divLoading").show();
    $('#hdnViewPatientHistory').val(PatientId);
    $("#frmViewPatientHistory").submit();
    $("#divLoading").hide();
}

function CallPlan(id) {
    $("#hdnPatienHistoryId").val(id);
    $("#frmPatientListPlan").submit();
}

function OpenDeletePopUpForMessage(id, check) {
    $('#patient_delete').modal('show');
    $('#btn_yes').show();
    $('#btn_no').html('No');
    $('.modal-footer').css('text-align', 'right');
    $('#desc_message').text('Are you sure you want to Delete Message?');
    $('#btn_yes').attr('onclick', ' deletemessage("' + id + '","' + check + '")');
}

//Delete Functions
function deletemessage(id, check) {

    $.ajax({
        url: "/Inbox/RemoveMessageByID",
        type: "POST",
        data: { MessageId: id, MessageDisplayType: check },
        success: function (data, status, xhr) {
            if (data) {
                //$.toaster({ priority: 'success', title: 'Success', message: 'Message deleted successfully.' });
                $('#desc_message').text('Message deleted successfully.');
                $('.modal-footer').css('text-align', 'center');
                $('#msg_' + id).remove();
                $('#btn_yes').hide();
                $('#btn_no').html('Ok');
            }
        }

    });
}
//Open Pop up for Colleague
function OpenSendReferralPopupForColleague(ColleagueId) {

    var SearchText = "";
    var PageIndex = $("#PatientPageIndex").val();
    if ($("#txtSearchPatient").val() == null) {
        GetSearchText = null;
    } else if ($('#txtSearchPatient').length == 0) {
        GetSearchText = null;
    }
    else {
        if ($('txtSearchPatient').val() == 'Find patient to refer...') {
            GetSearchText = null;
        }
        else {
            GetSearchText = $('txtSearchPatient').val();
        }
    }
    //$('#divLoading').show();
    $.ajax({
        url: "/Colleagues/LocationList",
        type: "POST",
        async: true,
        data: { UserId: ColleagueId },
        success: function (data) {
            $("#ReferPatient").html(data);
            var singlelocation = $("#SingleLocationId").val();
            if (singlelocation != 0) {
                $("#ReferPatient").modal('show');
                $("#hdnlocationid").val(0);
                $("#hdnColleagueId").val(ColleagueId);
                PatientReferralSend();
            }
            else {
                $("#ReferPatient").modal('show');
                //$("#ReferPatient").html(data);
                $('#Patient_Block').hide();
                $('#refer_heading').text('Select Location');
                $("#hdnColleagueId").val(ColleagueId);
            }
        }
    })
}

function PatientReferralSend() {

    var locationid = $("#hdnlocationid").val();
    if (locationid == null || locationid == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select at least one Location.' });
        ToggleSaveButton("btn_send_Patient");
        return false;
    }
    else {
        if ($("#SingleLocationId").val() != 0) {
            $('#divLoading').hide();
        }
        else {
            $('#divLoading').show();
        }
        $('.referral-list').html('');
        $(".list-holder").unbind('scroll');
    }
    var SearchText = "";
    var PageIndex = $("#PatientPageIndex").val();
    if ($("#txtSearchPatient").val() == null) {
        GetSearchText = null;
    } else if ($('#txtSearchPatient').length == 0) {
        GetSearchText = null;
    }
    else {
        if ($('txtSearchPatient').val() == 'Find patient to refer...') {
            GetSearchText = null;
        }
        else {
            GetSearchText = $('txtSearchPatient').val();
        }
    }
    $.ajax({
        url: "/Colleagues/NewSearchPatientFromPopUp",
        type: "POST",
        async: true,
        data: { SearchText: SearchText },
        success: function (data) {
            $('#divLoading').hide();
            $(".referral-list").html(data);
            $('#Patient_Block').show();
            $('#refer_heading').text('Select Patient To Refer');
            $("#hdnlocationid").val(locationid);
            $('#btn_send_Patient').hide();
            $('#btn_send_clg').show();
            $(".list-holder").bind('scroll', function (event) {
                if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                    var data = GetPatientByScroll().toString();
                    if (data == "" || data.indexOf('Patient not found.') > -1) {
                        $('.referral-list').append(data);
                        $("#PatientPageIndex").val("-1");
                    }
                    else {
                        $('.referral-list').append(data);
                        //Start: XQ1-227: Duplicate patient list appears in Patient Pop up  
                        PageIndex = parseInt(PageIndex) + parseInt(1);
                        //END
                        $("#PatientPageIndex").val(PageIndex);
                    }
                }
            });
        }
    })
}

function CheckAuthorization(FeatureId) {
    $.ajax({
        url: "/Common/CheckAuthorizationByFeature",
        type: "post",
        data: { FeatureId: FeatureId },
        async: true,
        success: function (data) {
            if (data == true) {
                switch (parseInt(FeatureId)) {
                    case NewPatientLead:
                        location.href = "/Patients/AddPatient"
                        break;
                    case ColleagueLead:
                        location.href = "/Colleagues/SearchColleagues"
                        break;
                }
            }
            else {
                if (FeatureId == 8) {
                    swal({ html: true, title: "Upgrade Membership", text: 'Please <a href="/FreeToPaid/UpgradeAccount">upgrade</a> your membership to be able to add more colleagues.' });
                }
                else {
                    swal({ html: true, title: "Upgrade Membership", text: 'Please <a href="/FreeToPaid/UpgradeAccount">upgrade</a> your membership to be able to add more patients.' });
                }
            }
        }
    });
}
function OpenSendReferralPopupForPatient(pid) {
    $('#PopupId').val('patient');
    $('#hdnPatientId').val('');
    $('#hdnColleagueId').val('');
    var PatientId = pid;
    $('#hdnPatientId').val(pid);
    var GetSearchText = null;
    if ($("#txtSearchColleague").val() == null) {
        GetSearchText = null;
    } else if ($('#txtSearchColleague').length == 0) {
        GetSearchText = null;
    }
    else {
        if ($('txtSearchColleague').val() == 'Find colleague to refer to...') {
            GetSearchText = null;
        }
        else {
            GetSearchText = $('txtSearchColleague').val();
        }
    }
    $.ajax({
        url: '/Patients/NewGetListSearchColleagueFromPopUp',
        type: 'post',
        data: { SearchText: GetSearchText },
        success: function (data) {
            $("#NewRefferalPopUpForDashboard").modal('show');
            $("#NewRefferalPopUpForDashboard").html(data);
            $("#btn_send_pat").hide();
            $("#btn_send_dashboard").hide();
            $("#btn_send_patient").show();
            $(".list-holder").bind('scroll', function (event) {
                if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                    var data = GetColleaguesByScroll().toString();
                    if (data.indexOf('Colleague not found') > -1) {
                        $('.referral-list').append(data);
                        $("#ColleaguPageIndex").val("-1");
                    }
                    else {
                        $('.referral-list').append(data);
                    }
                }
            });
        }
    })
}
//Delete an individual patient
function OpenDeletePopUp(id) {
    $('#patient_delete').modal('show');
    $('#btn_yes').show();
    $('#btn_no').html('No');
    $('.modal-footer').css('text-align', 'right');
    $('#desc_message').text('Are you sure you want to Delete Patient?');
    //$('#btn_yes').attr('onclick', ' DeletePatient("' + id + '","' + PageIndex + '")');
    $('#btn_yes').attr('onclick', ' DeletePatient("' + id + '")');
}
function DeletePatient(id) {
    $.ajax({
        url: "/Patients/DeleteSelectedPatient",
        type: "post",
        data: { PatientId: id },
        success: function (data) {
            var removed = data.RemovedCount;
            var unremoved = data.UnRemovedCount;
            if (unremoved == 1) {
                $('#desc_message').text('This patient is associated with another dentist.');
                $('#btn_yes').hide();
                $('#btn_no').html('Ok');
                $('.modal-footer').css('text-align', 'center');
                return false;
            } else {
                if (removed == 1) {
                    $('#desc_message').text('Patient removed successfully.');
                    $('.modal-footer').css('text-align', 'center');
                    $('#pat_' + id).remove();
                    $('#btn_yes').hide();
                    $('#btn_no').html('Ok');
                    return false;
                } else {
                    $('#desc_message').text('Failed to remove patient.');
                }
            }
        }
    });
}
function GetPatientListByHeadings(ColId) {
    var PageIndex = $('#PL_PageIndex').val();
    var PageSize = $('#PL_PageSize').val();
    var FilterBy = $("#FilterBy").find(':selected').attr('data-id');
    var NewSearchText = $("ul.alphabet > li.active > a").text();
    var StrLocation = $("#LocationBy").find(':selected').attr('data-id');
    var SortColumn = $("#" + ColId).attr('sort-col');
    $("#Sort_Column_Patient").val(SortColumn);
    var SearchText = null;
    var SortDirection = $("#" + ColId).attr('sort-dir');
    if (SortDirection == 1) {
        $("#" + ColId).attr('sort-dir', 2);
        SortDirection = 2;
    } else {
        $("#" + ColId).attr('sort-dir', 1);
        SortDirection = 1;
    }
    var obj3 = {};
    obj3 = {
        'PageIndex': PageIndex,
        'NewSearchtext': null,
        'FilterBy': FilterBy,
        'LocationBy': StrLocation,
        'SortDirection': SortDirection,
        'SortColumn': SortColumn,
        'SearchText': SearchText,
        'NewSearchtext': NewSearchText,
        'IsDashboard': true
    }
    $.ajax({
        url: "/Patients/NewGetPatientDetailsList",
        type: "post",
        data: { FilterObj: obj3 },
        async: true,
        success: function (data) {
            $('#AppendPatient').html(data);
        }
    });
}
function SearchPatient() {
    var GetSearchText = null;
    if ($('#txtSearchPatient') == null) {
        GetSearchText = null;
    } else if ($('#txtSearchPatient').legnth == 0) {
        GetSearchText = null;
    }
    else {
        if ($('#txtSearchPatient').val() == "Find patient to refer...") {
            GetSearchText = null;
        }
        else {
            GetSearchText = $('#txtSearchPatient').val();
        }
    }
    $('#divLoading').show();
    $.ajaxSetup({ async: false });
    $.post("/Colleagues/NewSearchPatientFromItem",
        { SearchText: GetSearchText }, function (data) {
            $('#divLoading').hide();
            $('.referral-list').html('');
            $('.referral-list').append(data);
        });
    $.ajaxSetup({ async: true });
    $(".referral-list").bind('scroll', function (event) {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            var data = GetPatientByScroll().toString();
            $(this).append(data);
        }
    });
}
function ResetFields() {
    $('#hdnColleagueId').val('');
    $('#hdnPatientId').val('');
    $('#hdnlocationid').val('');
}
function OpenAppointment(PatientId, ReferredById) {
    if (PatientId > 0 && ReferredById > 0) {
        location.href = "/Appointment?PatientId=" + PatientId + "&DoctorId=" + ReferredById;
    }
}
function SendForms(PatientId, Email, Name) {
    $.ajax({
        url: '/Patients/SendForm',
        type: 'post',
        data: { PatientId: PatientId, Email: Email, PatientName: Name },
        success: function (data) {
            if (data == true) {

                $.toaster({ priority: 'success', title: 'success', message: 'Form send successfully' });
            }
            else {
                $.toaster({ priority: 'danger', title: 'Failed', message: 'Failed to send the form.' });
            }
        }
    });
}
function RequestReviews(PatientId, Email, FirstName, LastName) {
    $.ajax({
        url: '/Patients/RequestReviews',
        type: 'post',
        data: { PatientId: PatientId, Email: Email, FirstName: FirstName, LastName: LastName },
        success: function (data) {
            if (data == true) {

                $.toaster({ priority: 'success', title: 'success', message: 'Request send successfully.' });
            }
            else {
                $.toaster({ priority: 'danger', title: 'Failed', message: 'Failed to send the Request.' });
            }
            datalist = data;
        }
    });
}
//Method for Open Request Payment Pop Up
function OpenRequestPayment(PatientId) {
    $.ajax({
        url: '/Dashboard/ReferPaymentFromPopUp',
        type: 'post',
        data: { PatientId: PatientId },
        success: function (data) {
            $("#NewRefferalPopUp").modal('show');
            $("#NewRefferalPopUp").html(data);
        }
    })
}
function SubmitReferPayment(PatientId) {
    if ($('#Amount').val().length > 0) {
        $.ajax({
            url: '/Patients/SubmitReferPayment',
            type: 'post',
            data: { PatientId: PatientId, Amount: $('#Amount').val() },
            success: function (data) {
                if (data == true) {
                    $("#NewRefferalPopUp").modal('hide');
                    $.toaster({ priority: 'success', title: 'success', message: 'Payement request send successfully' });
                }
                else {
                    $.toaster({ priority: 'danger', title: 'Failed', message: 'Failed to send payment request.' });
                }
            }
        });
    }
    else {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter amount.' });
    }
}