﻿$(".opener-btn").click(function () {
    $(this).toggleClass("active");
});
$(document).on("click", ".article-block", function (e) {
    var UserName = $(this).data("username");
    var URL = window.location.href;
    var URL = URL.replace("Profiles/SearchResult", 'p/' + UserName);
    window.open(URL, '_blank');
    e.stopPropagation();
});
$(document).on("click", "#viewprofile", function (e) {
    $("#viewprofile").removeAttr('target');
    var UserName = $(this).data("username");
    window.location.href = "/p/" + UserName;
    e.stopPropagation();
});
$(document).on("click", "#scheduleapp", function (e) {
    $("#scheduleapp").removeAttr('target');
    var UserName = $(this).data("username");
    window.location.href = "/p/" + UserName + "?issearch=true";
    e.stopPropagation();
});
$(document).ready(function () {
    $(window).scrollTop(0);
    $("#myInput").select2({
        //  minimumInputLength: 1,
        multiple: true,
        closeOnSelect: false // Mantis bug no : 6977 for dropdown doesnt close 
    });
    var lstDoctorAddressDetails = [{ "ExactAddress": "35 W Blue Ridge Way", "Address2": "", "City": "Chandler", "State": "AZ", "Country": "US", "ZipCode": "85248", "EmailAddress": "", "Phone": "", "Fax": "", "ContactType": 1, "Location": "Chandler", "TimeZoneId": 0, "Mobile": "", "AccountName": "Recordlinc" }];
    initialize(lstDoctorAddressDetails);
    $("#totalCount").val($("#loopCount").val());
    $("#count").html($("#totalCount").val());
});
$("#txtKeyword").keypress(function (event) {
    if (event.which == 13) {
        SearchByKeyword('1');
    }
});
$('#txtZipCode').keypress(function (e) {
    var regex = new RegExp("^[0-9-]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }

    e.preventDefault();
    return false;
});
$(function () {
    var bindScrollHandler = function () {
        $(window).scroll(function () {
            //if ($(window).scrollTop() + $(window).height() > $(document).height() - 900) {
            if (parseInt($(window).scrollTop()) >= parseInt($(document).height() - $(window).height() - 800)) {
                $(window).unbind("scroll");
                // $(window).scrollTop($(window).scrollTop() - parseInt($(window).scrollTop() / 2))
                var PageIndex = $("#hdnPageIndex").val();
                if (parseInt(PageIndex) != -1 && $("#doctorlist center").length == 0) {
                    $(window).scrollTop($(window).scrollTop() - parseInt($(window).scrollTop() / 2))
                    GetDentistlistonScroll();
                }
                bindScrollHandler();
            }
            else {
            }
        });
    }
    bindScrollHandler();
});
function myFunction() {
    // Declare variables
    var input, filter, ul, li, a, i;
    input = document.getElementById('myInput');
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName('li');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        a = li[i].getElementsByTagName("a")[0];
        if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
            li[i].style.display = "";
        } else {
            li[i].style.display = "none";
        }
    }
}

$('.map-holder.map_pin').affix({
    offset: {
        top: 500,
        bottom: $('#main-footer').outerHeight(true)/* height of footer in this case none */
    }
}).on('affix-bottom.bs.affix', function () {
    $(this).css('bottom', 'auto');
});
function SearchByKeyword(id) {
    //if (id == 1) {
    //    var Keyword = $("#txtKeyword").val();
    //    if (Keyword == null || Keyword == "") {
    //        return false;
    //    }
    //}
    var Count = $("#count").html();
    var PageIndex = $("#hdnPageIndex").val();
    var Obj = {};
    Obj = {
        'fname': $("#txtFirstName").val(),
        'LastName': $("#txtLastName").val(),
        'keywords': $("#txtKeyword").val(),
        'City': $("#txtCity").val(),
        'ZipCode': $("#txtZipCode").val(),
        'SpecialityList': $("#myInput").val(),
        'Miles': $("#ddlMiles").val(),
        'PageIndex': 1,
        'PageSize': 35,
    }
    $.ajax({
        url: "/Profiles/PartialSearchResult",
        type: "post",
        data: { Obj: Obj },
        beforeSend: function () {
            $("#divLoading").show();
        },
        success: function (data) {
            if (data.indexOf("No More Record Found") > -1) {
                $("#doctorlist").html("");
                if ($("#doctorlist center").length == 0) {
                    $("#doctorlist").append(data);
                }

                $("#count").html(0);
                $("#hdnPageIndex").val(-1);
            }
            else {
                $("#doctorlist").html("");
                $("#doctorlist").append(data);
                $("#totalCount").val($("#loopCount").val());
                $("#count").html($("#totalCount").val());
                $("#hdnPageIndex").val(1);
            }
            $("#divLoading").hide();
        },
        error: function (err) {
            // alert(err.statusText);
            $("#divLoading").hide();
        }
    });

}
function GetDentistlistonScroll() {
    var PageIndex = $("#hdnPageIndex").val();
    if (PageIndex == -1) {
        return false;
    } else {
        PageIndex = parseInt(PageIndex) + parseInt(1);
    }
    var Obj = {};
    Obj = {
        'fname': $("#txtFirstName").val(),
        'LastName': $("#txtLastName").val(),
        'keywords': $("#txtKeyword").val(),
        'City': $("#txtCity").val(),
        'ZipCode': $("#txtZipCode").val(),
        'SpecialityList': $("#myInput").val(),
        'Miles': $("#ddlMiles").val(),
        'PageIndex': PageIndex,
        'PageSize': $("#hdnPageSize").val()
    }

    $.ajax({
        url: "/Profiles/PartialSearchResult",
        type: "post",
        data: { Obj: Obj },
        beforeSend: function () {
            $("#divLoading").show();
        },
        success: function (data) {
            if (data.indexOf("No More Record Found") > -1) {
                if ($("#doctorlist center").length == 0) {
                    $("#doctorlist").append(data);
                }
                $("#hdnPageIndex").val(-1);
            }
            else {
                $("#doctorlist").append(data);
                $("#hdnPageIndex").val(PageIndex);
                $("#count").html(parseInt($("#totalCount").val()) + parseInt($("#loopCount").val()));
                $("#totalCount").val(parseInt($("#totalCount").val()) + parseInt($("#loopCount").val()));

            }
            $("#divLoading").hide();
        },
        error: function (err) {
            // alert(err.statusText);
            $("#divLoading").hide();
        }
    });

}
var geocoder;
var map;
var bounds = ("undefined" !== typeof google) ? new google.maps.LatLngBounds() : null;
$(document).on("mouseenter", ".article-block", function () {
    var Id = $(this).attr("id");
    var ExactAddress = $(this).data("exactaddress");
    var Address2 = $(this).data("address");
    var City = $(this).data("location");
    var State = $(this).data("state");
    var Country = $(this).data("country");
    var ZipCode = $(this).data("zipcode");
    var Location = $(this).data("location");
    var Account = $(this).data("account");
    var lstDoctorAddressDetails = [{ "ExactAddress": ExactAddress, "Address2": Address2, "City": City, "State": State, "Country": Country, "ZipCode": ZipCode, "EmailAddress": "drmac@dentalmex.org", "Phone": "(404)521-8129", "Fax": "", "ContactType": 1, "Location": Location, "TimeZoneId": 0, "Mobile": "", "AccountName": Account }];
    initialize(lstDoctorAddressDetails);
});
function initialize(lst) {
    map = new google.maps.Map(
        document.getElementById("map_canvas"), {
            center: new google.maps.LatLng(33.2370638, -111.8443309),
            zoom: 12,
            minZoom: 2,
            maxZoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scaleControl: false,
            scrollwheel: false,
        });
    geocoder = new google.maps.Geocoder();
    for (i = 0; i < lst.length; i++) {
        geocodeAddress(lst[i]);
    }
}
function geocodeAddress(lst) {
    var address = '';
    address = lst.Location + ' ' + lst.ExactAddress
    if (lst.Address2) {
        address += "," + lst.Address2 + " "
    }
    address += " " + lst.City
    if (lst.State) {
        address += "," + (lst.State + " ")
    }
    if (lst.ZipCode) {
        address += lst.ZipCode
    }
    geocoder.geocode({
        'address': address
    },
        function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location,
                    title: address,
                    address: address,
                })

                var html = '<b>' + lst.AccountName + '</b><br/> ' + lst.ExactAddress
                if (lst.Address2) {
                    html += ",<br/>" + lst.Address2;
                }
                if (lst.City) {
                    html += " " + lst.City
                }
                if (lst.State) {
                    html += ",<br/>" + (lst.State + " ")
                }
                if (lst.ZipCode) {
                    html += lst.ZipCode
                }
                var infowindow = new google.maps.InfoWindow(
                    {
                        content: html,
                        size: new google.maps.Size(50, 50)
                    });
                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.open(map, marker);
                });
                bounds.extend(marker.getPosition());
                map.fitBounds(bounds);
            }

        });
}
$('.backtop').click(function () {
    $("html, body").animate({ scrollTop: 0 }, 600);
    return false;
});