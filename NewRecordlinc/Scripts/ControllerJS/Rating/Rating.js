﻿$(document).ready(function () {
    $('#StatusList li > a:first').trigger('click');
    $('#SortFieldList li > a:first').trigger('click');
    var initRating = 0;
    $(".patentRatingList").each(function () {
        $(this).rateYo({
            rating: parseFloat($(this).attr("ratingValue")),
            numStars: 5,
            precision: 2,
            minValue: 1,
            maxValue: 5,
            starWidth: "25px",
            spacing: "3px",
            readOnly: true
        });
    });
    $(".checkedAll").change(function () {
        if (this.checked) {
            $(".checkSingle").each(function () {
                $(this).prop('checked', true);
                $(".all-select span").removeClass('jcf-unchecked').addClass('jcf-checked');
                $("#btndelete").addClass('active');
                $("#btndelete").removeClass('disableClick');
                $("#btnapprove").addClass('active');
                $("#btnapprove").removeClass('disableClick');
            })
        } else {
            $(".checkSingle").each(function () {
                $(this).prop('checked', false);
                $(".all-select span").removeClass('jcf-checked jcf-focus').addClass('jcf-unchecked');
                $("#btndelete").removeClass('active');
                $("#btndelete").addClass('disableClick');
                $("#btnapprove").removeClass('active');
                $("#btnapprove").addClass('disableClick');
            })
        }
    });
    $(document).on('change', '.checkSingle', function (e) {
        var IsAnyCheckBoxChecked = 0;
        if ($(this).is(":checked")) {
            var isAllChecked = 0;
            $(".checkSingle").each(function () {
                if (!this.checked) {
                    isAllChecked = 1;
                }
            })
            if (isAllChecked == 0) { $(".checkedAll").prop("checked", true); $(".checkedAll").parent().removeClass('jcf-unchecked').addClass('jcf-checked'); }
        } else {
            $(this).parent().removeClass('jcf-focus');
            $(".checkedAll").prop("checked", false);
            $(".checkedAll").parent().removeClass('jcf-checked jcf-focus').addClass('jcf-unchecked')
        }
        $(".checkSingle").each(function () {
            if ($(this).is(":checked")) {
                IsAnyCheckBoxChecked++;
            }

        });
        if (IsAnyCheckBoxChecked > 0) {
            $("#btndelete").addClass('active');
            $("#btndelete").removeClass('disableClick');
            $("#btnapprove").addClass('active');
            $("#btnapprove").removeClass('disableClick');
        } else {
            $("#btndelete").removeClass('active');
            $("#btndelete").addClass('disableClick');
            $("#btnapprove").removeClass('active');
            $("#btnapprove").addClass('disableClick');
        }
    });
});

$('#StatusList li > a').click(function (e) {
    $('#btnViewOnly').text(this.innerHTML);
    $('.approvebtn').hide();
    $('.deletebtn').hide();
    $('#SortList').hide();
    var StatusID = ($(this).attr('data-id'));
    $("#hdddlviewonly").val($(this).attr('data-id'));
    var SortID = $("#hdddlsorting").val();
    $.ajaxSetup({ async: false });
    $.post("/Reports/GetRatingReport?status=" + StatusID + "&SortBy=" + SortID,
        function (data) {
            $('#ReportList').html(data);
            if (data.indexOf("No record found") <= 0) {
                ShowHideDiv(StatusID);
            }
        });
    $.ajaxSetup({ async: true });
});

function ShowHideDiv(StatusID) {
    $(".checkedAll").prop("checked", false);
    $(".checkedAll").parent().removeClass('jcf-checked jcf-focus').addClass('jcf-unchecked');
    if (StatusID == 1) {
        $('.approvebtn').hide();
        $('.deletebtn').show();
    }
    if (StatusID == 2) {
        $('.approvebtn').show();
        $('.deletebtn').hide();
    }
    if (StatusID == 0) {
        $('.approvebtn').show();
        $('.deletebtn').show();
    }
    $('#SortList').show();
}

$('#SortFieldList li > a').click(function (e) {
    var SortID = $(this).attr('data-id');
    $('#btnSortBy').text(this.innerHTML);
    $("#hdddlsorting").val($(this).attr('data-id'));
    var StatusID = $('#hdddlviewonly').val();
    $.ajaxSetup({ async: false });
    $.post("/Reports/GetRatingReport?status=" + StatusID + "&SortBy=" + SortID,
        function (data) {
            $('#ReportList').html(data);
            if (data.indexOf("No record found") <= 0) {
                ShowHideDiv(StatusID);
            }
        });
    $.ajaxSetup({ async: true });
});

function ApproveRating() {
    if ($("input[name='item.IsCheck']").is(":checked")) {
        swal({
            title: "",
            text: "Are you sure to approve selected rating(s) ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'POST',
                        url: '/Reports/ApproveRating',
                        data: $('form').serialize(),
                        dataType: 'json',
                        success: function (data) {
                            if (data == "1") {
                                showPaging($("#pageIndex").val());
                                swal({
                                    title: "Successfully",
                                    text: "Rating is approved.",
                                    timer: 1500,
                                    showConfirmButton: false
                                });
                            }
                            else {
                                swal({ title: "", text: 'Failed to approve rating', type: 'error' });
                            }
                        },
                        fail: function () {
                            swal({ title: "", text: 'Error occured while approving rating', type: 'error' });
                        }
                    });
                }
            });

    }
    else {
        swal({ title: "", text: 'Select at least one rating for approval.' });
    }
}

function DeleteRating() {
    if ($("input[name='item.IsCheck']").is(":checked")) {
        swal({
            title: "",
            text: "Are you sure to delete selected rating(s) ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Yes",
            cancelButtonText: "No",
            closeOnConfirm: false,
            closeOnCancel: true
        },
            function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        type: 'POST',
                        url: '/Reports/DeleteRating',
                        data: $('form').serialize(),
                        dataType: 'json',
                        success: function (data) {
                            if (data == "1") {
                                showPaging($("#pageIndex").val());
                                swal({
                                    title: "Successfully",
                                    text: "Rating(s) deleted.",
                                    timer: 1500,
                                    showConfirmButton: false
                                });
                            }
                            else {
                                swal({ title: "", text: 'Failed to delete rating', type: 'error' });
                            }

                        },
                    });
                }
            });

    }
    else {
        swal({ title: "", text: 'Select at least one rating to delete.' });
    }
}

function showPaging(page) {
    var StatusID = $('#hdddlviewonly').val();
    var SortID = $('#hdddlsorting').val();
    $("#pageIndex").val(page);
    $.ajaxSetup({ async: false });
    $.post("/Reports/GetRatingReport?status=" + StatusID + "&page=" + page + "&SortBy=" + SortID,
        function (data) {
            $('#ReportList').html(data);
            $(window).scrollTop(0);
            if (data.indexOf("No record found") <= 0) {
                ShowHideDiv(StatusID);
            }
            else {
                $('.approvebtn').hide();
                $('.deletebtn').hide();
                $('#SortList').hide();
                $(".checkedAll").prop("checked", false);
                $(".checkedAll").parent().removeClass('jcf-checked jcf-focus').addClass('jcf-unchecked');
            }
        });
    $.ajaxSetup({ async: true });
}