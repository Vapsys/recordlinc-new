﻿$(document).ready(function () {
    select2Dropdown('patientlist', 'Search Patient', 'Inbox', 'GetPatientList', false);
    //$("#patientlist").select2({
    //    placeholder: 'Search Patient',
    //    minimumInputLength: 1,
    //    multiple: false
    //});    
});

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function select2Dropdown(Select2ID, ph, Controller, listAction, isMultiple) {
    var sid = '#' + Select2ID;
    $(sid).select2({
        placeholder: ph,
        minimumInputLength: 1,
        multiple: isMultiple,
        ajax: {
            url: "/" + Controller + "/" + listAction,
            dataType: 'json',
            delay: 550,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            error: function (response, status, xhr) {
                if (response.status == 403) {
                    SessionExpire(response.responseText)
                }
            }
        }
    })
}
function loaddatepicker() {
    var date_input = $('input[name="date"]'); //our date input has the name "date"
    //var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
    date_input.datepicker({
        todayHighlight: true,
        autoclose: true,
        format: "mm/dd/yyyy",
        todayBtn: "linked",
        language: "de",
        "singleDatePicker": true,
        "showDropdowns": true,
        "autoApply": true
    });
}
$("#patientlist").on("select2:open", function () {
    $("#addbtndiv").css('display', 'none');
});
$("#patientlist").on("select2:close", function () {
    $("#addbtndiv").css('display', 'block');
});

$("#patientlist").on('change', function () {
    $("#searchpanel").hide();
    $("#PartialPlan").show();
    $(".reply-btn").show();
    var selectedPatient = $('#patientlist').val();
    performAjax({
        url: "/Plan/PlanPatientDetails",
        type: "post",
        data: { patientId: selectedPatient },
        async: true,
        success: function (data) {
            if (data != null && data != "") {
                $("#PartialPlan").html('');
                $("#PartialPlan").append(data);
                $('#tblPlanStage').on('click', '.delplan', function (event) {
                    $(this).closest('tr').remove();
                    $("tbody").children().each(function (index) {
                        $(this).find('#stepplan').html(index)
                        $(this).find('.txtdescription').attr('id', 'description_' + index);
                        $(this).find('.txtcompletiondate').attr('id', 'CompletionDate_' + index);
                        $(this).find('.ddlcolleaglst').attr('id', 'Colleaglst_' + index);
                        $(this).find('.hdncasestep').attr('id', 'CaseStepId_' + index);
                    });
                    return false;
                });
            }
            else {
                $("#PartialPlan").html("No record found!");
            }
        }
    });
});
function gotoback() {
    window.location.reload();
    //$("#searchpanel").show();
    //$("#PartialPlan").hide();
    $(".reply-btn").hide();
}
function Showloader() {
    $("#divLoading").show();
}
function Hideloader() {
    $("#divLoading").hide();
}
