﻿function FindDentist() {
    window.location.href = "/Profiles/SearchResult?keywords=" + $("#txtname").val().trim();
}
var geocoder;
var map;
var bounds = ("undefined" !== typeof google) ? new google.maps.LatLngBounds() : null;
function initialize(lst) {
    map = new google.maps.Map(
    document.getElementById("map_canvas"), {
        center: new google.maps.LatLng(37.4419, -122.1419),
        zoom: 12,
        minZoom: 2,
        maxZoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scaleControl: false,
        scrollwheel: false,
    });
    geocoder = new google.maps.Geocoder();
    for (i = 0; i < lst.length; i++) {
        geocodeAddress(lst[i]);
    }
}
function geocodeAddress(lst) {
    var address = '';
    address = lst.Location + ' ' + lst.ExactAddress
    if (lst.Address2) {
        address += "," + lst.Address2 + " "
    }
    address += " " + lst.City
    if (lst.State) {
        address += "," + (lst.State + " ")
    }
    if (lst.ZipCode) {
        address += lst.ZipCode
    }
    geocoder.geocode({
        'address': address
    },
 function (results, status) {
     if (status == google.maps.GeocoderStatus.OK) {
         var marker = new google.maps.Marker({
             map: map,
             position: results[0].geometry.location,
             title: address,
             address: address,
         })

         var html = lst.Location + '<br/> ' + lst.ExactAddress
         if (lst.Address2) {
             html += "," + lst.Address2;
         }
         html += "<br/> " + lst.City
         if (lst.State) {
             html += "," + (lst.State + " ")
         }
         if (lst.ZipCode) {
             html += lst.ZipCode
         }
         var infowindow = new google.maps.InfoWindow(
                         {
                             content: html,
                             size: new google.maps.Size(150, 50)
                         });
         google.maps.event.addListener(marker, 'click', function () {
             infowindow.open(map, marker);
         });
         bounds.extend(marker.getPosition());
         map.fitBounds(bounds);
     }

 });
}
$(document).ready(function () {
    if ("undefined" !== typeof google) {
        initialize(lstDoctorAddressDetails);
        $(".customheader").removeClass('inner').addClass('inner profile');
    }
    var is = $("#IsAppOpen").val();
    if (is != null && is != "") {
        $("#plzopen").trigger('click');
    }

    $(".patentRatingList").each(function () {

        $(this).rateYo({
            rating: parseFloat($(this).attr("ratingValue")),
            numStars: 5,
            precision: 2,
            minValue: 1,
            maxValue: 5,
            starWidth: "25px",
            spacing: "3px",
            readOnly: true
        });

    });
    var $lightbox = $('#lightbox');
    $('[data-target="#lightbox"]').on('click', function (event) {
        var $img = $(this).find('img'),
            src = $img.attr('src'),
            alt = $img.attr('alt'),
            css = {
                'maxWidth': $(window).width() - 100,
                'maxHeight': $(window).height() - 100
            };

        $lightbox.find('.close').addClass('hidden--');
        $lightbox.find('img').attr('src', src);
        $lightbox.find('img').attr('alt', alt);
        $lightbox.find('img').css(css);
    });

    var $lightbox_video = $('#lightbox_video');

    $('[data-target="#lightbox_video"]').on('click', function (event) {
        var $video = $(this).find('iframe'),
            src = $video.attr('src'),
            alt = $video.attr('alt'),
            css = {
                'maxWidth': $(window).width() - 100,
                'maxHeight': $(window).height() - 100
            };

        $lightbox_video.find('.close').addClass('hidden--');
        $lightbox_video.find('iframe').attr('src', src);
        $lightbox_video.find('iframe').attr('alt', alt);
        $lightbox_video.find('iframe').css(css);
    });

    $lightbox_video.on('shown.bs.modal', function (e) {
        var $video = $lightbox_video.find('video');

        $lightbox_video.find('.modal-dialog').css({ 'width': $video.width() });
        $lightbox_video.find('.close').removeClass('hidden');
    });

    $('#AppointmentModal').on('shown.bs.modal', function () {
        $(".showMoreSlots").show();
        $(".loadMoreSlots").hide();
    })

    $('#AppointmentModal').on('hidden.bs.modal', function () {
        setTimeout(function () { $('[data-target="#AppointmentModal"]').blur(); }, 10);
    });
})

$(document).ready(function () {
    var $lightbox_video = $('#lightbox_video');

    $('[data-target="#lightbox_video"]').on('click', function (event) {
        var $video = $(this).find('iframe'),
            src = $video.attr('src'),
            alt = $video.attr('alt'),
            css = {
                'maxWidth': $(window).width() - 100,
                'maxHeight': $(window).height() - 100
            };

        $lightbox_video.find('.close').addClass('hidden--');
        $lightbox_video.find('iframe').attr('src', src);
        $lightbox_video.find('iframe').attr('alt', alt);
        $lightbox_video.find('iframe').css(css);
    });

    $lightbox_video.on('shown.bs.modal', function (e) {
        var $video = $lightbox_video.find('video');

        $lightbox_video.find('.modal-dialog').css({ 'width': $video.width() });
        $lightbox_video.find('.close').removeClass('hidden');
    });

    $(".showMoreSlots").click(function () {
        $(".showMoreSlots").hide();
        $(".loadMoreSlots").show();
    })

    $(".timing .date_slot span").not(".empty").click(function (e) {
        bookAppointment(e);
    });
});

function GetDefaultOP(LocationId) {
    var DoctorId = $.trim($("#DDoctorId").val());
    var encoded = encodeURIComponent($.trim($("#PublicProfileUrl").val()));
    var TheatreId = 0;
    $.ajaxSetup({ async: false });
    $.ajax({
        url: "/p/" + encoded + "/GetDefaultOP?DoctorId=" + DoctorId + "&LocationId=" + LocationId,
        type: "GET",
        success: function (data) {
            if (data) {
                TheatreId = data.id;
                $("#AppointmentDefaultResourceId").val(TheatreId);
            }
            GetTimeslotGrid();
        },
        error: function (data) {
            //alert("error while binding theatre.");
            swal({ title: "", text: 'Error while binding theater', type: 'error' });
        }
    });
    $.ajaxSetup({ async: true });
    return TheatreId;
}
function GetTimeslotGrid(Startdate) {
    Startdate = (Startdate) ? $("#cal" + Startdate + "Day").val() : "";
    $("#loadtimeslotegrid").empty();
    var DoctorId = $.trim($("#DDoctorId").val());
    var ProfileUrl = $.trim($("#PublicProfileUrl").val());
    var encoded = encodeURIComponent(ProfileUrl);
    var TheatreId = $.trim($("#AppointmentDefaultResourceId").val());

    if (DoctorId.length > 0 && ProfileUrl.length > 0 && TheatreId.length > 0) {
        $("#loadtimeslotegrid").load("/p/" + encoded + "/AppointmentSlotGrid?DoctorId=" + DoctorId + "&StartDate=" + Startdate + "&TheatreId=" + TheatreId + "", function () {
            $(".showMoreSlots").click(function () {
                $(".showMoreSlots").hide();
                $(".loadMoreSlots").show();
            })

            $(".timing .date_slot span").not(".empty").click(function (e) {
                bookAppointment(e);
            });
        });


    }
    else {
        $("#loadtimeslotegrid").empty();
    }

}

function bookAppointment(e) {
    var ctrl = e.target;
    if ($(ctrl).text().toLowerCase() == "more") {
        return;
    }
    var apptDate = new Date($(ctrl).closest(".date_slot").attr("for") + " " + $(ctrl).text());
    var apptTime = apptDate.getHours() + ":" + apptDate.getMinutes()
    console.log(new Date($(ctrl).parent().attr("for") + " " + $(ctrl).text()));
    var encoded = encodeURIComponent($.trim($("#PublicProfileUrl").val()));
    var frmdata = {
        DoctorId: $.trim($("#DDoctorId").val()),
        AppointmentDate: $(ctrl).closest(".date_slot").attr("for"),
        AppointmentTime: apptTime,
        ServiceTypeId: $.trim($("#AppointmentServiceId").val()),
        ApptResourceId: $.trim($("#AppointmentDefaultResourceId").val()),
        InsuranceId: $.trim($("#InsuranceId").val()).length == 0 ? "0" : $.trim($("#InsuranceId").val())
    }

    if ($.trim($("#AppointmentServiceId").val()).length == 0) {
        showErrorMsg("Please select service", null, "appt_");
        setTimeout(function () { $("#AppointmentServiceId").focus() }, 0);
        return;
    }
    $.ajax({
        url: "/p/" + encoded + "/CheckOverLapping",
        type: "post",
        data: frmdata,
        success: function (data) {
            if ($.trim(data).length != 0) {
                showErrorMsg(data, null, "appt_");
            }
            else {
                $("#AppointmentDate").val(frmdata.AppointmentDate);
                $("#AppointmentTime").val(frmdata.AppointmentTime);
                $("#ServiceTypeId").val(frmdata.ServiceTypeId);
                $("#InsuranceId_form").val(frmdata.InsuranceId);
                $("#__AjaxAntiForgeryForm").submit();

            }
        }
    });
}
