﻿
function AddTeamMember(UserId, ParentUserId) {
    var FirstName = $('#txtAddFirstName').val();
    var LastName = $('#txtAddLastName').val();
    var Email = $('#txtAddEmail').val();
    var Address2, state, city, country, zip = null;
    var strSpecialities = $('#drpSpeciality').val();
    //var Address2 = $('#txtAddAddress2').val();
    //var state = $('#drpAddState').val();
    //var city = $('#txtAddCity').val();
    //var country = $('#drpAddCountry').val();
    //var zip = $('#txtAddZipcode').val();
    var LocationId = $("#LocationId").val();
    var ProviderId = $("#txtProviderId").val();
    if (FirstName != null && FirstName != "") {
        var FirstNameCheck = IsCharactersOnly(FirstName);
        if (FirstNameCheck == false) {
            swal({ title: "", text: 'Please enter a valid First Name.' });
            $('#txtAddFirstName').focus();
            return false;
        }
        else {
            if (FirstName.length > 30) {
                swal({ title: "", text: 'First Name allow maximum 30 characters only.' });
                $('#txtAddFirstName').focus();
                return false;
            }
        }
    }
    else {
        swal({ title: "", text: 'Please enter First Name.' });
        $('#txtAddFirstName').focus();
        return false;
    }
    if (LastName != null && LastName != "") {
        var LastNameCheck = IsCharactersOnly(LastName);
        if (LastNameCheck == false) {
            swal({ title: "", text: 'Last Name allow characters only.' });
            $('#txtAddLastName').focus();
            return false;
        }
        else {
            if (LastName.length > 30) {
                swal({ title: "", text: 'Last Name allow maximum 30 characters only.' });
                $('#txtAddLastName').focus();
                return false;
            }
        }
    }
    else {
        swal({ title: "", text: 'Please enter Last Name.' });
        $('#txtAddLastName').focus();
        return false;
    }
    if (Email != null && Email != "") {
        var resultEmail = IsEmail(Email);
        if (resultEmail != true) {
            swal({ title: "", text: 'Please enter a valid Email Address.' });
            $('#txtAddEmail').focus();
            return false;
        }
    }
    else {
        swal({ title: "", text: 'Please enter Email.' });
        $('#txtAddEmail').focus();
        return false;
    }
    if (strSpecialities == "0") {
        swal({ title: "", text: 'Please select Specialty.' });
        $('#drpSpeciality').focus();
        return false;
    }
    //if (Address2 == null || Address2 == "") {
    //    swal({ title: "", text: 'Please enter street address.' });
    //    $('#txtAddAddress2').focus();
    //    return false;
    //}
    //if (state == "0") {
    //    swal({ title: "", text: 'Please select state.' });
    //    $('#drpAddState').focus();
    //    return false;
    //}
    //if (country == "0") {
    //    swal({ title: "", text: 'Please select country.' });
    //    $('#drpAddCountry').focus();
    //    return false;
    //}
    //if (city == null || city == "") {
    //    swal({ title: "", text: 'Please enter city.' });
    //    $('#txtAddCity').focus();
    //    return false;
    //}
    //if (zip == null || zip == "") {
    //    swal({ title: "", text: 'Please Enter Valid ZipCode.' });
    //    $('#txtAddZipcode').focus();
    //    return false;

    //}
    //else {
    //    if (zip != null && zip != "") {
    //        var regx = ValidateZipCode(zip);
    //        if (!regx) {
    //            swal({ title: "", text: 'Please Enter Valid ZipCode.' });
    //            $('#txtAddZipcode').focus();
    //            return false;
    //        }
    //    }
    //}

    $.post("/Profile/CheckEmailExistsAsDoctor",
                                { Email: Email }, function (data) {
                                    if (data != null && data != "") {
                                        $("#divLoading").show();
                                        $.post("/Profile/TeamMemberInsertAndUpdate",
                                { UserId: UserId, ParentUserId: ParentUserId, Email: Email, FirstName: FirstName, LastName: LastName, OfficeName: $('#txtAddOfficeName').val(), ExactAddress: $('#txtAddExactAddress').val(), Address2: Address2, City: city, State: state, Country: country, Zipcode: zip, Specialty: strSpecialities, LocationId: LocationId, ProviderId: ProviderId }, function (data) {
                                    if (data == "1") {
                                        $("#divLoading").hide();
                                        //swal("", "Team member added successfully!", "success");
                                        swal({
                                            title: "Successfully",
                                            text: "Team member added successfully",
                                            timer: 1500,
                                            showConfirmButton: false
                                        });
                                        window.location = document.URL;
                                    }
                                    else {
                                        swal({ title: "", text: 'Failed to add team member.' });
                                    }
                                });
                                    }
                                    else {
                                        swal({ title: "", text: 'This Email Address is already in use.' });
                                    }
                                });
    $("#divLoading").hide();
}
function EditTeamMemberDetailsById(UserId, TeamMemberId) {
    $.ajaxSetup({ async: false });
    $.post("/Profile/GetTeamMemberDetailsById", { UserId: UserId, TeamMemberId: TeamMemberId }, function (data) {
        if (TeamMemberId != 0) {
            document.getElementById("teams").innerHTML = data.toString();
        }
        else {
            document.getElementById("add_team_members").innerHTML = data.toString();
        }
    });
    $.ajaxSetup({ async: true });
}
