$(document).ready(function () {
    $("input[name='optradio']").click(function () {
        //if ($(this).is(":checked"))
        //    $(".Yes-show").show();
        $("#activate-step-2").show();

        if ($(this).val() == "1") {
            $(".Yes-show").show();
            $(".No-show").hide();
        }
        else {
            $(".Yes-show").hide();
            $(".No-show").show();
        }


    });
    $('.clsnumericOnly').mask("(999) 999-9999");
    $(".trigger_pro").click(AddNewProcedure);
});

function AddNewProcedure() {
    $("#procedures-section").append($(".procedures-form-group").first().outerHTML());

    $(".trigger_pro")
        .addClass("remove")
        .unbind()
        .html('Remove')
        .click(function () {
            RemoveElement(this);
        });

    $(".trigger_pro")
        .last()
        .removeClass("remove")
        .html("Add Another")
        .unbind()
        .click(function () {
            AddNewProcedure();
        });
}

function ShowCommunication(ctrl) {
    var selector = "." + $(ctrl).attr("class") + "-show";
    $(selector + " input").val("");
    $(selector).toggle();
}

function RemoveElement(ctrl) {
    $(ctrl).parent().remove();
}

//-- Logic for insurnace estimator form

function Submitdata() {
    var DentalProcedureId = [];
    var EstimateViaCall;
    var EstimateViaText;
    var EstimateViaEmail;
    var InsuranceFees = $("#Fees").val();
    var isInsurance = $('input:radio[name=optradio]:checked').val();
    var InsuranceCompanyId = $("#Insurance").val();
    var token = $('input[name="__RequestVerificationToken"]').val();
    var isProcedureSame = false;
    $(".procedure").each(function () {
        if (DentalProcedureId.indexOf(parseInt($(this).val())) >= 0) {
            // showmessage("procedure", "You could not select same value on the Procedures");
            showErrorMsg(formatMessageForInsurance("You can not Select same value on the Procedures"));
            isProcedureSame = true;
            return false;
        }
        if ($.trim($(this).val()).length != 0)
            DentalProcedureId.push(parseInt($(this).val()));
    });
    if (isProcedureSame) {
        return false;
    }
    if (DentalProcedureId.length == 0) {
        showErrorMsg(formatMessageForInsurance("Please Select Procedure"));
        return false;
    }
    if (isInsurance == 0) {
        if (InsuranceFees == "" || InsuranceFees == null) {
            //showmessage("Fees", "Please select Insurance Fees");
            showErrorMsg(formatMessageForInsurance("Please Select Insurance Fees"));
            return false;
        }
        else {
            if ($(".Call_No").is(":checked")) {
                var Mobile = $.trim($("#Notxtcall").val());
                if (Mobile.length == 0) {
                    showErrorMsg(formatMessageForInsurance("Please Enter valid Phone Number to Inform via Call"));
                    return false;
                }
                if (Mobile.match(/\d/g).length < 10) {
                    showErrorMsg(formatMessageForInsurance("Please Enter valid Phone Number to Inform via Call"));
                    return false;
                }
                EstimateViaCall = Mobile;
            }
            if ($(".Text_No").is(":checked")) {
                var Mobile = $.trim($("#Notxttext").val());
                if (Mobile.length == 0) {
                    showErrorMsg(formatMessageForInsurance("Please enter valid phone number to inform via text"));
                    return false;
                }
                if (Mobile.match(/\d/g).length < 10) {
                    showErrorMsg(formatMessageForInsurance("Please Enter valid Phone Number to Inform via Text"));
                    return false;
                }
                EstimateViaText = Mobile;
            }
            if ($(".Email_No").is(":checked")) {
                if (!IsEmail($("#Notxtemail").val())) {
                    //showmessage("Notxtemail", "Please enter valid email address");
                    showErrorMsg(formatMessageForInsurance("Please Enter valid Email Address to Inform via Email"));
                    return false;
                }
                EstimateViaEmail = $("#Notxtemail").val();
            }
            //if (!($(".Call_No").first().is(":checked") || $(".Text_No").first().is(":checked") || $(".Email_No").first().is(":checked"))) {
            //  showErrorMsg(formatMessageForInsurance("Please select Call, Text or Email."))
            //    return false;
            // }

        }
    } else {
        if (InsuranceCompanyId == "" || InsuranceCompanyId == null) {
            // showmessage("Insurance", "Please select Insurance");
            showErrorMsg(formatMessageForInsurance("Please Select Insurance"));
            return false;
        }
        else {
            if ($(".Call").is(":checked")) {
                var Mobile = $.trim($("#Yestxtcall").val());
                if (Mobile.length == 0) {
                    showErrorMsg(formatMessageForInsurance("Please Enter valid Phone Number to Inform via Call"));
                    return false;
                }
                if (Mobile.match(/\d/g).length < 10) {
                    showErrorMsg(formatMessageForInsurance("Please Enter valid Phone Number to Inform via Call"));
                    return false;
                }
                EstimateViaCall = Mobile;
            }
            if ($(".Text").is(":checked")) {
                var Mobile = $.trim($("#Yestxttext").val());
                if (Mobile.length == 0) {
                    showErrorMsg(formatMessageForInsurance("Please Enter valid Phone Number to Inform via Text"));
                    return false;
                }
                if (Mobile.match(/\d/g).length < 10) {
                    showErrorMsg(formatMessageForInsurance("Please Enter valid Phone Number to Inform via Text"));
                    return false;
                }
                EstimateViaText = $("#Yestxttext").val();
            }
            if ($(".Email").is(":checked")) {
                if (!IsEmail($("#Yestxtemail").val())) {
                    // showmessage("Yestxtemail", "Please enter valid email address");
                    showErrorMsg(formatMessageForInsurance("Please Enter valid Email Address to Inform via Email"));
                    return false;
                }
                EstimateViaEmail = $("#Yestxtemail").val();
            }

            //if (!($(".Call").first().is(":checked") || $(".Text").first().is(":checked") || $(".Email").first().is(":checked"))) {
            //  showErrorMsg(formatMessageForInsurance("Please select Call, Text or Email."))
            // return false;
            // }
        }
    }
    $("#step-1").css("display", "none");
    $("#divLoading").css("display", "block");
    var model = {};
    model = {
        'DentalProcedureId': DentalProcedureId,
        'isInsurance': isInsurance == 1,
        'InsuranceCompanyId': InsuranceCompanyId,
        'EstimateViaCall': EstimateViaCall,
        'EstimateViaText': EstimateViaText,
        'EstimateViaEmail': EstimateViaEmail,
        'FeesType': InsuranceFees,
        'PatientName': $("#FirstName").val()
    }
    $.ajax({
        url: "/p/" + CURRENT_USER + "/CalculateInsurance",
        type: "POST",
        data: { __RequestVerificationToken: token, model: model },
        success: function (data) {

            $("#divLoading").css("display", "none");
            $("#divFees").css("display", "block");
            if ($.trim(data.TotalCost).length > 0) {
                var vTotalCost = data.TotalCost.toFixed(2);
                $("#totalcost").html("$" + vTotalCost);
            }


            if ($.trim(data.ServiceCost).length > 0) {
                var vServiceCost = parseFloat(data.ServiceCost).toFixed(2);
                $("#lbl_serviceCost").text("$" + vServiceCost);
                if (parseFloat(data.ServiceCost) <= 0) {
                    $("#section_services").hide();
                }
                else {
                    var serviceCostText = "$" + vServiceCost + " time of service";
                    $("#serviceCost").html(serviceCostText);
                }
            }

            if ($.trim(data.ThreePayments).length > 0) {
                var vThreePayments = parseFloat(data.ThreePayments).toFixed(2);
                var threePaymentText = "3 payments (60 days) $" + vThreePayments + "/each";
                $("#threePayments").html(threePaymentText);
            }

            if ($.trim(data.PaymentEMI).length > 0) {
                var vPaymentEMI = parseFloat(data.PaymentEMI).toFixed(2);
                var paymentText = "Compassionate dental finance from $" + vPaymentEMI + "/month";
                $("#paymentEMI").html(paymentText);
            }


            $("#HistoryId").val(data.HistoryId);

            $(".procedurename").html(data.ProceduresName);
        },
        error: function (err) {
            showErrorMsg(formatMessageForInsurance(err.statusText));
        }
    });
}

function changestep() {
    $("#divFees").css("display", "none");
    $("#divAppointment").css("display", "block");
    $('.clsnumericOnly').mask({ mask: '(999) 999-9999' });
}
function signuppatient() {
    //if ($.trim($("#FirstName").val()).length == 0) {
    //    showErrorMsg(formatMessageForInsurance("Please enter first name"), null, "last")
    //    return false;
    //}
    //if ($.trim($("#LastName").val()).length == 0) {
    //    showErrorMsg(formatMessageForInsurance("Please enter last name"), null, "last")
    //    return false;
    //}
    //if ($.trim($("#Email").val()).length == 0) {
    //    showErrorMsg(formatMessageForInsurance("Please enter email address"), null, "last")
    //    return false;
    //}
    //else if (!IsEmail($("#Email").val())) {
    //    showErrorMsg(formatMessageForInsurance("Please enter valid email address"), null, "last")
    //    return false;
    //}
    //if ($.trim($("#Phone").val()).length == 0) {
    //    showErrorMsg(formatMessageForInsurance("Please enter phone number"), null, "last")
    //    return false;
    //}
    if ($("#frmPatientSignup").valid()) {
        var PatientSignup = {
            'FirstName': $("#FirstName").val(),
            'LastName': $("#LastName").val(),
            'Email': $("#Email").val(),
            'Phone': $("#Phone").val()

        }

        var TotalCost = $("#totalcost").html().replace("$", "");
        var ProcedureName = $(".procedurename").first().html();
        var InsuranceCompanyName = $("#Insurance option:selected").text();
        var EstimateViaCall = null;
        var EstimateViaText = null;
        var EstimateViaEmail = null;
        var ServiceCost = $("#lbl_serviceCost").html().replace("$", "");
        var isInsurance = $('input:radio[name=optradio]:checked').val();
        if (isInsurance == 0) {
            if ($(".Call_No").is(":checked")) {
                EstimateViaCall = $("#Notxtcall").val();
            }
            if ($(".Text_No").is(":checked")) {
                EstimateViaText = $("#Notxtcall").val();
            }
            if ($(".Email_No").is(":checked")) {
                EstimateViaEmail = $("#Notxtemail").val();
            }
        } else {
            if ($(".Call").is(":checked")) {
                EstimateViaCall = $("#Yestxtcall").val();
            }
            if ($(".Text").is(":checked")) {
                EstimateViaText = $("#Yestxttext").val();
            }
            if ($(".Email").is(":checked")) {
                EstimateViaEmail = $("#Yestxtemail").val();
            }
        }
        var SelectedChoices = "";

        $.ajax({
            url: "/p/" + CURRENT_USER + "/SendRequest",
            type: "POST",
            data: {
                objPatient: PatientSignup,
                TotalCost: TotalCost,
                ProceduresName: ProcedureName,
                SelectedChoice: SelectedChoices,
                HistoryId: $("#HistoryId").val(),
                InsuranceCompanyName: InsuranceCompanyName,
                EstimateViaCall: EstimateViaCall,
                EstimateViaEmail: EstimateViaEmail,
                EstimateViaText: EstimateViaText,
                ServiceCost: ServiceCost
            },
            success: function (data) {
                $(window).scrollTop(5);
                showSuccessMsg(formatMessageForInsurance("Thank you for using Insurance Cost Estimator"), 5000, "last");
                setTimeout(function () { location.href = "/p/" + CURRENT_USER + "/InsuranceEstimatorWidget"; }, 6000);
                $("#divAppointment input[type='text']").val("");
            },
            error: function (err) {
                showErrorMsg(formatMessageForInsurance(err.statusText));
            }
        });
    }
}
function lstone() {
    var DentalProcedureId = [];
    var EstimateViaCall;
    var EstimateViaText;
    var EstimateViaEmail;
    var InsuranceFees = $("#Fees").val();
    var isInsurance = $('input:radio[name=optradio]:checked').val();
    var InsuranceCompanyId = $("#Insurance").val();
    var token = $('input[name="__RequestVerificationToken"]').val();
    var isProcedureSame = false;
    $(".procedure").each(function () {
        if (DentalProcedureId.indexOf(parseInt($(this).val())) >= 0) {
            // showmessage("procedure", "You could not select same value on the Procedures");
            showErrorMsg(formatMessageForInsurance("You could not Select same value on the Procedures"));
            isProcedureSame = true;
            return false;
        }
        if ($.trim($(this).val()).length != 0)
            DentalProcedureId.push(parseInt($(this).val()));
    });
    if (isProcedureSame) {
        return false;
    }
    if (DentalProcedureId.length == 0) {
        showErrorMsg(formatMessageForInsurance("Please Select a Dental Procedure"));
        return false;
    }
    if (isInsurance == 0) {
        if (InsuranceFees == "" || InsuranceFees == null) {
            //showmessage("Fees", "Please select Insurance Fees");
            showErrorMsg(formatMessageForInsurance("Please Select Insurance Fees"));
            return false;
        }
        else {
            if ($("input:checkbox[name=Call]:checked")) {
                EstimateViaCall = $("#Notxtcall").val();
            }
            if ($("input:checkbox[name=Text]:checked")) {
                EstimateViaText = $("#Notxtcall").val();
            }
            if ($(".Email_No").is(":checked")) {
                if (!IsEmail($("#Notxtemail").val())) {
                    //showmessage("Notxtemail", "Please enter valid email address");
                    showErrorMsg(formatMessageForInsurance("Please Enter valid Email Address"));
                    return false;
                }
                EstimateViaEmail = $("#Notxtemail").val();
            }
            // if (!($(".Call_No").first().is(":checked") || $(".Text_No").first().is(":checked") || $(".Email_No").first().is(":checked"))) {
            //     showErrorMsg(formatMessageForInsurance("Please select Call, Text or Email."))
            //    return false;
            //  }

        }
    } else {
        if (InsuranceCompanyId == "" || InsuranceCompanyId == null) {
            // showmessage("Insurance", "Please select Insurance");
            showErrorMsg(formatMessageForInsurance("Please Select Insurance"));
            return false;
        }
        else {
            if ($("input:checkbox[name=Call]:checked")) {
                EstimateViaCall = $("#Yestxtcall").val();
            }
            if ($("input:checkbox[name=Text]:checked")) {
                EstimateViaText = $("#Yestxttext").val();
            }
            if ($(".Email").is(":checked")) {
                if (!IsEmail($("#Yestxtemail").val())) {
                    // showmessage("Yestxtemail", "Please enter valid email address");
                    showErrorMsg(formatMessageForInsurance("Please Enter valid Email Address"));
                    return false;
                }
                EstimateViaEmail = $("#Yestxtemail").val();
            }

            //if (!($(".Call").first().is(":checked") || $(".Text").first().is(":checked") || $(".Email").first().is(":checked"))) {
            //   showErrorMsg(formatMessageForInsurance("Please select Call, Text or Email."))
            //     return false;
            // }
        }
    }
    //$("#step-1").css("display", "none");
    // $("#divLoading").css("display", "block");
    var model = {};
    model = {
        'DentalProcedureId': DentalProcedureId,
        'isInsurance': isInsurance == 1,
        'InsuranceCompanyId': InsuranceCompanyId,
        'EstimateViaCall': EstimateViaCall,
        'EstimateViaText': EstimateViaText,
        'EstimateViaEmail': EstimateViaEmail,
        'FeesType': InsuranceFees,
        'PatientName': $("#FirstName").val() + " " + $("#LastName").val()
    }
    $.ajax({
        url: "/p/" + CURRENT_USER + "/CalculateInsurance",
        type: "POST",
        data: { __RequestVerificationToken: token, model: model },
        success: function (data) {
            $(window).scrollTop(5);
            showSuccessMsg(formatMessageForInsurance("Thank you for using Insurance Cost Estimator"), null, "last")
            $("#divAppointment input[type='text']").val("");
            setTimeout(function () {
                window.location.reload();
            }, 5000)
        },
        error: function (err) {
            showErrorMsg(formatMessageForInsurance(err.statusText));
        }
    });
}
function formatMessageForInsurance(msg) {
    return "<center> " + msg + "</center>";
}