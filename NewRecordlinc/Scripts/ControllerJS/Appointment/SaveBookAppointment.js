﻿$(document).ready(function () {
    $("#lbltimeselection").hide();
    $("#lblslotmessage").hide();
    $("#loadtimeslotegrid").empty();
    //$("#hdAppointmentTime").val("");
    var FromTime = $("#hdFromtime").val();

    GetTimeslotGrid(FromTime);
    $("#hdStep").val("3");

})
var selecteddate = '';
function GetTimeslotGrid(FromTime) {
    $("#loadtimeslotegrid").empty();
    var DentistId = $("#hddentistId").val();
    var TheatreID = $("#hdTheatreId").val();
    $("#hdFromtime").val(FromTime);
    $("#loadtimeslotegrid").load("/Appointment/AppointmentSlotGrid?DoctorId=" + DentistId + "&StartDate=" + FromTime + "&TheatreID=" + TheatreID + "&AppointmentId=" + CheckAppointmentIdExist(), function (response, status, xhr) {
        if (xhr.status == 403) {
            SessionExpire(xhr.responseText);
        }
        $("#txtdate").datepicker({
            minDate: 0,
            format: 'm-d-Y',
            onSelect: function (dateText) {
                GetTimeslotGrid(this.value)
            }
        });
        if (new Date($("#dvprev").attr("data-id")) <= new Date()) {
            $("#dvprev").hide();
        } else {
            $("#dvprev").show();
        }
        var i = 0, j = 0, k = 0;
        $('.devboxlop a').each(function (index, item) {
            k++;
            if ($("#hdselectedtime").val()) {
                if ($(this).hasClass('blacakatebox')) {
                    j++;
                }
                if ($("#hdselectedtime").val() === $(item).html()) {

                    if (!$(this).hasClass('graydatebox') && !$(this).hasClass('blacakatebox')) {
                        i++;
                        $(this).click();

                    }
                }
            }
        });
        if (i == 0 && j == 0) {
            $("a").removeClass("appointmentactivecls");
            $("#hdAppointmentTime").val('');
            $("#dvslotnewdesc").hide();
            //$("#hdselectedtime").val('');
        }
        if (k > 0) { $("#dvslotnewdesc").show(); }
    });
}
function GotoPrevStep2() {
    var LocationId = $("#ddlLocation").val();
    if (LocationId == "" && LocationId == null) {
        LocationId = 0;
    }
    //check dentist id with querystring doctor id;
    var UserId = 0;
    var TempDoctorId = getParameterByName('DoctorId');
    var HdDoctorId = $("#hddentistId").val();
    if (TempDoctorId == HdDoctorId) {
        UserId = HdDoctorId;

    }
    var frotime = $("#hdFromtime").val();
    $("a").removeClass("appointmentactivecls");
    $("#hdAppointmentTime").val('');
    // $("#hdselectedtime").val('');
    if (parseInt($("#hdAppointmentId").val()) > 0 || parseInt($("#hdmode").val()) > 0) {
        $("#modelbookedappdetailsviewbody").load("/Appointment/CreateAppointmentTimeSlotView?PatientId=" + $("#PaintId").val() + "&CalndFromTime=" + frotime + "&LocationId=" + LocationId + "&DoctorId=" + UserId, function (response, status, xhr) {
            if (xhr.status == 403) {
                SessionExpire(xhr.responseText)
            }
            $("#btnPrevStep1").hide();
        });
    } else {
        $(".bookappointmentInnederBody").load("/Appointment/CreateAppointmentTimeSlotView?PatientId=" + $("#PaintId").val() + "&CalndFromTime=" + frotime + "&LocationId=" + LocationId, function (response, status, xhr) {
            if (xhr.status == 403) {
                SessionExpire(xhr.responseText)
            }
        });
    }

}
function Step2Validation() {
    var ServiceId = $("#ddlServiceId :selected").val();
    if (ServiceId == '') {
        swal({ title: "", text: 'Please Select Service.' });
        return false;
    } else { $("#hdserviceId").val(ServiceId); $("#hdserviceName").val($("#ddlServiceId :selected").text()) }
    var TheatreId = $("#ddlTheatrelst :selected").val();
    if (TheatreId == '') {
        swal({ title: "", text: 'Please Select Theatre.' });
        return false;

    } else { $("#hdTheatreId").val(TheatreId); $("#hdTheatreName").val($("#ddlTheatrelst :selected").text()) }
    var DentistId = $("#ddlDentistlst :selected").val();
    if (DentistId == '') {
        swal({ title: "", text: 'Please Select Dentist.' });
        return false;
    } else {
        $("#hddentistId").val(DentistId);

    }
    $(".bookappointmentInnederBody").load("/Appointment/CreateAppointmentTimeSlotView?PatientId=" + $("#PaintId").val() + "&CalndFromTime=" + frotime + "&LocationId=" + LocationId, function (response, status, xhr) {
        if (xhr.status == 403) {
            SessionExpire(xhr.responseText)
        }

    });
}
function PrevStep1() {
    var ServiceId = $("#ddlServiceId :selected").val();
    $("#hdserviceId").val(ServiceId); $("#hdserviceName").val($("#ddlServiceId :selected").text())
    var TheatreId = $("#ddlTheatrelst :selected").val();
    $("#hdTheatreId").val(TheatreId); $("#hdTheatreName").val($("#ddlTheatrelst :selected").text())
    var DentistId = $("#ddlDentistlst :selected").val();
    $("#hddentistId").val(DentistId);

    $('#divLoading').show();
    $("#modelbookappointmentbody").load("/Appointment/BookAppointment?CalFromTime=" + $('#hdFromtime').val(), function (response, status, xhr) {
        if (xhr.status == 403) {
            SessionExpire(xhr.responseText)
        }
        renderCircles();
        $('#divLoading').hide();
    });
}
function BookAppointmentForPatientFn(Bdate, Btime, id, Bday, time) {
    $("a").removeClass("appointmentactivecls");
    $("a.blacakatebox").addClass("devdatebox").removeClass("blacakatebox");
    $("#TimeCheckBx_" + id).addClass("appointmentactivecls");
    $(".xdsoft_label").css("z-index", "0");
    $("#hdAppointmentTime").val(Btime);
    selecteddate = Bdate;

}
function CheckAppointmentIdExist() {
    var AppointmetId = 0;
    if (parseInt($("#hdAppointmentId").val()) > 0) {
        AppointmetId = parseInt($("#hdAppointmentId").val());
    }
    return AppointmetId
}
function SaveAppointment() {
    if ($("#hdAppointmentTime").val()) {
        var objCreateAppointment = {};
        if (parseInt($("#hdAppointmentId").val())) { objCreateAppointment.AppointmentId = $("#hdAppointmentId").val(); } else { objCreateAppointment.AppointmentId = 0; }
        objCreateAppointment.DoctorId = $("#hddentistId").val().trim();
        objCreateAppointment.PatientId = $("#PaintId").val().trim();
        objCreateAppointment.CurrentDate = $("#hdFromtime").val().trim();
        objCreateAppointment.FromTime = $("#hdAppointmentTime").val().trim();
        objCreateAppointment.AppointmentLength = $("#hdAppointmentlength").val().trim();
        objCreateAppointment.ServiceId = $("#hdserviceId").val().trim();
        objCreateAppointment.ApptResourceId = $("#hdTheatreId").val().trim();
        objCreateAppointment.Note = $("#txtnote").val().trim();
        var frmdata = $("#frmAppointmentBookingData1").serialize();
        var tempdate = new Date(selecteddate);
        tempdate = getFormattedDate(tempdate);
        var newdate = new Date(tempdate + ' ' + objCreateAppointment.FromTime);
        //var now = new Date();
        //if (newdate < now) {
        //    $("#lblslotmessage").html("This time has past please select a future time.");
        //    $("#lblslotmessage").show();
        //    setTimeout(function () { $("#lblslotmessage").hide(); $("#lblslotmessage").html(""); }, 5000);
        //    return false;
        //}
        //Check OverLapping
        var retunflag = true;
        $.ajaxSetup({ async: false });
        $.ajax({
            url: "/Appointment/CheckOverLapping",
            type: "post",
            data: objCreateAppointment,
            success: function (data) {
                if (data != "") {
                    $("#lblslotmessage").html(data);
                    $("#lblslotmessage").show();
                    setTimeout(function () { $("#lblslotmessage").hide(); $("#lblslotmessage").html(""); }, 5000);
                    retunflag = false;
                    return false;
                }
            },
            error: function (response, status, xhr) {
                if (response.status == 403) {
                    SessionExpire(response.responseText)
                }
            }
        });
        $.ajaxSetup({ async: true });
        if (!retunflag) {
            return retunflag;
        }
        $.ajaxSetup({ async: false });
        $.ajax({
            url: "/Appointment/SubmitAppointmentBookingData",
            type: "post",
            data: objCreateAppointment,
            success: function (data) {
                if (objCreateAppointment.AppointmentId > 0) {
                    $('#calendar').fullCalendar('removeEvents', objCreateAppointment.AppointmentId);
                }
                var EventDataList = [];
                $.each(data, function (index, item) {
                    var eventData = new Object();
                    var EventTitle = "";
                    if (item.PatientName) { EventTitle += item.PatientName + " \n" }
                    if (item.ServiceName) { EventTitle += item.ServiceName + " \n" }
                    if (item.PatientsPhone) { EventTitle += item.PatientsPhone + " \n" }
                    if (item.DoctorName) { EventTitle += "Dr. " + item.DoctorName + " \n" }
                    eventData.title = EventTitle;
                    eventData.backgroundColor = item.EventBackgroundColor;
                    eventData.textColor = item.EventTextColor
                    eventData.borderColor = item.EventBorderColor
                    eventData.start = item.FromDateTime;
                    eventData.end = item.ToDateTime;
                    eventData.id = item.AppointmentId;
                    eventData.isProcessed = true;
                    eventData.ForMonth = new Date().getMonth();
                    eventData.FromDateTime = new Date(item.FromDateTime);
                    eventData.resourceId = item.ApptResourceId.toString();
                    EventDataList.push(eventData);
                    if ($("#ddlDentist").val() == item.DoctorId || $("#ddlDentist").val() == 0) {
                        $('#calendar').fullCalendar('renderEvent', eventData, true);
                    }
                });
                var str = '';
                if (!$.trim($("#hdPatientName").val())) {
                    $.ajaxSetup({ async: false });
                    $.post("/Patients/getPatientDetailsById/" + $("#PaintId").val(), function (data) {
                        $("#hdPatientName").val(data);
                    });
                    $.ajaxSetup({ async: true });
                }
                
                if ($.trim($("#hddentistName").val()) && $.trim($("#hdPatientName").val())) {
                    str = "The appointment for " + $.trim($("#hdPatientName").val()) + " has been booked with Dr. " + $.trim($("#hddentistName").val()) + " at " + moment(new Date("2017/05/04 " + $.trim($("#hdAppointmentTime").val()))).format("hh:mm A") + " on " + moment(new Date($("#hdtempselecteddate").val())).format("MMMM Do, YYYY");
                }

                ClearHiddenFileds();
                if (objCreateAppointment.AppointmentId > 0 || parseInt($("#hdmode").val()) > 0) {
                    $('#modelbookedappdetailsview').modal('hide');
                    $("#modelbookedappdetailsviewbody").html("");
                }
                else {
                    $('#modelbookappointment').modal('hide');
                    $("#modelbookappointmentbody").html("");
                }
                swal({
                    title: '',
                    text: objCreateAppointment.AppointmentId > 0 ? 'Your appointment has been updated!' : "<span style='font-size:14px;'>Your appointment has been created! " + str + "</span>",
                    html: true,
                    type: "",
                    // imageUrl: "../content/images/thumbs-up.jpg"
                    // timer: 5000
                }, function (isConfirm) {
                    window.location.href = "/Appointment";
                });

            },
            error: function (response, status, xhr) {
                if (response.status == 403) {
                    SessionExpire(response.responseText)
                }
            }
        });
        $.ajaxSetup({ async: true });
    }
    else {
        $("#lbltimeselection").show();
        setTimeout(function () {
            $("#lbltimeselection").hide();
        }, 2000);
        return false;

    }
}
function getFormattedDate(date) {
    var year = date.getFullYear();
    var month = (1 + date.getMonth()).toString();
    month = month.length > 1 ? month : '0' + month;
    var day = date.getDate().toString();
    day = day.length > 1 ? day : '0' + day;
    return month + '/' + day + '/' + year;
}
function gettodaydate() {
    var d = new Date();
    return (d.getMonth() + 1) + "-" + d.getDate() + "-" + d.getFullYear();
}
function ShowCalender() {
    $('#txtdate').show().focus().hide();

}