﻿var NumberOfCountAllows = 0, DoesMembershipAllow = false;
var timer = null;
function GetNumberOfCountAllows() {
    $.post("/Common/GetNumberOfCountAllow",
                                    { FeatureId: 13 }, function (data) {
                                        NumberOfCountAllows = data.MaxCount;
                                        DoesMembershipAllow = data.DoesMembershipAllow;
                                    });
}
jQuery(document).ready(function () {
    ShowDownloadZipSection();
    GetNumberOfCountAllows();
    var timer;
    $('ul.search_listing li').on('click', function (event) {
        $('ul.search_listing li div').css('border-color', '#C8C8C8');
        var myInput = $('input', this)
        if (myInput.attr('checked')) {
            var divid = myInput.attr('value');
            $("#" + divid).css('border-color', '#C8C8C8');
            myInput.attr('checked', false);
        }
        else {
            var divid = myInput.attr('value');
            $("input:checked").attr('checked', false);
            $("#" + divid).css('border-color', '#0890C4');
            $("#hdnSelectedPatientGender").val($("#" + divid).attr("data-val"))
            myInput.attr('checked', true);
        }
        event.stopPropagation();
    });
    $("input.checkbox_add_patient:checkbox").on('click', function (event) {
        $('ul.search_listing li div').css('border-color', '#C8C8C8');
        var myInput = $(this);
        if (myInput.attr('checked')) {
            var divid = myInput.attr('value');
            $("input:checked").attr('checked', false);
            $("#" + divid).css('border-color', '#0890C4');
            $("#hdnSelectedPatientGender").val($("#" + divid).attr("data-val"))
            myInput.attr('checked', true);
        }
        else {
            var divid = myInput.attr('value');
            $("input:checked").attr('checked', false);
            $("#" + divid).css('border-color', '#C8C8C8');
            myInput.attr('checked', false);
        }
        event.stopPropagation();
    });
    $(".search_listing").bind('scroll', function (event) {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {

            var data = SearchColleagueOnScroll().toString();
            $(this).append(data);
        }
    });
    $('#add_doctor_popup').on('keypress', function (e) {
        var Keycodeof = e.keyCode
        if (Keycodeof == 13) {
            $("#SearchBtnHide").trigger('click');
        }
    });
    // add family member    
    $('#sendReferral').on('keypress', function (e) {
        var Keycodeof = e.keyCode
        if (Keycodeof == 13) {
            $("#SearchBtnHide").trigger('click');
        }
    });
    jQuery('.green_btn').on('mouseenter', function () {
        jQuery(this).find('.tooltipc').show();
        return false;
    });
    jQuery('.green_btn').on('mouseout', function () {
        jQuery(this).find('.tooltipc').hide();
        return false;
    });
    $('#SelectTeamMemberandColleagues').keyup(function () {
        clearTimeout(timer);
        timer = setTimeout(RecieverRepeat, 10)
    });
    BindUserListofteam();
    $("#SelectTeamMemberandColleagues").change(function () {
        $("#selectTeam_annoninput").find("input").attr("placeholder", "Add or remove referring colleagues here");
        $("#selectTeam_annoninput").find("input").css("width", "424px");
    });
    $("#txtMessageFromPatientHistory").click(function () {
        $(".maininput").focus();
        $(".maininput").blur();
        $("#txtMessageFromPatientHistory").focus();
    });

    $("input.checkcolleaguelocation:checkbox").on('click', function (event) {
        $(".location_each").css('border-color', '#C8C8C8');
        var myInput = $(this);
        if (myInput.attr('checked')) {
            var divid = myInput.attr('value');
            $("input:checked").attr('checked', false);
            $("#" + divid).css('border-color', '#0890C4');
            myInput.attr('checked', true);
        }
        else {
            var divid = myInput.attr('value');
            $("input:checked").attr('checked', false);
            $("#" + divid).css('border-color', '#C8C8C8');
            myInput.attr('checked', false);
        }
        event.stopPropagation();
    });
    $('div.class_locationcolleagues div').on('click', function (event) {
        $(".location_each").css('border-color', '#C8C8C8');
        var myInput = $('input', this)
        if (myInput.attr('checked')) {
            var divid = myInput.attr('value');
            $("#" + divid).css('border-color', '#C8C8C8');
            $("input:checked").attr('checked', false);
            myInput.attr('checked', false);
        }
        else {
            var divid = myInput.attr('value');
            $("#" + divid).css('border-color', '#0890C4');
            $("input:checked").attr('checked', false);
            myInput.attr('checked', true);
        }
        event.stopPropagation();
    });

});
function RecieverRepeat() {
    var lav = $('#SelectTeamMemberandColleagues').children('ul').children('li').attr('rel');
    var relattr = "";
    $('#SelectTeamMemberandColleagues').children('ul').children('li').each(function () {
        relattr += ',' + $(this).attr('rel');
    });
    relattr = relattr.split(',');
    for (i = 0; i < relattr.length; ++i) {
        $('#SelectTeamMemberandColleagues').children('div').children('ul').children('li').each(function () {
            if (relattr[i] == $(this).attr('rel')) {
                $(this).css('display', 'none');
            }
        });
    }
}
//Changes for RM-240
function ToggleSaveButton(id) {
    $('#' + id).attr('disabled', true);
    setTimeout(function () {
        $('#' + id).attr('disabled', false);
    }, 4000);
}

function IsProfileImage()
{
    
    if ($("#fuImage").val() == 'undefined' || $("#fuImage").val() == '')
    {
        $("#txtimage").text('Please choose an appropriate image.');
        ToggleSaveButton('fuImage');
    }
    else
    {
        $("#txtimage").text('');
        $("#frmPatientProfileImage").submit();
    }
}

function RedirectToPrint(PatientId) {
    debugger;
    var DentalSite = $('#ExternalCall').val();
    var patientId = PatientId;
    patientId = (parseInt(patientId) + 45844584);
    var UserId = $('#SessionCall').val();
    UserId = (parseInt(UserId) + 45844584);
    window.open(DentalSite + "PrintForms?Printid=" + patientId + '&UserId=' + UserId, '_blank');
}
function readURL(input) {    
    if (input.files && input.files[0]) {
        
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.jcrop-holder').remove();
            $('#cropDisplay').empty().html('<img id="profileImageEditor" src="' + e.target.result + '"/>');

            jQuery('#profileImageEditor').Jcrop({
                onChange: showPreview,
                onSelect: showPreview,
                setSelect: [0, 10, 100, 100],
                aspectRatio: 1,
                boxWidth: 500,
                boxHeight: 500
            });
        }

        reader.readAsDataURL(input.files[0]);

    }
}
$("#fuImage").change(function () {
    readURL(this);
});
function showPreview(coords) {
    if (parseInt(coords.w) > 0) {
        $('#Top').val(coords.y);
        $('#Left').val(coords.x);
        $('#Bottom').val(coords.y2);
        $('#Right').val(coords.x2);
    }
}

// add family member    
function OpenPatientsPopup(PatientId) {
    $("#sendReferral").html('');
    $('#txtSearchPatient').val('');
    $('#hdselectedpatient').val('');
    SearchPatient(PatientId);
}
function SearchPatient(PatientId) {

    document.getElementById('search_listing_pageindex').value = "1";
    var GetSearchText = null;
    if (document.getElementById('txtSearchPatient') == null) {
        GetSearchText = null;
    } else if ($('#txtSearchPatient').length == 0) {
        GetSearchText = null;
    }
    else {
        if (document.getElementById('txtSearchPatient').value == "Find patient to add as family member..." || document.getElementById('txtSearchPatient').value == "Find colleague to refer to...") {
            GetSearchText = null;
        }
        else {
            GetSearchText = document.getElementById('txtSearchPatient').value;
        }
    }
    $.ajaxSetup({ async: false });
    $.post("/Patients/SearchPatientFromPopUp",
                                { SearchText: GetSearchText, PatientId: PatientId }, function (data) {
                                    document.getElementById("sendReferral").innerHTML = data;
                                });
    $.ajaxSetup({ async: true });
    $(".search_listing").bind('scroll', function (event) {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            var data = SearchPatientLI().toString();
            $(this).append(data);
        }
    });
}
function SearchPatientLI() {
    
    $.ajaxSetup({ async: false });
    var GetSearchText = null; if ($("#txtSearchPatient").val() == null) {
        GetSearchText = null;
    } else if ($('#txtSearchPatient').length == 0) {
        GetSearchText = null;
    }
    else {
        if ($("#txtSearchPatient").val() == "Find patient to add as family member..." || $("#txtSearchPatient").val() == "Find colleague to refer to...") {
            GetSearchText = null;
        }
        else {
            GetSearchText = $("#txtSearchPatient").val();
        }
    }

    var lidata = '';
    var strpageindex = $("#search_listing_pageindex").val();
    if (strpageindex == "-1") {
        return "";
    }
    else {

        strpageindex = parseInt(strpageindex) + parseInt(1);
        $("#search_listing_pageindex").val(strpageindex);


        $.post("/Patients/SearchPatientListForSendReferralPopUpLI",
                            { SearchText: GetSearchText, pageindex: strpageindex, pagesize: '30' }, function (data) {
                                if (data != null) {
                                    if (data == '' || data == "<center>No Record Found</center>" || data == "<center>No More Record Found</center>") {                                        
                                        $("#search_listing_pageindex").val('-1');
                                    }
                                }
                                lidata = data.toString();
                            });
    }
    $.ajaxSetup({ async: true });
    return lidata;
}
function CloseEditPatientListpop() {
    $(".mfp-close").click();
}
function AddPatientsAsMemeber() {
    var PatientId = null;
    var c = new Array();
    c = document.getElementsByTagName('input');
    for (var i = 0; i < c.length; i++) {
        if (c[i].type == 'checkbox' && c[i].id == 'chkPatient' && c[i].checked == true) {
            if (PatientId == null) {
                PatientId = c[i].value;
            }
            else {
                PatientId = PatientId + ',' + c[i].value;
            }
        }
    }
    if (PatientId == null) {
        swal({ title: "", text: 'Notice: Please select any one Colleague.' });
    }
    else {
        $("#hdselectedpatient").val(PatientId);
        GetFamilyDetails();

    }


}

function GetFamilyDetails() {
    var PatientId = document.getElementById('hdnPatientId').value;
    $.ajaxSetup({ async: false });
    $.post("/Patients/PartialFamilyMembers", { PatientId: PatientId }, function (data) {
        $(".search_main_div").hide();
        $(".cntnt").hide();
        $("#ColleagueLocation").html(data);
        $("#TitleRefer").html("Select Relation");
    });
    $.ajaxSetup({ async: true });
}

//////////////
//function GetAllPatientDocument() {

//    $.post("/Patients/GetPatientImagesAll", { PatientId: '@Model.PatientId' }, function (data) {
//        if (data != null && data != '') {
//            $('#divPatientImages').empty().html(data);
//        }

//    });

//    $.post("/Patients/GetPatientDocumentsAll", { PatientId: '@Model.PatientId' }, function (data) {
//        if (data != null && data != '') {
//            $('#divPatientDocuments').empty().html(data);
//        }
//    });

//}
//////////////
function CheckIsImage() {
    var item = $("#fuImage").val();
    var extension = item.replace(/^.*\./, '');
    extension = extension.toLowerCase();
    //Change for RM-404:Blank page for .tif image
    if (extension == "jpg" || extension == "jpeg" || extension == "gif" || extension == "png" || extension == "bmp") {
        return true;
    }
    else if ($("#fuImage").val() == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select an image.' });
        ToggleSaveButton('fuImage');
        //alert('Please select an image');
        return false;
    } else {
       // swal({ title: "", text: 'Please upload only Jpg,Png and Gif Image.' });
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please upload only Jpg,Png,Bmp and Gif Image.' });
        $("#fuImage").val('');
        ToggleSaveButton('fuImage');
        // alert('Please upload only Jpg,Png and Gif Image');
        return false;
    }

}
//////////////
//function SendPatientPassword(PatientId) {
//    $.ajaxSetup({ async: false });

//    $.post("/Patients/SendPasswordResetLinkToPatinet", { PatientId: PatientId }, function (data) {
//        swal({ title: "", text: data });
//        // alert(data);
//    });
//    $.ajaxSetup({ async: true });
//}

// comunication dropdown
$(window).load(function () {
    $("#selectTeam_annoninput").find("input").attr("placeholder", "Add or remove referring colleagues here");
    $("#selectTeam_annoninput").find("input").css("width", "424px");
});
function BindUserListofteam() {
    jQuery.ajaxSetup({ async: false });
    var x = '';
    $.post("/Patients/ByDefaultselectedTeamMemberandTreatingDoctorList",
          function (data) {
              x = data;
          });
    jQuery.ajaxSetup({ async: true });
    var ColleaguesId = 0;
    if ($('#selectTeam').val() != null && $('#selectTeam').val() != "") {
        x = $('#selectTeam').val().toString();
    }
    if (ColleaguesId != null && ColleaguesId != "") {
        x = ColleaguesId;
    }

    jQuery.ajaxSetup({ async: false });
    $.post("/Patients/GetUserListofTeam", { selected: x },
        function (data) {
            if (data != '') {
                document.getElementById("SelectTeamMemberandColleagues").innerHTML = '';
                document.getElementById("SelectTeamMemberandColleagues").innerHTML = data.toString();
                $("#selectTeam").select2({
                    minimumInputLength: 1
                })
            }
            else {
                document.getElementById("SelectTeamMemberandColleagues").innerHTML = '';
                document.getElementById("SelectTeamMemberandColleagues").innerHTML = 'Doctor not found';
            }
        });
    jQuery.ajaxSetup({ async: true });
}

// function for view Message dialog
function ViewDoctorReferralView(Id, MessageTypeId) {
    $("#divLoading").show();
    $.post("/Patients/GetMessageBodyOfInboxReferrals",
                                        { MessageId: Id, MessageTypeId: MessageTypeId }).done(function (data) {
                                            if (data != null && data != "") {
                                                $('#patient_file_referral').modal('show');
                                                $('#patient_file_referral').html(data);
                                                var hreflink = $("#ReplyBtn").attr("href");
                                                $("#ReferralReplyBtn").attr("href", hreflink);
                                                $("#ReplyBtn").hide();
                                                $("#DeleteBtn").hide();                                                
                                                $("#divLoading").hide();
                                            }
                                        })
 .fail(function (xhr, textStatus, errorThrown) {
     $("#divLoading").hide();
 });
}

// function for Sorting history
function PatientCommunicationHistorySorting(PatientId, SortCol) {
    var SortDir = $("#PatientHCol_" + SortCol).attr('sort-dir');
    if (SortDir == 1) {
        $("#PatientHCol_" + SortCol).attr('sort-dir', 2);
    }
    else {
        $("#PatientHCol_" + SortCol).attr('sort-dir', 1);
    }
    $("#PatientHistoryThead").empty();
    LoadPatientHistoryForPagingAndSorting(PatientId, 1, SortCol, SortDir);
    if (SortDir == 1) {
        $("#PatientHCol_" + SortCol).attr('sort-dir', 2);
    }
    else {
        $("#PatientHCol_" + SortCol).attr('sort-dir', 1);
    }
}
function LoadPatientHistoryForPagingAndSorting(PatientId, PageIndex, SortColumn, SortDirection) {
    $.ajaxSetup({ async: false });
    $.post("/Patients/GetPatientHostoryCommunicationOnSorting", { PatientId: PatientId, PageIndex: PageIndex, SortColumn: SortColumn, SortDirection: SortDirection },
   function (data) {
       if (data == 'No record found') {
           $("#PageIndexCommPatientHistory").val(0);
           $("#PatientHistoryThead").append("<tr><td style='width:18% !important;text-align:center' colspan='5'></td></tr>");
       }
       else {
           //$('#communicationHistory').html('');           
           $('#CommunicationAppend').html(data);
       }
   });
    $.ajaxSetup({ async: true });
}
function LoadPatientscrolling(PatientId, PageIndex, SortColumn, SortDirection) {
    $.ajaxSetup({ async: false });
    $.post("/Patients/LoadPatientscrolling", { PatientId: PatientId, PageIndex: PageIndex, SortColumn: SortColumn, SortDirection: SortDirection },
   function (data) {

       if ($.trim(data) == '') {
           
           $("#PageIndexCommPatientHistory").val(parseInt(0));
           $("#PatientHistoryThead").append("<tr><td style='width:18% !important;text-align:center' colspan='5'></td></tr>");
       }
       else {
           
           //$('#communicationHistory').html('');
           //var t = $('#communicationHistory');        
           //var g = $(data).find('#patientHistoryscrollId').html();
           //$($(data).find('#patientHistoryscrollId').html()).insertAfter($('#communicationHistory'));
           //console.log(g);
           //var s = $(g).insertAfter(t);
           //t.append(g);
           $('#CommunicationAppend').append(data);
       }
   });
    $.ajaxSetup({ async: true });
}
// View Colleagues message
function ViewMessageOfColleagues(id, messagetypeid) {
    $("#divLoading").show();
    $.post("/Patients/ViewColleagueMessage", { MessageId: id }).done(function (data) {
        if (data != null && data != "") {
            $('#colleague_file_message').modal('show');
            $("#colleague_file_message").html(data);            
            $("#divLoading").hide();
        }
    })
 .fail(function (xhr, textStatus, errorThrown) {
     $("#divLoading").hide();
 });
    $("#divLoading").hide();
    $.ajaxSetup({ async: true });
}
// View patients message
function ViewMessage(id) {
    $("#divLoading").show();
    $.post("/Patients/ViewPatientMessage", { MessageId: id }).done(function (data) {
        if (data != null && data != "") {
            $('#patient_file_message').modal('show');
            $("#patient_file_message").html(data);            
            $("#divLoading").hide();
        }
    })
 .fail(function (xhr, textStatus, errorThrown) {
     $("#divLoading").hide();
 });
}
// close all popup
function RemovePopupCss() {
    $(".mfp-close").trigger('click');
    $("#showMessageForPatient").empty();
    $("#bootstraprm").remove();
}

function SendMessageFromPatientHistory(PatientId) {
    
    var LengthOfMessage = 0;
    $(".template-download").each(function () {
        LengthOfMessage++;
    });
    if (DoesMembershipAllow == true && NumberOfCountAllows) {
        if (parseInt(NumberOfCountAllows) > 0) {
            if (NumberOfCountAllows < LengthOfMessage) {
                swal({
                    title: "",
                    text: "You have reached the maximum number of patient documents allowed with your plan. Please <a href='/FreeToPaid/UpgradeAccount'>click here</a> to upgrade.",
                    html: true
                });
                return false;
            }
        }
    }
    else if (DoesMembershipAllow == false) {
        swal({
            title: "",
            text: "You can't send attachments with your current plan. Please <a href='/FreeToPaid/UpgradeAccount'>click here</a> to upgrade.",
            html: true
        });
        return false;
    }

    var senders = $('#selectTeam').val();
    if (senders == null || senders == '') {
        swal({ title: "", text: 'Please select colleague.' });
        return;
    }
    else {
        //Pass RecieverIds when you will have treating doctor
        var RecId = $('#selectTeam').val() + '';
        if (RecId == null || RecId == '' || RecId == 'null') {
            RecId = '';
        }

        var Message = $("#txtMessageFromPatientHistory").val();
        if (Message == '' || Message.trim() == '') {
            swal({ title: "", text: 'Message can not be blank.' });
            return;
        }
        $("#divLoading").show();
        $.post("/Patients/SentMessageFromPatientHistory", { PatientId: PatientId, Message: Message, RecieverIds: RecId }).done(function (data) {
            //LoadPatientHistoryForPagingAndSorting(PatientId, 1, 1, 2);
            BindUserListofteam();
            ShowDownloadZipSectionAttached();
            $("#txtMessageFromPatientHistory").val('');
            $('.files').html('');
            $("#divLoading").hide();
            var pid = parseInt($('#hdnPatientId').val());
            $("#hdnViewPatientHistory").val(pid);
            $("#frmViewPatientHistory").submit();

        })
      .fail(function (xhr, textStatus, errorThrown) {
          $("#divLoading").hide();
      });
    }
}
function ShowDownloadZipSection() {
    if ($('#IsAuthorize').val() !== undefined) {
        var auth = $('#IsAuthorize').val();
        if (auth != false || auth == false) {
            var pDoc = $('#DocumentCount').val();
            var pImg = $('#ImageCount').val();
            if (parseInt(pDoc) > 0 || parseInt(pImg) > 0) {
                $('#_downloadZip').css('display', 'block');
            }
        }
    }
}
function ShowDownloadZipSectionAttached() {
    if ($('#IsAuthorize').val() !== undefined) {
        var auth = $('#IsAuthorize').val();
        if (auth != false || auth == false) {
            var pDoc = $('#pImgCount').val();
            var pImg = $('#pDocCount').val();
            if (parseInt(pDoc) > 0 || parseInt(pImg) > 0) {
                $('#_downloadZip').css('display', 'block');
            }
            else {
                $('#_downloadZip').css('display', 'none');
            }
        }
    }
}
function FillNotesPopUp(PatientId, Id, Type) {
    $.post("/Patients/FillNotesPopUpById", { PatientId: parseInt(PatientId), Id: parseInt(Id), Type: Type },
        function (data) {

            if (data != null && data != "") {
                $("#patient_file_edit").modal('show');
                $("#patient_file_edit").html(data);
                //$("#txtcreated").datepicker();
                //$("#txtmodified").datepicker();
                //$('#Updatenotes').html('');
                //$('#Updatenotes').html(data);
                //$('#OpenEditNotes').click();                
                //$('.chkdate').keypress(function (e) {
                //    var key_codes = [127, 8];
                //    if (!($.inArray(e.which, key_codes) >= 0)) {
                //        e.preventDefault();
                //    }
                //    $('.chkdate').val("");
                //});
            }
            else {
                $('#patient_file_edit').html('');
            }
        });
}
//function ClickUpdateNote() {
//    $("#txtcreated").datepicker();
//    $('.chkdate').keypress(function (e) {
//        var key_codes = [127, 8];
//        if (!($.inArray(e.which, key_codes) >= 0)) {
//            e.preventDefault();
//        }
//        $('.chkdate').val("");
//    });
//    $("#txtmodified").datepicker();
//    $('.chkdate').keypress(function (e) {
//        var key_codes = [127, 8];
//        if (!($.inArray(e.which, key_codes) >= 0)) {
//            e.preventDefault();
//        }
//        $('.chkdate').val("");
//    });
//}
function UpdateDescriptionById(Id, Type) {
    
    var creation = $('#txtcreated').val();
    var modified = $('#txtmodified').val();
    var Notes = $('#txtnotes').val();
    if (Type == "Image") {
        $.post("/Patients/UpdateDescriptionofImage", { Id: Id, CreationDate: creation, LastModifiedDate: modified, Description: Notes }, function (data) {
            if (data != null && data != "") {
                swal({ title: "", text: 'Your image description updated successfully', showConfirmButton: false, timer: 5000 }, function () {
                    var pid = parseInt($('#hdnPatientId').val());
                    $("#hdnViewPatientHistory").val(pid);
                    $("#frmViewPatientHistory").submit();
                });
                //GetAllPatientImagesOnPostBack($('#PatientId').val());                
            }
        });
    }
    else {
        $.post("/Patients/UpdateDescriptionofDocument", { Id: Id, CreationDate: creation, LastModifiedDate: modified, Description: Notes }, function (data) {
            if (data != null && data != "") {
                swal({ title: "", text: 'Your document has been uploaded successfully', showConfirmButton: false, timer: 5000 }, function () {
                    var pid = parseInt($('#hdnPatientId').val());
                    $("#hdnViewPatientHistory").val(pid);
                    $("#frmViewPatientHistory").submit();
                });                
            }
        });
    }
}
function GetAllPatientImagesOnPostBack(PatientId) {
    $.ajaxSetup({ async: false });
    $.post("/Patients/GetAllPatientImagesByID", { PatientId: PatientId },
    function (data) {
        if (data == 'No record found') {
            $("#PatientImagesSection").append("<tr><td style='width:18% !important;text-align:center' colspan='5'></td></tr>");
        }
        else {
            $('#PatientImagesSection').html('');
            $('#PatientImagesSection').html(data);
            ShowDownloadZipSectionAttached();
        }
    });
    $.ajaxSetup({ async: true });
}
function GetAllPatientDocumentsOnPostBack(PatientId) {
    $.ajaxSetup({ async: false });
    $.post("/Patients/GetAllPatientDocumentByID", { PatientId: PatientId },
    function (data) {
        if (data == 'No record found') {
            $("#PatientImagesSection").append("<tr><td style='width:18% !important;text-align:center' colspan='5'></td></tr>");
        }
        else {
            $('#PatientDocumentsSection').html('');
            $('#PatientDocumentsSection').html(data);
            ShowDownloadZipSectionAttached();
        }
    });
    $.ajaxSetup({ async: true });
}


function DeleteDocumentPopup(MontageId, PatientId) {
    $.get("/Patients/ShowDeletePopup", function (data) {
        $("#patient_file_delete").modal('show');
        $("#patient_file_delete").html(data);
        $("#del_desc").text('Are you sure you want to remove document?');
        $("#deletePatientFileId").attr("onclick", 'RemovePatientDocument("' + MontageId + '","' + PatientId + '")');
    });
}

function DeleteImagePopup(ImagesId, MontageId, PatientId) {
    $.get("/Patients/ShowDeletePopup", function (data) {
        $("#patient_file_delete").modal('show');
        $("#patient_file_delete").html(data);
        $("#del_desc").text('Are you sure you want to remove image?');
        $("#deletePatientFileId").attr("onclick", 'RemovePatientImage("' + ImagesId + '","' + MontageId + '","' + PatientId + '")');
    });
}

function RemovePatientImage(ImageId, MontageId, PatientId) {
    
    $.ajaxSetup({ async: false });
    $.post("/Patients/RemovePatientImage", { ImageId: ImageId, MotangeId: MontageId, PatientId: PatientId }, function (data) {
        if (data != null && data != "") {
            //GetAllPatientDocumentsOnPostBack(PatientId);
            //$("body").removeClass("stop-scrolling");            
            var pid = parseInt($('#hdnPatientId').val());
            $("#hdnViewPatientHistory").val(pid);
            $("#frmViewPatientHistory").submit();
        }
        else {
            $("#del_desc").text('Failed to remove Image, try again.');            
        }
    });
    $.ajaxSetup({ async: true });
}

function RemovePatientDocument(MontageId, PatientId)
{
    $.ajaxSetup({ async: false });
    $.post("/Patients/RemovePatientDocument", { MotangeId: MontageId, PatientId: PatientId }, function (data) {
        if (data != null && data != "") {
            //GetAllPatientDocumentsOnPostBack(PatientId);
            //$("body").removeClass("stop-scrolling");            
            var pid = parseInt($('#hdnPatientId').val());
            $("#hdnViewPatientHistory").val(pid);
            $("#frmViewPatientHistory").submit();
        }
        else {
            $("#del_desc").text('Failed to remove Image, try again.');
        }
    });
    $.ajaxSetup({ async: true });
}
  
function CallBackUploadMethod(fileuplad) {
    if (fileuplad != undefined) {
        var status = fileuplad.options.ctrlType;
        if (status == false) {
            var item = $('.name').html()
            var extension = item.replace(/^.*\./, '');
            $('#_uploaderContentFile').html('');
            if (extension == "jpg" || extension == "jpeg" || extension == "gif" || extension == "png" || extension == "bmp" || extension == "psd" || extension == "pspimage" || extension == "thm" || extension == "tif" || extension == "yuv") {
                var pid = parseInt($('#hdnPatientId').val());
                $("#hdnViewPatientHistory").val(pid);
                $("#frmViewPatientHistory").submit();
            }
            else {
                var pid = parseInt($('#hdnPatientId').val());
                $("#hdnViewPatientHistory").val(pid);
                $("#frmViewPatientHistory").submit();
            }
        }
    }
}
function ClosePopUp() {
    $(".mfp-close").click();
}
function GetLocationColleagues() {

    var PatientId = document.getElementById('hdnPatientId').value;
    var PatientId = $('#PatientId').val();
    var MultipleColleague = null;
    var c = new Array();
    c = document.getElementsByTagName('input');
    for (var i = 0; i < c.length; i++) {
        if (c[i].type == 'checkbox' && c[i].id == 'checkcolleague' && c[i].checked == true) {
            if (MultipleColleague == null) {
                MultipleColleague = c[i].value;
            }
            else {
                MultipleColleague = MultipleColleague + ',' + c[i].value;
            }
        }
    }

    if (MultipleColleague != null && MultipleColleague != "") {
        $.ajaxSetup({ async: false });
        $.post("/Patients/GetLocationColleagues", { ColleagueId: MultipleColleague, PatientId: PatientId }, function (data) {
            if (data == "2") {
                swal({ title: "", text: 'Something is Wrong.' });
            }
            if (data == "1") {
                if (MultipleColleague != null && MultipleColleague != "") {
                    callRefral(PatientId, MultipleColleague, undefined);
                }
                else {
                    swal({ title: "", text: 'Please select at least one colleague.' });
                }
            }
            else {
                $(".patient_searchbar").hide();
                $(".cntnt").hide();
                $("#ColleagueLocation").html(data);
                $("#ReferralTitle").html("Select Location");
                $("#ColleaguesId").val(MultipleColleague);
                $("#PatientId").val(PatientId);
            }

        });
        $.ajaxSetup({ async: true });
    }
    else {
        swal({ title: "", text: 'Please select at least one colleague.' });
    }
}
function callRefral(PatientId, ColleagueIds, ColleagueLocation) {
    if (ColleagueLocation != undefined && ColleagueLocation != null) {
        $.ajaxSetup({ async: false });
        var url = '/Referrals/Referral?PatientId=' + PatientId + '&ColleagueIds=' + ColleagueIds + '&AddressInfoId=' + ColleagueLocation;
        window.location.href = url;
        $.ajaxSetup({ async: true });
    }
    else {
        $.ajaxSetup({ async: false });
        var url = '/Referrals/Referral?PatientId=' + PatientId + '&ColleagueIds=' + ColleagueIds;
        window.location.href = url;
        $.ajaxSetup({ async: true });
    }
}

function SendReferralSelectLocation() {
    var ColleagueLocation = null;
    var c = new Array();
    c = document.getElementsByClassName('checkcolleaguelocation');
    for (var i = 0; i < c.length; i++) {
        if (c[i].type == 'checkbox' && c[i].id == 'colleaguelocation' && c[i].checked == true) {
            if (ColleagueLocation == null) {
                ColleagueLocation = c[i].value;
            }
            else {
                ColleagueLocation = ColleagueLocation + ',' + c[i].value;
            }
        }
    }
    var ColleaguesId = $("#ColleaguesId").val();
    var PatientId = $("#PatientId").val();
    if (ColleagueLocation != null && ColleagueLocation != "") {
        callRefral(PatientId, ColleaguesId, ColleagueLocation);
    }
    else {
        swal({ title: "", text: 'Please select at least one location.' });
    }
}

function RefreshFamilyMember() {
    var PatientId = $('#PatientId').val();
    $.ajaxSetup({ async: false });
    $.post("/Patients/PartialReleationHistory", { PatientId: PatientId },
    function (data) {
        if (data == 'No record found') {
            $("#familyMemberSection").append("<tr><td style='width:18% !important;text-align:center' colspan='5'></td></tr>");
        }
        else {
            $('#familyMemberSection').html('');
            $('#familyMemberSection').html(data);
        }
    });
    $.ajaxSetup({ async: true });
}

$(function () {
    $('input,textarea').focus(function () {
        $(this).data('placeholder', $(this).attr('placeholder'))
               .attr('placeholder', '');
    }).blur(function () {
        $(this).attr('placeholder', $(this).data('placeholder'));
    });
});

function BackToSelectColleagues() {
    $(".patient_searchbar").show();
    $(".cntnt").show();
    $("#ReferralTitle").html("Select the Colleagues to Send Referral")
    $("#ColleagueLocation").html("");
}

// For remove profile image  - start
function RemoveProfileImage(PatientId) {
    alert(PatientId);
    $.ajax({
        type: "POST",
        url: "/Patients/RemovePationtProfileImage",
        data: "{'PatientId':'" + PatientId + "'}",
        contentType: "application/json; charset=utf-8",
        //success: function (data) {
        //    window.location = document.URL;
        //}
    });
}

function SearchColleagueOnkeyPress() {
    var ctrl = $("#txtSearchPatient");
    clearTimeout(timer);
    var ms = 500; // milliseconds
    timer = setTimeout(function () {
        Onkeypresscolleaguelist(ctrl);
    }, ms);
}
function Onkeypresscolleaguelist(ctrl) {
    var value = $(ctrl).val().toLowerCase().trim();
    if ($.trim(value).length == 0) {
        value = "";
    }
    $('#divLoading').show();
    $.ajaxSetup({ async: false });
    $.post("/Patients/SearchColleagueFromPopUp",
        { SearchText: value }, function (data) {
            document.getElementById("add_doctor_popup").innerHTML = data;
        });
    $.ajaxSetup({ async: true });
    $(".search_listing").bind('scroll', function (event) {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            var data = SearchColleagueOnScroll().toString();
            $(this).append(data);
        }
    });
}
function SearchColleague() {
    $("#ColleaguPageIndex").val("1");
    var GetSearchText = null;
    if ($('#txtSearchPatient').length == 0) {
        GetSearchText = null;
    }
    else {
        GetSearchText = document.getElementById('txtSearchPatient').value;

        if (GetSearchText == "Find colleague to refer to...") {
            GetSearchText = null;
        }
    }

    $.ajaxSetup({ async: false });


    $.post("/Patients/SearchColleagueFromPopUp",
                                { SearchText: GetSearchText }, function (data) {

                                    document.getElementById("add_doctor_popup").innerHTML = data;


                                });


    $.ajaxSetup({ async: true });
    $(".search_listing").bind('scroll', function (event) {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {

            var data = SearchColleagueOnScroll().toString();
            $(this).append(data);
        }
    });
}


function SearchColleagueOnScroll() {

    var datalist = '';
    $.ajaxSetup({ async: false });
    var PageIndex = $("#ColleaguPageIndex").val();
    var SearchText = $("#txtSearchPatient").val();
    if (PageIndex == "-1") {
        return "";
    }
    else {
        PageIndex = parseInt(PageIndex) + parseInt(1);
        $.post("/Patients/GetColleagueListForSendReferralPopUpOnScroll",
           { PageIndex: PageIndex, SearchText: SearchText }, function (data) {
               if (data == '' || data == '<li><center>No more record found.</li>') {
                   $("#EmptyRecord").empty().html("<center>No more records found</center>");
                   $("#ColleaguPageIndex").val("-1");
               }
               else {
                   $("#ColleaguPageIndex").val(PageIndex);
               }
               datalist = data;
           });
    }
    $.ajaxSetup({ async: true });
    return datalist;
}

function CloseColleaguePopUp() {
    $(".mfp-close").click();
}
function ReferralSend() {
    var PatientId = document.getElementById('hdnPatientId').value;
    var MultipleColleague = null;
    var c = new Array();
    c = document.getElementsByTagName('input');

    for (var i = 0; i < c.length; i++) {
        if (c[i].type == 'checkbox' && c[i].id == 'checkcolleague' && c[i].checked == true) {

            if (MultipleColleague == null) {

                MultipleColleague = c[i].value;
            }
            else {

                MultipleColleague = MultipleColleague + ',' + c[i].value;
            }

        }
    }
    if (MultipleColleague == null) {
        swal({ title: "", text: 'Please select atleast one colleague.' });
        //alert("Please Select Atleast One Colleague.")
    }
    else {
        GetLocationColleagues(MultipleColleague, PatientId);
    }
}

function Attachementpopup(RelativePath, Type) {

    var sitename = window.location.host;
    if (sitename.indexOf('http://') == -1 || sitename.indexOf('https://')) {
        sitename = 'http://' + sitename
    }
    var str = RelativePath;
    var res = sitename + str.replace("/mydentalfiles/", "/").replace("~/", "/");
    var res = res.replace("mydentalfiles", "recordlinc");
    res = res.replace("../", "/")

    if (Type != "Image") {
        window.open(res, '_blank');
        window.focus();
    }
    else {
        document.getElementById('Divimagepopup').innerHTML = '<img src="' + res.toString() + '">';
        $("#ImagePopup").click();
    }

}
function SelectPatient(id) {
    $("#search_listing > li").each(function () { $(this).removeClass('active'); });
    if ($("." + id).hasClass('active')) {
        $("." + id).removeClass('active');
    } else {
        $("." + id).addClass('active');
        $("#btnNext").attr("disabled", false);
        $("#hdnSelectedPatientGender").val($(".active").find('#selectedpatient').data('gender'))
    }
}
function AddasFamilyMember() {
    
    var Patientid = $(".active").find('#selectedpatient').data('patientid');

    $("#hdselectedpatient").val(Patientid);
    if (Patientid === undefined || Patientid === 0 || Patientid === '') {
        //swal({ title: "", text: 'Please select any one Patient.' });
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select any one Patient.' });
        $("#btnNext").attr("disabled", true);
        return false;
    }
    $.post("/Patients/PartialFamilyMembers", { PatientId: Patientid }, function (data) {
        $(".search_main_div").hide();
        $(".form-referral").hide();
        $(".list-holder").html(data);
        $(".modal-title").html("Select Relation");
        $("#btnNext").attr('onclick', 'AddFamilyMember()');
    });
    $.ajaxSetup({ async: true });
}
function GoBack() {
    window.location = history.back();
}