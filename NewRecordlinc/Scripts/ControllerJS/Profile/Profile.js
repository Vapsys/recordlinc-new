﻿

/*Issue RM-240 
ToggleSaveButton(id) for disabling save button an prevent increse width of toaster.
*/
function ToggleSaveButton(id) {
    $('#' + id).attr('disabled', true);
    setTimeout(function () {
        $('#' + id).attr('disabled', false);
    }, 4000);
}


$(document).ready(function () {

    var date_input = $('input[name="date"]'); //our date input has the name "date"
    var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
    date_input.datepicker({
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
    })

    $('#id').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#id_text').toggle();
    });

    $('#id_text').click(function (e) {
        e.stopPropagation();
    });

    $('body').click(function () {
        $('#id_text').hide();
        $('#gender_text').hide();
    });

    $('#gender').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#gender_text').toggle();
    });

    $('#gender_text').click(function (e) {
        e.stopPropagation();
    });

    $('body').click(function () {
        $('#gender_text').hide();
    });

    $('#dob').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#dob_text').toggle();
    });

    $('#dob_text').click(function (e) {
        e.stopPropagation();
    });

    $('body').click(function () {
        $('#dob_text').hide();
    });

    $('#address').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#address_text').toggle();
    });

    $('#address_text').click(function (e) {
        e.stopPropagation();
    });
    $('body').click(function () {
        $('#address_text').hide();
    });

    $('#phone').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#phone_text').toggle();
    });

    $('#phone_text').click(function (e) {
        e.stopPropagation();
    });

    $('body').click(function () {
        $('#phone_text').hide();
    });

    $('#email').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#email_text').toggle();
    });

    $('#email_text').click(function (e) {
        e.stopPropagation();
    });

    $('body').click(function () {
        $('#email_text').hide();
    });

    $('#family_mem').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $('#family_mem_text').toggle();
    });

    $('#family_mem_text').click(function (e) {
        e.stopPropagation();
    });

    $('body').click(function () {
        $('#family_mem_text').hide();
    });
    $('a[data-toggle="tooltip"]').tooltip({
        container: 'body'
    });

    /*Change for Adding Active class if Dentist view  profile of a colleague*/
    var iscolleague = $('#IsColleague').val();
    if (iscolleague == 1) {
        setTimeout(function () {
            $(".nav").find("li.idprofile").removeClass("active");
        });
        setTimeout(function () {
            $(".nav").find("li.idcolleague").addClass("active");
        })
    }

    $('.simple_color_color_code').simpleColor({ displayColorCode: true });
});


function show() {
    $("#dThreshold").css('display', 'block');
}

$(".planing").click(function () {
    $('html,body').animate({
        scrollTop: $(".planes").offset().top
    }, 2000);
});

$(".patinet_form").click(function () {
    $('html,body').animate({
        scrollTop: $(".pateint_form_history").offset().top
    }
        , 2000);
});
$(".book_appiontment").click(function () {
    $('html,body').animate({
        scrollTop: $(".book_appiontment_new").offset().top
    }
        , 2000);
});
// For Edit Colleague Address - Start


function editAddress(idstr) {
    performAjax({
        url: "/Profile/NewGetAddressForColleagueByAddressInfoID",
        type: "POST",
        data: { AddressInfoID: idstr },
        async: true,
        success: function (data) {
            $("#addional-address").modal('show');
            $("#addional-address").html(data);
            $('#Phone').mask("(999) 999-9999");
            $('#Mobile').mask("(999) 999-9999");
            LoadScript();
        }
    })

}
//Save address.
function UpdateAddress(idstr) {
    if ($.trim($("#Location").val()).length == 0) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter Location.' });
        setTimeout(function () { $("#Location").focus(); }, 0);
        ToggleSaveButton('btn_address');
        return false;
    }
    else if ($.trim($("#Location").val()).length > 25) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter only 25 characters for Location.' });
        setTimeout(function () { $("#Location").focus(); }, 0);
        ToggleSaveButton('btn_address');
        return false;
    }

    if ($('#Address2').val() != null && $('#Address2').val() != "") {
        if ($('#Address2').val().toString().length > 100) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Suite/Unit allow maximum 100 characters only.' });
            $("#Address2").focus();
            ToggleSaveButton('btn_address');
            return false;
        }
    }

    if ($('#ExactAddress').val() == null || $('#ExactAddress').val() == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter Street Address.' });
        $("#ExactAddress").focus();
        ToggleSaveButton('btn_address');
        return false;
    }
    else {
        if ($('#ExactAddress').val().toString().length > 100) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Street Address allow maximum 100 characters only.' });
            $("#ExactAddress").focus();
            ToggleSaveButton('btn_address');
            return false;
        }
    }

    if ($('#City').val() == null || $('#City').val() == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter City.' });
        $("#City").focus();
        ToggleSaveButton('btn_address');
        return false;
    }
    else {
        if ($('#City').val().toString().length > 100) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'City allow maximum 100 characters only.' });
            $("#City").focus();
            ToggleSaveButton('btn_address');
            return false;
        }
    }

    if ($('#drpCountry').val() == 0) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select Country.' });
        $("#drpCountry").focus();
        ToggleSaveButton('btn_address');
        return false;
    }

    if ($('#drpState').val() == 0) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select State.' });
        $("#drpState").focus();
        ToggleSaveButton('btn_address');
        return false;
    }


    if ($('#Zipcode').val() == null || $('#Zipcode').val() == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter Zip Code.' });
        $("#Zipcode").focus();
        ToggleSaveButton('btn_address');
        return false;
    }
    else {
        var zip = $('#Zipcode').val();
        var ValidateZip = /^([FG]?\d{5}|\d{5}[AB])$/;
        if (!ValidateZip.test(zip)) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter a valid Zip Code.' });
            ToggleSaveButton('btn_address');
            return false;
        }
    }


    if ($('#Email').val() == null || $('#Email').val() == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter Email.' });
        $("#Email").focus();
        ToggleSaveButton('btn_address');
        return false;
    }
    else {
        var result = IsEmail($("#Email").val());
        if (result != true) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter a valid Email Address.' });
            $("#Email").focus();
            ToggleSaveButton('btn_address');
            return false;

        }
        else {
            if ($('#Email').val().toString().length > 100) {
                $.toaster({ priority: 'warning', title: 'Notice', message: 'Email allow maximun 100 characters only.' });
                $("#Email").focus();
                ToggleSaveButton('btn_address');
                return false;
            }
        }
    }

    // Priya : Issue  no : 0005184
    if ($('#Phone').val() != "") {
        var phone = $('#Phone').val();
        var RegExPhone = ValidationPhone(phone);
        if (!RegExPhone) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter valid Phone Number.' });
            ToggleSaveButton('btn_address');
            return false;
        }
    }

    if ($('#Mobile').val() != "") {
        var Mobile = $('#Mobile').val();
        var RegExMoblie = ValidationPhone(Mobile);
        //Updated Date : 07-02-2019 With Jira ID : RM-599
        //if (!RegExPhone) {
        if (!RegExMoblie) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter valid Mobile Number.' });
            ToggleSaveButton('btn_address');
            return false;
        }
    }
    if ($('#Fax').val() != "") {
        var phone = $('#Fax').val();
        var RegExEmail = /^(?:\([2-9]\d{2}\)\ ?|[2-9]\d{2}(?:\-?|\ ?))[2-9]\d{2}[- ]?\d{4}$/;
        if (!RegExEmail.test(phone)) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter valid Fax Number.' });
            ToggleSaveButton('btn_address');
            return false;
        }

    }
    var websiteurl = $("#txtwebsiteurl").val();
    if (websiteurl.trim() != null && websiteurl.trim() != '' && websiteurl.trim() != "") {
        if (!isUrl(websiteurl)) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter a valid Website URL.' });
            $("#txtwebsiteurl").val('');
            ToggleSaveButton('btn_address');
            return false;
        }
    }
    var IsPrimary = $('#chkprimary:checked').val() ? true : false;
    debugger
    var empsid = $('#EPMSId').val();
    if (empsid.trim() != null && empsid.trim() != "") {
        performAjax({
            url: "/Profile/GetEPMSIdbyAccountId",
            type: "POST",
            data: { ExternalPMSId: empsid, LocationId: idstr },
            success: function (data) {
                if (data == true) {
                    //Updated Date : 07-02-2019 With ID : RM-456
                    //$.toaster({ priority: 'warning', title: 'Notice', message: 'Extarnal PMS ID already exists!.' });
                    $.toaster({ priority: 'warning', title: 'Notice', message: 'External PMS ID already exists!.' });
                }
                else {
                    performAjax({
                        url: "/Profile/UpdateAddressForDoctor",
                        type: "POST",
                        //Olivia changes on 10-04-2018 for Dr. Lan Allan's website. - DLADCETK-1
                        data: { AddressInfoID: idstr, ExactAddress: $('#ExactAddress').val(), Address2: $('#Address2').val(), City: $('#City').val(), State: $('#drpState').val(), Country: $('#drpCountry').val(), Zipcode: $('#Zipcode').val(), IsPrimary: IsPrimary, Email: $('#Email').val(), Phone: $('#Phone').val(), Fax: $('#Fax').val(), Website: $("#txtwebsiteurl").val(), Location: $("#Location").val(), TimezoneId: $("#clsAddressDetailsById_TimeZoneId").val(), Mobile: $("#Mobile").val(), ExternalPMSId: $('#EPMSId').val(), SchedulingLink: $('#txtSchedulingLink').val() },
                        success: function (data) {
                            if (data == "1") {

                                if (idstr == 0) {
                                    $.toaster({ priority: 'success', title: 'Notice', message: 'Address Details added successfully.' });
                                    ToggleSaveButton('btn_address');
                                }
                                else {
                                    $.toaster({ priority: 'success', title: 'Notice', message: 'Address Details updated successfully.' });
                                    ToggleSaveButton('btn_address');
                                }
                                setTimeout(function () {
                                    window.location.reload();
                                }, 3000);
                            }
                            else {
                                $.toaster({ priority: 'info', title: 'Notice', message: 'Address Details already exists.' });
                                ToggleSaveButton('btn_address');
                            }
                        }
                    })
                }
            }
        })
    } else {
        performAjax({
            url: "/Profile/UpdateAddressForDoctor",
            type: "POST",
            data: { AddressInfoID: idstr, ExactAddress: $('#ExactAddress').val(), Address2: $('#Address2').val(), City: $('#City').val(), State: $('#drpState').val(), Country: $('#drpCountry').val(), Zipcode: $('#Zipcode').val(), IsPrimary: IsPrimary, Email: $('#Email').val(), Phone: $('#Phone').val(), Fax: $('#Fax').val(), Website: $("#txtwebsiteurl").val(), Location: $("#Location").val(), TimezoneId: $("#clsAddressDetailsById_TimeZoneId").val(), Mobile: $("#Mobile").val(), ExternalPMSId: $('#EPMSId').val(), SchedulingLink: $('#txtSchedulingLink').val()  },
            success: function (data) {
                if (data == "1") {

                    if (idstr == 0) {
                        $.toaster({ priority: 'success', title: 'Notice', message: 'Address Details added successfully.' });
                        ToggleSaveButton('btn_address');
                    }
                    else {
                        $.toaster({ priority: 'success', title: 'Notice', message: 'Address Details updated successfully.' });
                        ToggleSaveButton('btn_address');
                    }
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                }
                else {
                    $.toaster({ priority: 'info', title: 'Notice', message: 'Address Details already exists.' });
                    ToggleSaveButton('btn_address');
                }
            }
        })
    }
}

function OpenDeleteAddress(idstr, Pri) {
    $("#remove_item").modal('show');
    $('#desc_message').text('Are you sure you want to remove?');
    $('#btnyes').attr('onclick', ' RemoveAddress("' + idstr + '","' + Pri + '")');
}


function RemoveAddress(idstr, Pri) {
    if (Pri == 1) {
        $.toaster({ priority: 'info', title: 'Notice', message: 'You can not delete primary address, change other address as primary address to delete this.' });
        return false;
    }
    $("#remove_item").modal('hide');
    performAjax({
        url: "/Profile/RemoveDoctorAddressDetailsByAddressInfoID",
        type: "POST",
        data: { AddressInfoID: idstr },
        success: function (data) {
            if (data == "1") {
                $.toaster({ priority: 'success', title: 'Notice', message: 'Address Details deleted successfully' });
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            }
            else if (data == "0") {
                $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to delete Address Details.' });
            }
            else {
                $.toaster({ priority: 'info', title: 'Notice', message: 'Above team member using this Location, so you can not delete this Address.' });
            }
        }
    })


}

function FillState() {
    var id = $('#drpCountry').val();
    performAjax({
        url: "/Profile/FillState",
        type: "POST",
        async: true,
        data: { cid: id, sid: "" },
        success: function (data) {
            if (data != '') {
                $('#dvstate').html(data);
            }
        }
    })
}

function FillCountry(id) {
    performAjax({
        url: "/Profile/FillCountry",
        type: "POST",
        data: { cid: id },
        async: true,
        success: function (data) {
            if (data != '') {
                $('#dvCountry').html(data);
            }
        }
    })
}



function CheckPublicProfileUrlIsExistsAndUpdate(UserId) {
    var Publicprofileurl = $('#txtpublicprofileurl').val();
    if (Publicprofileurl.trim() == "" || Publicprofileurl.trim() == '' || Publicprofileurl.trim() == null) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter Public Profile URL.' });
        $('#txtpublicprofileurl').val('');
        ToggleSaveButton('btn-save-public-profile');
        return false;
    }

    performAjax({
        url: "/Profile/CheckPublicProfileUrlIsExistsAndUpdate",
        type: "POST",
        data: { UserId: UserId, PublicProfile: Publicprofileurl },
        success: function (data) {
            if (data != null && data != "") {
                if (data == "0") {
                    $.toaster({ priority: 'info', title: 'Notice', message: 'Profile Name is already exists.' });
                    ToggleSaveButton('btn-save-public-profile');
                }

                if (data == "1") {
                    $.toaster({ priority: 'success', title: 'Notice', message: 'Profile Name is updated successfully.' });
                    ToggleSaveButton('btn-save-public-profile');
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                }

                if (data == "2") {
                    $.toaster({ priority: 'success', title: 'Notice', message: 'Profile Name is updated successfully.' });
                    ToggleSaveButton('btn-save-public-profile');
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                }

            }
            else {
                $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to update profile name.' });
                ToggleSaveButton('btn-save-public-profile');
            }
        }

    })

}

// For Edit Public Profile  - Start
function EditPublicProfile(UserId) {
    performAjax({
        url: "/Profile/NewGetPublicProfileDetailByUserId",
        type: "POST",
        async: true,
        data: { UserId: UserId },
        success: function (data) {
            $('#edit-public-profile').modal('show');
            $('#edit-public-profile').html(data);
        }
    })
}

function GetPublicProfileSection(id) {
    performAjax({
        url: "/Profile/NewGetPublicProfileSection",
        type: "POST",
        data: { Userid: id },
        success: function (data) {
            if (data != null && data != "") {
                $('#edit-public-profile-choose-section').modal('show');
                $('#edit-public-profile-choose-section').html(data);
            }
        }
    })
}

function ChangeDentistFeatureStatus(Id) {
    var status;
    if ($('#chk_' + Id).prop("checked") == true) {
        status = 1;
    }
    else {
        status = 0;
    }

    $.ajax({
        url: "/Profile/UpdateFeatureSetting",
        type: "POST",
        data: { 'Id': Id, 'status': status },
        success: function (data) {

        },
    });
}

function GetFeatureSettingSection(id) {
    performAjax({
        url: "/Profile/GetFeatureSettingSection",
        type: "POST",
        data: { Userid: id },
        success: function (data) {
            if (data != null && data != "") {
                $('#edit-public-profile-featuresetting-section').modal('show');
                $('#edit-public-profile-featuresetting-section').html(data);
            }
        }
    })
}

function AddPublicProfileSection(SectionId, UserId) {
    var Obj = {};
    Obj = {
        'SectionId': SectionId,
        'UserId': UserId,
        'PatientForms': ($('#patientform').is(':checked') ? true : false),
        'PatientLogin': ($('#PatientLogin').is(':checked') ? true : false),
        'ReferPatient': ($('#ReferPatient').is(':checked') ? true : false),
        'SpecialOffers': ($('#SpecialOffers').is(':checked') ? true : false),
        'AppointmentBooking': ($('#AppointmentBooking').is(':checked') ? true : false),
        'Reviews': ($('#Reviews').is(':checked') ? true : false),
        'CreatedBy': UserId,
        'ModifiedBy': UserId
    }

    performAjax({
        url: "/Profile/InsertUpdatePublicProfileSection",
        type: "POST",
        data: { Obj: Obj },
        success: function (data) {
            if (data) {
                $.toaster({ priority: 'success', title: 'Notice', message: 'Details updated successfully.' });
                ToggleSaveButton('btn-profile-sections');
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            }
            else {
                $.toaster({ priority: 'danger', title: 'Notice', message: 'Oops!, Something went to wrong' });

            }
        }
    })
}


function Copyclipboard(id) {
    $("#" + id).select();
    document.execCommand('copy');
}

function ClosePopUp() {
    $(".close-circle").click();
}

function EditWebsiteURL(UserId) {
    performAjax({
        url: "/Profile/NewGetWebsiteURLByUserId",
        type: "POST",
        async: true,
        data: { UserId: UserId },
        success: function (data) {
            $("#insert-website").modal('show');
            $("#insert-website").html(data);
        }
    })
}


function WebsiteUrlUpdate(UserId) {
    var websiteurl = $('#txtwebsiteurl').val();
    if (websiteurl == null || websiteurl == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter Website URL.' });
        ToggleSaveButton('btn-website-up');
        return false;
    }

    if (!isUrl(websiteurl)) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter valid Website URL.' });
        ToggleSaveButton('btn-website-up');
        return false;
    }
    performAjax({
        url: "/Profile/WebsiteUrlUpdate",
        type: "POST",
        async: true,
        data: { UserId: UserId, Websiteurl: websiteurl },
        success: function (data) {
            if (data != null && data != "") {
                if (data == "1") {
                    $.toaster({ priority: 'success', title: 'Notice', message: 'Website added successfully' });
                    ToggleSaveButton('btn-website-up');
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                }
            }
            else {
                $.toaster({ priority: 'info', title: 'Notice', message: 'Website already exists!' });
                ToggleSaveButton('btn-website-up');
            }
        }
    })
}

function EditPrimarySecondryWebsiteURL(siteid, userid) {
    performAjax({
        url: "/Profile/GetPrimarySecondryWebsiteURLByUserId",
        type: "POST",
        async: true,
        data: { websiteId: siteid, UserId: userid },
        success: function (data) {
            $("#edit-primary-website-url").modal('show');
            $("#edit-primary-website-url").html(data);
        }
    })
}


//Common method to save both primary and secondary websites
function PrimarySecondaryWebsiteUrlUpdate(WebsiteId, UserId) {
    var Websiteurl = $('#txtwebsiteurl').val();
    if (!isUrl(Websiteurl)) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter a valid URL.' });
        ToggleSaveButton('btn-sec-web-update');
        return false;
    }

    performAjax({
        url: "/Profile/UpdatePrimarySecondryWesiteUrl",
        type: "POST",
        async: true,
        data: { UserId: UserId, Websiteurl: Websiteurl, siteid: WebsiteId },
        success: function (data) {
            if (data != null && data != "") {
                if (data == "1") {
                    $.toaster({ priority: 'success', title: 'Notice', message: 'Website updated successfully.' });
                    ToggleSaveButton('btn-sec-web-update');
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                }
            }
            else {
                $.toaster({ priority: 'info', title: 'Notice', message: 'Website already exists.' });
                ToggleSaveButton('btn-sec-web-update');
            }
        }
    })
}

function OpenRemovePrimaryAndSeconderyWebsite(SiteId, UserId) {
    $("#remove_item").modal('show');
    $('#desc_message').text('Are you sure you want to remove?');
    $('#btnyes').attr('onclick', ' RemovePrimaryAndSeconderyWebsite("' + SiteId + '","' + UserId + '")');
}


//Remove website
function RemovePrimaryAndSeconderyWebsite(SiteId, UserId) {
    $("#remove_item").modal('hide');
    performAjax({
        url: "/Profile/RemovePrimaryAndSecondaryWebsite",
        type: "POST",
        data: { SiteId: SiteId, UserId: UserId },
        success: function (data) {
            if (data == "1") {
                $.toaster({ priority: 'success', title: 'Notice', message: 'Secondary Website removed successfully.' });
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            }
            else {
                $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to remove  Secondary Website.' });
            }
        }
    })


}

//Socail Media Section
function EditSocialMedia(idstr) {
    performAjax({
        url: "/Profile/NewGetSocialMediaDetailByUserId",
        type: "POST",
        async: true,
        data: { UserId: idstr },
        success: function (data) {
            $('#edit-social-media-information').modal('show');
            $('#edit-social-media-information').html(data);
        }
    })
}

function UpdateSocialMediaDetails(idstr) {
    ////var FBRegx = /(http(?:s?):\/\/)?(?:www\.)?facebook\.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*([\w\-]*)$/;
    //var FBRegx = /(?:https?:\/\/)?(?:www\.)?facebook\.com\/.(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*([\w\-\.]*)/;
    //var TwitterRegx = /^(https?:\/\/)?((w{3}\.)?)twitter\.com\/(#!\/)?[a-z0-9_]+$/;
    //var GplusRegx = /^https?:\/\/plus\.google\.com\S*$/;
    //var LinkedInRegx = /((https?:\/\/)?((www|\w\w)\.)?linkedin\.com\/)((([\w]{2,3})?)|([^\/]+\/(([\w|\d-&#?=])+\/?){1,}))$/;
    //var PinterestRegx = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.)?pinterest.com\/\S*$/;
    ////var YoutubeRegx = /http(?:s?):\/\/(?:www\.)?youtu(?:be\.com\/watch\?v=|\.be\/)([\w\-\_]*)(&(amp;)???[\w\???=]*)?/;
    //var YoutubeRegx = /(http:|https:)?\/\/(www\.)?(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/;
    //var BlogRegx = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
    //var YelpRegx = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.)?yelp.com\/\S*$/;
    var FBRegx = new RegExp('/(http(?:s?):\/\/)?(?:www\.)?facebook\.com\/(?:(?:\w)*#!\/)?(?:pages\/)?(?:[\w\-]*\/)*([\w\-]*)$/');
    var TwitterRegx = /^(https?:\/\/)?((w{3}\.)?)twitter\.com\/(#!\/)?[a-z0-9_]+$/;
    var GplusRegx = /^https?:\/\/plus\.google\.com\S*$/;
    var LinkedInRegx = /((https?:\/\/)?((www|\w\w)\.)?linkedin\.com\/)((([\w]{2,3})?)|([^\/]+\/(([\w|\d-&#?=])+\/?){1,}))$/;
    var PinterestRegx = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.)?pinterest.com\/\S*$/;
    var YoutubeRegx = /(http:|https:)?\/\/(www\.)?(youtube.com|youtu.be)\/(watch)?(\?v=)?(\S+)?/;
    var BlogRegx = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
    var YelpRegx = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.)?yelp.com\/\S*$/;
    var InstagramRegx = /^https?:\/\/(www\.)?instagram\.com\/([A-Za-z0-9_](?:(?:[A-Za-z0-9_]|(?:\.(?!\.))){0,28}(?:[A-Za-z0-9_]))?)/;

    if ($('#FacebookUrl').val() != null && $('#FacebookUrl').val() != "") {
        if (!FBRegx.test($('#FacebookUrl').val().toLowerCase())) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter a valid Facebook URL.' });
            $('#FacebookUrl').focus();
            ToggleSaveButton('btn-save-social-media');
            return false;
        }
    }

    if ($('#TwitterUrl').val() != null && $('#TwitterUrl').val() != "") {
        if (!TwitterRegx.test($('#TwitterUrl').val().toLowerCase())) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter a valid Twitter URL.' });
            $('#TwitterUrl').focus();
            ToggleSaveButton('btn-save-social-media');
            return false;
        }
    }

    //RM-355

    if ($('#InstagramUrl').val() != null && $('#InstagramUrl').val() != "") {
        if (!InstagramRegx.test($('#InstagramUrl').val().toLowerCase())) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter a valid Instagram URL.' });
            $('#InstagramUrl').focus();
            ToggleSaveButton('btn-save-social-media');
            return false;
        }
    }

    if ($('#LinkedinUrl').val() != null && $('#LinkedinUrl').val() != "") {
        if (!LinkedInRegx.test($('#LinkedinUrl').val().toLowerCase())) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter a valid Linkedin URL.' });
            $('#LinkedinUrl').focus();
            ToggleSaveButton('btn-save-social-media');
            return false;
        }
    }


    if ($('#GooglePlusUrl1').val() != null && $('#GooglePlusUrl1').val() != "") {
        if (!GplusRegx.test($('#GooglePlusUrl1').val().toLowerCase())) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter a valid GooglePlus URL.' });
            $('#GooglePlusUrl1').focus();
            ToggleSaveButton('btn-save-social-media');
            return false;
        }
    }

    if ($('#PinterestUrl').val() != null && $('#PinterestUrl').val() != "") {
        if (!PinterestRegx.test($('#PinterestUrl').val().toLowerCase())) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter a valid Pinterest URL.' });
            $('#PinterestUrl').focus();
            ToggleSaveButton('btn-save-social-media');
            return false;
        }
    }

    if ($('#YouTubeUrl12').val() != null && $('#YouTubeUrl12').val() != "") {
        if (!YoutubeRegx.test($('#YouTubeUrl12').val().toLowerCase())) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter a valid YouTube URL.' });
            $('#YouTubeUrl12').focus();
            ToggleSaveButton('btn-save-social-media');
            return false;
        }
    }

    if ($('#BlogUrl').val() != null && $('#BlogUrl').val() != "") {
        if (!BlogRegx.test($('#BlogUrl').val().toLowerCase())) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter a valid Blog URL.' });
            $('#BlogUrl').focus();
            ToggleSaveButton('btn-save-social-media');
            return false;
        }
    }

    if ($('#YelpUrl').val() != null && $('#YelpUrl').val() != "") {
        if (!YelpRegx.test($('#YelpUrl').val().toLowerCase())) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter a valid Yelp URL.' });
            $('#YelpUrl').focus();
            ToggleSaveButton('btn-save-social-media');
            return false;
        }
    }

    //RM-388: Validation for Empty socail media details.
    var x = 0;
    $('#social_media input[type=text]').each(function () {
        var textbox = $(this).val();
        if (textbox == null || textbox == '') {
            x++;
        }
    });
    if (x == 9) {
        $.toaster({ priority: 'danger', title: 'Notice', message: 'Please enter atleast one Social Media Detail.' });
        ToggleSaveButton("btn-save-social-media");
        return false;
    }


    performAjax({
        url: "/Profile/UpdateSocialMediaForDoctor",
        type: "POST",
        data: { UserId: idstr, FacebookUrl: $('#FacebookUrl').val(), LinkedinUrl: $('#LinkedinUrl').val(), TwitterUrl: $('#TwitterUrl').val(), InstagramUrl: $('#InstagramUrl').val(), GoogleplusUrl: $('#GooglePlusUrl1').val(), YoutubeUrl: $('#YouTubeUrl12').val(), PinterestUrl: $('#PinterestUrl').val(), BlogUrl: $('#BlogUrl').val(), YelpUrl: $("#YelpUrl").val() },
        success: function (data) {
            if (data == "1") {
                $.toaster({ priority: 'success', title: 'Notice', message: 'Social Media Details updated successfully.' });
                ToggleSaveButton('btn-save-social-media');
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
            else {
                $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to update Social Media Details.' });
                ToggleSaveButton('btn-save-social-media');
            }
        }
    })
}


function EditDescription(idstr) {
    performAjax({
        url: "/Profile/NewGetDescriptionByUserId",
        type: "POST",
        async: true,
        data: { UserId: idstr },
        success: function (data) {
            $('#edit-description').modal('show');
            $('#edit-description').html(data);
            $("#txtdescriptionlength").val(parseInt($("#txtdescriptionlength").val()) - $("#txtdescription").val().length);
        }
    })
}

function UpdateDescription(idstr) {
    var dis = $('#txtdescription').val();
    if (dis != null && dis != "") {
        performAjax({
            url: "/Profile/UpdateDescriptionOfDoctor",
            type: "POST",
            data: { UserId: idstr, Description: dis },
            success: function (data) {
                if (data == "1") {
                    $.toaster({ priority: 'success', title: 'Notice', message: 'Description updated successfully' });
                    ToggleSaveButton('btn-description');
                    setTimeout(function () {
                        window.location.reload();
                    }, 2000);
                }
                else {
                    $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to Description.' });
                    ToggleSaveButton('btn-description');
                }
            }
        })
    }
    else {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter Description.' });
        return false;
    }
}

//Edication and training.
function EditEducationandTrainingDetails(idstr, UserId) {
    performAjax({
        url: "/Profile/NewGetEducationandTrainingDetailsById",
        type: "POST",
        async: true,
        data: { Id: idstr, UserId: UserId },
        success: function (data) {
            $('#add-education-training-info').modal('show');
            $('#add-education-training-info').html(data);
        }
    })
}


function EduandTraining(UserId) {

    //if ($('#Institute').val() != null && $('#Institute').val() != "") {
    //    if ($('#Institute').val().toString().length > 100) {
    //        $.toaster({ priority: 'warning', title: 'Notice', message: 'Institute allow maximum 100 characters only.' });
    //        $("#Institute").focus();
    //        ToggleSaveButton('btn-saveEducation');
    //        return false;
    //    }
    //}
    if ($('#Institute').val() == null || $('#Institute').val() == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select Institute.' });
        $("#Institute").focus();
        ToggleSaveButton('btn-saveEducation');
        return false;
    }else if ($('#hdnInstitute').val() == null || $('#hdnInstitute').val() == "") {       
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select valid Institute.' });
            $("#Institute").focus();
            ToggleSaveButton('btn-saveEducation');
            return false;       
    }
    else {
        ToggleSaveButton('btn-saveEducation');
    }
    if ($('#Specialisation').val() != null && $('#Specialisation').val() != "") {
        if ($('#Specialisation').val().toString().length > 100) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Specialisation allow maximum 100 characters only.' });
            $("#Specialisation").focus();
            ToggleSaveButton('btn-saveEducation');
            return false;
        }
    }
    else {
        ToggleSaveButton('btn-saveEducation');
    }

    var Institutevalue = $('#Institute').val();
    var Specialisationvalue = $('#Specialisation').val();
    var YearAttendedvalue = $('#YearAttended').val();;
    var strEducationID = $('#EducationID').val();

    if (Institutevalue != null && Institutevalue != "") {

        performAjax({
            url: "/Profile/EducationAndTrainingInsertAndUpdate",
            type: "POST",
            data: { Id: strEducationID, UserId: UserId, Institute: Institutevalue, Specialisation: Specialisationvalue, YearAttended: YearAttendedvalue },
            success: function (data) {
                if (data == "1") {
                    if (strEducationID != 0) {
                        $.toaster({ priority: 'success', title: 'Notice', message: 'Education and Training updated successfully' });
                        ToggleSaveButton('btn-saveEducation');
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                    else {
                        $.toaster({ priority: 'success', title: 'Notice', message: 'Education and Training added successfully.' });
                        ToggleSaveButton('btn-saveEducation');
                        setTimeout(function () {
                            window.location.reload();
                        }, 2000);
                    }
                }
                else {

                    if (strEducationID != 0) {
                        $.toaster({ priority: 'info', title: 'Notice', message: 'Institution and Field of Study already exists in Education and Training.' });
                        ToggleSaveButton('btn-saveEducation');
                    }
                    else {
                        $.toaster({ priority: 'info', title: 'Notice', message: 'Institution and Field of Study already exists in education and training.' });
                        ToggleSaveButton('btn-saveEducation');
                    }
                }
            }
        })
    }
    else {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter Institution Attended.' });
        $("#Institute").focus();
        ToggleSaveButton('btn-saveEducation');
        //return false;
    }
}


function OpenRemoveEducation(Id) {
    $("#remove_item").modal('show');
    $('#desc_message').text('Are you sure you want to remove?');
    $('#btnyes').attr('onclick', ' RemoveEduandTraining("' + Id + '")');

}

function RemoveEduandTraining(Id) {
    $("#remove_item").modal('hide');
    performAjax({
        url: "/Profile/RemoveEducationAndTraining",
        type: "POST",
        data: { Id: Id },
        success: function (data) {
            if (data == "1") {
                $.toaster({ priority: 'success', title: 'Notice', message: 'Education and Training removed successfully.' });
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            }
            else {
                $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to remove  Education and Training' });
            }
        }
    })

}

// For Edit Professional Membership  - Start
function EditProfessionalMembershipsFromProfilePage(idstr, UserId) {
    performAjax({
        url: "/Profile/NewGetProfessionalMembershipsById",
        type: "POST",
        async: true,
        data: { Id: idstr, UserId: UserId },
        success: function (data) {
            $('#add-professional-memberships').modal('show');
            $('#add-professional-memberships').html(data);
            var Editprofessional = $("#txtEditprofessional").val.length;
            var count = 272;
            count = count - Editprofessional;
            $("#txtEditprofessionalcount").val(count);
        }
    })
}

function EditOnKeyupProfessionalMemberships(e) {
    var Editprofessional = $("#txtEditprofessional").val();
    var count = 272;
    count = count - Editprofessional.length;
    $("#txtEditprofessionalcount").val(count);
    var Editkeycode;
    if (window.event) Editkeycode = window.event.keyCode;
    else if (e) Editkeycode = e.which;
    if (Editprofessional.length > 0) {

        performAjax({
            type: "POST",
            async: true,
            url: "/Profile/SearchProfessionalMemberships",
            data: { Memberships: Editprofessional },
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var datasource = eval(data)
                if (datasource) {
                    $("#txtEditprofessional").autocomplete({
                        sortResults: false,
                        scroll: false,
                        minLength: 0,
                        source: datasource,
                        focus: function (event, ui) {
                            var selectedObj = ui.item;
                            event.preventDefault();
                            return true;
                        },
                        select: function (event, ui) {
                            var label = ui.item.label;
                            var value = ui.item.value;
                            document.valueSelectedForAutocomplete = value
                        }
                    });
                }
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $.toaster({ priority: 'danger', title: 'Notice', message: 'failed.' });
            }
        });
    }
}

function AddOnKeyupProfessionalMemberships(e) {
    var Addprofessional = $("#txtAddprofessional").val();
    var count = 272;
    var max = 272;
    count = count - Addprofessional.length;
    $("#txtAddprofessionalcount").val(count);
    var Addkeycode;
    if (window.event) Addkeycode = window.event.keyCode;
    else if (e) Addkeycode = e.which;
    if (Addprofessional.length > 0) {
        performAjax({
            type: "POST",
            url: "/Profile/SearchProfessionalMemberships",
            data: { Memberships: Addprofessional },
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                var datasource = eval(data)
                if (datasource) {
                    $("#txtAddprofessional").autocomplete({
                        sortResults: false,
                        scroll: true,
                        minLength: 0,
                        source: datasource,
                        focus: function (event, ui) {
                            var selectedObj = ui.item;
                            event.preventDefault();
                            return true;
                        },
                        select: function (event, ui) {
                            var label = ui.item.label;
                            var value = ui.item.value;
                            document.valueSelectedForAutocomplete = value
                        }
                    });
                }

            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $.toaster({ priority: 'danger', title: 'Notice', message: 'failed.' });
            }
        });
    }
}

function EditProfessionalMemberships(Id, UserId) {
    var Membershipvalue = null;
    if (Id != 0) {
        Membershipvalue = $('#txtEditprofessional').val();
    }
    else {

        Membershipvalue = $('#txtAddprofessional').val();
    }
    if (Membershipvalue != null && Membershipvalue != "") {
        performAjax({
            url: "/Profile/ProfessionalMembershipsInsertAndUpdate",
            type: "POST",
            async: true,
            data: { Id: Id, UserId: UserId, Membership: Membershipvalue },
            success: function (data) {
                if (data == "1") {
                    if (Id != 0) {
                        $.toaster({ priority: 'success', title: 'Notice', message: 'Professional Membership updated successfully.' });
                        ToggleSaveButton('btn-pro-member');
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    }
                    else {
                        $.toaster({ priority: 'success', title: 'Notice', message: 'Professional Membership added successfully.' });
                        ToggleSaveButton('btn-pro-member');
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                    }
                }
                else {

                    if (Id != 0) {
                        $.toaster({ priority: 'info', title: 'Notice', message: 'Professional Membership already exists.' });
                        ToggleSaveButton('btn-pro-member');
                    }
                    else {
                        $.toaster({ priority: 'info', title: 'Notice', message: 'Professional Membership already exists.' });
                        ToggleSaveButton('btn-pro-member');
                    }
                }
            }
        })
    }
    else {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter Professional Membership.' });
        ToggleSaveButton('btn-pro-member');
        return false;
    }
}


function OpenRemoveMembership(Id) {
    $("#remove_item").modal('show');
    $('#desc_message').text('Are you sure you want to remove?');
    $('#btnyes').attr('onclick', ' RemoveProfessionalMemberships("' + Id + '")');

}


function RemoveProfessionalMemberships(Id) {
    $("#remove_item").modal('hide');
    performAjax({
        url: "/Profile/RemoveProfessionalMemberships",
        type: "POST",
        data: { Id: Id },
        success: function (data) {
            if (data == "1") {
                $.toaster({ priority: 'success', title: 'Notice', message: 'Professional Membership removed successfully.' });
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            }
            else {
                $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to remove Professional Membership.' });
            }
        }
    })


}

//Team member fucntions.
function EditTeamMemberDetailsById(UserId, TeamMemberId) {
    performAjax({
        url: "/Profile/NewGetTeamMemberDetailsById",
        type: "POST",
        async: true,
        data: { UserId: UserId, TeamMemberId: TeamMemberId },
        success: function (data) {
            if (TeamMemberId != 0) {
                $('#add-team-members').modal('show');
                $('#add-team-members').html(data);
            }
            else {
                $('#add-team-members').modal('show');
                $('#add-team-members').html(data);
            }
            jcf.replaceAll();
        }
    })
}

//For Edit Team member start
function EditTeamMember(UserId, ParentUserId) {
    var EditFirstName = $('#txtEditFirstName').val();
    var EditLastName = $('#txtEditLastName').val();
    var EditEmail = $('#txtEditEmail').val();
    var EditstrSpecialities = $('#drpSpeciality').val();
    var EditAddress2, Editstate, Editcity, Editcountry, Editzip = null;
    var LocationId = $("#LocationId").val();
    var ProvType = $('#chkProviderStaff').is(":checked");
    var Provider = $('#txtProviderId').val();
    if (EditFirstName != null && EditFirstName != "") {
        if (EditFirstName.trim().length == 0) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter a valid First Name.' });
            ToggleSaveButton('btn-edit-member');
            $('#txtEditFirstName').focus();
            return false;
        }
        else {

            var EditFirstNameCheck = IsCharactersOnly(EditFirstName);
            if (EditFirstNameCheck == false) {
                $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter a valid First Name.' });
                ToggleSaveButton('btn-edit-member');
                $('#txtEditFirstName').focus();
                return false;
            }
            else {
                if (EditFirstName.length > 30) {
                    $.toaster({ priority: 'warning', title: 'Notice', message: 'First Name allow maximum 30 characters only.' });
                    $('#txtEditFirstName').focus();
                    ToggleSaveButton('btn-edit-member');
                    return false;
                }
            }
        }
    }

    else {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter First Name.' });
        $('#txtEditFirstName').focus();
        ToggleSaveButton('btn-edit-member');
        return false;
    }


    if (EditLastName != null && EditLastName != "") {
        if (EditLastName.trim().length == 0) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter a valid Last Name.' });
            $('#txtEditLastName').focus();
            ToggleSaveButton('btn-edit-member');
            return false;
        }
        else {
            var EditLastNameCheck = IsCharactersOnly(EditLastName);
            if (EditLastNameCheck == false) {
                $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter a valid Last Name.' });
                $('#txtEditLastName').focus();
                ToggleSaveButton('btn-edit-member');
                return false;
            }
            else {
                if (EditLastName.length > 30) {
                    $.toaster({ priority: 'warning', title: 'Notice', message: 'Last Name allow maximum 30 characters only.' });
                    $('#txtEditLastName').focus();
                    ToggleSaveButton('btn-edit-member');
                    return false;
                }
            }
        }
    }
    else {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter Last Name.' });
        $('#txtEditLastName').focus();
        ToggleSaveButton('btn-edit-member');
        return false;
    }



    if (EditEmail != null && EditEmail != "") {

        var EditresultEmail = IsEmail(EditEmail);
        if (EditresultEmail != true) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter a valid Email Address.' });
            $('#txtEditEmail').focus();
            ToggleSaveButton('btn-edit-member');
            return false;
        }
    }
    else {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter Email.' });
        $('#txtEditEmail').focus();
        ToggleSaveButton('btn-edit-member');
        return false;
    }

    if (EditstrSpecialities == "0") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select Speciality.' });
        $('#drpSpeciality').focus();
        ToggleSaveButton('btn-edit-member');
        return false;
    }

    var OldEmail = $('#hdnEmail').val();
    if (OldEmail == EditEmail) {
        performAjax({
            url: "/Profile/TeamMemberInsertAndUpdate",
            type: "POST",
            async: true,
            data: { UserId: UserId, ParentUserId: ParentUserId, Email: EditEmail, FirstName: EditFirstName, LastName: EditLastName, OfficeName: $('#txtAddOfficeName').val(), ExactAddress: $('#txtEditExactAddress').val(), Address2: EditAddress2, City: Editcity, State: Editstate, Country: Editcountry, Zipcode: Editzip, Specialty: EditstrSpecialities, LocationId: LocationId, ProviderId: Provider, provtype: ProvType },
            success: function (data) {
                if (data == "1") {
                    if (ProvType == false) {
                        $.toaster({ priority: 'success', title: 'Notice', message: 'Team Member updated successfully.' });
                    } else {
                        $.toaster({ priority: 'success', title: 'Notice', message: 'Staff Member updated successfully.' });
                    }
                    ToggleSaveButton('btn-edit-member');
                    setTimeout(function () {
                        window.location.reload();
                    }, 3000);
                }
                else {
                    if (ProvType == false) {
                        $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to edit Team Member.' });
                    } else {
                        $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to edit staff Member.' });
                    }
                }
            }
        })
    }
    else {
        performAjax({
            url: "/Profile/CheckEmailExistsAsDoctor",
            type: "POST",
            async: true,
            data: { Email: EditEmail },
            success: function (data) {
                if (data != null && data != "") {
                    performAjax({
                        url: "/Profile/TeamMemberInsertAndUpdate",
                        type: "POST",
                        data: { UserId: UserId, ParentUserId: ParentUserId, Email: EditEmail, FirstName: EditFirstName, LastName: EditLastName, OfficeName: $('#txtAddOfficeName').val(), ExactAddress: $('#txtEditExactAddress').val(), Address2: EditAddress2, City: Editcity, State: Editstate, Country: Editcountry, Zipcode: Editzip, Specialty: EditstrSpecialities, LocationId: LocationId, ProviderId: Provider, provtype: ProvType },
                        success: function (data) {

                            if (data == "1") {
                             
                                if (ProvType == false) {                                
                                    $.toaster({ priority: 'success', title: 'Notice', message: 'Team Member added successfully' });
                                } else {
                                    $.toaster({ priority: 'success', title: 'Notice', message: 'Staff Member added successfully' });
                                }
                                ToggleSaveButton('btn-edit-member');
                                setTimeout(function () {
                                    window.location.reload();
                                }, 3000);
                            }
                            else {
                                if (ProvType == false) {
                                    $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to edit Team Member.' });
                                } else {
                                    $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to edit Staff Member.' });
                                }
                            }
                        }
                    });

                }
                else {
                    $.toaster({ priority: 'info', title: 'Notice', message: 'This Email Address is already in use.' });
                }
            }
        })
    }
}

function OpenRemoveTeamMember(UserId, ParentUserId) {
    $("#remove_item").modal('show');
    $('#desc_message').text('Are you sure you want to remove?');
    $('#btnyes').attr('onclick', ' RemoveTeamMember("' + UserId + '","' + ParentUserId + '")');
}

// For Remove Team Member - Start
function RemoveTeamMember(UserId, ParentUserId) {
    $("#remove_item").modal('hide');
    performAjax({
        url: "/Profile/RemoveTeamMemberOfDoctor",
        type: "POST",
        data: { UserId: UserId, ParentUserId: ParentUserId },
        success: function (data) {
            if (data == "1") {
                $.toaster({ priority: 'success', title: 'Notice', message: 'Member removed successfully.' });
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            }
            else {
                $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to remove Member.' });
            }
        }

    })
}
// For Remove Team Member - End

function OpenRemoveBanner(BannerId) {
    $("#remove_item").modal('show');
    $('#desc_message').text('Are you sure you want to remove the image?');
    $('#btnyes').attr('onclick', ' RemoveBanner("' + BannerId + '")');
}


//Banner Methods
function RemoveBanner(BannerId) {
    $("#remove_item").modal('hide');
    performAjax({
        type: "POST",
        url: "/Profile/RemoveBanner",
        data: { BannerId: BannerId },
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $.toaster({ priority: 'success', title: 'Notice', message: 'Banner removed successfully.' });
            setTimeout(function () {
                window.location.reload();
            }, 3000);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to remove Image.' });
        }
    });
}


function LicenseDetailsSave() {
    var LicenseNumber = $('#LicenseNumber').val();
    if (LicenseNumber == null || LicenseNumber == 'undefined' || LicenseNumber == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter License Number.' });
        $("#LicenseNumber").focus();
        ToggleSaveButton('btn-license');
        return false;
    }

    if ($('#LicenseNumber').val() != null && $('#LicenseNumber').val() != 'undefined' && $('#LicenseNumber').val() != "") {

        var LicenseNumber = $('#LicenseNumber').val();
        var RegExLicenseNumber = /^[A-Za-z0-9 _]*[A-Za-z0-9][A-Za-z0-9 _]*$/;
        //var RegExLicenseNumber = /^\d+$/;
        if (!RegExLicenseNumber.test(LicenseNumber)) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter valid Lincense Number.' });
            ToggleSaveButton('btn-license');
            return false;
        }
    }

    var LicenseState = $('#drpState :selected').val();
    if (LicenseState == 0) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select License State.' });
        ToggleSaveButton('btn-license');
        return false;
    }


    if ($('#LicenseExpiration').val() == null && $('#LicenseExpiration').val() == "undefined" && $('#LicenseExpiration').val() == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter License Expiration Date.' });
        $("#LicenseExpiration").focus();
        ToggleSaveButton('btn-license');
        return false;
    }
    else {
        var expireDate = new Date($("#LicenseExpiration").val());
        var todayDate = new Date();
        if (todayDate > expireDate) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select future License Expiration Date.' });
            $("#LicenseExpiration").focus();
            $("#LicenseExpiration").val('');
            ToggleSaveButton('btn-license');
            return false;
        }
    }
    if (CheckFirstAndLastName($('#LicenseExpiration').val())) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select License Expiration Date.' });
        ToggleSaveButton('btn-license');
        return false;
    }
    var LicenseNumber = $('#LicenseNumber').val();
    var LicenseState = $('#drpState :selected').val();
    var LicenseExpiration = $('#LicenseExpiration').val();
    if ($('#drpState :selected').val() == 0) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select License State.' });
        return false;
    }
    if (LicenseNumber != null && LicenseNumber != "" && LicenseExpiration != null && LicenseExpiration != "") {
        performAjax({
            url: "/Profile/InsertUpdateLicenseDetails",
            type: "POST",
            async: true,
            data: { LicenseNumber: LicenseNumber, LicenseState: LicenseState, LicenseExpiration: LicenseExpiration },
            success: function (data) {
                if (data == "1") {
                    if (data != 0) {
                        if ($('#add-license').val() != 1) {
                            $.toaster({ priority: 'success', title: 'Notice', message: 'License Details updated successfully.' });
                            ToggleSaveButton('btn-license');
                            setTimeout(function () {
                                window.location.reload();
                            }, 2000);
                            return false;
                        }
                        else {
                            $.toaster({ priority: 'success', title: 'Notice', message: 'License Details added successfully.' });
                            ToggleSaveButton('btn-license');
                            setTimeout(function () {
                                window.location.reload();
                            }, 3000);
                            return false;
                        }

                    }
                    else {
                        $.toaster({ priority: 'success', title: 'Notice', message: 'License Details added successfully.' });
                        ToggleSaveButton('btn-license');
                        setTimeout(function () {
                            window.location.reload();
                        }, 3000);
                        return false;
                    }
                }
                else {

                    if (data != 0) {
                        $.toaster({ priority: 'danger', title: 'Notice', message: 'There is some issue while updating License Detail, please try again.' });
                    }
                    else {
                        $.toaster({ priority: 'danger', title: 'Notice', message: 'There is some issue while adding License Detail, please try again.' });
                    }
                }
                window.location.href = document.URL.replace("#", "");
            }

        })

    }
    else {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter License Number.' });
        return false;
    }
}

function EditLicenseDetailsById(idstr, UserId) {
    performAjax({
        url: "/Profile/NewEditLicenseDetails",
        type: "POST",
        async: true,
        data: { Id: idstr, UserId: UserId },
        success: function (data) {
            $('#edit-license-details').modal('show');
            $('#edit-license-details').html(data);
        }
    })
}

//Method for insurance.
function AddInsurance() {
    performAjax({
        url: "/Profile/NewGetInsuranceDetail",
        type: "POST",
        async: true,
        success: function (data) {
            $("#edit-insurance-details").modal('show');
            $("#edit-insurance-details").html(data);
        }
    })
}

function CheckInsurance() {
    if ($("#relationlist input:checkbox:checked").length <= 0) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Select at least one Insurance.' });
        ToggleSaveButton('btn-insurance-save');
        return false;
    }
    var insurance = {};
    var inuranceid = [];
    $("#relationlist input:checkbox:checked").each(function () {
        //alert($(this).attr('id'));

    })
    if ($("div.relation-list-holder > ul.relation-list input:checkbox:checked").length > 0) {
        $.toaster({ priority: 'success', title: 'Notice', message: 'Insurance detail added successfully.' });
        $("#bannerform").submit();
        ToggleSaveButton('btn-insurance-save');
        setTimeout(function () {
            window.location.reload();
        }, 4000);
        return true;
    }

}

function OpenRemoveInsurance(InsuranceId) {
    $("#remove_item").modal('show');
    $('#desc_message').text('Are you sure you want to remove?');
    $('#btnyes').attr('onclick', ' RemoveInsurance("' + InsuranceId + '")');

}


function RemoveInsurance(InsuranceId) {
    $("#remove_item").modal('hide');
    performAjax({
        type: "POST",
        url: "/Profile/RemoveInsuranceMember",
        data: { Id: InsuranceId },
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $.toaster({ priority: 'success', title: 'Notice', message: 'Insurance removed successfully.' });
            setTimeout(function () {
                window.location.reload();
            }, 3000);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to remove Insurance.' });
        }
    });
}

function AddProcedure() {
    performAjax({
        url: "/Profile/NewGetProcedureList",
        type: "Post",
        success: function (data) {
            $("#add-procedure-detail").modal('show');
            $("#add-procedure-detail").html(data);
            var main = $("#maincheck").val();
            if (main == 'true') {
                $("#checkall").prop("checked", true);
                $(".all-select span").removeClass('jcf-unchecked').addClass('jcf-checked');
            }
            LoadScript();
        }
    })
}

function saveprocedurelist() {
    if ($(".trclone input:checkbox:checked").length <= 0) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Select at least one Procedure.' });
        ToggleSaveButton('btnSave');
        return false;
    }
    var ProcedureList = [];
    $(".trclone").each(function () {
        var proc = {};
        proc = {
            'ProcedureId': $(this).find("#ProcedureId").val(),
            'ProcedureName': $(this).find("#procedurename").val(),
            'CostPercentage': $(this).find("#costpercentage").val(),
            'StandardFees': $(this).find("#standardfees").val(),
            'Ischeck': $(this).find("#chkselect").is(':checked') ? true : false,
        }
        ProcedureList.push(proc);
    });
    performAjax({
        type: "POST",
        async: true,
        url: "/Profile/UpdateProcedureList",
        data: { Proc: ProcedureList },
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $.toaster({ priority: 'success', title: 'Notice', message: 'Procedure updated successfully.' });
            ToggleSaveButton('btnSave');
            setTimeout(function () {
                window.location = document.URL;
            }, 2000);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to update Procedure.' });
            ToggleSaveButton('btnSave');
        }
    })
}

function OpenRemoveProcedure(id) {
    $("#remove_item").modal('show');
    $('#desc_message').text('Are you sure you want to remove?');
    $('#btnyes').attr('onclick', ' RemoveProcedure("' + id + '")');
}


function RemoveProcedure(id) {
    $("#remove_item").modal('hide');
    performAjax({
        type: "POST",
        url: "/Profile/DeleteProcedure",
        data: { ProcedureId: id },
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $.toaster({ priority: 'success', title: 'Notice', message: 'Procedure removed successfully' });
            setTimeout(function () {
                window.location = document.URL;
            }, 2000);
        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to remove Procedure.' });
        }
    });


}


function AddGalleryItems(GallryId, UserId) {
    UserId = (UserId) ? UserId : 0;
    performAjax({
        url: "/Profile/NewAddEditGalleryItem",
        type: "POST",
        data: { GallaryId: GallryId, UserId: UserId },
        success: function (data) {
            $("#add-image").modal('show');
            $("#add-image").html(data);
        }
    })
}

function SaveGalleryDetails() {
    var VideoURL = $.trim($("#VideoURL").val());
    var filesList = $("#fileImageForGallery").prop("files");

    if (VideoURL.length > 0 && filesList.length > 0) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Select either image or video URL' });
        ToggleSaveButton('btn-save-gallery');
        return false;
    }

    if (filesList.length == 0 && VideoURL.length == 0) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Upload either gallery image OR enter video URL' });
        setTimeout(function () { $("#fileImageForGallery").focus() }, 0);
        ToggleSaveButton('btn-save-gallery');
        return false;

    }

    if (VideoURL.length > 0 && !isValidURL(VideoURL)) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Enter valid video URL' });
        setTimeout(function () { $("#VideoURL").focus() }, 0);
        ToggleSaveButton('btn-save-gallery');
        return false;

    }

    if (VideoURL.length > 0 && isValidURL(VideoURL)) {
        var EmbededURL = getEmbededURL(VideoURL);
        if (EmbededURL == 'error') {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Enter valid youtube URL' });
            ToggleSaveButton('btn-save-gallery');
        }
        else {
            $("#VideoURL").val("//www.youtube.com/embed/" + EmbededURL);
        }

    }
    $("#bannerform").submit();
    return true;
}

function EditDoctorDetialsById(idstr) {
    performAjax({
        url: "/Profile/NewGetDoctorDetialsById",
        type: "POST",
        async: true,
        data: { UserId: idstr },
        success: function (data) {
            $("#personal_information").modal('show');
            $("#personal_information").html(data);
            $("#drpSpeciality").select2({
                multiple: true,
                placeholder: "Please select a Speciality"
            });
            $("#ddlSalutation").val($("#hdSalutation").val());
        }
    })
}


function UpdateDoctorPersonalInformation(idstr) {
    var saluation = $('#ddlSalutation').val();
    if (saluation == null || saluation == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter Salutation.' });
        ToggleSaveButton('btn-update-info');
        return false;
    }



    var FirstName = $.trim($('#txtPersonalFirstName').val());
    if (FirstName != null && FirstName != "") {
        var FirstNameCheck = IsCharactersOnly(FirstName);
        if (FirstNameCheck == false) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'First name allow characters only.' });
            ToggleSaveButton('btn-update-info');
            return false;
        }
        else {
            if (FirstName.length > 30) {
                $.toaster({ priority: 'warning', title: 'Notice', message: 'First name allow maximum 30 characters only.' });
                ToggleSaveButton('btn-update-info');
                return false;
            }
        }
    } else {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter First Name.' });
        $('#txtPersonalFirstName').focus();
        ToggleSaveButton('btn-update-info');
        return false;
    }

    var MiddleName = $('#txtPersonalMiddleName').val();
    var MiddleNameCheck = IsCharactersOnly(MiddleName);
    if (MiddleNameCheck == false) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Middle Name allow characters only.' });
        ToggleSaveButton('btn-update-info');
        return false;
    }
    else {
        if (MiddleName.length > 30) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Middle Name allow maximum 30 characters only.' });
            ToggleSaveButton('btn-update-info');
            return false;
        }
    }

    var LastName = $.trim($('#txtPersonalLastName').val());
    if (LastName != null && LastName != "") {



        var LastNameCheck = IsCharactersOnly(LastName);
        if (LastNameCheck == false) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Last Name allow characters only.' });
            ToggleSaveButton('btn-update-info');
            return false;
        }
        else {
            if (LastName.length > 30) {
                $.toaster({ priority: 'warning', title: 'Notice', message: 'Last Name allow maximum 30 characters only.' });
                ToggleSaveButton('btn-update-info');
                return false;
            }
        }
    } else {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter Last Name.' });
        $('#txtPersonalLastName').focus();
        ToggleSaveButton('btn-update-info');
        return false;
    }



    //var OfficeName = $('#txtPersonalOfficeName').val();
    //var OfficeCheck = IsCharactersOnly(OfficeName);
    //if (OfficeCheck == false) {
    //    $.toaster({ priority: 'warning', title: 'Notice', message: 'Office name allow characters only.' });
    //    ToggleSaveButton('btn-update-info');
    //    return false;
    //}
    //else {
    //    if (OfficeName.length > 30) {
    //        $.toaster({ priority: 'warning', title: 'Notice', message: 'Office name allow maximum 30 characters only.' });
    //        ToggleSaveButton('btn-update-info');
    //        return false;
    //    }
    //}

    //RM-355: Changes Suggested by Lauri For profile page
    var OfficeName = $('#txtPersonalOfficeName').val();
    if (OfficeName.length > 50) {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Office name allow maximum 50 characters only.' });
        ToggleSaveButton('btn-update-info');
        return false;
    }


    var Title = $("#txtPersonalTitle").val();
    if (Title != null && Title != "" && Title != '') {
        var checkTitle = /^[a-zA-Z_ ]+(?:,[a-zA-Z]+)*$/;
        if (!checkTitle.test(Title)) {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Job title allow characters and comma.' });
            ToggleSaveButton('btn-update-info');
            return false;
        }
    }

    var specility = $('#drpSpeciality').val();
    if (specility == null || specility == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter Speciality.' });
        ToggleSaveButton('btn-update-info');
        return false;
    }



    performAjax({
        url: "/Profile/UpdateDoctorPersonalInformation",
        type: "POST",
        async: true,
        data: {
            UserId: idstr,
            FirstName: $.trim(($('#txtPersonalFirstName').val())),
            MiddleName: $.trim(($('#txtPersonalMiddleName').val())),
            LastName: $.trim(($('#txtPersonalLastName').val())),
            OfficeName: $.trim(($('#txtPersonalOfficeName').val())),
            Title: $('#txtPersonalTitle').val(),
            Specialities: $("#drpSpeciality").val().toString(),
            //Specialities: strSpecialities,
            Salutation: $('#ddlSalutation').val()
        },
        success: function (data) {
            if (data == "1") {
                $.toaster({ priority: 'success', title: 'Notice', message: 'Profile updated successfully!' });
                ToggleSaveButton('btn-update-info');
                setTimeout(function () {
                    window.location.reload();
                }, 3000);
            }
            else {
                $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to Profile.' });
            }
        }
    })
}

function LoadScript() {
    $(".numericOnly").bind("keypress", function (e) {
        var arr = [0, 8, 9, 16, 17, 20, 35, 36, 37, 38, 39, 40, 45, 46];
        // Allow letters
        for (var i = 48; i <= 57; i++) {
            arr.push(i);
        }

        // Prevent default if not in array
        if (jQuery.inArray(e.which, arr) === -1) {
            e.preventDefault();
            return false;
        }
    });
    $(".numericOnly").bind("paste", function (e) {
        return false;
    });
    $(".numericOnly").bind("drop", function (e) {
        return false;
    });
}

function AddBanner() {
    performAjax({
        url: "/Profile/NewAddBanner",
        type: "POST",
        success: function (data) {
            $("#add-banner").modal('show');
            $("#add-banner").html(data);
            $('.simple_color_color_code').simpleColor({ displayColorCode: true });
        }

    })
}

function EditBanner(BannerId) {
    performAjax({
        url: "/Profile/NewEditBanner",
        type: "POST",
        async: true,
        data: { BannerId: BannerId },
        success: function (data) {
            $("#add-banner").modal('show');
            $("#add-banner").html(data);
            $('.simple_color_color_code').simpleColor({ displayColorCode: true });
        }
    })

}

// For Banner InsertUpdate - Start
function SaveBannerDetail(BannerId) {
    //RM-355
    //if ($('#Title').val() == null || $('#Title').val() == "") {
    //    $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter Title.' });
    //    $("#Title").focus();
    //    ToggleSaveButton('btn-save-banner');
    //    return false;
    //}
    if (BannerId == 0) {
        if ($('#BannerImage').val() == null || $('#BannerImage').val() == "") {
            $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select Banner Image.' });
            $("#BannerImage").focus();
            ToggleSaveButton('btn-save-banner');
            return false;
        }
    }
    //RM-355
    //if ($('#ColorCode').val() == null || $('#ColorCode').val() == "") {
    //    $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select Color.' });
    //    $('#ColorCode').focus();
    //    ToggleSaveButton('btn-save-banner');
    //    return false;

    //}
    return true;
}

function BannerUpload() {
    if ($('#BannerImage').val() != null || $('#BannerImage').val() != "") {
        var item = $("#BannerImage").val();
        var extension = item.replace(/^.*\./, '');
        extension = extension.toLowerCase();
        if (extension == "jpg" || extension == "jpeg" || extension == "gif" || extension == "png" || extension == "bmp") {
        }
        else {
            $.toaster({ priority: 'danger', title: 'Notice', message: 'Please upload only Jpg,Jpeg,Png,Bmp and Gif Image.' });
            $("#BannerImage").val('');
            return false;
        }
    }
}

function crop_reset() {
    $("#addLogo").modal('show');
    $("#fuImage").val(null);
}

$("#closelogos").click(function () {
    $("#cropDisplay").text('');
    $("#txtimage").text('');
    $('#addLogo').modal('hide');
});

$("#closelogo").click(function () {
    $("#cropDisplay").text('');
    $("#txtimage").text('');
    $('#addLogo').modal('hide');
});

//$('body').removeClass('modal-open');

function IsProfileImage() {
    if ($("#fuImage").val() == 'undefined' || $("#fuImage").val() == '') {
        $("#txtimage").text('Please choose an appropriate image.');
        setTimeout(function () {
            $("#txtimage").css('display', 'none');
        }, 5000);
    }
    else {
        $("#txtimage").text('');
        $("#frmProfileImage").submit();
    }
}

//Profile Image functions.


function CheckIsImage() {
    var item = $("#fuImage").val();
    var extension = item.replace(/^.*\./, '');
    extension = extension.toLowerCase();
    //Change for RM-404:Blank page for .tif image
    if (extension == "jpg" || extension == "jpeg" || extension == "gif" || extension == "png" || extension == "bmp") {
        return true;
    }
    else if ($("#fuImage").val() == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select an image.' });
        ToggleSaveButton('fuImage');
        return false;
    } else {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please upload only Jpg,Png,Bmp and Gif Image.' });
        $('#fuImage').val('');
        $('#txtimage').text('');
        ToggleSaveButton('fuImage');
        return false;
    }
}

$("#fuImage").change(function () {
    readURL(this);
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.jcrop-holder').remove();
            $('#cropDisplay').empty().html('<img id="profileImageEditor" src="' + e.target.result + '"/>');

            jQuery('#profileImageEditor').Jcrop({
                onChange: showPreview,
                onSelect: showPreview,
                setSelect: [0, 10, 100, 100],
                aspectRatio: 1,
                boxWidth: 500,
                boxHeight: 500
            });
        }

        reader.readAsDataURL(input.files[0]);

    }
}

function showPreview(coords) {
    if (parseInt(coords.w) > 0) {
        $('#Top').val(coords.y);
        $('#Left').val(coords.x);
        $('#Bottom').val(coords.y2);
        $('#Right').val(coords.x2);
    }
}

function cropremove_reset() {
    //$("#cropContainer").html('<div id="cropDisplay"></div>');
    //$("#remove_image").modal('show');
    $("#addLogo").modal('show');
    $("#fuImage").val(null);
}


function RemoveProfileImage(UserId) {
    //$("#remove_image").modal('hide');
    performAjax({
        type: "POST",
        url: "/admin/RemoveDoctorProfileImage",
        data: { DoctorId: UserId },
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            $("#remove_image").modal('hide');
            $.toaster({ priority: 'success', title: 'Notice', message: 'Image removed successfully!' });
            setTimeout(function () {
                window.location.reload();
            }, 3000);

        },
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            $.toaster({ priority: 'danger', title: 'Error', message: 'Failed to remove an Image.' });
        }
    });
}

function javSearch(e) {
    var val = $("#Institute").val();
    $("#hdnInstitute").val('');
    var keycode;
    if (window.event) keycode = window.event.keyCode;
    else if (e) keycode = e.which;

    if (val.length > 0) {
        $.ajax({
            type: "POST",
            async: true,
            url: "/Profile/SearchUniversityNames",
            data: "{'UniversityNames':'" + val + "'}",
            contentType: "application/json; charset=utf-8",
            success: function (data) {

                var datasource = eval(data);
                if (datasource) {
                    $("#Institute").autocomplete({
                        sortResults: false,
                        scroll: false,
                        minLength: 0,
                        source: datasource,
                        focus: function (event, ui) {
                            var selectedObj = ui.item;
                            event.preventDefault();
                            return true;
                        },
                        select: function (event, ui) {                          
                            this.value = ui.item.value;
                            $("#hdnInstitute").val(ui.item.value);
                        }
                    });
                }

            },

            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $.toaster({ priority: 'danger', title: 'Error', message: 'Failed.' });
            }
        });


        function javAutoCompleteSelectHandler(event, ui)
        { }
    }
}

function OpenRemoveGallaryItem(GalleryId, UserId) {
    $("#remove_item").modal('show');
    $('#desc_message').text('Are you sure you want to remove gallery item ?');
    $('#btnyes').attr('onclick', ' NewRemoveGallaryItem("' + GalleryId + '","' + UserId + '")');

}


function NewRemoveGallaryItem(GalleryId, UserId) {
    $("#remove_item").modal('hide');
    UserId = (UserId) ? UserId : 0;
    performAjax({
        url: "/Profile/RemoveGallaryItem",
        type: "POST",
        data: { GallaryId: GalleryId, UserId: UserId },
        success: function (data) {
            $.toaster({ priority: 'success', title: 'Notice', message: ' Gallery item removed successfully!' });
            setTimeout(function () {
                window.location.reload();
            }, 3000);
        }
    })


}

function NewGallaryImageUpload(img) {
    var item = $("#fileImageForGallery").val();
    var extension = item.replace(/^.*\./, '');
    extension = extension.toLowerCase();
    if (extension == "tiff" || extension == "tif") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Select only valid image file. (i.e. .jpeg, .jpg, .gif, .png, .bmp).' });
        $("#fileImageForGallery").val(null);
        return false;
    }
    var filesList = $("#fileImageForGallery").prop("files");
    var fileUpload = img;
    if (typeof (fileUpload.files) != "undefined") {

        if (filesList.length > 0) {
            var fileName = $("#fileImageForGallery").prop("files")[0].name;
            if (!isImageFile(fileName)) {
                $.toaster({ priority: 'danger', title: 'Notice', message: 'Select only valid image file. (i.e. .jpeg, .jpg, .gif, .png, .bmp).' });
                setTimeout(function () { $("#fileImageForGallery").focus() }, 0);
                $("#fileImageForGallery").val(null);
                return false;
            }
        }

        var reader = new FileReader();
        reader.readAsDataURL(fileUpload.files[0]);
        reader.onload = function (e) {

            var image = new Image();
            image.src = e.target.result;
            image.onload = function () {

                var height = this.height;
                var width = this.width;
                if (width <= height) {
                    $.toaster({ priority: 'warning', title: 'Notice', message: 'Image should be in horizontal aspect ratio.' });
                    return false;
                }
                return true;
            };
        }
    } else {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'This browser does not support HTML5.' });
        return false;
    }
}

//Added for RM-389: Remove description
function RemoveDescription(idstr) {
    performAjax({
        url: "/Profile/UpdateDescriptionOfDoctor",
        type: "POST",
        data: { UserId: idstr, Description: '' },
        success: function (data) {
            if (data == "1") {
                $.toaster({ priority: 'success', title: 'Notice', message: 'Description removed successfully' });
                ToggleSaveButton('btn-des-removed');
                ToggleSaveButton('btn-description');
                setTimeout(function () {
                    window.location.reload();
                }, 2000);
            }
            else {
                $.toaster({ priority: 'danger', title: 'Notice', message: 'Failed to remove Description.' });
                ToggleSaveButton('btn-des-removed');
            }
        }
    })
}
