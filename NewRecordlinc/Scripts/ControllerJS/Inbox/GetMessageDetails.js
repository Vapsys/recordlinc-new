﻿$(document).ready(function () {   
    if ($('#IsDashboard').val() == 1) {
        $('#btn-rply').trigger('click');
    }
    var x = $('#IsDashboardFoward').val();
    if (x == 1)
    {
        $('#btn-forward').trigger('click');
    }

    if (PreviousURL) {
        if (PreviousURL.indexOf("Sent") > -1) {
            $("#lisentmessage").addClass('active');
        }
        else if (PreviousURL.indexOf("ReceiveReferral") > -1) {
            $("#lireceiveReferral").addClass('active');
        }
        else if (PreviousURL.indexOf("SentReferral") > -1) {
            $("#lisentreferral").addClass('active');
        }
    }
})
function GetColleaguesProfile(id) {
    $("#hdnViewProfile").val(id);
    $("#frmViewProfile").submit();
}
function GetPatientProfile(id) {
    $("#hdnViewPatientHistory").val(id);
    $("#frmViewPatientHistory").submit();
}
function getAttachedPatientHistory(id) {
    $("#hdnViewPatientHistory").val(id);
    $("#frmViewPatientHistory").submit();
}
function deletepatientmessage(id) {
    if (id != null || id != '') {
        swal({
            title: "",
            text: MSG_REMOVEDIALOG_TITLE,
            type: "warning",
            showCancelButton: true,
            confirmButtonText: MSG_REMOVEDIALOG_YES,
            cancelButtonText: MSG_REMOVEDIALOG_CANCEL,
            closeOnConfirm: true,
            showLoaderOnConfirm: true,
        },
function (isConfirm) {
    if (isConfirm) {
        $.post("/Inbox/DeletePatientMessage", { MessageId: id }, function (data, status, xhr) {
            if (xhr.status == 403) {
                SessionExpire(xhr.responseText)
            }
            $.toaster({ priority: 'success', title: 'Success', message: 'Message deleted successfully.' });
            //window.location.href = "/Inbox/Index";
            window.location.href = PreviousURL;
        });
    }
});
    } else {
        return false;
    }
}
function deletesentboxmessage(id) {
    if (id != null || id != '') {
        swal({
            title: "",
            text: MSG_REMOVEDIALOG_TITLE,
            type: "warning",
            showCancelButton: true,
            confirmButtonText: MSG_REMOVEDIALOG_YES,
            cancelButtonText: MSG_REMOVEDIALOG_CANCEL,
            closeOnConfirm: true,
            showLoaderOnConfirm: true,
        },
function (isConfirm) {
    if (isConfirm) {
        $.post("/Inbox/DeleteSentBoxMessage", { MessageId: id }, function (data, status, xhr) {
            if (xhr.status == 403) {
                SessionExpire(xhr.responseText)
            }
            $.toaster({ priority: 'success', title: 'Success', message: 'Message deleted successfully.' });
            window.location.href = "/Inbox/Sent";
        });
    }
});
    } else {
        return false;
    }
}
function deletemessage(id, check) {
    if (id != null || id != '') {
        swal({
            title: "",
            text: MSG_REMOVEDIALOG_TITLE,
            type: "warning",
            showCancelButton: true,
            confirmButtonText: MSG_REMOVEDIALOG_YES,
            cancelButtonText: MSG_REMOVEDIALOG_CANCEL,
            closeOnConfirm: true,
            showLoaderOnConfirm: true,
        },
function (isConfirm) {
    if (isConfirm) {
        $.post("/Inbox/DeleteMessageByMessageId", { MessageId: id, MessageType: check }, function (data, status, xhr) {
            if (xhr.status == 403) {
                $.toaster({ priority: 'success', title: 'Success', message: 'Message deleted successfully.' });
                SessionExpire(xhr.responseText)
            }
            if (data) {
                window.location.href = "/Inbox/Index";
            }
        });
    }
});
    }
    else {
        alert("Oops something want's to wrong try again later!");
    }
}
function getcomposeview(MessageId, IsPatientMessage, GetPreviousURL,Issent) {
    $("#divLoading").show();
    var objcomposedetails = {};
    objcomposedetails.MessageTypeId = 1;
    objcomposedetails.MessageId = MessageId;
    objcomposedetails.Issent = Issent;
    if (IsPatientMessage == 1) {
        $.post("/Inbox/PartialComposeMessage", { objcomposedetails: objcomposedetails }, function (data, status, xhr) {
            if (xhr.status == 403) {
                SessionExpire(xhr.responseText)
            }
            if (data != null) {
                $("#composeview").html('');
                $("#replayportion").hide();
                $("#composeview").show();
                $("#composeview").html(data);
                $('html, body').animate({ scrollTop: $("#composeview").offset().top }, "slow");
                $("#divLoading").hide();
                $("#btncancel").prop("href", GetPreviousURL)

            }
        });
    } else {
        $.post("/Patients/PartialPatientComposeMessage", { objcomposedetails: objcomposedetails }, function (data, status, xhr) {
            if (xhr.status == 403) {
                SessionExpire(xhr.responseText)
            }
            if (data != null) {
                $("#composeview").html('');
                $("#replayportion").hide();
                $("#composeview").show();
                $("#composeview").html(data);
                $('html, body').animate({ scrollTop: $("#composeview").offset().top }, "slow");
                $("#divLoading").hide();
                $("#btncancel").prop("href", GetPreviousURL)

            }
        });
    }

}
function SendAsForward(MessageId, IsPatientMessage, GetPreviousURL) {
    $("#divLoading").show();
    var objcomposedetails = {};
    objcomposedetails.MessageTypeId = 3;
    objcomposedetails.MessageId = MessageId
    if (IsPatientMessage == 1) {
        $.post("/Inbox/PartialColleagueForwardMessage", { objcomposedetails: objcomposedetails }, function (data, status, xhr) {
            if (xhr.status == 403) {
                SessionExpire(xhr.responseText)
            }
            if (data != null) {
                $("#composeview").html('');
                $("#replayportion").hide();
                $("#composeview").show();
                $("#composeview").append(data);
                $('html, body').animate({ scrollTop: $("#composeview").offset().top }, "slow");
                $("#btncancel").prop("href", GetPreviousURL);
                $("#divLoading").hide();
            }
        });
    } else {
        $.post("/Patients/PartialPatientForwardMessage", { objcomposedetails: objcomposedetails }, function (data, status, xhr) {
            if (xhr.status == 403) {
                SessionExpire(xhr.responseText)
            }
            if (data != null) {
                $("#composeview").html('');
                $("#replayportion").hide();
                $("#composeview").show();
                $("#composeview").append(data);
                $('html, body').animate({ scrollTop: $("#composeview").offset().top }, "slow");
                $("#btncancel").prop("href", GetPreviousURL);
                $("#divLoading").hide();

            }
        });
    }

}
function DownloadAtthachmentid(messageid, attachmentid, Isstatus) {
    $.post("/Inbox/DownloadAttachedfileofMessage", { DownId: messageid, AttaId: attachmentid, status: Isstatus }, function (data) {
    });
}