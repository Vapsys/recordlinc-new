﻿var PageIndex = 1, SortDirection = 2, PageSize = 10;

$(document).ready(function () {
    $('.jcf-scrollable').scroll(function () {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            if (PageIndex == -1) {
                return false;
            } else {
                GetInboxMessages(PageIndex + 1);
            }
        }
    });
    $(".checkedAll").change(function () {
        if (this.checked) {
            $(".checkSingle").each(function () {
                $(this).prop('checked', true);
                $(".all-select span").removeClass('jcf-unchecked').addClass('jcf-checked');
                $("#btndelete").addClass('active');
                $("#btndelete").removeClass('disableClick');
            })
        } else {
            $(".checkSingle").each(function () {
                $(this).prop('checked', false);
                $(".all-select span").removeClass('jcf-checked jcf-focus').addClass('jcf-unchecked');
                $("#btndelete").removeClass('active');
                $("#btndelete").addClass('disableClick');
            })
        }
    });
    $(document).on('change', '.checkSingle', function (e) {
        var IsAnyCheckBoxChecked = 0;
        if ($(this).is(":checked")) {
            var isAllChecked = 0;
            $(".checkSingle").each(function () {
                if (!this.checked) {
                    isAllChecked = 1;
                }
            })
            if (isAllChecked == 0) { $(".checkedAll").prop("checked", true); $(".checkedAll").parent().removeClass('jcf-unchecked').addClass('jcf-checked'); }
        } else {
            $(this).parent().removeClass('jcf-focus');
            $(".checkedAll").prop("checked", false);
            $(".checkedAll").parent().removeClass('jcf-checked jcf-focus').addClass('jcf-unchecked')
        }
        $(".checkSingle").each(function () {
            if ($(this).is(":checked")) {
                IsAnyCheckBoxChecked++;
            }

        });
        if (IsAnyCheckBoxChecked > 0) {
            $("#btndelete").addClass('active');
            $("#btndelete").removeClass('disableClick');
        } else {

            $("#btndelete").removeClass('active');
            $("#btndelete").addClass('disableClick');
        }
    });
    ManagePagging();
    $("#txtSearchMessage").keyup(function (event) {
        if (event.keyCode == 13) {
            ResetPagging();
            GetInboxMessages(1);
        }
    });
});
$('#ddlsorting li > a').click(function (e) {
    $('#btnsort').text(this.innerHTML);
    $("#hdddlsorting").val($(this).attr('data-id'));
    GetInboxMessages(1);
});
debugger
function GetInboxMessages(PageNo) {
    $("#lblmessage").html("");
    var SortColumn = $("#hdddlsorting").val();
    PageIndex = PageNo;
    if (PageIndex == 1) {
        $("#inboxmessages").html("");
        $(".checkedAll").prop("checked", false);
        $(".checkedAll").parent().removeClass('jcf-checked jcf-focus').addClass('jcf-unchecked');
    }
    var SearchText = $("#txtSearchMessage").val();
    $("#divLoading").show();
    $.post("/Inbox/PartialDraftMessages", { PageIndex: PageIndex, PageSize: PageSize, ShortColumn: SortColumn, ShortDirection: SortDirection, SearchText: SearchText, MessageDisplayType: MessageDisplayType },
       function (data) {
           if (PageIndex == 1) {
               $("#inboxmessages").html(''); ResetPagging();
               $("#btndelete").removeClass('active');
               $("#btndelete").addClass('disableClick');
           }
           $("#inboxmessages").append(data);
           if (parseInt($("#lblnumberofmessagedisplayed").val()) == 0) {
               $("#lblmessage").html("<center><br />No more message available</center>");
               PageIndex = -1;
           }
           else {
               //if (PageIndex == 1) {
               //    if (SortDirection == 1) { SortDirection = 2 } else { SortDirection = 1; }
               //}
           }
           ManagePagging();
           jcf.replaceAll();
           if ($(".checkedAll").is(':checked')) {
               $(".checkSingle").each(function () {
                   $(this).prop('checked', true);
                   $(".all-select span").removeClass('jcf-unchecked').addClass('jcf-checked');
                   $("#btndelete").addClass('active');
                   $("#btndelete").removeClass('disableClick');
               })
           }
           $("#divLoading").hide();
       });
}
function DeleteMessages() {
    if ($("#btndelete").hasClass('active')) {
        var SelectedMessages = [];
        $(".checkSingle:checked").each(function () {
            SelectedMessages.push($(this).attr('data-id'));
        });
        if (SelectedMessages <= 0) {
            alert("Please select at least one row to delete.");
            return false;
        }
        else {
            swal({
                title: "",
                text: MSG_REMOVEDIALOG_TITLE,
                type: "warning",
                showCancelButton: true,
                confirmButtonText: MSG_REMOVEDIALOG_YES,
                cancelButtonText: MSG_REMOVEDIALOG_CANCEL,
                closeOnConfirm: true,
                showLoaderOnConfirm: true,
            },
 function (isConfirm) {
     if (isConfirm) {
         $("#divLoading").show();
         $.ajaxSetup({ async: false });
         $.ajax({
             url: "/Inbox/RemoveDraftMessageByID",
             type: "post",
             data: { MessageId: SelectedMessages, MessageDisplayType: MessageDisplayType },
             success: function (data) {
                 if (data) {
                     $.each(SelectedMessages, function (n, val) {
                         $('#limsg_' + val).remove();
                         $("#limsg_" + val).find(".checkSingle:checked").prop('checked', false);
                         //$("#limsg_" + val).find(".checkSingle:checked").attr('disabled', true);
                     });
                     $(".lblnumberofmessagedisplayed").html(parseInt($(".lblnumberofmessagedisplayed").html()) - SelectedMessages.length);
                     $(".lbltotalmessage").html(parseInt($(".lbltotalmessage").html()) - SelectedMessages.length);
                     if (parseInt($(".lbltotalmessage").html()) > 0 && parseInt($(".lblnumberofmessagedisplayed").html()) == 0 || parseInt($(".lbltotalmessage").html()) == 0) {
                         GetInboxMessages(1);
                     }
                     $(".checkedAll").prop("checked", false);
                     $(".checkedAll").parent().removeClass('jcf-checked jcf-focus').addClass('jcf-unchecked');
                     jcf.replaceAll();
                     $("#divLoading").hide();
                     $("#btndelete").removeClass('active');
                     $("#btndelete").addClass('disableClick');
                     $.toaster({ priority: 'success', title: 'Success', message: SelectedMessages.length + '  message deleted successfully.' });
                 }
             },
             error: function (response, status, xhr) {
                 console.log("Error while deleting draft message");
             }
         });
         $.ajaxSetup({ async: true });

     }
 });
        }
    } else {
        return false;
    }
}
function ManagePagging() {
    $(".lblnumberofmessagedisplayed").html(parseInt($(".lblnumberofmessagedisplayed").html()) + parseInt($("#lblnumberofmessagedisplayed").val()));
    $(".lblintialpagenumber").html(parseInt($(".lblnumberofmessagedisplayed").html()) > 0 ? 1 : 0);
    $(".lbltotalmessage").html(parseInt($("#lbltotalmessage").val()));
    if (parseInt($("#lblintialpagenumber").val()) == 0) {
        $(".checkedAll").prop('disabled', true);
        $("#lblmessage").html("<center><br/>No messages found</center>");
    } else { $(".checkedAll").prop('disabled', false); }
}
function ResetPagging() {
    $(".lblnumberofmessagedisplayed").html('0');
    $(".lblintialpagenumber").html('0');
    $(".lbltotalmessage").html('0');
    $("#lblnumberofmessagedisplayed").val('0');
    $("#lblintialpagenumber").val('0');
    $("#lbltotalmessage").val('0');
}
