﻿$(document).ready(function () {
    if ($('#IsDashboard').val() == 1)
    {
        $('#btn-reply').trigger('click');
    }
    if (PreviousURL) {
        var TempUrl = PreviousURL.split('/');
        var TempPageName = TempUrl[TempUrl.length - 1].toString().toLowerCase();
        if (TempPageName == 'sent') {
            $("#lisentmessage").addClass('active');
        }
        else if (TempPageName == 'receivereferral') {
            $("#lireceiveReferral").addClass('active');
        }
        else if (TempPageName == 'sentreferral') {
            $("#lisentreferral").addClass('active');
        }
    }
})
function GetColleaguesProfile(id) {
    $("#hdnViewProfile").val(id);
    $("#frmViewProfile").submit();
}
function GetPatientProfile(id) {
    $("#hdnViewPatientHistory").val(id);
    $("#frmViewPatientHistory").submit();
}
function Deletemessage(id) {
    if (id != null && id != "") {
        swal({
            title: "",
            text: MSG_REMOVEDIALOG_TITLE,
            type: "warning",
            showCancelButton: true,
            confirmButtonText: MSG_REMOVEDIALOG_YES,
            cancelButtonText: MSG_REMOVEDIALOG_CANCEL,
            closeOnConfirm: true,
            showLoaderOnConfirm: true,
        },
      function (isConfirm) {
          if (isConfirm) {
              //if (confirm('Are you sure you want to delete this message?')) {
              $.post("/Inbox/DeleteMessageByMessageId", { MessageId: id }, function (data) {
                  if (data) {
                      window.location.href = "/Inbox/Index";
                  }
              });
          }
      });
    }
}
function getcomposeview(MessageId, GetPreviousURL,Issent) {
    $("#divLoading").show();
    var objcomposedetails = {};
    objcomposedetails.MessageTypeId = 1;
    objcomposedetails.MessageId = MessageId;
    objcomposedetails.Issent = Issent;
    $.post("/Inbox/PartialComposeMessage", { objcomposedetails: objcomposedetails }, function (data, status, xhr) {
        if (xhr.status == 403) {
            SessionExpire(xhr.responseText)
        }
        if (data != null) {
            $("#composeview").html('');
            $("#replayportion").hide();
            $("#composeview").show();
            $("#composeview").append(data);
            $("#btncancel").prop("href", GetPreviousURL)
            $('html, body').animate({ scrollTop: $("#composeview").offset().top }, "slow");
            $("#divLoading").hide();
        }
    });

}
function RemoveReferralFromInboxOfDoctor(id, messagetype) {
    swal({
        title: "",
        text: "Are you sure you want to delete message from Inbox",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: MSG_REMOVEDIALOG_YES,
        cancelButtonText: MSG_REMOVEDIALOG_CANCEL,
        closeOnConfirm: true,
        showLoaderOnConfirm: true,
    },
        function (isConfirm) {
            if (isConfirm) {
                $("#divLoading").show();
                $.post("/Inbox/DeleteMessageByMessageId", { MessageId: id, MessageType: messagetype }, function (data) {
                    if (data) {
                        $("#divLoading").hide();
                        $('#delete-ref').addClass('disabled');

                        //$.toaster({ priority: 'success', title: 'Success', message: 'Message deleted successfully.' });
                        //window.location.href = '/Inbox';
                        setTimeout(function () {
                            window.location.href = PreviousURL;
                        }, 2000);
                    }
                    else {
                        //$.toaster({ priority: 'success', title: 'Success', message: 'Message deleted successfully.' });
                    }
                });
            }
        });
}
function ResendReferralMessage(MessageId, ReferralCardId, PatientId, SenderName) {
    $("#divLoading").show();
    $.post("/Referrals/ResendMessage", { MessageId: MessageId, ReferralCardId: ReferralCardId, PatientId: PatientId, SenderName: SenderName },
         function (data, status, xhr) {
             if (xhr.status == 403) {
                 SessionExpire(xhr.responseText)
             }
             $("#divLoading").hide();
             $.toaster({ priority: 'success', title: 'Success', message: 'Referral resent successfully.' });
             setTimeout(function () {
                 window.location.href = PreviousURL;
             }, 2000);
         }).error(function () {
              $("#divLoading").hide();
         })
}

