﻿function AddGalleryItem(GallryId, UserId) {
    UserId = (UserId) ? UserId : 0;
    $.ajaxSetup({ async: false });
    $.post("/Profile/AddEditGalleryItem", { GallaryId: GallryId, UserId: UserId }, function (data) {
        
        //document.getElementById("add_image_gallery").innerHTML = data.toString();
        $("#add_image_gallery").html(data.toString());

    });
    $.ajaxSetup({ async: true });
}

function SaveGalleryDetail() {
    
    var VideoURL = $.trim($("#VideoURL").val());
    var filesList = $("#fileImageForGallery").prop("files");
    
    if (VideoURL.length > 0 && filesList.length > 0) {
        swal({ title: "", text: "Select either image or video URL", type: 'error' });
        return false;
    }

    if (filesList.length == 0 && VideoURL.length == 0) {
        swal({ title: "", text: "Upload either gallery image OR enter video URL", type: 'error' });
        setTimeout(function () { $("#fileImageForGallery").focus() }, 0);
        return false;

    }

    if (VideoURL.length > 0 && !isValidURL(VideoURL)) {
        swal({ title: "", text: "Enter valid video URL", type: 'error' });
        setTimeout(function () { $("#VideoURL").focus() }, 0);
        return false;

    }
    
    if (VideoURL.length > 0 && isValidURL(VideoURL))
    {
        var EmbededURL = getEmbededURL(VideoURL);
        if (EmbededURL == 'error')
        {
            swal({ title: "", text: "Enter valid youtube URL", type: 'error' });
        }
        else {
            $("#VideoURL").val("//www.youtube.com/embed/" + EmbededURL);
        }
        
    }
   
    return true;
}

function getEmbededURL(url) {
    var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExp);

    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return 'error';
    }
}


function isValidURL(str) {
    var pattern = /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i
    //if (!pattern.test(str)) {
    //    alert("Please enter a valid URL.");
    //    return false;
    //} else {
    //    return true;
    //}
    return pattern.test(str);
}
function RemoveGallaryItem(GalleryId, UserId) {
   
    UserId = (UserId) ? UserId : 0;
    swal({
        title: "",
        text: "Are you sure you want to remove gallery item ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, remove it!",
        closeOnConfirm: true,
    },
   function (isConfirm) {
       if (isConfirm) {
           
           $.ajaxSetup({ async: false });
           $.post("/Profile/RemoveGallaryItem", { GallaryId: GalleryId, UserId: UserId }, function (data) {
               location.reload();
           });
           $.ajaxSetup({ async: true });
       }
   });

}

function GallaryImageUpload(img) {
    
    var filesList = $("#fileImageForGallery").prop("files");
    var fileUpload = img;
    if (typeof (fileUpload.files) != "undefined") {

        if (filesList.length > 0) {
            var fileName = $("#fileImageForGallery").prop("files")[0].name;
            if (!isImageFile(fileName)) {
                swal({ title: "", text: "Select only valid image file. (i.e. .jpeg, .jpg, .gif, .tiff, .png, .bmp)", type: 'error' });
                setTimeout(function () { $("#fileImageForGallery").focus() }, 0);
                $("#fileImageForGallery").val(null);
                return false;
            }
        }

        var reader = new FileReader();
        reader.readAsDataURL(fileUpload.files[0]);
        reader.onload = function (e) {

            var image = new Image();
            image.src = e.target.result;
            image.onload = function () {

                var height = this.height;
                var width = this.width;
                if (width <= height) {
                    swal(Title = "Warning", Text = "Image should be in horizontal aspect ratio.");
                    return false;
                }
                return true;
            };
        }
    } else {
        swal(Title = "Warning", Text = "This browser does not support HTML5.");
        return false;
    }
}
function getFileExtension(fileName) {
    var extension = fileName.substr((fileName.lastIndexOf('.')));
    return extension.toLowerCase();
}
function isImageFile(fileName) {
    
    var ext = getFileExtension(fileName);
    return $.inArray(ext, getApprovedImageType()) >= 0;
}
function getApprovedImageType() {
    var imageTypes = new Array
        (
           ".jpeg",
           ".jpg",
           ".gif",
            ".tiff",
            ".png",
            ".bmp",
            ".tif"
        );

    return imageTypes;

}