﻿if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
    var msViewportStyle = document.createElement('style')
    msViewportStyle.appendChild(
    document.createTextNode(
        '@@-ms-viewport{width:auto!important}'
        )
    )
    document.querySelector('head').appendChild(msViewportStyle)
}
$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        $('.back-to-top').fadeIn();
    } else {
        $('.back-to-top').fadeOut();
    }
});
$('.back-to-top').click(function () {
    $("html, body").animate({ scrollTop: 0 }, 600);
    return false;
});
function SessionExpire(responseText) {
    var response = $.parseJSON(responseText);
    window.location = response.LogOnUrl;
}
$(document).ready(function () {
    $('#bs-example-navbar-collapse-1 ul li').each(function () {
        if (window.location.href.indexOf($(this).find('a:first').attr('href')) > -1) {
            $(this).addClass('active').siblings().removeClass('active');
        }
    });
});

