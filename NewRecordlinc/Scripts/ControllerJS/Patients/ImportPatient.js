﻿$('#Match_Field ,#Imported_Data,#Import_Finish').css('cursor', 'default');
function GoToStep2() {
    var FileName = "";
    FileName = $('#hdnfilename').val();
    if (FileName == null || FileName == "") {
        $("#ExcelUploadPopUp").modal('show');
        $('#desc_message').text('Upload .xlsx or .xls file.');
        $('#btn_yes').hide();
        $('#btn_no').css('right', '110px');
        $('#btn_no').text('Ok');
        return false;
    }
    else {
        var ext = FileName.split('.').pop();
        if (ext == "xls" || ext == "xlsx") {
            $.post("/Common/CheckAuthorizationForExecelByFeature",
                { FeatureId: New_Patient_Leads, UplaodeFileName: FileName }, function (data) {
                    if (data.Status == true) {
                        $('#ValidateStep2').click();
                        $("#Step2").addClass("active");
                        $.post("/Patients/NewGetImportPatientStep2",
                            { UplaodeFileName: FileName }, function (data) {
                                
                                if (data == 1)
                                {
                                    $("#not_valid_email").css('display', 'block');
                                    setTimeout(function () {
                                        $("#not_valid_email").css('display', 'none');
                                    }, 7000);
                                    setTimeout(function () {
                                        window.location.reload();
                                    }, 5000);
                                  return false;
                                }
                                if (data == 'empty') {
                                    $("#mandatory_fields").css('display', 'block');
                                    setTimeout(function () {
                                        $("#mandatory_fields").css('display', 'none');
                                    }, 10000);
                                    setTimeout(function () {
                                        window.location.reload();
                                    }, 5000);
                                    return false;
                                }

                                if (data != null && data != "") {
                                $("#Newstep1").hide();
                                $("#UploadExcel").removeClass("active");
                                $("#MatchField").addClass("active");
                                $("#Newstep2").append(data);
                                $('#StepMatch').val('1');
                                $('#btn_back').hide();
                                $("#hdnCurrentStep").val(2);
                                $(window).scrollTop(0);
                            }
                            if (data == false) {
                                $("#norecord").css('display', 'block');
                                setTimeout(function () {
                                    $("#norecord").css('display', 'none');
                                }, 7000);
                                }
                            });
                    } else if (data.Status == false) {

                        //swal({ html: true, title: "Upgrade Membership", text: 'You do not have permission to add patient. Please <a href="/FreeToPaid/UpgradeAccount">upgrade</a> your membership to add more patients.' });
                        //RM-231
                        //RM-195
                        $("#invalid_data").css('display', 'block');
                        setTimeout(function () {
                            $("#invalid_data").css('display', 'none');
                            window.location.reload();
                        }, 7000);
                        //swal({
                        //    title: "Import Patients",
                        //    text: data.ResponseMessage,
                        //    type: "error",
                        //    html: true
                        //});

                    }
                    else {
                        swal({ html: true, title: "Upgrade Membership", text: 'You do not have permission to add patient. Please <a href="/FreeToPaid/UpgradeAccount">upgrade</a> your membership to add more patients.' });
                        //$("#upgradeProfile").html(data)
                    }
                });
        }
        else {

            swal({ title: "", text: 'File must be in xlsx or xls in format' });
            // alert('File must be in xlsx or xls in format');
            return false;
        }
    }
}


//Go to Step 3
function GoToStep3() {
    
    var len = $('#mandatory_fields').text().length;
    //if ($('#mandatory_fields').text().length > 0)
    //{
    //    $(window).scrollTop(0);
    //    return false;
    //}
    var FileName = "";
    FileName = $('#hdnfilename').val();
    if (FileName == null || FileName == "") {
        $("#ExcelUploadPopUp").modal('show');
        $('#desc_message').text('Upload .xlsx or .xls file.');
        $('#btn_yes').hide();
        $('#ImageExcel').removeAttr('src');
        $('#btn_no').css('right', '110px');
        $('#btn_no').text('Ok');
        return false;
    }
    else {

        var FirstName = $('#ddlFirstName').val();
        var LastName = $('#ddlLastName').val();
        var Email = $('#ddlEmail').val();
        var Gender = $('#ddlGender').val();
        if (FirstName == "0" || LastName == "0" || Email == "0" || Gender == "0") {
            $("#ExcelUploadPopUp").modal('show');
            $('#desc_message').text('You must map all required fields.');
            $('#btn_yes').hide();
            $('#ImageExcel').removeAttr('src');
            $('#btn_no').css('right', '110px');
            $('#btn_no').text('Ok');
            return false;
        }

        var MappedColumns = null;
        MappedColumns = FirstName + ',' + $('#ddlMiddelName').val() + ',' + LastName + ',' + $('#ddlDateOfBirth').val() + ',' + $('#ddlGender').val() + ',' + Email + ',' + $('#ddlPassword').val() + ',' + $('#ddlAddress').val() + ',' + $('#ddlCity').val() + ',' + $('#ddlState').val() + ',' + $('#ddlCountry').val() + ',' + $('#ddlZip').val() + ',' + $('#ddlPhone').val();
        $('#hdnmappedcolumn').val(MappedColumns);
        $.post("/Patients/NewImportPatientShowExcelMappedData",
            { Mapping: MappedColumns }, function (data) {
                if (data != null && data != "") {
                    $("#Newstep2").hide();
                    $("#MatchField").removeClass('active');
                    $("#ImportedExcelData").addClass('active');
                    $("#addtable").append(data);
                    $("#ImportedPatients").hide();
                    $("#BlankRow").hide();
                    $('#btn_back').hide();
                    $("#step3").show();
                    $("#hdnCurrentStep").val(3);
                    var ModelCount = '0';
                    ModelCount = $("#ModelCount").val();
                    if (ModelCount == '0') {
                        $("#norecord").css('display', 'block');
                        $("#btnGo").hide();
                    }
                }
            });
        $('#ValidateStep3').click();
        $(window).scrollTop(0);
    }
}

//Go to step 4
function GoToStep4() {
    var MappedColumn = $('#hdnmappedcolumn').val();

    if (MappedColumn != null && MappedColumn != "") {
        $.post("/Patients/NewGetImportPatientStep4",
            { Mapping: MappedColumn }, function (data) {
                if (data != null && data != "") {
                    
                    $("#step3").hide();
                    $("#ImportedExcelData").removeClass("active");
                    $("#FinishImport").addClass("active");
                    $("#completestep4").show();
                    $('#NonImportedPatient').show();
                    $("#tableadd").append(data);
                    $("#hdnCurrentStep").val(4);
                    $("#NoImport").val('1');
                    $("#ImportedPatients").show();
                    $("#BlankRow").show();
                    $('#btn_back').hide();
                    $("#ValidateStep4").removeAttr('id');
                }
            });
    }
    $('#ValidateStep4').click();

    $(window).scrollTop(0);
}
//Example file
function ExampleFile(RelativePath) {
    var sitename = window.location.host;
    if (sitename.indexOf('http://') == -1 || sitename.indexOf('https://')) {
        sitename = 'http://' + sitename
    }
    var str = RelativePath;
    var res = sitename + str.replace("/mydentalfiles/", "/").replace("~/", "/");
    res = res.replace("mydentalfiles", "recordlinc");
    res = res.replace("../", "/");
    window.open(res, '_blank');
    window.focus();
}

//FileUpload Methods
function CountOfUpload() {

    var count = $('#hdnCount').val();
    if (count != null && count != "") {
        $("#ExcelUploadPopUp").modal('show');
        $('#desc_message').text('Allow only one .xlsx or .xls file at a time');
        $('#btn_yes').hide();
        $('#btn_no').css('right', '10px');
        $('#btn_no').text('Ok');
        return false;
    }
}
//Step Validation method
function ValidateClick(Step) {
    
    var FileNameCheck = "";
    FileNameCheck = $('#hdnfilename').val();
    if (FileNameCheck != null && FileNameCheck != "") {
        var ext = FileNameCheck.split('.').pop();
        if (ext == "xls" || ext == "xlsx") {
            var CurrentStep = $("#hdnCurrentStep").val();
            if (CurrentStep == "1") {
                if (CurrentStep == Step) {
                    return false;
                } else if (Step == "2") {
                    //GoToStep2();
                    //Showmessage('Click on button Next Step');
                    //$('#ImageExcel').removeAttr('src');
                } else {
                    //Showmessage('Click on button Next Step');
                    //$('#ImageExcel').removeAttr('src');
                    return false;
                }
            }
            if (CurrentStep == "2") {
                if (CurrentStep == Step) {
                    return false;
                } else if (Step == "1") {
                    window.location.reload();
                } else if (Step == "3") {
                    //GoToStep3();
                    //Showmessage('Click on Submit button.');
                    //$('#ImageExcel').removeAttr('src');
                } else {
                    //Showmessage('Click on Submit button.');
                    //$('#ImageExcel').removeAttr('src');
                    return false;
                }
            }
            if (CurrentStep == "3") {
                if (CurrentStep == Step) {
                    return false;
                } else if (Step == "1") {
                    window.location.reload();
                } else {
                    //Showmessage('Click on button Next Step');
                    //$('#ImageExcel').removeAttr('src');
                    return false;
                }
            }
            if (CurrentStep == "4") {
                if (CurrentStep == Step) {
                    return false;
                } else if (Step == "1") {
                    window.location.reload();
                } else {
                    //Showmessage('Click on button Finish!');
                    //$('#ImageExcel').removeAttr('src');
                    return false;
                }
            }
            //if (Step == "2") {
            //    if ($("#NoImport").val() == '1') {
            //        return false;
            //    } 
            //    if ($('#Newstep2').innerHTML == '') {
            //        $('#ValidateStep2').click();
            //        $("#MatchField").addClass("active");
            //    }
            //    else {
            //        Showmessage('Click on button Next Step');
            //        return false;
            //    }
            //}
            //if (Step == "3") {
            //    if ($("#NoImport").val() == '1')
            //    {
            //        return false;
            //    }               
            //    if ($("#AppendList").innerHTML == '') {
            //        $('#ValidateStep3').click();
            //        $("#ImportedExcelData").addClass("active");
            //    } else {
            //        Showmessage('Click on button Next Step.');
            //        return false;
            //    }
            //}
            //if (Step == "4") {
            //    if ($('#StepMatch').val() == '1')
            //    {
            //        Showmessage('Click on Submit button.');
            //        return false;
            //    }
            //    if ($("#AppendListImport").innerHTML == '') {
            //        $('#ValidateStep4').click();
            //        $("#FinishImport").addClass("active");                  
            //    }
            //    else {
            //        Showmessage('Click on button Next Step.');
            //        return false;
            //    }
            //}
        }
        else {

            swal({ title: "", text: 'File must be in xlsx or xls in format' });
            return false;

        }

    }
    else {
        if (Step == "1") {
            return false;
        } else {
            //Showmessage('Upload .xlsx or .xls file');
            return false;
        }
    }
    $(window).scrollTop(0);
}
function Showmessage(Message) {
    $("#ExcelUploadPopUp").modal('show');
    $('#desc_message').text(Message);
    $('#btn_yes').hide();
    $('#btn_no').css('right', '105px');
    $('#btn_no').text('Ok');
}
//Finish Import   
function FinishImport() {
    
    window.location.href = '../Patients';
    var y = $('#ImportHeading').val();
    if (y == '3') {
        $("#ExcelUploadPopUp").modal('show', 20000);
        $('#desc_message').text('Patients imported successfully.');
        $('#btn_yes').hide();
        $('#btn_no').css('right', '105px');
        $('#btn_no').hide();
        $('#btn_no').attr('onclick', location.href = '../Patients');
    }

    var x = $('#NonImportedPatient').is(':visible');
    if (x == true) {
        $("#ExcelUploadPopUp").modal('show', 20000);
        $('#desc_message').text('No patients imported.');
        $('#btn_yes').hide();
        $('#btn_no').css('right', '107px');
        $('#btn_no').hide();
        $('#btn_no').attr('onclick', location.href = '../Patients');
    }

    var z = $('#NoSingleRecord').is(':visible');
    if (z == true) {
        $("#ExcelUploadPopUp").modal('show', 20000);
        $('#desc_message').text('No patients imported.');
        $('#btn_yes').hide();
        $('#btn_no').css('right', '107px');
        $('#btn_no').hide();
        $('#btn_no').attr('onclick', location.href = '../Patients');
    }

}

function CheckAuthorization(FeatureId) {

    $.ajax({
        url: "/Common/CheckAuthorizationByFeature",
        type: "post",
        data: { FeatureId: FeatureId },
        async: true,
        success: function (data) {
            if (data == true) {
                switch (parseInt(FeatureId)) {
                    case New_Patient_Leads:
                        location.href = "/Patients/AddPatient"
                        break;
                    case ColleagueLead:
                        location.href = "/Colleagues/SearchColleagues"
                        break;
                }
            }
            else {
                swal({ html: true, title: "Upgrade Membership", text: 'You do not have permission to add patient. Please <a href="/FreeToPaid/UpgradeAccount">upgrade</a> your membership to add more patients.' });
            }
        }
    });
}