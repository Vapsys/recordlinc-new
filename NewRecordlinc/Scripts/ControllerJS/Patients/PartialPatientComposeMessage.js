﻿initSample();
$(document).ready(function () {
    if ($("#hdmessagetypeId").val() == "2") {
        $("#lidraft").addClass('active');
    }
    select2Dropdown('patientlist', 'Search Patient', 'Inbox', 'GetPatientList', true);
    //$("#tempfileupload").change(function () {

    //    // Checking whether FormData is available in browser
    //    if (window.FormData !== undefined) {
    //        var fileUpload = $("#tempfileupload").get(0);
    //        var files = fileUpload.files;
    //        var fileExtension = ['jpg', 'jpeg', 'bmp', 'gif', 'png', 'psd', 'pspimage', 'thm', 'tif', 'yuv', 'pdf', 'doc', 'docx', 'txt', 'xls', 'xlsx', 'zip', 'rar', 'dex', 'dcm'];
    //        for (var i = 0; i < files.length; i++) {
    //            if ($.inArray(files[i].name.split('.').pop().toLowerCase(), fileExtension) == -1) {
    //                //alert(fileExtension.join(', ') + " formats are to be uploaded.");

    //                $("#lblfileupload").html(fileExtension.join(', ') + " formats are to be uploaded.");
    //                $("input[type='file']").replaceWith($("input[type='file']").clone(true));
    //                $("#lblfileupload").show();
    //                setTimeout(function () {
    //                    $("#lblfileupload").hide();
    //                }, 15000);
    //                $("#tempfileupload").val('');
    //                return false;
    //            }
    //        }
    //        // Create FormData object
    //        var fileData = new FormData();
    //        // Looping over all files and add it to FormData object
    //        for (var i = 0; i < files.length; i++) {
    //            fileData.append(files[i].name, files[i]);
    //        }
    //        // Adding one more key to FormData object
    //        //fileData.append('username', 'Manas');
    //        $("#lblfileupload").hide();
    //        $("#divLoading").show();
    //        $.ajax({
    //            url: '/Common/UploadFiles',
    //            type: "POST",
    //            contentType: false, // Not to set any content header
    //            processData: false, // Not to process data
    //            data: fileData,
    //            success: function (data) {
    //                if (data) {
    //                    if ($("#hduploadfiles").val()) { $("#hduploadfiles").val(($("#hduploadfiles").val() + "," + data)) } else { $("#hduploadfiles").val(data) }
    //                    BindTeamFileUpload(data);
    //                    $("#tempfileupload").val('');
    //                    $("input[type='file']").replaceWith($("input[type='file']").clone(true));
    //                    $("#divLoading").hide();
    //                    $("#lblfileupload").hide();
    //                }
    //            },
    //            error: function (err) {
    //                alert(err.statusText);
    //            }
    //        });
    //    } else {
    //        alert("FormData is not supported.");
    //    }
    //});
});
function select2Dropdown(Select2ID, ph, Controller, listAction, isMultiple) {
    var sid = '#' + Select2ID;
    $(sid).select2({
        placeholder: ph,
        minimumInputLength: 1,
        multiple: isMultiple,
        ajax: {
            url: "/" + Controller + "/" + listAction,
            dataType: 'json',
            delay: 550,
            data: function (params) {
                return {
                    q: params.term,
                    page: params.page
                };
            },
            processResults: function (data, params) {
                params.page = params.page || 1;
                return {
                    results: data,
                    pagination: {
                        more: (params.page * 30) < data.total_count
                    }
                };
            },
            error: function (response, status, xhr) {
                if (response.status == 403) {
                    SessionExpire(response.responseText)
                }
            }
        }
    })
}
function BindTeamFileUpload(FileIds) {
    $("#divLoading").show();
    $.post("/Common/PartialFileUploadTemp", { FileIds: FileIds, MessageId: 0 },
       function (data) {
           $("#tempfilecontent").append(data);
           $("#divLoading").hide();
       });
}
function DeleteAttachMent(FileId, FileFrom, FilePath, TempFileId, ComposeType) {
    swal({
        title: "",
        text: MSG_REMOVEDIALOG_TITLE,
        type: "warning",
        showCancelButton: true,
        confirmButtonText: MSG_REMOVEDIALOG_YES,
        cancelButtonText: MSG_REMOVEDIALOG_CANCEL,
        showLoaderOnConfirm: true,
    },
       function (isConfirm) {
           if (isConfirm) {
               $("#divLoading").show();
               $.post("/Common/DeleteAttachedFile", { FileId: FileId, FileFrom: FileFrom, FileName: FilePath, ComposeType: ComposeType },
                  function (data) {
                      if (data) {
                          if (TempFileId.indexOf('F') >= 0 || TempFileId.indexOf('I') >= 0) {
                              if ($("#hduploadfiles").val()) { $("#hduploadfiles").val(RemoveValue($("#hduploadfiles").val(), TempFileId)); }
                          }
                          $("#" + TempFileId).remove();
                      }
                      $("#divLoading").hide();
                  });
           }
       });
}
function RemoveValue(list, value) {
    return list.replace(new RegExp(",?" + value + ",?"), function (match) {
        var first_comma = match.charAt(0) === ',',
            second_comma;
        if (first_comma &&
            (second_comma = match.charAt(match.length - 1) === ',')) {
            return ',';
        }
        return '';
    });
};
function SaveAsDraft(MessageId, ComposeType) {
    var Message = CKEDITOR.instances["editor"].getData();
    if ($("#patientlist").val() == null && Message == '' && $("#hduploadfiles").val() == '') {
        $("#danger-alert").alert();
        $("#danger-alert").fadeTo(3000, 3000).slideUp(3000, function () {
            $("#danger-alert").slideUp(3000);
        });
        $("html, body").animate({ scrollTop: 0 }, 600);
        return false;
    } else {
        $("#divLoading").show();
        var EncodedMessage = encodeURIComponent(Message);
        var objcomposedetail = {};
        objcomposedetail.MessageId = MessageId;
        objcomposedetail.PatientId = $("#patientlist").val() != null ? $("#patientlist").val().toString() : $("#patientlist").val();
        objcomposedetail.MessageBody = EncodedMessage;
        objcomposedetail.FileIds = $("#hduploadfiles").val();
        objcomposedetail.ComposeType = ComposeType;
        $.post("/Patients/SaveAsDraft", { objcomposedetail: objcomposedetail },
                        function (data, status, xhr) {
                            if (xhr.status == 403) {
                                var response = $.parseJSON(xhr.responseText);
                                window.location = response.LogOnUrl;
                            }
                            $("#divLoading").hide();
                            $.toaster({ priority: 'success', title: 'Success', message: 'Draft saved successfully' });
                            setTimeout(function () {
                                window.location = '/Inbox/Draft';
                            }, 3000);
                        });
    }
}
function SendMessage(MessageId, MessageTypeId) {
    $("#noemail").html("");
    $("#notreceivedmail").hide();
    var Message = CKEDITOR.instances["editor"].getData();
    var PatientId = $("#patientlist").val();
    var EncodedMessage = encodeURIComponent(Message);
    Messageflag = false, Patientflag = false
    if (PatientId == null) {
        Patientflag = true;
        $("#lblPatient").show();
    }
    //RM-412 : blank message issue
    var myContent = Message.toString();
    var x = $(myContent).text().trim();
    if (Message.toString() == '' || x == '') {
        Messageflag = true;
        $("#lblMessage").show();
    }

    if (Message.toString() == '') {
        Messageflag = true;
        $("#lblMessage").show();
    }
    if (Messageflag || Patientflag) {
        setTimeout(function () {
            HideValidation();
        }, 5000);
        return false;
    }
    $("#divLoading").show();
    var objcomposedetail = {};
    objcomposedetail.MessageId = MessageId;
    objcomposedetail.PatientId = $("#patientlist").val() != null ? $("#patientlist").val().toString() : $("#patientlist").val();
    objcomposedetail.MessageBody = EncodedMessage;
    objcomposedetail.FileIds = $("#hduploadfiles").val();
    objcomposedetail.MessageTypeId = MessageTypeId;
    $.post("/Patients/SendMessageToPatient", { objcomposedetail: objcomposedetail },
                     function (data, status, xhr) {
                         if (xhr.status == 403) {
                             var response = $.parseJSON(xhr.responseText);
                             window.location = response.LogOnUrl;
                         }
                         if (data == "1") {
                             $.toaster({ priority: 'success', title: 'Success', message: 'Your Message sent successfully.' });
                             setTimeout(function () {
                                 window.location = '/Inbox/Sent';
                             }, 3000);

                         }
                         else {
                             $("#noemail").html("Following patients haven't received email as they do not have valid email address.<br/><b>" + data + "</b>");
                             $("#notreceivedmail").show();
                         }
                         $("#divLoading").hide();

                     });

}
function HideValidation() {
    $("#lblMessage").hide();
    $("#lblPatient").hide();
}
function DeleteMessages(Id) {
    swal({
        title: "",
        text: MSG_REMOVEDIALOG_TITLE,
        type: "warning",
        showCancelButton: true,
        confirmButtonText: MSG_REMOVEDIALOG_YES,
        cancelButtonText: MSG_REMOVEDIALOG_CANCEL,
        closeOnConfirm: true,
        showLoaderOnConfirm: true,
    },
function (isConfirm) {
    if (isConfirm) {
        $("#divLoading").show();
        $.ajaxSetup({ async: false });
        $.ajax({
            url: "/Inbox/RemoveDraftMessageByID",
            type: "post",
            data: { MessageId: Id, MessageDisplayType: 1 },
            success: function (data) {
                if (data) {
                    $("#divLoading").hide();
                    $.toaster({ priority: 'success', title: 'Success', message: 'Message deleted successfully.' });
                    window.location.href = '../Inbox/Draft';
                }
            },
            error: function (response, status, xhr) {
                console.log("Error while deleting draft message");
            }
        });
        $.ajaxSetup({ async: true });
    }
});
}