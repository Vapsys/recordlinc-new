﻿//$('body').click(function () {
//    alert('hii');
//    $('.ui-menu-item').hide();
//});
function ClearPlaceHolder(input) {
    if (input.value == input.defaultValue) {
        input.value = "";
    }
}
function SetPlaceHolder(input) {
    if (input.value == "") {
        input.value = input.defaultValue;
    }
}

//function to get Value from parameter of URL
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

if ($('#redir').val() == 1)
{
    $('#patient_delete').modal('show');
    $('#btn_yes').hide();
    $('#btn_no').text('OK');
    $('#btn_no').css('right', '110px');
    $('#desc_message').text('Referral sent successfully.');
    $('#btn_no').on('click', function () {        
        var PatientId = getParameterByName('PatientId');
        var DoctorId = getParameterByName('DoctorId');
        if (PatientId > 0 && DoctorId > 0) {
            location.href = "/Appointment?PatientId=" + PatientId + "&DoctorId=" + DoctorId;
        }
    });
}

function CheckAuthorization(FeatureId) {
    $.ajax({
        url: "/Common/CheckAuthorizationByFeature",
        type: "post",
        data: { FeatureId: FeatureId },
        async: true,
        success: function(data) {
            if (data == true) {
                switch (parseInt(FeatureId)) {
                    case NewPatientLead:
                        location.href = "/Patients/AddPatient"
                        break;
                    case ColleagueLead:
                        location.href = "/Colleagues/SearchColleagues"
                        break;
                }
            }
            else {
                swal({ html: true, title: "Upgrade Membership", text: 'You do not have permission to add patient. Please <a href="/FreeToPaid/UpgradeAccount">upgrade</a> your membership to add more patients.' });
             }
        }
    });
  }

function OnSearchbtnClick() {
    $('#IsIndex').val(1);
    $('#search_btn').blur();
    var message = $('#txtHeaderSearchtext').val();
    $("#divLoading").show();
    GetPatientDetailsList(1, message, 11, 2)
    if (message != null && message != "") {
        $("#divClearSearch").css("display", "block");
        //$("#divClearSearch").html("<br/><a href='@Url.Action('Index', 'Patients')' style=\"text-decoration:none;\" title=\"Clear Search\" > &nbsp;Clear Search</a>");
    }
    else {
        GetPatientDetailsList(1, '', 11, 2)
        //document.getElementById('divSearchtextshow').innerHTML = '';
    }
    $("#divLoading").hide();
}
function HdrSearchPatientList(event) {
    if (event.keyCode == 13) {
        var message = $('#txtHeaderSearchtext').val();
        $("#divLoading").show();
        GetPatientDetailsList(1, message, 11, 2)
        if (message != null && message != "") {
            $("#divClearSearch").css("display", "block");
            //$("#divClearSearch").html("<br/><a href='@Url.Action('Index', 'Patients')' style=\"text-decoration:none;\" title=\"Clear Search\" > &nbsp;Clear Search</a>");
        }
        else {
            GetPatientDetailsList(1, '', 11, 2)
            //document.getElementById('divSearchtextshow').innerHTML = '';
        }
        $("#divLoading").hide();
    } else {
        return true;
    }
}
//Searchbox Code On Enter
function GetPatientDetailsList(PageIndex, SearchText, SortColumn, SortDirection) {
    var NewSearchText = $("ul.alphabet > li.active > a").text();
    var FilterBy = $("#FilterBy").find(':selected').attr('data-id');
    var message = $('#txtHeaderSearchtext').val();
    $('#PatientPageIndex').val("1");
    var obj = {};
    obj = {
        //UserId: $()
        'PageIndex': PageIndex,
        'NewSearchtext': NewSearchText,
        'FilterBy': FilterBy,
        'SortDirection': SortDirection,
        'SortColumn': SortColumn,
        'SearchText': SearchText
    }
    $.ajax({
        url: "/Patients/NewGetPatientDetailsList",
        type: "post",
        data: { FilterObj: obj },
        success: function (data) {
            if (data != null && data != "") {
                $('#AppendList').html(data);
                $('#PatientPageIndex').val("1");
                var ModelCount = $("#modelcount").val();
                $("#hdnmdlcountofPatients").val(parseInt(ModelCount));
                if (message == 'Search Patient') {
                    var ac = $('#count_actual').val();
                    $("#count").text(ac);
                }
                else {
                    $("#count").text(ModelCount);
                       }
            }else {
                $('#AppendList').html('');
            }
            $(window).scrollTop(0);
            $("#divLoading").hide();
            bindScrollHandler();
        }
    });
}

//Delete Selected Patients
function DeleteSelectedPatiets() {
    var userlist = [];
    var c = new Array();
    c = document.getElementsByTagName('input');
    var cnt = 0;
    var selected = "";
    for (var i = 0; i < c.length; i++) {
        if (c[i].type == 'checkbox' && c[i].id == 'CheckPateint') {
            if (c[i].checked == true) {
                cnt++;
                if (selected == "" && selected == null) {
                    selected = c[i].value;
                }
                else {
                    selected += c[i].value + ',';
                }
            }
        }
    }
    if (selected != null && selected != "") {
        $('#patient_delete').modal('show');
        $('#btn_yes').show();
        $('#btn_no').text('No');
        $('#desc_message').text('Are you sure you want to Delete Patients?');
        $('#btn_yes').attr('onclick', ' DeleteSeletedpatient("' + selected + '","' + cnt + '")');
    }
    else {
        $('#patient_delete').modal('show');
        $('#btn_yes').hide();
        $('#btn_no').text('OK');
        $('#desc_message').text('Please select at least one patient.');
        return false;
    }
}

function DeleteSeletedpatient(selected, cnt) {
        var userlist = selected.split(',');
        $.ajax({
            url: "/Patients/DeleteSelectedPatient",
            type: "post",
            data: { PatientId: selected, PageIndex: 0 },
            success: function (data) {
                var removed = data.RemovedCount;
                var unremoved = data.UnRemovedCount;
                if (removed > 0) {
                    
                    $('#desc_message').text('Patients(' + removed + ') removed successfully');
                    $('#btn_yes').hide();
                    $('#btn_no').html('Ok');
                    $('.modal-footer').css('text-align', 'center');
                    for (var i = 0; i < userlist.length; i++) {
                        $('#row_' + userlist[i]).remove();
                    }
                    var MyCount = $("#count").text();
                    var NewCount = parseInt(MyCount) - parseInt(cnt);
                    $("#count").text(NewCount);
                    cnt = 0;
                    if ($("#MainCheckBox").prop("checked")) {
                        $("#MainCheckBox").checked = false;
                        $("input[type=checkbox]").attr('checked', false);
                    }
                    //$('#patient_delete').modal('hide');
                    //if (userlist.length > 1) {
                    //    $('#desc_message').text('Patients Deleted Successfully!');
                    //    $('#btn_yes').hide();
                    //    $('#btn_no').html('Ok');
                    //    $('.modal-footer').css('text-align', 'center');
                        
                    //    return false;
                    //}
                    
                }
                else if (unremoved > 0) {
                    if (unremoved == 1) {
                        $('#patient_delete').modal('hide');
                        $.toaster({ priority: 'warning', title: 'Notice', message: 'This patient is associated with another dentist.' });
                        setTimeout(function () {
                            window.location.reload();
                        }, 5000);
                    }
                    else
                    {
                        $('#patient_delete').modal('hide');
                        $.toaster({ priority: 'warning', title: 'Notice', message:  'These patients are associated with another dentist.' });
                        setTimeout(function () {
                            window.location.reload();
                        }, 5000);
                    }                              
                }
                else {
                    $('#desc_message').text('Failed to remove patient.');
                }
            }
        });
    }
//Checkbox functions
function SetAllCheckBoxes(obj) {
    var c = new Array();
    c = document.getElementsByTagName('input');
    var selected = "";
    for (var i = 0; i < c.length; i++) {
        if (c[i].type == 'checkbox' && c[i].id == 'CheckPateint') {
            c[i].checked = obj.checked;
        }
    }
}


function UncheckMaincheckBox(id) {
    document.getElementById(id).checked = false;
}
//Delete an individual patient
function OpenDeletePopUp(id) {
    $('#patient_delete').modal('show');
    $('#btn_yes').show();
    $('#btn_no').html('No');
    $('.modal-footer').css('text-align','right');
    $('#desc_message').text('Are you sure you want to Delete Patient?');
    //$('#btn_yes').attr('onclick', ' DeletePatient("' + id + '","' + PageIndex + '")');
    $('#btn_yes').attr('onclick', ' DeletePatient("' + id + '")');
}
function DeletePatient(id)
{
    $.ajax({
        url: "/Patients/DeleteSelectedPatient",
        type: "post",
        data: { PatientId: id },
        success: function (data) {      
            var removed = data.RemovedCount;
            var unremoved = data.UnRemovedCount;
            if (unremoved == 1) {
                $('#desc_message').text('This patient is associated with another dentist.');
                $('#btn_yes').hide();
                $('#btn_no').html('Ok');
                $('.modal-footer').css('text-align', 'center');
                return false;
            } else {
                if (removed == 1) {
                    $('#desc_message').text('Patient removed successfully.');
                    $('.modal-footer').css('text-align', 'center');
                    $('#row_' + id).remove();
                    var MyCount = $("#count").text();
                    var NewCount = parseInt(MyCount) - 1;
                    $("#count").text(NewCount);
                    $('#btn_yes').hide();
                    $('#btn_no').html('Ok');
                    return false;
                    //$('#patient_delete').modal('hide');
                    //swal({ title: "Success", text: 'Patient Deleted Successfully!' });
                } else {
                    $('#desc_message').text('Failed to remove patient.');
                }
            }
        }
    });
}

//View patient History
function ViewPatientHistory(PatientId) {
    $("#divLoading").show();
    $('#hdnViewPatientHistory').val(PatientId);
    $("#frmViewPatientHistory").submit();
    $("#divLoading").hide();
}
//View Doctor Profile
function ViewProfile(id) {
    $("#divLoading").show();
    $('#hdnViewProfile').val(id);
    $("#frmViewProfile").submit();
    $("#divLoading").hide();
}
//Scrolling Functions.
function GetPatientListOnScroll() {
    debugger
    var datalist = '';
    var SortColumn = $("#Sort_Column").val();
    var NewSearchText = $("ul.alphabet > li.active > a").text();
    $.ajaxSetup({ async: false });
    if (NewSearchText != null) {
        NewSearchText =$("ul.alphabet > li.active > a").text();
    } else {
        NewSearchText = null;
    }
    var searchtext = $('#txtHeaderSearchtext').val();
    if (searchtext == 'Search Patient') {
        searchtext = '';
    }
    var filterby = $("#FilterBy").find(':selected').attr('data-id');
    var locationby = $("#LocationBy").find(':selected').attr('data-id');
    var obj = {};
    if (SortColumn == 2|| SortColumn == 14 || SortColumn == 13 || SortColumn == 9 || SortColumn == 10) {
        NewSearchText = null;
    }else{
        NewSearchText = $("ul.alphabet > li.active > a").text();
    }
    var SortDirection = $("#AppendList").data("sortirection");
    if (SortDirection == 1) {
        SortDirection = 2;
    } else {
        SortDirection = 1;
    }
    var pageindex = $('#PatientPageIndex').val();
    if (pageindex == '-1') {
        return "";
    }
    else {
        pageindex = parseInt(pageindex) + parseInt(1);
        obj = {
            'PageIndex': pageindex,
            'NewSearchtext': NewSearchText,
            'FilterBy': filterby,
            'LocationBy': locationby,
            'SortDirection': SortDirection,
            'SortColumn': SortColumn,
            'SearchText': searchtext
        }
        $.ajax({
            url: "/Patients/NewGetListOfPatientOnScroll",
            type: "post",
            data: { FilterObj: obj },
            success: function(data) {
                if (data == '' || data.trim() == '<tr><td colspan="8"><center>No more record found</center></td></tr>') {
                    $('#PatientPageIndex').val('-1');
                } else {
                    $('#PatientPageIndex').val(pageindex);
                }
                datalist = data;
            }
        });
    }
    $.ajaxSetup({ async: true });
    return datalist;
}
function bindScrollHandler() {
    $(window).scroll(function () {
        if ($(document).height() - $(window).height() == $(window).scrollTop()) {
            var data = GetPatientListOnScroll().toString();
            var isIndex=$('#IsIndex').val();
            if ($('#nomore').text() != 'No more record found')
            {
                $('#modelcount').remove();           
                $('#AppendList').append(data);
                if ($('#nomore').length === 0) {
                   

                    if (isIndex == 0)
                    {
                        var ac = $('#count_actual').val();
                        $("#count").text(ac);
                    }
                    else
                    {
                        var ModelCount = $("#modelcount").val();
                        var MdlHidenCount = $("#hdnmdlcountofPatients").val();
                        var final = $("#hdnmdlcountofPatients").val(parseInt(MdlHidenCount) + parseInt(ModelCount));
                        $("#count").text(final.val());
                    }
                }
            }
            //if ($("#MainCheckBox").prop("checked")) {
            //    $("input[type=checkbox]").attr('checked', true);
            //}
            if ($(".checkedAll").prop("checked")) {
                $(".all-select span").removeClass('jcf-unchecked').addClass('jcf-checked');
            }
        }
        else {
        }
    });
}

$(document).ready(function () {
    $('#search_main').show();
    $('#search_colleague').hide();
    bindScrollHandler();
    $('.alphabet > li ').click(function () {
        if ($('.alphabet > li').hasClass('active')) {
            $('.alphabet > li').removeClass('active');
        }
        if ($(this).hasClass('active') > 0) {
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }
    });

});
//Method For Alphabetic sorting A to Z .
function GetPatientListByAtoZ(NewSearchText) {
    var datalist = '';
    var LocationBy = $("#LocationBy").find(':selected').attr('data-id');
    var FilterBy = $("#FilterBy").find(':selected').attr('data-id');
    var message = $('#txtHeaderSearchtext').val();
    var SortColumn = "";
    if (SortColumn == null || SortColumn == 'undefined' || SortColumn == "") {
        SortColumn = 1;
    }
    $('#PatientPageIndex').val(1);
    var PageIndex = 1;
    var SearchText = null;
    var SortDirection = "1";
    var obj1 = {};
    obj1 = {
        'PageIndex': PageIndex,
        'NewSearchtext': NewSearchText,
        'FilterBy': FilterBy,
        'LocationBy': LocationBy,
        'SortDirection': SortDirection,
        'SortColumn': $("#AppendList").data("sortcolumn"),
        'SearchText': SearchText
    }

    $.ajax({
        url: "/Patients/NewGetPatientDetailsList",
        type: "post",
        data: { FilterObj: obj1 },
        async: true,
        success: function (data) {
            if (data != null && data != "") {             
                $('#AppendList').html(data);
                var ModelCount = $("#modelcount").val();
                var HidenCount = $("#modelcount").val();
                $("#hdnmdlcountofPatients").val(parseInt(ModelCount));
                if ($("#MainCheckBox").prop("checked")) {
                    $("#MainCheckBox").checked = false;
                    $("input[type=checkbox]").attr('checked', false);
                }
                if (message == 'Search Patient' && NewSearchText == 'All') {
                    var ac = $('#count_actual').val();
                    $("#count").text(ac);
                }
                else {
                    $("#count").text(ModelCount);
                }
            }
            else {
                $('#AppendList').html('');
            }
        }
    });
}
//Function of changing location New.
$('#LocationBy').on('change', function () {
    if ($("#MainCheckBox").prop("checked")) {
        $("#MainCheckBox").checked = false;
        $("input[type=checkbox]").attr('checked', false);
    }

    $('#IsIndex').val(1);
    var lid = $("#LocationBy").find(':selected').attr('data-id');//Value of th Location By.
    if (lid == 0)
    {
        var ac = $('#count_actual').val();
        $("#count").text(ac);
    }
    $('.alphabet > li').removeClass('active');
    $('select[id="FilterBy"] option[data-id="2"]').attr("selected","selected");
    $('ul.alphabet > li').first('a').addClass('active');
    var LocationBy = lid;
    var SearchText = null;
    var SortDirection = "1";
    var FilterBy = 3;
    var obj2 = {};
    obj2 = {
        'PageIndex': 1,
        'NewSearchtext': null,
        'FilterBy': FilterBy,
        'LocationBy': LocationBy,
        'SortDirection': SortDirection,
        'SortColumn': $("#AppendList").data("sortcolumn"),
        'SearchText': SearchText
    }
    $.ajax({
        url: "/Patients/NewGetPatientDetailsList",
        type: "post",
        data: { FilterObj: obj2 },
        async: true,
        success: function (data) {
            if (data != null && data != "") {
                $('#AppendList').html(data);
               
                if (lid == 0) {
                    var ac = $('#count_actual').val();
                    $("#count").text(ac);
                }
                else {
                    var ModelCount = $("#modelcount").val();
                    var HidenCount = $("#modelcount").val();
                    $("#hdnmdlcountofPatients").val(parseInt(ModelCount));
                    $("#count").text(ModelCount);
                }
            }
            else {
                $('#PatientPageIndex').val('-1');
            }
        }
    });
});
//Method for Sorting Via headings of table.
function GetPatientListByHeadings(PageIndex, SearchText, SortColumn, SortDirection) {
    $("#Sort_Column").val(SortColumn);
    var FilterBy = $("#FilterBy").find(':selected').attr('data-id');
    var NewSearchText = $("ul.alphabet > li.active > a").text();
    var StrLocation = $("#LocationBy").find(':selected').attr('data-id');
    var SortColumn = SortColumn;
    if (SortColumn == null || SortColumn == 'undefined' || SortColumn == "") {
        SortColumn = 1;
    }
    var SortDirection = SortDirection;
    if (SortDirection == 1) {
        SortDirection = 2;
    } else {
        SortDirection = 1;
    }
    if(SortColumn==2) {
        $("#sortIdLast").attr('onclick', ' GetPatientListByHeadings("' + PageIndex + '","' + SearchText + '","'+SortColumn+'","'+SortDirection+'")');
    }
    if(SortColumn==1) {
        $("#sortIdFirst").attr('onclick', ' GetPatientListByHeadings("' + PageIndex + '","' + SearchText + '","'+SortColumn+'","'+SortDirection+'")');
    }
    if(SortColumn==14) {
        $("#sortIdRef").attr('onclick', ' GetPatientListByHeadings("' + PageIndex + '","' + SearchText + '","'+SortColumn+'","'+SortDirection+'")');
    }
    if(SortColumn==9) {
        $("#sortIdPhone").attr('onclick', ' GetPatientListByHeadings("' + PageIndex + '","' + SearchText + '","'+SortColumn+'","'+SortDirection+'")');
    }
    if(SortColumn==10) {
        $("#sortIdEmail").attr('onclick', ' GetPatientListByHeadings("' + PageIndex + '","' + SearchText + '","'+SortColumn+'","'+SortDirection+'")');
    }
    if(SortColumn==13) {
        $("#sortIdLocation").attr('onclick', ' GetPatientListByHeadings("' + PageIndex + '","' + SearchText + '","'+SortColumn+'","'+SortDirection+'")');
    }
    var SearchText = null;
    var obj3 = {};
    obj3 = {
        'PageIndex': PageIndex,
        'NewSearchtext': null,
        'FilterBy': FilterBy,
        'LocationBy': StrLocation,
        'SortDirection': SortDirection,
        'SortColumn': SortColumn,
        'SearchText': SearchText,
        'NewSearchtext': NewSearchText == 'All' ? null : NewSearchText
    }
    $.ajax({
        url: "/Patients/NewGetPatientDetailsList",
        type: "post",
        data: { FilterObj: obj3 },
        async: true,
        success: function (data) {
            if (data == null || data == "") {
                $('#PatientPageIndex').val('-1');
            } else {
                $('#AppendList').html(data);
                var ModelCount = $("#modelcount").val();
                var HidenCount = $("#modelcount").val();
                $("#hdnmdlcountofPatients").val(parseInt(ModelCount));
                $("#count").text(ModelCount);
            }
        }
    });
}
function CallPlan(id)
{
    $("#hdnPatienHistoryId").val(id);
    $("#frmPatientListPlan").submit();
}
//Method for Open Send referral Pop Up
function OpenSendReferralPopup(pid)
{
    var PatientId = pid;
    $('#hdnPatientId').val(pid);
    var GetSearchText = null;
    if ($("#txtSearchColleague").val() == null) {
        GetSearchText = null;
    } else if ($('#txtSearchColleague').length == 0) {
        GetSearchText = null;
    }
    else {
        if ($('txtSearchColleague').val() == 'Find colleague to refer to...') {
            GetSearchText = null;
        }
        else {
            GetSearchText = $('txtSearchColleague').val();
        }
    }
    $.ajax({
        url: '/Patients/NewGetListSearchColleagueFromPopUp',
        type: 'post',
        data: { SearchText: GetSearchText },
        success: function(data) {

            $("#NewRefferalPopUp").modal('show');
            $("#NewRefferalPopUp").html(data);
            $("#btn_send_pat").hide();
            $("#btn_send_patient").show();
            $(".list-holder").bind('scroll', function(event) {
                if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
                    var data = GetColleaguesByScroll().toString();
                    if (data.indexOf('Colleague not found') > -1) {
                        $('.referral-list').append(data);
                        $("#ColleaguPageIndex").val("-1");
                    }
                    else {
                        $('.referral-list').append(data);
                        $("#ColleaguPageIndex").val(PageIndex);
                    }
                }
            });
        }
    })
}

var timer;
function SearchColleagueOnkeyPress() {
    var ctrl = $("#txtSearchColleague").val();
    clearTimeout(timer);
    var ms = 500; // milliseconds
    timer = setTimeout(function () {
        Onkeypresscolleaguelist(ctrl);
    }, ms);
}
//Bind Colleagues on scroll.
function Onkeypresscolleaguelist(ctrl) {
    var value = ctrl.toLowerCase().trim();
    if ($.trim(value).length == 0) {
        value = "";
    }
    $('#divLoading').show();
    $.ajaxSetup({ async: false });
    $.ajax({
        url: "/Patients/NewSearchColleagueFromPopUp",
        type: 'post',
        data: { SearchText: value },
        success: function(data) {
            $('#divLoading').hide();
            $('.referral-list').html('');
            $('.referral-list').append(data);
        }
    });

    $.ajaxSetup({ async: true });
    $(".list-holder").bind('scroll', function (event) {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            var data = GetColleaguesByScroll().toString();
            $(this).append(data);
        }
    });
}

function SearchColleague() {
    $("#ColleaguPageIndex").val("1");
    var GetSearchText = null;
    if ($('#txtSearchColleague').length == 0) {
        GetSearchText = null;
    } else {
        GetSearchText = document.getElementById('txtSearchColleague').value;
        if (GetSearchText == "Find colleague to refer to...") {
            GetSearchText = null;
        }
    }
    $.ajaxSetup({ async: false });
    $.ajax({
        url: "/Patients/NewGetListSearchColleagueFromPopUp",
        type: 'post',
        data: { SearchText: GetSearchText },
        success: function (data) {
            $('.referral-list').append(data);
        }
    })

    $.ajaxSetup({ async: true });
    $(".list-holder").bind('scroll', function (event) {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            var data = GetColleaguesByScroll().toString();
            $(this).append(data);
        }
    });
}
//Method For Scrolling
function GetColleaguesByScroll()
{
    var datalist = '';
    $.ajaxSetup({ async: false });
    var PageIndex = $("#ColleaguPageIndex").val();
    var SearchText = $("#txtSearchPatient").val();
    if (PageIndex == "-1") {
        return "";
    } else {
        PageIndex = parseInt(PageIndex) + parseInt(1);
        $.ajax({
            url: '/Patients/NewGetColleagueListForSendReferralPopUpOnScroll',
            type: 'post',
            data: { PageIndex: PageIndex, SearchText: SearchText },
            success: function (data) {
                if (data == '' || data.trim() == '<li><center>No more record found.</center></li>') {
                   
                    $("#ColleaguPageIndex").val("-1");
                }
                else {
                    $("#ColleaguPageIndex").val(PageIndex);
                }
                datalist = data;
            }
        });
    }
    $.ajaxSetup({ async: true });
    return datalist;
}
//Method to check Colleague
function SelectColleague(cid)
{
    $(".referral-list > li").each(function () { $(this).removeClass('active'); });
    if ($("." + cid).hasClass('active')) {
        $("." + cid).removeClass('active');
    } else {
        $("." + cid).addClass('active');
        $('#btn_send_pat').attr("disabled", false);
        $("#hdnColleagueId").val(cid);
    }
}
//Send Referral to Colleague
function FinalReferralSend() {
        
    var PatientId = $('#hdnPatientId').val();
    var ColleagueId = $("#hdnColleagueId").val();
    var LocationId = $("#hdnlocationid").val();
    if (ColleagueId == null || ColleagueId == "") {
        $("#NewRefferalPopUp").modal('show');
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select at least one colleague.' });
        //$('#btn_send').attr("disabled", true);
        ToggleSaveButton('btn_send_pat');
    }
    else if (LocationId == null || LocationId == "") {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select at least one location.' });
        ToggleSaveButton('btn_send_pat');
    }
    else {
        window.location.href = '/Referrals/Referral?PatientId=' + PatientId + '&ColleagueIds=' + ColleagueId + '&AddressInfoId=' + LocationId;
    }
}

function ToggleSaveButton(id) {
    $('#' + id).attr('disabled', true);
    setTimeout(function () {
        $('#' + id).attr('disabled', false);
    }, 4000);
}


//Send Message to selected.
    function SendMessageToSelected() {
        var c = new Array();
        c = document.getElementsByTagName('input');
        var selected = "";
        for (var i = 0; i < c.length; i++) {
            if (c[i].type == 'checkbox' && c[i].id == 'CheckPateint') {
                if (c[i].checked == true) {
                    if (selected == "" && selected == null) {
                        selected = c[i].value;
                    }
                    else {
                        selected += c[i].value + ',';
                    }
                }
            }
        }
        if (selected != null && selected != "") {
            $("#PatientId").val(selected);
            $("#frmcompose").submit();
        }
        else {
            $('#patient_delete').modal('show');
            $('#btn_yes').hide();
            $('#btn_no').html('Ok');
            $('.modal-footer').css('text-align', 'center');
            $('#desc_message').text('Please select at least one patient.');
            return false;
        }
    }

//Select Location Code
function SelectLocationForReferral() {
    var colleaguid = $("#hdnColleagueId").val();
    if (colleaguid == null || colleaguid == "") {
        //Jquery Error for toster
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please select at least one Colleague.' });
        ToggleSaveButton("btn_send_Location");
        return false;
    }
    else {
        $('.referral-list').html('');
        $(".list-holder").unbind('scroll');
    }
    $("#divLoading").show();

    $.ajax({
        url: "/Dashboard/LocationList",
        type: "POST",
        async: true,
        data: { UserId: colleaguid },
        success: function (data) {
            if (data == 1) {
                $("#hdnlocationid").val(0);
                FinalReferralSend();
            }
            else
            {
                $("#divLoading").hide();
                $(".referral-list").html(data);
                $("#btn_send_Location").hide();
                $("#btn_send_patient").hide();
                $('#Colleague_Search_Block').hide();
                $('#clg-heading').hide();
                $('#Patient_Search_Block').hide();
                $('#loc-heading').show();
                $("#btn_send_dashboard").show();
                $("#btn_send_pat").show();
                $('#btn_send_final').hide();
            }
         
            //$(".list-holder").bind('scroll', function (event) {
            //    if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            //        var data = GetPatientByScroll().toString();
            //        if (data == "" || data.indexOf('Patient not found.') > -1) {
            //            $('.referral-list').append(data);
            //            $("#PatientPageIndex").val("-1");
            //        }
            //        else {
            //            $('.referral-list').append(data);
            //            //$("#PatientPageIndex").val(PageIndex);
            //        }
            //    }
            //});
        }
    })

}

function SelectLocation(lid) {
    $(".referral-list > li").each(function () { $(this).removeClass('active'); });
    if ($("." + lid).hasClass('active')) {
        $("." + lid).removeClass('active');
    } else {
        $("." + lid).addClass('active');
        $('#btn_send_patient').attr("disabled", false);
        $("#hdnlocationid").val(lid);
    }
}

function OpenAppointment(PatientId, ReferredById) {
    if (PatientId > 0 && ReferredById > 0) {
        location.href = "/Appointment?PatientId=" + PatientId + "&DoctorId=" + ReferredById;
    }
}

function SendForms(PatientId,Email,Name) {
    $.ajax({
        url: '/Patients/SendForm',
        type: 'post',
        data: { PatientId: PatientId, Email: Email, PatientName: Name },
        success: function (data) {
            if (data == true) {

                $.toaster({ priority: 'success', title: 'success', message: 'Form send successfully' });
            }
            else {
                $.toaster({ priority: 'danger', title: 'Failed', message: 'Failed to send the form.' });
            }
        }
    });
}

//Method for Open Request Payment Pop Up
function OpenRequestPayment(PatientId) {
    $.ajax({
        url: '/Patients/ReferPaymentFromPopUp',
        type: 'post',
        data: { PatientId: PatientId },
        success: function (data) {
            $("#NewRefferalPopUp").modal('show');
            $("#NewRefferalPopUp").html(data);
        }
    })
}

function SubmitReferPayment(PatientId) {
    if ($('#Amount').val().length > 0)
    {
        $.ajax({
            url: '/Patients/SubmitReferPayment',
            type: 'post',
            data: { PatientId: PatientId, Amount: $('#Amount').val() },
            success: function (data) {
                if (data == true) {
                    $("#NewRefferalPopUp").modal('hide');
                    $.toaster({ priority: 'success', title: 'success', message: 'Payement request send successfully' });
                }
                else {
                    $.toaster({ priority: 'danger', title: 'Failed', message: 'Failed to send payment request.' });
                }
            }
        });
    }
    else {
        $.toaster({ priority: 'warning', title: 'Notice', message: 'Please enter amount.' });
    }
   
}
    
function RequestReviews(PatientId, Email, FirstName, LastName) {
    $.ajax({
        url: '/Patients/RequestReviews',
        type: 'post',
        data: { PatientId: PatientId, Email: Email,FirstName:FirstName, LastName:LastName },
        success: function (data) {
            if (data == true) {

                $.toaster({ priority: 'success', title: 'success', message: 'Request send successfully.' });
            }
            else {
                $.toaster({ priority: 'danger', title: 'Failed', message: 'Failed to send the Request.' });
            }
            datalist = data;
        }
    });
}

$(document).ready(function () {
    //$("#search_main").css("display", "block");
    //$("#search_main").html("<div class='search_main'><div class='searchbar'><input id='txtHeaderSearchtext' PlaceHolder='Search Patients' type='text' ></div></div>");
    $("#txtHeaderSearchtext").autocomplete({
        source: function (request, response) {
            $.post("/Patients/GetPatientAutocompleteList", { text: request.term }, function (data) {
                if (!data.length) {
                    var result = [
                        {
                            label: 'No record found',
                            value: 0
                        }
                    ];
                    response(result);
                }
                else {
                    // normal response
                    response($.map(data, function (item) {
                        return {
                            label: item.FirstName.trim() + ' ' + item.LastName.trim()
                            + ' (' + item.PatientId + ($.trim(item.Email).length > 0 ? ' - ' + item.Email : '') + ')'
                            , value: item.PatientId
                        };
                    }));
                }

            });
        },
        minLength: 3,
        select: function (event, ui) {
            event.preventDefault();
            if (ui.item.label != "No record found") {
                $("#txtHeaderSearchtext").val(ui.item.label);
                ViewPatientHistory(ui.item.value);
            }
            else {
                $("#txtHeaderSearchtext").val(null);
            }
        }

    });
}); 