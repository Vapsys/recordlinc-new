﻿$(document).ready(function () {
    $(document).on('change', '#tempfileupload', function () {        
        // Checking whether FormData is available in browser
        if (window.FormData !== undefined) {
            var fileUpload = $("#tempfileupload").get(0);
            var files = fileUpload.files;
            var fileExtension = ['jpg', 'jpeg', 'bmp', 'gif', 'png', 'psd','tif','yuv', 'pspimage', 'thm', 'yuv', 'pdf', 'doc', 'docx', 'txt', 'xls', 'xlsx', 'zip', 'rar', 'dex', 'dcm'];
            for (var i = 0; i < files.length; i++) {
                if ($.inArray(files[i].name.split('.').pop().toLowerCase(), fileExtension) == -1) {
                    //alert(fileExtension.join(', ') + " formats are to be uploaded.");
                    $("#lblfileupload").html(fileExtension.join(', ') + " formats are to be uploaded.");
                    $("input[type='file']").replaceWith($("input[type='file']").clone(true));
                    $("#lblfileupload").show();
                    setTimeout(function () {
                        $("#lblfileupload").hide();
                    }, 15000);
                    $("#tempfileupload").val('');
                    return false;
                }
            }
            // Create FormData object
            var fileData = new FormData();
            // Looping over all files and add it to FormData object
            for (var i = 0; i < files.length; i++) {
                fileData.append(files[i].name, files[i]);
            }

            // Adding one more key to FormData object
            $("#lblfileupload").hide();
            $("#divLoading").show();
            $.ajax({
                url: '/FileUpload/TempUpload',
                type: "POST",
                contentType: false, // Not to set any content header
                processData: false, // Not to process data
                data: fileData,
                success: function (data) {
                    if (data) {
                        if ($("#hduploadfiles").val()) { $("#hduploadfiles").val(($("#hduploadfiles").val() + "," + data)) } else { $("#hduploadfiles").val(data) }
                        BindTeamFileUpload(data, 1);
                        $("#tempfileupload").val('');
                        $("input[type='file']").replaceWith($("input[type='file']").clone(true));
                        $("#divLoading").hide();
                        $("#lblfileupload").hide();
                    }
                },
                error: function (err) {
                    alert(err.statusText);
                    $("#divLoading").hide();
                }
            });
        } else {
            alert("FormData is not supported.");
        }
    });

    $("#tempfileuploadcomm").change(function () {    
        // Checking whether FormData is available in browser
        if (window.FormData !== undefined) {
            var fileUpload = $("#tempfileuploadcomm").get(0);
            var files = fileUpload.files;
            var fileExtension = ['jpg', 'jpeg', 'bmp', 'gif', 'png', 'psd', 'pspimage', 'thm', 'tif', 'yuv', 'pdf', 'doc', 'docx', 'txt', 'xls', 'xlsx', 'zip', 'rar', 'dex', 'dcm'];
            for (var i = 0; i < files.length; i++) {
                if ($.inArray(files[i].name.split('.').pop().toLowerCase(), fileExtension) == -1) {
                    //alert(fileExtension.join(', ') + " formats are to be uploaded.");
                    $("#lblfileupload").html(fileExtension.join(', ') + " formats are to be uploaded.");
                    //$("input[type='file']").replaceWith($("input[type='file']").clone(true));
                    $("#lblfileupload").show();
                    setTimeout(function () {
                        $("#lblfileupload").hide();
                    }, 15000);
                    $("#tempfileuploadcomm").val('');
                    return false;
                }
            }
            // Create FormData object
            var fileData = new FormData();
            // Looping over all files and add it to FormData object
            for (var i = 0; i < files.length; i++) {
                fileData.append(files[i].name, files[i]);
            }

            // Adding one more key to FormData object               
            $("#lblfileupload").hide();
            $("#divLoading").show();
            $.ajax({
                url: '/FileUpload/TempUpload',
                type: "POST",
                contentType: false, // Not to set any content header
                processData: false, // Not to process data
                data: fileData,
                success: function (data) {
                    if (data) {
                        if ($("#hduploadfiles").val()) { $("#hduploadfiles").val(($("#hduploadfiles").val() + "," + data)) } else { $("#hduploadfiles").val(data) }
                        BindTeamFileUpload(data, 0);
                        $("#tempfileuploadcomm").val('');
                      //  $("input[type='file']").replaceWith($("input[type='file']").clone(true));
                        $("#divLoading").hide();
                        $("#lblfileupload").hide();
                    }
                },
                error: function (err) {
                    alert(err.statusText);
                    $("#divLoading").hide();
                }
            });
        } else {
            alert("FormData is not supported.");
        }
    });

    $("#patientExcelUpload").change(function () {
        // Checking whether FormData is available in browser
            if (window.FormData !== undefined) {
            var fileUpload = $("#patientExcelUpload").get(0);
            var files = fileUpload.files;
            var fileExtension = ['xlsx','xls'];
            for (var i = 0; i < files.length; i++) {
                if ($.inArray(files[i].name.split('.').pop().toLowerCase(), fileExtension) == -1) {
                    //swal({ title: "", text: 'Upload .xlsx or .xls file' });
                    $("#ExcelUploadPopUp").modal('show');
                    $('#desc_message').text('Upload .xlsx or .xls file.');
                    $('#btn_yes').hide();
                    $('#btn_no').css('right', '115px');
                    $('#btn_no').text('Ok');
                    //$("#lblfileupload").html(fileExtension.join(', ') + " formats are to be uploaded.");
                    $("input[type='file']").replaceWith($("input[type='file']").clone(true));
                    $("#lblfileupload").show();
                    setTimeout(function () {
                        $("#lblfileupload").hide();
                    }, 15000);
                    $("#patientExcelUpload").val('');
                    return false;
                }
            }
            // Create FormData object
            var fileData = new FormData();
            // Looping over all files and add it to FormData object
            for (var i = 0; i < files.length; i++) {
                fileData.append(files[i].name, files[i]);
            }

            // Adding one more key to FormData object               
            $("#lblfileupload").hide();
            $("#divLoading").show();
            $.ajax({
                url: '/Common/UploadExcel',
                type: "POST",
                contentType: false, // Not to set any content header
                processData: false, // Not to process data
                data: fileData,
                success: function (data) {
                    if (data) {
                        if ($("#hduploadfiles").val()) { $("#hduploadfiles").val(($("#hduploadfiles").val() + "," + data)) } else { $("#hduploadfiles").val(data) }
                        BindExcelFileUpload(data, 0);
                        $("#patientExcelUpload").val('');
                        //  $("input[type='file']").replaceWith($("input[type='file']").clone(true));
                        $("#divLoading").hide();
                        $("#lblfileupload").hide();
                    }
                },
                error: function (err) {
                    alert(err.statusText);
                    $("#divLoading").hide();
                }
            });
        } else {
            alert("FormData is not supported.");
        }
    });



});

function BindExcelFileUpload(FileIds, ids) {
    $("#divLoading").show();
    if (ids == 0) {
            $.post("/Common/PartialExcelFileUploadTempComm", { FileIds: FileIds, MessageId: 0 },
            function (data) {
                $("#tempExcelContent").append(data);
                $("#divLoading").hide();
            });
    }
    if (ids == 1) {
        $.post("/Common/PartialExcelFileUploadTempComm", { FileIds: FileIds, MessageId: 0 },
            function (data) {
                $("#tempExcelContent").append(data);
                $("#divLoading").hide();
            });
    }

}

function BindTeamFileUpload(FileIds, ids) {
    $("#divLoading").show();
    if (ids == 0)
    {
        $.post("/Common/PartialFileUploadTempComm", { FileIds: FileIds, MessageId: 0 },
        function (data) {
            $("#tempfilecontent").append(data);
            $("#divLoading").hide();
        });
    }
    if (ids == 1)
    {
        $.post("/Common/PartialFileUploadTemp", { FileIds: FileIds, MessageId: 0 },
        function (data) {
            $("#tempfilecontent").append(data);
            $("#divLoading").hide();
        });
    }
    
}

function DeleteAttachMent(FileId, FileFrom, FilePath, TempFileId, ComposeType) {
    debugger
    swal({
        title: "",
        text: MSG_REMOVEDIALOG_TITLE,
        type: "warning",
        showCancelButton: true,
        confirmButtonText: MSG_REMOVEDIALOG_YES,
        cancelButtonText: MSG_REMOVEDIALOG_CANCEL,
        showLoaderOnConfirm: true,
    },
        function (isConfirm) {
            if (isConfirm) {
                $("#divLoading").show();
                $.post("/FileUpload/DeleteAttachedFile", { FileId: FileId, FileFrom: FileFrom, FileName: FilePath, ComposeType: ComposeType },
                    function (data) {
                        debugger
                        if (data) {
                            debugger
                            if (TempFileId.indexOf('F') >= 0 || TempFileId.indexOf('I') >= 0) {
                                if ($("#hduploadfiles").val()) { $("#hduploadfiles").val(RemoveValue($("#hduploadfiles").val(), TempFileId)); }
                            }
                            $("#" + TempFileId).remove();
                        }
                        swal({
                            title: "Successfully",
                            text: "Attachment deleted successfully",
                            timer: 1500,
                            showConfirmButton: false
                        });
                        $("#divLoading").hide();
                    });
            }
        });
}

function DeleteExcel(FileId, FileFrom, FilePath, TempFileId, ComposeType) {
    swal({
        title: "",
        text: MSG_REMOVEDIALOG_TITLE,
        type: "warning",
        showCancelButton: true,
        confirmButtonText: MSG_REMOVEDIALOG_YES,
        cancelButtonText: MSG_REMOVEDIALOG_CANCEL,
        showLoaderOnConfirm: true,
    },
        function (isConfirm) {
            if (isConfirm) {
                $("#divLoading").show();
                $.post("/Common/DeleteAttachedExcel", { FileId: FileId, FileFrom: FileFrom, FileName: FilePath, ComposeType: ComposeType },
                    function (data) {
                        if (data) {
                            if (TempFileId.indexOf('F') >= 0 || TempFileId.indexOf('I') >= 0) {
                                if ($("#hduploadfiles").val()) { $("#hduploadfiles").val(RemoveValue($("#hduploadfiles").val(), TempFileId)); }
                            }
                            $("#" + TempFileId).remove();
                        }
                        swal({
                            title: "Successfully",
                            text: "Attachment deleted successfully",
                            timer: 1500,
                            showConfirmButton: false
                        });
                        $("#divLoading").hide();
                    });
            }
        });
}


function RemoveValue(list, value) {
    return list.replace(new RegExp(",?" + value + ",?"), function (match) {
        var first_comma = match.charAt(0) === ',',
            second_comma;
        if (first_comma &&
            (second_comma = match.charAt(match.length - 1) === ',')) {
            return ',';
        }
        return '';
    });
};