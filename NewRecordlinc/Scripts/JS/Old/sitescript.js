$(document).ready(function() {

    $( ".dropdown-submenu" ).click(function(event) {
        // stop bootstrap.js to hide the parents
        event.stopPropagation();
        // hide the open children
        $( this ).find(".dropdown-submenu").removeClass('open');
        // add 'open' class to all parents with class 'dropdown-submenu'
        $( this ).parents(".dropdown-submenu").addClass('open');
        // this is also open (or was)
        $( this ).toggleClass('open');
    });
    
});
$('[data-toggle=offcanvas]').click(function() {
    $('.row-offcanvas').toggleClass('active');
    $('.collapse').toggleClass('in').toggleClass('hidden-xs').toggleClass('visible-xs');
});
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

/*$(document).ready(function(){
	$(".opener1").click(function(){
		$("#content").toggleClass("active1");
	});
	
	$(".opener2").click(function(){
		$("#content").toggleClass("active2");
	});
	
	$(".opener3").click(function(){
		$("#content").toggleClass("active3");
	});
});*/