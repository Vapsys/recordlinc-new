﻿using System;
using System.Web;
using System.Web.Script.Serialization;

using System.IO;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using NewRecordlinc.Models;
using System.Configuration;
using DataAccessLayer.Common;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.PatientsData;
using System.Collections.Generic;

namespace NewRecordlinc
{
    /// <summary>
    /// Summary description for PatientTempFileUpload
    /// </summary>
    public class PatientTempFileUpload : IHttpHandler, System.Web.SessionState.IRequiresSessionState
    {
        clsCommon ObjCommon = new clsCommon();
        clsPatientsData ObjPatientsData = new clsPatientsData();

        public void ProcessRequest(HttpContext context)
        {


            if (context.Request.HttpMethod == "OPTIONS")
            {
                ReturnOptions(context);
            }

            try
            {
                context.Response.ContentType = "json";
                HttpPostedFile postedFile = context.Request.Files["Filedata"];

                string Sesionid = string.Empty;
                if (context.Session["UserId"] != null)
                {
                    Sesionid = context.Session["UserId"].ToString();
                }
                else
                {
                    // Code for when file uploaded from public profile set public profile user id - start
                    if (context.Session["PublicProfileUserId"] != null)
                    {
                        Sesionid = context.Session["PublicProfileUserId"].ToString();
                    }
                    // Code for when file uploaded from public profile set public profile user id - end
                }



                var lstFileUpload = new System.Collections.Generic.List<FileUpload>();
                JavaScriptSerializer js = new JavaScriptSerializer();


                int DeleteId = -1;
                string DeleteFileExtension = string.Empty;

                if (context.Request.QueryString["DeleteId"] != null && context.Request.QueryString["DeleteId"] != "")
                {
                    DeleteId = Convert.ToInt32(context.Request.QueryString["DeleteId"]); // Uploaded RecordId will in Respsoe

                    if (context.Request.QueryString["FileExtension"] != null && context.Request.QueryString["FileExtension"] != "")
                    {
                        DeleteFileExtension = context.Request.QueryString["FileExtension"]; // Uploaded Files type
                    }
                }

                string tempPath = System.Configuration.ConfigurationManager.AppSettings["TempUploadsFolderPath"];
                string savepath = context.Server.MapPath(tempPath);
                savepath = savepath.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");



                if (DeleteId == -1)
                {
                    for (int filecount = 0; filecount < context.Request.Files.Count; filecount++)
                    {
                        HttpPostedFile hpf = context.Request.Files[filecount] as HttpPostedFile;

                        if (hpf.ContentLength == 0)
                            continue;

                        string FileName = string.Empty;
                        string strFileExtension;

                        if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
                        {
                            string[] filenames = hpf.FileName.Split(new char[] { '\\' });
                            FileName = filenames[filenames.Length - 1];
                        }
                        else
                        {
                            FileName = hpf.FileName;
                        }
                        if (FileName.Contains("\\"))
                        {
                            string[] files = hpf.FileName.Split(new char[] { '\\' });
                            FileName = files[files.Length - 1];
                        }
                        if (FileName.Contains("/"))
                        {
                            string[] files = hpf.FileName.Split(new char[] { '/' });
                            FileName = files[files.Length - 1];
                        }
                        string strNewFileName = "$" + DateTime.Now.Ticks + "$" + FileName;

                        strFileExtension = Path.GetExtension(FileName).ToLower();

                        int RecordId = 0;
                        if (strFileExtension != ".jpg" && strFileExtension != ".jpeg" && strFileExtension != ".gif" && strFileExtension != ".png" && strFileExtension != ".bmp" && strFileExtension != ".psd" && strFileExtension != ".pspimage" && strFileExtension != ".thm")
                        {
                            RecordId = ObjCommon.Insert_Temp_UploadedFiles(Convert.ToInt32(Sesionid.Split('?')[0]), strNewFileName, clsCommon.GetUniquenumberForAttachments());
                        }
                        else
                        {
                            RecordId = ObjCommon.Insert_Temp_UploadedImages(Convert.ToInt32(Sesionid.Split('?')[0]), strNewFileName, clsCommon.GetUniquenumberForAttachments());
                        }




                        hpf.SaveAs(Path.Combine(savepath + strNewFileName));

                      



                        string ThumbUrl = string.Empty;
                        if (strFileExtension.ToLower() != ".jpg" && strFileExtension.ToLower() != ".jpeg" && strFileExtension.ToLower() != ".gif" && strFileExtension.ToLower() != ".png" && strFileExtension.ToLower() != ".bmp" && strFileExtension.ToLower() != ".psd" && strFileExtension.ToLower() != ".pspimage" && strFileExtension.ToLower() != ".thm" )
                        {
                            ThumbUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/Content/images/" + NewRecordlinc.Common.FileExtension.CheckFileExtenssionOnlyThumbName("unknown" + strFileExtension);
                        }
                        else
                        {
                            ThumbUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + tempPath.Replace("~/", "/") + strNewFileName;
                        }



                        lstFileUpload.Add(new FileUpload()
                        {
                            progress = "1.0",
                            name = FileName,
                            size = hpf.ContentLength,
                            type = hpf.ContentType,
                            url = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + tempPath.Replace("~/", "/") + strNewFileName,
                            thumbnail_url = ThumbUrl,
                            delete_url = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/PatientTempFileUpload.ashx?DeleteId=" + RecordId + "&FileExtension=" + strFileExtension,
                            delete_type = "DELETE"
                        });

                    }

                }
                else
                {
                    // Delete uploaded files
                    DataTable dt = new DataTable();
                    if (DeleteFileExtension != ".jpg" && DeleteFileExtension != ".jpeg" && DeleteFileExtension != ".gif" && DeleteFileExtension != ".png" && DeleteFileExtension != ".bmp" && DeleteFileExtension != ".psd" && DeleteFileExtension != ".pspimage" && DeleteFileExtension != ".thm")
                    {

                        dt = new DataTable();
                        dt = ObjCommon.Get_UploadedFilesById(DeleteId);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            string Name = string.Empty;
                            Name = savepath + Convert.ToString(dt.Rows[0]["DocumentName"]);
                            if (System.IO.File.Exists(Name))
                            {
                                System.IO.File.Delete(Name);

                                // Delete Document
                                ObjCommon.Temp_DeleteUploadedFileById(Convert.ToInt32(Sesionid), Convert.ToInt32(DeleteId));
                            }
                        }


                    }
                    else
                    {

                        dt = new DataTable();
                        dt = ObjCommon.Get_UploadedImagesById(DeleteId);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            string Name = string.Empty;
                            Name = savepath + Convert.ToString(dt.Rows[0]["DocumentName"]);
                            if (System.IO.File.Exists(Name))
                            {
                                System.IO.File.Delete(Name);

                                // Delete Image
                                ObjCommon.Temp_DeleteUploadedImageById(Convert.ToInt32(Sesionid), Convert.ToInt32(DeleteId));

                            }
                        }
                    }

                }

                context.Response.Write(js.Serialize(lstFileUpload));
            }
            catch (Exception ex)
            {
                bool status = ObjCommon.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                context.Response.Write(ex.Message + ex.StackTrace);
            }
        }

        private static void ReturnOptions(HttpContext context)
        {
            context.Response.AddHeader("Allow", "DELETE,GET,HEAD,POST,PUT,OPTIONS");
            context.Response.StatusCode = 200;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }

    public class FileUpload
    {
        public int group { get; set; }
        public string progress { get; set; }
        public string name { get; set; }
        public int size { get; set; }
        public string type { get; set; }
        public string url { get; set; }
        public string thumbnail_url { get; set; }
        public string delete_url { get; set; }
        public string delete_type { get; set; }
        public string error { get; set; }
        public string shortfilename { get; set; }
    }
}