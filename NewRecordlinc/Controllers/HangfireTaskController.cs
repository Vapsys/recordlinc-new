﻿using BusinessLogicLayer;
using Hangfire;
using NewRecordlinc.App_Start;
using System;
using System.Web.Mvc;

namespace NewRecordlinc.Controllers
{
    public class HangfireTaskController : Controller
    {
        // GET: HangfireTask
        public ActionResult Index()
        {
            return View();
            // RecurringJob.AddOrUpdate(() => BulkInsertProcedures(),Cron.HourInterval(5));
        }

        [CustomAuthorize]
        public void BulkInsertProcedures()
        {
            //RecurringJob.AddOrUpdate(() => BulkInsertBLL.BulkInsertProcedures(), Cron.MinuteInterval(10));
            var data = BulkInsertBLL.BulkInsertProcedures();
            BackgroundJob.Schedule(() => BulkInsertProcedures(), DateTime.Now.AddMinutes(10));
        }
        [CustomAuthorize]
        public void BulkInsertPatientAdjustments()
        {
            //RecurringJob.AddOrUpdate(() => BulkInsertBLL.BulkInsertPatientAdjustments(), Cron.MinuteInterval(10));
            var data = BulkInsertBLL.BulkInsertPatientAdjustments();
            BackgroundJob.Schedule(() => BulkInsertPatientAdjustments(), DateTime.Now.AddMinutes(12));
        }
        [CustomAuthorize]
        public void BulkInsertPatientDentrixInsurance()
        {
            //RecurringJob.AddOrUpdate(() => BulkInsertBLL.BulkInsertPatientDentrixInsurance(), Cron.MinuteInterval(10));
            var data = BulkInsertBLL.BulkInsertPatientDentrixInsurance();
            BackgroundJob.Schedule(() => BulkInsertPatientDentrixInsurance(), DateTime.Now.AddMinutes(10));
        }
        [CustomAuthorize]
        public void BulkInsertPatientDentrixStandardPayment()
        {
            var data = BulkInsertBLL.BulkInsertPatientDentrixStandardPayment();
            //RecurringJob.AddOrUpdate(() => BulkInsertBLL.BulkInsertPatientDentrixStandardPayment(), Cron.MinuteInterval(10));
            BackgroundJob.Schedule(() => BulkInsertPatientDentrixStandardPayment(), DateTime.Now.AddMinutes(10));
        }
        [CustomAuthorize]
        public void BulkInsertPatientDentrixFinanceCharges()
        {
            //RecurringJob.AddOrUpdate(() => BulkInsertBLL.BulkInsertPatientDentrixFinanceCharges(), Cron.MinuteInterval(10));
            var data = BulkInsertBLL.BulkInsertPatientDentrixFinanceCharges();
            BackgroundJob.Schedule(() => BulkInsertPatientDentrixFinanceCharges(), DateTime.Now.AddMinutes(15));
        }

        [CustomAuthorize]
        public void BulkInsertPatientDentrixAccountAging()
        {
            //RecurringJob.AddOrUpdate(() => BulkInsertBLL.BulkInsertPatientDentrixAccountAging(), Cron.MinuteInterval(10));
            var data = BulkInsertBLL.BulkInsertPatientDentrixAccountAging();
            BackgroundJob.Schedule(() => BulkInsertPatientDentrixAccountAging(), DateTime.Now.AddMinutes(15));
        }

        [CustomAuthorize]
        public void bulkinsertpatientappointment()
        {
            //RecurringJob.AddOrUpdate(() => BulkInsertBLL.BulkInsertPatientAppointment(), Cron.MinuteInterval(15));
            var data = BulkInsertBLL.BulkInsertPatientAppointment();
            BackgroundJob.Schedule(() => bulkinsertpatientappointment(), DateTime.Now.AddMinutes(20));
        }

        [CustomAuthorize]
        public void OnederfulIntegration()
        {
            OnederfulBLL.GetPatientInsuranceInfo();
        }

        [CustomAuthorize]
        public void AssignZipwhipToken()
        {
            var data = Zipwhip.AssignZipwhipToken();
            //RecurringJob.AddOrUpdate(() => Zipwhip.AssignZipwhipToken(), Cron.MinuteInterval(10));
            BackgroundJob.Schedule(() => AssignZipwhipToken(), DateTime.Now.AddMinutes(15));
        }

        [CustomAuthorize]
        public void InsuranceData()
        {
            var data = BulkInsertBLL.InsuranceData();
            //RecurringJob.AddOrUpdate(() => BulkInsertBLL.InsuranceData(), Cron.MinuteInterval(15));
            BackgroundJob.Schedule(() => InsuranceData(), DateTime.Now.AddMinutes(15));
        }
    }
}