﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewRecordlinc.App_Start;
using System.Data;
using DataAccessLayer.Common;
using DataAccessLayer.ColleaguesData;
namespace NewRecordlinc.Controllers
{
    public class FaqController : Controller
    {

        clsAdmin ObjAdmin = new clsAdmin();
        clsCommon ObjCommon = new clsCommon();
        //public ActionResult Index()
        //{
        //    if (SessionManagement.UserId != null && SessionManagement.UserId != "")
        //    {
        //        return RedirectToAction("Index", "Index");
        //    }
        //    else
        //    {
        //        return View();
        //    }
        //}

        //public ActionResult PrivacyPolicy()
        //{
        //    if (SessionManagement.UserId != null && SessionManagement.UserId != "")
        //    {
        //        return RedirectToAction("PrivacyPolicy", "Dashboard");
        //    }
        //    else if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
        //    {
        //        return RedirectToAction("PrivacyPolicy", "Dashboard");
        //    }

        //    else
        //    {
        //        DataTable dt = new DataTable();
        //        dt = ObjAdmin.GetAllCustomPageDetailsById(1);
        //        if (dt != null && dt.Rows.Count > 0)
        //        {
        //            ViewBag.PrivacyPolicy = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["PageDesc"]), string.Empty);
        //        }
        //        return PartialView("_PrivacyPolicy");
        //    }
        //}

        public ActionResult Index()
        {
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                return RedirectToAction("Index", "Index");
            }
            else
            {
                return View("FAQ");
            }
        }


        public ActionResult PrivacyPolicy()
        {
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                return RedirectToAction("PrivacyPolicy", "Dashboard");
            }
            else if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                return RedirectToAction("PrivacyPolicy", "Dashboard");
            }

            else
            {
                return View();
            }
        }



        //public ActionResult Termsconditions()
        //{
        //    if (SessionManagement.UserId != null && SessionManagement.UserId != "")
        //    {
        //        return RedirectToAction("Index", "Index");
        //    }
        //    else
        //    {
        //        DataTable dt = new DataTable();
        //        dt = ObjAdmin.GetAllCustomPageDetailsById(2);
        //        if (dt != null && dt.Rows.Count > 0)
        //        {
        //            ViewBag.TermsAndConditions = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["PageDesc"]), string.Empty);
        //        }
        //        return PartialView("_Termsconditions");
        //    }
        //}

        //public ActionResult HIPAA()
        //{
        //    if (SessionManagement.UserId != null && SessionManagement.UserId != "")
        //    {
        //        return RedirectToAction("Index", "Index");
        //    }
        //    else
        //    {
        //        return PartialView("_HIPAA");
        //    }
        //}
        
        //RM-414
        public ActionResult HIPAA()
        {
            return View();
        }

        //public ActionResult SiteMap()
        //{
        //    //-- 6211 - Commented session checking code becuse causing problem when redirected from public profile.
        //    //if (SessionManagement.UserId != null && SessionManagement.UserId != "")
        //    //{
        //    //    return RedirectToAction("Index", "Index");
        //    //}
        //    //else
        //    //{
        //    return PartialView("_SiteMap");
        //    //}
        //}

        //RM-414 Static page for Sitemap
        public ActionResult SiteMap()
        {
            return View();
        }

        public ActionResult TermsAndConditions()
        {
            return View();
        }

        public ActionResult HelpAndTraining()
        {
            return View();
        }

    }
}

