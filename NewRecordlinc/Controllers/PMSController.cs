﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogicLayer;
using BO.ViewModel;
using DataAccessLayer.Common;
using NewRecordlinc.App_Start;
namespace NewRecordlinc.Controllers
{
    public class PMSController : Controller
    {
        LoginBLL bllLogin;
        public PMSController()
        {
            bllLogin = new LoginBLL();

        }
        // GET: PMS
        [HttpPost]
        public ActionResult Login(string Token)
        {
            if ((Token ?? string.Empty).Trim().Length > 0)
            {
                IDictionary<bool, string> response = bllLogin.ValidateToken(Token); // TODO : Remove this
                if (response.Keys.FirstOrDefault())
                {
                    DoctorLoginForPMS login = bllLogin.GetUserCredentials(Token);
                    return RedirectToAction("Index", "User", new { UserName = login.Email, Password = new TripleDESCryptoHelper().decryptText(login.Password) });
                }
                else
                {
                    TempData["InActiveMsg"] = "Your Access Token is expired. Kindly re-login from PMS system to proceed further.";
                    TempData["IsValid"] = "InActive";
                    return RedirectToAction("Index", "Login");
                    //throw new HttpException(404, response.Values.FirstOrDefault());
                }
            }
            else
            {
                TempData["InActiveMsg"] = "Please suppy token to verify your identity.";
                TempData["IsValid"] = "InActive";
                return RedirectToAction("Index", "Login");
            }
        }

        [HttpPost]
        public ActionResult ReferPatient(string Token, PMSPatientDetails patient, PMSProviderDetails provider)
        {
            if ((Token ?? string.Empty).Trim().Length > 0)
            {
                SendReferralBLL bllSendReferral = new SendReferralBLL();

                DoctorLoginForPMS login = bllLogin.GetUserCredentials(Token);
                if (login != null)
                {
                    ValidationResponse response = bllSendReferral.ValidateInputsForPMSSendReferral(patient, provider);
                    if (response.Status)
                    {
                        new clsCompany().GetActiveCompany();

                        PMSSendReferralDetails obj = bllSendReferral.SendReferralFromPMS(login.UserId, patient, provider);
                        if (obj != null)
                        {
                            SessionManagement.PMSSendReferralDetails = obj;
                            return RedirectToAction("Index", "User", new { UserName = login.Email, Password = new TripleDESCryptoHelper().decryptText(login.Password) });
                        }
                        else
                        {
                            //throw new HttpException(404, "You did not configured your PMS correctly in RL.");
                            TempData["PMSConfigError"] = "PMS Configuration Issue";
                            // TempData["PMSReferralError"] = response.Message;
                            return RedirectToAction("Index", "User", new { UserName = login.Email, Password = new TripleDESCryptoHelper().decryptText(login.Password) });
                        }
                    }
                    else
                    {
                        TempData["PMSReferralError"] = "Automatic Send referral can not be processed due to incomplete or invalid details. Hence you can send referral manually by clicking on  \"Send Referral\" button.";
                       // TempData["PMSReferralError"] = response.Message;
                        return RedirectToAction("Index", "User", new { UserName = login.Email, Password = new TripleDESCryptoHelper().decryptText(login.Password) });
                    }
                }
                else
                {
                    TempData["InActiveMsg"] = "Your Access Token is expired. Kindly re-login from your PMS system to proceed further.";
                    TempData["IsValid"] = "InActive";
                    return RedirectToAction("Index", "Login");
                    // throw new HttpException(404, response.Values.FirstOrDefault());
                }


            }
            else
            {
                TempData["InActiveMsg"] = "Please suppy token to verify your identity.";
                TempData["IsValid"] = "InActive";
                return RedirectToAction("Index", "Login");
            }
        }


        public ActionResult DentrixButton()
        {
            return View();
        }
    }
}