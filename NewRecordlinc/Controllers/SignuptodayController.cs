﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewRecordlinc.Models.User;
using NewRecordlinc.App_Start;
using DataAccessLayer.Common;
using DataAccessLayer.ColleaguesData;
using System.Configuration;
using System.Data;


namespace NewRecordlinc.Controllers
{
    public class SignuptodayController : Controller
    {
        clsCompany ObjCompany = new clsCompany();
        public ActionResult Index()
        {
            ObjCompany.GetActiveCompany();
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                return RedirectToAction("Index", "Index");
            }
            else
            {
                return View();
            }
        }

    }
}
