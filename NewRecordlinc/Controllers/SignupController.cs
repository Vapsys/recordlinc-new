﻿using BO.ViewModel;
using DataAccessLayer.ColleaguesData;
using NewRecordlinc.App_Start;
using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BO.Models;
using DataAccessLayer.FlipTop;
using System.Web.Helpers;
using BusinessLogicLayer;

namespace NewRecordlinc.Controllers
{
    public class SignupController : Controller
    {
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        clsAdmin ObjAdmin = new clsAdmin();
        clsCompany objcompany = new clsCompany();
        clsTemplate ObjTemplate = new clsTemplate();
        clsCommon objCommon = new clsCommon();
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        // GET: Signup
        [HttpGet]
        public ActionResult Index()
        {
            objcompany.GetActiveCompany();
            MDLPatientSignUp ObjPatientSignUp = new MDLPatientSignUp();
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                return RedirectToAction("Index", "User");
            }
            if(Convert.ToString(TempData["DoctorSignup"]) == "True")
            {
                ViewBag.Roteatform = true;
            }
            if(!string.IsNullOrEmpty(Convert.ToString(TempData["Person"])))
            {
                ObjPatientSignUp = Matchfileds((Person)TempData["Person"]);
            }
            if(Convert.ToBoolean(TempData["IsSignUp"]) && string.IsNullOrEmpty(Convert.ToString(TempData["Message"])))
            {
                ViewBag.IsSignup = true;
                ViewBag.Message = "Sign-up Successfully.Please check your Email for activating your Account.";
            }
            else
            {
                ViewBag.IsSignup = false;
                ViewBag.Message = Convert.ToString(TempData["Message"]);
            }
            TempData["NewSignUp"] = "true";
            return View("NewIndex",ObjPatientSignUp);
        }
        [HttpPost]
        public ActionResult SignUpUser(MDLPatientSignUp ObjPatientSignUp)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    SignUpBLL Obj = new SignUpBLL();
                    IDictionary<bool, string> objm;
                    ObjPatientSignUp.CompanyWebSite = Convert.ToString(Session["CompanyWebsite"]);
                    DentistSignup Objs = new DentistSignup();
                    Objs.FirstName = ObjPatientSignUp.FirstName;
                    Objs.LastName = ObjPatientSignUp.LastName;
                    Objs.Phone = ObjPatientSignUp.Phone;
                    Objs.Password = ObjPatientSignUp.Password;
                    Objs.CompanyWebSite = ObjPatientSignUp.CompanyWebSite;
                    Objs.Email = ObjPatientSignUp.Email;
                    objm = Obj.SignuponRecordlinc(Objs);
                    TempData["IsSignUp"] = false;
                    TempData["Message"] = "";
                    TempData["IsSignUp"] = Convert.ToBoolean(objm.Keys.FirstOrDefault());
                    TempData["Message"] = Convert.ToString(objm.Values.FirstOrDefault());
                }
                return RedirectToAction("Index");

            }
            catch (Exception Ex)
            {

                throw;
            }
        }
        [HttpPost]
        public ActionResult GetSocialMediaPerson(string email)
        {
            try
            {
                string Email = string.Empty;
                if (!string.IsNullOrWhiteSpace(Request.QueryString["Email"]))
                {
                     Email = Convert.ToString(Request.QueryString["Email"]);
                }
                //connect to fliptop and pull the information down
                SocialMedia sm = new SocialMedia();
                Person person = sm.LookUpPersonClearBit(email);
                Person fullperson = sm.LookUpPersonfullcontact(email);
                person.FirstName = !string.IsNullOrEmpty(person.FirstName) ? person.FirstName : fullperson.FirstName;
                person.LastName = !string.IsNullOrEmpty(person.LastName) ? person.LastName : fullperson.LastName;
                person.FullName = !string.IsNullOrEmpty(person.FullName) ? person.FullName : fullperson.FullName;
                person.FacebookURL = !string.IsNullOrEmpty(person.FacebookURL) ? person.FacebookURL : fullperson.FacebookURL;
                person.LinkedInURL = !string.IsNullOrEmpty(person.LinkedInURL) ? person.LinkedInURL : fullperson.LinkedInURL;
                person.TwitterURL = !string.IsNullOrEmpty(person.TwitterURL) ? person.TwitterURL : fullperson.TwitterURL;
                person.ImageUrl = !string.IsNullOrEmpty(person.ImageUrl) ? person.ImageUrl : fullperson.ImageUrl;
                person.Description = !string.IsNullOrEmpty(fullperson.Description) ? fullperson.Description : person.Description;
                person.Email = email;
                TempData["Person"] = person;
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public MDLPatientSignUp Matchfileds(Person Obj)
        {
            try
            {
                MDLPatientSignUp newobj = new MDLPatientSignUp();
                newobj.FirstName = Obj.FirstName;
                newobj.LastName = Obj.LastName;
                newobj.Email = Obj.Email;
                return newobj;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("Signup-Controller", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        protected Person NewGetSocialMediaPerson(string email)
        {
            try
            {
                //connect to fliptop and pull the information down
                SocialMedia sm = new SocialMedia();
                Person person = sm.LookUpPersonClearBit(email);
                Person fullperson = sm.LookUpPersonfullcontact(email);
                person.FirstName = !string.IsNullOrEmpty(person.FirstName) ? person.FirstName : fullperson.FirstName;
                person.LastName = !string.IsNullOrEmpty(person.LastName) ? person.LastName : fullperson.LastName;
                person.FullName = !string.IsNullOrEmpty(person.FullName) ? person.FullName : fullperson.FullName;
                person.FacebookURL = !string.IsNullOrEmpty(person.FacebookURL) ? person.FacebookURL : fullperson.FacebookURL;
                person.LinkedInURL = !string.IsNullOrEmpty(person.LinkedInURL) ? person.LinkedInURL : fullperson.LinkedInURL;
                person.TwitterURL = !string.IsNullOrEmpty(person.TwitterURL) ? person.TwitterURL : fullperson.TwitterURL;
                person.ImageUrl = !string.IsNullOrEmpty(person.ImageUrl) ? person.ImageUrl : fullperson.ImageUrl;
                person.Description = !string.IsNullOrEmpty(fullperson.Description) ? fullperson.Description : person.Description;
                return person;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}