﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using NewRecordlinc.Models.Colleagues;
using System.Data;
using DataAccessLayer.ColleaguesData;
using System.Configuration;
using NewRecordlinc.Models.Admin;
using DataAccessLayer.Common;
using System.Text.RegularExpressions;
using NewRecordlinc.App_Start;
using System.IO;
using System.Web.Script.Serialization;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data.OleDb;
using System.Web.Helpers;
using NewRecordlinc.Models.Common;
using System.Drawing;
using Newtonsoft.Json;
using System.Net;
using System.Xml;
using DataAccessLayer.PatientsData;
using System.Threading;
using NewRecordlinc.Models;
using Excel;
using System.Globalization;
using DataAccessLayer.FlipTop;
using BO.Models;
using DataAccessLayer.ProfileData;
using BusinessLogicLayer;
using BO.ViewModel;
using BO.Enums;
using System.Data.SqlClient;
using DataAccessLayer.Admin;

namespace NewRecordlinc.Controllers
{
    public class AdminController : Controller
    {

        clsAdmin objAdmin = new clsAdmin();
        clsProfileData objProfileData = new clsProfileData();
        Member MdlMember = null;
        mdlColleagues MdlColleagues = null;
        mdlAdmin MdlAdmin = null;
        mdlCompany MdlCompany = null;
        mdlCommon MdlCommon = null;
        clsColleaguesData objColleaguesData = new clsColleaguesData();
        clsTemplate ObjTemplate = new clsTemplate();
        clsPatientsData ObjPatientsData = new clsPatientsData();
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        Membership_FeatureBLL ObjMemberBll = new Membership_FeatureBLL();
        clsCommon ObjCommon = new clsCommon();
       

        string ImageBankFolder = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("ImageBank"));
        string DefaultDoctorImage = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("DefaultDoctorImage"));
        string DoctorImage = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("DoctorImage"));
        int PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());
        string TempLoadFile = ConfigurationManager.AppSettings["TempUploadsFolderPath"].ToString();
        string UploadCompanyLogo = ConfigurationManager.AppSettings["UploadFromInviteCoulleague"];
        string FileNameNew = string.Empty;

        //when we create sign up need to put url here from web config that use for Active user
        public ActionResult Dashboard()
        {
            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        #region Active/InActive users
        [CustomAuthorize]
        public ActionResult ActiveUser()
        {
            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                ViewBag.tblScriptforActiveuser = ColleagueListOfDoctore(1, "", 1, null);
                Membership_FeatureBLL ObjMemberBll = new Membership_FeatureBLL();
                var lst = ObjMemberBll.GetMembershipPlan();
                List<SelectListItem> lstMembership = new List<SelectListItem>();
                foreach (var item in lst)
                {
                    SelectListItem li = new SelectListItem();
                    li.Text = item.Title;
                    li.Value = Convert.ToString(item.Membership_Id);
                    lstMembership.Add(li);
                }
                ViewBag.lstMembership = lstMembership;
                return View();
            }
            else
            {
                return RedirectToAction("Index", "User");
            }

        }
        [CustomAuthorize]
        public ActionResult InActiveUsers()
        {
            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                ViewBag.tblScriptforInActiveuser = ColleagueListOfDoctore(1, "", 2, null);
                return View();
            }
            else
            {
                return RedirectToAction("Index", "User");
            }

        }

        public string ColleagueListOfDoctoreContent(int PageIndex, string searchtext, int status, string SortByValue)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sbPageing = new StringBuilder();

            int size = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

            objAdmin = new clsAdmin();
            DataTable dt = new DataTable();
            dt = objAdmin.GetActiveInActiveUserList(PageIndex, size, searchtext, status, SortByValue);

            if (dt.Rows.Count > 0)
            {
                double TotalPages = Math.Ceiling(Convert.ToDouble(dt.Rows[0]["TotalCount"].ToString()) / size);
                foreach (DataRow item in dt.Rows)
                {
                    string ImageName = string.Empty;
                    string strLastLoggedInDate = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(item["LoginTime"])))
                    {
                        strLastLoggedInDate = new CommonBLL().ConvertToDate(Convert.ToDateTime(item["LoginTime"]), (int)BO.Enums.Common.DateFormat.GENERAL);
                    }
                    else
                    {
                        strLastLoggedInDate = new CommonBLL().ConvertToDate(Convert.ToDateTime(item["UpdatedDate"]), (int)BO.Enums.Common.DateFormat.GENERAL);
                    }
                    string strUpdatedDate = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(item["UpdatedDate"])))
                    {
                        strUpdatedDate = new CommonBLL().ConvertToDate(Convert.ToDateTime(item["UpdatedDate"]), (int)BO.Enums.Common.DateFormat.GENERAL);
                    }
                    if (item["ImageName"].ToString() == null || item["ImageName"].ToString() == "")
                    {
                        ImageName = DefaultDoctorImage;
                    }
                    else
                    {
                        ImageName = DoctorImage + item["ImageName"].ToString();
                    }
                    sb.Append("<div class=\"trow\" id=\"row" + item["ColleagueId"] + "\">");
                    sb.Append("<div class=\"dataTableRow\">");
                    if (status == 1)
                    {
                        sb.Append("<input type=\"checkbox\" value=\"" + item["ColleagueId"] + "\" name=\"checkcolleague\" id=\"checkcolleague\">");
                    }
                    sb.Append("<figure class=\"thumb\"><img style=\"height:75px;width:75px;\" alt=\"" + (item["FirstName"].ToString() != string.Empty && item["FirstName"].ToString() != null ? item["FirstName"].ToString() + ", " : string.Empty) + (item["LastName"].ToString() != string.Empty && item["LastName"].ToString() != null ? item["LastName"].ToString() + ", " : string.Empty) + (item["OfficeName"].ToString() != string.Empty && item["OfficeName"].ToString() != null ? item["OfficeName"].ToString() + ", " : string.Empty) + (item["City"].ToString() != string.Empty && item["City"].ToString() != null ? item["City"].ToString() + ", " : string.Empty) + (item["State"].ToString() != string.Empty && item["State"].ToString() != null ? item["State"].ToString() : string.Empty) + (item["ZipCode"].ToString() != string.Empty && item["ZipCode"].ToString() != null ? " and " + item["ZipCode"].ToString() : string.Empty) + "\" src=\"" + ImageName + "\"></figure>");
                    sb.Append("<div class=\"dataTableCell cell1\">");
                    sb.Append("<div class=\"data\">");
                    sb.Append("<h3 onclick=\"location.href='" + Url.Action("ViewProfile", "Admin", new { UserId = item["ColleagueId"].ToString() }) + "'\" style='cursor:pointer'>" + item["FirstName"] + " " + item["LastName"] + "");

                    if (!string.IsNullOrEmpty(item["Title"].ToString()))
                    {
                        sb.Append("," + item["Title"].ToString());
                    }
                    sb.Append("<span onclick=\"location.href='" + Url.Action("ViewProfile", "Admin", new { UserId = item["ColleagueId"].ToString() }) + "'\" style='cursor:pointer'>" + item["OfficeName"].ToString() + "</span></h3>");

                    sb.Append("<span>" + item["Specialities"].ToString() + "</span>");

                    sb.Append("<br/><span>Last Logged In:<br> " + strLastLoggedInDate + "<br></span>");
                    sb.Append("<span>Last Updated Profile:<br>" + strUpdatedDate + " </span>");
                    sb.Append("</div>");
                    sb.Append("</div>");
                    sb.Append("<div class=\"dataTableInner\">");
                    sb.Append("<div class=\"dataTableCell cell2\">");
                    sb.Append("<ul class=\"list list_ab\">");

                    if (!string.IsNullOrEmpty(item["Phone"].ToString()))
                    {
                        sb.Append("<li><i class=\"wphone\"></i>" + item["Phone"].ToString() + "</li>");
                    }

                    if (!string.IsNullOrEmpty(item["fax"].ToString()))
                    {
                        sb.Append("<li><i class=\"fax\"></i>" + item["fax"].ToString() + "</li>");
                    }
                    sb.Append("<li>");
                    if (!string.IsNullOrEmpty(item["ExactAddress"].ToString()))
                    {
                        sb.Append("<i class=\"address\"></i>");
                        sb.Append(item["ExactAddress"].ToString());
                    }
                    if (!string.IsNullOrEmpty(item["Address2"].ToString()))
                    {
                        sb.Append(" " + item["Address2"].ToString());
                    }
                    if (!string.IsNullOrEmpty(item["City"].ToString()))
                    {
                        sb.Append(" " + item["City"].ToString());
                    }
                    if (!string.IsNullOrEmpty(item["State"].ToString()))
                    {
                        sb.Append(", " + item["State"].ToString());
                    }

                    if (!string.IsNullOrEmpty(item["ZipCode"].ToString()))
                    {
                        sb.Append(" " + item["ZipCode"].ToString());
                    }

                    sb.Append("</li>");

                    if (!string.IsNullOrEmpty(item["EmailAddress"].ToString()))
                    {
                        sb.Append("<li><i class=\"email\"></i>" + (ObjCommon.CheckNull(Convert.ToString(item["SecondaryEmail"]), string.Empty) != string.Empty ? (item["EmailAddress"].ToString() + ", " + item["SecondaryEmail"].ToString()) : item["EmailAddress"].ToString()) + "</li>");
                    }
                    sb.Append("</ul> ");
                    sb.Append("</div>");
                    sb.Append("<div class=\"dataTableCell cell3\">");
                    sb.Append("<ul class=\"list\">");
                    sb.Append("<li>Referrals Sent: (" + item["ReferralSentCount"].ToString() + ")</li>");
                    sb.Append("<li>Referrals Received: (" + item["ReferralReceivedCount"].ToString() + ")</li>");
                    sb.Append("<li>Colleagues: (" + item["TotalColleagueCount"].ToString() + ")</li>");
                    sb.Append("</ul>");
                    sb.Append("</div>");
                    sb.Append("<div class=\"dataTableCell cell4\">");
                    sb.Append("<div class=\"button_panel\">");
                    if (status == 1)
                    {
                        sb.Append("<input type=\"submit\"  name=\"Send Email\" class=\"send_email\"  onclick=\"location.href='" + Url.Action("SendEmail", "Admin", new { UserId = item["ColleagueId"].ToString() }) + "'\">");
                        sb.Append("<input type=\"submit\" name=\"Send Password\" class=\"send_password_2\" onclick=\"ResetPasswordOfDoctor('" + item["ColleagueId"].ToString() + "')\">");
                        sb.Append("<input class=\"email_history\" name=\"Email History\" onclick=\"location.href='" + Url.Action("EmailHistory", "Admin", new { UserId = item["ColleagueId"].ToString() }) + "'\" type=\"submit\" />");
                        sb.Append("<input type=\"submit\" name=\"Edit Btn\" class=\"edit_btn\" onclick=\"location.href='" + Url.Action("ViewProfile", "Admin", new { UserId = item["ColleagueId"].ToString() }) + "'\">");
                        sb.Append("<input type=\"button\" class=\"add_dentist\" value=\"Add Dentist\" name=\"Add Dentist\" onclick=\"AddColleagues(" + item["ColleagueId"] + ")\">");
                    }
                    if (status == 2)
                    {
                        sb.Append("<input class=\"active_btn\" type=\"submit\" name=\"Active\" onclick=\"ActiveUser('" + item["ColleagueId"].ToString() + "','1','" + PageIndex + "')\">");
                    }

                    sb.Append("</div>");
                    sb.Append("</div>");
                    sb.Append("</div>");
                    sb.Append("</div>");
                    sb.Append("</div>");
                }
            }
            else if (PageIndex > 1)
            {
                sb.Append("<div class=\"trow\"><center>No more record found</center></div>");
            }
            else
            {
                sb.Append("<div class=\"trow\"><center>No record found</center></div>");
            }
            return sb.ToString();
        }


        public string ColleagueListOfDoctore(int PageIndex, string searchtext, int status, string SortByValue)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sbPageing = new StringBuilder();

            int size = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());


            objAdmin = new clsAdmin();
            DataTable dt = new DataTable();
            dt = objAdmin.GetActiveInActiveUserList(PageIndex, size, searchtext, status, SortByValue);

            if (dt.Rows.Count > 0)
            {
                double TotalPages = Math.Ceiling(Convert.ToDouble(dt.Rows[0]["TotalCount"].ToString()) / size);

                sb.Append("<div class=\"table_data colleagues\"  style=\"word-wrap:break-word;\">");

                if (status == 1)
                {

                    sb.Append("<div class=\"heading_panel\">");
                    sb.Append("<span>Sort By</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp<select id=\"GetaActiveUserlist\" style=\"height:25px;padding-top:0px;padding-bottom:0px;\"><option value=\"\">FirstName LastName</option><option value=\"CityAsc\" " + (SortByValue == "CityAsc" ? "selected" : "") + " >City Asc</option><option value=\"CityDesc\" " + (SortByValue == "CityDesc" ? "selected" : "") + ">City Desc</option><option value=\"StateAsc\" " + (SortByValue == "StateAsc" ? "selected" : "") + ">State Asc</option><option value=\"StateDesc\" " + (SortByValue == "StateDesc" ? "selected" : "") + ">State Desc</option><option value=\"ZipAsc\" " + (SortByValue == "ZipAsc" ? "selected" : "") + ">Zip Asc</option><option value=\"ZipDesc\" " + (SortByValue == "ZipDesc" ? "selected" : "") + ">Zip Desc</option><option value=\"ProfileUpdatedAsc\" " + (SortByValue == "ProfileUpdatedAsc" ? "selected" : "") + ">Profile updated Asc</option><option value=\"ProfileUpdatedDesc\" " + (SortByValue == "ProfileUpdatedDesc" ? "selected" : "") + ">Profile updated Desc</option> <option value=\"LastloginAsc\" " + (SortByValue == "LastloginAsc" ? "selected" : "") + ">Last Logged in Asc</option> <option value=\"LastloginDesc\" " + (SortByValue == "LastloginDesc" ? "selected" : "") + ">Last Logged in Desc</option></select> ");
                    // sb.Append("<input class=\"sort_email\" name=\"Sort Email\" onclick=\"ActionToSelected('sendemail')\" type=\"submit\" />&nbsp;");
                    sb.Append("<input class=\"block\" name=\"Block\" type=\"submit\" onclick=\"ActionToSelected('block','" + PageIndex + "')\" />&nbsp;");
                    //-- Commented following because as per chirag, it is outdated feature.
                    // sb.Append("<input class=\"merge_users\" name=\"Merge Users\" type=\"submit\" onclick=\"return megerclick()\" />&nbsp;");
                    sb.Append("<input class=\"remove\" name=\"Remove\" type=\"submit\" onclick=\"ActionToSelected('remove','" + PageIndex + "')\" />");
                    sb.Append("</div>");
                }
                sb.Append("<div class=\"clear\"></div>");

                sb.Append("<div class=\"tbody\" id=\"userlsittbody\" data-sortbyvalue=\"" + SortByValue + "\">");

                foreach (DataRow item in dt.Rows)
                {
                    string strLastLoggedInDate = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(item["LoginTime"])))
                    {
                        strLastLoggedInDate = new CommonBLL().ConvertToDate(Convert.ToDateTime(item["LoginTime"]), (int)BO.Enums.Common.DateFormat.GENERAL);
                    }
                    else
                    {
                        strLastLoggedInDate = new CommonBLL().ConvertToDate(Convert.ToDateTime(item["UpdatedDate"]), (int)BO.Enums.Common.DateFormat.GENERAL);
                    }
                    string strUpdatedDate = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(item["ProfilesLastUpdated"])))
                    {
                        strUpdatedDate = new CommonBLL().ConvertToDate(Convert.ToDateTime(item["ProfilesLastUpdated"]), (int)BO.Enums.Common.DateFormat.GENERAL);
                    }


                    string ImageName = string.Empty;
                    if (item["ImageName"].ToString() == null || item["ImageName"].ToString() == "")
                    {
                        ImageName = DefaultDoctorImage;
                    }
                    else
                    {
                        ImageName = DoctorImage + item["ImageName"].ToString();
                    }
                    sb.Append("<div class=\"trow\" id=\"row" + item["ColleagueId"] + "\" >");
                    sb.Append("<div class=\"dataTableRow\">");
                    if (status == 1)
                    {
                        sb.Append("<input type=\"checkbox\" value=\"" + item["ColleagueId"] + "\" name=\"checkcolleague\" id=\"checkcolleague\">");
                    }
                    sb.Append("<figure class=\"thumb\"><img style=\"height:75px;width:75px;\" alt=\"" + (item["FirstName"].ToString() != string.Empty && item["FirstName"].ToString() != null ? item["FirstName"].ToString() + ", " : string.Empty) + (item["LastName"].ToString() != string.Empty && item["LastName"].ToString() != null ? item["LastName"].ToString() + ", " : string.Empty) + (item["OfficeName"].ToString() != string.Empty && item["OfficeName"].ToString() != null ? item["OfficeName"].ToString() + ", " : string.Empty) + (item["City"].ToString() != string.Empty && item["City"].ToString() != null ? item["City"].ToString() + ", " : string.Empty) + (item["State"].ToString() != string.Empty && item["State"].ToString() != null ? item["State"].ToString() : string.Empty) + (item["ZipCode"].ToString() != string.Empty && item["ZipCode"].ToString() != null ? " and " + item["ZipCode"].ToString() : string.Empty) + "\" src=\"" + ImageName + "\"></figure>");
                    sb.Append("<div class=\"dataTableCell cell1\">");
                    sb.Append("<div class=\"data\">");
                    sb.Append("<h3 onclick=\"location.href='" + Url.Action("ViewProfile", "Admin", new { UserId = item["ColleagueId"].ToString() }) + "'\" style='cursor:pointer'>" + item["FirstName"] + " " + item["LastName"] + "");

                    if (!string.IsNullOrEmpty(item["Title"].ToString()))
                    {
                        sb.Append("," + item["Title"].ToString());
                    }
                    sb.Append("<span onclick=\"location.href='" + Url.Action("ViewProfile", "Admin", new { UserId = item["ColleagueId"].ToString() }) + "'\" style='cursor:pointer'>" + item["OfficeName"].ToString() + "</span></h3>");
                    sb.Append("<span>" + item["Specialities"].ToString() + "</span>");


                    sb.Append("<br/><span>Last Logged In:<br> " + strLastLoggedInDate + "<br></span>");
                    sb.Append("<span>Last Updated Profile:<br>" + strUpdatedDate + " </span>");
                    sb.Append("</div>");
                    sb.Append("</div>");
                    sb.Append("<div class=\"dataTableInner\">");
                    sb.Append("<div class=\"dataTableCell cell2\">");
                    sb.Append("<ul class=\"list list_ab\">");

                    if (!string.IsNullOrEmpty(item["Phone"].ToString()))
                    {
                        sb.Append("<li><i class=\"wphone\"></i>" + item["Phone"].ToString() + "</li>");
                    }

                    if (!string.IsNullOrEmpty(item["fax"].ToString()))
                    {
                        sb.Append("<li><i class=\"fax\"></i>" + item["fax"].ToString() + "</li>");
                    }
                    sb.Append("<li>");

                    if (!string.IsNullOrEmpty(item["ExactAddress"].ToString()))
                    {
                        sb.Append("<i class=\"address\"></i>");
                        sb.Append(item["ExactAddress"].ToString());
                    }
                    if (!string.IsNullOrEmpty(item["Address2"].ToString()))
                    {


                        sb.Append(" " + item["Address2"].ToString());
                    }
                    if (!string.IsNullOrEmpty(item["City"].ToString()))
                    {
                        sb.Append(" " + item["City"].ToString());
                    }
                    if (!string.IsNullOrEmpty(item["State"].ToString()))
                    {
                        sb.Append(", " + item["State"].ToString());
                    }

                    if (!string.IsNullOrEmpty(item["ZipCode"].ToString()))
                    {
                        sb.Append(" " + item["ZipCode"].ToString());
                    }

                    sb.Append("</li>");

                    if (!string.IsNullOrEmpty(item["EmailAddress"].ToString()))
                    {

                        sb.Append("<li><i class=\"email\"></i>" + (ObjCommon.CheckNull(Convert.ToString(item["SecondaryEmail"]), string.Empty) != string.Empty ? (item["EmailAddress"].ToString() + ", " + item["SecondaryEmail"].ToString()) : item["EmailAddress"].ToString()) + "</li>");
                    }

                    sb.Append("</ul> ");
                    sb.Append("</div>");
                    sb.Append("<div class=\"dataTableCell cell3\">");
                    sb.Append("<ul class=\"list\">");
                    sb.Append("<li>Referrals Sent: (" + item["ReferralSentCount"].ToString() + ")</li>");
                    sb.Append("<li>Referrals Received: (" + item["ReferralReceivedCount"].ToString() + ")</li>");
                    sb.Append("<li>Colleagues: (" + item["TotalColleagueCount"].ToString() + ")</li>");
                    sb.Append("</ul>");
                    sb.Append("</div>");
                    sb.Append("<div class=\"dataTableCell cell4\">");
                    sb.Append("<div class=\"button_panel\">");
                    if (status == 1)
                    {
                        //sb.Append("<input type=\"submit\"  name=\"Send Email\" class=\"send_email\"  onclick=\"location.href='" + Url.Action("SendEmail", "Admin", new { UserId = item["ColleagueId"].ToString() }) + "'\">");
                        sb.Append("<input type=\"submit\" name=\"Send Password\" class=\"send_password_2\" onclick=\"ResetPasswordOfDoctor('" + item["ColleagueId"].ToString() + "')\">");
                        // sb.Append("<input class=\"email_history\" name=\"Email History\" onclick=\"location.href='" + Url.Action("EmailHistory", "Admin", new { UserId = item["ColleagueId"].ToString() }) + "'\" type=\"submit\" />");
                        sb.Append("<input type=\"submit\" name=\"Edit Btn\" class=\"edit_btn\" onclick=\"location.href='" + Url.Action("ViewProfile", "Admin", new { UserId = item["ColleagueId"].ToString() }) + "'\">");
                        sb.Append("<input type=\"button\" class=\"add_dentist\" value=\"Add Dentist\" name=\"Add Dentist\" onclick=\"AddColleagues(" + item["ColleagueId"] + ")\">");
                        if (item["PaymentMode"] != null)
                        {
                            var str = Convert.ToString(item["PaymentMode"]);
                            if (str == "True" || str == "False")
                            {
                                sb.Append("<a class=\"btnMember\" id=\"ChangeMembership\" name=\"ChangeMembership\" onclick=\"ChangeUserMembership(" + item["ColleagueId"] + "," + 0 + ")\"><img src=\"../Images/Change-Membership.png\" /></a>");
                            }
                            else
                            {
                                sb.Append("<a class=\"btnMemberPay\" id=\"makePayment\" name=\"makePayment\" onclick=\"ChangeUserMembership(" + item["ColleagueId"] + "," + 1 + ")\"><img src=\"../Images/Make-Payment.png\" /></a>");
                            }
                        }
                        else
                        {
                            sb.Append("<a class=\"btnMemberPay\" id=\"makePayment\" name=\"makePayment\" onclick=\"ChangeUserMembership(" + item["ColleagueId"] + ","+ 1 +")\"><img src=\"../Images/Make-Payment.png\" /></a>");
                        }


                        if (item["ColleagueId"].ToString() == "55")
                        {
                            sb.Append("<input type=\"button\" class=\"sync_Details\" value=\"SyncPatientDetails\" name=\"Sync PatinetDetials of Dactor\" onclick=\"syncDoctorDetails(" + item["ColleagueId"] + ")\">");
                        }
                    }
                    if (status == 2)
                    {
                        sb.Append("<input class=\"active_btn\" type=\"submit\" name=\"Active\" onclick=\"ActiveUser('" + item["ColleagueId"].ToString() + "','1','" + PageIndex + "')\">");
                    }

                    sb.Append("</div>");
                    sb.Append("</div>");
                    sb.Append("</div>");
                    sb.Append("</div>");
                    sb.Append("</div>");
                }
                sb.Append("</div></div>");
                sb.Append("<br/>");
            }
            else
            {
                sb.Append("<center>No record found</center>");
            }
            return sb.ToString();
        }

        public JsonResult GetColleagueLsitOfDoctore(string index, string searchtext, string status, string sortbyvalue, string City, string State, string ZipCode, string LastLoginFrom, string LastLoginTo, string ProfileUpdateFrom, string ProfileUpdateTo)
        {
            return Json(ColleagueListOfDoctore(Convert.ToInt32(index), searchtext, Convert.ToInt32(status), sortbyvalue), JsonRequestBehavior.AllowGet);
        }


        public string SearchColleaguesResult(int DoctorId, int PageIndex, string SearchText, string OnScroll)
        {
            StringBuilder sb = new StringBuilder();
            MdlColleagues = new mdlColleagues();
            MdlColleagues.lstSearchColleagueForDoctor = MdlColleagues.GetSearchColleagueForDoctor(DoctorId, PageIndex, 16, null, null, null, null, null, null, null, null, SearchText);
            if (!string.IsNullOrEmpty(OnScroll))
            {

                if (MdlColleagues.lstSearchColleagueForDoctor.Count > 0)
                {
                    foreach (var item in MdlColleagues.lstSearchColleagueForDoctor)
                    {

                        sb.Append("<li><div class=\"dyheight\">");
                        sb.Append("<span class=\"check\"><input type=\"checkbox\" id=\"checkcolleague\" name=\"\" class=\"checkbox_add_patient\" value=\"" + item.UserId + "\"></span>");
                        sb.Append("<span class=\"thumb\"><a  href=\"Javascript:;\" onclick=\"ViewProfile('" + item.UserId + "')\" ><img alt=\"\" style=\"height: 95px; width: 85px;\" onclick=\"ViewProfile('" + item.UserId + "')\" src='" + item.ImageName + "'></a></span>");
                        sb.Append("<span class=\"description\">");
                        sb.Append("<h2><a class=\"hoverremoveclass\" href=\"Javascript:;\" style=\"word-wrap: break-word;\" onclick=\"ViewProfile('" + item.UserId + "')\">" + item.FirstName + " &nbsp;" + item.LastName + "<span class=\"sub_heading\">" + (!string.IsNullOrEmpty(item.OfficeName) ? item.OfficeName : "&nbsp;") + "</span></a></h2>");
                        sb.Append("<ul class=\"list\">");
                        sb.Append("<li>" + item.Specialities + "</li>  ");
                        sb.Append("</ul></span><span class=\"clear\"></span></div></li>");
                    }
                }
                else
                {
                    sb.Append("<center>No more colleague found.</center>");
                }
                return sb.ToString();
            }

            sb.Append("<h2 class=\"title\">Add Doctor</h2>");
            sb.Append("<div class=\"search_main_div\"><input style=\"display:none\" id=\"SearchBtnHide\" class=\"search_btn_popup\" name=\"Cancel\" type=\"button\" onclick=\"SearchColleague()\" value=\"Cancel\"><div class=\"patient_searchbar\"><input id='txtSearchDoctor' onfocus=\"if (this.value =='Search Doctor') {this.value = '';}\" onblur=\"if (this.value == '') {this.value = 'Search Doctor';}\" type=\"text\" value=\"Search Doctor\" name=\"Search Doctor\" >");
            sb.Append("</div></div><div class=\"cntnt\">");



            if (MdlColleagues.lstSearchColleagueForDoctor.Count > 0)
            {
                sb.Append("<ul class=\"search_listing\" style=\"width:100%;\">");
                foreach (var item in MdlColleagues.lstSearchColleagueForDoctor)
                {

                    sb.Append("<li><div class=\"dyheight\">");
                    sb.Append("<span class=\"check\"><input type=\"checkbox\" id=\"checkcolleagueToAdd\" name=\"\" class=\"checkbox_add_patient\" value=\"" + item.UserId + "\"></span>");
                    sb.Append("<span class=\"thumb\"><a  href=\"/Admin/ViewProfile?UserId=" + item.UserId + "\" ><img alt=\"\" style=\"height: 95px; width: 85px;\" onclick=\"ViewProfile('" + item.UserId + "')\" src='" + item.ImageName + "'></a></span>");
                    sb.Append("<span class=\"description\">");
                    sb.Append("<h2><a href=\"/Admin/ViewProfile?UserId=" + item.UserId + "\" style=\"word-wrap: break-word;\" >" + item.FirstName + " &nbsp;" + item.LastName + "<span class=\"sub_heading\">" + (!string.IsNullOrEmpty(item.OfficeName) ? item.OfficeName : "&nbsp;") + "</span></a></h2>");
                    sb.Append("<ul class=\"list\">");

                    sb.Append("<li>" + item.Specialities + "</li>  ");



                    sb.Append("</ul></span><span class=\"clear\"></span></div></li>");
                }

                sb.Append("</ul>");
                sb.Append("<div class=\"clear\"></div>");
                sb.Append("<div class=\"actions\">   ");
                sb.Append("<a href=\"JavaScript:;\" onclick=\" AddColleagueOfDoctor()\" ><img alt=\"\" src=\"../../Content/images/btn-next1.png\"/></a>");
                sb.Append("<div class=\"clear\"></div></div></div>");
            }
            else
            {

                sb.Append(" <center>Colleague not found. Please <a href=\"" + Url.Action("ActiveUser", "Admin") + "\" style=\"color: #0890c4;\"> click here</a> to search and add colleague.</center>");
            }

            sb.Append("</div><button class=\"mfp-close\" title=\"Close (Esc)\" type=\"button\">×</button>");
            return sb.ToString();
        }

        public string AddAsColleague(int DoctorId, string ColleagueId)
        {

            MdlColleagues = new mdlColleagues();
            ObjTemplate = new clsTemplate();
            bool status = true;
            string[] ColgId = ColleagueId.Split(',');
            foreach (var item in ColgId)
            {
                if (!string.IsNullOrEmpty(item.ToString()))
                {
                    status = MdlColleagues.AddAsColleague(Convert.ToInt32(DoctorId), Convert.ToInt32(item));
                }

            }

            string strReturnResult = "";
            if (status)
            {
                strReturnResult = "1";
                //Send Template to colleague
                // ObjTemplate.ColleagueInviteDoctor(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(ColleagueId), "");

            }
            else
            {
                strReturnResult = "0";
            }
            return strReturnResult;

        }

        [HttpPost]
        public JsonResult UpdateUserStatus(string UserId, int Status)
        {
            object obj = string.Empty;
            try
            {
                string[] arryUserId = UserId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var item in arryUserId)
                {
                    bool result = objAdmin.UpdateUserStatus(Convert.ToInt32(item), Status);
                    string email = string.Empty;
                    string fullname = string.Empty;
                    DataTable dt = objColleaguesData.GetDoctorDetailsbyId(Convert.ToInt32(item));
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        email = Convert.ToString(dt.Rows[0]["Username"]);
                        fullname = Convert.ToString(dt.Rows[0]["FirstName"] + " " + dt.Rows[0]["LastName"]);
                        // Remove email notification to user RM-871
                        // ObjTemplate.SendBlockDoctorEmail(email, fullname);
                    }
                    if (result)
                    {
                        obj = "1";
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ResetPasswordOfDoctor(int UserId)
        {
            object obj = string.Empty;
            try
            {
                string Password = ObjCommon.CreateRandomPassword(8);
                string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);

                // bool Result = false;

                //  Result = objAdmin.UpdateUserPassword(UserId, EncPassword);
                //  if (Result)
                //  {
                ObjTemplate.ResetPasswordOfDoctor(UserId, Password, EncPassword);//send template to doctor
                obj = "1";
                // }

            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult ActivateUser(int UserId, string Status)
        {
            object obj = string.Empty;
            try
            {
                string username = string.Empty;
                string Password = string.Empty;
                string Email = string.Empty;
                string EncPassword = string.Empty;

                DataSet ds = new DataSet();
                ds = objColleaguesData.GetDoctorDetailsById(UserId);
                if (ds != null && ds.Tables.Count > 0)
                {
                    username = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Username"]), string.Empty);
                    Password = Convert.ToString(ObjTripleDESCryptoHelper.decryptText(ds.Tables[0].Rows[0]["Password"].ToString()));
                    EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                    Email = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Username"]), string.Empty);

                    bool result = objAdmin.UpdateUserStatus(UserId, Convert.ToInt32(Status));// here we update status 4 because if we update 1 then show in active list but as per funcationality we show that user when login using sendded template link
                    ObjTemplate.NewUserEmailFormat(Email, Session["CompanyWebsite"].ToString() + "/User/Index?UserName=" + Email + "&Password=" + EncPassword, Password, Session["CompanyWebsite"].ToString());
                    obj = "1";
                }

            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }



        public string syncPatinetDetailsOfDoctorWithAPI(int DoctorId)
        {
            try
            {
                ObjPatientsData = new clsPatientsData();
                DataTable dt = ObjPatientsData.GetPatientDetialsForDoctor(DoctorId);
                GetDoctorDetails objGetDoctorDetails = new GetDoctorDetails();
                List<GetDoctorDetails> lst = new List<GetDoctorDetails>();
                lst = (from c in dt.AsEnumerable()
                       select new GetDoctorDetails
                       {
                           name = c["FirstName"].ToString() + " " + c["LastName"].ToString(),
                           emailId = c["Email"].ToString(),
                           phone = c["phone"].ToString(),
                           smsEnabled = 0
                       }).ToList();

                if (lst.Count > 0)
                {

                    foreach (var item in lst)
                    {
                        if (!string.IsNullOrEmpty(item.emailId))
                        {
                            if (!string.IsNullOrEmpty(item.phone))
                            {
                                item.phone = item.phone.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "");

                                if (item.phone.Length >= 10)
                                {
                                    item.phone = item.phone.Substring(item.phone.Length - Math.Min(10, item.phone.Length));

                                    string s1 = item.phone.Substring(0, 3);
                                    string s2 = item.phone.Substring(3, 3);
                                    string s3 = item.phone.Substring(6, 4);

                                    item.phone = s1 + "-" + s2 + "-" + s3;
                                }
                                else
                                {
                                    item.phone = "";
                                }

                            }


                            string JsoanData = JsonConvert.SerializeObject(item);
                            string url = "http://api.birdeye.com:8080/resources/v1/customer/checkin?bid=355874320&api_key=r797k827I829u69dqy2Q8933IJfQJdiI";

                            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                            httpWebRequest.ContentType = "application/json; charset=utf-8";
                            httpWebRequest.Method = "POST";

                            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                            {

                                streamWriter.Write(JsoanData);
                                streamWriter.Flush();
                                streamWriter.Close();
                            }

                            String post_response;
                            HttpWebResponse objResponse = (HttpWebResponse)httpWebRequest.GetResponse() as HttpWebResponse; ;
                            using (Stream responseStream = objResponse.GetResponseStream())
                            {
                                StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                                post_response = reader.ReadToEnd();

                            }
                        }
                    }

                    return "Contact succesfully synchronized";
                }
                else
                {
                    return "Patient Not Found.";
                }
            }

            catch (WebException ex)
            {

                WebResponse errorResponse = ex.Response;
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(errorResponse.GetResponseStream());
                XmlNodeList elemList = xmlDoc.GetElementsByTagName("message");
                return elemList[0].InnerXml;

            }

        }




        #endregion

        #region BlockUsers
        [CustomAuthorize]
        public ActionResult BlockUsers()
        {
            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                ViewBag.GetAllBlockUsers = GetListOfBlockUser(1, 2, 2, null);
                return View();
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        [HttpPost]
        public JsonResult GetBlockUsers(string PageIndex, string SortColumn, string SortDirection, string Searchtext)
        {
            return Json(GetListOfBlockUser(Convert.ToInt32(PageIndex), Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection), Searchtext), JsonRequestBehavior.AllowGet);
        }

        public string GetListOfBlockUser(int PageIndex, int SortColumn, int SortDirection, string Searchtext)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                objAdmin = new clsAdmin();
                DataTable dt = new DataTable();
                dt = objAdmin.GetAllBlockUsers(PageIndex, PageSize, SortColumn, SortDirection, Searchtext);

                if (dt.Rows.Count > 0)
                {
                    double TotalPages = Math.Ceiling(Convert.ToDouble(dt.Rows[0]["TotalRecord"].ToString()) / PageSize);
                    sb.Append("<div class=\"table_data reports_ref_received clearfix\">");
                    sb.Append("<div class=\"clear\"></div>");

                    sb.Append("<table class=\"signupTable\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"tblBlockUser\" data-sortcolumn=\"" + SortColumn + "\" data-sortirection=\"" + SortDirection + "\">");
                    sb.Append("<thead>");
                    sb.Append("<tr> ");
                    sb.Append("<th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetAllBlockUsers('" + PageIndex + "','" + 1 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\">Email</a></th>");
                    sb.Append("<th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetAllBlockUsers('" + PageIndex + "','" + 2 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\">date</a></th>");
                    sb.Append("<th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetAllBlockUsers('" + PageIndex + "','" + 3 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\">Phone</a></th>");
                    sb.Append("<th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetAllBlockUsers('" + PageIndex + "','" + 2 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\">Action</a></th>");
                    sb.Append("</tr>");
                    sb.Append("</thead>");
                    sb.Append("<tbody id=\"tblbodycontent\">");

                    foreach (DataRow item in dt.Rows)
                    {
                        string strBlockedDate = string.Empty;
                        if (!string.IsNullOrEmpty(Convert.ToString(item["Date"])))
                        {
                            strBlockedDate = new CommonBLL().ConvertToDateTime(Convert.ToDateTime(item["Date"]), (int)BO.Enums.Common.DateFormat.GENERAL);
                        }

                        sb.Append("<tr id=\"row" + item["UserId"] + "\">");
                        sb.Append("<td><h5>" + ObjCommon.CheckNull(Convert.ToString(item["FirstName"]), string.Empty) + "&nbsp;" + ObjCommon.CheckNull(Convert.ToString(item["LastName"]), string.Empty) + "</h5><p>" + ObjCommon.CheckNull(Convert.ToString(item["Username"]), string.Empty) + "</p></td>");
                        sb.Append(" <td>" + strBlockedDate + "</td>");
                        sb.Append("<td>" + ObjCommon.CheckNull(Convert.ToString(item["Phone"]), string.Empty) + "</td>");
                        sb.Append("<td><input class=\"active_btn_small\" name=\"Active\" type=\"submit\" onclick=\"ActiveBlockUser('" + ObjCommon.CheckNull(Convert.ToString(item["UserId"]), string.Empty) + "','" + ObjCommon.CheckNull(Convert.ToString(item["FirstName"]), string.Empty) + "','" + ObjCommon.CheckNull(Convert.ToString(item["LastName"]), string.Empty) + "','" + ObjCommon.CheckNull(Convert.ToString(item["Username"]), string.Empty) + "','" + ObjCommon.CheckNull(Convert.ToString(item["Phone"]), string.Empty) + "','" + ObjCommon.CheckNull(Convert.ToString(item["Date"]), string.Empty) + "','" + ObjCommon.CheckNull(Convert.ToString(item["IsInSystem"]), string.Empty) + "','" + PageIndex + "')\" /></td>");
                        sb.Append("</tr>");
                    }

                    sb.Append("</tbody></table></div>");
                }
                else
                {
                    sb.Append("<center>No record found</center>");
                }

            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        public string GetBlockUsersContent(string PageIndex, string SortColumn, string SortDirection, string Searchtext)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                objAdmin = new clsAdmin();
                DataTable dt = new DataTable();
                dt = objAdmin.GetAllBlockUsers(Convert.ToInt32(PageIndex), Convert.ToInt32(PageSize), Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection), Searchtext);

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        string strBlockedDate = string.Empty;
                        if (!string.IsNullOrEmpty(Convert.ToString(item["Date"])))
                        {
                            strBlockedDate = new CommonBLL().ConvertToDateTime(Convert.ToDateTime(item["Date"]), (int)BO.Enums.Common.DateFormat.GENERAL);
                        }

                        sb.Append("<tr id=\"row" + item["UserId"] + "\">");
                        sb.Append("<td><h5>" + ObjCommon.CheckNull(Convert.ToString(item["FirstName"]), string.Empty) + "&nbsp;" + ObjCommon.CheckNull(Convert.ToString(item["LastName"]), string.Empty) + "</h5><p>" + ObjCommon.CheckNull(Convert.ToString(item["Username"]), string.Empty) + "</p></td>");
                        sb.Append(" <td>" + strBlockedDate + "</td>");
                        sb.Append("<td>" + ObjCommon.CheckNull(Convert.ToString(item["Phone"]), string.Empty) + "</td>");
                        sb.Append("<td><input class=\"active_btn_small\" name=\"Active\" type=\"submit\" onclick=\"ActiveBlockUser('" + ObjCommon.CheckNull(Convert.ToString(item["UserId"]), string.Empty) + "','" + ObjCommon.CheckNull(Convert.ToString(item["FirstName"]), string.Empty) + "','" + ObjCommon.CheckNull(Convert.ToString(item["LastName"]), string.Empty) + "','" + ObjCommon.CheckNull(Convert.ToString(item["Username"]), string.Empty) + "','" + ObjCommon.CheckNull(Convert.ToString(item["Phone"]), string.Empty) + "','" + strBlockedDate + "','" + ObjCommon.CheckNull(Convert.ToString(item["IsInSystem"]), string.Empty) + "','" + PageIndex + "')\" /></td>");
                        sb.Append("</tr>");
                    }
                }
                else if (Convert.ToInt32(PageIndex) > 1)
                {
                    sb.Append("<tr><td colspan=\"4\"><center>No more record found</center></td></tr>");
                }
                else
                {
                    sb.Append("<tr><td colspan=\"4\"><center>No record found</center></td></tr>");
                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        [HttpPost]
        public JsonResult ActiveBlockUser(string UserId, string FirstName, string LastName, string Email, string Phone, string Date, string IsInSystem)
        {
            object obj = string.Empty;
            try
            {
                // Note: IsInSystem = 1 then that user from system like from member table and 0 means that user from temp_signup table
                if (!string.IsNullOrEmpty(Date))
                {
                    Date = DateTime.Now.ToString();
                }

                if (IsInSystem == "1")
                {
                    bool result = objAdmin.UpdateUserStatus(Convert.ToInt32(UserId), 1);
                    clsColleaguesData objColleagues = new clsColleaguesData();
                    clsHelper objHelper = new clsHelper();
                    SqlParameter[] strParameter = new SqlParameter[1];
                    strParameter[0] = new SqlParameter("@UserId", SqlDbType.Int);
                    strParameter[0].Value = UserId;
                    DataSet dtFromDoctorDetails = objHelper.GetDatasetData("USP_GetDoctorProfileDetails_By_Id", strParameter);

                    if (dtFromDoctorDetails != null && dtFromDoctorDetails.Tables.Count > 0 && dtFromDoctorDetails.Tables[0].Rows[0]["Username"] != null)
                    {
                        string Password = Convert.ToString(ObjTripleDESCryptoHelper.decryptText(dtFromDoctorDetails.Tables[0].Rows[0]["Password"].ToString()));
                        string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                        ObjTemplate.NewUserEmailFormat(Email, Session["CompanyWebsite"].ToString() + "/User/Index?UserName=" + Email + "&Password=" + EncPassword, Password, Session["CompanyWebsite"].ToString());
                    }
                    if (result)
                    {
                        obj = "1";
                    }
                }
                else
                {
                    // we need to code here to sign up that user from temp table in our system
                    string[] name = Email.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
                    string Password = ObjCommon.CreateRandomPassword(8);
                    string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                    string Accountkey = Crypto.SHA1(Convert.ToString(DateTime.Now.Ticks));

                    DataTable dt = new DataTable();
                    dt = objAdmin.GetTempSignupById(Convert.ToInt32(UserId));// get details from temp_signup table by Id
                    if (dt.Rows.Count > 0)
                    {
                        string NewFname = (FirstName == null || FirstName == "" ? name[0] : FirstName);
                        string NewLname = (LastName == null || LastName == "" ? name[0] : LastName);
                        int newuserId = objColleaguesData.SingUpDoctor(Email, EncPassword,
                            (ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["first_name"]), NewFname)),
                            (ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["last_name"]), NewLname)), null, null, null, Accountkey, null, null, null, null, null, null, Phone, null, false, null, null, null, null, null, null, null, null, null, null, null, null, null, 0, null, null, null, 0);
                        bool result = objAdmin.UpdateStatusInTempTableUserById(Convert.ToInt32(UserId), 1);// update status 1 in temp table 
                        ObjTemplate.NewUserEmailFormat(Email, Session["CompanyWebsite"].ToString() + "/User/Index?UserName=" + Email + "&Password=" + EncPassword, Password, Session["CompanyWebsite"].ToString());
                    }

                    obj = "1";
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Email Templates


        public ActionResult EmailTemplates()
        {
            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                ViewBag.tblEmailTemplates = EmailTemplatesList(1, "", 3, 2);
                return View();
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }
        [HttpPost]
        public JsonResult GetEmailTemplates(string PageIndex, string searchtext, int SortColumn, int SortDirection)
        {
            return Json(EmailTemplatesList(Convert.ToInt32(PageIndex), searchtext, SortColumn, SortDirection), JsonRequestBehavior.AllowGet);
        }

        public string EmailTemplatesList(int PageIndex, string searchtext, int SortColumn, int SortDirection)
        {
            StringBuilder sb = new StringBuilder();

            int size = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

            objAdmin = new clsAdmin();
            DataTable dt = new DataTable();
            dt = objAdmin.BindAllEmailTemp(PageIndex, size, searchtext, SortColumn, SortDirection);

            if (dt.Rows.Count > 0)
            {
                double TotalPages = Math.Ceiling(Convert.ToDouble(dt.Rows[0]["TotalRecord"].ToString()) / size);
                sb.Append("<div class=\"table_data reports_ref_received clearfix\"><h5>Manage Template<span class=\"buttons\"></span></h5>");
                sb.Append("<div class=\"clear\"></div>");

                sb.Append("<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" class=\"manageTemplate\" id=\"tblEmailTemplates\" data-sortcolumn=\"" + SortColumn + "\" data-sortirection=\"" + SortDirection + "\"> <thead><tr>");
                sb.Append(" <th><a class=\"sortable\" href=\"javascript:;\" onclick=\"GetEmailTemplates('" + PageIndex + "','" + 1 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Template Name</a></th>");
                sb.Append(" <th><a class=\"sortable\" href=\"javascript:;\" onclick=\"GetEmailTemplates('" + PageIndex + "','" + 2 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Subject</a></th>");
                sb.Append("<th><a class=\"sortable\" href=\"javascript:;\" onclick=\"GetEmailTemplates('" + PageIndex + "','" + 3 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Modified Date</a></th>");
                sb.Append(" <th><a class=\"sortable\" href=\"javascript:;\" onclick=\"GetEmailTemplates('" + PageIndex + "','" + 3 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Action</a></th>");
                sb.Append(" </tr></thead><tbody id=\"tblbodycontent\">");

                foreach (DataRow item in dt.Rows)
                {
                    string strModifiedDate = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(item["ModifiedDate"])))
                    {
                        strModifiedDate = new CommonBLL().ConvertToDate(Convert.ToDateTime(item["ModifiedDate"]), (int)BO.Enums.Common.DateFormat.GENERAL);
                    }
                    sb.Append("<tr>");
                    sb.Append("<td>" + item["TemplateName"].ToString() + "</td>");
                    sb.Append("<td>" + item["TemplateSubject"].ToString() + "</td>");
                    sb.Append("<td>" + strModifiedDate + "</td>");
                    sb.Append("<td><a href=\"" + Url.Action("EditEmailTemplates", "Admin", new { Id = item["TemplateId"].ToString() }) + "\"><input type=\"submit\" name=\"Send Email\" class=\"edit_btn\"></a></td>");
                    sb.Append("</tr>");
                }

                sb.Append("</tbody></table>  </div>");
            }
            else
            {
                sb.Append("<br/><center>No Record found</center>");
            }
            return sb.ToString();
        }

        public string GetEmailTemplatesContent(string PageIndex, string searchtext, int SortColumn, int SortDirection)
        {
            StringBuilder sb = new StringBuilder();
            int size = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());
            objAdmin = new clsAdmin();
            DataTable dt = new DataTable();
            dt = objAdmin.BindAllEmailTemp(Convert.ToInt32(PageIndex), Convert.ToInt32(size), searchtext, Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection));
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    string strModifiedDate = string.Empty;
                    if (!string.IsNullOrEmpty(Convert.ToString(item["ModifiedDate"])))
                    {
                        strModifiedDate = new CommonBLL().ConvertToDate(Convert.ToDateTime(item["ModifiedDate"]), (int)BO.Enums.Common.DateFormat.GENERAL);
                    }
                    sb.Append("<tr>");
                    sb.Append("<td>" + item["TemplateName"].ToString() + "</td>");
                    sb.Append("<td>" + item["TemplateSubject"].ToString() + "</td>");
                    sb.Append("<td>" + strModifiedDate + "</td>");
                    sb.Append("<td><a href=\"" + Url.Action("EditEmailTemplates", "Admin", new { Id = item["TemplateId"].ToString() }) + "\"><input type=\"submit\" name=\"Send Email\" class=\"edit_btn\"></a></td>");
                    sb.Append("</tr>");
                }
            }
            else if (Convert.ToInt32(PageIndex) > 1)
            {
                sb.Append("<tr><td colspan=\"4\"><center>No more record found</center></td></tr>");
            }
            else
            {
                sb.Append("<tr><td colspan=\"4\"><center>No record found</center></td></tr>");
            }
            return sb.ToString();
        }

        public ActionResult EditEmailTemplates(int Id = 0)
        {

            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                MdlAdmin = new mdlAdmin();
                MdlAdmin.lstEmailTemplate = MdlAdmin.GetEmailTemplateById(Id);
                return View(MdlAdmin);
            }
            else
            {
                return RedirectToAction("Index", "User");
            }

        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult EditEmailTemplateById(string TemplateId, string TemplateSubject, string TemplateDesc)
        {
            object obj = string.Empty;
            bool result = false;
            result = objAdmin.EditEmailTemplateById(Convert.ToInt32(TemplateId), TemplateSubject, TemplateDesc);
            if (true)
            {
                obj = "true";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Add Doctor

        public string SpecialitiesScript(string SpecialitiId)
        {
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.DoctorSpecialities();
            sb.Append("<select id=\"drpSpeciality\" name=\"drpSpeciality\" style=\"width:100%;\">");
            foreach (var item in lst)
            {

                if (SpecialitiId == item.Value)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
                else
                {
                    sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
            }
            sb.Append("</select>");
            return sb.ToString();
        }
        [CustomAuthorize]
        public ActionResult AddDoctor()
        {
            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                ViewBag.Specialities = SpecialitiesScript("");

                ViewBag.CountryPrimary = CountryScriptPrimary("0");
                ViewBag.StatePrimary = StateScriptPrimary("", "0");
                //ViewBag.LicenseState

                ViewBag.CountrySecondary = CountryScriptSecondary("0");
                ViewBag.StateSecondary = StateScriptSecondary("", "0");

                ViewBag.YearAttend = YearScript("0");

                ViewBag.TimeZoneList = new List<Models.Colleagues.TimeZones>();
                DataTable dt = objColleaguesData.GetTimeZones();
                if (dt != null && dt.Rows.Count > 0)
                {
                    ViewBag.TimeZoneList = (from p in dt.AsEnumerable()
                                            select new Models.Colleagues.TimeZones
                                            {
                                                TimeZoneId = Convert.ToInt32(p["TimeZoneId"]),
                                                TimeZoneText = Convert.ToString(p["TimeZoneText"])
                                            }).ToList();
                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "User");
            }

        }

        public JsonResult CheckDoctorIsexistsByEmail(string Email)
        {
            object obj = string.Empty;
            try
            {
                DataTable dtCheckEmail = new DataTable();
                dtCheckEmail = objColleaguesData.CheckEmailInSystem(Email);
                if (dtCheckEmail.Rows.Count == 0)
                {
                    obj = "1";
                }
            }
            catch (Exception)
            {

                throw;
            }

            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorize]
        public JsonResult AddDoctorManually(string AccountName, string Email, string Password, string FirstName, string LastName, string MiddleName, string PublicURL,
            string PrimaryAddress, string PrimaryAddress2, string PrimaryCity, string PrimaryState, string PrimaryCountry, string PrimaryZipCode, string PrimaryPhone,
            string PrimaryFax, string Title, string Description, string FacebookURL, string TwitterURL, string LinkidinURL, string Specialities, string InstitutionAttended,
            string YearGraduated, string SecondaryAddress, string SecondaryAddress2, string SecondaryCity, string SecondaryState, string SecondaryCountry, string SecondaryZip,
            string SecondaryPhone, string SecondaryFax, string LicenseState, string LicenseNumber, string LicenseExpiration, int PrimaryTimezone, int SecondaryTimezone = 74)
        {
            object obj = string.Empty;
            try
            {
                int NewUserId = 0;

                string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                string AccountKey = Crypto.SHA1(Convert.ToString(DateTime.Now.Ticks));
                //string Fname = FirstName.Replace("'", "''");
                //string Lname = LastName.Replace("'","''");

                if (LicenseExpiration != string.Empty)
                {
                    DateTime LicenseExp = new DateTime();
                    LicenseExp = DateTime.ParseExact(LicenseExpiration, "MM/dd/yyyy", CultureInfo.InvariantCulture);
                    NewUserId = objColleaguesData.SingUpDoctor(Email, EncPassword, FirstName, LastName, MiddleName, PublicURL, AccountName, AccountKey, PrimaryAddress, PrimaryAddress2,
                    PrimaryCity, PrimaryState, PrimaryCountry, PrimaryZipCode, PrimaryPhone, PrimaryFax, false, null, null, null, null, null, null, null, null, null, null, null, null, null, 0, LicenseState,
                    LicenseNumber, LicenseExp, PrimaryTimezone);
                    //RM-826 ANKIT HERE 03-06-2019: Change request by Travis for Manually add dentist using admin account. 
                    //ObjTemplate.NewUserEmailFormat(Email, Session["CompanyWebsite"].ToString() + "/User/Index?UserName=" + Email + "&Password=" + EncPassword, Password, Session["CompanyWebsite"].ToString());

                }
                else
                {
                    string LicenseExp = ObjCommon.CheckNull(LicenseExpiration, string.Empty);
                    DateTime tempDate;
                    NewUserId = objColleaguesData.SingUpDoctor(Email, EncPassword, FirstName, LastName, MiddleName, PublicURL, AccountName, AccountKey, PrimaryAddress, PrimaryAddress2,
                    PrimaryCity, PrimaryState, PrimaryCountry, PrimaryZipCode, PrimaryPhone, PrimaryFax, false, null, null, null, null, null, null, null, null, null, null, null, null, null, 0, LicenseState,
                    LicenseNumber, DateTime.TryParseExact(LicenseExp, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out tempDate) ? tempDate : (DateTime?)null, PrimaryTimezone);
                    //RM-826 ANKIT HERE 03-06-2019: Change request by Travis for Manually add dentist using admin account. 
                    //ObjTemplate.NewUserEmailFormat(Email, Session["CompanyWebsite"].ToString() + "/User/Index?UserName=" + Email + "&Password=" + EncPassword, Password, Session["CompanyWebsite"].ToString());
                }


                if (NewUserId != 0)
                {
                    Person person = GetSocialMediaPerson(Email);
                    // add secondary address
                    if (!string.IsNullOrEmpty(SecondaryAddress) || !string.IsNullOrEmpty(SecondaryAddress2))
                    {
                        //Olivia changes on 10-04-2018 for Dr. Lan Allan's website. - DLADCETK-1
                        int NewAddressInfoId = objColleaguesData.UpdateAddressDetailsByAddressInfoID(0, SecondaryAddress, SecondaryAddress2, SecondaryCity, SecondaryState, SecondaryCountry, SecondaryZip, NewUserId, 2, Email, SecondaryPhone, SecondaryFax, null, SecondaryTimezone, null,null,null);
                        bool Result = NewAddressInfoId != 0;
                    }//DD Changes 

                    bool Result1 = objColleaguesData.UpdateDoctorPersonalInformationFromProfile(NewUserId, FirstName, MiddleName, LastName, AccountName, Title, Specialities, "");
                    bool Result2 = objColleaguesData.UpdateSocialMediaDetailsByUserId(NewUserId, person.FacebookURL, person.LinkedInURL, person.TwitterURL, person.InstagramURL, null, null, null, null, null);
                    bool Result3 = objColleaguesData.UpdateMemberDetailsByUserId(NewUserId, "Description", person.Description);
                    bool Result4 = objColleaguesData.EducationAndTrainingInsertAndUpdate(0, NewUserId, InstitutionAttended, null, YearGraduated);
                    string FullName = FirstName + " " + LastName;
                    if (!string.IsNullOrEmpty(Convert.ToString(person.ImageUrl)))
                    {
                        string makefilename = "$" + System.DateTime.Now.Ticks + "$" + (ObjCommon.CheckNull(Convert.ToString(person.FullName), string.Empty)) + ".png";
                        string rootPath = "~/DentistImages/";
                        string fileName = System.IO.Path.Combine(Server.MapPath(rootPath), makefilename);
                        System.Drawing.Image image = ObjCommon.DownloadImageFromUrl(ObjCommon.CheckNull(Convert.ToString(person.ImageUrl), string.Empty));
                        bool IsUploadImage = objColleaguesData.UpdateDoctorProfileImageById(Convert.ToInt32(NewUserId), makefilename);
                        image.Save(fileName);

                    }
                    //Email from client on Fri, Jan 23, 2015 5:09 pm
                    //  ObjTemplate.NewUserEmailFormat(Email, Session["CompanyWebsite"] .ToString() + "/User/Index?UserName=" + Email + "&Password=" + EncPassword, Password, Session["CompanyWebsite"].ToString());
                }

                obj = "1";
            }
            catch (Exception ex)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        protected Person GetSocialMediaPerson(string email)
        {
            try
            {
                //connect to fliptop and pull the information down
                SocialMedia sm = new SocialMedia();
                Person person = sm.LookUpPersonClearBit(email);
                Person fullperson = sm.LookUpPersonfullcontact(email);
                person.FirstName = !string.IsNullOrEmpty(person.FirstName) ? person.FirstName : fullperson.FirstName;
                person.LastName = !string.IsNullOrEmpty(person.LastName) ? person.LastName : fullperson.LastName;
                person.FullName = !string.IsNullOrEmpty(person.FullName) ? person.FullName : fullperson.FullName;
                person.FacebookURL = !string.IsNullOrEmpty(person.FacebookURL) ? person.FacebookURL : fullperson.FacebookURL;
                person.LinkedInURL = !string.IsNullOrEmpty(person.LinkedInURL) ? person.LinkedInURL : fullperson.LinkedInURL;
                person.TwitterURL = !string.IsNullOrEmpty(person.TwitterURL) ? person.TwitterURL : fullperson.TwitterURL;
                person.ImageUrl = !string.IsNullOrEmpty(person.ImageUrl) ? person.ImageUrl : fullperson.ImageUrl;
                person.Description = !string.IsNullOrEmpty(fullperson.Description) ? fullperson.Description : person.Description;
                return person;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string CountryScriptPrimary(string countryId)
        {
            if (countryId == "")
            {
                countryId = "US";
            }
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.Country();
            sb.Append("<select id=\"drpCountryPrimary\" name=\"drpCountryPrimary\" onchange=\"FillStatePrimary();\"  style=\"width:100%;\">");

            foreach (var item in lst)
            {


                if (countryId == item.Value)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
                else
                {
                    sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
            }
            sb.Append("</select>");
            return sb.ToString();
        }

        public string StateScriptPrimary(string countryId, string stateId)
        {
            if (countryId == "")
            {
                countryId = "US";
            }
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.StateByCountryCode(countryId);
            sb.Append("<select id=\"drpStatePrimary\" name=\"drpStatePrimary\" style=\"width:100%;\">");
            foreach (var item in lst)
            {
                if (stateId == item.Value)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
                else
                {
                    sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
            }
            sb.Append("</select>");
            return sb.ToString();
        }



        public string CountryScriptSecondary(string countryId)
        {
            if (countryId == "")
            {
                countryId = "US";
            }
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.Country();
            sb.Append("<select id=\"drpCountrySecondary\" name=\"drpCountrySecondary\" onchange=\"FillStateSecondary();\"  style=\"width:100%;\">");
            foreach (var item in lst)
            {

                if (countryId == item.Value)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
                else
                {
                    sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
            }
            sb.Append("</select>");
            return sb.ToString();
        }

        public string StateScriptSecondary(string countryId, string stateId)
        {
            if (countryId == "")
            {
                countryId = "US";
            }
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.StateByCountryCode(countryId);
            sb.Append("<select id=\"drpStateSecondary\" name=\"drpStateSecondary\"  style=\"width:100%;\">");

            foreach (var item in lst)
            {
                if (stateId == item.Value)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
                else
                {
                    sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
            }
            sb.Append("</select>");
            return sb.ToString();
        }


        public JsonResult FillStatePrimary(string cid, string sid)
        {
            return Json(StateScriptPrimary(cid, sid), JsonRequestBehavior.AllowGet);
        }
        public JsonResult FillLicenseState(string cid, string sid)
        {
            return Json(LicenseState(cid, sid), JsonRequestBehavior.AllowGet);
        }
        public JsonResult FillStateSecondary(string cid, string sid)
        {
            return Json(StateScriptSecondary(cid, sid), JsonRequestBehavior.AllowGet);
        }
        public string LicenseState(string countryId, string stateId)
        {
            if (countryId == "")
            {
                countryId = "US";
            }
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.StateByCountryCode(countryId);
            sb.Append("<select id=\"drpLicenseState\" name=\"drpLicenseState\"  style=\"width:100%;\">");

            foreach (var item in lst)
            {
                if (stateId == item.Value)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
                else
                {
                    sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
            }
            sb.Append("</select>");
            return sb.ToString();
        }


        public string YearScript(string Year)
        {
            if (Year == "")
            {
                Year = DateTime.Now.Year.ToString();
            }
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.YearList();
            sb.Append("<select size=\"1\" id=\"txtyeargraduated\" name=\"txtyeargraduated\" style=\"width:100%;\">");

            foreach (var item in lst)
            {

                if (Year == item.Value)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
                else
                {
                    sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
            }
            sb.Append("</select>");
            return sb.ToString();
        }



        #endregion

        #region Import Doctor

        public ActionResult ImportDoctors()
        {

            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }



        // Import Step 2
        [HttpPost]
        public JsonResult GetImportPatientStep2(string UplaodeFileName)
        {
            return Json(ImportPatientStep2(UplaodeFileName), JsonRequestBehavior.AllowGet);
        }
        public string ImportPatientStep2(string UplaodeFileName)
        {

            StringBuilder sb = new StringBuilder();
            try
            {
                DataSet ds = new DataSet();
                DataTable dtExcelRecords = new DataTable();
                string fileName = Path.GetFileName(UplaodeFileName);
                string fileLocation = Server.MapPath("../CSVLoad/" + fileName);

                FileInfo fi = new FileInfo(fileLocation);
                if (!string.IsNullOrEmpty(UplaodeFileName))
                {


                    ds = ReadExcel(fi.FullName, fi.Extension, true);
                    dtExcelRecords = ds.Tables[0];
                    Session["dtDoctorMapping"] = dtExcelRecords;

                    string ddlMaping = GetDDLForMappingColumn(dtExcelRecords);


                    sb.Append("<ul class=\"tabbed_form_content tabbed_form_content1 match_field\">");



                    sb.Append("<li>");
                    sb.Append("<label>First Name<span class=\"required\">*</span></label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlFirstName\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping); //FirstName Dropdownlist
                    sb.Append("</select></div></li>");


                    sb.Append("<li>");
                    sb.Append("<label>Middle Name</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlMiddelName\"  class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//MiddelName Dropdownlist
                    sb.Append("</select></div></li>");


                    sb.Append("<li>");
                    sb.Append("<label>Last Name<span class=\"required\">*</span></label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlLastName\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//LastName Dropdownlist
                    sb.Append("</select></div></li>");



                    sb.Append("<li>");
                    sb.Append("<label>Public Path</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlPublicPath\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping); //PublicPath DropDownlist
                    sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>Account Name<span class=\"required\">*</span></label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlAccountName\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//AccountName Dropdownlist
                    sb.Append("</select></div></li>");


                    sb.Append("<li>");
                    sb.Append("<label>Email<span class=\"required\">*</span></label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append(" <select name=\"standard-dropdown\" id=\"ddlEmail\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Email Dropdownlist
                    sb.Append("</select></div></li>");



                    sb.Append("<li>");
                    sb.Append("<label>Password<span class=\"required\">*</span></label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlPassword\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Password Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>Web Site</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlWebsite\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Website Dropdownlist
                    sb.Append("</select></div></li>");

                    //sb.Append("<li>");
                    //sb.Append("<label>Photo</label>");
                    //sb.Append("<div class=\"grey_frmbg\">");
                    //sb.Append("<select name=\"standard-dropdown\" id=\"ddlPhoto\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    //sb.Append(ddlMaping);//Photo Dropdownlist
                    //sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>Dental Specialty</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlDentalSpecialty\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//DentalSpecialty Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>Job Title</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlJobTitle\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Job Title Dropdownlist
                    sb.Append("</select></div></li>");


                    sb.Append("<li>");
                    sb.Append("<label>Address</label>");
                    sb.Append(" <div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlAddress\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Address Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>City</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlCity\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//City Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>State</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlState\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//State Dropdownlist
                    sb.Append("</select></div></li>");


                    sb.Append("<li>");
                    sb.Append("<label>Country</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlCountry\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Country Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>Zip</label><div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlZip\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Zip Dropdownlist
                    sb.Append("</select></div></li>");


                    sb.Append(" <li>");
                    sb.Append("<label>Phone</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlPhone\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Phone Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Fax</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlFax\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Fax Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Facebook</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlFacebook\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Facebook Dropdownlist
                    sb.Append("</select></div></li>");


                    sb.Append(" <li>");
                    sb.Append("<label>LinkedIn</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlLinkedIn\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//LinkedIn Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Twitter</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlTwitter\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Twitter Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Blog</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlBlog\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Blog Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>YouTube</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlYouTube\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//YouTube Dropdownlist
                    sb.Append("</select></div></li>");


                    sb.Append(" <li>");
                    sb.Append("<label>Description</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlDescription\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Description Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>School</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlSchool\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//School Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Year Graduated</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlYearGraduated\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Year Graduated Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Degree</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlDegree\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Degree Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Professional Memberships</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlProfessionalMemberships\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Professional Memberships Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>License State</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlLicenseState\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//License States Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>License Number</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlLicenseNumber\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//License Number Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>License Expiration</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlLicenseExpiration\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//License Expiration Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append("</ul>");
                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        public string NewImportPatientStep2(string UplaodeFileName)
        {

            StringBuilder sb = new StringBuilder();
            try
            {
                DataSet ds = new DataSet();
                DataTable dtExcelRecords = new DataTable();
                string fileName = Path.GetFileName(UplaodeFileName);
                string fileLocation = Server.MapPath("../CSVLoad/" + fileName);

                FileInfo fi = new FileInfo(fileLocation);
                if (!string.IsNullOrEmpty(UplaodeFileName))
                {


                    ds = ReadExcel(fi.FullName, fi.Extension, true);
                    dtExcelRecords = ds.Tables[0];
                    Session["dtDoctorMapping"] = dtExcelRecords;

                    string ddlMaping = GetDDLForMappingColumn(dtExcelRecords);


                    sb.Append("<ul class=\"tabbed_form_content tabbed_form_content1 match_field\">");

                    sb.Append("<li>");
                    sb.Append("<label>AccountName<span class=\"required\">*</span></label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlAccountName\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//AccountName Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>FirstName<span class=\"required\">*</span></label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlFirstName\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping); //FirstName Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>LastName<span class=\"required\">*</span></label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlLastName\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//LastName Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>Dental Specialty</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlDentalSpecialty\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//DentalSpecialty Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>Job Title</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlJobTitle\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Job Title Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>Address</label>");
                    sb.Append(" <div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlAddress\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Address Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>City</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlCity\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//City Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>State</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlState\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//State Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>Zip</label><div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlZip\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Zip Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Phone</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlPhone\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Phone Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Fax</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlFax\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Fax Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>Web Site</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlWebsite\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Website Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>Password<span class=\"required\">*</span></label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlPassword\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Password Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>Email<span class=\"required\">*</span></label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append(" <select name=\"standard-dropdown\" id=\"ddlEmail\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Email Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>Photo</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlPhoto\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Photo Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>License State</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlLicenseState\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//License States Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>License Number</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlLicenseNumber\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//License Number Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>License Expiration</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlLicenseExpiration\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//License Expiration Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Professional Memberships</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlProfessionalMemberships\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Professional Memberships Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Professional Memberships1</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlProfessionalMemberships1\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Professional Memberships Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Professional Memberships2</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlProfessionalMemberships2\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Professional Memberships Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Professional Memberships3</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlProfessionalMemberships3\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Professional Memberships Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Professional Memberships4</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlProfessionalMemberships4\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Professional Memberships Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Professional Memberships5</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlProfessionalMemberships5\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Professional Memberships Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Professional Memberships6</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlProfessionalMemberships6\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Professional Memberships Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>School</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlSchool\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//School Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Year Graduated</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlYearGraduated\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Year Graduated Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Degree</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlDegree\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Degree Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>School2</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlSchool1\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//School Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Year Graduated2</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlYearGraduated1\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Year Graduated Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Degree2</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlDegree1\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Degree Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>LinkedIn</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlLinkedIn\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//LinkedIn Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Facebook</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlFacebook\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Facebook Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Twitter</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlTwitter\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Twitter Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>Blog</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlBlog\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Blog Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>YouTube</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlYouTube\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//YouTube Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append("</ul>");
                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }


        public string GetDDLForMappingColumn(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();
            try
            {

                if (dt != null && dt.Rows.Count > 0)
                {
                    sb.Append("<option value=\"0\">--Select--</option>");

                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        sb.Append("<option value=\"" + dt.Columns[i].ToString() + "\">" + dt.Columns[i].ToString() + "</option>");
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }

            return sb.ToString();
        }
        //New Import step 4
        public string NewGetImportPatientStep4(string Mapping)
        {
            string[] ArrayMapping = Mapping.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            DataTable dtExcelRecord = new DataTable();
            dtExcelRecord = (DataTable)Session["dtDoctorMapping"];


            StringBuilder DoctorIds = new StringBuilder();
            StringBuilder ExistsDoctorIds = new StringBuilder();
            SerielizeObjForDoctorImportInAdmin SerilzeObj = new SerielizeObjForDoctorImportInAdmin();
            // Code for insert data in Database as per columns map
            foreach (DataRow item in dtExcelRecord.Rows)
            {
                if (ArrayMapping[13] != "0")
                {
                    string Email = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[13]]), string.Empty);
                    if (!string.IsNullOrEmpty(Email))
                    {
                        if (ObjCommon.IsEmail(Email))
                        {
                            DataTable dtCheckEmail = new DataTable();
                            dtCheckEmail = objColleaguesData.CheckEmailInSystem(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[13]]), string.Empty));


                            string AccountName = string.Empty; string Phone = string.Empty; string City = string.Empty;
                            string Address = string.Empty; string State = string.Empty;
                            string Website = string.Empty; string Photo = string.Empty;
                            string DentalSpecialty = string.Empty; string JobTitle = string.Empty;
                            string Country = string.Empty; string PublicPath = string.Empty;
                            string ZipCode = string.Empty; string MiddleName = string.Empty;
                            string Facebook = string.Empty; string LinkedIn = string.Empty;
                            string Twitter = string.Empty; string Blog = string.Empty;
                            string YouTube = string.Empty; string Description = string.Empty;
                            string School = string.Empty; int? YearGraduated = 0; string School1 = string.Empty; int? YearGraduated1 = 0;
                            string Degree = string.Empty;
                            string Degree1 = string.Empty; string ProfessionalMemberships = string.Empty; string ProfessionalMemberships6 = string.Empty; string ProfessionalMemberships1 = string.Empty; string ProfessionalMemberships2 = string.Empty; string ProfessionalMemberships3 = string.Empty; string ProfessionalMemberships4 = string.Empty; string ProfessionalMemberships5 = string.Empty;
                            string LicenseState = string.Empty; string LicenseNumber = string.Empty;
                            DateTime? LicenseExpiration = null; string Fax = string.Empty;
                            string Address2 = string.Empty; string DentrixProviderId = string.Empty;
                            int PrimaryTimeZone = 0;string Mobile = string.Empty;



                            if (ArrayMapping[0] != "0")
                            {
                                AccountName = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[0]]), string.Empty);
                            }
                            if (ArrayMapping[11] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[11]]), string.Empty)))
                                {
                                    Website = Convert.ToString(item[ArrayMapping[11]]);
                                }

                            }
                            if (ArrayMapping[14] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[14]]), string.Empty)))
                                {
                                    Photo = Convert.ToString(item[ArrayMapping[14]]);
                                }
                            }
                            if (ArrayMapping[3] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[3]]), string.Empty)))
                                {
                                    DentalSpecialty = Convert.ToString(item[ArrayMapping[3]]);
                                }

                            }
                            if (ArrayMapping[4] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[4]]), string.Empty)))
                                {
                                    JobTitle = Convert.ToString(item[ArrayMapping[4]]);
                                }

                            }

                            if (ArrayMapping[5] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[5]]), string.Empty)))
                                {
                                    Address = Convert.ToString(item[ArrayMapping[5]]);
                                }

                            }

                            if (ArrayMapping[6] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[6]]), string.Empty)))
                                {
                                    City = Convert.ToString(item[ArrayMapping[6]]);
                                }

                            }

                            if (ArrayMapping[7] != "0")
                            {
                                DataTable dtState = new DataTable();
                                string qry = "select * from StateMaster where StateName='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[7]]), string.Empty) + "' or StateCode='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[7]]), string.Empty) + "'";
                                dtState = ObjCommon.DataTable(qry);
                                if (dtState != null && dtState.Rows.Count > 0)
                                {
                                    State = dtState.Rows[0]["StateCode"].ToString();
                                }
                            }

                            if (ArrayMapping[8] != "0")
                            {
                                bool CheckZip = false;
                                CheckZip = ObjCommon.IsValidZipCode(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[8]]), string.Empty));
                                if (CheckZip)
                                {
                                    ZipCode = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[8]]), string.Empty);
                                }
                            }


                            if (ArrayMapping[9] != "0")
                            {
                                bool CheckPhone = false;
                                CheckPhone = ObjCommon.IsValidPhone(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[9]]), string.Empty));
                                if (CheckPhone)
                                {
                                    Phone = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[9]]), string.Empty);
                                }
                            }
                            if (ArrayMapping[10] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[10]]), string.Empty)))
                                {
                                    Fax = Convert.ToString(item[ArrayMapping[10]]);
                                }

                            }
                            if (ArrayMapping[32] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[32]]), string.Empty)))
                                {
                                    Facebook = Convert.ToString(item[ArrayMapping[32]]);
                                }

                            }

                            if (ArrayMapping[31] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[31]]), string.Empty)))
                                {
                                    LinkedIn = Convert.ToString(item[ArrayMapping[31]]);
                                }

                            }

                            if (ArrayMapping[33] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[33]]), string.Empty)))
                                {
                                    Twitter = Convert.ToString(item[ArrayMapping[33]]);
                                }

                            }

                            if (ArrayMapping[34] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[34]]), string.Empty)))
                                {
                                    Blog = Convert.ToString(item[ArrayMapping[34]]);
                                }

                            }

                            if (ArrayMapping[35] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[35]]), string.Empty)))
                                {
                                    YouTube = Convert.ToString(item[ArrayMapping[35]]);
                                }

                            }

                            if (ArrayMapping[22] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[22]]), string.Empty)))
                                {
                                    Description = Convert.ToString(item[ArrayMapping[22]]);
                                }

                            }

                            if (ArrayMapping[25] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[25]]), string.Empty)))
                                {
                                    School = Convert.ToString(item[ArrayMapping[25]]);
                                }

                            }

                            if (ArrayMapping[26] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[26]]), string.Empty)))
                                {
                                    int num;

                                    if (int.TryParse(item[ArrayMapping[26]].ToString(), out num))
                                    {
                                        YearGraduated = Convert.ToInt32((ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[26]]), string.Empty)));
                                    }

                                }

                            }

                            if (ArrayMapping[27] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[27]]), string.Empty)))
                                {
                                    Degree = Convert.ToString(item[ArrayMapping[27]]);
                                }

                            }
                            if (ArrayMapping[28] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[28]]), string.Empty)))
                                {
                                    School1 = Convert.ToString(item[ArrayMapping[28]]);
                                }

                            }

                            if (ArrayMapping[29] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[29]]), string.Empty)))
                                {
                                    int num;

                                    if (int.TryParse(item[ArrayMapping[29]].ToString(), out num))
                                    {
                                        YearGraduated1 = Convert.ToInt32((ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[29]]), string.Empty)));
                                    }

                                }

                            }

                            if (ArrayMapping[30] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[30]]), string.Empty)))
                                {
                                    Degree1 = Convert.ToString(item[ArrayMapping[30]]);
                                }

                            }
                            if (ArrayMapping[18] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[18]]), string.Empty)))
                                {
                                    ProfessionalMemberships = Convert.ToString(item[ArrayMapping[18]]);
                                }

                            }
                            if (ArrayMapping[19] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[19]]), string.Empty)))
                                {
                                    ProfessionalMemberships1 = Convert.ToString(item[ArrayMapping[19]]);
                                }

                            }
                            if (ArrayMapping[20] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[20]]), string.Empty)))
                                {
                                    ProfessionalMemberships2 = Convert.ToString(item[ArrayMapping[20]]);
                                }

                            }
                            if (ArrayMapping[21] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[21]]), string.Empty)))
                                {
                                    ProfessionalMemberships3 = Convert.ToString(item[ArrayMapping[21]]);
                                }

                            }
                            if (ArrayMapping[22] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[22]]), string.Empty)))
                                {
                                    ProfessionalMemberships4 = Convert.ToString(item[ArrayMapping[22]]);
                                }

                            }
                            if (ArrayMapping[23] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[23]]), string.Empty)))
                                {
                                    ProfessionalMemberships5 = Convert.ToString(item[ArrayMapping[23]]);
                                }

                            }
                            if (ArrayMapping[24] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[24]]), string.Empty)))
                                {
                                    ProfessionalMemberships6 = Convert.ToString(item[ArrayMapping[24]]);
                                }

                            }
                            if (ArrayMapping[15] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[15]]), string.Empty)))
                                {
                                    DataTable dtState = new DataTable();
                                    string qry = "select * from StateMaster where StateName='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[15]]), string.Empty) + "' or StateCode='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[15]]), string.Empty) + "'";
                                    dtState = ObjCommon.DataTable(qry);
                                    if (dtState != null && dtState.Rows.Count > 0)
                                    {
                                        LicenseState = dtState.Rows[0]["StateCode"].ToString();
                                    }
                                }

                            }
                            if (ArrayMapping[16] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[16]]), string.Empty)))
                                {
                                    LicenseNumber = Convert.ToString(item[ArrayMapping[16]]);
                                }

                            }
                            if (ArrayMapping[17] != "0")
                            {
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[17]]), string.Empty)))
                                {

                                    LicenseExpiration = null;
                                }

                            }

                            int InsertDoctor = 0;
                            string Password = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[12]]), ObjCommon.CreateRandomPassword(8));
                            string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);



                            string Accountkey = Crypto.SHA1(Convert.ToString(DateTime.Now.Ticks));


                            if (dtCheckEmail == null || dtCheckEmail.Rows.Count == 0)
                            {
                                InsertDoctor = objColleaguesData.NewSingUpDoctor(Email, EncPassword,
                                ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[1].ToString()]), string.Empty), ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[2].ToString()]), string.Empty), MiddleName,
                                PublicPath, AccountName, Accountkey, Address,Address2,City, State, Country, ZipCode, Phone,Fax, false, Facebook, LinkedIn, Twitter,Blog, YouTube, Description, School, YearGraduated, Degree,
                                ProfessionalMemberships, Website, JobTitle, DentalSpecialty,1, LicenseState, LicenseNumber, LicenseExpiration, PrimaryTimeZone, DentrixProviderId, Mobile);
                            }
                            else
                            {
                                bool MemberSpecialties = false; bool MemberEducation = false; bool MemberAddressInfo = false; bool MemberAccount = false; bool MemberShip = false; bool Member = false;
                                bool MemberDetails = false;
                                if (DentalSpecialty != null || DentalSpecialty != "")
                                {
                                    MemberSpecialties = objColleaguesData.UpdateMemberSpecialtiesImportTime(Convert.ToInt32(dtCheckEmail.Rows[0][0]), DentalSpecialty);
                                }
                                if (AccountName != null || AccountName != "")
                                {
                                    MemberAccount = objColleaguesData.UpdateAccountNameImportTime(Convert.ToInt32(dtCheckEmail.Rows[0][0]), AccountName);
                                }
                                MemberEducation = objColleaguesData.UpdateMemberEducationImportTime(Convert.ToInt32(dtCheckEmail.Rows[0][0]), School, Convert.ToString(YearGraduated), Degree);
                                MemberAddressInfo = objColleaguesData.UpdateAddressInfoImportTime(Convert.ToInt32(dtCheckEmail.Rows[0][0]), Address, State, City, ZipCode, Phone, Fax, ArrayMapping[13]);
                                MemberShip = objColleaguesData.UpdateProfessionalMemberShipImportTIme(Convert.ToInt32(dtCheckEmail.Rows[0][0]), ProfessionalMemberships);
                                Member = objColleaguesData.UpdateMemberTableImportTime(Convert.ToInt32(dtCheckEmail.Rows[0][0]), EncPassword, Photo, Website, JobTitle, ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[1].ToString()]), string.Empty), ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[2].ToString()]), string.Empty));
                                MemberDetails = objColleaguesData.UpdateMemberDetailsImportTime(Convert.ToInt32(dtCheckEmail.Rows[0][0]), LicenseNumber, LicenseState, Convert.ToString(LicenseExpiration));
                            }
                            if (InsertDoctor != 0)
                            {
                                if (string.IsNullOrEmpty(DoctorIds.ToString()))
                                {
                                    DoctorIds.Append(Convert.ToString(InsertDoctor));
                                }
                                else
                                {
                                    DoctorIds.Append("," + Convert.ToString(InsertDoctor));
                                }
                            }

                            if (dtCheckEmail.Rows.Count != 0)
                            {
                                ExistsDoctorIds.Append(dtCheckEmail.Rows[0][0] + ",");
                            }


                        }
                    }


                }
            }

            SerilzeObj.NewDoctor = NewImportPatientStep4(DoctorIds.ToString());
            SerilzeObj.ExistsDoctor = NewImportPatientStep4(ExistsDoctorIds.ToString());
            var serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };

            var serializedResult = serializer.Serialize(SerilzeObj);
            return serializedResult.ToString();
        }



        // Import Step 4

        public string GetImportPatientStep4(string Mapping)
        {
            string[] ArrayMapping = Mapping.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            DataTable dtExcelRecord = new DataTable();
            dtExcelRecord = (DataTable)Session["dtDoctorMapping"];


            StringBuilder DoctorIds = new StringBuilder();
            StringBuilder ExistsDoctorIds = new StringBuilder();
            SerielizeObjForDoctorImportInAdmin SerilzeObj = new SerielizeObjForDoctorImportInAdmin();
            // Code for insert data in Database as per columns map
            foreach (DataRow item in dtExcelRecord.Rows)
            {
                if (ArrayMapping[5] != "0")
                {
                    string Email = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[5]]), string.Empty);
                    if (!string.IsNullOrEmpty(Email))
                    {
                        if (ObjCommon.IsEmail(Email))
                        {
                            DataTable dtCheckEmail = new DataTable();
                            dtCheckEmail = objColleaguesData.CheckEmailInSystem(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[5]]), string.Empty));

                            DataTable dtCheckPublicProfile = new DataTable();

                            if (ArrayMapping[3] != "0")
                            {
                                dtCheckPublicProfile = objColleaguesData.CheckPublicProfileUrlIsExists(0, Convert.ToString(item[ArrayMapping[3]]));
                            }

                            if ((dtCheckEmail == null || dtCheckEmail.Rows.Count == 0) && (dtCheckPublicProfile == null || dtCheckPublicProfile.Rows.Count == 0))
                            {
                                string AccountName = string.Empty; string Phone = string.Empty; string City = string.Empty;
                                string Address = string.Empty; string State = string.Empty;
                                string Website = string.Empty; string Photo = string.Empty;
                                string DentalSpecialty = string.Empty; string JobTitle = string.Empty;
                                string Country = string.Empty; string PublicPath = string.Empty;
                                string ZipCode = string.Empty; string MiddleName = string.Empty;
                                string Facebook = string.Empty; string LinkedIn = string.Empty;
                                string Twitter = string.Empty; string Blog = string.Empty;
                                string YouTube = string.Empty; string Description = string.Empty;
                                string School = string.Empty; int? YearGraduated = 0;
                                string Degree = string.Empty; string ProfessionalMemberships = string.Empty;
                                string LicenseState = string.Empty; string LicenseNumber = string.Empty;
                                DateTime? LicenseExpiration = null; string Fax = string.Empty;


                                if (ArrayMapping[1] != "0")
                                {
                                    MiddleName = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[1]]), string.Empty);
                                }


                                if (ArrayMapping[3] != "0")
                                {
                                    PublicPath = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[3]]), string.Empty);

                                }
                                if (ArrayMapping[4] != "0")
                                {
                                    AccountName = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[4]]), string.Empty);
                                }
                                if (ArrayMapping[7] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[7]]), string.Empty)))
                                    {
                                        Website = Convert.ToString(item[ArrayMapping[7]]);
                                    }

                                }
                                //if (ArrayMapping[8] != "0")
                                //{
                                //    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[8]]), string.Empty)))
                                //    {
                                //        Photo = Convert.ToString(item[ArrayMapping[8]]);
                                //    }

                                //}
                                if (ArrayMapping[8] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[8]]), string.Empty)))
                                    {
                                        DentalSpecialty = Convert.ToString(item[ArrayMapping[8]]);
                                    }

                                }
                                if (ArrayMapping[9] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[9]]), string.Empty)))
                                    {
                                        JobTitle = Convert.ToString(item[ArrayMapping[9]]);
                                    }

                                }

                                if (ArrayMapping[10] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[10]]), string.Empty)))
                                    {
                                        Address = Convert.ToString(item[ArrayMapping[10]]);
                                    }

                                }

                                if (ArrayMapping[11] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[11]]), string.Empty)))
                                    {
                                        City = Convert.ToString(item[ArrayMapping[11]]);
                                    }

                                }




                                if (ArrayMapping[12] != "0")
                                {
                                    DataTable dtState = new DataTable();
                                    string qry = "select * from StateMaster where StateName='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[12]]), string.Empty) + "' or StateCode='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[12]]), string.Empty) + "'";
                                    dtState = ObjCommon.DataTable(qry);
                                    if (dtState != null && dtState.Rows.Count > 0)
                                    {
                                        State = dtState.Rows[0]["StateCode"].ToString();
                                    }
                                }


                                if (ArrayMapping[13] != "0")
                                {
                                    DataTable dtCountry = new DataTable();
                                    string qry = "select * from CountryMaster where CountryName='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[13]]), string.Empty) + "' or CountryCode='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[13]]), string.Empty) + "'";
                                    dtCountry = ObjCommon.DataTable(qry);
                                    if (dtCountry != null && dtCountry.Rows.Count > 0)
                                    {
                                        Country = dtCountry.Rows[0]["CountryCode"].ToString();
                                    }
                                }


                                if (ArrayMapping[14] != "0")
                                {
                                    bool CheckZip = false;
                                    CheckZip = ObjCommon.IsValidZipCode(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[14]]), string.Empty));
                                    if (CheckZip)
                                    {
                                        ZipCode = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[14]]), string.Empty);
                                    }
                                }


                                if (ArrayMapping[15] != "0")
                                {
                                    bool CheckPhone = false;
                                    CheckPhone = ObjCommon.IsValidPhone(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[15]]), string.Empty));
                                    if (CheckPhone)
                                    {
                                        Phone = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[15]]), string.Empty);
                                    }
                                }
                                if (ArrayMapping[16] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[16]]), string.Empty)))
                                    {
                                        Fax = Convert.ToString(item[ArrayMapping[16]]);
                                    }

                                }
                                if (ArrayMapping[17] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[17]]), string.Empty)))
                                    {
                                        Facebook = Convert.ToString(item[ArrayMapping[17]]);
                                    }

                                }

                                if (ArrayMapping[18] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[18]]), string.Empty)))
                                    {
                                        LinkedIn = Convert.ToString(item[ArrayMapping[18]]);
                                    }

                                }

                                if (ArrayMapping[19] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[19]]), string.Empty)))
                                    {
                                        Twitter = Convert.ToString(item[ArrayMapping[19]]);
                                    }

                                }

                                if (ArrayMapping[20] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[20]]), string.Empty)))
                                    {
                                        Blog = Convert.ToString(item[ArrayMapping[20]]);
                                    }

                                }

                                if (ArrayMapping[21] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[21]]), string.Empty)))
                                    {
                                        YouTube = Convert.ToString(item[ArrayMapping[21]]);
                                    }

                                }

                                if (ArrayMapping[22] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[22]]), string.Empty)))
                                    {
                                        Description = Convert.ToString(item[ArrayMapping[22]]);
                                    }

                                }

                                if (ArrayMapping[23] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[23]]), string.Empty)))
                                    {
                                        School = Convert.ToString(item[ArrayMapping[23]]);
                                    }

                                }

                                if (ArrayMapping[24] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[24]]), string.Empty)))
                                    {
                                        int num;

                                        if (int.TryParse(item[ArrayMapping[24]].ToString(), out num))
                                        {
                                            YearGraduated = Convert.ToInt32(item[ArrayMapping[24]]);
                                        }

                                    }

                                }

                                if (ArrayMapping[25] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[25]]), string.Empty)))
                                    {
                                        Degree = Convert.ToString(item[ArrayMapping[25]]);
                                    }

                                }

                                if (ArrayMapping[26] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[26]]), string.Empty)))
                                    {
                                        ProfessionalMemberships = Convert.ToString(item[ArrayMapping[26]]);
                                    }

                                }
                                if (ArrayMapping[27] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[27]]), string.Empty)))
                                    {
                                        DataTable dtState = new DataTable();
                                        string qry = "select * from StateMaster where StateName='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[27]]), string.Empty) + "' or StateCode='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[27]]), string.Empty) + "'";
                                        dtState = ObjCommon.DataTable(qry);
                                        if (dtState != null && dtState.Rows.Count > 0)
                                        {
                                            LicenseState = dtState.Rows[0]["StateCode"].ToString();
                                        }
                                        //LicenseState = Convert.ToString(item[ArrayMapping[26]]);
                                    }

                                }
                                if (ArrayMapping[28] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[28]]), string.Empty)))
                                    {
                                        LicenseNumber = Convert.ToString(item[ArrayMapping[28]]);
                                    }

                                }
                                if (ArrayMapping[29] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[29]]), string.Empty)))
                                    {
                                        //-- ERROR - when user uses LicenseExpiration pending to fix.
                                        TimeSpan datefromexcel = new TimeSpan(Convert.ToInt32(item[ArrayMapping[29]]) - 2, 0, 0, 0);
                                        DateTime inputdate = new DateTime(1900, 1, 1).Add(datefromexcel);
                                        //DateTime LicenseExp = DateTime.ParseExact(Convert.ToString(inputdate) , "MM/dd/yyyy", CultureInfo.InvariantCulture);
                                        LicenseExpiration = Convert.ToDateTime(inputdate.Date);
                                    }

                                }

                                int InsertDoctor = 0;
                                string Password = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[6]]), ObjCommon.CreateRandomPassword(8));
                                string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);



                                string Accountkey = Crypto.SHA1(Convert.ToString(DateTime.Now.Ticks));

                                InsertDoctor = objColleaguesData.SingUpDoctor(Email, EncPassword,
                                    ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[0].ToString()]), string.Empty), ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[2].ToString()]), string.Empty), MiddleName,
                                    PublicPath, AccountName, Accountkey, Address, null, City, State, Country, ZipCode, Phone, Fax, false, Facebook, LinkedIn, Twitter, Blog, YouTube, Description, School, YearGraduated, Degree,
                                    ProfessionalMemberships, Website, JobTitle, DentalSpecialty, 1, LicenseState, LicenseNumber, LicenseExpiration, 0);

                                if (InsertDoctor != 0)
                                {
                                    if (string.IsNullOrEmpty(DoctorIds.ToString()))
                                    {
                                        DoctorIds.Append(Convert.ToString(InsertDoctor));
                                    }
                                    else
                                    {
                                        DoctorIds.Append("," + Convert.ToString(InsertDoctor));
                                    }
                                }
                            }
                            else
                            {
                                if (dtCheckEmail.Rows.Count != 0)
                                {
                                    ExistsDoctorIds.Append(dtCheckEmail.Rows[0][0] + ",");
                                }

                            }
                        }
                    }


                }
            }

            SerilzeObj.NewDoctor = ImportPatientStep4(DoctorIds.ToString());
            SerilzeObj.ExistsDoctor = ImportPatientStep4(ExistsDoctorIds.ToString());
            var serializer = new JavaScriptSerializer { MaxJsonLength = Int32.MaxValue };

            var serializedResult = serializer.Serialize(SerilzeObj);
            return serializedResult.ToString();
        }


        //NewImportPatientStep4

        public string NewImportPatientStep4(string DoctorIds)
        {
            StringBuilder sb = new StringBuilder();
            try
            {

                string[] ArrayPatientIds = DoctorIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (ArrayPatientIds.Length > 0)
                {


                    sb.Append("<div style=\"overflow:auto;width:100%\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"patientTableImport\">");
                    sb.Append("<thead><tr>");
                    sb.Append("<th>AccountName</th>");
                    sb.Append("<th>First Name</th>");
                    sb.Append("<th>Last Name</th>");
                    sb.Append("<th>Dental Specialty</th>");
                    sb.Append("<th>Job Title</th>");
                    sb.Append("<th>Address</th>");
                    sb.Append("<th>City</th>");
                    sb.Append("<th>State</th>");
                    sb.Append("<th>Zip</th>");
                    sb.Append("<th>Phone</th>");
                    sb.Append("<th>Fax</th>");
                    sb.Append("<th>Web Site</th>");
                    sb.Append("<th>Password</th>");
                    sb.Append("<th>Email</th>");
                    sb.Append("<th>Photo</th>");
                    sb.Append("<th>LicenseState</th>");
                    sb.Append("<th>LicenseNumber</th>");
                    sb.Append("<th>LicenseExpiration</th>");
                    sb.Append("<th>Professional Memberships</th>");
                    sb.Append("<th>Professional Memberships1</th>");
                    sb.Append("<th>Professional Memberships2</th>");
                    sb.Append("<th>Professional Memberships3</th>");
                    sb.Append("<th>Professional Memberships4</th>");
                    sb.Append("<th>Professional Memberships5</th>");
                    sb.Append("<th>Professional Memberships6</th>");
                    sb.Append("<th>School</th>");
                    sb.Append("<th>Year Graduated</th>");
                    sb.Append("<th>Degree</th>");
                    sb.Append("<th>School1</th>");
                    sb.Append("<th>Year Graduated1</th>");
                    sb.Append("<th>Degree1</th>");
                    sb.Append("<th>LinkedIn</th>");
                    sb.Append("<th>Facebook</th>");
                    sb.Append("<th>Twitter</th>");
                    sb.Append("<th>Blog</th>");
                    sb.Append("<th>YouTube</th>");
                    sb.Append("</tr></thead>");
                    sb.Append("<tbody>");

                    foreach (var item in ArrayPatientIds)
                    {

                        DataSet dsImported = new DataSet();

                        dsImported = objColleaguesData.GetDoctorDetailsById(Convert.ToInt32(item));
                        if (dsImported != null && dsImported.Tables.Count > 0)
                        {

                            for (int j = 0; j < dsImported.Tables[0].Rows.Count; j++)
                            {
                                sb.Append("<tr>");
                                if (dsImported.Tables[1].Rows.Count > 0)
                                {
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[1].Rows[j]["AccountName"]), string.Empty) + "</td>");//AccountName
                                }
                                else
                                {
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[1].Rows[j]["AccountName"]), string.Empty) + "</td>");//AccountName
                                }
                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[0].Rows[j]["FirstName"]), string.Empty) + "</td>");
                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[0].Rows[j]["LastName"]), string.Empty) + "</td>");
                                if (dsImported.Tables[7].Rows.Count > 0)
                                {
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[7].Rows[j]["Speciality"]), string.Empty) + "</td>");
                                }
                                else
                                {
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                }
                                if (dsImported.Tables[0].Rows.Count > 0)
                                {
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[0].Rows[j]["Title"]), string.Empty) + "</td>");
                                }
                                else
                                {
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                }
                                if (dsImported.Tables[2].Rows.Count > 0)
                                {
                                    sb.Append("<td class=\"numeric\"><div class=\"block_data\"  onmouseout=\"divonmouseout()\"  onmouseover=\"divonmouseover()\"><a href=\"javascript:;\" class=\"location_icon_import\"><div class=\"tooltip_import\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[2].Rows[j]["ExactAddress"]), string.Empty) + "</div></a></div></td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[2].Rows[j]["City"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[2].Rows[j]["State"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[2].Rows[j]["ZipCode"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[2].Rows[j]["Phone"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[2].Rows[j]["Fax"]), string.Empty) + "</td>");

                                }
                                else
                                {
                                    sb.Append("<td class=\"numeric\"><div class=\"block_data\"  onmouseout=\"divonmouseout()\"  onmouseover=\"divonmouseover()\"><a href=\"javascript:;\" class=\"location_icon_import\"><div class=\"tooltip_import\">" + string.Empty + "</div></a></div></td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");

                                }
                                if (dsImported.Tables[0].Rows.Count > 0)
                                {
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[0].Rows[j]["WebsiteURL"]), string.Empty) + "</td>");
                                }
                                else
                                {
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                }
                                string ExactPassword = string.Empty;
                                string newpass = ObjTripleDESCryptoHelper.decryptText(ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[0].Rows[j]["Password"]), string.Empty));
                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[0].Rows[j]["Password"]), string.Empty)))
                                {
                                    ExactPassword = Convert.ToBase64String(Encoding.ASCII.GetBytes(newpass));
                                }
                                sb.Append("<td class=\"numeric\">" + ExactPassword + "</td>");
                                sb.Append("<td><div class=\"block_data\"  onmouseout=\"divonmouseout()\"  onmouseover=\"divonmouseover()\" ><a href=\"javascript:;\" class=\"message_icon_import\"><div class=\"tooltip_import\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[0].Rows[j]["Username"]), string.Empty) + "</div></a></div></td>");
                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[0].Rows[j]["ImageName"]), string.Empty) + "</td>");//PublicPath
                                if (dsImported.Tables[10].Rows.Count > 0)
                                {
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[10].Rows[j]["LicenseState"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[10].Rows[j]["LicenseNumber"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[10].Rows[j]["LicenseExpiration"]), string.Empty) + "</td>");
                                }
                                else
                                {
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                }
                                if (dsImported.Tables[5].Rows.Count > 0)
                                {
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[5].Rows[j]["Membership"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");

                                }
                                else
                                {
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                }
                                if (dsImported.Tables[4].Rows.Count > 0)
                                {

                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[4].Rows[j]["Institute"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[4].Rows[j]["YearAttended"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[4].Rows[j]["Specialisation"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                }
                                else
                                {
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                }
                                if (dsImported.Tables[3].Rows.Count > 0)
                                {
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[3].Rows[j]["LinkedinUrl"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[3].Rows[j]["FacebookUrl"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[3].Rows[j]["TwitterUrl"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[3].Rows[j]["BlogUrl"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[3].Rows[j]["YoutubeUrl"]), string.Empty) + "</td>");
                                }
                                else
                                {
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                }
                                sb.Append("</tr>");
                            }
                        }
                    }
                    sb.Append("</tbody></table></div>");
                }
                else
                {
                    sb.Append("<br/><center class=\"CheckImportrecord\">No record imported</center>");
                }



            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        public string ImportPatientStep4(string DoctorIds)
        {
            StringBuilder sb = new StringBuilder();
            try
            {

                string[] ArrayPatientIds = DoctorIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (ArrayPatientIds.Length > 0)
                {


                    sb.Append("<div style=\"overflow:auto;width:100%\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"patientTableImport\">");
                    sb.Append("<thead><tr>");
                    sb.Append("<th>First Name</th>");
                    sb.Append("<th>Middle Name</th>");
                    sb.Append("<th>Last Name</th>");
                    sb.Append("<th>PublicPath</th>");
                    sb.Append("<th>AccountName</th>");
                    sb.Append("<th>Email</th>");
                    sb.Append("<th>Password</th>");
                    sb.Append("<th>Web Site</th>");
                    sb.Append("<th>Dental Specialty</th>");
                    sb.Append("<th>Job Title</th>");
                    sb.Append("<th>Address</th>");
                    sb.Append("<th>City</th>");
                    sb.Append("<th>State</th>");
                    sb.Append("<th>Country</th>");
                    sb.Append("<th>Zip</th>");
                    sb.Append("<th>Phone</th>");
                    sb.Append("<th>Fax</th>");

                    sb.Append("<th>Facebook</th>");
                    sb.Append("<th>LinkedIn</th>");
                    sb.Append("<th>Twitter</th>");
                    sb.Append("<th>Blog</th>");
                    sb.Append("<th>YouTube</th>");
                    sb.Append("<th>Description</th>");
                    sb.Append("<th>School</th>");
                    sb.Append("<th>Year Graduated</th>");
                    sb.Append("<th>Degree</th>");
                    sb.Append("<th>Professional Memberships</th>");
                    sb.Append("<th>LicenseState</th>");
                    sb.Append("<th>LicenseNumber</th>");
                    sb.Append("<th>LicenseExpiration</th>");

                    sb.Append("</tr></thead>");
                    sb.Append("<tbody>");

                    foreach (var item in ArrayPatientIds)
                    {

                        DataSet dsImported = new DataSet();

                        dsImported = objColleaguesData.GetDoctorDetailsById(Convert.ToInt32(item));
                        if (dsImported != null && dsImported.Tables.Count > 0)
                        {

                            for (int j = 0; j < dsImported.Tables[0].Rows.Count; j++)
                            {
                                sb.Append("<tr>");

                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[0].Rows[j]["FirstName"]), string.Empty) + "</td>");
                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[0].Rows[j]["MiddleName"]), string.Empty) + "</td>");
                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[0].Rows[j]["LastName"]), string.Empty) + "</td>");
                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[0].Rows[j]["PublicProfileUrl"]), string.Empty) + "</td>");//PublicPath


                                if (dsImported.Tables[1].Rows.Count > 0)
                                {
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[1].Rows[j]["AccountName"]), string.Empty) + "</td>");//AccountName
                                }
                                else
                                {
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[1].Rows[j]["AccountName"]), string.Empty) + "</td>");//AccountName
                                }

                                sb.Append("<td><div class=\"block_data\"  onmouseout=\"divonmouseout()\"  onmouseover=\"divonmouseover()\" ><a href=\"javascript:;\" class=\"message_icon_import\"><div class=\"tooltip_import\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[0].Rows[j]["Username"]), string.Empty) + "</div></a></div></td>");

                                string ExactPassword = string.Empty;

                                if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[0].Rows[j]["Password"]), string.Empty)))
                                {
                                    ExactPassword = ObjTripleDESCryptoHelper.decryptText(ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[0].Rows[j]["Password"]), string.Empty));
                                }
                                sb.Append("<td class=\"numeric\">" + ExactPassword + "</td>");

                                if (dsImported.Tables[0].Rows.Count > 0)
                                {
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[0].Rows[j]["Title"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[0].Rows[j]["WebsiteURL"]), string.Empty) + "</td>");
                                }
                                else
                                {
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                }
                                if (dsImported.Tables[7].Rows.Count > 0)
                                {
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[7].Rows[j]["Speciality"]), string.Empty) + "</td>");
                                }
                                else
                                {
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                }

                                if (dsImported.Tables[2].Rows.Count > 0)
                                {
                                    sb.Append("<td class=\"numeric\"><div class=\"block_data\"  onmouseout=\"divonmouseout()\"  onmouseover=\"divonmouseover()\"><a href=\"javascript:;\" class=\"location_icon_import\"><div class=\"tooltip_import\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[2].Rows[j]["ExactAddress"]), string.Empty) + "</div></a></div></td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[2].Rows[j]["City"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[2].Rows[j]["State"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[2].Rows[j]["Country"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[2].Rows[j]["ZipCode"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[2].Rows[j]["Phone"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[2].Rows[j]["Fax"]), string.Empty) + "</td>");

                                }
                                else
                                {
                                    sb.Append("<td class=\"numeric\"><div class=\"block_data\"  onmouseout=\"divonmouseout()\"  onmouseover=\"divonmouseover()\"><a href=\"javascript:;\" class=\"location_icon_import\"><div class=\"tooltip_import\">" + string.Empty + "</div></a></div></td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");

                                }

                                if (dsImported.Tables[3].Rows.Count > 0)
                                {

                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[3].Rows[j]["FacebookUrl"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[3].Rows[j]["LinkedinUrl"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[3].Rows[j]["TwitterUrl"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[3].Rows[j]["BlogUrl"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[3].Rows[j]["YoutubeUrl"]), string.Empty) + "</td>");
                                }
                                else
                                {

                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                }

                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[0].Rows[j]["Description"]), string.Empty) + "</td>");
                                if (dsImported.Tables[4].Rows.Count > 0)
                                {

                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[4].Rows[j]["Institute"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[4].Rows[j]["YearAttended"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[4].Rows[j]["Specialisation"]), string.Empty) + "</td>");
                                }
                                else
                                {
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                }
                                if (dsImported.Tables[5].Rows.Count > 0)
                                {
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[5].Rows[j]["Membership"]), string.Empty) + "</td>");
                                }
                                else
                                {
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                }
                                if (dsImported.Tables[10].Rows.Count > 0)
                                {
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[10].Rows[j]["LicenseState"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[10].Rows[j]["LicenseNumber"]), string.Empty) + "</td>");
                                    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dsImported.Tables[10].Rows[j]["LicenseExpiration"]), string.Empty) + "</td>");
                                }
                                else
                                {
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                    sb.Append("<td class=\"numeric\">" + string.Empty + "</td>");
                                }

                                sb.Append("</tr>");

                            }


                        }
                    }


                    sb.Append("</tbody></table></div>");

                }
                else
                {
                    sb.Append("<br/><center class=\"CheckImportrecord\">No record imported</center>");
                }



            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        //NewImportPatientShowExcelMappedData
        public string NewImportPatientShowExcelMappedData(string Mapping)
        {
            return NewImportPatientShowExcel(Mapping);
        }
        public string ImportPatientShowExcelMappedData(string Mapping)
        {
            return ImportPatientShowExcel(Mapping);
        }
        //NEwImportPatientShowExcel
        public string NewImportPatientShowExcel(string MappedColumns)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dtExcelRecord = new DataTable();
                dtExcelRecord = (DataTable)Session["dtDoctorMapping"];

                string[] ArrayMappedColumns = MappedColumns.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);


                sb.Append("<div style=\"overflow:auto;width:100%;\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"patientTableImport\">");
                sb.Append("<thead><tr>");
                sb.Append("<th>AccountName</th>");
                sb.Append("<th>First Name</th>");
                sb.Append("<th>Last Name</th>");
                sb.Append("<th>Dental Specialty</th>");
                sb.Append("<th>Job Title</th>");
                sb.Append("<th>Address</th>");
                sb.Append("<th>City</th>");
                sb.Append("<th>State</th>");
                sb.Append("<th>Zip</th>");
                sb.Append("<th>Phone</th>");
                sb.Append("<th>Fax</th>");
                sb.Append("<th>Web Site</th>");
                sb.Append("<th>Password</th>");
                sb.Append("<th>Email</th>");
                sb.Append("<th>Photo</th>");
                sb.Append("<th>LicenseState</th>");
                sb.Append("<th>LicenseNumber</th>");
                sb.Append("<th>LicenseExpiration</th>");
                sb.Append("<th>Professional Memberships</th>");
                sb.Append("<th>Professional Memberships1</th>");
                sb.Append("<th>Professional Memberships2</th>");
                sb.Append("<th>Professional Memberships3</th>");
                sb.Append("<th>Professional Memberships4</th>");
                sb.Append("<th>Professional Memberships5</th>");
                sb.Append("<th>Professional Memberships6</th>");
                sb.Append("<th>School</th>");
                sb.Append("<th>Year Graduated</th>");
                sb.Append("<th>Degree</th>");
                sb.Append("<th>School1</th>");
                sb.Append("<th>Year Graduated1</th>");
                sb.Append("<th>Degree1</th>");
                sb.Append("<th>LinkedIn</th>");
                sb.Append("<th>Facebook</th>");
                sb.Append("<th>Twitter</th>");
                sb.Append("<th>Blog</th>");
                sb.Append("<th>YouTube</th>");
                sb.Append("</tr></thead>");
                sb.Append("<tbody>");

                foreach (DataRow item in dtExcelRecord.Rows)
                {

                    sb.Append("<tr>");
                    #region AccountName
                    if (ArrayMappedColumns[0] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[0]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region Firstname
                    if (ArrayMappedColumns[1] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[1]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion


                    #region LastName

                    if (ArrayMappedColumns[2] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[2]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region Dental Specialty
                    if (ArrayMappedColumns[3] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[3]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region Job Title
                    if (ArrayMappedColumns[4] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[4]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region Address
                    if (ArrayMappedColumns[5] != "0")
                    {
                        sb.Append("<td class=\"numeric\"><div class=\"block_data\"  onmouseout=\"divonmouseout()\"  onmouseover=\"divonmouseover()\" ><a href=\"javascript:;\" class=\"location_icon_import\"><div class=\"tooltip_import\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[5]]), string.Empty) + "</div></a></div></td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region City
                    if (ArrayMappedColumns[6] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[6]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region State
                    if (ArrayMappedColumns[7] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[7]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion


                    //#region Country
                    //if (ArrayMappedColumns[13] != "0")
                    //{
                    //    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[13]]), string.Empty) + "</td>");
                    //}
                    //else
                    //{
                    //    sb.Append("<td class=\"numeric\"></td>");
                    //}
                    #endregion

                    #region Zip
                    if (ArrayMappedColumns[8] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[8]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region Phone
                    if (ArrayMappedColumns[9] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[9]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region Fax
                    if (ArrayMappedColumns[10] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[10]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region Website
                    if (ArrayMappedColumns[11] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[11]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region Password
                    if (ArrayMappedColumns[12] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[12]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }

                    #endregion
                    #region Email
                    if (ArrayMappedColumns[13] != "0")
                    {
                        sb.Append("<td><div class=\"block_data\" onmouseout=\"divonmouseout()\"  onmouseover=\"divonmouseover()\"><a href=\"javascript:;\" class=\"message_icon_import\"><div class=\"tooltip_import\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[13]]), string.Empty) + "</div></a></div></td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region photo
                    if (ArrayMappedColumns[14] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[14]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region License State
                    if (ArrayMappedColumns[15] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[15]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region License Number
                    if (ArrayMappedColumns[16] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[16]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region License Exp
                    if (ArrayMappedColumns[17] != "0")
                    {
                        string str = Convert.ToString(item[ArrayMappedColumns[17]]);
                        if (!string.IsNullOrWhiteSpace(str))
                        {
                            try
                            {

                                //string[] newstring = str.Split(' ');
                                //str = str.Trim();
                                //TimeSpan datefromexcel = new TimeSpan(Convert.ToInt32(newstring[0]) - 2, 0, 0, 0);
                                //DateTime inputdate = new DateTime(1900, 1, 1).Add(datefromexcel);
                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[17]]), string.Empty) + "</td>");
                            }
                            catch (Exception ex)
                            {
                                StringBuilder sbc = new StringBuilder();
                                sbc.Append("<div style=\"width:100%;\" class=\"Disclass\" disabled=\"false\">");
                                sbc.Append("<p style=\"color:red\">str=" + str + "ex=" + ex.Message + ex.StackTrace + " License Expiration date is not correct format, please insert date as <b>mm/dd/yyyy</b></p>");
                                sbc.Append("</div>");
                                sbc.Append("<style>.next_step_large{display:none;}</style>");
                                return sbc.ToString();
                            }
                        }
                        else
                        {
                            sb.Append("<td class=\"numeric\"></td>");
                        }

                        //sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[29]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion


                    #region Professional Membership
                    if (ArrayMappedColumns[18] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[18]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region Professional Membership1
                    if (ArrayMappedColumns[19] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[19]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region Professional Membership2
                    if (ArrayMappedColumns[20] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[20]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region Professional Membership3
                    if (ArrayMappedColumns[21] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[21]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region Professional Membership4
                    if (ArrayMappedColumns[22] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[22]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region Professional Membership5
                    if (ArrayMappedColumns[23] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[23]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region Professional Membership6
                    if (ArrayMappedColumns[24] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[24]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region School
                    if (ArrayMappedColumns[25] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[25]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region Year Graduated
                    if (ArrayMappedColumns[26] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[26]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region Degree
                    if (ArrayMappedColumns[27] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[27]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region School1
                    if (ArrayMappedColumns[28] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[28]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region Year Graduated1
                    if (ArrayMappedColumns[29] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[29]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region Degree1
                    if (ArrayMappedColumns[30] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[30]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region LinkedIn

                    if (ArrayMappedColumns[31] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[31]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }

                    #endregion
                    #region Facebook

                    if (ArrayMappedColumns[32] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[32]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }

                    #endregion

                    #region Twitter
                    if (ArrayMappedColumns[33] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[33]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region Blog
                    if (ArrayMappedColumns[34] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[34]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region YouTube
                    if (ArrayMappedColumns[35] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[35]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    //#region Description
                    //if (ArrayMappedColumns[22] != "0")
                    //{
                    //    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[22]]), string.Empty) + "</td>");
                    //}
                    //else
                    //{
                    //    sb.Append("<td class=\"numeric\"></td>");
                    //}
                    //#endregion


                    //#region Professional Memberships
                    //if (ArrayMappedColumns[26] != "0")
                    //{
                    //    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[26]]), string.Empty) + "</td>");
                    //}
                    //else
                    //{
                    //    sb.Append("<td class=\"numeric\"></td>");
                    //}
                    //#endregion



                    sb.Append("</tr>");
                }


                sb.Append("</tbody></table></div>");

            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        public string ImportPatientShowExcel(string MappedColumns)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dtExcelRecord = new DataTable();
                dtExcelRecord = (DataTable)Session["dtDoctorMapping"];

                string[] ArrayMappedColumns = MappedColumns.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);


                sb.Append("<div style=\"overflow:auto;width:100%;\"><table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"patientTableImport\">");
                sb.Append("<thead><tr>");
                sb.Append("<th>First Name</th>");
                sb.Append("<th>Middle Name</th>");
                sb.Append("<th>Last Name</th>");
                sb.Append("<th>PublicPath</th>");
                sb.Append("<th>AccountName</th>");
                sb.Append("<th>Email</th>");
                sb.Append("<th>Password</th>");
                sb.Append("<th>Web Site</th>");
                // sb.Append("<th>Photo</th>");
                sb.Append("<th>Dental Specialty</th>");
                sb.Append("<th>Job Title</th>");
                sb.Append("<th>Address</th>");
                sb.Append("<th>City</th>");
                sb.Append("<th>State</th>");
                sb.Append("<th>Country</th>");
                sb.Append("<th>Zip</th>");
                sb.Append("<th>Phone</th>");
                sb.Append("<th>Fax</th>");

                sb.Append("<th>Facebook</th>");
                sb.Append("<th>LinkedIn</th>");
                sb.Append("<th>Twitter</th>");
                sb.Append("<th>Blog</th>");
                sb.Append("<th>YouTube</th>");
                sb.Append("<th>Description</th>");
                sb.Append("<th>School</th>");
                sb.Append("<th>Year Graduated</th>");
                sb.Append("<th>Degree</th>");
                sb.Append("<th>Professional Memberships</th>");
                sb.Append("<th>LicenseState</th>");
                sb.Append("<th>LicenseNumber</th>");
                sb.Append("<th>LicenseExpiration</th>");



                sb.Append("</tr></thead>");
                sb.Append("<tbody>");

                foreach (DataRow item in dtExcelRecord.Rows)
                {

                    sb.Append("<tr>");

                    #region Firstname


                    if (ArrayMappedColumns[0] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[0]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region MiddleName


                    if (ArrayMappedColumns[1] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[1]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region LastName
                    if (ArrayMappedColumns[2] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[2]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region PublicPath
                    if (ArrayMappedColumns[3] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[3]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion


                    #region AccountName
                    if (ArrayMappedColumns[4] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[4]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region Email
                    if (ArrayMappedColumns[5] != "0")
                    {
                        sb.Append("<td><div class=\"block_data\" onmouseout=\"divonmouseout()\"  onmouseover=\"divonmouseover()\"><a href=\"javascript:;\" class=\"message_icon_import\"><div class=\"tooltip_import\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[5]]), string.Empty) + "</div></a></div></td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region Password
                    if (ArrayMappedColumns[6] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[6]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }

                    #endregion
                    #region Website
                    if (ArrayMappedColumns[7] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[7]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region Photo
                    //if (ArrayMappedColumns[8] != "0")
                    //{
                    //    sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[8]]), string.Empty) + "</td>");
                    //}
                    //else
                    //{
                    //    sb.Append("<td class=\"numeric\"></td>");
                    //}
                    #endregion
                    #region Dental Specialty
                    if (ArrayMappedColumns[8] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[8]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region Job Title
                    if (ArrayMappedColumns[9] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[9]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region Address
                    if (ArrayMappedColumns[10] != "0")
                    {
                        sb.Append("<td class=\"numeric\"><div class=\"block_data\"  onmouseout=\"divonmouseout()\"  onmouseover=\"divonmouseover()\" ><a href=\"javascript:;\" class=\"location_icon_import\"><div class=\"tooltip_import\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[10]]), string.Empty) + "</div></a></div></td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region City
                    if (ArrayMappedColumns[11] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[11]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region State
                    if (ArrayMappedColumns[12] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[12]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion


                    #region Country
                    if (ArrayMappedColumns[13] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[13]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region Zip
                    if (ArrayMappedColumns[14] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[14]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion


                    #region Phone


                    if (ArrayMappedColumns[15] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[15]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region Fax
                    if (ArrayMappedColumns[16] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[16]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region Facebook

                    if (ArrayMappedColumns[17] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[17]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }

                    #endregion

                    #region LinkedIn

                    if (ArrayMappedColumns[18] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[18]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }

                    #endregion

                    #region Twitter
                    if (ArrayMappedColumns[19] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[19]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region Blog
                    if (ArrayMappedColumns[20] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[20]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region YouTube
                    if (ArrayMappedColumns[21] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[21]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region Description
                    if (ArrayMappedColumns[22] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[22]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region School
                    if (ArrayMappedColumns[23] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[23]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region Year Graduated
                    if (ArrayMappedColumns[24] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[24]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region Degree
                    if (ArrayMappedColumns[25] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[25]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region Professional Memberships
                    if (ArrayMappedColumns[26] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[26]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region License State
                    if (ArrayMappedColumns[27] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[27]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region License Number
                    if (ArrayMappedColumns[28] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[28]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion
                    #region License Exp
                    if (ArrayMappedColumns[29] != "0")
                    {
                        string str = Convert.ToString(item[ArrayMappedColumns[29]]);
                        try
                        {
                            //TimeSpan datefromexcel = new TimeSpan(Convert.ToInt32(item[ArrayMappedColumns[29]]) - 2, 0, 0, 0);
                            //DateTime inputdate = new DateTime(1900, 1, 1).Add(datefromexcel);
                            //sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(inputdate), string.Empty) + "</td>");
                        }
                        catch (Exception ex)
                        {
                            StringBuilder sbc = new StringBuilder();
                            sbc.Append("<div style=\"width:100%;\" class=\"Disclass\" disabled=\"false\">");
                            sbc.Append("<p style=\"color:red\">str=" + str + "ex=" + ex.Message + ex.StackTrace + " License Expiration date is not correct format, please insert date as <b>mm/dd/yyyy</b></p>");
                            sbc.Append("</div>");
                            sbc.Append("<style>.next_step_large{display:none;}</style>");
                            return sbc.ToString();
                        }

                        //sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[29]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion


                    sb.Append("</tr>");
                }


                sb.Append("</tbody></table></div>");

            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        public DataTable getExcelColumn()
        {
            DataTable dt1 = new DataTable();
            try
            {
                OleDbConnection objConnection = new OleDbConnection(ConnectionString());
                objConnection.Open();

                DataTable tbl = objConnection.GetSchema("Tables");





                string sheetName = (string)tbl.Rows[0]["TABLE_NAME"];

                OleDbCommand objCmdSelect = new OleDbCommand("SELECT TOP 1 * FROM [" + sheetName + "]", objConnection);
                OleDbDataAdapter objAdapter = new OleDbDataAdapter(objCmdSelect);
                DataTable Dt = new DataTable();
                objAdapter.Fill(Dt);
                dt1 = new DataTable("Excel");


                dt1.Columns.Add("ExcelColumn", typeof(String));





                foreach (DataColumn dc in Dt.Columns)
                {
                    dt1.Rows.Add(new object[] { dc.ColumnName });

                }
                objConnection.Close();
                return dt1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private DataSet ReadExcel(string filePath, string Extension, bool isHDR)
        {
            FileStream stream = System.IO.File.Open(filePath, FileMode.Open, FileAccess.Read);
            IExcelDataReader excelReader = null;

            switch (Extension)
            {
                case ".xls":
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                    break;
                case ".xlsx": //Excel 07
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    break;
            }

            excelReader.IsFirstRowAsColumnNames = isHDR;
            DataSet result = excelReader.AsDataSet();

            while (excelReader.Read())
            {

            }

            excelReader.Close();

            return result;

        }
        private String ConnectionString()
        {
            return "Provider=Microsoft.ACE.OLEDB.12.0;" +
                "Data Source=" + FileNameNew + "; " +
                "Extended Properties=\"Excel 12.0;HDR=YES;\"";
        }
        public DataTable getExcelColumnData()
        {

            try
            {
                OleDbConnection objConnection = new OleDbConnection(ConnectionString());
                objConnection.Open();

                DataTable Table2 = objConnection.GetSchema("Tables");


                string sheetName = (string)Table2.Rows[0]["TABLE_NAME"];

                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + sheetName + "]", objConnection);
                OleDbDataAdapter objAdapter = new OleDbDataAdapter(objCmdSelect);
                DataTable Dt = new DataTable();
                objAdapter.Fill(Dt);

                objConnection.Close();
                return Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }






        #region Export Doctors

        public ActionResult ExportDoctors()
        {
            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                TempData["pagecount"] = "0";
                return View();
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        [HttpPost]
        public ActionResult ExportDoctors(string id)
        {

            DataTable dt = new DataTable();

            dt = Session["TotalExportDoctor"] as DataTable;

            Session["TotalExportDoctor"] = null;
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Remove("ColleagueId");
                dt.Columns.Remove("TotalCount");
                dt.AcceptChanges();
                ObjCommon.ExportToExcel(dt, "Doctor_Records");
            }
            return null;
        }

        public int GetTotalDoctorCount()
        {
            objAdmin = new clsAdmin();
            DataTable dt = new DataTable();
            dt = objAdmin.GetActiveInActiveUserList(1, 1, "", 1, null);
            int totalcount = 0;
            decimal total = 0;
            if (dt.Rows.Count > 0)
            {
                total = Convert.ToInt32(dt.Rows[0]["Totalcount"]);
                totalcount = Convert.ToInt32(Math.Round(total / 500));
                // totalcount = total;
            }
            return totalcount;
        }

        public ActionResult GetAllDoctor()
        {
            DataTable dt = new DataTable();
            //dt = objAdmin.GetActiveInActiveUserList(1, 50, "", 1, null);

            int totalcall = GetTotalDoctorCount();
            int currentpagecall = 0;
            if (Request["hddnpagecount"] != null)
            {
                currentpagecall = Convert.ToInt32(Request["hddnpagecount"]);
            }

            objAdmin = new clsAdmin();
            DataTable dt1 = new DataTable();
            string WebsiteURL = Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/p/";
            dt = objAdmin.GetActiveInActiveUserList(currentpagecall + 1, 500, "", 1, null, WebsiteURL);
            if (Session["TotalExportDoctor"] == null)
            {
                Session["TotalExportDoctor"] = dt;
            }
            else
            {
                dt1 = Session["TotalExportDoctor"] as DataTable;
                if (dt.Rows.Count > 0)
                {
                    dt1.Merge(dt);
                }
                Session["TotalExportDoctor"] = dt1;
            }
            TempData["pagecount"] = currentpagecall + 1;
            TempData["totalcount"] = totalcall;
            return View("ExportDoctors");
        }
        //Old Method to export doctor by ajax
        public JsonResult ExportDoctorsSplit(int Paging)
        {
            clsCommon objCommon = new clsCommon();
            try
            {
                objAdmin = new clsAdmin();
                DataTable dt = new DataTable();
                DataTable dt1 = new DataTable();

                dt = objAdmin.GetActiveInActiveUserList(Paging, 50, "", 1, null);

                if (Session["TotalExportDoctor"] == null)
                {
                    Session["TotalExportDoctor"] = dt;
                }
                else
                {
                    dt1 = Session["TotalExportDoctor"] as DataTable;
                    if (dt.Rows.Count > 0)
                    {
                        dt1.Merge(dt);
                    }

                    Session["TotalExportDoctor"] = dt1;
                }

            }
            catch (Exception)
            {
                throw;

            }
            return Json(true);
        }
        #endregion


        #region Logged Info

        public ActionResult LoggedInfo()
        {

            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                ViewBag.tblScriptLoggedInfo = LoggedInfoTable(1, "", 3, 2);
                return View();
            }
            else
            {
                return RedirectToAction("Index", "User");
            }

        }

        private string LoggedInfoTable(int PageIndex, string searchtext, int SortColumn, int SortDirection)
        {

            StringBuilder sb = new StringBuilder();
            StringBuilder sbPageing = new StringBuilder();
            double TotalPages = 0;
            int size = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

            int NewSortDirection = 1;
            if (SortDirection == 1)
            {
                NewSortDirection = 2;
            }
            objAdmin = new clsAdmin();
            DataTable dt = new DataTable();
            dt = objAdmin.GetUserLoggedInfo(PageIndex, size, searchtext, SortColumn, SortDirection);

            if (dt.Rows.Count > 0)
            {
                sb.Append("<div class=\"table_data reports_ref_received clearfix\">");
                sb.Append("<div class=\"clear\"></div>");
                sb.Append("<table class=\"responsiveTable\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" id=\"tblLoggedInfo\" data-sortcolumn=\"" + SortColumn + "\" data-sortirection=\"" + SortDirection + "\">");
                sb.Append("<thead>");
                sb.Append("<tr>");
                sb.Append("<th><a href=\"javascript:;\" onclick=\"GetListLoggedInfoTable(\'" + 1 + "\',\'" + 1 + "\',\'" + NewSortDirection + "\','" + searchtext + "')\" class=\"sortable\">Name</a></th>");
                sb.Append("<th><a href=\"javascript:;\" onclick=\"GetListLoggedInfoTable(\'" + 1 + "\','" + 2 + "\',\'" + NewSortDirection + "\','" + searchtext + "')\" class=\"sortable\">Referrals</a></th>");
                sb.Append("<th><a href=\"javascript:;\" onclick=\"GetListLoggedInfoTable(\'" + 1 + "\','" + 4 + "\',\'" + NewSortDirection + "\','" + searchtext + "')\" class=\"sortable\">Colleagues</a></th>");
                sb.Append("<th><a href=\"javascript:;\" onclick=\"GetListLoggedInfoTable(\'" + 1 + "\','" + 3 + "\',\'" + NewSortDirection + "\','" + searchtext + "')\" class=\"sortable\">Last logged In</a></th>");
                sb.Append("<th style=\"display:none;\"><a href=\"javascript:;\" onclick=\"GetListLoggedInfoTable(\'" + (PageIndex) + "\','" + 1 + "\',\'" + NewSortDirection + "\','" + searchtext + "')\" class=\"sortable\">Action</a></th>");
                sb.Append("</tr>");
                sb.Append("</thead>");
                sb.Append("<tbody id=\"lsittbody\">");
                TotalPages = Math.Ceiling(Convert.ToDouble(dt.Rows[0]["TotalCount"].ToString()) / size);


                foreach (DataRow item in dt.Rows)
                {

                    string strLoggedInDate = "-";
                    if (!string.IsNullOrEmpty(Convert.ToString(item["LoginTime"])))
                    {
                        strLoggedInDate = new CommonBLL().ConvertToDate(Convert.ToDateTime(item["LoginTime"]), (int)BO.Enums.Common.DateFormat.GENERAL);
                    }
                    sb.Append("<tr>");
                    sb.Append("<td><h5 onclick=\"location.href='" + Url.Action("ViewProfile", "Admin", new { UserId = item["ColleagueId"].ToString() }) + "'\" style='cursor:pointer'>" + item["FirstName"].ToString() + "&nbsp;" + item["LastName"].ToString() + "</h5>");
                    if (item["Specialities"].ToString() != null && item["Specialities"].ToString() != "")
                    {
                        sb.Append("<p>" + item["Specialities"].ToString() + ", " + item["Title"].ToString() + "</p>");
                    }
                    sb.Append("</td>");
                    sb.Append("<td>" + (Convert.ToInt32(item["ReferralSentCount"].ToString()) + Convert.ToInt32(item["ReferralReceivedCount"].ToString())) + "</td>");
                    sb.Append("<td>" + item["ColleagueCount"].ToString() + "</td>");// Static Code 
                    sb.Append("<td>" + strLoggedInDate + "</td>");
                    sb.Append("<td style=\"display:none;\"><input class=\"remove\" name=\"Remove\" type=\"submit\" onclick=\"UpdateUserStatus('" + item["ColleagueId"].ToString() + "','2')\"></td>");
                    sb.Append("</tr>");
                }

                sb.Append("</tbody>");
                sb.Append("</table> </div>");
            }
            else
            {
                sb.Append("<br/><center>No Record Found</center>");
            }






            return sb.ToString();

        }

        public string GetLoggedInfoTable(string index, string searchtext, string SortColumn, string SortDirection)
        {
            return LoggedInfoTable(Convert.ToInt32(index), searchtext, Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection));
        }
        public string GetLoggedInfoTableContent(string index, string searchtext, string SortColumn, string SortDirection)
        {
            StringBuilder sb = new StringBuilder();
            StringBuilder sbPageing = new StringBuilder();
            int size = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"].ToString());

            int NewSortDirection = 1;
            if (Convert.ToInt32(SortDirection) == 1)
            {
                NewSortDirection = 2;
            }
            objAdmin = new clsAdmin();
            DataTable dt = new DataTable();
            dt = objAdmin.GetUserLoggedInfo(Convert.ToInt32(index), Convert.ToInt32(size), searchtext, Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection));



            if (dt.Rows.Count > 0)
            {

                foreach (DataRow item in dt.Rows)
                {
                    string strLoggedInDate = "-";
                    if (!string.IsNullOrEmpty(Convert.ToString(item["LoginTime"])))
                    {
                        strLoggedInDate = new CommonBLL().ConvertToDate(Convert.ToDateTime(item["LoginTime"]), (int)BO.Enums.Common.DateFormat.GENERAL);
                    }

                    sb.Append("<tr>");
                    sb.Append("<td><h5>" + item["FirstName"].ToString() + "&nbsp;" + item["LastName"].ToString() + "</h5>");
                    if (item["Specialities"].ToString() != null && item["Specialities"].ToString() != "")
                    {
                        sb.Append("<p>" + item["Specialities"].ToString() + ", " + item["Title"].ToString() + "</p>");
                    }
                    sb.Append("</td>");
                    sb.Append("<td>" + (Convert.ToInt32(item["ReferralSentCount"].ToString()) + Convert.ToInt32(item["ReferralReceivedCount"].ToString())) + "</td>");
                    sb.Append("<td>" + item["ColleagueCount"].ToString() + "</td>");// Static Code 
                    sb.Append("<td>" + strLoggedInDate + "</td>");
                    sb.Append("<td style=\"display:none;\"><input class=\"remove\" name=\"Remove\" type=\"submit\" onclick=\"UpdateUserStatus('" + item["ColleagueId"].ToString() + "','2')\"></td>");
                    sb.Append("</tr>");
                }

            }
            else if (Convert.ToInt32(index) > 1)
            {
                sb.Append("<tr><td colspan=\"4\"><center>No more record found</center></td></tr>");
            }
            else
            {
                sb.Append("<tr><td colspan=\"4\"><center>No Record Found</center></td></tr>");
            }

            return sb.ToString();

        }


        #endregion

        #region Send Email

        public ActionResult SendEmail()
        {

            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                if (Request["UserId"] != null && Request["UserId"] != "")
                {
                    ViewBag.UserIds = Convert.ToString(Request["UserId"]);
                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        public JsonResult BindListofEmailTemplates()
        {
            return Json(ListofEmailTemplates(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetEmailTemplateDetailsByTemplateId(string Id)
        {
            object obj = string.Empty;
            string TemplateName = string.Empty;
            string TemplateSubject = string.Empty;
            string TemplateBody = string.Empty;
            List<EmailTemplate> lstEmailTemplate = new List<EmailTemplate>();
            try
            {
                MdlAdmin = new mdlAdmin();

                lstEmailTemplate = MdlAdmin.GetEmailTemplateById(Convert.ToInt32(Id));

                if (lstEmailTemplate.Count > 0)
                {
                    TemplateName = lstEmailTemplate[0].TemplateName;
                    TemplateSubject = lstEmailTemplate[0].TemplateSubject;
                    TemplateBody = lstEmailTemplate[0].TemplateDesc;
                }

            }
            catch (Exception)
            {

                throw;
            }
            return Json(new { TemplateName = TemplateName, TemplateSubject = TemplateSubject, TemplateBody = TemplateBody }, JsonRequestBehavior.AllowGet);
        }

        public string ListofEmailTemplates()
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();
                dt = objAdmin.GetTemplatesAdminCanSend();
                if (dt != null && dt.Rows.Count > 0)
                {
                    int rowcount = 0;
                    foreach (DataRow item in dt.Rows)
                    {
                        //priya
                        sb.Append("<li><label><input class=\"checkallbox\" style=\" margin-left:-140px !important;\" id=\"Check" + rowcount + "\" value=\"" + ObjCommon.CheckNull(Convert.ToString(item["TemplateId"]), string.Empty) + "\" name=\"\" type=\"checkbox\" onclick=\"selectOnlyThis(this.id)\">" + ObjCommon.CheckNull(Convert.ToString(item["TemplateName"]), string.Empty) + "</label></li>");
                        ++rowcount;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult SaveAndSendTemplateToUser(string ToUserId, string TemplateId, string TemplateName, string TemplateSubject, string TemplateBody, string Type)
        {
            object obj = string.Empty;
            try
            {
                string[] arryselected = ToUserId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var item in arryselected)
                {
                    // Add Email History 

                    bool add = ObjTemplate.SendEmailFromAdmin(Convert.ToInt32(item), Convert.ToInt32(TemplateId), TemplateName, TemplateSubject, TemplateBody);
                    if (add)
                    {
                        obj = "1";
                    }

                    if (Type == "Save")
                    {
                        // Save as New Template in system
                        bool result = objAdmin.AddEmailTempalteInSystem(TemplateName, TemplateBody, TemplateSubject, 0);
                        if (result == false)
                        {
                            obj = "2";
                        }
                        else
                        {
                            obj = "1";
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetUserList(string selected)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();
                dt = objAdmin.GetActiveInActiveUserList(1, PageSize, "", 1, null);

                sb.Append("<select id=\"select3\"  name=\"select3\">");



                string[] arryselected = selected.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var item in arryselected)
                {

                    DataSet ds = new DataSet();
                    ds = objColleaguesData.GetDoctorDetailsById(Convert.ToInt32(item));
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        sb.Append("<option value=\"" + ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["UserId"]), string.Empty) + "\" class=\"selected\">" + ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FullName"]), string.Empty) + "</option>");

                    }
                    else
                    {
                        sb.Append("<option value=\"" + ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["UserId"]), string.Empty) + "\">" + ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FullName"]), string.Empty) + "</option>");
                    }
                }

                sb.Append("</select>");




            }
            catch (Exception)
            {

                throw;
            }

            return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Email History of User
        public ActionResult EmailHistory(int UserId = 0)
        {
            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                if (UserId != 0)
                {
                    ViewBag.GetEmailHistory = GetEmailHistoryByUserId(UserId, 1, 1, 2, null);
                    return View();
                }
                else
                {
                    return RedirectToAction("Dashboard", "Admin");
                }
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }


        [HttpPost]
        public JsonResult GetEmailHistoryOfUser(string UserId, string PageIndex, string SortColumn, string SortDirection, string Searchtext)
        {
            return Json(GetEmailHistoryByUserId(Convert.ToInt32(UserId), Convert.ToInt32(PageIndex), Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection), Searchtext), JsonRequestBehavior.AllowGet);
        }


        public string GetEmailHistoryByUserId(int UserId, int PageIndex, int SortColumn, int SortDirection, string Searchtext)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                objAdmin = new clsAdmin();
                DataTable dt = new DataTable();
                dt = objAdmin.GetAllEmailHistoryByUserId(UserId, PageIndex, PageSize, SortColumn, SortDirection, Searchtext);
                if (dt.Rows.Count > 0)
                {
                    //nimesh
                    double TotalPages = Math.Ceiling(Convert.ToDouble(dt.Rows[0]["TotalRecord"]) / PageSize);
                    sb.Append("");
                    sb.Append("<div class=\"table_data reports_ref_received clearfix\"><h5>Email History<span class=\"buttons\"></span><div class=\"heading_panel\"><input class=\"remove\" type=\"submit\" name=\"Remove\" onclick=\"return RemoveEmailToSelected()\" /></div></h5><div class=\"clear\"></div>");

                    sb.Append("<table class=\"manageTemplate\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"tblEmailHistory\" data-userid=\"" + UserId + "\" data-sortcolumn=\"" + SortColumn + "\" data-sortirection=\"" + SortDirection + "\">");
                    sb.Append("<thead><tr>");
                    sb.Append("<th><input name=\"\" id=\"CheckAllEmail\" onclick=\"SetAllCheckBoxes(this)\" type=\"checkbox\" value=\"\"></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetEmailHistory('" + UserId + "','" + PageIndex + "','" + 1 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\">Date</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetEmailHistory('" + UserId + "','" + PageIndex + "','" + 2 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\">subject</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetEmailHistory('" + UserId + "','" + PageIndex + "','" + 3 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\">Description</a></th>");
                    sb.Append("</tr></thead>");
                    sb.Append("<tbody id=\"tblbodycontent\">");

                    foreach (DataRow item in dt.Rows)
                    {
                        string strEmailSentDate = string.Empty;
                        if (!string.IsNullOrEmpty(Convert.ToString(item["EmailSentDate"])))
                        {
                            strEmailSentDate = new CommonBLL().ConvertToDate(Convert.ToDateTime(item["EmailSentDate"]), (int)BO.Enums.Common.DateFormat.GENERAL);
                        }

                        string Body = string.Empty;
                        Body = Regex.Replace(Regex.Replace(ObjCommon.RemoveHTML(item["EmailContent"].ToString()), @"<[^>]+>|&nbsp;", string.Empty), @"[\r\n\t]+", string.Empty);
                        sb.Append("<tr id=\"row" + item["EmailId"] + "\"><td><input name=\"\" id=\"checkemail\" type=\"checkbox\" value=\"" + ObjCommon.CheckNull(Convert.ToString(item["EmailId"]), string.Empty) + "\" onclick=\"UncheckMaincheckBox('CheckAllEmail')\"></td>");
                        sb.Append("<td><a href=\"Javascript:;\"  style=\"color: #515152;text-decoration:none;\" onclick=\"GetSentEmailHistoryById('" + ObjCommon.CheckNull(Convert.ToString(item["EmailId"]), string.Empty) + "')\">" + strEmailSentDate + "</a></td>");
                        sb.Append("<td><a href=\"Javascript:;\"  style=\"color: #515152;text-decoration:none;\"  onclick=\"GetSentEmailHistoryById('" + ObjCommon.CheckNull(Convert.ToString(item["EmailId"]), string.Empty) + "')\">" + ObjCommon.CheckNull(Convert.ToString(item["EmailSubject"]), string.Empty) + "</a></td>");
                        sb.Append("<td><a href=\"Javascript:;\"  style=\"color: #515152;text-decoration:none;\" onclick=\"GetSentEmailHistoryById('" + ObjCommon.CheckNull(Convert.ToString(item["EmailId"]), string.Empty) + "')\">" + (Body.Length > 40 ? Body.Substring(0, 40) + "..." : Body) + "</a></td>");
                        sb.Append("</tr>");
                    }
                    sb.Append("</tbody></table></div>");

                }
                else
                {
                    sb.Append("<br/><center>No Record found</center>");
                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }


        public string GetEmailHistoryOfUserContent(string UserId, string PageIndex, string SortColumn, string SortDirection, string Searchtext)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                objAdmin = new clsAdmin();
                DataTable dt = new DataTable();
                dt = objAdmin.GetAllEmailHistoryByUserId(Convert.ToInt32(UserId), Convert.ToInt32(PageIndex), Convert.ToInt32(PageSize), Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection), Searchtext);


                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        string strEmailSentDate = string.Empty;
                        if (!string.IsNullOrEmpty(Convert.ToString(item["EmailSentDate"])))
                        {
                            strEmailSentDate = new CommonBLL().ConvertToDate(Convert.ToDateTime(item["EmailSentDate"]), (int)BO.Enums.Common.DateFormat.GENERAL);
                        }
                        string Body = string.Empty;
                        Body = Regex.Replace(Regex.Replace(ObjCommon.RemoveHTML(item["EmailContent"].ToString()), @"<[^>]+>|&nbsp;", string.Empty), @"[\r\n\t]+", string.Empty);
                        sb.Append("<tr id=\"row" + item["EmailId"] + "\"><td><input name=\"\" id=\"checkemail\" type=\"checkbox\" value=\"" + ObjCommon.CheckNull(Convert.ToString(item["EmailId"]), string.Empty) + "\" onclick=\"UncheckMaincheckBox('CheckAllEmail')\"></td>");
                        sb.Append("<td><a href=\"Javascript:;\"  style=\"color: #515152;text-decoration:none;\" onclick=\"GetSentEmailHistoryById('" + ObjCommon.CheckNull(Convert.ToString(item["EmailId"]), string.Empty) + "')\">" + strEmailSentDate + "</a></td>");
                        sb.Append("<td><a href=\"Javascript:;\"  style=\"color: #515152;text-decoration:none;\"  onclick=\"GetSentEmailHistoryById('" + ObjCommon.CheckNull(Convert.ToString(item["EmailId"]), string.Empty) + "')\">" + ObjCommon.CheckNull(Convert.ToString(item["EmailSubject"]), string.Empty) + "</a></td>");
                        sb.Append("<td><a href=\"Javascript:;\"  style=\"color: #515152;text-decoration:none;\" onclick=\"GetSentEmailHistoryById('" + ObjCommon.CheckNull(Convert.ToString(item["EmailId"]), string.Empty) + "')\">" + (Body.Length > 40 ? Body.Substring(0, 40) + "..." : Body) + "</a></td>");
                        sb.Append("</tr>");
                    }


                }
                else if (Convert.ToInt32(PageIndex) > 1)
                {
                    sb.Append("<tr><td colspan=\"4\"><center>No more record found</center></td></tr>");
                }
                else
                {
                    sb.Append("<tr><td colspan=\"4\"><center>No Record found</center></td></tr>");
                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        [HttpPost]
        public JsonResult RemoveEmailOfUser(string selected)
        {
            object obj = string.Empty;
            try
            {
                bool result = false;
                string[] arryselected = selected.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var item in arryselected)
                {
                    result = objAdmin.DeleteAdminEmailHistoryById(Convert.ToInt32(item));
                    obj = "1";
                }

            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetSentEmailHistoryById(string Id)
        {
            object obj = string.Empty;
            try
            {
                DataTable dt = new DataTable();
                dt = objAdmin.GetSentEmailHistoryById(Convert.ToInt32(Id));
                if (dt != null && dt.Rows.Count > 0)
                {
                    obj = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["EmailContent"]), string.Empty);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region SignsUpEmail
        public ActionResult SignsUpEmail()
        {
            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                ViewBag.SignsUpEmail = GetAllSignsUpEmail(1, null, 2, 2);
                return View();
            }
            else
            {
                return RedirectToAction("Index", "User");
            }

        }

        [HttpPost]
        public JsonResult GetListOfSignsUpEmail(string PageIndex, string Searchtext, string SortColumn, string SortDirection)
        {
            return Json(GetAllSignsUpEmail(Convert.ToInt32(PageIndex), Searchtext, Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection)), JsonRequestBehavior.AllowGet);
        }

        public string GetAllSignsUpEmail(int PageIndex, string Searchtext, int SortColumn, int SortDirection)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();
                dt = objAdmin.GetAllUserSignUpWithJustEmail(PageIndex, PageSize, Searchtext, SortColumn, SortDirection);
                if (dt != null && dt.Rows.Count > 0)
                {
                    double TotalPages = Math.Ceiling(Convert.ToDouble(dt.Rows[0]["TotalRecord"]) / PageSize);
                    sb.Append("<div class=\"table_data reports_ref_received clearfix\">");
                    sb.Append("<div class=\"heading_panel\">");
                    sb.Append("<input class=\"remove\" name=\"Remove\" type=\"submit\" onclick=\"ActionToSelected('remove','" + PageIndex + "')\" />&nbsp;");
                    sb.Append("<input class=\"active_btn_small\" name=\"Active\" type=\"submit\" onclick=\"ActionToSelected('active','" + PageIndex + "')\" />&nbsp;");
                    sb.Append("<input class=\"block\" name=\"Active\" type=\"submit\" onclick=\"ActionToSelected('block','" + PageIndex + "')\" />&nbsp;");
                    sb.Append("<input class=\"send_reminder\" name=\"Active\" type=\"submit\" onclick=\"ActionToSelected('sendreminder','" + PageIndex + "')\" />");
                    sb.Append("</div>");
                    sb.Append("<div class=\"clear\"></div>");
                    sb.Append("<table class=\"signupTable\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"tblsignupTable\" data-sortcolumn=\"" + SortColumn + "\" data-sortirection=\"" + SortDirection + "\">");
                    sb.Append("<thead>");
                    sb.Append("<tr>");
                    sb.Append("<th><input name=\"\" id=\"checkAllemail\" type=\"checkbox\" value=\"\" onclick=\"SetAllCheckBoxes(this)\"></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetListOfSignsUpEmail('" + PageIndex + "','" + Searchtext + "','" + 1 + "','" + (SortDirection == 1 ? 2 : 1) + "')\" >Email</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetListOfSignsUpEmail('" + PageIndex + "','" + Searchtext + "','" + 2 + "','" + (SortDirection == 1 ? 2 : 1) + "')\" >date</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetListOfSignsUpEmail('" + PageIndex + "','" + Searchtext + "','" + 3 + "','" + (SortDirection == 1 ? 2 : 1) + "')\" >Phone</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetListOfSignsUpEmail('" + PageIndex + "','" + Searchtext + "','" + 2 + "','" + (SortDirection == 1 ? 2 : 1) + "')\" >Action</a></th>");
                    sb.Append("</tr>");
                    sb.Append("</thead>");
                    sb.Append("<tbody id=\"tblbodycontent\">");

                    foreach (DataRow item in dt.Rows)
                    {
                        string strEmailDate = string.Empty;
                        if (!string.IsNullOrEmpty(Convert.ToString(item["date"])))
                        {
                            strEmailDate = new CommonBLL().ConvertToDateTime(Convert.ToDateTime(item["date"]), (int)BO.Enums.Common.DateFormat.GENERAL);
                        }

                        sb.Append("<tr id=\"row" + item["id"] + "\">");
                        sb.Append("<td><input name=\"\" type=\"checkbox\" id=\"checkemail\"  value=\"" + ObjCommon.CheckNull(Convert.ToString(item["id"]), string.Empty) + "\" onclick=\"UncheckMaincheckBox('checkAllemail')\"></td>");
                        sb.Append("<td>");
                        string Name = ObjCommon.CheckNull(Convert.ToString(item["name"]), string.Empty);
                        if (!string.IsNullOrEmpty(Name))
                        {
                            sb.Append("<h5>" + Name + "</h5>");
                        }
                        else
                        {
                            sb.Append("<h5>&nbsp;</h5>");
                        }


                        sb.Append("<p>" + ObjCommon.CheckNull(Convert.ToString(item["email"]), string.Empty) + "</p>");

                        sb.Append("</td>");
                        sb.Append("<td>" + strEmailDate + "</td>");
                        sb.Append("<td>" + ObjCommon.CheckNull(Convert.ToString(item["phoneno"]), string.Empty) + "</td>");
                        sb.Append("<td><input class=\"remove\" name=\"Remove\" type=\"submit\" onclick=\"RemoveSignupJustEmail('" + ObjCommon.CheckNull(Convert.ToString(item["id"]), string.Empty) + "','" + PageIndex + "')\"/></br><input class=\"active_btn_small\" name=\"Active\" type=\"submit\" onclick=\"ActiveUser('" + ObjCommon.CheckNull(Convert.ToString(item["id"]), string.Empty) + "')\" /></td>");
                        sb.Append("</tr>");
                    }


                    sb.Append("</tbody></table></div>");
                }
                else
                {
                    sb.Append("<center>No record found</center>");
                }
            }
            catch (Exception)
            {
                throw;
            }
            return sb.ToString();
        }

        public string GetListOfSignsUpEmailContent(string PageIndex, string Searchtext, string SortColumn, string SortDirection)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();
                dt = objAdmin.GetAllUserSignUpWithJustEmail(Convert.ToInt32(PageIndex), Convert.ToInt32(PageSize), Searchtext, Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection));
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        string strEmailDate = string.Empty;
                        if (!string.IsNullOrEmpty(Convert.ToString(item["date"])))
                        {
                            strEmailDate = new CommonBLL().ConvertToDate(Convert.ToDateTime(item["date"]), (int)BO.Enums.Common.DateFormat.GENERAL);
                        }

                        sb.Append("<tr id=\"row" + item["id"] + "\">");
                        sb.Append("<td><input name=\"\" type=\"checkbox\" id=\"checkemail\"  value=\"" + ObjCommon.CheckNull(Convert.ToString(item["id"]), string.Empty) + "\" onclick=\"UncheckMaincheckBox('checkAllemail')\"></td>");
                        sb.Append("<td>");
                        string Name = ObjCommon.CheckNull(Convert.ToString(item["name"]), string.Empty);
                        if (!string.IsNullOrEmpty(Name))
                        {
                            sb.Append("<h5>" + Name + "</h5>");
                        }
                        else
                        {
                            sb.Append("<h5>&nbsp;</h5>");
                        }


                        sb.Append("<p>" + ObjCommon.CheckNull(Convert.ToString(item["email"]), string.Empty) + "</p>");

                        sb.Append("</td>");
                        sb.Append("<td>" + strEmailDate + "</td>");
                        sb.Append("<td>" + ObjCommon.CheckNull(Convert.ToString(item["phoneno"]), string.Empty) + "</td>");
                        sb.Append("<td><input class=\"remove\" name=\"Remove\" type=\"submit\" onclick=\"RemoveSignupJustEmail('" + ObjCommon.CheckNull(Convert.ToString(item["id"]), string.Empty) + "','" + PageIndex + "')\"/></br><input class=\"active_btn_small\" name=\"Active\" type=\"submit\" onclick=\"ActiveUser('" + ObjCommon.CheckNull(Convert.ToString(item["id"]), string.Empty) + "')\" /></td>");
                        sb.Append("</tr>");
                    }
                }
                else if (Convert.ToInt32(PageIndex) > 1)
                {
                    sb.Append("<tr><td colspan=\"5\"><center>No more record found</center><td><tr>");
                }
                else
                {
                    sb.Append("<tr><td colspan=\"5\"><center>No record found</center><td><tr>");
                }
            }
            catch (Exception)
            {
                throw;
            }
            return sb.ToString();

        }

        [HttpPost]
        public JsonResult RemoveSignupJustEmail(string Id)
        {
            object obj = string.Empty;
            try
            {
                string[] arryId = Id.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var item in arryId)
                {
                    bool result = objAdmin.RemoveSignup_JustEmail(Convert.ToInt32(item));
                    if (result)
                    {
                        obj = "1";
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult BlockSignupJustEmail(string Id)
        {
            object obj = string.Empty;
            try
            {
                string[] arryId = Id.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var item in arryId)
                {
                    bool result = objAdmin.UpdateStatusInTempTableUserById(Convert.ToInt32(item), 2);
                    if (result)
                    {
                        obj = "1";
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult ActiveSignupJustEmail(string Id)
        {
            object obj = string.Empty;
            try
            {
                string[] arryId = Id.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in arryId)
                {
                    DataTable dt = new DataTable();
                    dt = objAdmin.GetTempSignupById(Convert.ToInt32(item));// get details from temp_signup table by Id
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        string Email = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["email"]), string.Empty);
                        // we need to code here to sign up that user from temp table in our system
                        string[] name = Email.Split(new char[] { '@' }, StringSplitOptions.RemoveEmptyEntries);
                        string Password = ObjCommon.CreateRandomPassword(8);
                        string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                        string Accountkey = Crypto.SHA1(Convert.ToString(DateTime.Now.Ticks));



                        if (dt.Rows.Count > 0)
                        {
                            int newuserId = objColleaguesData.SingUpDoctor(Email, EncPassword,
                                (ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["first_name"]), name[0])),
                                (ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["last_name"]), name[0])), null, null, null, Accountkey, null, null, null, null, null, null, (ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["phoneno"]), string.Empty)), null, false, ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["facebook"]), string.Empty), ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["linkedin"]), string.Empty), ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["twitter"]), string.Empty), null, null, ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["description"]), string.Empty), null, null, null, null, null, null, null, 0, null, null, null, 0);
                            bool result = objAdmin.UpdateStatusInTempTableUserById(Convert.ToInt32(item), 1);// update status 1 in temp table 
                            ObjTemplate.NewUserEmailFormat(Email, Session["CompanyWebsite"].ToString() + "/User/Index?UserName=" + Email + "&Password=" + EncPassword, Password, Session["CompanyWebsite"].ToString());
                            if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["image_url"])))
                            {
                                string makefilename = "$" + System.DateTime.Now.Ticks + "$" + (ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["first_name"]), name[0])) + ".png";
                                string rootPath = "~/DentistImages/";
                                string fileName = System.IO.Path.Combine(Server.MapPath(rootPath), makefilename);
                                System.Drawing.Image image = ObjCommon.DownloadImageFromUrl(ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["image_url"]), name[0]));
                                bool IsUploadImage = objColleaguesData.UpdateDoctorProfileImageById(Convert.ToInt32(newuserId), makefilename);
                                image.Save(fileName);

                            }
                        }

                        obj = "1";
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SendReminderSignupJustEmail(string Id)
        {
            object obj = string.Empty;
            try
            {
                string[] arryId = Id.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in arryId)
                {
                    DataTable dt = new DataTable();
                    dt = objAdmin.GetTempSignupById(Convert.ToInt32(item));
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        string Email = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["email"]), string.Empty);
                        string encryptedpwd = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["password"]), string.Empty);
                        string password = ObjTripleDESCryptoHelper.decryptText(encryptedpwd);
                        string signuppagename = Session["CompanyWebsite"].ToString() + "/User/Index?UserName=" + Email + "&Password=" + encryptedpwd;
                        ObjTemplate.ReminderMailNotification(Email, signuppagename, password);
                        obj = "1";
                    }
                }

            }
            catch (Exception)
            {
                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region ManageContent
        public ActionResult ManageContent()
        {

            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                ViewBag.ManageContentList = ManageContentList(1, 1, null);
                return View();
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        [HttpPost]
        public JsonResult GetAllManageContentList(string PageIndex, string SelectedValue, string Searchtext)
        {
            return Json(ManageContentList(Convert.ToInt32(PageIndex), Convert.ToInt32(SelectedValue), Searchtext), JsonRequestBehavior.AllowGet);
        }

        public string ManageContentList(int PageIndex, int SelectedValue, string Searchtext)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                if (SelectedValue == 4)
                {

                    sb.Append("<div class=\"table_data reports_ref_received clearfix\">");

                    sb.Append("<div style=\"width: 300px; float: right;\">");
                    sb.Append("<select id=\"standard-dropdown\" name=\"standard-dropdown\" class=\"select_menu_inbox custom-class1 custom-class2 right\" style=\"width: 150px;\" onchange=\"OptionChange()\">");
                    if (SelectedValue == 1)
                    {
                        sb.Append("<option value=\"1\" class=\"test-class-1\" selected=\"selected\">Institution Attended</option>");
                    }
                    else
                    {
                        sb.Append("<option value=\"1\" class=\"test-class-1\">Institution Attended</option>");
                    }

                    if (SelectedValue == 2)
                    {
                        sb.Append("<option value=\"2\" class=\"test-class-2\" selected=\"selected\" >Professional Membership</option>");
                    }
                    else
                    {
                        sb.Append("<option value=\"2\" class=\"test-class-2\">Professional Membership</option>");
                    }
                    if (SelectedValue == 3)
                    {
                        sb.Append("<option value=\"3\" class=\"test-class-3\" selected=\"selected\" >Speciality</option>");
                    }
                    else
                    {
                        sb.Append("<option value=\"3\" class=\"test-class-3\">Speciality</option>");
                    }


                    if (SelectedValue == 4)
                    {
                        sb.Append("<option value=\"4\" class=\"test-class-3\" selected=\"selected\" >Page Content</option>");
                    }
                    else
                    {
                        sb.Append("<option value=\"4\" class=\"test-class-3\">Page Content</option>");
                    }



                    sb.Append("</select><br/></div>");
                    sb.Append("<div class=\"clear\"></div>");

                    sb.Append("<div class=\"email_template form_listing\">");
                    sb.Append("<ul>");
                    // we use Searchtext parameter to identify which page is selected from dropdown list ddlselectpage
                    sb.Append("<li><label>Select Page:</label>");
                    List<SelectListItem> lst = new List<SelectListItem>();
                    MdlCommon = new mdlCommon();
                    lst = MdlCommon.CustomPageNames();
                    sb.Append("<select id=\"ddlselectpage\" name=\"ddlselectpage\" style=\"width:78%;\" onchange=\"OptionPageChange()\">");

                    foreach (var item in lst)
                    {
                        if (Searchtext == item.Value)
                        {
                            sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                        }
                        else
                        {
                            sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                        }
                    }
                    sb.Append("</select>");



                    sb.Append("</select></li>");
                    if (!string.IsNullOrEmpty(Searchtext))
                    {
                        DataTable dtdata = new DataTable();
                        dtdata = objAdmin.GetAllCustomPageDetailsById(Convert.ToInt32(Searchtext));
                        if (dtdata != null && dtdata.Rows.Count > 0)
                        {
                            sb.Append("<li><label>Page Name :</label><input type=\"text\" value=\"" + ObjCommon.CheckNull(Convert.ToString(dtdata.Rows[0]["PageName"]), string.Empty) + "\" id=\"txttempatename\" /></li>");
                            sb.Append("<li><label>Description:</label><span class=\"email_data\" style=\"float:right;width:78%;\"><textarea rows=\"150\" cols=\"50\" id=\"txttempatebody\" name=\"txttempatebody\">" + ObjCommon.CheckNull(Convert.ToString(dtdata.Rows[0]["PageDesc"]), string.Empty) + "</textarea></span></li>");
                            sb.Append("</ul><div style=\"width: 100%;\"><div style=\"width: 50%;float:left;\"><div class=\"actions\" style=\"margin:20px 0 0 0\"><input type=\"submit\" class=\"submit_new_btn\" onclick=\"return AddEditPageContent('" + ObjCommon.CheckNull(Convert.ToString(dtdata.Rows[0]["Id"]), "0") + "')\"  /></div></div></div>");
                        }

                    }
                    else
                    {
                        sb.Append("<li><label>Page Name :</label><input type=\"text\" value=\"\" id=\"txttempatename\" /></li>");
                        sb.Append("<li><label>Description:</label><span class=\"email_data\" style=\"float:right;width:78%;\"><textarea rows=\"\" cols=\"\" id=\"txttempatebody\" name=\"txttempatebody\"></textarea></span></li>");
                        sb.Append("</ul><div style=\"width: 100%;\"><div style=\"width: 50%;float:left;\"><div class=\"actions\" style=\"margin:20px 0 0 0\"><input type=\"submit\" class=\"submit_new_btn\" onclick=\"return AddEditPageContent('0')\"  /></div></div></div>");
                    }


                    sb.Append("</div>");

                    sb.Append("</div><br/>");
                }

                else
                {
                    DataTable dt = new DataTable();
                    dt = objAdmin.GetCustomSettingDetials(PageIndex, PageSize, SelectedValue, Searchtext);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        // SelectedValue =1 for Institution Attended,2 for  Professional Membership,3 for Specialty
                        double TotalPages = Math.Ceiling(Convert.ToDouble(dt.Rows[0]["TotalRecord"].ToString()) / PageSize);
                        sb.Append("<div class=\"table_data reports_ref_received clearfix\">");
                        sb.Append("<div style=\"width: auto; float: right; margin-left: 10px;\">");

                        sb.Append("<input class=\"add_new_membership\" name=\"edit\" type=\"submit\" onclick=\"GetCustomSettingDetialsById('0','" + SelectedValue + "','Add')\" />");

                        sb.Append("</div>");
                        sb.Append("<div style=\"width: 300px; float: right;\">");
                        sb.Append("<select id=\"standard-dropdown\" name=\"standard-dropdown\" class=\"select_menu_inbox custom-class1 custom-class2 right\" style=\"width: 150px;\" onchange=\"OptionChange()\">");
                        if (SelectedValue == 1)
                        {
                            sb.Append("<option value=\"1\" class=\"test-class-1\" selected=\"selected\">Institution Attended</option>");
                        }
                        else
                        {
                            sb.Append("<option value=\"1\" class=\"test-class-1\">Institution Attended</option>");
                        }

                        if (SelectedValue == 2)
                        {
                            sb.Append("<option value=\"2\" class=\"test-class-2\" selected=\"selected\" >Professional Membership</option>");
                        }
                        else
                        {
                            sb.Append("<option value=\"2\" class=\"test-class-2\">Professional Membership</option>");
                        }
                        if (SelectedValue == 3)
                        {
                            sb.Append("<option value=\"3\" class=\"test-class-3\" selected=\"selected\" >Speciality</option>");
                        }
                        else
                        {
                            sb.Append("<option value=\"3\" class=\"test-class-3\">Speciality</option>");
                        }


                        if (SelectedValue == 4)
                        {
                            sb.Append("<option value=\"4\" class=\"test-class-3\" selected=\"selected\" >Page Content</option>");
                        }
                        else
                        {
                            sb.Append("<option value=\"4\" class=\"test-class-3\">Page Content</option>");
                        }



                        sb.Append("</select></div>");
                        sb.Append("<div class=\"clear\"></div>");


                        sb.Append("<table class=\"responsiveTable manage_content\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">");
                        sb.Append("<tbody id=\"tblManageContentList\">");

                        foreach (DataRow item in dt.Rows)
                        {
                            sb.Append("<tr id=\"row" + item["Id"] + "\">");
                            sb.Append("<td></td>");
                            sb.Append("<td><p id=\"" + item["Id"] + "\">" + ObjCommon.CheckNull(Convert.ToString(item["Content"]), string.Empty) + "</p></td>");
                            sb.Append("<td>");
                            sb.Append("<input class=\"edit_btn\" name=\"edit\" type=\"submit\" onclick=\"GetCustomSettingDetialsById('" + ObjCommon.CheckNull(Convert.ToString(item["Id"]), string.Empty) + "','" + SelectedValue + "','Edit')\" />");
                            sb.Append("</td>");
                            sb.Append("</tr>");
                        }
                        sb.Append("</tbody></table>");
                    }
                    else
                    {
                        sb.Append("<br/><center>No record found</center>");
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        public string GetAllManageContentListContent(string PageIndex, string SelectedValue, string Searchtext)
        {

            StringBuilder sb = new StringBuilder();
            try
            {
                if (Convert.ToInt32(SelectedValue) != 4)
                {
                    DataTable dt = new DataTable();
                    dt = objAdmin.GetCustomSettingDetials(Convert.ToInt32(PageIndex), Convert.ToInt32(PageSize), Convert.ToInt32(SelectedValue), Searchtext);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            sb.Append("<tr id=\"row" + item["Id"] + "\">");
                            sb.Append("<td></td>");
                            sb.Append("<td><p id=\"" + item["Id"] + "\">" + ObjCommon.CheckNull(Convert.ToString(item["Content"]), string.Empty) + "</p></td>");
                            sb.Append("<td>");
                            sb.Append("<input class=\"edit_btn\" name=\"edit\" type=\"submit\" onclick=\"GetCustomSettingDetialsById('" + ObjCommon.CheckNull(Convert.ToString(item["Id"]), string.Empty) + "','" + SelectedValue + "','Edit')\" />");
                            sb.Append("</td>");
                            sb.Append("</tr>");
                        }
                    }
                    else if (Convert.ToInt32(PageIndex) > 1)
                    {
                        sb.Append("<tr><td colspan=\"3\"><center>No record found</center></td></tr>");
                    }
                    else
                    {
                        sb.Append("<tr><td colspan=\"3\"><center>No record found</center></td></tr>");
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();

        }


        [HttpPost]
        [ValidateInput(false)]
        public JsonResult AddEditPageContent(string Id, string PageName, string PageDesc)
        {
            object obj = string.Empty;
            try
            {
                bool result = objAdmin.AddEditPageContentById(Convert.ToInt32(Id), PageName, PageDesc);
                if (result)
                {
                    obj = "1";
                }
            }
            catch (Exception)
            {
                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetCustomSettingDetialsById(string Id, string SelectedValue, string Type)
        {
            return Json(ManageContentPopup(Id, SelectedValue, Type), JsonRequestBehavior.AllowGet);
        }


        public string ManageContentPopup(string Id, string SelectedValue, string Type)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();
                string Content = string.Empty;




                if (Type == "Edit")
                {
                    dt = objAdmin.GetCustomSettingDetialsByID(Convert.ToInt32(Id), Convert.ToInt32(SelectedValue));
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        Content = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["Content"]), string.Empty);
                    }
                    sb.Append("<h2 class=\"title\">Edit Content</h2>");

                }
                else
                {
                    sb.Append("<h2 class=\"title\">Add Content</h2>");
                }


                sb.Append("<div class=\"cntnt\">");
                sb.Append("<ul class=\"tablelist\"><li><label>Name:</label><input id=\"txtcustomsettingcontent\" type=\"text\" name=\"Search\" value=\"" + Content + "\"></li></ul>");
                sb.Append("<div class=\"actions\">");
                sb.Append("<input class=\"save_btn\" name=\"Save\" type=\"submit\" value=\"Save\" onclick=\"return InsertandUpdateContent('" + Id + "','" + SelectedValue + "','" + Type + "')\" />");

                if (Type == "Edit")
                {
                    sb.Append("<input type=\"submit\" value=\"Save\" name=\"Save\" class=\"remove_btn\" onclick=\"RemoveContentById('" + Id + "','" + SelectedValue + "')\" />");
                }


                sb.Append("<input class=\"cancel_btn\" name=\"Cancel\" type=\"submit\" value=\"Cancel\" onclick=\"closepopup()\" />");

                sb.Append("<div class=\"clear\"></div></div></div>");

            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }


        [HttpPost]
        public JsonResult RemoveContentById(string Id, string SeletedType)
        {
            object obj = string.Empty;
            try
            {
                bool result = objAdmin.RemoveCustomSettingDetials(Convert.ToInt32(Id), Convert.ToInt32(SeletedType));
                if (result)
                {
                    obj = "1";
                }

            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult InsertandUpdateContent(string Id, string SelectedValue, string Type, string EditString)
        {
            object obj = string.Empty;
            try
            {
                bool result = false;
                if (Type == "Add")
                {
                    result = objAdmin.AddCustomSettingDetials(0, Convert.ToInt32(SelectedValue), EditString);
                }
                else
                {
                    result = objAdmin.EditCustomSettingDetials(Convert.ToInt32(Id), Convert.ToInt32(SelectedValue), EditString);
                }
                if (result)
                {
                    obj = "1";
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ManageAccount
        public ActionResult ManageAccount()
        {

            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                ViewBag.ManageAccount = ListOfManageAccountUsers(1, "");
                return View();
            }
            else
            {
                return RedirectToAction("Index", "User");
            }

        }

        [HttpPost]
        public JsonResult ManageAccountUsers(string PageIndex, string Searchtext)
        {
            return Json(ListOfManageAccountUsers(Convert.ToInt32(PageIndex), Searchtext), JsonRequestBehavior.AllowGet);
        }

        public string ListOfManageAccountUsers(int PageIndex, string Searchtext)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                objAdmin = new clsAdmin();
                DataTable dt = new DataTable();
                dt = objAdmin.GetActiveInActiveUserList(PageIndex, PageSize, Searchtext, 1, null);

                if (dt != null && dt.Rows.Count > 0)
                {

                    double TotalPages = Math.Ceiling(Convert.ToDouble(dt.Rows[0]["TotalCount"].ToString()) / PageSize);
                    sb.Append("<div class=\"table_data reports_ref_received clearfix\">");
                    sb.Append("<h5>Manage account<span class=\"buttons\"></span></h5>");
                    sb.Append("<div class=\"clear\"></div>");
                    sb.Append("<table class=\"manageAccountList\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">");
                    sb.Append("<tbody id=\"tblbodycontent\">");

                    foreach (DataRow item in dt.Rows)
                    {

                        if (!string.IsNullOrWhiteSpace(item["FirstName"].ToString()) || !string.IsNullOrWhiteSpace(item["LastName"].ToString()) || !string.IsNullOrWhiteSpace(item["Title"].ToString()) || !string.IsNullOrWhiteSpace(item["OfficeName"].ToString()) || !string.IsNullOrWhiteSpace(item["Specialities"].ToString()))
                        {
                            string FullName = Convert.ToString(item["FirstName"]);
                            FullName += (string.IsNullOrEmpty(FullName) ? "" : "&nbsp;") + Convert.ToString(item["LastName"]);
                            sb.Append("<tr id=\"row" + item["ColleagueId"] + "\">");
                            FullName += (string.IsNullOrEmpty(FullName + Convert.ToString(item["Title"])) ? "" : ", ") + Convert.ToString(item["Title"]);
                            sb.Append("<tr id=\"row" + item["ColleagueId"] + "\">");
                            sb.Append("<td><p >" + FullName + "  <br/> <span id=\"" + item["ColleagueId"] + "\">" + ObjCommon.CheckNull(Convert.ToString(item["OfficeName"]), string.Empty) + " </span> <br/>" + ObjCommon.CheckNull(Convert.ToString(item["Specialities"]), string.Empty) + "</p></td>");
                            sb.Append("<td><input class=\"edit_btn\" name=\"Remove\" type=\"submit\" onclick=\"ManageAccountNamePopUp('" + ObjCommon.CheckNull(Convert.ToString(item["ColleagueId"]), string.Empty) + "','" + ObjCommon.CheckNull(Convert.ToString(item["OfficeName"]), string.Empty) + "')\"/></td>");
                            sb.Append("</tr>");
                        }
                    }

                    sb.Append("</tbody></table></div>");
                }
                else
                {
                    sb.Append("<center>No record found</center>");
                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        public string ManageAccountUsersContent(string PageIndex, string Searchtext)
        {

            StringBuilder sb = new StringBuilder();
            try
            {
                objAdmin = new clsAdmin();
                DataTable dt = new DataTable();
                dt = objAdmin.GetActiveInActiveUserList(Convert.ToInt32(PageIndex), Convert.ToInt32(PageSize), Searchtext, 1, null);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        if (!string.IsNullOrWhiteSpace(item["FirstName"].ToString()) || !string.IsNullOrWhiteSpace(item["LastName"].ToString()) || !string.IsNullOrWhiteSpace(item["Title"].ToString()) || !string.IsNullOrWhiteSpace(item["OfficeName"].ToString()) || !string.IsNullOrWhiteSpace(item["Specialities"].ToString()))
                        {
                            sb.Append("<tr id=\"row" + item["ColleagueId"] + "\">");
                            sb.Append("<td><p >" + ObjCommon.CheckNull(Convert.ToString(item["FirstName"]), string.Empty) + "&nbsp;" + ObjCommon.CheckNull(Convert.ToString(item["LastName"]), string.Empty) + (ObjCommon.CheckNull(Convert.ToString(item["Title"]), string.Empty) != string.Empty ? " ," + ObjCommon.CheckNull(Convert.ToString(item["Title"]), string.Empty) : ObjCommon.CheckNull(Convert.ToString(item["Title"]), string.Empty)) + " <br/> <span id=\"" + item["ColleagueId"] + "\">" + ObjCommon.CheckNull(Convert.ToString(item["OfficeName"]), string.Empty) + "</span> <br/>" + ObjCommon.CheckNull(Convert.ToString(item["Specialities"]), string.Empty) + "</p></td>");
                            sb.Append("<td><input class=\"edit_btn\" name=\"Remove\" type=\"submit\" onclick=\"ManageAccountNamePopUp('" + ObjCommon.CheckNull(Convert.ToString(item["ColleagueId"]), string.Empty) + "','" + ObjCommon.CheckNull(Convert.ToString(item["OfficeName"]), string.Empty) + "')\"/></td>");
                            sb.Append("</tr>");
                        }
                    }
                }
                else if (Convert.ToInt32(PageIndex) > 1)
                {
                    sb.Append("<tr><td colspan=\"2\"><center>No more record found</center></td><tr>");
                }
                else
                {
                    sb.Append("<tr><td colspan=\"2\"><center>No Record Found</center></td><tr>");
                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        [HttpPost]
        public JsonResult ManageAccountNamePopUp(string UserId, string AccounName)
        {
            return Json(EditAccountNamePopUp(Convert.ToInt32(UserId), AccounName), JsonRequestBehavior.AllowGet);
        }

        public string EditAccountNamePopUp(int UserId, string AccounName)
        {
            DataTable dt = objAdmin.GetAccountNameOfDoctorToEdit(UserId);

            StringBuilder sb = new StringBuilder();
            try
            {

                sb.Append("<h2 class=\"title\">Edit Manage Account</h2>");
                sb.Append("<div class=\"cntnt\">");
                sb.Append("<ul class=\"tablelist\"><li><label>Name:</label><input type=\"text\" id=\"txteditaccountpopup\" name=\"Search\" value=\"" + (dt.Rows.Count > 0 ? dt.Rows[0]["AccountName"] : "") + "\"></li></ul>");
                sb.Append("<div class=\"actions\">");
                sb.Append("<input class=\"save_btn\" name=\"Save\" type=\"submit\" value=\"Save\" onclick=\"return UpdateOfficeName('" + UserId + "')\">");
                sb.Append("<input class=\"cancel_btn\" name=\"Cancel\" type=\"submit\" value=\"Cancel\" onclick=\"closepopup()\">");
                sb.Append("<div class=\"clear\"></div></div></div>");

            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();


        }


        [HttpPost]
        public JsonResult UpdateAccountName(string UserId, string AccountName)
        {
            object obj = string.Empty;
            try
            {
                bool result = objAdmin.UpdateAccountNameOfDoctor(Convert.ToInt32(UserId), AccountName);
                if (result)
                {
                    obj = "1";
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region ManageCompany
        public ActionResult ManageCompany()
        {
            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                ViewBag.ManageCompany = GetListOfCompany(1, 3, 1, null);
                return View();
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        [HttpPost]
        public JsonResult GetAllComapanyList(string PageIndex, string SortColumn, string SortDirection, string Searchtext)
        {
            return Json(GetListOfCompany(Convert.ToInt32(PageIndex), Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection), Searchtext), JsonRequestBehavior.AllowGet);
        }

        public string GetListOfCompany(int PageIndex, int SortColumn, int SortDirection, string Searchtext)
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                DataTable dt = new DataTable();
                dt = objAdmin.GetAllComapanyList(PageIndex, PageSize, SortColumn, SortDirection, Searchtext);

                if (dt != null && dt.Rows.Count > 0)
                {
                    double TotalPages = Math.Ceiling(Convert.ToDouble(dt.Rows[0]["TotalRecord"].ToString()) / PageSize);
                    sb.Append("<div class=\"table_data reports_ref_received clearfix\">");
                    sb.Append("<h5>Manage Company<span class=\"buttons\"></span></h5>");
                    sb.Append("<div class=\"heading_panel\"><input onclick= \"Companydetails('" + 0 + "')\" class=\"add_new_company\" name=\"Edit Btn\" type=\"submit\" /></div>");


                    sb.Append("<div class=\"clear\"></div>");
                    sb.Append("<table class=\"manageCompanyList\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"tblComapanyList\" data-sortcolumn=\"" + SortColumn + "\" data-sortirection=\"" + SortDirection + "\">");
                    sb.Append("<thead>");
                    sb.Append("<tr>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetAllCompany('" + PageIndex + "','" + 1 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\" >Name</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetAllCompany('" + PageIndex + "','" + 2 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\" >Website</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetAllCompany('" + PageIndex + "','" + 3 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\" >Action</a></th>");
                    sb.Append("</tr>");
                    sb.Append("</thead>");
                    sb.Append("<tbody id=\"tblbodycontent\">");


                    foreach (DataRow item in dt.Rows)
                    {
                        var strCompanyWebsiteUrl = ObjCommon.CheckNull(Convert.ToString(item["CompanyWebsite"]), string.Empty);
                        if (strCompanyWebsiteUrl.ToLower().Contains("http://") || strCompanyWebsiteUrl.ToLower().Contains("https://"))
                        {

                        }
                        else
                        {
                            strCompanyWebsiteUrl = "//" + strCompanyWebsiteUrl;
                        }


                        sb.Append("<tr>");
                        sb.Append("<td>" + ObjCommon.CheckNull(Convert.ToString(item["CompanyName"]), string.Empty) + "</td>");
                        sb.Append("<td><a  target=\"_blank\" href=\"" + strCompanyWebsiteUrl + "\">" + ObjCommon.CheckNull(Convert.ToString(item["CompanyWebsite"]), string.Empty) + "</a></td>");
                        sb.Append("<td><a href=\"" + Url.Action("ManageCompanyAddresses", "Admin", new { CompanyId = ObjCommon.CheckNull(Convert.ToString(item["CompanyId"]), string.Empty) }) + "\" ><input class=\"address_btn\" name=\"Send Email\" type=\"submit\" /></a>");

                        if (ObjCommon.CheckNull(Convert.ToString(item["Isactive"]), string.Empty) == "Active")
                        {
                            sb.Append("<input class=\"active_btn\" name=\"Send Password\" type=\"submit\" /> &nbsp;&nbsp;");
                        }


                        sb.Append("<a href=\"Javascript:;\" ><input class=\"edit_btn\" name=\"Edit Btn\" type=\"submit\" onclick= \"Companydetails('" + ObjCommon.CheckNull(Convert.ToString(item["CompanyId"]), string.Empty) + "')\"   /></a></td>");
                        sb.Append("</tr>");
                    }
                    sb.Append("</tbody></table></div>");
                }
                else
                {
                    sb.Append("<center>No record found</center>");
                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        public string GetAllComapanyListContent(string PageIndex, string SortColumn, string SortDirection, string Searchtext)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();
                dt = objAdmin.GetAllComapanyList(Convert.ToInt32(PageIndex), Convert.ToInt32(PageSize), Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection), Searchtext);

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        var strCompanyWebsiteUrl = ObjCommon.CheckNull(Convert.ToString(item["CompanyWebsite"]), string.Empty);
                        if (strCompanyWebsiteUrl.ToLower().Contains("http://") || strCompanyWebsiteUrl.ToLower().Contains("https://"))
                        {
                        }
                        else
                        {
                            strCompanyWebsiteUrl = "//" + strCompanyWebsiteUrl;
                        }
                        sb.Append("<tr>");
                        sb.Append("<td>" + ObjCommon.CheckNull(Convert.ToString(item["CompanyName"]), string.Empty) + "</td>");
                        sb.Append("<td><a  target=\"_blank\" href=\"" + strCompanyWebsiteUrl + "\">" + ObjCommon.CheckNull(Convert.ToString(item["CompanyWebsite"]), string.Empty) + "</a></td>");
                        sb.Append("<td><a href=\"" + Url.Action("ManageCompanyAddresses", "Admin", new { CompanyId = ObjCommon.CheckNull(Convert.ToString(item["CompanyId"]), string.Empty) }) + "\" ><input class=\"address_btn\" name=\"Send Email\" type=\"submit\" /></a>");

                        if (ObjCommon.CheckNull(Convert.ToString(item["Isactive"]), string.Empty) == "Active")
                        {
                            sb.Append("<input class=\"active_btn\" name=\"Send Password\" type=\"submit\" /> &nbsp;&nbsp;");
                        }


                        sb.Append("<a href=\"Javascript:;\" ><input class=\"edit_btn\" name=\"Edit Btn\" type=\"submit\" onclick= \"Companydetails('" + ObjCommon.CheckNull(Convert.ToString(item["CompanyId"]), string.Empty) + "')\"   /></a></td>");
                        sb.Append("</tr>");
                    }
                    sb.Append("</tbody></table></div>");
                }
                else if (Convert.ToInt32(PageIndex) > 1)
                {
                    sb.Append("<tr><td colspan=\"3\"><center>No more record found</center></td></tr>");
                }
                else
                {
                    sb.Append("<tr><td colspan=\"3\"><center>No record found</center></td></tr>");
                }
            }
            catch (Exception)
            {
                throw;
            }
            return sb.ToString();
        }
        #endregion

        #region AddNewCompany
        //string CompanyId

        void LoadCompanyData(int CompnyID)
        {
            DataTable dt = new DataTable();
            MdlCompany = new mdlCompany();
            dt = objAdmin.GetComapanyDetailsById(CompnyID);
            if (dt != null && dt.Rows.Count > 0)
            {
                MdlCompany.CompanyId = Convert.ToInt32(dt.Rows[0]["CompanyId"]);
                MdlCompany.CompanyName = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyName"]), string.Empty);
                MdlCompany.Connectionstring = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyConnectionstring"]), string.Empty);
                MdlCompany.CompanyWebsite = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyWebsite"]), string.Empty);
                MdlCompany.SalesEmail = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["SalesEmail"]), string.Empty);
                MdlCompany.SupportEmail = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["SupportEmail"]), string.Empty);

                MdlCompany.PhoneNo = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["PhoneNo"]), string.Empty);
                MdlCompany.CompanyOwnerName = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyOwnerName"]), string.Empty);
                MdlCompany.CompanyOwnerTitle = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyOwnerTitle"]), string.Empty);
                MdlCompany.CompanyOwnerEmail = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyOwnerEmail"]), string.Empty);
                MdlCompany.CompanyFacebook = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyFacebook"]), string.Empty);

                MdlCompany.CompanyBlog = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyBlog"]), string.Empty);
                MdlCompany.CompanyYoutube = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyYoutube"]), string.Empty);
                MdlCompany.CompanyLinkedin = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyLinkedin"]), string.Empty);
                MdlCompany.CompanyTwitter = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyTwitter"]), string.Empty);
                MdlCompany.CompanyGooglePlus = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyGooglePlus"]), string.Empty);

                MdlCompany.CompanyFacebookLike = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyFacebookLike"]), string.Empty);
                MdlCompany.CompanyAlexa = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyAlexa"]), string.Empty);
                MdlCompany.oldPath = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["LogoPath"]), string.Empty);
                MdlCompany.LogoPath = string.Empty;
                MdlCompany.SMTPServer = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["SMTPServer"]), string.Empty);
                MdlCompany.SMTPPort = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["SMTPPort"]), string.Empty);
                MdlCompany.SMTPLogin = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["SMTPLogin"]), string.Empty);

                MdlCompany.SMTPPassword = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["SMTPPassword"]), string.Empty);
                MdlCompany.SMTPSsl = Convert.ToBoolean(Convert.ToString(DBNull.Value == dt.Rows[0]["SMTPSsl"] ? false : dt.Rows[0]["SMTPSsl"]));
                MdlCompany.CompanyAddress1 = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyAddress1"]), string.Empty);
                MdlCompany.CompanyAddress2 = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyAddress2"]), string.Empty);
                MdlCompany.CompanyCity = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyCity"]), string.Empty);
                MdlCompany.CompanyState = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyState"]), string.Empty);
                MdlCompany.CompanyZipCode = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyZipCode"]), string.Empty);
                MdlCompany.CompanyCountry = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyCountry"]), string.Empty);
                MdlCompany.Isactive = Convert.ToBoolean(Convert.ToString(DBNull.Value == dt.Rows[0]["Isactive"] ? false : dt.Rows[0]["Isactive"]));
            }
        }

        public ActionResult AddNewCompany()
        {
            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                if (Request["hdncompanyid"] != null && Request["hdncompanyid"] != null)
                {
                    int id = 0;
                    if (int.TryParse(Request["hdncompanyid"], out id))
                    {
                        LoadCompanyData(id);
                        return View("AddNewCompany", MdlCompany);
                    }
                    else
                    {
                        return RedirectToAction("ManageCompany", MdlCompany);
                    }
                }
                else
                {
                    return RedirectToAction("ManageCompany", MdlCompany);
                }
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        [HttpPost]
        public ActionResult AddCompnyLogo(HttpPostedFileBase fuCompanyLogo, string id)
        {
            bool HasFile = false;
            String filename1 = string.Empty;
            if (fuCompanyLogo != null)
            {
                HasFile = true;
                string extension = Path.GetExtension(fuCompanyLogo.FileName);
                filename1 = "$" + System.DateTime.Now.Ticks + "$" + fuCompanyLogo.FileName;
                fuCompanyLogo.SaveAs(Server.MapPath(TempLoadFile + filename1));
            }
            if (!String.IsNullOrEmpty(id))
                LoadCompanyData(Convert.ToInt32(id));
            else
                LoadCompanyData(0);
            if (HasFile)
                MdlCompany.LogoPath = TempLoadFile.Replace("~", "") + filename1;

            return View("AddNewCompany", MdlCompany);
        }
        [HttpPost]
        public JsonResult AddEditCompany(mdlCompany ObjCompany)
        {
            bool result = false;
            if (!String.IsNullOrEmpty(ObjCompany.LogoPath))
            {
                if (System.IO.File.Exists(Server.MapPath(ObjCompany.LogoPath)))
                {
                    System.IO.File.Move(Server.MapPath(ObjCompany.LogoPath), Server.MapPath(UploadCompanyLogo + ObjCompany.LogoPath.Substring(ObjCompany.LogoPath.LastIndexOf('/') + 1)));
                    System.IO.File.Delete(Server.MapPath(ObjCompany.LogoPath));
                    ObjCompany.LogoPath = UploadCompanyLogo + ObjCompany.LogoPath.Substring(ObjCompany.LogoPath.LastIndexOf('/') + 1);
                }
            }
            else
            {
                ObjCompany.LogoPath = ObjCompany.oldPath;
            }
            if (ObjCompany.CompanyId != 0)
            {
                result = objAdmin.UpdateComapanyList(ObjCompany.CompanyId,
                    ObjCompany.CompanyName, ObjCompany.Connectionstring, ObjCompany.CompanyWebsite,
                    ObjCompany.SalesEmail, ObjCompany.SupportEmail, ObjCompany.PhoneNo, ObjCompany.CompanyOwnerName,
                    ObjCompany.CompanyOwnerTitle, ObjCompany.CompanyOwnerEmail, ObjCompany.CompanyFacebook, ObjCompany.CompanyBlog,
                    ObjCompany.CompanyYoutube, ObjCompany.CompanyLinkedin, ObjCompany.CompanyTwitter, ObjCompany.CompanyGooglePlus,
                    ObjCompany.CompanyFacebookLike, ObjCompany.CompanyAlexa, (ObjCompany.Isactive == true ? 1 : 0), ObjCompany.LogoPath,
                    ObjCompany.SMTPServer, ObjCompany.SMTPPort, ObjCompany.SMTPLogin, ObjCompany.SMTPPassword, (ObjCompany.SMTPSsl == true ? 1 : 0),
                    ObjCompany.CompanyAddress1, ObjCompany.CompanyAddress2, ObjCompany.CompanyCity, ObjCompany.CompanyState, ObjCompany.CompanyZipCode,
                    ObjCompany.CompanyCountry, 1);
                LoadCompanyData(ObjCompany.CompanyId);

            }
            else
            {
                result = objAdmin.AddComapanyList(ObjCompany.CompanyId, ObjCompany.CompanyName, ObjCompany.Connectionstring, ObjCompany.CompanyWebsite, ObjCompany.SalesEmail, ObjCompany.SupportEmail, ObjCompany.PhoneNo, ObjCompany.CompanyOwnerName, ObjCompany.CompanyOwnerTitle, ObjCompany.CompanyOwnerEmail, ObjCompany.CompanyFacebook, ObjCompany.CompanyBlog, ObjCompany.CompanyYoutube, ObjCompany.CompanyLinkedin, ObjCompany.CompanyTwitter, ObjCompany.CompanyGooglePlus, ObjCompany.CompanyFacebookLike, ObjCompany.CompanyAlexa, (ObjCompany.Isactive == true ? 1 : 0), ObjCompany.LogoPath, ObjCompany.SMTPServer, ObjCompany.SMTPPort, ObjCompany.SMTPLogin, ObjCompany.SMTPPassword, (ObjCompany.SMTPSsl == true ? 1 : 0), ObjCompany.CompanyAddress1, ObjCompany.CompanyAddress2, ObjCompany.CompanyCity, ObjCompany.CompanyState, ObjCompany.CompanyZipCode, ObjCompany.CompanyCountry, 1);

            }
            return Json("1", JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region ManageCompanyAddresses
        public ActionResult ManageCompanyAddresses(int CompanyId = 0)
        {
            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                if (CompanyId != 0)
                {
                    ViewBag.ManageCompanyAddresses = GetListOfCompanyAddresses(CompanyId, 1, 2, 2, null);
                    return View();
                }
                else
                {
                    return RedirectToAction("ManageCompany", "Admin");
                }

            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        [HttpPost]
        public JsonResult GetAllCompanyAddresses(string CompanyId, string PageIndex, string SortColumn, string SortDirection, string Searchtext)
        {
            return Json(GetListOfCompanyAddresses(Convert.ToInt32(CompanyId), Convert.ToInt32(PageIndex), Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection), Searchtext), JsonRequestBehavior.AllowGet);
        }

        public string GetListOfCompanyAddresses(int CompanyId, int PageIndex, int SortColumn, int SortDirection, string Searchtext)
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                DataTable dt = new DataTable();
                dt = objAdmin.GetAllComapanyList_Address(CompanyId, PageIndex, PageSize, SortColumn, SortDirection, Searchtext);

                if (dt != null && dt.Rows.Count > 0)
                {
                    double TotalPages = Math.Ceiling(Convert.ToDouble(dt.Rows[0]["TotalRecord"].ToString()) / PageSize);
                    sb.Append("<div class=\"table_data reports_ref_received clearfix\">");
                    sb.Append("<h5>Manage Company Addresses<span class=\"buttons\"></span></h5>");
                    sb.Append("<div class=\"heading_panel\"><input onclick=\"CompanyAddressById('" + 0 + "','Add')\" class=\"add_new_company\" name=\"Edit Btn\" type=\"submit\" /></div>");
                    sb.Append("<div class=\"clear\"></div>");

                    sb.Append("<table class=\"manageCompanyList\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" id=\"tblCompanyAddresses\" data-companyid=\"" + CompanyId + "\" data-sortcolumn=\"" + SortColumn + "\" data-sortirection=\"" + SortDirection + "\">");
                    sb.Append("<thead>");
                    sb.Append("<tr>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetAllComapanyListAddress('" + CompanyId + "','" + PageIndex + "','" + 1 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\" >Address</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetAllComapanyListAddress('" + CompanyId + "','" + PageIndex + "','" + 2 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\" >Details</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetAllComapanyListAddress('" + CompanyId + "','" + PageIndex + "','" + 2 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\" >Action</a></th>");
                    sb.Append("</tr>");
                    sb.Append("</thead>");
                    sb.Append("<tbody id=\"tblbodycontent\">");


                    foreach (DataRow item in dt.Rows)
                    {

                        sb.Append("<tr>");
                        sb.Append("<td>" + ObjCommon.CheckNull(Convert.ToString(item["CompanyAddress1"]), string.Empty) + "<br/>" + ObjCommon.CheckNull(Convert.ToString(item["CompanyAddress2"]), string.Empty) + "</td>");
                        sb.Append("<td>" + ObjCommon.CheckNull(Convert.ToString(item["CompanyCity"]), string.Empty) + " ," + ObjCommon.CheckNull(Convert.ToString(item["CompanyState"]), string.Empty) + " ," + ObjCommon.CheckNull(Convert.ToString(item["CompanyZipCode"]), string.Empty) + " ," + ObjCommon.CheckNull(Convert.ToString(item["CompanyCountry"]), string.Empty) + "</td>");
                        sb.Append("<td><input class=\"edit_btn\" name=\"Edit Btn\" type=\"submit\" onclick=\"CompanyAddressById('" + ObjCommon.CheckNull(Convert.ToString(item["AddressId"]), string.Empty) + "','Edit')\" />");

                        if (ObjCommon.CheckNull(Convert.ToString(item["Isactive"]), string.Empty) == "Primary")
                        {
                            sb.Append(" &nbsp;&nbsp;<input class=\"active_btn\" name=\"Send Password\" type=\"submit\" />");
                        }
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    sb.Append("</tbody></table></div>");
                }
                else
                {
                    sb.Append("<center>No record found</center>");
                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        public string GetAllCompanyAddressesContent(string CompanyId, string PageIndex, string SortColumn, string SortDirection, string Searchtext)
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                DataTable dt = new DataTable();
                dt = objAdmin.GetAllComapanyList_Address(Convert.ToInt32(CompanyId), Convert.ToInt32(PageIndex), Convert.ToInt32(PageSize), Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection), Searchtext);

                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {

                        sb.Append("<tr>");
                        sb.Append("<td>" + ObjCommon.CheckNull(Convert.ToString(item["CompanyAddress1"]), string.Empty) + "<br/>" + ObjCommon.CheckNull(Convert.ToString(item["CompanyAddress2"]), string.Empty) + "</td>");
                        sb.Append("<td>" + ObjCommon.CheckNull(Convert.ToString(item["CompanyCity"]), string.Empty) + " ," + ObjCommon.CheckNull(Convert.ToString(item["CompanyState"]), string.Empty) + " ," + ObjCommon.CheckNull(Convert.ToString(item["CompanyZipCode"]), string.Empty) + " ," + ObjCommon.CheckNull(Convert.ToString(item["CompanyCountry"]), string.Empty) + "</td>");
                        sb.Append("<td><input class=\"edit_btn\" name=\"Edit Btn\" type=\"submit\" onclick=\"CompanyAddressById('" + ObjCommon.CheckNull(Convert.ToString(item["AddressId"]), string.Empty) + "','Edit')\" />");

                        if (ObjCommon.CheckNull(Convert.ToString(item["Isactive"]), string.Empty) == "Primary")
                        {
                            sb.Append(" &nbsp;&nbsp;<input class=\"active_btn\" name=\"Send Password\" type=\"submit\" />");
                        }
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
                else if (Convert.ToInt32(PageIndex) > 0)
                {
                    sb.Append("<tr><td colspan=\"3\"><center>No more record found</center></td></tr>");
                }
                else
                {
                    sb.Append("<tr><td colspan=\"3\"><center>No record found</center></td></tr>");
                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        [HttpPost]
        public JsonResult InsertUpdateCompanyAddress(string CompanyId, string Id, string Type, string CompanyAddress1, string CompanyAddress2, string CompanyCity, string CompanyState, string CompanyZipCode, string CompanyCountry, string Primary)
        {
            object obj = string.Empty;
            try
            {
                bool result = false;
                if (Type == "Add" && Id == "0")
                {
                    result = objAdmin.AddComapanyList_Address(Convert.ToInt32(CompanyId), CompanyAddress1, CompanyAddress2, CompanyCity, CompanyState, CompanyZipCode, CompanyCountry, (Primary == "true" ? 1 : 0));
                    if (result)
                    {
                        obj = "1";
                    }
                }
                else
                {
                    result = objAdmin.UpdateComapanyList_Address(Convert.ToInt32(Id), Convert.ToInt32(CompanyId), CompanyAddress1, CompanyAddress2, CompanyCity, CompanyState, CompanyZipCode, CompanyCountry, (Primary == "true" ? 1 : 0));
                    if (result)
                    {
                        obj = "1";
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult CompanyAddressById(string Id, string Type)
        {
            return Json(CompanyAddress(Convert.ToInt32(Id), Type), JsonRequestBehavior.AllowGet);
        }
        public string CompanyAddress(int Id, string Type)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();
                string CompanyAddress1 = string.Empty; string CompanyAddress2 = string.Empty;
                string CompanyCity = string.Empty; string CompanyState = string.Empty;
                string CompanyZipCode = string.Empty; string CompanyCountry = string.Empty;
                string Primary = string.Empty;



                if (Type == "Add")
                {
                    sb.Append("<h2 class=\"title\">Add Address</h2>");
                }
                else
                {
                    dt = objAdmin.GetComapanyList_Address_ID(Id);
                    sb.Append("<h2 class=\"title\">Edit Address</h2>");
                }
                if (dt != null && dt.Rows.Count > 0)
                {
                    CompanyAddress1 = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyAddress1"]), string.Empty);
                    CompanyAddress2 = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyAddress2"]), string.Empty);
                    CompanyCity = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyCity"]), string.Empty);
                    CompanyState = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyState"]), string.Empty);
                    CompanyZipCode = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyZipCode"]), string.Empty);
                    CompanyCountry = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CompanyCountry"]), string.Empty);
                    Primary = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["IsPrimary"]), string.Empty);
                }

                sb.Append("<div class=\"cntnt\">");
                sb.Append("<ul class=\"tablelist\">");
                sb.Append("<li><label>CompanyAddress1:</label><input id=\"txtCompanyAddress1\" type=\"text\" name=\"Search\" value=\"" + CompanyAddress1 + "\"></li>");
                sb.Append("<li><label>CompanyAddress2:</label><input type=\"text\" id=\"txtCompanyAddress2\" name=\"Search\" value=\"" + CompanyAddress2 + "\"></li>");
                sb.Append("<li><label>CompanyCity:</label><input type=\"text\" id=\"txtCompanyCity\" name=\"Search\" value=\"" + CompanyCity + "\"></li>");
                sb.Append("<li><label>CompanyState:</label><input type=\"text\" id=\"txtCompanyState\" name=\"Search\" value=\"" + CompanyState + "\"></li>");
                sb.Append("<li><label>CompanyZipCode:</label><input type=\"text\" id=\"txtCompanyZipCode\" name=\"Search\" value=\"" + CompanyZipCode + "\"></li>");
                sb.Append("<li><label>CompanyCountry:</label><input type=\"text\" id=\"txtCompanyCountry\"  name=\"Search\" value=\"" + CompanyCountry + "\"></li>");
                sb.Append("<li><div class=\"checkbox\"><label>");
                if (Primary == "False" || Primary == "false")
                {
                    sb.Append("<input id=\"checkprimary\" type=\"checkbox\" name=\"Search\" >");
                }
                else
                {
                    sb.Append("<input id=\"checkprimary\" type=\"checkbox\" name=\"Search\" checked=\"checked\" disabled=\"disabled\">");
                }

                sb.Append("Primary");
                sb.Append("</label></div></li>");
                sb.Append("</ul>");

                sb.Append("<div class=\"actions\">");
                sb.Append(" <input class=\"save_btn\" name=\"Save\" type=\"button\" value=\"Save\" onclick=\"InsertUpdateCompanyAddress('" + Id + "','" + Type + "')\" >");
                sb.Append(" <input class=\"cancel_btn\" name=\"Cancel\" type=\"button\" value=\"Cancel\" onclick=\"closepopup()\">");
                sb.Append(" <div class=\"clear\"></div>");
                sb.Append("</div></div>");
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }
        #endregion

        #region Vtiger service setting and Admin Password Update
        [HttpPost]
        public JsonResult GetVtigerServiceSetting()
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();
                dt = objAdmin.GetImportSettings("ImportServiceSelect");
                bool VTiger = false;
                bool Import = false;
                string LastModified = string.Empty;


                if (dt != null && dt.Rows.Count > 0)
                {
                    VTiger = Convert.ToBoolean(dt.Rows[0]["ServiceRun"]);
                    Import = Convert.ToBoolean(dt.Rows[0]["ImportWithDate"]);

                    if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["LastImportDate"])))
                    {
                        LastModified = new CommonBLL().ConvertToDate(Convert.ToDateTime(dt.Rows[0]["LastImportDate"]), (int)BO.Enums.Common.DateFormat.GENERAL);
                    }
                }
                sb.Append("<h2 class=\"title\"style=\"margin-bottom: 10px;\">VTiger Settings</h2>");
                sb.Append("<div style=\"float:right;\">&nbsp;Yes &nbsp;&nbsp;&nbsp;&nbsp;</div><div style=\"float:right;\">No</div> ");
                sb.Append("<div class=\"cntnt\">");

                sb.Append(" <ul class=\"tablelist vtiget_list\">");
                sb.Append("<li><label>VTiger Service Import:</label><span>");
                if (VTiger)
                {
                    sb.Append("<input  id=\"rdovtigerserviceyes\" name=\"radio_button_service\" style=\"margin-top: 9px;\"  type=\"radio\" value=\"\" checked=\"checked\" /> ");
                    sb.Append("<input id=\"rdovtigerserviceno\" name=\"radio_button_service\" style=\"margin-top: 9px;\" type=\"radio\" value=\"\"/>");
                }
                else
                {
                    sb.Append("<input  id=\"rdovtigerserviceyes\" name=\"radio_button_service\" style=\"margin-top: 9px;\"  type=\"radio\" value=\"\"  /> ");
                    sb.Append("<input id=\"rdovtigerserviceno\" name=\"radio_button_service\" style=\"margin-top: 9px;\" type=\"radio\" value=\"\" checked=\"checked\"/>");
                }

                sb.Append("</span></li>");
                sb.Append(" <li><label>Import With Last Modified Date:</label><span>");
                if (Import)
                {
                    sb.Append("<input id=\"rdoImportyes\"  name=\"last_mod_date\" type=\"radio\" style=\"margin-top: 9px;\" value=\"\" checked=\"checked\" />");
                    sb.Append("<input id=\"rdoImportno\"  name=\"last_mod_date\" type=\"radio\" style=\"margin-top: 9px;\" value=\"\"/>");
                }
                else
                {
                    sb.Append("<input id=\"rdoImportyes\"  name=\"last_mod_date\" type=\"radio\" style=\"margin-top: 9px;\" value=\"\"  /> ");
                    sb.Append("<input id=\"rdoImportno\"  name=\"last_mod_date\" type=\"radio\" style=\"margin-top: 9px;\" value=\"\" checked=\"checked\"/>");
                }

                sb.Append("</span></li>");
                sb.Append("<li><label class=\"fullwidthLabel\">Last Modified Date:</label><input id=\"txtLastModifiedVtigersetting\" type=\"text\" name=\"Search\" class=\"dateVtiger\" value=\"" + LastModified + "\"/><span >[MM/DD/YYYY]</span></li> ");
                sb.Append("</ul>");

                sb.Append("<div class=\"actions\"> ");
                sb.Append("<input class=\"save_btn\" name=\"Save\" type=\"button\" value=\"Save\" onclick=\"return AddUpdateImportSettings()\" />");
                sb.Append("<input class=\"cancel_btn\" name=\"Cancel\" type=\"button\" value=\"Cancel\" onclick=\"closepopup()\"/>");
                sb.Append("<div class=\"clear\"></div></div></div>");
            }
            catch (Exception)
            {

                throw;
            }
            return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult AddUpdateImportSettings(string ServiceImport, string ImportWith, string LastModified)
        {
            object obj = string.Empty;
            bool ServiceRun = false;
            bool ImportWithDate = false;
            string LastModifiedDate = "01/01/1900";
            try
            {
                if (ServiceImport == "true")
                {
                    ServiceRun = true;
                }
                if (ImportWith == "true")
                {
                    ImportWithDate = true;
                }
                if (!string.IsNullOrEmpty(LastModified))
                {
                    LastModifiedDate = LastModified;
                }

                bool result = objAdmin.AddUpdateImportSettings(ServiceRun, ImportWithDate, LastModifiedDate, "ImportServiceAddUpdate");
                if (result)
                {
                    obj = "1";
                }
            }
            catch (Exception)
            {

                throw;
            }


            return Json(obj, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult UpdateAdminPassword(string Password)
        {
            object obj = string.Empty;
            try
            {

                string encryptedPassword = ObjTripleDESCryptoHelper.encryptText(Password);

                int UserId = Convert.ToInt32(Session["AdminUserId"]);
                bool Result = objAdmin.UpdateAdminPassword(encryptedPassword, UserId);
                if (Result)
                {
                    obj = "1";
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Logout
        public ActionResult Logout()
        {
            Session.RemoveAll();
            return RedirectToAction("Index", "User");
        }
        #endregion

        #region View Profile
        public ActionResult ViewProfile(int UserId = 0)
        {

            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                ViewBag.UserId = UserId;
                string ImageName = null;
                MdlMember = new Member();

                DataSet ds = new DataSet();
                ds = MdlMember.GetProfileDetailsOfDoctorByID(UserId);

                MdlMember.UserId = Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]);
                MdlMember.FirstName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]), string.Empty);
                MdlMember.LastName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LastName"]), string.Empty);
                string Desciption = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Description"]), string.Empty);
                if (!string.IsNullOrEmpty(Desciption))
                {
                    MdlMember.Description = Desciption.Replace("\n", "<br/>");
                }
                MdlMember.Title = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Title"]), string.Empty);
                MdlMember.MiddleName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["MiddleName"]), string.Empty);
                ImageName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["ImageName"]), string.Empty);
                MdlMember.PublicProfile = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["PublicProfileUrl"]), string.Empty);
                MdlMember.WebsiteURL = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["WebsiteURL"]), string.Empty);
                if (string.IsNullOrEmpty(ImageName))
                {
                    MdlMember.ImageName = ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty);
                }
                else
                {
                    MdlMember.ImageName = ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + ImageName;
                }

                if (ds.Tables[1].Rows.Count > 0)
                {
                    MdlMember.OfficeName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[1].Rows[0]["AccountName"]), string.Empty);
                }
                SectionPublicProfile objprofiles = new SectionPublicProfile();
                if (ds.Tables[15].Rows.Count > 0)
                {
                    objprofiles.PatientForms = Convert.ToBoolean(ds.Tables[15].Rows[0]["PatientForms"]);
                    objprofiles.ReferPatient = Convert.ToBoolean(ds.Tables[15].Rows[0]["ReferPatient"]);
                    objprofiles.Reviews = Convert.ToBoolean(ds.Tables[15].Rows[0]["Reviews"]);
                    objprofiles.SpecialOffers = Convert.ToBoolean(ds.Tables[15].Rows[0]["SpecialOffers"]);
                    objprofiles.AppointmentBooking = Convert.ToBoolean(ds.Tables[15].Rows[0]["AppointmentBooking"]);
                    MdlMember.ObjProfileSection = objprofiles;
                }

                MdlMember.lstDoctorAddressDetails = MdlMember.GetDoctorAddressDetailsById(MdlMember.UserId);
                MdlMember.lstGetSocialMediaDetailByUserId = MdlMember.GetSocialMediaDetailByUserId(MdlMember.UserId);
                MdlMember.lstEducationandTraining = MdlMember.GetEducationandTrainingDetailsForDoctor(MdlMember.UserId);
                MdlMember.lstProfessionalMemberships = MdlMember.GetProfessionalMembershipsByUserId(MdlMember.UserId);
                MdlMember.lstTeamMemberDetailsForDoctor = MdlMember.GetTeamMemberDetailsOfDoctor(MdlMember.UserId, 1, 15);
                MdlMember.lstsecondarywebsitelist = MdlMember.GetSecondaryWebsitelistByUserId(MdlMember.UserId);
                MdlMember.lstSpeacilitiesOfDoctor = MdlMember.GetSpeacilitiesOfDoctorById(MdlMember.UserId);
                if (ds.Tables[10].Rows.Count > 0)
                {
                    MdlMember.licensedetails = MdlMember.GetLicensedetailsByUserId(MdlMember.UserId, SessionManagement.TimeZoneSystemName);
                }
                ViewBag.State = StateScript("", "");
                ViewBag.hdnViewProfile = false;
                string defaultPath = "/Resources/Gallery/" + MdlMember.UserId + "/";
                string filePath = ConfigurationManager.AppSettings.Get("GallaryFilePath");
                if (string.IsNullOrEmpty(filePath))
                {
                    filePath = defaultPath;
                }
                MdlMember.lstBanner = MdlMember.GetBannerDetailByUserId(MdlMember.UserId);
                MdlMember.lstGallary = mdlGallary.getGallaryList(MdlMember.UserId, 0);
                MdlMember.GallaryPath = filePath;
                ProfileBLL objProfile = new ProfileBLL();
                MdlMember.lstInsurance = objProfile.GetMemberInsuranceByUserId(MdlMember.UserId);
                return View(MdlMember);

            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }


        #endregion
        [HttpPost]
        public PartialViewResult EditLicenseDetails(int id, int Userid)
        {
            MdlMember = new Member();
            MdlMember.objLicense = MdlMember.LicensedetailsByUserId(Userid);
            ViewBag.state = StateScript("", MdlMember.objLicense.LicenseState);
            return PartialView("EditLicenseDetails", MdlMember.objLicense);
        }
        public string StateScript(string countryId, string stateId)
        {
            if (countryId == "")
            {
                countryId = "US";
            }
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.StateByCountryCode(countryId);
            sb.Append("<select id=\"drpState\" name=\"drpState\">");
            foreach (var item in lst)
            {

                if (stateId == item.Value)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
                else
                {
                    sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
            }
            sb.Append("</select>");
            return sb.ToString();
        }
        [HttpPost]
        public JsonResult InsertUpdateLicenseDetails(string LicenseNumber, string LicenseState, string LicenseExpiration, int UserId)
        {

            object obj = "";
            objColleaguesData = new clsColleaguesData();
            DateTime? dt_LicenseExpiration = null;
            if (!string.IsNullOrEmpty(LicenseExpiration))
            {
                dt_LicenseExpiration = Convert.ToDateTime(LicenseExpiration);
                dt_LicenseExpiration = clsHelper.ConvertToUTC(Convert.ToDateTime(dt_LicenseExpiration), SessionManagement.TimeZoneSystemName);
            }
            bool Result = objColleaguesData.InsertUpdateLicenseDetils(UserId, LicenseNumber, LicenseState, dt_LicenseExpiration);
            if (Result)
            {
                obj = "1";
            }
            else
            {
                obj = "0";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #region Chnage Profile pic of user
        [HttpPost]
        public ActionResult AdminUploadProfilePic(HttpPostedFileBase fuImage, String id)
        {
            object obj = string.Empty;
            try
            {
                if (fuImage != null)
                {
                    string filename1 = "$" + System.DateTime.Now.Ticks + "$" + fuImage.FileName;
                    string tempPath = "";

                    tempPath = "~/DentistImages/";
                    fuImage.SaveAs(Server.MapPath(tempPath + filename1));

                    int Bottom = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Bottom"].ToString())));
                    int Right = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Right"].ToString())));
                    int Top = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Top"].ToString())));
                    int Left = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Left"].ToString())));

                    var imagecrop = new WebImage(tempPath + filename1);
                    var height = imagecrop.Height;
                    var width = imagecrop.Width;
                    imagecrop.Crop((int)Top, (int)Left, (int)(height - Bottom), (int)(width - Right));
                    imagecrop.Save(tempPath + filename1);



                    int maxPixels = Convert.ToInt32(ConfigurationManager.AppSettings["Thumbimagediamention"]);
                    String SavePath = tempPath + "Thumb" + @"/" + filename1;
                    imagecrop.Resize(maxPixels, maxPixels, true, false);
                    imagecrop.Save(Server.MapPath(SavePath));

                    bool result = objColleaguesData.UpdateDoctorProfileImageById(Convert.ToInt32(id), filename1);
                    if (result)
                    {
                        obj = "1";
                        DataSet ds = new DataSet();
                        ds = objColleaguesData.GetDoctorDetailsById(Convert.ToInt32(id));
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            string AdminImage = string.Empty;
                            AdminImage = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["ImageName"]), string.Empty);
                            if (string.IsNullOrEmpty(AdminImage))
                            {
                                Session["AdminImage"] = ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty);
                            }
                            else
                            {
                                Session["AdminImage"] = ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + AdminImage;
                            }
                        }
                    }


                }
            }
            catch (Exception)
            {

                throw;
            }

            return RedirectToAction("Dashboard", "Admin");
        }


        [HttpPost]
        public ActionResult UploadProfilePic(HttpPostedFileBase fuImage, String id)
        {
            try
            {
                if (fuImage != null)
                {
                    string filename1 = "$" + System.DateTime.Now.Ticks + "$" + fuImage.FileName;
                    string tempPath = "";

                    tempPath = "~/DentistImages/";
                    fuImage.SaveAs(Server.MapPath(tempPath + filename1));



                    int Bottom = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Bottom"].ToString())));
                    int Right = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Right"].ToString())));
                    int Top = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Top"].ToString())));
                    int Left = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Left"].ToString())));


                    var imagecrop = new WebImage(tempPath + filename1);
                    var height = imagecrop.Height;
                    var width = imagecrop.Width;
                    imagecrop.Crop((int)Top, (int)Left, (int)(height - Bottom), (int)(width - Right));
                    imagecrop.Save(tempPath + filename1);



                    int maxPixels = Convert.ToInt32(ConfigurationManager.AppSettings["Thumbimagediamention"]);
                    String SavePath = tempPath + "Thumb" + @"/" + filename1;
                    imagecrop.Resize(maxPixels, maxPixels, true, false);
                    imagecrop.Save(Server.MapPath(SavePath));

                    bool result = objColleaguesData.UpdateDoctorProfileImageById(Convert.ToInt32(id), filename1);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return RedirectToAction("ViewProfile", "Admin", new { UserId = id });
        }
        #endregion

        #region Merge Doctor
        [HttpPost]
        public JsonResult MergeDoctor(string SelectedUserId, string AllUserIds)
        {
            object obj = string.Empty;
            try
            {
                string[] ArrayUserIds = AllUserIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var item in ArrayUserIds)
                {
                    if (SelectedUserId != item)
                    {
                        bool result = objAdmin.MergeUser(Convert.ToInt32(SelectedUserId), Convert.ToInt32(item));
                        if (result)
                        {
                            obj = "1";
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MergeAccountView()
        {
            return View();
        }

        [HttpPost]
        public JsonResult OpenMergeDoctorPopUp(string UserIds)
        {
            return Json(FillMergeDoctorPopUp(UserIds), JsonRequestBehavior.AllowGet);
        }
        public string FillMergeDoctorPopUp(string UserIds)
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                string[] ArrayUserIds = UserIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                sb.Append("<h2 class=\"title\">Merge Doctor</h2>");
                sb.Append("<div class=\"cntnt\">");
                sb.Append("<ul class=\"search_listing\" style=\"width:100%;\">");

                // sb.Append("<ul class=\"tablelist\">");
                // sb.Append("<li>Select the doctor which will remain in system.");
                int i = 0;
                foreach (var item in ArrayUserIds)
                {

                    DataSet ds = new DataSet();
                    ds = objColleaguesData.GetDoctorDetailsById(Convert.ToInt32(item));
                    if (ds.Tables.Count > 0)
                    {
                        if (i == 0)
                        {
                            sb.Append("<li style=\"width:50%;\"><div class=\"dyheight\" id=\"" + Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]) + "\" style=\"height:auto;\">");
                            sb.Append("<span class=\"check\" style=\"margin-top:42px;\"><input type=\"radio\" class=\"checkbox_add_patient\" checked=\"true\" name=\"mergedoctor\" id=\"checkmergedoctor\" value=\"" + Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]) + "\" /></span>");
                            sb.Append("<span class=\"thumb\"><a  href=\"Javascript:;\" onclick=\"javascript:void(0);\" ><img alt=\"\" style=\"height: 95px; width: 85px;\" src='../../DentistImages/" + (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["ImageName"])) ? Convert.ToString(ds.Tables[0].Rows[0]["ImageName"]) : "Doctor_no_image.gif") + "'></a></span>");
                            sb.Append("<span class=\"description\">");
                            sb.Append("<h2><a  href=\"Javascript:;\" style=\"word-wrap: break-word;\" onclick=\"javascript:void(0);\">" + ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FullName"]), string.Empty) + "<span class=\"sub_heading\">" + ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["UserName"]), string.Empty) + "</span></a></h2>");
                            sb.Append("<ul class=\"list\">");
                            sb.Append("<li>" + ObjCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["ExactAddress"]), string.Empty) + "</li>");
                            sb.Append("<li>" + ObjCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["City"]), string.Empty) + "");
                            sb.Append(", " + ObjCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["State"]), string.Empty) + "</li>");
                            sb.Append("<li>" + ObjCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["ZipCode"]), string.Empty) + "</li>");
                            sb.Append("/ul>");
                            sb.Append("</span><span class=\"clear\"></span></div></li>");
                        }
                        else
                        {
                            sb.Append("<li style=\"width:50%;\"><div class=\"dyheight\" id=\"" + Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]) + "\" style=\"height:auto;\">");
                            sb.Append("<span class=\"check\" style=\"margin-top:42px;\"><input type=\"radio\" class=\"checkbox_add_patient\" checked=\"true\" name=\"mergedoctor\" id=\"checkmergedoctor\" value=\"" + Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]) + "\" /></span>");
                            sb.Append("<span class=\"thumb\"><a  href=\"Javascript:;\" onclick=\"javascript:void(0);\" ><img alt=\"\" style=\"height: 95px; width: 85px;\" src='../../DentistImages/" + (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["ImageName"])) ? Convert.ToString(ds.Tables[0].Rows[0]["ImageName"]) : "Doctor_no_image.gif") + "'></a></span>");
                            sb.Append("<span class=\"description\">");
                            sb.Append("<h2><a  href=\"Javascript:;\" style=\"word-wrap: break-word;\" onclick=\"javascript:void(0);\">" + ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FullName"]), string.Empty) + "<span class=\"sub_heading\">" + ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["UserName"]), string.Empty) + "</span></a></h2>");
                            sb.Append("<ul class=\"list\">");
                            sb.Append("<li>" + ObjCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["ExactAddress"]), string.Empty) + "</li>");
                            sb.Append("<li>" + ObjCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["City"]), string.Empty) + "");
                            sb.Append(", " + ObjCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["State"]), string.Empty) + "</li>");
                            sb.Append("<li>" + ObjCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["ZipCode"]), string.Empty) + "</li>");
                            sb.Append("/ul>");
                            sb.Append("</span><span class=\"clear\"></span></div></li>");
                        }

                    }
                    i++;
                }
                //  sb.Append("</li>");
                sb.Append("</ul>");
                sb.Append("<div class=\"actions\">");
                sb.Append("<input class=\"save_btn\" name=\"Save\" type=\"submit\" id=\"btnSave\" onclick=\"return CallMergeDoctor('" + UserIds + "')\"/>");
                sb.Append(" <input class=\"cancel_btn\" name=\"Cancel\" type=\"submit\" value=\"Cancel\" onclick=\"closepopup()\" />");
                sb.Append("<div class=\"clear\"></div>");
                sb.Append("</div>");

                sb.Append("</div><button class=\"mfp-close\" type=\"button\" title=\"Close (Esc)\">×</button>");
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }
        #endregion


        #region Account setting of User
        [CustomAuthorize]
        public ActionResult AccountSetting(int UserId)
        {
            DataTable dtUserInfo = new clsColleaguesData().GetDoctorDetails(UserId);
            if (dtUserInfo.Rows.Count > 0)
            {
                var item = dtUserInfo.Rows[0];
                ViewBag.DoctorName = Convert.ToString(item["DoctorName"]);
            }
            return View();
        }
        #endregion

        #region Remove active user profile

        public ActionResult RemoveDoctorProfileImage(int DoctorId)
        {
            if (DoctorId > 0)
            {
                objColleaguesData = new clsColleaguesData();
                objColleaguesData.RemoveDoctorProfileImage(DoctorId);
            }
            return RedirectToAction("ViewProfile", "Admin", new { UserId = DoctorId });
        }
        #endregion


        #region Account Upgrade
        public ActionResult UpgradeAccount(string Userid)
        {
            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                clsCommon objCommon = new clsCommon();
                Member MdlMember = new Member();
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                PyamentDetails objPyamentDetails = new PyamentDetails();
                if (!string.IsNullOrEmpty(Userid))
                {
                    TripleDESCryptoHelper objTripleDESCryptoHelper = new TripleDESCryptoHelper();
                    Userid = Userid.Replace(" ", "+");
                    string uID = objTripleDESCryptoHelper.decryptText(Userid);
                    ds = MdlMember.GetProfileDetailsOfDoctorByID(Convert.ToInt32(uID));
                    dt = MdlMember.GetPaymentHostoryOnUserId(Convert.ToInt32(uID));
                    Membership_FeatureBLL ObjMembershipBLL = new Membership_FeatureBLL();
                    List<Membership> lst = new List<Membership>();
                    List<NewMembership> lst1 = new List<NewMembership>();
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        objPyamentDetails.ListOfMembership = MdlMember.GetMembershipPlan(Convert.ToInt32(ds.Tables[0].Rows[0]["MembershipId"]));
                        objPyamentDetails.Planes = Convert.ToString(ds.Tables[0].Rows[0]["MembershipId"]);
                    }
                    if (dt.Rows.Count > 0)
                        ViewBag.CurrentPlan = "PAID";
                    else
                        ViewBag.CurrentPlan = "FREE";

                    //objPyamentDetails.Amount = "$150";
                    //objPyamentDetails.Planes = "1";
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        objPyamentDetails.Firstname = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]), string.Empty);
                        objPyamentDetails.Lastname = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LastName"]), string.Empty);
                        objPyamentDetails.Email = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["UserName"]), string.Empty);
                    }
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        objPyamentDetails.City = objCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["City"]), string.Empty);
                        objPyamentDetails.State = objCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["State"]), string.Empty);
                        objPyamentDetails.Zip = objCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["ZipCode"]), string.Empty);
                        objPyamentDetails.Phone = objCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["Phone"]), string.Empty);
                    }
                    objPyamentDetails.Userid = uID;
                    clsColleaguesData objcls = new clsColleaguesData();
                    DataTable dttbl = objcls.getNewPaymentHistoryOnUserId(Convert.ToInt32(uID));
                    string membershipId = string.Empty;
                    string amount = string.Empty;
                    string subType = string.Empty;
                    if (dttbl != null && dttbl.Rows.Count > 0)
                    {
                        membershipId = Convert.ToString(dttbl.Rows[0]["membershipId"]);
                        amount = Convert.ToString(dttbl.Rows[0]["amount"]);
                        subType = Convert.ToString(dttbl.Rows[0]["subscriptionType"]);
                    }
                    if (subType == "3")
                    {
                        ViewBag.PlanDescription = "Quarterly Payment of $" + amount + " per quarter.";
                    }
                    if (subType == "1")
                    {
                        ViewBag.PlanDescription = "Monthly Payment of $" + amount + " per month.";
                    }
                    if (subType == "6")
                    {
                        ViewBag.PlanDescription = "Half-yearly Payment of $" + amount + " per half year.";
                    }
                    if (subType == "12")
                    {
                        ViewBag.PlanDescription = "Annually Payment of $" + amount + " per year.";
                    }
                    if (subType == "")
                    {
                        ViewBag.PlanDescription = "Free Package";
                    }
                }
                return View(objPyamentDetails);
            }
            return RedirectToAction("Index", "User");
        }
        public ActionResult MakePayment(PyamentDetails objPyamentDetails)
        {
            try
            {

                mdlCommon objCommoncls = new mdlCommon();
                clsColleaguesData ObjColleaguesData = new clsColleaguesData();
                // By default, this sample code is designed to post to our test server for
                // developer accounts: https://test.authorize.net/gateway/transact.dll
                // for real accounts (even in test mode), please make sure that you are
                // posting to: https://secure.authorize.net/gateway/transact.dll
                String post_url = ConfigurationManager.AppSettings["post_url"];
                string amount = string.Empty;
                string strRecurring = string.Empty;
                Dictionary<string, string> post_values = new Dictionary<string, string>();

                post_values.Add("x_login", ConfigurationManager.AppSettings["x_login"]);
                post_values.Add("x_tran_key", ConfigurationManager.AppSettings["x_tran_key"]);
                post_values.Add("x_delim_data", ConfigurationManager.AppSettings["x_delim_data"]);
                post_values.Add("x_delim_char", ConfigurationManager.AppSettings["x_delim_char"]);
                post_values.Add("x_relay_response", ConfigurationManager.AppSettings["x_relay_response"]);
                post_values.Add("x_response_format", ConfigurationManager.AppSettings["x_response_format"]);
                post_values.Add("x_version", ConfigurationManager.AppSettings["x_version"]);
                post_values.Add("x_type", ConfigurationManager.AppSettings["x_type"]);
                post_values.Add("x_method", ConfigurationManager.AppSettings["x_method"]);


                post_values.Add("x_card_type", objPyamentDetails.CardType);
                post_values.Add("x_card_num", objPyamentDetails.CardNumber);
                post_values.Add("x_exp_date", objPyamentDetails.expdate);
                Member objMember = new Member();
                TripleDESCryptoHelper objTripleDESCryptoHelper = new TripleDESCryptoHelper();
                objPyamentDetails.ListOfMembership = objMember.GetMembershipById(Convert.ToInt32(objPyamentDetails.Planes));


                string firstAmount = objCommoncls.GetAmount(objPyamentDetails.Planes);
                foreach (var item in objPyamentDetails.ListOfMembership)
                {
                    string ActualPrice = string.Empty;
                    if (objPyamentDetails.payInterval == "1")
                    {
                        ActualPrice = Convert.ToString(item.MonthlyPrice);
                        if (!string.IsNullOrEmpty(ActualPrice))
                        {
                            amount = ActualPrice;
                            objPyamentDetails.Amount = ActualPrice;
                        }
                        else
                        {
                            amount = firstAmount;
                            objPyamentDetails.Amount = amount;
                        }
                    }
                    if (objPyamentDetails.payInterval == "3")
                    {
                        ActualPrice = Convert.ToString(item.QuaterlyPrice);
                        if (!string.IsNullOrEmpty(ActualPrice))
                        {
                            amount = ActualPrice;
                            objPyamentDetails.Amount = ActualPrice;
                        }
                        else
                        {
                            var amt = Convert.ToString(Convert.ToInt32(firstAmount) * 3);
                            amount = amt;
                            objPyamentDetails.Amount = amount;
                        }
                    }
                    if (objPyamentDetails.payInterval == "6")
                    {
                        ActualPrice = Convert.ToString(item.HalfYearlyPrice);
                        if (!string.IsNullOrEmpty(ActualPrice))
                        {
                            amount = ActualPrice;
                            objPyamentDetails.Amount = ActualPrice;
                        }
                        else
                        {
                            var amt = Convert.ToString(Convert.ToInt32(firstAmount) * 6);
                            amount = amt;
                            objPyamentDetails.Amount = amount;
                        }
                    }
                    if (objPyamentDetails.payInterval == "12")
                    {
                        ActualPrice = Convert.ToString(item.AnnualPrice);
                        if (!string.IsNullOrEmpty(ActualPrice))
                        {
                            amount = ActualPrice;
                            objPyamentDetails.Amount = ActualPrice;
                        }
                        else
                        {
                            var amt = Convert.ToString(Convert.ToInt32(firstAmount) * 12);
                            amount = amt;
                            objPyamentDetails.Amount = amount;
                        }
                    }
                }

                post_values.Add("x_amount", amount);
                post_values.Add("x_description", objPyamentDetails.Description);
                post_values.Add("x_first_name", objPyamentDetails.Firstname);
                post_values.Add("x_last_name", objPyamentDetails.Lastname);
                post_values.Add("x_address", objPyamentDetails.Lastname);
                post_values.Add("x_state", objPyamentDetails.State);
                post_values.Add("x_zip", objPyamentDetails.Zip);

                String post_string = "";

                foreach (KeyValuePair<string, string> post_value in post_values)
                {
                    post_string += post_value.Key + "=" + HttpUtility.UrlEncode(post_value.Value) + "&";
                }
                post_string = post_string.TrimEnd('&');



                // create an HttpWebRequest object to communicate with Authorize.net
                HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(post_url);
                objRequest.Method = "POST";
                objRequest.ContentLength = post_string.Length;
                objRequest.ContentType = "application/x-www-form-urlencoded";
                objRequest.KeepAlive = false;
                objRequest.ProtocolVersion = HttpVersion.Version10;
                objRequest.ServicePoint.ConnectionLimit = 1;
                //System.Net.ServicePointManager.Expect100Continue = false;
                //SecurityProtocolType origSecurityProtocol = ServicePointManager.SecurityProtocol;
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                // post data is sent as a stream
                StreamWriter myWriter = null;
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(post_string);
                myWriter.Close();

                // returned values are returned as a stream, then read into a string
                String post_response;
                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
                {
                    post_response = responseStream.ReadToEnd();
                    responseStream.Close();
                }

                // the response string is broken into an array
                // The split character specified here must match the delimiting character specified above
                string[] response_array = post_response.Split('|');
                if (response_array != null && response_array[0] == "1")
                {
                    mdlCommon objCommon = new mdlCommon();
                    string transactionId = Convert.ToString(response_array[6]);
                    objPyamentDetails.Userid = objTripleDESCryptoHelper.decryptText(objPyamentDetails.Userid);
                    string resp = CallSubscription(objPyamentDetails, transactionId);

                    if (!string.IsNullOrEmpty(resp) && resp == "Done")
                    {
                        strRecurring = "Recurring is done successfully.";
                        TempData["strRecurring"] = strRecurring;
                    }
                    objPyamentDetails.PackageStartDate = System.DateTime.Now;
                    objPyamentDetails.PackageEndDate = objCommoncls.GetPackageEndDate(objPyamentDetails.Planes);

                    ObjColleaguesData.InsertTransactionDetails(Convert.ToInt32(objPyamentDetails.Userid), objPyamentDetails.PackageStartDate, objPyamentDetails.PackageEndDate, Convert.ToDecimal(amount), true, objPyamentDetails.CardType, objPyamentDetails.CardNumber, objPyamentDetails.expdate, response_array[6], objPyamentDetails.Cvv, true);
                    ObjColleaguesData.UpdatePaymentStatusOfExistingUser(Convert.ToInt32(objPyamentDetails.Userid), true);
                    string CardNumberWithMask = objPyamentDetails.CardNumber;

                    CardNumberWithMask = CardNumberWithMask.Substring(CardNumberWithMask.Length - 4).PadLeft(CardNumberWithMask.Length, '*');

                    string CardType = objCommon.GetCardType(objPyamentDetails.CardType);
                    ObjTemplate.AccountUpgradedSuccessfully(Convert.ToInt32(objPyamentDetails.Userid), CardNumberWithMask, CardType, response_array[6], objPyamentDetails.Amount);
                    TempData["Paymentstatus"] = response_array[3];
                    if (response_array[0] == "1")
                    {
                        TempData["TransactionId"] = response_array[6];
                    }
                }
                else
                {
                    ObjTemplate.PaymentFailed(objPyamentDetails.Email, objPyamentDetails.Firstname, objPyamentDetails.Lastname, objPyamentDetails.Phone, response_array[3], Request.Url.AbsoluteUri);
                    TempData["Paymentstatus"] = response_array[3];
                }
                //objPyamentDetails.Userid = objTripleDESCryptoHelper.encryptText(objPyamentDetails.Userid);
                return RedirectToAction("UpgradeAccount", "admin", new { Userid = objPyamentDetails.Userid });
            }
            catch (Exception ex)
            {

                throw;
            }
        }
        #endregion

        #region Insurance Detail

        public ActionResult GetInsuranceList(int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext)
        {
            mdlAdmin model = new mdlAdmin();
            model.lstInsurance = model.GetAllInsuranceDetail(PageIndex, PageSize, SortColumn, SortDirection, Searchtext);
            model.TotalInsurance = objAdmin.GetCountOfInsurance(Searchtext);
            model.CurrentPageNo = PageIndex;
            return PartialView("_InsuranceList", model);
        }

        public ActionResult ManageInsurance(int Page = 1)
        {

            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                mdlAdmin model = new mdlAdmin();
                model.lstInsurance = model.GetAllInsuranceDetail(Page, 50, 1, 1, null);
                model.TotalInsurance = objAdmin.GetCountOfInsurance(null);
                model.CurrentPageNo = Page;
                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        public ActionResult AddInsurance()
        {
            Insurance model = new Insurance();
            model.InsuranceId = 0;
            model.CurrentPageNo = 1;
            return PartialView("_AddInsurance", model);
        }

        public ActionResult EditInsurance(int InsuranceId, int page = 1)
        {
            Insurance insurance = new Insurance();
            MdlAdmin = new mdlAdmin();
            insurance = MdlAdmin.GetInsuranceDetailById(InsuranceId);
            insurance.CurrentPageNo = page;
            return PartialView("_AddInsurance", insurance);
        }

        public ActionResult InsertUpdateInsurance(Insurance model, HttpPostedFileBase File)
        {

            bool Result = false;
            object obj = "";
            string path = "", filename = "";
            if (File != null && File.ContentLength > 0)
            {
                string UserId = Convert.ToString(Session["UserId"]);
                string savepath = Server.MapPath(ConfigurationManager.AppSettings["InsuranceImage"]);
                if (!Directory.Exists(savepath))
                {
                    Directory.CreateDirectory(savepath);
                }
                filename = "$" + System.DateTime.Now.Ticks + "$" + File.FileName;
                path = Path.Combine(savepath, Path.GetFileName(filename));
                File.SaveAs(path);
                if (model.InsuranceId != 0)
                {
                    String Removepath = Server.MapPath(ConfigurationManager.AppSettings["InsuranceImage"] + model.LogoPath);
                    if (System.IO.File.Exists(Removepath))
                    {
                        System.IO.File.Delete(Removepath);
                    }
                }
            }
            else
            {
                filename = model.LogoPath;
            }
            Result = objAdmin.InsertUpdateInsurance(model.InsuranceId, model.Name, filename, model.Description, model.Link);

            if (model.InsuranceId == 0)
            {
                TempData["errorMessage"] = "Insurance inserted successfully";
            }
            else
            {
                TempData["errorMessage"] = "Insurance updated successfully";
            }

            return RedirectToAction("ManageInsurance", new { Page = model.CurrentPageNo });
        }

        public ActionResult DeleteInsurance(int InsuranceId)
        {
            object obj = string.Empty;
            try
            {
                bool result = false;
                Insurance insurance = new Insurance();
                MdlAdmin = new mdlAdmin();
                DataTable dt = new DataTable();
                dt = objProfileData.GetMemberInsuranceByInsuranceId(InsuranceId);
                if (dt.Rows.Count <= 0)
                {
                    insurance = MdlAdmin.GetInsuranceDetailById(InsuranceId);
                    result = objAdmin.DeleteInsuranceDetail(InsuranceId);
                    String path = Server.MapPath(ConfigurationManager.AppSettings["InsuranceImage"] + insurance.LogoPath);
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                    }
                    obj = "1";
                }
                else
                {
                    obj = "2";
                }


            }
            catch (Exception)
            {
                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public ActionResult UpdateProfilePictures()
        {
            string returnValue = string.Empty;
            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                clsCommon ObjCommon = new clsCommon();
                DataTable dt = new DataTable();

                string qry = "SELECT  UserId,ImageName FROM Member WHERE LEN(LTRIM(RTRIM(ISNULL(ImageName,'')))) != 0 ORDER BY ProfilesLastUpdated DESC";
                dt = ObjCommon.DataTable(qry);
                if (dt.Rows.Count > 0)
                {
                    string rootPath = "~/DentistImages/";
                    rootPath = Server.MapPath(rootPath);
                    foreach (DataRow item in dt.Rows)
                    {
                        //qry = "UPDATE Appointments SET AppointmentDate = '" + AppointmentDate.ToString("yyyy-MM-dd") + " 00:00:00', AppointmentTime = '" + AppointmentTime.ToString("HH:mm:ss") + "' WHERE AppointmentId = " + item["AppointmentId"].ToString() + "; SELECT @@ROWCOUNT;";
                        string filePath = rootPath + "\\" + Convert.ToString(item["ImageName"]);
                        if (!System.IO.File.Exists(filePath))
                        {
                            qry = "UPDATE Member SET ImageName = null, ProfilesLastUpdated = GETDATE() FROM Member WHERE UserId = " + Convert.ToString(item["UserId"]);
                            DataTable dt1 = ObjCommon.DataTable(qry);
                            returnValue += "<p>Image Path is <strong>" + filePath + "</strong> UserID : " + Convert.ToString(item["UserId"]) + "</p>";
                        }

                    }
                    returnValue += "<h1> Its Done for records : " + dt.Rows.Count + "</h1>";
                }
            }
            else
            {
                returnValue = "<h1> Unauthorized Access </h1>";
            }
            return Content(returnValue, "text/html");
        }
        [CustomAuthorizeAttribute]
        public ActionResult CheckSpecialityName(string SpecialityName, int SpecialityId)
        {
            bool result = false;
            try
            {
                result = objAdmin.CheckSpecialityName(SpecialityId, SpecialityName);
            }
            catch (Exception)
            {

                throw;
            }
            return Json(result, JsonRequestBehavior.AllowGet);


        }

        #region ManageMemberShip
        [AdminAuthorizeAttribute]
        public ActionResult ManageMemberShip(int Page = 1)
        {
            List<Membership> objlst = new List<Membership>();
            MemberShipBLL objmembershipbll = new MemberShipBLL();
            objlst = objmembershipbll.GetAllMembership(Page, 50, 2, 1, null);
            return View(objlst);

        }
        [AdminAuthorizeAttribute]
        public ActionResult AddMemberShip()
        {
            Membership obj = new Membership();
            MemberShipBLL objmembershipbll = new MemberShipBLL();
            obj.Membership_Id = 0;
            obj.SortOrder = objmembershipbll.GetMaxSortOrder();
            obj.lstTypeOfStatus = objmembershipbll.GetStatusList();
            obj.CurrentPageNo = 1;
            return PartialView("_AddMembership", obj);
        }
        [AdminAuthorizeAttribute]
        public ActionResult InsertUpdateMemberShip(Membership model)
        {
            MemberShipBLL objmembershipbll = new MemberShipBLL();
            bool Result = false;
            if (ModelState.IsValid)
            {
                model.CreatedBy = Convert.ToInt32(SessionManagement.AdminUserId);
                model.ModifiedBy = Convert.ToInt32(SessionManagement.AdminUserId);
                Result = objmembershipbll.InsertUpdate(model);
                if (Result == true)
                {
                    if (model.Membership_Id == 0)
                    {
                        TempData["errorMessage"] = "Membership added successfully";
                    }
                    else
                    {
                        TempData["errorMessage"] = "Membership updated successfully";
                    }
                }
                else
                {
                    if (model.Membership_Id == 0)
                    {
                        TempData["errorMessage"] = "Failed to add Membership";
                    }
                    else
                    {
                        TempData["errorMessage"] = "Failed to update Membership";
                    }
                }
            }

            return RedirectToAction("ManageMemberShip");
        }
        [AdminAuthorizeAttribute]
        public ActionResult EditMemberShip(int Membership_Id, int page = 1)
        {
            MemberShipBLL objmembershipbll = new MemberShipBLL();
            Membership obj = new Membership();
            obj = objmembershipbll.GetById(Membership_Id);
            obj.lstTypeOfStatus = objmembershipbll.GetStatusList();
            obj.CurrentPageNo = page;
            return PartialView("_AddMembership", obj);
        }
        [AdminAuthorizeAttribute]
        public JsonResult DeleteMemberShip(int Membership_Id)
        {
            bool Result = false;
            MemberShipBLL objmembershipbll = new MemberShipBLL();
            Result = objmembershipbll.Remove(Membership_Id);
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        [AdminAuthorizeAttribute]
        public ActionResult GetMemberShip(int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext)
        {
            List<Membership> objlst = new List<Membership>();
            MemberShipBLL objmembershipbll = new MemberShipBLL();
            objlst = objmembershipbll.GetAllMembership(PageIndex, PageSize, SortColumn, SortDirection, Searchtext);
            return PartialView("_MembershipList", objlst);
        }

        #endregion

        #region ManageFeature
        [AdminAuthorizeAttribute]
        public ActionResult ManageFeature(int Page = 1)
        {
            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {
                List<Features> objlst = new List<Features>();
                FeaturesBLL obj = new FeaturesBLL();
                objlst = obj.GetAll(Page, 50, 2, 1, null);
                return View(objlst);
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }
        [AdminAuthorizeAttribute]
        public ActionResult AddFeature()
        {
            Features obj = new Features();
            FeaturesBLL objbll = new FeaturesBLL();
            obj.Feature_Id = 0;
            obj.SortOrder = objbll.GetMaxSortOrder();
            obj.lstTypeOfStatus = objbll.GetStatusList();
            return PartialView("_AddFeature", obj);
        }
        [AdminAuthorizeAttribute]
        public ActionResult InsertUpdateFeature(Features model)
        {
            FeaturesBLL objbll = new FeaturesBLL();
            bool Result = false;
            if (ModelState.IsValid)
            {
                model.CreatedBy = Convert.ToInt32(SessionManagement.AdminUserId);
                model.ModifiedBy = Convert.ToInt32(SessionManagement.AdminUserId);
                Result = objbll.InsertUpdate(model);
                if (Result == true)
                {
                    if (model.Feature_Id == 0)
                    {
                        TempData["SuccessMessage"] = "Feature added successfully";
                    }
                    else
                    {
                        TempData["SuccessMessage"] = "Feature updated successfully";
                    }
                }
                else
                {
                    if (model.Feature_Id == 0)
                    {
                        TempData["errorMessage"] = "Feature already exists with same title";
                    }
                    else
                    {
                        TempData["errorMessage"] = "Failed to update Feature";
                    }
                }
            }

            return RedirectToAction("ManageFeature");
        }
        [AdminAuthorizeAttribute]

        public ActionResult EditFeature(int Feature_Id, int page = 1)
        {
            FeaturesBLL objbll = new FeaturesBLL();
            Features obj = new Features();
            obj = objbll.GetById(Feature_Id);
            obj.lstTypeOfStatus = objbll.GetStatusList();
            return PartialView("_AddFeature", obj);
        }
        [AdminAuthorizeAttribute]
        public JsonResult DeleteFeature(int Feature_Id)
        {
            bool Result = false;
            FeaturesBLL objbll = new FeaturesBLL();
            Result = objbll.Remove(Feature_Id);
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        [AdminAuthorizeAttribute]
        public ActionResult GetFeature(int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext)
        {
            List<Features> objlst = new List<Features>();
            FeaturesBLL objbll = new FeaturesBLL();
            objlst = objbll.GetAll(PageIndex, PageSize, SortColumn, SortDirection, Searchtext);
            return PartialView("_FeatureList", objlst);
        }
        [AdminAuthorizeAttribute]
        public ActionResult MembershipFeatures(int Id = 0)
        {
            Membership_FeatureBLL ObjMemberBll = new Membership_FeatureBLL();
            MembershipFeatures ObjMember = new MembershipFeatures();
            ObjMember.MemberShip = ObjMemberBll.GetMemberShipDetails(Id);
            ObjMember.lstFeatures = ObjMemberBll.GetFeaturesDetails(Id);
            if (Id != 0)
            {
                ObjMember = ObjMemberBll.GetMembershipFeaturesDetails(Id);
            }
            // ObjMember.MembershipName = ObjMember.MembershipName;
            return View(ObjMember);
        }
        [AdminAuthorizeAttribute]
        public ActionResult InsertMemberShipFeatures(MembershipFeatures ObjMember)
        {
            Membership_FeatureBLL ObjMemberBll = new Membership_FeatureBLL();
            ObjMember.CreatedBy = Convert.ToInt32(SessionManagement.AdminUserId);
            bool Result = ObjMemberBll.InsertUpdateMembershipfeaturebll(ObjMember);
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MF()
        {
            Membership_FeatureBLL ObjMemberBll = new Membership_FeatureBLL();
            List<MembershipFeatures> lst = new List<MembershipFeatures>();
            lst = ObjMemberBll.GetDetialsofMembershipFeature();
            return View(lst);
        }
        public ActionResult DeleteMemberFeature(int Id)
        {
            bool Result = false;
            Membership_FeatureBLL ObjMemberBll = new Membership_FeatureBLL();
            Result = ObjMemberBll.DeleteMembershipfeaturesById(Id);
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetFeaturesListing()
        {
            Membership_FeatureBLL ObjMemberBll = new Membership_FeatureBLL();
            List<Features> lstFeatures = new List<Features>();
            lstFeatures = ObjMemberBll.GetFeaturesDetails(0);
            return View(lstFeatures);
        }
        [AdminAuthorizeAttribute]
        public ActionResult GetMemberByMembership()
        {
            MemberByMemberShip lst = new MemberByMemberShip();
            Membership_FeatureBLL ObjMemberBll = new Membership_FeatureBLL();
            lst.LstMembership = ObjMemberBll.GetMembershipPlan();
            return View("UserMemberShip", lst);
        }
        public ActionResult GetMembershipDataofUser(int MembershipId = 0, string SearchText = null, int PageIndex = 1, int SortDirection = 1, int SortColumn = 1, int ModeValue = 0)
        {
            MemberByMemberShip lst = new MemberByMemberShip();
            MemberShipBLL objMem = new MemberShipBLL();
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings.Get("PageSize"));
            if (MembershipId != 0)
            {
                lst = objMem.GetMemberListByMembership(Convert.ToInt32(SessionManagement.AdminUserId), MembershipId, SearchText, pageSize, PageIndex, SortDirection, SortColumn);
            }
            else
            {
                lst = objMem.GetAllMemberListByMembership(Convert.ToInt32(SessionManagement.AdminUserId), MembershipId, SearchText, pageSize, PageIndex, SortDirection, SortColumn);
            }

            if (!string.IsNullOrEmpty(Convert.ToString(ModeValue)))
            {
                if (ModeValue == 1)
                {
                    lst.lstMemberofMembership = lst.lstMemberofMembership.Where(x => x.PaymentMode == "True").ToList();
                }
                if (ModeValue == 2)
                {
                    lst.lstMemberofMembership = lst.lstMemberofMembership.Where(x => x.PaymentMode == "False").ToList();
                }
            }

            ViewBag.count = lst.lstMemberofMembership.Count;
            ViewBag.lstMembership = ObjMemberBll.GetMemberShipDetails(0);
            return View("MemberOfMembership", lst.lstMemberofMembership);
        }
        public ActionResult UpdateMemberMembership(string[] UserId, int MembershipId, string OldValues, string NewValues, string MembershipName)
        {
            bool Result = false;
            Membership_FeatureBLL ObjMemberBll = new Membership_FeatureBLL();
            Result = ObjMemberBll.UpdateMemberMemberShip(UserId, MembershipId, OldValues, NewValues, MembershipName, Convert.ToInt32(SessionManagement.AdminUserId));
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult MembersHistory(int UserId = 0)
        {
            List<Membership_History> lst = new List<Membership_History>();
            Membership_FeatureBLL ObjMemberBll = new Membership_FeatureBLL();
            lst = ObjMemberBll.GetMemberShipHistory(UserId);
            return View(lst);
        }
        public ActionResult GetMembershipVaulebyPrice(int MonthlyPrice)
        {
            List<Membership> lst = new List<Membership>();
            Membership_FeatureBLL ObjMemberBll = new Membership_FeatureBLL();
            lst = ObjMemberBll.GetMembershipvaluebyPrice(MonthlyPrice);
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [CustomAuthorizeAttribute]
        public ActionResult ShowReportToAdmin()
        {
            return View();
        }
        public ActionResult ShowLandingReport(int PageIndex, int PageSize, int SortColumn, int SortDirection, string SearchText)
        {
            LandingBLL objLandingBLL = new LandingBLL();
            LandingHistory objLandingHistory = new LandingHistory();
            List<LandingHistory> lstLan = new List<LandingHistory>();
            objLandingHistory.lstLandingHistory = objLandingBLL.GetLandingHistoryDetails(PageIndex, PageSize, SortColumn, SortDirection, SearchText);
            lstLan = objLandingHistory.lstLandingHistory;
            return View("PartialLandingReport", lstLan);
        }
        public ActionResult SendSupportWelcomeEmail(string emailaddress)
        {
            object obj = string.Empty;
            clsTemplate objclsTemplate = new clsTemplate();
            var IsMailSent = objclsTemplate.SendSupportWelcomeEmail(emailaddress);
            if (IsMailSent)
            {
                obj = "1";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        #region Account Upgrade For Recurring Payment
        public string CallSubscription(PyamentDetails objPyamentDetails, string transactionId)
        {
            RecurringSubscription objRecurringSubscription = new RecurringSubscription();
            clsColleaguesData objcls = new clsColleaguesData();
            TripleDESCryptoHelper objtriple = new TripleDESCryptoHelper();
            mdlCommon objCommon = new mdlCommon();
            int memberId = Convert.ToInt32(objPyamentDetails.Userid);
            DataTable dt = objcls.getNewPaymentHistoryOnUserId(memberId);
            int subId = 0;
            bool hasSubscription = false;
            string createResponse = string.Empty;
            string updateResponse = string.Empty;
            string cancelResponse = string.Empty;
            int membershipId = 0;
            string strReturn = string.Empty;
            if (dt != null && dt.Rows.Count > 0)
            {
                membershipId = Convert.ToInt32(dt.Rows[0]["membershipId"]);
                subId = Convert.ToInt32(dt.Rows[0]["subscriptionId"]);
                hasSubscription = true;
            }

            if (hasSubscription)
            {
                //if (Convert.ToInt32(objPyamentDetails.Planes) > membershipId || Convert.ToInt32(objPyamentDetails.Planes) < membershipId)
                //{
                cancelResponse = objRecurringSubscription.cancelSubscriptionBySystem(Convert.ToInt32(subId));
                bool isCancelUpdate = false;
                if (!string.IsNullOrEmpty(cancelResponse))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(cancelResponse);

                    XmlNodeList grandelemList = xmlDoc.GetElementsByTagName("messages");
                    if (grandelemList.Count > 0)
                    {
                        string code = grandelemList[0]["resultCode"].InnerText;
                        if (code == "Ok")
                        {
                            clsColleaguesData objCCData = new clsColleaguesData();
                            var isUpdate = objCCData.UpdateSubscriptionStatus(memberId, Convert.ToString(subId));
                            isCancelUpdate = true;
                        }
                    }
                }
                if (isCancelUpdate)
                {
                    createResponse = objRecurringSubscription.createSubscriptionBySystem(objPyamentDetails);
                }
                //}
                //else
                //{
                //    updateResponse = objRecurringSubscription.UpdateSubscriptionBySystem(objPyamentDetails, Convert.ToString(subId));
                //    if (!string.IsNullOrEmpty(updateResponse))
                //    {
                //        XmlDocument xmlDoc = new XmlDocument();
                //        xmlDoc.LoadXml(updateResponse);

                //        XmlNodeList grandelemList = xmlDoc.GetElementsByTagName("messages");
                //        XmlNodeList elemList = xmlDoc.GetElementsByTagName("message");
                //        XmlNodeList getSubscriptionName = xmlDoc.GetElementsByTagName("subscriptionId");
                //        XmlNodeList getCustomerProfile = xmlDoc.GetElementsByTagName("profile");
                //        if (grandelemList.Count > 0)
                //        {

                //            string code = grandelemList[0]["resultCode"].InnerText;
                //            if (code != "Ok")
                //            {
                //                ObjTemplate.RecurringPaymentFailed(objPyamentDetails.Email, objPyamentDetails.Firstname, objPyamentDetails.Lastname, objPyamentDetails.Phone, elemList[0]["text"].InnerText);
                //            }
                //            else
                //            {
                //                if (getSubscriptionName.Count > 0 && getCustomerProfile.Count > 0)
                //                {
                //                    string customerId = getCustomerProfile[0]["customerProfileId"].InnerText;
                //                    string subscriptionId = getSubscriptionName[0].InnerText;
                //                    clsColleaguesData objCCData = new clsColleaguesData();
                //                    string amount = objPyamentDetails.Amount.Replace('$', ' ');
                //                    string suscriptionType = objPyamentDetails.payInterval;
                //                    var isInsert = objCCData.NewInsertUsertransactionDetails(memberId, amount, transactionId, Convert.ToString(membershipId), customerId, DateTime.Now, suscriptionType, subscriptionId);
                //                    strReturn = "Done";
                //                }
                //            }
                //        }
                //    }
                //}
            }
            else
            {
                createResponse = objRecurringSubscription.createSubscriptionBySystem(objPyamentDetails);
            }
            if (!string.IsNullOrEmpty(createResponse))
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(createResponse);

                XmlNodeList grandelemList = xmlDoc.GetElementsByTagName("messages");
                XmlNodeList elemList = xmlDoc.GetElementsByTagName("message");
                XmlNodeList getSubscriptionName = xmlDoc.GetElementsByTagName("subscriptionId");
                XmlNodeList getCustomerProfile = xmlDoc.GetElementsByTagName("profile");
                if (grandelemList.Count > 0)
                {
                    string code = grandelemList[0]["resultCode"].InnerText;
                    if (code != "Ok")
                    {
                        ObjTemplate.RecurringPaymentFailed(objPyamentDetails.Email, objPyamentDetails.Firstname, objPyamentDetails.Lastname, objPyamentDetails.Phone, elemList[0]["text"].InnerText);
                    }
                    else
                    {
                        if (getSubscriptionName.Count > 0 && getCustomerProfile.Count > 0)
                        {
                            string customerId = getCustomerProfile[0]["customerProfileId"].InnerText;
                            string subscriptionId = getSubscriptionName[0].InnerText;
                            clsColleaguesData objCCData = new clsColleaguesData();
                            string amount = objPyamentDetails.Amount;
                            string suscriptionType = objPyamentDetails.payInterval;
                            var isInsert = objCCData.NewInsertUsertransactionDetails(memberId, amount, transactionId, objPyamentDetails.Planes, customerId, DateTime.Now, suscriptionType, subscriptionId);
                            objCCData.UpdateMembershipByRecurring(objPyamentDetails.Userid, objPyamentDetails.Planes);
                            strReturn = "Done";
                        }
                    }
                }
            }
            return strReturn;
        }
        #endregion

        public JsonResult GetEncryptIdForAdmin(string UserIds)
        {
            Membership_FeatureBLL ObjMemberBll = new Membership_FeatureBLL();
            var lst = ObjMemberBll.GetMemberShipDetails(0);
            List<SelectListItem> lstMembership = new List<SelectListItem>();
            foreach (var item in lst)
            {
                SelectListItem li = new SelectListItem();
                li.Text = item.Title;
                li.Value = Convert.ToString(item.Membership_Id);
                lstMembership.Add(li);
            }
            ViewBag.lstMembership = lstMembership;

            TripleDESCryptoHelper obj = new TripleDESCryptoHelper();
            var data = obj.encryptText(UserIds);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpgradeAccountManually(string UserId, string IntervalId, string MembershipId)
        {
            string data = string.Empty;
            string amount = string.Empty;
            clsColleaguesData objCCData = new clsColleaguesData();
            Member mdlmember = new Member();
            var lstMembership = mdlmember.GetMembershipById(Convert.ToInt32(MembershipId));

            mdlCommon objCommoncls = new mdlCommon();
            string firstAmount = objCommoncls.GetAmount(MembershipId);
            foreach (var item in lstMembership)
            {
                string ActualPrice = string.Empty;
                if (IntervalId == "1")
                {
                    ActualPrice = Convert.ToString(item.MonthlyPrice);
                    if (!string.IsNullOrEmpty(ActualPrice))
                    {
                        amount = ActualPrice;
                    }
                    else
                    {
                        amount = firstAmount;
                    }
                }
                if (IntervalId == "3")
                {
                    ActualPrice = Convert.ToString(item.QuaterlyPrice);
                    if (!string.IsNullOrEmpty(ActualPrice))
                    {
                        amount = ActualPrice;
                    }
                    else
                    {
                        var amt = Convert.ToString(Convert.ToInt32(firstAmount) * 3);
                        amount = amt;
                    }
                }
                if (IntervalId == "6")
                {
                    ActualPrice = Convert.ToString(item.HalfYearlyPrice);
                    if (!string.IsNullOrEmpty(ActualPrice))
                    {
                        amount = ActualPrice;
                    }
                    else
                    {
                        var amt = Convert.ToString(Convert.ToInt32(firstAmount) * 6);
                        amount = amt;
                    }
                }
                if (IntervalId == "12")
                {
                    ActualPrice = Convert.ToString(item.AnnualPrice);
                    if (!string.IsNullOrEmpty(ActualPrice))
                    {
                        amount = ActualPrice;
                    }
                    else
                    {
                        var amt = Convert.ToString(Convert.ToInt32(firstAmount) * 12);
                        amount = amt;
                    }
                }
            }
            var IsUpdated = objCCData.UpdateUserMembershipManually(UserId, IntervalId, MembershipId, amount);
            if (IsUpdated)
            {
                objCCData.UpdateMembershipByRecurring(UserId, MembershipId);
                data = "1";
            }
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPackagesAd(string packageid)
        {
            Features objFeatures = new Features();
            PaymentPackage objPayPack = new PaymentPackage();
            MemberShipBLL objMembll = new MemberShipBLL();
            Membership_FeatureBLL objMemFeature = new Membership_FeatureBLL();

            Membership objMembership = objMembll.GetById(Convert.ToInt32(packageid));
            objPayPack.memshipamt = Convert.ToString(objMembership.MonthlyPrice);
            objPayPack.memshipid = Convert.ToString(objMembership.Membership_Id);
            objPayPack.memshipname = Convert.ToString(objMembership.Title);
            var lst = objMemFeature.GetMembershipFeaturesDetails(Convert.ToInt32(packageid));
            objPayPack.lstfeatures = lst.lstFeatures;
            return View("GetPackagesAd", objPayPack);
        }
        public ActionResult CheckMemberOfMembership(int MembershipId)
        {
            MembershipDLL Objdll = new MembershipDLL();
            int MembershipUserCount = Objdll.GetMemberCountByMembershipId(MembershipId);
            return Json(MembershipUserCount, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Procedure()
        {
            List<Procedure> lst = new List<Procedure>();
            lst = ProcedureBLL.GetProcedureList();
            return View("ProcedureList",lst);
        }
        public ActionResult AddProcedure(Procedure obj)
        {
            if(obj.ProcedureId > 0)
            {
                List<Procedure> lst = new List<Procedure>();
                lst = ProcedureBLL.GetProcedureList(obj.ProcedureId);
                return View(lst.FirstOrDefault());
            }
            else
            {
                return View(obj);
            }
            
        }
        public JsonResult InsertProcedure(Procedure Obj)
        {
            if(Obj != null)
            {
                if(Obj.ProcedureId > 0)
                {
                    ProcedureBLL.UpdateProcedureDetails(Obj);
                    return Json("Update", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (ProcedureBLL.CheckProcedureAlreadyExistornot(Obj))
                    {
                        return Json("Procedure", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        ProcedureBLL.InsertProcedure(Obj);
                        return Json("Insert", JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json("Fail", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult DeleteProcedure(Procedure Obj)
        {
            return Json(ProcedureBLL.DeleteProcedure(Obj), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Rewards()
        {
            List<ColleagueData> objDoctorList = new List<ColleagueData>();
            return View(objDoctorList);
        }

        [CustomAuthorize]
        public JsonResult GetDoctorList()
        {
            List<ColleagueData> objDoctorList = new List<ColleagueData>();
            string SearchText = string.Empty;
            if (Convert.ToString(Request["q"]) != null)
            {
                SearchText = Convert.ToString(Request["q"]);
            }
            Search obj = new Search();
            obj.fname = SearchText;
            obj.PageIndex = 1;
            obj.PageSize = 50;
            objDoctorList = SearchProfileOfDoctor.GetDoctorList(obj);
            return Json(objDoctorList, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorize]
        public ActionResult GetRewardsForAdmin(int DentistId)
        {
            List<DentistReward> lst = new List<DentistReward>();
            List<SelectListItem> CodeList = RewardBLL.GetDentistRewardCodeList();
            ViewBag.Codelist = CodeList;            
            lst = RewardBLL.GetDentistRewardSettingDetails(DentistId);
            if (lst.Count > 0)
            {
                Session["DentistId"] = DentistId;
            }
            else
            {
                Session["DentistId"] = 0;
            }
            return View("_PartialAdminRewards", lst);
        }

        [CustomAuthorize]
        public ActionResult UpdateRewardList(List<DentistReward> lstreward)
        {
            bool result = false;
            int DentistId = 0;
            if ((int)Session["DentistId"] != 0)
            {
                DentistId = (int)Session["DentistId"];
                result = RewardBLL.UpdateRewardListOfUser(lstreward, DentistId);
                result = (result == true) ? true : false;
            }
            else
            {                
                result = false;
            }                       
            return Json(result);
        }
        [AdminAuthorizeAttribute]
        public ActionResult MerchantDetail()
        {
            List<MerchantOwner> objMerchantOwnerList = new List<MerchantOwner>();
            objMerchantOwnerList = ZPPaymentBAL.GetMerchantDetailList(0,"", null, null);
            return View(objMerchantOwnerList);
        }
        [AdminAuthorizeAttribute]
        public ActionResult RegisterMerchantDetail(ZiftPayFilter FilterObj)
        {
            FilterObj.PageIndex = 1;
            FilterObj.SortColumn = 1;
            FilterObj.SortDirection = 2;
            FilterObj.PageSize = 35;
            List<MerchantOwner> objMerchantOwnerList = new List<MerchantOwner>();
            List<MerchantOwner> objMerchantOwner = new List<MerchantOwner>();
            FilterObj.UserId = Convert.ToInt32(TempData["DoctorId"]);
            objMerchantOwner = ZPPaymentBAL.GetMerchantDetailList(Convert.ToInt32(TempData["AccountId"]),"", null, null);
            objMerchantOwnerList = ZPPaymentBAL.GetDoctorPaymentDetailList(FilterObj);
            TempData.Keep();
            var obj = new Tuple<List<MerchantOwner>, List<MerchantOwner>>(objMerchantOwnerList, objMerchantOwner);
            return View(obj);
        }

        /// <summary>
        /// This function is used for inner page of merchant details.
        /// </summary>
        /// <param name="FilterObj"></param>
        /// <returns></returns>
        [AdminAuthorizeAttribute]
        public ActionResult GetFilteredMerchantDetail(ZiftPayFilter FilterObj)
        {
            List<MerchantOwner> objMerchantOwnerList = new List<MerchantOwner>();
            List<MerchantOwner> objMerchantOwner = new List<MerchantOwner>();
            objMerchantOwner = ZPPaymentBAL.GetMerchantDetailList(Convert.ToInt32(TempData["AccountId"]), "", null, null);
            TempData.Keep();
            objMerchantOwnerList = ZPPaymentBAL.GetDoctorPaymentDetailList(FilterObj);
            var obj = new Tuple<List<MerchantOwner>, List<MerchantOwner>>(objMerchantOwnerList, objMerchantOwner);
            return View("_PartialMerchantPaymentDetailItems", obj);
        }

        /// <summary>
        /// This function only used for hide query string parameter while calling the original function. 
        /// </summary>
        /// <param name="AccountId"></param>
        /// <param name="DoctorId"></param>
        /// <returns></returns>
        [AdminAuthorizeAttribute]
        public ActionResult GetAccountId(int AccountId, int DoctorId)
        {
            TempData["AccountId"] = AccountId;
            TempData["DoctorId"] = DoctorId;
            TempData.Keep();
            return RedirectToAction("RegisterMerchantDetail");
        }

        /// <summary>
        /// This function Get merchant details for admin.
        /// </summary>
        /// <param name="AccountName"></param>
        /// <param name="FromDate"></param>
        /// <param name="ToDate"></param>
        /// <returns></returns>
        [AdminAuthorizeAttribute]
        public ActionResult PartialMerchantDetail(string AccountName = "",Nullable<DateTime> FromDate=null, Nullable<DateTime> ToDate= null)
        {
            List<MerchantOwner> objMerchantOwnerList = new List<MerchantOwner>();
            objMerchantOwnerList = ZPPaymentBAL.GetMerchantDetailList(0, AccountName, FromDate, ToDate);
            return PartialView("_PartialMerchantDetail", objMerchantOwnerList);
        }

        /// <summary>
        /// This function used for getting accounts name which has registered with zift pay.
        /// </summary>
        /// <param name="AccountName"></param>
        /// <returns></returns>
        [HttpPost]
        [AdminAuthorizeAttribute]
        public string SearchAccountName(string AccountName)
        {
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            StringBuilder StrResult = new StringBuilder();
            clsCommon objCommon = new clsCommon();
            dt = ZPPaymentBAL.SearchAccountName(AccountName);
            if (dt.Rows.Count > 0)
            {
                sb.Append("[");
                foreach (DataRow item in dt.Rows)
                {
                    sb.Append("{'label':\"" + objCommon.CheckNull(Convert.ToString(item["AccountName"]), string.Empty) + "\",'value':\"" + objCommon.CheckNull(Convert.ToString(item["AccountName"]), string.Empty) + "\"},");
                }
                StrResult.Append(sb.ToString().Substring(0, sb.ToString().Length - 1));
                StrResult.Append("]");
            }

            return StrResult.ToString();
        }

        #region FeatureMaster

        [AdminAuthorizeAttribute]
        public ActionResult FeatureMaster()
        {
            try
            {
                List<FeatureMaster> lstFeatureMaster = new List<FeatureMaster>();
                DataTable dt = AdminBLL.GetAllFeatureMaster();
                if(dt != null)
                {
                  
                    for (int i = 0; i < dt.Rows.Count ; i++)
                    {
                        lstFeatureMaster.Add(new FeatureMaster {
                            Id = Convert.ToInt32(dt.Rows[i]["Id"]),
                            Feature = Convert.ToString(dt.Rows[i]["Feature"]),
                            IsActive = Convert.ToBoolean(dt.Rows[i]["IsActive"])
                        });
                    }
                }
                return View(lstFeatureMaster);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [AdminAuthorizeAttribute]
        public ActionResult AddFeatureMaster()
        {
            try
            {
                FeatureMaster obj = new FeatureMaster();
                FeaturesBLL objbll = new FeaturesBLL();
                obj.Id = 0;
                obj.lstTypeOfStatus = objbll.GetStatusList();
                return PartialView("_AddFeatureMaster", obj);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [AdminAuthorizeAttribute]
        public ActionResult InsertUpdateFeatureMaster(FeatureMaster model)
        {
            try
            {
                int Result = 0;
                if (ModelState.IsValid)
                {
                    model.CreatedBy = Convert.ToInt32(SessionManagement.AdminUserId);
                    model.IsActive = model.Status == 1 ? true : false;
                    Result = AdminBLL.InsertUpdateFeatureMaster(model);
                    if (Result > 0)
                    {
                        if (model.Id == 0)
                        {
                            TempData["SuccessMessage"] = "Feature added successfully";
                        }
                        else
                        {
                            TempData["SuccessMessage"] = "Feature updated successfully";
                        }
                    }
                    else
                    {
                        if (model.Id == 0)
                        {
                            TempData["errorMessage"] = "Feature already exists with same title";
                        }
                        else
                        {
                            TempData["errorMessage"] = "Failed to update Feature";
                        }
                    }
                }
                return RedirectToAction("FeatureMaster");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [AdminAuthorizeAttribute]
        public ActionResult EditFeatureMaster(int Feature_Id)
        {
            try
            {
                FeaturesBLL objbll = new FeaturesBLL();
                FeatureMaster obj = new FeatureMaster();
                obj = AdminBLL.GetFeatureMasterById(Feature_Id);
                obj.lstTypeOfStatus = objbll.GetStatusList();
                return PartialView("_AddFeatureMaster", obj);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult CheckFeature(string Feature, string initialFeature)
        {
            bool ifFeatureExist = false;
            try
            {
                if (Feature.ToLower() == initialFeature.ToLower())
                {
                    ifFeatureExist = false;
                }
                else
                {
                    ifFeatureExist = AdminBLL.CheckFeatureMasterExist(Feature);
                }

                return Json(!ifFeatureExist, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Account Feature Setting

        public ActionResult AccountFeatureSetting()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetDoctorAutocompleteList(string text)
        {
            var result = AdminBLL.GetDoctorAutocompleteList(1, 10, text);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAccountFeatureOfAccount(int AccountId)
        {
            try
            {
                List<chkFeatureSetting> lstFeature = AdminBLL.GetAccountFeatureSettingByAccountId(AccountId);
                return PartialView("_AccountFeatureList",lstFeature);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public ActionResult UpdateAdminStatus(int Id,int status)
        {
            try
            {
                AccountFeatureSetting obj = new AccountFeatureSetting();
                obj.id = Id;
                obj.AdminStatus = Convert.ToBoolean(status);
                int UpdateId = AdminBLL.InsertUpdateAccountFeatureSetting(obj, true);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region Sync
        public ActionResult DentistLastSync()
        {
            try
            {
                List<DentistSyncModel> obj = new List<DentistSyncModel>();
                obj = AdminBLL.DentistLastSync();
                View(obj);
            }
            catch (Exception)
            {

                throw;
            }
            return View();
        }
        #endregion


        #region Email Verification Listing
        [CustomAuthorize]
        public ActionResult EmailVerification()
        {
            if (SessionManagement.AdminUserId != null && SessionManagement.AdminUserId != "")
            {               
                return View();
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
            
        }

        public  ActionResult EmailVerificationList(EmailFilter objEmailFilter )
        {
            List<EmailVerification> objEmailVerificationList = new List<EmailVerification>();
            objEmailVerificationList = AdminBLL.GetEmailVerificationListReport(objEmailFilter, Convert.ToString(SessionManagement.TimeZoneSystemName));                     
            return PartialView("_EmailVerificationList", objEmailVerificationList);
        }
        #endregion
    }
}



