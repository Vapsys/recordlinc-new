﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewRecordlinc.Models.Colleagues;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Configuration;
using NewRecordlinc.Models.Common;
using NewRecordlinc.App_Start;
using System.Drawing;
using System.IO;
using System.Web.Script.Serialization;
using System.Text.RegularExpressions;
using System.Web.Helpers;
using DataAccessLayer.Appointment;
using NewRecordlinc.Models;
using DataAccessLayer.ProfileData;
using BO.Models;
using BusinessLogicLayer;

namespace NewRecordlinc.Controllers
{
    public class ProfileController : Controller
    {
        clsColleaguesData objColleaguesData = new clsColleaguesData();
        clsCommon objCommon = new clsCommon();
        Member MdlMember = null;
        mdlCommon MdlCommon = null;
        ProfileBLL objProfile = null;
        clsTemplate ObjTemplate = new clsTemplate();
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        clsAppointmentData objclsAppointmentData;
        public ActionResult NewIndex()
        {
            SectionPublicProfile objprofiles = new SectionPublicProfile();
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                bool Result = objCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
                string DoctoreId = Request.QueryString["DoctorId"];
                int ViewProfileId;
                int DoctId = 0;
                if (Request["hdnViewProfile"] != null)
                {
                    DoctId = Convert.ToInt32(Request["hdnViewProfile"]);
                }

                if (!string.IsNullOrEmpty(DoctoreId))
                {
                    ViewBag.hdnViewProfile = true;
                    ViewProfileId = int.Parse(DoctoreId);
                    if (ViewProfileId == Convert.ToInt32(Session["UserId"]))
                    {
                        ViewBag.hdnViewProfile = false;
                    }
                }
                else if (DoctId > 0)
                {
                    ViewBag.hdnViewProfile = true;
                    ViewProfileId = DoctId;
                    if (ViewProfileId == Convert.ToInt32(Session["UserId"]))
                    {
                        ViewBag.hdnViewProfile = false;
                    }
                }
                else
                {
                    ViewBag.hdnViewProfile = false;
                    ViewProfileId = Convert.ToInt32(Session["UserId"]);
                }



                string ImageName = null;
                MdlMember = new Member();


                DataSet ds = new DataSet();
                ds = MdlMember.GetProfileDetailsOfDoctorByID(ViewProfileId);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string Desciption = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Description"]), string.Empty);
                    MdlMember.UserId = Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]);
                    MdlMember.FirstName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]), string.Empty);
                    MdlMember.LastName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LastName"]), string.Empty);
                    MdlMember.Salutation = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Salutation"]), string.Empty);
                    if (!string.IsNullOrEmpty(Desciption))
                    {
                        MdlMember.Description = Desciption.Replace("\n", "<br/>");
                    }

                    MdlMember.Title = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Title"]), string.Empty);
                    MdlMember.MiddleName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["MiddleName"]), string.Empty);
                    MdlMember.PublicProfile = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["PublicProfileUrl"]), string.Empty);
                    MdlMember.WebsiteURL = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["WebsiteURL"]), string.Empty);

                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        MdlMember.City = objCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["City"]), string.Empty);
                        MdlMember.State = objCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["State"]), string.Empty);
                        MdlMember.ZipCode = objCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["ZipCode"]), string.Empty);

                        MdlMember.lstDoctorAddressDetails = (from p in ds.Tables[2].AsEnumerable()
                                                             select new AddressDetails
                                                             {
                                                                 AddressInfoID = Convert.ToInt32(p["AddressInfoID"]),
                                                                 Location = objCommon.CheckNull(Convert.ToString(p["LocationName"]), string.Empty),
                                                                 ExactAddress = objCommon.CheckNull(Convert.ToString(p["ExactAddress"]), string.Empty),
                                                                 Address2 = objCommon.CheckNull(Convert.ToString(p["Address2"]), string.Empty),
                                                                 City = objCommon.CheckNull(Convert.ToString(p["City"]), string.Empty),
                                                                 State = objCommon.CheckNull(Convert.ToString(p["State"]), string.Empty),
                                                                 Country = objCommon.CheckNull(Convert.ToString(p["Country"]), string.Empty),
                                                                 ZipCode = objCommon.CheckNull(Convert.ToString(p["ZipCode"]), string.Empty),
                                                                 Phone = objCommon.CheckNull(Convert.ToString(p["Phone"]), string.Empty),
                                                                 Fax = objCommon.CheckNull(Convert.ToString(p["Fax"]), string.Empty),
                                                                 EmailAddress = objCommon.CheckNull(Convert.ToString(p["EmailAddress"]), string.Empty),
                                                                 ContactType = Convert.ToInt32(p["ContactType"]),
                                                                 Mobile = objCommon.CheckNull(Convert.ToString(p["Mobile"]), string.Empty)
                                                             }).ToList();

                    }

                    ImageName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["ImageName"]), string.Empty);

                    if (string.IsNullOrEmpty(ImageName))
                    {
                        MdlMember.ImageName = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty);
                    }
                    else
                    {
                        MdlMember.ImageName = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + ImageName;
                    }
                    if (MdlMember.UserId == Convert.ToInt32(SessionManagement.UserId))
                    {
                        Session["DoctorImage"] = MdlMember.ImageName;
                    }


                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        MdlMember.OfficeName = objCommon.CheckNull(Convert.ToString(ds.Tables[1].Rows[0]["AccountName"]), string.Empty);
                    }
                    if (ds.Tables[15].Rows.Count > 0)
                    {
                        objprofiles.PatientForms = Convert.ToBoolean(ds.Tables[15].Rows[0]["PatientForms"]);
                        objprofiles.ReferPatient = Convert.ToBoolean(ds.Tables[15].Rows[0]["ReferPatient"]);
                        objprofiles.Reviews = Convert.ToBoolean(ds.Tables[15].Rows[0]["Reviews"]);
                        objprofiles.SpecialOffers = Convert.ToBoolean(ds.Tables[15].Rows[0]["SpecialOffers"]);
                        objprofiles.AppointmentBooking = Convert.ToBoolean(ds.Tables[15].Rows[0]["AppointmentBooking"]);
                        MdlMember.ObjProfileSection = objprofiles;
                    }
                    else
                    {
                        objprofiles.PatientForms =true;
                        objprofiles.ReferPatient = true;
                        objprofiles.Reviews = true;
                        objprofiles.SpecialOffers = true;
                        objprofiles.AppointmentBooking = true;
                        MdlMember.ObjProfileSection = objprofiles;
                    }
                    MdlMember.lstsecondarywebsitelist = MdlMember.GetSecondaryWebsitelistByUserId(MdlMember.UserId);

                    //MdlMember.lstDoctorAddressDetails = MdlMember.GetDoctorAddressDetailsById(Convert.ToInt32(Session["ParentUserId"]));
                    MdlMember.lstGetSocialMediaDetailByUserId = MdlMember.GetSocialMediaDetailByUserId(MdlMember.UserId);
                    MdlMember.lstEducationandTraining = MdlMember.GetEducationandTrainingDetailsForDoctor(MdlMember.UserId);
                    MdlMember.lstProfessionalMemberships = MdlMember.GetProfessionalMembershipsByUserId(MdlMember.UserId);
                    MdlMember.lstTeamMemberDetailsForDoctor = MdlMember.GetTeamMemberDetailsOfDoctor(MdlMember.UserId, 1, 50);
                    MdlMember.lstSpeacilitiesOfDoctor = MdlMember.GetSpeacilitiesOfDoctorById(MdlMember.UserId);
                    MdlMember.lstBanner = MdlMember.GetBannerDetailByUserId(MdlMember.UserId);
                    MdlMember.lstProcedure = MdlMember.GetUserProcedureList(MdlMember.UserId);
                    if (ds.Tables[10].Rows.Count > 0)
                    {
                        MdlMember.licensedetails = MdlMember.GetLicensedetailsByUserId(MdlMember.UserId, SessionManagement.TimeZoneSystemName);
                    }
                    //if (DoctId != 0)
                    //{
                    //    MdlMember.lstBanner = MdlMember.GetBannerDetailByUserId(DoctId);
                    //}
                    //else
                    //{
                    //    MdlMember.lstBanner = MdlMember.GetBannerDetailByUserId(Convert.ToInt32(Session["UserId"]));
                    //}
                    string defaultPath = "/Resources/Gallery/" + MdlMember.UserId + "/";
                    string filePath = ConfigurationManager.AppSettings.Get("GallaryFilePath");
                    if (string.IsNullOrEmpty(filePath))
                    {
                        filePath = defaultPath;
                    }
                    MdlMember.lstGallary = mdlGallary.getGallaryList(MdlMember.UserId, 0);
                    MdlMember.GallaryPath = filePath;
                    objProfile = new ProfileBLL();
                    MdlMember.lstInsurance = objProfile.GetMemberInsuranceByUserId(MdlMember.UserId);
                }
                return View("Index", MdlMember);
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }



        public ActionResult Index()
        {
            SectionPublicProfile objprofiles = new SectionPublicProfile();
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                bool Result = objCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
                string DoctoreId = Request.QueryString["DoctorId"];
                int ViewProfileId;
                int DoctId = 0;
                if (Request["hdnViewProfile"] != null)
                {
                    DoctId = Convert.ToInt32(Request["hdnViewProfile"]);
                }

                if (!string.IsNullOrEmpty(DoctoreId))
                {
                    ViewBag.hdnViewProfile = true;
                    ViewProfileId = int.Parse(DoctoreId);
                    if (ViewProfileId == Convert.ToInt32(Session["UserId"]))
                    {
                        ViewBag.hdnViewProfile = false;
                    }
                }
                else if (DoctId > 0)
                {
                    ViewBag.hdnViewProfile = true;
                    ViewProfileId = DoctId;
                    if (ViewProfileId == Convert.ToInt32(Session["UserId"]))
                    {
                        ViewBag.hdnViewProfile = false;
                    }
                }
                else
                {
                    ViewBag.hdnViewProfile = false;
                    ViewProfileId = Convert.ToInt32(Session["UserId"]);
                }



                string ImageName = null;
                MdlMember = new Member();


                DataSet ds = new DataSet();
                ds = MdlMember.GetProfileDetailsOfDoctorByID(ViewProfileId);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string Desciption = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Description"]), string.Empty);
                    MdlMember.UserId = Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]);
                    MdlMember.FirstName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]), string.Empty);
                    MdlMember.LastName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LastName"]), string.Empty);
                    MdlMember.Salutation = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Salutation"]), string.Empty);
                    if (!string.IsNullOrEmpty(Desciption))
                    {
                        MdlMember.Description = Desciption.Replace("\n", "<br/>");
                    }

                    MdlMember.Title = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Title"]), string.Empty);
                    MdlMember.MiddleName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["MiddleName"]), string.Empty);
                    MdlMember.PublicProfile = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["PublicProfileUrl"]), string.Empty);
                    MdlMember.WebsiteURL = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["WebsiteURL"]), string.Empty);

                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        MdlMember.City = objCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["City"]), string.Empty);
                        MdlMember.State = objCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["State"]), string.Empty);
                        MdlMember.ZipCode = objCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["ZipCode"]), string.Empty);

                        MdlMember.lstDoctorAddressDetails = (from p in ds.Tables[2].AsEnumerable()
                                                             select new AddressDetails
                                                             {
                                                                 AddressInfoID = Convert.ToInt32(p["AddressInfoID"]),
                                                                 Location = objCommon.CheckNull(Convert.ToString(p["LocationName"]), string.Empty),
                                                                 ExactAddress = objCommon.CheckNull(Convert.ToString(p["ExactAddress"]), string.Empty),
                                                                 Address2 = objCommon.CheckNull(Convert.ToString(p["Address2"]), string.Empty),
                                                                 City = objCommon.CheckNull(Convert.ToString(p["City"]), string.Empty),
                                                                 State = objCommon.CheckNull(Convert.ToString(p["State"]), string.Empty),
                                                                 Country = objCommon.CheckNull(Convert.ToString(p["Country"]), string.Empty),
                                                                 ZipCode = objCommon.CheckNull(Convert.ToString(p["ZipCode"]), string.Empty),
                                                                 Phone = objCommon.CheckNull(Convert.ToString(p["Phone"]), string.Empty),
                                                                 Fax = objCommon.CheckNull(Convert.ToString(p["Fax"]), string.Empty),
                                                                 EmailAddress = objCommon.CheckNull(Convert.ToString(p["EmailAddress"]), string.Empty),
                                                                 ContactType = Convert.ToInt32(p["ContactType"]),
                                                                 Mobile = objCommon.CheckNull(Convert.ToString(p["Mobile"]), string.Empty),
                                                                 //Olivia changes on 10-04-2018 for Dr. Lan Allan's website. - DLADCETK-1
                                                                 SchedulingLink = objCommon.CheckNull(Convert.ToString(p["SchedulingLink"]), string.Empty)
                                                             }).ToList();

                    }

                    //To Get Primary Location from the Location List.
                    MdlMember.lstDoctorSortedAddressDetails = MdlMember.lstDoctorAddressDetails;
                    MdlMember.lstDoctorAddressDetails =  MdlMember.lstDoctorSortedAddressDetails.Where(x => x.ContactType == 1).ToList();
                    MdlMember.lstDoctorAddressDetails.AddRange(MdlMember.lstDoctorSortedAddressDetails.Where(x => x.ContactType != 1).ToList());

                    ImageName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["ImageName"]), string.Empty);

                    if (string.IsNullOrEmpty(ImageName))
                    {
                        MdlMember.ImageName = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty);
                    }
                    else
                    {
                        MdlMember.ImageName = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + ImageName;
                    }
                    if (MdlMember.UserId == Convert.ToInt32(SessionManagement.UserId))
                    {
                        Session["DoctorImage"] = MdlMember.ImageName;
                    }


                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        MdlMember.OfficeName = objCommon.CheckNull(Convert.ToString(ds.Tables[1].Rows[0]["AccountName"]), string.Empty);
                    }
                    if (ds.Tables[15].Rows.Count > 0)
                    {
                        objprofiles.PatientForms = Convert.ToBoolean(ds.Tables[15].Rows[0]["PatientForms"]);
                        objprofiles.ReferPatient = Convert.ToBoolean(ds.Tables[15].Rows[0]["ReferPatient"]);
                        objprofiles.Reviews = Convert.ToBoolean(ds.Tables[15].Rows[0]["Reviews"]);
                        objprofiles.SpecialOffers = Convert.ToBoolean(ds.Tables[15].Rows[0]["SpecialOffers"]);
                        objprofiles.AppointmentBooking = Convert.ToBoolean(ds.Tables[15].Rows[0]["AppointmentBooking"]);
                        MdlMember.ObjProfileSection = objprofiles;
                    }
                    MdlMember.lstsecondarywebsitelist = MdlMember.GetSecondaryWebsitelistByUserId(MdlMember.UserId);

                    //MdlMember.lstDoctorAddressDetails = MdlMember.GetDoctorAddressDetailsById(Convert.ToInt32(Session["ParentUserId"]));
                    MdlMember.lstGetSocialMediaDetailByUserId = MdlMember.GetSocialMediaDetailByUserId(MdlMember.UserId);
                    MdlMember.lstEducationandTraining = MdlMember.GetEducationandTrainingDetailsForDoctor(MdlMember.UserId);
                    MdlMember.lstProfessionalMemberships = MdlMember.GetProfessionalMembershipsByUserId(MdlMember.UserId);
                    MdlMember.lstTeamMemberDetailsForDoctor = MdlMember.GetTeamMemberDetailsOfDoctor(MdlMember.UserId, 1, int.MaxValue);
                    MdlMember.lstSpeacilitiesOfDoctor = MdlMember.GetSpeacilitiesOfDoctorById(MdlMember.UserId);
                    MdlMember.lstBanner = MdlMember.GetBannerDetailByUserId(MdlMember.UserId);
                    MdlMember.lstProcedure = MdlMember.GetUserProcedureList(MdlMember.UserId);
                    if (ds.Tables[10].Rows.Count > 0)
                    {
                        MdlMember.licensedetails = MdlMember.GetLicensedetailsByUserId(MdlMember.UserId, SessionManagement.TimeZoneSystemName);
                    }
                    string defaultPath = "/Resources/Gallery/" + MdlMember.UserId + "/";
                    string filePath = ConfigurationManager.AppSettings.Get("GallaryFilePath");
                    if (string.IsNullOrEmpty(filePath))
                    {
                        filePath = defaultPath;
                    }
                    MdlMember.lstGallary = mdlGallary.getGallaryList(MdlMember.UserId, 0);
                    MdlMember.GallaryPath = filePath;
                    objProfile = new ProfileBLL();
                    MdlMember.lstInsurance = objProfile.GetMemberInsuranceByUserId(MdlMember.UserId);
                }
                return View("NewIndex", MdlMember);
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }


        #region Code for Doctor Addresses
        [HttpPost]
        public PartialViewResult GetAddressForColleagueByAddressInfoID(int AddressInfoID)
        {
            MdlMember = new Member();

            MdlMember.lstTimeZone = MdlMember.GetTimeZoneList();
            if (AddressInfoID > 0)
            {
                MdlMember.clsAddressDetailsById = MdlMember.GetDoctorAddressDetailsByAddressInfoID(AddressInfoID)[0];
                ViewBag.country = CountryScript(objCommon.CheckNull(MdlMember.clsAddressDetailsById.Country, string.Empty));
                ViewBag.state = StateScript(objCommon.CheckNull(MdlMember.clsAddressDetailsById.Country, string.Empty), MdlMember.clsAddressDetailsById.State.ToString());

            }
            else
            {
                MdlMember.clsAddressDetailsById = new AddressDetails();
                ViewBag.country = CountryScript("");
                ViewBag.state = StateScript("", "0");
            }

            ViewBag.Specialities = SpecialitiesScript("");
            return PartialView("PartialEditAddressPopUp", MdlMember);
        }

        public PartialViewResult NewGetAddressForColleagueByAddressInfoID(int AddressInfoID)
        {
            MdlMember = new Member();

            MdlMember.lstTimeZone = MdlMember.GetTimeZoneList();
            if (AddressInfoID > 0)
            {
                MdlMember.clsAddressDetailsById = MdlMember.GetDoctorAddressDetailsByAddressInfoID(AddressInfoID)[0];
                ViewBag.country = CountryScript(objCommon.CheckNull(MdlMember.clsAddressDetailsById.Country, string.Empty));
                ViewBag.state = StateScript(objCommon.CheckNull(MdlMember.clsAddressDetailsById.Country, string.Empty), MdlMember.clsAddressDetailsById.State.ToString());

            }
            else
            {
                MdlMember.clsAddressDetailsById = new AddressDetails();
                ViewBag.country = CountryScript("");
                ViewBag.state = StateScript("", "0");
            }

            ViewBag.Specialities = SpecialitiesScript("");
            return PartialView("_PartialAddEditAddressPopup", MdlMember);
        } 

        public string CountryScript(string countryId)
        {
            if (countryId == "")
            {
                countryId = "US";
            }
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.Country();
            sb.Append("<select class=\"form-control\"  id=\"drpCountry\" name=\"drpCountry\" onchange=\"FillState();\" tabindex=\"5\">");
            foreach (var item in lst)
            {
                if (countryId == item.Value)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
                else
                {
                    sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
            }
            sb.Append("</select>");
            return sb.ToString();
        }

        public string StateScript(string countryId, string stateId)
        {
            if (countryId == "")
            {
                countryId = "US";
            }
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.StateByCountryCode(countryId);
            sb.Append("<select class=\"form-control\" id=\"drpState\" name=\"drpState\">");
            foreach (var item in lst)
            {

                if (stateId == item.Value)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
                else
                {
                    sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
            }
            sb.Append("</select>");
            return sb.ToString();
        }

        public string SpecialitiesScript(string SpecialitiId)
        {
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.DoctorSpecialities();
            sb.Append("<select class=\"form-control\" id=\"drpSpeciality\" name=\"drpSpeciality\" tabindex=\"4\">");
            foreach (var item in lst)
            {
                if (SpecialitiId == item.Value)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
                else
                {
                    sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
            }
            sb.Append("</select>");
            return sb.ToString();
        }

        //Olivia changes on 10-04-2018 for Dr. Lan Allan's website. - DLADCETK-1
        [HttpPost]
        public JsonResult UpdateAddressForDoctor(int AddressInfoID, string ExactAddress, string Address2, string City, string State, string Country, string Zipcode, string IsPrimary, string Email, string Phone, string Fax, string Website, string Location, int TimezoneId, string Mobile, string ExternalPMSId, string SchedulingLink, int? Userid = 0)
        {


            int LocationId = 0;
            object obj = "0";
            MdlMember = new Member();

            LocationId = MdlMember.UpdateAddressForDoctor(AddressInfoID, ExactAddress, Address2, City, State, Country, Zipcode, IsPrimary, Email, Phone, Fax, Location, TimezoneId, Mobile, ExternalPMSId, SchedulingLink, Userid);
            if (LocationId != 0)
            {
                if (!string.IsNullOrEmpty(Website))
                {
                    var userId = Convert.ToInt32(Session["UserId"]);
                    var strWeb = UpdateWebsite(userId.ToString(), Website, LocationId);
                    if (strWeb)
                    {
                        obj = "1";
                    }
                }
                else
                {
                    obj = "1";
                }

                if (!string.IsNullOrEmpty(SessionManagement.LocationId))
                {
                    if (Convert.ToInt32(SessionManagement.LocationId) == AddressInfoID)
                        SessionManagement.TimeZoneSystemName = MdlMember.GetTimeZoneValue(TimezoneId);
                }
            }
            else
            {
                obj = "0";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RemoveDoctorAddressDetailsByAddressInfoID(int AddressInfoID)
        {
            bool Result = false;
            object obj = "";
            MdlMember = new Member();
            string Fullname = string.Empty;

            DataTable dt = new DataTable();
            dt = objColleaguesData.GetDoctorsByLocaiton(AddressInfoID);

            if (dt.Rows.Count > 0)
            {
                //Fullname = objCommon.CheckNull(Convert.ToString(dt.Rows[0]["FirstName"]), string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dt.Rows[0]["LastName"]), string.Empty);
                //obj = Fullname;
                foreach (DataRow row in dt.Rows)
                {
                    string fullName = Convert.ToString(row["FirstName"]) + " " + Convert.ToString(row["LastName"]);
                    if (string.IsNullOrEmpty(obj.ToString()))
                        obj = fullName;
                    else
                        obj += ", " + fullName;
                }
            }
            else
            {
                Result = MdlMember.RemoveDoctorAddressDetailsByAddressInfoID(AddressInfoID);
                if (Result)
                {
                    obj = "1";
                }
                else
                {
                    obj = "0";
                }
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public JsonResult FillCountry(string cid)
        {
            return Json(CountryScript(cid), JsonRequestBehavior.AllowGet);
        }
        public JsonResult FillState(string cid, string sid)
        {
            return Json(StateScript(cid, sid), JsonRequestBehavior.AllowGet);
        }



        #endregion

        #region Code for Social Media
        [HttpPost]
        public PartialViewResult GetSocialMediaDetailByUserId(int UserId)
        {
            MdlMember = new Member();
            MdlMember.UserId = UserId;

            MdlMember.lstGetSocialMediaDetailByUserId = MdlMember.GetSocialMediaDetailByUserId(UserId);

            if (MdlMember.lstGetSocialMediaDetailByUserId.Count > 0)
            {
                MdlMember.clsSocialMediaForDoctor = MdlMember.GetSocialMediaDetailByUserId(UserId)[0];
            }
            else
            {
                MdlMember.clsSocialMediaForDoctor = new SocialMediaForDoctor();
            }
            return PartialView("PartialEditSocialMediaInformation", MdlMember);
        }

        [HttpPost]
        public PartialViewResult NewGetSocialMediaDetailByUserId(int UserId)
        {
            MdlMember = new Member();
            MdlMember.UserId = UserId;

            MdlMember.lstGetSocialMediaDetailByUserId = MdlMember.GetSocialMediaDetailByUserId(UserId);

            if (MdlMember.lstGetSocialMediaDetailByUserId.Count > 0)
            {
                MdlMember.clsSocialMediaForDoctor = MdlMember.GetSocialMediaDetailByUserId(UserId)[0];
            }
            else
            {
                MdlMember.clsSocialMediaForDoctor = new SocialMediaForDoctor();
            }
            return PartialView("_PartialSocialMediaPopUp", MdlMember);
        }



        //RM-355: Changes for adding instagram at Social media section
        [HttpPost]
        public JsonResult UpdateSocialMediaForDoctor(int UserId, string FacebookUrl, string LinkedinUrl, string TwitterUrl,string InstagramUrl, string GoogleplusUrl, string YoutubeUrl, string PinterestUrl, string BlogUrl, string YelpUrl)
        {
            bool Result = false;
            MdlMember = new Member();
            object obj = "";

            Result = MdlMember.UpdateSocialMediaForDoctor(UserId, FacebookUrl, LinkedinUrl, TwitterUrl,InstagramUrl, GoogleplusUrl, YoutubeUrl, PinterestUrl, BlogUrl, YelpUrl);
            if (Result)
            {
                obj = "1";
            }
            else
            {
                obj = "0";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Code for Description

        [HttpPost]
        public PartialViewResult GetDescriptionByUserId(int UserId)
        {
            MdlMember = new Member();

            MdlMember.UserId = UserId;
            MdlMember.Description = MdlMember.GetGetDescriptionForDoctor(UserId);
            return PartialView("PartialEditDescription", MdlMember);
        }

        [HttpPost]
        public PartialViewResult NewGetDescriptionByUserId(int UserId)
        {
            MdlMember = new Member();
            MdlMember.UserId = UserId;
            MdlMember.Description = MdlMember.GetGetDescriptionForDoctor(UserId);
            return PartialView("_PartialEditDescriptionPopUp", MdlMember);
        }

        [HttpPost]
        [ValidateInput(false)]
        public JsonResult UpdateDescriptionOfDoctor(int UserId, string Description)
        {
            bool Result = false;
            object obj = "";

            Result = objColleaguesData.UpdateMemberDetailsByUserId(UserId, "Description", Description);
            if (Result)
            {
                obj = "1";
            }
            else
            {
                obj = "0";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Code For Education and Training
        [HttpPost]
        public PartialViewResult GetEducationandTrainingDetailsById(int Id, int UserId)
        {
            MdlMember = new Member();
            MdlMember.UserId = UserId;
            MdlMember.clsEducationandTrainingForDoctorById = MdlMember.GetEducationandTrainingDetailsForDoctorById(Id)[0];
            ViewBag.yearlist = YearScript(MdlMember.clsEducationandTrainingForDoctorById.YearAttended);


            return PartialView("PartialEditEducationAndTrainingInformation", MdlMember);
        }

        [HttpPost]
        public PartialViewResult NewGetEducationandTrainingDetailsById(int Id, int UserId)
        {
            MdlMember = new Member();
            MdlMember.UserId = UserId;
            MdlMember.clsEducationandTrainingForDoctorById = MdlMember.GetEducationandTrainingDetailsForDoctorById(Id)[0];
            ViewBag.yearlist = YearScript(MdlMember.clsEducationandTrainingForDoctorById.YearAttended);
            return PartialView("_PartialEducationAndTrainingPopUp", MdlMember);
        }


        [HttpPost]
        public JsonResult InsertUpdateLicenseDetails(string LicenseNumber, string LicenseState, string LicenseExpiration)
        {

            object obj = "";
            objColleaguesData = new clsColleaguesData();
            DateTime? dt_LicenseExpiration = null;
            if (!string.IsNullOrEmpty(LicenseExpiration))
            {
                dt_LicenseExpiration = Convert.ToDateTime(LicenseExpiration);
                dt_LicenseExpiration = clsHelper.ConvertToUTC(Convert.ToDateTime(dt_LicenseExpiration), SessionManagement.TimeZoneSystemName);
            }
            bool Result = objColleaguesData.InsertUpdateLicenseDetils(Convert.ToInt32(Session["UserId"]), LicenseNumber, LicenseState, dt_LicenseExpiration);
            if (Result)
            {
                obj = "1";
            }
            else
            {
                obj = "0";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult EducationAndTrainingInsertAndUpdate(int Id, int UserId, string Institute, string Specialisation, string YearAttended)
        {
            bool Result = false;
            object obj = "";
            MdlMember = new Member();
            Result = MdlMember.EducationAndTrainingInsertAndUpdate(Id, UserId, Institute, Specialisation, YearAttended);
            if (Result)
            {
                obj = "1";
            }
            else
            {
                obj = "0";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RemoveEducationAndTraining(int Id)
        {
            bool Result = false;
            object obj = "";
            MdlMember = new Member();
            Result = MdlMember.RemoveEducationAndTraining(Id);
            if (Result)
            {
                obj = "1";
            }
            else
            {
                obj = "0";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string SearchUniversityNames(string UniversityNames)
        {
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            StringBuilder StrResult = new StringBuilder();
            dt = objCommon.SearchUniversityNames(UniversityNames);
            if (dt.Rows.Count > 0)
            {
                sb.Append("[");
                foreach (DataRow item in dt.Rows)
                {
                    sb.Append("{'label':\"" + objCommon.CheckNull(Convert.ToString(item["UniversityName"]), string.Empty) + "\",'value':\"" + objCommon.CheckNull(Convert.ToString(item["UniversityName"]), string.Empty) + "\"},");
                }
                StrResult.Append(sb.ToString().Substring(0, sb.ToString().Length - 1));
                StrResult.Append("]");
            }

            return StrResult.ToString();
        }

        public string YearScript(string Year)
        {

            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.YearList();

            sb.Append("<select class=\"form-control\" size=\"1\" " + (string.IsNullOrEmpty(Year) ? "selected=\"selected\"" : "") + " id=\"YearAttended\" name=\"YearAttended\">");
            foreach (var item in lst)
            {
                if (Year == item.Value)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
                else
                {
                    sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
            }
            sb.Append("</select>");
            return sb.ToString();
        }

        #endregion

        #region Code For Professional Memberships
        [HttpPost]
        public PartialViewResult GetProfessionalMembershipsById(int Id, int UserId)
        {
            MdlMember = new Member();
            MdlMember.UserId = UserId;
            MdlMember.lstProfessionalMembershipForDoctorById = MdlMember.GetProfessionalMembershipsByUniqueId(Id);
            return PartialView("PartialEditProfessionalMemberships", MdlMember);
        }

        public PartialViewResult NewGetProfessionalMembershipsById(int Id, int UserId)
        {
            MdlMember = new Member();
            MdlMember.UserId = UserId;
            MdlMember.lstProfessionalMembershipForDoctorById = MdlMember.GetProfessionalMembershipsByUniqueId(Id);
            return PartialView("_PartialAddEditProfessionalMembershipPopUp", MdlMember);
        }

        [HttpPost]
        public PartialViewResult EditLicenseDetails(int id, int Userid)
        {
            MdlMember = new Member();
            MdlMember.objLicense = MdlMember.LicensedetailsByUserId(Userid);
            ViewBag.state = StateScript("", MdlMember.objLicense.LicenseState);
            return PartialView("EditLicenseDetails", MdlMember.objLicense);
        }

        [HttpPost]
        public PartialViewResult NewEditLicenseDetails(int id, int Userid)
        {
            MdlMember = new Member();
            MdlMember.objLicense = MdlMember.NewGetLicensedetailsByUserId(Userid, SessionManagement.TimeZoneSystemName);
            ViewBag.state = StateScript("", MdlMember.objLicense.LicenseState);
            return PartialView("_PartialEditLicenseDetailPopup", MdlMember.objLicense);
        }


        [HttpPost]
        public JsonResult ProfessionalMembershipsInsertAndUpdate(int Id, int UserId, string Membership)
        {
            bool Result = false;
            object obj = "";
            MdlMember = new Member();
            Result = MdlMember.ProfessionalMembershipsInsertAndUpdate(Id, UserId, Membership);
            if (Result)
            {
                obj = "1";
            }
            else
            {
                obj = "0";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RemoveProfessionalMemberships(int Id)
        {
            bool Result = false;
            object obj = "";
            MdlMember = new Member();
            Result = MdlMember.RemoveProfessionalMemberships(Id);
            if (Result)
            {
                obj = "1";
            }
            else
            {
                obj = "0";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemovePrimaryWebsite(int UserId)
        {
            bool Result = false;
            object obj = "";
            objColleaguesData = new clsColleaguesData();
            Result = objColleaguesData.RemovePrimaryWebSite(UserId);
            if (Result)
            {
                obj = "1";
            }
            else
            {
                obj = "0";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemoveSecondaryWebsite(int SecondaryId, int UserId)
        {
            bool Result = false;
            object obj = "";
            objColleaguesData = new clsColleaguesData();
            Result = objColleaguesData.RemoveSecondaryWebSite(SecondaryId, UserId);
            if (Result)
            {
                obj = "1";
            }
            else
            {
                obj = "0";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        //RemovePrimaryAndSecondaryWebsite
        public JsonResult RemovePrimaryAndSecondaryWebsite(int SiteId, int UserId)
        {
            object obj = "";
            bool Result = false;
            objColleaguesData = new clsColleaguesData();
            if (SiteId == 0)
            {
                Result = objColleaguesData.RemovePrimaryWebSite(UserId);
                if (Result)
                {
                    obj = "1";
                }
                else
                {
                    obj = "0";
                }
            }
            else
            {
                Result = objColleaguesData.RemoveSecondaryWebSite(SiteId, UserId);
                if (Result)
                {
                    obj = "1";
                }
                else
                {
                    obj = "0";
                }
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string SearchProfessionalMemberships(string Memberships)
        {
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            StringBuilder StrResult = new StringBuilder();
            dt = objCommon.SearchProMembership(Memberships);
            if (dt.Rows.Count > 0)
            {
                sb.Append("[");
                foreach (DataRow item in dt.Rows)
                {
                    sb.Append("{'label':\"" + objCommon.CheckNull(Convert.ToString(item["Membership"]), string.Empty) + "\",'value':\"" + objCommon.CheckNull(Convert.ToString(item["Membership"]), string.Empty) + "\"},");
                }
                StrResult.Append(sb.ToString().Substring(0, sb.ToString().Length - 1));
                StrResult.Append("]");

                // sb.Append("<select id=\"select4\" name=\"select4\" style=\"width:100%\">");
                // sb.Append("<option></option>");
                // foreach (DataRow item in dt.Rows)
                // {
                //    
                //         sb.Append("<option value=" + objCommon.CheckNull(Convert.ToString(item["Membership"]), string.Empty) + " class=\"selected\" selected=\"selected\">" + objCommon.CheckNull(Convert.ToString(item["Membership"]), string.Empty) + "</option>");
                //   
                // }
                //
                // sb.Append("</select>");
            }

            return StrResult.ToString();
        }

        #endregion

        #region Code For Edit Personal Information
        [HttpPost]
        public PartialViewResult GetDoctorDetialsById(int UserId)
        {
            MdlMember = new Member();
            DataSet ds = new DataSet();
            ds = MdlMember.GetProfileDetailsOfDoctorByID(UserId);
            if (ds != null && ds.Tables.Count > 0)
            {
                MdlMember.UserId = Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]);
                MdlMember.FirstName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]), string.Empty);
                MdlMember.MiddleName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["MiddleName"]), string.Empty);
                MdlMember.LastName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LastName"]), string.Empty);
                MdlMember.Title = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Title"]), string.Empty);
                MdlMember.Salutation = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Salutation"]), string.Empty);
                MdlMember.lstSpeacilitiesOfDoctor = MdlMember.GetSpeacilitiesOfDoctorById(MdlMember.UserId);

                string concat = String.Join(",", MdlMember.lstSpeacilitiesOfDoctor.Select(t => t.SpecialtyId).ToArray());


                ViewBag.Specialities = SpecialitiesCheckBoxScript(concat);

                if (ds.Tables[1].Rows.Count > 0)
                {
                    MdlMember.OfficeName = objCommon.CheckNull(Convert.ToString(ds.Tables[1].Rows[0]["AccountName"]), string.Empty);
                }
            }
            return PartialView("PartialEditPersonalInformation", MdlMember);
        }

        public PartialViewResult NewGetDoctorDetialsById(int UserId)
        {
            MdlMember = new Member();
            DataSet ds = new DataSet();
            ds = MdlMember.GetProfileDetailsOfDoctorByID(UserId);
            if (ds != null && ds.Tables.Count > 0)
            {
                MdlMember.UserId = Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]);
                MdlMember.FirstName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]), string.Empty);
                MdlMember.MiddleName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["MiddleName"]), string.Empty);
                MdlMember.LastName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LastName"]), string.Empty);
                MdlMember.Title = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Title"]), string.Empty);
                MdlMember.Salutation = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Salutation"]), string.Empty);
                MdlMember.lstSpeacilitiesOfDoctor = MdlMember.GetSpeacilitiesOfDoctorById(MdlMember.UserId);
                MdlMember.specialtyIds = String.Join(",", MdlMember.lstSpeacilitiesOfDoctor.Select(t => t.SpecialtyId).ToArray());
                ViewBag.Specialities = NewSpecialitiesScript(MdlMember.specialtyIds);
                if (ds.Tables[1].Rows.Count > 0)
                {
                    MdlMember.OfficeName = objCommon.CheckNull(Convert.ToString(ds.Tables[1].Rows[0]["AccountName"]), string.Empty);
                }
            }
            return PartialView("_PartialEditProfilePopUp", MdlMember);
        }

        public JsonResult UpdateDoctorPersonalInformation(int UserId, string FirstName, string MiddleName, string LastName, string OfficeName, string Title, string Specialities, string Salutation)
        {
            bool Result = false;
            object obj = "";
            Result = objColleaguesData.UpdateDoctorPersonalInformationFromProfile(UserId, FirstName, MiddleName, LastName, OfficeName, Title, Specialities, Salutation);
            if (Result)
            {
                obj = "1";
                Session["FirstName"] = FirstName;
            }
            else
            {
                obj = "0";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public string SpecialitiesCheckBoxScript(string SpecialitiId)
        {
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.DoctorSpecialities();


            string[] arrSpecialitiId = SpecialitiId.Split(',');

            sb.Append("<ul class=\"check_listing\" id=\"chklstcheck_listing\">");
            foreach (var item in lst)
            {
                if (item.Value == "0")
                {

                }
                else if (SpecialitiId != "" && arrSpecialitiId.Contains(item.Value))
                {
                    sb.Append("<li><label><input class=\"checkallbox\" name=\"chkSpecialities\" checked=\"checked\" type=\"checkbox\" value=\"" + item.Value + "\" name=\"\">" + item.Text + "</label></li>");
                }
                else
                {
                    sb.Append("<li><label><input class=\"checkallbox\" name=\"chkSpecialities\" type=\"checkbox\" value=\"" + item.Value + "\" name=\"\">" + item.Text + "</label></li>");
                }
            }
            sb.Append("</ul>");

            return sb.ToString();
        }

        public List<SelectListItem> NewSpecialitiesScript(string selected = "")
        {
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.NewDoctorSpecialities(selected);
            return lst;
        }



        #endregion

        #region Code For Team Member
        [HttpPost]
        public PartialViewResult GetTeamMemberDetailsById(int UserId, int TeamMemberId)
        {
            MdlMember = new Member();
            int ParentUserId = 0;
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["ParentUserId"]))) { ParentUserId = Convert.ToInt32(Session["ParentUserId"]); }
                else { ParentUserId = Convert.ToInt32(Session["UserId"]); }

            }
            else
            {
                ParentUserId = UserId;
            }
            MdlMember.LocationList = MdlMember.GetLocationDetailsByDoctorId(ParentUserId);
            if (TeamMemberId != 0)
            {
                MdlMember.UserId = UserId;
                DataSet ds = new DataSet();
                ds = MdlMember.GetProfileDetailsOfDoctorByID(TeamMemberId);
                if (ds != null && ds.Tables.Count > 0)
                {
                    MdlMember.TeamMemberUserId = Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]);
                    MdlMember.FirstName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]), string.Empty);
                    MdlMember.LastName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LastName"]), string.Empty);
                    MdlMember.Email = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Username"]), string.Empty);
                    MdlMember.LocationId = Convert.ToInt32(ds.Tables[0].Rows[0]["LocationId"]);
                    MdlMember.DentrixProviderId = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["DentrixProviderId"]), string.Empty);
                    MdlMember.provtype = (Convert.ToInt32(ds.Tables[0].Rows[0]["provtype"])== 2 ? true : false);
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        MdlMember.OfficeName = objCommon.CheckNull(Convert.ToString(ds.Tables[1].Rows[0]["AccountName"]), string.Empty);
                    }
                    MdlMember.lstDoctorAddressDetails = MdlMember.GetDoctorAddressDetailsById(TeamMemberId);
                    MdlMember.lstSpeacilitiesOfDoctor = MdlMember.GetSpeacilitiesOfDoctorById(TeamMemberId);

                    //ViewBag.country = CountryScript(objCommon.CheckNull(MdlMember.lstDoctorAddressDetails[0].Country, string.Empty));
                    //ViewBag.state = StateScript(objCommon.CheckNull(MdlMember.lstDoctorAddressDetails[0].Country, string.Empty), objCommon.CheckNull(MdlMember.lstDoctorAddressDetails[0].State, string.Empty));

                    if (MdlMember.lstSpeacilitiesOfDoctor != null && MdlMember.lstSpeacilitiesOfDoctor.Count > 0)
                        ViewBag.Specialities = SpecialitiesScript(MdlMember.lstSpeacilitiesOfDoctor[0].SpecialtyId.ToString());
                    else
                        ViewBag.Specialities = SpecialitiesScript("0");
                }



            }
            else
            {
                MdlMember.UserId = UserId;
                MdlMember.TeamMemberUserId = TeamMemberId;
                ViewBag.country = CountryScriptForAddTeamMember("");
                ViewBag.state = StateScriptForAddTeamMember("", "");
                ViewBag.Specialities = SpecialitiesScript("");
            }
            return PartialView("PartialEditTeamMember", MdlMember);
        }

        [HttpPost]//New Design Method
        public PartialViewResult NewGetTeamMemberDetailsById(int UserId, int TeamMemberId)
        {
            MdlMember = new Member();
            int ParentUserId = 0;
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["ParentUserId"]))) { ParentUserId = Convert.ToInt32(Session["ParentUserId"]); }
                else { ParentUserId = Convert.ToInt32(Session["UserId"]); }

            }
            else
            {
                ParentUserId = UserId;
            }
            MdlMember.LocationList = MdlMember.GetLocationDetailsByDoctorId(ParentUserId);
            if (TeamMemberId != 0)
            {
                MdlMember.UserId = UserId;
                DataSet ds = new DataSet();
                ds = MdlMember.GetProfileDetailsOfDoctorByID(TeamMemberId);
                if (ds != null && ds.Tables.Count > 0)
                {
                    MdlMember.TeamMemberUserId = Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]);
                    MdlMember.FirstName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]), string.Empty);
                    MdlMember.LastName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LastName"]), string.Empty);
                    MdlMember.Email = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Username"]), string.Empty);
                    MdlMember.LocationId = Convert.ToInt32(ds.Tables[0].Rows[0]["LocationId"]);
                    MdlMember.DentrixProviderId = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["DentrixProviderId"]), string.Empty);
                    MdlMember.provtype = Convert.ToInt32(ds.Tables[0].Rows[0]["provtype"]) == 2 ? true : false;
                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        MdlMember.OfficeName = objCommon.CheckNull(Convert.ToString(ds.Tables[1].Rows[0]["AccountName"]), string.Empty);
                    }
                    MdlMember.lstDoctorAddressDetails = MdlMember.GetDoctorAddressDetailsById(TeamMemberId);
                    MdlMember.lstSpeacilitiesOfDoctor = MdlMember.GetSpeacilitiesOfDoctorById(TeamMemberId);

                    //ViewBag.country = CountryScript(objCommon.CheckNull(MdlMember.lstDoctorAddressDetails[0].Country, string.Empty));
                    //ViewBag.state = StateScript(objCommon.CheckNull(MdlMember.lstDoctorAddressDetails[0].Country, string.Empty), objCommon.CheckNull(MdlMember.lstDoctorAddressDetails[0].State, string.Empty));

                    if (MdlMember.lstSpeacilitiesOfDoctor != null && MdlMember.lstSpeacilitiesOfDoctor.Count > 0)
                        ViewBag.Specialities = NewSpecialitiesScript(MdlMember.lstSpeacilitiesOfDoctor[0].SpecialtyId.ToString());
                    else
                        ViewBag.Specialities = NewSpecialitiesScript("0");
                }



            }
            else
            {
                MdlMember.UserId = UserId;
                MdlMember.TeamMemberUserId = TeamMemberId;
                ViewBag.country = CountryScriptForAddTeamMember("");
                ViewBag.state = StateScriptForAddTeamMember("", "");
                ViewBag.Specialities = NewSpecialitiesScript("");
            }
            return PartialView("_PartialAddEditTeamMemberPopUp", MdlMember);
        }

        [HttpPost]
        public JsonResult TeamMemberInsertAndUpdate(int UserId, int ParentUserId, string Email, string FirstName, string LastName, string OfficeName, string ExactAddress, string Address2, string City, string State, string Country, string ZipCode, string Specialty, int LocationId, string ProviderId,bool provtype)
        {
            bool Result = false;
            DataTable dtCheckEmail = new DataTable();
            object obj = "";
            MdlMember = new Member();
            ObjTemplate = new clsTemplate();
            objclsAppointmentData = new clsAppointmentData();
            var doctorDetails = objclsAppointmentData.GetDoctorDetails(ParentUserId);
            int ParentId = 0;
            if (doctorDetails.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(doctorDetails.Rows[0]["ParentUserId"])))
                {
                    ParentId = Convert.ToInt32(doctorDetails.Rows[0]["ParentUserId"]);
                    if (ParentId == 0)
                    {
                        ParentId = ParentUserId;
                    }
                }

            }

            Result = MdlMember.TeamMemberInsertAndUpdate(UserId, ParentId, Email, FirstName, LastName, OfficeName, ExactAddress, Address2, City, State, Country, ZipCode, Specialty, LocationId, ProviderId, provtype);
            if (Result)
            {
                obj = "1";
                if (UserId == 0)
                {
                    DataTable dtCheck = objColleaguesData.CheckEmailInSystem(Email);
                    if (dtCheck.Rows.Count > 0)
                    {
                        string companywebsite = "";
                        if (Session["CompanyWebsite"] != null)
                        {
                            companywebsite = Session["CompanyWebsite"].ToString();
                        }
                        string EncPassword = objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["Password"]), string.Empty);
                        ObjTemplate.NewUserEmailFormat(Email, companywebsite + "/User/Index?UserName=" + Email + "&Password=" + ObjTripleDESCryptoHelper.decryptText(EncPassword), ObjTripleDESCryptoHelper.decryptText(EncPassword), companywebsite);
                        ObjTemplate.TemplateForTeamMember(Convert.ToInt32(Session["UserId"]), FirstName + " " + LastName, Email, ObjTripleDESCryptoHelper.decryptText(EncPassword), "OldDoctor", provtype);
                    }
                    else
                    {
                        ObjTemplate.TemplateForTeamMember(Convert.ToInt32(Session["UserId"]), FirstName + " " + LastName, Email, "", "OldDoctor", provtype);
                    }


                }


            }
            else
            {
                obj = "0";
            }



            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public string CountryScriptForAddTeamMember(string countryId)
        {
            if (countryId == "")
            {
                countryId = "US";
            }
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.Country();
            sb.Append("<select  class=\"form-control\" id=\"drpAddCountry\" name=\"drpAddCountry\" onchange=\"FillStateForAdd();\" tabindex=\"9\">");
            foreach (var item in lst)
            {
                if (countryId == item.Value)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
                else
                {
                    sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
            }
            sb.Append("</select>");
            return sb.ToString();
        }

        public string StateScriptForAddTeamMember(string countryId, string stateId)
        {
            if (countryId == "")
            {
                countryId = "US";
            }
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.StateByCountryCode(countryId);
            sb.Append("<select class=\"form-control\" id=\"drpAddState\" name=\"drpAddState\" tabindex=\"7\">");
            foreach (var item in lst)
            {
                if (stateId == item.Value)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
                else
                {
                    sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
            }
            sb.Append("</select>");
            return sb.ToString();
        }


        public JsonResult FillStateForAdd(string cid, string sid)
        {
            return Json(StateScriptForAddTeamMember(cid, sid), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult RemoveTeamMemberOfDoctor(int UserId, int ParentUserId)
        {
            bool Result = false;
            object obj = "";
            MdlMember = new Member();
            Result = MdlMember.RemoveTeamMemberOfDoctor(UserId, ParentUserId);
            if (Result)
            {
                obj = "1";
            }
            else
            {
                obj = "0";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult CheckEmailExistsAsDoctor(string Email)
        {
            object obj = string.Empty;
            DataTable dtEmailChek = objColleaguesData.CheckEmailExistsAsDoctor(Email);
            if (dtEmailChek == null || dtEmailChek.Rows.Count == 0)
            {
                obj = "1";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Code for Public Profile

        [HttpPost]
        public JsonResult GetPublicProfileDetailByUserId(string UserId)
        {
            return Json(PublicProfileDetailByUserId(Convert.ToInt32(UserId)), JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult NewGetPublicProfileDetailByUserId(string UserId)
        {
            DataSet ds = new DataSet();
            BO.ViewModel.PublicProfileDetail profileobj = new BO.ViewModel.PublicProfileDetail();
            ds = objColleaguesData.GetDoctorDetailsById(Convert.ToInt32(UserId));
            if (ds.Tables[0].Rows.Count > 0)
            {
                profileobj.PublicProfileUrl = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["PublicProfileUrl"]), string.Empty);
                profileobj.UserId = Convert.ToInt32(UserId);
            }
            return View("_PartialEditPublicProfilePopUp", profileobj);
        }


        [HttpPost]
        public JsonResult GetWebsiteURLByUserId(string UserId)
        {
            return Json(WebsiteURLByUserId(Convert.ToInt32(UserId)), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult NewGetWebsiteURLByUserId(string UserId)
        {
            BO.ViewModel.WebSiteUrl wurl = new BO.ViewModel.WebSiteUrl();
            wurl.UserId = Convert.ToInt32(UserId);
            return View("_PartialInsertWebsitePopUp", wurl);
        }



        public string PublicProfileDetailByUserId(int UserId)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataSet ds = new DataSet();
                ds = objColleaguesData.GetDoctorDetailsById(UserId);
                string Pubulicprofile = string.Empty;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Pubulicprofile = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["PublicProfileUrl"]), string.Empty);
                }

                sb.Append("<h2 class=\"title\"> Edit Public Profile URL </h2>");
                sb.Append("<div class=\"cntnt\">");
                sb.Append("<ul class=\"tablelist\">");
                sb.Append(" <li>");
                sb.Append("<input type=\"text\" id=\"txtpublicprofileurl\" name=\"Search\" value=\"" + Pubulicprofile + "\">");

                sb.Append(" </li>");
                sb.Append("</ul>");
                sb.Append("<div class=\"actions\">");
                sb.Append("<input class=\"save_btn\" name=\"Save\" type=\"button\" value=\"Save\" onclick=\"CheckPublicProfileUrlIsExistsAndUpdate('" + UserId + "')\">");
                sb.Append("<input class=\"cancel_btn\" name=\"Cancel\" type=\"button\" value=\"Cancel\" onclick=\"ClosePopUp()\">");
                sb.Append("<div class=\"clear\"></div>");
                sb.Append("</div>");
                sb.Append("</div>");
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        public string WebsiteURLByUserId(int UserId)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataSet ds = new DataSet();
                //ds = objColleaguesData.GetDoctorDetailsById(UserId);
                //string WebsiteURL = string.Empty;
                //if (ds.Tables[0].Rows.Count > 0)
                //{
                //    WebsiteURL = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["WebsiteURL"]), string.Empty);
                //}

                sb.Append("<h2 class=\"title\"> Insert Website</h2>");
                sb.Append("<div class=\"cntnt\">");
                sb.Append("<ul class=\"tablelist\">");
                sb.Append(" <li>");
                sb.Append("<input type=\"text\" id=\"txtwebsiteurl\" name=\"Search\" value=\"\">");

                sb.Append(" </li>");
                sb.Append("</ul>");
                sb.Append("<div class=\"actions\">");
                sb.Append("<input class=\"save_btn\" name=\"Save\" type=\"button\" value=\"Save\" onclick=\"return WebsiteUrlUpdate('" + UserId + "');\">");
                sb.Append("<input class=\"cancel_btn\" name=\"Cancel\" type=\"button\" value=\"Cancel\" onclick=\"ClosePopUp()\">");
                sb.Append("<div class=\"clear\"></div>");
                sb.Append("</div>");
                sb.Append("</div>");
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }



        public JsonResult GetPrimaryWebsiteURLByUserId(string UserId)
        {
            return Json(PrimaryWebsiteURLByUserId(Convert.ToInt32(UserId)), JsonRequestBehavior.AllowGet);
        }
        public string PrimaryWebsiteURLByUserId(int UserId)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataSet ds = new DataSet();
                ds = objColleaguesData.GetDoctorDetailsById(UserId);
                string WebsiteURL = string.Empty;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    WebsiteURL = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["WebsiteURL"]), string.Empty);
                }

                sb.Append("<h2 class=\"title\"> Edit Primary Website URL </h2>");
                sb.Append("<div class=\"cntnt\">");
                sb.Append("<ul class=\"tablelist\">");
                sb.Append(" <li>");
                sb.Append("<input type=\"text\" id=\"txtwebsiteurl\" name=\"Search\" value=\"" + WebsiteURL + "\">");

                sb.Append(" </li>");
                sb.Append("</ul>");
                sb.Append("<div class=\"actions\">");
                sb.Append("<input class=\"save_btn\" name=\"Save\" type=\"button\" value=\"Save\" onclick=\"return PrimaryWebsiteUrlUpdate('" + UserId + "');\"><input class=\"remove_btn\" type=\"button\" onclick=\"RemovePrimaryWebsite('" + UserId + "')\" name=\"Save\" value=\"Save\">");
                sb.Append("<input class=\"cancel_btn\" name=\"Cancel\" type=\"button\" value=\"Cancel\" onclick=\"ClosePopUp()\">");
                sb.Append("<div class=\"clear\"></div>");
                sb.Append("</div>");
                sb.Append("</div>");
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }
        public JsonResult GetSecondaryWebsiteURLByUserId(string WebsiteId, string UserId)
        {
            return Json(SecondaryWebsiteURLByUserId(Convert.ToInt32(WebsiteId), Convert.ToInt32(UserId)), JsonRequestBehavior.AllowGet);
        }
        public string SecondaryWebsiteURLByUserId(int WebsiteId, int UserId)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dts = new DataTable();
                string qry = "select * from SecondaryDetails where UserId = '" + objCommon.CheckNull(Convert.ToString(UserId), string.Empty) + "' and SecondaryId = '" + objCommon.CheckNull(Convert.ToString(WebsiteId), string.Empty) + "'";
                dts = objCommon.DataTable(qry);
                string WebsiteURL = string.Empty;
                int SecondaryWebSiteId = 0;
                if (dts.Rows.Count > 0)
                {
                    WebsiteURL = objCommon.CheckNull(Convert.ToString(dts.Rows[0]["WebSite"]), string.Empty);
                    SecondaryWebSiteId = Convert.ToInt32(dts.Rows[0]["SecondaryId"]);
                }

                sb.Append("<h2 class=\"title\"> Edit Secondary Website URL </h2>");
                sb.Append("<div class=\"cntnt\">");
                sb.Append("<ul class=\"tablelist\">");
                sb.Append(" <li>");
                sb.Append("<input type=\"text\" id=\"txtwebsiteurl\" name=\"Search\" value=\"" + WebsiteURL + "\">");

                sb.Append(" </li>");
                sb.Append("</ul>");
                sb.Append("<div class=\"actions\">");
                sb.Append("<input class=\"save_btn\" name=\"Save\" type=\"button\" value=\"Save\" onclick=\"return SecondaryWebsiteUrlUpdate('" + SecondaryWebSiteId + "','" + UserId + "');\"><input class=\"remove_btn\" type=\"button\" onclick=\"RemoveSecondaryWebsite('" + SecondaryWebSiteId + "','" + UserId + "')\" name=\"Save\" value=\"Save\">");
                sb.Append("<input class=\"cancel_btn\" name=\"Cancel\" type=\"button\" value=\"Cancel\" onclick=\"ClosePopUp()\">");
                sb.Append("<div class=\"clear\"></div>");
                sb.Append("</div>");
                sb.Append("</div>");
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }
        [HttpPost]


        public ActionResult GetPrimarySecondryWebsiteURLByUserId(int websiteId, int UserId)
        {
            BO.ViewModel.WebSiteUrl wobj = new BO.ViewModel.WebSiteUrl();
            if (websiteId == 0)
            {
                DataSet ds = new DataSet();
                ds = objColleaguesData.GetDoctorDetailsById(UserId);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    wobj.WebsiteUrl = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["WebsiteURL"]), string.Empty);
                    wobj.WebsiteId = null;
                    wobj.UserId = UserId;
                }
            }
            else
            {
                DataTable dts = new DataTable();
                string qry = "select * from SecondaryDetails where UserId = '" + objCommon.CheckNull(Convert.ToString(UserId), string.Empty) + "' and SecondaryId = '" + objCommon.CheckNull(Convert.ToString(websiteId), string.Empty) + "'";
                dts = objCommon.DataTable(qry);
                if (dts.Rows.Count > 0)
                {
                    wobj.WebsiteUrl = objCommon.CheckNull(Convert.ToString(dts.Rows[0]["WebSite"]), string.Empty);
                    wobj.WebsiteId = Convert.ToInt32(dts.Rows[0]["SecondaryId"]);
                    wobj.UserId = UserId;
                }

            }

            return View("_PartialEditPrimarySecondaryWebsite", wobj);
        }

        public JsonResult CheckPublicProfileUrlIsExistsAndUpdate(string UserId, string PublicProfile)
        {
            object obj = string.Empty;
            try
            {
                DataTable dt = new DataTable();
                dt = objColleaguesData.CheckPublicProfileUrlIsExists(Convert.ToInt32(UserId), PublicProfile);
                if (dt.Rows.Count > 0)
                {
                    obj = "0";//Is Exists in system
                }
                else
                {
                    DataSet ds = new DataSet();
                    ds = objColleaguesData.GetDoctorDetailsById(Convert.ToInt32(UserId));
                    string PubulicprofileOld = string.Empty;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        PubulicprofileOld = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["PublicProfileUrl"]), string.Empty);
                    }

                    if (PubulicprofileOld != PublicProfile)
                    {
                        bool result = objColleaguesData.UpdateDoctorPublicProfileUrlById(Convert.ToInt32(UserId), PublicProfile);
                        if (result)
                        {
                            obj = "1";
                        }
                    }
                    else
                    {
                        obj = "2";
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }


        public bool UpdateWebsite(string UserId, string Websiteurl, int? LocationId = null)
        {
            var IsUpdateWebsite = false;
            try
            {
                bool result = objColleaguesData.UpdateDoctorWebsiteUrlById(Convert.ToInt32(UserId), Websiteurl,0, LocationId);
                if (result)
                {
                    IsUpdateWebsite = true;
                }

            }
            catch (Exception)
            {
                throw;
            }
            return IsUpdateWebsite;
        }
        [HttpPost]
        public JsonResult WebsiteUrlUpdate(string UserId, string Websiteurl)
        {
            object obj = string.Empty;
            var userId = Convert.ToInt32(Session["UserId"]);
            var strWeb = UpdateWebsite(UserId, Websiteurl);
            if (strWeb)
            {
                obj = "1";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }



        #endregion

        #region Code for Banner

        public ActionResult AddBanner(int UserId = 0)
        {
            Banner model = new Banner();
            if (!string.IsNullOrEmpty(SessionManagement.AdminUserId) && UserId != 0)
                model.UserId = UserId;
            model.BannerId = 0;
            return PartialView("PartialEditBanner", model);
        }

        public ActionResult NewAddBanner(int UserId = 0)
        {
            Banner model = new Banner();
            if (!string.IsNullOrEmpty(SessionManagement.AdminUserId) && UserId != 0)
                model.UserId = UserId;
            model.BannerId = 0;
            return PartialView("_PartialAddBannerPopUp", model);
        }


        public ActionResult InsertUpdateBanner(Banner model, HttpPostedFileBase File)
        {

            bool Result = false;
            object obj = "";
            string path = "", filename = "";
            string UserId = Convert.ToString(Session["UserId"]);
            if (!string.IsNullOrEmpty(SessionManagement.AdminUserId))
                UserId = Convert.ToString(model.UserId);
            if (File != null && File.ContentLength > 0)
            {

                string savepath = Server.MapPath(ConfigurationManager.AppSettings["BannerImage"] + UserId + "/");
                if (!Directory.Exists(savepath))
                {
                    Directory.CreateDirectory(savepath);
                }
                filename = File.FileName;
                if (File.FileName.Contains("\\"))
                {
                    string[] testfiles = File.FileName.Split(new char[] { '\\' });
                    filename = testfiles[testfiles.Length - 1];
                }
                else if (File.FileName.Contains("/"))
                {
                    string[] testfiles = File.FileName.Split(new char[] { '/' });
                    filename = testfiles[testfiles.Length - 1];
                }
                else
                {
                    filename = File.FileName;
                }
                filename = "$" + System.DateTime.Now.Ticks + "$" + filename;

                path = Path.Combine(savepath, Path.GetFileName(filename));
                File.SaveAs(path);
                if (model.BannerId != 0)
                {
                    String Removepath = Server.MapPath(ConfigurationManager.AppSettings["BannerImage"] + UserId + "/" + model.Path);
                    if (System.IO.File.Exists(Removepath))
                    {
                        System.IO.File.Delete(Removepath);
                    }
                }
            }
            else
            {
                filename = model.Path;
            }
            model.Position = 1;
            Result = objColleaguesData.InsertUpdateBanner(model.BannerId, Convert.ToInt32(UserId), model.Title,
                               filename, model.ColorCode, model.Position);

            if (model.BannerId == 0)
            {
                TempData["errorMessage"] = "Banner inserted successfully.";
            }
            else
            {
                TempData["errorMessage"] = "Banner updated successfully.";
            }
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId))
                return RedirectToAction("Index");
            else
                return RedirectToAction("ViewProfile", "Admin", new { UserId = UserId });
        }

        //New Method
        public ActionResult NewInsertUpdateBanner(Banner model, HttpPostedFileBase File)
        {

            bool Result = false;
            object obj = "";
            string path = "", filename = "";
            string UserId = Convert.ToString(Session["UserId"]);
            if (!string.IsNullOrEmpty(SessionManagement.AdminUserId))
                UserId = Convert.ToString(model.UserId);
            if (File != null && File.ContentLength > 0)
            {

                string savepath = Server.MapPath(ConfigurationManager.AppSettings["BannerImage"] + UserId + "/");
                if (!Directory.Exists(savepath))
                {
                    Directory.CreateDirectory(savepath);
                }
                filename = File.FileName;
                if (File.FileName.Contains("\\"))
                {
                    string[] testfiles = File.FileName.Split(new char[] { '\\' });
                    filename = testfiles[testfiles.Length - 1];
                }
                else if (File.FileName.Contains("/"))
                {
                    string[] testfiles = File.FileName.Split(new char[] { '/' });
                    filename = testfiles[testfiles.Length - 1];
                }
                else
                {
                    filename = File.FileName;
                }
                filename = "$" + System.DateTime.Now.Ticks + "$" + filename;

                path = Path.Combine(savepath, Path.GetFileName(filename));
                File.SaveAs(path);
                if (model.BannerId != 0)
                {
                    String Removepath = Server.MapPath(ConfigurationManager.AppSettings["BannerImage"] + UserId + "/" + model.Path);
                    if (System.IO.File.Exists(Removepath))
                    {
                        System.IO.File.Delete(Removepath);
                    }
                }
            }
            else
            {
                filename = model.Path;
            }
            model.Position = 1;
            Result = objColleaguesData.InsertUpdateBanner(model.BannerId, Convert.ToInt32(UserId), model.Title,
                               filename, model.ColorCode, model.Position);

            if (model.BannerId == 0)
            {
                TempData["errorMessage"] = "Banner inserted successfully.";
            }
            else
            {
                TempData["errorMessage"] = "Banner updated successfully.";
            }
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId))
                return RedirectToAction("Index");
            else
                return RedirectToAction("ViewProfile", "Admin", new { UserId = UserId });
        }

        [HttpPost]
        public JsonResult RemoveBanner(int BannerId, int UserId = 0)
        {
            bool Result = true;
            object obj = "";
            Banner banner = new Banner();
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || UserId == 0)
                UserId = Convert.ToInt32(SessionManagement.UserId);
            MdlMember = new Member();
            banner = MdlMember.GetBannerDetailById(BannerId);
            Result = objColleaguesData.RemoveBanner(BannerId);
            String path = Server.MapPath(ConfigurationManager.AppSettings["BannerImage"] + UserId + "/" + banner.Path);
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
            }
            //TempData["errorMessage"] = "Banner removed successfully";
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditBanner(int BannerId)
        {
            Banner banner = new Banner();
            MdlMember = new Member();
            banner = MdlMember.GetBannerDetailById(BannerId);
            return PartialView("PartialEditBanner", banner);
        }

        public ActionResult NewEditBanner(int BannerId)
        {
            Banner banner = new Banner();
            MdlMember = new Member();
            banner = MdlMember.GetBannerDetailById(BannerId);
            return PartialView("_PartialAddBannerPopUp", banner);
        }


        #endregion

        #region Insurance_Member

        public ActionResult GetInsuranceDetail(int UserId = 0)
        {
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || UserId == 0)
                UserId = Convert.ToInt32(SessionManagement.UserId);

            Insurance model = new Insurance();
            objProfile = new ProfileBLL();
            model.lstInsurance = objProfile.GetInsuranceDetail();
            List<Insurance> UserData = new List<Insurance>();
            UserData = objProfile.GetMemberInsuranceByUserId(UserId);
            foreach (var item in UserData)
            {
                //var InsuranceId = model.lstInsurance.Where(t => t.InsuranceId == item.InsuranceId).FirstOrDefault().InsuranceId;
                model.lstInsurance.Where(t => t.InsuranceId == item.InsuranceId).FirstOrDefault().Ischeck = true;
            }
            model.UserId = UserId;
            return PartialView("PartialInsuranceMember", model);
        }

        public ActionResult NewGetInsuranceDetail(int UserId = 0)
        {
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || UserId == 0)
                UserId = Convert.ToInt32(SessionManagement.UserId);

            Insurance model = new Insurance();
            objProfile = new ProfileBLL();
            model.lstInsurance = objProfile.GetInsuranceDetail();
            List<Insurance> UserData = new List<Insurance>();
            UserData = objProfile.GetMemberInsuranceByUserId(UserId);
            foreach (var item in UserData)
            {
                //var InsuranceId = model.lstInsurance.Where(t => t.InsuranceId == item.InsuranceId).FirstOrDefault().InsuranceId;
                model.lstInsurance.Where(t => t.InsuranceId == item.InsuranceId).FirstOrDefault().Ischeck = true;
            }
            model.UserId = UserId;
            return PartialView("_PartialAddEditInsuranceDetailPopUp", model);
        }

        public ActionResult InsertInsuranceDetail(Insurance insurance)
        {
            objProfile = new ProfileBLL();
            bool result;

            if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || insurance.UserId == 0)
                insurance.UserId = Convert.ToInt32(SessionManagement.UserId);

            result = objProfile.RemoveInsuranceMember(insurance.UserId);
            foreach (var item in insurance.lstInsurance)
            {
                if (item.Ischeck == true)
                {
                    item.Member_InsuranceId = 0;
                    item.UserId = insurance.UserId;
                    result = objProfile.InsertInsuranceMember(item);
                }
            }
            //TempData["ErrorMessage"] = "Insurance detail added successfully";
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId))
                return RedirectToAction("Index");
            else
                return RedirectToAction("ViewProfile", "Admin", new { UserId = insurance.UserId });
        }
        //public ActionResult InsertProcedureDetails(List<Procedure> Obj)
        //{

        //}
        public JsonResult RemoveInsuranceMember(int Id)
        {
            objProfile = new ProfileBLL();
            bool result = false;
            int UserId = Convert.ToInt32(SessionManagement.UserId);
            result = objProfile.RemoveInsuranceMemberById(Id, UserId);
            TempData["ErrorMessage"] = "Insurance removed successfully";
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public JsonResult UpdatePrimarySecondryWesiteUrl(string UserId, string Websiteurl, int siteid)
        {
            object obj = string.Empty;
            if (siteid == 0)
            {
                try
                {
                    objCommon = new clsCommon();
                    DataTable dt = new DataTable();
                    string qry = "Update Member set WebsiteURL='" + objCommon.CheckNull(Websiteurl, string.Empty) + "',LastModifiedDate = '" + DateTime.Now + "',ProfilesLastUpdated = '" + DateTime.Now + "' where userid='" + objCommon.CheckNull(UserId, string.Empty) + "'";
                    dt = objCommon.DataTable(qry);
                    if (dt == null)
                    {
                        obj = "1";
                        return Json(obj, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
            else
            {
                try
                {
                    objCommon = new clsCommon();
                    DataTable dt = new DataTable();
                    string qry = "IF NOT EXISTS(Select * from SecondaryDetails where userid = " + UserId +
                                 " and WebSite = '" + Websiteurl + "' and secondaryid != '" + siteid +
                                 "' ) update SecondaryDetails set WebSite = '" + objCommon.CheckNull(Websiteurl, string.Empty) +
                                 "' where UserId = '" + objCommon.CheckNull(UserId, string.Empty) + "' and SecondaryId = '" +
                                 objCommon.CheckNull(Convert.ToString(siteid), string.Empty) + "'"
                                 + " else (Select * from SecondaryDetails where userid = " + UserId +
                                 " and WebSite = '" + Websiteurl + "' and secondaryid != '" + siteid +
                                 "' ) ";
                    // string qry = "update SecondaryDetails set WebSite = '" + objCommon.CheckNull(Websiteurl, string.Empty) + "' where UserId = '" + objCommon.CheckNull(UserId, string.Empty) + "' and SecondaryId = '" + objCommon.CheckNull(WebsiteId, string.Empty) + "'";
                    dt = objCommon.DataTable(qry);
                    if (dt == null)
                    {
                        obj = "1";
                    }

                }
                catch (Exception)
                {
                    throw;
                }
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdatePrimaryWesiteUrl(string UserId, string Websiteurl)
        {
            object obj = string.Empty;
            try
            {
                objCommon = new clsCommon();
                DataTable dt = new DataTable();
                string qry = "Update Member set WebsiteURL='" + objCommon.CheckNull(Websiteurl, string.Empty) + "',LastModifiedDate = '" + DateTime.Now + "',ProfilesLastUpdated = '" + DateTime.Now + "' where userid='" + objCommon.CheckNull(UserId, string.Empty) + "'";
                dt = objCommon.DataTable(qry);
                if (dt == null)
                {
                    obj = "1";
                }

            }
            catch (Exception)
            {
                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);

        }
        public JsonResult UpdateSecondaryWesiteUrl(string UserId, string Websiteurl, string WebsiteId)
        {
            object obj = string.Empty;
            try
            {
                objCommon = new clsCommon();
                DataTable dt = new DataTable();
                string qry = "IF NOT EXISTS(Select * from SecondaryDetails where userid = " + UserId +
                             " and WebSite = '" + Websiteurl + "' and secondaryid != '" + WebsiteId +
                             "' ) update SecondaryDetails set WebSite = '" + objCommon.CheckNull(Websiteurl, string.Empty) +
                             "' where UserId = '" + objCommon.CheckNull(UserId, string.Empty) + "' and SecondaryId = '" +
                             objCommon.CheckNull(WebsiteId, string.Empty) + "'"
                             + " else (Select * from SecondaryDetails where userid = " + UserId +
                             " and WebSite = '" + Websiteurl + "' and secondaryid != '" + WebsiteId +
                             "' ) ";
                // string qry = "update SecondaryDetails set WebSite = '" + objCommon.CheckNull(Websiteurl, string.Empty) + "' where UserId = '" + objCommon.CheckNull(UserId, string.Empty) + "' and SecondaryId = '" + objCommon.CheckNull(WebsiteId, string.Empty) + "'";
                dt = objCommon.DataTable(qry);
                if (dt == null)
                {
                    obj = "1";
                }

            }
            catch (Exception)
            {
                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);

        }

        public ActionResult UploadProfilePic(HttpPostedFileBase fuImage, String id)
        {
            try
            {
                if (fuImage != null)
                {
                    string filename1 = "$" + System.DateTime.Now.Ticks + "$" + fuImage.FileName;
                    string tempPath = "";

                    tempPath = "~/DentistImages/";
                    fuImage.SaveAs(Server.MapPath(tempPath + filename1));
                    int Bottom = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Bottom"].ToString())));
                    int Right = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Right"].ToString())));
                    int Top = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Top"].ToString())));
                    int Left = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Left"].ToString())));
                    var imagecrop = new WebImage(tempPath + filename1);
                    var height = imagecrop.Height;
                    var width = imagecrop.Width;
                    imagecrop.Crop((int)Top, (int)Left, (int)(height - Bottom), (int)(width - Right));
                    imagecrop.Save(tempPath + filename1);
                    int maxPixels = Convert.ToInt32(ConfigurationManager.AppSettings["Thumbimagediamention"]);
                    String SavePath = tempPath + "Thumb" + @"/" + filename1;
                    imagecrop.Resize(maxPixels, maxPixels, true, false);
                    imagecrop.Save(Server.MapPath(SavePath));

                    bool result = objColleaguesData.UpdateDoctorProfileImageById(Convert.ToInt32(id), filename1);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return RedirectToAction("Index", "Profile");
        }

        public ActionResult NewUploadProfilePic(HttpPostedFileBase fuImage, String id)
        {
            try
            {
                if (fuImage != null)
                {
                    string filename1 = "$" + System.DateTime.Now.Ticks + "$" + fuImage.FileName;
                    string tempPath = "";

                    tempPath = "~/DentistImages/";
                    fuImage.SaveAs(Server.MapPath(tempPath + filename1));
                    int Bottom = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Bottom"].ToString())));
                    int Right = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Right"].ToString())));
                    int Top = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Top"].ToString())));
                    int Left = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Left"].ToString())));
                    var imagecrop = new WebImage(tempPath + filename1);
                    var height = imagecrop.Height;
                    var width = imagecrop.Width;
                    imagecrop.Crop((int)Top, (int)Left, (int)(height - Bottom), (int)(width - Right));
                    imagecrop.Save(tempPath + filename1);
                    int maxPixels = Convert.ToInt32(ConfigurationManager.AppSettings["Thumbimagediamention"]);
                    String SavePath = tempPath + "Thumb" + @"/" + filename1;
                    imagecrop.Resize(maxPixels, maxPixels, true, false);
                    imagecrop.Save(Server.MapPath(SavePath));

                    bool result = objColleaguesData.UpdateDoctorProfileImageById(Convert.ToInt32(id), filename1);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return RedirectToAction("Index", "Profile");
        }

        public ActionResult RemoveDoctorProfileImage(int DoctorId)
        {
            if (DoctorId > 0)
            {
                objColleaguesData = new clsColleaguesData();
                objColleaguesData.RemoveDoctorProfileImage(DoctorId);
            }
            return RedirectToAction("index");
        }

        public ActionResult AddEditGalleryItem(int GallaryId = 0, int UserId = 0)
        {
            var model = new mdlGallary();

            if (!string.IsNullOrEmpty(SessionManagement.UserId))
            {
                if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || UserId == 0)
                    UserId = Convert.ToInt32(SessionManagement.UserId);
                model.UserId = UserId;
                model.GallaryId = GallaryId;
                string defaultPath = "/Resources/Gallery/" + UserId + "/";
                if (GallaryId != 0)
                {
                    model = mdlGallary.getGallaryById(UserId, GallaryId, defaultPath);
                }
            }

            return PartialView("PartialAddGallary", model);

        }

        public ActionResult NewAddEditGalleryItem(int GallaryId = 0, int UserId = 0)
        {
            var model = new mdlGallary();

            if (!string.IsNullOrEmpty(SessionManagement.UserId))
            {
                if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || UserId == 0)
                    UserId = Convert.ToInt32(SessionManagement.UserId);
                model.UserId = UserId;
                model.GallaryId = GallaryId;
                string defaultPath = "/Resources/Gallery/" + UserId + "/";
                if (GallaryId != 0)
                {
                    model = mdlGallary.getGallaryById(UserId, GallaryId, defaultPath);
                }
            }

            return PartialView("_PartialAddGalleryPopUp", model);

        }

        [HttpPost]
        public ActionResult InsertUpdateGallary(mdlGallary model)
        {
            var objProfileData = new clsProfileData();
            bool Result = false;
            object obj = "";
            string path = "", filename = "";
            int UserId = model.UserId;
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId))
                UserId = Convert.ToInt32(SessionManagement.UserId);

            if (model.File != null && model.File.ContentLength > 0)
            {

                string defaultPath = "/Resources/Gallery/";
                string filePath = ConfigurationManager.AppSettings.Get("GallaryFilePath");
                if (string.IsNullOrEmpty(filePath))
                {
                    filePath = defaultPath;
                }
                // string savepath = filePath;
                filePath = Server.MapPath(filePath + "/" + UserId + "/");
                if (!Directory.Exists(filePath))
                {
                    Directory.CreateDirectory(filePath);
                }
                if (model.File.FileName.Contains("\\"))
                {
                    string[] testfiles = model.File.FileName.Split(new char[] { '\\' });
                    filename = testfiles[testfiles.Length - 1];
                }
                else if (model.File.FileName.Contains("/"))
                {
                    string[] testfiles = model.File.FileName.Split(new char[] { '/' });
                    filename = testfiles[testfiles.Length - 1];
                }
                else
                {
                    filename = model.File.FileName;
                }
                filename = "$" + System.DateTime.Now.Ticks + "$" + filename;
                path = Path.Combine(filePath, Path.GetFileName(filename));
                model.File.SaveAs(path);
                if (model.GallaryId != 0)
                {
                    //String Removepath = Server.MapPath(model.FilePath);
                    //if (System.IO.File.Exists(Removepath))
                    //{
                    //    System.IO.File.Delete(Removepath);
                    //}
                }
            }
            else
            {
                filename = model.FileName;
            }

            int MediaType = (int)mdlGallary.GalleryConstants.IMAGE_URL;
            string MediaURL = filename;
            if (string.IsNullOrEmpty(MediaURL))
            {
                MediaType = (int)mdlGallary.GalleryConstants.VIDEO_URL;
                MediaURL = model.VideoURL;
            }
            Result = objProfileData.InsertUpdateGallery(model.GallaryId, UserId, MediaURL, MediaType);

            if (model.GallaryId == 0)
            {
                TempData["errorMessage"] = "Gallery item inserted successfully";
            }
            else
            {
                TempData["errorMessage"] = "Gallery item updated successfully";
            }

            if (string.IsNullOrEmpty(SessionManagement.AdminUserId))
                return RedirectToAction("Index");
            else
                return RedirectToAction("ViewProfile", "Admin", new { UserId = model.UserId });
        }
        [HttpPost]
        public JsonResult RemoveGallaryItem(int GallaryId, int UserId = 0)
        {
            bool Result = true;
            object obj = "";
            var objProfileData = new clsProfileData();
            //banner = MdlMember.GetBannerDetailById(BannerId);
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || UserId == 0)
                UserId = Convert.ToInt32(SessionManagement.UserId);

            var model = new mdlGallary();
            string defaultPath = "/Resources/Gallery/" + UserId + "/";
            if (GallaryId != 0)
            {
                model = mdlGallary.getGallaryById(UserId, GallaryId, defaultPath);
            }
            Result = objProfileData.RemoveGallaryItem(UserId, GallaryId);
            //String path = Server.MapPath(model.FilePath);
            if (System.IO.File.Exists(model.FilePath))
            {
                System.IO.File.Delete(model.FilePath);
            }
            TempData["errorMessage"] = "Gallery item removed successfully";
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #region PublicProfileSection

        public ActionResult GetPublicProfileSection(int UserId)
        {
            DentistProfileBLL obj = new DentistProfileBLL();
            PublicProfileSection newobj = new PublicProfileSection();
            newobj = obj.GetPublicProfilesectionDetails(UserId);
            return View(newobj);
        }

        public ActionResult NewGetPublicProfileSection(int UserId)
        {
            DentistProfileBLL obj = new DentistProfileBLL();
            PublicProfileSection newobj = new PublicProfileSection();
            newobj = obj.GetPublicProfilesectionDetails(UserId);
            return View("_PartialGetPublicProfileSectionPopup", newobj);
        }

        public ActionResult InsertUpdatePublicProfileSection(PublicProfileSection Obj)
        {
            bool Result = false;
            DentistProfileBLL obj = new DentistProfileBLL();
            Result = obj.InsertUpdatePublicProfileSectionDetials(Obj);
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult GetProcedureList()
        {
            List<Procedure> lst = new List<Procedure>();
            List<Procedure> Final = new List<Procedure>();
            lst = ProcedureBLL.GetProcedureList();
            List<Procedure> Newlst = new List<Procedure>();
            Newlst = ProcedureBLL.GetProcedureList(0, Convert.ToInt32(SessionManagement.UserId));
            if (Newlst.Count > 0)
            {
                List<Procedure> Check = Newlst.ToList().Union(lst.ToList()).ToList();
                var Combined = Check.GroupBy(x => x.ProcedureName).Select(y => y.First());
                Final = Combined.ToList();
                foreach (var item in Newlst)
                {
                    Final.Where(t => t.ProcedureName == item.ProcedureName).FirstOrDefault().Ischeck = true;
                }
            }
            else
            {
                Final = lst;
                foreach (var item in lst)
                {
                    Final.Where(t => t.ProcedureName == item.ProcedureName).FirstOrDefault().Ischeck = false;
                }
            }
            return View(Final);
        }

        public ActionResult NewGetProcedureList()
        {
            List<Procedure> lst = new List<Procedure>();
            List<Procedure> Final = new List<Procedure>();
            lst = ProcedureBLL.GetProcedureList();
            List<Procedure> Newlst = new List<Procedure>();
            Newlst = ProcedureBLL.GetProcedureList(0, Convert.ToInt32(SessionManagement.UserId));
            if (Newlst.Count > 0)
            {
                List<Procedure> Check = Newlst.ToList().Union(lst.ToList()).ToList();
                var Combined = Check.GroupBy(x => x.ProcedureName).Select(y => y.First());
                Final = Combined.ToList();
                foreach (var item in Newlst)
                {
                    Final.Where(t => t.ProcedureName == item.ProcedureName).FirstOrDefault().Ischeck = true;
                }
            }
            else
            {
                Final = lst;
                foreach (var item in lst)
                {
                    Final.Where(t => t.ProcedureName == item.ProcedureName).FirstOrDefault().Ischeck = false;
                }
            }
            ViewBag.main = (Newlst.Count == Final.Count) ? "true" : "";
            return View("_PartialAddEditProcedureListPopUp", Final);
        }

        public JsonResult DeleteProcedure(int ProcedureId)
        {
            Procedure obj = new Procedure();
            obj.ProcedureId = ProcedureId;
            return Json(ProcedureBLL.DeleteProcedure(obj, Convert.ToInt32(SessionManagement.UserId)), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult UpdateProcedureList(List<Procedure> Proc)
        {
            return Json(ProcedureBLL.InsertUpdateProcedureofUser(Proc, Convert.ToInt32(Session["UserId"])), JsonRequestBehavior.AllowGet);
        }

        #region Feature Account Setting

        public ActionResult GetFeatureSettingSection(int UserId)
        {
            try
            {
                int AccountId = ColleaguesBLL.GetAccountIdbyUserId(UserId, "GetAccountByUserId");
                List<chkFeatureSetting> lstFeature = AdminBLL.GetAccountFeatureSettingByAccountId(AccountId);
                lstFeature = lstFeature.Where(T => T.AdminStatus == true).ToList();
                return PartialView("_PartialAccountFeatureSetting", lstFeature);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult UpdateFeatureSetting(int Id, int status)
        {
            try
            {
                AccountFeatureSetting obj = new AccountFeatureSetting();
                obj.id = Id;
                obj.DentistStatus = Convert.ToBoolean(status);
                int UpdateId = AdminBLL.InsertUpdateAccountFeatureSetting(obj, false);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public JsonResult GetLocationListForDentist(int Id)
        {
            List<LocationDetail> lst = new List<LocationDetail>();
            List<LocationDetail> Sortedlst = new List<LocationDetail>();
            lst = DentistProfileBLL.GetColleaguesLocationList(Convert.ToInt32(Id));
            lst = lst.OrderBy(t => t.ContactType != 1).ToList();
            //Sortedlst = lst;
            //lst = Sortedlst.Where(x => x.ContactType == 1).ToList();
            //lst.AddRange(Sortedlst.Where(x => x.ContactType != 1).ToList());
            if (lst.Count > 1)
            {
                return Json(lst, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            
        }

        #endregion

        public JsonResult GetEPMSIdbyAccountId(string ExternalPMSId ,int LocationId)
        {
            int UserId = Convert.ToInt32(SessionManagement.UserId);
            ProfileBLL profilebll = new ProfileBLL();
            bool result = profilebll.GetEPMSIdbyAccountId(UserId, ExternalPMSId, LocationId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}
