﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewRecordlinc.App_Start;
using DataAccessLayer;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Configuration;
using DataAccessLayer.PatientsData;
using NewRecordlinc.Models.ViewModel;
using System.Text;
using NewRecordlinc.Models.Patients;
using BusinessLogicLayer;

namespace NewRecordlinc.Controllers
{
    public class DentalFormsController : Controller
    {

        DataRepository objDataRepository = new DataRepository();
        clsPatientsData ObjPatientsData = new clsPatientsData();
        clsCommon objCommon = new clsCommon();
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();

         // Update On 19-04-2019 with jira : RM-904 
        public ActionResult Index(string ZSTGHSK)
        {            if (!string.IsNullOrWhiteSpace(ZSTGHSK))
            {                string DecryptString = ObjTripleDESCryptoHelper.decryptText(ZSTGHSK);                string[] PatientIdWithAuthority = DecryptString.Split('|');                int PatientId = Convert.ToInt32(PatientIdWithAuthority[0]);

                ViewBag.statelistdata = InsuanceEstimatorBLL.GetStateList();
                ViewBag.patientid = PatientId;
                if (PatientId != 0)
                {                    var model = getallvalue(PatientId);
                    ViewBag.IsAuthorize = PatientIdWithAuthority[1];
                    return View(model);
                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Patients");
            }
        }
        //public ActionResult Index(string ZSTGHSK)
        //{
        //    if (!string.IsNullOrWhiteSpace(ZSTGHSK))
        //    {
        //        string DecryptString = ObjTripleDESCryptoHelper.decryptText(ZSTGHSK);
        //        string[] PatientIdWithAuthority = DecryptString.Split('|');
        //        int PatientId = Convert.ToInt32(PatientIdWithAuthority[0]);
        //        ViewBag.patientid = PatientId;
        //        if (PatientId != 0)
        //        {
        //            var model = getallvalue(PatientId);
        //            ViewBag.IsAuthorize = PatientIdWithAuthority[1];
        //            return View(model);
        //        }
        //        return View();
        //    }
        //    else
        //    {
        //        return RedirectToAction("Index", "Patients");
        //    }
        //}


        private DentalFormMaster getallvalue(int PatientId)
        {
            var model = new DentalFormMaster();
            model.BasicInfo = objDataRepository.GetPatientBasicInfo(PatientId, SessionManagement.TimeZoneSystemName);
            model.RegistrationForm = objDataRepository.GetRegistration(PatientId);

            model.DentalHistory = objDataRepository.GetDentalHistory(PatientId);
            model.MedicalHistory = objDataRepository.GetMedicalHistory(PatientId);

            return model;
        }



        [HttpPost]
        public JsonResult PatientInformation(string PatientId, string FirstName, string LastName, string Email, string DateOfBirth, string Gender, string Address, string City, string State, string Zipcode, string PrimaryPhone, string SecondaryPhone, string EmergencyContactName, string EmergencyPhone)
        {
            object obj = string.Empty;
            try
            {
                bool EmailCheck = false;
                DataTable dtPatientDetails = ObjPatientsData.GetPatientsDetails(Convert.ToInt32(PatientId));
                string AssignedPatientId = string.Empty;
                string MiddelName = string.Empty;
                string ProfileImage = string.Empty;
                string Country = string.Empty;
                string Address2 = string.Empty;
                string Notes = string.Empty;
                string OldEmail = string.Empty;


                if (dtPatientDetails != null && dtPatientDetails.Rows.Count > 0)
                {
                    AssignedPatientId = objCommon.CheckNull(Convert.ToString(dtPatientDetails.Rows[0]["AssignedPatientId"]), string.Empty);
                    MiddelName = objCommon.CheckNull(Convert.ToString(dtPatientDetails.Rows[0]["MiddelName"]), string.Empty);
                    ProfileImage = objCommon.CheckNull(Convert.ToString(dtPatientDetails.Rows[0]["ProfileImage"]), string.Empty);
                    Country = objCommon.CheckNull(Convert.ToString(dtPatientDetails.Rows[0]["Country"]), string.Empty);
                    Address2 = objCommon.CheckNull(Convert.ToString(dtPatientDetails.Rows[0]["Address2"]), string.Empty);

                    Notes = objCommon.CheckNull(Convert.ToString(dtPatientDetails.Rows[0]["Comments"]), string.Empty);
                    OldEmail = objCommon.CheckNull(Convert.ToString(dtPatientDetails.Rows[0]["Email"]), string.Empty);
                }
                if (OldEmail == Email)
                {
                    if (!string.IsNullOrEmpty(PatientId))
                    {
                        int Result = 0;
                        DateTime? dt_DateOfBirth = null;
                        if (string.IsNullOrEmpty(DateOfBirth))
                        {
                            DateOfBirth = "1900-01-01 00:00:00.000";
                        }
                        else
                        {
                            // dt_DateOfBirth = clsHelper.ConvertToUTC(Convert.ToDateTime(DateOfBirth), SessionManagement.TimeZoneSystemName);
                            dt_DateOfBirth = DateTime.ParseExact(DateOfBirth, "MM/dd/yyyy", null);
                        }

                        Result = ObjPatientsData.PatientInsertAndUpdate(Convert.ToInt32(PatientId), AssignedPatientId, FirstName, MiddelName, LastName, dt_DateOfBirth, Convert.ToInt32(Gender), ProfileImage, PrimaryPhone, Email, Address, City, State, Zipcode, Country, Convert.ToInt32(Session["UserId"]), 0, Address2, "", Notes, null, null, null);
                        #region Emargency contact name,and phone

                        // Registartion Table


                        string Dob = DateOfBirth; string ddlgenderreg = Convert.ToString(Gender); string drpStatus = string.Empty;
                        string ResidenceStreet = Address;
                        string Zip = Zipcode; string ResidenceTelephone = PrimaryPhone;
                        string WorkPhone = SecondaryPhone;
                        string Fax = string.Empty; string ResponsiblepartyFname = string.Empty; string ResponsiblepartyLname = string.Empty;
                        string ResponsibleRelationship = string.Empty; string ResponsibleAddress = string.Empty; string ResponsibleCity = string.Empty;
                        string ResponsibleState = string.Empty; string ResponsibleZipCode = string.Empty; string ResponsibleDOB = string.Empty;
                        string ResponsibleContact = string.Empty; string Whommay = string.Empty; string Emailaddress = string.Empty;

                        string emergencyname = EmergencyContactName; string emergency = EmergencyPhone;

                        string chkInsurance = string.Empty; string chkCash = string.Empty; string chkCrediteCard = string.Empty;
                        string EmployeeName1 = string.Empty; string EmployeeDob1 = string.Empty; string EmployerName1 = string.Empty;
                        string YearsEmployed1 = string.Empty; string NameofInsurance1 = string.Empty; string InsuranceAddress1 = string.Empty;
                        string InsuranceTelephone1 = string.Empty; string EmployeeName2 = string.Empty; string EmployeeDob2 = string.Empty;
                        string EmployerName2 = string.Empty; string YearsEmployed2 = string.Empty; string NameofInsurance2 = string.Empty;
                        string InsuranceAddress2 = string.Empty; string InsuranceTelephone2 = string.Empty; string GroupNumber1 = string.Empty;
                        string GroupNumber2 = string.Empty;

                        DataTable dtRegi = new DataTable();
                        dtRegi = ObjPatientsData.GetRegistration(Convert.ToInt32(PatientId), "GetRegistrationform");

                        if (dtRegi != null && dtRegi.Rows.Count > 0)
                        {
                            ResponsiblepartyFname = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsiblepartyFname"]), string.Empty);
                            ResponsiblepartyLname = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsiblepartyLname"]), string.Empty);
                            ResponsibleRelationship = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleRelationship"]), string.Empty);
                            ResponsibleAddress = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleAddress"]), string.Empty);
                            ResponsibleZipCode = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleZipCode"]), string.Empty);
                            ResponsibleCity = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleCity"]), string.Empty);
                            ResponsibleState = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleState"]), string.Empty);
                            ResponsibleContact = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleContact"]), string.Empty);
                            EmployeeName1 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployeeName1"]), string.Empty);
                            EmployeeDob1 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployeeDob1"]), string.Empty);
                            EmployerName1 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployerName1"]), string.Empty);
                            NameofInsurance1 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtNameofInsurance1"]), string.Empty);
                            InsuranceAddress1 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsuranceAddress1"]), string.Empty);
                            InsuranceTelephone1 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsurancePhone1"]), string.Empty);
                            EmployeeName2 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployeeName2"]), string.Empty);
                            EmployeeDob2 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployeeDob2"]), string.Empty);
                            EmployerName2 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployerName2"]), string.Empty);
                            NameofInsurance2 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtNameofInsurance2"]), string.Empty);
                            YearsEmployed2 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtYearsEmployed2"]), string.Empty);
                            InsuranceAddress2 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsuranceAddress2"]), string.Empty);
                            InsuranceTelephone2 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsurancePhone2"]), string.Empty);
                            GroupNumber1 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsuranceTelephone1"]), string.Empty);
                            GroupNumber2 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsuranceTelephone2"]), string.Empty);
                            Emailaddress = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtemailaddress"]), string.Empty);
                        }


                        DataTable tblRegistration = new DataTable("Registration");
                        tblRegistration.Columns.Add("txtResponsiblepartyFname");
                        tblRegistration.Columns.Add("txtResponsiblepartyLname");
                        tblRegistration.Columns.Add("txtResponsibleRelationship");
                        tblRegistration.Columns.Add("txtResponsibleAddress");
                        tblRegistration.Columns.Add("txtResponsibleCity");
                        tblRegistration.Columns.Add("txtResponsibleState");
                        tblRegistration.Columns.Add("txtResponsibleZipCode");
                        tblRegistration.Columns.Add("txtResponsibleDOB");
                        tblRegistration.Columns.Add("txtResponsibleContact");
                        tblRegistration.Columns.Add("txtPatientPresentPosition");
                        tblRegistration.Columns.Add("txtPatientHowLongHeld");
                        tblRegistration.Columns.Add("txtParentPresentPosition");
                        tblRegistration.Columns.Add("txtParentHowLongHeld");
                        tblRegistration.Columns.Add("txtDriversLicense");
                        tblRegistration.Columns.Add("chkMethodOfPayment");
                        tblRegistration.Columns.Add("txtPurposeCall");
                        tblRegistration.Columns.Add("txtOtherFamily");
                        tblRegistration.Columns.Add("txtWhommay");
                        tblRegistration.Columns.Add("txtSomeonetonotify");
                        tblRegistration.Columns.Add("txtemergency");
                        tblRegistration.Columns.Add("txtemergencyname");
                        tblRegistration.Columns.Add("txtEmployeeName1");
                        tblRegistration.Columns.Add("txtEmployeeDob1");
                        tblRegistration.Columns.Add("txtEmployerName1");
                        tblRegistration.Columns.Add("txtYearsEmployed1");
                        tblRegistration.Columns.Add("txtNameofInsurance1");
                        tblRegistration.Columns.Add("txtInsuranceAddress1");
                        tblRegistration.Columns.Add("txtInsuranceTelephone1");
                        tblRegistration.Columns.Add("txtProgramorpolicy1");
                        tblRegistration.Columns.Add("txtSocialSecurity1");
                        tblRegistration.Columns.Add("txtUnionLocal1");
                        tblRegistration.Columns.Add("txtEmployeeName2");
                        tblRegistration.Columns.Add("txtEmployeeDob2");
                        tblRegistration.Columns.Add("txtEmployerName2");
                        tblRegistration.Columns.Add("txtYearsEmployed2");
                        tblRegistration.Columns.Add("txtNameofInsurance2");
                        tblRegistration.Columns.Add("txtInsuranceAddress2");
                        tblRegistration.Columns.Add("txtInsuranceTelephone2");
                        tblRegistration.Columns.Add("txtProgramorpolicy2");
                        tblRegistration.Columns.Add("txtSocialSecurity2");
                        tblRegistration.Columns.Add("txtUnionLocal2");
                        tblRegistration.Columns.Add("txtDigiSign");
                        tblRegistration.Columns.Add("txtInsurancePhone1");
                        tblRegistration.Columns.Add("txtInsurancePhone2");
                        tblRegistration.Columns.Add("txtemailaddress");
                        StringBuilder chkMethodOfPayment = new StringBuilder();


                        if (Dob == null || Dob == "")
                        {
                            Dob = "1900-01-01 00:00:00.000";
                        }

                        if (chkCash == "1")
                        {
                            if (!String.IsNullOrEmpty(chkMethodOfPayment.ToString()))
                            {
                                chkMethodOfPayment.Append("," + "Cash");
                            }
                            else
                            {
                                chkMethodOfPayment.Append("Cash");
                            }
                        }
                        if (chkInsurance == "1")
                        {
                            if (!String.IsNullOrEmpty(chkMethodOfPayment.ToString()))
                            {
                                chkMethodOfPayment.Append("," + "Insurance");
                            }
                            else
                            {
                                chkMethodOfPayment.Append("Insurance");
                            }
                        }
                        if (chkCrediteCard == "1")
                        {
                            if (!String.IsNullOrEmpty(chkMethodOfPayment.ToString()))
                            {
                                chkMethodOfPayment.Append("," + "Credit Card");
                            }
                            else
                            {
                                chkMethodOfPayment.Append("Credit Card");
                            }
                        }

                        tblRegistration.Columns.Add("CreatedDate");
                        tblRegistration.Columns.Add("ModifiedDate");


                        tblRegistration.Rows.Add(ResponsiblepartyFname, ResponsiblepartyLname, ResponsibleRelationship, ResponsibleAddress, ResponsibleCity, ResponsibleState,
                    ResponsibleZipCode, ResponsibleDOB, ResponsibleContact, "", "", "", "", "", chkMethodOfPayment.ToString(), "", "", Whommay, "", emergency, emergencyname, EmployeeName1,
                    EmployeeDob1, EmployerName1, YearsEmployed1, NameofInsurance1, InsuranceAddress1, GroupNumber1, "", "", "", EmployeeName2, EmployeeDob2,
                    EmployerName2, YearsEmployed2, NameofInsurance2, InsuranceAddress2, GroupNumber2, "", "", "", "", InsuranceTelephone1, InsuranceTelephone2, Emailaddress);




                        DataSet dsRegistration = new DataSet("Registration");
                        dsRegistration.Tables.Add(tblRegistration);

                        DataSet ds = dsRegistration.Copy();
                        dsRegistration.Clear();

                        string Registration = ds.GetXml();

                        bool status1 = ObjPatientsData.UpdateRegistrationForm(Convert.ToInt32(PatientId), "UpdatePatient", Convert.ToString(Dob), ddlgenderreg,
                            drpStatus, ResidenceStreet, City, State, Zip,
                             ResidenceTelephone, WorkPhone, Fax, Registration);
                        #endregion
                    }
                    obj = "2";
                }
                else
                {
                    //EmailCheck = ObjPatientsData.CheckPatientEmail(Email);
                    //if (EmailCheck)
                    //{
                    //    obj = "1";
                    //}
                    //else
                    //{
                    if (!string.IsNullOrEmpty(PatientId))
                    {
                        int Result = 0;
                        DateTime? dt_DateOfBirth = null;
                        if (string.IsNullOrEmpty(DateOfBirth))
                        {
                            DateOfBirth = "1900-01-01 00:00:00.000";
                        }
                        else
                        {
                            // dt_DateOfBirth = clsHelper.ConvertToUTC(Convert.ToDateTime(DateOfBirth), SessionManagement.TimeZoneSystemName);
                            dt_DateOfBirth = DateTime.ParseExact(DateOfBirth, "MM/dd/yyyy", null);
                        }

                        //else
                        //{
                        //    dt_DateOfBirth = clsHelper.ConvertToUTC(Convert.ToDateTime(DateOfBirth), SessionManagement.TimeZoneSystemName);
                        //}

                        Result = ObjPatientsData.PatientInsertAndUpdate(Convert.ToInt32(PatientId), AssignedPatientId, FirstName, MiddelName, LastName, dt_DateOfBirth, Convert.ToInt32(Gender), ProfileImage, PrimaryPhone, Email, Address, City, State, Zipcode, Country, Convert.ToInt32(Session["UserId"]), 0, Address2, "", Notes, null, null);
                        #region Emargency contact name,and phone

                        // Registartion Table


                        string Dob = DateOfBirth; string ddlgenderreg = Convert.ToString(Gender); string drpStatus = string.Empty;
                        string ResidenceStreet = Address;
                        string Zip = Zipcode; string ResidenceTelephone = PrimaryPhone;
                        string WorkPhone = SecondaryPhone;
                        string Fax = string.Empty; string ResponsiblepartyFname = string.Empty; string ResponsiblepartyLname = string.Empty;
                        string ResponsibleRelationship = string.Empty; string ResponsibleAddress = string.Empty; string ResponsibleCity = string.Empty;
                        string ResponsibleState = string.Empty; string ResponsibleZipCode = string.Empty; string ResponsibleDOB = string.Empty;
                        string ResponsibleContact = string.Empty; string Whommay = string.Empty; string Emailaddress = string.Empty;

                        string emergencyname = EmergencyContactName; string emergency = EmergencyPhone;

                        string chkInsurance = string.Empty; string chkCash = string.Empty; string chkCrediteCard = string.Empty;
                        string EmployeeName1 = string.Empty; string EmployeeDob1 = string.Empty; string EmployerName1 = string.Empty;
                        string YearsEmployed1 = string.Empty; string NameofInsurance1 = string.Empty; string InsuranceAddress1 = string.Empty;
                        string InsuranceTelephone1 = string.Empty; string EmployeeName2 = string.Empty; string EmployeeDob2 = string.Empty;
                        string EmployerName2 = string.Empty; string YearsEmployed2 = string.Empty; string NameofInsurance2 = string.Empty;
                        string InsuranceAddress2 = string.Empty; string InsuranceTelephone2 = string.Empty; string GroupNumber1 = string.Empty;
                        string GroupNumber2 = string.Empty;

                        DataTable dtRegi = new DataTable();
                        dtRegi = ObjPatientsData.GetRegistration(Convert.ToInt32(PatientId), "GetRegistrationform");

                        if (dtRegi != null && dtRegi.Rows.Count > 0)
                        {
                            ResponsiblepartyFname = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsiblepartyFname"]), string.Empty);
                            ResponsiblepartyLname = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsiblepartyLname"]), string.Empty);
                            ResponsibleRelationship = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleRelationship"]), string.Empty);
                            ResponsibleAddress = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleAddress"]), string.Empty);
                            ResponsibleZipCode = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleZipCode"]), string.Empty);
                            ResponsibleCity = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleCity"]), string.Empty);
                            ResponsibleState = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleState"]), string.Empty);
                            ResponsibleContact = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtResponsibleContact"]), string.Empty);
                            EmployeeName1 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployeeName1"]), string.Empty);
                            EmployeeDob1 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployeeDob1"]), string.Empty);
                            EmployerName1 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployerName1"]), string.Empty);
                            NameofInsurance1 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtNameofInsurance1"]), string.Empty);
                            InsuranceAddress1 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsuranceAddress1"]), string.Empty);
                            InsuranceTelephone1 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsurancePhone1"]), string.Empty);
                            EmployeeName2 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployeeName2"]), string.Empty);
                            EmployeeDob2 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployeeDob2"]), string.Empty);
                            EmployerName2 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtEmployerName2"]), string.Empty);
                            NameofInsurance2 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtNameofInsurance2"]), string.Empty);
                            YearsEmployed2 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtYearsEmployed2"]), string.Empty);
                            InsuranceAddress2 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsuranceAddress2"]), string.Empty);
                            InsuranceTelephone2 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsurancePhone2"]), string.Empty);
                            GroupNumber1 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsuranceTelephone1"]), string.Empty);
                            GroupNumber2 = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtInsuranceTelephone2"]), string.Empty);
                            Emailaddress = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtemailaddress"]), string.Empty);
                        }


                        DataTable tblRegistration = new DataTable("Registration");
                        tblRegistration.Columns.Add("txtResponsiblepartyFname");
                        tblRegistration.Columns.Add("txtResponsiblepartyLname");
                        tblRegistration.Columns.Add("txtResponsibleRelationship");
                        tblRegistration.Columns.Add("txtResponsibleAddress");
                        tblRegistration.Columns.Add("txtResponsibleCity");
                        tblRegistration.Columns.Add("txtResponsibleState");
                        tblRegistration.Columns.Add("txtResponsibleZipCode");
                        tblRegistration.Columns.Add("txtResponsibleDOB");
                        tblRegistration.Columns.Add("txtResponsibleContact");
                        tblRegistration.Columns.Add("txtPatientPresentPosition");
                        tblRegistration.Columns.Add("txtPatientHowLongHeld");
                        tblRegistration.Columns.Add("txtParentPresentPosition");
                        tblRegistration.Columns.Add("txtParentHowLongHeld");
                        tblRegistration.Columns.Add("txtDriversLicense");
                        tblRegistration.Columns.Add("chkMethodOfPayment");
                        tblRegistration.Columns.Add("txtPurposeCall");
                        tblRegistration.Columns.Add("txtOtherFamily");
                        tblRegistration.Columns.Add("txtWhommay");
                        tblRegistration.Columns.Add("txtSomeonetonotify");
                        tblRegistration.Columns.Add("txtemergency");
                        tblRegistration.Columns.Add("txtemergencyname");
                        tblRegistration.Columns.Add("txtEmployeeName1");
                        tblRegistration.Columns.Add("txtEmployeeDob1");
                        tblRegistration.Columns.Add("txtEmployerName1");
                        tblRegistration.Columns.Add("txtYearsEmployed1");
                        tblRegistration.Columns.Add("txtNameofInsurance1");
                        tblRegistration.Columns.Add("txtInsuranceAddress1");
                        tblRegistration.Columns.Add("txtInsuranceTelephone1");
                        tblRegistration.Columns.Add("txtProgramorpolicy1");
                        tblRegistration.Columns.Add("txtSocialSecurity1");
                        tblRegistration.Columns.Add("txtUnionLocal1");
                        tblRegistration.Columns.Add("txtEmployeeName2");
                        tblRegistration.Columns.Add("txtEmployeeDob2");
                        tblRegistration.Columns.Add("txtEmployerName2");
                        tblRegistration.Columns.Add("txtYearsEmployed2");
                        tblRegistration.Columns.Add("txtNameofInsurance2");
                        tblRegistration.Columns.Add("txtInsuranceAddress2");
                        tblRegistration.Columns.Add("txtInsuranceTelephone2");
                        tblRegistration.Columns.Add("txtProgramorpolicy2");
                        tblRegistration.Columns.Add("txtSocialSecurity2");
                        tblRegistration.Columns.Add("txtUnionLocal2");
                        tblRegistration.Columns.Add("txtDigiSign");
                        tblRegistration.Columns.Add("txtInsurancePhone1");
                        tblRegistration.Columns.Add("txtInsurancePhone2");
                        tblRegistration.Columns.Add("txtemailaddress");
                        StringBuilder chkMethodOfPayment = new StringBuilder();


                        if (Dob == null || Dob == "")
                        {
                            Dob = "1900-01-01 00:00:00.000";
                        }
                        if (chkCash == "1")
                        {
                            if (!String.IsNullOrEmpty(chkMethodOfPayment.ToString()))
                            {
                                chkMethodOfPayment.Append("," + "Cash");
                            }
                            else
                            {
                                chkMethodOfPayment.Append("Cash");
                            }
                        }
                        if (chkInsurance == "1")
                        {
                            if (!String.IsNullOrEmpty(chkMethodOfPayment.ToString()))
                            {
                                chkMethodOfPayment.Append("," + "Insurance");
                            }
                            else
                            {
                                chkMethodOfPayment.Append("Insurance");
                            }
                        }
                        if (chkCrediteCard == "1")
                        {
                            if (!String.IsNullOrEmpty(chkMethodOfPayment.ToString()))
                            {
                                chkMethodOfPayment.Append("," + "Credit Card");
                            }
                            else
                            {
                                chkMethodOfPayment.Append("Credit Card");
                            }
                        }

                        tblRegistration.Columns.Add("CreatedDate");
                        tblRegistration.Columns.Add("ModifiedDate");


                    //    tblRegistration.Rows.Add(ResponsiblepartyFname, ResponsiblepartyLname, ResponsibleRelationship, ResponsibleAddress, ResponsibleCity, ResponsibleState,
                    //ResponsibleZipCode, ResponsibleDOB, ResponsibleContact, "", "", "", "", "", chkMethodOfPayment.ToString(), "", "", Whommay, "", emergency, emergencyname, EmployeeName1,
                    //EmployeeDob1, EmployerName1, YearsEmployed1, NameofInsurance1, InsuranceAddress1, InsuranceTelephone1, "", "", "", EmployeeName2, EmployeeDob2,
                    //EmployerName2, YearsEmployed2, NameofInsurance2, InsuranceAddress2, InsuranceTelephone2, "", "", "", "");

                        tblRegistration.Rows.Add(ResponsiblepartyFname, ResponsiblepartyLname, ResponsibleRelationship, ResponsibleAddress, ResponsibleCity, ResponsibleState,
                   ResponsibleZipCode, ResponsibleDOB, ResponsibleContact, "", "", "", "", "", chkMethodOfPayment.ToString(), "", "", Whommay, "", emergency, emergencyname, EmployeeName1,
                   EmployeeDob1, EmployerName1, YearsEmployed1, NameofInsurance1, InsuranceAddress1, GroupNumber1, "", "", "", EmployeeName2, EmployeeDob2,
                   EmployerName2, YearsEmployed2, NameofInsurance2, InsuranceAddress2, GroupNumber2, "", "", "", "", InsuranceTelephone1, InsuranceTelephone2, Emailaddress);


                        DataSet dsRegistration = new DataSet("Registration");
                        dsRegistration.Tables.Add(tblRegistration);

                        DataSet ds = dsRegistration.Copy();
                        dsRegistration.Clear();

                        string Registration = ds.GetXml();

                        bool status1 = ObjPatientsData.UpdateRegistrationForm(Convert.ToInt32(PatientId), "UpdatePatient", Convert.ToString(Dob), ddlgenderreg,
                            drpStatus, ResidenceStreet, City, State, Zip,
                             ResidenceTelephone, WorkPhone, Fax, Registration);
                        #endregion
                        obj = "2";
                    }
                }
                //}



            }
            catch (Exception)
            {

                throw;
            }

            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult InsuranceCoverage(string PatientId, string ResponsFirstName, string ResponsLastName, string ResponsRelationship, string ResponsAddress, string ResponsCity, string ResponsState, string ResponsZipcode, string ResponsPhoneNumber, string PrimaryInsuranceCompany, string PrimaryInsurancePhone, string PrimaryNameOfInsured, string PrimaryDateOfBirth, string PrimaryMemberID, string PrimaryGroupNumber, string SecondaryInsuranceCompany, string SecondaryInsurancePhone, string SecondaryNameOfInsured, string SecondaryDateOfBirth, string SecondaryMemberID, string SecondaryGroupNumber, string emailaddress)
        {
            object obj = string.Empty;
            try
            {

                if (!string.IsNullOrEmpty(PatientId))
                {

                    DataSet dsp = new DataSet();
                    DataTable dtPatient = new DataTable();
                    dsp = ObjPatientsData.GetPateintDetailsByPatientId(Convert.ToInt32(PatientId));
                    dtPatient = dsp.Tables[0];
                    string Gender = string.Empty;
                    string Status = string.Empty;
                    string PatientAddress = string.Empty;
                    string DateOfBirth = string.Empty;

                    string PatientCity = string.Empty;
                    string PatientState = string.Empty;
                    string PatientZipcode = string.Empty;
                    string PatientPrimaryPhone = string.Empty;
                    string MobilePhone = string.Empty;
                    string PatiantFax = string.Empty;

                    string EmergencyContactName = string.Empty;
                    string EmergencyContactPhoneNumber = string.Empty;
                    string SecondaryPhone = string.Empty;


                    if (dtPatient != null && dtPatient.Rows.Count > 0)
                    {
                        DataTable dtRegi = new DataTable();
                        dtRegi = ObjPatientsData.GetRegistration(Convert.ToInt32(PatientId), "GetRegistrationform");
                        Gender = objCommon.CheckNull(Convert.ToString(dtPatient.Rows[0]["Gender"]), string.Empty) == "Male" ? "0" : "1";
                        Status = string.Empty;
                        PatientAddress = objCommon.CheckNull(Convert.ToString(dtPatient.Rows[0]["ExactAddress"]), string.Empty);
                        DateOfBirth = objCommon.CheckNull(Convert.ToString(dtPatient.Rows[0]["DateOfBirth"]), string.Empty);

                        PatientCity = objCommon.CheckNull(Convert.ToString(dtPatient.Rows[0]["City"]), string.Empty);
                        PatientState = objCommon.CheckNull(Convert.ToString(dtPatient.Rows[0]["State"]), string.Empty);
                        PatientZipcode = objCommon.CheckNull(Convert.ToString(dtPatient.Rows[0]["ZipCode"]), string.Empty);
                        PatientPrimaryPhone = objCommon.CheckNull(Convert.ToString(dtPatient.Rows[0]["Phone"]), string.Empty);
                        MobilePhone = string.Empty;
                        PatiantFax = string.Empty;

                        if (dtRegi != null && dtRegi.Rows.Count > 0)
                        {
                            EmergencyContactName = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtemergencyname"]), string.Empty);
                            EmergencyContactPhoneNumber = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtemergency"]), string.Empty);
                            SecondaryPhone = objCommon.CheckNull(Convert.ToString(dtRegi.Rows[0]["txtemergency"]), string.Empty);
                        }
                        DataTable dtt = new clsPatientsData().GetPatientsDetails(Convert.ToInt32(PatientId));
                        if (dtt != null && dtt.Rows.Count > 0)
                        {
                            SecondaryPhone = objCommon.CheckNull(Convert.ToString(dtt.Rows[0]["SecondaryPhone"]), string.Empty);
                        }

                    }

                    if (DateOfBirth == null || DateOfBirth == "")
                    {
                        DateOfBirth = "1900-01-01 00:00:00.000";
                    }

                    string Dob = DateOfBirth; string ddlgenderreg = Convert.ToString(Gender); string drpStatus = string.Empty;
                    string ResidenceStreet = PatientAddress; string City = PatientCity; string State = PatientState;
                    string Zip = PatientZipcode; string ResidenceTelephone = PatientPrimaryPhone;

                    string emergencyname = EmergencyContactName; string emergency = EmergencyContactPhoneNumber; string WorkPhone = SecondaryPhone;


                    string Fax = string.Empty;
                    string ResponsiblepartyFname = ResponsFirstName;
                    string ResponsiblepartyLname = ResponsLastName;
                    string ResponsibleRelationship = ResponsRelationship;
                    string ResponsibleAddress = ResponsAddress;
                    string ResponsibleCity = ResponsCity;
                    string ResponsibleState = ResponsState;
                    string ResponsibleZipCode = ResponsZipcode;
                    string ResponsibleDOB = string.Empty;
                    string ResponsibleContact = ResponsPhoneNumber;
                    string Whommay = string.Empty;



                    string chkInsurance = string.Empty; string chkCash = string.Empty; string chkCrediteCard = string.Empty;

                    DataTable tblRegistration = new DataTable("Registration");
                    tblRegistration.Columns.Add("txtResponsiblepartyFname");
                    tblRegistration.Columns.Add("txtResponsiblepartyLname");
                    tblRegistration.Columns.Add("txtResponsibleRelationship");
                    tblRegistration.Columns.Add("txtResponsibleAddress");
                    tblRegistration.Columns.Add("txtResponsibleCity");
                    tblRegistration.Columns.Add("txtResponsibleState");
                    tblRegistration.Columns.Add("txtResponsibleZipCode");
                    tblRegistration.Columns.Add("txtResponsibleDOB");
                    tblRegistration.Columns.Add("txtResponsibleContact");
                    tblRegistration.Columns.Add("txtPatientPresentPosition");
                    tblRegistration.Columns.Add("txtPatientHowLongHeld");
                    tblRegistration.Columns.Add("txtParentPresentPosition");
                    tblRegistration.Columns.Add("txtParentHowLongHeld");
                    tblRegistration.Columns.Add("txtDriversLicense");
                    tblRegistration.Columns.Add("chkMethodOfPayment");
                    tblRegistration.Columns.Add("txtPurposeCall");
                    tblRegistration.Columns.Add("txtOtherFamily");
                    tblRegistration.Columns.Add("txtWhommay");
                    tblRegistration.Columns.Add("txtSomeonetonotify");
                    tblRegistration.Columns.Add("txtemergency");
                    tblRegistration.Columns.Add("txtemergencyname");
                    tblRegistration.Columns.Add("txtEmployeeName1");
                    tblRegistration.Columns.Add("txtInsurancePhone1");
                    tblRegistration.Columns.Add("txtEmployeeDob1");
                    tblRegistration.Columns.Add("txtEmployerName1");
                    tblRegistration.Columns.Add("txtYearsEmployed1");
                    tblRegistration.Columns.Add("txtNameofInsurance1");
                    tblRegistration.Columns.Add("txtInsuranceAddress1");
                    tblRegistration.Columns.Add("txtInsuranceTelephone1");
                    tblRegistration.Columns.Add("txtProgramorpolicy1");
                    tblRegistration.Columns.Add("txtSocialSecurity1");
                    tblRegistration.Columns.Add("txtUnionLocal1");
                    tblRegistration.Columns.Add("txtEmployeeName2");
                    tblRegistration.Columns.Add("txtInsurancePhone2");
                    tblRegistration.Columns.Add("txtEmployeeDob2");
                    tblRegistration.Columns.Add("txtEmployerName2");
                    tblRegistration.Columns.Add("txtYearsEmployed2");
                    tblRegistration.Columns.Add("txtNameofInsurance2");
                    tblRegistration.Columns.Add("txtInsuranceAddress2");
                    tblRegistration.Columns.Add("txtInsuranceTelephone2");
                    tblRegistration.Columns.Add("txtProgramorpolicy2");
                    tblRegistration.Columns.Add("txtSocialSecurity2");
                    tblRegistration.Columns.Add("txtUnionLocal2");
                    tblRegistration.Columns.Add("txtDigiSign");

                    StringBuilder chkMethodOfPayment = new StringBuilder();

                    if (chkCash == "1")
                    {
                        if (!String.IsNullOrEmpty(chkMethodOfPayment.ToString()))
                        {
                            chkMethodOfPayment.Append("," + "Cash");
                        }
                        else
                        {
                            chkMethodOfPayment.Append("Cash");
                        }
                    }
                    if (chkInsurance == "1")
                    {
                        if (!String.IsNullOrEmpty(chkMethodOfPayment.ToString()))
                        {
                            chkMethodOfPayment.Append("," + "Insurance");
                        }
                        else
                        {
                            chkMethodOfPayment.Append("Insurance");
                        }
                    }
                    if (chkCrediteCard == "1")
                    {
                        if (!String.IsNullOrEmpty(chkMethodOfPayment.ToString()))
                        {
                            chkMethodOfPayment.Append("," + "Credit Card");
                        }
                        else
                        {
                            chkMethodOfPayment.Append("Credit Card");
                        }
                    }

                    tblRegistration.Columns.Add("CreatedDate");
                    tblRegistration.Columns.Add("ModifiedDate");

                    tblRegistration.Columns.Add("txtemailaddress");


                    tblRegistration.Rows.Add(ResponsiblepartyFname, ResponsiblepartyLname, ResponsibleRelationship, ResponsibleAddress, ResponsibleCity, ResponsibleState,
                ResponsibleZipCode, ResponsibleDOB, ResponsibleContact, "", "", "", "", "", chkMethodOfPayment.ToString(), "", "", Whommay, "", EmergencyContactPhoneNumber, EmergencyContactName, PrimaryInsuranceCompany, PrimaryInsurancePhone,
                PrimaryDateOfBirth, PrimaryNameOfInsured, "", PrimaryMemberID, "", PrimaryGroupNumber, "", "", "", SecondaryInsuranceCompany, SecondaryInsurancePhone, "",
                      SecondaryNameOfInsured, SecondaryDateOfBirth, SecondaryMemberID, "", SecondaryGroupNumber, "", "", "", "", "", "", emailaddress);




                    DataSet dsRegistration = new DataSet("Registration");
                    dsRegistration.Tables.Add(tblRegistration);

                    DataSet ds = dsRegistration.Copy();
                    dsRegistration.Clear();

                    string Registration = ds.GetXml();

                    bool status1 = ObjPatientsData.UpdateRegistrationForm(Convert.ToInt32(PatientId), "UpdatePatient", Convert.ToString(Dob), ddlgenderreg,
                        drpStatus, ResidenceStreet, City, State, Zip,
                         ResidenceTelephone, WorkPhone, Fax, Registration);
                }

                obj = "1";
            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult MedicalHistoryForm(string PatientId, MediaclHisotry objHistory)
        {
            object obj = string.Empty;
            if (!string.IsNullOrEmpty(PatientId))
            {
                try
                {
                    // Dental History Table
                    DataTable tblMedicalHistory = new DataTable("MedicalHistory");
                    tblMedicalHistory.Columns.Add("MrdQue1");
                    tblMedicalHistory.Columns.Add("MrnQue1");
                    tblMedicalHistory.Columns.Add("Mtxtphysicians");
                    tblMedicalHistory.Columns.Add("MrdQue2");
                    tblMedicalHistory.Columns.Add("MrnQue2");
                    tblMedicalHistory.Columns.Add("Mtxthospitalized");
                    tblMedicalHistory.Columns.Add("MrdQue3");
                    tblMedicalHistory.Columns.Add("MrnQue3");
                    tblMedicalHistory.Columns.Add("Mtxtserious");
                    tblMedicalHistory.Columns.Add("MrdQue4");
                    tblMedicalHistory.Columns.Add("MrnQue4");
                    tblMedicalHistory.Columns.Add("Mtxtmedications");
                    tblMedicalHistory.Columns.Add("MrdQue5");
                    tblMedicalHistory.Columns.Add("MrnQue5");
                    tblMedicalHistory.Columns.Add("MtxtRedux");
                    tblMedicalHistory.Columns.Add("MrdQue6");
                    tblMedicalHistory.Columns.Add("MrnQue6");
                    tblMedicalHistory.Columns.Add("MtxtFosamax");
                    tblMedicalHistory.Columns.Add("MrdQuediet7");
                    tblMedicalHistory.Columns.Add("MrnQuediet7");
                    tblMedicalHistory.Columns.Add("Mtxt7");
                    tblMedicalHistory.Columns.Add("Mrdotobacco8");
                    tblMedicalHistory.Columns.Add("Mrnotobacco8");
                    tblMedicalHistory.Columns.Add("Mtxt8");
                    tblMedicalHistory.Columns.Add("Mrdosubstances");
                    tblMedicalHistory.Columns.Add("Mrnosubstances");
                    tblMedicalHistory.Columns.Add("Mtxt9");

                    tblMedicalHistory.Columns.Add("Mrdopregnant");
                    tblMedicalHistory.Columns.Add("Mrnopregnant");
                    tblMedicalHistory.Columns.Add("Mtxt10");
                    tblMedicalHistory.Columns.Add("Mrdocontraceptives");
                    tblMedicalHistory.Columns.Add("Mrnocontraceptives");
                    tblMedicalHistory.Columns.Add("Mtxt11");

                    tblMedicalHistory.Columns.Add("MrdoNursing");
                    tblMedicalHistory.Columns.Add("MrnoNursing");
                    tblMedicalHistory.Columns.Add("Mtxt12");
                    tblMedicalHistory.Columns.Add("Mrdotonsils");
                    tblMedicalHistory.Columns.Add("Mrnotonsils");
                    tblMedicalHistory.Columns.Add("Mtxt13");

                    tblMedicalHistory.Columns.Add("MchkQue_1");
                    tblMedicalHistory.Columns.Add("MchkQueN_1");
                    tblMedicalHistory.Columns.Add("MchkQue_2");
                    tblMedicalHistory.Columns.Add("MchkQueN_2");
                    tblMedicalHistory.Columns.Add("MchkQue_3");
                    tblMedicalHistory.Columns.Add("MchkQueN_3");
                    tblMedicalHistory.Columns.Add("MchkQue_4");
                    tblMedicalHistory.Columns.Add("MchkQueN_4");
                    tblMedicalHistory.Columns.Add("MchkQue_5");
                    tblMedicalHistory.Columns.Add("MchkQueN_5");
                    tblMedicalHistory.Columns.Add("MchkQue_6");
                    tblMedicalHistory.Columns.Add("MchkQueN_6");
                    tblMedicalHistory.Columns.Add("MchkQue_7");
                    tblMedicalHistory.Columns.Add("MchkQueN_7");
                    tblMedicalHistory.Columns.Add("MchkQue_8");
                    tblMedicalHistory.Columns.Add("MchkQueN_8");
                    tblMedicalHistory.Columns.Add("MchkQue_9");
                    tblMedicalHistory.Columns.Add("MchkQueN_9");
                    tblMedicalHistory.Columns.Add("MtxtchkQue_9");
                    tblMedicalHistory.Columns.Add("MrdQueAIDS_HIV_Positive");
                    tblMedicalHistory.Columns.Add("MrdQueAlzheimer");
                    tblMedicalHistory.Columns.Add("MrdQueAnaphylaxis");

                    tblMedicalHistory.Columns.Add("MrdQueAnemia");
                    tblMedicalHistory.Columns.Add("MrdQueAngina");
                    tblMedicalHistory.Columns.Add("MrdQueArthritis_Gout");

                    tblMedicalHistory.Columns.Add("MrdQueArtificialHeartValve");
                    tblMedicalHistory.Columns.Add("MrdQueArtificialJoint");
                    tblMedicalHistory.Columns.Add("MrdQueAsthma");

                    tblMedicalHistory.Columns.Add("MrdQueBloodDisease");
                    tblMedicalHistory.Columns.Add("MrdQueBloodTransfusion");
                    tblMedicalHistory.Columns.Add("MrdQueBoneDisorders");

                    tblMedicalHistory.Columns.Add("MrdQueBreathing");
                    tblMedicalHistory.Columns.Add("MrdQueBruise");
                    tblMedicalHistory.Columns.Add("MrdQueCancer");

                    tblMedicalHistory.Columns.Add("MrdQueChemicalDependancy");
                    tblMedicalHistory.Columns.Add("MrdQueChemotherapy");
                    tblMedicalHistory.Columns.Add("MrdQueChest");

                    tblMedicalHistory.Columns.Add("MrdQueCold_Sores_Fever");
                    tblMedicalHistory.Columns.Add("MrdQueCongenital");
                    tblMedicalHistory.Columns.Add("MrdQueConvulsions");

                    tblMedicalHistory.Columns.Add("MrdQueCortisone");
                    tblMedicalHistory.Columns.Add("MrdQueCortisoneTretments");
                    tblMedicalHistory.Columns.Add("MrdQueDiabetes");

                    tblMedicalHistory.Columns.Add("MrdQueDrug");
                    tblMedicalHistory.Columns.Add("MrdQueEasily");
                    tblMedicalHistory.Columns.Add("MrdQueEmphysema");
                    tblMedicalHistory.Columns.Add("MrdQueEndocrineProblems");
                    tblMedicalHistory.Columns.Add("MrdQueEpilepsy");
                    tblMedicalHistory.Columns.Add("MrdQueExcessiveBleeding");
                    tblMedicalHistory.Columns.Add("MrdQueExcessiveThirst");
                    tblMedicalHistory.Columns.Add("MrdQueExcessiveUrination");
                    tblMedicalHistory.Columns.Add("MrdQueFainting");
                    tblMedicalHistory.Columns.Add("MrdQueFrequentCough");
                    tblMedicalHistory.Columns.Add("MrdQueFrequentDiarrhea");
                    tblMedicalHistory.Columns.Add("MrdQueFrequentHeadaches");
                    tblMedicalHistory.Columns.Add("MrdQueGenital");
                    tblMedicalHistory.Columns.Add("MrdQueGlaucoma");
                    tblMedicalHistory.Columns.Add("MrdQueHay");
                    tblMedicalHistory.Columns.Add("MrdQueHeartAttack_Failure");
                    tblMedicalHistory.Columns.Add("MrdQueHeartMurmur");
                    tblMedicalHistory.Columns.Add("MrdQueHeartPacemaker");
                    tblMedicalHistory.Columns.Add("MrdQueHeartTrouble_Disease");
                    tblMedicalHistory.Columns.Add("MrdQueHemophilia");
                    tblMedicalHistory.Columns.Add("MrdQueHepatitisA");
                    tblMedicalHistory.Columns.Add("MrdQueHepatitisBorC");
                    tblMedicalHistory.Columns.Add("MrdQueHerpes");
                    tblMedicalHistory.Columns.Add("MrdQueHighBloodPressure");
                    tblMedicalHistory.Columns.Add("MrdQueHighCholesterol");
                    tblMedicalHistory.Columns.Add("MrdQueHives");
                    tblMedicalHistory.Columns.Add("MrdQueHypoglycemia");
                    tblMedicalHistory.Columns.Add("MrdQueIrregular");
                    tblMedicalHistory.Columns.Add("MrdQueKidney");
                    tblMedicalHistory.Columns.Add("MrdQueLeukemia");
                    tblMedicalHistory.Columns.Add("MrdQueLiver");
                    tblMedicalHistory.Columns.Add("MrdQueLow");
                    tblMedicalHistory.Columns.Add("MrdQueLung");
                    tblMedicalHistory.Columns.Add("MrdQueMitral");
                    tblMedicalHistory.Columns.Add("MrdQueNervousDisorder");
                    tblMedicalHistory.Columns.Add("MrdQueOsteoporosis");
                    tblMedicalHistory.Columns.Add("MrdQuePain");
                    tblMedicalHistory.Columns.Add("MrdQueParathyroid");
                    tblMedicalHistory.Columns.Add("MrdQuePsychiatric");
                    tblMedicalHistory.Columns.Add("MrdQueRadiation");
                    tblMedicalHistory.Columns.Add("MrdQueRecent");
                    tblMedicalHistory.Columns.Add("MrdQueRenal");
                    tblMedicalHistory.Columns.Add("MrdQueRheumatic");
                    tblMedicalHistory.Columns.Add("MrdQueRheumatism");
                    tblMedicalHistory.Columns.Add("MrdQueScarlet");
                    tblMedicalHistory.Columns.Add("MrdQueShingles");
                    tblMedicalHistory.Columns.Add("MrdQueSickle");
                    tblMedicalHistory.Columns.Add("MrdQueSinus");
                    tblMedicalHistory.Columns.Add("MrdQueSpina");
                    tblMedicalHistory.Columns.Add("MrdQueStomach");
                    tblMedicalHistory.Columns.Add("MrdQueStroke");
                    tblMedicalHistory.Columns.Add("MrdQueSwelling");
                    tblMedicalHistory.Columns.Add("MrdQueThyroid");
                    tblMedicalHistory.Columns.Add("MrdQueTonsillitis");
                    tblMedicalHistory.Columns.Add("MrdQueTuberculosis");
                    tblMedicalHistory.Columns.Add("MrdQueTumors");
                    tblMedicalHistory.Columns.Add("MrdQueUlcers");
                    tblMedicalHistory.Columns.Add("MrdQueVenereal");
                    tblMedicalHistory.Columns.Add("MrdQueYellow");
                    tblMedicalHistory.Columns.Add("Mtxtillness");
                    tblMedicalHistory.Columns.Add("MtxtComments");
                    tblMedicalHistory.Columns.Add("CreatedDate");
                    tblMedicalHistory.Columns.Add("ModifiedDate");
                    tblMedicalHistory.Columns.Add("MrdQueAbnormal");

                    tblMedicalHistory.Columns.Add("MrdQueThinners");
                    tblMedicalHistory.Columns.Add("MrdQueFibrillation");
                    tblMedicalHistory.Columns.Add("MrdQueCigarette");
                    tblMedicalHistory.Columns.Add("MrdQueCirulation");
                    tblMedicalHistory.Columns.Add("MrdQuePersistent");
                    tblMedicalHistory.Columns.Add("MrdQuefillers");
                    tblMedicalHistory.Columns.Add("MrdQueDigestive");
                    tblMedicalHistory.Columns.Add("MrdQueDizziness");
                    tblMedicalHistory.Columns.Add("MrdQueSubstances");
                    tblMedicalHistory.Columns.Add("MrdQueAlcoholic");
                    tblMedicalHistory.Columns.Add("MrdQueHeart_defect");
                    tblMedicalHistory.Columns.Add("MrdQueHiatal");
                    tblMedicalHistory.Columns.Add("MrdQueBlood_pressure");
                    tblMedicalHistory.Columns.Add("MrdQueHow_much");
                    tblMedicalHistory.Columns.Add("MrdQueHow_often");
                    tblMedicalHistory.Columns.Add("MrdQueReplacement");
                    tblMedicalHistory.Columns.Add("MrdQueNeck_BackProblem");
                    tblMedicalHistory.Columns.Add("MrdQuePainJaw_Joints");
                    tblMedicalHistory.Columns.Add("MrdQueEndocarditis");
                    tblMedicalHistory.Columns.Add("MrdQueShortness");
                    tblMedicalHistory.Columns.Add("MrdQueSjorgren");
                    tblMedicalHistory.Columns.Add("MrdQueSwollen");
                    tblMedicalHistory.Columns.Add("MrdQueValve");
                    tblMedicalHistory.Columns.Add("MrdQueVision");
                    tblMedicalHistory.Columns.Add("MrdQueUnexplained");
                    tblMedicalHistory.Columns.Add("MrdQueArthritis");

                    tblMedicalHistory.Rows.Add(objHistory.MrdQue1,
                        objHistory.MrnQue1,
                        objHistory.Mtxtphysicians,
                        objHistory.MrdQue2,
                        objHistory.MrnQue2,
                        objHistory.Mtxthospitalized,
                        objHistory.MrdQue3,
                        objHistory.MrnQue3,
                        objHistory.Mtxtserious,
                        objHistory.MrdQue4,
                        objHistory.MrnQue4,
                        objHistory.Mtxtmedications,
                        objHistory.MrdQue5,
                        objHistory.MrnQue5,
                        objHistory.MtxtRedux,
                        objHistory.MrdQue6,
                        objHistory.MrnQue6,
                        objHistory.MtxtFosamax,
                        objHistory.MrdQuediet7,
                        objHistory.MrnQuediet7,
                        objHistory.Mtxt7,
                        objHistory.Mrdotobacco8,
                        objHistory.Mrnotobacco8,
                        objHistory.Mtxt8,
                        objHistory.Mrdosubstances,
                        objHistory.Mrnosubstances,
                        objHistory.Mtxt9,
                        objHistory.Mrdopregnant,
                        objHistory.Mrnopregnant,
                        objHistory.Mtxt10,
                        objHistory.Mrdocontraceptives,
                        objHistory.Mrnocontraceptives,
                        objHistory.Mtxt11,
                        objHistory.MrdoNursing,
                        objHistory.MrnoNursing,
                        objHistory.Mtxt12,
                        objHistory.Mrdotonsils,
                        objHistory.Mrnotonsils,
                        objHistory.Mtxt13,
                        objHistory.MchkQue_1,
                        objHistory.MchkQueN_1,
                        objHistory.MchkQue_2,
                        objHistory.MchkQueN_2,
                        objHistory.MchkQue_3,
                        objHistory.MchkQueN_3,
                        objHistory.MchkQue_4,
                        objHistory.MchkQueN_4,
                        objHistory.MchkQue_5,
                        objHistory.MchkQueN_5,
                        objHistory.MchkQue_6,
                        objHistory.MchkQueN_6,
                        objHistory.MchkQue_7,
                        objHistory.MchkQueN_7,
                        objHistory.MchkQue_8,
                        objHistory.MchkQueN_8,
                        objHistory.MchkQue_9,
                        objHistory.MchkQueN_9,
                        objHistory.MtxtchkQue_9,
                        objHistory.MrdQueAIDS_HIV_Positive,
                        objHistory.MrdQueAlzheimer,
                        objHistory.MrdQueAnaphylaxis,
                        objHistory.MrdQueAnemia,
                        objHistory.MrdQueAngina,
                        objHistory.MrdQueArthritis_Gout,
                        objHistory.MrdQueArtificialHeartValve,
                        objHistory.MrdQueArtificialJoint,
                        objHistory.MrdQueAsthma,
                        objHistory.MrdQueBloodDisease,
                        objHistory.MrdQueBloodTransfusion,
                        objHistory.MrdQueBoneDisorder,
                        objHistory.MrdQueBreathing,
                        objHistory.MrdQueBruise,
                        objHistory.MrdQueCancer,
                        objHistory.MrdQueChemicalDepandancy,
                        objHistory.MrdQueChemotherapy,
                        objHistory.MrdQueChest,
                        objHistory.MrdQueCold_Sores_Fever,
                        objHistory.MrdQueCongenital,
                        objHistory.MrdQueConvulsions,
                        objHistory.MrdQueCortisone,
                        objHistory.MrdQueCortisoneTretments,
                        objHistory.MrdQueDiabetes,
                        objHistory.MrdQueDrug,
                        objHistory.MrdQueEasily,
                        objHistory.MrdQueEmphysema,
                        objHistory.MrdQueEndorcrineProblems,
                        objHistory.MrdQueEpilepsy,
                        objHistory.MrdQueExcessiveBleeding,
                        objHistory.MrdQueExcessiveThirst,
                        objHistory.MrdQueExcessiveUrination,
                        objHistory.MrdQueFainting,
                        objHistory.MrdQueFrequentCough,
                        objHistory.MrdQueFrequentDiarrhea,
                        objHistory.MrdQueFrequentHeadaches,
                        objHistory.MrdQueGenital,
                        objHistory.MrdQueGlaucoma,
                        objHistory.MrdQueHay,
                        objHistory.MrdQueHeartAttack_Failure,
                        objHistory.MrdQueHeartMurmur,
                        objHistory.MrdQueHeartPacemaker,
                        objHistory.MrdQueHeartTrouble_Disease,
                        objHistory.MrdQueHemophilia,
                        objHistory.MrdQueHepatitisA,
                        objHistory.MrdQueHepatitisBorC,
                        objHistory.MrdQueHerpes,
                        objHistory.MrdQueHighBloodPressure,
                        objHistory.MrdQueHighCholesterol,
                        objHistory.MrdQueHives,
                        objHistory.MrdQueHypoglycemia,
                        objHistory.MrdQueIrregular,
                        objHistory.MrdQueKidney,
                        objHistory.MrdQueLeukemia,
                        objHistory.MrdQueLiver,
                        objHistory.MrdQueLow,
                        objHistory.MrdQueLung,
                        objHistory.MrdQueMitral,
                        objHistory.MrdQueNervousDisorder,
                        objHistory.MrdQueOsteoporosis,
                        objHistory.MrdQuePain,
                        objHistory.MrdQueParathyroid,
                        objHistory.MrdQuePsychiatric,
                        objHistory.MrdQueRadiation,
                        objHistory.MrdQueRecent,
                        objHistory.MrdQueRenal,
                        objHistory.MrdQueRheumatic,
                        objHistory.MrdQueRheumatism,
                        objHistory.MrdQueScarlet,
                        objHistory.MrdQueShingles,
                        objHistory.MrdQueSickle,
                        objHistory.MrdQueSinus,
                        objHistory.MrdQueSpina,
                        objHistory.MrdQueStomach,
                        objHistory.MrdQueStroke,
                        objHistory.MrdQueSwelling,
                        objHistory.MrdQueThyroid,
                        objHistory.MrdQueTonsillitis,
                        objHistory.MrdQueTuberculosis,
                        objHistory.MrdQueTumors,
                        objHistory.MrdQueUlcers,
                        objHistory.MrdQueVenereal,
                        objHistory.MrdQueYellow,
                        objHistory.Mtxtillness,
                        objHistory.MtxtComments,
                                    objHistory.MrdQueAbnormal,
                                    objHistory.MrdQueThinners,
                                objHistory.MrdQueFibrillation,
                                objHistory.MrdQueCigarette,
                                objHistory.MrdQueCirulation,
                                objHistory.MrdQuePersistent,
                                objHistory.MrdQuefillers,
                                objHistory.MrdQueDigestive,
                                objHistory.MrdQueDizziness,
                                objHistory.MrdQueSubstances,
                                objHistory.MrdQueAlcoholic,
                                objHistory.MrdQueHeart_defect,
                                objHistory.MrdQueHiatal,
                                objHistory.MrdQueBlood_pressure,
                                objHistory.MrdQueHow_much,
                                objHistory.MrdQueHow_often,
                                objHistory.MrdQueReplacement,
                                objHistory.MrdQueNeck_BackProblem,
                                objHistory.MrdQuePainJaw_Joints,
                                objHistory.MrdQueEndocarditis,
                                objHistory.MrdQueShortness,
                                objHistory.MrdQueSjorgren,
                                objHistory.MrdQueSwollen,
                                objHistory.MrdQueValve,
                                objHistory.MrdQueVision,
                                objHistory.MrdQueUnexplained,
                                objHistory.MrdQueArthritis);


                    DataSet dsMedicalHistory = new DataSet("MedicalHistory");
                    dsMedicalHistory.Tables.Add(tblMedicalHistory);

                    DataSet ds = dsMedicalHistory.Copy();
                    dsMedicalHistory.Clear();

                    string MedicalHistory = ds.GetXml();

                    bool status = ObjPatientsData.UpdateMedicalHistory(Convert.ToInt32(PatientId), "UpdateMedicalHistory", MedicalHistory);

                    obj = "1";
                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }

            return Json(obj, JsonRequestBehavior.AllowGet);

        }







        [HttpPost]
        public JsonResult DentalHistoryForm(string PatientId, string txtQue1, string txtQue2, string txtQue3, string txtQue4, string txtQue5, string txtQue5a, string txtQue5b, string txtQue5c, string txtQue6, string txtQue7, string rdQue7a, string rdQue8, string txtQue9a, string rdQue9, string rdQue10, string rdoQue11aFixedbridge, string rdoQue11bRemoveablebridge, string rdoQue11cDenture, string rdQue11dImplant, string txtQue12, string rdQue15, string rdQue16, string rdQue17, string rdQue18, string rdQue19, string chkQue20_1, string chkQue20_2, string chkQue20_3, string chkQue20_4, string rdQue21, string txtQue21a, string txtQue22, string txtQue23, string rdQue24, string txtQue26, string rdQue27, string rdQue28, string txtQue28a, string txtQue28b, string txtQue28c, string txtQue29, string txtQue29a, string rdQue30, string txtComments)
        {
            object obj = string.Empty;
            if (!string.IsNullOrEmpty(PatientId))
            {
                try
                {
                    DataTable tblDentalHistory = new DataTable("DentalHistory");
                    tblDentalHistory.Columns.Add("txtQue1");
                    tblDentalHistory.Columns.Add("txtQue2");
                    tblDentalHistory.Columns.Add("txtQue3");
                    tblDentalHistory.Columns.Add("txtQue4");
                    tblDentalHistory.Columns.Add("txtQue5");
                    tblDentalHistory.Columns.Add("txtQue5a");
                    tblDentalHistory.Columns.Add("txtQue5b");
                    tblDentalHistory.Columns.Add("txtQue5c");
                    tblDentalHistory.Columns.Add("txtQue6");
                    tblDentalHistory.Columns.Add("txtQue7");
                    tblDentalHistory.Columns.Add("rdQue7a");
                    tblDentalHistory.Columns.Add("rdQue8");
                    tblDentalHistory.Columns.Add("rdQue9");
                    tblDentalHistory.Columns.Add("txtQue9a");
                    tblDentalHistory.Columns.Add("rdQue10");
                    tblDentalHistory.Columns.Add("rdoQue11aFixedbridge");
                    tblDentalHistory.Columns.Add("rdoQue11bRemoveablebridge");
                    tblDentalHistory.Columns.Add("rdoQue11cDenture");
                    tblDentalHistory.Columns.Add("rdQue11dImplant");
                    tblDentalHistory.Columns.Add("txtQue12");
                    tblDentalHistory.Columns.Add("rdQue15");
                    tblDentalHistory.Columns.Add("rdQue16");
                    tblDentalHistory.Columns.Add("rdQue17");
                    tblDentalHistory.Columns.Add("rdQue18");
                    tblDentalHistory.Columns.Add("rdQue19");
                    tblDentalHistory.Columns.Add("chkQue20");

                    StringBuilder chkQue20 = new StringBuilder();




                    if (chkQue20_1 == "Y")
                    {
                        if (!String.IsNullOrEmpty(chkQue20.ToString()))
                        {
                            chkQue20.Append("," + "1");
                        }
                        else
                        {
                            chkQue20.Append("1");
                        }
                    }
                    if (chkQue20_2 == "Y")
                    {
                        if (!String.IsNullOrEmpty(chkQue20.ToString()))
                        {
                            chkQue20.Append("," + "2");
                        }
                        else
                        {
                            chkQue20.Append("2");
                        }
                    }
                    if (chkQue20_3 == "Y")
                    {
                        if (!String.IsNullOrEmpty(chkQue20.ToString()))
                        {
                            chkQue20.Append("," + "3");
                        }
                        else
                        {
                            chkQue20.Append("3");
                        }
                    }
                    if (chkQue20_4 == "Y")
                    {
                        if (!String.IsNullOrEmpty(chkQue20.ToString()))
                        {
                            chkQue20.Append("," + "4");
                        }
                        else
                        {
                            chkQue20.Append("4");
                        }
                    }
                    tblDentalHistory.Columns.Add("rdQue21");
                    tblDentalHistory.Columns.Add("txtQue21a");
                    tblDentalHistory.Columns.Add("txtQue22");


                    tblDentalHistory.Columns.Add("txtQue23");
                    tblDentalHistory.Columns.Add("rdQue24");

                    tblDentalHistory.Columns.Add("txtQue26");
                    tblDentalHistory.Columns.Add("rdQue27");
                    tblDentalHistory.Columns.Add("rdQue28");
                    tblDentalHistory.Columns.Add("txtQue28a");
                    tblDentalHistory.Columns.Add("txtQue28b");
                    tblDentalHistory.Columns.Add("txtQue28c");
                    tblDentalHistory.Columns.Add("txtQue29");
                    tblDentalHistory.Columns.Add("txtQue29a");
                    tblDentalHistory.Columns.Add("rdQue30");

                    tblDentalHistory.Columns.Add("txtComments");
                    
                    tblDentalHistory.Rows.Add(txtQue1, txtQue2, txtQue3, txtQue4, txtQue5, txtQue5a, txtQue5b, txtQue5c,
                        txtQue6, txtQue7, rdQue7a, rdQue8, rdQue9, txtQue9a, rdQue10,
                        rdoQue11aFixedbridge, rdoQue11bRemoveablebridge, rdoQue11cDenture,
                        rdQue11dImplant, txtQue12, rdQue15, rdQue16, rdQue17, rdQue18,
                        rdQue19, chkQue20.ToString(), rdQue21, txtQue21a, txtQue22,
                        txtQue23, rdQue24, txtQue26, rdQue27, rdQue28,
                        txtQue28a, txtQue28b, txtQue28c, txtQue29, txtQue29a, rdQue30, txtComments);


                    //Join Two Table                                                                                                                            
                    DataSet dsDentalHistory = new DataSet("DentalHistory");
                    dsDentalHistory.Tables.Add(tblDentalHistory);

                    DataSet ds = dsDentalHistory.Copy();
                    dsDentalHistory.Clear();

                    string DentalHistory = ds.GetXml();

                    bool status = ObjPatientsData.UpdateDentalHistory(Convert.ToInt32(PatientId), "UpdateDentalHistory", DentalHistory);
                    obj = "1";
                }
                catch (Exception ex)
                {
                    throw ex;
                }


            }
            return Json(obj, JsonRequestBehavior.AllowGet);

        }






    }
}
