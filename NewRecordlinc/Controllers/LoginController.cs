﻿using NewRecordlinc.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewRecordlinc.Models.User;
using System.Data;
using DataAccessLayer.ColleaguesData;
using BO.ViewModel;
using DataAccessLayer.Common;
using BusinessLogicLayer;
using System.Configuration;

namespace NewRecordlinc.Controllers
{
    [RoutePrefix("SignIn")]
    public class LoginController : Controller
    {
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        clsCommon objCommon = new clsCommon();
        // GET: Login
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        public ActionResult Index()
        {
            TempData.Peek("InActiveMsg");
            TempData.Peek("IsValid");
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                return RedirectToAction("Index", "User");
            }
            else
            {
                TempData["NewLogin"] = "Newlogin";
                if (Convert.ToString(TempData["Pass"]) == "Pass")
                {
                    ViewBag.IsValid = "Invalid";
                    ViewBag.UserName = TempData["UserName"];
                }
                else if (Convert.ToString(TempData["Pass1"]) == "pass")
                {
                    ViewBag.IsValid = "InActive";
                    ViewBag.UserName = TempData["UserName"];
                    ViewBag.InActiveMsg = "Your account was de-activated due to inactivity. We have emailed you a link to re-activate your account. Please check your email and if you did not receive our email within a few minutes please check your spam folder.";
                }
                else if (Convert.ToString(TempData["IsValid"]) == "InActive")
                {
                    ViewBag.IsValid = "InActive";
                }
                else
                {
                    ViewBag.IsValid = "Valid";
                }
                if(!string.IsNullOrWhiteSpace(Convert.ToString(TempData["RedirectURLForOCR"])))
                {
                    TempData.Keep();
                }
                return View("NewIndex");
            }

        }
        public ActionResult Logout()
        {
            Session.RemoveAll();

            return RedirectToAction("Index", "Login");
        }

        public ActionResult ForgetPassword()
        {
            mdlForgotPassword MdlForgotPassword = new mdlForgotPassword();
            if (Request["hdnemail"] != null && Request["hdnemail"] != "")
            {
                string strForgetEmail = Request["hdnemail"].ToString();
                DataTable dt = ObjColleaguesData.GetDoctorDetailsByUsername(strForgetEmail);
                if (dt != null && dt.Rows.Count > 0)
                {
                    MdlForgotPassword.Email = Convert.ToString(dt.Rows[0]["Username"]);
                }
                else
                {
                    MdlForgotPassword.Email = strForgetEmail;
                }
               
            }
            return PartialView("PartialForgotPassword", MdlForgotPassword);
            //else
            //{
            //    return RedirectToAction("Index");
            //}
        }
        public ActionResult Hook()
        {
            string Email = string.Empty;
            string Password = string.Empty;
            string DoctorId = string.Empty;
            string PatientId = string.Empty;
            int ISFROM = 0;
            PatientLogin obj = new PatientLogin();
            if (!string.IsNullOrWhiteSpace(Request["Encstr"]))
            {

                string[] splitwisedata = null;
                string checkspce = Request["Encstr"].ToString().Replace(" ", "+");
                string Decryptstring = ObjTripleDESCryptoHelper.decryptText(checkspce);
                splitwisedata = Decryptstring.Split(new string[] { "||" }, StringSplitOptions.None);
                Email = splitwisedata[0];
                Password = splitwisedata[1];
                DoctorId = splitwisedata[2];
                PatientId = splitwisedata[3];
                ISFROM = Convert.ToInt32(splitwisedata[4]);
            }
            new clsCompany().GetActiveCompany();
            if (!string.IsNullOrWhiteSpace(Email) && !string.IsNullOrWhiteSpace(Password))
            {
                obj.Email = Email;
                obj.Password = Password;
                IDictionary<bool, string> response = new LoginBLL().LoginonRecordlinc(obj);
                if (response.Keys.FirstOrDefault())
                {
                    DataTable dtCheck = ObjColleaguesData.CheckEmailInSystem(Email);
                    int IsInSystem = Convert.ToInt32(dtCheck.Rows[0]["IsInSystem"]);
                    if (IsInSystem == 1)
                    {
                        TripleDESCryptoHelper cryptoHelper = new TripleDESCryptoHelper();
                        string encryptedPassword = cryptoHelper.decryptText(Password);
                        DataTable dtLogin = ObjColleaguesData.LoginUserBasedOnUsernameandPassword(Email, encryptedPassword);
                        #region Login As Doctor
                        if (dtLogin != null && dtLogin.Rows.Count > 0)
                        {
                            int valid = Convert.ToInt32(dtLogin.Rows[0]["UserId"]);
                            SessionManagement.UserId = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["UserId"]), string.Empty);
                            SessionManagement.ParentUserId = (!string.IsNullOrEmpty(Convert.ToString(dtLogin.Rows[0]["ParentUserId"])) ? Convert.ToString(dtLogin.Rows[0]["ParentUserId"]) : Convert.ToString(Session["UserId"]));
                            SessionManagement.LocationId = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["LocationId"]), string.Empty);
                            SessionManagement.FirstName = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["FirstName"]), string.Empty);
                            string ImageName = "";
                            ImageName = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["ImageName"]), string.Empty);
                            if (string.IsNullOrEmpty(ImageName))
                            {
                                if (!System.IO.File.Exists(ImageName))
                                {
                                    SessionManagement.DoctorImage = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty);
                                }
                                else
                                {
                                    SessionManagement.DoctorImage = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + ImageName;
                                }
                            }
                            else
                            {
                                SessionManagement.DoctorImage = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + ImageName;
                            }
                            Session["DoctorImage"] = SessionManagement.DoctorImage;

                        }
                        switch (ISFROM)
                        {
                            case (int)BO.Enums.Common.RedirectFrom.PatientHistory:
                                return RedirectToAction("PatientHistory", "Patients", new { PatientId = PatientId });

                        }
                        #endregion
                    }
                }
            }
            return Redirect("/Error");
        }
    }
}