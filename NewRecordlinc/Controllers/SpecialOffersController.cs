﻿using BO.Models;
using BusinessLogicLayer;
using DataAccessLayer;
using NewRecordlinc.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NewRecordlinc.Controllers
{
    public class SpecialOffersController : Controller
    {
        SpecialOffersBLL objOfferBLL = new SpecialOffersBLL();
        // GET: SpecialOffers
        public ActionResult Index()
        {
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                if (!string.IsNullOrWhiteSpace(Convert.ToString(TempData["errorMessage"])))
                {
                    ViewBag.Message = Convert.ToString(TempData["errorMessage"]);
                }
                SpecialOffers model = new SpecialOffers();
                model.lstOffers = objOfferBLL.GetSpecialOfferByUserId(Convert.ToInt32(SessionManagement.UserId));
                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        public ActionResult AddSpecialOffer()
        {
            SpecialOffers model = new SpecialOffers();
            model.OfferId = 0;
            model.Status = true;
            return PartialView("_AddSpecialOffers", model);
        }

        public ActionResult InsertUpdateSpecialOffer(SpecialOffers model)
        {
            if (ModelState.IsValid)
            {
                bool Result = false;
                int OfferExist;
                object obj = "";
                model.UserId = Convert.ToInt32(SessionManagement.UserId);
                OfferExist = objOfferBLL.CheckSpecialOfferById(model);
                if (OfferExist == 1)
                {
                    TempData["errorMessage"] = "Special offer already exits.";
                }
                else
                {
                    Result = objOfferBLL.InsertUpdateSpecialOffers(model);
                    if (Result == true)
                    {
                        if (model.OfferId == 0)
                        {
                            TempData["errorMessage"] = "Special offer inserted successfully";
                        }
                        else
                        {
                            TempData["errorMessage"] = "Special offer updated successfully";
                        }
                    }
                    else
                    {
                        if (model.OfferId == 0)
                        {
                            TempData["errorMessage"] = "Failed to add special offer";
                        }
                        else
                        {
                            TempData["errorMessage"] = "Failed to update special offer";
                        }
                    }
                }
            }

            return RedirectToAction("Index");
        }

        public ActionResult EditSpecialOffer(int OfferId)
        {
            SpecialOffers model = new SpecialOffers();
            model = objOfferBLL.GetSpecialOffersById(OfferId);
            return PartialView("_AddSpecialOffers", model);
        }

        public JsonResult DeleteSpecialOffer(int OfferId)
        {
            bool Result = false;
            Result = objOfferBLL.RemoveSpecialOffer(OfferId);
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
    }
}