﻿
using BusinessLogicLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using System.Text;
using System.Web.Mvc;
using static BO.Enums.Common;

namespace NewRecordlinc.Controllers
{
    public class WebHookController : Controller
    {
        /// <summary>
        /// Get the notification from the webhook and insert into the Database
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Index()
        {
            RecurringBLL RL = new RecurringBLL();
            DataTable dt = new DataTable();
            HttpContext.Response.ContentType = "application/json";
            HttpContext.Response.ContentEncoding = Encoding.UTF8;
            System.IO.Stream body = HttpContext.Request.InputStream;
            System.Text.Encoding encoding = HttpContext.Request.ContentEncoding;
            System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);

            // For QA and Live server 
            var signatureKey = Request.Headers;
            var X_Anet_Signature = Request.Headers["X-ANET-Signature"];
            if (X_Anet_Signature != null)
            {
                var AnetSignature = X_Anet_Signature.Split('=')[1];
                string strreader = reader.ReadToEnd();
                String AddData = RL.InsertRecurringData(strreader, AnetSignature);
            }

            // For Local Testing
            //var strreader = EventData();
            //String AddData = RL.InsertRecurringData(strreader[0].Item1, null);

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        public List<Tuple<string>> EventData()
        {
            var results = new List<Tuple<string>>();

            //  // Subscription Event

            //results.Add(new Tuple<string>(
            //    "{\"notificationId\":\"2e37d897-6975-42bc-82eb-e4c76dd0b85f\"," +
            //    "\"eventType\":\"net.authorize.customer.subscription.updated\","
            //    + "\"eventDate\":\"2018-04-30T10:57:45.5023789Z\",\"webhookId\":\"5745b00d-85e8-4aae-8435-c94b929db152\","
            //    + "\"payload\":{\"entityName\":\"subscription\",\"id\":\"5145398\",\"name\":\"\"," +
            //    "\"amount\": 15 ,\"status\" : \"active\" , \"profile\" : {\"customerProfileId\":1504283677,"
            //    + "\"customerPaymentProfileId\":1503596747 }}}"
            //));


            //  // Customer Event
            //  results.Add(new Tuple<string>(
            //     "{\"notificationId\":\"5c3f7e00-1265-4e8e-abd0-a7d734163881\"," +
            //     "\"eventType\":\"net.authorize.customer.deleted\"," +
            //     "\"eventDate\":\"2016-03-23T05:23:06.5430555Z\"," +
            //     "\"webhookId\":\"0b90f2e8-02ae-4d1d-b2e0-1bd167e60176\"," +
            //     "\"payload\":{\"paymentProfiles\" : [{\"id\" : \"694\" , \"customerType\" : \"individual\" }]," +
            //     "\"merchantCustomerId\":\"cust457\",\"description\":\"Profile created by Subscription: 1447\"" +
            //     ",\"entityName\":\"customerProfile\",\"id\":\"1504283677\"}}"
            //));

            // Payment Event
            results.Add(new Tuple<string>(
               "{\"notificationId\":\"d0e8e7fe-c3e7-4add-a480-27bc5ce28a18\"," +
               "\"eventType\":\"net.authorize.payment.void.created\","
               + "\"eventDate\":\"2017-03-29T20:48:02.0080095Z\"," +
               "\"webhookId\":\"63d6fea2-aa13-4b1d-a204-f5fbc15942b7\","
               + "\"payload\":{\"responseCode\":1,\"authCode\":\"LZ6I19\",\"avsResponse\":\"Y\"," +
               "\"authAmount\": 45.00,\"entityName\":\"transaction\",\"id\":\"60102621895\"}}"
           ));

            //  // Payment Profile Event
            //  results.Add(new Tuple<string>(
            //     "{\"notificationId\":\"7201C905-B01E-4622-B807-AC2B646A3815\"," +
            //     "\"eventType\":\"net.authorize.customer.paymentProfile.created\","
            //     + "\"eventDate\":\"2016-03-23T06:19:09.5297562Z\"," +
            //     "\"webhookId\":\"6239A0BE-D8F4-4A33-8FAD-901C02EED51F\","
            //     + "\"payload\":{\"customerProfileId\":394,\"entityName\":\"customerPaymentProfile\"," +
            //     "\"id\":\"694\",\"customerType\" : \"business\"}}"
            // ));


            //  // Fraud Event
            //  results.Add(new Tuple<string>(
            //     "{\"notificationId\":\"26024a6c-3b78-4d18-8ce7-53dd020aee72\"," +
            //     "\"eventType\":\"net.authorize.payment.fraud.held\","
            //     + "\"eventDate\":\"2016-10-24T17:47:39.7740424Z\"," +
            //     "\"webhookId\":\"71400fce-085f-46fe-9758-8311ca01d33e\","
            //     + "\"payload\":{\"responseCode\":4,\"authCode\":\"24904A\",\"avsResponse\":\"Y\"," +
            //     "\"authAmount\": 50000.0,\"fraudList\" : [ { \"fraudFilter\" : \"AmountFilter\"," +
            //     " \"fraudAction\" : \"authAndHold\" } ], \"entityName\":\"transaction\",\"id\":\"2154067719\"}}"
            // ));


            return results;
        }
    }
}