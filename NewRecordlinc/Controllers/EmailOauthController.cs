﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using BO.Models;
using BusinessLogicLayer;
using NewRecordlinc.App_Start;
using DotNetOpenAuth.OAuth2;
using DotNetOpenAuth.Messaging;
using MailBee;
using System.Collections.Specialized;

namespace NewRecordlinc.Controllers
{
    public class EmailOauthController : Controller
    {
        // GET: EmailOauth
        [CustomAuthorize]
        public ActionResult Index()
        {
            int userId = Convert.ToInt32(SessionManagement.UserId);
            List<EmailProviderDetails> lstEPD = EmailProviderBLL.GetAllEmailProviderDetails(userId);
            if (TempData["IsDeleted"] != null)
            {
                if ((bool)TempData["IsDeleted"])
                {
                    ViewBag.IsDeleted = true;    
                    ViewBag.DeleteMessage = (string)TempData["DeleteMessage"];                    
                }
                else
                {
                    ViewBag.IsDeleted = false;
                    ViewBag.DeleteMessage = (string)TempData["DeleteMessage"];                    
                }
            }            
            else
            {
                ViewBag.IsDeleted = null;
                ViewBag.DeleteMessage = null;
                TempData["IsDeleted"] = null;
                TempData["DeleteMessage"] = null;                
            }
            if (TempData["OAuthDone"] != null)
            {
                if ((bool)TempData["OAuthDone"])
                {
                    ViewBag.IsAuthMsg = true;
                    ViewBag.OauthMsg = (string)TempData["OauthMsg"];
                }
                else
                {
                    ViewBag.IsAuthMsg = false;
                    ViewBag.OauthMsg = (string)TempData["OauthMsg"];
                }
            }
            else
            {
                ViewBag.IsAuthMsg = null;
                ViewBag.OauthMsg = null;
                TempData["OAuthDone"] = null;
                TempData["OauthMsg"] = null;
            }
            if (TempData["IsUpdated"] != null)
            {
                if ((bool)TempData["IsUpdated"])
                {
                    ViewBag.IsUpdated = true;
                    ViewBag.UpdateMessage = (string)TempData["UpdateMessage"];
                }
                else
                {
                    ViewBag.IsUpdated = false;
                    ViewBag.UpdateMessage = (string)TempData["UpdateMessage"];
                }
            }
            else
            {
                ViewBag.IsUpdated = null;
                ViewBag.UpdateMessage = null;
                TempData["IsUpdated"] = null;
                TempData["UpdateMessage"] = null;
            }
            return View("AccountList", lstEPD);
        }

        [CustomAuthorize]
        public ActionResult AddAccount()
        {             
            EmailProviderDetails epd = new EmailProviderDetails();
            epd.lstProvider = EmailProviderBLL.GetAllEmailProvider();
            if (TempData["IsInserted"] != null)
            {
                if ((bool)TempData["IsInserted"])
                {
                    ViewBag.IsInserted = true;
                    ViewBag.InsertMessage = (string)TempData["InsertMessage"];
                }
                else
                {
                    ViewBag.IsInserted = false;
                    ViewBag.InsertMessage = (string)TempData["InsertMessage"];
                }                
            }
            else
            {
                ViewBag.IsInserted = null;
                ViewBag.InsertMessage = null;
                TempData["IsInserted"] = null;
                TempData["InsertMessage"] = null;
            }

            if (TempData["IsExists"] != null)
            {
                if ((bool)TempData["IsExists"])
                {
                    ViewBag.IsExists = true;
                    ViewBag.ExistsMessage = (string)TempData["ExistsMessage"];
                }
                else
                {
                    ViewBag.IsExists = false;
                    ViewBag.ExistsMessage = (string)TempData["ExistsMessage"];
                }
            }
            else
            {               
                ViewBag.IsExists = null;
                ViewBag.ExistsMessage = null;
                TempData["IsExists"] = null;
                TempData["ExistsMessage"] = null;                
            }

            epd.sendEnabled = true;
            epd.requireSSL = true;
            return View(epd);
        }

        [CustomAuthorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddAccount(EmailProviderDetails _emailProviderDetails)
        {
            bool IsInserted = false;
            var IsValid = EmailProviderBLL.CheckEmailAlreadyExists(_emailProviderDetails.emailAddress, _emailProviderDetails.providerId, SessionManagement.UserId);
            if (IsValid)
            {
                TempData["IsExists"] = true;
                TempData["ExistsMessage"] = "Same account information is already exists in the system. Please check details and try again.";
                return RedirectToAction("AddAccount");
            }
            IsInserted = EmailProviderBLL.InsertEmailProviderDetails(_emailProviderDetails);
            if (IsInserted)
            {
                TempData["IsInserted"] = true;
                TempData["InsertMessage"] = "Record added successfully.";
                return RedirectToAction("AddAccount");
            }
            else
            {
                return RedirectToAction("AddAccount", _emailProviderDetails);
            }
        }

        [CustomAuthorize]
        public ActionResult ConnectWithOAuth(string emailid, string typeid, string pdId = null)
        {
            if (!string.IsNullOrEmpty(pdId))
            {
                var IsDelete = EmailProviderBLL.DeleteEmailProviderDetailsById(Convert.ToInt32(pdId), Convert.ToInt32(SessionManagement.UserId));
            }
            Session["AuthEmail"] = emailid;
            if (typeid == Convert.ToString((int)BO.Enums.Common.EmailType.GmailOAuth2))
            {
                Session["providerId"] = (int)BO.Enums.Common.EmailType.GmailOAuth2;
                var response = BO.OAuthHelperClass.CallOAuthGmail(Models.Config.Configurations.gclientID, Models.Config.Configurations.gclientSecret, Models.Config.Configurations.gredirectUri);
                return new Models.Wrapper.WrapperHttpResponseMessageResult(response);
            }
            else if(typeid == Convert.ToString((int)BO.Enums.Common.EmailType.OutlookOAuth2))
            {
                Session["providerId"] = (int)BO.Enums.Common.EmailType.OutlookOAuth2;
                string url = BO.OAuthHelperClass.CallOAuthOutlook(Models.Config.Configurations.oclientID, Models.Config.Configurations.oclientSecret, Models.Config.Configurations.oredirectUri);                                
                return Redirect(url);
            }
            return View();                       
        }

        [CustomAuthorize]
        public ActionResult SetEmailDefault(string epdId)
        {
            try
            {
                bool IsUpdated = EmailProviderBLL.UpdateEmailProviderDetailsForDefault(epdId, SessionManagement.UserId);
                return RedirectToAction("Index", "EmailOauth");
            }
            catch (Exception ex)
            {
                throw;
            }            
        }

        [CustomAuthorize]
        public ActionResult EditAccount(string epdId)
        {
            try
            {
                EmailProviderDetails _epd = new EmailProviderDetails();
                _epd = EmailProviderBLL.GetAllEmailProviderDetailsById(epdId, SessionManagement.UserId);                
                _epd.lstProvider = EmailProviderBLL.GetAllEmailProvider();
                return View(_epd);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [CustomAuthorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditAccount(EmailProviderDetails _epd)
        {
            bool IsUpdated = EmailProviderBLL.UpdateEmailProviderDetails(_epd);
            if (IsUpdated)
            {
                TempData["IsUpdated"] = true;
                TempData["UpdateMessage"] = "Record updated successfully.";
                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("EditAccount", _epd.providerDetailsId);
            }            
        }

        [CustomAuthorize]
        public ActionResult DeleteAccount(string epdId)
        {
            bool IsDeleted = EmailProviderBLL.DeleteEmailProviderDetailsById(Convert.ToInt32(epdId), Convert.ToInt32(SessionManagement.UserId));
            if (IsDeleted)
            {
                TempData["IsDeleted"] = true;
                //Change for Message 
                //TempData["DeleteMessage"] = "Record #" + epdId + " is removed successfully.";
                TempData["DeleteMessage"] = "Record removed successfully.";
                return RedirectToAction("Index");
            }
            else
            {
                TempData["IsDeleted"] = false;
                TempData["DeleteMessage"] = "Record #" + epdId + " is not removed successfully. Please try again.";
                return RedirectToAction("Index");
            }            
        }
    }
}