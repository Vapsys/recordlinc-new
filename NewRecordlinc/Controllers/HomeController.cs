﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Net;
using BusinessLogicLayer;

namespace NewRecordlinc.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //SendReferralBLL.GetReferralForOneClick();
            //ViewBag.Message = "Welcome to ASP.NET MVC!";
            //string path = "C:\\recordlinc\\Files\\Mytest.txt";
            try
            {

                //System.IO.File.AppendAllText(path, "start");
                //var str = Request.InputStream;
                //// Find number of bytes in stream.
                //var strLen = Convert.ToInt32(str.Length);
                //// Create a byte array.
                //byte[] strArr = new byte[strLen];
                //// Read stream into byte array.
                //var strRead = str.Read(strArr, 0, strLen);
                //string strmContents = "";
                //for (int counter = 0; counter < strLen; counter++)
                //{
                //    strmContents = strmContents + strArr[counter].ToString();
                //}
                //string result = System.Text.Encoding.UTF8.GetString(strArr);
                //System.IO.File.AppendAllText(path, result);

                //System.IO.File.AppendAllText(path, "try - end");
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                //System.IO.File.AppendAllText(path,"Error Message: " +ex.Message+ Environment.NewLine+ "Stack Trace: "+ex.StackTrace+Environment.NewLine+ "catch - end");
                throw;
            }
        }
        public ActionResult Error()
        {
            if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.UrlReferrer)))
            {
                ViewBag.CurrentLink = Convert.ToString(Request.UrlReferrer);
            }
            return View();
        }

    }
}
