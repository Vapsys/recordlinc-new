﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using NewRecordlinc.Models;
using System.Configuration;
using NewRecordlinc.App_Start;
using DataAccessLayer.Common;
using DataAccessLayer.ColleaguesData;

namespace NewRecordlinc.Controllers
{
    public class IndexController : Controller
    {
        clsTemplate ObjTemplate = new clsTemplate();
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        public ActionResult Index(int PId = 0)
        {
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                TempData.Keep("PMSReferralError");
                TempData.Keep("PMSConfigError");
                //  if (SessionManagement.UserId != "36390" && SessionManagement.UserId != "55" && SessionManagement.UserId != "45366" && SessionManagement.UserId != "346" && SessionManagement.UserId != "45379" && SessionManagement.UserId != "42063")
                //  ObjTemplate.SigninNotification(Convert.ToInt32(Session["UserId"]));
                //DataTable dt = new DataTable();
                //dt = ObjColleaguesData.GetFirstLoginCount(Convert.ToInt32(SessionManagement.UserId));
                //if (dt.Rows.Count == 0)
                //{
                ////    ObjTemplate.Firstlimeloginsupportmail(Convert.ToInt32(SessionManagement.UserId));
                //}
                if (PId != 0)
                {
                    string isfrom = "mdf";
                    return RedirectToAction("PatientHistory", "Patients", new { PatientId = PId, Isfrom = isfrom });
                }

                if (Convert.ToBoolean(TempData["IsFrom"]))
                {
                    TempData.Keep();
                }
                //Logic that executes code when it comes from PMSController
                if (SessionManagement.PMSSendReferralDetails != null)
                {
                    return RedirectToAction("Referral", "Referrals", SessionManagement.PMSSendReferralDetails);
                }
                else
                {
                    return RedirectToAction("Dashboard", "Dashboard");
                }
                //return View(); 
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }
    }
}
