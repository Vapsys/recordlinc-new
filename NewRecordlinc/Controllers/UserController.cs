﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewRecordlinc.Models.User;
using NewRecordlinc.App_Start;
using DataAccessLayer.Common;
using DataAccessLayer.ColleaguesData;
using System.Configuration;
using System.Data;
using System.Web.Helpers;
using DataAccessLayer.FlipTop;
using System.Web.Script.Serialization;
using BO.Models;
using BusinessLogicLayer;
using BO.ViewModel;
using BO.Constatnt;
namespace NewRecordlinc.Controllers
{
    public class UserController : Controller
    {
        mdlForgotPassword MdlForgotPassword = null;
        clsCompany ObjCompany = new clsCompany();
        clsCommon objCommon = new clsCommon();
        clsTemplate ObjTemplate = new clsTemplate();
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        clsAdmin ObjAdmin = new clsAdmin();
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        CommonBLL objcommonBll = new CommonBLL();

        public ActionResult Index(string UserName = null, string Password = null, string PatId = null,string Status = null,string IsFrom = null)
        {
            TempData.Keep("PMSReferralError");
            TempData.Keep("PMSConfigError");
            string[] SplitCredential = null; string messageid = string.Empty; string Messagetypeid = string.Empty;
            if (Request["DTPMKC"] != null && Request["DTPMKC"] != "")
            {
                string CheckSPace = Request["DTPMKC"].ToString().Replace(" ", "+");
                string DecryptString = ObjTripleDESCryptoHelper.decryptText(CheckSPace);
                SplitCredential = DecryptString.Split(new string[] { "||" }, StringSplitOptions.None);
                UserName = SplitCredential[0];
                Password = SplitCredential[1];
                messageid = SplitCredential[2];
                Messagetypeid = SplitCredential[2];
            }
            if (TempData["EncriptedString"] != null)
            {
                ViewBag.EncriptesString = TempData["EncriptedString"].ToString();
            }
            TempData["IsFrom"] = false;
            if(IsFrom == BO.Constatnt.Common.IsFrom)
            {
                Session["iLuvDentistImage"] = "~/Content/images/ilogo.png";
                TempData["IsFrom"] = true;
            }
            //if (Request.QueryString["p"] != null)
            //{
            //    if (!string.IsNullOrWhiteSpace(Request.QueryString["p"]))
            //    {
            //        ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
            //        string CheckSPace = Request.QueryString["p"].ToString().Replace(' ', '+');
            //        string DecryptString = ObjTripleDESCryptoHelper.decryptText(CheckSPace);
            //        SplitCredential = DecryptString.Split(new string[] { "||" }, StringSplitOptions.None);
            //        UserName = SplitCredential[0];
            //        Password = SplitCredential[1];
            //        messageid = SplitCredential[2];
            //        Messagetypeid = SplitCredential[2];
            //    }
            //}
            ObjCompany.GetActiveCompany();
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                DataTable dtCheckFirst = new DataTable();

                dtCheckFirst = ObjColleaguesData.GetFirstLoginCount(Convert.ToInt32(SessionManagement.UserId));
                if (dtCheckFirst.Rows.Count == 0)
                {
                    bool Result = false;
                    Result = objCommon.InsertLogedInfo(Convert.ToInt32(SessionManagement.UserId));
                    return RedirectToAction("Index", "Index", new { newuser = Convert.ToInt32(SessionManagement.UserId) });
                }
                else
                {

                    if (SplitCredential != null)
                    {

                        return RedirectToAction("Index", "Inbox", new { messageid = messageid, mtypeid = Messagetypeid });
                    }
                    return RedirectToAction("Index", "Index");
                }

            }
            else if (!string.IsNullOrEmpty(UserName))
            {
                if (!string.IsNullOrEmpty(Password))
                {
                    Password = Password.Replace(" ", "+");
                    DataTable dtLogin = new DataTable();
                    DataTable dtFirstLogin = new DataTable();
                    DataTable dtCheckFirst = new DataTable();
                    int valid = 0;
                    DataTable dtCheck = ObjColleaguesData.CheckEmailInSystem(UserName);
                    if (dtCheck != null && dtCheck.Rows.Count > 0)
                    {
                        int IsInSystem = Convert.ToInt32(dtCheck.Rows[0]["IsInSystem"]);
                        if (IsInSystem == 1)
                        {
                            TripleDESCryptoHelper cryptoHelper = new TripleDESCryptoHelper();
                            //RM-283 Password is passed instead of encryptedPassword
                            string encryptedPassword = cryptoHelper.encryptText(Password);
                            string decryptedPatId = cryptoHelper.decryptText(PatId);
                            int PatientID = Convert.ToInt32(decryptedPatId);
                            dtLogin = ObjColleaguesData.LoginUserBasedOnUsernameandPassword(UserName,encryptedPassword);
                            #region Login As Doctor
                            if (dtLogin != null && dtLogin.Rows.Count > 0)
                            {
                                valid = Convert.ToInt32(dtLogin.Rows[0]["UserId"]);
                                SessionManagement.UserId = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["UserId"]), string.Empty);
                                SessionManagement.ParentUserId = (!string.IsNullOrEmpty(Convert.ToString(dtLogin.Rows[0]["ParentUserId"])) ? Convert.ToString(dtLogin.Rows[0]["ParentUserId"]) : Convert.ToString(Session["UserId"]));
                                SessionManagement.LocationId = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["LocationId"]), string.Empty);
                                SessionManagement.FirstName = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["FirstName"]), string.Empty);
                                SessionManagement.UserPaymentVerified = PaymentBLL.CheckUserPayment(Convert.ToInt32(SessionManagement.UserId));
                                SessionManagement.GetMemberPlanDetails = objcommonBll.GetUserPlanDetails(Convert.ToInt32(dtLogin.Rows[0]["UserId"]));
                                string ImageName = "";
                                ImageName = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["ImageName"]), string.Empty);
                                if (string.IsNullOrEmpty(ImageName))
                                {
                                    if (!System.IO.File.Exists(ImageName))
                                    {
                                        SessionManagement.DoctorImage = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty);
                                    }
                                    else
                                    {
                                        SessionManagement.DoctorImage = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + ImageName;
                                    }
                                }
                                else
                                {
                                    SessionManagement.DoctorImage = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + ImageName;
                                }
                                Session["DoctorImage"] = SessionManagement.DoctorImage;


                                if (!string.IsNullOrEmpty(Status))
                                {
                                    if (Status == "2")
                                    {
                                        bool result = ObjAdmin.UpdateUserStatus(Convert.ToInt32(dtLogin.Rows[0]["UserId"]), 1); // When user comes from link of DeActive 
                                    }

                                }


                                dtCheckFirst = ObjColleaguesData.GetFirstLoginCount(Convert.ToInt32(dtCheck.Rows[0]["UserId"]));
                                if (dtCheckFirst.Rows.Count == 0)
                                {
                                    bool Result = false;
                                    Result = objCommon.InsertLogedInfo(Convert.ToInt32(SessionManagement.UserId));
                                    //return RedirectToAction("Index", "Index", new { newuser = Convert.ToInt32(dtCheck.Rows[0]["UserId"]) });
                                    //RM-117 Redirection to dashboard while clicking on the Team member mail first time. 
                                    return RedirectToAction("Index", "Index");

                                }
                                else
                                {
                                    bool Result = false;
                                    Result = objCommon.InsertLogedInfo(Convert.ToInt32(SessionManagement.UserId));
                                    if (SplitCredential != null)
                                    {

                                        messageid = SplitCredential[2];
                                        Messagetypeid = SplitCredential[3];
                                        return RedirectToAction("Index", "Inbox", new { messageid = messageid, mtypeid = Messagetypeid });
                                    }
                                    return RedirectToAction("Index", "Index",new {PId = PatientID });
                                }
                            }


                            if (valid != 0)
                            {
                                return RedirectToAction("Index", "Index");
                            }
                            else
                            {
                                if (SplitCredential != null)
                                {
                                    ViewBag.Credentials = SplitCredential;
                                    ViewBag.UserName = SplitCredential[0];
                                    ViewBag.EncriptesString = Request["DTPMKC"].ToString();
                                    return View();
                                }
                                if (Convert.ToString(TempData["NewLogin"]) == "Newlogin")
                                {
                                    return RedirectToAction("Index", "Login");
                                }
                                else
                                {
                                    return RedirectToAction("Index", "User", new { valid = "Invalid" });
                                }
                            }

                            #endregion

                        }

                        else
                        {

                            dtFirstLogin = ObjAdmin.GetTempSignupByEmail(UserName);
                            if (dtFirstLogin != null && dtFirstLogin.Rows.Count > 0)
                            {
                                int NewUserId = 0;
                                dtCheckFirst = ObjColleaguesData.GetFirstLoginCount(Convert.ToInt32(dtFirstLogin.Rows[0]["id"]));
                                if (dtCheckFirst.Rows.Count == 0)
                                {
                                    DataTable transactioHistory = new DataTable();
                                    bool isPaidStatus = false;
                                    // Move data from temp table to main member table
                                    transactioHistory = ObjColleaguesData.GetTransactionHistoryByUserId(Convert.ToInt32(dtFirstLogin.Rows[0]["id"]));
                                    if (transactioHistory.Rows.Count > 0)
                                    {
                                        isPaidStatus = true;
                                    }
                                    string EncPassword = objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["password"]), string.Empty);

                                    string AccountKey = Crypto.SHA1(Convert.ToString(DateTime.Now.Ticks));
                                    NewUserId = ObjColleaguesData.SingUpDoctor(UserName, EncPassword, objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["first_name"]), string.Empty), objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["last_name"]), string.Empty), null, null, null, AccountKey, null, null, null, null, null, null, objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["phoneno"]), string.Empty), null, isPaidStatus, objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["facebook"]), string.Empty), objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["linkedin"]), string.Empty), objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["twitter"]), string.Empty), null, null, objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["description"]), string.Empty), null, null, null, null, null, null, null, 0, null, null, null,0);
                                    if (!string.IsNullOrEmpty(Convert.ToString(dtFirstLogin.Rows[0]["image_url"])))
                                    {
                                        string makefilename = "$" + System.DateTime.Now.Ticks + "$" + (objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["first_name"]), string.Empty)) + ".png";
                                        string rootPath = "~/DentistImages/";
                                        string fileName = System.IO.Path.Combine(Server.MapPath(rootPath), makefilename);
                                        System.Drawing.Image image = objCommon.DownloadImageFromUrl(objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["image_url"]), string.Empty));
                                        bool IsUploadImage = ObjColleaguesData.UpdateDoctorProfileImageById(Convert.ToInt32(NewUserId), makefilename);
                                        image.Save(fileName);

                                    }
                                    if (NewUserId != 0)
                                    {
                                        bool Result2 = ObjColleaguesData.UpdateSocialMediaDetailsByUserId(NewUserId, objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["facebook"]), string.Empty), objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["linkedin"]), string.Empty), objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["twitter"]), string.Empty),null, null, null, null, null, null);
                                    }

                                    if (isPaidStatus)
                                    {
                                        ObjColleaguesData.UpdateNewUserID(Convert.ToInt32(transactioHistory.Rows[0]["TransactionId"]), NewUserId, true);
                                    }
                                    // update temp table status here 

                                    bool result = ObjAdmin.UpdateStatusInTempTableUserById(Convert.ToInt32(dtFirstLogin.Rows[0]["id"]), 1);// status 1 for that we move records from temp table to our system
                                    dtLogin = new DataTable();
                                    dtLogin = ObjColleaguesData.LoginUserBasedOnUsernameandPassword(UserName, EncPassword);
                                    #region Login As Doctor
                                    if (dtLogin != null && dtLogin.Rows.Count > 0)
                                    {
                                        valid = Convert.ToInt32(dtLogin.Rows[0]["UserId"]);
                                        SessionManagement.UserId = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["UserId"]), string.Empty);
                                        SessionManagement.ParentUserId = (!string.IsNullOrEmpty(Convert.ToString(dtLogin.Rows[0]["ParentUserId"])) ? Convert.ToString(dtLogin.Rows[0]["ParentUserId"]) : Convert.ToString(Session["UserId"]));
                                        SessionManagement.LocationId = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["LocationId"]), string.Empty);
                                        SessionManagement.FirstName = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["FirstName"]), string.Empty);
                                        SessionManagement.GetMemberPlanDetails = objcommonBll.GetUserPlanDetails(Convert.ToInt32(dtLogin.Rows[0]["UserId"]));
                                        SessionManagement.UserPaymentVerified = PaymentBLL.CheckUserPayment(Convert.ToInt32(SessionManagement.UserId));
                                        List<MemberPlanDetail> obj = SessionManagement.GetMemberPlanDetails;
                                        ObjTemplate.Firstlimeloginsupportmail(Convert.ToInt32(SessionManagement.UserId));
                                        string ImageName = "";
                                        ImageName = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["ImageName"]), string.Empty);
                                        if (string.IsNullOrEmpty(ImageName))
                                        {
                                            SessionManagement.DoctorImage = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty);
                                        }
                                        else
                                        {
                                            SessionManagement.DoctorImage = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + ImageName;
                                        }
                                        Session["DoctorImage"] = SessionManagement.DoctorImage;
                                    }
                                    #endregion
                                }
                                dtCheckFirst = ObjColleaguesData.GetFirstLoginCount(Convert.ToInt32(dtCheck.Rows[0]["UserId"]));
                                if (dtCheckFirst.Rows.Count == 0)
                                {
                                    bool Result = false;
                                    Result = objCommon.InsertLogedInfo(Convert.ToInt32(SessionManagement.UserId));
                                    //return RedirectToAction("Index", "Inbox", new { newuser = Convert.ToInt32(dtCheck.Rows[0]["UserId"]) });
                                    return RedirectToAction("Index", "Index", new { newuser = Convert.ToInt32(dtCheck.Rows[0]["UserId"]) });

                                }
                                return RedirectToAction("Index", "Index", new { newuser = NewUserId });
                            }
                            else
                            {
                                return RedirectToAction("Index", "User", new { valid = IsInSystem });
                            }

                        }


                    }
                    if (Convert.ToString(TempData["NewLogin"]) == "Newlogin")
                    {

                        return RedirectToAction("Index", "Login");
                    }
                    else
                    {
                        return RedirectToAction("Index", "User", new { valid = "Invalid" });
                    }




                }
                return View("NewIndex");
            }
            else
            {


                return View("NewIndex");
            }
        }




        public ActionResult Login()
        {
            string RedirectURLForOCR = string.Empty;
            MdlForgotPassword = new mdlForgotPassword();
            bool isfalse = true;
            if (Request["hdnemail"] != null && Request["hdnemail"] != "")
            {
                string strForgetEmail = Request["hdnemail"].ToString();
                DataTable dt = ObjColleaguesData.GetDoctorDetailsByUsername(strForgetEmail);
                if (dt != null && dt.Rows.Count > 0)
                {
                    MdlForgotPassword.Email = Convert.ToString(dt.Rows[0]["Username"]);
                }
                else
                {
                    MdlForgotPassword.Email = strForgetEmail;
                }
                return PartialView("PartialForgotPassword", MdlForgotPassword);
            }
            if(Request["hdnRedirectURL"] != null && Request["hdnRedirectURL"] != "")
            {
                RedirectURLForOCR = Convert.ToString(Request["hdnRedirectURL"]);
            }
            //Here put Hardcoded UserId for Demo

            string UserName = string.Empty;
            string Password = string.Empty;
            if (Request["UserName"] != null && Request["UserName"] != "")
            {
                


                int valid = 0; int AttemptsCount = 0; int UserId = 0;
                UserName = Convert.ToString(Request["UserName"]).ToLower();
                TempData["UserName"] = UserName;
                if (Request["Password"] != null && Request["Password"] != "")
                {
                    Password = Convert.ToString(Request["Password"]);

                    Session["UserName"] = Request["UserName"].ToString();
                    Session["Password"] = Request["Password"].ToString();
                }
               
                if (Request["Count"] != null && Request["Count"] != "")
                {
                    AttemptsCount = Convert.ToInt32(Request["Count"]);
                }
                if (Request["userid"] != null && Request["userid"] != "")
                {
                    UserId = Convert.ToInt32(Request["userid"]);
                }

                if (!string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(Password))
                {
                    DataTable dtLogin = new DataTable();


                    TripleDESCryptoHelper cryptoHelper = new TripleDESCryptoHelper();
                    string encryptedPassword = cryptoHelper.encryptText(Password);

                    DataTable dtCheck = ObjColleaguesData.CheckEmailInSystem(UserName);

                    if (dtCheck != null && dtCheck.Rows.Count > 0)
                    {
                        if (Request["CheckBoxRemember"] != null)
                        {
                            Response.Cookies["UserCredential"].Value = UserName;
                            Response.Cookies["UserCredential"].Expires = DateTime.Now.AddMonths(1);
                        }
                        int UserStatus = Convert.ToInt32(dtCheck.Rows[0]["Status"]);
                        int IsInSystem = Convert.ToInt32(dtCheck.Rows[0]["IsInSystem"]);
                        if (IsInSystem == 1)
                        {
                            int ValidUserId = Convert.ToInt32(dtCheck.Rows[0]["UserId"]);
                            dtLogin = ObjColleaguesData.LoginUserBasedOnUsernameandPassword(UserName, encryptedPassword);

                            if (dtLogin != null && dtLogin.Rows.Count > 0)
                            {
                                SessionManagement.TimeZoneId = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["TimeZoneId"]), string.Empty);
                                SessionManagement.TimeZoneSystemName = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["TimeZoneSystemName"]), string.Empty);
                            }


                            if (UserName == "admin")
                            {
                                if (dtLogin != null && dtLogin.Rows.Count > 0)
                                {
                                    //Redirect to admin module
                                    SessionManagement.AdminUserId = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["UserId"]), string.Empty);
                                    string AdminImage = string.Empty;
                                    AdminImage = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["ImageName"]), string.Empty);
                                    if (string.IsNullOrEmpty(AdminImage))
                                    {
                                        Session["AdminImage"] = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty);
                                    }
                                    else
                                    {
                                        Session["AdminImage"] = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + AdminImage;
                                    }
                                    return RedirectToAction("Dashboard", "Admin");
                                }
                                else
                                {
                                    if (Convert.ToString(TempData["NewLogin"]) == "Newlogin")
                                    {
                                        TempData["Pass"] = "Pass";
                                        return RedirectToAction("Index", "Login");
                                    }
                                    else
                                    {
                                        return RedirectToAction("Index", "User", new { valid = "Invalid" });
                                    }
                                }
                            }
                            else
                            {
                                if (UserStatus == 1)
                                {
                                    if (AttemptsCount < 5)
                                    {
                                        #region Login As Doctor
                                        if (dtLogin != null && dtLogin.Rows.Count > 0)
                                        {
                                            valid = Convert.ToInt32(dtLogin.Rows[0]["UserId"]);
                                            SessionManagement.UserId = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["UserId"]), string.Empty);

                                            SessionManagement.FirstName = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["FirstName"]), string.Empty);
                                            if (!string.IsNullOrEmpty(dtLogin.Rows[0]["StayloggedMins"].ToString()))
                                            {
                                                Session.Timeout = Convert.ToInt32(dtLogin.Rows[0]["StayloggedMins"]);
                                            }


                                            string ImageName = "";
                                            ImageName = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["ImageName"]), string.Empty);
                                            if (string.IsNullOrEmpty(ImageName))
                                            {
                                                SessionManagement.DoctorImage = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty);
                                            }
                                            else
                                            {
                                                SessionManagement.DoctorImage = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + ImageName;
                                            }
                                            Session["DoctorImage"] = SessionManagement.DoctorImage;

                                            bool Result = false;
                                            Result = objCommon.InsertLogedInfo(Convert.ToInt32(dtLogin.Rows[0]["UserId"]));

                                            string[] SplitCredential = null; string messageid = string.Empty; string Messagetypeid = string.Empty;
                                            objCommon.InsertErrorLog("1", "1", "1");
                                            if (Request["DTPMKC"] != null && Request["DTPMKC"] != "")
                                            {
                                                string CheckSPace = Request["DTPMKC"].ToString().Replace(" ", "+");
                                                string DecryptString = ObjTripleDESCryptoHelper.decryptText(CheckSPace);
                                                SplitCredential = DecryptString.Split(new string[] { "||" }, StringSplitOptions.None);

                                                messageid = SplitCredential[2];
                                                Messagetypeid = SplitCredential[3];
                                                return RedirectToAction("Index", "Inbox", new { messageid = messageid, mtypeid = Messagetypeid });
                                            }
                                            SessionManagement.LocationId = objCommon.CheckNull(Convert.ToString(dtLogin.Rows[0]["LocationId"]), string.Empty);
                                            SessionManagement.ParentUserId = (!string.IsNullOrEmpty(Convert.ToString(dtLogin.Rows[0]["ParentUserId"])) ? Convert.ToString(dtLogin.Rows[0]["ParentUserId"]) : Convert.ToString(Session["UserId"]));
                                            SessionManagement.GetMemberPlanDetails = objcommonBll.GetUserPlanDetails(Convert.ToInt32(dtLogin.Rows[0]["UserId"]));
                                            SessionManagement.UserPaymentVerified = PaymentBLL.CheckUserPayment(Convert.ToInt32(SessionManagement.UserId));

                                            // Changes 
                                            int LocationId = Convert.ToInt32(SessionManagement.LocationId);
                                            string accountID = ObjColleaguesData.GetAccountID(LocationId);
                                            Session["Account_ID"] = accountID;

                                            List<MemberPlanDetail> obj=SessionManagement.GetMemberPlanDetails;
                                            if (!string.IsNullOrWhiteSpace(RedirectURLForOCR))
                                            {
                                                return Redirect(RedirectURLForOCR);
                                            }
                                            return RedirectToAction("Index", "Index");
                                        }


                                        if (valid != 0)
                                        {
                                            if (!string.IsNullOrWhiteSpace(RedirectURLForOCR))
                                            {
                                                return Redirect(RedirectURLForOCR);
                                            }
                                            return RedirectToAction("Index", "Index");
                                        }
                                        else
                                        {
                                            int CheckAttemps = 1;
                                            if (UserId == ValidUserId)
                                            {
                                                CheckAttemps = (AttemptsCount + 1);
                                            }
                                            if (Request["DTPMKC"] != null && Request["DTPMKC"] != "")
                                            {
                                                TempData["EncriptedString"] = Request["DTPMKC"].ToString();
                                            }
                                            objCommon.InsertErrorLog("1", "1", "1");
                                            if (Convert.ToString(TempData["NewLogin"]) == "Newlogin")
                                            {
                                                TempData["Pass"] = "Pass";
                                                return RedirectToAction("Index", "Login");
                                            }
                                            else
                                            {
                                                return RedirectToAction("Index", "User", new { valid = "Invalid", count = CheckAttemps, userid = ValidUserId });
                                            }

                                        }

                                        #endregion
                                    }
                                    else
                                    {

                                        // we need to code for Account lockout for invalid attempts
                                        ViewBag.IsValid = "InActive";
                                        ViewBag.InActiveMsg = "Your account has been blocked by Admin";
                                        bool result = ObjAdmin.UpdateUserStatus(ValidUserId, 3);
                                        return RedirectToAction("Index", "Login");
                                    }
                                }

                                if (UserStatus == 2)
                                {
                                    string companywebsite = "";
                                    if (Session["CompanyWebsite"] != null)
                                    {
                                        companywebsite = Session["CompanyWebsite"].ToString();
                                    }
                                    string FullName = objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["FirstName"]), string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["LastName"]), string.Empty);
                                    string EncPassword = objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["Password"]), string.Empty);
                                    ObjTemplate.NewUserEmailFormat(UserName, companywebsite + "/User/Index?UserName=" + UserName + "&Password=" + EncPassword + "&Status=2", ObjTripleDESCryptoHelper.decryptText(EncPassword), companywebsite);
                                    if (Convert.ToString(TempData["NewLogin"]) == "Newlogin")
                                    {
                                        TempData["Pass1"] = "pass";
                                        return RedirectToAction("Index", "Login");
                                    }
                                    return RedirectToAction("Index", "User", new { valid = "2" });
                                }
                                if (UserStatus == 3)
                                {
                                    TempData["IsValid"] = "InActive";
                                    TempData["InActiveMsg"] = "Your account has been blocked by Admin";
                                    return RedirectToAction("Index", "Login");
                                }

                            }


                        }



                        else
                        {
                            string companywebsite = "";
                            if (Session["CompanyWebsite"] != null)
                            {
                                companywebsite = Session["CompanyWebsite"].ToString();
                            }

                            ObjTemplate.NewUserEmailFormat(UserName, companywebsite + "/User/Index?UserName=" + UserName + "&Password=" + objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["Password"]), string.Empty), Password, companywebsite);
                            return RedirectToAction("Index", "User", new { valid = "Signup" });
                        }


                    }
                    if (Convert.ToString(TempData["NewLogin"]) == "Newlogin")
                    {
                        TempData["Pass"] = "Pass";
                        return RedirectToAction("Index", "Login");
                    }
                    else
                    {
                        return RedirectToAction("Index", "User", new { valid = "Invalid" });
                    }



                }
                return RedirectToAction("Index", "User", new { valid = "Invalid" });
            }
            else
            {
                return new EmptyResult();
            }
            #region Need to set valid userid



            #endregion
        }

        public ActionResult Logout()
        {
            Session.RemoveAll();

            return RedirectToAction("Index", "User");
        }

        public string CheckDoctorLoginNameIsExistsOrNot(string Username)
        {
            DataTable dtCheckEmail = new DataTable();
            dtCheckEmail = ObjColleaguesData.CheckEmailInSystem(Username);
            if (dtCheckEmail.Rows.Count > 0)
            {
                return "true";
            }
            return "false";
        }

        
        public ActionResult DoctorSingup()
        {
            try
            {
                if (Request["Email"] != null && Request["Email"] != "")
                {
                    bool status = objCommon.InsertErrorLog(Request.Url.ToString(), Request["Email"].ToString(), "");
                    string Email = string.Empty; string FirstName = string.Empty; string Phone = string.Empty;
                    string From = string.Empty; string LastName = string.Empty; string DentistType = string.Empty;
                    Email = Convert.ToString(Request["Email"]);
                    if (Request["FirstName"] != null && Request["FirstName"] != "" && Request["FirstName"] != "Name")
                    {
                        FirstName = Convert.ToString(Request["FirstName"]);
                    }
                    if (Request["LastName"] != null && Request["LastName"] != "" && Request["LastName"] != "")
                    {
                        LastName = Convert.ToString(Request["LastName"]);
                    }
                    if (Request["Phone"] != null && Request["Phone"] != "" && Request["Phone"] != "Phone")
                    {
                        Phone = Convert.ToString(Request["Phone"]);
                    }
                    if (Request["general_specialist"] != null && Request["general_specialist"] != "")
                    {
                        DentistType = Convert.ToString(Request["general_specialist"]);
                    }
                    if (Request["From"] != null && Request["From"] != "")
                    {
                        From = Convert.ToString(Request["From"]);
                    }
                    DataTable dtCheckEmail = new DataTable();
                    dtCheckEmail = ObjColleaguesData.CheckEmailInSystem(Email);


                    if (dtCheckEmail.Rows.Count > 0)
                    {
                        // send template for forgot password

                        DataTable dtCheck = ObjColleaguesData.CheckEmailExistsAsDoctor(Email);
                        if (dtCheck != null && dtCheck.Rows.Count > 0)
                        {
                            string FullName = string.Empty; string EncPassword = string.Empty;
                            int AllowLogin = Convert.ToInt32(dtCheckEmail.Rows[0]["Status"]);
                            if (AllowLogin == 1)
                            {
                                #region Send Password to user if already in system
                                // send password tempalte
                                FullName = objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["FirstName"]), string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["LastName"]), string.Empty);
                                EncPassword = objCommon.CheckNull(Convert.ToString(dtCheckEmail.Rows[0]["Password"]), string.Empty);
                                ObjTemplate.SendingForgetPassword(FullName, Email, EncPassword);

                                if (!string.IsNullOrEmpty(From))
                                {
                                    return RedirectToAction("Index", "Signuptoday", new { valid = "Signup" });
                                }
                                else
                                {
                                    return RedirectToAction("Index", "User", new { valid = "Signup" });
                                }
                                #endregion

                            }
                            else
                            {
                                if (AllowLogin == 2)//If Email is De Actvie by Admin then send this link as below
                                {
                                    string companywebsite = "";
                                    if (Session["CompanyWebsite"] != null)
                                    {
                                        companywebsite = Session["CompanyWebsite"].ToString();
                                        //companywebsite = "http://localhost:2298/";
                                    }
                                    FullName = objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["FirstName"]), string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["LastName"]), string.Empty);
                                    EncPassword = objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["Password"]), string.Empty);
                                    ObjTemplate.NewUserEmailFormat(Email, companywebsite + "/User/Index?UserName=" + Email + "&Password=" + EncPassword + "&Status=2", ObjTripleDESCryptoHelper.decryptText(EncPassword), companywebsite);

                                }

                                if (!string.IsNullOrEmpty(From))
                                {
                                    return RedirectToAction("Index", "Signuptoday", new { valid = AllowLogin });
                                }
                                else
                                {
                                    return RedirectToAction("Index", "User", new { valid = AllowLogin });
                                }

                            }

                        }

                        else
                        {
                            #region Re Send sign up tempalte to user of temp table
                            DataTable dttemp = new DataTable();
                            string EncPassword = string.Empty;
                            string companywebsite = "";
                            if (Session["CompanyWebsite"] != null)
                            {
                                companywebsite = Session["CompanyWebsite"].ToString();
                            }
                            dttemp = ObjAdmin.GetTempSignupByEmail(Email);
                            if (dttemp.Rows.Count > 0)
                            {
                                EncPassword = objCommon.CheckNull(Convert.ToString(dttemp.Rows[0]["password"]), string.Empty);
                                ObjTemplate.NewUserEmailFormat(Email, companywebsite + "/User/Index?UserName=" + Email + "&Password=" + EncPassword, ObjTripleDESCryptoHelper.decryptText(EncPassword), companywebsite); // send tempate to admin if doctor sing up in system
                            }
                            if (!string.IsNullOrEmpty(From))
                            {
                                return RedirectToAction("Index", "Signuptoday", new { valid = "Signup" });
                            }
                            else
                            {
                                return RedirectToAction("Index", "User", new { valid = "Signup" });
                            }
                            #endregion

                        }

                    }
                    else
                    {
                        #region Doctor sign up in system


                        string Password = objCommon.CreateRandomPassword(8);
                        string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                        string Accountkey = Crypto.SHA1(Convert.ToString(DateTime.Now.Ticks));
                        //Doctor sign up

                        Person person = GetSocialMediaPerson(Email); //Get Social Media detail


                        string SocialMedia = "";
                        string NameforTemplate = "";
                        string PhoneForTemplate = "";

                        string fullName = FirstName + ' ' + LastName;

                        string firstname = FirstName;
                        string lastname = LastName;
                        person.FirstName = firstname;
                        person.LastName = lastname;                        

                        if (!string.IsNullOrEmpty(Phone))
                        {
                            PhoneForTemplate = "Phone: " + Phone;
                        }

                        string companywebsite = "";
                        if (Session["CompanyWebsite"] != null)
                        {
                           companywebsite = Session["CompanyWebsite"].ToString();
                        }

                        int NewUserId = 0;
                        // Here we put code for sign up doctor in sytem or Temp table

                        NewUserId = ObjColleaguesData.Insert_Temp_Signup(Email, EncPassword, false, person.ImageUrl, person.FirstName, person.LastName, person.Age, person.Gender, person.Location, person.Company, person.Title, person.FacebookURL, person.TwitterURL, person.LinkedInURL, person.InfluenceScore, Phone, person.Description);

                        if (NewUserId != 0)
                        {
                            ObjTemplate.NewUserEmailFormat(Email, companywebsite + "/User/Index?UserName=" + Email + "&Password=" + EncPassword, Password, companywebsite); // send tempate to admin if doctor sing up in system

                            ObjTemplate.SignUpEMail(Email, fullName, Phone, person.FacebookURL, person.TwitterURL, person.LinkedInURL);// send tempate to admin if doctor sing up in system

                        }
                        if(Convert.ToString(TempData["NewSignUp"]) == "true")
                        {
                            TempData["DoctorSignup"] = true;
                            return RedirectToAction("Index", "Signup");
                        }

                        if (!string.IsNullOrEmpty(From))
                        {
                            return RedirectToAction("Index", "Signuptoday", new { valid = "Signup" });
                        }
                        else
                        {
                            return RedirectToAction("Index", "User", new { valid = "Signup" });
                        }
                        #endregion

                    }


                }

                return View();
            }
            catch (Exception ex)
            {
                bool status1 = objCommon.InsertErrorLog(Request.Url.ToString(), ex.StackTrace.ToString(), ex.Message.ToString());
                throw;
            }
        }


        protected Person GetSocialMediaPerson(string email)
        {
            try
            {
                //connect to fliptop and pull the information down
                SocialMedia sm = new SocialMedia();
                Person person = sm.LookUpPersonClearBit(email);
                Person fullperson = sm.LookUpPersonfullcontact(email);
                person.FirstName = !string.IsNullOrEmpty(person.FirstName) ? person.FirstName : fullperson.FirstName;
                person.LastName = !string.IsNullOrEmpty(person.LastName) ? person.LastName : fullperson.LastName;
                person.FullName = !string.IsNullOrEmpty(person.FullName) ? person.FullName : fullperson.FullName;
                person.FacebookURL = !string.IsNullOrEmpty(person.FacebookURL) ? person.FacebookURL : fullperson.FacebookURL;
                person.LinkedInURL = !string.IsNullOrEmpty(person.LinkedInURL) ? person.LinkedInURL : fullperson.LinkedInURL;
                person.TwitterURL = !string.IsNullOrEmpty(person.TwitterURL) ? person.TwitterURL : fullperson.TwitterURL;
                person.ImageUrl = !string.IsNullOrEmpty(person.ImageUrl) ? person.ImageUrl : fullperson.ImageUrl;
                person.Description = !string.IsNullOrEmpty(fullperson.Description) ? fullperson.Description : person.Description;
                return person;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #region ForGot Password
        [HttpPost]
        public JsonResult ForgotPassword(string Email)
        {
            object obj = string.Empty; string FullName = string.Empty; string EncPassword = string.Empty;
            try
            {
                DataTable dtCheck = ObjColleaguesData.CheckEmailInSystem(Email);// Check in system email is there or not
                if (dtCheck.Rows.Count > 0)
                {
                    string companywebsite = "";
                    if (Session["CompanyWebsite"] != null)
                    {
                        companywebsite = Session["CompanyWebsite"].ToString();
                    }

                    if (Convert.ToInt32(dtCheck.Rows[0]["IsInSystem"]) == 1 && Convert.ToInt32(dtCheck.Rows[0]["Status"]) == 1)
                    {
                        FullName = objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["FirstName"]), string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["LastName"]), string.Empty);
                        EncPassword = objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["Password"]), string.Empty);
                        DataTable dtCheckFirst = new DataTable();
                        dtCheckFirst = ObjColleaguesData.GetFirstLoginCount(Convert.ToInt32(dtCheck.Rows[0]["UserId"]));
                        if (dtCheckFirst.Rows.Count == 0)
                        {
                            ObjTemplate.NewUserEmailFormat(Email, companywebsite + "/User/Index?UserName=bhushan@drdds.com&Password=" + EncPassword, ObjTripleDESCryptoHelper.decryptText(EncPassword), companywebsite);
                        }
                        else
                        {
                            ObjTemplate.SendingForgetPassword(FullName, Email, EncPassword);
                        }

                        obj = "Password reset link has been sent to your email address.";


                    }
                    else if (Convert.ToInt32(dtCheck.Rows[0]["Status"]) == 2)
                    {
                        FullName = objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["FirstName"]), string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["LastName"]), string.Empty);
                        EncPassword = objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["Password"]), string.Empty);


                        ObjTemplate.NewUserEmailFormat(Email, companywebsite + "/User/Index?UserName=" + Email + "&Password=" + EncPassword + "&Status=2", ObjTripleDESCryptoHelper.decryptText(EncPassword), companywebsite);
                        obj = "2";
                    }
                    else if (Convert.ToInt32(dtCheck.Rows[0]["Status"]) == 3)
                    {
                        obj = "3";
                    }
                    else
                    {
                        #region Re Send sign up tempalte to user of temp table
                        DataTable dttemp = new DataTable();
                        string EncPasswordTemp = string.Empty;

                        dttemp = ObjAdmin.GetTempSignupByEmail(Email);
                        if (dttemp.Rows.Count > 0)
                        {
                            EncPasswordTemp = objCommon.CheckNull(Convert.ToString(dttemp.Rows[0]["password"]), string.Empty);
                            ObjTemplate.NewUserEmailFormat(Email, companywebsite + "/User/Index?UserName=" + Email + "&Password=" + EncPasswordTemp, ObjTripleDESCryptoHelper.decryptText(EncPasswordTemp), companywebsite); // send tempate to admin if doctor sing up in system
                        }


                        #endregion
                        obj = "Password reset link has been sent to your email address.";
                    }


                }
                else
                {
                    obj = "This Email Address Does Not Exist In Our System.";
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region ResetPassword
        public ActionResult ResetPassword(string UserName)
        {

            ObjCompany.GetActiveCompany();
            UserName = UserName.Replace(" ", "+");
            UserName = ObjTripleDESCryptoHelper.decryptText(UserName);
            DataTable dtCheck = ObjColleaguesData.CheckEmailExistsAsDoctor(UserName);
            if (dtCheck.Rows.Count > 0)
            {
                ViewBag.UserFLname = ((!string.IsNullOrEmpty(dtCheck.Rows[0]["FirstName"].ToString()) ? dtCheck.Rows[0]["FirstName"].ToString() : "") + " " + (!string.IsNullOrEmpty(dtCheck.Rows[0]["LastName"].ToString()) ? dtCheck.Rows[0]["LastName"].ToString() : ""));
            }
            return View();
        }

        public JsonResult UpdatePasswordOfDoctor(string Email, string NewPassword)
        {
            object obj = string.Empty;
            try
            {
                DataTable dtCheck = new DataTable();
                string EncryptedPassword = string.Empty;
                dtCheck = ObjColleaguesData.CheckEmailInSystem(Email);
                if (dtCheck.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(NewPassword))
                    {
                        EncryptedPassword = ObjTripleDESCryptoHelper.encryptText(NewPassword);
                    }
                    bool updatepass = ObjColleaguesData.UpdatePasswordOfDoctor(Convert.ToInt32(dtCheck.Rows[0]["UserId"]), EncryptedPassword);
                    if (updatepass)
                    {
                        obj = "1";
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public string UpdatePasswordOfDoctorFromResetEmailLink(string Email, string NewPassword)
        {

            string[] obj = new string[2];
            try
            {
                DataTable dtCheck = new DataTable();
                string EncryptedPassword = string.Empty;

                Email = ObjTripleDESCryptoHelper.decryptText(Email);
                dtCheck = ObjColleaguesData.CheckEmailInSystem(Email);
                if (dtCheck.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(NewPassword))
                    {
                        EncryptedPassword = ObjTripleDESCryptoHelper.encryptText(NewPassword);
                    }
                    bool updatepass = ObjColleaguesData.UpdatePasswordOfDoctor(Convert.ToInt32(dtCheck.Rows[0]["UserId"]), EncryptedPassword);
                    if (updatepass)
                    {
                        obj[0] = Email;
                        obj[1] = EncryptedPassword;
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            JavaScriptSerializer js = new JavaScriptSerializer();
            string json = js.Serialize(obj);

            return json.ToString();
        }
        #endregion
        //-- DO NOT DELETE BELOW COMMENTED UpdateTimezoneBasedOnLocation FUNCTION - ITS USEFUL FOR TIMEZONE CHANGES
        //public string UpdateTimezoneBasedOnLocation()
        //{
        //    clsCommon ObjCommon = new clsCommon();
        //    DataTable dt = new DataTable();
        //    System.Text.StringBuilder sb = new System.Text.StringBuilder();
        //    string qry = "SELECT DISTINCT City,[State] FROM AddressInfo where LEN(LTRIM(RTRIM(ISNULL(City,'')))) > 0 AND LEN(LTRIM(RTRIM(ISNULL([State],'')))) > 0   AND AddressInfoId NOT IN (SELECT AddressInfoId FROM Temp_AddressInfo)  ";
        //    dt = ObjCommon.DataTable(qry);
        //    if (dt.Rows.Count > 0)
        //    {
        //        foreach (DataRow item in dt.Rows)
        //        {
        //            var City = Convert.ToString(item["City"]);
        //            var State = Convert.ToString(item["State"]);
        //            var TimeZoneId = 74;
        //            string ZipCode = "0";
        //            try
        //            {
        //                using (var webClient = new System.Net.WebClient())
        //                {
        //                    var url = "http://maps.googleapis.com/maps/api/geocode/json?address=" + City + "," + State + "&sensor=false";
        //                    var json = webClient.DownloadString(url);
        //                    var obj = Newtonsoft.Json.Linq.JObject.Parse(json);
        //                    if (Convert.ToString(obj["status"]) == "OK")
        //                    {
        //                        //if(obj["results"][0]["address_components"].Count() >= 5)
        //                        //    ZipCode = Convert.ToInt64(obj["results"][0]["address_components"].Last()["long_name"]);
        //                        foreach (var item1 in obj["results"][0]["address_components"])
        //                        {
        //                            if(Array.IndexOf(item1["types"].ToArray(), "postal_code") >= 0)
        //                            {
        //                                ZipCode = Convert.ToString(item1["long_name"]);
        //                            }
        //                        }
        //                        url = "https://maps.googleapis.com/maps/api/timezone/json?location=" + (double)obj["results"][0]["geometry"]["location"]["lat"] + ", " + (double)obj["results"][0]["geometry"]["location"]["lng"] + "&timestamp=0&sensor=false";
        //                        json = webClient.DownloadString(url);
        //                        var obj1 = Newtonsoft.Json.Linq.JObject.Parse(json);
        //                        if (Convert.ToString(obj1["status"]) == "OK")
        //                        {
        //                            var TimeZoneSystemName = Convert.ToString(obj1["timeZoneName"]);

        //                            qry = "SELECT TimeZoneId FROM TimeZone WHERE TimeZoneValue = '" + TimeZoneSystemName + "' OR (TimeZoneText like '%" + TimeZoneSystemName + "%' AND TimeZoneText like '%' + (SELECT TOP 1 Data FROM [dbo].Split('" + Convert.ToString(obj1["timeZoneId"]) + "','/') ORDER BY ROW_NUMBER() OVER (ORDER BY Data) DESC) +'%');";
        //                            DataTable dt2 = ObjCommon.DataTable(qry);
        //                            if (dt2.Rows.Count > 0)
        //                            {
        //                                TimeZoneId = Convert.ToInt32(dt2.Rows[0][0]);
        //                            }
        //                            else
        //                            {
        //                                sb.Append("<p>Invalid time zone received from API : " + TimeZoneSystemName + "</p>");
        //                            }
        //                        }

        //                        else
        //                        {
        //                            sb.Append("<p>Error for location : " + City + "," + State + "</p>");
        //                        }
        //                        if (Convert.ToString(obj1["status"]) == "OVER_QUERY_LIMIT")
        //                        {
        //                            sb.Append("<h1 style='color:red'> Daily limit over </h1>");
        //                            break;
        //                        }

        //                    }
        //                    if (Convert.ToString(obj["status"]) == "OVER_QUERY_LIMIT")
        //                    {
        //                        sb.Append("<h1 style='color:red'> Daily limit over </h1>");
        //                        break;

        //                    }

        //                }
        //                City = City.Replace("'", "''");
        //                State = State.Replace("'", "''");
        //                ZipCode = ZipCode.Replace("'", "''");
        //                qry = @"DECLARE @City NVARCHAR(100) = '{0}';
        //                        DECLARE @State NVARCHAR(100) = '{1}';
        //                        DECLARE @TimeZoneId INT = {2};
        //                        DECLARE @ZipCode VARCHAR(20) = '{3}';


        //                        INSERT INTO Temp_AddressInfo (
        //                        	AddressInfoId
        //                        	,TimezoneId
        //                        	,ZIPCode
        //                        	,CreationDate
        //                        	)
        //                        SELECT AddressInfoId
        //                        	,@TimeZoneID
        //                        	,@ZipCode
        //                        	,GETDATE()
        //                        FROM AddressInfo
        //                        WHERE City = @City AND [State] = @State ;

        //                        UPDATE AddressInfo
        //                        SET TimeZoneId = @TimeZoneId
        //                        	,ZipCode = CASE 
        //                        		WHEN LEN(LTRIM(RTRIM(ISNULL(ZipCode, '')))) = 0
        //                        			AND @ZipCode != 0
        //                        			THEN CONVERT(NVARCHAR(50), @ZipCode)
        //                        		ELSE ZipCode
        //                        		END
        //                        WHERE City = @City AND [State] = @State ;

        //                        SELECT @@ROWCOUNT;";
        //                qry = string.Format(qry,City,State, TimeZoneId, ZipCode);
        //                sb.Append("<p>Query :" + qry);
        //                DataTable dt1 = ObjCommon.DataTable(qry);
        //                sb.Append(" <p>City and State : " + City + ", " + State + " is " + (Convert.ToInt32(dt1.Rows[0][0]) != 0 ? "" : "not") + " updated with TimezoneId = " + TimeZoneId + ". </p>");
        //            }
        //            catch (Exception e)
        //            {
        //                sb.Append("<p style='color:red'> Error for City : " + item["City"] + "," + item["State"] + " : " + e.ToString() + " </p>");
        //            }

        //        }
        //    }
        //    return sb.ToString();
        //}
        public ActionResult NewIndex()
        {
            ObjCompany.GetActiveCompany();
            return View();
        }
        public ActionResult Userlearnmore()
        {
            return View();
        }
    }
}
