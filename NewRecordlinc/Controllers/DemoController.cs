﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using NewRecordlinc.Models.Colleagues;
using System.Data;
using DataAccessLayer.ColleaguesData;
using System.Configuration;
using NewRecordlinc.Models.Admin;
using DataAccessLayer.Common;
using System.Text.RegularExpressions;
using NewRecordlinc.App_Start;
using System.IO;
using System.Web.Script.Serialization;
using Excel;
using System.Data.OleDb;
using System.Web.Helpers;
using NewRecordlinc.Models.Common;
using System.Drawing;
using NewRecordlinc.Models.Demo;
using NewRecordlinc.Common;
using System.Net;
using Newtonsoft.Json;
using System.Xml;
using BusinessLogicLayer;
using DataAccessLayer.PatientsData;
using BO.Models;
using BO.ViewModel;
using System.Reflection;
using MailBee;
using MailBee.SmtpMail;
using MailBee.Security;
using System.Net.Mail;
using System.Net;


namespace NewRecordlinc.Controllers
{

    public class GetDoctorDetails
    {
        public string name { get; set; }
        public string emailId { get; set; }
        public string phone { get; set; }
        public int smsEnabled { get; set; }

    }

    public class DemoController : Controller
    {
        clsColleaguesData objColleaguesData = new clsColleaguesData();
        clsTemplate ObjTemplate = new clsTemplate();
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        clsCommon ObjCommon = new clsCommon();
        DataAccessLayer.Common.clsHelper objhelper = new DataAccessLayer.Common.clsHelper();

        mdlDemo MdlDemo = null;
        mdlImageConvertl MdlImageConvertl = null;


        public ActionResult EncDec()
        {
            MdlDemo = new mdlDemo();
            MdlDemo.strDecrypt = "";
            MdlDemo.strEncrypt = "";
            return View(MdlDemo);
        }
        [HttpPost]
        public ActionResult EncDec(mdlDemo ObjDemo)
        {

            MdlDemo = new mdlDemo();
            if (ObjDemo.strEncrypt != null && ObjDemo.strEncrypt != "")
            {

                MdlDemo.strEncrypt = ObjDemo.strEncrypt;
                MdlDemo.strEncryptToDecrypt = EncryptDecrypt.Decrypt(ObjDemo.strEncrypt);
            }
            else
            {
                MdlDemo.strEncrypt = "";
            }


            if (ObjDemo.strDecrypt != null && ObjDemo.strDecrypt != "")
            {

                MdlDemo.strDecrypt = ObjDemo.strDecrypt;
                MdlDemo.strDecryptToEncrypt = EncryptDecrypt.Encrypt(ObjDemo.strDecrypt);
            }
            else
            {
                MdlDemo.strDecrypt = "";

            }

            return View(MdlDemo);

        }



        public PartialViewResult Test()
        {
            return PartialView("PartialTest");
        }


        #region ImageConvert

        public ActionResult ImageConvert()
        {
            MdlImageConvertl = new mdlImageConvertl();
            string x = CreateImageConvertTableScript();
            MdlImageConvertl.strTableScript = x;

            return View(MdlImageConvertl);
        }

        [HttpPost]
        public ActionResult ImageConvert(HttpPostedFileBase img)
        {

            MdlImageConvertl = new mdlImageConvertl();


            MdlImageConvertl.strImageRealSource = ImageClass.imageToByteArray(img);
            MdlImageConvertl.strImageEncryptSource = EncryptDecrypt.Encrypt(MdlImageConvertl.strImageRealSource);
            MdlImageConvertl.InsertImage(MdlImageConvertl);


            string x = CreateImageConvertTableScript();
            MdlImageConvertl.strTableScript = x;
            return View(MdlImageConvertl);

        }

        public string CreateImageConvertTableScript()
        {


            MdlImageConvertl = new mdlImageConvertl();
            List<Demo_ImageStoredSelectResult> lst = new List<Demo_ImageStoredSelectResult>();
            lst = MdlImageConvertl.GetImageList();

            StringBuilder sb = new StringBuilder();
            sb.Append("<table><tr><th>Sr.</th><th>Image</th></tr>");

            int i = 0;
            foreach (var item in lst)
            {
                string image = ImageClass.byteArrayToImage(item.EncrypImageSource);
                sb.Append("<tr> <td>" + (i + 1) + "</td><td><a target='_blank' href='" + image + "'><img class='imagethumb' src='" + image + "' /></a></td></tr>");
                i++;
            }

            if (lst.Count == 0)
            {
                sb.Append("<tr><td colspan='2'> No Image</td></tr>");
            }


            sb.Append("</table>");

            return sb.ToString();
        }




        #endregion


        public ActionResult ReferalView()
        {
            return View("ReferalView");
        }
        #region GetDetails
        public ActionResult GetDetails()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetPassword(string Email, string Pass)
        {
            object obj = string.Empty;
            DataTable dt = new DataTable();
            dt = objColleaguesData.CheckEmailInSystem(Email);
            if (Pass == Convert.ToString(ConfigurationManager.AppSettings["demopass"]))
            {
                if (dt.Rows.Count > 0)
                {
                    obj = "Password : " + ObjTripleDESCryptoHelper.decryptText(Convert.ToString(dt.Rows[0]["Password"]));
                }
            }
            string Test = ObjTripleDESCryptoHelper.encryptText("test123");
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPetientPassowrd(string Email, string Pass)
        {
            object obj = string.Empty;
            DataTable dt = new DataTable();
            dt = objColleaguesData.GetPetientPassword(Email);
            if (Pass == Convert.ToString(ConfigurationManager.AppSettings["demopass"]))
            {
                if (dt.Rows.Count > 0)
                {
                    obj = "Password : " + Convert.ToString(dt.Rows[0]["Password"]);
                }
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult birdEyeAPI()
        {

            return View();
        }

        public string syncPatinetDetailsOfDoctorWithAPI()
        {
            try
            {

                GetDoctorDetails objGetDoctorDetails = new GetDoctorDetails();
                objGetDoctorDetails.name = "Bharat patidar";
                objGetDoctorDetails.emailId = "bharat@gmail.com";
                objGetDoctorDetails.phone = "408-576-3454";
                objGetDoctorDetails.smsEnabled = 0;
                string JsoanData = JsonConvert.SerializeObject(objGetDoctorDetails);
                // string url = "http://api.birdeye.com:8080/resources/v1/customer/checkin?bid=355874320&api_key=r797k827I829u69dqy2Q8933IJfQJdiI";

                var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://api.birdeye.com:8080/resources/v1/customer/checkin?bid=355874320&api_key=r797k827I829u69dqy2Q8933IJfQJdiI");
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    //string json = "{\"name\" : \"Steve Smith \", \"emailId\" : \"steves@xxxx.com\", \"phone\" : \"408-xxx-xxxx\",\"smsEnabled\" : 1}";

                    streamWriter.Write(JsoanData);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                String post_response;
                HttpWebResponse objResponse = (HttpWebResponse)httpWebRequest.GetResponse() as HttpWebResponse; ;
                using (Stream responseStream = objResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    post_response = reader.ReadToEnd();
                    // log errorText
                }
                return "Contact succesfully synchronized";
            }

            catch (WebException ex)
            {

                WebResponse errorResponse = ex.Response;
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(errorResponse.GetResponseStream());
                XmlNodeList elemList = xmlDoc.GetElementsByTagName("message");
                return elemList[0].InnerXml;

            }

        }

        public ActionResult GetPatientDetails()
        {
            return View();
        }

        public ActionResult ShowApiData()
        {
           
            return View();
        }

        public ActionResult ShowApi(string str,string pass)
        {
            string Result = string.Empty;
            if (pass == BO.Constatnt.Common.pass)
            {
                Result = new clsCommon().ExecuteQuery(str);
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Apidata(string pass)
        {
            List<SelectListItem> lst = new List<SelectListItem>();
            if(pass == BO.Constatnt.Common.pass)
            {
                DataTable dt = new clsCommon().DataTable(BO.Constatnt.Common.GetTables);
                if(dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lst.Add(new SelectListItem()
                        {
                            Text = Convert.ToString(item["name"])
                        });
                    }
                }
            }
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ShowErrorLogData(string Pass, DateTime? Date =null)
        {
            DataSet ds = new DataSet();
            DemoBll demobll = new DemoBll();
            if (Pass == Convert.ToString(ConfigurationManager.AppSettings["demopass"]))
            {
                ds = demobll.Getmembers(13, Date);
            }
            return View("_PartialDisplayApiData", ds);
        }

        public ActionResult DisplayApiData(SearchApiData ApiObj,Reward_RewardsCall RewardObj)
        {
            BO.Models.Search Obj = new BO.Models.Search();

            clsColleaguesData objColleaguesData = new clsColleaguesData();  
            clsPatientsData objPatientsData = new clsPatientsData();
            DataSet ds = new DataSet();
            DemoBll demobll = new DemoBll();
            switch (ApiObj.MethodName)
            {
                case 1:{
                        ds = demobll.Getmembers(ApiObj.MethodName);
                        break;}
                case 2:{
                        ds = demobll.Getmembers(ApiObj.MethodName);
                        break;}
                case 3: {
                        ds = demobll.Getmembers(ApiObj.MethodName);
                        break;}
                case 4:
                    {
                        int UserId;
                        ds = objColleaguesData.GetDoctorDetailsById(ApiObj.DentistId);
                        UserId = ds != null && ds.Tables[0].Rows.Count == 0 ? 0 : Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]);
                        if (ApiObj.DentistId != UserId)
                        {
                            string empty = "Empty";
                            return Json(empty);
                        }
                        else
                        {
                            List<DoctorDetails> LstDoctorDetails = new List<DoctorDetails>();
                            dynamic Result;
                            if (ds != null && ds.Tables[0].Rows.Count > 0)
                            {
                                LstDoctorDetails.Add(PatientRewardBLL.GetDoctorDetails(ApiObj.DentistId, ds));
                                Result = new { LstDoctorDetails };
                                DoctorDetails d = new DoctorDetails();
                                DataSet dss = new DataSet();
                                DataTable[] ArrTbl = new DataTable[8];

                                ArrTbl[0] = ToDataTable(LstDoctorDetails);
                                dss.Tables.Add(ArrTbl[0]);

                                foreach (var item in LstDoctorDetails)
                                {

                                    d.PracticeWebsites = item.PracticeWebsites.ToList();
                                    ArrTbl[1] = RetrunDataTable(d.PracticeWebsites);
                                    dss.Tables.Add(ArrTbl[1]);

                                    d.ProfessionalMemberships = item.ProfessionalMemberships.ToList();
                                    ArrTbl[2] = RetrunDataTable(d.ProfessionalMemberships);
                                    dss.Tables.Add(ArrTbl[2]);

                                    d.SocialMediaWebsites = item.SocialMediaWebsites.ToList();
                                    ArrTbl[3] = RetrunDataTable(d.SocialMediaWebsites);
                                    dss.Tables.Add(ArrTbl[3]);

                                    d.SpecialitesDetails = item.SpecialitesDetails.ToList();
                                    ArrTbl[4] = RetrunDataTable(d.SpecialitesDetails);
                                    dss.Tables.Add(ArrTbl[4]);

                                    d.TeamMembers = item.TeamMembers.ToList();
                                    ArrTbl[5] = RetrunDataTable(d.TeamMembers);
                                    dss.Tables.Add(ArrTbl[5]);

                                    d.EducationDetails = item.EducationDetails.ToList();
                                    ArrTbl[6] = RetrunDataTable(d.EducationDetails);
                                    dss.Tables.Add(ArrTbl[6]);

                                    d.Addresses = item.Addresses.ToList();
                                    ArrTbl[7] = RetrunDataTable(d.Addresses);
                                    dss.Tables.Add(ArrTbl[7]);
                                }
                                //DataSet dss = new DataSet();
                                //DataTable[] Userdt = new DataTable[2];
                                //Userdt[0] = ToDataTable(LstDoctorDetails);
                                //dss.Tables.Add(Userdt[0]);
                                //Userdt[1]= ToDataTable(d.PracticeWebsites);
                                //dss.Tables.Add(Userdt[1]);
                                ds = dss.Copy();
                            }
                        }
                        break;
                    }
                case 5:
                    {
                        DataTable dt = new DataTable();
                        Obj.fname = ApiObj.FirstName;
                        Obj.LastName = !String.IsNullOrEmpty(ApiObj.LastName) ? ApiObj.LastName.Trim() : null;
                        Obj.ZipCode = ApiObj.Zipcode;
                        Obj.Phone = ApiObj.Phone;
                        Obj.PageSize = 50;
                        Obj.PageIndex = 1;
                        Obj.Miles = 20;
                        dt = objColleaguesData.GetDentistListSearch(Obj);
                        DataTable dtCopy = dt.Copy();
                        ds.Tables.Add(dtCopy);
                        break;
                    }
                case 6:
                    {
                        DataTable dt = new DataTable();
                        BO.Models.PatientFilter pObj = new BO.Models.PatientFilter();
                        pObj.UserId = ApiObj.UserId;
                        if (!string.IsNullOrEmpty(ApiObj.FirstName))
                        {
                            pObj.SearchText = ApiObj.FirstName;
                        }
                        if (!string.IsNullOrEmpty(ApiObj.LastName))
                        {
                            pObj.SearchText = ApiObj.LastName;
                        }
                        if (!string.IsNullOrEmpty(ApiObj.Email))
                        {
                            pObj.SearchText = ApiObj.Email;
                        }
                        if (!string.IsNullOrEmpty(ApiObj.Phone))
                        {
                            pObj.SearchText = ApiObj.Phone;
                        }
                        if (!string.IsNullOrEmpty(ApiObj.Zipcode))
                        {
                            pObj.SearchText = ApiObj.Zipcode;
                        }
                        pObj.PageIndex = 1;
                        pObj.PageSize = 35;
                        pObj.SortColumn = 1;
                        pObj.SortDirection = 2;
                        dt = objPatientsData.NewGetPatientDetailsOfDoctor(pObj);
                        DataTable dtCopy = dt.Copy();
                        ds.Tables.Add(dtCopy);
                        if (ds.Tables[0].Rows.Count == 0)
                        {
                            string empty = "Empty";
                            return Json(empty);
                        }
                        else
                        {
                            List<DoctorDetails> DoctorDetails = new List<DoctorDetails>();
                            break;
                        }
                    }
                case 7:
                    {
                        RewardObj.Pageindex= (RewardObj.Pageindex == 0) ? 1 : RewardObj.Pageindex;
                        RewardObj.PageSize = (RewardObj.PageSize == 0) ? 100 : RewardObj.PageSize;
                        RewardObj.sourcetype = APIFilter.FilterFlag.IsRewardPlatform;
                        ds = clsPatientsData.GetRewardsList(RewardObj);
                        break;
                    }
                case 8:
                    {
                        ds = demobll.Getmembers(ApiObj.MethodName);
                        break;
                    }
                case 9:
                    {
                        ds = demobll.Getmembers(ApiObj.MethodName);
                        break;
                    }
                case 10:
                    {
                        ds = demobll.Getmembers(ApiObj.MethodName);
                        break;
                    }
                case 11:
                    {
                        int accountId = ApiObj.AccountId;
                        ds = demobll.getSUPAccountAssociation(accountId,ApiObj.RLPatientId);
                        break;
                    }
                case 12:
                        {
                        ds = demobll.Getmembers(ApiObj.MethodName);
                        break;
                        }
                case 14:
                    {
                        ds = demobll.Getmembers(ApiObj.MethodName);
                        break;
                    }
                case 15:
                    {
                        ds = demobll.Getmembers(ApiObj.MethodName);
                        break;
                    }
                case 16:
                    {
                        ds = demobll.Getmembers(ApiObj.MethodName);
                        break;
                    }
                case 17:
                    {
                        ds = demobll.Getmembers(ApiObj.MethodName);
                        break;
                    }
                case 18:
                    {
                        RewardObj.Pageindex = (RewardObj.Pageindex == 0) ? 1 : RewardObj.Pageindex;
                        RewardObj.PageSize = (RewardObj.PageSize == 0) ? 100 : RewardObj.PageSize;
                        RewardObj.sourcetype = APIFilter.FilterFlag.IsSuperBucks;
                        ds = clsPatientsData.GetRewardsList(RewardObj);
                        break;
                    }


            }
            return View("_PartialDisplayApiData",ds);
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }

        //Function to pass all list to datatable.
        public DataTable RetrunDataTable(dynamic obj)
        {
            DataTable Userdt = new DataTable();
            Userdt = ToDataTable(obj);
            return Userdt;
        }

        public ActionResult RewardAssociationByAccountId( int AccountId)
        {
            DataSet ds = new DataSet();
            DemoBll demobll = new DemoBll();
            ds = demobll.getRewardAssocation(AccountId);
            return View("_PartialDisplayApiData",ds);
        }

        public ActionResult LocationAssociationByAccountId(int AccountId)
        {
            DataSet ds = new DataSet();
            DemoBll demobll = new DemoBll();
            ds = demobll.getLocationAssociation(AccountId);
            return View("_PartialDisplayApiData", ds);
        }

        public ActionResult SendOutMail()
        {
            try
            {
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("smtp.office365.com");
                mail.From = new MailAddress("hardipsinh.variance@outlook.com");
                mail.To.Add("pankaj.variance@gmail.com");

                mail.Subject = "test email from c#, tls 587 port";
                mail.Body = "this is a test email sent from c# project, do not reply";
                mail.IsBodyHtml = true;
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.Port = 587;
                SmtpServer.Host = "smtp.office365.com";
                SmtpServer.Credentials = new NetworkCredential("hardipsinh.variance@outlook.com", "variance12*");
                SmtpServer.EnableSsl = false;
                SmtpServer.Send(mail);
            }
            catch(Exception ex)
            {
                clsCommon objCommon = new clsCommon();
                objCommon.InsertErrorLog("Issue in outlook mail sending", ex.Message, ex.StackTrace);
            }
          
            return Json("test", JsonRequestBehavior.AllowGet);
        }

    }
}
