﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewRecordlinc.Models.Colleagues;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Configuration;
using NewRecordlinc.Models.Common;
using NewRecordlinc.App_Start;
using BO.ViewModel;
using BusinessLogicLayer;
using BO.Models;

namespace NewRecordlinc.Controllers
{
       public class ProfilesController : Controller
    {
        clsColleaguesData objColleaguesData = new clsColleaguesData();
        clsCommon objCommon = new clsCommon();
        string DoctorImage = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("DoctorImage"));
        string DefaultDoctorImage = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("DefaultDoctorImage"));

        // Priya : Issue no : 5209
        //public ActionResult SearchResult(string alpha = null, string keywords = null, string city = null, string state = null, string country = null, string fname = null , string lname = null, string Speciality = null)
        //{

        //    ViewBag.Specialities = SpecialitiesCheckBoxScript("");
        //    ViewBag.keyword = keywords;
        //    ViewBag.DentistListBySearch = GetDentistListBySearch(1, keywords, lname, 0, fname, null, null, Speciality, city, alpha, country, state);

        //    return View();
        //}

        //[HttpPost]
        //public JsonResult SeletedSpecialities(string SpecialitiId)
        //{
        //    return Json(SpecialitiesCheckBoxScript(SpecialitiId), JsonRequestBehavior.AllowGet);
        //}
        //public string SpecialitiesCheckBoxScript(string SpecialitiId)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    List<SelectListItem> lst = new List<SelectListItem>();
        //    MdlCommon = new mdlCommon();
        //    lst = MdlCommon.DoctorSpecialities();


        //    sb.Append("<ul>");
        //    foreach (var item in lst)
        //    {
        //        List<string> x = new List<string>();

        //        x = SpecialitiId.Split(',').ToList();


        //        if (item.Value == "0")
        //        {

        //        }
        //        else if (SpecialitiId != "" && x.Contains(item.Value))
        //        {
        //            sb.Append("<li><input  id=\"CheckSpecialities\" type=\"checkbox\" value=\"" + item.Value + "\" name=\"\" checked=\"checked\" onclick=\"SeletedSpecialities()\" ><label>" + item.Text + "</label></li>");
        //        }
        //        else
        //        {
        //            sb.Append("<li><input  id=\"CheckSpecialities\" type=\"checkbox\" value=\"" + item.Value + "\" name=\"\" onclick=\"SeletedSpecialities()\" ><label>" + item.Text + "</label></li>");
        //        }

        //    }
        //    sb.Append("</ul>");

        //    return sb.ToString();
        //}


        //[HttpPost]
        //public JsonResult GetDentistList(string PageIndex, string keyword, string LastName, string Miles, string FirstName, string CompanyName, string zipcode, string SpecialityList1, string City, string Alpha, string Country, string State)
        //{
        //    SpecialityList1 = SpecialityList1.TrimEnd(',');
        //    return Json(GetDentistListBySearch(Convert.ToInt32(PageIndex), keyword, LastName, Convert.ToInt32(Miles), FirstName, CompanyName, zipcode, SpecialityList1, City, Alpha, Country, State), JsonRequestBehavior.AllowGet);
        //}


        //public string GetDentistListBySearch(int PageIndex, string keyword, string LastName, int Miles, string FirstName, string CompanyName, string zipcode, string SpecialityList1, string City, string Alpha, string Country, string State)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    try
        //    {
        //        DataSet dsProfile = new DataSet();
        //        DataTable dtstate = objColleaguesData.GetStateCode(State);
        //        if (dtstate.Rows.Count > 0)
        //        {
        //            State = dtstate.Rows[0]["StateCode"].ToString();
        //        }
        //        dsProfile = objColleaguesData.GetDentistListBySearch(PageIndex, PageSize, keyword, null, LastName, 0, Miles, FirstName, null, CompanyName, null, null, null, zipcode, SpecialityList1, City, Alpha, Country, State);
        //        if (dsProfile != null && dsProfile.Tables[0].Rows.Count > 0)
        //        {
        //            sb.Append("<h2 class=\"title\">Search Dentist</h2>");
        //            double TotalPages = Math.Ceiling(Convert.ToDouble(dsProfile.Tables[0].Rows[0]["TotalRecord"]) / PageSize);
        //            sb.Append("<ul class=search_listing><div id=\"searchdetails\" data-keyword=\"" + keyword + "\" data-lastname=\"" + LastName + "\" data-miles=\"" + Miles + "\" data-firstname=\"" + FirstName + "\" data-companyname=\"" + CompanyName + "\" data-zipcode=\"" + zipcode + "\" data-specialitylist=\"" + SpecialityList1 + "\" data-city=\"" + City + "\" data-alpha=\"" + Alpha + "\" data-country=\"" + Country + "\" data-state=\"" + State + "\"></div>");
        //            int k = 1;
        //            for (int i = 0; i < dsProfile.Tables[0].Rows.Count; i++)
        //            {

        //                string ImagePath = "";
        //                string image = objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["image"]), string.Empty);
        //                string StateDB = Convert.ToString(dsProfile.Tables[0].Rows[i]["State"]);
        //                if (StateDB == "TOP")
        //                {
        //                    StateDB = "";
        //                }
        //                image = image.Replace(" ", "%20");
        //                if (string.IsNullOrEmpty(image))
        //                {
        //                    ImagePath = DefaultDoctorImage;
        //                }
        //                else
        //                {
        //                    ImagePath = DoctorImage + image;
        //                }

        //                if (i % 2 == 0)
        //                {
        //                    //sb.Append("<li class=leftcol" + k + " ><span class=check style=display:none;><input type=checkbox></span><span class=thumb><a href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + "  target=_blank><img src=" + ImagePath + " alt=\"" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["FirstName"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["LastName"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["name"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["State"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["City"]), string.Empty) + " and " + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["ZipCode"]), string.Empty) + "\"></a></span><span class=description><h2><a href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + " target=_blank>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["name"]), string.Empty) + "<span class=sub_heading>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["AccountName"]), string.Empty) + "</span></a></h2><ul class=list><li>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["Type"]), string.Empty) + "</li><li class=evenitem>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["City"]), string.Empty) + " " + objCommon.CheckNull(StateDB, string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["Miles"]), string.Empty) + "</li></ul></span><h2 style='text-align:right;'><a title='Book Appointment' href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + "/AppointmentBooking target=_blank><img src='/Content/Appointment/Images/appointment_IconSearchResult.png' style='width:19%;cursor:pointer;'/></a></h2><span class=action style=display:none;><input class=add_dentist type=button value=Add Dentist name=Add Dentist></span><span class=clear></span></li>");
        //                    sb.Append("<li class=leftcol" + k + " ><span class=check style=display:none;><input type=checkbox></span><span class=thumb><a href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + "  target=_blank><img src=" + ImagePath + " alt=\"" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["FirstName"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["LastName"]), string.Empty) + "\"></a></span><span class=description><h2><a href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + " target=_blank>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["name"]), string.Empty) + "<span class=sub_heading>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["AccountName"]), string.Empty) + "</span></a></h2><ul class=list><li>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["Type"]), string.Empty) + "</li><li class=evenitem>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["City"]), string.Empty) + " " + objCommon.CheckNull(StateDB, string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["Miles"]), string.Empty) + "</li></ul></span><h2 style='text-align:right;'><a title='Book Appointment' href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + " target=_blank><img src='/Content/Appointment/Images/appointment_IconSearchResult.png' style='width:19%;cursor:pointer;'/></a></h2><span class=action style=display:none;><input class=add_dentist type=button value=Add Dentist name=Add Dentist></span><span class=clear></span></li>");
        //                    k++;
        //                }
        //                else
        //                {
        //                    //sb.Append("<li class='rightcol" + (k - 1) + " evenitem'><span class=check style=display:none;><input type=checkbox></span><span class=thumb><a href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + "  target=_blank><img src=" + ImagePath + " alt=\"" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["FirstName"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["LastName"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["name"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["State"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["City"]), string.Empty) + " and " + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["ZipCode"]), string.Empty) + "\"></a></span><span class=description><h2><a href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + " target=_blank>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["name"]), string.Empty) + "<span class=sub_heading>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["AccountName"]), string.Empty) + "</span></a></h2><ul class=list><li>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["Type"]), string.Empty) + "</li><li class=evenitem>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["City"]), string.Empty) + " " + objCommon.CheckNull(StateDB, string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["Miles"]), string.Empty) + "</li></ul></span><h2 style='text-align:right;'><a title='Book Appointment' href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + "/AppointmentBooking target=_blank><img src='/Content/Appointment/Images/appointment_IconSearchResult.png' style='width:19%;cursor:pointer;'/></a></h2><span class=action style=display:none;><input class=add_dentist type=button value=Add Dentist name=Add Dentist></span><span class=clear></span></li>");
        //                    sb.Append("<li class='rightcol" + (k - 1) + " evenitem'><span class=check style=display:none;><input type=checkbox></span><span class=thumb><a href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + "  target=_blank><img src=" + ImagePath + " alt=\"" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["FirstName"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["LastName"]), string.Empty) + "\"></a></span><span class=description><h2><a href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + " target=_blank>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["name"]), string.Empty) + "<span class=sub_heading>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["AccountName"]), string.Empty) + "</span></a></h2><ul class=list><li>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["Type"]), string.Empty) + "</li><li class=evenitem>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["City"]), string.Empty) + " " + objCommon.CheckNull(StateDB, string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["Miles"]), string.Empty) + "</li></ul></span><h2 style='text-align:right;'><a title='Book Appointment' href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + " target=_blank><img src='/Content/Appointment/Images/appointment_IconSearchResult.png' style='width:19%;cursor:pointer;'/></a></h2><span class=action style=display:none;><input class=add_dentist type=button value=Add Dentist name=Add Dentist></span><span class=clear></span></li>");

        //                }

        //            }
        //            sb.Append(" </ul><div class=\"clear\"></div>");



        //        }
        //        else
        //        {
        //            sb.Append("<br/><br/><center>No Record Found</center>");
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //        throw;
        //    }
        //    return sb.ToString();

        //}

        //public string GetDentistListBySearchOnScroll(int PageIndex, string keyword, string LastName, int Miles, string FirstName, string CompanyName,string zipcode, string SpecialityList1, string City, string Alpha, string Country, string State)
        //{

        //    StringBuilder sb = new StringBuilder();
        //    DataSet dsProfile = new DataSet();
        //    dsProfile = objColleaguesData.GetDentistListBySearch(PageIndex, PageSize, keyword, null, LastName, 0, Miles, FirstName, null, CompanyName, null, null, null, zipcode, SpecialityList1, City, Alpha, Country, State);
        //    if (dsProfile != null && dsProfile.Tables[0].Rows.Count > 0)
        //    {
        //        int k = 1;
        //        for (int i = 0; i < dsProfile.Tables[0].Rows.Count; i++)
        //        {

        //            string ImagePath = "";
        //            string image = objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["image"]), string.Empty);
        //            string StateDB = Convert.ToString(dsProfile.Tables[0].Rows[i]["State"]);
        //            if (StateDB == "TOP")
        //            {
        //                StateDB = "";
        //            }
        //            image = image.Replace(" ", "%20");
        //            if (string.IsNullOrEmpty(image))
        //            {
        //                ImagePath = DefaultDoctorImage;
        //            }
        //            else
        //            {
        //                ImagePath = DoctorImage + image;
        //            }

        //            if (i % 2 == 0)
        //            {
        //                //sb.Append("<li class=leftcol" + k + " ><span class=check style=display:none;><input type=checkbox></span><span class=thumb><a href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + "  target=_blank><img src=" + ImagePath + " alt=\"" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["FirstName"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["LastName"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["name"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["State"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["City"]), string.Empty) + " and " + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["ZipCode"]), string.Empty) + "\"></a></span><span class=description><h2><a href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + " target=_blank>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["name"]), string.Empty) + "<span class=sub_heading>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["AccountName"]), string.Empty) + "</span></a></h2><ul class=list><li>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["Type"]), string.Empty) + "</li><li class=evenitem>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["City"]), string.Empty) + " " + objCommon.CheckNull(StateDB, string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["Miles"]), string.Empty) + "</li></ul></span><h2 style='text-align:right;'><a title='Book Appointment' href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + "/AppointmentBooking target=_blank><img src='/Content/Appointment/Images/appointment_IconSearchResult.png' style='width:19%;cursor:pointer;'/></a></h2><span class=action style=display:none;><input class=add_dentist type=button value=Add Dentist name=Add Dentist></span><span class=clear></span></li>");
        //                sb.Append("<li class=leftcol" + k + " ><span class=check style=display:none;><input type=checkbox></span><span class=thumb><a href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + "  target=_blank><img src=" + ImagePath + " alt=\"" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["FirstName"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["LastName"]), string.Empty) + "\"></a></span><span class=description><h2><a href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + " target=_blank>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["name"]), string.Empty) + "<span class=sub_heading>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["AccountName"]), string.Empty) + "</span></a></h2><ul class=list><li>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["Type"]), string.Empty) + "</li><li class=evenitem>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["City"]), string.Empty) + " " + objCommon.CheckNull(StateDB, string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["Miles"]), string.Empty) + "</li></ul></span><h2 style='text-align:right;'><a title='Book Appointment' href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + "/AppointmentBooking target=_blank><img src='/Content/Appointment/Images/appointment_IconSearchResult.png' style='width:19%;cursor:pointer;'/></a></h2><span class=action style=display:none;><input class=add_dentist type=button value=Add Dentist name=Add Dentist></span><span class=clear></span></li>");
        //                k++;
        //            }
        //            else
        //            {
        //                //sb.Append("<li class='rightcol" + (k - 1) + " evenitem'><span class=check style=display:none;><input type=checkbox></span><span class=thumb><a href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + "  target=_blank><img src=" + ImagePath + " alt=\"" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["FirstName"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["LastName"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["name"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["State"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["City"]), string.Empty) + " and " + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["ZipCode"]), string.Empty) + "\"></a></span><span class=description><h2><a href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + " target=_blank>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["name"]), string.Empty) + "<span class=sub_heading>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["AccountName"]), string.Empty) + "</span></a></h2><ul class=list><li>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["Type"]), string.Empty) + "</li><li class=evenitem>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["City"]), string.Empty) + " " + objCommon.CheckNull(StateDB, string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["Miles"]), string.Empty) + "</li></ul></span><h2 style='text-align:right;'><a title='Book Appointment' href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + "/AppointmentBooking target=_blank><img src='/Content/Appointment/Images/appointment_IconSearchResult.png' style='width:19%;cursor:pointer;'/></a></h2><span class=action style=display:none;><input class=add_dentist type=button value=Add Dentist name=Add Dentist></span><span class=clear></span></li>");
        //                sb.Append("<li class='rightcol" + (k - 1) + " evenitem'><span class=check style=display:none;><input type=checkbox></span><span class=thumb><a href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + "  target=_blank><img src=" + ImagePath + " alt=\"" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["FirstName"]), string.Empty) + "," + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["LastName"]), string.Empty) + "\"></a></span><span class=description><h2><a href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + " target=_blank>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["name"]), string.Empty) + "<span class=sub_heading>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["AccountName"]), string.Empty) + "</span></a></h2><ul class=list><li>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["Type"]), string.Empty) + "</li><li class=evenitem>" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["City"]), string.Empty) + " " + objCommon.CheckNull(StateDB, string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["Miles"]), string.Empty) + "</li></ul></span><h2 style='text-align:right;'><a title='Book Appointment' href=../p/" + objCommon.CheckNull(Convert.ToString(dsProfile.Tables[0].Rows[i]["PublicPath"]).Replace(" ", "%20"), string.Empty) + "/AppointmentBooking target=_blank><img src='/Content/Appointment/Images/appointment_IconSearchResult.png' style='width:19%;cursor:pointer;'/></a></h2><span class=action style=display:none;><input class=add_dentist type=button value=Add Dentist name=Add Dentist></span><span class=clear></span></li>");

        //            }



        //        }
        //    }
        //    else
        //    {
        //        sb.Append("<li>No more record found</li>");
        //    }
        //    return sb.ToString();
        //}

       
        public ActionResult SearchResult(Search Obj)
        {
            ViewBag.name = (Obj.isFrom) ? "Location" : "Name";
            Obj.PageIndex = 1;
            Obj.PageSize = Convert.ToInt32(ConfigurationManager.AppSettings["PageSize"]);
            SearchProfileOfDoctor ObjOfBLL = new SearchProfileOfDoctor();
            Obj.lst = ObjOfBLL.DoctorSpecialities();
            return View("SearchResultNew", Obj);
        }
        public ActionResult PartialSearchResult(Search Obj)
        {
            SearchProfileOfDoctor ObjOfBLL = new SearchProfileOfDoctor();
            List<DentistDetial> lst = new List<DentistDetial>();
            lst = ObjOfBLL.GetSearchResult(Obj);
            return View(lst);
        }
    }
    
}
