﻿using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using DataAccessLayer.PatientsData;
using NewRecordlinc.Models.Patients;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace NewRecordlinc.Controllers
{

    /// <summary>
    /// 
    /// </summary>
    public class APIPatientDetailsController : ApiController
    {
        clsPatientsData objPatientsData = new clsPatientsData();
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        TripleDESCryptoHelper cryptoHelper = new TripleDESCryptoHelper();
        clsCommon objCommon = new clsCommon();
        PatientDetailResponse ObjResponse;

        /// <summary>
        /// this method will return list of patient with details
        /// </summary>
        /// <param name="Username">Username of doctor</param>
        /// <param name="Password">Password of doctor</param>
        /// <param name="PageSize">size of page like:15 Eg:- if page size is 15 then it will show 15 patient on each page</param>
        /// <param name="PageIndex">Page number</param>
        /// <returns></returns>
        public PatientDetailResponse GetPatientsDetails(string Username, string Password, int PageSize, int PageIndex)
        {

            ObjResponse = new PatientDetailResponse();
            List<PatientDetails> Patientlst = new List<PatientDetails>();
            DataTable dtlogin = new DataTable();
            DataTable dt = new DataTable();

            string encryptedPassword = cryptoHelper.encryptText(Password);
            dtlogin = ObjColleaguesData.LoginUserBasedOnUsernameandPassword(Username, encryptedPassword);
            if (dtlogin.Rows.Count > 0)
            {
                dt = objPatientsData.GetPatientDetailsOfDoctor(Convert.ToInt32(dtlogin.Rows[0]["UserId"]), PageIndex, PageSize, null, 12, 2,null,0,0);
                if (dt.Rows.Count > 0)
                {
                    Patientlst = (from p in dt.AsEnumerable()
                                  select new PatientDetails
                                  {
                                      FirstName = objCommon.CheckNull(Convert.ToString(p["FirstName"]), string.Empty),
                                      LastName = objCommon.CheckNull(Convert.ToString(p["LastName"]), string.Empty),
                                      Phone = objCommon.CheckNull(Convert.ToString(p["Phone"]), string.Empty),
                                      Email = objCommon.CheckNull(Convert.ToString(p["Email"]), string.Empty),
                                      Address = objCommon.CheckNull(Convert.ToString(p["ExactAddress"]), string.Empty),
                                      BirthDate = objCommon.CheckNull(Convert.ToString(p["Age"]), string.Empty),
                                      LastAppointmentDate = null,
                                      AppointmentDate = null,
                                  }).ToList();

                    ObjResponse.PageSize = PageSize;
                    ObjResponse.PageIndex = PageIndex;
                    ObjResponse.TotalRecords = Convert.ToInt32(dt.Rows[0]["TotalRecord"]);
                    ObjResponse.IsSuccess = true;
                    ObjResponse.StatusMessage = string.Empty;
                    ObjResponse.MethodName = "GetPatientsDetails";
                    ObjResponse.ResultCode = 200;
                    ObjResponse.PatientList = Patientlst;
                    return ObjResponse;
                }
                else
                {
                    dt = objPatientsData.GetPatientDetailsOfDoctor(Convert.ToInt32(dtlogin.Rows[0]["UserId"]), 1, 1, null, 12, 2,null,0,0);
                    if (dt.Rows.Count > 0)
                        ObjResponse.TotalRecords = Convert.ToInt32(dt.Rows[0]["TotalRecord"]);
                    else
                        ObjResponse.TotalRecords = 0;
                    ObjResponse.PageSize = PageSize;
                    ObjResponse.PageIndex = PageIndex;
                    ObjResponse.IsSuccess = true;
                    ObjResponse.StatusMessage = "No Record Found";
                    ObjResponse.MethodName = "GetPatientsDetails";
                    ObjResponse.ResultCode = 200;
                    ObjResponse.PatientList = Patientlst;
                    return ObjResponse;
                }
            }
            else
            {
                ObjResponse.IsSuccess = false;
                ObjResponse.StatusMessage = "Please enter valid Username or Password";
                ObjResponse.MethodName = "GetPatientsDetails";
                ObjResponse.ResultCode = -1;
                return ObjResponse;
            }
        }



    }

}
