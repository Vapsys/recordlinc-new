﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewRecordlinc.Models.Colleagues;
using System.Dynamic;
using System.Data;
using BusinessLogicLayer;
using System.Net.Mail;
using System.Configuration;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using System.Text.RegularExpressions;
using BO.Models;

namespace NewRecordlinc.Controllers
{
    public class ClaimProfileController : Controller
    {
        clsCommon objCommon = new clsCommon();
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        clsTemplate ObjTemplate = new clsTemplate();
        // GET: ClaimProfile
        //public ActionResult Index(string identity)
        //{
        //    int UserId = 0;
        //    Member mdlmember = new Member();
        //    DataSet ds = new DataSet();
        //    identity = ObjTripleDESCryptoHelper.decryptText(identity.Replace(" ", "+"));
        //    UserId = Convert.ToInt32(identity);
        //    ds = mdlmember.GetProfileDetailsOfDoctorByID(UserId);
        //    if (ds.Tables.Count > 0)
        //    {
        //        dynamic claim = new ExpandoObject();
        //        claim.Firstname = ds.Tables[0].Rows[0]["FirstName"];
        //        claim.LastName = ds.Tables[0].Rows[0]["LastName"];
        //        claim.Phone = ds.Tables[2].Rows[0]["Phone"];
        //        claim.Email = ds.Tables[2].Rows[0]["EmailAddress"];
        //        string eml = Convert.ToString(ds.Tables[2].Rows[0]["EmailAddress"]);
        //        string result = Regex.Replace(eml, @"(?<=[\w]{0})[\w-\._\+%]*(?=[\w]{0}@)", m => new string('*', m.Length));
        //        claim.MaskMail = result;
        //        return View(claim);
        //    }
        //    else
        //    {
        //        return View();
        //    }
        //}

        public ActionResult Index(string identity)

        {
            int UserId = 0;
            ClaimProfile clmProfile = new ClaimProfile();
            ProfileBLL profileBll = new ProfileBLL();
            identity = ObjTripleDESCryptoHelper.decryptText(identity.Replace(" ", "+"));
            UserId = Convert.ToInt32(identity);
            clmProfile = profileBll.GetProfileDetailsOfDoctorByID(UserId);
            clmProfile.MaskEmail = Regex.Replace(clmProfile.Email, @"(?<=[\w]{0})[\w-\._\+%]*(?=[\w]{0}@)", m => new string('*', m.Length));
            return View("NewIndex", clmProfile);
        }


        //public JsonResult SendMailForClaimProfile(string FirstName, string LastName, string Phone, string Email)
        //{
        //    string EncPassword = string.Empty;
        //    clsColleaguesData objclscolleguesdata = new clsColleaguesData();
        //    DataTable dt = new DataTable();
        //    dt = objclscolleguesdata.CheckEmailInSystem(Email);
        //    if (dt.Rows.Count > 0)
        //    {
        //        if (Convert.ToInt32(dt.Rows[0]["IsInSystem"]) == 1 && Convert.ToInt32(dt.Rows[0]["Status"]) == 1)
        //        {
        //            FirstName = objCommon.CheckNull(Convert.ToString(dt.Rows[0]["FirstName"]), string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dt.Rows[0]["LastName"]), string.Empty);
        //            EncPassword = objCommon.CheckNull(Convert.ToString(dt.Rows[0]["Password"]), string.Empty);
        //            DataTable dtCheckFirst = new DataTable();
        //            ObjTemplate.ClaimThisProfile(FirstName, Email, EncPassword);
        //            return Json(1, JsonRequestBehavior.AllowGet);
        //        }
        //        return Json(1, JsonRequestBehavior.AllowGet);
        //    }
        //    return Json(JsonRequestBehavior.AllowGet);
        //}

        public JsonResult SendMailForClaimProfile(ClaimProfile clmobj)
        {
            string EncPassword = string.Empty;
            clsColleaguesData objclscolleguesdata = new clsColleaguesData();
            DataTable dt = new DataTable();
            dt = objclscolleguesdata.CheckEmailInSystem(clmobj.Email);
            if (dt.Rows.Count > 0)
            {
                if (Convert.ToInt32(dt.Rows[0]["IsInSystem"]) == 1 && Convert.ToInt32(dt.Rows[0]["Status"]) == 1)
                {
                    clmobj.Firstname = objCommon.CheckNull(Convert.ToString(dt.Rows[0]["FirstName"]), string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dt.Rows[0]["LastName"]), string.Empty);
                    EncPassword = objCommon.CheckNull(Convert.ToString(dt.Rows[0]["Password"]), string.Empty);
                    DataTable dtCheckFirst = new DataTable();
                    ObjTemplate.ClaimThisProfile(clmobj.Firstname, clmobj.Email, EncPassword);
                    return Json(1, JsonRequestBehavior.AllowGet);
                }
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            return Json(JsonRequestBehavior.AllowGet);
        }
    }
}