﻿using Excel;
using NewRecordlinc.App_Start;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogicLayer;
using BO.ViewModel;
using BO.Models;

namespace NewRecordlinc.Controllers
{
    [RoutePrefix("CostEstimator")]
    public class CostEstimatorController : Controller
    {
        // GET: CostEstimator
     
        public ActionResult Index()
        {
            return Redirect("/Error");
        }
        public JsonResult ImportFile(string UplaodeFileName)
        {
            DataSet ds = new DataSet();
            DataTable dtExcelRecords = new DataTable();
            string fileName = Path.GetFileName(UplaodeFileName);
            string fileLocation = Server.MapPath("../CSVLoad/" + fileName);

            FileInfo fi = new FileInfo(fileLocation);
            if (!string.IsNullOrEmpty(UplaodeFileName))
            {
                ds = ReadExcel(fi.FullName, fi.Extension, true);
                dtExcelRecords = ds.Tables[0];
                Session["dtMapping"] = dtExcelRecords;
            }
            return Json(ds, JsonRequestBehavior.AllowGet);
        }
        private DataSet ReadExcel(string filePath, string Extension, bool isHDR)
        {
            FileStream stream = System.IO.File.Open(filePath, FileMode.Open, FileAccess.Read);
            IExcelDataReader excelReader = null;

            switch (Extension)
            {
                case ".xls":
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                    break;
                case ".xlsx": //Excel 07
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    break;
            }

            excelReader.IsFirstRowAsColumnNames = isHDR;
            DataSet result = excelReader.AsDataSet();


            while (excelReader.Read())
            {

            }

            excelReader.Close();

            return result;

        }
        [CustomAuthorizeAttribute]
        
        [Route("Estimate Cost of Insurance")]
        public ActionResult Procedure()
        {
            DataTable dt = new DataTable();
            InsuranceProcedureMapping Model = new InsuranceProcedureMapping();
            Model = InsuanceEstimatorBLL.GetInsuranceCostEstimatorPivot(Convert.ToInt32(Session["UserId"]));
            return View(Model);
        }
        public ActionResult GetInsuranceName(int InsuranceId)
        {
            Insurance model = new Insurance();
            model = InsuanceEstimatorBLL.GetInsuraceDetialsById(InsuranceId);
            return View(model);
        }
        public ActionResult GetProcedureDetials(int ProcedureId)
        {
            Procedure Model = new Procedure();
            Model = InsuanceEstimatorBLL.GetProcedureDetialsById(ProcedureId);
            return View(Model);
        }
        [HttpPost]
        public JsonResult InsertUpdateInsuranceCostEstimatorAmount(List<InsuranceProcedureMapp> lst)
        {
            return Json(InsuanceEstimatorBLL.InsertProcedureInsurenceData(lst,Convert.ToInt32(Session["UserId"])), JsonRequestBehavior.AllowGet);
        }
    }
}