﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewRecordlinc.App_Start;
using NewRecordlinc.Models.Patients;
using NewRecordlinc.Models.Colleagues;
using NewRecordlinc.Common;
using NewRecordlinc.Controllers;
using System.Text;
using System.Data;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using DataAccessLayer.PatientsData;
using System.Configuration;
using System.IO;
using System.Drawing;
using System.Web.Configuration;
using System.Threading;
using System.Net;
using System.Web.Script.Serialization;
using NewRecordlinc.Models.Common;
using ICSharpCode.SharpZipLib.Zip;

namespace NewRecordlinc.Controllers
{
    public class Inbox_OldController : Controller
    {


        mdlColleagues MdlColleagues = null;
        mdlPatient MdlPatient = null;
        clsCommon ObjCommon = new clsCommon();
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        clsTemplate ObjTemplate = new clsTemplate();
        string TempFolder = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("TempUploadsFolderPath"));
        string ImageBankFolder = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("ImageBank"));
        int PageSizeMaster = Convert.ToInt32(WebConfigurationManager.AppSettings["PageSize"]);
        string DraftFolder = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("DraftAttachments"));
        string DefaultDoctorImage = System.Configuration.ConfigurationManager.AppSettings.Get("DefaultDoctorImage");
        string DefaultDoctorFemaleImage = System.Configuration.ConfigurationManager.AppSettings.Get("DefaultDoctorFemaleImage");

        string DoctorFullname = string.Empty;


        #region InBox


        public ActionResult Index()
        {
            try
            {
                if (SessionManagement.UserId != null && SessionManagement.UserId != "")
                {
                    //MdlColleagues = new mdlColleagues();
                    bool Result = ObjCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
                    ViewBag.Startfunction = DateTime.Now;
                    ViewBag.InboxOfDoctor = GetInboxOfDoctor(Convert.ToInt32(Session["UserId"]), 1, 1, 2, null, "Onload", null, Request["messageid"] != null ? Request["messageid"].ToString() : "");
                    if (Request["Email"] != null)
                    {
                        ViewBag.SearchInbox = Request["Email"].ToString();
                    }
                    if (Request["Referral"] != null)
                    {
                        ViewBag.IsRefarral = Request["Referral"].ToString();
                    }
                    //   MdlColleagues.lstInboxOfDoctor = MdlColleagues.GetInboxOfDoctor(Convert.ToInt32(Session["UserId"]), 1, PageSizeMaster, 1, 2, null, null);
                    // if (Request["messageid"] != null && Request["mtypeid"] != null)
                    // {

                    //ViewBag.GetRefferalDetails = MdlPatient.GetRefferalDetailsById(Convert.ToInt32(Session["UserId"]), lst[i].MessageId, "Inbox");
                    //MdlColleagues.lstInboxOfDoctor = MdlColleagues.GetInboxOfDoctor(Convert.ToInt32(Session["UserId"]), 1, PageSizeMaster, 1, 2, null, null);
                    //if (Request["messageid"] != null && Request["mtypeid"] != null)
                    //{

                    //    if (MdlColleagues.lstInboxOfDoctor.Count > 0)
                    //    {
                    //        ViewBag.MessageDetailsById = GetMessageBody(Convert.ToInt32(Request["messageid"]), (MdlColleagues.lstInboxOfDoctor.Count >= 2 ? MdlColleagues.lstInboxOfDoctor[1].MessageId : 0), Convert.ToInt32(Request["mtypeid"]), (MdlColleagues.lstInboxOfDoctor.Count >= 2 ? MdlColleagues.lstInboxOfDoctor[1].MessageTypeId : 0), MdlColleagues.lstInboxOfDoctor[0].IsRead, "");
                    //    }

                    //}
                    //else if (MdlColleagues.lstInboxOfDoctor.Count > 0)
                    //{
                    //    ViewBag.MessageDetailsById = GetMessageBody(MdlColleagues.lstInboxOfDoctor[0].MessageId, (MdlColleagues.lstInboxOfDoctor.Count >= 2 ? MdlColleagues.lstInboxOfDoctor[1].MessageId : 0), MdlColleagues.lstInboxOfDoctor[0].MessageTypeId, (MdlColleagues.lstInboxOfDoctor.Count >= 2 ? MdlColleagues.lstInboxOfDoctor[1].MessageTypeId : 0), MdlColleagues.lstInboxOfDoctor[0].IsRead, "Onload");
                    //}
                    //else
                    //{
                    //    ViewBag.MessageDetailsById = "<div class=\"inbox_icons clearfix\"> <a class=\"inboxCompose\" title=\"Compose\" href=\"/Inbox/Compose\"></a></div>";
                    //}
                    //  }
                    ViewBag.EndTime = DateTime.Now;
                    return View(MdlColleagues);
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }
            }
            catch (Exception ex)
            {
                bool status = ObjCommon.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                throw ex;
            }

        }
        [HttpPost]
        public ActionResult InboxPageing(int PageIndex, int OrderByID, int OrderBy, string Searchtext, string Onload, string IsRefarral)
        {
            MdlColleagues = new mdlColleagues();
            List<MessageListOfDoctor> lst = new List<MessageListOfDoctor>();
            ViewBag.Sorting = "true";
            lst = GetInboxOfDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, OrderByID, OrderBy, Searchtext, Onload, IsRefarral, "");
            return View("PartialInbox", lst);
        }
        public List<MessageListOfDoctor> GetInboxOfDoctor(int UserId, int PageIndex, int OrderByID, int OrderBy, string Searchtext, string Onload, string IsRefarral, string FromEmail)
        {
            try
            {
                MdlPatient = new mdlPatient();
                MdlColleagues = new mdlColleagues();
                List<MessageListOfDoctor> lst = new List<MessageListOfDoctor>();
                lst = MdlColleagues.GetInboxOfDoctor(UserId, PageIndex, PageSizeMaster, OrderByID, OrderBy, Searchtext, IsRefarral, SessionManagement.TimeZoneSystemName);
                ViewBag.FromEmail = FromEmail;
                ViewBag.PageIndex = PageIndex;
                ViewBag.OrderByID = OrderByID;
                ViewBag.OrderBy = OrderBy;
                ViewBag.Searchtext = Searchtext;
                ViewBag.Onload = Onload;
                ViewBag.IsRefarral = IsRefarral;
                if (lst.Count > 0)
                    ViewBag.MessageId = lst[0].MessageId;
                else
                    ViewBag.MessageId = 0;
                if (OrderBy == 1)
                {
                    ViewBag.OrderbyTest = 2;
                }
                else
                {
                    ViewBag.OrderbyTest = 1;
                }
                ViewBag.EndTime = DateTime.Now;
                return lst;
            }
            catch (Exception ex)
            {
                bool status = ObjCommon.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                throw;
            }
        }
        public JsonResult DownloadReferralAttachment(int MessageId, string Type)
        {
            try
            {
                clsColleaguesData CLScolleagues = new clsColleaguesData();
                DataTable dt = new DataTable();
                int ReferralCardId = 0;
                int PatientId = 0;
                DataTable dtsNew = new DataTable();
                dtsNew = ObjColleaguesData.GetReferralViewByID(MessageId, Type);
                if (dtsNew.Rows.Count > 0 && dtsNew != null)
                {
                    ReferralCardId = Convert.ToInt32(dtsNew.Rows[0]["ReferralCardId"]);
                    PatientId = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(dtsNew.Rows[0]["PatientId"])) ? 0 : dtsNew.Rows[0]["PatientId"]);
                }
                //ObjColleaguesData.GetRefferalImageById(Convert.ToInt32(item["ReferralCardId"]), ObjRefferalDetails.PatientId)
                dt = CLScolleagues.GetRefferalImageById(ReferralCardId, PatientId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    getFilenames.Columns.Add("FileName", System.Type.GetType("System.String"));
                    foreach (DataRow item in dt.Rows)
                    {
                        DataRow dr = getFilenames.NewRow();
                        dr["FileName"] = ConfigurationManager.AppSettings.Get("ImageBank") + ObjCommon.CheckNull(item["Name"].ToString(), string.Empty);
                        getFilenames.Rows.Add(dr);
                    }
                }
                DataTable ds = CLScolleagues.GetRefferalDocumentAttachemntsById(ReferralCardId, PatientId);
                if(ds != null && ds.Rows.Count > 0)
                {
                    if(dt == null || dt.Rows.Count == 0)
                    {
                        getFilenames.Columns.Add("FileName", System.Type.GetType("System.String"));
                    }
                    foreach (DataRow item in ds.Rows)
                    {
                        DataRow dr = getFilenames.NewRow();
                        dr["FileName"] = ConfigurationManager.AppSettings.Get("ImageBank") + ObjCommon.CheckNull(item["DocumentName"].ToString(), string.Empty);
                        getFilenames.Rows.Add(dr);
                    }
                }

                if (getFilenames != null && getFilenames.Rows.Count > 0)
                {
                    ZipAllFiles(MessageId);
                }
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }

            return Json(JsonRequestBehavior.AllowGet);
        }
        DataTable getFilenames = new DataTable();
        public JsonResult DownloadAttachmentofMessage(int DownId)
        {
            try
            {
                clsColleaguesData CLScolleagues = new clsColleaguesData();
                DataTable dt = new DataTable();
                dt = CLScolleagues.GetColleageMessageAttachemntsById(DownId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    getFilenames.Columns.Add("FileName", System.Type.GetType("System.String"));
                    foreach (DataRow item in dt.Rows)
                    {
                        DataRow dr = getFilenames.NewRow();
                        dr["FileName"] = ConfigurationManager.AppSettings.Get("ImageBank") + item["AttachmentName"].ToString();
                        getFilenames.Rows.Add(dr);
                    }
                }
                else
                {
                    DataTable dts = new DataTable();
                    dts = CLScolleagues.GetPatientMessageAttachemntsById(DownId);
                    if (dts != null && dts.Rows.Count > 0)
                    {
                        getFilenames.Columns.Add("FileName", System.Type.GetType("System.String"));
                        foreach (DataRow item in dts.Rows)
                        {
                            DataRow dr = getFilenames.NewRow();
                            dr["FileName"] = ConfigurationManager.AppSettings.Get("ImageBank") + item["AttachmentName"].ToString();
                            getFilenames.Rows.Add(dr);
                        }
                    }
                }
                if (getFilenames != null && getFilenames.Rows.Count > 0)
                {
                    ZipAllFiles(DownId);
                }
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }

            return Json(JsonRequestBehavior.AllowGet);
        }
        public void ZipAllFiles(int MessageId)
        {
            byte[] buffer = new byte[4096];
            var tempFileName = Server.MapPath(ConfigurationManager.AppSettings["TempUploadsFolderPath"]) + MessageId + ".zip";
            var zipOutputStream = new ZipOutputStream(System.IO.File.Create(tempFileName));
            var filePath = String.Empty;
            var fileName = String.Empty;
            var readBytes = 0;
            foreach (DataRow item in getFilenames.Rows)
            {

                if (item["FileName"].ToString() != "")
                {
                    fileName = item["FileName"].ToString();
                    filePath = Server.MapPath(fileName);
                    FileInfo fi = new FileInfo(filePath);
                    string PF_Name = String.Empty;
                    if (fi.Exists)
                    {
                        String[] History_Pat = fileName.Split('$');

                        if (History_Pat[0].Length > 0)
                        {
                            if (History_Pat[0].Equals(ConfigurationManager.AppSettings.Get("ImageBank")))
                            {
                                PF_Name = History_Pat[2].ToString();
                            }
                            else
                            {
                                PF_Name = fileName;
                            }
                        }
                        var zipEntry = new ZipEntry(PF_Name.Replace(" ", ""));
                        zipOutputStream.PutNextEntry(zipEntry);
                        using (var fs = System.IO.File.OpenRead(filePath))
                        {
                            do
                            {
                                readBytes = fs.Read(buffer, 0, buffer.Length);
                                zipOutputStream.Write(buffer, 0, readBytes);
                            } while (readBytes > 0);
                        }
                    }
                }
            }
            if (zipOutputStream.Length == 0)
            {
                return;
            }
            zipOutputStream.Finish();
            zipOutputStream.Close();
            Response.ContentType = "application/x-zip-compressed";
            Response.AppendHeader("Content-Disposition", "attachment; filename=MessageId_" + MessageId + "_Records.zip");
            Response.BufferOutput = true;
            Response.WriteFile(tempFileName);
            Response.Flush();
            Response.Close();
            //delete the temp file  
            if (System.IO.File.Exists(tempFileName))
                System.IO.File.Delete(tempFileName);
        }

        public ActionResult GetInboxOfDoctorOnScroll(int PageIndex, int OrderByID, int OrderBy, string Searchtext, string IsRefarral)
        {

            MdlColleagues = new mdlColleagues();
            MdlPatient = new mdlPatient();
            List<MessageListOfDoctor> lst = new List<MessageListOfDoctor>();

            //int OrderbyTest;            
            ViewBag.PageIndex = PageIndex;
            ViewBag.OrderByID = OrderByID;
            ViewBag.OrderBy = OrderBy;
            ViewBag.Searchtext = Searchtext;
            ViewBag.IsRefarral = IsRefarral;
            lst = MdlColleagues.GetInboxOfDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, PageSizeMaster, OrderByID, OrderBy, Searchtext, IsRefarral,SessionManagement.TimeZoneSystemName);

            return View("PartialInbox", lst);

        }

        public string GetMessageBody(int MessageId, int? NextId, int MessageTypeId, int? NextMessagetypeId, string IsRead, string Onload)
        {

            StringBuilder sb = new StringBuilder();
            try
            {
                if (MessageTypeId == 2)
                {
                    #region Referral Body
                    MdlPatient = new mdlPatient();
                    List<ReferralDetails> lst = new List<ReferralDetails>();
                    lst = MdlPatient.GetRefferalDetailsById(Convert.ToInt32(Session["UserId"]), MessageId, "Inbox", SessionManagement.TimeZoneSystemName);

                    if (lst.Count > 0)
                    {



                        sb.Append("<div class=\"message_content\" style=\"float: right; max-width: 100%; position: relative; width:100% !important;\">");
                        sb.Append("<div class=\"inner\">");
                        sb.Append("<div id=\"view_referrals\">");

                        sb.Append("<div class=\"patient_info\">");
                        sb.Append("<h4 class=\"sub_title\" style=\"cursor:pointer;\" onclick=\"ViewPatientHistory('" + lst[0].PatientId + "')\">");
                        sb.Append(lst[0].FullName + "<span>" + (!string.IsNullOrEmpty(lst[0].AssignedPatientId) ? "(" + lst[0].AssignedPatientId + ")" : string.Empty) + "</span><a style=\"float:right; \" class=\"inboxDltMsg\" title=\"Delete\" onclick=\"RemoveReferralFromInboxOfDoctor('" + lst[0].MessageId + "','" + NextId + "','" + NextMessagetypeId + "')\" href=\"javascript:;\"></a><a style=\"float:right; margin-right:15px;\" class=\"inboxReply marginLeft0\" title=\"Reply, Reply All\" href=\"/Inbox/Compose?ColleaguesId=" + lst[0].SenderId + "&amp;PatientId=" + lst[0].PatientId + "&amp;Type=reply\"></a></h4>");
                        sb.Append("<ul class=\"patient_info\">");
                        sb.Append("<li class=\"first\" style=\"width:57% !important\">");
                        if (!String.IsNullOrEmpty(lst[0].ProfileImage))
                            sb.Append("<figure onclick=\"ViewPatientHistory('" + lst[0].PatientId + "')\" style=\"cursor:pointer;\" class=\"thumb\"><img width=\"104\" height=\"104\" alt=\"Ashlyn\" src=" + ImageBankFolder + lst[0].ProfileImage + "></figure>");
                        else if (lst[0].Gender == "Female")
                            sb.Append("<figure class=\"thumb\" onclick=\"ViewPatientHistory('" + lst[0].PatientId + "')\" style=\"cursor:pointer;\"><img width=\"104\" height=\"104\" alt=\"Ashlyn\" src=" + DefaultDoctorFemaleImage + "></figure>");
                        else
                            sb.Append("<figure class=\"thumb\" onclick=\"ViewPatientHistory('" + lst[0].PatientId + "')\" style=\"cursor:pointer;\"><img width=\"104\" height=\"104\" alt=\"Ashlyn\" src=" + DefaultDoctorImage + "></figure>");
                        sb.Append("<ul class=\"list\">");
                        var Datetime = Convert.ToDateTime(lst[0].CreationDate).ToString(System.Configuration.ConfigurationManager.AppSettings["DateFormateMMMddyyyy"]).Split(' ');
                        var CreationDate = Datetime[0];
                        var CreationTime = Datetime[1] + ", " + Datetime[2];
                        sb.Append("<li><label>Referral sent date:</label>" + CreationDate + " " + CreationTime + "</li>");
                        sb.Append("<li><label>Referred by: </label>" + lst[0].SenderName + "</li>");
                        sb.Append("<li><label>Referred to: </label>" + lst[0].ReceiverName + "</li>");
                        sb.Append("<li><label>Location: </label><label style=\"width:111px !important;line-height: 17px;\">" + lst[0].Location + "</label></li></ul>");
                        sb.Append("<div class=\"clear\"></div>");
                        sb.Append("</li>");
                        sb.Append("<li class=\"second\" style=\"width:42.5% !important\">");
                        sb.Append("<ul class=\"list\">");
                        if (!String.IsNullOrEmpty(lst[0].FullAddress))
                            sb.Append("<li><i class=\"address\"></i>" + lst[0].FullAddress + "</li>");
                        if (!String.IsNullOrEmpty(lst[0].Phone))
                            sb.Append("<li><i class=\"wphone\"></i>" + lst[0].Phone + "</li>");
                        if (!String.IsNullOrEmpty(lst[0].EmailAddress))
                            sb.Append("<li><i class=\"email\"></i>" + lst[0].EmailAddress + "</li>");
                        sb.Append("<li><span>Gender: </span>" + lst[0].Gender + "</li>");
                        sb.Append("<li><span>Date of Birth: </span>" + (!string.IsNullOrEmpty(lst[0].DateOfBirth) ? Convert.ToDateTime(lst[0].DateOfBirth).ToString(System.Configuration.ConfigurationManager.AppSettings["DateFormateMMMddyyyy"]) : "") + "</li>");
                        sb.Append("</ul>");
                        sb.Append("</li>");
                        sb.Append("<br class=\"clear\">");
                        sb.Append("</ul>");
                        sb.Append("<div class=\"clear\">");
                        sb.Append("</div>");
                        sb.Append("</div>");

                        sb.Append("<div class=\"table_container\">");
                        sb.Append("<h4 class=\"sub_title\">");
                        sb.Append("Regarding</h4>");
                        sb.Append("<ul class=\"check_listing\">");


                        string[] strRegardingNo = lst[0].RegardOption.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                        foreach (String item in strRegardingNo)
                        {
                            sb.Append("<li><label>" + "" + MdlPatient.ReferralRegarding[Convert.ToInt32(item) - 1] + "</label></li>");
                        }
                        if (!String.IsNullOrEmpty(lst[0].OtherComments))
                            sb.Append("<br/><li><label>" + "" + lst[0].OtherComments + "</label></li>");

                        sb.Append("</ul>");
                        sb.Append("</div>");

                        sb.Append("<div class=\"table_container\">");
                        sb.Append(" <h4 class=\"sub_title\">");
                        sb.Append("Request</h4>");
                        sb.Append("<ul class=\"check_listing\">");

                        string[] strRequest = lst[0].RequestingOption.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                        foreach (String item in strRequest)
                        {
                            sb.Append("<li><label>" + "" + MdlPatient.ReferralRequest[Convert.ToInt32(item) - 1] + "</label></li>");
                        }

                        if (!String.IsNullOrEmpty(lst[0].RequestComments))
                            sb.Append("<li><label>" + "" + lst[0].RequestComments + "</label></li>");
                        sb.Append("</ul>");
                        sb.Append("</div>");

                        sb.Append("<div class=\"table_container\">");
                        sb.Append("<h4 class=\"sub_title\">");
                        sb.Append(" Visual Instructions</h4>");
                        sb.Append("<div class=\"visuals\">");
                        sb.Append("<div class=\"left panel\">");
                        sb.Append("<h5>Permanent Teeth</h5><div class=\"block blockborder\">");
                        sb.Append("<p>Right</p>");
                        sb.Append("<ul class=\"teeth_listing\">");
                        sb.Append("<div class=\"permanentteeth\">");
                        ///Permanent Teeth Right 
                        ///
                        Dictionary<int, String> dictPermanentRight = new Dictionary<int, string>();
                        dictPermanentRight.Add(1, "<li><p>1</p><input type=\"checkbox\" disabled id=\"1\"/></li>");
                        dictPermanentRight.Add(2, "<li><p>2</p><input type=\"checkbox\" disabled id=\"2\"/></li>");
                        dictPermanentRight.Add(3, "<li><p>3</p><input type=\"checkbox\" disabled id=\"3\"/></li>");
                        dictPermanentRight.Add(4, "<li><p>4</p><input type=\"checkbox\" disabled id=\"4\"/></li>");
                        dictPermanentRight.Add(5, "<li><p>5</p><input type=\"checkbox\" disabled id=\"5\"/></li>");
                        dictPermanentRight.Add(6, "<li><p>6</p><input type=\"checkbox\" disabled id=\"6\"/></li>");
                        dictPermanentRight.Add(7, "<li><p>7</p><input type=\"checkbox\" disabled id=\"7\"/></li>");
                        dictPermanentRight.Add(8, "<li><p>8</p><input type=\"checkbox\" disabled id=\"8\"/></li></div>");
                        dictPermanentRight.Add(32, "<li><p>32</p><input type=\"checkbox\" disabled id=\"32\"/></li>");
                        dictPermanentRight.Add(31, "<li><p>31</p><input type=\"checkbox\" disabled id=\"31\"/></li>");
                        dictPermanentRight.Add(30, "<li><p>30</p><input type=\"checkbox\" disabled id=\"30\"/></li>");
                        dictPermanentRight.Add(29, "<li><p>29</p><input type=\"checkbox\" disabled id=\"29\"/></li>");
                        dictPermanentRight.Add(28, "<li><p>28</p><input type=\"checkbox\" disabled id=\"28\"/></li>");
                        dictPermanentRight.Add(27, "<li><p>27</p><input type=\"checkbox\" disabled id=\"27\"/></li>");
                        dictPermanentRight.Add(26, "<li><p>26</p><input type=\"checkbox\" disabled id=\"26\"/></li>");
                        dictPermanentRight.Add(25, "<li><p>25</p><input type=\"checkbox\" disabled id=\"25\"/></li>");


                        if (lst[0].lstToothList.Count > 0)
                        {
                            foreach (ToothList objToothList in lst[0].lstToothList)
                            {

                                if (dictPermanentRight.ContainsKey(objToothList.ToothType))
                                {
                                    if (objToothList.ToothType.ToString() == "8")
                                    {
                                        dictPermanentRight[objToothList.ToothType] = "<li class=\"active\"><p>" + objToothList.ToothType.ToString() + "</p><input type=\"checkbox\" disabled checked=\"checked\" id=\"" + objToothList.ToothType.ToString() + "\"/></li></div>";
                                    }
                                    else
                                    {
                                        dictPermanentRight[objToothList.ToothType] = "<li class=\"active\"><p>" + objToothList.ToothType.ToString() + "</p><input type=\"checkbox\" disabled checked=\"checked\" id=\"" + objToothList.ToothType.ToString() + "\"/></li>";
                                    }

                                }
                            }
                        }


                        sb.Append(GetLine(dictPermanentRight));

                        sb.Append("</ul>");
                        sb.Append("</div>");

                        ///Permanent Teeth Left
                        sb.Append("<div class=\"block\">");
                        sb.Append("<p>Left</p>");
                        sb.Append("<ul class=\"teeth_listing\">");
                        sb.Append("<div class=\"permanentteeth\">");


                        Dictionary<int, String> dictPermanentLeft = new Dictionary<int, string>();
                        dictPermanentLeft.Add(9, "<li><p>9</p><input type=\"checkbox\" disabled id=\"9\"/></li>");
                        dictPermanentLeft.Add(10, "<li><p>10</p><input type=\"checkbox\" disabled id=\"10\"/></li>");
                        dictPermanentLeft.Add(11, "<li><p>11</p><input type=\"checkbox\" disabled id=\"11\"/></li>");
                        dictPermanentLeft.Add(12, "<li><p>12</p><input type=\"checkbox\" disabled id=\"12\"/></li>");
                        dictPermanentLeft.Add(13, "<li><p>13</p><input type=\"checkbox\" disabled id=\"13\"/></li>");
                        dictPermanentLeft.Add(14, "<li><p>14</p><input type=\"checkbox\" disabled id=\"14\"/></li>");
                        dictPermanentLeft.Add(15, "<li><p>15</p><input type=\"checkbox\" disabled id=\"15\"/></li>");
                        dictPermanentLeft.Add(16, "<li><p>16</p><input type=\"checkbox\" disabled id=\"16\"/></li></div>");
                        dictPermanentLeft.Add(24, "<li><p>24</p><input type=\"checkbox\" disabled id=\"24\"/></li>");
                        dictPermanentLeft.Add(23, "<li><p>23</p><input type=\"checkbox\" disabled id=\"23\"/></li>");
                        dictPermanentLeft.Add(22, "<li><p>22</p><input type=\"checkbox\" disabled id=\"22\"/></li>");
                        dictPermanentLeft.Add(21, "<li><p>21</p><input type=\"checkbox\" disabled id=\"21\"/></li>");
                        dictPermanentLeft.Add(20, "<li><p>20</p><input type=\"checkbox\" disabled id=\"20\"/></li>");
                        dictPermanentLeft.Add(19, "<li><p>19</p><input type=\"checkbox\" disabled id=\"19\"/></li>");
                        dictPermanentLeft.Add(18, "<li><p>18</p><input type=\"checkbox\" disabled id=\"18\"/></li>");
                        dictPermanentLeft.Add(17, "<li><p>17</p><input type=\"checkbox\" disabled id=\"17\"/></li>");

                        if (lst[0].lstToothList.Count > 0)
                        {
                            foreach (ToothList objToothList in lst[0].lstToothList)
                            {
                                if (dictPermanentLeft.ContainsKey(objToothList.ToothType))
                                {
                                    if (objToothList.ToothType.ToString() == "16")
                                    {
                                        dictPermanentLeft[objToothList.ToothType] = "<li class=\"active\"><p>" + objToothList.ToothType.ToString() + "</p><input type=\"checkbox\" disabled checked=\"checked\" id=\"" + objToothList.ToothType.ToString() + "\"/></li></div>";
                                    }
                                    else
                                    {
                                        dictPermanentLeft[objToothList.ToothType] = "<li class=\"active\"><p>" + objToothList.ToothType.ToString() + "</p><input type=\"checkbox\" disabled checked=\"checked\" id=\"" + objToothList.ToothType.ToString() + "\"/></li>";
                                    }
                                }
                            }
                        }

                        sb.Append(GetLine(dictPermanentLeft));

                        sb.Append("<br class=\"clear\">");
                        sb.Append("</ul>");
                        sb.Append("</div>");

                        sb.Append("</div>");


                        //Primary Teeth right
                        sb.Append("<div class=\"right panel\">");
                        sb.Append("<h5>Primary Teeth</h5>");
                        sb.Append("<div class=\"block blockborder\">");
                        sb.Append("<p>Right</p><ul class=\"teeth_listing\">");
                        sb.Append("<div class=\"permanentteeth\">");

                        Dictionary<int, String> dictPrimaryRight = new Dictionary<int, string>();
                        dictPrimaryRight.Add(33, "<li><p>A</p><input type=\"checkbox\" disabled id=\"33\"/></li>");
                        dictPrimaryRight.Add(34, "<li><p>B</p><input type=\"checkbox\" disabled id=\"34\"/></li>");
                        dictPrimaryRight.Add(35, "<li><p>C</p><input type=\"checkbox\" disabled id=\"35\"/></li>");
                        dictPrimaryRight.Add(36, "<li><p>D</p><input type=\"checkbox\" disabled id=\"36\"/></li>");
                        dictPrimaryRight.Add(37, "<li><p>E</p><input type=\"checkbox\" disabled id=\"37\"/></li></div>");
                        dictPrimaryRight.Add(52, "<li><p>T</p><input type=\"checkbox\" disabled id=\"52\"/></li>");
                        dictPrimaryRight.Add(51, "<li><p>S</p><input type=\"checkbox\" disabled id=\"51\"/></li>");
                        dictPrimaryRight.Add(50, "<li><p>R</p><input type=\"checkbox\" disabled id=\"50\"/></li>");
                        dictPrimaryRight.Add(49, "<li><p>Q</p><input type=\"checkbox\" disabled id=\"49\"/></li>");
                        dictPrimaryRight.Add(48, "<li><p>P</p><input type=\"checkbox\" disabled id=\"48\"/></li>");

                        if (lst[0].lstToothList.Count > 0)
                        {
                            foreach (ToothList objToothList in lst[0].lstToothList)
                            {
                                if (dictPrimaryRight.ContainsKey(objToothList.ToothType))
                                {
                                    String str = dictPrimaryRight[objToothList.ToothType];
                                    dictPrimaryRight[objToothList.ToothType] = str.Replace("<li>", "<li class=\"active Primary\">");
                                }
                            }
                        }
                        sb.Append(GetLine(dictPrimaryRight));

                        sb.Append("<br class=\"clear\">");
                        sb.Append("</ul>");
                        sb.Append("</div>");


                        ///Primary Teeth Left
                        ///
                        sb.Append("<div class=\"block\">");
                        sb.Append("<p>Left</p>");
                        sb.Append("<ul class=\"teeth_listing\">");
                        sb.Append("<div class=\"permanentteeth\">");

                        Dictionary<int, String> dictPrimaryLeft = new Dictionary<int, string>();
                        dictPrimaryLeft.Add(38, "<li><p>F</p><input type=\"checkbox\" disabled id=\"38\"/></li>");
                        dictPrimaryLeft.Add(39, "<li><p>G</p><input type=\"checkbox\" disabled id=\"39\"/></li>");
                        dictPrimaryLeft.Add(40, "<li><p>H</p><input type=\"checkbox\" disabled id=\"40\"/></li>");
                        dictPrimaryLeft.Add(41, "<li><p>I</p><input type=\"checkbox\" disabled id=\"41\"/></li>");
                        dictPrimaryLeft.Add(42, "<li><p>J</p><input type=\"checkbox\" disabled id=\"42\"/></li></div>");
                        dictPrimaryLeft.Add(47, "<li><p>O</p><input type=\"checkbox\" disabled id=\"47\"/></li>");
                        dictPrimaryLeft.Add(46, "<li><p>N</p><input type=\"checkbox\" disabled id=\"46\"/></li>");
                        dictPrimaryLeft.Add(45, "<li><p>M</p><input type=\"checkbox\" disabled id=\"45\"/></li>");
                        dictPrimaryLeft.Add(44, "<li><p>L</p><input type=\"checkbox\" disabled id=\"44\"/></li>");
                        dictPrimaryLeft.Add(43, "<li><p>K</p><input type=\"checkbox\" disabled id=\"43\"/></li>");

                        if (lst[0].lstToothList.Count > 0)
                        {
                            foreach (ToothList objToothList in lst[0].lstToothList)
                            {
                                if (dictPrimaryLeft.ContainsKey(objToothList.ToothType))
                                {
                                    String str = dictPrimaryLeft[objToothList.ToothType];
                                    dictPrimaryLeft[objToothList.ToothType] = str.Replace("<li>", "<li class=\"active Primary\">");
                                }
                            }
                        }
                        sb.Append(GetLine(dictPrimaryLeft));

                        sb.Append("    <br class=\"clear\">");
                        sb.Append("</ul>");
                        sb.Append("</div>");
                        sb.Append("</div>");
                        sb.Append("<div class=\"clear\"></div>");




                        sb.Append("</div></div>");


                        ///Code For Images

                        if (lst[0].lstImages.Count > 0)
                        {
                            sb.Append("<div class=\"table_container\">");
                            sb.Append("<h4 class=\"sub_title\">");
                            sb.Append("Images</h4>");
                            sb.Append("</div>");
                            sb.Append("<ul class=\"list\">");
                            foreach (MessageAttachment objAttachment in lst[0].lstImages)
                            {
                                string AttachmentName = objAttachment.AttachmentName;
                                AttachmentName = AttachmentName.Substring(AttachmentName.ToString().LastIndexOf("$") + 1);
                                sb.Append("<li onclick=\"Attachementpopup('" + (ImageBankFolder + objAttachment.AttachmentName) + "','Image')\" style=\"cursor: pointer;\"><span><i class=\"attach\"></i>" + AttachmentName + "</span></li>");
                            }
                            sb.Append("</ul>");
                        }

                        ///Code For Documents
                        if (lst[0].lstDocuments.Count > 0)
                        {
                            sb.Append("<div class=\"table_container\">");
                            sb.Append("<h4 class=\"sub_title\">");
                            sb.Append("Documents</h4>");
                            sb.Append("</div>");
                            sb.Append("<ul class=\"list\">");
                            foreach (MessageAttachment objAttachment in lst[0].lstDocuments)
                            {
                                string AttachmentName = objAttachment.AttachmentName;
                                AttachmentName = AttachmentName.Substring(AttachmentName.ToString().LastIndexOf("$") + 1);
                                sb.Append("<li onclick=\"Attachementpopup('" + (ImageBankFolder + objAttachment.AttachmentName) + "','Doc')\" style=\"cursor: pointer;\"> <span><i class=\"attach\"></i>" + AttachmentName + "</span></li>");
                            }
                            sb.Append("</ul>");
                        }

                        string comments = lst[0].Comments;
                        comments = comments.Replace("\n", "<br/>");
                        sb.Append("<div class=\"table_container\">");
                        sb.Append("<h4 class=\"sub_title\"  >Comments</h4>");
                        sb.Append("<div class=\"comments\" ><label>" + comments + "</label></div>");
                        sb.Append("</div>");


                        sb.Append(" </div></div>");
                        ///Message Body End
                        ///
                        sb.Append("</div>");
                    }
                    #endregion

                    if (IsRead == "0")
                    {
                        if (Onload != "Onload")
                        {
                            ObjColleaguesData.UpdateInBoxReadStatus(MessageTypeId, MessageId);
                        }

                    }
                }
                else
                {
                    #region MessageBody


                    List<MessageDetails> lst = new List<MessageDetails>();
                    MdlColleagues = new mdlColleagues();
                    DataTable AttachedPatientDetials = new DataTable();
                    lst = MdlColleagues.GetMessageDetailsById(Convert.ToInt32(Session["UserId"]), MessageId, MessageTypeId,SessionManagement.TimeZoneSystemName);
                    if (lst.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(lst[0].AttachedPatientsId))
                        {
                            AttachedPatientDetials = ObjColleaguesData.GetPatientAttachedDetails(lst[0].AttachedPatientsId);
                        }
                    }

                    if (lst.Count > 0)
                    {
                        ///



                        sb.Append(" <div class=\"inbox_icons clearfix\">");

                        if (MessageTypeId == 0)
                        {
                            sb.Append(" <a href=\"" + Url.Action("PatientMessageCompose", "Patients", new { PatientId = lst[0].SenderId, Type = "reply" }) + "\" title=\"Reply, Reply All\" class=\"inboxReply marginLeft0\"></a>");
                            sb.Append("<a href=\"" + Url.Action("Compose", "Inbox") + "\" title=\"Compose\" class=\"inboxCompose\"></a>");
                            sb.Append("<a href=\"javascript:;\" onclick=\"RemovePatientMessageFromInboxOfDoctor('" + lst[0].MessageId + "','" + NextId + "','" + NextMessagetypeId + "')\" title=\"Delete\" class=\"inboxDltMsg\"></a>");
                        }
                        else
                        {
                            sb.Append(" <a href=\"" + Url.Action("Compose", "Inbox", new { ColleaguesId = lst[0].SenderId, Type = "reply" }) + "\" title=\"Reply, Reply All\" class=\"inboxReply marginLeft0\"></a>");
                            sb.Append("<a href=\"" + Url.Action("Compose", "Inbox") + "\" title=\"Compose\" class=\"inboxCompose\"></a>");
                            sb.Append("<a href=\"javascript:;\" onclick=\"RemoveMessageFromInboxOfDoctor('" + lst[0].MessageId + "','" + NextId + "','" + NextMessagetypeId + "')\" title=\"Delete\" class=\"inboxDltMsg\"></a>");
                        }


                        sb.Append("</div>");

                        sb.Append("<h3>&nbsp;<span>&nbsp");


                        sb.Append("</span></h3>");

                        sb.Append("<ul class=\"list\">");
                        sb.Append("<li></li>");
                        sb.Append("<li><label>From:</label><a class=\"hylyt_field\">" + lst[0].From + "<span>&lt;" + lst[0].SenderEmail + "&gt;</span></a></li>");
                        sb.Append("<li><label>To:</label>" + lst[0].To + " &lt;" + lst[0].ReceiverEmail + "&gt;</li>");
                        sb.Append("<li><label>Date:</label>" + lst[0].CreationDate + "</li>");

                        if (lst[0].lstMessageAttachment.Count > 0)
                        {
                            sb.Append("<li>");

                            foreach (var item in lst[0].lstMessageAttachment)
                            {
                                string AttachmentName = string.Empty;
                                AttachmentName = item.AttachmentName;
                                AttachmentName = AttachmentName.Substring(AttachmentName.ToString().LastIndexOf("$") + 1);
                                string extension = Path.GetExtension(AttachmentName);
                                string Type = string.Empty;

                                if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd" && extension.ToLower() != ".pspimage" && extension.ToLower() != ".thm" && extension.ToLower() != ".tif")
                                {
                                    Type = "Doc";
                                }
                                else
                                {
                                    Type = "Image";
                                }
                                sb.Append("<span title=\"" + AttachmentName + "\" onclick=\"Attachementpopup('" + ImageBankFolder + item.AttachmentName + "','" + Type + "')\" style=\"cursor: pointer;\" ><i class=\"attach\"></i> " + AttachmentName + "</span>");


                            }
                            sb.Append("</li>");
                            sb.Append("<a href=\"/Inbox/DownloadAttachmentofMessage?DownId=" + MessageId + "\"><img src=\"/Content/images/print-all-forms.png\" style=\"float:right;margin-top:-10px;cursor:pointer;\"/></a>");
                        }
                        string inboxbody = lst[0].Body;
                        inboxbody = inboxbody.Replace("\n", "<br/>");
                        sb.Append("</ul>");
                        sb.Append("<table style='width:100%'><tr><td>");
                        sb.Append("<div class=\"message_content\" style=\"float: left; max-width: 100%; position: relative;\">" + inboxbody + "");
                        if (AttachedPatientDetials.Rows.Count == 0)
                        {
                            sb.Append("</td></tr></table>");
                        }


                        if (AttachedPatientDetials.Rows.Count > 0)
                        {
                            sb.Append("</td></tr><tr><td>");
                            // sb.Append("<lable>Attached Patient (Click on patient to view history)</lable>");
                            sb.Append("</td></tr><tr><td>");
                            sb.Append("<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" class=\"patientTable\"><thead><tr><th style=\"width:25% !important;\">First Name</th><th> Last Name</th></tr></thead> ");
                            sb.Append("<tbody>");
                            foreach (DataRow item in AttachedPatientDetials.Rows)
                            {
                                sb.Append("<tr>");
                                sb.Append("<td><a href=\"javascript:;\" onclick=\"getAttachedPatientHistory(" + item["Patientid"] + ")\">" + item["FirstName"] + "</a></td>");
                                sb.Append("<td><a href=\"javascript:;\" onclick=\"getAttachedPatientHistory(" + item["Patientid"] + ")\">" + item["LastName"] + "</a></td>");
                                sb.Append("</tr>");
                            }
                            sb.Append("</tbody>");

                            sb.Append("</table></td></tr></table>");
                        }
                        sb.Append("</div>");
                        if (IsRead == "0")
                        {
                            if (Onload != "Onload")
                            {
                                if (MessageTypeId == 0)
                                {
                                    ObjColleaguesData.EditPatientMessageReadStatus(MessageId, "Read_Flag");
                                }
                                else
                                {
                                    ObjColleaguesData.UpdateInBoxReadStatus(1, MessageId);
                                }
                            }
                        }
                    }
                    else
                    {
                        sb.Append("<center>This message has been deleted</center>");
                    }
                    #endregion
                }

            }
            catch (Exception ex)
            {
                bool status = ObjCommon.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                throw;
            }
            return sb.ToString();
        }
        [HttpPost]
        public JsonResult InBoxMessageShowOnClick(int MessageId, int NextId, int MessageTypeId, int NextMessagetypeId, string IsRead, string Onload)
        {
            MdlColleagues = new mdlColleagues();
            return Json(GetMessageBody(MessageId, NextId, MessageTypeId, NextMessagetypeId, IsRead, Onload), JsonRequestBehavior.AllowGet);
        }


        public string RemoveMessageFromInboxOfDoctor(int MessageId, string NextId, string NextMessagTypeId, string PageIndex)
        {
            object obj = string.Empty;
            bool result = false;
            string message = null;
            string inboxlst = null;
            SerializeObjForInbox obj1 = new SerializeObjForInbox();
            try
            {
                result = ObjColleaguesData.RemoveMessageFromInboxOfDoctor(Convert.ToInt32(Session["UserId"]), MessageId);
                if (result)
                {
                    if (MessageId.ToString() != NextId)
                    {
                        if (string.IsNullOrEmpty(NextId))
                        {
                            NextId = "-1";
                        }
                        if (string.IsNullOrEmpty(NextMessagTypeId))
                        {
                            NextMessagTypeId = "-1";
                        }
                        //  inboxlst = GetInboxOfDoctor(Convert.ToInt32(Session["UserId"]), (!string.IsNullOrEmpty(PageIndex) ? Convert.ToInt16(PageIndex) : 1), 1, 2, "", "click", "");
                        message = GetMessageBody(Convert.ToInt32(NextId), null, Convert.ToInt32(NextMessagTypeId), null, "0", "click");
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
            // return Json(obj, JsonRequestBehavior.AllowGet);
            obj1.MessageBody = message;
            obj1.InboxList = inboxlst;
            var json = new JavaScriptSerializer().Serialize(obj1);
            return json.ToString();
        }



        public string RemovePatientMessageFromInboxOfDoctor(int MessageId, string NextId, string NextMessagTypeId, string PageIndex)
        {
            object obj = string.Empty;
            bool result = false;
            string message = null;
            string inboxlst = null;

            SerializeObjForInbox obj1 = new SerializeObjForInbox();
            try
            {
                result = ObjColleaguesData.RemovePateintMessageFromInBoxOfDoctor(Convert.ToInt32(Session["UserId"]), MessageId);
                if (result)
                {
                    if (MessageId.ToString() != NextId)
                    {
                        if (string.IsNullOrEmpty(NextId))
                        {
                            NextId = "-1";
                        }
                        if (string.IsNullOrEmpty(NextMessagTypeId))
                        {
                            NextMessagTypeId = "-1";
                        }
                        // inboxlst = GetInboxOfDoctor(Convert.ToInt32(Session["UserId"]), (!string.IsNullOrEmpty(PageIndex) ? Convert.ToInt16(PageIndex) : 1), 1, 2, "", "click", "");
                        message = GetMessageBody(Convert.ToInt32(NextId), null, Convert.ToInt32(NextMessagTypeId), null, "0", "click");

                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
            obj1.MessageBody = message;
            obj1.InboxList = inboxlst;
            var json = new JavaScriptSerializer().Serialize(obj1);
            return json.ToString();
        }


        public string RemoveReferralFromInboxOfDoctor(int MessageId, string NextId, string NextMessagTypeId)
        {
            object obj = string.Empty;
            bool result = false;
            string message = null;
            string inboxlst = null;
            SerializeObjForInbox obj1 = new SerializeObjForInbox();
            try
            {
                result = ObjColleaguesData.RemovereferralFromDoctorInbox(Convert.ToInt32(Session["UserId"]), MessageId);
                if (result)
                {
                    if (MessageId.ToString() != NextId)
                    {
                        if (string.IsNullOrEmpty(NextId))
                        {
                            NextId = "-1";
                        }
                        if (string.IsNullOrEmpty(NextMessagTypeId))
                        {
                            NextMessagTypeId = "-1";
                        }
                        // inboxlst = GetInboxOfDoctor(Convert.ToInt32(Session["UserId"]), (!string.IsNullOrEmpty(PageIndex) ? Convert.ToInt16(PageIndex) : 1), 1, 2, "", "click", "");
                        message = GetMessageBody(Convert.ToInt32(NextId), null, Convert.ToInt32(NextMessagTypeId), null, "0", "click");
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            obj1.MessageBody = message;
            obj1.InboxList = inboxlst;
            var json = new JavaScriptSerializer().Serialize(obj1);
            return json.ToString();

        }
        #endregion






        #region Sent Box

        public ActionResult sent(int? MessageId)
        {
            mdlColleagues MdlColleagues = new mdlColleagues();
            try
            {
                if (SessionManagement.UserId != null && SessionManagement.UserId != "")
                {

                    ViewBag.SentboxOfDoctor = GetSentBoxOfDoctor(Convert.ToInt32(Session["UserId"]), 1, 1, 2, null, null, SessionManagement.TimeZoneSystemName);
                    if (Request["Referral"] != null)
                    {
                        ViewBag.IsReferralSent = Request["Referral"].ToString();
                    }
                    if (Request["Email"] != null)
                    {
                        ViewBag.Searchsentbox = Request["Email"].ToString();
                    }
                    MdlColleagues.lstSentboxOfDoctor = MdlColleagues.GetSentboxOfDoctor(Convert.ToInt32(Session["UserId"]), 1, PageSizeMaster, 1, 2, null, null, SessionManagement.TimeZoneSystemName);

                    //if (MdlColleagues.lstSentboxOfDoctor.Count > 0)
                    //{
                    //    ViewBag.SentMessageDetailsById = GetSentMessageBody(MdlColleagues.lstSentboxOfDoctor[0].MessageId, (MdlColleagues.lstSentboxOfDoctor.Count > 2 ? MdlColleagues.lstSentboxOfDoctor[1].MessageId : 0), MdlColleagues.lstSentboxOfDoctor[0].MessageTypeId, (MdlColleagues.lstSentboxOfDoctor.Count > 2 ? MdlColleagues.lstSentboxOfDoctor[0].MessageTypeId : 0));
                    //}
                    //else
                    //{
                    //    ViewBag.SentMessageDetailsById = null;
                    //}


                    return View("PartialSent", MdlColleagues);
                    // }
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }
            }
            catch (Exception ex)
            {
                bool status = ObjCommon.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                throw;
            }
        }

        [HttpPost]
        public ActionResult SentboxPageing(int PageIndex, int OrderByID, int OrderBy, string Searchtext, string IsReferral)
        {
            //MdlColleagues = new mdlColleagues();
            //return Json(GetSentBoxOfDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, OrderByID, OrderBy, Searchtext, IsReferral), JsonRequestBehavior.AllowGet);
            MdlColleagues = new mdlColleagues();
            List<MessageListOfDoctor> lst = new List<MessageListOfDoctor>();
            ViewBag.Sorting = "true";
            lst = GetSentBoxOfDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, OrderByID, OrderBy, Searchtext, IsReferral, SessionManagement.TimeZoneSystemName);
            return View("PartialSentBox", lst);
        }
        public List<MessageListOfDoctor> GetSentBoxOfDoctor(int UserId, int PageIndex, int OrderByID, int OrderBy, string Searchtext, string IsReferral, string TimeZoneSystemName)
        {
            MdlColleagues = new mdlColleagues();
            MdlPatient = new mdlPatient();
            ViewBag.PageIndex = PageIndex;
            ViewBag.OrderByID = OrderByID;
            ViewBag.OrderBy = OrderBy;
            ViewBag.Searchtext = Searchtext;
            ViewBag.IsRefarral = IsReferral;
            List<MessageListOfDoctor> lst = new List<MessageListOfDoctor>();
            lst = MdlColleagues.GetSentboxOfDoctor(UserId, PageIndex, PageSizeMaster, OrderByID, OrderBy, Searchtext, IsReferral, TimeZoneSystemName);
            if (lst.Count > 0)
                ViewBag.MessageId = lst[0].MessageId;
            else
                ViewBag.MessageId = 0;
            return lst;
            //try
            //{
            //    List<MessageListOfDoctor> lst = new List<MessageListOfDoctor>();
            //    int OrderbyTest;

            //    if (Searchtext == "")
            //    {
            //        Searchtext = null;
            //    }
            //    lst = MdlColleagues.GetSentboxOfDoctor(UserId, PageIndex, PageSizeMaster, OrderByID, OrderBy, Searchtext, IsReferral);


            //    if (OrderBy == 1)
            //    {
            //        OrderbyTest = 2;

            //    }
            //    else
            //    {
            //        OrderbyTest = 1;
            //    }

            //    StringBuilder sb = new StringBuilder();
            //    if (lst.Count > 0)
            //    {
            //        sb.Append(" <div id=\"GetPageIndex\" lbl='" + PageIndex + "' class=\"msgtab\" >");
            //        sb.Append("<div class=\"sort_inbox clearfix\">");
            //        sb.Append("<select id=\"standard-dropdown\" name=\"standard-dropdown\"  onchange=\"SentboxPageing('" + PageIndex + "','" + OrderByID + "','" + OrderbyTest + "','" + Searchtext + "')\" class=\"select_menu_upload custom-class1 custom-class2 right\" style=\"width: 150px;\">");
            //        if (OrderByID == 1)
            //        {
            //            sb.Append("<option value=\"1\" selected=\"selected\" class=\"test-class-1\">Sort by Date</option>");
            //        }
            //        else
            //        {
            //            sb.Append("<option value=\"1\" class=\"test-class-1\">Sort by Date</option>");
            //        }


            //        if (OrderByID == 3)
            //        {
            //            sb.Append("<option value=\"3\" selected=\"selected\" class=\"test-class-3\">Sort by Sender</option>");
            //        }
            //        else
            //        {
            //            sb.Append("<option value=\"3\" class=\"test-class-3\">Sort by Sender</option>");
            //        }


            //        sb.Append("</select>");
            //        sb.Append("</div></div><div id=\"GetScrollDetails\" data-orderbyid=\"" + OrderByID + "\" data-orderby=\"" + OrderBy + "\" data-searchtext=\"" + Searchtext + "\" data-isrefarral=\"" + IsReferral + "\" ></div>");
            //        int j = 0;
            //        for (int i = 0; i < lst.Count; i++)
            //        {
            //            if (j < lst.Count - 1)
            //            {
            //                ++j;
            //            }

            //            sb.Append("<div class=\"msgtab\" id=" + lst[i].MessageId + ">");
            //            sb.Append("<h4 style=\"cursor:pointer;\" onclick=\"SentBoxMessageShowOnClick('" + lst[i].MessageId + "','" + lst[j].MessageId + "','" + lst[i].MessageTypeId + "','" + lst[j].MessageTypeId + "')\"><span style=\"font-weight:17.5px;\">" + lst[i].Field + "</span>");

            //            if (lst[i].AttachmentId != null && lst[i].AttachmentId != "" && lst[i].AttachmentId != "0")
            //            {
            //                sb.Append("<i class=\"attach\"></i>");
            //            }



            //            sb.Append("<span class=\"date\" style=\"cursor:pointer;\" onclick=\"SentBoxMessageShowOnClick('" + lst[i].MessageId + "','" + lst[j].MessageId + "','" + lst[i].MessageTypeId + "','" + lst[j].MessageTypeId + "')\">" + lst[i].CreationDate.ToString(System.Configuration.ConfigurationManager.AppSettings["DateTimeWithoutsec"]) + "</span></h4>");
            //            sb.Append("<ul class=\"list\">");

            //            if (lst[i].Body.Contains("Refer Patient"))
            //            {
            //                lst[i].Body = "Patient Referral for ";
            //            }
            //            if (lst[i].MessageTypeId == 2)
            //            {
            //                List<ReferralDetails> lst1 = new List<ReferralDetails>();
            //                lst1 = MdlPatient.GetRefferalDetailsById(Convert.ToInt32(Session["UserId"]), lst[i].MessageId, "Inbox");
            //                if (lst1.Count > 0)
            //                {
            //                    sb.Append("<li style=\"cursor:pointer;\" onclick=\"SentBoxMessageShowOnClick('" + lst[i].MessageId + "','" + lst[j].MessageId + "','" + lst[i].MessageTypeId + "','" + lst[j].MessageTypeId + "')\">" + (lst[i].Body.ToString().Length > 40 ? lst[i].Body.Substring(0, 40) + "..." : lst[i].Body) + lst1[0].FullName + "</li>");
            //                }
            //            }
            //            else
            //            {
            //                sb.Append("<li style=\"cursor:pointer;\" onclick=\"SentBoxMessageShowOnClick('" + lst[i].MessageId + "','" + lst[j].MessageId + "','" + lst[i].MessageTypeId + "','" + lst[j].MessageTypeId + "')\">" + (lst[i].Body.ToString().Length > 40 ? lst[i].Body.Substring(0, 40) + "..." : lst[i].Body) + "</li>");
            //            }

            //            sb.Append(" </ul></div>");
            //        }


            //    }
            //    else
            //    {

            //        sb.Append("<center><br />No messages found");
            //    }

            //    sb.Append("</center>");
            //    return sb.ToString();
            //}
            //catch (Exception ex)
            //{
            //    bool status = ObjCommon.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
            //    throw;
            //}

        }

        public ActionResult GetSentBoxOfDoctorOnScroll(int PageIndex, int OrderByID, int OrderBy, string Searchtext, string IsReferral)
        {
            List<MessageListOfDoctor> lst = new List<MessageListOfDoctor>();
            MdlColleagues = new mdlColleagues();
            ViewBag.PageIndex = PageIndex;
            ViewBag.OrderByID = OrderByID;
            ViewBag.OrderBy = OrderBy;
            ViewBag.Searchtext = Searchtext;
            ViewBag.IsRefarral = IsReferral;
            lst = MdlColleagues.GetSentboxOfDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, PageSizeMaster, OrderByID, OrderBy, Searchtext, IsReferral, SessionManagement.TimeZoneSystemName);
            return View("PartialSentBox", lst);
            //if (lst.Count > 0)
            //{
            //    int j = 0;
            //    for (int i = 0; i < lst.Count; i++)
            //    {
            //        if (j < lst.Count - 1)
            //        {
            //            ++j;
            //        }

            //        sb.Append("<div class=\"msgtab\" id=" + lst[i].MessageId + ">");
            //        sb.Append("<h4 style=\"cursor:pointer;\" onclick=\"SentBoxMessageShowOnClick('" + lst[i].MessageId + "','" + lst[j].MessageId + "','" + lst[i].MessageTypeId + "','" + lst[j].MessageTypeId + "')\"><span style=\"font-weight:17.5px;\">" + lst[i].Field + "</span>");

            //        if (lst[i].AttachmentId != null && lst[i].AttachmentId != "" && lst[i].AttachmentId != "0")
            //        {
            //            sb.Append("<i class=\"attach\"></i>");
            //        }



            //        sb.Append("<span class=\"date\" style=\"cursor:pointer;\" onclick=\"SentBoxMessageShowOnClick('" + lst[i].MessageId + "','" + lst[j].MessageId + "','" + lst[i].MessageTypeId + "','" + lst[j].MessageTypeId + "')\">" + lst[i].CreationDate.ToString(System.Configuration.ConfigurationManager.AppSettings["DateTimeWithoutsec"]) + "</span></h4>");
            //        sb.Append("<ul class=\"list\">");

            //        if (lst[i].Body.Contains("Refer Patient"))
            //        {
            //            lst[i].Body = "Patient Referral for ";
            //        }
            //        if (lst[i].MessageTypeId == 2)
            //        {
            //            List<ReferralDetails> lst1 = new List<ReferralDetails>();
            //            lst1 = MdlPatient.GetRefferalDetailsById(Convert.ToInt32(Session["UserId"]), lst[i].MessageId, "Inbox");
            //            if (lst1.Count > 0)
            //            {
            //                sb.Append("<li style=\"cursor:pointer;\" onclick=\"SentBoxMessageShowOnClick('" + lst[i].MessageId + "','" + lst[j].MessageId + "','" + lst[i].MessageTypeId + "','" + lst[j].MessageTypeId + "')\">" + (lst[i].Body.ToString().Length > 40 ? lst[i].Body.Substring(0, 40) + "..." : lst[i].Body) + lst1[0].FullName + "</li>");
            //            }
            //        }
            //        else
            //        {
            //            sb.Append("<li style=\"cursor:pointer;\" onclick=\"SentBoxMessageShowOnClick('" + lst[i].MessageId + "','" + lst[j].MessageId + "','" + lst[i].MessageTypeId + "','" + lst[j].MessageTypeId + "')\">" + (lst[i].Body.ToString().Length > 40 ? lst[i].Body.Substring(0, 40) + "..." : lst[i].Body) + "</li>");
            //        }


            //        sb.Append(" </ul></div>");
            //    }
            //}
            //else
            //{
            //    sb.Append("<center>No more record found</center>");
            //}

        }
        public ActionResult GetMessageBodayOfSentReferrals(int MessageId, int? NextMessageId, int MessageTypeId, int? NextMessageTypeId)
        {
            try
            {
                MdlPatient = new mdlPatient();
                ViewBag.MessageTypeId = MessageTypeId;
                ViewBag.NextMessageId = NextMessageId;
                ViewBag.NextMessageTypeId = NextMessageTypeId;
                MdlColleagues = new mdlColleagues();
                List<ReferralDetails> lstRefferalDetails = new List<ReferralDetails>();
                MdlColleagues.lstRefferalDetails = MdlPatient.GetRefferalDetailsById(Convert.ToInt32(Session["UserId"]), MessageId, "Sentbox", SessionManagement.TimeZoneSystemName);
                ViewBag.ReferralCardId = MdlPatient.ReferralCardId;
                ViewBag.EndMessageTime = DateTime.Now;
                return View(MdlColleagues.lstRefferalDetails);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public ActionResult GetPlainMessageBodyOfSentBox(int MessageId, int? NextMessageId, int MessageTypeId, int? NextMessageTypeId)
        {
            try
            {
                MdlColleagues = new mdlColleagues();
                ViewBag.MessageTypeId = MessageTypeId;
                ViewBag.NextMessageId = NextMessageId;
                ViewBag.NextMessageTypeId = NextMessageTypeId;
                List<MessageDetails> lstMessageDetails = new List<MessageDetails>();
                MdlColleagues.lstMessageDetails = MdlColleagues.GetSentMessageDetailsById(Convert.ToInt32(Session["UserId"]), MessageId, MessageTypeId, SessionManagement.TimeZoneSystemName);
                ViewBag.EndMessageTime = DateTime.Now;
                return View(MdlColleagues.lstMessageDetails.Take(1).ToList());
            }
            catch (Exception)
            {

                throw;
            }
        }
        public ActionResult PartialViewForReferralcategoryvalues(int RefCardId)
        {
            try
            {
                MdlColleagues = new mdlColleagues();
                MdlColleagues.NewReferralView = MdlColleagues.GetNewReferralViewByRefCardId(Convert.ToInt32(RefCardId));
                return View("PartialNewReferral", MdlColleagues.NewReferralView);

            }
            catch (Exception EX)
            {
                ObjCommon.InsertErrorLog(Request.Url.ToString(), EX.Message, EX.StackTrace);
                throw;
            }

        }
        public ActionResult GetMessageBodyOfInboxReferrals(int MessageId, int? NextMessageId, int MessageTypeId, int? NextMessageTypeId, string IsRead, string Onload)
        {
            try
            {
                MdlPatient = new mdlPatient();
                ViewBag.MessageTypeId = MessageTypeId;
                ViewBag.NextMessageId = NextMessageId;
                ViewBag.NextMessageTypeId = NextMessageTypeId;
                MdlColleagues = new mdlColleagues();
                List<ReferralDetails> lstRefferalDetails = new List<ReferralDetails>();
                MdlColleagues.lstRefferalDetails = MdlPatient.GetRefferalDetailsById(Convert.ToInt32(Session["UserId"]), MessageId, "Inbox", SessionManagement.TimeZoneSystemName);
                ViewBag.ReferralCardId = MdlPatient.ReferralCardId;
                ViewBag.EndMessageTime = DateTime.Now;
                return View(MdlColleagues.lstRefferalDetails);

            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                throw;
            }
        }
        public ActionResult GetPlainMessageBodyOfInbox(int MessageId, int? NextMessageId, int MessageTypeId, int? NextMessageTypeId, string IsRead, string Onload)
        {

            MdlColleagues = new mdlColleagues();
            ViewBag.MessageTypeId = MessageTypeId;
            ViewBag.NextMessageId = NextMessageId;
            ViewBag.NextMessageTypeId = NextMessageTypeId;
            List<MessageDetails> lstMessageDetails = new List<MessageDetails>();
            MdlColleagues.lstMessageDetails = MdlColleagues.GetMessageDetailsById(Convert.ToInt32(Session["UserId"]), MessageId, MessageTypeId,SessionManagement.TimeZoneSystemName);
            ViewBag.EndMessageTime = DateTime.Now;
            return View(MdlColleagues.lstMessageDetails.Take(1).ToList());
        }
        // [HttpPost]
        //public JsonResult InBoxMessageShowOnClickbyView(int MessageId, int NextId, int MessageTypeId, int NextMessagetypeId, string IsRead, string Onload)
        //{
        //    MdlColleagues = new mdlColleagues();
        //  //  var data = GetMessageBodyOfSentAndInboxReferrals(MessageId, NextId, MessageTypeId, NextMessagetypeId);
        //    return Json(GetMessageBodyOfSentAndInboxReferrals(MessageId, NextId, MessageTypeId, NextMessagetypeId), JsonRequestBehavior.AllowGet);
        //}
        public string GetSentMessageBody(int MessageId, int? NextMessageId, int MessageTypeId, int? NextMessageTypeId)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                if (MessageTypeId == 2)
                {
                    #region Referral Body


                    MdlPatient = new mdlPatient();
                    List<ReferralDetails> lst = new List<ReferralDetails>();
                    lst = MdlPatient.GetRefferalDetailsById(Convert.ToInt32(Session["UserId"]), MessageId, "Sentbox", SessionManagement.TimeZoneSystemName);

                    if (lst.Count > 0)
                    {




                        ///Message Body Start

                        sb.Append("<div class=\"message_content\" style=\"float: left; width: 100%; position: relative;\">");
                        sb.Append("<div class=\"inner\">");
                        sb.Append("<div id=\"view_referrals\">");

                        sb.Append("<div class=\"patient_info\">");
                        sb.Append("<h4 class=\"sub_title\" style=\"cursor:pointer;\" onclick=\"ViewPatientHistory('" + lst[0].PatientId + "')\">");
                        sb.Append(lst[0].FullName + "<span>" + (!string.IsNullOrEmpty(lst[0].AssignedPatientId) ? "(" + lst[0].AssignedPatientId + ")" : string.Empty) + "</span><a style=\"float:right; \" class=\"inboxDltMsg\" title=\"Delete\" onclick=\"RemoveReferralFromSentOfDoctor('" + lst[0].MessageId + "','" + NextMessageId + "','" + NextMessageTypeId + "')\" href=\"javascript:;\"></a></h4>");
                        sb.Append("<ul class=\"patient_info\">");
                        sb.Append("<li class=\"first\" style=\"width:57% !important\">");
                        if (!String.IsNullOrEmpty(lst[0].ProfileImage))
                            sb.Append("<figure class=\"thumb\"><img width=\"104\" height=\"104\" alt=\"Ashlyn\" src=" + ImageBankFolder + lst[0].ProfileImage + "></figure>");
                        else
                            sb.Append("<figure class=\"thumb\"><img width=\"104\" height=\"104\" alt=\"Ashlyn\" src=" + DefaultDoctorImage + "></figure>");
                        sb.Append("<ul class=\"list\">");
                        var Datetime = lst[0].CreationDate.Split(' ');
                        var CreationDate = Datetime[0];
                        var CreationTime = Datetime[1] + Datetime[2];
                        sb.Append("<li><label>Referral sent date:</label>" + (!string.IsNullOrEmpty(lst[0].CreationDate) ? Convert.ToDateTime(lst[0].CreationDate).ToString(System.Configuration.ConfigurationManager.AppSettings["DateFormateMMMddyyyy"]) : "") + "</li>");
                        sb.Append("<li><label>Referred by:</label>" + lst[0].ReceiverName + "</li>");
                        sb.Append("<li><label>Referred to:</label>" + lst[0].SenderName + "</li>");
                        sb.Append("<li><label>Location:</label><label  style=\"width:111px !important;line-height: 17px;\">" + lst[0].Location + "</label></li></ul>");
                        sb.Append("<div class=\"clear\"></div>");
                        sb.Append("</li>");
                        sb.Append("<li class=\"second\" style=\"width:42.5% !important\">");
                        sb.Append("<ul class=\"list\">");
                        //if (!String.IsNullOrEmpty(lst[0].FullAddress))
                        //    sb.Append("<li><i class=\"address\"></i>" + lst[0].FullAddress + "</li>");
                        //if (!String.IsNullOrEmpty(lst[0].Phone))
                        //    sb.Append("<li><i class=\"wphone\"></i>" + lst[0].Phone + "</li>");
                        if (!String.IsNullOrEmpty(lst[0].EmailAddress))
                            sb.Append("<li><i class=\"email\"></i>" + lst[0].EmailAddress + "</li>");

                        sb.Append("<li><label>Gender:</label>" + lst[0].Gender + "</li>");
                        sb.Append("<li><label>Date of Birth:</label>" + (!string.IsNullOrEmpty(lst[0].DateOfBirth) ? Convert.ToDateTime(lst[0].DateOfBirth).ToString(System.Configuration.ConfigurationManager.AppSettings["DateFormateMMMddyyyy"]) : "") + "</li>");
                        sb.Append("</ul>");
                        sb.Append("</li>");
                        sb.Append("<br class=\"clear\">");
                        sb.Append("</ul>");
                        sb.Append("<div class=\"clear\">");
                        sb.Append("</div>");
                        sb.Append("</div>");

                        sb.Append("<div class=\"table_container\">");
                        sb.Append("<h4 class=\"sub_title\">");
                        sb.Append("Regarding</h4>");
                        sb.Append("<ul class=\"check_listing\">");


                        string[] strRegardingNo = lst[0].RegardOption.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (String item in strRegardingNo)
                        {
                            if (!string.IsNullOrEmpty(item))
                            {
                                if ((Convert.ToInt32(item) - 1) < MdlPatient.ReferralRegarding.Length)
                                    sb.Append("<li><label>" + "" + MdlPatient.ReferralRegarding[Convert.ToInt32(item) - 1] + "</label></li>");
                            }

                        }
                        if (!String.IsNullOrEmpty(lst[0].OtherComments))
                            sb.Append("<br/><li><label>" + "" + lst[0].OtherComments + "</label></li>");


                        sb.Append("</ul>");
                        sb.Append("</div>");

                        sb.Append("<div class=\"table_container\">");
                        sb.Append(" <h4 class=\"sub_title\">");
                        sb.Append("Request</h4>");
                        sb.Append("<ul class=\"check_listing\">");



                        string[] strRequest = lst[0].RequestingOption.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (String item in strRequest)
                        {
                            if (!string.IsNullOrEmpty(item))
                            {
                                if ((Convert.ToInt32(item) - 1) < MdlPatient.ReferralRequest.Length)
                                    sb.Append("<li><label>" + "" + MdlPatient.ReferralRequest[Convert.ToInt32(item) - 1] + "</label></li>");
                            }
                        }

                        if (!String.IsNullOrEmpty(lst[0].RequestComments))
                            sb.Append("<li><label>" + "" + lst[0].RequestComments + "</label></li>");


                        sb.Append("</ul>");
                        sb.Append("</div>");

                        sb.Append("<div class=\"table_container\">");
                        sb.Append("<h4 class=\"sub_title\">");
                        sb.Append(" Visual Instructions</h4>");
                        sb.Append(" <p>Select the tooth or teeth that indicates the area of concern.</p>");
                        sb.Append("<div class=\"visuals\">");
                        sb.Append("<div class=\"left panel\">");
                        sb.Append("<h5>Permanent Teeth</h5><div class=\"block blockborder\">");
                        sb.Append("<p>Right</p>");
                        sb.Append("<ul class=\"teeth_listing\">");
                        sb.Append("<div style=\"border-bottom:1px solid #ccc; padding-bottom: 15px;\">");
                        ///Permanent Teeth Right 
                        ///

                        Dictionary<int, String> dictPermanentRight = new Dictionary<int, string>();
                        dictPermanentRight.Add(1, "<li><p>1</p><input type=\"checkbox\" disabled id=\"1\"/></li>");
                        dictPermanentRight.Add(2, "<li><p>2</p><input type=\"checkbox\" disabled id=\"2\"/></li>");
                        dictPermanentRight.Add(3, "<li><p>3</p><input type=\"checkbox\" disabled id=\"3\"/></li>");
                        dictPermanentRight.Add(4, "<li><p>4</p><input type=\"checkbox\" disabled id=\"4\"/></li>");
                        dictPermanentRight.Add(5, "<li><p>5</p><input type=\"checkbox\" disabled id=\"5\"/></li>");
                        dictPermanentRight.Add(6, "<li><p>6</p><input type=\"checkbox\" disabled id=\"6\"/></li>");
                        dictPermanentRight.Add(7, "<li><p>7</p><input type=\"checkbox\" disabled id=\"7\"/></li>");
                        dictPermanentRight.Add(8, "<li><p>8</p><input type=\"checkbox\" disabled id=\"8\"/></li></div>");
                        dictPermanentRight.Add(32, "<li><p>32</p><input type=\"checkbox\" disabled id=\"32\"/></li>");
                        dictPermanentRight.Add(31, "<li><p>31</p><input type=\"checkbox\" disabled id=\"31\"/></li>");
                        dictPermanentRight.Add(30, "<li><p>30</p><input type=\"checkbox\" disabled id=\"30\"/></li>");
                        dictPermanentRight.Add(29, "<li><p>29</p><input type=\"checkbox\" disabled id=\"29\"/></li>");
                        dictPermanentRight.Add(28, "<li><p>28</p><input type=\"checkbox\" disabled id=\"28\"/></li>");
                        dictPermanentRight.Add(27, "<li><p>27</p><input type=\"checkbox\" disabled id=\"27\"/></li>");
                        dictPermanentRight.Add(26, "<li><p>26</p><input type=\"checkbox\" disabled id=\"26\"/></li>");
                        dictPermanentRight.Add(25, "<li><p>25</p><input type=\"checkbox\" disabled id=\"25\"/></li>");

                        foreach (ToothList objToothList in lst[0].lstToothList)
                        {

                            if (dictPermanentRight.ContainsKey(objToothList.ToothType))
                            {
                                if (objToothList.ToothType.ToString() == "8")
                                {
                                    dictPermanentRight[objToothList.ToothType] = "<li class=\"active\"><p>" + objToothList.ToothType.ToString() + "</p><input type=\"checkbox\" disabled checked=\"checked\" id=\"" + objToothList.ToothType.ToString() + "\"/></li></div>";
                                }
                                else
                                {
                                    dictPermanentRight[objToothList.ToothType] = "<li class=\"active\"><p>" + objToothList.ToothType.ToString() + "</p><input type=\"checkbox\" disabled checked=\"checked\" id=\"" + objToothList.ToothType.ToString() + "\"/></li>";
                                }
                            }
                        }

                        sb.Append(GetLine(dictPermanentRight));
                        sb.Append("</ul>");
                        sb.Append("</div>");

                        ///Permanent Teeth Left
                        ///

                        sb.Append("<div class=\"block\">");
                        sb.Append("<p>Left</p>");
                        sb.Append("<ul class=\"teeth_listing\">");
                        sb.Append("<div style=\"border-bottom:1px solid #ccc; padding-bottom: 15px;\">");

                        Dictionary<int, String> dictPermanentLeft = new Dictionary<int, string>();
                        dictPermanentLeft.Add(9, "<li><p>9</p><input type=\"checkbox\" disabled id=\"9\"/></li>");
                        dictPermanentLeft.Add(10, "<li><p>10</p><input type=\"checkbox\" disabled id=\"10\"/></li>");
                        dictPermanentLeft.Add(11, "<li><p>11</p><input type=\"checkbox\" disabled id=\"11\"/></li>");
                        dictPermanentLeft.Add(12, "<li><p>12</p><input type=\"checkbox\" disabled id=\"12\"/></li>");
                        dictPermanentLeft.Add(13, "<li><p>13</p><input type=\"checkbox\" disabled id=\"13\"/></li>");
                        dictPermanentLeft.Add(14, "<li><p>14</p><input type=\"checkbox\" disabled id=\"14\"/></li>");
                        dictPermanentLeft.Add(15, "<li><p>15</p><input type=\"checkbox\" disabled id=\"15\"/></li>");
                        dictPermanentLeft.Add(16, "<li><p>16</p><input type=\"checkbox\" disabled id=\"16\"/></li></div>");
                        dictPermanentLeft.Add(24, "<li><p>24</p><input type=\"checkbox\" disabled id=\"24\"/></li>");
                        dictPermanentLeft.Add(23, "<li><p>23</p><input type=\"checkbox\" disabled id=\"23\"/></li>");
                        dictPermanentLeft.Add(22, "<li><p>22</p><input type=\"checkbox\" disabled id=\"22\"/></li>");
                        dictPermanentLeft.Add(21, "<li><p>21</p><input type=\"checkbox\" disabled id=\"21\"/></li>");
                        dictPermanentLeft.Add(20, "<li><p>20</p><input type=\"checkbox\" disabled id=\"20\"/></li>");
                        dictPermanentLeft.Add(19, "<li><p>19</p><input type=\"checkbox\" disabled id=\"19\"/></li>");
                        dictPermanentLeft.Add(18, "<li><p>18</p><input type=\"checkbox\" disabled id=\"18\"/></li>");
                        dictPermanentLeft.Add(17, "<li><p>17</p><input type=\"checkbox\" disabled id=\"17\"/></li>");

                        foreach (ToothList objToothList in lst[0].lstToothList)
                        {
                            if (dictPermanentLeft.ContainsKey(objToothList.ToothType))
                            {
                                if (objToothList.ToothType.ToString() == "16")
                                {
                                    dictPermanentLeft[objToothList.ToothType] = "<li class=\"active\"><p>" + objToothList.ToothType.ToString() + "</p><input type=\"checkbox\" disabled checked=\"checked\" id=\"" + objToothList.ToothType.ToString() + "\"/></li></div>";
                                }
                                else
                                {
                                    dictPermanentLeft[objToothList.ToothType] = "<li class=\"active\"><p>" + objToothList.ToothType.ToString() + "</p><input type=\"checkbox\" disabled checked=\"checked\" id=\"" + objToothList.ToothType.ToString() + "\"/></li>";
                                }
                            }
                        }

                        sb.Append(GetLine(dictPermanentLeft));
                        sb.Append("<br class=\"clear\">");
                        sb.Append("</ul>");
                        sb.Append("</div>");
                        sb.Append("</div>");


                        //Primary Teeth right


                        sb.Append("<div class=\"right panel\">");
                        sb.Append("<h5>Primary Teeth</h5>");
                        sb.Append("<div class=\"block blockborder\">");
                        sb.Append("<p>Right</p><ul class=\"teeth_listing\">");
                        sb.Append("<div style=\"border-bottom:1px solid #ccc; padding-bottom: 15px;\">");

                        Dictionary<int, String> dictPrimaryRight = new Dictionary<int, string>();
                        dictPrimaryRight.Add(33, "<li><p>A</p><input type=\"checkbox\" disabled id=\"33\"/></li>");
                        dictPrimaryRight.Add(34, "<li><p>B</p><input type=\"checkbox\" disabled id=\"34\"/></li>");
                        dictPrimaryRight.Add(35, "<li><p>C</p><input type=\"checkbox\" disabled id=\"35\"/></li>");
                        dictPrimaryRight.Add(36, "<li><p>D</p><input type=\"checkbox\" disabled id=\"36\"/></li>");
                        dictPrimaryRight.Add(37, "<li><p>E</p><input type=\"checkbox\" disabled id=\"37\"/></li></div>");
                        dictPrimaryRight.Add(52, "<li><p>T</p><input type=\"checkbox\" disabled id=\"52\"/></li>");
                        dictPrimaryRight.Add(51, "<li><p>S</p><input type=\"checkbox\" disabled id=\"51\"/></li>");
                        dictPrimaryRight.Add(50, "<li><p>R</p><input type=\"checkbox\" disabled id=\"50\"/></li>");
                        dictPrimaryRight.Add(49, "<li><p>Q</p><input type=\"checkbox\" disabled id=\"49\"/></li>");
                        dictPrimaryRight.Add(48, "<li><p>P</p><input type=\"checkbox\" disabled id=\"48\"/></li>");

                        foreach (ToothList objToothList in lst[0].lstToothList)
                        {
                            if (dictPrimaryRight.ContainsKey(objToothList.ToothType))
                            {
                                String str = dictPrimaryRight[objToothList.ToothType];
                                dictPrimaryRight[objToothList.ToothType] = str.Replace("<li>", "<li class=\"active Primary\">");
                            }
                        }
                        sb.Append(GetLine(dictPrimaryRight));
                        sb.Append("<br class=\"clear\">");
                        sb.Append("</ul>");
                        sb.Append("</div>");

                        ///Primary Teeth Left
                        ///


                        sb.Append("<div class=\"block\">");
                        sb.Append("<p>Left</p>");
                        sb.Append("<ul class=\"teeth_listing\">");
                        sb.Append("<div style=\"border-bottom:1px solid #ccc; padding-bottom: 15px;\">");

                        Dictionary<int, String> dictPrimaryLeft = new Dictionary<int, string>();
                        dictPrimaryLeft.Add(38, "<li><p>F</p><input type=\"checkbox\" disabled id=\"38\"/></li>");
                        dictPrimaryLeft.Add(39, "<li><p>G</p><input type=\"checkbox\" disabled id=\"39\"/></li>");
                        dictPrimaryLeft.Add(40, "<li><p>H</p><input type=\"checkbox\" disabled id=\"40\"/></li>");
                        dictPrimaryLeft.Add(41, "<li><p>I</p><input type=\"checkbox\" disabled id=\"41\"/></li>");
                        dictPrimaryLeft.Add(42, "<li><p>J</p><input type=\"checkbox\" disabled id=\"42\"/></li></div>");
                        dictPrimaryLeft.Add(47, "<li><p>O</p><input type=\"checkbox\" disabled id=\"47\"/></li>");
                        dictPrimaryLeft.Add(46, "<li><p>N</p><input type=\"checkbox\" disabled id=\"46\"/></li>");
                        dictPrimaryLeft.Add(45, "<li><p>M</p><input type=\"checkbox\" disabled id=\"45\"/></li>");
                        dictPrimaryLeft.Add(44, "<li><p>L</p><input type=\"checkbox\" disabled id=\"44\"/></li>");
                        dictPrimaryLeft.Add(43, "<li><p>K</p><input type=\"checkbox\" disabled id=\"43\"/></li>");

                        foreach (ToothList objToothList in lst[0].lstToothList)
                        {
                            if (dictPrimaryLeft.ContainsKey(objToothList.ToothType))
                            {
                                String str = dictPrimaryLeft[objToothList.ToothType];
                                dictPrimaryLeft[objToothList.ToothType] = str.Replace("<li>", "<li class=\"active Primary\">");
                            }
                        }
                        sb.Append(GetLine(dictPrimaryLeft));

                        sb.Append("<br class=\"clear\">");
                        sb.Append("</ul>");
                        sb.Append("</div>");
                        sb.Append("</div>");
                        sb.Append("<div class=\"clear\"></div>");



                        sb.Append("</div></div>");


                        ///Code For Images

                        if (lst[0].lstImages.Count > 0)
                        {
                            sb.Append("<div class=\"table_container\">");
                            sb.Append("<h4 class=\"sub_title\">");
                            sb.Append("Images</h4>");
                            sb.Append("</div>");
                            sb.Append("<ul class=\"list\">");
                            foreach (MessageAttachment objAttachment in lst[0].lstImages)
                            {
                                string AttachmentName = objAttachment.AttachmentName;
                                AttachmentName = AttachmentName.Substring(AttachmentName.ToString().LastIndexOf("$") + 1);
                                sb.Append("<li onclick=\"Attachementpopup('" + (ImageBankFolder + objAttachment.AttachmentName) + "','Image')\" style=\"cursor: pointer;\"><span><i class=\"attach\"></i>" + AttachmentName + "</span></li>");
                            }
                            sb.Append("</ul>");
                        }
                        ///Code For Documents
                        if (lst[0].lstDocuments.Count > 0)
                        {
                            sb.Append("<div class=\"table_container\">");
                            sb.Append("<h4 class=\"sub_title\">");
                            sb.Append("Documents</h4>");
                            sb.Append("</div>");
                            sb.Append("<ul class=\"list\">");
                            foreach (MessageAttachment objAttachment in lst[0].lstDocuments)
                            {
                                string AttachmentName = objAttachment.AttachmentName;
                                AttachmentName = AttachmentName.Substring(AttachmentName.ToString().LastIndexOf("$") + 1);
                                sb.Append("<li onclick=\"Attachementpopup('" + (ImageBankFolder + objAttachment.AttachmentName) + "','Doc')\" style=\"cursor: pointer;\"> <span><i class=\"attach\"></i>" + AttachmentName + "</span></li>");
                            }
                            sb.Append("</ul>");
                        }
                        string Comments = lst[0].Comments;
                        Comments = Comments.Replace("\n", "<br/>");
                        sb.Append("<div class=\"table_container\">");
                        sb.Append("<h4 class=\"sub_title\"  >Comments</h4>");
                        sb.Append("<div class=\"comments\" ><label>" + Comments + "</label></div>");
                        sb.Append("</div>");


                        sb.Append(" </div></div>");
                        ///Message Body End
                        ///
                        sb.Append("</div>");
                    }
                    #endregion
                }
                else
                {
                    #region Message Body

                    MdlColleagues = new mdlColleagues();
                    List<MessageDetails> lst = new List<MessageDetails>();
                    DataTable AttachedPatientDetials = new DataTable();
                    lst = MdlColleagues.GetSentMessageDetailsById(Convert.ToInt32(Session["UserId"]), MessageId, MessageTypeId, SessionManagement.TimeZoneSystemName);
                    if (lst.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(lst[0].AttachedPatientsId))
                        {
                            AttachedPatientDetials = ObjColleaguesData.GetPatientAttachedDetails(lst[0].AttachedPatientsId);
                        }
                    }
                    if (lst.Count > 0)
                    {
                        sb.Append(" <div class=\"inbox_icons clearfix\">");

                        if (MessageTypeId == 3)
                        {
                            sb.Append("<a href=\"" + Url.Action("Compose", "Inbox") + "\" title=\"Compose\" class=\"inboxCompose\"></a>");
                            sb.Append("<a href=\"javascript:;\" onclick=\"RemovePatientMessageFromSentboxOfDoctor('" + lst[0].MessageId + "','" + NextMessageId + "','" + NextMessageTypeId + "')\" title=\"Delete\" class=\"inboxDltMsg\"></a>");

                        }
                        else
                        {
                            sb.Append("<a href=\"" + Url.Action("Compose", "Inbox") + "\" title=\"Compose\" class=\"inboxCompose\"></a>");
                            sb.Append("<a href=\"javascript:;\" onclick=\"RemoveMessageFromSentboxOfDoctor('" + lst[0].MessageId + "','" + NextMessageId + "','" + NextMessageTypeId + "')\" title=\"Delete\" class=\"inboxDltMsg\"></a>");
                        }

                        sb.Append("</div>");

                        sb.Append("<h3>&nbsp;<span>&nbsp");

                        sb.Append("</span></h3>");

                        sb.Append("<ul class=\"list\">");
                        sb.Append("<li></li>");
                        sb.Append("<li><label>From:</label><a class=\"hylyt_field\">" + lst[0].From + "<span>&lt;" + lst[0].SenderEmail + "&gt;</span></a></li>");
                        sb.Append("<li><label>To:</label>" + lst[0].To + " &lt;" + lst[0].ReceiverEmail + "&gt;</li>");
                        sb.Append("<li><label>Date:</label>" + lst[0].CreationDate + "</li>");
                        if (lst[0].lstMessageAttachment.Count > 0)
                        {
                            sb.Append("<li>");
                            foreach (var item in lst[0].lstMessageAttachment)
                            {
                                string AttachmentName = string.Empty;
                                AttachmentName = item.AttachmentName;
                                AttachmentName = AttachmentName.Substring(AttachmentName.ToString().LastIndexOf("$") + 1);


                                string extension = Path.GetExtension(AttachmentName);
                                string Type = string.Empty;

                                if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd" && extension.ToLower() != ".pspimage" && extension.ToLower() != ".thm" && extension.ToLower() != ".tif")
                                {
                                    Type = "Doc";
                                }
                                else
                                {
                                    Type = "Image";
                                }
                                sb.Append("<span title=\"" + AttachmentName + "\" onclick=\"Attachementpopup('" + ImageBankFolder + item.AttachmentName + "','" + Type + "')\" style=\"cursor: pointer;\" ><i class=\"attach\"></i> " + AttachmentName + "</span>");


                            }
                            sb.Append("</li>");
                            sb.Append("<a href=\"/Inbox/DownloadAttachmentofMessage?DownId=" + MessageId + "\"><img src=\"/Content/images/print-all-forms.png\" style=\"float:right;margin-top:-10px;cursor:pointer;\"/></a>");

                        }
                        string sentbody = lst[0].Body;
                        sentbody = sentbody.Replace("\n", "<br/>");
                        sb.Append("</ul>");
                        sb.Append("<table style='width:100%'><tr><td>");
                        sb.Append("<div class=\"message_content\" style=\"float: left;width: 100%; position: relative;\">" + sentbody + " </div>");
                        if (AttachedPatientDetials.Rows.Count == 0)
                        {
                            sb.Append("</td></tr></table>");
                        }

                        if (AttachedPatientDetials.Rows.Count > 0)
                        {
                            sb.Append("</td></tr><tr><td>&nbsp;</td></tr><tr><td>");
                            sb.Append("<lable>Attached Patient (Click on patient to view history)</lable>");
                            sb.Append("</td></tr><tr><td>");
                            sb.Append("<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" class=\"patientTable\" style=\"\"><thead><tr><th>Image</th><th>First Name</th><th> Last Name</th><th>Gender</th></tr></thead> ");
                            sb.Append("<tbody>");
                            foreach (DataRow item in AttachedPatientDetials.Rows)
                            {
                                sb.Append("<tr><td><a href=\"javascript:;\" onclick=\"getAttachedPatientHistory(" + item["Patientid"] + ")\">");

                                if (!String.IsNullOrEmpty(item["ProfileImage"].ToString()))
                                    sb.Append("<figure style=\"cursor:pointer;\" class=\"thumb\"><img width=\"104\" height=\"104\" alt=\"Ashlyn\" src=" + ImageBankFolder + item["ProfileImage"] + "></figure>");
                                else if (item["Gender"].ToString() == "10")
                                    sb.Append("<figure class=\"thumb\" style=\"cursor:pointer;\"><img width=\"104\" height=\"104\" alt=\"Ashlyn\" src=" + DefaultDoctorFemaleImage + "></figure>");
                                else
                                    sb.Append("<figure class=\"thumb\" style=\"cursor:pointer;\"><img width=\"104\" height=\"104\" alt=\"Ashlyn\" src=" + DefaultDoctorImage + "></figure>");
                                sb.Append("</a></td>");
                                sb.Append("<td><a href=\"javascript:;\" onclick=\"getAttachedPatientHistory(" + item["Patientid"] + ")\">" + item["FirstName"] + "</a></td>");
                                sb.Append("<td><a href=\"javascript:;\" onclick=\"getAttachedPatientHistory(" + item["Patientid"] + ")\">" + item["LastName"] + "</a></td>");
                                sb.Append("<td><a href=\"javascript:;\" onclick=\"getAttachedPatientHistory(" + item["Patientid"] + ")\">" + (item["Gender"].ToString() == "0" ? "Male" : item["Gender"].ToString() == "1" ? "Female" : "") + "</a></td></tr>");
                            }
                            sb.Append("</tbody>");

                            sb.Append("</table></td></tr></table>");
                        }

                    }
                    #endregion
                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        [HttpPost]
        public JsonResult SentBoxMessageShowOnClick(int MessageId, int NextMessageId, int MessageTypeId, int NextMessageTypeId)
        {
            MdlColleagues = new mdlColleagues();
            return Json(GetSentMessageBody(MessageId, NextMessageId, MessageTypeId, NextMessageTypeId), JsonRequestBehavior.AllowGet);
        }

        public string RemoveMessageFromSentBoxOfDoctor(int MessageId, string NextMessageId, string NextMessageTypeId, string PageIndex)
        {
            object obj = string.Empty;
            bool result = false;
            SerializeObjForSentBox obj1 = new SerializeObjForSentBox();
            try
            {
                result = ObjColleaguesData.RemoveMessageFromSentBoxOfDoctor(Convert.ToInt32(Session["UserId"]), MessageId);
                if (result)
                {
                    if (string.IsNullOrEmpty(NextMessageId))
                    {
                        NextMessageId = "-1";
                    }
                    if (string.IsNullOrEmpty(NextMessageTypeId))
                    {
                        NextMessageTypeId = "-1";
                    }
                    //  obj1.SentboxList = GetSentBoxOfDoctor(Convert.ToInt32(Session["UserId"]), (!string.IsNullOrEmpty(PageIndex) ? Convert.ToInt32(PageIndex) : 1), 1, 2, null, null);
                    obj1.MessageBody = GetSentMessageBody(Convert.ToInt32(NextMessageId), null, Convert.ToInt32(NextMessageTypeId), null);
                }

            }
            catch (Exception)
            {

                throw;
            }
            var json = new JavaScriptSerializer().Serialize(obj1);
            return json;
        }



        public string RemovePatientMessageFromSentBoxOfDoctor(int MessageId, string NextMessageId, string NextMessageTypeId, string PageIndex)
        {
            object obj = string.Empty;
            bool result = false;
            SerializeObjForSentBox obj1 = new SerializeObjForSentBox();
            try
            {
                result = ObjColleaguesData.RemovePatientMessageFromSentBoxOfDoctor(Convert.ToInt32(Session["UserId"]), MessageId);
                if (result)
                {
                    if (string.IsNullOrEmpty(NextMessageId))
                    {
                        NextMessageId = "-1";
                    }
                    if (string.IsNullOrEmpty(NextMessageTypeId))
                    {
                        NextMessageTypeId = "-1";
                    }
                    // obj1.SentboxList = GetSentBoxOfDoctor(Convert.ToInt32(Session["UserId"]), (!string.IsNullOrEmpty(PageIndex) ? Convert.ToInt32(PageIndex) : 1), 1, 2, null, null);
                    obj1.MessageBody = GetSentMessageBody(Convert.ToInt32(NextMessageId), null, Convert.ToInt32(NextMessageTypeId), null);
                }

            }
            catch (Exception)
            {

                throw;
            }


            var json = new JavaScriptSerializer().Serialize(obj1);
            return json;
        }

        public string RemoveReferralFromSentBox(int MessageId, string NextMessageId, string NextMessageTypeId, string PageIndex)
        {
            object obj = string.Empty;
            bool result = false;
            SerializeObjForSentBox obj1 = new SerializeObjForSentBox();
            try
            {
                result = ObjColleaguesData.RemoveReferralFromSentBox(Convert.ToInt32(Session["UserId"]), MessageId);
                if (result)
                {
                    if (string.IsNullOrEmpty(NextMessageId))
                    {
                        NextMessageId = "-1";
                    }
                    if (string.IsNullOrEmpty(NextMessageTypeId))
                    {
                        NextMessageTypeId = "-1";
                    }
                    // obj1.SentboxList = GetSentBoxOfDoctor(Convert.ToInt32(Session["UserId"]), (!string.IsNullOrEmpty(PageIndex) ? Convert.ToInt32(PageIndex) : 1), 1, 2, null, null);
                    obj1.MessageBody = GetSentMessageBody(Convert.ToInt32(NextMessageId), null, Convert.ToInt32(NextMessageTypeId), null);
                }

            }
            catch (Exception)
            {

                throw;
            }


            var json = new JavaScriptSerializer().Serialize(obj1);
            return json;
        }
        #endregion

        #region Compose
        public ActionResult Compose()
        {
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {

                bool Result = ObjCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments()); // For Remove All Uplaoded Temp Images and Files

                MdlColleagues = new mdlColleagues();

                ViewBag.countatt = null;
                if (Request.QueryString["ColleaguesId"] != null && Convert.ToString(Request.QueryString["ColleaguesId"]) != "")
                {
                    ViewBag.ColleaguesId = Convert.ToString(Request.QueryString["ColleaguesId"]);
                }
                else
                {
                    ViewBag.ColleaguesId = 0;
                }
                if (Request.QueryString["Type"] != null && Convert.ToString(Request.QueryString["Type"]) != "")
                {
                    ViewBag.Type = Convert.ToString(Request.QueryString["Type"]);
                }
                else
                {
                    ViewBag.Type = null;
                }
                if (Request.QueryString["PatientId"] != null && Convert.ToString(Request.QueryString["PatientId"]) != "")
                {
                    ViewBag.PatientId = Convert.ToString(Request.QueryString["PatientId"]);
                }
                else
                {
                    ViewBag.PatientId = 0;
                }
                if (Request.QueryString["ReceiverId"] != null && Convert.ToString(Request.QueryString["ReceiverId"]) != "")
                {
                    ViewBag.ReceiverId = Convert.ToString(Request.QueryString["ReceiverId"]);
                }
                else
                {
                    ViewBag.ReceiverId = 0;
                }
                return PartialView("PartialCompose", MdlColleagues);
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }


        [HttpPost]
        public JsonResult RemoveTempDoc(int Id, string DocName)
        {
            string extension = Path.GetExtension(DocName);
            string SuccessMessage = string.Empty;
            bool Result = false;
            object obj = null;
            try
            {
                if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd" && extension.ToLower() != ".pspimage" && extension.ToLower() != ".thm" && extension.ToLower() != ".tif")
                {
                    Result = ObjCommon.Temp_DeleteUploadedFileById(Convert.ToInt32(Session["UserId"]), Id);
                    SuccessMessage = "Document removed successfully";
                }
                else
                {
                    Result = ObjCommon.Temp_DeleteUploadedImageById(Convert.ToInt32(Session["UserId"]), Id);
                    SuccessMessage = "Image removed successfully";
                }

                if (Result)
                {

                    string DeleteDocument = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, TempFolder.Replace("/", "").Replace("~", ""));
                    DeleteDocument = DeleteDocument + "\\" + DocName;

                    if (System.IO.File.Exists(DeleteDocument))
                    {
                        System.IO.File.Delete(DeleteDocument);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            obj = new
            {
                Success = true,

                Message = SuccessMessage,

            };
            return Json(obj, JsonRequestBehavior.DenyGet);


        }

        [HttpPost]
        public JsonResult GetUserList(string selected,string ReceiverId)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();

                dt = ObjColleaguesData.GetColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), 1, int.MaxValue, null, 11, 2);
                if (dt != null && dt.Rows.Count > 0)
                {
                    sb.Append("<select id=\"select3\" name=\"select3\" multiple=\"multiple\" style=\"width:100%\">");


                    foreach (DataRow item in dt.Rows)
                    {
                        string Id = Convert.ToString(item["ColleagueId"]);
                        // if (ReceiverId.Split(',').Contains(Id))
                        if(ReceiverId != null && ReceiverId != "0")
                        {
                            if (ReceiverId.Split(',').Contains(Id))
                            {
                                sb.Append("<option value=" + Convert.ToInt32(item["ColleagueId"]) + " class=\"selected\" selected=\"selected\">" + item["FirstName"] + "&nbsp;" + item["LastName"] + "</option>");
                            }
                            else
                            {
                                sb.Append("<option value=" + Convert.ToInt32(item["ColleagueId"]) + ">" + item["FirstName"] + "&nbsp;" + item["LastName"] + "</option>");
                            }
                        }
                        else
                        {
                            if (selected.Split(',').Contains(Id))
                            {
                                sb.Append("<option value=" + Convert.ToInt32(item["ColleagueId"]) + " class=\"selected\" selected=\"selected\">" + item["FirstName"] + "&nbsp;" + item["LastName"] + "</option>");
                            }
                            else
                            {
                                sb.Append("<option value=" + Convert.ToInt32(item["ColleagueId"]) + ">" + item["FirstName"] + "&nbsp;" + item["LastName"] + "</option>");
                            }
                        }
                    }

                    sb.Append("</select>");

                    sb.ToString();
                }

            }
            catch (Exception)
            {

                throw;
            }

            return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetPatientList(string selected)
        {
            clsPatientsData ObjPatientsData = new clsPatientsData();
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();

                dt = ObjPatientsData.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), 1, int.MaxValue, null, 11, 2, null, 0,0);
                if (dt != null && dt.Rows.Count > 0)
                {
                    sb.Append("<select id=\"select4\" name=\"select4\"  multiple=\"multiple\" style=\"width:100%\">");
                    sb.Append("<option></option>");
                    foreach (DataRow item in dt.Rows)
                    {
                        string Id = Convert.ToString(item["PatientId"]);
                        if (selected.Split(',').Contains(Id))
                        {
                            sb.Append("<option value=" + Convert.ToInt32(item["PatientId"]) + " class=\"selected\" selected=\"selected\">" + item["FirstName"] + "&nbsp;" + item["LastName"] + "</option>");
                        }
                        else
                        {
                            sb.Append("<option value=" + Convert.ToInt32(item["PatientId"]) + ">" + item["FirstName"] + "&nbsp;" + item["LastName"] + "</option>");
                        }
                    }

                    sb.Append("</select>");

                    sb.ToString();
                }

            }
            catch (Exception)
            {

                throw;
            }

            return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetAllTempUplaodedDocuments()
        {
            List<TempUpload> lstTempUplaod = new List<TempUpload>();





            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();
                dt = ObjCommon.GetRecordsFromTempTablesForDoctor(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstTempUplaod.Add(new TempUpload()
                        {
                            DocumentId = Convert.ToInt32(row["DocumentId"]),
                            DocumentName = ObjCommon.CheckNull(Convert.ToString(row["DocumentName"]), string.Empty),
                        });
                    }
                }


                if (lstTempUplaod.Count > 0)
                {
                    foreach (var item in lstTempUplaod)
                    {
                        sb.Append("<div style=\"background-color: #E5E5E5; width: 50%; border: 2px solid #E5E5E5; font-family: 11px Verdana,Geneva,sans-serif;border-radius: 5px; margin: 5px 5px 5px 114px; padding: 5px;\">");
                        sb.Append("<span style=\"color:#0890C4;cursor:pointer;\" onclick=\"OpenTempDoc('" + ConfigurationManager.AppSettings["TempUploadsFolderPath"].ToString() + item.DocumentName + "')\">" + item.DocumentName.Substring(item.DocumentName.LastIndexOf("$") + 1) + "</span>");
                        sb.Append("<img alt=\"\" src=\"../../Content/images/Upload_Cancel.png\" style=\"float: right;\" onclick=\"RemoveTempDoc('" + item.DocumentId + "','" + item.DocumentName + "')\" /></div>");
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
        }
        //Ankit Changes For Draft implimentation 02-03-2017
        [HttpPost]
        public JsonResult SendMessageToColleagues(string SelectedColleagueIds, string Message, string AttachPatientIds,bool Status)
        {

            object obj = string.Empty;
            try
            {

                string[] ColleagueIds = SelectedColleagueIds.ToString().Split(new char[] { ',' });
                if (ColleagueIds.Length != 0)
                {
                    foreach (var item in ColleagueIds)
                    {

                        if (!string.IsNullOrEmpty(item))
                        {
                            if (string.IsNullOrEmpty(AttachPatientIds))
                            {
                                AttachPatientIds = null;
                            }
                            #region For MessageType 1
                            int NewMessageId = 0;
                            if (Status)
                            {
                                NewMessageId = ObjColleaguesData.ComposeMessageOfDoctor("", Message, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(item), 1, AttachPatientIds,Status, Convert.ToInt32(Session["UserId"]));
                            }
                            else
                            {
                                NewMessageId = ObjColleaguesData.ComposeMessageOfDoctor("", Message, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(item), 1, AttachPatientIds, Status, Convert.ToInt32(Session["UserId"]));
                            }
                            
                            if (NewMessageId != 0)
                            {
                                #region Code For Colleagues's Message Attachemnts
                                DataTable dtTempUploadedDocs = ObjCommon.GetRecordsFromTempTablesForDoctor(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
                                if (dtTempUploadedDocs != null && dtTempUploadedDocs.Rows.Count > 0)
                                {

                                    foreach (DataRow itemTempUploadedDocs in dtTempUploadedDocs.Rows)
                                    {

                                        string DocName = Convert.ToString(itemTempUploadedDocs["DocumentName"]);
                                        DocName = DocName.Substring(DocName.ToString().LastIndexOf("$") + 1);
                                        string NewFileName = Convert.ToString("$" + System.DateTime.Now.Ticks + "$" + DocName);
                                        string Extension = Path.GetExtension(NewFileName);
                                        DocName = Convert.ToString(itemTempUploadedDocs["DocumentName"]);
                                        DocName = DocName.Replace("../TempUploads/", "");

                                        string sourceFile = Server.MapPath(TempFolder) + "\\" + DocName;
                                        string destinationFile = string.Empty;
                                        sourceFile = sourceFile.Replace("\\Inbox\\", "\\");
                                        destinationFile = destinationFile.Replace("\\Inbox\\", "\\");
                                        if (Status)
                                        {
                                            //Only For Draft Functionality
                                            string directoryPath = Server.MapPath(DraftFolder);
                                            if (!Directory.Exists(directoryPath))
                                            {
                                                Directory.CreateDirectory(directoryPath);
                                            }
                                                

                                            destinationFile = Server.MapPath(DraftFolder) + "\\" + NewFileName;
                                            System.IO.File.Copy(sourceFile, destinationFile);

                                        }
                                        else
                                        {
                                            destinationFile = Server.MapPath(ImageBankFolder) + "\\" + NewFileName;
                                            System.IO.File.Copy(sourceFile, destinationFile);
                                        }
                                        if (Extension.ToLower() == ".jpg" || Extension.ToLower() == ".jpeg" || Extension.ToLower() == ".gif" || Extension.ToLower() == ".png" || Extension.ToLower() == ".bmp" || Extension.ToLower() == ".psd" || Extension.ToLower() == ".pspimage" || Extension.ToLower() == ".thm" || Extension.ToLower() == ".tif")
                                        {
                                            string savepathThumb = "";
                                            string tempPathThumb = "";
                                            string thambnailimage = NewFileName;
                                            string Savepath = Server.MapPath(ImageBankFolder);
                                            string Draftimages = Server.MapPath(DraftFolder);

                                            Savepath = Savepath.Replace("\\Inbox\\", "\\");

                                            tempPathThumb = System.Configuration.ConfigurationManager.AppSettings["FolderPathThumb"];
                                            savepathThumb = Server.MapPath(tempPathThumb);

                                            savepathThumb = savepathThumb.Replace("\\Inbox\\", "\\");

                                            
                                            // Load image.
                                            if (Status)
                                            {
                                                Image image = Image.FromFile(Draftimages + @"\" + NewFileName);
                                                Size thumbnailSize = ObjCommon.GetThumbnailSize(image);
                                                System.Drawing.Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                                               thumbnailSize.Height, null, IntPtr.Zero);
                                                thumbnail.Save(savepathThumb + @"\" + thambnailimage);
                                            }
                                            else
                                            {
                                                Image image = Image.FromFile(Savepath + @"\" + NewFileName);
                                                // Compute thumbnail size.
                                                Size thumbnailSize = ObjCommon.GetThumbnailSize(image);
                                                // Get thumbnail.
                                                System.Drawing.Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                                                    thumbnailSize.Height, null, IntPtr.Zero);
                                                // Save thumbnail.
                                                thumbnail.Save(savepathThumb + @"\" + thambnailimage);
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(NewFileName))
                                        {
                                            bool Result1 = false;
                                            // Code for Insert Attachment
                                            if (Status)
                                            {
                                               // Result1 = ObjColleaguesData.InsertDraftAttachments(NewMessageId, NewFileName, Convert.ToInt32(Session["UserId"]), 0, 0);
                                                bool Attach = ObjColleaguesData.InsertMessageAttachemntsForColleague(NewMessageId, NewFileName);
                                            }
                                            else
                                            {
                                                bool Attach = ObjColleaguesData.InsertMessageAttachemntsForColleague(NewMessageId, NewFileName);
                                            }
                                            
                                        }

                                    }
                                }
                                #endregion

                                obj = "Message sent successfully";

                            }
                            #endregion

                            ObjTemplate.SendMessageToColleague(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(item), NewMessageId, 1);
                            if (!string.IsNullOrEmpty(AttachPatientIds))
                            {
                                string[] AttachPatientIdsinsplit = AttachPatientIds.ToString().Split(new char[] { ',' });
                                ObjColleaguesData = new clsColleaguesData();
                                for (int i = 0; i < AttachPatientIdsinsplit.Count(); i++)
                                {
                                    ObjColleaguesData.AddPatientToReferredDoctor(Convert.ToInt32(item), Convert.ToInt32(AttachPatientIdsinsplit[i]));
                                }

                            }


                            // Send tempalte

                        }



                    }



                    DataTable dtfileuploaddelete = ObjCommon.GetRecordsFromTempTablesForDoctor(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());

                    foreach (DataRow item in dtfileuploaddelete.Rows)
                    {

                        string DocName = Convert.ToString(item["DocumentName"]);


                        DocName = Convert.ToString(item["DocumentName"]);
                        DocName = DocName.Replace("../TempUploads/", "");

                        string sourceFile = Server.MapPath(ConfigurationManager.AppSettings.Get("TempUploadsFolderPath")) + DocName;

                        sourceFile = sourceFile.Replace("\\Inbox\\", "");

                        if (System.IO.File.Exists(sourceFile))
                        {
                            System.IO.File.Delete(sourceFile);
                        }
                    }

                     ObjCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments()); // For Remove All Uplaoded Temp Images and Files


                }



            }
            catch (Exception)
            {
                ObjCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
                throw;
            }
            bool Result = ObjCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult Reply()
        {
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                return PartialView("PartialReply");
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        #region Code For Convert Disctionary To String
        string GetLine(Dictionary<int, String> d)
        {
            // Build up each line one-by-one and then trim the end
            StringBuilder builder = new StringBuilder();
            foreach (KeyValuePair<int, String> pair in d)
            {
                builder.Append(pair.Value);
            }
            string result = builder.ToString();
            // Remove the final delimiter
            return result;
        }
        #endregion

        public string GetDoctorDetails(int UserId)
        {
            DataSet ds = new DataSet();
            string DoctorFullname = string.Empty;
            ds = ObjColleaguesData.GetDoctorDetailsById(UserId);
            if (ds != null && ds.Tables.Count > 0)
            {
                DoctorFullname = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FullName"]), string.Empty);
            }
            return DoctorFullname;
        }
        //AK changes for Draft functionality 02-28-2017
        public ActionResult Draft(bool Flag)
        {
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                if(Flag)
                {
                    ViewBag.IsReferral = true;
                }
                else
                {
                    ViewBag.IsReferral = false;
                }
                return View();
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }
        //AK changes for Draft functionality 02-28-2017
        public ActionResult DraftPaging(string PageIndex, string PageSize, string ShortColumn, string ShortDirection,string SearchText,bool IsReferral)
        {
            MdlColleagues = new mdlColleagues();
            List<MessageListOfDoctor> lstMessagelistofDoctor = new List<MessageListOfDoctor>();
            lstMessagelistofDoctor = MdlColleagues.GetDraftofDoctor(Convert.ToInt32(Session["Userid"]),Convert.ToInt32(PageIndex), Convert.ToInt32(PageSize), Convert.ToInt32(ShortColumn), Convert.ToInt32(ShortDirection),SearchText, IsReferral);
            return View(lstMessagelistofDoctor);
        }
        public ActionResult GetMessageBodyofDraft(int MessageId, int? NextMessageId, int MessageTypeId, int? NextMessageTypeId)
        {
            if(MessageTypeId == 2)
            {
                MdlPatient = new mdlPatient();
                List<ReferralDetails> lst = new List<ReferralDetails>();
                lst = MdlPatient.GetRefferalDetailsById(Convert.ToInt32(Session["UserId"]), MessageId, "Sentbox", SessionManagement.TimeZoneSystemName);
                return View("GetMessageBodayOfSentReferrals", lst);
            }
            else
            {
                MdlColleagues = new mdlColleagues();
                List<MessageDetails> lstMessageDetails = new List<MessageDetails>();
                MdlColleagues.lstMessageDetails = MdlColleagues.GetSentMessageDetailsById(Convert.ToInt32(Session["UserId"]), MessageId, MessageTypeId, SessionManagement.TimeZoneSystemName);
                return View("GetPlainMessageBodyOfSentBox", MdlColleagues.lstMessageDetails.Take(1).ToList());
            }
        }
    }
}
