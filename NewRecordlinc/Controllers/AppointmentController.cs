﻿using DataAccessLayer.Appointment;
using DataAccessLayer.Common;
using NewRecordlinc.App_Start;
using NewRecordlinc.Models.Appointment;
using NewRecordlinc.Models.Common;
using DataAccessLayer.ColleaguesData;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using BusinessLogicLayer;
using NewRecordlinc.Models.Patients;

namespace NewRecordlinc.Controllers
{

    public class AppointmentController : Controller
    {
        mdlPatient MdlPatient = new mdlPatient();
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        clsCommon ObjCommon = new clsCommon();
        const string CONSTDATEFORMATE = AppointmentSetting.CONSTDATEFORMATE;// "MM-dd-yyyy";
        const string CONSTTIMEFORMATE = AppointmentSetting.CONSTTIMEFORMATE;// "hh:mm tt";
        const string CONSTDATETIMEFORMATE = AppointmentSetting.CONSTDATETIMEFORMATE;// "MM/dd/yyyy hh:mm tt";
        clsAppointmentData objclsAppointmentData;
        // GET: Appointment


        [CustomAuthorizeAttribute]
        public ActionResult Index()
        {


            bool Result = ObjCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
            objclsAppointmentData = new clsAppointmentData();
            var objAppointmentDetails = new CreateAppointment();
            if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["PatientId"])))
            {
                DataTable dtpatient = new DataTable();
                int num;
                bool isNum = Int32.TryParse(Request.QueryString["PatientId"].ToString(), out num);
                if (isNum)
                {
                    if (Convert.ToInt64(Request.QueryString["PatientId"].ToString()) > 0)
                    {
                        dtpatient = objclsAppointmentData.GetPateintDetailsByPatientId(Convert.ToInt64(Request.QueryString["PatientId"].ToString()),Convert.ToInt32(SessionManagement.UserId));
                        if (dtpatient.Rows.Count <= 0)
                        {
                            TempData["ErrorMessage"] = "Invalid Patient you are trying to access.";
                            return RedirectToAction("Index", "Patients");

                        }
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Invalid Patient you are trying to access.";
                        return RedirectToAction("Index", "Patients");

                    }
                }
                else
                {
                    TempData["ErrorMessage"] = "Invalid Patient you are trying to access.";
                    return RedirectToAction("Index", "Patients");

                }

            }

            int UserId = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ParentUserId"]))) { UserId = Convert.ToInt32(Session["ParentUserId"]); }
            else { UserId = Convert.ToInt32(Session["UserId"]); }
            var dt = objclsAppointmentData.GetLocationForAppointment(UserId);
            if (dt != null)
            {
                objAppointmentDetails.locationList = (from p in dt.AsEnumerable()
                                                      select new LocationForAppointment
                                                      {
                                                          locationId = Convert.ToInt32(p["AddressInfoId"]),
                                                          locationName = Convert.ToString(p["Location"])
                                                      }
                                ).ToList();
            }
            if (Request.Cookies["LocationId"] != null)
            {

                if ((!string.IsNullOrEmpty(Convert.ToString(Request.Cookies["LocationId"]))))
                {
                    objAppointmentDetails.locationId = Convert.ToInt32(Request.Cookies["LocationId"].Value);
                    if (Request.Cookies["DoctorId"] != null)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(Request.Cookies["DoctorId"].Value)))
                        {
                            if (Convert.ToInt32(Request.Cookies["DoctorId"].Value) > 0)
                            {
                                objAppointmentDetails.DoctorId = Convert.ToInt32(Request.Cookies["DoctorId"].Value);
                            }
                        }
                    }

                }
                else
                {
                    objAppointmentDetails.locationId = Convert.ToInt32(Session["LocationId"]);
                    objAppointmentDetails.DoctorId = Convert.ToInt32(Session["UserId"]);
                    Response.Cookies["LocationId"].Value = Convert.ToString(objAppointmentDetails.locationId);
                    Response.Cookies["LocationId"].Expires = DateTime.Now.AddHours(24);
                    if (objAppointmentDetails.DoctorId > 0)
                    {
                        Response.Cookies["DoctorId"].Value = Convert.ToString(objAppointmentDetails.DoctorId);
                        Response.Cookies["DoctorId"].Expires = DateTime.Now.AddHours(24);
                    }

                }

            }
            else
            {
                objAppointmentDetails.locationId = Convert.ToInt32(Session["LocationId"]);
                objAppointmentDetails.DoctorId = Convert.ToInt32(Session["UserId"]);
                Response.Cookies["LocationId"].Value = Convert.ToString(objAppointmentDetails.locationId);
                Response.Cookies["LocationId"].Expires = DateTime.Now.AddHours(24);
                if (objAppointmentDetails.DoctorId > 0)
                {
                    Response.Cookies["DoctorId"].Value = Convert.ToString(objAppointmentDetails.DoctorId);
                    Response.Cookies["DoctorId"].Expires = DateTime.Now.AddHours(24);
                }

            }


            objAppointmentDetails.ParentUserId = Convert.ToInt32(Session["ParentUserId"]);
            var dtdentallist = objclsAppointmentData.GetDentistForAppointment(UserId, Convert.ToInt32(Session["LocationId"]));
            if (dtdentallist != null)
            {
                objAppointmentDetails.DentistListCustom = (from p in dtdentallist.AsEnumerable()
                                                           select new DentistList
                                                           {
                                                               UserId = Convert.ToInt32(p["UserId"]),
                                                               FullName = Convert.ToString(p["FullName"])
                                                           }
                                ).ToList();
            }
            //objAppointmentDetails.LocationcheckedForSync = GetLocationId(Convert.ToInt32(Session["UserId"]));
            objAppointmentDetails.lstLocationForSync = CommonAppointment.GetLastSyncDateforLocation(Convert.ToInt32(Session["UserId"]));
            return View(objAppointmentDetails);
        }
        #region Create Doctor Services
        [CustomAuthorizeAttribute]
        [HttpGet]
        public ActionResult Services()
        {
            MdlPatient = new mdlPatient();
            //int Count = MdlPatient.GetPatientCountOfDoctor(Convert.ToInt32(SessionManagement.UserId), "", "", 2, 0);
            //ViewBag.cnt = Count;
            objclsAppointmentData = new clsAppointmentData();
            int UserId = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ParentUserId"]))) { UserId = Convert.ToInt32(Session["ParentUserId"]); }
            else { UserId = Convert.ToInt32(Session["UserId"]); }
            var AppServiceDt = objclsAppointmentData.GetAppointmentServices(UserId, 0);
            var services = new AppointmentViewModel();
            if (AppServiceDt.Rows.Count > 0 && AppServiceDt != null)
            {
                services.AppointmentServices = (from p in AppServiceDt.AsEnumerable()
                                                select new AppointmentService
                                                {
                                                    AppointmentServiceId = Convert.ToInt32(p["AppointmentServiceId"]),
                                                    DoctorId = Convert.ToInt32(p["DoctorId"]),
                                                    ServiceType = Convert.ToString(p["ServiceType"]),
                                                    TimeDuration = Convert.ToString(p["TimeDuration"]),
                                                    Price = Convert.ToString(p["Price"]),
                                                }).ToList();
            }
            else
            {
                //  services.AppointmentServices.Add(new AppointmentService());
            }

            return View(services);
        }

        [CustomAuthorizeAttribute]
        public ActionResult CreateService(int ServiceId)
        {
            objclsAppointmentData = new clsAppointmentData();
            var appService = new AppointmentService();

            if (ServiceId != 0)
            {
                var AppServiceDt = objclsAppointmentData.GetAppointmentServices(0, ServiceId);
                if (AppServiceDt.Rows.Count > 0)
                {
                    appService = (from p in AppServiceDt.AsEnumerable()
                                  select new AppointmentService
                                  {
                                      AppointmentServiceId = Convert.ToInt32(p["AppointmentServiceId"]),
                                      DoctorId = Convert.ToInt32(p["DoctorId"]),
                                      ServiceType = Convert.ToString(p["ServiceType"]),
                                      TimeDuration = Convert.ToString(p["TimeDuration"]),
                                      Price = Convert.ToString(p["Price"]),
                                  }).FirstOrDefault();
                }
            }
            return View(appService);
        }

        [CustomAuthorizeAttribute]
        public ActionResult CheckServiceName(string ServiceName, int ServiceId)
        {
            objclsAppointmentData = new clsAppointmentData();
            int UserId = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ParentUserId"]))) { UserId = Convert.ToInt32(Session["ParentUserId"]); }
            else { UserId = Convert.ToInt32(Session["UserId"]); }
            return Json(objclsAppointmentData.CheckServiceName(ServiceName.Trim(), ServiceId, UserId));
        }

        [CustomAuthorizeAttribute]
        public ActionResult SubmitServiceData(AppointmentService appointmentViewModel)
        {
            objclsAppointmentData = new clsAppointmentData();
            int UserId = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ParentUserId"]))) { UserId = Convert.ToInt32(Session["ParentUserId"]); }
            else { UserId = Convert.ToInt32(Session["UserId"]); }
            var serviceTime = TimeSpan.FromMinutes(Convert.ToInt64(appointmentViewModel.TimeDuration));
            objclsAppointmentData.InsertAppointmentServices(appointmentViewModel.AppointmentServiceId, UserId, appointmentViewModel.ServiceType.Trim(), serviceTime, appointmentViewModel.Price);
            return Json("");
        }

        [CustomAuthorizeAttribute]
        public ActionResult DeleteService(int appointmentServiceId)
        {
            objclsAppointmentData = new clsAppointmentData();
            int UserId = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ParentUserId"]))) { UserId = Convert.ToInt32(Session["ParentUserId"]); }
            else { UserId = Convert.ToInt32(Session["UserId"]); }
            int i = objclsAppointmentData.DeleteAppointmentService(appointmentServiceId, UserId);
            ResultType objresulttype = new ResultType();
            switch (i)
            {
                case (int)ReturnStatus.IsDeleted:
                    objresulttype.MsgType = 1;
                    objresulttype.Message = "Service deleted successfully.";

                    break;
                case (int)ReturnStatus.IsExist:
                    objresulttype.MsgType = 0;
                    objresulttype.Message = "One or more appointments are booked with this service.";

                    break;
                case (int)ReturnStatus.IsAtleastOne:
                    objresulttype.MsgType = 0;
                    objresulttype.Message = "Atleast one service should be there.";
                    break;

            }
            return Json(objresulttype);
        }
        #endregion 

        #region Set Week Schedule

        [CustomAuthorizeAttribute]
        [HttpGet]
        public ActionResult SetWeekSchedule()
        {
            objclsAppointmentData = new clsAppointmentData();
            DataTable dt = new DataTable();
            var ap = new AppointmentWeekSchedule();
            int DoctorId = Convert.ToInt32(Session["UserId"]);
            ap.Times = AppointmentSetting.GetAllTime();
            dt = objclsAppointmentData.GetDoctorAppointmentWeekScheduleDetails(DoctorId);

            if (dt.Rows.Count > 0 && dt != null)
            {
                foreach (DataRow item in dt.Rows)
                {
                    ap.AppointmentWeekScheduleId = Convert.ToInt32(item["AppointmentWeekScheduleId"]);
                    ap.DoctorId = Convert.ToInt32(Session["UserId"]);


                    var newtime = AppointmentSetting.GetAllTime();

                    ap.WeekDay.Add(new WeekDay()
                    {
                        AppointmentWeekScheduleId = Convert.ToInt32(item["AppointmentWeekScheduleId"]),
                        Day = Convert.ToInt32(item["Day"]),
                        DayName = Convert.ToString(item["DayName"]),
                        IsOffDay = (item["IsOffDay"].ToString().Contains("Y")) == true ? false : true,
                        AppointmentWorkingTimeAllocationId = Convert.ToInt32(item["AppointmentWorkingTimeAllocationId"]),
                        MorningFromTime = !string.IsNullOrEmpty(Convert.ToString(item["MorningFromTime"])) ? new CommonBLL().ConvertToTime(Convert.ToDateTime(DateTime.Today.Add(TimeSpan.Parse(Convert.ToString(item["MorningFromTime"])))), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL)) : string.Empty,
                        MorningToTime = !string.IsNullOrEmpty(Convert.ToString(item["MorningToTime"])) ? new CommonBLL().ConvertToTime(Convert.ToDateTime(DateTime.Today.Add(TimeSpan.Parse(Convert.ToString(item["MorningToTime"])))), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL)) : string.Empty,
                        EveningFromTime = !string.IsNullOrEmpty(Convert.ToString(item["EveningFromTime"])) ? new CommonBLL().ConvertToTime(Convert.ToDateTime(DateTime.Today.Add(TimeSpan.Parse(Convert.ToString(item["EveningFromTime"])))), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL)) : string.Empty,
                        EveningToTime = !string.IsNullOrEmpty(Convert.ToString(item["EveningToTime"])) ? new CommonBLL().ConvertToTime(Convert.ToDateTime(DateTime.Today.Add(TimeSpan.Parse(Convert.ToString(item["EveningToTime"])))), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL)) : string.Empty,
                        Times = ap.Times.Select(l => new SelectListItem { Selected = (l.Value == Convert.ToString(item["MorningFromTime"])), Text = l.Text, Value = l.Value }).ToList()
                    });

                }


                return View(ap);
            }
            else
            {
                ap.WeekDay = AppointmentSetting.GetWeekSetting();
                return View(ap);
            }

        }

        [CustomAuthorizeAttribute]
        [HttpPost]
        public ActionResult SetWeekSchedule(AppointmentWeekSchedule objApp)
        {
            objclsAppointmentData = new clsAppointmentData();
            int DoctorId = Convert.ToInt32(Session["UserId"]);
            DateTime CreatedOn = DateTime.Now;
            foreach (var item in objApp.WeekDay)
            {
                if (item.AppointmentWeekScheduleId != null && item.AppointmentWeekScheduleId != 0)
                {
                    TimeSpan? FEveningTime = null;
                    TimeSpan? TEveningTime = null;
                    TimeSpan? FMorningTime = null;
                    TimeSpan? TMorningTime = null;
                    if (!string.IsNullOrEmpty(item.EveningFromTime))
                    {
                        DateTime tempDateTime;
                        DateTime.TryParse(item.EveningFromTime, out tempDateTime);
                        FEveningTime = tempDateTime.TimeOfDay;
                        //FEveningTime = TimeSpan.Parse(item.EveningFromTime.);
                    }
                    if (!string.IsNullOrEmpty(item.EveningToTime))
                    {
                        DateTime tempDateTime;
                        DateTime.TryParse(item.EveningToTime, out tempDateTime);
                        TEveningTime = tempDateTime.TimeOfDay;
                        //TEveningTime = TimeSpan.Parse(item.EveningToTime);
                    }
                    if (!string.IsNullOrEmpty(item.MorningFromTime))
                    {
                        DateTime tempDateTime;
                        DateTime.TryParse(item.MorningFromTime, out tempDateTime);
                        FMorningTime = tempDateTime.TimeOfDay;
                        //FMorningTime = TimeSpan.Parse(item.MorningFromTime);
                    }
                    if (!string.IsNullOrEmpty(item.MorningToTime))
                    {
                        DateTime tempDateTime;
                        DateTime.TryParse(item.MorningToTime, out tempDateTime);
                        TMorningTime = tempDateTime.TimeOfDay;
                        //TMorningTime = TimeSpan.Parse(item.MorningToTime);
                    }

                    objclsAppointmentData.UpdateAppointmentWeekSchedule(item.AppointmentWeekScheduleId ?? default(int),
                        item.AppointmentWorkingTimeAllocationId, item.IsOffDay, FMorningTime,
                        TMorningTime, FEveningTime, TEveningTime, DoctorId, CreatedOn, item.Day);
                }
                else
                {
                    TimeSpan? FEveningTime = null;
                    TimeSpan? TEveningTime = null;
                    TimeSpan? FMorningTime = null;
                    TimeSpan? TMorningTime = null;
                    if (!string.IsNullOrEmpty(item.EveningFromTime))
                    {
                        FEveningTime = Convert.ToDateTime(item.EveningFromTime).TimeOfDay;
                        //TimeSpan.Parse(item.EveningFromTime);
                    }
                    if (!string.IsNullOrEmpty(item.EveningToTime))
                    {
                        TEveningTime = Convert.ToDateTime(item.EveningToTime).TimeOfDay;
                        //TimeSpan.Parse(item.EveningToTime);
                    }
                    if (!string.IsNullOrEmpty(item.MorningFromTime))
                    {
                        FMorningTime = Convert.ToDateTime(item.MorningFromTime).TimeOfDay;
                        //TimeSpan.Parse(item.MorningFromTime);
                    }
                    if (!string.IsNullOrEmpty(item.MorningToTime))
                    {
                        TMorningTime = Convert.ToDateTime(item.MorningToTime).TimeOfDay;
                        //TimeSpan.Parse(item.MorningToTime);
                    }

                    objclsAppointmentData.InsertAppointmentWeekSchedule(item.AppointmentWeekScheduleId ?? default(int),
                        item.AppointmentWorkingTimeAllocationId, DoctorId, item.Day,
                        item.IsOffDay, FMorningTime, TMorningTime,
                        FEveningTime, TEveningTime, CreatedOn);
                }
            }
            return View(objApp);
        }

        #endregion

        #region Create Appointment 
        [CustomAuthorizeAttribute]
        public ActionResult CreateAppointments()
        {
            objclsAppointmentData = new clsAppointmentData();
            var createAppointment = new CreateAppointment();

            var dtappServicedt = objclsAppointmentData.GetAppointmentServices(Convert.ToInt32(Session["UserId"]), 0);
            if (dtappServicedt.Rows.Count > 0 && dtappServicedt != null)
            {
                createAppointment.DoctorServices = (from p in dtappServicedt.AsEnumerable()
                                                    select new SelectListItem
                                                    {
                                                        Value = Convert.ToString(p["AppointmentServiceId"]),
                                                        Text = Convert.ToString(p["ServiceType"]),
                                                    }).ToList();
            }

            var dtPatientList = objclsAppointmentData.GetPatientList(Convert.ToInt32(Session["UserId"]), 0);
            if (dtPatientList.Rows.Count > 0 && dtPatientList != null)
            {
                createAppointment.PatientList = (from p in dtPatientList.AsEnumerable()
                                                 select new SelectListItem
                                                 {
                                                     Value = ObjCommon.CheckNull(Convert.ToString(p["PatientId"]), string.Empty),
                                                     Text = ObjCommon.CheckNull(Convert.ToString(p["PatientName"]), string.Empty),
                                                 }).ToList();
            }

            return View(createAppointment);
        }

        #endregion
        [CustomAuthorizeAttribute]
        public ActionResult ViewAppointment()
        {
            DateTime CurrentDateTime = clsHelper.ConvertFromUTC(Convert.ToDateTime(DateTime.Now.ToString()), SessionManagement.TimeZoneSystemName);
            string strCurrentDateTime = new CommonBLL().ConvertToDateTime(Convert.ToDateTime(CurrentDateTime), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL));
            TempData["CurrentDateTime"] = strCurrentDateTime.ToString();
            return View();
        }
        //public string GetLocationId(int UserId)
        //{
        //    try
        //    {
        //        DataTable dt = new DataTable();
        //        string locationid = string.Empty;
        //        dt = ObjColleaguesData.GetLocationIdforSyncinAppointmentSide(UserId);
        //        if(dt != null && dt.Rows.Count >0)
        //        {
        //            foreach (DataRow item in dt.Rows)
        //            {
        //                locationid += Convert.ToString(item["LocationId"])+",";
        //            }
        //            locationid = locationid.TrimEnd(',');
        //        }
        //        return locationid;
        //    }
        //    catch (Exception Ex)
        //    {
        //        ObjCommon.InsertErrorLog("AppointmentController - GetLocationId", Ex.Message, Ex.StackTrace);
        //        throw;
        //    }
        //}
        [CustomAuthorizeAttribute]
        public ActionResult GetBookedAppointmentForCalendar(string strFromDate, string strToDate, int UserId, int LocationId)
        {
            DateTime MonthStart = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime MonthEnd = MonthStart.AddMonths(1).AddDays(-1);
            DateTime FromDate = (!string.IsNullOrEmpty(strFromDate)) ? Convert.ToDateTime(strFromDate) : MonthStart;
            DateTime ToDate = (!string.IsNullOrEmpty(strToDate)) ? Convert.ToDateTime(strToDate) : MonthEnd;
            Response.Cookies["LocationId"].Value = Convert.ToString(LocationId);
            if (UserId > 0)
            {
                Response.Cookies["DoctorId"].Value = Convert.ToString(UserId);
            }
            else
            {

                Response.Cookies["DoctorId"].Value = Convert.ToString(Request.Cookies["DoctorId"].Value);
            }

            objclsAppointmentData = new clsAppointmentData();
            var CalendarList = CommonAppointment.CalenderViewIntoList(objclsAppointmentData.GetBookedAppointmentForCalendar(UserId, FromDate, ToDate, LocationId), SessionManagement.TimeZoneSystemName);
            return Json(CalendarList);
        }
        [CustomAuthorizeAttribute]
        public ActionResult GetOprationTheatorForCalendarByLocation(int LocationId)
        {

            objclsAppointmentData = new clsAppointmentData();
            var ResourceList = CommonAppointment.CalendarResourceIntoList(objclsAppointmentData.GetOprationTheatorForCalendarByLocation(LocationId));
            return Json(ResourceList);
        }



        [CustomAuthorizeAttribute]
        public ActionResult BookAppointment(string CalFromTime, string searchText, int locationId = 0)
        {
            objclsAppointmentData = new clsAppointmentData();
            var createAppointment = new CreateAppointment();

            var dt = objclsAppointmentData.GetLocationForAppointment(Convert.ToInt32(Session["UserId"]));
            if (dt != null)
            {
                createAppointment.locationList = (from p in dt.AsEnumerable()
                                                  select new LocationForAppointment
                                                  {
                                                      locationId = Convert.ToInt32(p["AddressInfoId"]),
                                                      locationName = Convert.ToString(p["Location"])
                                                  }
                                ).ToList();
            }
            return View(createAppointment);

        }
        [CustomAuthorizeAttribute]
        public JsonResult GetLocation(int DoctorId = 0)
        {
            objclsAppointmentData = new clsAppointmentData();
            var createAppointment = new CreateAppointment();
            int UserId = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ParentUserId"]))) { UserId = Convert.ToInt32(Session["ParentUserId"]); }
            else { UserId = Convert.ToInt32(Session["UserId"]); }
            if (DoctorId > 0)
            {
                UserId = DoctorId;
            }
            var dt = objclsAppointmentData.GetLocationForAppointment(UserId);
            if (dt != null)
            {
                createAppointment.locationList = (from p in dt.AsEnumerable()
                                                  select new LocationForAppointment
                                                  {
                                                      locationId = Convert.ToInt32(p["AddressInfoId"]),
                                                      locationName = Convert.ToString(p["Location"])
                                                  }
                                ).ToList();
            }
            return Json(createAppointment, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorizeAttribute]
        public JsonResult GetOprationTheatorByLocation(int LocationId)
        {

            objclsAppointmentData = new clsAppointmentData();
            var ResourceList = CommonAppointment.CalendarResourceIntoList(objclsAppointmentData.GetOprationTheatorForCalendarByLocation(LocationId));
            return Json(ResourceList, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorizeAttribute]
        public JsonResult GetDentistList(int LocationId, int DoctorId = 0)
        {
            objclsAppointmentData = new clsAppointmentData();
            var createAppointment = new CreateAppointment();
            Response.Cookies["LocationId"].Value = Convert.ToString(LocationId);
            int UserId = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ParentUserId"]))) { UserId = Convert.ToInt32(Session["ParentUserId"]); }
            else { UserId = Convert.ToInt32(Session["UserId"]); }
            var dtdentallist = new DataTable();
            if (DoctorId > 0)
            {
                UserId = DoctorId;
            }
            dtdentallist = objclsAppointmentData.GetDentistForAppointment(UserId, LocationId);
            if (dtdentallist != null)
            {
                createAppointment.DentistListCustom = (from p in dtdentallist.AsEnumerable()
                                                       select new DentistList
                                                       {
                                                           UserId = Convert.ToInt32(p["UserId"]),
                                                           FullName = Convert.ToString(p["FullName"])
                                                       }
                                ).ToList();
            }
            return Json(createAppointment, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetIsurancelist(int UserId)
        {
            DataTable dts = new DataTable();
            objclsAppointmentData = new clsAppointmentData();
            var createAppointment = new CreateAppointment();
            dts = objclsAppointmentData.GetInuranceNamesOfDoctorForAppointment(UserId);
            if (dts != null && dts.Rows.Count > 0)
            {
                createAppointment.lstInsurance = (from p in dts.AsEnumerable()
                                            select new InsuranceListForAppointment
                                            {
                                                InsuranceId = Convert.ToInt32(p["InsuranceId"]),
                                                InsuranceName = Convert.ToString(p["Name"])
                                            }).ToList();
            }
            return Json(createAppointment, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorizeAttribute]
        public JsonResult GetTheatreList(int LocationId)
        {
            objclsAppointmentData = new clsAppointmentData();
            var createAppointment = new CreateAppointment();
            int UserId = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ParentUserId"]))) { UserId = Convert.ToInt32(Session["ParentUserId"]); }
            else { UserId = Convert.ToInt32(Session["UserId"]); }
            var dtdentallist = objclsAppointmentData.GetDentistForAppointment(UserId, LocationId);
            if (dtdentallist != null)
            {
                createAppointment.DentistListCustom = (from p in dtdentallist.AsEnumerable()
                                                       select new DentistList
                                                       {
                                                           UserId = Convert.ToInt32(p["UserId"]),
                                                           FullName = Convert.ToString(p["FullName"])
                                                       }
                                ).ToList();
            }
            return Json(createAppointment, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorizeAttribute]
        public ActionResult LoadPaintsList(string CalFromTime, string searchText, int locationId = 0)
        {
            objclsAppointmentData = new clsAppointmentData();
            var createAppointment = new CreateAppointment();
            createAppointment.FromTime = CalFromTime;
            bool isLoadPartial = true;
            if (string.IsNullOrEmpty(searchText))
            {
                isLoadPartial = false;
            }
            else if (searchText == "$Empty$")
            {
                searchText = null;
                isLoadPartial = true;
            }
            var dtPatientList = objclsAppointmentData.GetPatientList(Convert.ToInt32(Session["UserId"]), 0, locationId, searchText);
            if (dtPatientList.Rows.Count > 0 && dtPatientList != null)
            {
                createAppointment.PatientlistCustom = (from p in dtPatientList.AsEnumerable()
                                                       select new PatientList
                                                       {
                                                           PatientId = Convert.ToInt32(p["PatientId"]),
                                                           PatientName = ObjCommon.CheckNull(Convert.ToString(p["PatientName"]), string.Empty),
                                                           PateintEmail = ObjCommon.CheckNull(Convert.ToString(p["Email"]), string.Empty),
                                                           PatientCercleName = ObjCommon.CheckNull(Convert.ToString(p["PatientCircleName"]), string.Empty),
                                                           locationId = String.IsNullOrEmpty(Convert.ToString(p["Location"])) ? Convert.ToInt32(0) : Convert.ToInt32(Convert.ToString(p["Location"])),
                                                           ImageName = (ObjCommon.CheckNull(Convert.ToString(p["Gender"]), string.Empty) == "Female") ? ObjCommon.CheckNull(Convert.ToString(p["ProfileImage"]), string.Empty) == string.Empty ? ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"]), string.Empty) : ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["ImageBank"]), string.Empty) + ObjCommon.CheckNull(Convert.ToString(p["ProfileImage"]), string.Empty) : ObjCommon.CheckNull(Convert.ToString(p["ProfileImage"]), string.Empty) == string.Empty ? ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty) : ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["ImageBank"]), string.Empty) + ObjCommon.CheckNull(Convert.ToString(p["ProfileImage"]), string.Empty),
                                                           PhoneNumber = ObjCommon.ConvertPhoneNumber(ObjCommon.CheckNull(Convert.ToString(p["WorkPhone"]), string.Empty)),
                                                           PatientNumber = ObjCommon.CheckNull(Convert.ToString(p["AssignedPatientId"]), string.Empty),
                                                       }).ToList();
            }
            return PartialView("_appointmentPatientList", createAppointment);


        }



        [CustomAuthorizeAttribute]
        public ActionResult CreateAppointmentTimeSlotView(int PatientId, string CalndFromTime, int LocationId, int DoctorId = 0)
        {

            objclsAppointmentData = new clsAppointmentData();

            int UserId = 0;
            DateTime? dt = null;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ParentUserId"]))) { UserId = Convert.ToInt32(Session["ParentUserId"]); }
            else { UserId = Convert.ToInt32(Session["UserId"]); }
            if (DoctorId > 0)
            {
                UserId = DoctorId;
            }
            var dtAppService = objclsAppointmentData.GetAppointmentServices(UserId, 0);
            var ServiceData = new CreateAppointment();

            var dtPatientList = objclsAppointmentData.GetPatientList(UserId, PatientId);


            if (dtPatientList.Rows.Count > 0)
            {
                ServiceData.PatientId = Convert.ToInt32(dtPatientList.Rows[0]["PatientId"]);
                ServiceData.PatientName = Convert.ToString(dtPatientList.Rows[0]["PatientName"]);
                ServiceData.PatientCercleName = Convert.ToString(dtPatientList.Rows[0]["PatientCircleName"]);
                ServiceData.PatientImageName = (ObjCommon.CheckNull(Convert.ToString(dtPatientList.Rows[0]["Gender"]), string.Empty) == "Female") ? ObjCommon.CheckNull(Convert.ToString(dtPatientList.Rows[0]["ProfileImage"]), string.Empty) == string.Empty ? ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"]), string.Empty) : ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["ImageBank"]), string.Empty) + ObjCommon.CheckNull(Convert.ToString(dtPatientList.Rows[0]["ProfileImage"]), string.Empty) : ObjCommon.CheckNull(Convert.ToString(dtPatientList.Rows[0]["ProfileImage"]), string.Empty) == string.Empty ? ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty) : ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["ImageBank"]), string.Empty) + ObjCommon.CheckNull(Convert.ToString(dtPatientList.Rows[0]["ProfileImage"]), string.Empty);
                ServiceData.PatientPhoneNumber = ObjCommon.ConvertPhoneNumber(ObjCommon.CheckNull(Convert.ToString(dtPatientList.Rows[0]["WorkPhone"]), string.Empty));
                ServiceData.PatientNumber = ObjCommon.CheckNull(Convert.ToString(dtPatientList.Rows[0]["AssignedPatientId"]), string.Empty);
                ServiceData.DateOfBirth = ObjCommon.CheckNull(Convert.ToString(dtPatientList.Rows[0]["DateOfBirth"]), string.Empty) == "" ? dt : Convert.ToDateTime(dtPatientList.Rows[0]["DateOfBirth"]);
            }
            if (dtAppService.Rows.Count > 0 && dtPatientList != null)
            {
                ServiceData.ServiceListCustom = (from p in dtAppService.AsEnumerable()
                                                 select new ServiceList
                                                 {
                                                     ServiceId = Convert.ToInt32(p["AppointmentServiceId"]),
                                                     ServiceName = Convert.ToString(p["ServiceType"]),
                                                     TimeDuration = Convert.ToString(p["TimeDuration"])
                                                 }).ToList();
            }
            ServiceData.FromTime = Convert.ToDateTime(CalndFromTime).ToString(CONSTDATETIMEFORMATE);
            var TheatreList = CommonAppointment.AppointmentResourceIntoList(objclsAppointmentData.GetOprationTheatorForCalendarByLocation(LocationId));
            if (TheatreList != null)
            {
                ServiceData.TheatreListCustom = TheatreList;
            }
            else
            {
                ServiceData.TheatreListCustom = new List<AppointmentResource>();
            }
            var dtdentallist = new DataTable();
            if (DoctorId > 0)
            {
                dtdentallist = objclsAppointmentData.GetDentistForAppointment(Convert.ToInt32(DoctorId), Convert.ToInt32(LocationId));
            }
            else
            {
                dtdentallist = objclsAppointmentData.GetDentistForAppointment(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Session["LocationId"]));
            }

            if (dtdentallist != null)
            {
                ServiceData.DentistListCustom = (from p in dtdentallist.AsEnumerable()
                                                 select new DentistList
                                                 {
                                                     UserId = Convert.ToInt32(p["UserId"]),
                                                     FullName = Convert.ToString(p["FullName"])
                                                 }
                                ).ToList();
            }

            //ServiceData.DateOfBirth = ServiceData.DateOfBirth.HasValue ? clsHelper.ConvertFromUTC(Convert.ToDateTime(ServiceData.DateOfBirth), SessionManagement.TimeZoneSystemName) : ServiceData.DateOfBirth;
            //DataTable dts = new DataTable();
            //dts = objclsAppointmentData.GetInuranceNamesOfDoctorForAppointment(UserId);
            //if(dts != null && dts.Rows.Count > 0)
            //{
            //    ServiceData.lstInsurance = (from p in dts.AsEnumerable()
            //                                select new InsuranceListForAppointment
            //                                {
            //                                    InsuranceId = Convert.ToInt32(p["InsuranceId"]),
            //                                    InsuranceName = Convert.ToString(p["Name"])
            //                                }).ToList();
            //}
            return View(ServiceData);
        }
        [CustomAuthorizeAttribute]
        public ActionResult SaveBookAppointMent(int DoctorId)
        {
            var ServiceData = new CreateAppointment();
            objclsAppointmentData = new clsAppointmentData();
            var dt = objclsAppointmentData.GetDoctorDetails(DoctorId);
            if (dt != null)
            {
                string ImageName = dt.Rows[0]["ImageName"].ToString();
                if (string.IsNullOrEmpty(ImageName))
                {
                    ServiceData.DoctorImage = ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty);
                }
                else
                {
                    ServiceData.DoctorImage = ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + ImageName;
                }
                ServiceData.DoctorName = dt.Rows[0]["DoctorName"].ToString();
                //var TimeSlot = CommonAppointment.GetAppointmentDataIntoList(objclsAppointmentData.GetCreateAppointmentTimeSlotViewData(DoctorId));
                //var DoctorAvalailableTimeSlot = CommonAppointment.TimeSlotAllocation(TimeSlot, new DataTable(), StartDate);
                //ServiceData.AppointmentDayViewList = DoctorAvalailableTimeSlot.AppointmentDayViewList;
                //ServiceData.AppointmentServiceList = DoctorAvalailableTimeSlot.AppointmentServiceList;
            }
            return View(ServiceData);
        }
        [CustomAuthorizeAttribute]
        public ActionResult AppointmentSlotGrid(int DoctorId, string StartDate, int TheatreID, int AppointmentId)
        {
            objclsAppointmentData = new clsAppointmentData();
            var TimeSlot = CommonAppointment.GetAppointmentDataIntoList(objclsAppointmentData.GetCreateAppointmentTimeSlotViewData(DoctorId, TheatreID, 0), SessionManagement.TimeZoneSystemName);
            var DoctorAvalailableTimeSlot = CommonAppointment.TimeSlotAllocation(TimeSlot, new DataTable(), StartDate, AppointmentId, SessionManagement.TimeZoneSystemName);
            DoctorAvalailableTimeSlot.AppointmentDayViewList = DoctorAvalailableTimeSlot.AppointmentDayViewList.Where(x => x.CurrentDate.Date == Convert.ToDateTime(StartDate).Date).ToList();
            return View(DoctorAvalailableTimeSlot);
        }

        [CustomAuthorizeAttribute]
        public ActionResult SubmitAppointmentBookingData(CreateAppointment objCreateAppointment)
        {
            var ObjTemplate = new clsTemplate();
            objclsAppointmentData = new clsAppointmentData();
            DateTime AppointmentDate;
            TimeSpan AppointmentTime;
            // AppointmentDate = Convert.ToDateTime(objCreateAppointment.FromTime);
            AppointmentDate = Convert.ToDateTime(objCreateAppointment.CurrentDate.ToString("yyyy-MM-dd") + " " + objCreateAppointment.FromTime);
            AppointmentDate = clsHelper.ConvertToUTC(AppointmentDate, SessionManagement.TimeZoneSystemName);
            AppointmentTime = AppointmentDate.TimeOfDay;
            int NewAppId = objclsAppointmentData.SubmitAppointmentBookingData(objCreateAppointment.DoctorId, objCreateAppointment.PatientId, AppointmentDate.Date, AppointmentTime, objCreateAppointment.AppointmentLength, objCreateAppointment.ServiceId, objCreateAppointment.Note, Convert.ToInt32(Session["UserId"]), 0, objCreateAppointment.ApptResourceId, objCreateAppointment.AppointmentId > 0 ? Convert.ToInt32(Session["UserId"]) : 0, objCreateAppointment.AppointmentId > 0 ? objCreateAppointment.AppointmentId : 0,0);
            var AppointmentData = CommonAppointment.CalenderViewIntoList(objclsAppointmentData.GetSingleAppointmentData(NewAppId), SessionManagement.TimeZoneSystemName);

            //Send mail to pateint and doctor for newly created appointment
            //var AppMaildt = objclsAppointmentData.GetEmailAllFieldForCreateAppointmentByAppointmentId(NewAppId);
            //ObjTemplate.CreateAppointmentNotificationforPatient(AppMaildt);
            //ObjTemplate.CreateAppointmentNotificationforDoctor(AppMaildt);

            return Json(AppointmentData);
        }


        [CustomAuthorizeAttribute]
        //public ActionResult CheckOverLappingAndDoctorAvailability(CreateAppointment objCreateAppointment)
        //{
        //    return Json(CommonAppointment.CheckOverLappingAndDoctorAvailability(objCreateAppointment, Convert.ToInt32(Session["UserId"])));
        //}

        [CustomAuthorizeAttribute]
        public ActionResult ViewDetailsForBookedAppointment(int AppointmentId)
        {
            objclsAppointmentData = new clsAppointmentData();
            var Appdetailsdt = objclsAppointmentData.ViewDetailsForBookedAppointment(AppointmentId);
            var ObjAppDetailsData = new AppointmentData();
            var appointmentdt = objclsAppointmentData.GetAppointmentStaus();
            DateTime? dt = null;
            if (Appdetailsdt != null)
            {

                ObjAppDetailsData = (from p in Appdetailsdt.AsEnumerable()
                                     select new AppointmentData
                                     {
                                         AppointmentId = Convert.ToInt32(p["AppointmentId"]),
                                         DoctorId = Convert.ToInt32(p["DoctorId"]),
                                         PatientId = Convert.ToInt32(p["PatientId"]),
                                         AppointmentDate = Convert.ToDateTime(p["AppointmentDate"]),
                                         AppointmentTime = TimeSpan.Parse(Convert.ToString(p["AppointmentTime"])),
                                         AppointmentLength = Convert.ToInt32(Convert.ToString(p["AppointmentLength"])),
                                         ServiceTypeId = Convert.ToString(p["AppointmentServiceTypeId"]),
                                         //Status = Convert.ToString(p["Status"]),
                                         PatientName = Convert.ToString(p["FullName"]),
                                         ServiceName = Convert.ToString(p["ServiceType"]),
                                         PatientCercleName = Convert.ToString(p["PatientCircleName"]),
                                         PatientsImage = (ObjCommon.CheckNull(Convert.ToString(p["Gender"]), string.Empty) == "Female") ? ObjCommon.CheckNull(Convert.ToString(p["ProfileImage"]), string.Empty) == string.Empty ? ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"]), string.Empty) : ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["ImageBank"]), string.Empty) + ObjCommon.CheckNull(Convert.ToString(p["ProfileImage"]), string.Empty) : ObjCommon.CheckNull(Convert.ToString(p["ProfileImage"]), string.Empty) == string.Empty ? ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty) : ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["ImageBank"]), string.Empty) + ObjCommon.CheckNull(Convert.ToString(p["ProfileImage"]), string.Empty),
                                         PatientPhoneNumber = ObjCommon.ConvertPhoneNumber(ObjCommon.CheckNull(Convert.ToString(p["WorkPhone"]), string.Empty)),
                                         Gender = ObjCommon.CheckNull(Convert.ToString(p["Gender"]), string.Empty),
                                         Location = ObjCommon.CheckNull(Convert.ToString(p["Location"]), string.Empty),
                                         PatientNumber = ObjCommon.CheckNull(Convert.ToString(p["AssignedPatientId"]), string.Empty),
                                         DoctorImage = ObjCommon.CheckNull(Convert.ToString(p["ImageName"]), string.Empty) == string.Empty ? ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty) : ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + Convert.ToString(p["ImageName"]),
                                         ResourceName = ObjCommon.CheckNull(Convert.ToString(p["ResourceName"]), string.Empty),
                                         DoctorName = ObjCommon.CheckNull(Convert.ToString(p["DoctorName"]), string.Empty),
                                         CreatedByName = ObjCommon.CheckNull(Convert.ToString(p["CreatedByName"]), string.Empty),
                                         PNote = ObjCommon.CheckNull(Convert.ToString(p["Note"]), string.Empty),
                                         Age = Convert.ToInt32(p["Age"]),
                                         DateOfBirth = ObjCommon.CheckNull(Convert.ToString(p["DateOfBirth"]), string.Empty) == "" ? dt : Convert.ToDateTime(p["DateOfBirth"]),
                                         LocationId = Convert.ToInt32(p["LocationId"]),
                                         ApptResourceId = Convert.ToInt32(p["ApptResourceId"]),
                                         AppointmentTimeSlot = string.Format("{0}", DateTime.Today.Add(TimeSpan.Parse(Convert.ToString(p["AppointmentTime"]))).ToString("HH:mm:ss")),
                                         AppointmentStatusId = Convert.ToInt32(p["AppointmentStatusId"]),
                                     }).FirstOrDefault();

                DateTime AppointmentDate;
                TimeSpan AppointmentTime;

                //Changes for RM-178
                //ObjAppDetailsData.DateOfBirth = ObjAppDetailsData.DateOfBirth.HasValue ? clsHelper.ConvertFromUTC(Convert.ToDateTime(ObjAppDetailsData.DateOfBirth), SessionManagement.TimeZoneSystemName) : ObjAppDetailsData.DateOfBirth;

                AppointmentDate = Convert.ToDateTime(ObjAppDetailsData.AppointmentDate.ToString("yyyy-MM-dd") + " " + DateTime.Today.Add(ObjAppDetailsData.AppointmentTime).ToString("hh:mm tt"));
                AppointmentDate = clsHelper.ConvertFromUTC(AppointmentDate, SessionManagement.TimeZoneSystemName);
                AppointmentTime = AppointmentDate.TimeOfDay;
                ObjAppDetailsData.AppointmentDate = AppointmentDate;
                ObjAppDetailsData.AppointmentTime = AppointmentTime;
                //ObjAppDetailsData.strAppointmentDate = ObjAppDetailsData.AppointmentDate.ToString(AppointmentSetting.CONSTDATEFORMATE1);
                ObjAppDetailsData.strAppointmentDate = new CommonBLL().ConvertToDate(Convert.ToDateTime(ObjAppDetailsData.AppointmentDate), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL));
                // ObjAppDetailsData.TimeLable = DateTime.Today.Add(ObjAppDetailsData.AppointmentTime).ToString(CONSTTIMEFORMATE) + " to " + DateTime.Today.Add(ObjAppDetailsData.AppointmentTime.Add(TimeSpan.FromMinutes(ObjAppDetailsData.AppointmentLength))).ToString(CONSTTIMEFORMATE);
                ObjAppDetailsData.TimeLable = new CommonBLL().ConvertToTime(Convert.ToDateTime(DateTime.Today.Add(ObjAppDetailsData.AppointmentTime)), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL)) + " to " + new CommonBLL().ConvertToTime(Convert.ToDateTime(DateTime.Today.Add(ObjAppDetailsData.AppointmentTime.Add(TimeSpan.FromMinutes(ObjAppDetailsData.AppointmentLength)))), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL));
                if (appointmentdt != null)
                {
                    ObjAppDetailsData.AppointmentStatusList = (from a in appointmentdt.AsEnumerable()
                                                               select new AppointmentStatus
                                                               {
                                                                   Id = Convert.ToInt32(a["Id"]),
                                                                   Status = Convert.ToString(a["AppointmentStatus"])
                                                               }
                                   ).ToList();
                }
            }
            return View(ObjAppDetailsData);
        }

        [CustomAuthorizeAttribute]
        public ActionResult CancelAppointment(int AppointmentId)
        {
            objclsAppointmentData = new clsAppointmentData();
            objclsAppointmentData.CancelAppointment(AppointmentId, Convert.ToInt32(Session["UserId"]), 0, "", 11);
            return Json("");
        }
        [CustomAuthorizeAttribute]
        public ActionResult ChaneAppointmentStatus(int AppointmentId, int AppointmentStatusId)
        {
            objclsAppointmentData = new clsAppointmentData();
            objclsAppointmentData.ChangeAppointmentStatus(AppointmentId, AppointmentStatusId);
            return Json("");
        }
        [CustomAuthorizeAttribute]
        public ActionResult GetTimeDuration(int ServiceId)
        {
            objclsAppointmentData = new clsAppointmentData();
            var dtAppService = objclsAppointmentData.GetAppointmentServices(Convert.ToInt32(Session["UserId"]), ServiceId);
            double time = TimeSpan.Parse(dtAppService.Rows[0]["TimeDuration"].ToString()).TotalMinutes;
            return Json(time);
        }

        [CustomAuthorizeAttribute]
        public ActionResult GetWeekSheduleData(int DoctorId, int TheatreID, int LocationId)
        {

            objclsAppointmentData = new clsAppointmentData();
            if (DoctorId > 0)
            {
                CreateAppointmentDataManagement timeSlotData = CommonAppointment.GetAppointmentDataIntoList(objclsAppointmentData.GetCreateAppointmentTimeSlotViewData(DoctorId, TheatreID, 0), SessionManagement.TimeZoneSystemName);
                return Json(timeSlotData.WeekSchedulesData);
            }
            else
            {
                var WeekSchedulesData = CommonAppointment.WeekScheduleIntoList(objclsAppointmentData.GetWeekScheduleData(LocationId));
                return Json(WeekSchedulesData);
            }
        }
        [CustomAuthorizeAttribute]
        public ActionResult CheckOverLapping(CreateAppointment objCreateAppointment)
        {
            var objAppointment = new clsAppointmentData();
            var ts = TimeSpan.Parse(objCreateAppointment.FromTime);
            TimeSpan temptime = ts.Add(TimeSpan.FromMinutes(Convert.ToInt32(objCreateAppointment.AppointmentLength)));
            objCreateAppointment.FromTime = Convert.ToDateTime(objCreateAppointment.CurrentDate.ToString(AppointmentSetting.CONSTDATEFORMATE1) + " " + TimeSpan.Parse(objCreateAppointment.FromTime)).ToString();
            objCreateAppointment.ToTime = Convert.ToDateTime(objCreateAppointment.CurrentDate.ToString(AppointmentSetting.CONSTDATEFORMATE1) + " " + temptime).ToString();
            string message = CommonAppointment.CheckOverLappingAndDoctorAvailability(objCreateAppointment, objCreateAppointment.DoctorId, SessionManagement.TimeZoneSystemName);

            if (!string.IsNullOrEmpty(message))
            {
                return Json("Your selected service is " + objCreateAppointment.AppointmentLength + " minute, it can't be booked at selected time slot.");
            }
            else if ((CommonAppointment.CheckAvalibleDoctorByDateTimeTheatreDoctor(objCreateAppointment, objCreateAppointment.DoctorId, SessionManagement.TimeZoneSystemName) > 0))
            {

                return Json("Provider is not available for specified time slot.");
            }
            else if ((CommonAppointment.CheckAvalibleDoctorByDateTimeTheatrePatient(objCreateAppointment, objCreateAppointment.DoctorId, SessionManagement.TimeZoneSystemName) > 0))
            {

                return Json("Patient is not available for specified time slot.");
            }
            return Json("");
        }


        #region Create Doctor Operation Theatres
        [CustomAuthorizeAttribute]
        [HttpGet]
        public ActionResult OperationTheatre(int locationId = 0)
        {
            MdlPatient = new mdlPatient();
            //int Count = MdlPatient.GetPatientCountOfDoctor(Convert.ToInt32(SessionManagement.UserId), "", "", 2, 0);
            //ViewBag.cnt = Count;
            objclsAppointmentData = new clsAppointmentData();
            int UserId = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ParentUserId"]))) { UserId = Convert.ToInt32(Session["ParentUserId"]); }
            else { UserId = Convert.ToInt32(Session["UserId"]); }
            var AppTheatreDt = objclsAppointmentData.GetAppointmentRosocurces(UserId, locationId);
            var model = new OperationTheatreModel();
            if (AppTheatreDt.Rows.Count > 0 && AppTheatreDt != null)
            {
                model.TheatreList = (from p in AppTheatreDt.AsEnumerable()
                                     select new OperationTheatre
                                     {
                                         ApptResourceId = Convert.ToInt32(p["ApptResourceId"]),
                                         ResourceName = Convert.ToString(p["ResourceName"]),
                                         EventColor = Convert.ToString(p["EventColor"]),
                                         EventBackgroundColor = Convert.ToString(p["EventBackgroundColor"]),
                                         EventBorderColor = Convert.ToString(p["EventBorderColor"]),
                                         EventTextColor = Convert.ToString(p["EventTextColor"]),
                                         LocationId = Convert.ToInt32(p["LocationId"])
                                     }
                                    ).ToList();
            }
            else
            {
                model.TheatreList = new List<Models.Appointment.OperationTheatre>();
            }

            var dt = objclsAppointmentData.GetLocationForAppointment(UserId);
            if (dt != null)
            {
                model.LocationList = (from p in dt.AsEnumerable()
                                      select new LocationForAppointment
                                      {
                                          locationId = Convert.ToInt32(p["AddressInfoId"]),
                                          locationName = Convert.ToString(p["Location"])
                                      }
                                ).ToList();
            }

            return View(model);
        }

        [CustomAuthorizeAttribute]
        public ActionResult CreateOperationThreatre(int locationId = 0, int resourceId = 0)
        {
            var model = new OperationTheatre();
            objclsAppointmentData = new clsAppointmentData();

            if (resourceId != 0)
            {
                var AppTheatreDt = objclsAppointmentData.GetAppointmentRosocurces(Convert.ToInt32(Session["UserId"]), locationId, resourceId);


                if (AppTheatreDt.Rows.Count > 0 && AppTheatreDt != null)
                {
                    model = (from p in AppTheatreDt.AsEnumerable()
                             select new OperationTheatre
                             {
                                 ApptResourceId = Convert.ToInt32(p["ApptResourceId"]),
                                 ResourceName = Convert.ToString(p["ResourceName"]),
                                 EventColor = Convert.ToString(p["EventColor"]),
                                 EventBackgroundColor = Convert.ToString(p["EventBackgroundColor"]),
                                 EventBorderColor = Convert.ToString(p["EventBorderColor"]),
                                 EventTextColor = Convert.ToString(p["EventTextColor"]),
                                 LocationId = Convert.ToInt32(p["LocationId"])
                             }).FirstOrDefault();


                }
                else
                {
                }
            }
            int UserId = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ParentUserId"]))) { UserId = Convert.ToInt32(Session["ParentUserId"]); }
            else { UserId = Convert.ToInt32(Session["UserId"]); }
            var dt = objclsAppointmentData.GetLocationForAppointment(UserId);
            if (dt != null)
            {
                model.LocationList = (from p in dt.AsEnumerable()
                                      select new LocationForAppointment
                                      {
                                          locationId = Convert.ToInt32(p["AddressInfoId"]),
                                          locationName = Convert.ToString(p["Location"])
                                      }
                                ).ToList();
            }
            return View(model);
        }

        [CustomAuthorizeAttribute]
        public ActionResult CheckResourceName(string ResourceName, int LocationId, int ApptResourceId)
        {
            objclsAppointmentData = new clsAppointmentData();
            return Json(objclsAppointmentData.CheckResourceName(ResourceName.Trim(), LocationId, ApptResourceId));
        }

        [CustomAuthorizeAttribute]
        public ActionResult SubmitOperationTheatre(OperationTheatre model)
        {

            if (ModelState.IsValid)
            {
                objclsAppointmentData = new clsAppointmentData();
                objclsAppointmentData.InsertAppointmentResources(
                        model.LocationId,
                        model.ResourceName,
                        model.EventTextColor,
                        model.EventBackgroundColor,
                        model.EventBorderColor,
                        model.EventColor,
                        Convert.ToInt32(Convert.ToInt32(Session["UserId"])),
                        model.ApptResourceId
                    );
                if (model.ApptResourceId == 0)
                {
                    TempData["ErrorMessage"] = "Operatory Created successfully";
                }
                else
                {
                    TempData["ErrorMessage"] = "Operatory Updated successfully";
                }

            }
            return RedirectToAction("OperationTheatre");
        }

        [CustomAuthorizeAttribute]
        public ActionResult DeleteOperationTheatre(int locationId, int resourceId)
        {
            try
            {
                objclsAppointmentData = new clsAppointmentData();
                int UserId = 0;
                if (!string.IsNullOrEmpty(Convert.ToString(Session["ParentUserId"]))) { UserId = Convert.ToInt32(Session["ParentUserId"]); }
                else { UserId = Convert.ToInt32(Session["UserId"]); }
                int i = objclsAppointmentData.DeleteOperationTheatre(locationId, resourceId, UserId);
                ResultType objresulttype = new ResultType();
                switch (i)
                {
                    case (int)ReturnStatus.IsDeleted:
                        objresulttype.MsgType = 1;
                        objresulttype.Message = "Operatory deleted successfully.";

                        break;
                    case (int)ReturnStatus.IsExist:
                        objresulttype.MsgType = 0;
                        objresulttype.Message = "One or more appointments are booked with this Operatory.";

                        break;
                    case (int)ReturnStatus.IsAtleastOne:
                        objresulttype.MsgType = 0;
                        objresulttype.Message = "Atleast one operatory should be there.";
                        break;

                }
                return Json(objresulttype, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [CustomAuthorizeAttribute]
        public ActionResult UpdateAppointmentForCalender(CreateAppointment objCreateAppointment)
        {

            var objAppointment = new clsAppointmentData();
            var ts = TimeSpan.Parse(objCreateAppointment.FromTime);
            var AppointmentData = objAppointment.GetSingleAppointmentData(Convert.ToInt32(objCreateAppointment.AppointmentId));
            objCreateAppointment.AppointmentLength = Convert.ToInt32(AppointmentData.Rows[0]["AppointmentLength"].ToString());
            objCreateAppointment.DoctorId = Convert.ToInt32(AppointmentData.Rows[0]["DoctorId"].ToString());
            TimeSpan temptime = ts.Add(TimeSpan.FromMinutes(Convert.ToInt32(objCreateAppointment.AppointmentLength)));

            objCreateAppointment.FromTime = Convert.ToDateTime(objCreateAppointment.CurrentDate.ToString(AppointmentSetting.CONSTDATEFORMATE1) + " " + TimeSpan.Parse(objCreateAppointment.FromTime)).ToString();
            objCreateAppointment.ToTime = Convert.ToDateTime(objCreateAppointment.CurrentDate.ToString(AppointmentSetting.CONSTDATEFORMATE1) + " " + temptime).ToString();
            string message = CommonAppointment.CheckOverLappingAndDoctorAvailability(objCreateAppointment, objCreateAppointment.DoctorId, SessionManagement.TimeZoneSystemName);

            if (!string.IsNullOrEmpty(message))
            {
                return Json("Your selected service is " + objCreateAppointment.AppointmentLength + " minute, it can't be booked at selected time slot.");
            }
            else if ((CommonAppointment.CheckAvalibleDoctorByDateTimeTheatreDoctor(objCreateAppointment, objCreateAppointment.DoctorId, SessionManagement.TimeZoneSystemName) > 0))
            {

                return Json("Provider is not available for specified time slot.");
            }
            else
            {
                DateTime AppointmentDate;
                TimeSpan AppointmentTime;
                AppointmentDate = Convert.ToDateTime(objCreateAppointment.FromTime);
                AppointmentDate = clsHelper.ConvertToUTC(AppointmentDate, SessionManagement.TimeZoneSystemName);
                AppointmentTime = AppointmentDate.TimeOfDay;
                objAppointment.UpdateAppointmentBookingData(objCreateAppointment.CurrentDate, AppointmentTime, objCreateAppointment.ApptResourceId, objCreateAppointment.AppointmentId);
            }
            return Json("");
        }
        public JsonResult GetSingleAppointmentData(int AppointmentId)
        {
            objclsAppointmentData = new clsAppointmentData();
            int UserId = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ParentUserId"]))) { UserId = Convert.ToInt32(Session["ParentUserId"]); }
            else { UserId = Convert.ToInt32(Session["UserId"]); }
            var Appdetailsdt = objclsAppointmentData.GetSingleAppointmentData(AppointmentId, UserId);
            var ObjAppDetailsData = new AppointmentData();
            DateTime? dt = null;
            if (Appdetailsdt != null)
            {
                ObjAppDetailsData = (from p in Appdetailsdt.AsEnumerable()
                                     select new AppointmentData
                                     {
                                         AppointmentId = Convert.ToInt32(p["AppointmentId"]),
                                         DoctorId = Convert.ToInt32(p["DoctorId"]),
                                         PatientId = Convert.ToInt32(p["PatientId"]),
                                         strAppointmentDate = Convert.ToString(p["AppointmentDate"]),
                                         AppointmentTime = TimeSpan.Parse(Convert.ToString(p["AppointmentTime"])),
                                         AppointmentLength = Convert.ToInt32(Convert.ToString(p["AppointmentLength"])),
                                         ServiceTypeId = Convert.ToString(p["ServiceTypeId"]),
                                         LocationId = Convert.ToInt32(p["LocationId"]),
                                         CreatedBy = Convert.ToInt32(p["CreatedBy"])
                                     }).FirstOrDefault();


            }
            if (ObjAppDetailsData != null) { return Json(ObjAppDetailsData, JsonRequestBehavior.AllowGet); } else { return Json(null); }

        }
        #endregion
        //-- DO NOT DELETE BELOW COMMENTED UpdateAllAppointmentTimeZone ACTION METHOD - ITS USEFUL FOR TIMEZONE CHANGES       
        //public string UpdateAllAppointmentTimeZone()
        //{
        //    clsCommon ObjCommon = new clsCommon();
        //    DataTable dt = new DataTable();
        //    string returnValue = string.Empty;
        //    string qry = "select * from Appointments";
        //    dt = ObjCommon.DataTable(qry);
        //    if (dt.Rows.Count > 0)
        //    {
        //        DateTime AppointmentDate;
        //        DateTime AppointmentTime = DateTime.Today;
        //        foreach (DataRow item in dt.Rows)
        //        {
        //            AppointmentDate = Convert.ToDateTime(Convert.ToDateTime(item["AppointmentDate"]).ToString("MM/dd/yyyy") + " " + item["AppointmentTime"]);
        //            AppointmentDate = clsHelper.ConvertToUTC(Convert.ToDateTime(AppointmentDate), null);
        //            AppointmentTime = DateTime.Today.Add(AppointmentDate.TimeOfDay);

        //            qry = "UPDATE Appointments SET AppointmentDate = '" + AppointmentDate.ToString("yyyy-MM-dd") + " 00:00:00', AppointmentTime = '" + AppointmentTime.ToString("HH:mm:ss") + "' WHERE AppointmentId = " + item["AppointmentId"].ToString() + "; SELECT @@ROWCOUNT;";
        //            DataTable dt1 = ObjCommon.DataTable(qry);
        //            returnValue += "<p>AppointmentID : " + item["AppointmentId"].ToString() + " is " + (Convert.ToInt32(dt1.Rows[0][0]) != 0 ? "" : "not") + " updated. </p>";
        //        }
        //    }
        //    return returnValue;
        //}


        public JsonResult SyncAppointmentNow(int[] LocationId)
        {
            bool Result = false;
            //string[] Splite = null;
            //Splite = LocationId.Split(',');
            foreach (var item in LocationId)
            {
                int UserId = Convert.ToInt32(Session["UserId"]);
                bool IsProcessed = false;
                DateTime CreatedDate = DateTime.Now;
                string ProcessDate = string.Empty;
                string EventType = "Sync Dentrix Appointment To Recordlinc";
                string NewEventType = "Sync Recordlinc Appointment To Dentrix";
                string EventSource = "Recordlinc";
                string RequestedUserType = "Dentist";
                Result = ObjColleaguesData.SyncNowAppointmentSide(item, UserId, IsProcessed, EventType, EventSource, CreatedDate, ProcessDate, RequestedUserType);
                Result = ObjColleaguesData.SyncNowAppointmentSide(item, UserId, IsProcessed, NewEventType, EventSource, CreatedDate, ProcessDate, RequestedUserType);
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult CheckDentrixSyncButton(int LocationId)
        {
            bool Result = ObjColleaguesData.CheckSyncNowButtoninAppointmentSide(LocationId);
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorizeAttribute]
        public JsonResult GetDoctorDetails(int DoctorId)
        {
            var ServiceData = new CreateAppointment();
            objclsAppointmentData = new clsAppointmentData();
            var dt = objclsAppointmentData.GetDoctorDetails(DoctorId);
            if (dt != null && dt.Rows.Count > 0)
            {
                ServiceData.locationId = Convert.ToInt32(dt.Rows[0]["LocationId"]);
                ServiceData.DoctorName = Convert.ToString(dt.Rows[0]["DoctorName"]);

            }
            return Json(ServiceData, JsonRequestBehavior.AllowGet);
        }

    }
}