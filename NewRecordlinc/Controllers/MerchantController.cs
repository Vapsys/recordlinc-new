﻿using BO.Models;
using BusinessLogicLayer;
using NewRecordlinc.App_Start;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace NewRecordlinc.Controllers
{
    
    public class MerchantController : Controller
    {
        // GET: Merchant
        public ActionResult Index()
        {
            return View();
        }

        [CustomAuthorize]
        public ActionResult MakeMerchant()
        {
            ViewBag.path = ZPPaymentBAL.CreateMerchant(SessionManagement.UserId);
            ViewBag.msg = "";
            return View();
        }

        public ActionResult Cancel()
        {
            ViewBag.msg = "Your request for merchant creation is cancelled.";            
            return View();
        }

        public ActionResult Return()
        {
            try
            {
                var dict = HttpUtility.ParseQueryString(Request.QueryString.ToString().TrimEnd('&'));
                string json = new JavaScriptSerializer().Serialize(
                                    dict.AllKeys.ToDictionary(k => k, k => dict[k]));
                MerchantResponse objMerchantResponse = JsonConvert.DeserializeObject<MerchantResponse>(json);
                if (objMerchantResponse.responseCode == "A")
                {
                   
                    ZPPaymentBAL.AddMerchant(objMerchantResponse, SessionManagement.UserId);
                    //ViewBag.msg = "Merchant created successfully. Your Merchant id is: " + objMerchantResponse.accountId;
                    TempData["Valid"] = true;
                    TempData["msg"] = "Merchant created successfully. Your Merchant id is: " + objMerchantResponse.accountId;
                }
                else
                {
                    //ViewBag.ErrorMsg = "Your merchant request has been " + objMerchantResponse.responseMessage + ". Your ApplicationId : " + objMerchantResponse.applicationId;
                    TempData["Valid"] = false;
                    TempData["msg"] = "Your merchant request has been " + objMerchantResponse.responseMessage + ". Your ApplicationId : " + objMerchantResponse.applicationId;
                }
                return RedirectToAction("Integration", "AccountSettings");
            }
            catch (Exception ex)
            {
                throw ex;
            }     
            
        }
    }
}