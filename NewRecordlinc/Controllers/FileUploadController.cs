﻿using DataAccessLayer.Common;
using NewRecordlinc.App_Start;
using NewRecordlinc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NewRecordlinc.Controllers
{
    public class FileUploadController : Controller
    {
        public static IDictionary<string,bool> response = null;
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        // GET: FileUpload
        public ActionResult Index()
        {
            return View();
        }
        
        public void ImageBankUpload()
        {
            try
            {
                HttpContext context = System.Web.HttpContext.Current;
                int DoctorId = Convert.ToInt32(context.Session["UserId"]);
                int PatientId = Convert.ToInt32(context.Request.QueryString["PatientId"]);
                Fup_Request model = new Fup_Request();
                model.Context = context;
                model.DoctorId = Convert.ToInt32(context.Session["UserId"]);
                model.PatientId = Convert.ToInt32(context.Request.QueryString["PatientId"]);
                model.Request_Type = BO.Enums.Common.FileUploadType.ImageBankUpload;
                Models.FileUploadModel.FileUploadOnImageBank(model);
            }
            catch (Exception Ex)
            {
                throw;
            }

        }

        [HttpPost]
        public ActionResult TempUpload(string SessionId = "", string path = null,string DoctorId = null, bool isFromReferral = false)
        {
            HttpFileCollectionBase files = Request.Files;
            Fup_Request model = new Fup_Request();
            model.files = Request.Files;
            if(string.IsNullOrEmpty(DoctorId))
            {
                if (!string.IsNullOrEmpty(SessionId))
                {
                    model.UserId = Convert.ToInt32(SessionId);
                }
                else
                {
                    model.UserId = Convert.ToInt32(SessionManagement.UserId);
                }
            }
            else
            {
                model.UserId = Convert.ToInt32(ObjTripleDESCryptoHelper.decryptText(DoctorId));
            }
          
            model.Request_Type = BO.Enums.Common.FileUploadType.TempUpload;
            model.isFromReferral = isFromReferral;
            response = Models.FileUploadModel.FileUploadOnImageBank(model);
            if(isFromReferral)
            {
                return Content(response.Keys.FirstOrDefault());
                //return Json(response.Values.FirstOrDefault(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Content(response.Keys.FirstOrDefault());
                //   return Json(response.Values.FirstOrDefault(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteAttachedFile(int FileId, int FileFrom, string FileName, int ComposeType = 0)
        {
            if (!string.IsNullOrWhiteSpace(FileName) && FileId > 0 && FileFrom > 0)
            {
                Fup_Request model = new Fup_Request();
                model.FileId = FileId;
                model.FileFrom = FileFrom;
                model.FileName = FileName;
                model.ComposeType = ComposeType;
                model.Request_Type = BO.Enums.Common.FileUploadType.DeleteFromTempUpload;
                response = Models.FileUploadModel.FileUploadOnImageBank(model);
            }
            return Json(response.Keys.FirstOrDefault(), JsonRequestBehavior.AllowGet);
        }

        public IDictionary<string, bool> TempToImageBankUpload(int PatientId,int DoctorId,bool IsReferral = false)
        {
            try
            {
                Fup_Request model = new Fup_Request();
                model.DoctorId = DoctorId;
                model.PatientId = PatientId;
                model.Request_Type = BO.Enums.Common.FileUploadType.MoveToImageBank;
                model.isFromReferral = IsReferral;
                response = FileUploadModel.FileUploadOnImageBank(model);
                return response;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("FileUpload Controller - TempToImageBankUpload", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
    }
}