using BO.ViewModel;
using BusinessLogicLayer;
using NewRecordlinc.App_Start;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Configuration;
using ICSharpCode.SharpZipLib.Zip;
using System.Linq;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using System.Text.RegularExpressions;
using BO.Models;
using Excel;
using System.Web.UI;
using System.Web.Script.Serialization;

namespace NewRecordlinc.Controllers
{
    public class CommonController : Controller
    {

        string TempFolderPath = System.Configuration.ConfigurationManager.AppSettings["TempUploadsFolderPath"];
        string ExcelUploadPath = System.Configuration.ConfigurationManager.AppSettings["CSVUploadFolderPath"];
        string DraftFolderPath = System.Configuration.ConfigurationManager.AppSettings["DraftAttachments"];
        string OriginalFolderPath = System.Configuration.ConfigurationManager.AppSettings["OriginalPath"];
        string ThumbPath = System.Configuration.ConfigurationManager.AppSettings["ImageBankThumb"];
        string VcardFilePath = System.Configuration.ConfigurationManager.AppSettings["VcardFilePath"];
        string BannerImages = ConfigurationManager.AppSettings["BannerImage"].Replace('~', ' ');
        string GalleryFiles = ConfigurationManager.AppSettings["GallaryImage"];
        string DentistImage = ConfigurationManager.AppSettings["DoctorImage"];
        DataTable getFilenames = new DataTable();
        clsCommon ObjCommon = new clsCommon();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ShowPaging(int totalItems, int? page, string ScriptName, int pageSize = 10)
        {
            Pager model = new Pager(totalItems, page, ScriptName, pageSize);
            return View(model);
        }

        public ActionResult PartialFileUploadTemp(string FileIds, int MessageId = 0, int ComposeType = 0)
        {
            CommonBLL ObjCommonbll = new CommonBLL();
            List<TempFileAttacheMents> objlistofattachements = new List<TempFileAttacheMents>();
            objlistofattachements = ObjCommonbll.GetTempAttachments(FileIds, MessageId, ComposeType);
            return View(objlistofattachements);
        }

        public ActionResult PartialForwardFileUploadTemp(string FileIds, int MessageId = 0, int ComposeType = 0)
        {
            CommonBLL ObjCommonbll = new CommonBLL();
            List<TempFileAttacheMents> objlistofattachements = new List<TempFileAttacheMents>();
            objlistofattachements = ObjCommonbll.GetForwardAttachMents(FileIds, MessageId, ComposeType);
            return View(objlistofattachements);
        }

        public ActionResult PartialFileUploadTempComm(string FileIds, int MessageId = 0, int ComposeType = 0)
        {
            CommonBLL ObjCommonbll = new CommonBLL();
            List<TempFileAttacheMents> objlistofattachements = new List<TempFileAttacheMents>();
            objlistofattachements = ObjCommonbll.GetTempAttachments(FileIds, MessageId, ComposeType);
            return View(objlistofattachements);
        }

        public ActionResult PartialExcelFileUploadTempComm(string FileIds, int MessageId = 0, int ComposeType = 0)
        {
            CommonBLL ObjCommonbll = new CommonBLL();
            List<TempFileAttacheMents> objlistofattachements = new List<TempFileAttacheMents>();
            objlistofattachements = ObjCommonbll.GetTempAttachments(FileIds, MessageId, ComposeType);
            return View(objlistofattachements);
        }

        public JsonResult DeleteAttachedFile(int FileId, int FileFrom, string FileName, int ComposeType = 0)
        {
            CommonBLL ObjCommonbll = new CommonBLL();
            bool Status = false;
            if (!string.IsNullOrWhiteSpace(FileName) && FileId > 0 && FileFrom > 0)
            {
                string FilePath = string.Empty;
                switch (FileFrom)
                {
                    case (int)BO.Enums.Common.FileFromFolder.TempFile_TempFolder:
                    case (int)BO.Enums.Common.FileFromFolder.TempImage_TempFolder:
                        FilePath = Server.MapPath(TempFolderPath + FileName);
                        break;
                    case (int)BO.Enums.Common.FileFromFolder.Draft_DraftAttachments:
                        FilePath = Server.MapPath(DraftFolderPath + FileName);
                        break;
                }
                if (ObjCommonbll.Temp_AttachedFileDeleteById(FileId, FileFrom, ComposeType))
                {
                    Status = true;
                    if (System.IO.File.Exists(FilePath))
                        System.IO.File.Delete(FilePath);
                }
            }
            return Json(Status, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteAttachedExcel(int FileId, int FileFrom, string FileName, int ComposeType = 0)
        {
            CommonBLL ObjCommonbll = new CommonBLL();
            bool Status = false;
            if (!string.IsNullOrWhiteSpace(FileName) && FileId > 0 && FileFrom > 0)
            {
                string FilePath = string.Empty;
                switch (FileFrom)
                {
                    case (int)BO.Enums.Common.FileFromFolder.TempFile_TempFolder:
                    case (int)BO.Enums.Common.FileFromFolder.TempImage_TempFolder:
                        FilePath = Server.MapPath(ExcelUploadPath + FileName);
                        break;
                    case (int)BO.Enums.Common.FileFromFolder.Draft_DraftAttachments:
                        FilePath = Server.MapPath(ExcelUploadPath + FileName);
                        break;
                }
                if (ObjCommonbll.Temp_AttachedFileDeleteById(FileId, FileFrom, ComposeType))
                {
                    Status = true;
                    if (System.IO.File.Exists(FilePath))
                        System.IO.File.Delete(FilePath);
                }
            }
            return Json(Status, JsonRequestBehavior.AllowGet);
        }

        public FileResult DownloadAttachedFile(int FileId, int FileFrom, string FileName) // @HTML.ActionLink //TODO: FileFrom must be Enum and download file if that doctor owns file.

        {

            string FilePath = string.Empty;
            //FileName = CommonBLL.GetFileNameBasedOnId(FileId, FileFrom);
            if (!string.IsNullOrWhiteSpace(FileName))
            {
                switch (FileFrom)
                {
                    case (int)BO.Enums.Common.FileFromFolder.TempFile_TempFolder:
                    case (int)BO.Enums.Common.FileFromFolder.TempImage_TempFolder:
                        FilePath = Server.MapPath(TempFolderPath + FileName);
                        break;
                    case (int)BO.Enums.Common.FileFromFolder.Draft_DraftAttachments:
                        FilePath = Server.MapPath(DraftFolderPath + FileName);
                        break;
                    case (int)BO.Enums.Common.FileFromFolder.Original_ForwardAttachments:
                        FilePath = Server.MapPath(OriginalFolderPath + FileName);
                        break;
                }
            }
            return File(FilePath, System.Net.Mime.MediaTypeNames.Application.Octet, FileName.Split('$').Last().Replace(" ", ""));

        }
        public string GetBase64ContentOFImage(long FileId, BO.Enums.Common.FileFromFolder FileFrom, string FileName) // @Html.Action("GetBase64ContentOFImage","Common", new {})
        {

            string FilePath = string.Empty;
            // FileName = CommonBLL.GetFileNameBasedOnId(FileId, FileFrom);
            if (!string.IsNullOrWhiteSpace(FileName))
            {
                switch (FileFrom)
                {
                    case BO.Enums.Common.FileFromFolder.TempFile_TempFolder:
                    case BO.Enums.Common.FileFromFolder.TempImage_TempFolder:
                        FilePath = Server.MapPath(TempFolderPath + FileName);
                        break;
                    case BO.Enums.Common.FileFromFolder.Draft_DraftAttachments:
                        FilePath = Server.MapPath(DraftFolderPath + FileName);
                        break;
                    case BO.Enums.Common.FileFromFolder.Original_ForwardAttachments:
                        FilePath = Server.MapPath(OriginalFolderPath + FileName);
                        break;
                    case BO.Enums.Common.FileFromFolder.Banner_Images:
                        FilePath = Server.MapPath(BannerImages + FileName);
                        break;
                    case BO.Enums.Common.FileFromFolder.Gallery_Images:
                        FilePath = Server.MapPath(GalleryFiles + FileName);
                        break;
                    case BO.Enums.Common.FileFromFolder.ImageBank:
                        FilePath = Server.MapPath(ThumbPath + FileName);
                        break;
                    case BO.Enums.Common.FileFromFolder.DentistImage:
                        FilePath = Server.MapPath(DentistImage + FileName);
                        break;
                    case BO.Enums.Common.FileFromFolder.DirectPath:
                        FilePath = FileName.Replace("../../../", "/");
                        FilePath = FileName.Replace("../../", "/");
                        FilePath = FileName.Replace("../", "/");
                        FilePath = Server.MapPath(FilePath);
                        break;
                }
            }
            if (System.IO.File.Exists(FilePath))
            {
                return clsHelper.GetImageBase64(FilePath);
            }
            else
            {
                return null;
            }

        }
        //public void SplitDirectImagePathwithBase64(string FullFileName)
        //{
        //    string FileName = string.Empty;
        //    string FileFrom = string.Empty;
        //    string Split = null;
        //    if(!string.IsNullOrWhiteSpace(FullFileName))
        //    {
        //        Split = FullFileName.Substring(FullFileName.LastIndexOf("/"));
        //    }
        //}
        public ActionResult DownloadPerticulareFileForReferral(int ReferralCardId, int DocumentId, string IsDocument)
        {
            try
            {
                MessagesBLL Model = new MessagesBLL();
                getFilenames = Model.DownloadParticulareFileForReferral(ReferralCardId, DocumentId, Convert.ToBoolean(IsDocument));
                if (getFilenames != null && getFilenames.Rows.Count > 0)
                {
                    ZipAllFiles(DocumentId);
                }
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("Common Controller--DownloadPerticulareFileForReferral", Ex.Message, Ex.StackTrace);
                throw;
            }
            return Json(JsonRequestBehavior.AllowGet);
        }
        public ActionResult DownloadAllAttachmentofReferral(int ReferralCardId, int PatientId)
        {
            try
            {
                MessagesBLL Model = new MessagesBLL();
                getFilenames = Model.GetFileForDownloadReferralAllAttachment(ReferralCardId, PatientId);
                if (getFilenames != null && getFilenames.Rows.Count > 0)
                {
                    ZipAllFiles(ReferralCardId);
                }
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("Common Controller--DownloadAllAttachmentofReferral", Ex.Message, Ex.StackTrace);
                throw;
            }
            return Json(JsonRequestBehavior.AllowGet);
        }
        //public ActionResult DownloadPerticularFilesofRefrral(i)
        public ActionResult DownloadAllAttachmentofMessage(int DownId, bool Isreferral)
        {
            try
            {
                MessagesBLL Model = new MessagesBLL();
                getFilenames = Model.GetDownloadZipFileDataForMessage(DownId, Isreferral);
                if (getFilenames != null && getFilenames.Rows.Count > 0)
                {
                    ZipAllFiles(DownId);
                }
                else
                {
                    return Json("No files found.");
                }
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("INBOX Controller--DownloadAttachmentofMessage", Ex.Message, Ex.StackTrace);
                throw;
            }
            return Json(JsonRequestBehavior.AllowGet);
        }
        public ActionResult DownloadPerticularfileofMessage(int DownId, int AttaId)
        {
            try
            {
                clsColleaguesData CLScolleagues = new clsColleaguesData();
                MessagesBLL Model = new MessagesBLL();
                getFilenames = Model.GetAttachmentFileofMessage(DownId, AttaId);
                if (getFilenames != null && getFilenames.Rows.Count > 0)
                {
                    ZipAllFiles(AttaId);
                }
                else
                {
                    return Json("No files found.");
                }

            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("INBOX Controller--DownloadAttachedfileofMessage", Ex.Message, Ex.StackTrace);
                throw;
            }
            return Json(JsonRequestBehavior.AllowGet);
        }
        public void ZipAllFiles(int MessageId)
        {
            try
            {
                byte[] buffer = new byte[4096];
                var tempFileName = Server.MapPath(ConfigurationManager.AppSettings["TempUploadsFolderPath"]) + MessageId + ".zip";
                var zipOutputStream = new ZipOutputStream(System.IO.File.Create(tempFileName));
                var filePath = String.Empty;
                var fileName = String.Empty;
                var readBytes = 0;
                foreach (DataRow item in getFilenames.Rows)
                {
                    if (item["FileName"].ToString() != "")
                    {
                        fileName = item["FileName"].ToString();
                        filePath = Server.MapPath(fileName);
                        FileInfo fi = new FileInfo(filePath);
                        string PF_Name = String.Empty;
                        if (fi.Exists)
                        {
                            String[] History_Pat = fileName.Split('$');
                            if (History_Pat[0].Length > 0)
                            {
                                if (History_Pat[0].Equals(ConfigurationManager.AppSettings.Get("ImageBank")))
                                {
                                    PF_Name = History_Pat[2].ToString();
                                }
                                else
                                {
                                    PF_Name = fileName;
                                }
                            }
                            var zipEntry = new ZipEntry(PF_Name.Replace(" ", ""));
                            zipOutputStream.PutNextEntry(zipEntry);
                            using (var fs = System.IO.File.OpenRead(filePath))
                            {
                                do
                                {
                                    readBytes = fs.Read(buffer, 0, buffer.Length);
                                    zipOutputStream.Write(buffer, 0, readBytes);
                                } while (readBytes > 0);
                            }
                        }
                    }
                }
                getFilenames.Clear();
                if (zipOutputStream.Length == 0)
                {
                    return;
                }
                zipOutputStream.Finish();
                zipOutputStream.Close();
                Response.ContentType = "application/x-zip-compressed";
                Response.AppendHeader("Content-Disposition", "attachment; filename=MessageId_" + MessageId + "_Records.zip");
                Response.BufferOutput = true;
                Response.WriteFile(tempFileName);
                Response.Flush();
                Response.Close();
                //delete the temp file  
                if (System.IO.File.Exists(tempFileName))
                    System.IO.File.Delete(tempFileName);
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("ZipAllFile---Common", Ex.Message, Ex.StackTrace);
                throw;
            }


        }

        [HttpPost]
        public ActionResult UploadFiles(string SesionId = "", string path = null)
        {
            CommonBLL ObjCommonbll = new CommonBLL();
            string UploadedFileIds = string.Empty;
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    List<string> objId = new List<string>();
                    if (!string.IsNullOrEmpty(SessionManagement.UserId))
                    {
                        SesionId = SessionManagement.UserId.ToString();
                    }
                    else
                    {
                        // Code for when file uploaded from public profile set public profile user id - start
                        if (!string.IsNullOrEmpty(Convert.ToString(Session["PublicProfileUserId"])))
                        {
                            SesionId = Convert.ToString(Session["PublicProfileUserId"]);
                        }
                        // Code for when file uploaded from public profile set public profile user id - end
                    }
                    for (int i = 0; i < files.Count; i++)
                    {

                        HttpPostedFileBase file = files[i];
                        int RecordId = 0; string Filename = string.Empty; string FileExtension = string.Empty; string NewFileName = string.Empty;
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            Filename = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            Filename = file.FileName;
                        }

                        Filename = Regex.Replace(Filename, @"[^0-9a-zA-Z\._]", string.Empty);
                        NewFileName = "$" + DateTime.Now.Ticks + "$" + Filename;
                        FileExtension = Path.GetExtension(Filename).ToLower();

                        if (!new CommonBLL().CheckFileIsValidImage(Filename))
                        {
                            RecordId = ObjCommonbll.Insert_Temp_UploadedFiles(Convert.ToInt32(SesionId.Split('?')[0]), NewFileName, ObjCommonbll.GetUniquenumberForAttachments());
                            objId.Add("F_" + RecordId);
                        }
                        else
                        {
                            RecordId = ObjCommonbll.Insert_Temp_UploadedImages(Convert.ToInt32(SesionId.Split('?')[0]), NewFileName, ObjCommonbll.GetUniquenumberForAttachments());
                            objId.Add("I_" + RecordId);
                        }
                        NewFileName = Path.Combine(Server.MapPath(TempFolderPath), NewFileName);
                        file.SaveAs(NewFileName);
                    }
                    UploadedFileIds = string.Join(",", objId);
                    return Json(UploadedFileIds);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }

        public ActionResult UploadExcel(string SesionId = "", string path = null)
        {
            CommonBLL ObjCommonbll = new CommonBLL();
            string UploadedFileIds = string.Empty;
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    List<string> objId = new List<string>();
                    if (!string.IsNullOrEmpty(SessionManagement.UserId))
                    {
                        SesionId = SessionManagement.UserId.ToString();
                    }
                    else
                    {
                        // Code for when file uploaded from public profile set public profile user id - start
                        if (!string.IsNullOrEmpty(Convert.ToString(Session["PublicProfileUserId"])))
                        {
                            SesionId = Convert.ToString(Session["PublicProfileUserId"]);
                        }
                        // Code for when file uploaded from public profile set public profile user id - end
                    }
                    for (int i = 0; i < files.Count; i++)
                    {

                        HttpPostedFileBase file = files[i];
                        int RecordId = 0; string Filename = string.Empty; string FileExtension = string.Empty; string NewFileName = string.Empty;
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            Filename = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            Filename = file.FileName;
                        }

                        Filename = Regex.Replace(Filename, @"[^0-9a-zA-Z\._]", string.Empty);
                        NewFileName = "$" + DateTime.Now.Ticks + "$" + Filename;
                        FileExtension = Path.GetExtension(Filename).ToLower();

                        if (!new CommonBLL().CheckFileIsValidImage(Filename))
                        {
                            RecordId = ObjCommonbll.Insert_Temp_UploadedFiles(Convert.ToInt32(SesionId.Split('?')[0]), NewFileName, ObjCommonbll.GetUniquenumberForAttachments());
                            objId.Add("F_" + RecordId);
                        }
                        else
                        {
                            RecordId = ObjCommonbll.Insert_Temp_UploadedImages(Convert.ToInt32(SesionId.Split('?')[0]), NewFileName, ObjCommonbll.GetUniquenumberForAttachments());
                            objId.Add("I_" + RecordId);
                        }
                        if (!string.IsNullOrWhiteSpace(path))
                        {
                            NewFileName = Path.Combine(path, NewFileName);
                        }
                        else
                        {
                            NewFileName = Path.Combine(Server.MapPath(ExcelUploadPath), NewFileName);
                        }
                        file.SaveAs(NewFileName);
                    }
                    UploadedFileIds = string.Join(",", objId);
                    return Json(UploadedFileIds);
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }



        public ActionResult UploadExcelFile()
        {
            CommonBLL ObjCommonbll = new CommonBLL();
            var UploadedFileIds = string.Empty;
            string SesionId = null;
            var jsonObj = string.Empty;
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    List<string> objId = new List<string>();
                    var lstFileUpload = new System.Collections.Generic.List<FileUpload>();
                    if (!string.IsNullOrEmpty(SessionManagement.UserId))
                    {
                        SesionId = SessionManagement.UserId.ToString();
                    }
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        int RecordId = 0; string Filename = string.Empty; string FileExtension = string.Empty; string NewFileName = string.Empty;
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            Filename = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            Filename = file.FileName;
                        }

                        Filename = Regex.Replace(Filename, @"[^0-9a-zA-Z\._]", string.Empty);
                        NewFileName = "$" + DateTime.Now.Ticks + "$" + Filename;

                        string extension = Path.GetExtension(Filename);
                        string savepathInviteCoulleague = "";
                        string tempPathInviteCoulleague = "";
                        tempPathInviteCoulleague = System.Configuration.ConfigurationManager.AppSettings["UploadFromInviteCoulleague"].ToString();
                        savepathInviteCoulleague = Server.MapPath(tempPathInviteCoulleague);
                        savepathInviteCoulleague = savepathInviteCoulleague.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");
                        string newpath = Path.Combine(savepathInviteCoulleague + NewFileName);

                        file.SaveAs(Path.Combine(savepathInviteCoulleague + NewFileName));

                        string ThumbUrl = string.Empty;
                        if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd" && extension.ToLower() != ".pspimage" && extension.ToLower() != ".thm" && extension.ToLower() != ".tif")
                        {
                            ThumbUrl = Request.Url.Scheme + "://" + Request.Url.Authority + "/Content/images/" + NewRecordlinc.Common.FileExtension.CheckFileExtenssionOnlyThumbName("unknown" + extension);
                        }
                        else
                        {
                            ThumbUrl = Request.Url.Scheme + "://" + Request.Url.Authority + tempPathInviteCoulleague + NewFileName;
                        }
                        lstFileUpload.Add(new FileUpload()
                        {
                            progress = "1.0",
                            name = NewFileName,
                            shortfilename = NewFileName.Split('$')[2],
                            size = file.ContentLength,
                            type = file.ContentType,
                            url = Request.Url.Scheme + "://" + Request.Url.Authority + tempPathInviteCoulleague + NewFileName,
                            thumbnail_url = ThumbUrl,
                            delete_url = Request.Url.Scheme + "://" + Request.Url.Authority + "/PatientFileUpload.ashx?DeleteId=1",
                            delete_type = "DELETE"
                        });
                    }
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    var uploadedFiles = lstFileUpload.ToArray();
                    jsonObj = js.Serialize(uploadedFiles);
                    Response.Write(jsonObj.ToString());
                    //UploadedFileIds = lstFileUpload.ToArray();
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
                return Json(jsonObj);
            }
            else
            {
                return Json("No files selected.");
            }
        }
        public ActionResult CheckAuthorizationByFeature(int FeatureId, int PatientId = 0)
        {

            PermissionResultBLL objpermissionbll = new PermissionResultBLL();
            BO.Models.PermissionResult objPermissionResult = new BO.Models.PermissionResult();
            objPermissionResult = objpermissionbll.CheckPermissionAndMemberShip(Convert.ToInt32(SessionManagement.UserId), FeatureId, SessionManagement.GetMemberPlanDetails, PatientId);
            if (objPermissionResult.HasPermission || !objPermissionResult.DoesMembershipAllow)
            {
                objPermissionResult.Message = PermissionResultBLL.GetPermissionMessage(FeatureId);
                return PartialView("~/Views/Shared/UpgradeMembership.cshtml", objPermissionResult);

            }
            return Json(true, JsonRequestBehavior.AllowGet);

        }
        public ActionResult CheckAuthorizationByFeatureDialog(int FeatureId, int PatientId = 0, int FileCount = 0)
        {

            PermissionResultBLL objpermissionbll = new PermissionResultBLL();
            BO.Models.PermissionResult objPermissionResult = new BO.Models.PermissionResult();
            objPermissionResult = objpermissionbll.CheckPermissionAndMemberShip(Convert.ToInt32(SessionManagement.UserId), FeatureId, SessionManagement.GetMemberPlanDetails, PatientId);
            int RemainCount = 0;
            if (Convert.ToInt32(objPermissionResult.MaxCount) > 0)
            {
                RemainCount = objPermissionResult.MaxCount - objPermissionResult.UsedCount;
            }
            if (objPermissionResult.HasPermission || !objPermissionResult.DoesMembershipAllow)
            {
                objPermissionResult.Message = PermissionResultBLL.GetDynamicPermissionMessage(FeatureId, objPermissionResult.MaxCount, objPermissionResult.UsedCount);
                return PartialView("~/Views/Shared/UpgradeMembershipDialog.cshtml", objPermissionResult);

            }
            else if (objPermissionResult.HasPermission || objPermissionResult.DoesMembershipAllow && FileCount > RemainCount && RemainCount > 0)
            {
                objPermissionResult.Message = PermissionResultBLL.GetDynamicPermissionMessage(FeatureId, objPermissionResult.MaxCount, objPermissionResult.UsedCount);
                return PartialView("~/Views/Shared/UpgradeMembershipDialog.cshtml", objPermissionResult);
            }
            return Json(true, JsonRequestBehavior.AllowGet);

        }

        public ActionResult CheckAuthorizationForExecelByFeature(int FeatureId, string UplaodeFileName = null)
        {

            PermissionResultBLL objpermissionbll = new PermissionResultBLL();
            BO.Models.PermissionResult objPermissionResult = new BO.Models.PermissionResult();
            ResponseStatus RS = new ResponseStatus();
            try
            {
                DataSet ds = new DataSet();
                DataTable dtExcelRecords = new DataTable();
                string fileName = Path.GetFileName(UplaodeFileName);
                string fileLocation = Server.MapPath("../CSVLoad/" + fileName);
                int RemainCount = 0;
                FileInfo fi = new FileInfo(fileLocation);
                if (!string.IsNullOrEmpty(UplaodeFileName))
                {
                    ds = ReadExcel(fi.FullName, fi.Extension, true);
                    dtExcelRecords = ds.Tables[0];
                    if (dtExcelRecords.Rows.Count > 0)
                    {
                        objPermissionResult = objpermissionbll.CheckPermissionAndMemberShip(Convert.ToInt32(SessionManagement.UserId), FeatureId, SessionManagement.GetMemberPlanDetails);
                        if (Convert.ToInt32(objPermissionResult.MaxCount) > 0)
                        {
                            RemainCount = objPermissionResult.MaxCount - objPermissionResult.UsedCount;
                        }
                        if (objPermissionResult.HasPermission || !objPermissionResult.DoesMembershipAllow)
                        {
                            objPermissionResult.Message = PermissionResultBLL.GetDynamicPermissionMessage(FeatureId, objPermissionResult.MaxCount, objPermissionResult.UsedCount);
                            return PartialView("~/Views/Shared/UpgradeMembershipDialog.cshtml", objPermissionResult);

                        }
                        else if (objPermissionResult.HasPermission || objPermissionResult.DoesMembershipAllow && dtExcelRecords.Rows.Count > RemainCount && RemainCount > 0)
                        {
                            objPermissionResult.Message = PermissionResultBLL.GetDynamicPermissionMessage(FeatureId, objPermissionResult.MaxCount, objPermissionResult.UsedCount);
                            return PartialView("~/Views/Shared/UpgradeMembershipDialog.cshtml", objPermissionResult);
                        }
                    }

                }
                RS.Status = true;
                return Json(RS, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                RS.Status = false;
                RS.ResponseMessage = "Excel data should be in proper format.";
                return Json(RS, JsonRequestBehavior.AllowGet);
            }



        }

        public ActionResult CheckAuthorizationForExecelByColleague(int FeatureId, string UplaodeFileName = null, string Email = null)
        {

            PermissionResultBLL objpermissionbll = new PermissionResultBLL();
            BO.Models.PermissionResult objPermissionResult = new BO.Models.PermissionResult();
            ResponseStatus RS = new ResponseStatus();
            try
            {
                DataSet ds = new DataSet();
                DataTable dtExcelRecords = new DataTable();
                string fileName = Path.GetFileName(UplaodeFileName);
                string fileLocation = Server.MapPath("../CSVLoad/" + fileName);
                int RemainCount = 0;
                int NumberOfColleagues = 0;
                // Call if there is Textbox aslo contain emails  
                if (Email != null && Email != "")
                {
                    string EmailAddress = Convert.ToString(Email);
                    string[] NumberOfEmails = EmailAddress.Split(',');
                    NumberOfColleagues = NumberOfEmails.Count();
                }
                FileInfo fi = new FileInfo(fileLocation);
                if (!string.IsNullOrEmpty(UplaodeFileName))
                {
                    ds = ReadExcel(fi.FullName, fi.Extension, true);
                    dtExcelRecords = ds.Tables[0];
                    if (dtExcelRecords.Rows.Count > 0)
                    {
                        NumberOfColleagues += dtExcelRecords.Rows.Count;
                    }

                }
                objPermissionResult = objpermissionbll.CheckPermissionAndMemberShip(Convert.ToInt32(SessionManagement.UserId), FeatureId, SessionManagement.GetMemberPlanDetails);
                if (Convert.ToInt32(objPermissionResult.MaxCount) > 0)
                {
                    RemainCount = objPermissionResult.MaxCount - objPermissionResult.UsedCount;
                }
                if (objPermissionResult.HasPermission || !objPermissionResult.DoesMembershipAllow)
                {
                    objPermissionResult.Message = PermissionResultBLL.GetDynamicPermissionMessage(FeatureId, objPermissionResult.MaxCount, objPermissionResult.UsedCount);
                    return PartialView("~/Views/Shared/UpgradeMembershipDialog.cshtml", objPermissionResult);

                }
                else if (objPermissionResult.HasPermission || objPermissionResult.DoesMembershipAllow && NumberOfColleagues > RemainCount && RemainCount > 0)
                {
                    objPermissionResult.Message = PermissionResultBLL.GetDynamicPermissionMessage(FeatureId, objPermissionResult.MaxCount, objPermissionResult.UsedCount);
                    return PartialView("~/Views/Shared/UpgradeMembershipDialog.cshtml", objPermissionResult);
                }
                RS.Status = true;
                return Json(RS, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                RS.Status = false;
                RS.ResponseMessage = "Your file must be in .xlsx or .xls format and at least one column name should be named Email.";
                return Json(RS, JsonRequestBehavior.AllowGet);
            }



        }
        public ActionResult GetNumberOfCountAllow(int FeatureId)
        {

            PermissionResultBLL objpermissionbll = new PermissionResultBLL();
            BO.Models.PermissionResult objPermissionResult = new BO.Models.PermissionResult();
            objPermissionResult = objpermissionbll.CheckPermissionAndMemberShip(Convert.ToInt32(SessionManagement.UserId), FeatureId, SessionManagement.GetMemberPlanDetails);
            if (objPermissionResult.HasPermission || !objPermissionResult.DoesMembershipAllow)
            {
                objPermissionResult.Message = PermissionResultBLL.GetPermissionMessage(FeatureId);
            }
            return Json(objPermissionResult, JsonRequestBehavior.AllowGet);

        }

        private DataSet ReadExcel(string filePath, string Extension, bool isHDR)
        {
            FileStream stream = System.IO.File.Open(filePath, FileMode.Open, FileAccess.Read);
            IExcelDataReader excelReader = null;

            switch (Extension)
            {
                case ".xls":
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                    break;
                case ".xlsx": //Excel 07
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    break;
            }

            excelReader.IsFirstRowAsColumnNames = isHDR;
            DataSet result = excelReader.AsDataSet();


            while (excelReader.Read())
            {

            }

            excelReader.Close();

            return result;

        }
        public ActionResult PartialFooter()
        {
            try
            {
                CommonBLL objbll = new CommonBLL();
                FooterDetails objfooter = new FooterDetails();
                objfooter.lstFooterCity = objbll.GetFooterCity();
                //objfooter.lstlocationList = objbll.GetLocationHistory();
                objfooter.lstSpecialityList = objbll.GetSpecialityHistory();
                return PartialView("PartialFooter", objfooter);
            }
            catch (Exception)
            {
                throw;
            }

        }
        public FileResult DownloadVcardOfDoctor(int UserId)
        {
            try
            {
                VCardBLL ObjVcardBLL = new VCardBLL();
                VCardForDoctor ObjVcard = new VCardForDoctor();
                ObjVcard = ObjVcardBLL.GetVCardDetialsOfDoctor(UserId);
                string FIlePath = VcardFilePath + ObjVcard.FName + "_" + ObjVcard.Userid + ".vcf";
                string filename = ObjVcard.FName + "_" + ObjVcard.Userid + ".vcf";
                if (System.IO.File.Exists(Server.MapPath(FIlePath)))
                {
                    string contents = System.IO.File.ReadAllText(Server.MapPath(FIlePath));
                    contents = contents.Replace(contents, ObjVcardBLL.BulidVCard(ObjVcard));
                    System.IO.File.WriteAllText(Server.MapPath(FIlePath), contents);
                }
                else
                {
                    var Myfile = System.IO.File.Create(Server.MapPath(FIlePath));
                    Myfile.Close();
                    string Content = ObjVcardBLL.BulidVCard(ObjVcard);
                    System.IO.File.WriteAllText(Server.MapPath(FIlePath), Content);
                }
                ////System.IO.File.c
                //using (var vCardFile = System.IO.File.OpenWrite(Server.MapPath(FIlePath)))
                //{
                //    using (var swWriter = new StreamWriter(vCardFile))
                //    {
                //        //string ModifiedContent = 
                //        swWriter.Write(ObjVcardBLL.BulidVCard(ObjVcard));
                //    }
                //}
                return File(FIlePath, System.Net.Mime.MediaTypeNames.Application.Octet, filename);
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("VcardImplimentation----DownloadVcardOfDoctor", Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        public ActionResult ImportExcel()
        {
            return PartialView("ImportExcelFile");
        }
        public ActionResult OGMetaTag(int UserId = 0)
        {
            MetaTag obj = new MetaTag();

            return View(obj);
        }
        /// <summary>
        /// This Action method used for One-Click Referral side For Direct Login into Recordlinc.
        /// </summary>
        /// <returns></returns>
        public ActionResult LoginRequestForOCR()
        {
            int UserId = 0; string RedirectURL = string.Empty;
            if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.Form["Redirect_URL"])))
            {
                RedirectURL = Convert.ToString(Request.Form["Redirect_URL"]);
                TempData["RedirectURLForOCR"] = RedirectURL;
                if (RedirectURL.Contains("Dashboard"))
                {
                    TempData["OpenReferral"] = "OpenReferralPopup";
                }
                if (RedirectURL.Contains("PatientHistory"))
                {
                    TempData["OCR"] = "PatientHistory";
                }
            }
            //Check User already login with Recordlinc
            if (string.IsNullOrWhiteSpace(SessionManagement.UserId))
            {
                //Check Request has access_token 
                if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.Form["access_token"])))
                {
                    //Get Dentist details based on access_token
                    DataTable Dt = clsCommon.GetDentistDetailsByAccessToken(Convert.ToString(Request.Form["access_token"]));
                    if (Dt != null && Dt.Rows.Count > 0)
                    {
                        UserId = Convert.ToInt32(Dt.Rows[0]["UserId"]);
                        //Generated Common Method which is fill the session as we are used its 2 time.
                        bool SessionStatus = FillSession(UserId, RedirectURL);
                        if (SessionStatus)
                        {
                            return Redirect(RedirectURL);
                        }
                        else
                        {
                            //IF Redirect URL is Empty then return to Login Page.
                            //return RedirectToAction("Index", "Login");
                            return RedirectToAction("Index", "Login");
                        }
                    }
                    else
                    {
                        //Redirect user to Login Page.
                        //return RedirectToAction("Index", "Login");
                        return RedirectToAction("Index", "Login");
                    }
                }
                else
                {
                    //Redirect User to Login Page.
                    // return RedirectToAction("Index", "Login");
                    return RedirectToAction("Index", "Login");
                }
            }
            else
            {
                #region This Code is written for If we are already generate a session for User1 and OCR side User1 is sign-out and login with User2 that time we need to clear all session of recordlinc and refill the User2 session.
                if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.Form["access_token"])))
                {
                    DataTable Dt = clsCommon.GetDentistDetailsByAccessToken(Convert.ToString(Request.Form["access_token"]));
                    if (Dt.Rows.Count > 0)
                    {
                        //Check User1 and access_token user id same if same then redirect to appropriate URL.
                        if (Convert.ToInt32(SessionManagement.UserId) == Convert.ToInt32(Dt.Rows[0]["UserId"]))
                        {
                            if (!string.IsNullOrWhiteSpace(RedirectURL))
                            {
                                return Redirect(RedirectURL);
                            }
                            else
                            {
                                //IF Redirect URL is Empty then return to Login Page.
                                //return RedirectToAction("Index", "Login");
                                return RedirectToAction("Index", "Login");
                            }
                        }
                        //Not Match with Session then Clear the session and re-build the session with latest user id.
                        else
                        {
                            Session.Clear();
                            TempData.Clear();
                            //Generated Common Method which is fill the session as we are used its 2 time.
                            bool SessionStatus = FillSession(Convert.ToInt32(Dt.Rows[0]["UserId"]), RedirectURL);
                            if (SessionStatus)
                            {
                                return Redirect(RedirectURL);
                            }
                            else
                            {
                                //IF Redirect URL is Empty then return to Login Page.
                                //return RedirectToAction("Index", "Login");
                                return RedirectToAction("Index", "Login");
                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(RedirectURL))
                        {
                            return Redirect(RedirectURL);
                        }
                        else
                        {
                            //IF Redirect URL is Empty then return to Login Page.
                            //return RedirectToAction("Index", "Login");
                            return RedirectToAction("Index", "Login");
                        }
                    }
                }
                ///If the Access token is empty then redirect to login page.
                else
                {
                    return RedirectToAction("Index", "Login");
                }
                #endregion
            }
        }
        public bool FillSession(int UserId, string RedirectURL)
        {
            DataTable dtLogin = clsCommon.GetDentistDetailsByUserId(UserId);
            if (dtLogin != null && dtLogin.Rows.Count > 0)
            {
                #region Fill the SessionManagement using User details
                SessionManagement.UserId = new clsCommon().CheckNull(Convert.ToString(dtLogin.Rows[0]["UserId"]), string.Empty);
                SessionManagement.ParentUserId = (!string.IsNullOrEmpty(Convert.ToString(dtLogin.Rows[0]["ParentUserId"])) ? Convert.ToString(dtLogin.Rows[0]["ParentUserId"]) : Convert.ToString(Session["UserId"]));
                SessionManagement.LocationId = new clsCommon().CheckNull(Convert.ToString(dtLogin.Rows[0]["LocationId"]), string.Empty);
                SessionManagement.FirstName = new clsCommon().CheckNull(Convert.ToString(dtLogin.Rows[0]["FirstName"]), string.Empty);
                SessionManagement.UserPaymentVerified = PaymentBLL.CheckUserPayment(Convert.ToInt32(SessionManagement.UserId));
                SessionManagement.GetMemberPlanDetails = new CommonBLL().GetUserPlanDetails(Convert.ToInt32(dtLogin.Rows[0]["UserId"]));
                string ImageName = "";
                ImageName = new clsCommon().CheckNull(Convert.ToString(dtLogin.Rows[0]["ImageName"]), string.Empty);
                if (string.IsNullOrEmpty(ImageName))
                {
                    if (!System.IO.File.Exists(ImageName))
                    {
                        SessionManagement.DoctorImage = new clsCommon().CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty);
                    }
                    else
                    {
                        SessionManagement.DoctorImage = new clsCommon().CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + ImageName;
                    }
                }
                else
                {
                    SessionManagement.DoctorImage = new clsCommon().CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + ImageName;
                }
                #endregion
                Session["DoctorImage"] = SessionManagement.DoctorImage;
                DataTable
                dtCheckFirst = new clsColleaguesData().GetFirstLoginCount(Convert.ToInt32(SessionManagement.UserId));
                bool Result = false;
                Result = new clsCommon().InsertLogedInfo(Convert.ToInt32(SessionManagement.UserId));
                //Check Redirect URL is Null or not.
                if (!string.IsNullOrWhiteSpace(RedirectURL))
                {
                    return true;
                }
                else
                {
                    //IF Redirect URL is Empty then return to Login Page.
                    return false;
                }
            }
            else
            {
                //Redirect user to Login Page as .
                //return RedirectToAction("Index", "Login");
                return false;
            }
        }
    }
}
