using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using NewRecordlinc.Models.Patients;
using DataAccessLayer.PatientsData;
using DataAccessLayer.Common;
using System.Data;
using System.Text;
using DataAccessLayer.ColleaguesData;
using NewRecordlinc.App_Start;
using System.Configuration;
using System.IO;
using System.Web.UI.WebControls;
using NewRecordlinc.Models.AccountSettings;
using System.Net;
using System.Web.Script.Serialization;
using System.Web.Helpers;
using BusinessLogicLayer;
using PagedList;
using BO.ViewModel;
using BO.Models;

namespace NewRecordlinc.Controllers
{
    public class AccountSettingsController : Controller
    {
        //
        // GET: /AccountSettings/
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        clsPatientsData ObjPatientsData = new clsPatientsData();
        clsCommon ObjCommon = new clsCommon();
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        DataAccessLayer.Appointment.clsAppointmentData objclsAppointmentData = new DataAccessLayer.Appointment.clsAppointmentData();
        public ActionResult Index()
        {
            int UserId = 0;
            if (!string.IsNullOrEmpty(Convert.ToString(Session["ParentUserId"]))) { UserId = Convert.ToInt32(Session["ParentUserId"]); }
            else { UserId = Convert.ToInt32(Session["UserId"]); }
            var dt = objclsAppointmentData.GetLocationForAppointment(UserId);
            ViewBag.locationList = null;
            if (dt != null)
            {
                ViewBag.locationList = (from p in dt.AsEnumerable()
                                        select new Models.AccountSettings.Location
                                        {
                                            locationId = Convert.ToInt32(p["AddressInfoId"]),
                                            locationName = Convert.ToString(p["Location"])
                                        }
                                ).ToList();
            }
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                return View();
            }

            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        [HttpPost]
        public JsonResult GetCurrentData(int UserId = 0)
        {

            string PrimaryEmail = string.Empty;
            string SecondaryEmail = string.Empty;
            string DropdownList = string.Empty;
            string Staylogged = string.Empty;
            string LoginUserName = string.Empty;
            string GuidKay = string.Empty;
            string AccountKey = string.Empty;
            string AccountId = string.Empty;
            int LocationId = 0;

            if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || UserId == 0)
                UserId = Convert.ToInt32(SessionManagement.UserId);
            string DentrixProviderId = string.Empty;
            bool SyncInfusionSoft = false;
            try
            {

                DataSet ds = new DataSet();
                ds = ObjColleaguesData.GetDoctorDetailsById(UserId);

                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        PrimaryEmail = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Username"]), string.Empty);
                        SecondaryEmail = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["SecondaryEmail"]), string.Empty);
                        DropdownList = FillStayloggedMins(ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["StayloggedMins"]), string.Empty));
                        Staylogged = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["StayloggedMins"]), string.Empty);
                        LoginUserName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LoginUserName"]), string.Empty);
                        LocationId = Convert.ToInt32(ds.Tables[0].Rows[0]["LocationId"]);
                        DentrixProviderId = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["DentrixProviderId"]), string.Empty);


                    }
                    if (ds.Tables[8].Rows.Count > 0)
                    {
                        AccountKey = ObjCommon.CheckNull(Convert.ToString(ds.Tables[8].Rows[0]["AccountKey"]), string.Empty);
                        AccountId = ObjCommon.CheckNull(Convert.ToString(ds.Tables[8].Rows[0]["AccountID"]), string.Empty);
                    }
                    if (ds.Tables[9].Rows.Count > 0)
                    {
                        SyncInfusionSoft = (bool)ds.Tables[9].Rows[0]["IsActive"];
                    }

                }
                DataTable dt = new DataTable();
                dt = ObjColleaguesData.GetDentrixConnectorValuesofUser(UserId);

                if (dt.Rows.Count > 0 && dt != null)
                {
                    string Last = string.Empty;
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        Last = Convert.ToString(dt.Rows[i]["ProcessDate"]);
                        Last = new CommonBLL().ConvertToDateTime(Convert.ToDateTime(dt.Rows[i]["ProcessDate"]), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL));
                        string View = string.Empty;
                        if (string.IsNullOrWhiteSpace(Last))
                        {
                            View = "You have never sync dentrix";
                        }
                        else
                        {
                            View = "Last sync date : " + Last;
                        }
                        GuidKay += "<li class=\"li\"><label>GUID Key</label><img src=\"/Content/images/old2-icon_delete.png\" style=\"float:right;margin-top:11px;\" onclick=\"return DeleteDentrixConnectorFromId('" + dt.Rows[i]["DentrixConnectorId"] + "');\"/><img src=\"/Images/sync_now.png\" onclick=\"IsSyncDentrix('" + dt.Rows[i]["DentrixConnectorId"] + "');\" value=\"SyncDentrix\" style=\"float:right;margin-top:11px;height:36px;margin-right:4px;\" /><span class=\"fluid fluid_cont\" id=\"SelectLocation" + i + "\"></span><div class=\"grey_frmbg\" style=\"width: 30%;\"><input type=\"text\" title=\"GUID Key\" id=\"" + dt.Rows[i]["DentrixConnectorId"] + "\" name=\"txtGUID\" readonly=\"readonly\" style=\"float:right;padding: 9.5px !important;\" value=\"" + dt.Rows[i]["AccountKey"] + "\" /></div><p style=\"margin-top:42px;color: #515152;font-size:14px;\">" + View + "</p></li>?" + dt.Rows[i]["LocationId"] + "";
                        GuidKay += "*";
                    }
                    GuidKay = GuidKay.TrimEnd('*');
                }

            }
            catch (Exception)
            {
                throw;
            }

            return Json(new { LID = LocationId, PE = PrimaryEmail, SE = SecondaryEmail, DDL = DropdownList, SLV = Staylogged, AKey = AccountKey, AId = AccountId, LOGU = LoginUserName, blInfusionSoft = SyncInfusionSoft, Guidis = GuidKay, DentrixProviderId = DentrixProviderId }, JsonRequestBehavior.AllowGet);
        }

        public string FillStayloggedMins(string value)
        {
            StringBuilder sb = new StringBuilder();



            try
            {
                List<SelectListItem> lst = new List<SelectListItem>();
                lst = GetStayloggedMins();

                if (string.IsNullOrEmpty(value))
                {
                    value = "60"; // Default values
                }
                sb.Append("<select id=\"ddlStayloggedMins\" >");


                foreach (var item in lst)
                {

                    if (item.Value == value)
                    {
                        sb.Append("<option value=\"" + item.Value + "\" selected=\"selected\">" + item.Text + "</option>");
                    }
                    else
                    {

                        sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");

                    }
                }
                sb.Append("</select>");



            }
            catch (Exception)
            {

                throw;
            }

            return sb.ToString();
        }

        public List<SelectListItem> GetStayloggedMins()
        {
            List<SelectListItem> _StayloggedMins = new List<SelectListItem>();



            _StayloggedMins.Add(new SelectListItem { Text = "1 Hour", Value = "60" });
            _StayloggedMins.Add(new SelectListItem { Text = "2 Hour", Value = "120" });
            _StayloggedMins.Add(new SelectListItem { Text = "3 Hour", Value = "180" });
            _StayloggedMins.Add(new SelectListItem { Text = "4 Hour", Value = "240" });
            _StayloggedMins.Add(new SelectListItem { Text = "5 Hour", Value = "300" });
            _StayloggedMins.Add(new SelectListItem { Text = "6 Hour", Value = "360" });
            _StayloggedMins.Add(new SelectListItem { Text = "7 Hour", Value = "420" });
            _StayloggedMins.Add(new SelectListItem { Text = "8 Hour", Value = "480" });
            _StayloggedMins.Add(new SelectListItem { Text = "1 Day", Value = "1440" });
            _StayloggedMins.Add(new SelectListItem { Text = "1 Week", Value = "10080" });

            return _StayloggedMins;
        }

        [HttpPost]
        public JsonResult GetLocationList(string selected, int UserId = 0)
        {

            if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || UserId == 0)
                UserId = Convert.ToInt32(SessionManagement.UserId);

            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();
                dt = ObjColleaguesData.GetDoctorLocaitonById(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    sb.Append("<select id=\"select4\" class=\"selection\" name=\"select4\" style=\"width:100%\">");
                    foreach (DataRow item in dt.Rows)
                    {
                        string Id = Convert.ToString(item["AddressInfoId"]);
                        if (selected.Split(',').Contains(Id))
                        {
                            if (Id != null)
                            {
                                sb.Append("<option value=" + Convert.ToInt32(item["AddressInfoId"]) + " class=\"selected\" selected=\"selected\">" + item["Location"]);
                            }

                        }
                        //else if (Convert.ToInt32(item["ContactType"]) == 1)
                        //{
                        //    sb.Append("<option value=" + Convert.ToInt32(item["AddressInfoId"]) + " class=\"selected\" selected=\"selected\">" + item["Location"]);
                        //}
                        else
                        {
                            sb.Append("<option value=" + Convert.ToInt32(item["AddressInfoId"]) + ">" + item["Location"]);
                        }
                    }

                    sb.Append("</select>");

                    sb.ToString();
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public List<SelectListItem> NewGetLocationList(string selected, int UserId = 0)
        {
            List<SelectListItem> lst = new List<SelectListItem>();
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || UserId == 0)
                UserId = Convert.ToInt32(SessionManagement.UserId);
            try
            {
                DataTable dtd = new DataTable();// Dentrix location
                dtd = ObjColleaguesData.GetDentrixConnectorValuesofUser(UserId);

                DataTable dt = new DataTable();//all location
                dt = ObjColleaguesData.GetDoctorLocaitonById(UserId);


                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        string Id = Convert.ToString(item["AddressInfoId"]);
                        if (selected.Split(',').Contains(Id))
                        {
                            lst.Add(new SelectListItem { Text = item["Location"].ToString(), Value = item["AddressInfoId"].ToString(), Selected = true });
                        }
                        else
                        {
                            lst.Add(new SelectListItem { Text = item["Location"].ToString(), Value = item["AddressInfoId"].ToString() });
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lst;
        }

        [HttpPost]
        public JsonResult UpdateData(string NewPrimaryEmail, string NewSecondaryEmail, string NewPassword, string Identifier, string NewStayloggedMins, string LoginUserName, int LocationId)
        {


            string OldPrimaryEmail = string.Empty;
            string OldSecondaryEmail = string.Empty;
            string OldStaylogged = string.Empty;
            string OldPassword = string.Empty;
            string OldLoginUserName = string.Empty;

            string EncryptedPassword = string.Empty;
            string[] Indentifiesarray = new string[5];

            string obj = string.Empty;

            try
            {
                DataSet ds = new DataSet();
                ds = ObjColleaguesData.GetDoctorDetailsById(Convert.ToInt32(Session["UserId"]));
                if (ds != null && ds.Tables.Count > 0)
                {
                    OldPrimaryEmail = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Username"]), string.Empty);
                    OldSecondaryEmail = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["SecondaryEmail"]), string.Empty);
                    OldPassword = ObjTripleDESCryptoHelper.decryptText(ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Password"]), string.Empty));
                    OldStaylogged = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["StayloggedMins"]), string.Empty);
                    OldLoginUserName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LoginUserName"]), string.Empty);
                }
                if (!string.IsNullOrWhiteSpace(NewStayloggedMins))
                {
                    Session.Timeout = Convert.ToInt32(NewStayloggedMins);
                }



                if (!string.IsNullOrEmpty(NewPassword))
                {
                    EncryptedPassword = ObjTripleDESCryptoHelper.encryptText(NewPassword);
                }
                else
                {
                    EncryptedPassword = ObjTripleDESCryptoHelper.encryptText(OldPassword);
                }

                if (!string.IsNullOrEmpty(Identifier))
                {
                    string Identifiestocheck = Identifier;
                    if (!string.IsNullOrEmpty(LoginUserName))
                    {
                        DataTable dttbl = new DataTable();
                        dttbl = ObjColleaguesData.CheckEmailInSystem(LoginUserName);
                        if (Identifier != "5")
                        {
                            if (dttbl != null && dttbl.Rows.Count > 0)
                            {
                                obj = "User Name already exists in system.";
                                return Json(new { obj = obj }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {

                                Identifier = Identifier.Replace("5", "");
                                bool p1 = ObjColleaguesData.UpdateUserName(Convert.ToInt32(Session["UserId"]), LoginUserName);
                                if (p1)
                                {
                                    obj = "User Name updated successfully.";
                                }
                                else
                                {
                                    obj = "Failed to update User Name.";


                                }

                            }
                        }
                    }
                    DataTable dt = new DataTable();
                    if (Identifier == "1")
                    {
                        #region Check and Update Primary Email
                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailInSystem(NewPrimaryEmail);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "This primary email address is already in use.";
                        }
                        else
                        {
                            bool p1 = ObjColleaguesData.UpdatePrimaryEmail(Convert.ToInt32(Session["UserId"]), NewPrimaryEmail);
                            if (p1)
                            {
                                if (Identifiestocheck.Contains("5"))
                                {
                                    obj = "Account settings updated successfully.";
                                }
                                else
                                {
                                    obj = "Primary email updated successfully";
                                }
                            }
                            else
                            {
                                obj = "Failed to update primary email";

                            }

                        }
                        #endregion
                    }
                    if (Identifier == "2")
                    {
                        #region Check and Update Secondary Email
                        // dt = new DataTable();
                        // dt = ObjColleaguesData.CheckEmailInSystem(NewSecondaryEmail);
                        // if (dt != null && dt.Rows.Count > 0)
                        // {
                        //     obj = "Secondary email is already exists in system";
                        // }
                        // else
                        // {
                        bool s1 = ObjColleaguesData.UpdateSecondaryEmail(Convert.ToInt32(Session["UserId"]), NewSecondaryEmail);
                        if (s1)
                        {
                            if (Identifiestocheck.Contains("5"))
                            {
                                obj = "Account settings updated successfully.";
                            }
                            else
                            {
                                obj = "Secondary email updated successfully";
                            }
                        }
                        else
                        {
                            obj = "Failed to update secondary email";

                        }
                        //   }
                        #endregion
                    }
                    if (Identifier == "3")
                    {
                        #region Update Password
                        bool updatepass = ObjColleaguesData.UpdatePasswordOfDoctor(Convert.ToInt32(Session["UserId"]), EncryptedPassword);
                        if (updatepass)
                        {
                            if (Identifiestocheck.Contains("5"))
                            {
                                obj = "Account settings updated successfully.";
                            }
                            else
                            {
                                obj = "Password updated successfully";
                            }
                        }
                        else
                        {
                            obj = "Failed to update password";

                        }
                        #endregion

                    }

                    if (Identifier.Contains("4"))
                    {
                        #region Update Staylogged time
                        bool result = ObjColleaguesData.UpdateStayloggedMinsOfDoctor(Convert.ToInt32(Session["UserId"]), NewStayloggedMins);
                        if (result)
                        {
                            if (Identifiestocheck.Contains("5"))
                            {
                                obj = "Account settings updated successfully.";
                            }
                            else
                            {
                                obj = "Logged in Time has been updated successfully";
                            }
                        }
                        else
                        {
                            obj = "Failed to update loggedintime";

                        }
                        #endregion
                    }
                    if (Identifier == "5")
                    {
                        #region Update Login UserName
                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailInSystem(LoginUserName);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "User Name is already exists in system";
                        }
                        else
                        {
                            bool p1 = ObjColleaguesData.UpdateUserName(Convert.ToInt32(Session["UserId"]), LoginUserName);
                            if (p1)
                            {
                                obj = "User Name updated successfully.";
                            }
                            else
                            {
                                obj = "Failed to update User Name.";

                            }

                        }
                        #endregion
                    }


                    if (Identifier == "12")
                    {
                        #region Check and update Primary and Secodary Email


                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailInSystem(NewPrimaryEmail);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "This primary email address is already in use.";
                        }
                        else
                        {
                            // Update Primary Email method here 
                            bool Up1 = ObjColleaguesData.UpdatePrimaryEmail(Convert.ToInt32(Session["UserId"]), NewPrimaryEmail);
                            if (Up1)
                            {
                                DataTable dtsec = new DataTable();
                                dtsec = ObjColleaguesData.CheckEmailInSystem(NewSecondaryEmail);
                                if (dtsec != null && dtsec.Rows.Count > 0)
                                {
                                    obj = "Secondary email is already exists in system";
                                }
                                else
                                {
                                    bool s2 = ObjColleaguesData.UpdateSecondaryEmail(Convert.ToInt32(Session["UserId"]), NewSecondaryEmail);
                                    if (s2)
                                    {
                                        obj = "Account settings updated successfully.";
                                    }
                                    else
                                    {
                                        obj = "Failed to update secondary email";
                                    }

                                }
                            }
                            else
                            {
                                obj = "Falied to update primary email";
                            }


                        }
                        #endregion
                    }
                    if (Identifier == "13")
                    {
                        #region Check and update Primary Email and Password


                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailInSystem(NewPrimaryEmail);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "This primary email address is already in use.";
                        }
                        else
                        {
                            // Update Primary Email method here 
                            bool Up1 = ObjColleaguesData.UpdatePrimaryEmail(Convert.ToInt32(Session["UserId"]), NewPrimaryEmail);
                            if (Up1)
                            {
                                #region Update Password
                                bool updatepass = ObjColleaguesData.UpdatePasswordOfDoctor(Convert.ToInt32(Session["UserId"]), EncryptedPassword);
                                if (updatepass)
                                {

                                    obj = "Account settings updated successfully.";
                                }
                                else
                                {
                                    obj = "Falied to update primary email and password";
                                }
                                #endregion
                            }
                            else
                            {
                                obj = "Falied to update primary email";
                            }


                        }
                        #endregion
                    }

                    if (Identifier == "14")
                    {
                        #region Check and update Primary Email and LoggedInTime


                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailInSystem(NewPrimaryEmail);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "This primary email address is already in use.";
                        }
                        else
                        {
                            // Update Primary Email method here 
                            bool Up1 = ObjColleaguesData.UpdatePrimaryEmail(Convert.ToInt32(Session["UserId"]), NewPrimaryEmail);
                            if (Up1)
                            {
                                #region Update Staylogged time
                                bool result = ObjColleaguesData.UpdateStayloggedMinsOfDoctor(Convert.ToInt32(Session["UserId"]), NewStayloggedMins);
                                if (result)
                                {
                                    obj = "Account settings updated successfully.";
                                }
                                else
                                {
                                    obj = "Failed to update loggedintime";
                                }
                                #endregion
                            }
                            else
                            {
                                obj = "Falied to update primary email";
                            }


                        }
                        #endregion
                    }
                    if (Identifier == "23")
                    {
                        #region Check and update Secondary Email and Password


                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailInSystem(NewSecondaryEmail);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "Secondary email is already exists in system";
                        }
                        else
                        {
                            // Update SecondaryEmail Email method here 
                            bool Up1 = ObjColleaguesData.UpdateSecondaryEmail(Convert.ToInt32(Session["UserId"]), NewSecondaryEmail);
                            if (Up1)
                            {
                                #region Update Password
                                bool updatepass = ObjColleaguesData.UpdatePasswordOfDoctor(Convert.ToInt32(Session["UserId"]), EncryptedPassword);
                                if (updatepass)
                                {
                                    obj = "Account settings updated successfully.";
                                }
                                else
                                {
                                    obj = "Falied to update secondary email and password";
                                }
                                #endregion
                            }
                            else
                            {
                                obj = "Falied to update secondary email";
                            }


                        }
                        #endregion
                    }

                    if (Identifier == "24")
                    {
                        #region Check and update Secondary Email and LoggedIntime


                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailInSystem(NewSecondaryEmail);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "Secondary email is already exists in system";
                        }
                        else
                        {
                            // Update SecondaryEmail Email method here 
                            bool Up1 = ObjColleaguesData.UpdateSecondaryEmail(Convert.ToInt32(Session["UserId"]), NewSecondaryEmail);
                            if (Up1)
                            {
                                #region Update Staylogged time
                                bool result = ObjColleaguesData.UpdateStayloggedMinsOfDoctor(Convert.ToInt32(Session["UserId"]), NewStayloggedMins);
                                if (result)
                                {
                                    obj = "Account settings updated successfully.";
                                }
                                else
                                {
                                    obj = "Failed to update loggedintime";
                                }
                                #endregion
                            }
                            else
                            {
                                obj = "Falied to update secondary email";
                            }


                        }
                        #endregion
                    }

                    if (Identifier == "34")
                    {
                        #region Update Password and loggedintime
                        bool updatepass = ObjColleaguesData.UpdatePasswordOfDoctor(Convert.ToInt32(Session["UserId"]), EncryptedPassword);
                        if (updatepass)
                        {
                            #region Update Staylogged time
                            bool result = ObjColleaguesData.UpdateStayloggedMinsOfDoctor(Convert.ToInt32(Session["UserId"]), NewStayloggedMins);
                            if (result)
                            {
                                obj = "Account settings updated successfully.";
                            }
                            else
                            {
                                obj = "Failed to update loggedintime";
                            }
                            #endregion
                        }
                        else
                        {
                            obj = "Failed to update password";
                        }
                        #endregion
                    }

                    if (Identifier == "123")
                    {
                        #region Check and Update Primary Email,Seconday Email and Password
                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailInSystem(NewPrimaryEmail);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "This primary email address is already in use.";
                        }
                        else
                        {
                            bool p2 = ObjColleaguesData.UpdatePrimaryEmail(Convert.ToInt32(Session["UserId"]), NewPrimaryEmail);
                            if (p2)
                            {
                                #region Check and Update Secondary Email
                                DataTable dtsec = new DataTable();
                                dtsec = ObjColleaguesData.CheckEmailInSystem(NewSecondaryEmail);
                                if (dtsec != null && dtsec.Rows.Count > 0)
                                {
                                    obj = "Secondary email is already exists in system";
                                }
                                else
                                {
                                    bool s2 = ObjColleaguesData.UpdateSecondaryEmail(Convert.ToInt32(Session["UserId"]), NewSecondaryEmail);
                                    if (s2)
                                    {
                                        #region Update Password
                                        bool updatepass = ObjColleaguesData.UpdatePasswordOfDoctor(Convert.ToInt32(Session["UserId"]), EncryptedPassword);
                                        if (updatepass)
                                        {
                                            obj = "Account settings updated successfully.";
                                        }
                                        else
                                        {
                                            obj = "Failed to update password";
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        obj = "Failed to update secondary email";
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                obj = "Failed to update primary email";
                            }
                        }
                        #endregion
                    }

                    if (Identifier == "124")
                    {
                        #region Check and Update Primary Email,Seconday Email and LoggedInTime
                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailInSystem(NewPrimaryEmail);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "This primary email address is already in use.";
                        }
                        else
                        {
                            bool p2 = ObjColleaguesData.UpdatePrimaryEmail(Convert.ToInt32(Session["UserId"]), NewPrimaryEmail);
                            if (p2)
                            {
                                #region Check and Update Secondary Email
                                DataTable dtsec = new DataTable();
                                dtsec = ObjColleaguesData.CheckEmailInSystem(NewSecondaryEmail);
                                if (dtsec != null && dtsec.Rows.Count > 0)
                                {
                                    obj = "Secondary email is already exists in system";
                                }
                                else
                                {
                                    bool s2 = ObjColleaguesData.UpdateSecondaryEmail(Convert.ToInt32(Session["UserId"]), NewSecondaryEmail);
                                    if (s2)
                                    {
                                        #region Update Staylogged time
                                        bool result = ObjColleaguesData.UpdateStayloggedMinsOfDoctor(Convert.ToInt32(Session["UserId"]), NewStayloggedMins);
                                        if (result)
                                        {
                                            obj = "Account settings updated successfully.";
                                        }
                                        else
                                        {
                                            obj = "Failed to update loggedintime";
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        obj = "Failed to update secondary email";
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                obj = "Failed to update primary email";
                            }
                        }
                        #endregion
                    }

                    if (Identifier == "234")
                    {
                        #region Check and Update Secondary Email,Password and LoggedinTime
                        DataTable dtsec = new DataTable();
                        dtsec = ObjColleaguesData.CheckEmailInSystem(NewSecondaryEmail);
                        if (dtsec != null && dtsec.Rows.Count > 0)
                        {
                            obj = "Secondary email is already exists in system";
                        }
                        else
                        {
                            bool s2 = ObjColleaguesData.UpdateSecondaryEmail(Convert.ToInt32(Session["UserId"]), NewSecondaryEmail);
                            if (s2)
                            {
                                #region Update Password
                                bool updatepass = ObjColleaguesData.UpdatePasswordOfDoctor(Convert.ToInt32(Session["UserId"]), EncryptedPassword);
                                if (updatepass)
                                {
                                    #region Update Staylogged time
                                    bool result = ObjColleaguesData.UpdateStayloggedMinsOfDoctor(Convert.ToInt32(Session["UserId"]), NewStayloggedMins);
                                    if (result)
                                    {
                                        obj = "Account settings updated successfully.";
                                    }
                                    else
                                    {
                                        obj = "Failed to update loggedintime";
                                    }
                                    #endregion
                                }
                                else
                                {
                                    obj = "Failed to update password";
                                }
                                #endregion

                            }
                            else
                            {
                                obj = "Failed to update secondary email";
                            }
                        }
                        #endregion
                    }

                    if (Identifier == "1234")
                    {
                        #region Check and Update Primary Email,secondary email, password and loggedtime
                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailInSystem(NewPrimaryEmail);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "This primary email address is already in use.";
                        }
                        else
                        {
                            bool p1 = ObjColleaguesData.UpdatePrimaryEmail(Convert.ToInt32(Session["UserId"]), NewPrimaryEmail);
                            if (p1)
                            {
                                #region Check and Update Secondary Email
                                DataTable dtsec = new DataTable();
                                dtsec = ObjColleaguesData.CheckEmailInSystem(NewSecondaryEmail);
                                if (dtsec != null && dtsec.Rows.Count > 0)
                                {
                                    obj = "Secondary email is already exists in system";
                                }
                                else
                                {
                                    bool s1 = ObjColleaguesData.UpdateSecondaryEmail(Convert.ToInt32(Session["UserId"]), NewSecondaryEmail);
                                    if (s1)
                                    {
                                        #region Update Password
                                        bool updatepass = ObjColleaguesData.UpdatePasswordOfDoctor(Convert.ToInt32(Session["UserId"]), EncryptedPassword);
                                        if (updatepass)
                                        {
                                            #region Update Staylogged time
                                            bool result = ObjColleaguesData.UpdateStayloggedMinsOfDoctor(Convert.ToInt32(Session["UserId"]), NewStayloggedMins);
                                            if (result)
                                            {
                                                obj = "Account settings updated successfully.";
                                            }
                                            else
                                            {
                                                obj = "Failed to update loggedintime";
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            obj = "Failed to update password";
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        obj = "Failed to update secondary email";
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                obj = "Failed to update primary email";
                            }

                        }
                        #endregion

                    }
                    if (Identifier.Contains("6"))
                    {
                        #region Update Current location
                        bool result = ObjColleaguesData.UpdateLocationIdOfDoctor(Convert.ToInt32(Session["UserId"]), LocationId);
                        if (result)
                        {
                            if (Identifier == "6")
                            {
                                SessionManagement.TimeZoneSystemName = new DataAccessLayer
                                                                            .Appointment
                                                                            .clsAppointmentData()
                                                                            .GetTimeZoneOfDoctor(Convert.ToInt32(Session["UserId"]));
                                obj = "Location has been updated successfully";
                                Session["LocationId"] = LocationId;
                                Response.Cookies["LocationId"].Value = Convert.ToString(LocationId);
                                Response.Cookies["LocationId"].Expires = DateTime.Now.AddHours(24);
                            }
                            else
                            {
                                obj = "Account settings updated successfully.";
                                Session["LocationId"] = LocationId;
                                Response.Cookies["LocationId"].Value = Convert.ToString(LocationId);
                                Response.Cookies["LocationId"].Expires = DateTime.Now.AddHours(24);
                            }


                        }
                        else
                        {
                            obj = "Failed to update Location";
                        }
                        #endregion
                    }


                }
                else
                {

                    if (!string.IsNullOrEmpty(NewPrimaryEmail))
                    {
                        if (NewSecondaryEmail != OldSecondaryEmail)
                        {
                            bool s1 = ObjColleaguesData.UpdateSecondaryEmail(Convert.ToInt32(Session["UserId"]), NewSecondaryEmail);
                            if (s1)
                            {
                                obj = "Secondary email is updated successfully";
                            }
                            else
                            {
                                obj = "Failed to update secondary email";
                            }
                        }
                        else
                        {
                            obj = "";
                        }

                        if (OldStaylogged != NewStayloggedMins)
                        {
                            bool s1 = ObjColleaguesData.UpdateStayloggedMinsOfDoctor(Convert.ToInt32(Session["UserId"]), NewStayloggedMins);
                            if (s1)
                            {
                                obj = "";
                            }
                            else
                            {
                                obj = "Failed to update secondary email";
                            }
                        }
                        else
                        {
                            obj = "";
                        }


                    }
                    else
                    {
                        obj = "Please enter primary email.";
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(new { obj = obj }, JsonRequestBehavior.AllowGet);
        }

        #region Methods for admin
        [HttpPost]
        public JsonResult GetCurrentDataFromAdmin(string UserId)
        {
            string PrimaryEmail = string.Empty;
            string SecondaryEmail = string.Empty;
            string DropdownList = string.Empty;
            string Staylogged = string.Empty;
            string LoginUserName = string.Empty;
            string enUID = string.Empty;
            string AccountKey = string.Empty;
            string AccountId = string.Empty;
            try
            {

                TripleDESCryptoHelper objTripleDESCryptoHelper = new TripleDESCryptoHelper();

                if (!string.IsNullOrEmpty(UserId))
                {
                    enUID = objTripleDESCryptoHelper.encryptText(UserId);
                }
                DataSet ds = new DataSet();

                ds = ObjColleaguesData.GetDoctorDetailsById(Convert.ToInt32(UserId));
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        PrimaryEmail = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Username"]), string.Empty);
                        SecondaryEmail = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["SecondaryEmail"]), string.Empty);
                        DropdownList = FillStayloggedMins(ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["StayloggedMins"]), string.Empty));
                        Staylogged = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["StayloggedMins"]), string.Empty);
                        LoginUserName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LoginUserName"]), string.Empty);
                    }
                    if (ds.Tables[8].Rows.Count > 0)
                    {
                        AccountKey = ObjCommon.CheckNull(Convert.ToString(ds.Tables[8].Rows[0]["AccountKey"]), string.Empty); ;
                        AccountId = ObjCommon.CheckNull(Convert.ToString(ds.Tables[8].Rows[0]["AccountID"]), string.Empty); ;
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }

            return Json(new { PE = PrimaryEmail, SE = SecondaryEmail, DDL = DropdownList, SLV = Staylogged, AKey = AccountKey, AId = AccountId, LOGU = LoginUserName, UID = enUID }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateDataFromAdmin(string UserId, string NewPrimaryEmail, string NewSecondaryEmail, string NewPassword, string Identifier, string NewStayloggedMins, string LoginUserName)
        {


            string OldPrimaryEmail = string.Empty;
            string OldSecondaryEmail = string.Empty;
            string OldStaylogged = string.Empty;
            string OldPassword = string.Empty;

            string EncryptedPassword = string.Empty;

            string obj = string.Empty;

            try
            {
                DataSet ds = new DataSet();
                ds = ObjColleaguesData.GetDoctorDetailsById(Convert.ToInt32(UserId));
                if (ds != null && ds.Tables.Count > 0)
                {
                    OldPrimaryEmail = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Username"]), string.Empty);
                    OldSecondaryEmail = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["SecondaryEmail"]), string.Empty);
                    OldPassword = ObjTripleDESCryptoHelper.decryptText(ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Password"]), string.Empty));
                    OldStaylogged = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["StayloggedMins"]), string.Empty);
                }




                if (!string.IsNullOrEmpty(NewPassword))
                {
                    EncryptedPassword = ObjTripleDESCryptoHelper.encryptText(NewPassword);
                }
                else
                {
                    EncryptedPassword = ObjTripleDESCryptoHelper.encryptText(OldPassword);
                }




                if (!string.IsNullOrEmpty(Identifier))
                {
                    string Identifiestocheck = Identifier;
                    if (!string.IsNullOrEmpty(LoginUserName))
                    {
                        DataTable dttbl = new DataTable();
                        dttbl = ObjColleaguesData.CheckEmailInSystem(LoginUserName);
                        if (Identifier != "5")
                        {
                            if (dttbl != null && dttbl.Rows.Count > 0)
                            {
                                obj = "User Name is already exists in system";
                                return Json(new { obj = obj }, JsonRequestBehavior.AllowGet);
                            }
                            else
                            {

                                Identifier = Identifier.Replace("5", "");
                                bool p1 = ObjColleaguesData.UpdateUserName(Convert.ToInt32(UserId), LoginUserName);
                                if (p1)
                                {
                                    obj = "User Name updated successfully.";
                                }
                                else
                                {
                                    obj = "Failed to update User Name.";


                                }

                            }
                        }
                    }
                    DataTable dt = new DataTable();
                    if (Identifier == "1")
                    {
                        #region Check and Update Primary Email
                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailForDoctorSetting(NewPrimaryEmail, Convert.ToInt32(UserId));
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "This primary email address is already in use.";
                        }
                        else
                        {
                            bool p1 = ObjColleaguesData.UpdatePrimaryEmail(Convert.ToInt32(UserId), NewPrimaryEmail);
                            if (p1)
                            {
                                obj = "Primary email updated successfully";
                            }
                            else
                            {
                                obj = "Failed to update primary email";
                            }

                        }
                        #endregion
                    }
                    if (Identifier == "2")
                    {
                        #region Check and Update Secondary Email
                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailInSystem(NewSecondaryEmail);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "Secondary email is already exists in system";
                        }
                        else
                        {
                            bool s1 = ObjColleaguesData.UpdateSecondaryEmail(Convert.ToInt32(UserId), NewSecondaryEmail);
                            if (s1)
                            {
                                obj = "Secondary email updated successfully";
                            }
                            else
                            {
                                obj = "Failed to update secondary email";
                            }
                        }
                        #endregion
                    }
                    if (Identifier == "3")
                    {
                        #region Update Password
                        bool updatepass = ObjColleaguesData.UpdatePasswordOfDoctor(Convert.ToInt32(UserId), EncryptedPassword);
                        if (updatepass)
                        {
                            obj = "Password updated successfully";
                        }
                        else
                        {
                            obj = "Failed to update password";
                        }
                        #endregion

                    }

                    if (Identifier == "4")
                    {
                        #region Update Staylogged time
                        bool result = ObjColleaguesData.UpdateStayloggedMinsOfDoctor(Convert.ToInt32(UserId), NewStayloggedMins);
                        if (result)
                        {
                            obj = "Logged in Time has been updated successfully";
                        }
                        else
                        {
                            obj = "Failed to update loggedintime";
                        }
                        #endregion
                    }
                    if (Identifier == "5")
                    {
                        #region Update Login UserName
                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailForDoctorSetting(LoginUserName, Convert.ToInt32(UserId));
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "User Name is already exists in system";
                        }
                        else
                        {
                            bool p1 = ObjColleaguesData.UpdateUserName(Convert.ToInt32(UserId), LoginUserName);
                            if (p1)
                            {
                                obj = "User Name updated successfully.";
                            }
                            else
                            {
                                obj = "Failed to update User Name.";

                            }

                        }
                        #endregion
                    }
                    if (Identifier == "12")
                    {
                        #region Check and update Primary and Secodary Email


                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailInSystem(NewPrimaryEmail);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "This primary email address is already in use.";
                        }
                        else
                        {
                            // Update Primary Email method here 
                            bool Up1 = ObjColleaguesData.UpdatePrimaryEmail(Convert.ToInt32(UserId), NewPrimaryEmail);
                            if (Up1)
                            {
                                DataTable dtsec = new DataTable();
                                dtsec = ObjColleaguesData.CheckEmailInSystem(NewSecondaryEmail);
                                if (dtsec != null && dtsec.Rows.Count > 0)
                                {
                                    obj = "Secondary email is already exists in system";
                                }
                                else
                                {
                                    bool s2 = ObjColleaguesData.UpdateSecondaryEmail(Convert.ToInt32(UserId), NewSecondaryEmail);
                                    if (s2)
                                    {
                                        obj = "Account settings updated successfully.";
                                    }
                                    else
                                    {
                                        obj = "Failed to update secondary email";
                                    }

                                }
                            }
                            else
                            {
                                obj = "Falied to update primary email";
                            }


                        }
                        #endregion
                    }
                    if (Identifier == "13")
                    {
                        #region Check and update Primary Email and Password


                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailInSystem(NewPrimaryEmail);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "This primary email address is already in use.";
                        }
                        else
                        {
                            // Update Primary Email method here 
                            bool Up1 = ObjColleaguesData.UpdatePrimaryEmail(Convert.ToInt32(UserId), NewPrimaryEmail);
                            if (Up1)
                            {
                                #region Update Password
                                bool updatepass = ObjColleaguesData.UpdatePasswordOfDoctor(Convert.ToInt32(UserId), EncryptedPassword);
                                if (updatepass)
                                {

                                    obj = "Account settings updated successfully.";
                                }
                                else
                                {
                                    obj = "Falied to update primary email and password";
                                }
                                #endregion
                            }
                            else
                            {
                                obj = "Falied to update primary email";
                            }


                        }
                        #endregion
                    }

                    if (Identifier == "14")
                    {
                        #region Check and update Primary Email and LoggedInTime


                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailInSystem(NewPrimaryEmail);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "This primary email address is already in use.";
                        }
                        else
                        {
                            // Update Primary Email method here 
                            bool Up1 = ObjColleaguesData.UpdatePrimaryEmail(Convert.ToInt32(UserId), NewPrimaryEmail);
                            if (Up1)
                            {
                                #region Update Staylogged time
                                bool result = ObjColleaguesData.UpdateStayloggedMinsOfDoctor(Convert.ToInt32(UserId), NewStayloggedMins);
                                if (result)
                                {
                                    obj = "Account settings updated successfully.";
                                }
                                else
                                {
                                    obj = "Failed to update loggedintime";
                                }
                                #endregion
                            }
                            else
                            {
                                obj = "Falied to update primary email";
                            }


                        }
                        #endregion
                    }
                    if (Identifier == "23")
                    {
                        #region Check and update Secondary Email and Password


                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailInSystem(NewSecondaryEmail);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "Secondary email is already exists in system";
                        }
                        else
                        {
                            // Update SecondaryEmail Email method here 
                            bool Up1 = ObjColleaguesData.UpdatePrimaryEmail(Convert.ToInt32(UserId), NewSecondaryEmail);
                            if (Up1)
                            {
                                #region Update Password
                                bool updatepass = ObjColleaguesData.UpdatePasswordOfDoctor(Convert.ToInt32(UserId), EncryptedPassword);
                                if (updatepass)
                                {
                                    obj = "Account settings updated successfully.";
                                }
                                else
                                {
                                    obj = "Falied to update secondary email and password";
                                }
                                #endregion
                            }
                            else
                            {
                                obj = "Falied to update secondary email";
                            }


                        }
                        #endregion
                    }

                    if (Identifier == "24")
                    {
                        #region Check and update Secondary Email and LoggedIntime


                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailInSystem(NewSecondaryEmail);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "Secondary email is already exists in system";
                        }
                        else
                        {
                            // Update SecondaryEmail Email method here 
                            bool Up1 = ObjColleaguesData.UpdatePrimaryEmail(Convert.ToInt32(UserId), NewSecondaryEmail);
                            if (Up1)
                            {
                                #region Update Staylogged time
                                bool result = ObjColleaguesData.UpdateStayloggedMinsOfDoctor(Convert.ToInt32(UserId), NewStayloggedMins);
                                if (result)
                                {
                                    obj = "Account settings updated successfully.";
                                }
                                else
                                {
                                    obj = "Failed to update loggedintime";
                                }
                                #endregion
                            }
                            else
                            {
                                obj = "Falied to update secondary email";
                            }


                        }
                        #endregion
                    }

                    if (Identifier == "34")
                    {
                        #region Update Password and loggedintime
                        bool updatepass = ObjColleaguesData.UpdatePasswordOfDoctor(Convert.ToInt32(UserId), EncryptedPassword);
                        if (updatepass)
                        {
                            #region Update Staylogged time
                            bool result = ObjColleaguesData.UpdateStayloggedMinsOfDoctor(Convert.ToInt32(UserId), NewStayloggedMins);
                            if (result)
                            {
                                obj = "Account settings updated successfully.";
                            }
                            else
                            {
                                obj = "Failed to update loggedintime";
                            }
                            #endregion
                        }
                        else
                        {
                            obj = "Failed to update password";
                        }
                        #endregion
                    }

                    if (Identifier == "123")
                    {
                        #region Check and Update Primary Email,Seconday Email and Password
                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailInSystem(NewPrimaryEmail);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "This primary email address is already in use.";
                        }
                        else
                        {
                            bool p2 = ObjColleaguesData.UpdatePrimaryEmail(Convert.ToInt32(UserId), NewPrimaryEmail);
                            if (p2)
                            {
                                #region Check and Update Secondary Email
                                DataTable dtsec = new DataTable();
                                dtsec = ObjColleaguesData.CheckEmailInSystem(NewSecondaryEmail);
                                if (dtsec != null && dtsec.Rows.Count > 0)
                                {
                                    obj = "Secondary email is already exists in system";
                                }
                                else
                                {
                                    bool s2 = ObjColleaguesData.UpdateSecondaryEmail(Convert.ToInt32(UserId), NewSecondaryEmail);
                                    if (s2)
                                    {
                                        #region Update Password
                                        bool updatepass = ObjColleaguesData.UpdatePasswordOfDoctor(Convert.ToInt32(UserId), EncryptedPassword);
                                        if (updatepass)
                                        {
                                            obj = "Account settings updated successfully.";
                                        }
                                        else
                                        {
                                            obj = "Failed to update password";
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        obj = "Failed to update secondary email";
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                obj = "Failed to update primary email";
                            }
                        }
                        #endregion
                    }

                    if (Identifier == "124")
                    {
                        #region Check and Update Primary Email,Seconday Email and LoggedInTime
                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailInSystem(NewPrimaryEmail);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "This primary email address is already in use.";
                        }
                        else
                        {
                            bool p2 = ObjColleaguesData.UpdatePrimaryEmail(Convert.ToInt32(UserId), NewPrimaryEmail);
                            if (p2)
                            {
                                #region Check and Update Secondary Email
                                DataTable dtsec = new DataTable();
                                dtsec = ObjColleaguesData.CheckEmailInSystem(NewSecondaryEmail);
                                if (dtsec != null && dtsec.Rows.Count > 0)
                                {
                                    obj = "Secondary email is already exists in system";
                                }
                                else
                                {
                                    bool s2 = ObjColleaguesData.UpdateSecondaryEmail(Convert.ToInt32(UserId), NewSecondaryEmail);
                                    if (s2)
                                    {
                                        #region Update Staylogged time
                                        bool result = ObjColleaguesData.UpdateStayloggedMinsOfDoctor(Convert.ToInt32(UserId), NewStayloggedMins);
                                        if (result)
                                        {
                                            obj = "Account settings updated successfully.";
                                        }
                                        else
                                        {
                                            obj = "Failed to update loggedintime";
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        obj = "Failed to update secondary email";
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                obj = "Failed to update primary email";
                            }
                        }
                        #endregion
                    }

                    if (Identifier == "234")
                    {
                        #region Check and Update Secondary Email,Password and LoggedinTime
                        DataTable dtsec = new DataTable();
                        dtsec = ObjColleaguesData.CheckEmailInSystem(NewSecondaryEmail);
                        if (dtsec != null && dtsec.Rows.Count > 0)
                        {
                            obj = "Secondary email is already exists in system";
                        }
                        else
                        {
                            bool s2 = ObjColleaguesData.UpdateSecondaryEmail(Convert.ToInt32(UserId), NewSecondaryEmail);
                            if (s2)
                            {
                                #region Update Password
                                bool updatepass = ObjColleaguesData.UpdatePasswordOfDoctor(Convert.ToInt32(UserId), EncryptedPassword);
                                if (updatepass)
                                {
                                    #region Update Staylogged time
                                    bool result = ObjColleaguesData.UpdateStayloggedMinsOfDoctor(Convert.ToInt32(UserId), NewStayloggedMins);
                                    if (result)
                                    {
                                        obj = "Account settings updated successfully.";
                                    }
                                    else
                                    {
                                        obj = "Failed to update loggedintime";
                                    }
                                    #endregion
                                }
                                else
                                {
                                    obj = "Failed to update password";
                                }
                                #endregion

                            }
                            else
                            {
                                obj = "Failed to update secondary email";
                            }
                        }
                        #endregion
                    }

                    if (Identifier == "1234")
                    {
                        #region Check and Update Primary Email,secondary email, password and loggedtime
                        dt = new DataTable();
                        dt = ObjColleaguesData.CheckEmailInSystem(NewPrimaryEmail);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            obj = "This primary email address is already in use.";
                        }
                        else
                        {
                            bool p1 = ObjColleaguesData.UpdatePrimaryEmail(Convert.ToInt32(UserId), NewPrimaryEmail);
                            if (p1)
                            {
                                #region Check and Update Secondary Email
                                DataTable dtsec = new DataTable();
                                dtsec = ObjColleaguesData.CheckEmailInSystem(NewSecondaryEmail);
                                if (dtsec != null && dtsec.Rows.Count > 0)
                                {
                                    obj = "Secondary email is already exists in system";
                                }
                                else
                                {
                                    bool s1 = ObjColleaguesData.UpdateSecondaryEmail(Convert.ToInt32(UserId), NewSecondaryEmail);
                                    if (s1)
                                    {
                                        #region Update Password
                                        bool updatepass = ObjColleaguesData.UpdatePasswordOfDoctor(Convert.ToInt32(UserId), EncryptedPassword);
                                        if (updatepass)
                                        {
                                            #region Update Staylogged time
                                            bool result = ObjColleaguesData.UpdateStayloggedMinsOfDoctor(Convert.ToInt32(UserId), NewStayloggedMins);
                                            if (result)
                                            {
                                                obj = "Account settings updated successfully.";
                                            }
                                            else
                                            {
                                                obj = "Failed to update loggedintime";
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            obj = "Failed to update password";
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        obj = "Failed to update secondary email";
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                obj = "Failed to update primary email";
                            }

                        }
                        #endregion


















                    }



                }
                else
                {

                    if (!string.IsNullOrEmpty(NewPrimaryEmail))
                    {
                        if (NewSecondaryEmail != OldSecondaryEmail)
                        {
                            bool s1 = ObjColleaguesData.UpdateSecondaryEmail(Convert.ToInt32(UserId), NewSecondaryEmail);
                            if (s1)
                            {
                                obj = "Secondary email is updated successfully";
                            }
                            else
                            {
                                obj = "Failed to update secondary email";
                            }
                        }
                        else
                        {
                            obj = "";
                        }

                        if (OldStaylogged != NewStayloggedMins)
                        {
                            bool s1 = ObjColleaguesData.UpdateStayloggedMinsOfDoctor(Convert.ToInt32(UserId), NewStayloggedMins);
                            if (s1)
                            {
                                obj = "";
                            }
                            else
                            {
                                obj = "Failed to update secondary email";
                            }
                        }
                        else
                        {
                            obj = "";
                        }


                    }
                    else
                    {
                        obj = "Please enter primary email.";
                    }

                }








            }
            catch (Exception)
            {

                throw;
            }
            return Json(new { obj = obj }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Sync Data

        public JsonResult GetDataSyncInfusionSoft(int UserId = 0)
        {
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || UserId == 0)
                UserId = Convert.ToInt32(SessionManagement.UserId);
            mdlSyncInfusionSoft MdlSyncInfusionSoft = new mdlSyncInfusionSoft();

            try
            {

                DataSet ds = new DataSet();
                ds = ObjColleaguesData.GetDataSyncInfusionSoft(UserId);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        MdlSyncInfusionSoft.APIKey = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["APIKey"]), string.Empty);
                        MdlSyncInfusionSoft.SyncPerDay = (int)ds.Tables[0].Rows[0]["SyncPerDay"];
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return Json(MdlSyncInfusionSoft, JsonRequestBehavior.AllowGet);
        }

        public ActionResult NewGetDataSyncInfusionSoft(int UserId = 0)
        {
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || UserId == 0)
                UserId = Convert.ToInt32(SessionManagement.UserId);
            mdlSyncInfusionSoft MdlSyncInfusionSoft = new mdlSyncInfusionSoft();

            try
            {

                DataSet ds = new DataSet();
                ds = ObjColleaguesData.GetDataSyncInfusionSoft(UserId);
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        MdlSyncInfusionSoft.APIKey = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["APIKey"]), string.Empty);
                        MdlSyncInfusionSoft.SyncPerDay = (int)ds.Tables[0].Rows[0]["SyncPerDay"];
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return View("_PartialInfusionSoftPopup", MdlSyncInfusionSoft);
        }

        public JsonResult SaveDataSyncInfusionSoft(mdlSyncInfusionSoft SyncInfusionSoft, int UserId = 0)
        {
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || UserId == 0)
                UserId = Convert.ToInt32(SessionManagement.UserId);
            bool result = ObjColleaguesData.SaveDataSyncInfusionSoft(UserId,
                                                                        SyncInfusionSoft.SyncPerDay,
                                                                        true);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateDataSyncInfusionSoft(int UserId = 0)
        {
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || UserId == 0)
                UserId = Convert.ToInt32(SessionManagement.UserId);

            bool result = ObjColleaguesData.UpdateDataSyncInfusionSoft(UserId,
                                                                        false);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SyncDentrix(string Id, int UserId = 0)
        {
            //int UserId = Convert.ToInt32(Session["UserId"]);
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || UserId == 0)
                UserId = Convert.ToInt32(SessionManagement.UserId);
            bool IsProcessed = false;
            DateTime CreatedDate = DateTime.Now;
            string ProcessDate = string.Empty;
            string EventType = "Sync Dentrix Appointment To Recordlinc";
            string NewEventType = "Sync Recordlinc Appointment To Dentrix";
            string EventSource = "Recordlinc";
            string RequestedUserType = "Dentist";
            bool Result = ObjColleaguesData.InsertDentrixSyncData(UserId, IsProcessed, EventType, EventSource, CreatedDate, ProcessDate, RequestedUserType, Id);
            Result = ObjColleaguesData.InsertDentrixSyncData(UserId, IsProcessed, NewEventType, EventSource, CreatedDate, ProcessDate, RequestedUserType, Id);

            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public int IsDentrixSyncInProcessOrNot()
        {
            int Result = 0;
            try
            {
                int UserId = Convert.ToInt32(Session["UserId"]);
                Result = ObjColleaguesData.CheckUserSyncProcessIsRunning(UserId);
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("IsDentrixSyncInProcessOrNot", Ex.Message, Ex.StackTrace);
            }
            return Result;
        }
        #region API

        private string DeveloperAppKey = ConfigurationManager.AppSettings["infusionsoftDeveloperAppKey"];
        private string DeveloperAppSecret = ConfigurationManager.AppSettings["infusionsoftDeveloperAppSecret"];
        private string CallbackUrl = "";

        public ActionResult Authorize()
        {
            var CallbackUrl = Request.Url.Scheme + "://" + Request.Url.Authority + "/AccountSettings/callback";
            string authorizeUrlFormat = "https://signin.infusionsoft.com/app/oauth/authorize?scope=full&redirect_uri={0}&response_type=code&client_id={1}";
            return Redirect(string.Format(authorizeUrlFormat, Server.UrlEncode(CallbackUrl), DeveloperAppKey));
        }

        public ActionResult Callback(string code)
        {

            var CallbackUrl = Request.Url.Scheme + "://" + Request.Url.Authority + "/AccountSettings/callback";


            if (!string.IsNullOrEmpty(code))
            {
                string tokenUrl = "https://api.infusionsoft.com/token";

                string tokenDataFormat = "code={0}&client_id={1}&client_secret={2}&redirect_uri={3}&grant_type=authorization_code";

                HttpWebRequest request = HttpWebRequest.Create(tokenUrl) as HttpWebRequest;
                request.Method = "POST";
                request.KeepAlive = true;
                request.ContentType = "application/x-www-form-urlencoded";

                string dataString = string.Format(tokenDataFormat, code, DeveloperAppKey, DeveloperAppSecret, Server.UrlEncode(CallbackUrl));
                var dataBytes = Encoding.UTF8.GetBytes(dataString);
                using (Stream reqStream = request.GetRequestStream())
                {
                    reqStream.Write(dataBytes, 0, dataBytes.Length);
                }

                string resultJSON = string.Empty;
                using (WebResponse response = request.GetResponse())
                {
                    var sr = new StreamReader(response.GetResponseStream());
                    resultJSON = sr.ReadToEnd();
                    sr.Close();
                }

                var jsonSerializer = new JavaScriptSerializer();

                var tokenData = jsonSerializer.Deserialize<TokenData>(resultJSON);

                bool result = ObjColleaguesData.UpdateDataSyncTokenInfusionSoft(Convert.ToInt32(Session["UserId"]), tokenData.Access_Token, tokenData.Refresh_Token, code);

                ViewBag.msg = "Infusionsoft API integration successful.";

            }
            else
            {
                ViewBag.msg = "Infusionsoft API integration failed, Please contact to administration.";
            }
            return View();
        }

        public string GetTimezone(int LocationId)
        {
            string returnValue = string.Empty;
            returnValue = ObjColleaguesData.GetTimeZoneBasedOnLocation(LocationId);
            return returnValue;

        }

        #endregion

        #endregion

        #region Integrations
        [CustomAuthorize]
        public ActionResult Integrations()
        {
            return View("Integrations");
        }

        [CustomAuthorize]
        public ActionResult Integration()
        {
            string PrimaryEmail = string.Empty;
            string SecondaryEmail = string.Empty;
            string DropdownList = string.Empty;
            string Staylogged = string.Empty;
            string LoginUserName = string.Empty;
            string GuidKay = string.Empty;
            string AccountKey = string.Empty;
            string AccountId = string.Empty;
            int LocationId = 0;
            int UserId = 0;
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || UserId == 0)
                UserId = Convert.ToInt32(SessionManagement.UserId);
            string DentrixProviderId = string.Empty;
            bool SyncInfusionSoft = false;
            //List<BO.Models.AccountIntegration> ListAI = new List<BO.Models.AccountIntegration>();
            BO.Models.AccountSetting AccSetting = new BO.Models.AccountSetting();
            try
            {

                DataSet ds = new DataSet();
                ds = ObjColleaguesData.GetDoctorDetailsById(UserId);

                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        PrimaryEmail = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Username"]), string.Empty);
                        SecondaryEmail = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["SecondaryEmail"]), string.Empty);
                        DropdownList = FillStayloggedMins(ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["StayloggedMins"]), string.Empty));
                        Staylogged = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["StayloggedMins"]), string.Empty);
                        LoginUserName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LoginUserName"]), string.Empty);
                        LocationId = Convert.ToInt32(ds.Tables[0].Rows[0]["LocationId"]);
                        DentrixProviderId = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["DentrixProviderId"]), string.Empty);
                        string member = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["MembershipTitle"]), string.Empty);
                        ViewBag.IsMember = (member.Contains("ELITE") || member.Contains("PREMIUM"));
                        ViewBag.DentrixProviderId = DentrixProviderId;

                    }
                    if (ds.Tables[8].Rows.Count > 0)
                    {
                        AccountKey = ObjCommon.CheckNull(Convert.ToString(ds.Tables[8].Rows[0]["AccountKey"]), string.Empty);
                        AccountId = ObjCommon.CheckNull(Convert.ToString(ds.Tables[8].Rows[0]["AccountID"]), string.Empty);
                        //AccSetting.IsRewardPartner = Convert.ToInt32(ds.Tables[8].Rows[0]["IsRewardPartner"]) == 1 ? true : false;
                        ViewBag.accountId = AccountId;
                    }
                    if (ds.Tables[9].Rows.Count > 0)
                    {
                        SyncInfusionSoft = (bool)ds.Tables[9].Rows[0]["IsActive"];
                        AccSetting.SyncInfusionSoft = SyncInfusionSoft;
                    }
                    if (ds.Tables[16].Rows.Count > 0)
                    {
                        AccSetting.RewardPartnerId = Convert.ToInt32(ds.Tables[16].Rows[0]["RewardCompanyID"]);
                        AccSetting.IsRewardPartner = true;
                    }
                    else
                    {
                        AccSetting.IsRewardPartner = false;
                    }
                    if (ds.Tables[17].Rows.Count > 0)
                    {
                        AccSetting.IsSuperDentist = true;
                        //AccSetting.Points = Convert.ToDecimal(ds.Tables[17].Rows[0]["Points"]);
                        //AccSetting.Examcodes = Convert.ToDecimal(ds.Tables[17].Rows[0]["ExamCodes"]);
                    }
                    if(ds.Tables[20].Rows.Count > 0)
                    {
                        AccSetting.IsReceiveReferral = Convert.ToBoolean(ds.Tables[20].Rows[0]["IsReceiveReferral"]);
                        AccSetting.IsSendReferral = Convert.ToBoolean(ds.Tables[20].Rows[0]["IsSendReferral"]);
                    }
                }
                DataTable dt = new DataTable();
                dt = ObjColleaguesData.GetDentrixConnectorValuesofUser(UserId);

                if (dt.Rows.Count > 0 && dt != null)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        AccSetting.lst.Add(new BO.Models.AccountIntegration
                        {
                            GUIDKey = dt.Rows[i]["AccountKey"].ToString(),
                            LastSyncDate = Convert.ToString(dt.Rows[i]["ProcessDate"]),
                            LocationId = Convert.ToString(dt.Rows[i]["Location"]),
                            DentrixConnectorId = Convert.ToInt32(dt.Rows[i]["DentrixConnectorId"])
                        });
                    }
                }
                int cntZiftPay = ZPPaymentBAL.GetAccountZiftPayDetailsByUserId(UserId);
                if (cntZiftPay > 0)
                {
                    AccSetting.IsZiftPayAccount = true;
                    AccSetting.ZiftPayAccountId = cntZiftPay;
                }
                else
                {
                    AccSetting.IsZiftPayAccount = false;
                }


                if (TempData["Valid"] != null)
                {
                    if (Convert.ToBoolean(TempData["Valid"]) == true)
                    {
                        ViewBag.msg = TempData["msg"];
                    }
                    else
                    {
                        ViewBag.ErrorMsg = TempData["msg"];
                    }

                }

            }
            catch (Exception)
            {
                throw;
            }
            ViewBag.LocationList = NewGetLocationList("0");
            return View("NewIntegrations", AccSetting);
        }



        public JsonResult AddDentrixConnectorRow()
        {
            int UserId = Convert.ToInt32(Session["UserId"]);
            string AccountKey = Crypto.SHA1(Convert.ToString(DateTime.Now.Ticks));
            //int ConnectorId = ObjColleaguesData.InsertDentrixConnector(Convert.ToInt32(Session["UserId"]), AccountKey);
            return Json(AccountKey + "--", JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult LocationListForDentist(int UserId = 0)
        {
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || UserId == 0)
                UserId = Convert.ToInt32(SessionManagement.UserId);
            List<SelectListItem> DentrixLocations = new List<SelectListItem>();
            DataTable dt = new DataTable();
            dt = ObjColleaguesData.GetDentrixConnectorValuesofUser(UserId);
            foreach (DataRow item in dt.Rows)
            {
                DentrixLocations.Add(new SelectListItem
                {
                    Text = item["Location"].ToString(),
                    Value = item["LocationId"].ToString(),
                });
            }
            List<SelectListItem> AllLocation = NewGetLocationList("0");
            AllLocation = AllLocation.Where(p => !DentrixLocations.Any(p2 => p2.Text == p.Text)).ToList();
            return Json(AllLocation);
        }
        public JsonResult DeleteDentrixConnector(string AccountId)
        {
            try
            {
                bool result = ObjColleaguesData.DeleteDentrixConnectorId(Convert.ToInt32(AccountId));
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public JsonResult AddDentrix(string[] GUIDKEY, string dentrixProviderId, int UserId = 0)
        {
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || UserId == 0)
                UserId = Convert.ToInt32(Session["UserId"]);

            string obj = string.Empty;
            try
            {
                //DataSet ds = new DataSet();
                //ds = ObjColleaguesData.GetDoctorDetailsById(UserId);
                //if (!string.IsNullOrWhiteSpace(GUIDKEY.ToString()) || GUIDKEY.ToString() != null)
                if (GUIDKEY != null)
                {
                    foreach (var item in GUIDKEY)
                    {
                        string[] Splitedata = item.Split('-');
                        int ConnectorId = ObjColleaguesData.InsertDentrixConnector(UserId, Splitedata[0], Splitedata[1]);
                    }
                }
                if (!string.IsNullOrWhiteSpace(dentrixProviderId))
                {
                    ObjColleaguesData.UpdateDentrixProvideId(UserId, dentrixProviderId);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return Json(new
            {
                obj = obj
            }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Patient NotificationSetting
        [CustomAuthorizeAttribute]
        public ActionResult PatientNotificationSetting(int? page)
        {
            mdlPatient MdlPatient = new mdlPatient();
            IPagedList<PatientDetailsOfDoctor> Patient = null;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings.Get("PageSize"));
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            int Id = Convert.ToInt32(Session["UserId"]);
            MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetAllPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), null, 2, 1, null, 0);
            Patient = MdlPatient.lstPatientDetailsOfDoctor.ToPagedList(pageIndex, pageSize);
            return View("PatientNotificationSetting", Patient);
        }

        [HttpPost]
        public ActionResult InsertUpdateNotificationDetail(List<PatientDetailsOfDoctor> lstPatientDetailsOfDoctor, int? page)
        {
            try
            {
                foreach (var item in lstPatientDetailsOfDoctor)
                {
                    // if (item.Voice == true || item.Text == true)
                    ObjPatientsData.NotificationSettingInsertAndUpdate(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(item.PatientId), item.AccountId, item.Voice, item.Text, item.ChkEmail);
                }
                TempData["ErrorMessage"] = "Notifcation setting updated successfully";
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("PatientNotificationSetting", new { page = page });
        }

        [HttpPost]
        public JsonResult InsertUpdateNotificationDetailForAll(string Voice, string Text, string Email)
        {
            object obj = string.Empty;
            try
            {
                ObjPatientsData.NotificationSettingInsertAndUpdateForAll(Convert.ToInt32(Session["UserId"]), Convert.ToBoolean(Voice), Convert.ToBoolean(Text), Convert.ToBoolean(Email));
            }
            catch (Exception)
            {
                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult GetDentrixLocations()
        {
            List<SelectListItem> DentrixLocations = new List<SelectListItem>();
            DataTable dt = new DataTable();
            dt = ObjColleaguesData.GetDentrixConnectorValuesofUser(Convert.ToInt32(SessionManagement.UserId));
            foreach (DataRow item in dt.Rows)
            {
                DentrixLocations.Add(new SelectListItem
                {
                    Text = item["Location"].ToString(),
                    Value = item["LocationId"].ToString(),
                });
            }
            return Json(DentrixLocations, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorizeAttribute]
        public ActionResult GetUserDetails()
        {
            PatientRewardMDL Obj = new PatientRewardMDL();
            responsemdl Response = new responsemdl();
            responseerror errobj = new responseerror();
            BO.ViewModel.APIResponse ApiRes = new BO.ViewModel.APIResponse();
            //bool IsRewardPartner= 
            IDictionary<responsemdl, responseerror> Restype = new Dictionary<responsemdl, responseerror>();
            bool result = PatientRewardBLL.AddUserAsRewardPartner(Convert.ToInt32(SessionManagement.UserId));
            Obj = PatientRewardBLL.GetMemberFullDetails(Convert.ToInt32(SessionManagement.UserId));
            //This Call Register Company on Reward Partner.
            Restype = PatientRewardBLL.InsertCompnyonRewardPartner(Obj, Convert.ToInt32(SessionManagement.UserId));
            if (Restype.Keys.FirstOrDefault().company != null)
            {
                ApiRes.company_id = Convert.ToInt32(Restype.Keys.FirstOrDefault().company.company_id);
            }
            else
            {

                ApiRes.message = Restype.Values.FirstOrDefault().errors[0].message.ToString();
                if (ApiRes.message == "Invalid Token")
                {
                    ApiRes.field = "Invalid Token";
                }
                else
                {
                    ApiRes.field = Restype.Values.FirstOrDefault().errors[0].field.ToString();
                }
                ApiRes.company_id = null;
            }
            return Json(ApiRes, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorizeAttribute]
        public ActionResult GetLoctionDetails(int Companyid, int LocationId)
        {
            responseerror errobj = new responseerror();
            BO.ViewModel.APIResponse ApiRes = new BO.ViewModel.APIResponse();
            PatientRewardMDL Obj = new PatientRewardMDL();
            responseLoc ResonseLocation = new responseLoc();
            IDictionary<responseLoc, responseerror> Restype = new Dictionary<responseLoc, responseerror>();
            BO.ViewModel.Location LocObj = new BO.ViewModel.Location();
            Obj = PatientRewardBLL.GetMemberFullDetails(Convert.ToInt32(SessionManagement.UserId));
            LocObj = PatientRewardBLL.GetMemberLocationDetails(Convert.ToInt32(SessionManagement.UserId), LocationId);
            if (LocObj == null)
            {
                return Json("no_loc", JsonRequestBehavior.AllowGet);
            }
            else
            {

                if (string.IsNullOrEmpty(LocObj.contact_first_name) || string.IsNullOrEmpty(LocObj.contact_last_name))
                {
                    LocObj.contact_first_name = Obj.contact_first_name;
                    LocObj.contact_last_name = Obj.contact_last_name;
                }
                Restype = PatientRewardBLL.InsertMemberLocationDetails(LocObj, Convert.ToInt32(SessionManagement.UserId));
                if (Restype.Keys.FirstOrDefault().location != null)
                {
                    ApiRes.loctionId = Convert.ToInt32(Restype.Keys.FirstOrDefault().location.location_id);
                }
                else
                {
                    ApiRes.message = Restype.Values.FirstOrDefault().errors[0].message.ToString();
                    if (ApiRes.message == "Invalid Token")
                    {
                        ApiRes.field = "Invalid Token";
                    }
                    else
                    {
                        ApiRes.field = Restype.Values.FirstOrDefault().errors[0].field.ToString();
                    }
                    ApiRes.loctionId = null;
                }

            }
            return Json(ApiRes, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorizeAttribute]
        public ActionResult LocationListOfuser(int UserId)
        {
            List<SelectListItem> lst = new List<SelectListItem>();
            DataTable dt = new DataTable();
            if (string.IsNullOrEmpty(SessionManagement.AdminUserId) || UserId == 0)
                UserId = Convert.ToInt32(SessionManagement.UserId);
            dt = ObjColleaguesData.GetDentrixConnectorValuesofUser(UserId);
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    lst.Add(new SelectListItem()
                    {
                        Value = Convert.ToString(item["LocationId"]),
                        Text = Convert.ToString(item["Location"])
                    });
                }
            }
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorizeAttribute]
        public ActionResult AddPointsandExamcodes(decimal Points, int ExamCode)
        {
            int UserId = Convert.ToInt32(SessionManagement.UserId);
            bool result = CommonBLL.AddPointsAndExamCodes(UserId, Points, ExamCode);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorizeAttribute]
        public ActionResult RegisterSuperBuck()
        {
            int UserId = Convert.ToInt32(SessionManagement.UserId);
            bool result = CommonBLL.RegisterSuperBuck(UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get All details of Speciality,Services,RefferalForm Settings,Loaction.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorizeAttribute]
        public ActionResult ReferralFormSettings()
        {
            MainReferralForm rfobj = new MainReferralForm();
            List<ReferralFormSetting> lst = new List<ReferralFormSetting>();
            List<ReferralFormServices> lstservice = new List<ReferralFormServices>();

            rfobj = ReferralFormBLL.GetDentistSpeciality(Convert.ToInt32(SessionManagement.UserId));
            List<PatientSectionMapping> lstpsmobj = new List<PatientSectionMapping>();
            lstpsmobj = ReferralFormBLL.GetDentistPatientmappingDetail(Convert.ToInt32(SessionManagement.UserId));
            var obj = new Tuple<MainReferralForm, List<PatientSectionMapping>>(rfobj, lstpsmobj);
            return View(obj);

        }

        /// <summary>
        /// save RefferalForm Settings,Speciality,Services.
        /// </summary>
        /// <param name="rfsobj,spcltyObj,servobj"></param>
        /// <returns></returns>
        /// 
        [CustomAuthorizeAttribute]
        public ActionResult SaveReferralFormSettings(List<ReferralFormSetting> rfsobj, List<ReferralFormSetting> spcltyObj, List<ReferralFormServices> servobj, List<ReferralFormSubField> subfieldObj)
        {
            int UserId = Convert.ToInt32(SessionManagement.UserId);
            bool result = ReferralFormBLL.AddRefferalFormSetting(UserId, rfsobj, spcltyObj, servobj, subfieldObj);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// save Dentist Referral Details from RecordLinc.
        /// </summary>
        /// <param name="rfsobj,spcltyObj,servobj"></param>
        /// <returns></returns>
        /// 
        [CustomAuthorizeAttribute]
        public ActionResult InsertDentistPatientreferralFormSetting(List<PatientSectionMapping> secobj, List<PatientFieldMapping> fildObj, List<PatientSubFieldMapping> subfieldObj)
        {
            int UserId = Convert.ToInt32(SessionManagement.UserId);
            bool result = ReferralFormBLL.AddDentistPatientreferralFormSetting(UserId, secobj, fildObj, subfieldObj);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get Dentist Patient Referral Details.
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        [CustomAuthorizeAttribute]
        public ActionResult DentistPatientReferralFormSetting()
        {
            List<PatientSectionMapping> lstpsmobj = new List<PatientSectionMapping>();
            lstpsmobj = ReferralFormBLL.GetDentistPatientmappingDetail(Convert.ToInt32(SessionManagement.UserId));
            return View(lstpsmobj);
        }

        /// <summary>
        /// Old Method use for Get patient referral setting when click on link from mail. Use in One Click Referral.
        /// </summary>
        /// <returns></returns>

        //public ActionResult GetPatientReferralForm()
        //{

        //    Compose1Click ComposeReferral = new Compose1Click();
        //    string DecryptString = string.Empty;
        //    if (ViewBag.alert != null)
        //    {
        //        DecryptString = (string)TempData["String"];
        //    }

        //    else
        //    {
        //        DecryptString = Convert.ToString(Request.QueryString["TYHJNABGA"]);

        //    }

        //    TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();

        //    int PatientId = 0; int DoctorId = 0;
        //    bool flag = false;
        //    if (!string.IsNullOrEmpty(DecryptString))
        //    {
        //        //DecryptString = DecryptString;
        //        string[] DecryptedDoctorId = DecryptString.Split('|');
        //        if(DecryptedDoctorId.Length == 3)
        //        {
        //            flag = true;
        //        }
        //        //PatientId = Convert.ToInt32(ObjTripleDESCryptoHelper.decryptText(DecryptedDoctorId[0]));
        //        PatientId = Convert.ToInt32(DecryptedDoctorId[0]) - 999;
        //        TempData["PatientId"] = PatientId;
        //        //DoctorId = Convert.ToInt32(ObjTripleDESCryptoHelper.decryptText(DecryptedDoctorId[1]));
        //        DoctorId = Convert.ToInt32(DecryptedDoctorId[1]) - 999;
        //        TempData["DoctorId"] = DoctorId;
        //        TempData.Keep();
        //    }
        //    else
        //    {
        //        PatientId = Convert.ToInt32(TempData["PatientId"]);
        //        DoctorId = Convert.ToInt32(TempData["DoctorId"]);
        //    }
        //    //string DecryptedPatientId = ObjTripleDESCryptoHelper.decryptText(Request.QueryString["PatientId"]);

        //    ComposeReferral = ReferralFormBLL.GetPatientInfo(PatientId);
        //    ComposeReferral._innerService = ReferralFormBLL.GetSpecialityForPatientReferralFormSetting(DoctorId);
        //    ComposeReferral._insuranceCoverage = ReferralFormBLL.GetInsuranceDetails(PatientId);
        //    ComposeReferral._specialtyService = ReferralFormBLL.GetPatientReferalVisibleSection(Convert.ToInt32(DoctorId));
        //    ComposeReferral._medicalHistory = ReferralFormBLL.GetMedicalHistoryDetails(PatientId);
        //    ComposeReferral._dentalhistory = ReferralFormBLL.GetDentalHistoryDetails(PatientId);
        //    ComposeReferral.ReceiverId = Convert.ToString(DoctorId);
        //    ComposeReferral.SenderId = Convert.ToString(PatientId);
        //    ComposeReferral.IsReferral = flag;
        //    ViewBag.alert = TempData["success"];
        //    TempData["success"] = null;
        //    return View(ComposeReferral);
        //}


        /// <summary>
        /// New Method use for Get patient referral setting when click on link from mail. Use in One Click Referral.
        /// </summary>
        /// <returns></returns>

        public ActionResult GetPatientReferralForm()
        {

            Compose1Click ComposeReferral = new Compose1Click();
            string DecryptString = string.Empty;
            if (ViewBag.alert != null)
            {
                DecryptString = (string)TempData["String"];
            }

            else
            {
                DecryptString = Convert.ToString(Request.QueryString["TYHJNABGA"]);

            }

            TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();

            int PatientId = 0; int DoctorId = 0;
            bool flag = false;
            if (!string.IsNullOrEmpty(DecryptString))
            {
                //DecryptString = DecryptString;
                string[] DecryptedDoctorId = DecryptString.Split('|');
                if (DecryptedDoctorId.Length == 3)
                {
                    flag = true;
                }
                //PatientId = Convert.ToInt32(ObjTripleDESCryptoHelper.decryptText(DecryptedDoctorId[0]));
                PatientId = Convert.ToInt32(DecryptedDoctorId[0]) - 999;
                TempData["PatientId"] = PatientId;
                //DoctorId = Convert.ToInt32(ObjTripleDESCryptoHelper.decryptText(DecryptedDoctorId[1]));
                DoctorId = Convert.ToInt32(DecryptedDoctorId[1]) - 999;
                TempData["DoctorId"] = DoctorId;
                TempData.Keep();
            }
            else
            {
                PatientId = Convert.ToInt32(TempData["PatientId"]);
                DoctorId = Convert.ToInt32(TempData["DoctorId"]);
            }
            //string DecryptedPatientId = ObjTripleDESCryptoHelper.decryptText(Request.QueryString["PatientId"]);
            ComposeReferral = ReferralFormBLL.GetPatientInfo(PatientId);
            ComposeReferral._innerService = ReferralFormBLL.GetSpecialityForPatientForm(DoctorId);
            ComposeReferral._insuranceCoverage = ReferralFormBLL.GetInsuranceDetails(PatientId);
            ComposeReferral._specialtyService = ReferralFormBLL.GetPatientReferalVisibleSection(Convert.ToInt32(DoctorId));
            ComposeReferral._medicalHistory = ReferralFormBLL.GetMedicalHistoryDetails(PatientId);
            ComposeReferral._dentalhistory = ReferralFormBLL.GetDentalHistoryDetails(PatientId);
            ComposeReferral._contactInfo.EMGContactName = ComposeReferral._insuranceCoverage.EMGContactName;
            ComposeReferral._contactInfo.EMGContactNo = ComposeReferral._insuranceCoverage.EMGContactNo;
            ComposeReferral.StateList = PatientBLL.GetStateList("US");
            ComposeReferral.ReceiverId = Convert.ToString(DoctorId);
            ComposeReferral.SenderId = Convert.ToString(PatientId);
            ComposeReferral.IsReferral = flag;
            ViewBag.alert = TempData["success"];
            TempData["success"] = null;
            return View("GetPatientReferralForm", ComposeReferral);
        }

        /// <summary>
        /// Method use for update Patient detail when patient click on link from mail. Use in One Click Referral.
        /// </summary>
        /// <param name="mdlpatientsignup"></param>
        /// <returns></returns>
        [HttpPost]
        //[CustomAuthorizeAttribute]
        public ActionResult SubmitPatientDetail(Compose1Click mdlpatientsignup)
        {
            clsCommon objCommon = new clsCommon();
            //Change for XQ1 - 1059
            bool result = false;
            int intResult = ReferralFormBLL.ComposeReferral1Click(mdlpatientsignup, true);
            if (intResult > 0)
            {
                result = true;
            }
            TempData["success"] = result;
            return RedirectToAction("GetPatientReferralForm");
        }

        /// <summary>
        /// This method getting referral form setting details for that user.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [CustomAuthorize]
        public SpecialityService GetVisibleSpeciality(int UserId)
        {
            SpecialityService sp = new SpecialityService();
            sp = ReferralFormBLL.GetVisibleSpeciality(UserId);
            return sp;
        }

        [CustomAuthorize]
        public ActionResult DownloadFile(int IsFromPopup = 0)
        {
            var _serverPath = string.Empty;
            if (IsFromPopup != 0)
            {
                _serverPath = Server.MapPath(Convert.ToString(ConfigurationManager.AppSettings["DentrixLodder"])) + "\\" + "DentrixLodder.zip";
                File(_serverPath, System.Net.Mime.MediaTypeNames.Application.Zip, "DentrixLodder.zip");
                return RedirectToAction("Index", "Reward");
            }
            else
            {
                _serverPath = Server.MapPath(Convert.ToString(ConfigurationManager.AppSettings["DentrixLodder"])) + "\\" + "DentrixLodder.zip";
                return File(_serverPath, System.Net.Mime.MediaTypeNames.Application.Zip, "DentrixLodder.zip");
                //Json(0,JsonRequestBehavior.AllowGet);
            }
        }
        [CustomAuthorize]
        public ActionResult SaveReferralStatus(OCR_ReferralStatus _ReferralStatus)
        {
            _ReferralStatus.UserId = Convert.ToInt32(SessionManagement.UserId);
            bool result = DentistBLL.SaveReferralStatusOfUser(_ReferralStatus);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Save Manage Order by sequence for doctor
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult SaveManageSequence(FieldSequence data)
        {
            ReferralFormBLL.SaveManageSequence(data);
            return Json(true);
        }

        /// <summary>
        /// Save Manage Order by sequence for patient
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public ActionResult SavePatientManageSequence(FieldSequence data)
        {
            ReferralFormBLL.SavePatientManageSequence(data);
            return Json(true);
        }

    }
}
