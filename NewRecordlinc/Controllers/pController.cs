﻿using BO.Models;
using BO.ViewModel;
using BusinessLogicLayer;
using DataAccessLayer.Appointment;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using DataAccessLayer.PatientsData;
using NewRecordlinc.App_Start;
using NewRecordlinc.Models.Appointment;
using NewRecordlinc.Models.Colleagues;
using NewRecordlinc.Models.Patients;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security;
using System.Web;
using System.Web.Mvc;

namespace NewRecordlinc.Controllers
{
    public class pController : Controller
    {
        // GET: r
        clsCompany ObjCompany = new clsCompany();
        DentistProfileDetails objProfileDetails = new DentistProfileDetails();
        clsPatientsData ObjPatientsData = new clsPatientsData();
        DataTable dtUserID = new DataTable();
        clsTemplate ObjTemplate = new clsTemplate();
        clsCommon objCommon = new clsCommon();
        mdlPatient MdlPatient = new mdlPatient();
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        DentistProfileMethod objprofileMethod = new DentistProfileMethod();
        DentistProfileBLL objdentistprofile = new DentistProfileBLL();

        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        public ActionResult AboutDoctor(bool isWidget, string username = null, bool issearch = false)
        {

            DentistProfileDetail objProfileDetails = new DentistProfileDetail();
            if (!string.IsNullOrEmpty(username))
            {
                int UserId = 0;
                UserId = objdentistprofile.GetIdOnPublicProfileURL(username);

                if (UserId > 0)
                {
                    string TimezoneSystemName = new clsAppointmentData().GetTimeZoneOfDoctor(UserId);
                    //Changes for 0 Time Zone issue in QA
                    if (string.IsNullOrWhiteSpace(TimezoneSystemName))
                    {
                        clsCommon ObjCommon = new clsCommon();
                        ObjCommon.InsertErrorLog(username, "Error while fetching timezone for public profile :" + username, null);
                    }
                    TimezoneSystemName = (string.IsNullOrWhiteSpace(TimezoneSystemName)) ? "Eastern Standard Time" : TimezoneSystemName;
                    objProfileDetails = objdentistprofile.DoctorPublicProfile(UserId);
                    var ResourceId = CommonAppointment.CalendarResourceIntoList(new clsAppointmentData().GetOprationTheatorForCalendarByLocation(objProfileDetails.LocationId));
                    string ResId = CheckEventIsExists(ResourceId);
                    //objProfileDetails.AppointmentDefaultResourceId = string.IsNullOrEmpty(ResourceId) ? 0 : Convert.ToInt32(ResourceId);
                    objProfileDetails.TimezoneSystemName = TimezoneSystemName;
                    new CommonBLL().FillSessions(UserId, isWidget, ResId, TimezoneSystemName);
                    ViewBag.IsAppOpen = issearch;
                }
                else
                {
                    return RedirectToAction("Index", "Error");
                }

                bool IsFormActive = objdentistprofile.GetPublicProfilesectionDetails(UserId).PatientForms;
                if (!IsFormActive)
                {
                    ViewBag.HideSpecOffBtn = true;
                }
                else
                {
                    ViewBag.HideSpecOffBtn = false;
                }
            }
            else { Response.End(); }
            return View(objProfileDetails);
        }
        public ActionResult PatientLogin(bool isWidget, string username, string valid = null)
        {
            PatientLogin PatientLogin = new PatientLogin();
            int userid = ObjColleaguesData.GetIdOnPublicProfileURL(username);
            if (userid > 0)
            {
                var ResourceId = CommonAppointment.CalendarResourceIntoList(new clsAppointmentData().GetOprationTheatorForCalendarByLocation(new CommonBLL().GetLocationIdOfDoctor(userid)));
                string ResId = CheckEventIsExists(ResourceId);
                string TimezoneSystemName = new clsAppointmentData().GetTimeZoneOfDoctor(userid);
                new CommonBLL().FillSessions(userid, isWidget, ResId, TimezoneSystemName);
            }
            else
            {

                return RedirectToAction("Index", "Error");
            }
            return View(PatientLogin);
        }
        public ActionResult LoginForCurrentPatients(PatientLogin PatientLogins, string username)
        {
            object obj = string.Empty;
            try
            {
                DataTable dtPatient = new DataTable();
                dtPatient = ObjPatientsData.PatientLoginform(PatientLogins.Email, PatientLogins.Password);
                if (dtPatient != null && dtPatient.Rows.Count > 0)
                {
                    Session["PatientId"] = dtPatient.Rows[0]["PatientId"].ToString();
                    Session["UserIdPublic"] = string.Empty;
                    Session["PatientFullName"] = " " + dtPatient.Rows[0]["FirstName"].ToString() + " " + dtPatient.Rows[0]["LastName"].ToString();
                    Session["UserId"] = null;
                    TripleDESCryptoHelper cryptoHelper = new TripleDESCryptoHelper();
                    string encryptedPassword = cryptoHelper.encryptText(PatientLogins.Password);

                    return Redirect(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("DentalSite")) + "/patientlogin?username=" + Convert.ToString(PatientLogins.Email) + "&pass=" + encryptedPassword);

                }
                else
                {
                    DataTable dttemp = new DataTable();
                    dttemp = ObjPatientsData.CheckTempTable(PatientLogins.Email);
                    if (dttemp != null && dttemp.Rows.Count > 0)
                    {
                        obj = "1";
                    }
                    else
                    {
                        obj = "2";
                    }

                    return RedirectToAction("PatientLogin", new { valid = obj, isWidget = Convert.ToBoolean(Session["isWidget"]) });
                }
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog(Request.Url.AbsoluteUri, ex.Message, ex.StackTrace);
                throw;
            }
        }
        public JsonResult MForgotpassword(string Email)
        {
            object obj = null;
            ObjPatientsData = new clsPatientsData();
            try
            {
                DataTable dtPatient = ObjPatientsData.GetPatientPassword(Email);
                if (dtPatient == null || dtPatient.Rows.Count == 0)
                {
                    dtPatient = ObjPatientsData.Dental_GetPatientPasswordTemp(Email);
                }
                if (dtPatient != null && dtPatient.Rows.Count > 0)
                {
                    string Password = dtPatient.Rows[0]["Password"].ToString();
                    string PatientFirstName = dtPatient.Rows[0]["FirstName"].ToString();
                    string PatientLastName = dtPatient.Rows[0]["LastName"].ToString();
                    string PatientEmail = Email;
                    //Send Password To Patient Email
                    ObjTemplate.ResetPatientPassword(PatientEmail, PatientFirstName, PatientLastName);
                    obj = new
                    {
                        Success = true,
                        Message = "Password sent successfully to your email.",
                    };
                }
                else
                {
                    obj = new
                    {
                        Success = false,
                        Message = "Email address does not exist in system.",
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public ActionResult PatientForms(bool isWidget, string username = null, string Email = null)
        {
            // MDLPatientSignUp mdlpatientsignup = new MDLPatientSignUp();
            ViewBag.statelistdata = InsuanceEstimatorBLL.GetStateList();
            MDLPatientSignUpDetail mdlpatientsignup = new MDLPatientSignUpDetail();
            CommonBLL objcommonbll = new CommonBLL();
            int userid = ObjColleaguesData.GetIdOnPublicProfileURL(username);
            if (userid > 0)
            {
                var ResourceId = CommonAppointment.CalendarResourceIntoList(new clsAppointmentData().GetOprationTheatorForCalendarByLocation(new CommonBLL().GetLocationIdOfDoctor(userid)));
                string ResId = CheckEventIsExists(ResourceId);
                string TimezoneSystemName = new clsAppointmentData().GetTimeZoneOfDoctor(userid);
                objcommonbll.FillSessions(userid, isWidget, ResId, TimezoneSystemName);
                if (!string.IsNullOrWhiteSpace(Convert.ToString(TempData["Email"])))
                {
                    Person person = objcommonbll.GetSocialMediaPerson(Convert.ToString(TempData["Email"]));
                    mdlpatientsignup.Email = Convert.ToString(TempData["Email"]);
                    mdlpatientsignup.FirstName = person.FirstName;
                    mdlpatientsignup.LastName = person.LastName;
                    TempData["Email"] = null;
                }
                if (!string.IsNullOrWhiteSpace(Convert.ToString(TempData["OfferCode"])) && !string.IsNullOrWhiteSpace(Convert.ToString(TempData["OfferId"])))
                {
                    mdlpatientsignup.PromoCode = Convert.ToString(TempData["OfferCode"]);
                    mdlpatientsignup.OfferID = Convert.ToString(TempData["OfferId"]);
                    TempData["OfferCode"] = null;
                    TempData["OfferId"] = null;

                }

                mdlpatientsignup.PublicProfileUserId = userid;
                mdlpatientsignup.PublicProfileUserName = username;
                TempData.Keep();
            }
            else
            {

                return RedirectToAction("Index", "Error");
            }
            //return View("PatientSignUp", mdlpatientsignup);
            return View("PatientSignUpNew", mdlpatientsignup);
        }
        [HttpGet]
        public ActionResult PatientFormsFullForm(bool isWidget, string username = null, string Email = null)
        {
            MDLPatientSignUp mdlpatientsignup = new MDLPatientSignUp();
            //MDLPatientSignUpDetail mdlpatientsignup = new MDLPatientSignUpDetail();
            CommonBLL objcommonbll = new CommonBLL();
            int userid = ObjColleaguesData.GetIdOnPublicProfileURL(username);
            if (userid > 0)
            {
                var ResourceId = CommonAppointment.CalendarResourceIntoList(new clsAppointmentData().GetOprationTheatorForCalendarByLocation(new CommonBLL().GetLocationIdOfDoctor(userid)));
                string ResId = CheckEventIsExists(ResourceId);
                string TimezoneSystemName = new clsAppointmentData().GetTimeZoneOfDoctor(userid);
                objcommonbll.FillSessions(userid, isWidget, ResId, TimezoneSystemName);
                if (!string.IsNullOrWhiteSpace(Convert.ToString(TempData["Email"])))
                {
                    Person person = objcommonbll.GetSocialMediaPerson(Convert.ToString(TempData["Email"]));
                    mdlpatientsignup.Email = Convert.ToString(TempData["Email"]);
                    mdlpatientsignup.FirstName = person.FirstName;
                    mdlpatientsignup.LastName = person.LastName;
                    TempData["Email"] = null;
                }
                if (!string.IsNullOrWhiteSpace(Convert.ToString(TempData["OfferCode"])) && !string.IsNullOrWhiteSpace(Convert.ToString(TempData["OfferId"])))
                {
                    mdlpatientsignup.PromoCode = Convert.ToString(TempData["OfferCode"]);
                    mdlpatientsignup.OfferID = Convert.ToString(TempData["OfferId"]);
                    TempData["OfferCode"] = null;
                    TempData["OfferId"] = null;

                }
                mdlpatientsignup.PublicProfileUserId = userid;
                TempData.Keep();
            }
            else
            {

                return RedirectToAction("Index", "Error");
            }
            return View("PatientSignUp", mdlpatientsignup);
            //return View("PatientSignUpNew", mdlpatientsignup);
        }

        public ActionResult PartialSignUpEmail()
        {
            return View();
        }
        [HttpPost]
        public ActionResult PatientSignUpPublicProfile(string Email = null)
        {
            TempData["Email"] = Email;
            return RedirectToAction("PatientForms", new { isWidget = Convert.ToBoolean(Session["isWidget"]) });

        }
        [HttpGet]
        public ActionResult PatientSignUpBySpecialOffer(string OfferCode = null, string OfferId = null)
        {
            TempData["OfferCode"] = OfferCode;
            TempData["OfferId"] = OfferId;
            return RedirectToAction("PatientForms", new { isWidget = Convert.ToBoolean(Session["isWidget"]) });

        }


        [HttpPost]
        //[ValidateAntiForgeryToken]
        [ValidateAntiForgeryToken()]
        public JsonResult PatientSignUpPost(MDLPatientSignUp mdlpatientsignup)
        {
            try
            {
                int NewPatientId = 0;
                if (ModelState.IsValid)
                {

                    if (Session["PublicProfilePatientId"] != null)
                    {
                        mdlpatientsignup.PatientId = Convert.ToInt32(Session["PublicProfilePatientId"]);
                    }
                    DentistProfileBLL objDentistProfile = new DentistProfileBLL();
                    NewPatientId = objDentistProfile.InsertPatient(mdlpatientsignup);
                    mdlpatientsignup.PatientId = NewPatientId;
                    mdlpatientsignup.ObjContactInfo.PatientId = NewPatientId;
                    TempData["PublicProfilePatientId"] = NewPatientId;
                    if (Convert.ToInt32(Session["PublicProfilePatientId"]) == NewPatientId)
                    {
                        ViewBag.SucessMsg = "Patient updated successfully";
                    }
                    else
                    {
                        ViewBag.SucessMsg = "Patient inserted successfully";
                    }
                    Session["PublicProfilePatientId"] = NewPatientId;
                    Session["MainPatientSignUp"] = mdlpatientsignup;
                }
                return Json(NewPatientId, JsonRequestBehavior.AllowGet);
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog(Request.RawUrl, Ex.Message, Ex.StackTrace);
                throw;
            }

        }


        [HttpPost]
        //[ValidateAntiForgeryToken]
        [ValidateAntiForgeryToken()]
        public JsonResult SubmitPatientSignUp(MDLPatientSignUpDetail mdlpatientsignup)
        {
            try
            {
                int NewPatientId = 0;
                DentistProfileBLL objDentistProfile = new DentistProfileBLL();
                NewPatientId = objDentistProfile.InsertNewPatient(mdlpatientsignup);
                mdlpatientsignup.PatientId = NewPatientId;

                //Insert address of patient
                mdlpatientsignup.ObjContactInfo.PatientId = NewPatientId;
                bool Result = objDentistProfile.InsertPatientAddress(mdlpatientsignup.ObjContactInfo);
                bool isObjectHasvalue = clsCommon.AllStringPropertyValuesAreNonEmpty(mdlpatientsignup.ObjContactInfo.objFamilyInformation);

                if (isObjectHasvalue)
                {
                    //Insert Spouse and with relation ship
                    if (!string.IsNullOrWhiteSpace(mdlpatientsignup.ObjContactInfo.objFamilyInformation.SpouseFirstName) && !string.IsNullOrWhiteSpace(mdlpatientsignup.ObjContactInfo.objFamilyInformation.SpouseLastName))
                    {
                        int SpouseId = objDentistProfile.InsertPatient(FillObjctofPatientSignup(mdlpatientsignup.ObjContactInfo.objFamilyInformation, Convert.ToInt32(Session["PublicProfileUserId"])));
                        int Realtion = SaveFamilyDetails(mdlpatientsignup.ObjContactInfo.PatientId, SpouseId, Convert.ToInt32(mdlpatientsignup.ObjContactInfo.objFamilyInformation.Relationship), Convert.ToString(mdlpatientsignup.ObjContactInfo.Gender));
                    }
                    if (!string.IsNullOrWhiteSpace(mdlpatientsignup.ObjContactInfo.objFamilyInformation.ChildFirstName) && !string.IsNullOrWhiteSpace(mdlpatientsignup.ObjContactInfo.objFamilyInformation.ChildLastName))
                    {
                        //Insert Child and with realtion ship
                        int ChildId = objDentistProfile.InsertPatient(FillChildSignupObject(mdlpatientsignup.ObjContactInfo.objFamilyInformation.ChildFirstName, mdlpatientsignup.ObjContactInfo.objFamilyInformation.ChildLastName, Convert.ToInt32(Session["PublicProfileUserId"])));
                        int ChildRealtion = SaveFamilyDetails(mdlpatientsignup.ObjContactInfo.PatientId, ChildId, Convert.ToInt32(mdlpatientsignup.ObjContactInfo.objFamilyInformation.Relationship), Convert.ToString(mdlpatientsignup.ObjContactInfo.Gender));
                    }
                }
                //Insert Insurance Coverage Data
                int i = objdentistprofile.InsertInsuranceCoverage(mdlpatientsignup.objInsuranceCoverage, Convert.ToInt32(NewPatientId));
                //Insert History Data
                objdentistprofile.InsertMediacalHistory(mdlpatientsignup.objHistory, Convert.ToInt32(NewPatientId));
                //Insert Dental History Data
                objdentistprofile.InsertDentalHistory(mdlpatientsignup.objdentalhistory, Convert.ToInt32(NewPatientId));
                ModelState.Clear();
                TempData["DentalSuccess"] = "Patient details inserted successfully";
                return Json(NewPatientId, JsonRequestBehavior.AllowGet);
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog(Request.RawUrl, Ex.Message, Ex.StackTrace);
                throw;
            }

        }


        //AK Implement Code for Insert patient address and generated family logic.
        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult InsertPatientContactInfo(PatientContactInfo ObjPatientContactInfo)
        {
            try
            {
                ObjPatientContactInfo.PatientId = Convert.ToInt32(TempData["PublicProfilePatientId"]);
                TempData.Keep();
                DentistProfileBLL objDentistProfile = new DentistProfileBLL();
                //Insert address of patient
                bool Result = objDentistProfile.InsertPatientAddress(ObjPatientContactInfo);
                bool isObjectHasvalue = clsCommon.AllStringPropertyValuesAreNonEmpty(ObjPatientContactInfo.objFamilyInformation);
                if (isObjectHasvalue)
                {
                    //Insert Spouse and with relation ship
                    if (!string.IsNullOrWhiteSpace(ObjPatientContactInfo.objFamilyInformation.SpouseFirstName) && !string.IsNullOrWhiteSpace(ObjPatientContactInfo.objFamilyInformation.SpouseLastName))
                    {
                        int SpouseId = objDentistProfile.InsertPatient(FillObjctofPatientSignup(ObjPatientContactInfo.objFamilyInformation, Convert.ToInt32(Session["PublicProfileUserId"])));
                        Session["SpouseId"] = SpouseId;
                        int Realtion = SaveFamilyDetails(ObjPatientContactInfo.PatientId, SpouseId, Convert.ToInt32(ObjPatientContactInfo.objFamilyInformation.Relationship), Convert.ToString(ObjPatientContactInfo.Gender));
                    }
                    if (!string.IsNullOrWhiteSpace(ObjPatientContactInfo.objFamilyInformation.ChildFirstName) && !string.IsNullOrWhiteSpace(ObjPatientContactInfo.objFamilyInformation.ChildLastName))
                    {
                        //Insert Child and with realtion ship
                        int ChildId = objDentistProfile.InsertPatient(FillChildSignupObject(ObjPatientContactInfo.objFamilyInformation.ChildFirstName, ObjPatientContactInfo.objFamilyInformation.ChildLastName, Convert.ToInt32(Session["PublicProfileUserId"])));
                        Session["ChildId"] = ChildId;
                        int ChildRealtion = SaveFamilyDetails(ObjPatientContactInfo.PatientId, ChildId, Convert.ToInt32(ObjPatientContactInfo.objFamilyInformation.Relationship), Convert.ToString(ObjPatientContactInfo.Gender));
                    }
                }

                Session["PatientContactInfo"] = ObjPatientContactInfo;
                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog(Request.RawUrl, Ex.Message, Ex.StackTrace);
                throw;
            }
        }
        #region User Ratings 
        public ActionResult Reviews(bool isWidget, string username = null, int page = 1)
        {
            DataTable dt = new DataTable();
            DentistRatingModel model = new DentistRatingModel();

            if (TempData["PatientId"] != null)
            {
                dt = PatientBLL.GetPatientDetailsBy_Id(Convert.ToInt32(TempData["PatientId"]));
            }

            int userid = ObjColleaguesData.GetIdOnPublicProfileURL(username);
            //DentistRatingModel model = new DentistRatingModel();
            if (userid > 0)
            {
                model.ratings = new Ratings();
                model.ratings.ReceiverId = userid;

                var ResourceId = CommonAppointment.CalendarResourceIntoList(new clsAppointmentData().GetOprationTheatorForCalendarByLocation(new CommonBLL().GetLocationIdOfDoctor(userid)));
                string TimezoneSystemName = new clsAppointmentData().GetTimeZoneOfDoctor(userid);
                string ResId = CheckEventIsExists(ResourceId);
                new CommonBLL().FillSessions(userid, isWidget, ResId, TimezoneSystemName);
                model.ratingList = new DentistRatingBLL().GetDentistRatings(userid, page);
                model.totalRatings = new DentistRatingBLL().GetDentistRatingsCount(userid);

                if (dt != null && dt.Rows.Count > 0)
                {
                    model.ratings.SenderName = Convert.ToString(dt.Rows[0]["FirstName"]) + " " + Convert.ToString(dt.Rows[0]["LastName"]);
                    model.ratings.SenderEmail = Convert.ToString(dt.Rows[0]["Email"]);
                }
            }
            else
            {
                return RedirectToAction("Index", "Error");
            }
            return View(model);

        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult InsertReviews(DentistRatingModel model, string username = null)
        {
            int userid = ObjColleaguesData.GetIdOnPublicProfileURL(username);
            if (userid > 0)
            {
                if (ModelState.IsValid)
                {
                    bool result = new DentistRatingBLL().InsertRatings(model);
                    if (result)
                        TempData["Message"] = "Rating submitted successfully. It will be displayed on this page after required verification.";
                    else
                        TempData["Message"] = "Error: Rating not recorded in our database. Please contact Administrator.";

                }
            }
            else
            {

                return RedirectToAction("Index", "Error");
            }
            return RedirectToAction("Reviews", new { isWidget = Convert.ToBoolean(Session["isWidget"]) });
        }
        #endregion


        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult InsuranceCoverage(BO.Models.InsuranceCoverage objInsuranceCoverage)
        {
            int Result = 0;
            try
            {
                int PatientId = 0;
                if (Session["PublicProfilePatientId"] != null)
                {
                    Result = objdentistprofile.InsertInsuranceCoverage(objInsuranceCoverage, Convert.ToInt32(Session["PublicProfilePatientId"]));
                    //Result = objdentistprofile.InsertInsuranceCoverage(objInsuranceCoverage, 304544);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult MedicalHistoryForm(MediacalHisotry objHistory)
        {
            int Result = 0;
            if (Session["PublicProfilePatientId"] != null)
            {
                try
                {
                    Result = objdentistprofile.InsertMediacalHistory(objHistory, Convert.ToInt32(Session["PublicProfilePatientId"]));
                    //Result = objdentistprofile.InsertMediacalHistory(objHistory, 304544);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return Json(Result, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public JsonResult DentalHistoryForm(DentalHistory onjdentalhistory)
        {
            int Result = 0;
            if (Session["PublicProfilePatientId"] != null)
            {
                try
                {
                    Result = objdentistprofile.InsertDentalHistory(onjdentalhistory, Convert.ToInt32(Session["PublicProfilePatientId"]));
                    //Result = objdentistprofile.InsertDentalHistory(onjdentalhistory, 304544);
                    Session["PublicProfilePatientId"] = string.Empty;
                    Session.Clear();
                    ModelState.Clear();
                    TempData["DentalSuccess"] = "Patient details inserted successfully";
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public MDLPatientSignUp FillObjctofPatientSignup(FamilyInformation ObjFamilyInfo, int OwnerId)
        {
            try
            {
                MDLPatientSignUp ObjPatientSignup = new MDLPatientSignUp();
                if (ObjFamilyInfo != null)
                {
                    ObjPatientSignup.PublicProfileUserId = OwnerId;
                    ObjPatientSignup.FirstName = ObjFamilyInfo.SpouseFirstName;
                    ObjPatientSignup.LastName = ObjFamilyInfo.SpouseLastName;
                    ObjPatientSignup.Email = ObjFamilyInfo.Email;
                    ObjPatientSignup.Phone = ObjFamilyInfo.Phone;
                }
                return ObjPatientSignup;

            }
            catch (Exception Ex)
            {

                throw;
            }
        }
        public MDLPatientSignUp FillChildSignupObject(string FirstName, string LastName, int OwnerId)
        {
            MDLPatientSignUp ObjPatientSignup = new MDLPatientSignUp();
            try
            {
                ObjPatientSignup.PublicProfileUserId = OwnerId;
                ObjPatientSignup.FirstName = FirstName;
                ObjPatientSignup.LastName = LastName;
                return ObjPatientSignup;
            }

            catch (Exception Ex)
            {

                throw;
            }
        }
        public int SaveFamilyDetails(int PatientId, int Selectedpatient, int RelativeId, string Gender)
        {
            try
            {
                int status = 0;
                MdlPatient = new mdlPatient();
                if (MdlPatient.CheckReleationIsExist(PatientId, Selectedpatient) > 0)
                {
                    status = (int)FamilyStatus.IsExist;
                }
                else
                {
                    status = MdlPatient.InsertFamilyRelationDetails(PatientId, Selectedpatient, RelativeId) == true ? (int)FamilyStatus.IsSuccess : (int)FamilyStatus.IsFailed;
                    if (status > 0)
                    {
                        status = 0;
                        int CrossRelativeId = MdlPatient.GetCrossReletiveId(RelativeId, Gender);
                        status = MdlPatient.InsertFamilyRelationDetails(Selectedpatient, PatientId, CrossRelativeId) == true ? (int)FamilyStatus.IsSuccess : (int)FamilyStatus.IsFailed;
                    }
                }
                return status;
            }
            catch (Exception)
            {
                throw;
            }

        }
        public ActionResult SpecialOffers(bool isWidget, string username)
        {
            SpecialOffersBLL objOfferBLL = new SpecialOffersBLL();
            CommonBLL objCommonBll = new CommonBLL();
            SpecialOffers model = new SpecialOffers();
            int userid = ObjColleaguesData.GetIdOnPublicProfileURL(username);
            if (userid > 0)
            {
                var ResourceId = CommonAppointment.CalendarResourceIntoList(new clsAppointmentData().GetOprationTheatorForCalendarByLocation(new CommonBLL().GetLocationIdOfDoctor(userid)));
                string TimezoneSystemName = new clsAppointmentData().GetTimeZoneOfDoctor(userid);
                string ResId = CheckEventIsExists(ResourceId);
                objCommonBll.FillSessions(userid, isWidget, ResId, TimezoneSystemName);
                model.lstOffers = objOfferBLL.GetSpecialOfferByUserId(userid);
                model.lstOffers = model.lstOffers.Where(t => t.Status == true).ToList();
            }
            bool IsFormActive = objdentistprofile.GetPublicProfilesectionDetails(userid).PatientForms;
            if (!IsFormActive)
            {
                ViewBag.HideSpecOffBtn = true;
            }
            else
            {
                ViewBag.HideSpecOffBtn = false;
            }
            return View(model);
        }
        #region Send Referral

        public ActionResult ReferPatient(bool isWidget, string Username = null)
        {
            //SendReferralModel model = new SendReferralModel();
            //model.username = Username;

            //int userid = ObjColleaguesData.GetIdOnPublicProfileURL(Username);
            //if (userid > 0)
            //{
            //    model.UserId = userid;
            //    var ResourceId = CommonAppointment.CalendarResourceIntoList(new clsAppointmentData().GetOprationTheatorForCalendarByLocation(new CommonBLL().GetLocationIdOfDoctor(userid))).FirstOrDefault().id;
            //    string TimezoneSystemName = new clsAppointmentData().GetTimeZoneOfDoctor(userid);
            //    new CommonBLL().FillSessions(userid, isWidget, ResourceId, TimezoneSystemName);

            //}
            //else
            //{

            //    return RedirectToAction("Index", "Error");
            //}
            ViewBag.statelistdata = InsuanceEstimatorBLL.GetStateList();
            Compose1Click _compose1Click = new Compose1Click();
            _compose1Click._sendReferralModel = new SendReferralModel();
            _compose1Click._sendReferralModel.username = Username;
            int userid = ObjColleaguesData.GetIdOnPublicProfileURL(Username);
            if (userid > 0)
            {
                _compose1Click._sendReferralModel.UserId = userid;
                var ResourceId = CommonAppointment.CalendarResourceIntoList(new clsAppointmentData().GetOprationTheatorForCalendarByLocation(new CommonBLL().GetLocationIdOfDoctor(userid)));
                _compose1Click.LocationId = new CommonBLL().GetLocationIdOfDoctor(userid);
                string TimezoneSystemName = new clsAppointmentData().GetTimeZoneOfDoctor(userid);
                string ResId = CheckEventIsExists(ResourceId);
                new CommonBLL().FillSessions(userid, isWidget, ResId, TimezoneSystemName);
                _compose1Click._specialtyService = GetVisibleSpeciality(Convert.ToInt32(userid));
                _compose1Click.lstSpecialityMaster = ReferralFormBLL.GetPatientReferralForm(Convert.ToInt32(userid));
                _compose1Click._innerService = ReferralFormBLL.GetPatientInnerService(Convert.ToInt32(userid));
            }
            else
            {

                return RedirectToAction("Index", "Error");
            }
            ViewBag.alert = TempData["success"];
            TempData["success"] = null;
            return View("ReferPatient", _compose1Click);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitSendReferral(SendReferralModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    int userid = ObjColleaguesData.GetIdOnPublicProfileURL(model.username);
                    if (userid > 0)
                    {
                        //object obj = string.Empty;
                        //int[] ReferralId = new int[2];
                        //int FromPublicProfilePatientId = 0;// we use this bcz first check in system if exsits then assign them other wise create new patient

                        //int FromPublicProfileDoctorId = 0;// we use this bcz first check in system if exsits then assign them other wise create new patient

                        //string FromPublicProfileDoctorFullName = string.Empty;
                        //string ToPublicProfileDoctorEmail = string.Empty;// here is email id of doctor whom public prfile is 
                        bool result = new SendReferralBLL().ProcessSendReferral(model, userid);

                        TempData["Message"] = "Patient referred successfully.";


                    }

                }
                catch (Exception)
                {

                    throw;
                }
            }
            return RedirectToAction("ReferPatient", new { isWidget = Convert.ToBoolean(Session["isWidget"]) });
        }
        public SpecialityService GetVisibleSpeciality(int UserId)
        {
            SpecialityService sp = new SpecialityService();
            sp = ReferralFormBLL.GetVisibleSpeciality(UserId);
            return sp;
        }
        public ActionResult PartialFileUploadTempForSendReferral(string FileIds, int MessageId = 0)
        {
            CommonBLL ObjCommonbll = new CommonBLL();
            List<TempFileAttacheMents> objlistofattachements = new List<TempFileAttacheMents>();
            objlistofattachements = ObjCommonbll.GetTempAttachments(FileIds, MessageId);
            return View(objlistofattachements);
        }

        #endregion

        public ActionResult CheckOverLapping(AppointmentData objAppointmentData)
        {
            var objAppointment = new clsAppointmentData();

            string TimezoneSystemName = objAppointment.GetTimeZoneOfDoctor(objAppointmentData.DoctorId);
            var objCreateAppointment = new CreateAppointment();
            var Service = objAppointment.GetAppointmentServices(0, Convert.ToInt32(objAppointmentData.ServiceTypeId));
            int Applength = Convert.ToInt32(Service.Rows[0]["TimeDuration"].ToString());
            TimeSpan temptime = objAppointmentData.AppointmentTime.Add(TimeSpan.FromMinutes(Convert.ToInt32(Applength)));
            objCreateAppointment.ApptResourceId = objAppointmentData.ApptResourceId;
            objCreateAppointment.FromTime = Convert.ToDateTime(objAppointmentData.AppointmentDate.ToString(AppointmentSetting.CONSTDATEFORMATE1) + " " + objAppointmentData.AppointmentTime).ToString();
            //  objCreateAppointment.FromTime = clsHelper.ConvertToUTC(Convert.ToDateTime(objCreateAppointment.FromTime), TimezoneSystemName).ToString();
            objCreateAppointment.ToTime = Convert.ToDateTime(objAppointmentData.AppointmentDate.ToString(AppointmentSetting.CONSTDATEFORMATE1) + " " + temptime).ToString();
            //  objCreateAppointment.ToTime = clsHelper.ConvertToUTC(Convert.ToDateTime(objCreateAppointment.ToTime), TimezoneSystemName).ToString();

            string message = CommonAppointment.CheckOverLappingAndDoctorAvailability(objCreateAppointment, objAppointmentData.DoctorId, TimezoneSystemName);
            objCreateAppointment.AppointmentLength = Applength;
            objCreateAppointment.CurrentDate = Convert.ToDateTime(objAppointmentData.AppointmentDate.ToString(AppointmentSetting.CONSTDATEFORMATE1));
            objCreateAppointment.DoctorId = objAppointmentData.DoctorId;
            if (!string.IsNullOrEmpty(message))
            {
                return Json("Your selected service is " + Applength + " minute, it can't be booked at selected time slot.");
                //return Json(message);
            }
            else if ((CommonAppointment.CheckAvalibleDoctorByDateTimeTheatreDoctor(objCreateAppointment, objCreateAppointment.DoctorId, TimezoneSystemName) > 0))
            {

                return Json("Provider is not available for specified time slot.");
            }

            return Json("");
        }
        public ActionResult AppointmentBooking(bool isWidget, string username = null)
        {
            int UserId = GetIdOnPublicProfileURL(username);
            if (UserId > 0)
            {
                var ResourceId = CommonAppointment.CalendarResourceIntoList(new clsAppointmentData().GetOprationTheatorForCalendarByLocation(new CommonBLL().GetLocationIdOfDoctor(UserId)));
                string TimezoneSystemName = new clsAppointmentData().GetTimeZoneOfDoctor(UserId);
                string ResId = CheckEventIsExists(ResourceId);
                new CommonBLL().FillSessions(UserId, isWidget, ResId, TimezoneSystemName);
            }
            else
            {

                return RedirectToAction("Index", "Error");

            }
            return View();
        }
        public int GetIdOnPublicProfileURL(string url)
        {

            dtUserID = objCommon.GetUserIDFromProfilePath(url);
            if (dtUserID != null && dtUserID.Rows.Count > 0)
            {
                return Convert.ToInt32(dtUserID.Rows[0]["UserID"]);

            }
            else
            {
                return -1;
            }
        }
        #region Appointment Booking
        public ActionResult AppointmentSlotGrid(int DoctorId, string StartDate, int TheatreId)
        {

            clsAppointmentData objclsAppointmentData = new clsAppointmentData();
            string TimezoneSystemName = objclsAppointmentData.GetTimeZoneOfDoctor(DoctorId);
            var TimeSlot = CommonAppointment.GetAppointmentDataIntoList(objclsAppointmentData.GetCreateAppointmentTimeSlotViewData(DoctorId, TheatreId, 0), TimezoneSystemName);
            CreateAppointment DoctorAvalailableTimeSlot = CommonAppointment.TimeSlotAllocation(TimeSlot, new DataTable(), StartDate, 0, TimezoneSystemName);
            // return View(DoctorAvalailableTimeSlot);


            return View("_AppointmentSlotGrid", DoctorAvalailableTimeSlot);

        }

        public ActionResult SignInAndSignUpPage(string DoctorId, string ServiceId, string AppointmentDate, string AppointmentTime)
        {

            return View();
        }

        public ActionResult SignInAndBookPopup()
        {
            return View();
        }
        public ActionResult SignUpAndBookPopup()
        {

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult MemberSignin(int UserId,int AppointmentDefaultResourceId, DateTime AppointmentDate, TimeSpan AppointmentTime, int ServiceTypeId)
        public ActionResult MemberSignin(AppointmentBooking ObjApp, bool isWidget)
        {
            //AppointmentBooking ObjApp = new BO.ViewModel.AppointmentBooking();
            ObjApp.TimeZoneName = new clsAppointmentData().GetTimeZoneOfDoctor(ObjApp.UserId);
            DateTime Appttime = DateTime.Today.Add(ObjApp.AppointmentTime);
            ObjApp.ApptTime = Appttime.ToString("hh:mm tt");
            ObjApp.ApptDate = Convert.ToString(ObjApp.AppointmentDate.ToString("MMM d"));
            ObjApp.ObjDentistProfile = objdentistprofile.DoctorPublicProfile(ObjApp.UserId);
            if (ObjApp.UserId > 0)
            {
                var ResourceId = CommonAppointment.CalendarResourceIntoList(new clsAppointmentData().GetOprationTheatorForCalendarByLocation(new CommonBLL().GetLocationIdOfDoctor(ObjApp.UserId)));
                string TimezoneSystemName = new clsAppointmentData().GetTimeZoneOfDoctor(ObjApp.UserId);
                string ResId = CheckEventIsExists(ResourceId);
                new CommonBLL().FillSessions(ObjApp.UserId, isWidget, ResId, TimezoneSystemName);
            }
            else
            {

                return RedirectToAction("Index", "Error");
            }
            return View(ObjApp);
        }
        public ActionResult SubmitAppointmentBookingDataWithSignIn(AppointmentData objAppointmentData)
        {
            var objAppointment = new clsAppointmentData();
            string Email = objAppointmentData.SignInEmail.Trim();
            ObjTemplate = new clsTemplate();
            var Patientexists = ObjPatientsData.PatientLoginform(Email, objAppointmentData.Password);
            string TimeZoneSystemName = objCommon.CheckNull(Convert.ToString(Patientexists.Rows[0]["TimeZoneSystemName"]), "Eastern Standard Time");
            //if (string.IsNullOrWhiteSpace(TimeZoneSystemName))
            //    TimeZoneSystemName = "Eastern Standard Time";
            string password = string.Empty;

            int patientid = 0;
            if (Patientexists != null && Patientexists.Rows.Count > 0)
            {
                ObjTemplate = new clsTemplate();
                patientid = Convert.ToInt32(Patientexists.Rows[0]["PatientId"]);
                password = Convert.ToString(Patientexists.Rows[0]["ActullPassword"]);
                string EncrPassword = ObjTripleDESCryptoHelper.encryptText(password);
                string EncrEmail = ObjTripleDESCryptoHelper.encryptText(Email);
                DateTime AppointmentDate;
                //TimeSpan AppointmentTime;
                AppointmentDate = objAppointmentData.AppointmentDate.Date.Add(objAppointmentData.AppointmentTime);
                AppointmentDate = clsHelper.ConvertToUTC(AppointmentDate, TimeZoneSystemName);

                //AppointmentDate = Convert.ToDateTime(objAppointmentData.AppointmentDate);
                //AppointmentTime = objAppointmentData.AppointmentTime;
                var Service = objAppointment.GetAppointmentServices(0, Convert.ToInt32(objAppointmentData.ServiceTypeId));
                if (Service.Rows.Count > 0)
                {
                    objAppointmentData.AppointmentLength = Convert.ToInt32(Service.Rows[0]["TimeDuration"].ToString());
                }
                int NewAppId = objAppointment.SubmitAppointmentBookingData(objAppointmentData.DoctorId, patientid, AppointmentDate.Date, AppointmentDate.TimeOfDay, objAppointmentData.AppointmentLength, objAppointmentData.ServiceTypeId, objAppointmentData.PNote, patientid, 1, objAppointmentData.ApptResourceId, 0, 0, objAppointmentData.InsuranceId);
                //int NewAppId = objAppointment.SubmitAppointmentBookingData(objAppointmentData.DoctorId, Convert.ToInt32(Session["PatientId"]), AppointmentDate.Date, AppointmentDate.TimeOfDay, objAppointmentData.AppointmentLength, Convert.ToInt32(objAppointmentData.ServiceTypeId), objAppointmentData.AppNote, Convert.ToInt32(Session["PatientId"]), 1, objAppointmentData.ApptResourceId, 0, 0);
                //Send mail to pateint and doctor for newly created appointment
                //var AppMaildt = objAppointment.GetEmailAllFieldForCreateAppointmentByAppointmentId(NewAppId);
                //ObjTemplate.CreateAppointmentNotificationforPatient(AppMaildt);
                //ObjTemplate.CreateAppointmentNotificationforDoctor(AppMaildt);

                return Json(new { Ismember = "true", Email = EncrEmail, Password = EncrPassword });

            }
            return Json(new { Ismember = "false" });
        }

        public ActionResult SubmitAppointmentBookingDataWithSignUp(AppointmentData objAppointmentData)
        {
            var objAppointment = new clsAppointmentData();
            string Email = objAppointmentData.PEmail.Trim();
            ObjTemplate = new clsTemplate();
            var patientexists = objAppointment.CheckPatientExists(Email);
            string TimezoneSystemName = objAppointment.GetTimeZoneOfDoctor(objAppointmentData.DoctorId);
            int patientid = 0;
            if (patientexists.Rows.Count == 0)
            {
                clsPatientsData ObjPatientsData = new clsPatientsData();
                patientid = ObjPatientsData.PatientInsert(0, "", objAppointmentData.PFirstName, "", objAppointmentData.PLastName, null, 0, null, null, Email, null, null, null, null, null, objAppointmentData.DoctorId, 0, null, objAppointmentData.Password, null);
                ObjTemplate.ResetPatientPassword(Email, objAppointmentData.PFirstName, objAppointmentData.PLastName);


                DateTime AppointmentDate;
                //TimeSpan AppointmentTime;
                AppointmentDate = objAppointmentData.AppointmentDate.Date.Add(objAppointmentData.AppointmentTime);
                AppointmentDate = clsHelper.ConvertToUTC(AppointmentDate, TimezoneSystemName);

                //AppointmentDate = Convert.ToDateTime(objAppointmentData.AppointmentDate);
                //AppointmentTime = objAppointmentData.AppointmentTime;
                var Service = objAppointment.GetAppointmentServices(0, Convert.ToInt32(objAppointmentData.ServiceTypeId));
                if (Service.Rows.Count > 0)
                {
                    objAppointmentData.AppointmentLength = Convert.ToInt32(Service.Rows[0]["TimeDuration"].ToString());
                }
                int NewAppId = objAppointment.SubmitAppointmentBookingData(objAppointmentData.DoctorId, patientid, AppointmentDate.Date, AppointmentDate.TimeOfDay, objAppointmentData.AppointmentLength, objAppointmentData.ServiceTypeId, objAppointmentData.PNote, patientid, 1, objAppointmentData.ApptResourceId, 0, 0, objAppointmentData.InsuranceId);
                //Send mail to pateint and doctor for newly created appointment
                //var AppMaildt = objAppointment.GetEmailAllFieldForCreateAppointmentByAppointmentId(NewAppId);
                //ObjTemplate.CreateAppointmentNotificationforPatient(AppMaildt);
                //ObjTemplate.CreateAppointmentNotificationforDoctor(AppMaildt);

                var temppatientexists = objAppointment.CheckPatientExists(Email);
                string EncrPassword = ObjTripleDESCryptoHelper.encryptText(Convert.ToString(temppatientexists.Rows[0]["ActullPassword"]));
                string EncrEmail = ObjTripleDESCryptoHelper.encryptText(Email);

                return Json(new { Ismember = "true", Email = EncrEmail, Password = EncrPassword });
            }
            return Json(new { Ismember = "false" });
            #endregion
        }
        [HttpGet]
        public JsonResult GetDefaultOP(int DoctorId, int LocationId)
        {
            //int UserId = 0;
            var ResourceList = new CalendarResource();
            //if (!string.IsNullOrEmpty(Convert.ToString(Session["ParentUserId"]))) { UserId = Convert.ToInt32(Session["ParentUserId"]); }
            //else { UserId = Convert.ToInt32(Session["UserId"]); }
            clsAppointmentData objclsAppointmentData = new clsAppointmentData();

            var doctorDetails = objclsAppointmentData.GetDoctorDetails(DoctorId);
            if (doctorDetails.Rows.Count > 0)
            {
                ResourceList = CommonAppointment.CalendarResourceIntoList(objclsAppointmentData.GetOprationTheatorForCalendarByLocation(LocationId)).FirstOrDefault();

            }
            return Json(ResourceList, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetWeekSheduleData(int DoctorId, int TheatreID)
        {
            clsAppointmentData objclsAppointmentData = new clsAppointmentData();
            string TimezoneSystemName = objclsAppointmentData.GetTimeZoneOfDoctor(DoctorId);
            CreateAppointmentDataManagement timeSlotData = CommonAppointment.GetAppointmentDataIntoList(objclsAppointmentData.GetCreateAppointmentTimeSlotViewData(DoctorId, TheatreID, 0), TimezoneSystemName);
            return Json(timeSlotData.WeekSchedulesData);
        }
        public ActionResult InsuranceEstimator(bool isWidget, string username)
        {
            CommonBLL objCommonBll = new CommonBLL();
            SpecialOffers model = new SpecialOffers();
            int userid = ObjColleaguesData.GetIdOnPublicProfileURL(username);
            if (userid > 0)
            {
                var ResourceId = CommonAppointment.CalendarResourceIntoList(new clsAppointmentData().GetOprationTheatorForCalendarByLocation(new CommonBLL().GetLocationIdOfDoctor(userid)));
                string TimezoneSystemName = new clsAppointmentData().GetTimeZoneOfDoctor(userid);
                string ResId = CheckEventIsExists(ResourceId);
                objCommonBll.FillSessions(userid, isWidget, ResId, TimezoneSystemName);
            }

            return View();
        }

        public ActionResult InsuranceEstimatorWidget(bool isWidget, string username)
        {
            CommonBLL objCommonBll = new CommonBLL();
            int userid = ObjColleaguesData.GetIdOnPublicProfileURL(username);
            if (userid > 0)
            {
                InsuranceEstimatorModel model = new InsuranceEstimatorModel();
                model.CompaniesList = InsuanceEstimatorBLL.GetInsuranceCompaniesList(userid);
                model.DentalProceduresList = InsuanceEstimatorBLL.GetDentalProceduresList(userid);
                //Olivia changes on 10-04-2018 for Dr. Lan Allan's website. - DLADCETK-1
                model.SchedulingLink = InsuanceEstimatorBLL.GetAppointmentLink(userid);
                ViewBag.SchedulingLink = model.SchedulingLink;
                return View(model);
            }
            else
            {
                return RedirectToAction("Index", "Error");
            }

        }
        [HttpPost]
        public JsonResult CalculateInsurance(bool isWidget, string username, CalculateInsuranceModel model)
        {
            int UserId = GetIdOnPublicProfileURL(username);
            model = InsuanceEstimatorBLL.CalculateInsurance(model, UserId);
            return Json(model, JsonRequestBehavior.AllowGet);
        }
        public ActionResult InsuranceCostExtimationPatientSignup()
        {
            PatientSignup Obj = new PatientSignup();
            return View(Obj);
        }
        [HttpPost]
        public JsonResult SendRequest(SendInsuranceRequestBO model, string username)
        {
            int UserId = GetIdOnPublicProfileURL(username);
            bool Result = true;
            Result = InsuanceEstimatorBLL.SendCostEstimationRequest(model, UserId);
            return Json(Result);
        }
        [HttpPost]
        public ActionResult SendReferal(Compose1Click model)
        {
            string TextValue = string.Empty;
            string XMLstring = "<NewDataSet>";
            foreach (var item in Request.Form.Keys)
            {
                if (!String.IsNullOrEmpty(Convert.ToString(Request.Form[item.ToString()])))
                {
                    if (item.ToString().StartsWith("sub"))
                    {
                        string[] SplitString; string CategoryID = string.Empty;
                        string SubCatId = string.Empty;
                        string SubSubCatId = string.Empty;
                        SplitString = item.ToString().Split('_');
                        CategoryID = SplitString[1];
                        SubCatId = SplitString[2];
                        if (SplitString[3] == "0")
                        {
                            SplitString[3] = null;
                        }
                        SubSubCatId = ((SplitString[3] != "" && SplitString[3] != null) ? SplitString[3] : null);
                        TextValue = Convert.ToString(Request.Form[item.ToString()]);
                        XMLstring += "<Table><catId>" + SecurityElement.Escape(CategoryID) + "</catId>";
                        XMLstring += "<SubCatId>" + SecurityElement.Escape(SubCatId) + "</SubCatId>";
                        XMLstring += "<SubSubCatId>" + SecurityElement.Escape(SubSubCatId) + "</SubSubCatId>";
                        XMLstring += "<Value>" + SecurityElement.Escape(TextValue) + "</Value>";
                        XMLstring += "<ReferrelCardId>" + null + "</ReferrelCardId></Table>";
                    }
                }
            }
            XMLstring += "</NewDataSet>";
            model.XMLstring = XMLstring;

           bool result = SendReferralBLL.ComposeReferral(model, model._sendReferralModel.UserId);
            //FileUploadController objFileUpload = new FileUploadController();
            //IDictionary<bool, string> img_result = objFileUpload.TempToImageBankUpload(PatientId, Convert.ToInt32(SessionManagement.UserId), false);
            //if(img_result.FirstOrDefault().Key == true)
            //{
            //    TempData["success"] = true;
            //    ViewBag.alert = true;
            //}
            //else
            //{
            //    TempData["success"] = false;
            //    ViewBag.alert = false;
            //}
            return RedirectToAction("ReferPatient");

        }
        public static string CheckEventIsExists(List<CalendarResource> Lst)
        {
            try
            {
                string ResId = string.Empty;
                if (Lst.Count > 0)
                {
                    ResId = Convert.ToString(Lst.FirstOrDefault().id);
                }
                return ResId;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("P Controller", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public ActionResult RequestReview(string PatientId)
        {
            TempData["PatientId"] = PatientId;
            return RedirectToAction("Reviews");
        }
    }
}

