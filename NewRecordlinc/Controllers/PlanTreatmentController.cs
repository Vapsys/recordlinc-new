﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BO.Models;
using BusinessLogicLayer;
using NewRecordlinc.App_Start;
using NewRecordlinc.Models.ViewModel;
using BO.ViewModel;
using System.Net;

namespace NewRecordlinc.Controllers
{
    [RoutePrefix("Plan")]
    [Route("{action}")]
    public class PlanTreatmentController : Controller
    {

        [CustomAuthorize]
        // GET: PlanTreatment
        [Route("")]
        public ActionResult Index()
        {
            try
            {
                CaseDefaultStep objCaseDefaultStep = new CaseDefaultStep();
                objCaseDefaultStep.PatientListing = PlanTreatmentBLL.GetPatientList(Convert.ToInt32(SessionManagement.UserId), null, "0");
                if (Request.Form["hdnPatienHistoryId"] != null)
                {
                    TempData["PatientHistoryPatientId"] = Request.Form["hdnPatienHistoryId"].ToString();
                }
                else if (Request.QueryString["PatientId"] != null)
                {
                    TempData["PatientHistoryPatientId"] = Request.QueryString["PatientId"].ToString();
                }

                TempData.Keep();
                return View(objCaseDefaultStep);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public ActionResult PlanPatientDetails(string patientId = "0")
        {
            int Userid = Convert.ToInt32(SessionManagement.UserId);
            CaseDefaultStep objCaseDefaultStep = new CaseDefaultStep();
            if(TempData["PatientHistoryPatientId"] != null)
            {
                var PatientId = Convert.ToInt32(TempData["PatientHistoryPatientId"]);
                objCaseDefaultStep = PlanTreatmentBLL.GetPatientDetials(PatientId, SessionManagement.TimeZoneSystemName);
                TempData.Keep();
            }
            if (!string.IsNullOrEmpty(patientId))
            {
                var PatientId = Convert.ToInt32(patientId);
                objCaseDefaultStep = PlanTreatmentBLL.GetPatientDetials(PatientId, SessionManagement.TimeZoneSystemName);
            }
            return View("PlanPatientDetails", objCaseDefaultStep);
        }

        [CustomAuthorize]
        public ActionResult PartialPlanTreatmentSection(string _patientId = null, string _caseId = null)
        {
            try
            {
                int Userid = Convert.ToInt32(SessionManagement.UserId);
                List<CaseSteps> lstCaseDefaultSteps = new List<CaseSteps>();
                List<SelectListItem> objColleagueList = PlanTreatmentBLL.GetColleagueList("0");
                if (!string.IsNullOrEmpty(_patientId) && !string.IsNullOrEmpty(_caseId))
                {
                    var cases = PlanTreatmentBLL.GetPlanDetailsByCaseId(Convert.ToInt32(_caseId), Convert.ToInt32(_patientId), SessionManagement.TimeZoneSystemName);
                    lstCaseDefaultSteps = cases._caseStepList;
                    ViewBag.ColleagueList = objColleagueList;
                }
                else
                {
                    lstCaseDefaultSteps = PlanTreatmentBLL.GetAllCaseDefaultSteps(Userid);                    
                    ViewBag.ColleagueList = objColleagueList;
                }
                return View("PartialPlanTreatmentList", lstCaseDefaultSteps);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [CustomAuthorize]
        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult Create(PlanTreatmentVM model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    bool result = false;
                    int _patientId = Convert.ToInt32(model.PatientId);
                    var IsPatientPlanExist = false;//PlanTreatmentBLL.CheckPatientPlanByPatientId(_patientId);
                    if (!IsPatientPlanExist)
                    {
                        result = PlanTreatmentBLL.InsertPlanDetails(model);
                        if (result)
                        {
                            return Json(new { Success = true, Message = "OK" });
                        }
                        else
                        {
                            return Json(new { Success = false, Message = "Plan is not saved. Please try again!" });
                        }
                    }
                    else
                    {
                        return Json(new { Success = false, Message = "Plan is already exists for the Patient #" + _patientId + ": " + model.fullname + "" });
                    }
                }
                else
                {
                    return Json(new { Success = false, Message = HttpStatusCode.BadRequest });
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [CustomAuthorize]
        public ActionResult EditPlan(string caseId, string patientId)
        {
            TempData["CaseId"] = Convert.ToInt32(caseId);
            TempData["PatientId"] = Convert.ToInt32(patientId);
            return RedirectToAction("Edit");
        }

        [CustomAuthorize]
        public ActionResult Edit()
        {
            if (TempData["CaseId"] != null || TempData["PatientId"] != null)
            {
                var GetPlanDetails = PlanTreatmentBLL.GetPlanDetailsByCaseId(Convert.ToInt32(TempData["CaseId"]),Convert.ToInt32(TempData["PatientId"]), SessionManagement.TimeZoneSystemName);
                List<SelectListItem> objColleagueList = PlanTreatmentBLL.GetColleagueList("0");
                ViewBag.ColleagueList = objColleagueList;
                TempData.Keep();
                return View("EditPlan", GetPlanDetails);
            }
            else
            {
                return RedirectToAction("Index");
            }
            
        }

        [CustomAuthorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPlan(PlanTreatmentVM objPlanTreatmentVM)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var result = PlanTreatmentBLL.UpdatePlanDetails(objPlanTreatmentVM);
                    if (result)
                    {
                        return Json(new { Success = true, Message = "OK" });
                    }
                    else
                    {
                        return Json(new { Success = false, Message = "Plan is not updated. Please try again!" });
                    }
                }
                else
                {
                    return Json(new { Success = false, Message = HttpStatusCode.BadRequest });
                }           
            }
            catch (Exception ex)
            {
                throw;
            }            
        }
        public JsonResult DeleteCaseStep(int CaseStepId)
        {
            return Json(PlanTreatmentBLL.DeleteCaseStep(CaseStepId, Convert.ToInt32(SessionManagement.UserId)), JsonRequestBehavior.AllowGet);
        }
    }
}
