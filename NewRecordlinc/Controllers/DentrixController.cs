﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewRecordlinc.App_Start;

namespace NewRecordlinc.Controllers
{
    public class DentrixController : Controller
    {
        

        public ActionResult Index()
        {
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                return RedirectToAction("Index", "Index");
            }
            else
            {
                return View();

            }
        }

    }
}
