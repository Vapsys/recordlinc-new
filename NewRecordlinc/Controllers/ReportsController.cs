﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using NewRecordlinc.App_Start;
using NewRecordlinc.Models.Patients;
using DataAccessLayer.PatientsData;
using DataAccessLayer.Common;
using System.Web.Configuration;
using System.Data;
using System.IO;
using PagedList;
using NewRecordlinc.Models.Colleagues;
using System.Configuration;
using BO.ViewModel;
using BO.Models;
using BusinessLogicLayer;
using System.Text;

namespace NewRecordlinc.Controllers
{
    [RoutePrefix("Reports")]
    [Route("{action}")]
    public class ViewReportsController : Controller
    {
        mdlPatient MdlPatient = null;
        clsPatientsData ObjPatientsData = new clsPatientsData();
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        mdlColleagues MdlColleagues = null;
        clsCommon ObjCommon = new clsCommon();
        int PageSize = Convert.ToInt32(WebConfigurationManager.AppSettings["PageSize"]);
        string ImageBankFolder = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("ImageBank"));
        [CustomAuthorizeAttribute]
        [Route("")]
        public ActionResult Index()
        {
            bool Result = ObjCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
            int CheckId= ZPPaymentBAL.CheckDoctoreExistsOrNot(Convert.ToInt32(Session["UserId"]));
            ViewBag.CheckUser = CheckId;
            return View();
        }
        [CustomAuthorizeAttribute]
        public ActionResult PatientNote()
        {
            return View();
        }
        [CustomAuthorizeAttribute]
        public ActionResult PatientNotepartial(int PageSize, int PageIndex, int ShortColumn, int ShortDirection)
        {
            int UserId = Convert.ToInt32(Session["UserId"]);
            MdlPatient = new mdlPatient();
            return View(MdlPatient.PatientNoteForDoctor(UserId, PageIndex, PageSize, ShortDirection, ShortColumn, SessionManagement.TimeZoneSystemName));
        }
        [CustomAuthorizeAttribute]
        public string PatientNotepartialOnScroll(int PageSize, int PageIndex, int ShortColumn, int ShortDirection)
        {
            int UserId = Convert.ToInt32(Session["UserId"]);
            MdlPatient = new mdlPatient();
            string viewName = "PatientNotepartial";
            var lst = MdlPatient.PatientNoteForDoctor(UserId, PageIndex, PageSize, ShortDirection, ShortColumn, SessionManagement.TimeZoneSystemName);
            if (lst.Count.Equals(0))
            {
                return "1";
            }
            ViewData.Model = lst;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }

        }
        [CustomAuthorizeAttribute]
        public ActionResult AppointmentReport()
        {

            return View();
        }
        [CustomAuthorizeAttribute]
        public ActionResult PartialAppointmentView(string SortColumn, string SortDirection, string SearchText, string Name, int Age, string DoctorName, string Alert, DateTime? LastHygiene, string HygieneType, DateTime? NextAppointmentTime, DateTime? LastAppointmentTime)
        {
            if (SearchText == "undefined")
            {
                SearchText = "";
            }
            mdlPatient MdlPatient = new mdlPatient();
            return View(MdlPatient.GetAppointmentReportList(Convert.ToInt32(Session["UserId"]), SortColumn, SortDirection, 1, 7000, SearchText, SessionManagement.TimeZoneSystemName, Name, Age, DoctorName, Alert, LastHygiene, HygieneType, NextAppointmentTime, LastAppointmentTime));
        }
        [CustomAuthorizeAttribute]
        public ActionResult AppointmentSorting(string SortColumn, string SortDirection, string SearchText, string Name, int Age, string DoctorName, string Alert, DateTime? LastHygiene, string HygieneType, DateTime? NextAppointmentTime, DateTime? LastAppointmentTime)
        {
            mdlPatient MDLpatient = new mdlPatient();
            //   var data = PartialAppointmentView(SortColumn, SortDirection, SearchText,Name,Age,DoctorName,Alert,LastHygiene,HygieneType,NextAppointmentTime,LastAppointmentTime);
            // return Json(Convert.ToString(data), JsonRequestBehavior.AllowGet);
            mdlPatient MdlPatient = new mdlPatient();
            return PartialView("PartialAppointmentView", MdlPatient.GetAppointmentReportList(Convert.ToInt32(Session["UserId"]), SortColumn, SortDirection, 1, 7000, SearchText, SessionManagement.TimeZoneSystemName, Name, Age, DoctorName, Alert, LastHygiene, HygieneType, NextAppointmentTime, LastAppointmentTime));
        }
        
        
        [CustomAuthorizeAttribute]
        [Route("TreatmentPlanReport")]
        public ActionResult CaseManagementList()
        {
            return View();
        }

        [CustomAuthorizeAttribute]
        public ActionResult PartialCaseManagementView(string SortColumn, string SortDirection, string SearchText, string PatientName, string DoctorName, string TreatmentStatus, DateTime? StartDate, DateTime? EndDate)
        {
            if (SearchText == "undefined")
            {
                SearchText = "";
            }
            PlanTreatmentReport objTreatment = new PlanTreatmentReport();
            List<PlanTreatmentReport> treatmentlist = new List<PlanTreatmentReport>();
            // int PatientId = 0;
            //Boolean IsTreatmentAccepted = true;          
            treatmentlist = PlanTreatmentBLL.GetCaseReportList(Convert.ToInt32(Session["UserId"]), SortColumn, SortDirection, 1, 50, SearchText, SessionManagement.TimeZoneSystemName, PatientName, DoctorName, 0, TreatmentStatus, StartDate, EndDate,"3");

            return View(treatmentlist);
        }

        [CustomAuthorizeAttribute]
        public ActionResult CaseManagementSorting(string SortColumn, string SortDirection, string SearchText, string PatientName, string DoctorName, string TreatmentStatus, DateTime? StartDate, DateTime? EndDate,string PlanStatus)
        {
          
            return PartialView("PartialCaseManagementView", PlanTreatmentBLL.GetCaseReportList(Convert.ToInt32(Session["UserId"]), SortColumn, SortDirection, 1, 50, SearchText, SessionManagement.TimeZoneSystemName, PatientName, DoctorName, 0, TreatmentStatus, StartDate, EndDate, PlanStatus)); 
        }


        #region Patient Notification Setting
        [CustomAuthorizeAttribute]
        public ActionResult PatientNotificationSetting(int? page)
        {
            MdlPatient = new mdlPatient();
            IPagedList<PatientDetailsOfDoctor> Patient = null;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings.Get("PageSize"));
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            int Id = Convert.ToInt32(Session["UserId"]);
            MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetAllPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), null, 2, 1, null, 0);
            Patient = MdlPatient.lstPatientDetailsOfDoctor.ToPagedList(pageIndex, pageSize);
            return View("PatientNotificationSetting", Patient);
        }

        [HttpPost]
        public ActionResult InsertUpdateNotificationDetail(List<PatientDetailsOfDoctor> lstPatientDetailsOfDoctor, int? page)
        {
            try
            {
                foreach (var item in lstPatientDetailsOfDoctor)
                {
                    // if (item.Voice == true || item.Text == true)
                    ObjPatientsData.NotificationSettingInsertAndUpdate(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(item.PatientId), item.AccountId, item.Voice, item.Text, item.ChkEmail);
                }
                TempData["ErrorMessage"] = "Notifcation setting updated successfully";
            }
            catch (Exception)
            {
                throw;
            }
            return RedirectToAction("PatientNotificationSetting", new { page = page });
        }

        [HttpPost]
        public JsonResult InsertUpdateNotificationDetailForAll(string Voice, string Text, string Email)
        {
            object obj = string.Empty;
            try
            {
                ObjPatientsData.NotificationSettingInsertAndUpdateForAll(Convert.ToInt32(Session["UserId"]), Convert.ToBoolean(Voice), Convert.ToBoolean(Text), Convert.ToBoolean(Email));
            }
            catch (Exception)
            {
                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Rating Report
        [CustomAuthorizeAttribute]
        public ActionResult RatingReport(int page = 1, int status = 0)
        {
            {
                if (SessionManagement.UserId != null && SessionManagement.UserId != "")
                {
                    int userid = Convert.ToInt32(SessionManagement.UserId);

                    DentistRatingModel model = new DentistRatingModel();
                    if (userid > 0)
                    {
                        model.ratings = new Ratings();
                        model.ratings.ReceiverId = userid;
                        model.ratingList = new DentistRatingBLL().GetDentistRatingsByStatus(userid, 0, status, page, 10, 2, 1);
                        model.totalRatings = new DentistRatingBLL().GetDentistRatingsCountByStatus(userid, status);
                    }
                    else
                    {
                        return Content("<h1> Invalid request </h1>", "text/html");
                    }
                    return View(model);
                }
                else
                {
                    return RedirectToAction("Index", "User");
                }
            }
        }
        [CustomAuthorizeAttribute]
        [System.Web.Http.HttpPost]
        public ActionResult GetRatingReport(int status = 0, int page = 1, string SortBy = "1,2")
        {
            int userid = Convert.ToInt32(SessionManagement.UserId);
            DentistRatingModel model = new DentistRatingModel();
            if (userid > 0)
            {
                int SortOrder = 2;
                if (SortBy == "1,1" || SortBy == "3,1" || SortBy == "3,1" || SortBy == "4,1" || SortBy == "5,1")
                {
                    SortOrder = 1;
                }
                string[] str = SortBy.Split(',');
                int SortById = Convert.ToInt32(str[0]);
                model.ratings = new Ratings();
                model.ratings.ReceiverId = userid;
                model.ratingList = new DentistRatingBLL().GetDentistRatingsByStatus(userid, 0, status, page, 10, SortOrder, SortById);
                model.totalRatings = new DentistRatingBLL().GetDentistRatingsCountByStatus(userid, status);
            }
            else
            {
                return Content("<h1> Invalid request </h1>", "text/html");
            }
            return PartialView("_RatingList", model);
        }
        [CustomAuthorizeAttribute]
        public JsonResult ApproveRating(FormCollection formData)
        {
            var obj = false;
            DentistRatingBLL objBLL = new DentistRatingBLL();
            int userid = Convert.ToInt32(SessionManagement.UserId);
            var fileIds = formData["item.RatingId"].Split(',');
            var selectedIndices = formData["item.IsCheck"].Replace("true,false", "true").Split(',').Select((item, index) => new
            {
                item = item,
                index = index
            }).Where(row => row.item == "true").Select(row => row.index).ToArray();
            if (selectedIndices.Count() > 0)
            {
                foreach (var index in selectedIndices)
                {
                    int checkedID = Convert.ToInt32(fileIds[index]);
                    obj = objBLL.ApproveDeleteRating(checkedID, userid, 1);
                }
            }
            TempData["Message"] = "Reviews approved successfully";
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorizeAttribute]
        public JsonResult DeleteRating(FormCollection formData)
        {
            var obj = false;
            DentistRatingBLL objBLL = new DentistRatingBLL();
            int userid = Convert.ToInt32(SessionManagement.UserId);
            var fileIds = formData["item.RatingId"].Split(',');
            var selectedIndices = formData["item.IsCheck"].Replace("true,false", "true").Split(',').Select((item, index) => new
            {
                item = item,
                index = index
            }).Where(row => row.item == "true").Select(row => row.index).ToArray();
            if (selectedIndices.Count() > 0)
            {
                foreach (var index in selectedIndices)
                {
                    int checkedID = Convert.ToInt32(fileIds[index]);
                    obj = objBLL.ApproveDeleteRating(checkedID, userid, 2);
                }
            }
            TempData["Message"] = "Reviews deleted successfully";
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        #endregion
        [CustomAuthorizeAttribute]
        public ActionResult InsuredAppointments()
        {
            return View();
        }
        [CustomAuthorizeAttribute]
        public ActionResult GetInsuredList(int SortColumn, int SortDirection, int PageIdex, int PageSize, string SearchText)
        {
            AppointmentBLL ObjApp = new AppointmentBLL();
            List<InsuredAppointment> lst = new List<InsuredAppointment>();
            lst = ObjApp.GetInsuredAppointmentlist(Convert.ToInt32(SessionManagement.UserId), SortColumn, SortDirection, PageIdex, PageSize, SearchText, SessionManagement.TimeZoneSystemName);
            return View(lst);
        }
        public ActionResult RequestiLuvMyDentist(int? Page)
        {
            MdlPatient = new mdlPatient();
            IPagedList<PatientDetailsOfDoctor> Patient = null;
            int pageSize = Convert.ToInt32(ConfigurationManager.AppSettings.Get("PageSize"));
            int pageIndex = 1;
            pageIndex = Page.HasValue ? Convert.ToInt32(Page) : 1;
            int Id = Convert.ToInt32(Session["UserId"]);
            MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetAllPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), null, 2, 1, null, 0);
            Patient = MdlPatient.lstPatientDetailsOfDoctor.ToPagedList(pageIndex, pageSize);
            return View(Patient);
        }
        public JsonResult SendRequestToPatient(SendMail Obj)
        {
            try
            {
                bool Result = false;
                try
                {
                    CommonBLL objecting = new CommonBLL();
                    Result = objecting.SendRequestForIluvMyDentist(Convert.ToInt32(Session["UserId"]), Obj.PatientId, Obj.PatientName, Obj.PatientEmail);
                }
                catch (Exception)
                {
                    throw;
                }
                return Json(Result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public JsonResult SendRequestToSelectedPatient(List<int> PatientIds)
        {
            bool Result = false;
            if(PatientIds != null)
            {
                foreach (var item in PatientIds)
                {
                    try
                    {
                        PatientDetail Obj = new PatientDetail();
                        Obj = new MessagesBLL().GetPatientDetail(Convert.ToInt32(item), Convert.ToInt32(SessionManagement.UserId));
                        CommonBLL objecting = new CommonBLL();
                        Result = objecting.SendRequestForIluvMyDentist(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(item), Obj.PatientName, Obj.Email);
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                }
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult SendRequestToAllPatient()
        {
            bool Result = false;
            MdlPatient = new mdlPatient();
            MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetAllPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), null, 2, 1, null, 0);
            foreach (var item in MdlPatient.lstPatientDetailsOfDoctor)
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(item.Email))
                    {
                        CommonBLL objecting = new CommonBLL();
                        Result = objecting.SendRequestForIluvMyDentist(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(item.PatientId), item.FirstName + " " + item.LastName, item.Email);
                    }
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorize]
        [Route("PatientEstimationReport")]
        public ActionResult InsuranceReport()
        {
            List<EstimateReport> lst = new List<EstimateReport>();
            InsuanceEstimatorBLL objBLL = new InsuanceEstimatorBLL();
            lst = InsuanceEstimatorBLL.GetPatientEstimateReport(Convert.ToInt32(SessionManagement.UserId));
            return View(lst);
        }
        [CustomAuthorizeAttribute]
        public ActionResult PatientRewards()
        {
            PatientReport Bll = new PatientReport();
            List<PatientReward> lst = new List<PatientReward>();
            lst = Bll.GetPatientList(Convert.ToInt32(SessionManagement.UserId), 1, 50, 1, 1);
            return View(lst);
        }
        [CustomAuthorizeAttribute]
        public ActionResult PatientRewardList(CommonParameter Obj)
        {
            PatientReport Bll = new PatientReport();
            List<PatientReward> lst = new List<PatientReward>();
            lst = Bll.GetPatientList(Convert.ToInt32(SessionManagement.UserId),Obj.PageIndex,Obj.PageSize,Obj.SortColumn,Obj.SortDirection);
            return View("View",lst);
        }
        [CustomAuthorizeAttribute]
        public ActionResult OneClickReferralHistory(ReferralFilter FilterObj)
        {
           // int UserId = Convert.ToInt32(SessionManagement.UserId);
            FilterObj.PageIndex = 1;
            FilterObj.SortColumn = 1;
            FilterObj.SortDirection = 2;
            FilterObj.PageSize = 35;
            FilterObj.UserId = Convert.ToInt32(SessionManagement.UserId);
            PatientReport Bll = new PatientReport();
            List<OneClickReferralDetails> lst = new List<OneClickReferralDetails>();
            lst = Bll.GetOneClickReferralHistoryById(FilterObj);
            List<Disposition> Dispositionlistlist = new List<Disposition>();
            Dispositionlistlist = Bll.GetDispositionlist();
            ViewBag.Dispositionlist = Dispositionlistlist;
            return View(lst);
        }
        [CustomAuthorizeAttribute]
        public ActionResult GetOneClickReferralHistory(ReferralFilter FilterObj)
        {
            int UserId = Convert.ToInt32(SessionManagement.UserId);
            FilterObj.UserId = UserId;
            FilterObj.PageSize = 35;
            PatientReport Bll = new PatientReport();
            List<OneClickReferralDetails> list = new List<OneClickReferralDetails>();
            list = Bll.GetOneClickReferralHistoryById(FilterObj);
            return View("_PartialOneClickreferralItems",list);
        }
        
        [HttpPost]
        public ActionResult UpdateDispositionStatus(int Disposition,int MessageId)
        { 
            bool Result = PatientReport.UpdateDispositionStatus(Disposition, MessageId, Convert.ToInt32(SessionManagement.UserId));
            return Json(Result,JsonRequestBehavior.AllowGet);
        }

        public ActionResult PaymentHistory(ZiftPayFilter FilterObj)
        {
            FilterObj.SortColumn = 1;
            FilterObj.SortDirection = 2;
            int UserId = Convert.ToInt32(SessionManagement.UserId);
            ZPPaymentBAL zpbal = new ZPPaymentBAL();
            FilterObj.UserId = UserId;
            List<MerchantOwner> list = new List<MerchantOwner>();
            list = ZPPaymentBAL.GetDoctorPaymentDetailList(FilterObj);         
            return View("GetZiftPayHistoryByDentistId", list);
        }

        public ActionResult GetPaymentHistory(ZiftPayFilter FilterObj)
        {
            int UserId = Convert.ToInt32(SessionManagement.UserId);
            FilterObj.UserId = Convert.ToInt32(SessionManagement.UserId);
            List<MerchantOwner> list = new List<MerchantOwner>();
            list = ZPPaymentBAL.GetDoctorPaymentDetailList(FilterObj);
            return View("_PartialPaymentReportForDentist", list);
        }
        [HttpPost]
        public string SearchdoctorName(string doctorName)
        {
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            StringBuilder StrResult = new StringBuilder();
            clsCommon objCommon = new clsCommon();
            dt = PatientReport.SearchdoctorName(doctorName);
            if (dt.Rows.Count > 0)
            {
                sb.Append("[");
                foreach (DataRow item in dt.Rows)
                {
                    sb.Append("{'label':\"" + objCommon.CheckNull(Convert.ToString(item["DoctorName"]), string.Empty) + "\",'value':\"" + objCommon.CheckNull(Convert.ToString(item["DoctorName"]), string.Empty) + "\"},");
                }
                StrResult.Append(sb.ToString().Substring(0, sb.ToString().Length - 1));
                StrResult.Append("]");
            }

            return StrResult.ToString();
        }
    }
}
