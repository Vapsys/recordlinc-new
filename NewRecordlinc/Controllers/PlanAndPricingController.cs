﻿using System.Web.Mvc;
using BusinessLogicLayer;
using BO.Models;
using BO.ViewModel;
using System.Linq;
using System.Collections.Generic;
using DataAccessLayer.ColleaguesData;
using System.Data;
using System;
using DataAccessLayer.Common;
using System.Web.Helpers;
using NewRecordlinc.Models.Colleagues;
using NewRecordlinc.Models.Common;
using System.Configuration;
using RestSharp.Contrib;
using System.Net;
using System.IO;
using NewRecordlinc.Models;
using System.Globalization;
using DataAccessLayer.FlipTop;

namespace NewRecordlinc.Controllers
{
    public class PlanAndPricingController : Controller
    {
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        clsAdmin ObjAdmin = new clsAdmin();
        clsCompany objcompany = new clsCompany();
        clsTemplate ObjTemplate = new clsTemplate();
        clsCommon objCommon = new clsCommon();
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        public ActionResult Index()
        {
            PlanAndPricing Obj = new PlanAndPricing();
            PlanAndPricingBLL getPlanAndPricing = new PlanAndPricingBLL();
            Obj.objPlanPricing = getPlanAndPricing.GetPlanPricing();            
            return View("NewIndex", Obj);
        }
        public ActionResult NewIndex()
        {
            PlanAndPricing Obj = new PlanAndPricing();
            PlanAndPricingBLL getPlanAndPricing = new PlanAndPricingBLL();
            Obj.objPlanPricing = getPlanAndPricing.GetPlanPricing();
            Obj.lstPlanPrice = getPlanAndPricing.GetPlanDetialsForTablet();
            return View("NewIndex", Obj);
        }
        public ActionResult Step1()
        {            
            return View();
        }
        public ActionResult Step2()
        {
            Members ObjMember = new Members();            
            return View(ObjMember);
        }
        public ActionResult Step3()
        {
            BillingInfo ObjBillingInfo = new BillingInfo();            
            return View(ObjBillingInfo);
        }
        public ActionResult Step4()
        {
            return View();
        }

        public JsonResult SubmitSignup(Members model) 
        {
            //Unmask the Phone number
            model.Phone = model.Phone.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "");
            var result = new Dictionary<bool, string>();
            if (ModelState.IsValid)
            {
                result = PlanAndPricingBLL.Signup(model);
            }
            else
            {
                result.Add(false, "2");
            }
            return Json(result.FirstOrDefault());
        }
        public JsonResult SubmitBillingInfo(BillingInfo Model)
        {
            bool result = false;            
            if (ModelState.IsValid)
            {
                result = true;
            }
            return Json(result,JsonRequestBehavior.AllowGet);
        }
        public JsonResult FinalSubmitForm(BO.ViewModel.PlanAndPricing ObjPlanAndPrice)
        {
            var result = new Dictionary<bool, string>();
            bool isResult = false;
            if (ModelState.IsValid)
            {
                DataTable dtCheckEmail = new DataTable();
                dtCheckEmail = ObjColleaguesData.CheckEmailInSystem(ObjPlanAndPrice.Signup.Email);
                if (dtCheckEmail != null && dtCheckEmail.Rows.Count > 0)
                {
                    DataTable dtFirstLogin = ObjAdmin.GetTempSignupByEmail(ObjPlanAndPrice.Signup.Email);
                    if (dtFirstLogin != null && dtFirstLogin.Rows.Count > 0)
                    {
                        string Password = objCommon.CreateRandomPassword(8);
                        string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                        string Accountkey = Crypto.SHA1(Convert.ToString(DateTime.Now.Ticks));
                        int UserId = 0;
                        if (dtFirstLogin.Rows.Count > 0)
                        {
                            UserId = ObjColleaguesData.SingUpDoctor(ObjPlanAndPrice.Signup.Email, EncPassword,
                                (objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["first_name"]), ObjPlanAndPrice.Signup.FirstName)),
                                (objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["last_name"]), ObjPlanAndPrice.Signup.LastName)), null, null, null, Accountkey, null, null, null, null, null, null, (objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["phoneno"]), string.Empty)), null, false, objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["facebook"]), string.Empty), objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["linkedin"]), string.Empty), objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["twitter"]), string.Empty), null, null, objCommon.CheckNull(Convert.ToString(dtFirstLogin.Rows[0]["description"]), string.Empty), null, null, null, null, null, null, null, 0, null, null, null, 0);
                            ObjAdmin.UpdateStatusInTempTableUserById(Convert.ToInt32(dtFirstLogin.Rows[0]["id"]), 1);// update status 1 in temp table 
                            ObjTemplate.NewUserEmailFormat(ObjPlanAndPrice.Signup.Email, Session["CompanyWebsite"].ToString() + "/User/Index?UserName=" + ObjPlanAndPrice.Signup.Email + "&Password=" + EncPassword, Password, Session["CompanyWebsite"].ToString());

                        }
                        //Here is the billing code for the user.
                        #region Payment code
                        BillingInfo objbill = ObjPlanAndPrice.ObjBillingInfo;
                        Members objmem = ObjPlanAndPrice.Signup;

                        PyamentDetails objPyamentDetails = new PyamentDetails();
                        objPyamentDetails.Planes = Convert.ToString(ObjPlanAndPrice.MembershipId);
                        objPyamentDetails.Firstname = objmem.FirstName;
                        objPyamentDetails.Lastname = objmem.LastName;
                        objPyamentDetails.Email = objmem.Email;
                        objPyamentDetails.Phone = objmem.Phone;
                        objPyamentDetails.Zip = objbill.BillingZipCode;
                        objPyamentDetails.Cvv = objbill.CVV;
                        objPyamentDetails.CardNumber = objbill.CardNumber;
                        objPyamentDetails.expdate = Convert.ToString(DateTime.ParseExact(objbill.ExpirationMonth, "MMMM", CultureInfo.CurrentCulture).Month + "/" + objbill.ExpirationYear);
                        objPyamentDetails.payInterval = objbill.BillPayInterval;
                        objPyamentDetails.Amount = Convert.ToString(objbill.BillingAmount);
                        objPyamentDetails.Userid = Convert.ToString(UserId);
                        bool IsPlanPrice = true;
                        Models.RecurringSubscription objSubs = new Models.RecurringSubscription();
                        string strResult = objSubs.MakePayment(objPyamentDetails, IsPlanPrice, Request.Url.AbsoluteUri);
                        #endregion
                        if (strResult == "Done")
                        {
                            isResult = true;
                            result.Add(true, "Done");//0 = User move to temp table to Member table and send activation mail.                        
                        }
                        else
                        {                            
                            result.Add(false, "Not Done");//0 = User move to temp table to Member table and send activation mail.                        
                        }
                    }
                    else
                    {
                        string FullName = string.Empty; string EnccPassword = string.Empty;
                        int AllowLogin = Convert.ToInt32(dtCheckEmail.Rows[0]["Status"]);
                        if (AllowLogin == 1)
                        {
                            #region Send Password to user if already in system
                            // send password template
                            FullName = objCommon.CheckNull(Convert.ToString(dtCheckEmail.Rows[0]["FirstName"]), string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dtCheckEmail.Rows[0]["LastName"]), string.Empty);
                            EnccPassword = objCommon.CheckNull(Convert.ToString(dtCheckEmail.Rows[0]["Password"]), string.Empty);
                            ObjTemplate.SendingForgetPassword(FullName, ObjPlanAndPrice.Signup.Email, EnccPassword);
                            result.Add(false, "You are already signed up, Please active your account.");
                            #endregion
                            isResult = false;
                        }
                        else
                        {
                            if (AllowLogin == 2)//If Email is De Actvie by Admin then send this link as below
                            {
                                string companywebsite = "";
                                if (Session["CompanyWebsite"] != null)
                                {
                                    companywebsite = Session["CompanyWebsite"].ToString();
                                    //companywebsite = "http://localhost:2298/";
                                }
                                FullName = objCommon.CheckNull(Convert.ToString(dtCheckEmail.Rows[0]["FirstName"]), string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dtCheckEmail.Rows[0]["LastName"]), string.Empty);
                                EnccPassword = objCommon.CheckNull(Convert.ToString(dtCheckEmail.Rows[0]["Password"]), string.Empty);
                                ObjTemplate.NewUserEmailFormat(ObjPlanAndPrice.Signup.Email, companywebsite + "/User/Index?UserName=" + ObjPlanAndPrice.Signup.Email + "&Password=" + EnccPassword + "&Status=2", ObjTripleDESCryptoHelper.decryptText(EnccPassword), companywebsite);

                            }
                            isResult = false;
                            result.Add(false, "You are already signed up, Please active your account.");
                        }
                    }
                }
                else
                {
                    //Add Signup Code
                    int UserId = 0;
                    string companywebsite = "";
                    if (Session["CompanyWebsite"] != null)
                    {
                        companywebsite = Session["CompanyWebsite"].ToString();
                        //companywebsite = "http://localhost:2298/";
                    }
                    //RM-304 Changes (Pasing Pass instead of encpass at NewUserEmailFormet)
                    string Password = objCommon.CreateRandomPassword(8);
                    string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                    string Accountkey = Crypto.SHA1(Convert.ToString(DateTime.Now.Ticks));
                    UserId = ObjColleaguesData.SingUpDoctor(ObjPlanAndPrice.Signup.Email, EncPassword, ObjPlanAndPrice.Signup.FirstName, ObjPlanAndPrice.Signup.LastName, null, null, null, Accountkey, null, null, null, null, null, null, ObjPlanAndPrice.Signup.Phone, null, true, null, null, null, null, null, null, null, null, null, null, null, null, null, 0, null, null, null, 0);
                    ObjTemplate.NewUserEmailFormat(ObjPlanAndPrice.Signup.Email, companywebsite + "/User/Index?UserName=" + ObjPlanAndPrice.Signup.Email + "&Password=" + Password + "&Status=2", ObjTripleDESCryptoHelper.decryptText(EncPassword), companywebsite);
                    
                    #region Payment code
                    BillingInfo objbill = ObjPlanAndPrice.ObjBillingInfo;
                    Members objmem = ObjPlanAndPrice.Signup;

                    PyamentDetails objPyamentDetails = new PyamentDetails();
                    objPyamentDetails.Planes = Convert.ToString(ObjPlanAndPrice.MembershipId);
                    objPyamentDetails.Firstname = objmem.FirstName;
                    objPyamentDetails.Lastname = objmem.LastName;
                    objPyamentDetails.Email = objmem.Email;
                    objPyamentDetails.Phone = objmem.Phone;
                    objPyamentDetails.Zip = objbill.BillingZipCode;
                    objPyamentDetails.Cvv = objbill.CVV;
                    objPyamentDetails.CardNumber = objbill.CardNumber;
                    objPyamentDetails.expdate = Convert.ToString(DateTime.ParseExact(objbill.ExpirationMonth, "MMMM", CultureInfo.CurrentCulture).Month + "/" + objbill.ExpirationYear);
                    objPyamentDetails.payInterval = objbill.BillPayInterval;
                    objPyamentDetails.Amount = Convert.ToString(objbill.BillingAmount);
                    objPyamentDetails.Userid = Convert.ToString(UserId);
                    bool IsPlanPrice = true;
                    Models.RecurringSubscription objSubs = new Models.RecurringSubscription();
                    string strResult = objSubs.MakePayment(objPyamentDetails, IsPlanPrice, Request.Url.AbsoluteUri);
                    #endregion

                    Person person = GetSocialMediaPerson(objmem.Email); //Get Social Media detail


                    string SocialMedia = "";
                    string NameforTemplate = "";
                    string PhoneForTemplate = "";

                    string fullName = objmem.FirstName + ' ' + objmem.LastName;

                    string firstname = objmem.FirstName;
                    string lastname = objmem.LastName;
                    person.FirstName = firstname;
                    person.LastName = lastname;

                    if (!string.IsNullOrEmpty(objmem.Phone))
                    {
                        PhoneForTemplate = "Phone: " + objmem.Phone;
                    }
                    ObjTemplate.SignUpEMail(objmem.Email, fullName, objPyamentDetails.Phone, person.FacebookURL, person.TwitterURL, person.LinkedInURL);
                    if (strResult == "Done")
                    {
                        isResult = true;
                        result.Add(true, "Done");
                    }
                    else
                    {
                        isResult = false;                        
                        result.Add(false, "Not Done");
                    }                    
                }
            }
            else
            {
                isResult = false;
                result.Add(false, "The model state is not valid");
            }            
            return Json(isResult, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PackageSection(string id)
        {
            Membership objMembership = new Membership();
            ViewBag.IsError = false;
            Member mdlmember = new Member();
            var lstMembership = mdlmember.GetMembershipById(Convert.ToInt32(id));
            foreach (var item in lstMembership)
            {
                objMembership.AnnualPrice = item.AnnualPrice;
                objMembership.HalfYearlyPrice = item.HalfYearlyPrice;
                objMembership.Membership_Id = item.Membership_Id;
                objMembership.MonthlyPrice = item.MonthlyPrice;
                objMembership.QuaterlyPrice = item.QuaterlyPrice;
                objMembership.Title = item.Title;
            }            
            return View("PackageSection", objMembership);
        }

        protected Person GetSocialMediaPerson(string email)
        {
            try
            {
                //connect to fliptop and pull the information down
                SocialMedia sm = new SocialMedia();
                Person person = sm.LookUpPersonClearBit(email);
                Person fullperson = sm.LookUpPersonfullcontact(email);
                person.FirstName = !string.IsNullOrEmpty(person.FirstName) ? person.FirstName : fullperson.FirstName;
                person.LastName = !string.IsNullOrEmpty(person.LastName) ? person.LastName : fullperson.LastName;
                person.FullName = !string.IsNullOrEmpty(person.FullName) ? person.FullName : fullperson.FullName;
                person.FacebookURL = !string.IsNullOrEmpty(person.FacebookURL) ? person.FacebookURL : fullperson.FacebookURL;
                person.LinkedInURL = !string.IsNullOrEmpty(person.LinkedInURL) ? person.LinkedInURL : fullperson.LinkedInURL;
                person.TwitterURL = !string.IsNullOrEmpty(person.TwitterURL) ? person.TwitterURL : fullperson.TwitterURL;
                person.ImageUrl = !string.IsNullOrEmpty(person.ImageUrl) ? person.ImageUrl : fullperson.ImageUrl;
                person.Description = !string.IsNullOrEmpty(fullperson.Description) ? fullperson.Description : person.Description;
                return person;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public JsonResult GetPercentageById(string id, string amt)
        {
            object data = string.Empty;
            Membership_FeatureBLL obj = new Membership_FeatureBLL();
            var AnnualAmt = obj.GetMemberShipDetails(Convert.ToInt32(id)).FirstOrDefault().AnnualPrice;
            decimal strAmount = Convert.ToDecimal(Convert.ToInt32(amt) * 12);
            if (AnnualAmt >= strAmount)
            {
                data = "0";
            }
            if (AnnualAmt < strAmount)
            {
                var leftamt = strAmount - AnnualAmt;
                var percentage = (leftamt * 100) / strAmount;
                data = Math.Round(percentage,2);
            }
            
            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetPlanDetials()
        {
            PlanAndPricingBLL getPlanAndPricing = new PlanAndPricingBLL();
            List<PlanAndPrice> lst = new List<PlanAndPrice>();
            lst = getPlanAndPricing.GetPlanDetialsForTablet();
            return View("_PlansForTablet",lst);
        }
        public ActionResult GetMembershipHeadDetials(int Id)
        {
            Membership objMembership = new Membership();
            MemberShipBLL Obj = new MemberShipBLL();
            objMembership = Obj.GetById(Id);
            return View("_PlansValues", objMembership);
        }
    }
}
