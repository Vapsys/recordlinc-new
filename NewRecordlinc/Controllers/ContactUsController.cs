﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using System.Configuration;
using NewRecordlinc.App_Start;
using BO.Models;
using DataAccessLayer.Common;
using System.Data;

namespace NewRecordlinc.Controllers
{
    public class ContactUsController : Controller
    {
       
        string SalesEmail = Convert.ToString(ConfigurationManager.AppSettings.Get("SaleEmail"));
        //Old Page
        //public ActionResult Index()
        //{
        //    //-- 6211 - Commented session checking code becuse causing problem when redirected from public profile.
        //    //if (SessionManagement.UserId != null && SessionManagement.UserId != "")
        //    //{
        //    //    return RedirectToAction("Index", "Index");
        //    //}
        //    //else
        //    //{

        //    return View();
        //    //}
        //}

        //RM-291 New Contact us Page
        public ActionResult Index()
        {
            ContactUs cuobj = new ContactUs();
            return View("NewIndex", cuobj);
            
        }


        [HttpPost]
        public JsonResult ContactUs(string FirstName, string LastName, string Email, string Phone, string Message)
        {
            string NewMessage = string.Empty;
            if (!string.IsNullOrWhiteSpace(Message))
            {
             NewMessage = "Message : " + Message;
            }
            CustomeMailPropery objCustomeMailPropery = new CustomeMailPropery();
            objCustomeMailPropery.ToMailAddress= new List<string> { ConfigurationManager.AppSettings.Get("emailoftravis"), ConfigurationManager.AppSettings.Get("MailNotificationEmail_To") };
            objCustomeMailPropery.MailTemplateName = "contact-us-request";
            string CompanyName = "RecordLinc";
            DataTable dt = new DataTable();
            dt.Columns.Add("FieldName");
            dt.Columns.Add("FieldValue");

            dt.Rows.Add("FromMail", Email);
            dt.Rows.Add("FirstName", FirstName);
            dt.Rows.Add("LastName", LastName);
            dt.Rows.Add("Phone", Phone);
            dt.Rows.Add("Message", NewMessage);
            dt.Rows.Add("CompanyName", CompanyName);
            objCustomeMailPropery.MergeTags = dt;

            SendCustomeMail.SendMail(Mailtype.ManDrill, objCustomeMailPropery, 0);
            return Json(true);
        }





    }
}
