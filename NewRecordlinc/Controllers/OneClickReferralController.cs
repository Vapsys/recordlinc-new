﻿using NewRecordlinc.Models.Patients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer.Common;
using BusinessLogicLayer;
using NewRecordlinc.App_Start;
using BO.Models;
using BO.ViewModel;
using System.Net.Mail;
using System.Text;
using DataAccessLayer.PatientsData;
using System.Configuration;
using System.Security;

namespace NewRecordlinc.Controllers
{
    public class OneClickReferralController : Controller
    {
        // GET: OneClickReferral

        mdlPatient MdlPatient = null;
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        /// <summary>
        /// Method use for get all field which is done enable from referral setting by doctor.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            string DecryptedUserId = ObjTripleDESCryptoHelper.decryptText(Request.QueryString["UserId"]);
            string Encr = Request.QueryString["SenderId"];
            Encr = Encr.Replace(" ", "+");
            string DecryptedSenderId = ObjTripleDESCryptoHelper.decryptText(Encr);
            TempData["UserId"] = Convert.ToInt32(DecryptedUserId);
            TempData["SenderId"] = Convert.ToInt32(DecryptedSenderId);
            List<NewRecordlinc.Models.Patients.mdlPatient.SpecialityService> spList = new List<NewRecordlinc.Models.Patients.mdlPatient.SpecialityService>();
            spList = GetVisibleSpeciality(Convert.ToInt32(DecryptedUserId));
            PatientDentistReferral PRObj = new PatientDentistReferral();
            PRObj = ReferralFormBLL.GetPatientReferralFormField(Convert.ToInt32(TempData["UserId"]));
            TempData.Keep();
            var obj = new Tuple<List<mdlPatient.SpecialityService>, PatientDentistReferral>(spList, PRObj);
            return View(obj);
        }
        /// <summary>
        /// Method use for get speciallity for specific user.
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public List<mdlPatient.SpecialityService> GetVisibleSpeciality(int UserId)
        {
            MdlPatient = new mdlPatient();
            List<mdlPatient.SpecialityService> spList = new List<mdlPatient.SpecialityService>();
            spList = MdlPatient.GetVisibleSpeciality(UserId);
            return spList;
        }
        /// <summary>
        /// Method use for save patient referral data from one click referral.
        /// </summary>
        /// <param name="mdlpatientsignup"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SubmitPatientDetail(PatientDentist_Referral mdlpatientsignup)
        {
            clsCommon objCommon = new clsCommon();
            clsTemplate template = new clsTemplate();
            StringBuilder sb = new StringBuilder();
            try
            {
                int NewPatientId = 0;
                DentistProfileBLL objdentistprofile = new DentistProfileBLL();
                mdlpatientsignup.UserId = Convert.ToInt32(TempData["UserId"]);
                TempData.Keep();
                mdlpatientsignup.PatientId = NewPatientId;
               // NewPatientId = DentistProfileBLL.AddNewPatient(mdlpatientsignup);
                mdlpatientsignup.PatientId = NewPatientId;
                TempData["PatientID"] = NewPatientId;
                string DoctorName = clsPatientsData.GetDoctorNameFormId(mdlpatientsignup.UserId);
                //Insert address of patient
                //mdlPatientData.UserId = Convert.ToInt32(TempData["UserId"]);
                //TempData.Keep();
                //bool Result = objdentistprofile.AddPatientContactInfoFromOneclick(mdlpatientsignup);

                //Insert Insurance Coverage Data
                int i = objdentistprofile.AddInsuranceCoverage(mdlpatientsignup, Convert.ToInt32(NewPatientId));
                //Insert History Data
                objdentistprofile.AddMediacalHistory(mdlpatientsignup, Convert.ToInt32(NewPatientId));
                //Insert Dental History Data
                objdentistprofile.AddDentalHistory(mdlpatientsignup, Convert.ToInt32(NewPatientId));

                string EncrPatientId = ObjTripleDESCryptoHelper.encryptText(Convert.ToString(NewPatientId));
                string EnUserId = ObjTripleDESCryptoHelper.encryptText(Convert.ToString(mdlpatientsignup.UserId));
                string EncrptedString = EncrPatientId + '|' + EnUserId;
                string Urllink = ConfigurationManager.AppSettings["OneClickreferralURL"] + "AccountSettings/GetPatientReferralForm?TYHJNABGA=" + EncrptedString;
                //bool res = objdentistprofile.InsertReferralMessage("Refferal sent From one click refferal", "Refferal sent From one click refferal", 2, Convert.ToInt32(TempData["SenderId"]), Convert.ToInt32(NewPatientId), mdlpatientsignup.UserId, true);
                //bool res = objdentistprofile.InsertReferral("Refferal sent From one click refferal", "Refferal sent From one click refferal", 2, Convert.ToInt32(TempData["UserId"]), Convert.ToInt32(NewPatientId), 0, Convert.ToInt32(mdlpatientsignup.UserId), true, XMLstring);

                template.SendMailToPatientForRefferalSettings(mdlpatientsignup.txtEmail, DoctorName, Urllink,0);
                ModelState.Clear();

                return Json(NewPatientId, JsonRequestBehavior.AllowGet);
            }
            catch (Exception Ex)
        {
                objCommon.InsertErrorLog(Request.RawUrl, Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        
        [HttpPost]
        public ActionResult SubmitReferral(FormCollection frmc)
        {
            string userId = Convert.ToString(frmc.Get("hdnuserId"));
            string SenderId = Convert.ToString(frmc.Get("hdnsenderId"));
            string DecryptedUserId = ObjTripleDESCryptoHelper.decryptText(userId);
            string Encr = SenderId;
            Encr = Encr.Replace(" ", "+");
            string DecryptedSenderId = ObjTripleDESCryptoHelper.decryptText(Encr);
            string TextValue = string.Empty;
            string XMLstring = "<NewDataSet>";
            DentistProfileBLL objDentistProfile = new DentistProfileBLL();
            clsTemplate template = new clsTemplate();
            PatientDentist_Referral mdlpatientsignup = new PatientDentist_Referral();
            int NewPatientId = 0;
           // NewPatientId = objDentistProfile.FormMethodPostAddNewPatient(frmc, Convert.ToInt32(DecryptedUserId));
            TempData["PatientID"] = NewPatientId;

            string DoctorName = clsPatientsData.GetDoctorNameFormId(Convert.ToInt32(DecryptedUserId));
            // bool Result = objDentistProfile.FormMethodPostAddPatientContactInfoFromOneclick(frmc, NewPatientId);
            bool Result;
            //Insert Insurance Coverage Data
            Result = objDentistProfile.FormMethodAddInsuranceCoverage(frmc, Convert.ToInt32(NewPatientId));

            //Insert History Data
            Result = objDentistProfile.FormMethodAddMediacalHistory(frmc, Convert.ToInt32(NewPatientId));

            //Insert Dental History Data
            Result = objDentistProfile.FormMethodAddDentalHistory(frmc, Convert.ToInt32(NewPatientId));
            string EncrPatientId = ObjTripleDESCryptoHelper.encryptText(Convert.ToString(NewPatientId));
            string EnUserId = ObjTripleDESCryptoHelper.encryptText(Convert.ToString(Convert.ToInt32(DecryptedUserId)));
            string EncrptedString = EncrPatientId + '|' + EnUserId;
            string Urllink = ConfigurationManager.AppSettings["OneClickreferralURL"] + "AccountSettings/GetPatientReferralForm?TYHJNABGA=" + EncrptedString;
            template.SendMailToPatientForRefferalSettings(frmc.Get("txtEmail"), DoctorName, Urllink,0);

            foreach (var item in Request.Form.Keys)
            {
                if (!String.IsNullOrEmpty(Convert.ToString(Request.Form[item.ToString()])))
                {
                    if (item.ToString().StartsWith("sub"))
                    {
                        string[] SplitString; string CategoryID = string.Empty;
                        string SubCatId = string.Empty;
                        string SubSubCatId = string.Empty;
                        SplitString = item.ToString().Split('_');
                        CategoryID = SplitString[1];
                        SubCatId = SplitString[2];
                        if (SplitString[3] == "0")
        {
                            SplitString[3] = null;
                        }
                        SubSubCatId = ((SplitString[3] != "" && SplitString[3] != null) ? SplitString[3] : null);
                        TextValue = Convert.ToString(Request.Form[item.ToString()]);
                        XMLstring += "<Table><catId>" + SecurityElement.Escape(CategoryID) + "</catId>";
                        XMLstring += "<SubCatId>" + SecurityElement.Escape(SubCatId) + "</SubCatId>";
                        XMLstring += "<SubSubCatId>" + SecurityElement.Escape(SubSubCatId) + "</SubSubCatId>";
                        XMLstring += "<Value>" + SecurityElement.Escape(TextValue) + "</Value>";
                        XMLstring += "<ReferrelCardId>" + null + "</ReferrelCardId></Table>";
                    }
                }
        }
            XMLstring += "</NewDataSet>";
            bool res = objDentistProfile.InsertReferral("Refferal sent From one click refferal", "Refferal sent From one click refferal", 2, Convert.ToInt32(DecryptedSenderId), Convert.ToInt32(TempData["PatientID"]), null, null, 0, Convert.ToInt32(DecryptedUserId), XMLstring, true);
            TempData.Keep();
            return View();
        }
    }
}