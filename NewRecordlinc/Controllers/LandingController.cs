﻿using BO.Models;
using BusinessLogicLayer;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using DataAccessLayer.FlipTop;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace NewRecordlinc.Controllers
{
    public class LandingController : Controller
    {
        clsTemplate ObjTemplate = new clsTemplate();
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        clsCommon objCommon = new clsCommon();
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        clsAdmin objAdmin = new clsAdmin();

        // GET: Landing
        [ActionName("A-Dentists-Guide-to-Referrals")]        
        public ActionResult Index()
        {
            return View("Index");
        }        

        [ActionName("Referral-Machine-for-Dentists")]
        public ActionResult LandingPage2()
        {
            return View("LandingPage2");
        }

        [ActionName("dos-and-donts-for-online-dentist-reviews")]
        public ActionResult LandingPage3()
        {
            return View("LandingPage3");
        }

        [ActionName("Referral-Autopilot-for-dentist")]
        public ActionResult LandingPage4()
        {
            return View("LandingPage4");
        }

        [HttpPost]
        public ActionResult SendLandingEmail(string emailId, string pdfType)
        {
            object obj = string.Empty;
            string emailAddress = Convert.ToString(emailId);
            string PdfTypeLink = string.Empty;
            string strPdfType = string.Empty;
            string strPageLink = string.Empty;
            if (pdfType == Convert.ToString((int)BO.Enums.Common.Landing.DentistsGuideToReferrals))
            {
                strPdfType = "A-Dentists-Guide-to-Referrals";
                strPageLink = Convert.ToString(ConfigurationManager.AppSettings["Lanpag1"]);
                PdfTypeLink = Convert.ToString(ConfigurationManager.AppSettings["pdf1"]);
            }
            if (pdfType == Convert.ToString((int)BO.Enums.Common.Landing.DosAndDontsForDentists))
            {
                strPdfType = "dos-and-donts-for-online-dentist-reviews";
                strPageLink = Convert.ToString(ConfigurationManager.AppSettings["Lanpag2"]);
                PdfTypeLink = Convert.ToString(ConfigurationManager.AppSettings["pdf2"]);
            }
            if (pdfType == Convert.ToString((int)BO.Enums.Common.Landing.ReferralAutopilotforDentist))
            {
                strPdfType = "Referral-Autopilot-for-dentist";
                strPageLink = Convert.ToString(ConfigurationManager.AppSettings["Lanpag3"]);
                PdfTypeLink = Convert.ToString(ConfigurationManager.AppSettings["pdf3"]);
            }
            if (pdfType == Convert.ToString((int)BO.Enums.Common.Landing.ReferralMachineForDentists))
            {
                strPdfType = "Referral-Machine-for-Dentists";
                strPageLink = Convert.ToString(ConfigurationManager.AppSettings["Lanpag4"]);
                PdfTypeLink = Convert.ToString(ConfigurationManager.AppSettings["pdf4"]);
            }

            clsTemplate objclsTemplate = new clsTemplate();
            
            //send email to user for download the pdf.
            var IsMailSent = objclsTemplate.SendLandingEmailToUser(emailAddress, PdfTypeLink, strPdfType);            

            //store user information in the database.
            LandingBLL objLandingBLL = new LandingBLL();
            LandingHistory objLandingHistory = new LandingHistory();
            objLandingHistory.Email = emailAddress;
            objLandingHistory.LandingPageName = strPageLink;
            var IsResult = objLandingBLL.InsertLandingHistory(objLandingHistory);            
            if (IsMailSent || IsResult)
            {
                obj = "1";                
            }            
            return Json(obj, JsonRequestBehavior.AllowGet);
        }        


        public ActionResult ContactUs(string FullName, string Email, string Message, string CurrentUrl)
        {                       
            try
            {
                object obj = string.Empty;
                clsTemplate objclsTemplate = new clsTemplate();
                string UserMail = Email;
                var strCurrentUrl = CurrentUrl.Split('/');
                string pageUrl = Convert.ToString(strCurrentUrl[3]);                
                string SalesEmail = Convert.ToString(ConfigurationManager.AppSettings.Get("SaleEmail"));
                var IsMailSent = objclsTemplate.SendEmailToSalesFromLanding(UserMail, SalesEmail, FullName, Message, pageUrl);
                if (IsMailSent)
                {
                    obj = "1";                   
                }
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SignupFromLanding(string email)
        {
            try
            {
                object obj = string.Empty;

                //Call Socialmedia API            
                CommonBLL objCommonBLL = new CommonBLL();
                Person person = objCommonBLL.GetSocialMediaPerson(email);
                DataTable dtCheckEmail = new DataTable();
                dtCheckEmail = ObjColleaguesData.CheckEmailInSystem(email);


                if (dtCheckEmail.Rows.Count > 0)
                {
                    // send template for forgot password

                    DataTable dtCheck = ObjColleaguesData.CheckEmailExistsAsDoctor(email);
                    if (dtCheck != null && dtCheck.Rows.Count > 0)
                    {
                        string FullName = string.Empty; string EncPassword = string.Empty;
                        int AllowLogin = Convert.ToInt32(dtCheckEmail.Rows[0]["Status"]);
                        if (AllowLogin == 1)
                        {
                            #region Send Password to user if already in system
                            // send password tempalte
                            FullName = objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["FirstName"]), string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["LastName"]), string.Empty);
                            EncPassword = objCommon.CheckNull(Convert.ToString(dtCheckEmail.Rows[0]["Password"]), string.Empty);
                            ObjTemplate.SendingForgetPassword(FullName, email, EncPassword);
                            #endregion
                            obj = "4";
                        }
                        else
                        {
                            if (AllowLogin == 2)//If Email is De Active by Admin then send this link as below
                            {
                                string companywebsite = "";
                                if (Session["CompanyWebsite"] != null)
                                {
                                    companywebsite = Session["CompanyWebsite"].ToString();
                                    //companywebsite = "http://localhost:2298/";
                                }
                                FullName = objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["FirstName"]), string.Empty) + " " + objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["LastName"]), string.Empty);
                                EncPassword = objCommon.CheckNull(Convert.ToString(dtCheck.Rows[0]["Password"]), string.Empty);
                                ObjTemplate.NewUserEmailFormat(email, companywebsite + "/User/Index?UserName=" + email + "&Password=" + EncPassword + "&Status=2", ObjTripleDESCryptoHelper.decryptText(EncPassword), companywebsite);
                                obj = "3";
                            }
                        }

                    }
                    else
                    {
                        #region Re Send sign up tempalte to user of temp table
                        DataTable dttemp = new DataTable();
                        string EncPassword = string.Empty;
                        string companywebsite = "";
                        if (Session["CompanyWebsite"] != null)
                        {
                            companywebsite = Session["CompanyWebsite"].ToString();
                        }
                        dttemp = objAdmin.GetTempSignupByEmail(email);
                        if (dttemp.Rows.Count > 0)
                        {
                            EncPassword = objCommon.CheckNull(Convert.ToString(dttemp.Rows[0]["password"]), string.Empty);
                            ObjTemplate.NewUserEmailFormat(email, companywebsite + "/User/Index?UserName=" + email + "&Password=" + EncPassword, ObjTripleDESCryptoHelper.decryptText(EncPassword), companywebsite); // send tempate to admin if doctor sing up in system
                            obj = "2";
                        }                       
                        #endregion

                    }

                }
                else
                {
                    #region Doctor sign up in system
                    string Password = objCommon.CreateRandomPassword(8);
                    string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                    string Accountkey = Crypto.SHA1(Convert.ToString(DateTime.Now.Ticks));
                    //Doctor sign up                    

                    string fullName = person.FullName;
                    string firstname = person.FirstName;
                    string lastname = person.LastName;
                    string phone = string.Empty;                    

                    string companywebsite = "";
                    if (Session["CompanyWebsite"] != null)
                    {
                        companywebsite = Session["CompanyWebsite"].ToString();
                    }

                    int NewUserId = 0;
                    // Here we put code for sign up doctor in sytem or Temp table

                    NewUserId = ObjColleaguesData.Insert_Temp_Signup(email, EncPassword, false, person.ImageUrl, person.FirstName, person.LastName, person.Age, person.Gender, person.Location, person.Company, person.Title, person.FacebookURL, person.TwitterURL, person.LinkedInURL, person.InfluenceScore, phone, person.Description);

                    if (NewUserId != 0)
                    {
                        ObjTemplate.NewUserEmailFormat(email, companywebsite + "/User/Index?UserName=" + email + "&Password=" + EncPassword, Password, companywebsite); // send tempate to admin if doctor sing up in system

                        ObjTemplate.SignUpEMail(email, fullName, phone, person.FacebookURL, person.TwitterURL, person.LinkedInURL);// send tempate to admin if doctor sing up in system
                        obj = "1";
                    }                    
                    #endregion

                }
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex) 
            {
                return Json(ex.ToString(), JsonRequestBehavior.AllowGet);
            }                       
        }

    }
}