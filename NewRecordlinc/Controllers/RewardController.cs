﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogicLayer;
using NewRecordlinc.App_Start;
using BO.ViewModel;
using BO.Models;

namespace NewRecordlinc.Controllers
{
    public class RewardController : Controller
    {
        // GET: Reward
        [CustomAuthorize]
        public ActionResult Index()
        {
            List<DentistReward> lst = new List<DentistReward>();
            List<SelectListItem> CodeList = RewardBLL.GetDentistRewardCodeList();
            ViewBag.Codelist = CodeList;
            lst = RewardBLL.GetDentistRewardSettingDetails(Convert.ToInt32(SessionManagement.UserId));
            return View(lst);
        }

        /// <summary>
        /// This Method Used for Update Reward of Procedure Code.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult UpdateRewardList(List<DentistReward> lstreward)
        {
            bool result = false;
            result = RewardBLL.UpdateRewardListOfUser(lstreward, Convert.ToInt32(SessionManagement.UserId));
            result = (result == true) ? true : false;
            return Json(result);
        }

        /// <summary>
        /// This Method Delete Point Multiplier detail in Database
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public ActionResult InsertRewardPointForProcedureCode(BO.ViewModel.ProcedureCode rc)
        {
            rc.AccountId = Convert.ToInt32(SessionManagement.UserId);
            if (rc.ProcedureCodeId == 0)
                rc.CreatedBy = Convert.ToInt32(SessionManagement.UserId);
            else
                rc.ModifiedBy = Convert.ToInt32(SessionManagement.UserId);
            string res = "";
            res = RewardBLL.InsertRewardPointForProcedureCode(rc);
            return Json(res);
        }

        /// <summary>
        /// This Method Delete Point Multiplier detail in Database
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public PartialViewResult AddEditRewardCode(ProcedureCodes rc)
        {
            ProcedureCodes pc = new ProcedureCodes();
            pc = RewardBLL.AddEditRewardCode(rc);
            return PartialView("_PartialEditRewardCode", pc);
        }

        /// <summary>
        /// This Method Delete Point Multiplier detail in Database
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public ActionResult InsertProcedureCodesForExams(ProcedureExamCode ecobj)
        {
            ecobj.ExamId = 2;
            ecobj.AccountId = 36390;
            ecobj.ProcedureName = "Hygene";
            ecobj.ExamCode = 121;
            ecobj.CreatedBy = 36390;
            ecobj.ModifiedBy = 36390;
            bool res = false;
            res = RewardBLL.InsertProcedureCodesForExams(ecobj);
            return Json(res);
        }

        /// <summary>
        /// This Method Delete Point Multiplier detail in Database
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public ActionResult InsertPointMultiplierForPointBalance(PointBalance pbobj)
        {
            pbobj.PointId = 1;
            pbobj.AccountId = 36390;
            pbobj.PointLevel = 1000;
            pbobj.Multiplier = 1.5m;
            pbobj.CreatedBy = 36390;
            pbobj.ModifiedBy = 36390;
            bool res = false;
            res = RewardBLL.InsertPointMultiplierForPointBalance(pbobj);
            return Json(res);
        }

        /// <summary>
        /// This Method Delete Point Multiplier detail in Database
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public ActionResult DeleteRewardPointForProcedureCode(ProcedureCodes rc)
        {
            //rc.ProcedureCodeId = 1;
            bool res = false;
            res = RewardBLL.DeleteRewardPointForProcedureCode(rc);
            return Json(res);
        }

        /// <summary>
        /// This Method Delete Point Multiplier detail in Database
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public ActionResult DeleteProcedureCodesForExams(ProcedureExamCode ecobj)
        {
            //ecobj.ExamId = 1;
            bool res = false;
            res = RewardBLL.DeleteProcedureCodesForExams(ecobj);
            return Json(res);
        }

        public ActionResult DeleteRewardMultiplierForLongevity(int LMId)
        {
            //rlobj.LMId = 1;
            string res = "";
            res = RewardBLL.DeleteRewardMultiplierForLongevity(LMId);
            return Json(res);
        }

        /// <summary>
        /// This Method Delete Point Multiplier detail in Database
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public ActionResult DeletePointMultiplierForPointBalance(PointBalance pbobj)
        {
            //pbobj.PointId = 1;
            bool res = false;
            res = RewardBLL.DeletePointMultiplierForPointBalance(pbobj);
            return Json(res);
        }

        /// <summary>
        /// This Method Get All Procedure detail from Database
        /// </summary>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult ManageSuperBucks()
        {
            int UserId = Convert.ToInt32(SessionManagement.UserId);
            string result = CommonBLL.GetPointsAndExamExists(UserId);
            if (result == "11")
            {
                TempData["script"] = "Redirect";
                return RedirectToAction("Integration", "AccountSettings");

            }
                 
            else
            {
                List<ProcedureCode> Proc = new List<ProcedureCode>();
                Proc = RewardBLL.GetProcedureCode(UserId);
                return View(Proc);
            }
        }

        /// <summary>
        /// This Method Get All Plan detail  from Database in Edit time.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public PartialViewResult OpenPopup(SuperPlan sp)
        {
            SuperPlan spGet = new SuperPlan();
            spGet = RewardBLL.AddEditSuperPlan(sp);
            return PartialView("_PartialAddEditSuperPlan", spGet);
        }

        /// <summary>
        /// This Method insert Plan detail in Database
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public ActionResult AddEditSuperPlan(SuperPlan sp)
        {
            string res = "";
            sp.UserId = Convert.ToInt32(SessionManagement.UserId);
            sp.CreatedBy = Convert.ToInt32(SessionManagement.UserId);
            res = RewardBLL.AddSuperPlan(sp);
            return Json(res);
        }

        /// <summary>
        /// This Method Get Plan detail from Database
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult SuperPlan()
        {
            List<SuperPlan> Proc = new List<SuperPlan>();
            int UserID = Convert.ToInt32(SessionManagement.UserId);
            Proc = RewardBLL.GetSuperPlan(UserID);
            return View(Proc);
        }

        /// <summary>
        /// This Method Delete Plan detail from Database
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult DeleteSuperPlan(SuperPlan sp)
        {
            string res;
            sp.UserId = Convert.ToInt32(SessionManagement.UserId);
            sp.CreatedBy = Convert.ToInt32(SessionManagement.UserId);
            res = RewardBLL.DeleteSuperPlan(sp);
            return Json(res);
        }

        /// <summary>
        /// This Method Get Procedure Plan Details For Mapping form database .
        /// </summary>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult MapProcedurePlan()
        {
            int PlanId = 0;
            if (TempData["PlanId"] != null)
            {
                PlanId = Convert.ToInt32(TempData["PlanId"]);
                ViewBag.PlanID = PlanId;
            }
            else
            {
                PlanId = 0;
                ViewBag.PlanID = 1;
            }
            int UserId = 0;
            UserId = Convert.ToInt32(SessionManagement.UserId);
            BindDropdownList();

            List<ProcedureCode> Proc = new List<ProcedureCode>();
            int UserID = Convert.ToInt32(SessionManagement.UserId);
             
            Proc = RewardBLL.GetProcedureCodeData(UserID, PlanId);
             
            return View("ProcedureExamAssociation", Proc);
        }

        /// <summary>
        /// This Method Insert Map Procedure Plan on DB
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [HttpPost]
        [CustomAuthorize]
        public JsonResult InsertProcedureExamAssociation(List<ProcedureCode> ProcedureList)
        {
            string res = "";
            res = RewardBLL.InsertProcedureExamAssociation(ProcedureList, Convert.ToInt32(SessionManagement.UserId));
            return Json(res);
        }

        /// <summary>
        /// This Method use for delete data from Procedure Plan from DB
        /// </summary>
        /// <param name="ProcedureCodeId,PlanId"></param>
        /// <returns></returns>
        public JsonResult DeleteProcedure(int ProcedureCodeId,int PlanId)
        {
            string res = "";
            res = RewardBLL.DeleteProcedureForPlan(ProcedureCodeId,PlanId);
            return Json(res);
        }

        
        /// <summary>
        /// This Method Insert Procedure into Map Procedure Plan on DB
        /// </summary>
        /// <param name="Obj,PlanId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult InsertProcedure(List<ProcedureCode> ProcedureList,int PlanId)
        {
            string res = "";
            res = RewardBLL.InsertProcedureForPlan(ProcedureList, Convert.ToInt32(SessionManagement.UserId), PlanId);
            return Json(res);
        }


        /// <summary>
        /// This Method Get Procedure Detail by Plan Or Exam DB
        /// </summary>
        /// <param name="PlanId,isExam"></param>
        /// <returns></returns>
        public ActionResult GetSelectedPlanData(int PlanId,int isExam)
        {

            int UserId = 0;
            UserId = Convert.ToInt32(SessionManagement.UserId);
            BindDropdownList();

            List<ProcedureCode> Proc = new List<ProcedureCode>();
            int UserID = Convert.ToInt32(SessionManagement.UserId);

            Proc = RewardBLL.GetProcedureCodeData(UserID, PlanId);
            if (isExam != 2)
                Proc = Proc.Where(p => p.IsExam == Convert.ToBoolean(isExam)).ToList();
            return View("_PartialProcedureData", Proc);
        }

        /// <summary>
        /// This Method Get LongevityMultiplier Detail by Plan DB
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult LongevityMultiplier()
        {
            List<SuperPlan> Splist = new List<BO.ViewModel.SuperPlan>();
            Splist = RewardBLL.GetSuperPlan(Convert.ToInt32(SessionManagement.UserId));
            ViewBag.PlanList = Splist;
            int PlanId = 0;
            if (TempData["PlanId"]!=null)
            {
                PlanId = Convert.ToInt32(TempData["PlanId"]);
                ViewBag.PlanId = PlanId;
            }
            else
            {
                PlanId = 0;
                ViewBag.PlanId = 1;
            }
            BindDropdownList();
            List<RewardsLongevity> Proc = new List<RewardsLongevity>();
            int UserId = Convert.ToInt32(SessionManagement.UserId);
            Proc = RewardBLL.Get_LongevityMultiplier(UserId, PlanId);
            
            return View(Proc);
        }

        /// <summary>
        /// This Method Get Longevity Multiplier Details form database for Edit time.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [CustomAuthorize]
        public PartialViewResult OpenLongitivtyPopUp(RewardsLongevity RL)
        {
            List<SuperPlan> Splist = new List<BO.ViewModel.SuperPlan>();
            Splist = RewardBLL.GetSuperPlan(Convert.ToInt32(SessionManagement.UserId));
            ViewBag.PlanList = Splist;

            RewardsLongevity RLGet = new RewardsLongevity();
            RL.AccountId = Convert.ToInt32(SessionManagement.UserId);
            RLGet = RewardBLL.GetLongitivtyPopUp(RL);
            return PartialView("_PartialAddEditLongevityMultiplier", RLGet);
        }

        /// <summary>
        /// This Method Insert Longevity Multiplier Details in database.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult InsertRewardMultiplierForLongevity(RewardLongevity rlobj)
        {
            //List<SuperPlan> Splist = new List<BO.ViewModel.SuperPlan>();
            //Splist = RewardBLL.GetSuperPlan(Convert.ToInt32(SessionManagement.UserId));
            //ViewBag.PlanList = Splist;
            string res = "";
            rlobj.AccountId = Convert.ToInt32(SessionManagement.UserId);
            if (rlobj.LMId > 0)
                rlobj.ModifiedBy = Convert.ToInt32(SessionManagement.UserId);
            else
                rlobj.CreatedBy = Convert.ToInt32(SessionManagement.UserId);
            res = RewardBLL.InsertRewardMultiplierForLongevity(rlobj);
            return Json(res);
        }

        /// <summary>
        /// This Method Get Longevity Multiplier Details by Plan from database.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult GetSelectedData(int PlanId)
        {
            List<RewardsLongevity> Proc = new List<RewardsLongevity>();
            int UserId = Convert.ToInt32(SessionManagement.UserId);
            Proc = RewardBLL.Get_LongevityMultiplier(UserId, PlanId);
            return View("_PartialLongevityMultiplier", Proc);
        }

        /// <summary>
        /// This Method Get All Procedure Details by Plan from database.
        /// </summary>
        /// <param name="PlanId"></param>
        /// <returns></returns>
        [CustomAuthorize]
        public PartialViewResult OpenProcedurePopUp(int PlanId)
        {
            List<ProcedureCode> Proc = new List<ProcedureCode>();
            int UserId = Convert.ToInt32(SessionManagement.UserId);
             
            Proc = RewardBLL.GetAllProcedure(UserId, PlanId);
            return PartialView("_PartialAddProcedureForPlan", Proc);
        }


        [CustomAuthorize]
        public ActionResult GetProcedureWithPlan()
        {
            int PlanId = 0;
            if (TempData["PlanId"] != null)
            {
                PlanId = Convert.ToInt32(TempData["PlanId"]);
            }

            int UserId = 0;
            UserId = Convert.ToInt32(SessionManagement.UserId);
            BindDropdownList();

            List<ProcedureCode> Proc = new List<ProcedureCode>();
            int UserID = Convert.ToInt32(SessionManagement.UserId);

            Proc = RewardBLL.GetProcedureCodeData(UserID, PlanId);
            return View("ProcedureExamAssociation", Proc);
        }

        /// <summary>
        /// This Method Get Procedure plan Details by Plan from database.
        /// </summary>
        /// <param name="PlanId"></param>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult PlanProcedure(int PlanId)
        {
            TempData["PlanId"] = PlanId;
            return RedirectToAction("MapProcedurePlan");
        }

        /// <summary>
        /// This Method Get Longevity Details by Plan from database.
        /// </summary>
        /// <param name="PlanId"></param>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult PlanLongevity(int PlanId)
        {
            TempData["PlanId"] = PlanId;
            return RedirectToAction("LongevityMultiplier");
        }

        /// <summary>
        /// This Method Bind Dropdown for Plan from database.
        /// </summary>
        /// <param name="PlanId"></param>
        /// <returns></returns>
        [CustomAuthorize]
        public void BindDropdownList()
        {
            List<SuperPlan> Splist = new List<BO.ViewModel.SuperPlan>();
            List<SelectListItem> items = new List<SelectListItem>();
            int UserId = 0;
            UserId = Convert.ToInt32(SessionManagement.UserId);
            Splist = RewardBLL.GetSuperPlan(UserId);
            ViewBag.PlanList = Splist;
            items.Add(new SelectListItem
            { Text = "All", Value = "2" });
            items.Add(new SelectListItem
            { Text = "Exam Code", Value = "1" });
            items.Add(new SelectListItem
            { Text = "Only Procedure", Value = "0" });
            ViewBag.items = items;
        }

        /// <summary>
        /// This Method Get List of Point Multiplier of Users.
        /// </summary>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult PointMultiplier()
        {
            List<PointsBalances> Proc = new List<PointsBalances>();
            int UserId = Convert.ToInt32(SessionManagement.UserId);
            int PlanId = 0;
            if (TempData["PlanId"] != null)
            {
                PlanId = Convert.ToInt32(TempData["PlanId"]);
                ViewBag.PlanID = PlanId;
            }
            else
            {
                PlanId = 0;
                ViewBag.PlanID = 1;
            }
            BindDropdownList();
            Proc = RewardBLL.GetPointMultiplier(UserId, PlanId);
            return View(Proc);
        }

        /// <summary>
        /// This Method Get Point Multiplier Details form database for Edit time.
        /// </summary>
        /// <param name="PointId"></param>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult GetPointMultiplier(int PointId)
        {
            PointsBalances ObjPoint = new PointsBalances();
            int UserId = Convert.ToInt32(SessionManagement.UserId);
            ObjPoint = RewardBLL.GetPointMuliplierDetails(PointId, UserId);
            return View("_PartialAddEditPointMultiplier", ObjPoint);
        }

        /// <summary>
        /// This Method Get Multiplier Details By Selected Plan form database .
        /// </summary>
        /// <param name="PlanId"></param>
        /// <returns></returns>
        public PartialViewResult SelectedPlanData(int PlanId)
        {

            int UserId = 0;
            UserId = Convert.ToInt32(SessionManagement.UserId);
            BindDropdownList();

            List<PointsBalances> Proc = new List<PointsBalances>();
            int UserID = Convert.ToInt32(SessionManagement.UserId);

            Proc = RewardBLL.GetPointMultiplier(UserId, PlanId);
            return PartialView("_PartialPointMultiplier", Proc);
        }

        /// <summary>
        /// This Method Insert Point Multiplier on DB
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult InsertPointMultiplier(PointsBalances Obj)
        {
            int UserId = Convert.ToInt32(SessionManagement.UserId);
            int result = RewardBLL.InsertPointMultiplier(Obj, UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// This Method is used for delete the Point Multiplier.
        /// </summary>
        /// <param name="PointId"></param>
        /// <returns></returns>
        [CustomAuthorize]
        public ActionResult DeletePointMultiplier(int PointId)
        {
            int UserId = Convert.ToInt32(SessionManagement.UserId);
            bool result = RewardBLL.DeletePointMultiplier(PointId, UserId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}