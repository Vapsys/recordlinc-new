﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewRecordlinc.Models.Colleagues;
using NewRecordlinc.Models.Patients;
using NewRecordlinc.Models;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.PatientsData;
using DataAccessLayer.Common;
using System.Data;
using System.Text;
using NewRecordlinc.Models.Common;
using System.Configuration;
using NewRecordlinc.App_Start;
using System.Text.RegularExpressions;
using DataAccessLayer.Appointment;
using System.IO;
using BO.Models;
using BusinessLogicLayer;

namespace NewRecordlinc.Controllers
{
    public class DashboardController : Controller
    {

        Member MdlMember = null;
        clsCommon objCommon = new clsCommon();
        clsCompany ObjCompany = new clsCompany();
        clsTemplate ObjTemplate = new clsTemplate();
        clsAdmin ObjAdmin = new clsAdmin();
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        mdlColleagues MdlColleagues = null;
        mdlPatient MdlPatient = null;
        mdlCommon MdlCommon = null;
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        string ImageName = "";

        //public ActionResult dashboard(string UserId)
        //{
        //    TempData.Keep("PMSReferralError");

        //    if (SessionManagement.UserId != null && SessionManagement.UserId != "")
        //    {
        //        bool Result = objCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
        //        DataTable dtCheckFirst = new DataTable();
        //        if(Convert.ToBoolean(TempData["IsFrom"]))
        //        {

        //        }
        //        dtCheckFirst = ObjColleaguesData.GetFirstLoginCount(Convert.ToInt32(SessionManagement.UserId));
        //        //RM-119 Number of received emails from support representative increasing everytime newly created doctor changes tab in Dashboard.
        //        //if (dtCheckFirst.Rows.Count == 1)
        //        //{
        //        //    ObjTemplate.Firstlimeloginsupportmail(Convert.ToInt32(SessionManagement.UserId));
        //        //}
        //        if (dtCheckFirst.Rows.Count > 0)
        //        {
        //            MdlPatient = new mdlPatient();
        //            MdlMember = new Member();
        //            MdlColleagues = new mdlColleagues();
        //            DataSet dsDash = new DataSet();
        //            dsDash = MdlMember.GetProfileDetailsOfDoctorByID(Convert.ToInt32(Session["UserId"]));
        //            if (dsDash.Tables != null && dsDash.Tables.Count > 0)
        //            {
        //                MdlMember.UserId = Convert.ToInt32(dsDash.Tables[0].Rows[0]["UserId"]);
        //                MdlMember.FirstName = objCommon.CheckNull(Convert.ToString(dsDash.Tables[0].Rows[0]["FirstName"]), string.Empty);
        //                MdlMember.LastName = objCommon.CheckNull(Convert.ToString(dsDash.Tables[0].Rows[0]["LastName"]), string.Empty);
        //                MdlMember.Description = objCommon.CheckNull(Convert.ToString(dsDash.Tables[0].Rows[0]["Description"]), string.Empty);
        //                MdlMember.Title = objCommon.CheckNull(Convert.ToString(dsDash.Tables[0].Rows[0]["Title"]), string.Empty);
        //                MdlMember.MiddleName = objCommon.CheckNull(Convert.ToString(dsDash.Tables[0].Rows[0]["MiddleName"]), string.Empty);
        //                ImageName = objCommon.CheckNull(Convert.ToString(dsDash.Tables[0].Rows[0]["ImageName"]), string.Empty);

        //                if (string.IsNullOrEmpty(ImageName))
        //                {
        //                    MdlMember.ImageName = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty);
        //                }
        //                else
        //                {
        //                    MdlMember.ImageName = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + ImageName;
        //                }

        //                if (dsDash.Tables[1].Rows.Count > 0)
        //                {
        //                    MdlMember.OfficeName = objCommon.CheckNull(Convert.ToString(dsDash.Tables[1].Rows[0]["AccountName"]), string.Empty);
        //                }
        //                if (dsDash.Tables[4].Rows.Count > 0)
        //                {
        //                    MdlMember.Institute = objCommon.CheckNull(Convert.ToString(dsDash.Tables[4].Rows[0]["Institute"]), string.Empty);
        //                }
        //                if (dsDash.Tables[5].Rows.Count > 0)
        //                {
        //                    MdlMember.MemberShip = objCommon.CheckNull(Convert.ToString(dsDash.Tables[5].Rows[0]["Membership"]), string.Empty);
        //                }
        //            }
        //            //  MdlMember.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), 1, 15, null, 11, 2, null, 0);
        //            ViewBag.PatientCount = MdlPatient.GetPatientCountOfDoctor(Convert.ToInt32(Session["UserId"]));

        //            MdlMember.lstSpeacilitiesOfDoctor = MdlMember.GetSpeacilitiesOfDoctorById(Convert.ToInt32(Session["UserId"]));

        //            //   ViewBag.peopleyoumayknow = GetPeopleYouMayKnow(Convert.ToInt32(Session["UserId"]), 1);
        //            //   ViewBag.PatientListForSendReferralPopUp = GetPatientListForSendReferralPopUp(null);


        //            ViewBag.InBoxOfDoctor = GetInboxOfDoctorGrid(Convert.ToInt32(Session["UserId"]), 1, 1, 2, null, null);
        //            ViewBag.AppDashBoardNotification = AppointmentDashboardNotification(5, 2, 1);
        //            int CompleteProfile = 0;
        //            string RemainProfile = string.Empty;
        //            if (MdlMember.FirstName != null && MdlMember.LastName != null)
        //            {
        //                if (MdlMember.lstSpeacilitiesOfDoctor.Count > 0)
        //                {
        //                    if (MdlMember.Title != null)
        //                    {
        //                        CompleteProfile = CompleteProfile + 1;
        //                    }
        //                    else
        //                    {
        //                        RemainProfile = "Title,";
        //                    }
        //                }
        //                else
        //                {
        //                    RemainProfile += "Speciality,";
        //                }

        //            }
        //            else
        //            {
        //                RemainProfile += "Name,";
        //            }
        //            if (MdlMember.Description != null)
        //            {
        //                CompleteProfile = CompleteProfile + 1;
        //            }
        //            else
        //            {
        //                RemainProfile += "Description,";
        //            }
        //            if (MdlMember.ImageName != "../../DentistImages/Doctor_no_image.gif" && MdlMember.ImageName != null)
        //            {
        //                CompleteProfile = CompleteProfile + 1;
        //            }
        //            else
        //            {
        //                RemainProfile += "Image,";
        //            }
        //            if (MdlMember.Institute != null)
        //            {
        //                CompleteProfile = CompleteProfile + 1;
        //            }
        //            else
        //            {
        //                RemainProfile += "Institute,";
        //            }
        //            if (MdlMember.MemberShip != null)
        //            {
        //                CompleteProfile = CompleteProfile + 1;
        //            }
        //            else
        //            {
        //                RemainProfile += "MemberShip,";
        //            }
        //            MdlMember.RemainList = RemainProfile;
        //            MdlMember.ProfileComplete = CompleteProfile;
        //            MdlMember.Profilepercentage = Convert.ToString(CompleteProfile * 20);
        //            return PartialView("PartialDashboard", MdlMember);
        //        }


        //        return RedirectToAction("Index", "Inbox", new { newuser = UserId });
        //    }
        //    else
        //    {
        //        return RedirectToAction("Index", "User");
        //    }
        //}
        [HttpPost]
        public JsonResult GetAppointmentofDoctor(int SortColumn, int SortDirection, int PageIndex)
        {
            MdlColleagues = new mdlColleagues();
            return Json(AppointmentDashboardNotification(SortColumn, SortDirection, PageIndex), JsonRequestBehavior.AllowGet);

        }
        public string AppointmentDashboardNotification(int SortColumn, int SortDirection, int PageIndex)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();
            clsAppointmentData clsApp = new clsAppointmentData();


            dt = clsApp.GetDoctorAppointmentData(Convert.ToInt32(Session["UserId"]), SortColumn, SortDirection, PageIndex, 7);
            int newSortDirection;
            if (SortDirection == 1)
            {
                newSortDirection = 2;
            }
            else
            {
                newSortDirection = 1;
            }

            if (dt.Rows.Count > 0)
            {
                sb.Append("<div class=\"thead\">");
                sb.Append("<div class=\"col1\" style=\"width:25%!important\">");
                sb.Append("<a class=\"sortable\" href=\"javascript:;\" onclick=\"SortingApointmentColumns('" + 1 + "','" + newSortDirection + "','" + PageIndex + "')\">Date</a></div>");
                sb.Append("<div class=\"col2\" style=\"width:25%!important\">");
                sb.Append("<a class=\"sortable\" href=\"javascript:;\" onclick=\"SortingApointmentColumns('" + 2 + "','" + newSortDirection + "','" + PageIndex + "')\">Patient Name</a></div>");
                sb.Append("<div class=\"col3\" style=\"width:25%!important\">");
                sb.Append("<a class=\"sortable\" href=\"javascript:;\" onclick=\"SortingApointmentColumns('" + 3 + "','" + newSortDirection + "','" + PageIndex + "')\">Service Type</a></div>");
                sb.Append("<div class=\"col4\" style=\"width:25%!important\">");
                sb.Append("<a class=\"sortable\" href=\"javascript:;\" onclick=\"SortingApointmentColumns('" + 4 + "','" + newSortDirection + "','" + PageIndex + "')\">Status</a></div>");
                sb.Append("</div><div id=\"ScrollAppointment\" style=\"overflow-y:auto; max-height: 358px;\" class=\"tbody\" data-sortcolumn=\"" + SortColumn + "\" data-sortirection=\"" + SortDirection + "\">");
                sb.Append("<div class=\"tbody\">");

                foreach (DataRow item in dt.Rows)
                {
                    var strAppointmentDate = "&ndash;";
                    if (!string.IsNullOrEmpty(Convert.ToString(item["AppointmentDate"])))
                    {
                        var dtAppointmentDate = Convert.ToDateTime(item["AppointmentDate"]);
                        dtAppointmentDate = clsHelper.ConvertFromUTC(dtAppointmentDate, SessionManagement.TimeZoneSystemName);
                        strAppointmentDate = new CommonBLL().ConvertToDateTime(Convert.ToDateTime(dtAppointmentDate), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL));
                        //strAppointmentDate = dtAppointmentDate.ToString("MM/dd/yyyy hh:mm tt");

                    }

                    if (item["Status"].ToString() == "CANCELLED")
                    {
                        sb.Append("<div class=\"trow\">");
                        sb.Append("<div class=\"col1\"style=\"width:25%!important\">");
                        sb.Append(strAppointmentDate);
                        sb.Append("</div>");
                        sb.Append("<div class=\"col2\"style=\"width:25%!important\">");
                        sb.Append("<a href='javascript:void(0)' style='cursor: default;pointer-events: none;'>" + item["PatientName"] + "</a></div>");
                        sb.Append("<div class=\"col3\"style=\"width:25%!important\">");
                        sb.Append("<a href='javascript:void(0)' style='cursor: default;pointer-events: none;'>" + item["ServiceType"] + "</a></div>");
                        sb.Append("<div class=\"col4\"style=\"width:25%!important\">");
                        sb.Append("<a href='javascript:void(0)' style='cursor: default;pointer-events: none;'>" + item["Status"] + "</a></div>");
                        sb.Append("<div class=\"clear\"></div>");
                        sb.Append("</div>");
                    }
                    else
                    {
                        sb.Append("<div class=\"trow\">");
                        sb.Append("<div class=\"col1\"style=\"width:25%!important\">");
                        sb.Append("<a href='" + Url.Action("Index", "Appointment", new { AppointmentId = item["AppointmentId"] }) + "' >" + strAppointmentDate + "</a>");
                        sb.Append("</div>");
                        sb.Append("<div class=\"col2\"style=\"width:25%!important\">");
                        sb.Append("<a href='" + Url.Action("Index", "Appointment", new { AppointmentId = item["AppointmentId"] }) + "'>" + item["PatientName"] + "</a></div>");
                        sb.Append("<div class=\"col3\"style=\"width:25%!important\">");
                        sb.Append("<a href='" + Url.Action("Index", "Appointment", new { AppointmentId = item["AppointmentId"] }) + "'>" + item["ServiceType"] + "</a></div>");
                        sb.Append("<div class=\"col4\"style=\"width:25%!important\">");
                        sb.Append("<a href='" + Url.Action("Index", "Appointment", new { AppointmentId = item["AppointmentId"] }) + "'>" + item["Status"] + "</a></div>");
                        sb.Append("<div class=\"clear\"></div>");
                        sb.Append("</div>");
                    }
                }
                sb.Append("</div>");
            }
            else
            {
                sb.Append("<center><br /></center>");
            }
            return sb.ToString();
        }
        public string AppointmentListOnScroll(int SortColumn, int SortDirection, int PageIndex)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();
            clsAppointmentData clsApp = new clsAppointmentData();
            dt = clsApp.GetDoctorAppointmentData(Convert.ToInt32(Session["UserId"]), SortColumn, SortDirection, PageIndex, 7);


            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {

                    if (item["Status"].ToString() == "CANCELLED")
                    {
                        sb.Append("<div class=\"trow\">");
                        sb.Append("<div class=\"col1\"style=\"width:25%!important\">");
                        sb.Append(item["AppointmentDate"]);
                        sb.Append("</div>");
                        sb.Append("<div class=\"col2\"style=\"width:25%!important\">");
                        sb.Append("<a href='javascript:void(0)' style='cursor: default;pointer-events: none;'>" + item["PatientName"] + "</a></div>");
                        sb.Append("<div class=\"col3\"style=\"width:25%!important\">");
                        sb.Append("<a href='javascript:void(0)' style='cursor: default;pointer-events: none;'>" + item["ServiceType"] + "</a></div>");
                        sb.Append("<div class=\"col4\"style=\"width:25%!important\">");
                        sb.Append("<a href='javascript:void(0)' style='cursor: default;pointer-events: none;'>" + item["Status"] + "</a></div>");
                        sb.Append("<div class=\"clear\"></div>");
                        sb.Append("</div>");
                    }
                    else
                    {
                        string AppointmentDate = new CommonBLL().ConvertToDateTime(Convert.ToDateTime(item["AppointmentDate"]), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL));
                        sb.Append("<div class=\"trow\">");
                        sb.Append("<div class=\"col1\"style=\"width:25%!important\">");
                        sb.Append("<a href='" + Url.Action("Index", "Appointment", new { AppointmentId = item["AppointmentId"] }) + "' >" + AppointmentDate + "</a>");
                        sb.Append("</div>");
                        sb.Append("<div class=\"col2\"style=\"width:25%!important\">");
                        sb.Append("<a href='" + Url.Action("Index", "Appointment", new { AppointmentId = item["AppointmentId"] }) + "'>" + item["PatientName"] + "</a></div>");
                        sb.Append("<div class=\"col3\"style=\"width:25%!important\">");
                        sb.Append("<a href='" + Url.Action("Index", "Appointment", new { AppointmentId = item["AppointmentId"] }) + "'>" + item["ServiceType"] + "</a></div>");
                        sb.Append("<div class=\"col4\"style=\"width:25%!important\">");
                        sb.Append("<a href='" + Url.Action("Index", "Appointment", new { AppointmentId = item["AppointmentId"] }) + "'>" + item["Status"] + "</a></div>");
                        sb.Append("<div class=\"clear\"></div>");
                        sb.Append("</div>");
                    }

                }
                sb.Append("</div>");
            }
            else
            {
                sb.Append("<center><br /></center>");
            }
            return sb.ToString();

        }
        [HttpPost]
        public JsonResult GetInboxofDoctor(string PageIndex, int SortColumn, int SortDirection, string Searchtext, string IsRefarral)
        {

            MdlColleagues = new mdlColleagues();

            return Json(GetInboxOfDoctorGrid(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(PageIndex), SortColumn, SortDirection, Searchtext, IsRefarral), JsonRequestBehavior.AllowGet);
        }

        public string GetInboxOfDoctorGrid(int UserId, int PageIndex, int SortColumn, int SortDirection, string Searchtext, string IsRefarral)
        {


            //List<MessageListOfDoctor> lst = new List<MessageListOfDoctor>();

            int newSortDirection;
            List<BO.ViewModel.InboxMessages> lst = new List<BO.ViewModel.InboxMessages>();
            InboxDetailBLL objinbox = new InboxDetailBLL();

            mdlColleagues MdlColleagues = new mdlColleagues();
            lst = objinbox.GetInboxOfDoctor(UserId, PageIndex, 6, SortColumn, SortDirection, Searchtext, false, SessionManagement.TimeZoneSystemName);
            // lst = MdlColleagues.GetInboxOfDoctor(UserId, PageIndex, 6, SortColumn, SortDirection, Searchtext, IsRefarral, SessionManagement.TimeZoneSystemName);

            if (SortDirection == 1)
            {
                newSortDirection = 2;
            }
            else
            {
                newSortDirection = 1;
            }

            StringBuilder sb = new StringBuilder();
            if (lst.Count > 0)
            {


                sb.Append(" <div class=\"thead\"><div class=\"col1 customdateinbox\"><a class=\"sortable\" href=\"javascript:;\" onclick=\"SortingColumns('" + PageIndex + "','" + 1 + "','" + newSortDirection + "')\" >Date</a></div>");
                sb.Append("<div class=\"col2\"><a class=\"sortable\" href=\"javascript:;\" onclick=\"SortingColumns('" + PageIndex + "','" + 2 + "','" + newSortDirection + "')\" >From</a></div>");
                //sb.Append(" <div class=\"col3\"><a class=\"sortable\" href=\"javascript:;\" onclick=\"SortingColumns('" + PageIndex + "','" + 3 + "','" + newSortDirection + "')\">Type</a></div>");
                sb.Append(" <div class=\"col4 custommessageinbox\"><a class=\"sortable\" href=\"javascript:;\" onclick=\"SortingColumns('" + PageIndex + "','" + 4 + "','" + newSortDirection + "')\">Message</a></div>");
                sb.Append(" <div class=\"customiconinbox\"></div>");
                sb.Append(" <div class=\"clear\"></div></div>");

                sb.Append("<div id=\"ScrollInbox\" style=\"overflow-y:auto; max-height: 300px;\" class=\"tbody\" data-sortcolumn=\"" + SortColumn + "\" data-sortirection=\"" + SortDirection + "\">");
                foreach (var item in lst)
                {
                    sb.Append("<div class=\"trow\">");
                    sb.Append("<div class=\"col1 customdateinbox\">");
                    //sb.Append("<a href='" + Url.Action("Index", "Inbox", new { messageid = item.MessageId, mtypeid = item.MessageTypeId }) + "'>" + item.CreationDate.ToString("MM/dd/yy hh:mm tt") + "</a></div>");
                    string CreationDate = new CommonBLL().ConvertToDateTime(Convert.ToDateTime(item.CreationDate), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL));
                    sb.Append("<a href='" + Url.Action("NewGetMessageDetails", "Inbox", new { MessageId = item.MessageId, MessageDisplayType = item.MessageDisplayType, MessageTypeId = item.MessageTypeId, IsColleagueMesage = item.IsColleagueMesage, Isread = item.IsRead }) + "'>" + CreationDate + "</a></div>");
                    sb.Append("<div class=\"col2\">");
                    sb.Append("<a href='" + Url.Action("NewGetMessageDetails", "Inbox", new { MessageId = item.MessageId, MessageDisplayType = item.MessageDisplayType, MessageTypeId = item.MessageTypeId, IsColleagueMesage = item.IsColleagueMesage, Isread = item.IsRead }) + "'>" + item.Field + "</a></div>");

                    if (item.Body.Contains("<a target=_blank"))
                    {
                        sb.Append("<div class=\"col4 custommessageinbox\">New note from " + item.Field + "</div>");
                    }
                    else
                    {

                        sb.Append("<div class=\"col4 custommessageinbox\"><a href='" + Url.Action("NewGetMessageDetails", "Inbox", new { MessageId = item.MessageId, MessageDisplayType = item.MessageDisplayType, MessageTypeId = item.MessageTypeId, IsColleagueMesage = item.IsColleagueMesage, Isread = item.IsRead }) + "'>" + Regex.Replace(Regex.Replace(objCommon.RemoveHTML(Convert.ToString(item.Body)), @"<[^>]+>|&nbsp;", string.Empty), @"[\r\n\t]+", string.Empty) + "</a></div>");
                    }
                    sb.Append("<div class=\"customiconinbox\">");
                    sb.Append("<a href='" + Url.Action("NewGetMessageDetails", "Inbox", new { MessageId = item.MessageId, MessageDisplayType = item.MessageDisplayType, MessageTypeId = item.MessageTypeId, IsColleagueMesage = item.IsColleagueMesage, Isread = item.IsRead }) + "'>");
                    if (!item.IsRead && (item.MessageTypeId == 1 || item.MessageTypeId == 3 || item.MessageTypeId == 0))
                    {
                        sb.Append("<i class='ico-unread-message' title='Unread Message'></i>");
                    }
                    else if (item.IsRead && item.MessageTypeId == 1 || item.MessageTypeId == 3 || item.MessageTypeId == 0)
                    {
                        sb.Append("<i class='ico-read-message' title='Read Message'></i>");
                    }
                    if (!item.IsRead && item.MessageTypeId == 2)
                    {
                        sb.Append("<i class='ico-unread-Send-Referral' title='Unread Referral'></i>");
                    }
                    else if (item.IsRead && item.MessageTypeId == 2)
                    {
                        sb.Append("<i class='ico-read-Send-Referral' title='Read Referral'></i>");
                    }
                    sb.Append("</a> ");
                    sb.Append("</div>");

                    sb.Append("<div class=\"clear\">");
                    sb.Append("</div></div>");

                }
                sb.Append("</div>");



            }

            else
            {

                sb.Append("<center><br /></center>");
            }


            return sb.ToString();
        }


       



        public string DashbordInboxlist(int PageIndex, int SortColumn, int SortDirection)
        {
            List<BO.ViewModel.InboxMessages> lst = new List<BO.ViewModel.InboxMessages>();
            InboxDetailBLL objinbox = new InboxDetailBLL();
            StringBuilder sb = new StringBuilder();
            mdlColleagues MdlColleagues = new mdlColleagues();
            lst = objinbox.GetInboxOfDoctor(Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(PageIndex), 6, SortColumn, SortDirection, null, false, SessionManagement.TimeZoneSystemName);
            //lst = MdlColleagues.GetInboxOfDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, 6, SortColumn, SortDirection, null, null, SessionManagement.TimeZoneSystemName);
            if (lst.Count > 0)
            {
                foreach (var item in lst)
                {
                    sb.Append("<div class=\"trow\">");
                    sb.Append("<div class=\"col1 customdateinbox\">");
                    string CreationDate = new CommonBLL().ConvertToDateTime(Convert.ToDateTime(item.CreationDate), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL));
                    sb.Append("<a href='" + Url.Action("NewGetMessageDetails", "Inbox", new { MessageId = item.MessageId, MessageDisplayType = item.MessageDisplayType, MessageTypeId = item.MessageTypeId, IsColleagueMesage = item.IsColleagueMesage, Isread = item.IsRead }) + "'>" + CreationDate + "</a></div>");
                    sb.Append("<div class=\"col2\">");
                    sb.Append("<a href='" + Url.Action("NewGetMessageDetails", "Inbox", new { MessageId = item.MessageId, MessageDisplayType = item.MessageDisplayType, MessageTypeId = item.MessageTypeId, IsColleagueMesage = item.IsColleagueMesage, Isread = item.IsRead }) + "'>" + item.Field + "</a></div>");
                    if (item.Body.Contains("<a target=_blank"))
                    {
                        sb.Append("<div class=\"col4 custommessageinbox\"'>New note from " + item.Field + "</div>");
                    }
                    else
                    {

                        sb.Append("<div class=\"col4 custommessageinbox\"'><a href='" + Url.Action("NewGetMessageDetails", "Inbox", new { MessageId = item.MessageId, MessageDisplayType = item.MessageDisplayType, MessageTypeId = item.MessageTypeId, IsColleagueMesage = item.IsColleagueMesage, Isread = item.IsRead }) + "'>" + Regex.Replace(Regex.Replace(objCommon.RemoveHTML(Convert.ToString(item.Body)), @"<[^>]+>|&nbsp;", string.Empty), @"[\r\n\t]+", string.Empty) + "</a></div>");
                    }
                    sb.Append("<div class=\"customiconinbox\">");
                    sb.Append("<a href='" + Url.Action("NewGetMessageDetails", "Inbox", new { MessageId = item.MessageId, MessageDisplayType = item.MessageDisplayType, MessageTypeId = item.MessageTypeId, IsColleagueMesage = item.IsColleagueMesage, Isread = item.IsRead }) + "'>");
                    if (!item.IsRead && (item.MessageTypeId == 1 || item.MessageTypeId == 3 || item.MessageTypeId == 0))
                    {
                        sb.Append("<i class='ico-unread-message' title='Unread Message'></i>");
                    }
                    else if (item.IsRead && item.MessageTypeId == 1 || item.MessageTypeId == 3 || item.MessageTypeId == 0)
                    {
                        sb.Append("<i class='ico-read-message' title='Read Message'></i>");
                    }
                    if (!item.IsRead && item.MessageTypeId == 2)
                    {
                        sb.Append("<i class='ico-unread-Send-Referral' title='Unread Send Referral'></i>");
                    }
                    else if (item.IsRead && item.MessageTypeId == 2)
                    {
                        sb.Append("<i class='ico-read-Send-Referral' title='Read Send Referral'></i>");
                    }
                    sb.Append("</a> ");
                    sb.Append("</div>");


                    sb.Append("<div class=\"clear\">");
                    sb.Append("</div></div>");

                }
            }
            else
            {

                sb.Append("<center><br /></center>");
            }
            return sb.ToString();
        }






        [HttpPost]
        public JsonResult SendReferralByEmail(string Email)
        {
            object obj = string.Empty;
            try
            {
                DataTable dtCheckInSystem = new DataTable();
                dtCheckInSystem = ObjColleaguesData.CheckEmailExistsAsDoctor(Email);
                if (dtCheckInSystem != null && dtCheckInSystem.Rows.Count > 0)
                {
                    DataTable dtCheckAsColleague = ObjColleaguesData.GetColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), 1, int.MaxValue, Email, 1, 2);
                    if (dtCheckAsColleague != null && dtCheckAsColleague.Rows.Count > 0)
                    {
                        obj = Convert.ToString(dtCheckAsColleague.Rows[0]["ColleagueId"]);
                    }
                    else
                    {
                        // Add as Colleague
                        bool Result = false;
                        int colleagueId = Convert.ToInt32(dtCheckInSystem.Rows[0]["UserId"]);
                        if (colleagueId != Convert.ToInt32(Session["UserId"]))
                        {
                            Result = ObjColleaguesData.AddAsColleague(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(dtCheckInSystem.Rows[0]["UserId"]));
                            if (Result)
                            {
                                //Send Template Add as colleague
                                ObjTemplate.ColleagueInviteDoctor(Convert.ToInt32(Session["UserId"]), colleagueId, "");

                                DataTable dt = ObjColleaguesData.GetColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), 1, int.MaxValue, Email, 1, 2);
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    obj = Convert.ToString(dt.Rows[0]["ColleagueId"]);
                                }

                            }
                        }
                        else
                        {
                            obj = "false";
                        }

                    }
                }
                else
                {

                    obj = "SendTemplate";
                    // Send Template to join recordlinc
                    ObjTemplate.InviteDoctor(Email, Convert.ToInt32(Session["UserId"]), "I would like to invite you join in my professional referral network on RecordLinc.");
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }










        //public PartialViewResult Help()
        //{
        //    return PartialView("PartialHelp");
        //}

        //RM-414 : New UI for static pages
        public ActionResult Help()
        {
            return View();
        }

        //public PartialViewResult Contact()
        //{
        //    return PartialView("PartialContact");
        //}

        //RM-414 : New UI for static pages
        public ActionResult ContactUs()
        {
            ContactUs cuobj = new ContactUs();
            return View(cuobj);
        }


        //public PartialViewResult PrivacyPolicy()
        //{
        //    DataTable dt = new DataTable();
        //    dt = ObjAdmin.GetAllCustomPageDetailsById(1);
        //    if (dt != null && dt.Rows.Count > 0)
        //    {
        //        ViewBag.PrivacyPolicy = objCommon.CheckNull(Convert.ToString(dt.Rows[0]["PageDesc"]), string.Empty);
        //    }

        //    return PartialView("PartialPrivacyPolicy");
        //}

        //RM-414 :NewUI for static pages.
        public ActionResult PrivacyPolicy()
        {
            return View();
        }


        //public PartialViewResult TermsAndConditions()
        //{
        //    DataTable dt = new DataTable();
        //    dt = ObjAdmin.GetAllCustomPageDetailsById(2);
        //    if (dt != null && dt.Rows.Count > 0)
        //    {
        //        ViewBag.TermsAndConditions = objCommon.CheckNull(Convert.ToString(dt.Rows[0]["PageDesc"]), string.Empty);
        //    }
        //    return PartialView("PartialTermsAndConditions");
        //}

        public ActionResult TermsAndConditions()
        {

            return View();
        }


        #region Code For People you may know
        [HttpPost]
        public JsonResult PeopleYouMayKnowNextPrev(int PageIndex)
        {
            MdlMember = new Member();
            MdlColleagues = new mdlColleagues();
            return Json(GetPeopleYouMayKnow(Convert.ToInt32(Session["UserId"]), PageIndex), JsonRequestBehavior.AllowGet);
        }

        public string GetPeopleYouMayKnow(int UserId, int PageIndex)
        {
            StringBuilder sb = new StringBuilder();
            ObjColleaguesData = new clsColleaguesData();
            MdlMember.lstColleaguesDetails = MdlColleagues.ColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, 6, null, 13, 1, null, 0);
            MdlMember.lstColleaguesDetails = MdlMember.lstColleaguesDetails.OrderByDescending(t => t.ColleageReferralCount).ToList();
            try
            {
                if (MdlMember.lstColleaguesDetails.Count > 0)
                {
                    double TotalPages = Math.Ceiling((double)MdlMember.lstColleaguesDetails.ElementAt(0).TotalRecord / 5);
                    sb.Append("<div><ul style=\"height: auto !important;\"  class=\"clearfix slides\"><li id=\"ScrollPeopleYouKNow\" style=\"overflow-y:scroll;overflow-x: hidden;max-height: 500px !important;\">");


                    foreach (var item in MdlMember.lstColleaguesDetails)
                    {


                        sb.Append("<div id=\"row" + item.ColleagueId + "\" class=\"dataList\"><div class=\"thumb\">");
                        sb.Append("<img style=\"max-height: 80px;cursor: pointer;\"  alt=\"" + (!string.IsNullOrEmpty(item.FirstName) ? item.FirstName + ", " : string.Empty) + (!string.IsNullOrEmpty(item.LastName) ? item.LastName + ", " : string.Empty) + "\" src=\"" + item.ImageName + "\" onclick=\"ViewProfile(" + item.ColleagueId + " )\"/></div>");
                        sb.Append("<div class=\"details\" style=\"font-size:11px;\" ><div style=\"word-wrap:break-word; width:63%; display:inline; float:left; font-size:17px!important\"><a onclick=\"ViewProfile(" + item.ColleagueId + " )\" style=\"cursor: pointer;\" title=\"Click to view profile\" href=\"javascript:;\">" + item.FirstName + "&nbsp;" + item.LastName + "</a></div>");
                        sb.Append("<div style=\"float:right; padding-top:3px;width:65px;margin-right:8px;text-align:right;display:inline-block;\"><a href=\"javascript:;\" title=\"Send Referral\" data-role=\"OpenPopup\" data-value=\"" + item.ColleagueId + "\" data-location=\""+item.LocationId+"\"><img src=\"../Content/images/send-referal-small.png \" style=\"padding:2px 8px 0px 0px;\"></a>");
                        //sb.Append("<a href=\"/Inbox/Compose?ColleaguesId=" + item.ColleagueId + "\" title=\"Send Message\">Send Message</a></div><div style=\"word-wrap:break-word; width:38%; padding-top:3px; float:left;line-height:15px;\">");
                        sb.Append("<a href=\"/Inbox/ComposeMessage?ColleaguesId=" + item.ColleagueId + "\" title=\"Send Message\"><img src=\"../Content/images/send-message-small.png \" style=\"width:25px;\"></a></div><div style=\"word-wrap:break-word; width:38%; padding-top:3px; float:left;line-height:15px;\">");
                        if (item.lstSpeacilitiesOfDoctor.Count > 0)
                        {
                            sb.Append(item.lstSpeacilitiesOfDoctor[0].SpecialtyDescription + "<br>");
                        }
                        sb.Append((item.City != string.Empty && item.City != null ? item.City + ", " : string.Empty) + item.State + "</div></div>");
                        //DataTable dtReferralRece = new DataTable();
                        //DataTable dtReferralSent = new DataTable();

                        //dtReferralRece = ObjColleaguesData.GetReferralReceivedCountOfDoctor(Convert.ToInt32(Session["UserId"]), item.ColleagueId);
                        //dtReferralSent = ObjColleaguesData.GetReferralSentCountOfDoctor(Convert.ToInt32(Session["UserId"]), item.ColleagueId);
                        //sb.Append("<input type=\"hidden\" value=\"" + dtReferralRece.Rows[0]["TotalRecord"] + "\" id=\""+ item.ColleagueId + "\" />");
                        //sb.Append("<input type=\"hidden\" value=\"" + dtReferralSent.Rows[0]["TotalRecord"] + "\" id=\"" + item.ColleagueId + "\" />");
                        //sb.Append("<div><a href=\"javascript:;\" title=\"Send Referral\" data-role=\"OpenPopup\" data-value=\"" + item.ColleagueId + "\">Send Referral</a><br/>");
                        //// sb.Append("</br>");
                        //sb.Append("<a href=\"/Inbox/Compose?ColleaguesId=" + item.ColleagueId + "\" title=\"Send Message\">Send Message</a></div></div>");
                        //sb.Append("<a href=\"Javascript:;\" onclick=\"AddColleagues(" + item.ColleagueId + " )\" title=\"Add as colleague\">Quick Add</a></div>");
                        sb.Append("<div class=\"clear\"></div></div>");
                    }

                    sb.Append("</li></ul></div>");

                }

            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        public string PeopleDoyouKnowList(int PageIndex)
        {
            try
            {
                StringBuilder sb = new StringBuilder();
                mdlColleagues MdlColleagues = new mdlColleagues();
                Member MdlMember = new Member();
                MdlMember.lstColleaguesDetails = MdlColleagues.ColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, 6, null, 13, 1, null, 0);
                if (MdlMember.lstColleaguesDetails.Count > 0)
                {

                    foreach (var item in MdlMember.lstColleaguesDetails)
                    {
                        sb.Append("<div id=\"row" + item.ColleagueId + "\" class=\"dataList\"><div class=\"thumb\">");
                        //sb.Append("<img tyle=\"max-height: 80px;cursor: pointer;\"  alt=\"" + (!string.IsNullOrEmpty(item.FirstName) ? item.FirstName + ", " : string.Empty) + (!string.IsNullOrEmpty(item.LastName) ? item.LastName + ", " : string.Empty) + "\" src=\"" + Html.Action("GetBase64ContentOFImage", "Common", new { FileId = 0, FileFrom = (int)BO.Enums.Common.FileFromFolder.DirectPath, FileName = item.ImageName }) + "\" onclick=\"ViewProfile(" + item.ColleagueId + " )\"/></div>");
                        sb.Append("<img tyle=\"max-height: 80px;cursor: pointer;\"  alt=\"" + (!string.IsNullOrEmpty(item.FirstName) ? item.FirstName + ", " : string.Empty) + (!string.IsNullOrEmpty(item.LastName) ? item.LastName + ", " : string.Empty) + "\" src=\"" + clsHelper.EscapeUriString(item.ImageName) + "\" onclick=\"ViewProfile(" + item.ColleagueId + " )\"/></div>");
                        sb.Append("<div class=\"details\" style=\"font-size:11px;\" ><div style=\"word-wrap:break-word; width:63%; display:inline; float:left; font-size:17px!important\"><a onclick=\"ViewProfile(" + item.ColleagueId + " )\" style=\"cursor: pointer;\" title=\"Click to view profile\" href=\"javascript:;\">" + item.FirstName + "&nbsp;" + item.LastName + "</a></div>");
                        sb.Append("<div style=\"float:right; padding-top:3px;width:65px;margin-right:8px;text-align:right;display:inline-block;\"><a href=\"javascript:;\" title=\"Send Referral\" data-role=\"OpenPopup\" data-value=\"" + item.ColleagueId + "\"><img src=\"../Content/images/send-referal-small.png \" style=\"padding:2px 8px 0px 0px;\"></a>");
                        //sb.Append("<a href=\"/Inbox/Compose?ColleaguesId=" + item.ColleagueId + "\" title=\"Send Message\">Send Message</a></div><div style=\"word-wrap:break-word; width:38%; padding-top:3px; float:left;line-height:15px;\">");
                        sb.Append("<a href=\"/Inbox/Compose?ColleaguesId=" + item.ColleagueId + "\" title=\"Send Message\"><img src=\"../Content/images/send-message-small.png \" style=\"width:25px;\"></a></div><div style=\"word-wrap:break-word; width:38%; padding-top:3px; float:left;line-height:15px;\">");
                        if (item.lstSpeacilitiesOfDoctor.Count > 0)
                        {
                            sb.Append(item.lstSpeacilitiesOfDoctor[0].SpecialtyDescription + "<br>");
                        }
                        sb.Append((item.City != string.Empty && item.City != null ? item.City + ", " : string.Empty) + item.State + "</div></div>");
                        //DataTable dtReferralRece = new DataTable();
                        //DataTable dtReferralSent = new DataTable();

                        //dtReferralRece = ObjColleaguesData.GetReferralReceivedCountOfDoctor(Convert.ToInt32(Session["UserId"]), item.ColleagueId);
                        //dtReferralSent = ObjColleaguesData.GetReferralSentCountOfDoctor(Convert.ToInt32(Session["UserId"]), item.ColleagueId);
                        //sb.Append("<input type=\"hidden\" value=\"" + dtReferralRece.Rows[0]["TotalRecord"] + "\" id=\"" + item.ColleagueId + "\" />");
                        //sb.Append("<input type=\"hidden\" value=\"" + dtReferralSent.Rows[0]["TotalRecord"] + "\" id=\"" + item.ColleagueId + "\" />");
                        sb.Append("<div class=\"clear\"></div></div>");
                    }
                }
                else
                {

                    sb.Append("<center>No More Record Found</center>");

                }
                return sb.ToString();
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("Colleagues list on Dashboard Page.", Ex.Message, Ex.StackTrace);
                throw;
            }

        }
        #endregion

        


        [HttpPost]
        public string SearchPatientFromPopUp(string SearchText)
        {
            return GetPatientListForSendReferralPopUp(SearchText);
        }
        public string GetPatientListForSendReferralPopUp(string SearchText)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                MdlPatient = new mdlPatient();
                MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), 1, 30, SearchText, 11, 2, null, 0);

                //sb.Append("<style>#Checkallarea input[type=checkbox]{display: none;}</style>");
                sb.Append(" <h2 id=\"TitleRefer\" class=\"title\">Select Patient to Refer</h2>");
                sb.Append(" <div class=\"search_main_div\"><input style=\"display:none\" id=\"SearchBtnHide\" class=\"search_btn_popup\" name=\"Cancel\" type=\"button\" value=\"Cancel\" onclick=\"SearchPatient()\"><div class=\"patient_searchbar\"><input  onfocus=\"if (this.value =='Find patient to refer...') {this.value = '';}\"  id=\"txtSearchPatient\" onkeyup=\"SearchPatientOnKeyup()\" onblur=\"if (this.value == '') {this.value = 'Find patient to refer...';}\"  type=\"text\" value=\"Find patient to refer...\" name=\"\"></div></div>");
                sb.Append("<div id=\"ColleagueLocation\"></div>");
                sb.Append("<div id=\"Patientcallonscroll\" class=\"cntnt\"><input type=\"hidden\" id=\"LocationID\" value=\"\">");


                if (MdlPatient.lstPatientDetailsOfDoctor.Count > 0)
                {
                    sb.Append("<ul id=\"ScondIdScroll\" class=\"search_listing\" style=\"width:100%;\">");


                    foreach (var item in MdlPatient.lstPatientDetailsOfDoctor.Take(500))
                    {
                        sb.Append("<li><div class=\"clearfix\" id=\"" + item.PatientId + "\" style=\"display:block !important\">");
                        sb.Append("<span class=\"check\"><input type=\"checkbox\" name=\"\" class=\"checkbox_add_patient\" id=\"chkPatient\" value=\"" + item.PatientId + "\"></span>");
                        //sb.Append("<span class=\"thumb\"><img style=\"height: 95px;width: 85px;\" alt=\"\" src='" + ((!string.IsNullOrEmpty(item.ImageName) ? Url.Action("GetBase64ContentOFImage", "Common", new { FileId = 0, FileFrom = (int)BO.Enums.Common.FileFromFolder.DirectPath, FileName = item.ImageName }) : (item.Gender.ToString() == "Female") ? System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"] : System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorImage"])) + "' ></span> <span class=\"description\"><h2><a  style=\"word-wrap: break-word;\" href=\"javascript:;\"\">" + item.FirstName + "&nbsp;" + item.LastName + "</a></h2></span>");
                        sb.Append("<span class=\"thumb\"><img style=\"height: 95px;width: 85px;\" alt=\"\" src='" + ((!string.IsNullOrEmpty(item.ImageName) ? clsHelper.EscapeUriString(item.ImageName) : (item.Gender.ToString() == "Female") ? System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"] : System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorImage"])) + "' ></span> <span class=\"description\"><h2><a  style=\"word-wrap: break-word;\" href=\"javascript:;\"\">" + item.FirstName + "&nbsp;" + item.LastName + "</a></h2></span>");
                        sb.Append("<span class=\"clear\"></span></div></li>");
                    }

                    //sb.Append("</ul><div class=\"clear\"></div><br /><div id=\"EmptyRecord\"></div><div class=\"actions\"><input style=\"margin-left:2px;\" class=\"add_patientz\" onclick=\"return ReferralSend()\" name=\"Send\" type=\"button\" value=\"Send\"><input class=\"cancel_btn\" name=\"Cancel\" type=\"submit\" onclick=\"CloseEditPatientListpop()\" value=\"Cancel\"><div class=\"clear\">");
                    //sb.Append("</ul><div class=\"clear\"></div><br /><div id=\"EmptyRecord\"></div><div class=\"actions\"><a onclick=\"return ReferralSend()\"><img src=\"../../Content/images/btn-next.png\" style=\"height:43px;margin-top: -1px;\"></a><input class=\"cancel_btn\" name=\"Cancel\" type=\"submit\" onclick=\"CloseEditPatientListpop()\" value=\"Cancel\"><div class=\"clear\">");
                    sb.Append("</ul><div class=\"clear\"></div><br /><div id=\"EmptyRecord\"></div><div class=\"actions\"><a onclick=\"return ReferralSend()\"><img src=\"../../Content/images/btn-next.png\" class=\"btn-popup pull-right\"></a><input class=\"New_cancel_btn btn-popup\" name=\"Cancel\" type=\"submit\" onclick=\"CloseEditPatientListpop()\" value=\"Cancel\"><div class=\"clear\">");
                }
                else
                {
                    sb.Append("<center>Patient not found. Please<a href=\"" + Url.Action("AddPatient", "Patients") + "\" style=\"color: #0890c4;\"> click here</a> to add new patient.</center>");
                }

                sb.Append("</div><button class=\"mfp-close\" type=\"button\" title=\"Close (Esc)\">×</button>");
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }


        public string SearchPatientListForSendReferralPopUpLI(string SearchText, string pageindex, string pagesize)
        {

            return GetPatientListForSendReferralPopUpLI(SearchText, int.Parse(pageindex), int.Parse(pagesize));
        }

        //Get Only LI Containt 
        public string GetPatientListForSendReferralPopUpLI(string SearchText, int pageindex, int pagesize)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                MdlPatient = new mdlPatient();
                MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), pageindex, pagesize, SearchText, 11, 2, null, 0);

                if (MdlPatient.lstPatientDetailsOfDoctor.Count > 0)
                {
                    foreach (var item in MdlPatient.lstPatientDetailsOfDoctor)
                    {
                        sb.Append("<li><div class=\"clearfix\" id=\"" + item.PatientId + "\">");
                        sb.Append("<span class=\"check\"><input type=\"checkbox\" name=\"\" class=\"checkbox_add_patient\" id=\"chkPatient\" value=\"" + item.PatientId + "\"></span>");
                        sb.Append("<span class=\"thumb\"><img style=\"height: 95px;width: 85px;\" alt=\"\" src='" + ((!string.IsNullOrEmpty(item.ImageName) ? clsHelper.EscapeUriString(item.ImageName) : (item.Gender.ToString() == "Female") ? System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"] : System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorImage"])) + "' ></span> <span class=\"description\"><h2><a  style=\"word-wrap: break-word;\" href=\"javascript:;\"\">" + item.FirstName + "&nbsp;" + item.LastName + "</a></h2></span>");
                        sb.Append("<span class=\"clear\"></span></div></li>");
                    }
                }
                else if (pageindex == 1)
                {
                    sb.Append("<center>No Record Found</center>");
                }
                else
                {
                    sb.Append("<center>No More Record Found</center>");
                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }



        [HttpPost]
        public JsonResult InviteColleagueByEmail(string Email)
        {
            object obj = string.Empty;
            int status;
            try
            {
                MdlColleagues = new mdlColleagues();
                DataSet ds = new DataSet();
                ds = ObjColleaguesData.GetDoctorDetailsById(Convert.ToInt32(Session["UserId"]));
                if (ds != null && ds.Tables.Count > 0)
                {
                    string UserEmail = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Username"]), string.Empty);
                    if (UserEmail != Email)
                    {

                        status = MdlColleagues.GetExistingEmail(Convert.ToInt32(Session["UserId"]), Email, "I have added you as a colleague in my professional referral network on RecordLinc.");
                        if (status == 0)
                        {
                            obj = "statusfalse";
                        }
                        else
                        {
                            obj = status.ToString();
                        }

                    }
                    else
                    {
                        obj = "false";
                    }
                }
                if (true)
                {

                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        #region First Time Logged In Pop Up


        public ActionResult OpenFirstimeLoginpopup(string UserId)
        {
            MdlMember = new Member();
            DataTable dt = new DataTable();
            DataSet ds = new DataSet();
            ds = ObjColleaguesData.GetDoctorDetailsById(Convert.ToInt32(UserId));

            if (ds.Tables.Count > 0)
            {
                MdlMember.UserId = Convert.ToInt32(UserId);
                MdlMember.FirstName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]), string.Empty);
                MdlMember.LastName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LastName"]), string.Empty);
                ViewBag.country = CountryScript("");
                ViewBag.state = StateScript("", "0");
            }



            return PartialView("PartialFirstTimeLogIn", MdlMember);
        }
        public string CountryScript(string countryId)
        {
            if (countryId == "")
            {
                countryId = "US";
            }
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.Country();
            sb.Append("<select id=\"drpCountry\" name=\"drpCountry\" onchange=\"FillState();\">");

            foreach (var item in lst)
            {
                if (countryId == item.Value)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
                else
                {
                    sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
            }
            sb.Append("</select>");
            return sb.ToString();
        }

        public string StateScript(string countryId, string stateId)
        {
            if (countryId == "")
            {
                countryId = "US";
            }
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.StateByCountryCode(countryId);
            sb.Append("<select id=\"drpState\" name=\"drpState\">");
            foreach (var item in lst)
            {
                if (stateId == item.Value)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
                else
                {
                    sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
            }
            sb.Append("</select>");
            return sb.ToString();
        }
        public JsonResult FillCountry(string cid)
        {
            return Json(CountryScript(cid), JsonRequestBehavior.AllowGet);
        }
        public JsonResult FillState(string cid, string sid)
        {
            return Json(StateScript(cid, sid), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateDoctorDetails(string UserId, string Password, string FirstName, string LastName, string Officename, string Exactaddress, string City, string State, string ZipCode, string Country)
        {
            object obj = string.Empty;
            try
            {
                DataSet ds = ObjColleaguesData.GetDoctorDetailsById(Convert.ToInt32(UserId));
                if (ds.Tables.Count > 0)
                {
                    bool result = ObjColleaguesData.UpdateDoctorPersonalInformationFromProfile(Convert.ToInt32(UserId), FirstName, objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["MiddleName"]), string.Empty), LastName, Officename, objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Title"]), string.Empty), "", "");
                    bool updatepass = ObjColleaguesData.UpdatePasswordOfDoctor(Convert.ToInt32(UserId), ObjTripleDESCryptoHelper.encryptText(Password));
                    if (ds.Tables[2].Rows.Count > 0)
                    {
                        //bool Resultaddress = ObjColleaguesData.UpdateAddressDetailsByAddressInfoID(Convert.ToInt32(ds.Tables[2].Rows[0]["AddressInfoId"]), Exactaddress, objCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["Address2"]), string.Empty), City, State, Country, ZipCode, Convert.ToInt32(UserId), 1, objCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["EmailAddress"]), string.Empty), objCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["Phone"]), string.Empty), objCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["Fax"]), string.Empty), string.Empty);
                    }//DD Changes
                    bool Result = false;
                    Result = objCommon.InsertLogedInfo(Convert.ToInt32(UserId));
                    Session["FirstName"] = FirstName;
                    obj = "1";
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion
        [HttpPost]
        public string SearchColleagueFromPopUp(string SearchText)
        {
            return GetListSearchColleagueFromPopUp(SearchText);
        }
        public string GetListSearchColleagueFromPopUp(string SearchText)
        {

            StringBuilder sb = new StringBuilder();
            try
            {
                MdlColleagues = new mdlColleagues();
                MdlColleagues.lstColleaguesDetails = MdlColleagues.ColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), 1, 16, SearchText, 11, 2, null, 0);

                sb.Append("<h2 id=\"ReferralTitle\" class=\"title\">Select the Colleagues to Send Referral </h2>");


                //sb.Append("<h2 id=\"ReferralTitle\" class=\"title\">    </h2>");

                sb.Append("<div  class=\"search_main_div\"><input style=\"display:none\" id=\"SearchBtnHide\" class=\"search_btn_popup\" name=\"Cancel\" type=\"button\" onclick=\"DashboardSearchColleague()\" value=\"Cancel\"><div class=\"patient_searchbar\"><input id='txtSearchColleagues' onkeyup=\"SearchColleague()\" onfocus=\"if (this.value =='Find colleague to refer to...') {this.value = '';}\" onblur=\"if (this.value == '') {this.value = 'Find colleague to refer to...';}\" type=\"text\" value=\"Find colleague to refer to...\" name=\"Search Doctor\" >");
                sb.Append("</div></div><div class=\"cntnt\">");



                if (MdlColleagues.lstColleaguesDetails.Count > 0)
                {
                    sb.Append("<ul class=\"search_listing\" style=\"width:100%;\">");
                    foreach (var item in MdlColleagues.lstColleaguesDetails)
                    {

                        sb.Append("<li><div class=\"dyheight\" id=\"" + item.ColleagueId + "\">");
                        sb.Append("<span class=\"check\"><input type=\"checkbox\" id=\"checkcolleague\" name=\"\" class=\"checkbox_add_patient\" value=\"" + item.ColleagueId + "\"></span>");
                        sb.Append("<span class=\"thumb\"><a  href=\"Javascript:;\" onclick=\"javascript:void(0);\" ><img alt=\"\" style=\"height: 95px; width: 85px;\" src='" + clsHelper.EscapeUriString(item.ImageName) + "'></a></span>");
                        sb.Append("<span class=\"description\">");
                        sb.Append("<h2><a  href=\"Javascript:;\" style=\"word-wrap: break-word;\" onclick=\"javascript:void(0);\">" + item.FirstName + "&nbsp;" + item.LastName + "<span class=\"sub_heading\">" + (!string.IsNullOrEmpty(item.Officename) ? item.Officename : "&nbsp;") + "</span></a></h2>");
                        sb.Append("<ul class=\"list\">");
                        if (item.lstSpeacilitiesOfDoctor.Count > 0)
                        {
                            foreach (var itemSpeacilitiesOfDoctor in item.lstSpeacilitiesOfDoctor)
                            {
                                sb.Append("<li>" + (!string.IsNullOrWhiteSpace(itemSpeacilitiesOfDoctor.SpecialtyDescription) ? itemSpeacilitiesOfDoctor.SpecialtyDescription : "<br />") + "</li>  ");
                            }
                        }
                        else
                        {
                            sb.Append("<li>&nbsp;</li>");
                        }
                        if(!string.IsNullOrWhiteSpace(item.Location))
                        {
                            sb.Append("<h2>"+item.Location+"</h2>");
                        }

                        sb.Append("</ul></span><span class=\"clear\"></span></div></li>");
                    }

                    sb.Append("</ul>");
                    sb.Append("<div class=\"clear\"></div>");
                    sb.Append("<div class=\"actions\"><div id=\"EmptyRecord\"></div>");
                    sb.Append("<a href=\"JavaScript:;\" onclick=\"DashBoardReferralSend()\" ><img alt=\"\" src=\"../../Content/images/btn-next.png\"/></a>");
                    sb.Append("<div class=\"clear\"></div></div></div><div id=\"ColleaguesLocation\" style=\"max-height :400px\"></div>");
                }
                else
                {

                    sb.Append(" <center>Colleague not found. Please <a href=\"" + Url.Action("SearchColleagues", "Colleagues") + "\" style=\"color: #0890c4;\"> click here</a> to search and add colleague.</center>");
                }

                sb.Append("<button class=\"mfp-close\" title=\"Close (Esc)\" type=\"button\">×</button></div>");
                sb.Append("<div id=\"ColleagueLocation\"></div>");
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }


        public string GetColleagueListForSendReferralPopUpOnScroll(int PageIndex, string SearchText = null)
        {

            StringBuilder sb = new StringBuilder();
            MdlColleagues = new mdlColleagues();
            MdlColleagues.lstColleaguesDetails = MdlColleagues.ColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, 16, SearchText, 11, 2, null, 0);
            if (MdlColleagues.lstColleaguesDetails.Count > 0)
            {
                foreach (var item in MdlColleagues.lstColleaguesDetails)
                {

                    sb.Append("<li><div class=\"dyheight\">");
                    sb.Append("<span class=\"check\"><input type=\"checkbox\" id=\"checkcolleague\" name=\"\" class=\"checkbox_add_patient\" value=\"" + item.ColleagueId + "\"></span>");
                    sb.Append("<span class=\"thumb\"><a  href=\"Javascript:;\"><img alt=\"\" style=\"height: 95px; width: 85px;\"\" src='" + clsHelper.EscapeUriString(item.ImageName) + "'></a></span>");
                    sb.Append("<span class=\"description\">");
                    sb.Append("<h2><a  href=\"Javascript:;\" style=\"word-wrap: break-word;\" onclick=\"ViewProfile('" + item.ColleagueId + "')\">" + item.FirstName + "&nbsp;" + item.LastName + "<span class=\"sub_heading\">" + (!string.IsNullOrEmpty(item.Officename) ? item.Officename : "&nbsp;") + "</span></a></h2>");
                    sb.Append("<ul class=\"list\">");
                    if (item.lstSpeacilitiesOfDoctor.Count > 0)
                    {
                        foreach (var itemSpeacilitiesOfDoctor in item.lstSpeacilitiesOfDoctor)
                        {
                            sb.Append("<li>" + itemSpeacilitiesOfDoctor.SpecialtyDescription + "</li>  ");
                        }
                    }
                    //else
                    //{
                    //    sb.Append("<li>&nbsp;</li>");
                    //}
                    if (!string.IsNullOrWhiteSpace(item.Location))
                    {
                        sb.Append("<h2>" + item.Location + "</h2>");
                    }

                    sb.Append("</ul></span><span class=\"clear\"></span></div></li>");
                }
            }
            else
            {
                sb.Append("<li><center>No more record found.</li>");
            }
            return sb.ToString();
        }

        public string GetLocationColleagues(int ColleagueId, string viewName = "PartialColleaguesLocation")
        {
            try
            {

                List<AddressDetails> AddressDetails = new List<AddressDetails>();

                clsColleaguesData objColleaguesData = new clsColleaguesData();
                clsCommon objCommon = new clsCommon();
                DataTable ds = new DataTable();
                ds = objColleaguesData.GetDoctorLocaitonById(ColleagueId);
                string GetString = string.Empty;
                if (ds != null && ds.Rows.Count > 0)
                {
                    //For Address details of Doctor

                    AddressDetails = (from p in ds.AsEnumerable()
                                      select new AddressDetails
                                      {
                                          AddressInfoID = Convert.ToInt32(p["AddressInfoID"]),

                                          ExactAddress = objCommon.CheckNull(Convert.ToString(p["ExactAddress"]), string.Empty),
                                          Address2 = objCommon.CheckNull(Convert.ToString(p["Address2"]), string.Empty),
                                          City = objCommon.CheckNull(Convert.ToString(p["City"]), string.Empty),
                                          State = objCommon.CheckNull(Convert.ToString(p["State"]), string.Empty),
                                          Country = objCommon.CheckNull(Convert.ToString(p["Country"]), string.Empty),
                                          ZipCode = objCommon.CheckNull(Convert.ToString(p["ZipCode"]), string.Empty),
                                          Phone = objCommon.CheckNull(Convert.ToString(p["Phone"]), string.Empty),
                                          ContactType = Convert.ToInt32(p["ContactType"]),
                                          Location = objCommon.CheckNull(Convert.ToString(p["Location"]), string.Empty)
                                      }).ToList();
                    ViewBag.ColleagueId = ColleagueId;
                    ViewData.Model = AddressDetails;
                    if (AddressDetails.Count > 1)
                    {


                        using (var sw = new StringWriter())
                        {
                            var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                            var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                            viewResult.View.Render(viewContext, sw);
                            viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                            GetString = sw.GetStringBuilder().ToString();
                        }
                        return GetString;
                    }
                    return "1";
                }

            }
            catch (Exception)
            {
                throw;
            }
            return "2";
        }
        public string SearchDashboardPatientFromPopUp(string SearchText)
        {
            return GetDashboardPatientListForSendReferralPopUp(SearchText);
        }
        public string GetDashboardPatientListForSendReferralPopUp(string SearchText)
        {
            StringBuilder sb = new StringBuilder();
            string searchValue = "Find patient to refer...";
            if (!string.IsNullOrEmpty(SearchText))
            {
                searchValue = SearchText;
            }
            try
            {
                MdlPatient = new mdlPatient();
                MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), 1, 30, SearchText, 11, 2, null, 0);

                //sb.Append("<style>#Checkallarea input[type=checkbox]{display: none;}</style>");
                sb.Append(" <h2 class=\"title\">Select Patient to Refer</h2>");
                sb.Append(" <div class=\"search_main_div\"><input style=\"display:none\" id=\"SearchBtnHide\" class=\"search_btn_popup\" name=\"Cancel\" type=\"button\" value=\"Cancel\" onclick=\"SearchDashBoardPatient()\"><div class=\"patient_searchbar\"><input  onfocus=\"if (this.value =='Find patient to refer...') {this.value = '';}\" onkeyup=\"SearchPatientOnKeyup()\"  id=\"txtSearchPatient\" onblur=\"if (this.value == '') {this.value = 'Find patient to refer...';}\"  type=\"text\" value=\"" + searchValue + "\" name=\"\"></div></div>");
                sb.Append("<div id=\"ColleagueLocation\"></div>");
                sb.Append("<div id=\"Patientcallonscroll\" class=\"cntnt\"><input type=\"hidden\" id=\"LocationID\" value=\"\">");


                if (MdlPatient.lstPatientDetailsOfDoctor.Count > 0)
                {
                    sb.Append("<ul id=\"ScondIdScroll\" class=\"search_listing\" style=\"width:100%;\">");


                    foreach (var item in MdlPatient.lstPatientDetailsOfDoctor.Take(500))
                    {
                        sb.Append("<li><div class=\"clearfix\" id=\"" + item.PatientId + "\" style=\"display:block !important\" > ");
                        sb.Append("<span class=\"check\"><input type=\"checkbox\" name=\"\" class=\"checkbox_add_patient\" id=\"chkPatient\" value=\"" + item.PatientId + "\"></span>");
                        sb.Append("<span class=\"thumb\"><img style=\"height: 95px;width: 85px;\" alt=\"\" src='" + ((!string.IsNullOrEmpty(item.ImageName) ? item.ImageName : (item.Gender.ToString() == "Female") ? System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"] : System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorImage"])) + "' ></span> <span class=\"description\"><h2><a  style=\"word-wrap: break-word;\" href=\"javascript:;\" onclick=\"ViewPatientHistory('" + item.PatientId + "');\">" + item.FirstName + "&nbsp;" + item.LastName + "</a></h2></span>");
                        sb.Append("<span class=\"clear\"></span></div></li>");
                    }

                    sb.Append("</ul><div class=\"clear\"></div><br /><div id=\"EmptyRecord\"></div><div class=\"actions\"><a href=\"JavaScript:;\" onclick=\"DashboardReferralSend()\" ><img alt=\"\" class=\"btn-popup pull-right\" src=\"../../Content/images/btn-next.png\"/></a><input class=\"New_cancel_btn btn-popup \" name=\"Cancel\" type=\"submit\" onclick=\"CloseEditPatientListpop()\" value=\"Cancel\"><div class=\"clear\">");
                }
                else
                {
                    sb.Append("<center>Patient not found. Please<a href=\"" + Url.Action("AddPatient", "Patients") + "\" style=\"color: #0890c4;\"> click here</a> to add new patient.</center>");
                }

                sb.Append("</div><button class=\"mfp-close\" type=\"button\" title=\"Close (Esc)\">×</button>");
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        public string DashBoardSearchPatientListForSendReferralPopUpLI(string SearchText, string pageindex, string pagesize)
        {

            return GetDashBoardPatientListForSendReferralPopUpLI(SearchText, int.Parse(pageindex), int.Parse(pagesize));
        }

        //Get Only LI Containt 
        public string GetDashBoardPatientListForSendReferralPopUpLI(string SearchText, int pageindex, int pagesize)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                MdlPatient = new mdlPatient();
                MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), pageindex, pagesize, SearchText, 11, 2, null, 0);

                if (MdlPatient.lstPatientDetailsOfDoctor.Count > 0)
                {
                    foreach (var item in MdlPatient.lstPatientDetailsOfDoctor)
                    {
                        sb.Append("<li><div class=\"clearfix\">");
                        sb.Append("<span class=\"check\"><input type=\"checkbox\" name=\"\" class=\"checkbox_add_patient\" id=\"chkPatient\" value=\"" + item.PatientId + "\"></span>");
                        sb.Append("<span class=\"thumb\"><img style=\"height: 95px;width: 85px;\" alt=\"\" src='" + ((!string.IsNullOrEmpty(item.ImageName) ? item.ImageName : (item.Gender.ToString() == "Female") ? System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"] : System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorImage"])) + "' ></span> <span class=\"description\"><h2><a  style=\"word-wrap: break-word;\" href=\"javascript:;\" onclick=\"ViewPatientHistory('" + item.PatientId + "');\">" + item.FirstName + "&nbsp;" + item.LastName + "</a></h2></span>");
                        sb.Append("<span class=\"clear\"></span></div></li>");
                    }
                }
                else if (pageindex == 1)
                {
                    sb.Append("<center>No Record Found</center>");
                }
                else
                {
                    sb.Append("<center>No More Record Found</center>");
                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        /// <summary>
        ///Method for Loading Dashboard data for the Dentist.
        /// </summary>
        /// <returns>Inbox,Patients and Colleague details of a patients</returns>
        [CustomAuthorize]
        public ActionResult dashboard(string userid)
        {
            TempData.Keep("PMSReferralError");
            ViewBag.IsFromOCR = Convert.ToString(TempData["OpenReferral"]);
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                bool Result = objCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
                DataTable dtCheckFirst = new DataTable();
                dtCheckFirst = ObjColleaguesData.GetFirstLoginCount(Convert.ToInt32(SessionManagement.UserId));
                if (dtCheckFirst.Rows.Count > 0)
                {
                    if (DentistBLL.CheckUserHasEnableOCR(Convert.ToInt32(SessionManagement.UserId)) && string.IsNullOrEmpty(Convert.ToString(TempData["OpenReferral"])))
                    {
                        DataTable Dt = clsColleaguesData.CheckUserHasEnableOneClick(Convert.ToInt32(SessionManagement.UserId));
                        bool sent = false, receive = false;
                        if (Dt != null && Dt.Rows.Count > 0)
                        {
                            sent = Convert.ToBoolean(Dt.Rows[0]["IsSendReferral"]);
                            receive = Convert.ToBoolean(Dt.Rows[0]["IsReceiveReferral"]);
                        }
                        ViewBag.Sent = sent;
                        ViewBag.Receiver = receive;
                        return View("OCRDashboard");
                    }
                    else
                    {
                        clsRestriction objRestriction = new clsRestriction();
                        PatientBLL patientBll = new PatientBLL();
                        MDLDashboard mdldashboard = new MDLDashboard();
                        //int UsrId = Convert.ToInt32(SessionManagement.UserId);
                        //Obj.Id = UsrId;
                        //FilterObj.UserId = UsrId;
                        //mdldashboard.InboxList = InboxDetailBLL.NewGetInboxOfDoctorGrid(Convert.ToInt32(Session["UserId"]), 1, 1, 2, null, null, SessionManagement.TimeZoneSystemName);
                        //mdldashboard.lstColleaguesDetails = ColleaguesBLL.DashboardColleagueDetails(Obj);
                        //mdldashboard.TotalColleague = mdldashboard.lstColleaguesDetails.Count > 0 ? mdldashboard.lstColleaguesDetails[0].TotalRecord : 0;
                        mdldashboard.TotalColleague = objRestriction.GetCountOfColleaguesByUserId(Convert.ToInt32(SessionManagement.UserId));
                        //mdldashboard.lstPatients = PatientBLL.PatientListOfDoctor(FilterObj);
                        //mdldashboard.TotalPatients = mdldashboard.lstPatients.Count > 0 ? mdldashboard.lstPatients[0].TotalRecord : 0;
                        mdldashboard.TotalPatients = patientBll.NewGetPatientCountOfDoctor(Convert.ToInt32(SessionManagement.UserId), "", "", 2, 0);

                        return View("_Index", mdldashboard);
                    }
                }
                return RedirectToAction("Index", "Inbox", new { newuser = userid });
            }
            else
            {
                return RedirectToAction("Index", "User");
            }           
        }
        public ActionResult NewDashbordInboxlist(int PageIndex, int SortColumn, int SortDirection)
        {
            List<BO.ViewModel.InboxMessages> lst = new List<BO.ViewModel.InboxMessages>();
            InboxDetailBLL objinbox = new InboxDetailBLL();
            lst = objinbox.GetInboxOfDoctor(Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(PageIndex),10, SortColumn, SortDirection, null, false, SessionManagement.TimeZoneSystemName);
            return View("_PartialDashboardInboxItems",lst);
        }
       

        //New Method For Search Colleagues Load Time.
        public ActionResult NewGetListSearchColleagueFromPopUp(string SearchText)
        {
            ColleaguesBLL colleagueBLL = new ColleaguesBLL();
            colleagueBLL.lstColleague = colleagueBLL.ColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), 1, 16, SearchText, 11, 2, null, 0);
            return View("_PartialSendReferralPopUp",colleagueBLL.lstColleague);
        }

        public ActionResult NewSearchColleagueFromPopUp(string SearchText)
        {
            ColleaguesBLL colleagueBLL = new ColleaguesBLL();
            colleagueBLL.lstColleague = colleagueBLL.ColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), 1, 16, SearchText, 11, 2, null, 0);
            return View("_PartialSearchColleagueItem", colleagueBLL.lstColleague);
        }

        //Scroll
        public ActionResult NewGetColleagueListForSendReferralPopUpOnScroll(int PageIndex, string SearchText = null)
        {
            ColleaguesBLL colleagueBLL = new ColleaguesBLL();
            colleagueBLL.lstColleague = colleagueBLL.ColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, 16, SearchText, 11, 2, null, 0);
            return View("_PartialSearchColleagueItem",colleagueBLL.lstColleague);
        }



        //Patient
        public ActionResult NewSearchPatientFromItem(string SearchText)
        {
            string searchValue = "Find patient to refer...";
            PatientFilter FilterObj = new PatientFilter();
            FilterObj.SearchText = SearchText;
            FilterObj.UserId = Convert.ToInt32(Session["UserId"]);
            FilterObj.PageIndex = 1; FilterObj.PageSize = 30;
            FilterObj.SortColumn = 11;FilterObj.SortDirection = 2;
            PatientBLL patientBLL = new PatientBLL();
            patientBLL.lstPatientDetailsOfDoctor = PatientBLL.NewGetPatientDetailsOfDoctor(FilterObj);
            return View("_PartialSearchPatientItem", patientBLL.lstPatientDetailsOfDoctor);
        }

        // Get Patient list on scroll
        public ActionResult NewGetPatientListForSendReferralPopUpLI(string SearchText, int pageindex, int pagesize)
        {
            PatientBLL patientBLL = new PatientBLL();
            PatientFilter FilterObj = new PatientFilter();
            FilterObj.UserId = Convert.ToInt32(Session["UserId"]);
            FilterObj.SearchText = SearchText; FilterObj.PageIndex = pageindex;
            FilterObj.PageSize = pagesize;FilterObj.SortColumn = 11;
            FilterObj.SortDirection = 2;
            patientBLL.lstPatientDetailsOfDoctor = PatientBLL.NewGetPatientDetailsOfDoctor(FilterObj);
            return View("_PartialSearchPatientItem", patientBLL.lstPatientDetailsOfDoctor);
        }

        public ActionResult NewGetPatientDetailsList(PatientFilter FilterObj)
        {
            FilterObj.UserId = Convert.ToInt32(Session["UserId"]);
            FilterObj.PageSize = 10;
            if (FilterObj.NewSearchtext == "All")
            {
                FilterObj.NewSearchtext = string.Empty;
                FilterObj.SortColumn = 2;
            }
            if (FilterObj.SearchText == "Search Patient")
            {
                FilterObj.SearchText = null;
                FilterObj.SortColumn = 2;
                FilterObj.SortDirection = 1;
            }
            return View("_PartialDashboardPatientList", PatientBLL.NewGetPatientDetailsOfDoctor(FilterObj));
        }
        [HttpPost]
        public JsonResult SearchPatient(string SearchName)
        {
            clsPatientsData ObjPatientsData = new clsPatientsData();
            DataTable dt = new DataTable();
            dt = ObjPatientsData.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), 1, 10, SearchName, 11, 2, SearchName, 1, 0);
            StringBuilder sb= new StringBuilder();
            List<PatientDetails> Patientlst = new List<PatientDetails>();
            if (dt != null && dt.Rows.Count > 0)
            {
                Patientlst = (from p in dt.AsEnumerable()
                              select new PatientDetails
                              {
                                  FirstName = objCommon.CheckNull(Convert.ToString(p["FirstName"]), string.Empty),
                                  LastName = objCommon.CheckNull(Convert.ToString(p["LastName"]), string.Empty),
                                  Email = objCommon.CheckNull(Convert.ToString(p["Email"]), string.Empty)
                              }).ToList();
                //foreach (DataRow item in dt.Rows)
                //{

                //        if (Convert.ToString(item["Email"])!= "")
                //            sb.Append("<option value='" + item["FirstName"]+" "+ item["LastName"] + "(" + item["Email"] + ")' data-id="+ item["PatientId"] + "><br/>");
                //        else
                //        sb.Append("<option value='" + item["FirstName"] +" " +item["LastName"] + "' onchange=onselect(" + item["PatientId"] + ");><br/>");
                //    //else
                //    //    sb.Append("<option value='" + item.FirstName + "(" + item.ColleagueId+ ")' onchange=GetDetail(" + item.ColleagueId + ");><br/>");

                //}
            }
            return Json(sb.ToString(),JsonRequestBehavior.AllowGet);
        }

        //Location partial view
        public ActionResult LocationList(string UserId)
        {                 
           List<BO.ViewModel.AddressDetails> lst = new List<BO.ViewModel.AddressDetails>();
           lst = ColleaguesBLL.GetDentistLocationsById(UserId);
            if (lst.Count == 1)
            {
                return Json(1);
            }
            else
            {
                return View("_PartialLocationItem", lst);
            }                  
        }

        public ActionResult ReferPaymentFromPopUp(int PatientId)
        {
            TempData["PatientId"] = PatientId;
            return PartialView("_PartialRequestPayment");
        }
        #region START OCR DASHBOARD RELATED CODE FOR XQ1-249 CHANGES
        [CustomAuthorize]
        public ActionResult Dashboards()
        {
            DataTable Dt = clsColleaguesData.CheckUserHasEnableOneClick(Convert.ToInt32(SessionManagement.UserId));
            bool sent = false, receive = false;
            if (Dt != null && Dt.Rows.Count > 0)
            {
                sent = Convert.ToBoolean(Dt.Rows[0]["IsSendReferral"]);
                receive = Convert.ToBoolean(Dt.Rows[0]["IsReceiveReferral"]);
            }
            ViewBag.Sent = sent;
            ViewBag.Receiver = receive;
            return View("OCRDashboard");
        }
        public ActionResult DeleteReferral(DeleteMessage Obj)
        {
            return Json(ReferralFormBLL.DeleteMessageOfOCRFromRL(Obj,Convert.ToInt32(SessionManagement.UserId)), JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetOCRReferrals(ReferralFilter referralFilter)
        {
            referralFilter.UserId = Convert.ToInt32(SessionManagement.UserId);
            List<OneClickReferralDetails> oneClickReferralDetails = new PatientReport().GetOneClickReferralHistoryById(referralFilter);
            return View("GetReceivedReferral", oneClickReferralDetails);
        }
        public ActionResult GetDispostionList()
        {
            return Json(new PatientReport().GetDispositionlist(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult ChangeDisposionStatus(UpdateDisposition Obj)
        {
            return Json(PatientReport.UpdateDispositionStatus(Obj.DispositionId, Obj.MessageId, Convert.ToInt32(SessionManagement.UserId)), JsonRequestBehavior.AllowGet);
        }
        #endregion
    }

}
