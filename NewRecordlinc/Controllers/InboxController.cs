﻿using BO.ViewModel;
using BusinessLogicLayer;
using ICSharpCode.SharpZipLib.Zip;
using NewRecordlinc.App_Start;
using System;
using System.Collections.Generic;
using System.Configuration;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.PatientsData;
using DataAccessLayer.Common;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewRecordlinc.Common;
using System.Drawing;
using NewRecordlinc.Models.Colleagues;
using BO.Models;

namespace NewRecordlinc.Controllers
{
    public class InboxController : Controller
    {
        DataTable getFilenames = new DataTable();
        clsCommon ObjCommon = new clsCommon();
        clsTemplate ObjTemplate = new clsTemplate();
        //string TempFolderPath = System.Configuration.ConfigurationManager.AppSettings["TempUploadsFolderPath"];
        //string DraftFolderPath = System.Configuration.ConfigurationManager.AppSettings["DraftAttachments"];
        //string ImageBankFolderPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("ImageBank"));
        //string TempPathThumb = System.Configuration.ConfigurationManager.AppSettings["ThumbPath"];

        public static string NewTempFileUpload = System.Configuration.ConfigurationManager.AppSettings["TempFileUpload"];
        public static string ImageBankFileUpload = System.Configuration.ConfigurationManager.AppSettings["ImageBankFileUpload"];
        public static string ThumbFileUpload = System.Configuration.ConfigurationManager.AppSettings["ThumbFileUpload"];
        public static string DraftFileUpload = System.Configuration.ConfigurationManager.AppSettings["DraftFileUpload"];

        [CustomAuthorize]
        public ActionResult Draft()
        {
            return View();
        }
        public ActionResult ComposeMessage(string ColleaguesId = null, int PatientId = 0, int MessageTypeId = 0)
        {
            ComposeMessage objcomposedetails = new ComposeMessage();
            MessagesBLL objMessageBLL = new MessagesBLL();
            if (!string.IsNullOrWhiteSpace(ColleaguesId))
            {
                objcomposedetails.ColleagueId = Convert.ToString(ColleaguesId);
                objcomposedetails.ColleagueList = objMessageBLL.GetColleagueList(Convert.ToInt32(SessionManagement.UserId), null, objcomposedetails.ColleagueId);
            }
            if (PatientId > 0)
            {
                objcomposedetails.PatientId = Convert.ToString(PatientId);
                objcomposedetails.PatientList = objMessageBLL.GetPatientList(Convert.ToInt32(SessionManagement.UserId), null, objcomposedetails.PatientId);
            }

            //objcomposedetails.MessageTypeId = MessageTypeId;
            TempData["objcomposedetails"] = objcomposedetails;
            return RedirectToAction("Compose", "Inbox");
        }

        [CustomAuthorize]
        public ActionResult Compose()
        {
            ComposeMessage objcomposedetails = new ComposeMessage();
            if (TempData["objcomposedetails"] != null)
            {
                TempData.Keep();
                objcomposedetails = (ComposeMessage)TempData["objcomposedetails"];
            }
            return View(objcomposedetails);
        }
        [CustomAuthorize]
        [HttpPost]
        public ActionResult Compose(ComposeMessage objcomposedetails)
        {
            MessagesBLL objMessageBLL = new MessagesBLL();
            objcomposedetails.ColleagueId = Convert.ToString(objcomposedetails.ColleagueId);
            objcomposedetails.ColleagueList = objMessageBLL.GetColleagueList(Convert.ToInt32(SessionManagement.UserId), null, objcomposedetails.ColleagueId);
            TempData["objcomposedetails"] = objcomposedetails;
            return RedirectToAction("Compose", "Inbox");
        }
        [CustomAuthorize]
        public ActionResult PartialDraftMessages(string PageIndex, string PageSize, string ShortColumn, string ShortDirection, string SearchText, int MessageDisplayType)
        {
            InboxDetailBLL objMessagebll = new InboxDetailBLL();
            List<InboxMessages> lstMessagelistofDoctor = new List<InboxMessages>();
            lstMessagelistofDoctor = objMessagebll.GetDraftofDoctor(Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(PageIndex), Convert.ToInt32(PageSize), Convert.ToInt32(ShortColumn), Convert.ToInt32(ShortDirection), SearchText, false, SessionManagement.TimeZoneSystemName);
            return View(lstMessagelistofDoctor);
        }
        [CustomAuthorize]
        public ActionResult PartialInBoxMessages(string PageIndex, string PageSize, string ShortColumn, string ShortDirection, string SearchText, int MessageDisplayType,bool IsDashboard=false)
        {
            InboxDetailBLL objMessagebll = new InboxDetailBLL();
            List<InboxMessages> lstMessagelistofDoctor = new List<InboxMessages>();
            switch (MessageDisplayType)
            {
                ////This is for Draft listing on page.
                //case (int)BO.Enums.Common.DisplayMessageType.Draft:
                //    lstMessagelistofDoctor = objMessagebll.GetDraftofDoctor(Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(PageIndex), Convert.ToInt32(PageSize), Convert.ToInt32(ShortColumn), Convert.ToInt32(ShortDirection), SearchText, false, SessionManagement.TimeZoneSystemName);
                //    break;
                //This is for Inbox listing on page.
                case (int)BO.Enums.Common.DisplayMessageType.Inbox:
                    lstMessagelistofDoctor = objMessagebll.GetInboxOfDoctor(Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(PageIndex), Convert.ToInt32(PageSize), Convert.ToInt32(ShortColumn), Convert.ToInt32(ShortDirection), SearchText, false, SessionManagement.TimeZoneSystemName);
                    break;
                //This is for SentBox listing on page.
                case (int)BO.Enums.Common.DisplayMessageType.SentBox:

                    lstMessagelistofDoctor = objMessagebll.GetSentBoxOfDoctor(Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(PageIndex), Convert.ToInt32(PageSize), Convert.ToInt32(ShortColumn), Convert.ToInt32(ShortDirection), SearchText, false, SessionManagement.TimeZoneSystemName);
                    ViewBag.endtime = DateTime.Now;
                    break;
                //This is for ReferralReceive listing on page.
                case (int)BO.Enums.Common.DisplayMessageType.ReceiveReferral:
                    lstMessagelistofDoctor = objMessagebll.GetReceiveReferralOfDoctor(Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(PageIndex), Convert.ToInt32(PageSize), Convert.ToInt32(ShortColumn), Convert.ToInt32(ShortDirection), SearchText, SessionManagement.TimeZoneSystemName);
                    break;
                //This is for ReferralSent listing on page.
                case (int)BO.Enums.Common.DisplayMessageType.SentReferral:
                    lstMessagelistofDoctor = objMessagebll.GetSentReferralOfDoctor(Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(PageIndex), Convert.ToInt32(PageSize), Convert.ToInt32(ShortColumn), Convert.ToInt32(ShortDirection), SearchText, SessionManagement.TimeZoneSystemName);
                    break;
            }
            if (IsDashboard)
            {
                return View("_PartialDashboardInboxItems", lstMessagelistofDoctor);
            }
            else
            {
                return View(lstMessagelistofDoctor);
            }
           
        }
        [CustomAuthorize]
        public ActionResult PartialComposeMessage(ComposeMessage objcomposedetails)
        {
            switch (objcomposedetails.MessageTypeId)
            {
                case (int)BO.Enums.Common.ComposeMessageType.Replay:
                    ViewBag.Title = "Reply";
                    MessagesBLL Message = new MessagesBLL();
                    objcomposedetails = Message.GetCommopseMessgeDetails(objcomposedetails.MessageId, (int)BO.Enums.Common.ComposeMessageType.Replay, Convert.ToInt32(SessionManagement.UserId),objcomposedetails.Issent);
                    TempData["objcomposedetails"] = objcomposedetails;
                    break;
                case (int)BO.Enums.Common.ComposeMessageType.Compose:
                    ViewBag.Title = "Compose Message";
                    objcomposedetails.ComposeType = (int)BO.Enums.Common.ComposeType.ColleagueCompose;
                    break;
                case (int)BO.Enums.Common.ComposeMessageType.Draft:
                    ViewBag.Title = "Compose Message";
                    objcomposedetails.ComposeType = (int)BO.Enums.Common.ComposeType.ColleagueCompose;
                    break;


            }
            return View("PartialComposeMessage", objcomposedetails);
        }
        [CustomAuthorize]
        public JsonResult GetColleagueList()
        {
            string SearchText = string.Empty;
            if (Convert.ToString(Request["q"]) != null)
            {
                SearchText = Convert.ToString(Request["q"]);
            }
            string Selectedid = string.Empty;

            MessagesBLL Message = new MessagesBLL();
            List<ColleagueData> objcolleagueList = new List<ColleagueData>();
            objcolleagueList = Message.GetColleagueList(Convert.ToInt32(SessionManagement.UserId), SearchText);
            return Json(objcolleagueList, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorize]
        public JsonResult GetPatientList()
        {
            MessagesBLL Message = new MessagesBLL();
            string SearchText = string.Empty;
            if (Convert.ToString(Request["q"]) != null)
            {
                SearchText = Convert.ToString(Request["q"]);
            }
            List<PatientsData> objpatientList = new List<PatientsData>();
            objpatientList = Message.GetPatientList(Convert.ToInt32(SessionManagement.UserId), SearchText);
            return Json(objpatientList, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorize]
        public ActionResult NewGetMessageDetails(int MessageId, int MessageDisplayType, int MessageTypeId, int IsColleagueMesage = 0, bool Isread = false,int IsDashboard=0 ,int IsDashboardForward=0)
        {
            //ViewBag.CurrentLink = Request.UrlReferrer.ToString();
            GetMessageDetails objGetMessageDetails = new GetMessageDetails();
            objGetMessageDetails.MessageId = MessageId;
            objGetMessageDetails.MessageDisplayType = MessageDisplayType;
            objGetMessageDetails.MessageTypeId = MessageTypeId;
            objGetMessageDetails.IsColleagueMesage = IsColleagueMesage;
            objGetMessageDetails.Isread = Isread;
            TempData["GetMessageDetails"] = objGetMessageDetails;
            int IsDash = (IsDashboard == 1) ? 1 : 0;
            TempData["IsDashboard"] = IsDash;
            int IsDashForward = (IsDashboardForward == 1) ? 1 : 0;
            TempData["IsDashForward"] = IsDashForward;
            TempData.Keep();
            return RedirectToAction("GetMessageDetails");
        }
        [CustomAuthorize]
        public ActionResult GetMessageDetails()
        {
            ViewBag.dashboard = Convert.ToInt32(TempData["IsDashboard"]);
            ViewBag.dashforward = Convert.ToInt32(TempData["IsDashForward"]);
            if (!string.IsNullOrWhiteSpace(Convert.ToString(Request.UrlReferrer)))
            {
                ViewBag.CurrentLink = Convert.ToString(Request.UrlReferrer);
            }
            GetMessageDetails ObjGetMessageDetails = new GetMessageDetails();

            if (TempData["GetMessageDetails"] != null)
            {
                ObjGetMessageDetails = (GetMessageDetails)TempData["GetMessageDetails"];
                TempData.Keep();
            }
            //This condtion for normal messages.
            if (ObjGetMessageDetails.MessageTypeId != 2)
            {
               
                MessagesBLL Message = new MessagesBLL();
                NormalMessage Model = new NormalMessage();
                bool Result = false;
                if ((ObjGetMessageDetails.MessageDisplayType == 2 || ObjGetMessageDetails.MessageDisplayType == 3) && ObjGetMessageDetails.MessageId > 0 && !ObjGetMessageDetails.Isread)
                {
                    if (ObjGetMessageDetails.IsColleagueMesage > 0)
                    {
                        Result = Message.UpdateMessageReadStatus(null, ObjGetMessageDetails.MessageId);
                    }
                    else
                    {
                        Result = Message.UpdateMessageReadStatus(ObjGetMessageDetails.MessageId, null);
                    }
                }
                switch (ObjGetMessageDetails.MessageDisplayType)
                {
                    case (int)BO.Enums.Common.DisplayMessageType.Draft:
                        ComposeMessage objcomposedetails = new ComposeMessage();
                        if (ObjGetMessageDetails.IsColleagueMesage > 0)
                        {
                            objcomposedetails = Message.GetCommopseMessgeDetails(ObjGetMessageDetails.MessageId, (int)BO.Enums.Common.ComposeMessageType.Draft, Convert.ToInt32(SessionManagement.UserId));
                            TempData["objcomposedetails"] = objcomposedetails;
                            return RedirectToAction("Compose", "Inbox");
                        }
                        else {
                            objcomposedetails = Message.GetPatientCommopseMessgeDetails(ObjGetMessageDetails.MessageId, (int)BO.Enums.Common.ComposeMessageType.Draft, Convert.ToInt32(SessionManagement.UserId));
                            TempData["objcomposedetails"] = objcomposedetails;
                            return RedirectToAction("PatientMessageCompose", "Patients");
                        }
                    case (int)BO.Enums.Common.DisplayMessageType.Inbox:
                        Model.MessageList = Message.GetPlainMessageBodyByMessageId(Convert.ToInt32(SessionManagement.UserId), ObjGetMessageDetails.MessageId, ObjGetMessageDetails.MessageTypeId, SessionManagement.TimeZoneSystemName);
                        break;
                    case (int)BO.Enums.Common.DisplayMessageType.SentBox:
                        Model.MessageList = Message.GetSentBoxMessageDetailsByMessageId(Convert.ToInt32(SessionManagement.UserId), ObjGetMessageDetails.MessageId, ObjGetMessageDetails.MessageTypeId, SessionManagement.TimeZoneSystemName);
                        break;
                }
                return View(Model.MessageList.Take(1).ToList());
            }
            //This condtion for Referral messages.
            else
            {
                MessagesBLL Message = new MessagesBLL();
                ReferralMessage Model = new ReferralMessage();
                bool check = false;
                bool Result = false;
                if (ObjGetMessageDetails.MessageDisplayType == 2 || ObjGetMessageDetails.MessageDisplayType == 4)
                {
                    check = true;
                }
                if (check && ObjGetMessageDetails.MessageId > 0 && !ObjGetMessageDetails.Isread)
                {
                    Result = Message.UpdateMessageReadStatus(null, ObjGetMessageDetails.MessageId);
                }
                switch (ObjGetMessageDetails.MessageDisplayType)
                {
                    case (int)BO.Enums.Common.DisplayMessageType.Inbox:
                    case (int)BO.Enums.Common.DisplayMessageType.ReceiveReferral:
                        Model.ReferralMessageList = Message.GetReferralMessageBodyDetails(Convert.ToInt32(SessionManagement.UserId), ObjGetMessageDetails.MessageId, "Inbox", SessionManagement.TimeZoneSystemName);
                        break;
                    case (int)BO.Enums.Common.DisplayMessageType.SentBox:
                    case (int)BO.Enums.Common.DisplayMessageType.SentReferral:
                        Model.ReferralMessageList = Message.GetReferralMessageBodyDetails(Convert.ToInt32(SessionManagement.UserId), ObjGetMessageDetails.MessageId, "SentBox", SessionManagement.TimeZoneSystemName);
                        break;
                }
                return View("GetReferralMessageDetails", Model.ReferralMessageList.Take(1).ToList());
            }
        }
        public ActionResult SentMessageDetails(int MessageId, int MessageTypeId)
        {
            MessagesBLL Message = new MessagesBLL();
            NormalMessage Model = new NormalMessage();
            Model.MessageList = Message.GetSentBoxMessageDetailsByMessageId(Convert.ToInt32(SessionManagement.UserId), MessageId, MessageTypeId, SessionManagement.TimeZoneSystemName);
            return View("GetMessageDetails", Model.MessageList.Take(1).ToList());
        }
        public ActionResult PartialFileUpload(string Ids)
        {
            return View("PartialFileUploaded");
        }
        public bool DeleteMessageByMessageId(int MessageId, string MessageType)
        {
            bool Result = false;
            clsColleaguesData CLScolleagues = new clsColleaguesData();
            if (MessageType == "Inbox")
            {
                Result = CLScolleagues.RemoveMessageFromInboxOfDoctor(Convert.ToInt32(SessionManagement.UserId), MessageId);
            }
            else
            {
                Result = CLScolleagues.RemoveMessageFromSentBoxOfDoctor(Convert.ToInt32(SessionManagement.UserId), MessageId);
            }

            return Result;
        }
        public bool DeletePatientMessage(int MessageId)
        {
            bool Result = false;
            clsPatientsData clsPatientData = new clsPatientsData();
            Result = clsPatientData.RemovePatientMessageByDoctor(MessageId);
            return Result;
        }
        public bool DeleteSentBoxMessage(int MessageId)
        {
            bool Result = false;
            clsColleaguesData clsColleaguData = new clsColleaguesData();
            Result = clsColleaguData.RemoveMessageFromSentBoxOfDoctor(Convert.ToInt32(SessionManagement.UserId), MessageId);
            return Result;
        }
        [HttpPost]
        public JsonResult RemoveMessageByID(string[] MessageId, int MessageDisplayType)
        {
            InboxDetailBLL objMessagebll = new InboxDetailBLL();
            CommonBLL ObjCommonbll = new CommonBLL();
            bool Status = false;
            string str = String.Join(",", MessageId);
            TypesofMessage objtype = new TypesofMessage();
            switch (MessageDisplayType)
            {
                case (int)BO.Enums.Common.DisplayMessageType.Inbox:
                case (int)BO.Enums.Common.DisplayMessageType.ReceiveReferral:
                    objtype = ObjCommonbll.GetColleaguePAtientList(str);
                    Status = objMessagebll.DeleteInboxMessage(string.Join(",", objtype.PatientMessageId), string.Join(",", objtype.ColleagueMessageId), Convert.ToInt32(SessionManagement.UserId));
                    break;
                case (int)BO.Enums.Common.DisplayMessageType.SentBox:
                case (int)BO.Enums.Common.DisplayMessageType.SentReferral:
                    objtype = ObjCommonbll.GetColleaguePAtientList(str);
                    Status = objMessagebll.DeleteSentBoxMessage(string.Join(",", objtype.PatientMessageId), string.Join(",", objtype.ColleagueMessageId), Convert.ToInt32(SessionManagement.UserId));
                    break;
            }
            return Json(Status, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult RemoveDraftMessageByID(string[] MessageId, int MessageDisplayType)
        {
            InboxDetailBLL objMessagebll = new InboxDetailBLL();
            CommonBLL ObjCommonbll = new CommonBLL();
            bool Status = false;
            string str = String.Join(",", MessageId);
            switch (MessageDisplayType)
            {
                case (int)BO.Enums.Common.DisplayMessageType.Draft:
                    List<TempFileAttacheMents> objlistofattachements = new List<TempFileAttacheMents>();
                    TypesofMessage objtype = new TypesofMessage();
                    objtype = ObjCommonbll.GetColleaguePAtientList(str);
                    objlistofattachements = ObjCommonbll.GetDraftAttachmentsByMultiMessage(string.Join(",", objtype.ColleagueMessageId), string.Join(",", objtype.PatientMessageId));
                    DeleteMultipleDraftAttachments(objlistofattachements);
                    Status = objMessagebll.DeleteDraftMessage(string.Join(",", objtype.ColleagueMessageId), string.Join(",", objtype.PatientMessageId));
                    break;

            }
            return Json(Status, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorize]
        public ActionResult Index()
        {
            return View();
        }
        [CustomAuthorize]
        public ActionResult Sent()
        {
            ViewBag.starttime = DateTime.Now;
            return View();
        }
        [CustomAuthorize]
        public ActionResult ReceiveReferral()
        {
            return View();
        }
        [CustomAuthorize]
        public ActionResult SentReferral()
        {
            return View();
        }
        public ActionResult GetReferralMessageDetails()
        {
            return View();
        }
        public ActionResult GetPartialViewForReferralCategory(int ReferralCardId)
        {
            MessagesBLL Message = new MessagesBLL();
            ReferralMessage Model = new ReferralMessage();
            Model.ReferralCategory = Message.GetReferralCategoryValue(ReferralCardId);
            return View(Model.ReferralCategory);
        }
        [CustomAuthorize]
        public JsonResult SaveAsDraft(ComposeMessage objcomposedetail)
        {
            MessagesBLL objmessagebll = new MessagesBLL();
            CommonBLL ObjCommonbll = new CommonBLL();
            int RecordId = 0;
            objcomposedetail.ComposeType = (int)BO.Enums.Common.ComposeType.ColleagueCompose;
            RecordId = objmessagebll.InsertUpdateDraftMessage(objcomposedetail, Convert.ToInt32(SessionManagement.UserId));
            List<TempFileAttacheMents> objlistofattachements = new List<TempFileAttacheMents>();
            objlistofattachements = ObjCommonbll.GetTempAttachments(objcomposedetail.FileIds);
            foreach (var item in objlistofattachements)
            {
                if (System.IO.File.Exists(NewTempFileUpload + item.FilePath))
                {
                    System.IO.File.Move(NewTempFileUpload + item.FilePath, DraftFileUpload + item.FilePath);
                    objmessagebll.InsertDraftAttachments(RecordId, item.FilePath, Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item.FileExtension), (int)BO.Enums.Common.ComposeType.ColleagueCompose);
                }

            }
            ObjCommonbll.DeleteAllAttachements(objcomposedetail.FileIds);
            return Json(RecordId > 0 ? true : false, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorize]
        public JsonResult SendMessageToColleagues(ComposeMessage objcomposedetail)
        {
            bool Result = false;
            MessagesBLL objmessagebll = new MessagesBLL();
            CommonBLL ObjCommonbll = new CommonBLL();
            List<TempFileAttacheMents> objlistofattachements = new List<TempFileAttacheMents>();
            objlistofattachements = ObjCommonbll.GetTempAttachments(objcomposedetail.FileIds, objcomposedetail.MessageId, (int)BO.Enums.Common.ComposeType.ColleagueCompose);
            string[] ColleagueIds = objcomposedetail.ColleagueId.ToString().Split(new char[] { ',' });
            if (ColleagueIds.Length > 0)
            {
                if (objcomposedetail.MessageId > 0)
                {
                    switch (objcomposedetail.MessageTypeId)
                    {
                        case (int)BO.Enums.Common.ComposeMessageType.Draft:
                            int i = 0;
                            foreach (var item in ColleagueIds)
                            {
                                int RecordId = 0;
                                if (i == 0)
                                {
                                    i++;
                                    RecordId = objmessagebll.InsertUpdateColleagueMessage("", objcomposedetail.MessageBody, Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item), 1, objcomposedetail.PatientId, 0, Convert.ToInt32(SessionManagement.UserId), objcomposedetail.MessageId);
                                    ManageAttachement(objlistofattachements, RecordId);
                                    ObjTemplate.SendMessageToColleague(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(item), RecordId, 1);
                                    if (!string.IsNullOrEmpty(objcomposedetail.PatientId)){ AddPatientToReferredDoctor(Convert.ToInt32(item), objcomposedetail.PatientId);}
                                    Result = true;
                                }
                                else {
                                    RecordId = objmessagebll.InsertUpdateColleagueMessage("", objcomposedetail.MessageBody, Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item), 1, objcomposedetail.PatientId, 0, Convert.ToInt32(SessionManagement.UserId));
                                    ManageAttachement(objlistofattachements, RecordId);
                                    ObjTemplate.SendMessageToColleague(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(item), RecordId, 1);
                                    if (!string.IsNullOrEmpty(objcomposedetail.PatientId)) { AddPatientToReferredDoctor(Convert.ToInt32(item), objcomposedetail.PatientId); }
                                    Result = true;
                                }

                            }
                            break;
                        case (int)BO.Enums.Common.ComposeMessageType.Replay:
                            foreach (var item in ColleagueIds)
                            {
                                int RecordId = 0;
                                RecordId = objmessagebll.InsertUpdateColleagueMessage("", objcomposedetail.MessageBody, Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item), 1, objcomposedetail.PatientId, 0, Convert.ToInt32(SessionManagement.UserId));
                                ManageAttachement(objlistofattachements, RecordId);
                                objmessagebll.ManageReplyForwardMessage(objcomposedetail.MessageId, RecordId, (int)BO.Enums.Common.ComposeMessageType.Replay, (int)BO.Enums.Common.ComposeType.ColleagueCompose);
                                ObjTemplate.SendMessageToColleague(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(item), RecordId, 1);
                                if (!string.IsNullOrEmpty(objcomposedetail.PatientId)) { AddPatientToReferredDoctor(Convert.ToInt32(item), objcomposedetail.PatientId); }
                                Result = true;
                            }
                            break;
                    }
                }
                else {
                    foreach (var item in ColleagueIds)
                    {
                        int RecordId = 0;
                        RecordId = objmessagebll.InsertUpdateColleagueMessage("", objcomposedetail.MessageBody, Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item), 1, objcomposedetail.PatientId, 0, Convert.ToInt32(SessionManagement.UserId));
                        ManageAttachement(objlistofattachements, RecordId);
                        ObjTemplate.SendMessageToColleague(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(item), RecordId, 1);
                        if (!string.IsNullOrEmpty(objcomposedetail.PatientId)) { AddPatientToReferredDoctor(Convert.ToInt32(item), objcomposedetail.PatientId); }
                        Result = true;
                    }
                }
                ObjCommonbll.DeleteAllAttachements(objcomposedetail.FileIds, objcomposedetail.MessageId);
                DeleteAttachmentFiles(objlistofattachements);
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }

        public bool AddPatientToReferredDoctor(int ColleagueId, string AttachPatientIds)
        {
            bool Result = false;
            string[] PatientIds = AttachPatientIds.ToString().Split(new char[] { ',' });
            MessagesBLL objmessagebll = new MessagesBLL();
            for (int i = 0; i < PatientIds.Count(); i++)
            {
                Result = objmessagebll.AddPatientToReferredDoctor(Convert.ToInt32(ColleagueId), Convert.ToInt32(PatientIds[i]));
            }
            return Result;
        }

        public bool ManageAttachement(List<TempFileAttacheMents> objlistofattachements, int RecordId = 0)
        {
            CommonBLL ObjCommonbll = new CommonBLL();
            MessagesBLL objmessagebll = new MessagesBLL();
            bool Result = false;
            if (objlistofattachements.Count > 0 && RecordId > 0)
            {
                foreach (var item in objlistofattachements)
                {
                    string NewFileName = Convert.ToString(item.FilePath);
                    NewFileName = NewFileName.Substring(NewFileName.ToString().LastIndexOf("$") + 1);
                    NewFileName = Convert.ToString("$" + System.DateTime.Now.Ticks + "$" + NewFileName);
                    switch (item.FileFrom)
                    {
                        case (int)BO.Enums.Common.FileFromFolder.Draft_DraftAttachments:
                            switch (item.FileExtension)
                            {
                                case (int)BO.Enums.Common.FileTypeExtension.File:
                                    if (System.IO.File.Exists( DraftFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Copy(DraftFileUpload + item.FilePath, ImageBankFileUpload + NewFileName);
                                        objmessagebll.InsertMessageAttachemntsForColleague(RecordId, NewFileName);
                                        Result = true;
                                    }
                                    break;
                                case (int)BO.Enums.Common.FileTypeExtension.Image:
                                    if (System.IO.File.Exists(DraftFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Copy(DraftFileUpload + item.FilePath, ImageBankFileUpload + NewFileName);
                                        using (Image image = Image.FromFile(DraftFileUpload + item.FilePath))
                                        {
                                            Size thumbnailSize = ObjCommon.GetThumbnailSize(image);
                                            using (System.Drawing.Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width, thumbnailSize.Height, null, IntPtr.Zero))
                                            {

                                                thumbnail.Save(ThumbFileUpload + NewFileName);
                                            }
                                        }

                                        objmessagebll.InsertMessageAttachemntsForColleague(RecordId, NewFileName);
                                        Result = true;
                                    }
                                    break;
                            }
                            break;
                        case (int)BO.Enums.Common.FileFromFolder.TempFile_TempFolder:
                        case (int)BO.Enums.Common.FileFromFolder.TempImage_TempFolder:
                            switch (item.FileExtension)
                            {
                                case (int)BO.Enums.Common.FileTypeExtension.File:
                                    if (System.IO.File.Exists(NewTempFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Copy(NewTempFileUpload + item.FilePath, ImageBankFileUpload + NewFileName);
                                        objmessagebll.InsertMessageAttachemntsForColleague(RecordId, NewFileName);
                                        Result = true;
                                    }
                                    break;
                                case (int)BO.Enums.Common.FileTypeExtension.Image:
                                    if (System.IO.File.Exists(NewTempFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Copy(NewTempFileUpload + item.FilePath, ImageBankFileUpload + NewFileName);
                                        using (Image image = Image.FromFile(NewTempFileUpload + item.FilePath))
                                        {
                                            Size thumbnailSize = ObjCommon.GetThumbnailSize(image);
                                            using (System.Drawing.Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width, thumbnailSize.Height, null, IntPtr.Zero))
                                            {
                                                thumbnail.Save(ThumbFileUpload + NewFileName);
                                            }
                                        }
                                        objmessagebll.InsertMessageAttachemntsForColleague(RecordId, NewFileName);
                                        Result = true;
                                    }
                                    break;
                            }

                            break;
                    }
                }
            }
            return Result;
        }
        public bool DeleteAttachmentFiles(List<TempFileAttacheMents> objlistofattachements)
        {
            bool Result = false;
            if (objlistofattachements.Count > 0)
            {
                foreach (var item in objlistofattachements)
                {
                    switch (item.FileFrom)
                    {
                        case (int)BO.Enums.Common.FileFromFolder.Draft_DraftAttachments:
                            switch (item.FileExtension)
                            {
                                case (int)BO.Enums.Common.FileTypeExtension.File:
                                    if (System.IO.File.Exists(DraftFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Delete(DraftFileUpload + item.FilePath);
                                        Result = true;
                                    }
                                    break;
                                case (int)BO.Enums.Common.FileTypeExtension.Image:
                                    if (System.IO.File.Exists(DraftFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Delete(DraftFileUpload + item.FilePath);
                                        Result = true;
                                    }
                                    break;
                            }
                            break;
                        case (int)BO.Enums.Common.FileFromFolder.TempFile_TempFolder:
                        case (int)BO.Enums.Common.FileFromFolder.TempImage_TempFolder:
                            switch (item.FileExtension)
                            {
                                case (int)BO.Enums.Common.FileTypeExtension.File:
                                    if (System.IO.File.Exists(NewTempFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Delete(NewTempFileUpload + item.FilePath);
                                        Result = true;
                                    }
                                    break;
                                case (int)BO.Enums.Common.FileTypeExtension.Image:
                                    if (System.IO.File.Exists(NewTempFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Delete(NewTempFileUpload + item.FilePath);
                                        Result = true;
                                    }
                                    break;
                            }

                            break;
                    }
                }
            }
            return Result;
        }
        public bool DeleteMultipleDraftAttachments(List<TempFileAttacheMents> objlistofattachements)
        {
            bool Result = false;
            if (objlistofattachements.Count > 0)
            {
                foreach (var item in objlistofattachements)
                {
                    switch (item.FileExtension)
                    {
                        case (int)BO.Enums.Common.FileTypeExtension.File:
                            if (System.IO.File.Exists(DraftFileUpload + item.FilePath))
                            {
                                System.IO.File.Delete(DraftFileUpload + item.FilePath);
                                Result = true;
                            }
                            break;
                        case (int)BO.Enums.Common.FileTypeExtension.Image:
                            if (System.IO.File.Exists(DraftFileUpload + item.FilePath))
                            {
                                System.IO.File.Delete(DraftFileUpload+ item.FilePath);
                                Result = true;
                            }
                            break;
                    }

                }
            }
            return Result;

        }
        public ActionResult GetUnreadMessageCount(int UserId)
        {
            MessagesBLL Message = new MessagesBLL();
            UnreadMessageCount Model = new UnreadMessageCount();
            Model.Count = Message.GetUnreadMessageCountOfDoctor(UserId);
            return View("UnreadMessageCount", Model);
        }

        [CustomAuthorize]
        public ActionResult PartialColleagueForwardMessage(ComposeMessage objcomposedetails)
        {
            MessagesBLL Message = new MessagesBLL();
            ViewBag.Title = "Forward";
            objcomposedetails = Message.GetCommopseMessgeDetails(objcomposedetails.MessageId, (int)BO.Enums.Common.ComposeMessageType.Forward, Convert.ToInt32(SessionManagement.UserId));
            TempData["objcomposedetails"] = objcomposedetails;
            return View("PartialColleagueForwardMessage", objcomposedetails);
        }

        [CustomAuthorize]
        public JsonResult ForwardSaveAsDraft(ComposeMessage objcomposedetail)
        {
            #region Logic moved to BLL of ForwardSaveAsDraft
            //MessagesBLL objmessagebll = new MessagesBLL();
            //CommonBLL ObjCommonbll = new CommonBLL();
            //int RecordId = 0;
            //objcomposedetail.ComposeType = (int)BO.Enums.Common.ComposeType.ColleagueCompose;
            //objcomposedetail.MessageId = 0;
            //RecordId = objmessagebll.InsertUpdateDraftMessage(objcomposedetail, Convert.ToInt32(SessionManagement.UserId));
            //List<TempFileAttacheMents> objlistofattachements = new List<TempFileAttacheMents>();
            //objlistofattachements = ObjCommonbll.GetTempAttachments(objcomposedetail.FileIds);
            //foreach (var item in objlistofattachements)
            //{
            //    if (System.IO.File.Exists(NewTempFileUpload + item.FilePath))
            //    {
            //        System.IO.File.Move(NewTempFileUpload + item.FilePath, DraftFileUpload + item.FilePath);
            //        objmessagebll.InsertDraftAttachments(RecordId, item.FilePath, Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item.FileExtension), (int)BO.Enums.Common.ComposeType.ColleagueCompose);
            //    }

            //}
            //List<TempFileAttacheMents> objlistofforwardattachements = new List<TempFileAttacheMents>();
            //objlistofforwardattachements = ObjCommonbll.GetForwardAttachMentsById(objcomposedetail.ForwardFileId == null ? string.Empty : objcomposedetail.ForwardFileId, (int)BO.Enums.Common.ComposeType.ColleagueCompose);
            //foreach (var item in objlistofforwardattachements)
            //{
            //    string NewFileName = Convert.ToString(item.FileName);
            //    NewFileName = NewFileName.Substring(NewFileName.ToString().LastIndexOf("$") + 1);
            //    NewFileName = Convert.ToString("$" + System.DateTime.Now.Ticks + "$" + NewFileName);
            //    if (System.IO.File.Exists(ImageBankFileUpload + item.FilePath))
            //    {
            //        if (!new CommonBLL().CheckFileIsValidImage(item.FilePath))
            //        {
            //            item.FileExtension = 1;
            //        }
            //        else {
            //            item.FileExtension = 0;
            //        }
            //        System.IO.File.Copy(ImageBankFileUpload + item.FilePath,DraftFileUpload + NewFileName);
            //        objmessagebll.InsertDraftAttachments(RecordId, NewFileName, Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item.FileExtension), (int)BO.Enums.Common.ComposeType.ColleagueCompose);
            //    }

            //}
            //ObjCommonbll.DeleteAllAttachements(objcomposedetail.FileIds);
            #endregion
            return Json(new MessagesBLL().ForwardSaveAsDraft(new SendMessageToColleaguesRequest()
            {
                UserId = Convert.ToInt32(SessionManagement.UserId),
                DraftFileUpload = DraftFileUpload,
                ImageBankFileUpload = ImageBankFileUpload,
                NewTempFileUpload = NewTempFileUpload,
                objcomposedetail = objcomposedetail,
                ThumbFileUpload = ThumbFileUpload

            }), JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorize]
        public JsonResult ForwardMessageToColleagues(ComposeMessage objcomposedetail)
        {
            #region Logic moved to BLL of ForwardMessageToColleagues
            //bool Result = false;
            //MessagesBLL objmessagebll = new MessagesBLL();
            //CommonBLL ObjCommonbll = new CommonBLL();
            //List<TempFileAttacheMents> objlistofattachements = new List<TempFileAttacheMents>();
            //objlistofattachements = ObjCommonbll.GetTempAttachments(objcomposedetail.FileIds, 0, (int)BO.Enums.Common.ComposeType.ColleagueCompose);
            //List<TempFileAttacheMents> objlistofforwardattachements = new List<TempFileAttacheMents>();
            //objlistofforwardattachements = ObjCommonbll.GetForwardAttachMentsById(objcomposedetail.ForwardFileId == null ? string.Empty : objcomposedetail.ForwardFileId, (int)BO.Enums.Common.ComposeType.ColleagueCompose);
            //string[] ColleagueIds = objcomposedetail.ColleagueId.ToString().Split(new char[] { ',' });
            //if (ColleagueIds.Length > 0)
            //{
            //    if (objcomposedetail.MessageId > 0)
            //    {
            //        switch (objcomposedetail.MessageTypeId)
            //        {
            //            case (int)BO.Enums.Common.ComposeMessageType.Forward:
            //                foreach (var item in ColleagueIds)
            //                {
            //                    int RecordId = 0;
            //                    RecordId = objmessagebll.InsertUpdateColleagueMessage("", objcomposedetail.MessageBody, Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item), 1, objcomposedetail.PatientId, 0, Convert.ToInt32(SessionManagement.UserId));
            //                    ManageAttachement(objlistofattachements, RecordId);
            //                    foreach (var obj in objlistofforwardattachements)
            //                    {
            //                        if (System.IO.File.Exists(ImageBankFileUpload + obj.FilePath))
            //                        {
            //                            objmessagebll.InsertMessageAttachemntsForColleague(RecordId, obj.FilePath);

            //                        }

            //                    }
            //                    objmessagebll.ManageReplyForwardMessage(objcomposedetail.MessageId, RecordId, (int)BO.Enums.Common.ComposeMessageType.Forward, (int)BO.Enums.Common.ComposeType.ColleagueCompose);
            //                    ObjTemplate.SendMessageToColleague(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(item), RecordId, 1);
            //                    if (!string.IsNullOrEmpty(objcomposedetail.PatientId)) { AddPatientToReferredDoctor(Convert.ToInt32(item), objcomposedetail.PatientId); }
            //                    Result = true;
            //                }
            //                break;
            //        }
            //    }
            //    ObjCommonbll.DeleteAllAttachements(objcomposedetail.FileIds, objcomposedetail.MessageId);
            //    DeleteAttachmentFiles(objlistofattachements);

            //}
            #endregion
            return Json(new MessagesBLL().ForwardMessageToColleagues(new SendMessageToColleaguesRequest()
            {
                UserId = Convert.ToInt32(SessionManagement.UserId),
                DraftFileUpload = DraftFileUpload,
                ImageBankFileUpload = ImageBankFileUpload,
                NewTempFileUpload = NewTempFileUpload,
                objcomposedetail = objcomposedetail,
                ThumbFileUpload = ThumbFileUpload

            }), JsonRequestBehavior.AllowGet);
        }

        public ActionResult PartialViewForReferralcategoryvalues(int RefCardId)
        {
            try
            {
                mdlColleagues MdlColleagues = new mdlColleagues();
                MdlColleagues.NewReferralView = MdlColleagues.GetNewReferralViewByRefCardId(Convert.ToInt32(RefCardId));
                return View("PartialNewReferral", MdlColleagues.NewReferralView);

            }
            catch (Exception EX)
            {
                ObjCommon.InsertErrorLog(Request.Url.ToString(), EX.Message, EX.StackTrace);
                throw;
            }

        }
        public ActionResult GetMessageBodyOfInboxReferrals(int MessageId, int? NextMessageId, int MessageTypeId, int? NextMessageTypeId, string IsRead, string Onload)
        {
            try
            {
                Models.Patients.mdlPatient MdlPatient = new Models.Patients.mdlPatient();
                ViewBag.MessageTypeId = MessageTypeId;
                ViewBag.NextMessageId = NextMessageId;
                ViewBag.NextMessageTypeId = NextMessageTypeId;
                mdlColleagues MdlColleagues = new mdlColleagues();
                List<ReferralDetails> lstRefferalDetails = new List<ReferralDetails>();
                MdlColleagues.lstRefferalDetails = MdlPatient.GetRefferalDetailsById(Convert.ToInt32(Session["UserId"]), MessageId, "Inbox", SessionManagement.TimeZoneSystemName);
                ViewBag.ReferralCardId = MdlPatient.ReferralCardId;
                ViewBag.EndMessageTime = DateTime.Now;
                return View(MdlColleagues.lstRefferalDetails);

            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                throw;
            }
        }
        public JsonResult DownloadReferralAttachment(int MessageId, string Type)
        {
            try
            {
                clsColleaguesData CLScolleagues = new clsColleaguesData();
                DataTable dt = new DataTable();
                int ReferralCardId = 0;
                int PatientId = 0;
                DataTable dtsNew = new DataTable();
                dtsNew = CLScolleagues.GetReferralViewByID(MessageId, Type);
                if (dtsNew.Rows.Count > 0 && dtsNew != null)
                {
                    ReferralCardId = Convert.ToInt32(dtsNew.Rows[0]["ReferralCardId"]);
                    PatientId = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(dtsNew.Rows[0]["PatientId"])) ? 0 : dtsNew.Rows[0]["PatientId"]);
                }
                //ObjColleaguesData.GetRefferalImageById(Convert.ToInt32(item["ReferralCardId"]), ObjRefferalDetails.PatientId)
                dt = CLScolleagues.GetRefferalImageById(ReferralCardId, PatientId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    getFilenames.Columns.Add("FileName", System.Type.GetType("System.String"));
                    foreach (DataRow item in dt.Rows)
                    {
                        DataRow dr = getFilenames.NewRow();
                        dr["FileName"] = ConfigurationManager.AppSettings.Get("ImageBank") + ObjCommon.CheckNull(item["Name"].ToString(), string.Empty);
                        getFilenames.Rows.Add(dr);
                    }
                }
                DataTable ds = CLScolleagues.GetRefferalDocumentAttachemntsById(ReferralCardId, PatientId);
                if (ds != null && ds.Rows.Count > 0)
                {
                    if (dt == null || dt.Rows.Count == 0)
                    {
                        getFilenames.Columns.Add("FileName", System.Type.GetType("System.String"));
                    }
                    foreach (DataRow item in ds.Rows)
                    {
                        DataRow dr = getFilenames.NewRow();
                        dr["FileName"] = ConfigurationManager.AppSettings.Get("ImageBank") + ObjCommon.CheckNull(item["DocumentName"].ToString(), string.Empty);
                        getFilenames.Rows.Add(dr);
                    }
                }

                if (getFilenames != null && getFilenames.Rows.Count > 0)
                {
                    ZipAllFiles(MessageId);
                }
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }

            return Json(JsonRequestBehavior.AllowGet);
        }

        public void ZipAllFiles(int MessageId)
        {
            byte[] buffer = new byte[4096];
            var tempFileName = Server.MapPath(ConfigurationManager.AppSettings["TempUploadsFolderPath"]) + MessageId + ".zip";
            var zipOutputStream = new ZipOutputStream(System.IO.File.Create(tempFileName));
            var filePath = String.Empty;
            var fileName = String.Empty;
            var readBytes = 0;
            foreach (DataRow item in getFilenames.Rows)
            {

                if (item["FileName"].ToString() != "")
                {
                    fileName = item["FileName"].ToString();
                    filePath = Server.MapPath(fileName);
                    FileInfo fi = new FileInfo(filePath);
                    string PF_Name = String.Empty;
                    if (fi.Exists)
                    {
                        String[] History_Pat = fileName.Split('$');

                        if (History_Pat[0].Length > 0)
                        {
                            if (History_Pat[0].Equals(ConfigurationManager.AppSettings.Get("ImageBank")))
                            {
                                PF_Name = History_Pat[2].ToString();
                            }
                            else
                            {
                                PF_Name = fileName;
                            }
                        }
                        var zipEntry = new ZipEntry(PF_Name.Replace(" ", ""));
                        zipOutputStream.PutNextEntry(zipEntry);
                        using (var fs = System.IO.File.OpenRead(filePath))
                        {
                            do
                            {
                                readBytes = fs.Read(buffer, 0, buffer.Length);
                                zipOutputStream.Write(buffer, 0, readBytes);
                            } while (readBytes > 0);
                        }
                    }
                }
            }
            if (zipOutputStream.Length == 0)
            {
                return;
            }
            zipOutputStream.Finish();
            zipOutputStream.Close();
            Response.ContentType = "application/x-zip-compressed";
            Response.AppendHeader("Content-Disposition", "attachment; filename=MessageId_" + MessageId + "_Records.zip");
            Response.BufferOutput = true;
            Response.WriteFile(tempFileName);
            Response.Flush();
            Response.Close();
            //delete the temp file  
            if (System.IO.File.Exists(tempFileName))
                System.IO.File.Delete(tempFileName);
        }
        public ActionResult PrintReferralMessage()
        {
            List<ReferralMessageDetails> obj = new List<ReferralMessageDetails>();
            ViewBag.Result = GetMessageDetails();
            obj = ViewBag.Result.Model;
            return View("GetMessage",obj);
        }
        public ActionResult PrintMessage()
        {
            List<NormalMessageDetails> lst = new List<NormalMessageDetails>();
            ViewBag.Result = GetMessageDetails();
            lst = ViewBag.Result.Model;
            return View(lst);
        }
    }
}