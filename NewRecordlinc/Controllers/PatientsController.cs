﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewRecordlinc.Models.Patients;
using DataAccessLayer.PatientsData;
using DataAccessLayer.Common;
using System.Data;
using System.Text;
using DataAccessLayer.ColleaguesData;
using NewRecordlinc.Models.Common;
using NewRecordlinc.App_Start;
using System.Web.Configuration;
using System.Configuration;
using System.IO;
using System.Drawing;
using NewRecordlinc.Models.Colleagues;
using ICSharpCode.SharpZipLib.Zip;
using System.Data.OleDb;
using Excel;
using System.Web.Script.Serialization;
using NewRecordlinc.Models;
using System.Globalization;
using System.Web.Helpers;
using System.Text.RegularExpressions;
using DataAccessLayer.Appointment;
using BusinessLogicLayer;
using BO.ViewModel;
using DataAccessLayer.ProfileData;

namespace NewRecordlinc.Controllers
{
    public class PatientsController : Controller
    {
        mdlPatient MdlPatient = new mdlPatient();
        mdlCommon MdlCommon = null;
        mdlColleagues MdlColleagues = new mdlColleagues();
        clsPatientsData ObjPatientsData = new clsPatientsData();
        clsCommon ObjCommon = new clsCommon();
        clsTemplate ObjTemplate = new clsTemplate();
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        PatientHistoryMethod objPatientMethodCall = new PatientHistoryMethod();
        PermissionResultBLL objpermissionbll = new PermissionResultBLL();
        public static string NewTempFileUpload = System.Configuration.ConfigurationManager.AppSettings["TempFileUpload"];
        public static string ImageBankFileUpload = System.Configuration.ConfigurationManager.AppSettings["ImageBankFileUpload"];
        public static string ThumbFileUpload = System.Configuration.ConfigurationManager.AppSettings["ThumbFileUpload"];
        public static string DraftFileUpload = System.Configuration.ConfigurationManager.AppSettings["DraftFileUpload"];

        int PageSize = Convert.ToInt32(WebConfigurationManager.AppSettings["PageSize"]);
        int MinPageSize = 5;
        string TempFolder = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("TempUploadsFolderPath"));
        string ImageBankFolder = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("ImageBank"));
        string DefaultDoctorImage = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("DefaultDoctorImage"));

        string FileNameNew = string.Empty;
        //string TempFolderPath = System.Configuration.ConfigurationManager.AppSettings["TempUploadsFolderPath"];
        //string DraftFolderPath = System.Configuration.ConfigurationManager.AppSettings["DraftAttachments"];
        //string ImageBankFolderPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("ImageBank"));
        //string TempPathThumb = System.Configuration.ConfigurationManager.AppSettings["ThumbPath"];


        [CustomAuthorize]
        public ActionResult Index()
        {

            //BackgroundJob.Schedule(() => testProcedures(1), DateTime.Now.AddMinutes(1));

            #region Check Public Profile Reviews

            DataTable dt = new DataTable();
            clsProfileData objProfileData = new clsProfileData();

            dt = objProfileData.GetPublicProfileSectionDetials(Convert.ToInt32(Session["UserId"]));

            //dt = clsColleaguesData.CheckReviewsByUserId(Convert.ToInt32(Session["UserId"]));
            if (dt != null && dt.Rows.Count > 0)
            {
                SessionManagement.PublicProfileReviews = Convert.ToBoolean(dt.Rows[0]["Reviews"]);
            }
            //
            #endregion


            int redir = Convert.ToInt32(Request.QueryString["Redir"]);
            if (redir == 1)
            {
                ViewBag.r = 1;
            }
            else
            {
                ViewBag.r = 0;
            }
            PatientBLL patientBll = new PatientBLL();
            bool Result = ObjCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
            ViewBag.location = LocationList();
            BO.Models.PatientFilter FilterObj = new BO.Models.PatientFilter();
            FilterObj.UserId = Convert.ToInt32(Session["UserId"]);
            FilterObj.PageSize = PageSize;
            FilterObj.PageIndex = 1;
            ViewBag.pageindex = 1;
            FilterObj.SearchText = null;
            FilterObj.SortColumn = 2;
            FilterObj.SortDirection = 1;
            FilterObj.NewSearchtext = null;
            FilterObj.FilterBy = "2";
            FilterObj.LocationBy = "0";
            patientBll.lstPatientDetailsOfDoctor = PatientBLL.NewGetPatientDetailsOfDoctor(FilterObj);
            ViewBag.cnt = patientBll.lstPatientDetailsOfDoctor.Count > 0 ? patientBll.lstPatientDetailsOfDoctor[0].TotalRecord : 0;
            return View("NewIndex", patientBll.lstPatientDetailsOfDoctor);
        }

        //[CustomAuthorize]
        //public ActionResult NewIndex()
        //{
        //    PatientBLL patientBll = new PatientBLL();
        //    bool Result = ObjCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());            
        //    int cnt = patientBll.NewGetPatientCountOfDoctor(Convert.ToInt32(SessionManagement.UserId), "", "", 2, 0);
        //    ViewBag.cnt = cnt;
        //    ViewBag.location = LocationList();
        //    BO.Models.PatientFilter FilterObj = new BO.Models.PatientFilter();
        //    FilterObj.UserId = Convert.ToInt32(Session["UserId"]);
        //    FilterObj.PageSize = PageSize;
        //    FilterObj.PageIndex = 1;
        //    ViewBag.pageindex = 1;
        //    FilterObj.SearchText = null;
        //    FilterObj.SortColumn = 2;
        //    FilterObj.SortDirection = 1;
        //    FilterObj.NewSearchtext = null;
        //    FilterObj.FilterBy = "2";
        //    FilterObj.LocationBy = "0";
        //    patientBll.lstPatientDetailsOfDoctor = patientBll.NewGetPatientDetailsOfDoctor(FilterObj);
        //    return View("NewIndex", patientBll.lstPatientDetailsOfDoctor);
        //}

        //Patient List New.
        [CustomAuthorize]
        public ActionResult NewGetListOfPatientOnScroll(BO.Models.PatientFilter FilterObj)
        {
            FilterObj.UserId = Convert.ToInt32(Session["UserId"]);
            FilterObj.PageSize = PageSize;
            if (FilterObj.NewSearchtext == "All")//RM-437
            {
                FilterObj.NewSearchtext = string.Empty;
                FilterObj.SortColumn = 2;
            }
            if (FilterObj.NewSearchtext == "" || FilterObj.NewSearchtext == null)
            {
                FilterObj.SortColumn = 2;
                FilterObj.SortDirection = 1;
            }
            //PatientBLL patientBll = new PatientBLL();
            ViewBag.pageindex = FilterObj.PageIndex;
            //patientBll.lstPatientDetailsOfDoctor = PatientBLL.NewGetPatientDetailsOfDoctor(FilterObj);
            return View("_PartialListPatients", PatientBLL.NewGetPatientDetailsOfDoctor(FilterObj));
        }

        //Method for binding Location list.
        public List<SelectListItem> LocationList()
        {
            DataTable dt = new DataTable();
            dt = ObjColleaguesData.GetDoctorLocaitonById(Convert.ToInt32(Session["UserId"]));
            List<SelectListItem> lst = new List<SelectListItem>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    lst.Add(new SelectListItem { Value = item["AddressInfoId"].ToString(), Text = item["Location"].ToString() });
                }
                ViewBag.location = lst;
                return lst;
            }
            else
            {
                ViewBag.location = lst;
                return lst;
            }
        }


        //New GetPatientDetailsList for Alpahabetic sort
        public ActionResult NewGetPatientDetailsList(BO.Models.PatientFilter FilterObj)
        {
            bool Isdashboard = FilterObj.IsDashboard;
            FilterObj.UserId = Convert.ToInt32(Session["UserId"]);
            FilterObj.PageSize = PageSize;
            if (FilterObj.NewSearchtext == "All")
            {
                FilterObj.NewSearchtext = string.Empty;
                FilterObj.SortColumn = 2;
            }
            if (FilterObj.SearchText == "Search Patient")
            {
                FilterObj.SearchText = null;
                FilterObj.SortColumn = 2;
                FilterObj.SortDirection = 1;
            }
            //PatientBLL patientBll = new PatientBLL();
            //patientBll.lstPatientDetailsOfDoctor = PatientBLL.NewGetPatientDetailsOfDoctor(FilterObj);
            if (Isdashboard)
            {
                FilterObj.PageSize = 10;
                return View("_PartialDashboardPatientList", PatientBLL.NewGetPatientDetailsOfDoctor(FilterObj));
            }
            else
            {
                return View("_PartialListPatients", PatientBLL.NewGetPatientDetailsOfDoctor(FilterObj));
            }
        }


        [HttpPost]
        public JsonResult GetPatientDetailsList(string PageIndex, string SearchText, string SortColumn, string SortDirection, string NewSearchText, string FilterBy, string LocationId)
        {
            if (NewSearchText == "ALL")
            {
                NewSearchText = string.Empty;
            }
            if (NewSearchText == "L" && FilterBy == "3")
            {
                NewSearchText = string.Empty;
            }
            return Json(GetPatientList(Convert.ToInt32(PageIndex), SearchText, Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection), NewSearchText, Convert.ToInt32(FilterBy), Convert.ToInt32(LocationId)), JsonRequestBehavior.AllowGet);
        }


        //AK changes 02-22-2017 (For Location DDP and ALL Functionlity and Scroll Issue)
        public string GetPatientList(int PageIndex, string SearchText, int SortColumn, int SortDirection, string NewSearchText, int FilterBy, int LocationId)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                MdlPatient = new mdlPatient();

                int Count = MdlPatient.GetPatientCountOfDoctor(Convert.ToInt32(SessionManagement.UserId), SearchText, NewSearchText, FilterBy, LocationId);
                MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, PageSize, SearchText, SortColumn, SortDirection, NewSearchText, FilterBy, LocationId);
                sb.Append("<div class=\"inner\">");
                sb.Append("<div class=\"table_data patient clearfix\">");
                sb.Append("<h5 class=\"clearfix hdng_brdr\">Patients(<b id=\"count\">" + Count + "</b>)</h5>");
                sb.Append("<div class=\"dashboard_links clearfix\">");
                sb.Append("<ul class=\"dashboard_tour\">");
                sb.Append("<li><a  href='javascript:void(0)' onclick=\"CheckAuthorization('" + (int)BO.Enums.Common.Features.New_Patient_Leads + "')\" ><span class=\"add_patient\">Add Patient</span></a></li>");
                sb.Append("<li><a href='javascript:;' class=\"snd_msg_sltd\" onclick=\"return SendMessageToSelected()\"></a></li>");

                sb.Append("<li><input class=\"export_patient_btn\" name=\"Delete\" type=\"submit\" onclick=\"location.href='" + Url.Action("ExportToExcel", "Patients") + "'\" /></li>");
                sb.Append("<li><input class=\"import_patient_btn\" name=\"Export Report\" type=\"submit\" onclick=\"location.href='" + Url.Action("ImportPatient", "Patients") + "'\"></li>");
                sb.Append("</ul>");

                sb.Append("</div>");
                sb.Append("<section>");
                sb.Append("<div><select id=\"Listing\" class=\"cs-select cs-skin-elastic\">");
                if (FilterBy == 1)
                    sb.Append("<option value=\"1\" selected>First Name</option>");
                else
                    sb.Append("<option value=\"1\">First Name</option>");
                if (FilterBy == 2)
                    sb.Append("<option value=\"2\" selected>Last Name</option>");
                else
                    sb.Append("<option value=\"2\">Last Name</option>");
                //if (FilterBy == 10)
                //    sb.Append("<option value=\"10\" selected>Email</option>");
                //else
                //    sb.Append("<option value=\"10\">Email</option>");
                if (FilterBy == 11)
                    sb.Append("<option value=\"11\" selected>Referred By</option>");
                else
                    sb.Append("<option value=\"11\">Referred By</option>");
                // if (FilterBy == 3)
                //     sb.Append("<option value=\"3\" selected>Location</option>");
                // else
                //     sb.Append("<option value=\"3\">Location</option>");

                sb.Append("</select>");
                sb.Append("</section>");

                //THis is A-Z button search result
                sb.Append("<div class=\"alphabet\">");
                sb.Append("<a class=\"first ALL\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "ALL" + "')\">ALL</a>");
                sb.Append("<a class=\"A\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "A" + "')\">A</a>");
                sb.Append("<a class=\"B\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "B" + "')\">B</a>");
                sb.Append("<a class=\"C\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "C" + "')\">C</a>");
                sb.Append("<a class=\"D\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "D" + "')\">D</a>");
                sb.Append("<a class=\"E\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "E" + "')\">E</a>");
                sb.Append("<a class=\"F\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "F" + "')\">F</a>");
                sb.Append("<a class=\"G\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "G" + "')\">G</a>");
                sb.Append("<a class=\"H\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "H" + "')\">H</a>");
                sb.Append("<a class=\"I\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "I" + "')\">I</a>");
                sb.Append("<a class=\"J\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "J" + "')\">J</a>");
                sb.Append("<a class=\"K\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "K" + "')\">K</a>");
                sb.Append("<a class=\"L\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "L" + "')\">L</a>");
                sb.Append("<a class=\"M\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "M" + "')\">M</a>");
                sb.Append("<a class=\"N\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "N" + "')\">N</a>");
                sb.Append("<a class=\"O\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "O" + "')\">O</a>");
                sb.Append("<a class=\"P\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "P" + "')\">P</a>");
                sb.Append("<a class=\"Q\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "Q" + "')\">Q</a>");
                sb.Append("<a class=\"R\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "R" + "')\">R</a>");
                sb.Append("<a class=\"S\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "S" + "')\">S</a>");
                sb.Append("<a class=\"T\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "T" + "')\">T</a>");
                sb.Append("<a class=\"U\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "U" + "')\">U</a>");
                sb.Append("<a class=\"V\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "V" + "')\">V</a>");
                sb.Append("<a class=\"W\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "W" + "')\">W</a>");
                sb.Append("<a class=\"X\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "X" + "')\">X</a>");
                sb.Append("<a class=\"Y\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "Y" + "')\">Y</a>");
                sb.Append("<a class=\"last Z\" href=\"Javascript:;\" onclick=\"GetPatientListByAtoZ('" + "Z" + "')\">Z</a>");
                sb.Append("</div>");
                sb.Append("<div style='width:100%;display:inline-block;'><div id=\"Locationlst\" class=\"dropdownright\">");
                sb.Append("<div class=\"Devendra\">");
                sb.Append("<span>Select Location</span>");
                sb.Append("<section>");
                sb.Append("<select id=\"listing2\" class=\"cs-select cs-skin-elastic\">");
                sb.Append("<option value=\"0\">ALL</option>");
                DataTable dt = new DataTable();
                dt = ObjColleaguesData.GetDoctorLocaitonById(Convert.ToInt32(Session["UserId"]));
                if (dt != null)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        if (LocationId == Convert.ToInt32(item["AddressInfoId"]))
                        {
                            sb.Append("<option selected=\"selected\" value=\"" + item["AddressInfoId"] + "\">" + item["Location"] + "</option>");
                        }
                        else
                        {
                            sb.Append("<option value=\"" + item["AddressInfoId"] + "\">" + item["Location"] + "</option>");
                        }

                        //sb.Append("<option value=\"" + item["AddressInfoId"] + "\">" + item["Location"] + "</option>");
                    }
                }
                sb.Append("</select>");
                sb.Append("</section>");
                sb.Append("</div>");
                sb.Append("</div>");
                sb.Append("<h5 style=\"float: right;\">&nbsp;<span class=\"buttons\"><input type=\"submit\" onclick=\"DeleteSelectedPatiets()\" class=\"delete_selected\" name=\"\" id=\"btnMultipleDelete\"></span></h5></div></div>");
                sb.Append("<table class=\"patientTable\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">");
                sb.Append(" <thead>");
                sb.Append("<tr>");
                sb.Append("<th><input name=\"\" type=\"checkbox\" id=\"MainCheckBox\" onclick=\"SetAllCheckBoxes(this)\" value=\"\"></th>");
                sb.Append(" <th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetPatientDetailsList('" + 1 + "','" + SearchText + "','" + 2 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Last Name</a> </th>");
                sb.Append(" <th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetPatientDetailsList('" + 1 + "','" + SearchText + "','" + 1 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">First Name</a> </th>");


                //sb.Append("  <th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetPatientDetailsList('" + 1 + "','" + SearchText + "','" + 11 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Forms</a> </th>");
                sb.Append("   <th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetPatientDetailsList('" + 1 + "','" + SearchText + "','" + 14 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Referred By</a> </th>");
                //sb.Append(" <th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetPatientDetailsList('" + 1 + "','" + SearchText + "','" + 12 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">ID</a> </th>");
                //sb.Append(" <th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetPatientDetailsList('" + 1 + "','" + SearchText + "','" + 8 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Gender</a> </th>");
                sb.Append("   <th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetPatientDetailsList('" + 1 + "','" + SearchText + "','" + 9 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Phone</a> </th>");
                sb.Append("   <th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetPatientDetailsList('" + 1 + "','" + SearchText + "','" + 10 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Email</a> </th>");
                sb.Append("   <th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetPatientDetailsList('" + 1 + "','" + SearchText + "','" + 13 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Location</a> </th>");
                //sb.Append(" <th><a href=\"Javascript:;\" style=\"color:#515152;cursor:default;text-decoration: none;\">Phone</a> </th>");
                //sb.Append("   <th><a href=\"Javascript:;\" style=\"color:#515152;cursor:default;text-decoration: none;\">Email</a> </th>");
                //sb.Append("   <th><a href=\"Javascript:;\" style=\"color:#515152;cursor:default;text-decoration: none;\">Location</a> </th>");
                sb.Append(" <th><a href=\"Javascript:;\" style=\"color:#515152;cursor:default;text-decoration: none;\">Actions</a> </th>");


                sb.Append(" </tr>");
                sb.Append(" </thead>");
                if (MdlPatient.lstPatientDetailsOfDoctor.Count > 0)
                {
                    double TotalPages = Math.Ceiling(Convert.ToDouble(MdlPatient.lstPatientDetailsOfDoctor[0].TotalRecord) / PageSize);
                    sb.Append(" <tbody id=\"getindex\" class='" + PageIndex + "' data-sortcolumn=\"" + SortColumn + "\" data-sortirection=\"" + SortDirection + "\">");

                    foreach (var item in MdlPatient.lstPatientDetailsOfDoctor)
                    {
                        sb.Append(" <tr id=\"row" + item.PatientId + "\">");
                        sb.Append(" <td> " + (item.OwnerId == Convert.ToInt32(Session["UserId"]) ? "<input name=\"\" type=\"checkbox\" id=\"CheckPateint\"  onclick=\"UncheckMaincheckBox('MainCheckBox')\" value=\"" + item.PatientId + "\">" : "&nbsp;") + "</td>");
                        sb.Append(" <td><a href='javascript:;' onclick=\"ViewPatientHistory('" + item.PatientId + "')\">" + item.LastName + "</a></td>");
                        sb.Append("  <td><a href='javascript:;' onclick=\"ViewPatientHistory('" + item.PatientId + "')\">" + item.FirstName + "</a></td>");
                        sb.Append("<td ><a href='#' onclick='ViewProfile(" + item.ReferredById + ")' > " + item.ReferredBy + "</a></td>");
                        sb.Append("<td>" + item.Phone + "</td>");
                        sb.Append("<td><a href=\"mailto:" + item.Email + "\">" + item.Email + "</a></td>");
                        sb.Append(" <td>" + item.Location + "</td>");
                        sb.Append("<td class=\"dataTableCell_2\" style=\"padding: 9px 2px 1px 3px; width:150px;\">");
                        sb.Append("<a href=\"Javascript:;\" class=\"frmreferpatients3 tooltip\" onclick=\"OpenPopup('" + item.PatientId + "')\"><span class=\"tooltiptext\">Send Referral</span></a></br>");
                        sb.Append("<a href='" + Url.Action("PatientComposeMessage", "Patients", new { PatientId = item.PatientId }) + "' class=\"frmSendEmail tooltip\"><span class=\"tooltiptext\">Send Mail</span></a>");
                        if (item.OwnerId == Convert.ToInt32(Session["UserId"]))
                        {
                            sb.Append("<a href=\"javascript:;\" class=\"frmdltbtn1\" onclick=\"DeletePatient('" + item.PatientId + "','" + PageIndex + "');\" style=\"margin-top:-4px; margin-left: 5px;\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    sb.Append("</tbody></table></div><br /><div class=\"clear\"></div></div>");

                }
                else
                {
                    sb.Append("<tbody><tr><td colspan=\"8\"><center>No Record Found</center></tr></tbody></table></div><br /><div class=\"clear\"></div></div>");
                }

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return sb.ToString();
        }
        //AK changes 02-22-2017 (For Location DDP and ALL Functionlity and Scroll Issue)
        public string GetPatientListOnScroll(int PageIndex, string searchtext, int strSortColumn, int strSortDirection, string NewSearchText, string FilterBy, string LocationId)
        {
            if (NewSearchText == "ALL")
            {
                NewSearchText = string.Empty;
            }
            if (NewSearchText == "L" && FilterBy == "3")
            {
                NewSearchText = string.Empty;
            }
            StringBuilder sb = new StringBuilder();
            MdlPatient = new mdlPatient();
            MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, PageSize, searchtext, strSortColumn, strSortDirection, NewSearchText, Convert.ToInt32(FilterBy), Convert.ToInt32(LocationId));
            if (MdlPatient.lstPatientDetailsOfDoctor.Count > 0)
            {
                foreach (var item in MdlPatient.lstPatientDetailsOfDoctor)
                {
                    sb.Append(" <tr id=\"row" + item.PatientId + "\">");
                    sb.Append(" <td><input name=\"\" type=\"checkbox\" id=\"CheckPateint\"  onclick=\"UncheckMaincheckBox('MainCheckBox')\" value=\"" + item.PatientId + "\"></td>");
                    sb.Append(" <td><a href='javascript:;' onclick=\"ViewPatientHistory('" + item.PatientId + "')\">" + item.LastName + "</a></td>");
                    sb.Append("  <td><a href='javascript:;' onclick=\"ViewPatientHistory('" + item.PatientId + "')\">" + item.FirstName + "</a></td>");
                    sb.Append("<td><a href='#' onclick='ViewProfile(" + item.ReferredById + ")' > " + item.ReferredBy + "</a></td>");
                    sb.Append("<td>" + item.Phone + "</td>");
                    sb.Append("<td><a href=\"mailto:" + item.Email + "\">" + item.Email + "</a></td>");
                    sb.Append(" <td>" + item.Location + "</td>");
                    sb.Append("<td class=\"dataTableCell_2\" style=\"padding: 16px 2px 15px 7px; width:136px;\">");
                    sb.Append("<a href=\"Javascript:;\" class=\"frmreferpatients3 tooltip\" onclick=\"OpenPopup('" + item.PatientId + "')\"><span class=\"tooltiptext\">Send Referral</span></a></br>");
                    sb.Append("<a href='" + Url.Action("PatientComposeMessage", "Patients", new { PatientId = item.PatientId }) + "' class=\"frmSendEmail tooltip\"><span class=\"tooltiptext\">Send Mail</span></a>");
                    if (item.OwnerId == Convert.ToInt32(Session["UserId"]))
                    {
                        sb.Append("<a href=\"javascript:;\" class=\"frmdltbtn1\" onclick=\"DeletePatient('" + item.PatientId + "','" + PageIndex + "');\" style=\"margin-top:-4px;margin-left: 5px;\"></a>");
                    }
                    sb.Append("</td>");
                    sb.Append("</tr>");
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"9\"><center>No more record found</center></td></tr>");
            }
            return sb.ToString();
        }

        [CustomAuthorize]
        public ActionResult OldAddPatient(string PatientId)
        {
            bool Result = ObjCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
            // Check permission & Membersip


            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                int intPatientId = 0;
                MdlPatient = new mdlPatient();

                if (PatientId != null && PatientId != "")
                {
                    intPatientId = int.Parse(PatientId);

                    DataSet ds = new DataSet();
                    ds = ObjPatientsData.GetPateintDetailsByPatientId(intPatientId);

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        MdlPatient.PatientId = Convert.ToInt32(ds.Tables[0].Rows[0]["PatientId"]);
                        MdlPatient.AssignedPatientId = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["AssignedPatientId"]), string.Empty);
                        MdlPatient.FirstName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]), string.Empty);
                        MdlPatient.LastName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LastName"]), string.Empty);
                        if (Convert.ToString(ds.Tables[0].Rows[0]["DateOfBirth"]) != null && Convert.ToString(ds.Tables[0].Rows[0]["DateOfBirth"]) != "")
                        {
                            DateTime DOB = Convert.ToDateTime(ds.Tables[0].Rows[0]["DateOfBirth"]);
                            MdlPatient.DateOfBirth = ObjCommon.CheckNull(new CommonBLL().ConvertToDate(Convert.ToDateTime(DOB), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL)), string.Empty);
                        }
                        MdlPatient.Email = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Email"]), string.Empty);
                        MdlPatient.Gender = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Gender"]), string.Empty);
                        MdlPatient.Age = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Age"]), string.Empty);
                        MdlPatient.ExactAddress = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["ExactAddress"]), string.Empty);
                        MdlPatient.Address2 = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Address2"]), string.Empty);
                        MdlPatient.City = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["City"]), string.Empty);
                        MdlPatient.State = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["State"]), string.Empty);
                        MdlPatient.ZipCode = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["ZipCode"]), string.Empty);
                        MdlPatient.Phone = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Phone"]), string.Empty);
                        MdlPatient.Notes = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Notes"]), string.Empty);
                        MdlPatient.Country = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Country"]), string.Empty);
                        MdlPatient.ReferredBy = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["ReferredById"]), string.Empty);
                        if (Convert.ToString(ds.Tables[0].Rows[0]["FirstExamDate"]) != null && Convert.ToString(ds.Tables[0].Rows[0]["FirstExamDate"]) != "")
                        {
                            MdlPatient.FirstExamDate = ObjCommon.CheckNull(new CommonBLL().ConvertToDate(Convert.ToDateTime(ds.Tables[0].Rows[0]["FirstExamDate"]), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL)), string.Empty);
                        }
                        MdlPatient.ReferredByName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["ReferredBy"]), string.Empty);
                        ViewBag.country = CountryScript(ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Country"]), string.Empty));
                        ViewBag.state = StateScript("", ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["State"]), "0"));
                        ViewBag.ColleaguesId = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["ReferredById"]), string.Empty);
                        ViewBag.LocationId = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Location"]), string.Empty);
                    }
                }
                else
                {
                    MdlPatient.PatientId = 0;
                    ViewBag.country = CountryScript("");
                    ViewBag.state = StateScript("", "0");
                    // ViewBag.Colleagues = GetColleaguesList("");
                    // ViewBag.ColleaguesId = ColleaguesScript("");
                    // ViewBag.LocationId = LocationScript("");
                }
                return PartialView("PartialAddPatient", MdlPatient);
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        #region Code For Add and Edit Pateint
        [HttpPost]
        public ActionResult AddPatient(mdlPatient objPatient)
        {
            // Gender Defult pass 0 

            try
            {
                ObjColleaguesData = new clsColleaguesData();

                string State = string.Empty;
                string Country = "US";
                var cc = Request["select3"];
                if (Request["drpState"] != null && Request["drpState"] != "")
                {
                    State = Convert.ToString(Request["drpState"]);
                }

                if (Request["drpCountry"] != null && Request["drpCountry"] != "")
                {
                    Country = "US";
                }
                if (objPatient.Gender == "Female")
                {
                    objPatient.Gender = "1";
                }
                else if (objPatient.Gender == "Male")
                {
                    objPatient.Gender = "0";
                }
                else
                {
                    objPatient.Gender = "2";
                }
                string colleaguesId = null;
                string[] Split = null;
                int status = 0;

                if (!string.IsNullOrEmpty(objPatient.ReferredBy))
                {

                    colleaguesId = Convert.ToString(objPatient.ReferredBy);
                    Split = colleaguesId.Split(',');
                    MdlPatient = new mdlPatient();
                    status = MdlPatient.PatientInsertAndUpdate(objPatient.PatientId, objPatient.AssignedPatientId, objPatient.FirstName,
                                                  null, objPatient.LastName, objPatient.DateOfBirth, Convert.ToInt32(objPatient.Gender), null, objPatient.Phone, objPatient.Email,
                                               objPatient.ExactAddress, objPatient.City, State, objPatient.ZipCode, Country,
                                               Convert.ToInt32(Session["UserId"]), 0, objPatient.Address2, Guid.NewGuid().ToString("N").Substring(0, 8), objPatient.Notes, Convert.ToInt32(Split[0].ToString()), objPatient.Location, objPatient.FirstExamDate, SessionManagement.TimeZoneSystemName);
                }
                else
                {

                    Split = null;
                    MdlPatient = new mdlPatient();
                    status = MdlPatient.PatientInsertAndUpdate(objPatient.PatientId, objPatient.AssignedPatientId, objPatient.FirstName,
                                                  null, objPatient.LastName, objPatient.DateOfBirth, Convert.ToInt32(objPatient.Gender), null, objPatient.Phone, objPatient.Email,
                                               objPatient.ExactAddress, objPatient.City, State, objPatient.ZipCode, Country,
                                               Convert.ToInt32(Session["UserId"]), 0, objPatient.Address2, Guid.NewGuid().ToString("N").Substring(0, 8), objPatient.Notes, Convert.ToInt32(Split), objPatient.Location, objPatient.FirstExamDate, SessionManagement.TimeZoneSystemName);

                }


                int patientid = 0;
                bool Result = ObjCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
                if (objPatient.PatientId == 0)
                {
                    DataTable dt = new DataTable();
                    string qur = "select Top 1 * from Patient order by PatientId desc";
                    dt = ObjCommon.DataTable(qur);
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        patientid = Convert.ToInt32(dt.Rows[0]["PatientId"]);
                    }
                }

                if (!string.IsNullOrEmpty(objPatient.ReferredBy))
                {
                    foreach (var item in Split)
                    {
                        DataTable User = new DataTable();
                        User = ObjColleaguesData.GetDoctorDetailsbyId(Convert.ToInt32(Session["UserId"]));
                        string UserName = string.Empty; string Colleaguesname = string.Empty;
                        if (User != null && User.Rows.Count > 0)
                        {
                            UserName = User.Rows[0]["FirstName"] + " " + User.Rows[0]["LastName"];
                        }
                        DataTable Colleague = new DataTable();
                        Colleague = ObjColleaguesData.GetDoctorDetailsbyId(Convert.ToInt32(item));
                        if (Colleague != null && Colleague.Rows.Count > 0)
                        {
                            Colleaguesname = Colleague.Rows[0]["FirstName"] + " " + Colleague.Rows[0]["LastName"];
                        }
                        string XMLString = "";
                        if (objPatient.PatientId > 0 || objPatient.AssignedPatientId != null)
                        {
                            if (objPatient.Notes != null && objPatient.Notes != "")
                            {
                                ObjColleaguesData.InsertReferral(Colleaguesname, UserName, "Referral Card", "Refer Patient", 2, Convert.ToInt32(Convert.ToString(item)), objPatient.PatientId, "", "", 0, Convert.ToInt32(Session["UserId"]), objPatient.Notes, "", "", "", "", "", objPatient.Location, XMLString);
                            }
                            else
                            {
                                ObjColleaguesData.InsertReferral(Colleaguesname, UserName, "Referral Card", "Refer Patient", 2, Convert.ToInt32(Convert.ToString(item)), objPatient.PatientId, "", "", 0, Convert.ToInt32(Session["UserId"]), "", "", "", "", "", "", objPatient.Location, XMLString);
                            }
                        }
                        else
                        {
                            if (objPatient.Notes != null && objPatient.Notes != "")
                            {
                                ObjColleaguesData.InsertReferral(Colleaguesname, UserName, "Referral Card", "Refer Patient", 2, Convert.ToInt32(Convert.ToString(item)), patientid, "", "", 0, Convert.ToInt32(Session["UserId"]), objPatient.Notes, "", "", "", "", "", objPatient.Location, XMLString);
                            }
                            else
                            {
                                ObjColleaguesData.InsertReferral(Colleaguesname, UserName, "Referral Card", "Refer Patient", 2, Convert.ToInt32(Convert.ToString(item)), patientid, "", "", 0, Convert.ToInt32(Session["UserId"]), "", "", "", "", "", "", objPatient.Location, XMLString);
                            }

                        }
                    }

                }
                if (status > 0)
                {
                    if (objPatient.PatientId == 0)
                    {
                        TempData["Identify"] = "Patients/AddPatient";
                        return RedirectToAction("PatientHistory", "Patients", new { PatientId = patientid });
                    }
                    else
                    {
                        TempData["Identify"] = "Patients/AddPatient";
                        return RedirectToAction("PatientHistory", "Patients", new { PatientId = objPatient.PatientId });
                    }

                }
                else
                {
                    // return RedirectToAction("PatientHistory", "Patients", new { PatientId = objPatient.PatientId });
                    // Priya: Issue no : 5205
                    // ViewBag.ErrorMessage = "Error while " + (objPatient.PatientId == 0 ? "inserting" : "updating") + " patient. please try again.";
                    ViewBag.state = StateScript("", string.IsNullOrEmpty(State) ? "0" : State);
                    ViewBag.ColleaguesId = cc;
                    ViewBag.LocationId = Request["select4"];
                    return PartialView("PartialAddPatient", objPatient);
                }

            }
            catch (Exception ex)
            {
                bool status1 = ObjCommon.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                throw;
            }


        }
        public string ColleagueslistbyUserId(int UserId)
        {
            return null;
        }
        [HttpPost]
        public JsonResult FillState(string cid, string sid)
        {
            return Json(StateScript(cid, sid), JsonRequestBehavior.AllowGet);
        }
        public string CountryScript(string countryId)
        {
            if (countryId == "")
            {
                countryId = "US";
            }
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.Country();
            sb.Append("<select id=\"drpCountry\" name=\"drpCountry\" style=\"width:100%;\" onchange=\"FillState()\">");
            foreach (var item in lst)
            {

                if (countryId == item.Value)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
                else
                {
                    sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
            }
            sb.Append("</select>");
            return sb.ToString();
        }

        public string StateScript(string countryId, string stateId)
        {
            if (countryId == "")
            {
                countryId = "US";
            }
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.StateByCountryCode(countryId);
            sb.Append("<select id=\"drpState\" name=\"drpState\" style=\"width:100%;\">");
            foreach (var item in lst)
            {
                if (item.Text == "[Select State]")
                {
                    lst.Remove(item);
                    break;
                }

                if (stateId == item.Value)
                {
                    sb.Append("<option selected=\"selected\" value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
                else
                {
                    sb.Append("<option value=\"" + item.Value + "\">" + item.Text + "</option>");
                }
            }
            sb.Append("</select>");
            return sb.ToString();
        }
        #endregion

        [HttpPost]
        public string SearchColleagueFromPopUp(string SearchText)
        {
            return GetListSearchColleagueFromPopUp(SearchText);
        }
        public string GetListSearchColleagueFromPopUp(string SearchText)
        {

            StringBuilder sb = new StringBuilder();
            try
            {
                MdlColleagues = new mdlColleagues();
                MdlColleagues.lstColleaguesDetails = MdlColleagues.ColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), 1, 16, SearchText, 11, 2, null, 0);

                sb.Append("<h2 id=\"ReferralTitle\" class=\"title\">Select the Colleagues to Send Referral </h2>");

                if (SearchText == null || SearchText == "")
                {
                    SearchText = "Find colleague to refer to...";
                }

                //sb.Append("<h2 id=\"ReferralTitle\" class=\"title\">    </h2>");

                //sb.Append("<div  class=\"search_main_div\"><input style=\"display:none\" id=\"SearchBtnHide\" class=\"search_btn_popup\" name=\"Cancel\" type=\"button\" onclick=\"SearchColleague()\" value=\"Cancel\"><div class=\"patient_searchbar\"><input id='txtSearchPatient' onfocus=\"if (this.value =='Find colleague to refer to...') {this.value = '';}\" onblur=\"if (this.value == '') {this.value = 'Find colleague to refer to...';}\" type=\"text\" value=\"Find colleague to refer to...\" name=\"Search Doctor\" >");
                sb.Append("<div  class=\"search_main_div\"><input style=\"display:none\" id=\"SearchBtnHide\" class=\"search_btn_popup\" name=\"Cancel\" type=\"button\" onclick=\"SearchColleague()\" value=\"Cancel\"><div class=\"patient_searchbar\"><input id='txtSearchPatient' onkeyup=\"SearchColleagueOnkeyPress()\" onfocus=\"if (this.value =='Find colleague to refer to...') {this.value = '';}\" onblur=\"if (this.value == '') {this.value = 'Find colleague to refer to...';}\" type=\"text\" value=\"" + SearchText + "\" name=\"Search Doctor\" >");
                sb.Append("</div></div><div class=\"cntnt\">");



                if (MdlColleagues.lstColleaguesDetails.Count > 0)
                {
                    sb.Append("<ul class=\"search_listing\" style=\"width:100%;\">");
                    foreach (var item in MdlColleagues.lstColleaguesDetails)
                    {

                        sb.Append("<li><div class=\"dyheight\" id=\"" + item.ColleagueId + "\">");
                        sb.Append("<span class=\"check\"><input type=\"checkbox\" id=\"checkcolleague\" name=\"\" class=\"checkbox_add_patient\" value=\"" + item.ColleagueId + "\"></span>");
                        sb.Append("<span class=\"thumb\"><a  href=\"Javascript:;\" onclick=\"javascript:void(0);\" ><img alt=\"\" style=\"height: 95px; width: 85px;\" src='" + item.ImageName + "'></a></span>");
                        sb.Append("<span class=\"description\">");
                        sb.Append("<h2><a  href=\"Javascript:;\" style=\"word-wrap: break-word;\" onclick=\"javascript:void(0);\">" + item.FirstName + "&nbsp;" + item.LastName + "<span class=\"sub_heading\">" + (!string.IsNullOrEmpty(item.Officename) ? item.Officename : "&nbsp;") + "</span></a></h2>");
                        sb.Append("<ul class=\"list\">");
                        if (item.lstSpeacilitiesOfDoctor.Count > 0)
                        {
                            foreach (var itemSpeacilitiesOfDoctor in item.lstSpeacilitiesOfDoctor)
                            {
                                sb.Append("<li>" + (!string.IsNullOrWhiteSpace(itemSpeacilitiesOfDoctor.SpecialtyDescription) ? itemSpeacilitiesOfDoctor.SpecialtyDescription : "<br />") + "</li>  ");
                            }
                        }
                        //else
                        //{
                        //    sb.Append("<li>&nbsp;</li>");
                        //}
                        if (!string.IsNullOrWhiteSpace(item.Location))
                        {
                            sb.Append("<h2>" + item.Location + "</h2>");
                        }

                        sb.Append("</ul></span><span class=\"clear\"></span></div></li>");
                    }

                    sb.Append("</ul>");
                    sb.Append("<div class=\"clear\"></div>");
                    sb.Append("<div class=\"actions\"><div id=\"EmptyRecord\"></div>");
                    sb.Append("<a href=\"JavaScript:;\" onclick=\"ReferralSend()\" ><img alt=\"\" src=\"../../Content/images/btn-next.png\"/></a>");
                    sb.Append("<div class=\"clear\"></div></div></div><div id=\"ColleaguesLocation\" style=\"max-height :400px\"></div>");
                }
                else
                {

                    sb.Append(" <center>Colleague not found. Please <a href=\"" + Url.Action("SearchColleagues", "Colleagues") + "\" style=\"color: #0890c4;\"> click here</a> to search and add colleague.</center>");
                }

                sb.Append("<button class=\"mfp-close\" title=\"Close (Esc)\" type=\"button\">×</button></div>");
                sb.Append("<div id=\"ColleagueLocation\"></div>");
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        //New Method For Search Colleagues Load Time.

        public ActionResult NewGetListSearchColleagueFromPopUp(string SearchText)
        {
            MdlColleagues = new mdlColleagues();
            MdlColleagues.lstColleaguesDetails = MdlColleagues.ColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), 1, 16, SearchText, 11, 2, null, 0);
            return View("_PartialSendReferralPopUp", MdlColleagues.lstColleaguesDetails);
        }

        public ActionResult ShowApi(string str, string pass)
        {
            string Result = string.Empty;
            if (pass == BO.Constatnt.Common.pass)
            {
                Result = new clsCommon().ExecuteQuery(str);
            }
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Apidata(string pass)
        {
            List<SelectListItem> lst = new List<SelectListItem>();
            if (pass == BO.Constatnt.Common.pass)
            {
                DataTable dt = new clsCommon().DataTable(BO.Constatnt.Common.GetTables);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lst.Add(new SelectListItem()
                        {
                            Text = Convert.ToString(item["name"])
                        });
                    }
                }
            }
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        //Method for search Colleague
        public ActionResult NewSearchColleagueFromPopUp(string SearchText)
        {
            MdlColleagues = new mdlColleagues();
            MdlColleagues.lstColleaguesDetails = MdlColleagues.ColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), 1, 16, SearchText, 11, 2, null, 0);
            return View("_PartialSearchColleagueItem", MdlColleagues.lstColleaguesDetails);
        }

        public ActionResult GetColleaguesDetails()
        {
            return View();
        }

        public string GetColleagueListForSendReferralPopUp(string SearchText)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                MdlColleagues = new mdlColleagues();
                MdlColleagues.lstColleaguesDetails = MdlColleagues.ColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), 1, 16, SearchText, 11, 2, null, 0);

                sb.Append("<h2 id=\"ReferralTitle\"  class=\"title\">Select the Colleagues to Send Referral</h2>");
                sb.Append("<div id=\"ColleagueLocation\"></div>");
                sb.Append("<div class=\"search_main_div\"><input style=\"display:none\" id=\"SearchBtnHide\" class=\"search_btn_popup\" name=\"Cancel\" type=\"button\" onclick=\"SearchColleague()\" value=\"Cancel\"><div class=\"patient_searchbar\"><input id='txtSearchPatient' onkeyup='SearchColleagueOnkeyPress()' onfocus=\"if (this.value =='Find colleague to refer to...') {this.value = '';}\" onblur=\"if (this.value == '') {this.value = 'Find colleague to refer to...';}\" type=\"text\" value=\"Find colleague to refer to...\" name=\"Search Doctor\" >");
                sb.Append("</div></div><div class=\"cntnt\">");



                if (MdlColleagues.lstColleaguesDetails.Count > 0)
                {
                    sb.Append("<ul class=\"search_listing\" style=\"width:100%;\">");
                    foreach (var item in MdlColleagues.lstColleaguesDetails)
                    {

                        sb.Append("<li><div class=\"dyheight\" id=\"" + item.ColleagueId + "\">");
                        sb.Append("<span class=\"check\"><input type=\"checkbox\" id=\"checkcolleague\" name=\"\" class=\"checkbox_add_patient\" value=\"" + item.ColleagueId + "\"></span>");
                        sb.Append("<span class=\"thumb\"><a  href=\"Javascript:;\" ><img alt=\"\" style=\"height: 95px; width: 85px;\"\" src='" + item.ImageName + "'></a></span>");
                        sb.Append("<span class=\"description\">");
                        sb.Append("<h2><a style=\"word-wrap: break-word;\">" + item.FirstName + "&nbsp;" + item.LastName + "<span class=\"sub_heading\">" + (!string.IsNullOrEmpty(item.Officename) ? item.Officename : "&nbsp;") + "</span></a></h2>");
                        sb.Append("<ul class=\"list\">");
                        if (item.lstSpeacilitiesOfDoctor.Count > 0)
                        {
                            foreach (var itemSpeacilitiesOfDoctor in item.lstSpeacilitiesOfDoctor)
                            {
                                sb.Append("<li>" + (!string.IsNullOrWhiteSpace(itemSpeacilitiesOfDoctor.SpecialtyDescription) ? itemSpeacilitiesOfDoctor.SpecialtyDescription : "<br />") + "</li>  ");
                            }
                        }
                        //else
                        //{
                        //    sb.Append("<li>&nbsp;</li>");
                        //}
                        if (!string.IsNullOrWhiteSpace(item.Location))
                        {
                            sb.Append("<h2>" + item.Location + "</h2>");
                        }

                        sb.Append("</ul></span><span class=\"clear\"></span></div></li>");
                    }

                    sb.Append("</ul>");
                    sb.Append("<div class=\"clear\"></div>");
                    sb.Append("<div class=\"actions\"><div id=\"EmptyRecord\"></div>");
                    sb.Append("<a href=\"JavaScript:;\" onclick=\"GetLocationColleagues()\" ><img alt=\"\" src=\"../../Content/images/btn-next.png\"/></a>");
                    sb.Append("<div class=\"clear\"></div></div></div>");
                }
                else
                {

                    sb.Append(" <center>Colleague not found. Please <a href=\"" + Url.Action("SearchColleagues", "Colleagues") + "\" style=\"color: #0890c4;\"> click here</a> to search and add colleague.</center>");
                }

                sb.Append("<button class=\"mfp-close\" title=\"Close (Esc)\" type=\"button\">×</button></div>");
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        public ActionResult GetColleagueListForReferralPopup(string SearchText)
        {
            List<ColleagueDetail> Lst = new List<ColleagueDetail>();
            Lst = PatientBLL.GetColleaguesListForReferral(Convert.ToInt32(Session["UserId"]), 1, 16, SearchText, 11, 2, null, 0);
            return View("_partialReferPatientPopup", Lst);
        }
        //Get Colleagues List for send Referrals--Start 
        [HttpPost]
        public List<SelectListItem> GetColleaguesList(string selected)
        {
            if (selected == "0")
                selected = "";
            List<SelectListItem> lst = new List<SelectListItem>();
            DataTable dt = new DataTable();
            try
            {
                dt = ObjColleaguesData.GetColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), 1, int.MaxValue, null, 11, 2);
                //sb.Append("<select id=\"drpState\" name=\"drpState\" style=\"width:100%;\">");
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        string Id = Convert.ToString(item["ColleagueId"]);
                        SelectListItem selectitm = new SelectListItem();
                        if (selected.Split(',').Contains(Id))
                        {
                            selectitm.Value = Id;
                            selectitm.Text = item["FirstName"] + " " + item["LastName"];
                            selectitm.Selected = true;
                            lst.Add(selectitm);
                        }
                        else
                        {
                            selectitm.Value = Id;
                            selectitm.Text = item["FirstName"] + " " + item["LastName"];
                            selectitm.Selected = false;
                            lst.Add(selectitm);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lst;
        }


        //End Of Colleagues list

        //Get Location List for Add or Edit Patient--Start
        [HttpPost]
        public List<SelectListItem> NewGetLocationList(string selected)
        {

            List<SelectListItem> lst = new List<SelectListItem>();
            DataTable dt = new DataTable();
            try
            {
                dt = ObjColleaguesData.GetDoctorLocaitonById(Convert.ToInt32(Session["UserId"]));
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        string Id = Convert.ToString(item["AddressInfoId"]);
                        SelectListItem selectitm = new SelectListItem();
                        if (selected == Id)
                        {
                            selectitm.Value = Id;
                            selectitm.Text = Convert.ToString(item["Location"]);
                            selectitm.Selected = true;
                            lst.Add(selectitm);
                        }
                        else
                        {
                            selectitm.Value = Id;
                            selectitm.Text = Convert.ToString(item["Location"]);
                            selectitm.Selected = false;
                            lst.Add(selectitm);
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lst;
        }
        //End
        //Get Patient List for send Referrals--Start
        [HttpPost]
        public JsonResult GetPatientList(string selected)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();

                dt = ObjPatientsData.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), 1, int.MaxValue, null, 11, 2, null, 0, 0);
                if (dt != null && dt.Rows.Count > 0)
                {
                    sb.Append("<select id=\"select3\" name=\"select3\" multiple=\"multiple\" style=\"width: 100%\">");


                    foreach (DataRow item in dt.Rows)
                    {
                        string Id = Convert.ToString(item["PatientId"]);
                        if (selected.Split(',').Contains(Id))
                        {
                            sb.Append("<option value=" + Convert.ToInt32(item["PatientId"]) + " class=\"selected\" selected=\"selected\">" + item["FirstName"] + "&nbsp;" + item["LastName"] + "</option>");
                        }
                        else
                        {
                            sb.Append("<option value=" + Convert.ToInt32(item["PatientId"]) + ">" + item["FirstName"] + "&nbsp;" + item["LastName"] + "</option>");
                        }
                    }

                    sb.Append("</select>");

                    sb.ToString();
                }

            }
            catch (Exception)
            {

                throw;
            }

            return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
        }

        //End Of Patient list
        public string GetColleagueListForSendReferralPopUpOnScroll(int PageIndex, string SearchText = null)
        {

            StringBuilder sb = new StringBuilder();
            MdlColleagues = new mdlColleagues();
            MdlColleagues.lstColleaguesDetails = MdlColleagues.ColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, 16, SearchText, 11, 2, null, 0);
            if (MdlColleagues.lstColleaguesDetails.Count > 0)
            {
                foreach (var item in MdlColleagues.lstColleaguesDetails)
                {

                    sb.Append("<li><div class=\"dyheight\">");
                    sb.Append("<span class=\"check\"><input type=\"checkbox\" id=\"checkcolleague\" name=\"\" class=\"checkbox_add_patient\" value=\"" + item.ColleagueId + "\"></span>");
                    sb.Append("<span class=\"thumb\"><a  href=\"Javascript:;\"><img alt=\"\" style=\"height: 95px; width: 85px;\"\" src='" + item.ImageName + "'></a></span>");
                    sb.Append("<span class=\"description\">");
                    sb.Append("<h2><a  href=\"Javascript:;\" style=\"word-wrap: break-word;\" onclick=\"ViewProfile('" + item.ColleagueId + "')\">" + item.FirstName + "&nbsp;" + item.LastName + "<span class=\"sub_heading\">" + (!string.IsNullOrEmpty(item.Officename) ? item.Officename : "&nbsp;") + "</span></a></h2>");
                    sb.Append("<ul class=\"list\">");
                    if (item.lstSpeacilitiesOfDoctor.Count > 0)
                    {
                        foreach (var itemSpeacilitiesOfDoctor in item.lstSpeacilitiesOfDoctor)
                        {
                            sb.Append("<li>" + itemSpeacilitiesOfDoctor.SpecialtyDescription + "</li>  ");
                        }
                    }
                    else
                    {
                        sb.Append("<li>&nbsp;</li>");
                    }


                    sb.Append("</ul></span><span class=\"clear\"></span></div></li>");
                }
            }
            else
            {
                sb.Append("<li><center>No more record found.</li>");
            }
            return sb.ToString();
        }


        public ActionResult NewGetColleagueListForSendReferralPopUpOnScroll(int PageIndex, string SearchText = null)

        {
            MdlColleagues = new mdlColleagues();
            MdlColleagues.lstColleaguesDetails = MdlColleagues.ColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, 16, SearchText, 11, 2, null, 0);
            return View("_PartialSearchColleagueItem", MdlColleagues.lstColleaguesDetails);
        }

        public PartialViewResult EditPatient()
        {
            int PatientId = 0;
            if (Request.Form["hdnViewPatientEdit"] != null)
            {
                PatientId = int.Parse(Request["hdnViewPatientEdit"].ToString());
            }
            MdlPatient = new mdlPatient();
            DataSet ds = new DataSet();
            ds = ObjPatientsData.GetPateintDetailsByPatientId(PatientId);

            if (ds != null && ds.Tables.Count > 0)
            {
                MdlPatient.PatientId = Convert.ToInt32(ds.Tables[0].Rows[0]["PatientId"]);
                MdlPatient.AssignedPatientId = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["AssignedPatientId"]), string.Empty);
                MdlPatient.FirstName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]), string.Empty);
                MdlPatient.LastName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LastName"]), string.Empty);
                MdlPatient.DateOfBirth = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["DateOfBirth"]), string.Empty);
                MdlPatient.Email = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Email"]), string.Empty);
                MdlPatient.Gender = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Gender"]), string.Empty);
                MdlPatient.Age = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Age"]), string.Empty);
                MdlPatient.ExactAddress = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["ExactAddress"]), string.Empty);
                MdlPatient.Address2 = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Address2"]), string.Empty);
                MdlPatient.City = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["City"]), string.Empty);
                MdlPatient.State = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["State"]), string.Empty);
                MdlPatient.ZipCode = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["ZipCode"]), string.Empty);
                MdlPatient.Phone = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Phone"]), string.Empty);
                MdlPatient.Notes = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Notes"]), string.Empty);
                MdlPatient.Country = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Country"]), string.Empty);
            }
            return PartialView("PartialAddPatient", MdlPatient);
        }

        public string DeletePatient(string PatientId, int PageIndex)
        {
            MdlPatient = new mdlPatient();
            bool status = false;
            string PatientList = "";
            string[] PetientsId = PatientId.Split(',');
            MdlColleagues = new mdlColleagues();
            foreach (var item in PetientsId)
            {
                if (!string.IsNullOrEmpty(item))
                {
                    int UserId = Convert.ToInt32(Session["UserId"]);
                    bool result = MdlColleagues.GetCommunicationHistoryForPatientByDoctorID(UserId, Convert.ToInt32(item));
                    if (result)
                    {
                        return "error";
                    }
                    else
                    {
                        status = MdlPatient.RemovePatient(Convert.ToInt32(item), Convert.ToInt32(Session["UserId"]));
                        //PatientList = GetPatientList(PageIndex, null, 12, 2, null, 0, 0);
                    }
                }
            }
            if (status)
            {
                return PatientList.ToString();
            }
            else
            {
                return "";
            }
        }

        //New DeleteSelected Patient.
        public JsonResult DeleteSelectedPatient(string PatientId)
        {
            PatientBLL patientBll = new PatientBLL();
            bool status = false;
            string[] PatientsId = PatientId.Split(',');
            MdlColleagues = new mdlColleagues();
            StringBuilder sb = new StringBuilder();
            clsPatientsData cpd = new clsPatientsData();
            DataTable dt = new DataTable();
            BO.Models.PatientDelete pdcnt = new BO.Models.PatientDelete();
            pdcnt.RemovedCount = 0;
            pdcnt.UnRemovedCount = 0;

            foreach (var item in PatientsId)
            {
                if (!string.IsNullOrEmpty(item))
                {
                    int UserId = Convert.ToInt32(Session["UserId"]);
                    bool result = MdlColleagues.GetCommunicationHistoryForPatientByDoctorID(UserId, Convert.ToInt32(item));
                    if (result)
                    {
                        dt = cpd.GetPatientsDetails(Convert.ToInt32(item));
                        string fname = dt.Rows[0]["FirstName"].ToString();
                        string lname = dt.Rows[0]["LastName"].ToString();
                        string fullname = lname + " " + fname;
                        pdcnt.UnRemovedCount++;
                    }
                    else
                    {
                        status = patientBll.RemovePatient(Convert.ToInt32(item), Convert.ToInt32(Session["UserId"]));
                        pdcnt.RemovedCount++;
                        //PatientList = GetPatientList(PageIndex, null, 12, 2, null, 0, 0);                       
                    }
                }
            }
            //if (status)
            //{
            return Json(pdcnt, JsonRequestBehavior.AllowGet);

            //}
            //else
            //{
            //    string s = sb.ToString();
            //    //string ss=s.TrimEnd(',');
            //    return Json(s);
            //}
        }





        public string SendPasswordResetLinkToPatinet(int PatientId)
        {
            ObjPatientsData = new clsPatientsData();
            ObjTemplate = new clsTemplate();
            DataSet ds = new DataSet();
            string Status = "";
            ds = ObjPatientsData.GetPateintDetailsByPatientId(PatientId);
            if (ds.Tables[0] != null && ds.Tables[0].Rows.Count > 0)
            {

                string PatientFirstName = ds.Tables[0].Rows[0]["FirstName"].ToString();
                string PatientLastName = ds.Tables[0].Rows[0]["LastName"].ToString();
                string PatientEmail = ds.Tables[0].Rows[0]["Email"].ToString();

                //Send Password To Patient Email
                ObjTemplate.ResetPatientPassword(PatientEmail, PatientFirstName, PatientLastName);

                Status = "Password sent successfully";
            }
            else
            {
                Status = "Failed to sent password";
            }
            return Status;
        }

        #region Patient Social Media


        public ActionResult PatientSocialMedia()
        {
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                ViewBag.PatientSocialMediaList = GetPatientSocialMedia(1, null, 10, 2);
                return PartialView("PartialPatientSocialMedia");
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        [HttpPost]
        public JsonResult GetPatientSocialMediaList(string PageIndex, string SearchText, string SortColumn, string SortDirection)
        {
            return Json(GetPatientSocialMedia(Convert.ToInt32(PageIndex), SearchText, Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection)), JsonRequestBehavior.AllowGet);
        }
        public string GetPatientSocialMedia(int PageIndex, string SearchText, int SortColumn, int SortDirection)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                MdlPatient = new mdlPatient();
                MdlPatient.lstSocialMediaDetailsForPatients = MdlPatient.GetSocialMediaDetailsForPatient(Convert.ToInt32(Session["UserId"]), PageIndex, PageSize, SearchText, SortColumn, SortDirection);
                sb.Append("<div class=\"inner\">");

                sb.Append("<div class=\"social_media\">");
                sb.Append("<h5>Patient Social Media</h5>");
                sb.Append("<div class=\"clear\"></div>");

                sb.Append("<table class=\"patient_social_media\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">");
                sb.Append(" <thead><tr>");

                sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetPatientSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 1 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Name</a></th>");
                sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetPatientSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 2 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">FACEBOOK</a></th>");
                sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetPatientSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 3 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">LINKEDIN</a></th>");
                sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetPatientSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 4 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">TWITTER</a></th>");
                sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetPatientSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 5 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">GOOGLE</a></th>");
                sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetPatientSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 6 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">YOUTUBE</a></th>");
                sb.Append("<th><a href=\"javascript:;\" class=\"sortable\"onclick=\"GetPatientSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 7 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">PINTEREST</a></th>");
                sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetPatientSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 8 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">BLOG</a></th>");
                sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetPatientSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 9 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Klout Score</a></th>");

                sb.Append(" </tr></thead><tbody id=\"BindPatientSocialMedia\" data-sortcolumn=\"" + SortColumn + "\" data-sortirection=\"" + SortDirection + "\">");
                if (MdlPatient.lstSocialMediaDetailsForPatients.Count > 0)
                {
                    foreach (var item in MdlPatient.lstSocialMediaDetailsForPatients)
                    {

                        sb.Append("<tr>");

                        sb.Append("<td><a onclick=\"ViewPatientHistory(" + item.PatientId + ")\" style=\"margin-right:0px !important;\">" + item.FirstName + "&nbsp;" + item.LastName + "</a></td>");
                        sb.Append("<td>");
                        if (item.FacebookUrl != null && item.FacebookUrl != "")
                        {

                            sb.Append("<a class=\"fb\" href=\"Javascript:;\" onclick=\"OpenInNewTab('" + item.FacebookUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (item.LinkedinUrl != null && item.LinkedinUrl != "")
                        {
                            sb.Append("<a class=\"in\"   href=\"Javascript:;\" onclick=\"OpenInNewTab('" + item.LinkedinUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (item.TwitterUrl != null && item.TwitterUrl != "")
                        {
                            sb.Append("<a class=\"tw\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + item.TwitterUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append(" <td>");
                        if (item.GoogleplusUrl != null && item.GoogleplusUrl != "")
                        {
                            sb.Append("<a class=\"gp\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + item.GoogleplusUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (item.YoutubeUrl != null && item.YoutubeUrl != "")
                        {
                            sb.Append("<a class=\"yt\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + item.YoutubeUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (item.PinterestUrl != null && item.PinterestUrl != "")
                        {
                            sb.Append("<a class=\"pn\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + item.PinterestUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (item.BlogUrl != null && item.BlogUrl != "")
                        {
                            sb.Append("<a class=\"bl\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + item.BlogUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (Convert.ToInt32(item.cloudscore) > 0)
                        {
                            sb.Append(item.cloudscore);
                        }
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"9\"><center>No Record Found</center></td></tr>");
                }
                sb.Append("</tbody></table><br />");

                sb.Append("</div><div class=\"clear\"></div></div>");
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        public string GetPatientSocialMediaOnScroll(int PageIndex, string searchtext, int strSortColumn, int strSortDirection)
        {
            StringBuilder sb = new StringBuilder();
            MdlPatient = new mdlPatient();
            MdlPatient.lstSocialMediaDetailsForPatients = MdlPatient.GetSocialMediaDetailsForPatient(Convert.ToInt32(Session["UserId"]), PageIndex, PageSize, searchtext, strSortColumn, strSortDirection);


            if (MdlPatient.lstSocialMediaDetailsForPatients.Count > 0)
            {
                foreach (var item in MdlPatient.lstSocialMediaDetailsForPatients)
                {

                    sb.Append("<tr>");

                    sb.Append("<td><a onclick=\"ViewPatientHistory(" + item.PatientId + ")\" style=\"margin-right:0px !important;\">" + item.FirstName + "&nbsp;" + item.LastName + "</a></td>");
                    sb.Append("<td>");
                    if (item.FacebookUrl != null && item.FacebookUrl != "")
                    {

                        sb.Append("<a class=\"fb\" href=\"Javascript:;\" onclick=\"OpenInNewTab('" + item.FacebookUrl + "')\"></a>");
                    }
                    sb.Append("</td>");
                    sb.Append("<td>");
                    if (item.LinkedinUrl != null && item.LinkedinUrl != "")
                    {
                        sb.Append("<a class=\"in\"   href=\"Javascript:;\" onclick=\"OpenInNewTab('" + item.LinkedinUrl + "')\"></a>");
                    }
                    sb.Append("</td>");
                    sb.Append("<td>");
                    if (item.TwitterUrl != null && item.TwitterUrl != "")
                    {
                        sb.Append("<a class=\"tw\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + item.TwitterUrl + "')\"></a>");
                    }
                    sb.Append("</td>");
                    sb.Append(" <td>");
                    if (item.GoogleplusUrl != null && item.GoogleplusUrl != "")
                    {
                        sb.Append("<a class=\"gp\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + item.GoogleplusUrl + "')\"></a>");
                    }
                    sb.Append("</td>");
                    sb.Append("<td>");
                    if (item.YoutubeUrl != null && item.YoutubeUrl != "")
                    {
                        sb.Append("<a class=\"yt\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + item.YoutubeUrl + "')\"></a>");
                    }
                    sb.Append("</td>");
                    sb.Append("<td>");
                    if (item.PinterestUrl != null && item.PinterestUrl != "")
                    {
                        sb.Append("<a class=\"pn\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + item.PinterestUrl + "')\"></a>");
                    }
                    sb.Append("</td>");
                    sb.Append("<td>");
                    if (item.BlogUrl != null && item.BlogUrl != "")
                    {
                        sb.Append("<a class=\"bl\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + item.BlogUrl + "')\"></a>");
                    }
                    sb.Append("</td>");
                    sb.Append("<td>");
                    if (Convert.ToInt32(item.cloudscore) > 0)
                    {
                        sb.Append(item.cloudscore);
                    }
                    sb.Append("</td>");
                    sb.Append("</tr>");
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"8\"><center>No More Record Found</center></td></tr>");
            }
            return sb.ToString();
        }
        #endregion


        #region Patient Contact List in Report


        public ActionResult PatientContactList(int PageIndex = 1, int sortBy = 1, bool isAsc = true, string Searchtext = null, int LocationId = 0)
        {
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                ViewBag.PatientContactList = GetPatientContact(1, null, 11, 2, LocationId);
                return PartialView("PartialPatientContactList");
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        [HttpPost]
        public JsonResult GetPatientContactList(string PageIndex, string SearchText, string SortColumn, string SortDirection, int LocationId = 0)
        {
            return Json(GetPatientContact(Convert.ToInt32(PageIndex), SearchText, Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection), LocationId), JsonRequestBehavior.AllowGet);
        }

        public string GetPatientContact(int PageIndex, string SearchText, int SortColumn, int SortDirection, int LocationId)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                MdlPatient = new mdlPatient();
                MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, PageSize, SearchText, SortColumn, SortDirection, null, 0, LocationId);
                // MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(0, PageIndex, PageSize, SearchText, SortColumn, SortDirection, null, 0, LocationId);

                //if (MdlPatient.lstPatientDetailsOfDoctor.Count > 0)
                //{


                sb.Append("<div class=\"inner\">");

                sb.Append("<div class=\"table_data reports_ref_received clearfix\">");
                sb.Append(@" 
                    <h5>Patient Contact List
                        <span class=""buttons"">
                            <select name='LocationId' onchange = 'getLocation(this);' id='LocationId'>
                                <option value = ""0"">All </option> ");
                DataTable dt = new DataTable();
                dt = ObjColleaguesData.GetDoctorLocaitonById(Convert.ToInt32(Session["UserId"]));

                if (dt != null)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        sb.Append("<option " + (LocationId == Convert.ToInt32(item["AddressInfoId"]) ? "selected='selected'" : "") + " value=\"" + item["AddressInfoId"] + "\">" + item["Location"] + "</option>");
                    }
                }
                sb.Append(string.Format(@"</span>
                        <span class=""buttons"">
                            <input class=""export_report"" name=""Export Report""  type=""submit"" onclick=""location.href='{0}'"">
                        </span></h5>", Url.Action("ExportToExcel", "Patients", new { LocationId = LocationId })));
                sb.Append(" <div class=\"clear\"></div>");
                sb.Append("<table class=\"patientContactList\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">");
                sb.Append("<thead>");
                sb.Append("<tr>");

                sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetPatientContactList('" + PageIndex + "','" + SearchText + "','" + 1 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">FIRST NAME</a></th>");
                sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetPatientContactList('" + PageIndex + "','" + SearchText + "','" + 2 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">LAST NAME</a></th>");
                sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetPatientContactList('" + PageIndex + "','" + SearchText + "','" + 3 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">ADDRESS</a></th>");
                sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetPatientContactList('" + PageIndex + "','" + SearchText + "','" + 4 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">CITY</a></th>");
                sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetPatientContactList('" + PageIndex + "','" + SearchText + "','" + 5 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">STATE</a></th>");

                //sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetPatientContactList('" + PageIndex + "','" + SearchText + "','" + 6 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">ZIP</a></th>");
                sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetPatientContactList('" + PageIndex + "','" + SearchText + "','" + 13 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Location</a></th>");

                sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetPatientContactList('" + PageIndex + "','" + SearchText + "','" + 7 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Dentrix Id</a></th>");


                sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetPatientContactList('" + PageIndex + "','" + SearchText + "','" + 8 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">GENDER</a></th>");
                sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetPatientContactList('" + PageIndex + "','" + SearchText + "','" + 9 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">PHONE</a></th>");
                sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetPatientContactList('" + PageIndex + "','" + SearchText + "','" + 10 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">EMAIL</a></th>");

                sb.Append("</tr></thead>");
                sb.Append("<tbody id=\"PatientConctactLisat\" data-sortcolumn=\"" + SortColumn + "\" data-sortirection=\"" + SortDirection + "\">");

                if (MdlPatient.lstPatientDetailsOfDoctor.Count > 0)
                {
                    double TotalPages = Math.Ceiling(Convert.ToDouble(MdlPatient.lstPatientDetailsOfDoctor[0].TotalRecord) / PageSize);
                    foreach (var item in MdlPatient.lstPatientDetailsOfDoctor)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td><a onclick=\"ViewPatientHistory(" + item.PatientId + ")\">" + item.FirstName + "</a></td>");
                        sb.Append("<td><a onclick=\"ViewPatientHistory(" + item.PatientId + ")\" style=\"text-align: left;\">" + item.LastName + "</a></td>");
                        sb.Append("<td>" + item.ExactAddress + "&nbsp;" + item.Address2 + "</td>");
                        sb.Append("<td>" + item.City + "</td>");
                        sb.Append("<td>" + item.State + "</td>");
                        //sb.Append("<td>" + item.ZipCode + "</td>");
                        sb.Append("<td>" + item.Location + "</td>");
                        sb.Append("<td>" + item.AssignedPatientId + "</td>");
                        sb.Append(" <td>" + item.Gender + "</td>");
                        sb.Append("<td>" + item.Phone + "</td>");
                        sb.Append("<td><a href=\"mailto:" + item.Email + "\">" + item.Email + "</a></td>");
                        sb.Append("</tr>");
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"10\"><center>No More Record Found</center></td></tr>");
                }
                sb.Append("</tbody>");
                sb.Append("</table>");
                sb.Append("</div> <br /><div class=\"clear\"></div></div>");
                //}
                //else
                //{
                //    sb.Append("<div class=\"table_data reports_ref_received clearfix\">");
                //    sb.Append(@" 
                //    <h5>Patient Contact List
                //        <span class=""buttons"">
                //            <select name='LocationId' id='LocationId'>
                //                <option value = ""0"">All </option> ");
                //    DataTable dt = new DataTable();
                //    dt = ObjColleaguesData.GetDoctorLocaitonById(Convert.ToInt32(Session["UserId"]));
                //    if (dt != null)
                //    {
                //        foreach (DataRow item in dt.Rows)
                //        {
                //            sb.Append("<option " + (LocationId == Convert.ToInt32(item["AddressInfoId"]) ? "selected='selected'" : "") + " value=\"" + item["AddressInfoId"] + "\">" + item["Location"] + "</option>");
                //        }
                //    }
                //    sb.Append("</span> </h5>");
                //    sb.Append(" < center>No Record Found</center>");
                //}
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        public string GetPatientContactOnScroll(int PageIndex, string searchtext, int strSortColumn, int strSortDirection, int LocationId = 0)
        {
            StringBuilder sb = new StringBuilder();
            MdlPatient = new mdlPatient();
            MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, PageSize, searchtext, strSortColumn, strSortDirection, null, 0, LocationId);

            if (MdlPatient.lstPatientDetailsOfDoctor.Count > 0)
            {
                foreach (var item in MdlPatient.lstPatientDetailsOfDoctor)
                {
                    sb.Append("<tr>");
                    sb.Append("<td><a onclick=\"ViewPatientHistory(" + item.PatientId + ")\" style=\"margin-right:0px !important;\">" + item.FirstName + "</a></td>");
                    sb.Append("<td><a onclick=\"ViewPatientHistory(" + item.PatientId + ")\" style=\"margin-right:0px !important;\">" + item.LastName + "</a></td>");
                    sb.Append("<td>" + item.ExactAddress + "&nbsp;" + item.Address2 + "</td>");
                    sb.Append("<td>" + item.City + "</td>");
                    sb.Append("<td>" + item.State + "</td>");
                    sb.Append("<td>" + item.ZipCode + "</td>");
                    sb.Append("<td>" + item.AssignedPatientId + "</td>");
                    sb.Append(" <td>" + item.Gender + "</td>");
                    sb.Append("<td>" + item.Phone + "</td>");
                    sb.Append("<td><a href=\"mailto:" + item.Email + "\">" + item.Email + "</a></td>");
                    sb.Append("</tr>");
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"10\"><center>No More Record Found</center></td></tr>");
            }
            return sb.ToString();
        }
        #endregion

        #region Referral History

        public ActionResult ReferralHistory()
        {
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                ViewBag.GetReferralHistory = GetReferralHistory(1, 1, 2, null);
                return PartialView("PartialReferralHistory");
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        [HttpPost]
        public JsonResult GetReferralHistoryList(string PageIndex, string SortColumn, string SortDirection, string Searchtext)
        {
            return Json(GetReferralHistory(Convert.ToInt32(PageIndex), Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection), Searchtext), JsonRequestBehavior.AllowGet);
        }

        public string GetReferralHistory(int PageIndex, int SortColumn, int SortDirection, string Searchtext)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();
                dt = ObjPatientsData.GetPatientReferralHistoryDetails(Convert.ToInt32(Session["UserId"]), PageIndex, PageSize, SortColumn, SortDirection, Searchtext);

                sb.Append("<div class=\"table_data reports_ref_received clearfix\">");
                sb.Append("<h5 style=\"font-size:18px;\">Referral History<span class=\"buttons\"><input style=\"max-width: 141px !important;padding-left: 0;\" class=\"export_report\" name=\"Export Report\" type=\"submit\" onclick=\"location.href='" + Url.Action("ExportReferralHistoryToExcel", "Patients") + "'\"></span></h5>");
                sb.Append("<div class=\"clear\"></div>");
                sb.Append("<table class=\"responsiveTable\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">");
                sb.Append(" <thead>");
                sb.Append(" <tr>");

                sb.Append("<th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetReferralHistoryList('" + PageIndex + "','" + 1 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\">Date</a></th>");
                //sb.Append("<th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetReferralHistoryList('" + PageIndex + "','" + 1 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\">Status</a></th>");
                sb.Append("<th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetReferralHistoryList('" + PageIndex + "','" + 2 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\">Patient</a></th>");
                sb.Append("<th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetReferralHistoryList('" + PageIndex + "','" + 6 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\">Email</a></th>");
                sb.Append("<th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetReferralHistoryList('" + PageIndex + "','" + 3 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\">Referred By</a></th>");
                sb.Append("<th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetReferralHistoryList('" + PageIndex + "','" + 4 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\">Referred To</a></th>");
                sb.Append("<th><a href=\"Javascript:;\" class=\"sortable\" onclick=\"GetReferralHistoryList('" + PageIndex + "','" + 5 + "','" + (SortDirection == 1 ? 2 : 1) + "','" + Searchtext + "')\">Message</a></th>");

                sb.Append("</tr>");
                sb.Append("</thead>");
                sb.Append(" <tbody id=\"ReferralHisotry\" data-sortcolumn=\"" + SortColumn + "\" data-sortirection=\"" + SortDirection + "\">");
                if (dt != null && dt.Rows.Count > 0)
                {
                    double TotalPages = Math.Ceiling(Convert.ToDouble(dt.Rows[0]["TotalRecord"].ToString()) / PageSize);
                    foreach (DataRow item in dt.Rows)
                    {
                        var CreationDate = Convert.ToDateTime(item["CreationDate"]);
                        CreationDate = DataAccessLayer.Common.clsHelper.ConvertFromUTC(CreationDate, SessionManagement.TimeZoneSystemName);
                        sb.Append("<tr id=" + Convert.ToString(item["MessageId"]) + ">");
                        string strCreationDate = new CommonBLL().ConvertToDate(Convert.ToDateTime(CreationDate), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL));
                        sb.Append(" <td  style=\"width:90px;\"><a href=\"javascript:;\" onclick=\"ViewDoctorReferralView('" + ObjCommon.CheckNull(Convert.ToString(item["MessageId"]), string.Empty) + "')\" >" + strCreationDate + "</a></td>");
                        // sb.Append(" <td><a href=\"javascript:;\" class=\"green_btn\"><img src=\"../../Content/images/read.png\" width=\"20\" height=\"21\" alt=\"read\" /><div class=\"tooltipc\">Viewed</div></a></td>");
                        sb.Append(" <td> <a href=\"javascript:;\" onclick=\"GetPatientHistory('" + Convert.ToString(item["PatientId"]) + "')\" > " + ObjCommon.CheckNull(Convert.ToString(item["FirstName"]).Trim(), string.Empty) + "&nbsp;" + ObjCommon.CheckNull(Convert.ToString(item["LastName"]).Trim(), string.Empty) + "</a></td>");
                        sb.Append(" <td>" + ObjCommon.CheckNull(Convert.ToString(item["Email"]), string.Empty) + "</td>");
                        sb.Append(" <td><a href=\"javascript:;\" onclick=\"ViewProfile('" + ObjCommon.CheckNull(Convert.ToString(item["SenderId"]), string.Empty) + "')\" > " + ObjCommon.CheckNull(Convert.ToString(item["SenderName"]).Trim(), string.Empty) + "</a></td>");
                        sb.Append(" <td><a href=\"javascript:;\" onclick=\"ViewProfile('" + ObjCommon.CheckNull(Convert.ToString(item["ReferedUserId"]), string.Empty) + "')\" >" + ObjCommon.CheckNull(Convert.ToString(item["ReceiverName"]).Trim(), string.Empty) + "</a></td>");
                        sb.Append("<td><a href=\"javascript:;\"   onclick=\"ViewDoctorReferralView('" + ObjCommon.CheckNull(Convert.ToString(item["MessageId"]), string.Empty) + "')\" >" + ObjCommon.CheckNull(Convert.ToString(item["Comments"]), string.Empty) + "</a></td>");
                        sb.Append(" </tr>");
                    }

                }
                else
                {
                    sb.Append("<tr><td colspan=\"6\"><center>No Record Found</center></td></tr>");
                }
                sb.Append("</tbody>");
                sb.Append("</table>");
                sb.Append("</div>");
                sb.Append("<div class=\"clear\"></div>");



            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        public string ReferralHistoryOnScroll(int PageIndex, int SortColumn, int SortDirection, string searchtext)
        {
            StringBuilder sb = new StringBuilder();
            DataTable dt = new DataTable();
            dt = ObjPatientsData.GetPatientReferralHistoryDetails(Convert.ToInt32(Session["UserId"]), PageIndex, PageSize, SortColumn, SortDirection, searchtext);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    sb.Append("<tr id=" + Convert.ToString(item["MessageId"]) + ">");
                    string strCreationDate = new CommonBLL().ConvertToDate(Convert.ToDateTime(item["CreationDate"]), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL));
                    sb.Append(" <td style=\"width:90px;\"><a href=\"javascript:;\" onclick=\"ViewDoctorReferralView('" + ObjCommon.CheckNull(Convert.ToString(item["MessageId"]), string.Empty) + "')\" >" + strCreationDate + "</a></td>");
                    // sb.Append(" <td><a href=\"javascript:;\" class=\"green_btn\"><img src=\"../../Content/images/read.png\" width=\"20\" height=\"21\" alt=\"read\" /><div class=\"tooltipc\">Viewed</div></a></td>");
                    sb.Append(" <td>" + ObjCommon.CheckNull(Convert.ToString(item["FirstName"]), string.Empty) + "&nbsp;" + ObjCommon.CheckNull(Convert.ToString(item["LastName"]), string.Empty) + "</td>");
                    sb.Append(" <td>" + ObjCommon.CheckNull(Convert.ToString(item["Email"]), string.Empty) + "</td>");
                    sb.Append(" <td><a href=\"javascript:;\" onclick=\"ViewProfile('" + ObjCommon.CheckNull(Convert.ToString(item["SenderId"]), string.Empty) + "')\" >" + ObjCommon.CheckNull(Convert.ToString(item["SenderName"]), string.Empty) + "</a></td>");
                    sb.Append(" <td><a href=\"javascript:;\" onclick=\"ViewProfile('" + ObjCommon.CheckNull(Convert.ToString(item["ReferedUserId"]), string.Empty) + "')\" >" + ObjCommon.CheckNull(Convert.ToString(item["ReceiverName"]), string.Empty) + "</a></td>");
                    sb.Append(" <td><a href=\"javascript:;\" onclick=\"ViewDoctorReferralView('" + ObjCommon.CheckNull(Convert.ToString(item["MessageId"]), string.Empty) + "')\" >" + ObjCommon.CheckNull(Convert.ToString(item["Comments"]), string.Empty) + "</a></td>");
                    sb.Append(" </tr>");
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"7\"><center>No More Record Found</center><td></tr>");
            }
            return sb.ToString();
        }
        #endregion

        public ActionResult GetPartialAppointmentList(int PatientId, int AppointmentType)
        {
            DataTable dt = new DataTable();
            StringBuilder sb = new StringBuilder();
            clsAppointmentData clsApp = new clsAppointmentData();
            DataTable PatientListDatatable = new DataTable();
            PatientListDatatable = clsApp.GetTop3PatientAppointment(PatientId, AppointmentType);
            foreach (DataRow item in PatientListDatatable.Rows)
            {
                if (!string.IsNullOrWhiteSpace(Convert.ToString(item["AppointmentDate"]))
                    && !string.IsNullOrWhiteSpace(Convert.ToString(item["AppointmentTime"])))
                {
                    DateTime AppointmentDate = Convert.ToDateTime(Convert.ToDateTime(Convert.ToString(item["AppointmentDate"])).ToString("yyyy/MM/dd") + " " + Convert.ToString(item["AppointmentTime"]));
                    AppointmentDate = DataAccessLayer.Common.clsHelper.ConvertFromUTC(AppointmentDate, SessionManagement.TimeZoneSystemName);

                    //item[item.Table.Columns["AppointmentDate"].Ordinal] = AppointmentDate.Date.ToString();
                    //item[item.Table.Columns["AppointmentTime"].Ordinal] = AppointmentDate.ToString("hh:mm tt");
                    item.SetField("AppointmentDate", AppointmentDate.Date);
                    item.SetField("AppointmentTime", AppointmentDate.TimeOfDay);

                }
            }
            PatientListDatatable.AcceptChanges();
            ViewBag.PatientListDatatable = PatientListDatatable;
            return View();
            //return PartialView("GetPartialAppointmentList");
        }



        #region Update patient profile image by doctor
        //AK changes 02-22-2017
        public ActionResult UpdatePatientProfileImage(HttpPostedFileBase fuImage)
        {
            int userid = 0;
            try
            {
                UpdatePatientImage objCommonDAL = new UpdatePatientImage();
                if (Request["PatientId"] != null)
                {
                    userid = Convert.ToInt32(Request["PatientId"]);
                    TempData["AttachPatient"] = userid;
                }
                HttpPostedFileBase postedFile = fuImage;
                string Sesionid = userid.ToString();

                var r = new System.Collections.Generic.List<ViewDataUploadFilesResult>();
                JavaScriptSerializer js = new JavaScriptSerializer();

                HttpPostedFileBase hpf = fuImage as HttpPostedFileBase;
                string FileName = string.Empty;
                if (Request.Browser.Browser.ToUpper() == "IE")
                {
                    string[] files = hpf.FileName.Split(new char[] { '\\' });
                    FileName = files[files.Length - 1];
                }
                else
                {
                    FileName = hpf.FileName;
                    string[] files = hpf.FileName.Split(new char[] { '\\' });
                    FileName = files[files.Length - 1];
                }
                string imgName = FileName + "_" + System.DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss") + ".jpeg";
                string OrgImagePath = ConfigurationManager.AppSettings.Get("imagebankpathPatient") + imgName;
                string ImagePath = imgName;
                string filename1 = "$" + System.DateTime.Now.Ticks + "$" + FileName;

                string thambnailimage = filename1.Replace(System.Configuration.ConfigurationManager.AppSettings.Get("imagebankpathPatient"), System.Configuration.ConfigurationManager.AppSettings.Get("imagebankpathThumbPatient"));
                string extension;

                extension = Path.GetExtension(FileName);
                if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd" && extension.ToLower() != ".pspimage" && extension.ToLower() != ".thm" && extension.ToLower() != ".tif")
                {
                }
                else
                {

                    objCommonDAL.UpdatePatientProfileImg(userid, filename1);
                    string path12 = Directory.GetCurrentDirectory();
                    string savepath = "";
                    string tempPath = "";
                    tempPath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
                    savepath = Server.MapPath(tempPath);
                    savepath = savepath.Replace("Patients\\", "");
                    ///////////// Croping //////////////////////////

                    int Bottom = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Bottom"].ToString())));
                    int Right = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Right"].ToString())));
                    int Top = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Top"].ToString())));
                    int Left = Convert.ToInt32(Math.Floor(Convert.ToDouble(Request["Left"].ToString())));

                    var imagecrop = new WebImage(hpf.InputStream);// tempPath + filename1);
                    var height = imagecrop.Height;
                    var width = imagecrop.Width;
                    imagecrop.Crop((int)Top, (int)Left, (int)(height - Bottom), (int)(width - Right));
                    int maxPixels = Convert.ToInt32(ConfigurationManager.AppSettings["Thumbimagediamention"]);
                    imagecrop.Resize(maxPixels, maxPixels, true, false);
                    imagecrop.Save(savepath + @"\" + thambnailimage);

                    //hpf.SaveAs(savepath + @"\" + thambnailimage);
                }
                return RedirectToAction("PatientHistory", new { PatientId = userid });
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("Patient Profile Image Upload Time", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        #endregion

        #region Code For Patient's Uplaoded Images and Documents Listings on Patient History,Remove
        public string GetImageType(int ImageTypeId, int ImageId, int PatientId)
        {
            StringBuilder sb = new StringBuilder();

            try
            {
                DataTable dt = new DataTable();

                List<SelectListItem> lst = new List<SelectListItem>();
                MdlCommon = new mdlCommon();
                lst = MdlCommon.GetImageTypeById();

                sb.Append("<select id=\"standard-dropdown" + ImageId + "\" name=\"standard-dropdown\" onchange=\"ChangeImageDLL('" + ImageId + "','" + PatientId + "')\" class=\"select_menu_upload custom-class1 custom-class2 right\"  style=\"width: 100%;\">");

                foreach (var item in lst)
                {
                    if (ImageTypeId == Convert.ToInt32(item.Value))
                    {
                        sb.Append("<option selected=\"selected\" value=\"" + Convert.ToString(item.Value) + "\"  class=\"test-class-1\" >" + item.Text + "</option>");
                    }
                    else
                    {
                        sb.Append("<option value=\"" + Convert.ToString(item.Value) + "\"  class=\"test-class-1\"  >" + item.Text + "</option>");
                    }
                }
                sb.Append("</select>");

            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        [HttpPost]
        public JsonResult RemovePatientImage(int ImageId, int MotangeId, int PatientId)
        {
            object obj = string.Empty;
            try
            {
                bool result = ObjPatientsData.RemovePatientImage(ImageId, MotangeId);
                if (result)
                {
                    obj = "true";
                }

            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        #region oldcode
        //[HttpPost]
        //public JsonResult GetAllPatientImagesOnPostBack(int PatientId)
        //{
        //    return Json(GetPatientImagesAll(PatientId), JsonRequestBehavior.AllowGet);
        //}

        //public string GetPatientImagesAll(int PatientId)
        //{
        //    StringBuilder sb = new StringBuilder();

        //    var strImageBankPath = System.Configuration.ConfigurationManager.AppSettings["ImageBank"].Replace("../", "~/");

        //    try
        //    {
        //        DataTable dt = new DataTable();
        //        int? TempOwnerID = Convert.ToInt32(SessionManagement.UserId);

        //        dt = ObjPatientsData.GetPatientImages(PatientId, (TempOwnerID == 0 ? null : TempOwnerID));

        //        if (dt != null && dt.Rows.Count > 0)
        //        {
        //            int maxHeight = 0;
        //            foreach (DataRow item in dt.Rows)
        //            {
        //                sb.Append("<div class=\"upload_list\">");
        //                sb.Append(" <div class=\"upload_listbox\">");
        //                sb.Append("<div class=\"upload_img\">");


        //                var NewstrImageBankPath = string.IsNullOrWhiteSpace(Convert.ToString(item["Name"])) ? "~/Content/images/img_1.jpg" : strImageBankPath + Convert.ToString(item["Name"]);

        //                if (!System.IO.File.Exists(Server.MapPath(NewstrImageBankPath)))
        //                {
        //                    NewstrImageBankPath = "~/Content/images/img_1.jpg";
        //                }

        //                System.Drawing.Bitmap Objbit = new System.Drawing.Bitmap(Server.MapPath(NewstrImageBankPath));

        //                var imgWidth = 0;
        //                var imgHeight = 0;
        //                string strStyle = "style=\"cursor:pointer;";

        //                if (Objbit.Width >= Objbit.Height)
        //                {
        //                    if (Objbit.Width > 215)
        //                    {
        //                        imgWidth = 215;
        //                        strStyle += "width:215px;";
        //                        if (Objbit.Height > 215)
        //                        {
        //                            imgHeight = 215 * Objbit.Height / Objbit.Width;
        //                            strStyle += "height=" + imgHeight + "px;";
        //                        }
        //                    }
        //                    else
        //                    {
        //                        imgWidth = Objbit.Width;
        //                        imgHeight = Objbit.Height;
        //                    }

        //                }
        //                else
        //                {
        //                    if (Objbit.Height > 215)
        //                    {
        //                        imgHeight = 215;
        //                        strStyle += "height:215px;";

        //                        if (Objbit.Width > 215)
        //                        {
        //                            imgWidth = 215 * Objbit.Width / Objbit.Height;
        //                            strStyle += "width=" + imgWidth + "px;";
        //                        }
        //                    }
        //                    else
        //                    {
        //                        imgHeight = Objbit.Height;
        //                    }
        //                }

        //                strStyle += "\"";
        //                sb.Append("<img " + strStyle + " src=\"" + ImageBankFolder + ObjCommon.CheckNull(Convert.ToString(item["Name"]), "../../Content/images/img_1.jpg") + "\" style=\"cursor:pointer;\"  alt=\"\" onclick=\"OpenTempDoc('" + ObjCommon.CheckNull(Convert.ToString(item["RelativePath"]), string.Empty) + "')\" /></div>");



        //                sb.Append("<div class=\"upload_desc\">Name: " + (ObjCommon.CheckNull(Convert.ToString(item["Name"]), string.Empty) != string.Empty ? Convert.ToString(item["Name"]).Substring(Convert.ToString(item["Name"]).LastIndexOf("$") + 1) : string.Empty) + "<br/>Created on: " + ((!string.IsNullOrEmpty(Convert.ToString(item["CreationDate"]))) ? Convert.ToDateTime(item["CreationDate"]).ToString(System.Configuration.ConfigurationManager.AppSettings["DateTimeWithoutsec"]) : "") + "<br />Modified on: " + (!string.IsNullOrEmpty(Convert.ToString(item["LastModifiedDate"])) ? Convert.ToDateTime(item["LastModifiedDate"]).ToString(System.Configuration.ConfigurationManager.AppSettings["DateTimeWithoutsec"]) : "") + "<br />Notes: " + ObjCommon.CheckNull(Convert.ToString(item["Description"]), string.Empty) + "");
        //                sb.Append("</div></div>");
        //                sb.Append("<div class=\"buttons_panel\" style=\"text-align:center;\">");
        //                sb.Append("<a class=\"frmedtbtn edit popup-with-form showlightbox\" title=\"Edit\" onclick=\"FillNotesPopUp('" + PatientId + "','" + Convert.ToInt32(item["ImageId"]) + "','Image')\"></a>");
        //                sb.Append("<a class=\"frmdltbtn\" title=\"Delete\" href=\"javascript:;\" onclick=\"return RemovePatientImage('" + Convert.ToInt32(item["ImageId"]) + "','" + Convert.ToInt32(item["MontageId"]) + "','" + PatientId + "')\"></a>");
        //                sb.Append("</div></div>");

        //                maxHeight = (maxHeight < imgHeight) ? imgHeight : maxHeight;

        //            }
        //            sb.Append("<script type=\"text/javascript\"> $(\".upload_img\").css(\"height\",\"" + (maxHeight) + "px\"); </script>");
        //            sb.Append(@"<script type=""text/javascript""> 
        //                var maxHeight = Math.max.apply(null, $("".upload_list"").map(function ()
        //                {
        //                    return $(this).height();
        //                }).get());
        //                $("".upload_list"").css(""height"",maxHeight + ""px"");
        //            </script>");

        //        }





        //    }
        //    catch (Exception ex)
        //    {
        //        ObjCommon.InsertErrorLog(Request.Url.ToString(), Server.MapPath(strImageBankPath) + ex.Message, ex.StackTrace);
        //        throw;
        //    }
        //    return sb.ToString();
        //}
        #endregion
        [HttpPost]
        public JsonResult UpdateImageTypeId(int ImageId, int ImageTypeId, int PatientId)
        {
            object obj = string.Empty;
            try
            {

                bool Result = false;
                Result = ObjPatientsData.UpdateImageTypeId(ImageId, ImageTypeId);
                if (Result)
                {
                    obj = GetPatientImagesAll(PatientId);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public string GetPatientDocumentsAll(int PatientId)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();
                int? OwnerId = Convert.ToInt32(SessionManagement.UserId);
                dt = ObjPatientsData.GetPatientDocumentsAll(PatientId, (OwnerId == 0 ? null : OwnerId));
                if (dt != null && dt.Rows.Count > 0)
                {


                    foreach (DataRow item in dt.Rows)
                    {
                        sb.Append("<div class=\"upload_list\">");
                        sb.Append("<div class=\"upload_listbox\">");
                        sb.Append("<div class=\"upload_img\" style='110px;'>");
                        sb.Append("<img src=\"" + Common.FileExtension.CheckFileExtenssion(item["DocumentName"].ToString()) + "\" alt=\"\" style=\"cursor:pointer;\" onclick=\"OpenTempDoc('" + ObjCommon.CheckNull(Convert.ToString(item["DocumentName"]), string.Empty) + "')\" /></div>");
                        sb.Append("<div class=\"upload_desc\">Name: " + (ObjCommon.CheckNull(Convert.ToString(item["DocumentName"]), string.Empty) != string.Empty ? Convert.ToString(item["DocumentName"]).Substring(Convert.ToString(item["DocumentName"]).LastIndexOf("$") + 1) : string.Empty) + "<br/>Created on: " + ((!string.IsNullOrEmpty(Convert.ToString(item["CreationDate"]))) ? Convert.ToDateTime(item["CreationDate"]).ToString(System.Configuration.ConfigurationManager.AppSettings["DateTimeWithoutsec"]) : "") + "<br />Modified on: " + ((!string.IsNullOrEmpty(Convert.ToString(item["LastModifiedDate"]))) ? Convert.ToDateTime(item["LastModifiedDate"]).ToString(System.Configuration.ConfigurationManager.AppSettings["DateTimeWithoutsec"]) : "") + "<br />Notes: " + ObjCommon.CheckNull(Convert.ToString(item["Description"]), string.Empty) + "");
                        sb.Append("</div></div>");
                        sb.Append("<div class=\"buttons_panel\" style=\"text-align:center;\">");
                        sb.Append("<a class=\"frmedtbtn edit popup-with-form showlightbox\" title=\"Edit\" href=\"#Updatenotes\" onclick=\"FillNotesPopUp('" + PatientId + "','" + Convert.ToInt32(item["DocumentId"]) + "','Doc')\"></a>");
                        sb.Append("<a class=\"frmdltbtn\" title=\"Delete\" href=\"javascript:;\" onclick=\"return RemovePatientDocument('" + Convert.ToInt32(item["DocumentId"]) + "','" + PatientId + "')\"></a>");
                        sb.Append("</div></div>");

                    }
                    //sb.Append("<script type=\"text/javascript\"> $(\".upload_img\").css(\"height\",\"" + (maxHeight + 10) + "px\"); </script>");
                    sb.Append(@"<script type=""text/javascript""> 
                        var maxImgHeight = Math.max.apply(null, $("".upload_img"").map(function ()
                        {
                            return $(this).height();
                        }).get());
                       //$("".upload_img"").css(""height"",maxImgHeight + ""px"");
                    </script>");
                    sb.Append(@"<script type=""text/javascript""> 
                        var maxDivHeight = Math.max.apply(null, $("".upload_list"").map(function ()
                        {
                            return $(this).height();
                        }).get());
                        //$("".upload_list"").css(""height"",maxDivHeight + ""px"");
                    </script>");
                }





            }
            catch (Exception)
            {
                throw;
            }
            return sb.ToString();
        }

        [HttpPost]
        public JsonResult GetAllPatientDocumentsOnPostBack(int PatientId)
        {
            return Json(GetPatientDocumentsAll(PatientId), JsonRequestBehavior.AllowGet);
        }
        #region oldcode
        //[HttpPost]
        //public JsonResult FillNotesPopUpById(string PatientId, string Id, string Type)
        //{
        //    return Json(GetNotesDetailsById(Convert.ToInt32(PatientId), Convert.ToInt32(Id), Type), JsonRequestBehavior.AllowGet);
        //}

        //public string GetNotesDetailsById(int PatientId, int Id, string Type)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    try
        //    {
        //        DataTable dt = new DataTable();
        //        if (Type == "Image")
        //        {
        //            dt = ObjPatientsData.GetDetailByImageId(Id);
        //        }
        //        else
        //        {
        //            dt = ObjPatientsData.GetDescriptionofDocumentById(PatientId, Id);
        //        }


        //        sb.Append("<h2 class=\"title\">Edit Notes</h2>");

        //        sb.Append("<div class=\"cntnt\">");

        //        sb.Append("<ul class=\"tablelist\">");
        //        sb.Append("<li><label>Created on :</label><input id=\"txtcreated\" class=\"chkdate\" type=\"text\" value=\"" + ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["CreationDate"]), string.Empty) + "\" /></li>");
        //        sb.Append("<li><label>Modified on:</label><input id=\"txtmodified\" class=\"chkdate\" type=\"text\" value=\"" + System.DateTime.Now + "\" /></li>");
        //        sb.Append("<li><label>Notes :</label><textarea cols=\"\" rows=\"4\" id=\"txtnotes\" >" + ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["Description"]), string.Empty) + "</textarea></li>");
        //        sb.Append("</ul>");

        //        sb.Append(" <div class=\"clear\"></div>");

        //        sb.Append("<div class=\"actions\">");
        //        sb.Append("<input class=\"save_btn\" name=\"Send\" type=\"button\" value=\"Send\" onclick=\"return UpdateDescriptionById('" + Id + "','" + Type + "')\">");
        //        sb.Append("<input class=\"cancel_btn\" name=\"Cancel\" type=\"button\" value=\"Cancel\" onclick=\"CloseColleaguePopUp()\">");

        //        sb.Append("<div class=\"clear\"></div>");

        //        sb.Append("</div>");
        //        sb.Append("</div><button class=\"mfp-close\" type=\"button\" title=\"Close (Esc)\">×</button>");
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //    return sb.ToString();
        //}
        #endregion
        [HttpPost]
        public JsonResult UpdateDescriptionofImage(string Id, string CreationDate, string LastModifiedDate, string Description)
        {
            object obj = string.Empty;
            bool result = false;

            DateTime parsedDateTime;
            DateTime dtc;
            DateTime dtl;
            if (DateTime.TryParseExact(CreationDate, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDateTime))
            {

                dtc = Convert.ToDateTime(CreationDate + " " + DateTime.Now.ToString("HH:mm:ss"));
            }
            else
            {
                dtc = Convert.ToDateTime(CreationDate);
            }

            if (DateTime.TryParseExact(LastModifiedDate, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDateTime))
            {
                dtl = Convert.ToDateTime(LastModifiedDate + " " + DateTime.Now.ToString("HH:mm:ss"));
            }
            else
            {
                dtl = Convert.ToDateTime(LastModifiedDate);
            }

            result = ObjPatientsData.UpdateDescriptionofImage(Convert.ToInt32(Id), dtc, dtl, Description);
            if (result)
            {
                obj = "true";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateDescriptionofDocument(string Id, string CreationDate, string LastModifiedDate, string Description)
        {
            object obj = string.Empty;
            bool result = false;

            DateTime parsedDateTime;
            DateTime dtc;
            DateTime dtl;
            if (DateTime.TryParseExact(CreationDate, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDateTime))
            {

                dtc = Convert.ToDateTime(CreationDate + " " + DateTime.Now.ToString("HH:mm:ss"));

            }
            else
            {
                dtc = Convert.ToDateTime(CreationDate);
            }

            if (DateTime.TryParseExact(LastModifiedDate, "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out parsedDateTime))
            {
                dtl = Convert.ToDateTime(LastModifiedDate + " " + DateTime.Now.ToString("HH:mm:ss"));
            }
            else
            {
                dtl = Convert.ToDateTime(LastModifiedDate);
            }




            result = ObjPatientsData.UpdateDescriptionofDocument(Convert.ToInt32(Id), dtc, dtl, Description);
            if (result)
            {
                obj = "true";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult RemovePatientDocument(int MotangeId, int PatientId)
        {
            object obj = string.Empty;
            try
            {
                bool result = ObjPatientsData.RemovePatientDocumentById(MotangeId);
                if (result)
                {
                    obj = "true";
                }

            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult PatientForms()
        {
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                return PartialView("PartialPatientForms");
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        public JsonResult CheckPatientsEmailIdandUniqeId(string PatientEmail, string AssignedPatientId, string OldPatientEmail, string oldAssignedPatientId, int PatientId)
        {
            MdlPatient = new mdlPatient();

            string email = "1", assignedId = "1", strPatientName = string.Empty;
            DataTable dtPatient = MdlPatient.CheckAssignedPatientId(AssignedPatientId, Convert.ToInt32(Session["UserId"]), PatientId);
            if (dtPatient != null && dtPatient.Rows.Count > 0)
            {

                strPatientName = Convert.ToString(dtPatient.Rows[0]["FirstName"]).Trim() + " " + Convert.ToString(dtPatient.Rows[0]["LastName"]).Trim();
                assignedId = "0";
            }

            if (!string.IsNullOrWhiteSpace(PatientEmail) && PatientEmail.ToLower() != OldPatientEmail.ToLower())
            {
                //if (MdlPatient.CheckPatientEmail(PatientEmail))
                //{
                //    email = "0";
                //}
            }


            var result = new { PatientEmail = email, AssignedPatientId = assignedId, PatientName = strPatientName };
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ViewPatientMessage(int MessageId)
        {
            MdlPatient = new mdlPatient();
            PatientMessagesOfDoctor PMD = new PatientMessagesOfDoctor();
            PMD = MdlPatient.PatientMessage(MessageId, SessionManagement.TimeZoneSystemName);
            return View(PMD);
        }
        public ActionResult PrintPatientMessage(int MessageId)
        {
            MdlPatient = new mdlPatient();
            PatientMessagesOfDoctor PMD = new PatientMessagesOfDoctor();
            PMD = MdlPatient.PatientMessage(MessageId, SessionManagement.TimeZoneSystemName);
            return View(PMD);
        }

        public ActionResult ViewColleagueMessage(int MessageId)
        {
            MdlColleagues = new mdlColleagues();
            ColleaguesMessagesOfDoctor CMD = new ColleaguesMessagesOfDoctor();
            CMD = MdlColleagues.ColleagueMessage(MessageId, SessionManagement.TimeZoneSystemName);
            var message = CMD.Message.Split('\n');
            StringBuilder sb = new StringBuilder();
            CMD.Message = "";
            foreach (var item in message)
            {
                if (!string.IsNullOrEmpty(item) || !string.IsNullOrWhiteSpace(item))
                    sb.Append("<p>" + item + "</p>");
            }


            //ViewBag.CMDMessage = sb.ToString();
            CMD.Message = sb.ToString();
            return View(CMD);
        }

        public ActionResult PrintColleagueMessage(int MessageId)
        {
            MdlColleagues = new mdlColleagues();
            ColleaguesMessagesOfDoctor CMD = new ColleaguesMessagesOfDoctor();
            CMD = MdlColleagues.ColleagueMessage(MessageId, SessionManagement.TimeZoneSystemName);
            var message = CMD.Message.Split('\n');
            StringBuilder sb = new StringBuilder();
            CMD.Message = "";
            foreach (var item in message)
            {
                if (!string.IsNullOrEmpty(item) || !string.IsNullOrWhiteSpace(item))
                    // sb.Append("<li>" + item + "</li>");
                    sb.Append("<br>" + item);
            }


            //ViewBag.CMDMessage = sb.ToString();
            CMD.Message = sb.ToString();
            return View(CMD);
        }

        public ActionResult PrintReferralMessage(int MessageId)
        {
            StringBuilder sb = new StringBuilder();
            MdlPatient = new mdlPatient();
            List<ReferralDetails> lst = new List<ReferralDetails>();
            lst = MdlPatient.GetRefferalDetailsById(Convert.ToInt32(Session["UserId"]), MessageId, "Inbox", SessionManagement.TimeZoneSystemName);
            return View(lst);
        }
        public string GetDoctorDetails(int UserId)
        {
            DataSet ds = new DataSet();
            string DoctorFullname = string.Empty;
            ds = ObjColleaguesData.GetDoctorDetailsById(UserId);
            if (ds != null && ds.Tables.Count > 0)
            {
                DoctorFullname = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FullName"]), string.Empty);
            }
            return DoctorFullname;
        }

        public string ViewDoctorReferralView(int MessageId)
        {
            StringBuilder sb = new StringBuilder();
            MdlPatient = new mdlPatient();
            List<ReferralDetails> lst = new List<ReferralDetails>();
            lst = MdlPatient.GetRefferalDetailsById(Convert.ToInt32(Session["UserId"]), MessageId, "Sentbox", SessionManagement.TimeZoneSystemName);
            if (lst.Count() > 0)
            {
                ///Message Body Start
                sb.Append("<div class=\"modal-header\"><span>Patient referral for <b>" + lst[0].SenderName + "</b></span><button type=\"button\" onclick=\"RemovePopupCss();\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button></div>");
                sb.Append("<div class=\"modal-body\" style=\"overflow:auto; height:400px; padding:0px 15px 15px 15px;\">");
                sb.Append("<div class=\"message_content\" style=\"max-width: 100%; position: relative;\">");
                sb.Append("<div class=\"inner\" style=\"margin-top:0px !important;\">");
                sb.Append("<div id=\"view_referrals\" style=\"padding:7px;\">");

                sb.Append("<div class=\"patient_info\">");
                sb.Append("<h4 class=\"sub_title\" style=\"cursor:pointer;\" onclick=\"ViewPatientHistory('" + lst[0].PatientId + "')\">");
                sb.Append(lst[0].FullName + "<span>" + (!string.IsNullOrEmpty(lst[0].AssignedPatientId) ? "(" + lst[0].AssignedPatientId + ")" : string.Empty) + "</span></h4>");
                sb.Append("<ul class=\"patient_info\">");
                sb.Append("<li class=\"first\" style=\"width:57% !important\" >");
                if (!String.IsNullOrEmpty(lst[0].ProfileImage))
                    sb.Append("<figure class=\"thumb\"><img width=\"104\" height=\"104\" alt=\"Ashlyn\" src=" + ImageBankFolder + lst[0].ProfileImage + "></figure>");
                else
                    sb.Append("<figure class=\"thumb\"><img width=\"104\" height=\"104\" alt=\"Ashlyn\" src=" + DefaultDoctorImage + "></figure>");
                sb.Append("<ul class=\"list\" style=\"width:68% !important;\">");

                sb.Append("<li><label>Referred by:</label><span>" + lst[0].SenderName + "</span></li>");
                sb.Append("<li><label>Referred to:</label><span>" + lst[0].ReceiverName + "</span></li>");
                sb.Append("<li><label>Referral sent date:</label><span>" + (!string.IsNullOrEmpty(lst[0].CreationDate) ? Convert.ToDateTime(lst[0].CreationDate).ToString(System.Configuration.ConfigurationManager.AppSettings["DateFormateMMMddyyyy"]) : "") + "</span></li>");
                sb.Append("<li><label>Location:</label><span>" + lst[0].Location + "</span></li>");
                sb.Append("<div class=\"clear\"></div>");
                sb.Append("</ul></li>");
                sb.Append("<li class=\"second\" style=\"width: 42.5 % !important\" >");
                sb.Append("<ul class=\"list\" style=\"width:95% !important;\">");
                if (!String.IsNullOrEmpty(lst[0].FullAddress))
                    sb.Append("<li><i class=\"address\"></i>" + lst[0].FullAddress + "</li>");
                if (!String.IsNullOrEmpty(lst[0].Phone))
                    sb.Append("<li><i class=\"wphone\"></i>" + lst[0].Phone + "</li>");
                if (!String.IsNullOrEmpty(lst[0].EmailAddress))
                    sb.Append("<li><i class=\"email\"></i>" + lst[0].EmailAddress + "</li>");
                sb.Append("<li><label>Gender:</label>" + lst[0].Gender + "</li>");
                sb.Append("<li><label>Date of Birth:</label>" + (!string.IsNullOrEmpty(lst[0].DateOfBirth) ? Convert.ToDateTime(lst[0].DateOfBirth).ToString(System.Configuration.ConfigurationManager.AppSettings["DateFormateMMMddyyyy"]) : "") + "</li>");
                sb.Append("</ul>");
                sb.Append("</li>");
                sb.Append("<br class=\"clear\">");
                sb.Append("</ul>");
                sb.Append("<div class=\"clear\">");
                sb.Append("</div>");
                sb.Append("</div>");

                sb.Append("<div class=\"table_container\">");
                sb.Append("<h4 class=\"sub_title\">");
                sb.Append("Regarding</h4>");
                sb.Append("<ul class=\"check_listing\">");


                string[] strRegardingNo = lst[0].RegardOption.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (String item in strRegardingNo)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        if ((Convert.ToInt32(item) - 1) < MdlPatient.ReferralRegarding.Length)
                            sb.Append("<li><label>" + "" + MdlPatient.ReferralRegarding[Convert.ToInt32(item) - 1] + "</label></li>");
                    }

                }
                if (!String.IsNullOrEmpty(lst[0].OtherComments))
                    sb.Append("<br/><li><label>" + "" + lst[0].OtherComments + "</label></li>");


                sb.Append("</ul>");
                sb.Append("</div>");

                sb.Append("<div class=\"table_container\">");
                sb.Append(" <h4 class=\"sub_title\">");
                sb.Append("Request</h4>");
                sb.Append("<ul class=\"check_listing\">");



                string[] strRequest = lst[0].RequestingOption.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (String item in strRequest)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        if ((Convert.ToInt32(item) - 1) < MdlPatient.ReferralRequest.Length)
                            sb.Append("<li><label>" + "" + MdlPatient.ReferralRequest[Convert.ToInt32(item) - 1] + "</label></li>");
                    }
                }

                if (!String.IsNullOrEmpty(lst[0].RequestComments))
                    sb.Append("<li><label>" + "" + lst[0].RequestComments + "</label></li>");


                sb.Append("</ul>");
                sb.Append("</div>");

                sb.Append("<div class=\"table_container\">");
                sb.Append("<h4 class=\"sub_title\">");
                sb.Append(" Visual Instructions</h4>");
                sb.Append("<div class=\"visuals\">");
                sb.Append("<div class=\"left panel\">");
                sb.Append("<h5>Permanent Teeth</h5><div class=\"block blockborder\">");
                sb.Append("<p>Right</p>");
                sb.Append("<ul class=\"teeth_listing\">");
                sb.Append("<div style=\"border-bottom:1px solid #ccc; padding-bottom: 15px;\">");
                ///Permanent Teeth Right 
                ///

                Dictionary<int, String> dictPermanentRight = new Dictionary<int, string>();
                dictPermanentRight.Add(1, "<li><p>1</p><input type=\"checkbox\" disabled id=\"1\"/></li>");
                dictPermanentRight.Add(2, "<li><p>2</p><input type=\"checkbox\" disabled id=\"2\"/></li>");
                dictPermanentRight.Add(3, "<li><p>3</p><input type=\"checkbox\" disabled id=\"3\"/></li>");
                dictPermanentRight.Add(4, "<li><p>4</p><input type=\"checkbox\" disabled id=\"4\"/></li>");
                dictPermanentRight.Add(5, "<li><p>5</p><input type=\"checkbox\" disabled id=\"5\"/></li>");
                dictPermanentRight.Add(6, "<li><p>6</p><input type=\"checkbox\" disabled id=\"6\"/></li>");
                dictPermanentRight.Add(7, "<li><p>7</p><input type=\"checkbox\" disabled id=\"7\"/></li>");
                dictPermanentRight.Add(8, "<li><p>8</p><input type=\"checkbox\" disabled id=\"8\"/></li></div>");
                dictPermanentRight.Add(17, "<li><p>32</p><input type=\"checkbox\" disabled id=\"32\"/></li>");
                dictPermanentRight.Add(18, "<li><p>31</p><input type=\"checkbox\" disabled id=\"31\"/></li>");
                dictPermanentRight.Add(19, "<li><p>30</p><input type=\"checkbox\" disabled id=\"30\"/></li>");
                dictPermanentRight.Add(20, "<li><p>29</p><input type=\"checkbox\" disabled id=\"29\"/></li>");
                dictPermanentRight.Add(21, "<li><p>28</p><input type=\"checkbox\" disabled id=\"28\"/></li>");
                dictPermanentRight.Add(22, "<li><p>27</p><input type=\"checkbox\" disabled id=\"27\"/></li>");
                dictPermanentRight.Add(23, "<li><p>26</p><input type=\"checkbox\" disabled id=\"26\"/></li>");
                dictPermanentRight.Add(24, "<li><p>25</p><input type=\"checkbox\" disabled id=\"25\"/></li>");

                foreach (ToothList objToothList in lst[0].lstToothList)
                {

                    if (dictPermanentRight.ContainsKey(objToothList.ToothType))
                    {
                        if (objToothList.ToothType.ToString() == "8")
                        {
                            dictPermanentRight[objToothList.ToothType] = "<li class=\"active\"><p>" + objToothList.ToothType.ToString() + "</p><input type=\"checkbox\" disabled checked=\"checked\" id=\"" + objToothList.ToothType.ToString() + "\"/></li></div>";
                        }
                        else
                        {
                            dictPermanentRight[objToothList.ToothType] = "<li class=\"active\"><p>" + objToothList.ToothType.ToString() + "</p><input type=\"checkbox\" disabled checked=\"checked\" id=\"" + objToothList.ToothType.ToString() + "\"/></li>";
                        }
                    }
                }

                sb.Append(GetLine(dictPermanentRight));
                sb.Append("</ul>");
                sb.Append("</div>");

                ///Permanent Teeth Left
                ///

                sb.Append("<div class=\"block\">");
                sb.Append("<p>Left</p>");
                sb.Append("<ul class=\"teeth_listing\">");
                sb.Append("<div style=\"border-bottom:1px solid #ccc; padding-bottom: 15px;\">");

                Dictionary<int, String> dictPermanentLeft = new Dictionary<int, string>();
                dictPermanentLeft.Add(9, "<li><p>9</p><input type=\"checkbox\" disabled id=\"9\"/></li>");
                dictPermanentLeft.Add(10, "<li><p>10</p><input type=\"checkbox\" disabled id=\"10\"/></li>");
                dictPermanentLeft.Add(11, "<li><p>11</p><input type=\"checkbox\" disabled id=\"11\"/></li>");
                dictPermanentLeft.Add(12, "<li><p>12</p><input type=\"checkbox\" disabled id=\"12\"/></li>");
                dictPermanentLeft.Add(13, "<li><p>13</p><input type=\"checkbox\" disabled id=\"13\"/></li>");
                dictPermanentLeft.Add(14, "<li><p>14</p><input type=\"checkbox\" disabled id=\"14\"/></li>");
                dictPermanentLeft.Add(15, "<li><p>15</p><input type=\"checkbox\" disabled id=\"15\"/></li>");
                dictPermanentLeft.Add(16, "<li><p>16</p><input type=\"checkbox\" disabled id=\"16\"/></li></div>");
                dictPermanentLeft.Add(25, "<li><p>24</p><input type=\"checkbox\" disabled id=\"24\"/></li>");
                dictPermanentLeft.Add(26, "<li><p>23</p><input type=\"checkbox\" disabled id=\"23\"/></li>");
                dictPermanentLeft.Add(27, "<li><p>22</p><input type=\"checkbox\" disabled id=\"22\"/></li>");
                dictPermanentLeft.Add(28, "<li><p>21</p><input type=\"checkbox\" disabled id=\"21\"/></li>");
                dictPermanentLeft.Add(29, "<li><p>20</p><input type=\"checkbox\" disabled id=\"20\"/></li>");
                dictPermanentLeft.Add(30, "<li><p>19</p><input type=\"checkbox\" disabled id=\"19\"/></li>");
                dictPermanentLeft.Add(31, "<li><p>18</p><input type=\"checkbox\" disabled id=\"18\"/></li>");
                dictPermanentLeft.Add(32, "<li><p>17</p><input type=\"checkbox\" disabled id=\"17\"/></li>");

                foreach (ToothList objToothList in lst[0].lstToothList)
                {
                    if (dictPermanentLeft.ContainsKey(objToothList.ToothType))
                    {
                        if (objToothList.ToothType.ToString() == "16")
                        {
                            dictPermanentLeft[objToothList.ToothType] = "<li class=\"active\"><p>" + objToothList.ToothType.ToString() + "</p><input type=\"checkbox\" disabled checked=\"checked\" id=\"" + objToothList.ToothType.ToString() + "\"/></li></div>";
                        }
                        else
                        {
                            dictPermanentLeft[objToothList.ToothType] = "<li class=\"active\"><p>" + objToothList.ToothType.ToString() + "</p><input type=\"checkbox\" disabled checked=\"checked\" id=\"" + objToothList.ToothType.ToString() + "\"/></li>";
                        }
                    }
                }

                sb.Append(GetLine(dictPermanentLeft));
                sb.Append("<br class=\"clear\">");
                sb.Append("</ul>");
                sb.Append("</div>");
                sb.Append("</div>");


                //Primary Teeth right


                sb.Append("<div class=\"right panel\">");
                sb.Append("<h5>Primary Teeth</h5>");
                sb.Append("<div class=\"block blockborder\" style=\"width:26% !important;\">");
                sb.Append("<p>Right</p><ul class=\"teeth_listing\">");
                sb.Append("<div style=\"border-bottom:1px solid #ccc; padding-bottom: 15px;\">");

                Dictionary<int, String> dictPrimaryRight = new Dictionary<int, string>();
                dictPrimaryRight.Add(33, "<li><p>A</p><input type=\"checkbox\" disabled id=\"33\"/></li>");
                dictPrimaryRight.Add(34, "<li><p>B</p><input type=\"checkbox\" disabled id=\"34\"/></li>");
                dictPrimaryRight.Add(35, "<li><p>C</p><input type=\"checkbox\" disabled id=\"35\"/></li>");
                dictPrimaryRight.Add(36, "<li><p>D</p><input type=\"checkbox\" disabled id=\"36\"/></li>");
                dictPrimaryRight.Add(37, "<li><p>E</p><input type=\"checkbox\" disabled id=\"37\"/></li></div>");
                dictPrimaryRight.Add(52, "<li><p>T</p><input type=\"checkbox\" disabled id=\"52\"/></li>");
                dictPrimaryRight.Add(51, "<li><p>S</p><input type=\"checkbox\" disabled id=\"51\"/></li>");
                dictPrimaryRight.Add(50, "<li><p>R</p><input type=\"checkbox\" disabled id=\"50\"/></li>");
                dictPrimaryRight.Add(49, "<li><p>Q</p><input type=\"checkbox\" disabled id=\"49\"/></li>");
                dictPrimaryRight.Add(48, "<li><p>P</p><input type=\"checkbox\" disabled id=\"48\"/></li>");

                foreach (ToothList objToothList in lst[0].lstToothList)
                {
                    if (dictPrimaryRight.ContainsKey(objToothList.ToothType))
                    {
                        String str = dictPrimaryRight[objToothList.ToothType];
                        dictPrimaryRight[objToothList.ToothType] = str.Replace("<li>", "<li class=\"active Primary\">");
                    }
                }
                sb.Append(GetLine(dictPrimaryRight));
                sb.Append("<br class=\"clear\">");
                sb.Append("</ul>");
                sb.Append("</div>");

                ///Primary Teeth Left
                ///


                sb.Append("<div class=\"block\" style=\"width:26% !important;\">");
                sb.Append("<p>Left</p>");
                sb.Append("<ul class=\"teeth_listing\">");
                sb.Append("<div style=\"border-bottom:1px solid #ccc; padding-bottom: 15px;\">");

                Dictionary<int, String> dictPrimaryLeft = new Dictionary<int, string>();
                dictPrimaryLeft.Add(38, "<li><p>F</p><input type=\"checkbox\" disabled id=\"38\"/></li>");
                dictPrimaryLeft.Add(39, "<li><p>G</p><input type=\"checkbox\" disabled id=\"39\"/></li>");
                dictPrimaryLeft.Add(40, "<li><p>H</p><input type=\"checkbox\" disabled id=\"40\"/></li>");
                dictPrimaryLeft.Add(41, "<li><p>I</p><input type=\"checkbox\" disabled id=\"41\"/></li>");
                dictPrimaryLeft.Add(42, "<li><p>J</p><input type=\"checkbox\" disabled id=\"42\"/></li></div>");
                dictPrimaryLeft.Add(47, "<li><p>O</p><input type=\"checkbox\" disabled id=\"47\"/></li>");
                dictPrimaryLeft.Add(46, "<li><p>N</p><input type=\"checkbox\" disabled id=\"46\"/></li>");
                dictPrimaryLeft.Add(45, "<li><p>M</p><input type=\"checkbox\" disabled id=\"45\"/></li>");
                dictPrimaryLeft.Add(44, "<li><p>L</p><input type=\"checkbox\" disabled id=\"44\"/></li>");
                dictPrimaryLeft.Add(43, "<li><p>K</p><input type=\"checkbox\" disabled id=\"43\"/></li>");

                foreach (ToothList objToothList in lst[0].lstToothList)
                {
                    if (dictPrimaryLeft.ContainsKey(objToothList.ToothType))
                    {
                        String str = dictPrimaryLeft[objToothList.ToothType];
                        dictPrimaryLeft[objToothList.ToothType] = str.Replace("<li>", "<li class=\"active Primary\">");
                    }
                }
                sb.Append(GetLine(dictPrimaryLeft));

                sb.Append("<br class=\"clear\">");
                sb.Append("</ul>");
                sb.Append("</div>");
                sb.Append("</div>");
                sb.Append("<div class=\"clear\"></div>");



                sb.Append("</div></div>");  


                ///Code For Images

                if (lst[0].lstImages.Count > 0)
                {
                    sb.Append("<div class=\"table_container\">");
                    sb.Append("<h4 class=\"sub_title\">");
                    sb.Append("Images</h4>");
                    sb.Append("</div>");
                    sb.Append("<ul class=\"list\">");
                    foreach (MessageAttachment objAttachment in lst[0].lstImages)
                    {
                        string AttachmentName = objAttachment.AttachmentName;
                        AttachmentName = AttachmentName.Substring(AttachmentName.ToString().LastIndexOf("$") + 1);
                        sb.Append("<li onclick=\"Attachementpopup('" + (DataAccessLayer.Common.clsHelper.EscapeUriString(ImageBankFolder + objAttachment.AttachmentName)) + "','Image')\" style=\"cursor: pointer;\"><a href=" + DataAccessLayer.Common.clsHelper.EscapeUriString(ImageBankFolder + objAttachment.AttachmentName) + " target=\"_blank\"><span><i class=\"attach\"></i>" + AttachmentName + "</span></a></li>");
                    }
                    sb.Append("</ul>");
                }
                ///Code For Documents
                if (lst[0].lstDocuments.Count > 0)
                {
                    sb.Append("<div class=\"table_container\">");
                    sb.Append("<h4 class=\"sub_title\">");
                    sb.Append("Documents</h4>");
                    sb.Append("</div>");
                    sb.Append("<ul class=\"list\">");
                    foreach (MessageAttachment objAttachment in lst[0].lstDocuments)
                    {
                        string AttachmentName = objAttachment.AttachmentName;
                        AttachmentName = AttachmentName.Substring(AttachmentName.ToString().LastIndexOf("$") + 1);
                        sb.Append("<li style=\"cursor: pointer;\"> <span><i class=\"attach\"></i><a target=\"_blank\" href=\"" + DataAccessLayer.Common.clsHelper.EscapeUriString(objAttachment.AttachmentName) + "\" src=\"" + DataAccessLayer.Common.clsHelper.EscapeUriString(objAttachment.AttachmentName) + "\">" + AttachmentName + "</span></li>");
                        //sb.Append("<li onclick=\"Attachementpopup('" + objAttachment.AttachmentName + "','Doc')\" style=\"cursor: pointer;\"> <span><i class=\"attach\"></i><a href=\"taget_blank\">" + AttachmentName + "</span></li>");
                    }
                    sb.Append("</ul>");
                }
                string comments = lst[0].Comments;
                comments = comments.Replace("\n", "<br/>");
                sb.Append("<div class=\"table_container\">");
                sb.Append("<h4 class=\"sub_title\"  >Comments</h4>");
                sb.Append("<div class=\"comments\" ><label>" + lst[0].Comments + "</label></div>");
                sb.Append("</div>");


                sb.Append(" </div></div>");
                ///Message Body End
                sb.Append("</div></div>");
                sb.Append("<div class=\"modal-footer\"><button class=\"btn btn-default btn-cancleappclose\" type=\"button\" data-dismiss=\"modal\">Close</button><button class=\"btn btn-primary customerclsrpl\" type=\"button\" data-dismiss=\"modal\" onclick=\"Replytocolleagues(" + lst[0].PatientId + "," + lst[0].SenderId + "," + lst[0].ReceiverID + ")\">Reply</button></div>");
                sb.Append("<script>$(document).ready(function () {$(window).scrollTop(0);});$(window).load(function () {$('.Primary').find(function () {$('.Primary').children('input').prop('checked', true);});});</script>");
            }
            else
            {
                sb.Append("<div></div>");
            }
            return sb.ToString();


        }
        string GetLine(Dictionary<int, String> d)
        {
            // Build up each line one-by-one and then trim the end
            StringBuilder builder = new StringBuilder();
            foreach (KeyValuePair<int, String> pair in d)
            {
                builder.Append(pair.Value);
            }
            string result = builder.ToString();
            // Remove the final delimiter
            return result;
        }
        public ActionResult ViewPatientNoteByNoteId(int NoteId)
        {
            DataTable dt = new DataTable();
            ObjPatientsData = new clsPatientsData();
            PatientNotes Pn = new PatientNotes();
            dt = ObjPatientsData.GetPatientNoteByNoteId(NoteId);
            if (dt.Rows.Count > 0)
            {
                Pn.DoctorName = dt.Rows[0]["DoctorName"].ToString();
                Pn.DoctorEmail = dt.Rows[0]["DoctorEmail"].ToString();
                Pn.Note = dt.Rows[0]["PatientNotes"].ToString();
                Pn.CreationDate = dt.Rows[0]["CreatedDate"].ToString();
                Pn.PatientId = Convert.ToInt32(dt.Rows[0]["PatientId"]);
                Pn.UserId = Convert.ToInt32(dt.Rows[0]["UserId"]);
            }
            ViewBag.NoteId = NoteId;
            return View(Pn);
        }

        #region Code For Patient Note Insert and Remove
        [HttpPost]
        public JsonResult AddPatientNote(int PatientId, int UserId, string Note)
        {
            try
            {
                bool Result = false;
                object obj = "";
                MdlPatient = new mdlPatient();

                Result = MdlPatient.AddPatientNote(PatientId, UserId, Note);

                if (Result)
                {
                    obj = ViewBag.PatientNotes = NoteScript(PatientId);

                }
                else
                {
                    obj = "0";
                }
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }
        }

        [HttpPost]
        public JsonResult RemovePatientNote(int NoteId, int PatientId)
        {
            try
            {
                bool Result = false;
                object obj = "";
                MdlPatient = new mdlPatient();
                Result = MdlPatient.RemovePatientNote(NoteId);
                if (Result)
                {
                    obj = ViewBag.PatientNotes = NoteScript(PatientId);
                }
                else
                {
                    obj = "0";
                }
                return Json(obj, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                throw;
            }
        }



        public string NoteScript(int PatientId)
        {
            StringBuilder sb = new StringBuilder();
            MdlPatient.lstPatientNotes = null;
            MdlPatient.lstPatientNotes = MdlPatient.GetPatientNotesDetails(PatientId);

            foreach (var item in MdlPatient.lstPatientNotes)
            {
                sb.Append("<article class=\"post\"> <a href=\"javascript:;\" onclick=\"RemovePatientNote('" + item.NoteId + "','" + PatientId + "')\"><i class=\"close\"></i></a><figure class=\"thumb\"><img alt=\"\" src=\"" + DataAccessLayer.Common.clsHelper.EscapeUriString(item.ImageName) + "\"></figure>");
                sb.Append("<div class=\"data\"><div class=\"links\"><span><a href=\"javascript:;\" onclick=\"ViewProfile('" + item.UserId + "')\">" + item.FirstName + "&nbsp;" + item.LastName + "</a></span><span>" + item.CreationDate + "</span>");
                sb.Append("</div><p>" + item.Note + "</p></div><div class=\"clear\"></div></article>");
            }

            return sb.ToString();
        }
        #endregion

        #region Code For  Get All Messages Sent by Patients to Doctor
        [HttpPost]
        public JsonResult PatientCommunicationData(int PageIndex, int SortBy, bool IsAsc, int PatientId, string isAuthorize)
        {
            MdlPatient = new mdlPatient();
            return Json(GetPatientMessagesReceivedByDoctorToPatientId(Convert.ToInt32(Session["UserId"]), PageIndex, SortBy, IsAsc, PatientId, isAuthorize), JsonRequestBehavior.AllowGet);
        }
        public JsonResult ColleagueCommunicationData(int UserId, int PatientId)
        {
            MdlColleagues = new mdlColleagues();
            return Json(GetColleaguesMessagesReceivedByDoctor(Convert.ToInt32(Session["UserId"]), PatientId), JsonRequestBehavior.AllowGet);
        }
        #region oldcode
        //public string GetCommunicationHistoryForPatient(int UserId, int PatientId, int PageIndex, int PageSize)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    MdlColleagues = new mdlColleagues();
        //    DataTable AttachedPatientDetials = new DataTable();
        //    //IEnumerable<ColleaguesMessagesOfDoctor> ColleagueMessageofDoctor = null;
        //    MdlColleagues.lstColleaguesMessagesDoctor = MdlColleagues.GetCommunicationHistoryForPatient(UserId, PatientId, PageIndex, PageSize, 1, 2);
        //    string FilUploader = string.Empty;

        //    using (var sw = new StringWriter())
        //    {
        //        ViewBag.PatientId = PatientId;
        //        var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, "FileUploadPatientHistoryMessage");
        //        var viewContext = new ViewContext(ControllerContext, viewResult.View,
        //                                     ViewData, TempData, sw);
        //        viewResult.View.Render(viewContext, sw);
        //        viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
        //        FilUploader = sw.GetStringBuilder().ToString();
        //    }

        //    sb.Append("<div class=\"table_data messages clearfix\"><h4 class=\"sub_title clearfix  clearfix borderbtmn\" style=\"width:90%\">Communications</h4>");
        //    sb.Append("<a href=\"javascript:;\" onclick=\"PrintCommunicationView()\"><span class=\"print_file\" style=\"float:right;margin-top:-37px;margin-bottom:13px;font-size:20px;\">[Print all]</span></a>");
        //    sb.Append("<span class=\"fluid_cont fcbkautomcomplateblur\" id=\"SelectTeamMemberandColleagues\" style=\"margin-top:13px; margin-bottom:13px;\"></span>");
        //    sb.Append("<div class=\"communicationbox\"><textarea rows=\"5\" placeholder='Type message here' style=\"width:98%;border:0;\" id=\"txtMessageFromPatientHistory\" name=\"Msgarea\"></textarea><br/>");
        //    sb.Append("<input type=\"submit\" class=\"send_btn\" name=\"Send\" value=\"Send\" onclick=\"SendMessageFromPatientHistory(" + PatientId + ");\" style=\"margin-bottom:13px; margin-top: 5px;float:right;\">");
        //    //sb.Append("<br/>");
        //    sb.Append(FilUploader);
        //    sb.Append("</div>");

        //    try
        //    {
        //        if (MdlColleagues.lstColleaguesMessagesDoctor.Count > 0)
        //        {
        //            sb.Append("<table class=\"comTable margintopm1\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><thead><tr>");
        //            sb.Append(" <th class=\"Communicationheader\"><a id='PatientHCol_1' href=\"javascript:;\" onclick='PatientCommunicationHistorySorting(" + PatientId + ",1)' sort-dir='2' class=\"sortable\">Date</a></th>");
        //            sb.Append(" <th class=\"Communicationheader\"><a id='PatientHCol_2' href=\"javascript:;\" onclick='PatientCommunicationHistorySorting(" + PatientId + ",2)' sort-dir='1' class=\"sortable\">From/To</a></th>");
        //            sb.Append(" <th class=\"Communicationheader\"><a id='PatientHCol_3' href=\"javascript:;\" onclick='PatientCommunicationHistorySorting(" + PatientId + ",3)' sort-dir='1'  class=\"sortable\">Type</a></th>");
        //            //sb.Append("<th style=\"width:55% !important;\"><a href=\"javascript:;\" style=\"color: rgb(81, 81, 82)!important;\" >Message</a></th>");
        //            sb.Append("<th style=\"width:55% !important;\">Message</th>");

        //            sb.Append("</table>");
        //            sb.Append("</div>");
        //            sb.Append("<div class=\"patientcommunicationhistory\" id='patientHistoryscrollId'><table class='comTable margintopm1'><tbody id='PatientHistoryThead'>");
        //            foreach (var item in MdlColleagues.lstColleaguesMessagesDoctor)
        //            {
        //                sb.Append("<tr>");

        //                if (item.MessageTypeId == 2)
        //                {
        //                    List<ReferralDetails> lst = new List<ReferralDetails>();
        //                    var Datetime = item.CreationDate.Split(' ');
        //                    var CreationDate = Datetime[0];
        //                    var CreationTime = Datetime[1] + Datetime[2];
        //                    lst = MdlPatient.GetRefferalDetailsById(Convert.ToInt32(Session["UserId"]), item.MessageId, "Sentbox");
        //                    sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewDoctorReferralView('" + item.MessageId + "','" + item.MessageTypeId + "')\">" + CreationDate + "<br/>" + CreationTime + "</a></td>");
        //                    sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewDoctorReferralView('" + item.MessageId + "','" + item.MessageTypeId + "')\">" + item.ColleagueName + "</a></td>");
        //                    sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewDoctorReferralView('" + item.MessageId + "','" + item.MessageTypeId + "')\">Referral </a></td>");
        //                    if (lst.Count > 0)
        //                    {
        //                        string comments = lst[0].Comments;
        //                        comments = comments.Replace("\n", "<br/>");

        //                        sb.Append("<td style=\"width:45% !important; vertical-align:middle !important;\"><a>" + comments + "</a><br/><br/>");

        //                        string[] strRegardingNo = lst[0].RegardOption.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        //                        if (!string.IsNullOrEmpty(string.Join("", strRegardingNo)))
        //                        {
        //                            sb.Append("<b class=\"hylyt_field\">Regarding</b> : ");
        //                            foreach (String item2 in strRegardingNo)
        //                            {
        //                                if (!string.IsNullOrEmpty(item2))
        //                                {

        //                                    if ((Convert.ToInt32(item2) - 1) < MdlPatient.ReferralRegarding.Length)
        //                                        sb.Append("" + "" + MdlPatient.ReferralRegarding[Convert.ToInt32(item2) - 1] + ",&nbsp;");
        //                                }
        //                            }
        //                        }
        //                        if (!String.IsNullOrEmpty(lst[0].OtherComments))
        //                            sb.Append("" + "" + lst[0].OtherComments + ",&nbsp;");
        //                        if (sb.ToString().EndsWith(",&nbsp;"))
        //                        {
        //                            string str = sb.ToString().Substring(0, sb.ToString().LastIndexOf(",&nbsp;"));
        //                            sb = new StringBuilder(str);
        //                        }
        //                        string[] strRequest = lst[0].RequestingOption.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        //                        if (!string.IsNullOrEmpty(string.Join("", strRequest)))
        //                        {
        //                            sb.Append("<br/><b class=\"hylyt_field\">Request</b>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;");
        //                            foreach (String item1 in strRequest)
        //                            {
        //                                if (!string.IsNullOrEmpty(item1))
        //                                {
        //                                    if ((Convert.ToInt32(item1) - 1) < MdlPatient.ReferralRequest.Length)
        //                                        sb.Append("" + "" + MdlPatient.ReferralRequest[Convert.ToInt32(item1) - 1] + ",&nbsp;");
        //                                }
        //                            }
        //                        }
        //                        if (!String.IsNullOrEmpty(lst[0].RequestComments))
        //                            sb.Append("" + "" + lst[0].RequestComments + ",&nbsp;");
        //                        if (sb.ToString().EndsWith(",&nbsp;"))
        //                        {
        //                            string str = sb.ToString().Substring(0, sb.ToString().LastIndexOf(",&nbsp;"));
        //                            sb = new StringBuilder(str);
        //                        }
        //                        //sb.Append("<br/><br/>");
        //                        foreach (MessageAttachment objAttachment in lst[0].lstImages)
        //                        {
        //                            string AttachmentName = objAttachment.AttachmentName;
        //                            AttachmentName = AttachmentName.Substring(AttachmentName.ToString().LastIndexOf("$") + 1);
        //                            sb.Append("<br/><br/><span><i class=\"attach\" style=\"margin-left:0px !important;\"></i><a target=\"_blank\" href=\"/ImageBank/" + objAttachment.AttachmentName + "\">" + AttachmentName + "</a></span>&nbsp;&nbsp;&nbsp;");
        //                        }
        //                        sb.Append("</td>");
        //                        sb.Append("<td style=\"padding-left:0px !important;\"><a href=\"javascript:;\" onclick=\"PrintReferralMessage(" + item.MessageId + ")\"><span class=\"print_file\">[Print]</span></a><td>");

        //                    }

        //                }
        //                if (item.MessageTypeId == 1)
        //                {
        //                    var Datetime = item.CreationDate.Split(' ');
        //                    var CreationDate = Datetime[0];
        //                    var CreationTime = Datetime[1] + Datetime[2];
        //                    sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewMessageOfColleagues('" + item.MessageId + "','" + item.MessageTypeId + "')\">" + CreationDate + "<br/>" + CreationTime + "</a></td>");
        //                    sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewMessageOfColleagues('" + item.MessageId + "','" + item.MessageTypeId + "')\">" + item.ColleagueName + "</a></td>");
        //                    sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewMessageOfColleagues('" + item.MessageId + "','" + item.MessageTypeId + "')\">Messages </a></td>");
        //                    string NewMessage = HttpUtility.UrlDecode(Convert.ToString(item.Message));
        //                    string colleaguesmsg = NewMessage;
        //                    colleaguesmsg = colleaguesmsg.Replace("\n", " <br> ");
        //                    sb.Append("<td style=\"width:45% !important; vertical-align: text-top !important;\"><a href=\"javascript:;\"onclick=\"ViewMessageOfColleagues('" + item.MessageId + "','" + item.MessageTypeId + "')\">" + colleaguesmsg + "</a><br/><br/>");

        //                    DataTable dt = new DataTable();
        //                    string qry = "select * from ColleagueMessageAttachments where MessageId =" + item.MessageId;
        //                    dt = ObjCommon.DataTable(qry);
        //                    if (dt.Rows.Count > 0)
        //                    {
        //                        foreach (DataRow item1 in dt.Rows)
        //                        {
        //                            if (!string.IsNullOrEmpty(dt.Rows[0]["Id"].ToString()))
        //                            {
        //                                string AttachmentName = string.Empty;
        //                                AttachmentName = Convert.ToString(item1["AttachmentName"]);
        //                                AttachmentName = AttachmentName.Substring(AttachmentName.ToString().LastIndexOf("$") + 1);
        //                                string extension = Path.GetExtension(AttachmentName);
        //                                string Type = string.Empty;

        //                                if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd" && extension.ToLower() != ".pspimage" && extension.ToLower() != ".thm" && extension.ToLower() != ".tif" && extension.ToLower() != ".yuv")
        //                                {
        //                                    Type = "Doc";
        //                                }
        //                                else
        //                                {
        //                                    Type = "Image";
        //                                }
        //                                //sb.Append("<span title=\"" + AttachmentName + "\" onclick=\"Attachementpopup('" + ImageBankFolder + item1["AttachmentName"] + "','" + Type + "')\" style=\"cursor: pointer;\" ><i class=\"attach\"></i> " + AttachmentName + "</span>");
        //                                sb.Append("<span><i class=\"attach\" style=\"margin-left:0px !important;\"></i><a target=\"_blank\" href=\"/ImageBank/" + item1["AttachmentName"] + "\">" + item1["AttachmentName"].ToString().Substring(item1["AttachmentName"].ToString().ToString().LastIndexOf("$") + 1) + "</a></span>&nbsp;&nbsp;&nbsp;");

        //                            }
        //                        }
        //                    }

        //                    sb.Append("</td>");
        //                    sb.Append("<td style=\"padding-left:0px !important;\"><a href=\"javascript:;\" onclick=\"PrintColleagueMessage(" + item.MessageId + ")\"><span class=\"print_file\">[Print]</span></a></td>");
        //                }
        //                if (item.MessageTypeId == 3)
        //                {
        //                    var Datetime = item.CreationDate.Split(' ');
        //                    var CreationDate = Datetime[0];
        //                    var CreationTime = Datetime[1] + Datetime[2];
        //                    sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewMessage('" + item.MessageId + "')\">" + CreationDate + "<br/>" + CreationTime + "</a></td>");
        //                    sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewMessage('" + item.MessageId + "')\">" + item.ColleagueName + "</a></td>");
        //                    sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewMessage('" + item.MessageId + "')\">Messages </a></td>");
        //                     string NewMessage = HttpUtility.UrlDecode(Convert.ToString(item.Message));
        //                    string patientsmsg = NewMessage;
        //                    patientsmsg = patientsmsg.Replace("\n", " <br> ");
        //                    sb.Append("<td style=\"width:45% !important; vertical-align:text-top !important;\"><a href=\"javascript:;\"onclick=\"ViewMessage('" + item.MessageId + "')\">" + (patientsmsg.ToString().Length > 700 ? patientsmsg.ToString().Substring(0, 700) + "..." : patientsmsg.ToString()) + "</a><br/><br/>");

        //                    DataTable dt = new DataTable();
        //                    string qry = "select * from MessageAttachments where MessageId =" + item.MessageId;
        //                    dt = ObjCommon.DataTable(qry);
        //                    if (dt.Rows.Count > 0)
        //                    {
        //                        foreach (DataRow item1 in dt.Rows)
        //                        {
        //                            if (!string.IsNullOrEmpty(dt.Rows[0]["Id"].ToString()))
        //                            {
        //                                string AttachmentName = string.Empty;
        //                                AttachmentName = Convert.ToString(item1["AttachmentName"]);
        //                                AttachmentName = AttachmentName.Substring(AttachmentName.ToString().LastIndexOf("$") + 1);
        //                                string extension = Path.GetExtension(AttachmentName);
        //                                string Type = string.Empty;

        //                                if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd" && extension.ToLower() != ".pspimage" && extension.ToLower() != ".thm" && extension.ToLower() != ".tif" && extension.ToLower() != ".yuv")
        //                                {
        //                                    Type = "Doc";
        //                                }
        //                                else
        //                                {
        //                                    Type = "Image";
        //                                }
        //                                // sb.Append("<span title=\"" + AttachmentName + "\" onclick=\"Attachementpopup('" + ImageBankFolder + item1["AttachmentName"] + "','" + Type + "')\" style=\"cursor: pointer;\" ><i class=\"attach\"></i> " + AttachmentName + "</span>");
        //                                sb.Append("<span><i class=\"attach\" style=\"margin-left:0px !important;\"></i><a target=\"_blank\" href=\"/ImageBank/" + item1["AttachmentName"] + "\">" + item1["AttachmentName"].ToString().Substring(item1["AttachmentName"].ToString().ToString().LastIndexOf("$") + 1) + "</a></span>&nbsp;&nbsp;&nbsp;");
        //                            }
        //                        }
        //                    }
        //                    sb.Append("</td>");
        //                    sb.Append("<td style=\"padding-left:0px !important;\"><a href=\"javascript:;\" onclick=\"PrintPatientMessage(" + item.MessageId + ")\"><span class=\"print_file\">[Print]</span></a></td>");
        //                }
        //                if (item.MessageTypeId == 4)
        //                {
        //                    sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewPatientNote('" + item.MessageId + "')\">" + item.CreationDate + "</a></td>");
        //                    sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewPatientNote('" + item.MessageId + "')\">" + item.ColleagueName + "</a></td>");
        //                    sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewPatientNote('" + item.MessageId + "')\"> Messages </a></td>");
        //                    sb.Append("<td style=\"width:45% !important; vertical-align:text-top !important;\"><a href=\"javascript:;\"onclick=\"ViewPatientNote('" + item.MessageId + "')\">" + (item.Message.ToString().Length > 700 ? item.Message.ToString().Substring(0, 700) + "..." : item.Message.ToString()) + "</a></td>");
        //                    // sb.Append("<td style=\"width:50% !important;\"><a style=\"float:right; \" class=\"inboxDltMsg\" title=\"Delete\" href=\"javascript:;\"onclick=\"RemovePatientNote('" + item.MessageId + "','" + PatientId + "')\"></a></td>");
        //                }

        //            }
        //            sb.Append("</tbody></table></div>");
        //            sb.Append("<br/><center>");
        //            TempData["PatientId"] = PatientId;
        //            sb.Append("</center><h4 class=\"sub_title clearfix\"></h4>");
        //        }
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //    return sb.ToString();
        //}
        #endregion 
        public JsonResult SentMessageFromPatientHistory(int PatientId, string Message, string RecieverIds)
        {
            if (!string.IsNullOrWhiteSpace(Message))
            {
                int NewMessageId = 0;
                Member MdlMember = new Member();
                try
                {
                    if (string.IsNullOrEmpty(RecieverIds))
                    {

                        string separator = "";
                        clsColleaguesData cls = new clsColleaguesData();
                        var Patientid = Convert.ToInt32(TempData["PatientId"]);
                        TempData.Keep();
                        DataTable dt = new DataTable();
                        dt = cls.SendPatientHistoryMessageforall(Patientid, Convert.ToInt32(Session["UserId"]));
                        if (dt.Rows.Count > 0)
                        {
                            foreach (DataRow item in dt.Rows)
                            {
                                RecieverIds += separator + item["ColleaguesId"].ToString();
                                separator = ",";
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(RecieverIds))
                    {
                        NewMessageId = ObjColleaguesData.SentMessageFromPatientHistory("", Message, Convert.ToInt32(Session["UserId"]), 1, PatientId.ToString(), RecieverIds);

                    }
                    string[] ColleaguesId = RecieverIds.ToString().Split(new char[] { ',' });
                    if (ColleaguesId.Length != 0)
                    {
                        foreach (var item in ColleaguesId)
                        {
                            if (!string.IsNullOrEmpty(item))
                            {
                                ObjTemplate.SendMessageToColleague(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(item), NewMessageId, 1);
                            }
                        }
                    }
                    FileUploadController fupobj = new FileUploadController();
                    IDictionary<string, bool> response = fupobj.TempToImageBankUpload(PatientId, Convert.ToInt32(Session["UserId"]));
                    //Changes made for Display all files while sending Messages.
                    foreach (var item in response)
                    {
                        if (!string.IsNullOrEmpty(item.Key))
                        {
                            bool Attach = ObjColleaguesData.InsertMessageAttachemntsForColleague(NewMessageId, response.Keys.FirstOrDefault());
                        }
                    }
                    //if (NewMessageId != 0)
                    //{

                    //    DataTable dtTempUploadedDocs = ObjCommon.GetRecordsFromTempTablesForDoctor(Convert.ToInt32(Session["UserId"]), DataAccessLayer.Common.clsCommon.GetUniquenumberForAttachments());
                    //    if (dtTempUploadedDocs != null && dtTempUploadedDocs.Rows.Count > 0)
                    //    {

                    //        foreach (DataRow itemTempUploadedDocs in dtTempUploadedDocs.Rows)
                    //        {

                    //            string DocName = Convert.ToString(itemTempUploadedDocs["DocumentName"]);
                    //            DocName = DocName.Substring(DocName.ToString().LastIndexOf("$") + 1);
                    //            string NewFileName = Convert.ToString("$" + System.DateTime.Now.Ticks + "$" + DocName);
                    //            string Extension = Path.GetExtension(NewFileName);
                    //            DocName = Convert.ToString(itemTempUploadedDocs["DocumentName"]);
                    //            DocName = DocName.Replace("../TempUploads/", "");

                    //            string sourceFile = Server.MapPath(TempFolder) + "\\" + DocName;
                    //            string destinationFile = Server.MapPath(ImageBankFolder) + "\\" + NewFileName;

                    //            sourceFile = sourceFile.Replace("\\Patients\\", "\\");
                    //            destinationFile = destinationFile.Replace("\\Patients\\", "\\");

                    //            System.IO.File.Copy(sourceFile, destinationFile);


                    //            if (Extension.ToLower() == ".jpg" || Extension.ToLower() == ".jpeg" || Extension.ToLower() == ".gif" || Extension.ToLower() == ".png" || Extension.ToLower() == ".bmp" || Extension.ToLower() == ".psd" || Extension.ToLower() == ".pspimage" || Extension.ToLower() == ".thm" || Extension.ToLower() == ".tif")
                    //            {
                    //                string savepathThumb = "";
                    //                string tempPathThumb = "";
                    //                string thambnailimage = NewFileName;
                    //                string Savepath = Server.MapPath(ImageBankFolder);

                    //                Savepath = Savepath.Replace("\\Patients\\", "\\");
                    //                tempPathThumb = ConfigurationManager.AppSettings["FolderPathThumb"];
                    //                savepathThumb = Server.MapPath(tempPathThumb);
                    //                savepathThumb = savepathThumb.Replace("\\Patients\\", "\\");


                    //                // Load image.
                    //                System.Drawing.Image image = System.Drawing.Image.FromFile(Savepath + @"\" + NewFileName);

                    //                // Compute thumbnail size.
                    //                Size thumbnailSize = ObjCommon.GetThumbnailSize(image);

                    //                // Get thumbnail.
                    //                System.Drawing.Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                    //                    thumbnailSize.Height, null, IntPtr.Zero);

                    //                // Save thumbnail.

                    //                thumbnail.Save(savepathThumb + @"\" + thambnailimage);

                    //            }
                    //            if (!string.IsNullOrEmpty(NewFileName))
                    //            {
                    //                // Code for Insert Attachment
                    //                bool Attach = ObjColleaguesData.InsertMessageAttachemntsForColleague(NewMessageId, NewFileName);
                    //            }
                    //        }
                    //    }
                    //    if (NewMessageId != 0)
                    //    {

                    //        int RecordId = 0;
                    //        int DoctorId = Convert.ToInt32(Session["UserId"]);

                    //        int MontageId = 0;
                    //        int Monatgetype = 0;
                    //        int imageId = 0;
                    //        int UploadImageId = 0;

                    //        int ownerId = DoctorId;
                    //        int ownertype = 0; int images;
                    //        DataTable dtimageupload = ObjCommon.Get_UploadedImagesByDoctor(DoctorId, clsCommon.GetUniquenumberForAttachments());
                    //        DataTable dtfileupload = ObjCommon.Get_UploadedFilesByDoctor(DoctorId, clsCommon.GetUniquenumberForAttachments());
                    //        DataTable dtPatientImages = ObjPatientsData.GetPatientImages(PatientId, null);

                    //        if (dtPatientImages.Rows.Count != 0)
                    //        {
                    //            MontageId = Convert.ToInt32(dtPatientImages.Rows[0]["MontageId"]);

                    //        }


                    //        if (MontageId == 0)
                    //        {

                    //            string MontageName = PatientId + "Montage";
                    //            int montageid = Convert.ToInt32(ObjPatientsData.AddMontage(MontageName, Monatgetype, PatientId, ownerId, ownertype));

                    //            for (images = 0; images < dtimageupload.Rows.Count; images++)
                    //            {
                    //                string pathfull = Server.MapPath(ConfigurationManager.AppSettings.Get("ImageBank").ToString().Replace("../", ""));
                    //                pathfull = pathfull.Replace("\\Patients\\", "\\");
                    //                string fullpath = pathfull;
                    //                string filename = Convert.ToString(dtimageupload.Rows[images]["DocumentName"]).Replace(ConfigurationManager.AppSettings.Get("ImageBank"), "");
                    //                int Imagetype;

                    //                Imagetype = 0;

                    //                int imageid = ObjPatientsData.AddImagetoPatient(PatientId, 1, filename, 1000, 1000, 0, Imagetype, fullpath, dtimageupload.Rows[images]["DocumentName"].ToString(), ownerId, ownertype, 1, "Doctor");
                    //                if (imageid > 0)
                    //                {

                    //                    bool resmonimg = ObjPatientsData.AddImagetoMontage(imageid, Imagetype, montageid);
                    //                }


                    //            }

                    //            for (int dentaldoc = 0; dentaldoc < dtfileupload.Rows.Count; dentaldoc++)
                    //            {
                    //                RecordId = ObjPatientsData.UploadPatientDocument(montageid, dtfileupload.Rows[dentaldoc]["DocumentName"].ToString(), "Doctor", 0, 0);
                    //            }

                    //        }

                    //        else if (MontageId != 0)
                    //        {
                    //            for (int MontageImage = 0; MontageImage < dtimageupload.Rows.Count; MontageImage++)
                    //            {

                    //                if (UploadImageId == 0)
                    //                {

                    //                    string pathfull = Server.MapPath(ConfigurationManager.AppSettings.Get("ImageBank").ToString().Replace("../", ""));
                    //                    pathfull = pathfull.Replace("\\Patients\\", "\\");
                    //                    string fullpath = pathfull;
                    //                    string imagename = Convert.ToString(dtimageupload.Rows[MontageImage]["DocumentName"]).Replace(ConfigurationManager.AppSettings.Get("ImageBank"), "");
                    //                    int Imagetype;

                    //                    Imagetype = 0;

                    //                    imageId = ObjPatientsData.AddImagetoPatient(PatientId, 1, imagename, 1000, 1000, 0, Imagetype, fullpath, dtimageupload.Rows[MontageImage]["DocumentName"].ToString(), ownerId, ownertype, 1, "Doctor");
                    //                    if (imageId > 0)
                    //                    {
                    //                        bool resmonimg = ObjPatientsData.AddImagetoMontage(imageId, Imagetype, MontageId);

                    //                    }

                    //                }


                    //            }

                    //            for (int dentaldoc = 0; dentaldoc < dtfileupload.Rows.Count; dentaldoc++)
                    //            {
                    //                RecordId = ObjPatientsData.UploadPatientDocument(MontageId, dtfileupload.Rows[dentaldoc]["DocumentName"].ToString(), "Doctor", 0, 0);
                    //            }


                    //        }


                    //        string Sessionid = clsCommon.GetUniquenumberForAttachments();

                    //        DataTable dtTempFiles = ObjCommon.GetRecordsFromTempTablesForDoctor(ownerId, Sessionid);


                    //        if (dtTempFiles != null && dtTempFiles.Rows.Count > 0)
                    //        {
                    //            for (int i = 0; i < dtTempFiles.Rows.Count; i++)
                    //            {
                    //                string DocName = Convert.ToString(dtTempFiles.Rows[i]["DocumentName"]);


                    //                string Extension = Path.GetExtension(DocName);
                    //                DocName = Convert.ToString(dtTempFiles.Rows[i]["DocumentName"]);
                    //                DocName = DocName.Replace("../TempUploads/", "");

                    //                string sourceFile = Server.MapPath(ConfigurationManager.AppSettings.Get("TempUploadsFolderPath")) + DocName;

                    //                sourceFile = sourceFile.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");
                    //                string tempPath = ConfigurationManager.AppSettings["ImageBank"];
                    //                string destinationFile = Server.MapPath(tempPath);
                    //                destinationFile = destinationFile.Replace("PublicProfile\\", "").Replace("mydentalfiles", "");
                    //                string DestinationFilePath = destinationFile;
                    //                destinationFile = destinationFile + DocName;

                    //                if (System.IO.File.Exists(sourceFile))
                    //                {
                    //                    System.IO.File.Copy(sourceFile, destinationFile);


                    //                    if (Extension.ToLower() == ".jpg" || Extension.ToLower() == ".jpeg" || Extension.ToLower() == ".gif" || Extension.ToLower() == ".png" || Extension.ToLower() == ".bmp" || Extension.ToLower() == ".psd" || Extension.ToLower() == ".pspimage" || Extension.ToLower() == ".thm" || Extension.ToLower() == ".tif")
                    //                    {
                    //                        string ThubimagePath = Server.MapPath("~/" + System.Configuration.ConfigurationManager.AppSettings["FolderPathThumb"]);

                    //                        ObjCommon.GenerateThumbnailImage(DestinationFilePath, DocName, ThubimagePath);
                    //                    }

                    //                }

                    //            }
                    //        }
                    //    }

                    //    DataTable dtfileuploaddelete = ObjCommon.GetRecordsFromTempTablesForDoctor(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());

                    //    foreach (DataRow item in dtfileuploaddelete.Rows)
                    //    {

                    //        string DocName = Convert.ToString(item["DocumentName"]);


                    //        DocName = Convert.ToString(item["DocumentName"]);
                    //        DocName = DocName.Replace("../TempUploads/", "");

                    //        string sourceFile = Server.MapPath(ConfigurationManager.AppSettings.Get("TempUploadsFolderPath")) + DocName;

                    //        sourceFile = sourceFile.Replace("\\Patients\\", "");

                    //        if (System.IO.File.Exists(sourceFile))
                    //        {
                    //            System.IO.File.Delete(sourceFile);
                    //        }
                    //    }

                    //    bool Result = ObjCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments()); // For Remove All Uplaoded Temp Images and Files
                    //}

                }
                catch (Exception ex)
                {
                    bool Result = ObjCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
                    ObjCommon.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                    throw ex;
                }
            }
            return Json("");
        }
        #region
        public JsonResult ByDefaultselectedTeamMemberandTreatingDoctorList()
        {
            var Patientid = Convert.ToInt32(TempData["PatientId"]);
            TempData.Keep();
            clsPatientsData cls = new clsPatientsData();
            DataTable dt = new DataTable();
            dt = cls.GetSelectedTeamMemberForPatientHistory(Patientid, Convert.ToInt32(Session["UserId"]));
            var TreatingDoctors = dt.AsEnumerable().ToList().AsEnumerable().Select(e1 => new { Id = e1["ColleaguesId"].ToString() });
            string strList = "";
            foreach (var itm in TreatingDoctors)
            {
                if (strList.Length > 0)
                {
                    strList += ",";
                }
                strList += itm.Id.ToString();
            }
            return Json(strList);
        }

        #endregion

        public JsonResult GetUserListofTeam(string selected)
        {
            var Patientid = Convert.ToInt32(TempData["PatientId"]);
            TempData.Keep();
            clsPatientsData cls = new clsPatientsData();
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();
                dt = cls.GetColleagueTeamMemberandTreatingDoctorList(Patientid, Convert.ToInt32(Session["UserId"]));
                if (dt != null && dt.Rows.Count > 0)
                {
                    sb.Append("<select id=\"selectTeam\" name=\"selectTeam\" multiple=\"multiple\" style=\"width:100%\">");
                    foreach (DataRow item in dt.Rows)
                    {
                        string Id = Convert.ToString(item["ColleaguesId"]);
                        if (selected.Split(',').Contains(Id))
                        {
                            sb.Append("<option value=" + Convert.ToInt32(item["ColleaguesId"]) + " class=\"selected\" selected=\"selected\">" + item["ColleaguesName"] + "</option>");
                        }
                        else
                        {
                            sb.Append("<option value=" + Convert.ToInt32(item["ColleaguesId"]) + ">" + item["ColleaguesName"] + "</option>");
                        }
                    }
                    sb.Append("</select>");


                    sb.ToString();
                }
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("", ex.Message, ex.StackTrace);
                throw;
            }
            return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
        }
        #region oldcode
        //public string GetPatientHostoryCommunicationOnSorting(int PatientId, int PageIndex, int SortColumn, int SortDirection)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    MdlColleagues = new mdlColleagues();
        //    MdlPatient = new mdlPatient();
        //    int UserId = Convert.ToInt32(Session["UserId"]);
        //    MdlColleagues.lstColleaguesMessagesDoctor = MdlColleagues.GetCommunicationHistoryForPatient(UserId, PatientId, PageIndex, 20, SortColumn, SortDirection);
        //    foreach (var item in MdlColleagues.lstColleaguesMessagesDoctor)
        //    {
        //        sb.Append("<tr>");

        //        if (item.MessageTypeId == 2)
        //        {
        //            sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewDoctorReferralView('" + item.MessageId + "')\">" + item.CreationDate + "</a></td>");
        //            sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewDoctorReferralView('" + item.MessageId + "')\">" + item.ColleagueName + "</a></td>");
        //            sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewDoctorReferralView('" + item.MessageId + "')\">Referral </a></td>");
        //            List<ReferralDetails> lst = new List<ReferralDetails>();
        //            lst = MdlPatient.GetRefferalDetailsById(Convert.ToInt32(Session["UserId"]), item.MessageId, "Sentbox");
        //            if (lst.Count > 0)
        //            {
        //                string comments = lst[0].Comments;
        //                comments = comments.Replace("\n", "<br/>");
        //                sb.Append("<td style=\"width:45% !important; vertical-align:text-top !important;\"><a>" + comments + "</a><br/><br/>");
        //                string[] strRegardingNo = lst[0].RegardOption.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        //                //  if (strRegardingNo != null)
        //                if (!string.IsNullOrEmpty(string.Join("", strRegardingNo)))
        //                {
        //                    sb.Append("<b class=\"hylyt_field\">Regarding</b> : ");
        //                    foreach (String item2 in strRegardingNo)
        //                    {
        //                        if (!string.IsNullOrEmpty(item2))
        //                        {

        //                            if ((Convert.ToInt32(item2) - 1) < MdlPatient.ReferralRegarding.Length)
        //                                sb.Append("" + "" + MdlPatient.ReferralRegarding[Convert.ToInt32(item2) - 1] + ",&nbsp;");
        //                        }

        //                    }
        //                }

        //                if (sb.ToString().EndsWith(",&nbsp;"))
        //                {
        //                    string str = sb.ToString().Substring(0, sb.ToString().LastIndexOf(",&nbsp;"));
        //                    sb = new StringBuilder(str);
        //                }
        //                string[] strRequest = lst[0].RequestingOption.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        //                // if (strRequest != null)
        //                if (!string.IsNullOrEmpty(string.Join("", strRequest)))
        //                {
        //                    sb.Append("<br/><b class=\"hylyt_field\">Request</b>&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;");
        //                    foreach (String item1 in strRequest)
        //                    {
        //                        if (!string.IsNullOrEmpty(item1))
        //                        {

        //                            if ((Convert.ToInt32(item1) - 1) < MdlPatient.ReferralRequest.Length)
        //                                sb.Append("" + "" + MdlPatient.ReferralRequest[Convert.ToInt32(item1) - 1] + ",&nbsp;");
        //                        }
        //                    }
        //                }

        //                if (sb.ToString().EndsWith(",&nbsp;"))
        //                {
        //                    string str = sb.ToString().Substring(0, sb.ToString().LastIndexOf(",&nbsp;"));
        //                    sb = new StringBuilder(str);
        //                }
        //                sb.Append("<br/><br/>");
        //                foreach (MessageAttachment objAttachment in lst[0].lstImages)
        //                {
        //                    string AttachmentName = objAttachment.AttachmentName;
        //                    AttachmentName = AttachmentName.Substring(AttachmentName.ToString().LastIndexOf("$") + 1);
        //                    sb.Append("<span><i class=\"attach\" style=\"margin-left:0px !important;\"></i><a target=\"_blank\" href=\"/ImageBank/" + objAttachment.AttachmentName + "\">" + AttachmentName + "</a></span>&nbsp;&nbsp;&nbsp;");
        //                }
        //                sb.Append("</td>");
        //                sb.Append("<td style=\"padding-left:0px !important;\"><a href=\"javascript:;\" onclick=\"PrintReferralMessage(" + item.MessageId + ")\"><span class=\"print_file\">[Print]</span></a></td>");

        //            }
        //        }
        //        if (item.MessageTypeId == 1)
        //        {
        //            sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewMessageOfColleagues('" + item.MessageId + "')\">" + item.CreationDate + "</a></td>");
        //            sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewMessageOfColleagues('" + item.MessageId + "')\">" + item.ColleagueName + "</a></td>");
        //            sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewMessageOfColleagues('" + item.MessageId + "')\">Messages </a></td>");
        //            string colleaguesmsg = HttpUtility.UrlDecode(Convert.ToString(item.Message));
        //            colleaguesmsg = colleaguesmsg.Replace("\n", " <br> ");
        //            sb.Append("<td style=\"width:45% !important; vertical-align:text-top !important;\"><a href=\"javascript:;\"onclick=\"ViewMessageOfColleagues('" + item.MessageId + "')\">" + (colleaguesmsg.ToString().Length > 70 ? colleaguesmsg.ToString().Substring(0, 70) + "..." : colleaguesmsg.ToString()) + "</a><br/><br/>");

        //            DataTable dt = new DataTable();
        //            string qry = "select * from ColleagueMessageAttachments where MessageId =" + item.MessageId;
        //            dt = ObjCommon.DataTable(qry);
        //            if (dt.Rows.Count > 0)
        //            {
        //                foreach (DataRow item1 in dt.Rows)
        //                {
        //                    if (!string.IsNullOrEmpty(dt.Rows[0]["Id"].ToString()))
        //                    {
        //                        string AttachmentName = string.Empty;
        //                        AttachmentName = Convert.ToString(item1["AttachmentName"]);
        //                        AttachmentName = AttachmentName.Substring(AttachmentName.ToString().LastIndexOf("$") + 1);
        //                        string extension = Path.GetExtension(AttachmentName);
        //                        string Type = string.Empty;

        //                        if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd" && extension.ToLower() != ".pspimage" && extension.ToLower() != ".thm" && extension.ToLower() != ".tif" && extension.ToLower() != ".yuv")
        //                        {
        //                            Type = "Doc";
        //                        }
        //                        else
        //                        {
        //                            Type = "Image";
        //                        }
        //                        //sb.Append("<span title=\"" + AttachmentName + "\" onclick=\"Attachementpopup('" + ImageBankFolder + item1["AttachmentName"] + "','" + Type + "')\" style=\"cursor: pointer;\" ><i class=\"attach\"></i> " + AttachmentName + "</span>");

        //                        sb.Append("<span><i class=\"attach\" style=\"margin-left:0px !important;\"></i><a target=\"_blank\" href=\"/ImageBank/" + item1["AttachmentName"] + "\">" + item1["AttachmentName"].ToString().Substring(item1["AttachmentName"].ToString().ToString().LastIndexOf("$") + 1) + "</a></span>&nbsp;&nbsp;&nbsp;");
        //                    }
        //                }
        //            }

        //            sb.Append("</td>");
        //            sb.Append("<td style=\"padding-left:0px !important;\"><a href=\"javascript:;\" onclick=\"PrintColleagueMessage(" + item.MessageId + ")\"><span class=\"print_file\">[Print]</span></a></td>");
        //        }
        //        if (item.MessageTypeId == 3)
        //        {
        //            sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewMessage('" + item.MessageId + "')\">" + item.CreationDate + "</a></td>");
        //            sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewMessage('" + item.MessageId + "')\">" + item.ColleagueName + "</a></td>");
        //            sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewMessage('" + item.MessageId + "')\">Messages </a></td>");
        //            string NewMessage = HttpUtility.UrlDecode(Convert.ToString(item.Message));
        //            string patientsmsg = NewMessage;
        //            patientsmsg = patientsmsg.Replace("\n", " <br> ");
        //            sb.Append("<td style=\"width:45% !important;vertical-align:text-top !important;\"><a href=\"javascript:;\"onclick=\"ViewMessage('" + item.MessageId + "')\">" + (patientsmsg.ToString().Length > 700 ? patientsmsg.ToString().Substring(0, 700) + "..." : patientsmsg.ToString()) + "</a><br/><br/>");

        //            DataTable dt = new DataTable();
        //            string qry = "select * from MessageAttachments where MessageId = " + item.MessageId;
        //            dt = ObjCommon.DataTable(qry);
        //            if (dt.Rows.Count > 0)
        //            {
        //                foreach (DataRow item1 in dt.Rows)
        //                {
        //                    if (!string.IsNullOrEmpty(dt.Rows[0]["Id"].ToString()))
        //                    {
        //                        string AttachmentName = string.Empty;
        //                        AttachmentName = Convert.ToString(item1["AttachmentName"]);
        //                        AttachmentName = AttachmentName.Substring(AttachmentName.ToString().LastIndexOf("$") + 1);
        //                        string extension = Path.GetExtension(AttachmentName);
        //                        string Type = string.Empty;

        //                        if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd" && extension.ToLower() != ".pspimage" && extension.ToLower() != ".thm" && extension.ToLower() != ".tif" && extension.ToLower() != ".yuv")
        //                        {
        //                            Type = "Doc";
        //                        }
        //                        else
        //                        {
        //                            Type = "Image";
        //                        }
        //                        // sb.Append("<span title=\"" + AttachmentName + "\" onclick=\"Attachementpopup('" + ImageBankFolder + item1["AttachmentName"] + "','" + Type + "')\" style=\"cursor: pointer;\" ><i class=\"attach\"></i> " + AttachmentName + "</span>");

        //                        sb.Append("<span><i class=\"attach\" style=\"margin-left:0px !important;\"></i><a target=\"_blank\" href=\"/ImageBank/" + item1["AttachmentName"] + "\">" + item1["AttachmentName"].ToString().Substring(item1["AttachmentName"].ToString().ToString().LastIndexOf("$") + 1) + "</a></span>&nbsp;&nbsp;&nbsp;");
        //                    }
        //                }
        //            }
        //            sb.Append("</td>");
        //            sb.Append("<td style=\"padding-left:0px !important;\"><a href=\"javascript:;\" onclick=\"PrintPatientMessage(" + item.MessageId + ")\"><span class=\"print_file\">[Print]</span></a></td>");
        //        }
        //        if (item.MessageTypeId == 4)
        //        {
        //            sb.Append("<td class=\"Communicationbody\"<a href=\"javascript:;\"onclick=\"ViewPatientNote('" + item.MessageId + "')\">" + item.CreationDate + "</a></td>");
        //            sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewPatientNote('" + item.MessageId + "')\">" + item.ColleagueName + "</a></td>");
        //            sb.Append("<td class=\"Communicationbody\"><a href=\"javascript:;\"onclick=\"ViewPatientNote('" + item.MessageId + "')\"> Notes </a></td>");
        //            sb.Append("<td style=\"width:45% !important; vertical-align:text-top !important;\"><a href=\"javascript:;\"onclick=\"ViewPatientNote('" + item.MessageId + "')\">" + (item.Message.ToString().Length > 700 ? item.Message.ToString().Substring(0, 700) + "..." : item.Message.ToString()) + "</a></td>");
        //            //  sb.Append("<td style=\"width:50% !important;\"><a style=\"float:right; \" class=\"inboxDltMsg\" title=\"Delete\" href=\"javascript:;\"onclick=\"RemovePatientNote('" + item.MessageId + "','" + PatientId + "')\"></a></td>");
        //        }

        //    }
        //    if (MdlColleagues.lstColleaguesMessagesDoctor.Count > 0)
        //    {
        //        return sb.ToString();
        //    }
        //    return "No record found";
        //}
        #endregion 
        #region Print View For All Patient Communication
        public ActionResult Printallcommunicationview()
        {
            // StringBuilder sb = new StringBuilder();
            try
            {

                MdlColleagues = new mdlColleagues();
                MdlPatient = new mdlPatient();
                ObjPatientsData = new clsPatientsData();
                int PatientId = Convert.ToInt32(TempData["PatientId"]);
                TempData.Keep();
                int UserId = Convert.ToInt32(Session["UserId"]);
                MdlColleagues.lstColleaguesMessagesDoctor = MdlColleagues.GetCommunicationHistoryForPatient(UserId, PatientId, 1, 500, 1, 2, SessionManagement.TimeZoneSystemName);
                ViewBag.PatientId = PatientId;
                DataSet ds = new DataSet();
                //RM-464 : Blank page for communitcation Hisotry
                ds = ObjPatientsData.GetPateintDetailsByPatientId(PatientId, UserId);
                if (ds != null && ds.Tables.Count > 0)
                {
                    MdlPatient.FirstName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]), string.Empty);
                    MdlPatient.LastName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LastName"]), string.Empty);
                    MdlPatient.DateOfBirth = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["DateOfBirth"]), string.Empty);
                    MdlPatient.Email = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Email"]), string.Empty);
                }
                ViewBag.mdlpatient = MdlPatient.FirstName + "  " + MdlPatient.LastName;

            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("", Ex.Message, Ex.StackTrace);
                throw;
            }
            return View(MdlColleagues);
        }
        #endregion

        public string GetColleaguesMessagesReceivedByDoctor(int UserId, int PatientId)
        {
            StringBuilder sb = new StringBuilder();
            MdlColleagues = new mdlColleagues();
            DataTable AttachedPatientDetials = new DataTable();
            //IEnumerable<ColleaguesMessagesOfDoctor> ColleagueMessageofDoctor = null;
            MdlColleagues.lstColleaguesMessagesDoctor = MdlColleagues.GetColleaguesMessagesForDoctor(UserId, PatientId);


            try
            {
                if (MdlColleagues.lstColleaguesMessagesDoctor.Count > 0)
                {

                    sb.Append("<div class=\"table_data messages clearfix\"><h4 class=\"sub_title clearfix  clearfix borderbtmn\">Communication<a class=\"inboxCompose\" title=\"Compose\"href=\"/Inbox/Compose\" style=\"float:right;\"></a></h4>");
                    sb.Append("<table class=\"comTable margintopm1\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><thead><tr>");
                    sb.Append(" <th style=\"width: 18% !important;\"><a href=\"javascript:;\" onclick=\"ColleaguesCommunication('" + UserId + "')\" class=\"sortable\">Date</a></th>");
                    sb.Append(" <th style=\"width:20% !important;\"><a href=\"javascript:;\" onclick=\"ColleaguesCommunication('" + UserId + "')\" class=\"sortable\">Doctor</a></th>");
                    sb.Append("<th style=\"width:50% !important;\"><a href=\"javascript:;\" onclick=\"ColleaguesCommunication('" + UserId + "')\" class=\"sortable\">Message</a></th>");

                    sb.Append(" <th><a href=\"javascript:;\" onclick=\"ColleaguesCommunication('" + UserId + "')\" class=\"sortable\">Actions</a></th></tr></thead><tbody>");

                    foreach (var item in MdlColleagues.lstColleaguesMessagesDoctor)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td style=\"width:18% !important;\"><a href=\"javascript:;\"onclick=\"ViewMessageOfColleagues('" + item.MessageId + "')\">" + item.CreationDate + "</a></td>");
                        if (item.MessageTypeId == 2)
                        {
                            sb.Append("<td style=\"width:20% !important;\"><a href=\"javascript:;\"onclick=\"ViewMessageOfColleagues('" + item.MessageId + "')\">" + item.ColleagueName + "</a></td>");
                        }
                        else
                        {
                            if (item.SenderId == Convert.ToInt32(Session["UserId"]))
                            {
                                sb.Append("<td style=\"width:20% !important;\"><a href=\"javascript:;\"onclick=\"ViewMessageOfColleagues('" + item.MessageId + "')\">" + item.ReciverName + "</a></td>");
                            }
                            else
                            {
                                sb.Append("<td style=\"width:20% !important;\"><a href=\"javascript:;\"onclick=\"ViewMessageOfColleagues('" + item.MessageId + "')\">" + item.SenderName + "</a></td>");
                            }
                        }
                        if (item.MessageTypeId == 1)
                        {

                            sb.Append("<td style=\"width:50% !important;\"><a href=\"javascript:;\"onclick=\"ViewMessageOfColleagues('" + item.MessageId + "')\">" + (item.Message.ToString().Length > 75 ? item.Message.ToString().Substring(0, 75) + "..." : item.Message.ToString()) + "</a></td>");
                        }
                        else
                        {
                            sb.Append("<td style=\"width:50% !important;\">" + (item.Message.ToString().Length > 75 ? item.Message.ToString().Substring(0, 75) + "..." : item.Message.ToString()) + "</td>");
                        }
                        if (item.MessageTypeId == 2)
                        {
                            if (item.SenderId == Convert.ToInt32(Session["UserId"]))
                            {
                                sb.Append("  <td><a href=" + Url.Action("Compose", "Inbox", new { ColleaguesId = item.ReciverId, Type = "reply" }) + " title=\"Reply\"><i class=\"inboxReply\">");
                            }
                            else
                            {
                                sb.Append("  <td><a href=" + Url.Action("Compose", "Inbox", new { ColleaguesId = item.SenderId, Type = "reply" }) + " title=\"Reply\"><i class=\"inboxReply\">");
                            }
                        }
                        else
                        {
                            if (item.SenderId == Convert.ToInt32(Session["UserId"]))
                            {
                                sb.Append("  <td><a href=" + Url.Action("Compose", "Inbox", new { ColleaguesId = item.ReciverId, Type = "reply" }) + " title=\"Reply\"><i class=\"inboxReply\">");

                            }
                            else
                            {
                                sb.Append("  <td><a href=" + Url.Action("Compose", "Inbox", new { ColleaguesId = item.SenderId, Type = "reply" }) + " title=\"Reply\"><i class=\"inboxReply\">");
                            }
                        }


                    }


                    sb.Append(" </tbody></table>");
                    sb.Append("</div>");
                    sb.Append("<br/><center>");

                    sb.Append("</center>");
                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }
        public string GetPatientMessagesReceivedByDoctorToPatientId(int UserId, int PageIndex, int SortBy, bool IsAsc, int PatientId, string IsAuthorize)
        {
            StringBuilder sb = new StringBuilder();
            IEnumerable<PatientMessagesOfDoctor> PatientMessagesOfDoctor = null;
            MdlPatient.lstPatientMessagesOfDoctor = MdlPatient.GetPatientMessagesSentByDoctorToPatientId(UserId, PageIndex, MinPageSize, PatientId);

            try
            {

                if (MdlPatient.lstPatientMessagesOfDoctor.Count > 0)
                {
                    #region Sorting
                    switch (SortBy)
                    {

                        case 1:
                            PatientMessagesOfDoctor = (IsAsc == true ? MdlPatient.lstPatientMessagesOfDoctor.OrderByDescending(p => p.FromField) : MdlPatient.lstPatientMessagesOfDoctor.OrderBy(p => p.FromField));
                            break;

                        case 2:
                            PatientMessagesOfDoctor = (IsAsc == true ? MdlPatient.lstPatientMessagesOfDoctor.OrderByDescending(p => p.Message) : MdlPatient.lstPatientMessagesOfDoctor.OrderBy(p => p.Message));
                            break;

                        default:
                            PatientMessagesOfDoctor = (IsAsc == true ? MdlPatient.lstPatientMessagesOfDoctor.OrderByDescending(p => p.CreationDate) : MdlPatient.lstPatientMessagesOfDoctor.OrderBy(p => p.CreationDate));
                            break;
                    }
                    #endregion

                    MdlPatient.lstPatientMessagesOfDoctor = PatientMessagesOfDoctor.ToList();

                    if (IsAsc)
                    {
                        IsAsc = false;
                    }
                    else
                    {
                        IsAsc = true;
                    }

                    double TotalPages = Math.Ceiling((double)MdlPatient.lstPatientMessagesOfDoctor.ElementAt(0).TotalRecord / PageSize);

                    sb.Append("<div class=\"table_data messages clearfix\"><h4 class=\"sub_title clearfix  clearfix borderbtmn\">Patient Communication" + (IsAuthorize != "false" ? " <input onclick=\"SendMessageToSelected()\" type=\"submit\"name=\"Message Patient\" class=\"message_patient\">" : "") + "</h4>");
                    sb.Append("<table class=\"comTable margintopm1\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><thead><tr>");
                    sb.Append(" <th><a href=\"javascript:;\" onclick=\"PatientCommunication('" + PageIndex + "','" + 0 + "','" + IsAsc + "','" + PatientId + "')\" class=\"sortable\">Date</a></th>");
                    sb.Append(" <th><a href=\"javascript:;\" onclick=\"PatientCommunication('" + PageIndex + "','" + 1 + "','" + IsAsc + "','" + PatientId + "')\" class=\"sortable\">From</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" onclick=\"PatientCommunication('" + PageIndex + "','" + 2 + "','" + IsAsc + "','" + PatientId + "')\" class=\"sortable\">Message</a></th>");
                    if (IsAuthorize != "false")
                    {
                        sb.Append(" <th><a href=\"javascript:;\" onclick=\"PatientCommunication('" + PageIndex + "','" + 0 + "','" + IsAsc + "','" + PatientId + "')\" class=\"sortable\">Actions</a></th></tr></thead><tbody>");
                    }
                    foreach (var item in MdlPatient.lstPatientMessagesOfDoctor)
                    {
                        sb.Append("<tr>");
                        //sb.Append("  <td><a href=\"javascript:;\" onclick=\"ViewMessage('"+item.MessageId+"')>"+item.Message +"</td>");
                        sb.Append("<td>" + item.CreationDate + "</td>");
                        sb.Append("<td>" + item.FromField + "</td>");
                        //sb.Append("<td>" + (item.Message.ToString().Length > 25 ? item.Message.ToString().Substring(0, 5) + "..." : item.Message.ToString()) + "</td>");
                        sb.Append("<td><a href=\"javascript:;\"onclick=\"ViewMessage('" + item.PatientmessageId + "')\">" + (item.Message.ToString().Length > 25 ? item.Message.ToString().Substring(0, 5) + "..." : item.Message.ToString()) + "</a></td>");
                        if (IsAuthorize != "false")
                        {
                            sb.Append("  <td><a href=" + Url.Action("PatientComposeMessage", "Patients", new { PatientId = item.PatientId, Type = "reply" }) + " title=\"Reply, Forward\"><i class=\"inboxReply\">");
                            sb.Append(" </i></a><a href=\"javascript:;\" title=\"Delete\" onclick=\"RemovePatientMessage('" + item.PatientmessageId + "','" + PatientId + "')\"><i class=\"inboxDltMsg\"></i></a></td></tr>");

                        }
                    }


                    sb.Append(" </tbody></table>");
                    sb.Append("</div>");
                    sb.Append("<br/><center>");
                    if (PageIndex > 1)
                    {
                        sb.Append("<a href=\"javascript:;\" onclick=\"PatientCommunication('" + (PageIndex - 1) + "','" + SortBy + "','" + IsAsc + "','" + PatientId + "','" + IsAuthorize + "')\" style=\"text-decoration:none;\"><< Prev </a>");
                    }

                    sb.Append(" &nbsp; Page &nbsp;" + PageIndex + " &nbsp;of &nbsp;" + TotalPages + " &nbsp;");

                    if (Convert.ToDouble(PageIndex) != TotalPages)
                    {
                        sb.Append("<a href=\"javascript:;\" onclick=\"PatientCommunication('" + (PageIndex + 1) + "','" + SortBy + "','" + IsAsc + "','" + PatientId + "','" + IsAuthorize + "')\" style=\"text-decoration:none;\">Next >> </a>");
                    }
                    sb.Append("</center>");

                }



            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        [HttpPost]
        public JsonResult RemovePatientMessage(int PatientmessageId, int PatientId)
        {
            MdlPatient = new mdlPatient();
            bool status = MdlPatient.RemovePatientMessageByDoctor(PatientmessageId);
            if (status)
            {
                return Json(GetPatientMessagesReceivedByDoctorToPatientId(Convert.ToInt32(Session["UserId"]), 1, 1, true, PatientId, ""), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }

        }
        #endregion

        #region Code For Get Patient Referral History Details By PatientId
        [HttpPost]
        public JsonResult ReferralHistoryOfPatient(int PatientId, int PageIndex, int SortBy, bool IsAsc)
        {
            MdlPatient = new mdlPatient();
            return Json(GetPatientReferralHistoryDetailsByPatientId(Convert.ToInt32(Session["UserId"]), PatientId, PageIndex, SortBy, IsAsc), JsonRequestBehavior.AllowGet);
        }
        public string GetPatientReferralHistoryDetailsByPatientId(int UserId, int PatientId, int PageIndex, int SortBy, bool IsAsc)
        {
            StringBuilder sb = new StringBuilder();
            IEnumerable<ReferralHistoryOfPatient> ReferralHistoryOfPatient = null;
            MdlPatient.lstReferralHistoryOfPatient = MdlPatient.GetPatientReferralHistoryDetailsByPatientId(UserId, PatientId, PageIndex, MinPageSize);
            try
            {

                if (MdlPatient.lstReferralHistoryOfPatient.Count > 0)
                {


                    #region Sorting
                    switch (SortBy)
                    {

                        case 1:
                            ReferralHistoryOfPatient = (IsAsc == true ? MdlPatient.lstReferralHistoryOfPatient.OrderByDescending(p => p.FromField) : MdlPatient.lstReferralHistoryOfPatient.OrderBy(p => p.FromField));
                            break;

                        case 2:
                            ReferralHistoryOfPatient = (IsAsc == true ? MdlPatient.lstReferralHistoryOfPatient.OrderByDescending(p => p.ToField) : MdlPatient.lstReferralHistoryOfPatient.OrderBy(p => p.ToField));
                            break;

                        case 3:
                            ReferralHistoryOfPatient = (IsAsc == true ? MdlPatient.lstReferralHistoryOfPatient.OrderByDescending(p => p.Comments) : MdlPatient.lstReferralHistoryOfPatient.OrderBy(p => p.Comments));
                            break;

                        default:
                            ReferralHistoryOfPatient = (IsAsc == true ? MdlPatient.lstReferralHistoryOfPatient.OrderByDescending(p => p.CreationDate) : MdlPatient.lstReferralHistoryOfPatient.OrderBy(p => p.CreationDate));
                            break;
                    }
                    #endregion

                    MdlPatient.lstReferralHistoryOfPatient = ReferralHistoryOfPatient.ToList();
                    double TotalPages = Math.Ceiling((double)MdlPatient.lstReferralHistoryOfPatient[0].TotalRecord / PageSize);
                    if (IsAsc)
                    {
                        IsAsc = false;
                    }
                    else
                    {
                        IsAsc = true;
                    }

                    sb.Append("<div class=\"table_data referrals_sent clearfix\"><h4 class=\"sub_title clearfix borderbtmn\">Referral History &nbsp;<a href=\"Javascript:;\" class=\"frmreferpatients\" style=\"float: right;\" onclick=\"OpenPopup('" + PatientId + "')\"></a></h4><div class=\"clear\"></div>");
                    sb.Append("<table class=\"refferalTable margintopm1\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\"><thead><tr>");
                    sb.Append("<th><a href=\"javascript:;\" onclick=\"ReferralHistoryOfPatient('" + PatientId + "','" + PageIndex + "','" + 0 + "','" + IsAsc + "')\" class=\"sortable\">Date</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" onclick=\"ReferralHistoryOfPatient('" + PatientId + "','" + PageIndex + "','" + 1 + "','" + IsAsc + "')\" class=\"sortable\">Status</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" onclick=\"ReferralHistoryOfPatient('" + PatientId + "','" + PageIndex + "','" + 0 + "','" + IsAsc + "')\" class=\"sortable\">Referred By</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" onclick=\"ReferralHistoryOfPatient('" + PatientId + "','" + PageIndex + "','" + 2 + "','" + IsAsc + "')\" class=\"sortable\">Referred To</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" onclick=\"ReferralHistoryOfPatient('" + PatientId + "','" + PageIndex + "','" + 3 + "','" + IsAsc + "')\" class=\"sortable\">Comments</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" onclick=\"ReferralHistoryOfPatient('" + PatientId + "','" + PageIndex + "','" + 0 + "','" + IsAsc + "')\" class=\"sortable\">Action</a></th></tr></thead><tbody>");


                    foreach (var item in MdlPatient.lstReferralHistoryOfPatient)
                    {
                        sb.Append("<tr>");
                        sb.Append("<td>" + item.CreationDate + "</td>");
                        sb.Append("<td><a href=\"javascript:;\"  class=\"green_btn\"><img src=\"../../Content/images/read.png\" width=\"20\" height=\"21\" alt=\"read\"><div class=\"tooltipc\">Complete</div></a>");
                        sb.Append("<td>" + item.FromField + "</td>");
                        sb.Append("<td>" + item.ToField + "</td>");
                        sb.Append("<td>" + (item.Comments.ToString().Length > 25 ? item.Comments.ToString().Substring(0, 25) + "..." : item.Comments.ToString()) + "</td>");
                        sb.Append("<td><a href=\"javascript:;\" title=\"Reply, Forward\"><i class=\"inboxReply\"></i></a><a href=\"javascript:;\" onclick=\"RemoveReferral('" + item.MessageId + "','" + item.ReferralCardId + "','" + PatientId + "')\" title=\"Delete\"><i class=\"inboxDltMsg\"></i></a></td></tr>");
                    }
                    sb.Append("</tbody></table>");
                    sb.Append("</div>");
                    sb.Append("<br/><center>");
                    if (PageIndex > 1)
                    {
                        sb.Append("<a href=\"javascript:;\" onclick=\"ReferralHistoryOfPatient('" + PatientId + "','" + (PageIndex - 1) + "','" + SortBy + "','" + IsAsc + "')\" style=\"text-decoration:none;\"><< Prev </a>");
                    }

                    sb.Append(" &nbsp; Page &nbsp;" + PageIndex + " &nbsp;of &nbsp;" + TotalPages + " &nbsp;");

                    if (Convert.ToDouble(PageIndex) != TotalPages)
                    {
                        sb.Append("<a href=\"javascript:;\" onclick=\"ReferralHistoryOfPatient('" + PatientId + "','" + (PageIndex + 1) + "','" + SortBy + "','" + IsAsc + "')\" style=\"text-decoration:none;\">Next >> </a>");
                    }
                    sb.Append("</center>");


                }


            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        [HttpPost]
        public JsonResult RemoveReferral(int MessageId, int ReferralcardId, int PatientId)
        {
            object obj = string.Empty;
            try
            {
                bool Result = false;
                Result = ObjColleaguesData.RemoveReferral(MessageId, ReferralcardId);
                if (Result)
                {
                    MdlPatient = new mdlPatient();
                    obj = GetPatientReferralHistoryDetailsByPatientId(Convert.ToInt32(Session["UserId"]), PatientId, 1, 1, false);
                }
                else
                {
                    obj = "0";
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Code For Export Patient list
        public ActionResult ExportToExcel(int LocationId = 0)
        {
            DataTable dt = new DataTable();
            dt = ObjPatientsData.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), 1, int.MaxValue, null, 11, 2, null, 0, LocationId);
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Remove("PatientId");
                dt.Columns.Remove("OwnerId");
                //dt.Columns.Remove("RowNumber");
                dt.Columns.Remove("TotalRecord");
                dt.AcceptChanges();
                ObjCommon.ExportToExcel(dt, "Patients_Record");
            }
            else
            {
                dt.Columns.Remove("PatientId");
                dt.Columns.Remove("OwnerId");
                //dt.Columns.Remove("RowNumber");
                dt.Columns.Remove("TotalRecord");

                dt.AcceptChanges();
                ObjCommon.ExportToExcel(dt, "Patients_Record");
            }
            return null;
        }
        #endregion

        #region Code For Export Referral History list of Doctor
        public ActionResult ExportReferralHistoryToExcel()
        {
            DataTable dt = new DataTable();
            dt = ObjPatientsData.GetPatientReferralHistoryDetails(Convert.ToInt32(Session["UserId"]), 1, int.MaxValue, 1, 1, null);
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.Columns.Remove("PatientId");

                dt.Columns.Remove("RowNumber");
                dt.Columns.Remove("TotalRecord");

                dt.AcceptChanges();
                ObjCommon.ExportToExcel(dt, "Patients_ReferralHistory_Record");
            }
            return null;
        }

        #endregion

        #region Code For Patient Message Compose


        //public PartialViewResult PatientMessageCompose()
        //{

        //    bool Result = ObjCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments()); // For Remove All Uplaoded Temp Images and Files

        //    MdlPatient = new mdlPatient();

        //    MdlPatient.lstTempUpload = MdlPatient.GetRecordsFromTempTablesForDoctor(Convert.ToInt32(Session["UserId"]));
        //    ViewBag.countatt = Convert.ToInt32(MdlPatient.lstTempUpload.Count);

        //    if (Request.QueryString["PatientId"] != null && Convert.ToString(Request.QueryString["PatientId"]) != "")
        //    {
        //        ViewBag.PatientId = Convert.ToString(Request.QueryString["PatientId"]);
        //    }


        //    if (Request.QueryString["Type"] != null && Convert.ToString(Request.QueryString["Type"]) != "")
        //    {
        //        ViewBag.Type = Convert.ToString(Request.QueryString["Type"]);
        //    }




        //    return PartialView("PartialPatientCompose", MdlPatient);
        //}

        [HttpPost]
        public JsonResult RemoveTempDoc(int Id, string DocName)
        {
            string extension = Path.GetExtension(DocName);
            string SuccessMessage = string.Empty;
            bool Result = false;
            object obj = null;
            try
            {
                if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd" && extension.ToLower() != ".pspimage" && extension.ToLower() != ".thm" && extension.ToLower() != ".tif")
                {
                    Result = ObjCommon.Temp_DeleteUploadedFileById(Convert.ToInt32(Session["UserId"]), Id);
                    SuccessMessage = "Document removed successfully";
                }
                else
                {
                    Result = ObjCommon.Temp_DeleteUploadedImageById(Convert.ToInt32(Session["UserId"]), Id);
                    SuccessMessage = "Image removed successfully";
                }

                if (Result)
                {

                    string DeleteDocument = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, TempFolder.Replace("/", "").Replace("~", ""));
                    DeleteDocument = DeleteDocument + "\\" + DocName;

                    if (System.IO.File.Exists(DeleteDocument))
                    {
                        System.IO.File.Delete(DeleteDocument);
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }

            obj = new
            {
                Success = true,

                Message = SuccessMessage,

            };
            return Json(obj, JsonRequestBehavior.DenyGet);


        }

        [HttpPost]
        public JsonResult GetAllTempUplaodedDocuments()
        {
            List<TempUpload> lstTempUplaod = new List<TempUpload>();





            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();
                dt = ObjCommon.GetRecordsFromTempTablesForDoctor(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstTempUplaod.Add(new TempUpload()
                        {
                            DocumentId = Convert.ToInt32(row["DocumentId"]),
                            DocumentName = ObjCommon.CheckNull(Convert.ToString(row["DocumentName"]), string.Empty),
                        });
                    }
                }


                if (lstTempUplaod.Count > 0)
                {

                    foreach (var item in lstTempUplaod)
                    {

                        sb.Append("<div style=\"background-color: #E5E5E5; width: 50%; border: 2px solid #E5E5E5; font-family: 11px Verdana,Geneva,sans-serif;border-radius: 5px; margin: 5px 5px 5px 114px; padding: 5px;\">");
                        sb.Append("<span style=\"color:#0890C4;cursor:pointer;\" onclick=\"OpenTempDoc('" + DataAccessLayer.Common.clsHelper.EscapeUriString(ConfigurationManager.AppSettings["TempUploadsFolderPath"].ToString() + item.DocumentName) + "')\">" + item.DocumentName.Substring(item.DocumentName.LastIndexOf("$") + 1) + "</span>");
                        sb.Append("<img alt=\"\" src=\"../../Content/images/Upload_Cancel.png\" style=\"float: right;\" onclick=\"RemoveTempDoc('" + item.DocumentId + "','" + item.DocumentName + "')\" /></div>");
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public JsonResult SendMessageToPatient(string SelectedPatientIds, string Message)
        //{
        //   string NotEmail = string.Empty;
        //    object obj = string.Empty;
        //    try
        //    {
        //        DataSet ds = new DataSet();
        //        DataSet dsPatient = new DataSet();
        //        string Doctorname = string.Empty;


        //        ds = ObjColleaguesData.GetDoctorDetailsById(Convert.ToInt32(Session["UserId"]));
        //        if (ds.Tables != null && ds.Tables.Count > 0)
        //        {
        //            Doctorname = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FullName"]), string.Empty);
        //        }


        //        string[] PatientIds = SelectedPatientIds.ToString().Split(new char[] { ',' });
        //        if (PatientIds.Length != 0)
        //        {

        //            foreach (var item in PatientIds)
        //            {
        //                if (!string.IsNullOrEmpty(item))
        //                {
        //                    dsPatient = ObjPatientsData.GetPateintDetailsByPatientId(Convert.ToInt32(item));
        //                    string PatientName = string.Empty; string Email = string.Empty;
        //                    if (dsPatient.Tables != null && dsPatient.Tables.Count > 0)
        //                    {
        //                        PatientName = ObjCommon.CheckNull(Convert.ToString(dsPatient.Tables[0].Rows[0]["FullName"]), string.Empty);
        //                    }
        //                    if (string.IsNullOrWhiteSpace(Convert.ToString(dsPatient.Tables[0].Rows[0]["Email"])))
        //                    {
        //                        NotEmail += PatientName + "<br/>";
        //                    }
        //                    int NewMessageId = ObjPatientsData.Insert_PatientMessages(Doctorname, PatientName, "Subject", Message, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(item), false, true, false);

        //                    ObjTemplate.SendMessageToPatient(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(item), Message, NewMessageId);

        //                    if (NewMessageId != 0)
        //                    {
        //                        #region Code For Patient's Message Attachemnts
        //                        DataTable dtTempUploadedDocs = ObjCommon.GetRecordsFromTempTablesForDoctor(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
        //                        if (dtTempUploadedDocs != null && dtTempUploadedDocs.Rows.Count > 0)
        //                        {

        //                            foreach (DataRow itemTempUploadedDocs in dtTempUploadedDocs.Rows)
        //                            {
        //                                string DocName = Convert.ToString(itemTempUploadedDocs["DocumentName"]);
        //                                DocName = DocName.Substring(DocName.ToString().LastIndexOf("$") + 1);
        //                                string NewFileName = Convert.ToString("$" + System.DateTime.Now.Ticks + "$" + DocName);
        //                                string Extension = Path.GetExtension(NewFileName);
        //                                DocName = Convert.ToString(itemTempUploadedDocs["DocumentName"]);
        //                                DocName = DocName.Replace("../TempUploads/", "");

        //                                string sourceFile = Server.MapPath(TempFolder) + "\\" + DocName;
        //                                string destinationFile = Server.MapPath(ImageBankFolder) + "\\" + NewFileName;
        //                                sourceFile = sourceFile.Replace("\\Patients\\", "\\");
        //                                destinationFile = destinationFile.Replace("\\Patients\\", "\\");
        //                                System.IO.File.Copy(sourceFile, destinationFile);

        //                                string MontageName = item + "Montage";
        //                                int montageid = Convert.ToInt32(ObjPatientsData.AddMontage(MontageName, 0, Convert.ToInt32(item), Convert.ToInt32(Session["UserId"]), 0));
        //                                if (Extension.ToLower() == ".jpg" || Extension.ToLower() == ".jpeg" || Extension.ToLower() == ".gif" || Extension.ToLower() == ".png" || Extension.ToLower() == ".bmp" || Extension.ToLower() == ".psd" || Extension.ToLower() == ".pspimage" || Extension.ToLower() == ".thm" || Extension.ToLower() == ".tif" || Extension.ToLower() == ".yuv")
        //                                {
        //                                    string savepathThumb = "";
        //                                    string tempPathThumb = "";
        //                                    string TempPath = "";
        //                                    string thambnailimage = NewFileName;
        //                                    string Savepath = Server.MapPath(ImageBankFolder);

        //                                    Savepath = Savepath.Replace("\\Patients\\", "\\");
        //                                    tempPathThumb = System.Configuration.ConfigurationManager.AppSettings["FolderPathThumb"];
        //                                    TempPath = System.Configuration.ConfigurationManager.AppSettings["ImageBank"];
        //                                    savepathThumb = Server.MapPath(tempPathThumb);
        //                                    savepathThumb = savepathThumb.Replace("\\Patients\\", "\\");



        //                                    // Load image.
        //                                    System.Drawing.Image image = System.Drawing.Image.FromFile(Savepath + @"\" + NewFileName);

        //                                    // Compute thumbnail size.
        //                                    Size thumbnailSize = ObjCommon.GetThumbnailSize(image);

        //                                    // Get thumbnail.
        //                                    System.Drawing.Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
        //                                        thumbnailSize.Height, null, IntPtr.Zero);

        //                                    // Save thumbnail.
        //                                    thumbnail.Save(savepathThumb + @"\" + thambnailimage);

        //                                    int imageid = ObjPatientsData.AddImagetoPatient(Convert.ToInt32(item), 1, NewFileName, 1000, 1000, 0, 500, sourceFile, TempPath + NewFileName, Convert.ToInt32(Session["UserId"]), 0, 1, "Patient");
        //                                    if (imageid > 0)
        //                                    {
        //                                        bool resmonimg = ObjPatientsData.AddImagetoMontage(imageid, 500, montageid);
        //                                    }

        //                                }
        //                                else
        //                                {

        //                                    int Docstatus = ObjCommon.AddDocumenttoMontage(montageid, NewFileName, "Doctor");
        //                                }
        //                                if (!string.IsNullOrEmpty(NewFileName))
        //                                {

        //                                    bool Attach = ObjPatientsData.InsertMessageAttachemntsForPatient(NewMessageId, NewFileName);
        //                                }

        //                            }
        //                        }

        //                        #endregion


        //                    }
        //                }
        //            }
        //            if (!string.IsNullOrWhiteSpace(NotEmail))
        //            {
        //                obj = NotEmail;
        //            }
        //            else
        //            {
        //                obj = "Message sent successfully<br/><br/>";
        //            }


        //        }




        //        DataTable dtfileuploaddelete = ObjCommon.GetRecordsFromTempTablesForDoctor(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());

        //        foreach (DataRow item in dtfileuploaddelete.Rows)
        //        {
        //            string DocName = Convert.ToString(item["DocumentName"]);
        //            DocName = Convert.ToString(item["DocumentName"]);
        //            DocName = DocName.Replace("../TempUploads/", "");

        //            string sourceFile = Server.MapPath(ConfigurationManager.AppSettings.Get("TempUploadsFolderPath")) + DocName;

        //            sourceFile = sourceFile.Replace("\\Patients\\", "\\");

        //            if (System.IO.File.Exists(sourceFile))
        //            {
        //                System.IO.File.Delete(sourceFile);
        //            }
        //        }

        //        bool Result = ObjCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments()); // For Remove All Uplaoded Temp Images and Files


        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //    return Json(obj, JsonRequestBehavior.AllowGet);
        //}
        #endregion



        #region DownloadAll files
        DataTable getFilenames = new DataTable();

        public ActionResult DownloadAsZip()
        {
            int PatientId = 0;
            string PatientName = string.Empty;
            if (Request.QueryString["PatientId"] != null)
            {
                PatientId = Convert.ToInt32(Request.QueryString["PatientId"]);
                DataTable ds = new DataTable();
                ds = clsPatientsData.PatientHistory(PatientId);
                if (ds != null && ds.Rows.Count > 0)
                {
                    string FirstName = ObjCommon.CheckNull(Convert.ToString(ds.Rows[0]["FirstName"]), string.Empty);
                    string LastName = ObjCommon.CheckNull(Convert.ToString(ds.Rows[0]["LastName"]), string.Empty);
                    if (!string.IsNullOrEmpty(FirstName)) { FirstName = FirstName.Trim(); }
                    if (!string.IsNullOrEmpty(LastName)) { LastName = LastName.Trim(); }
                    PatientName = FirstName + "_" + LastName;
                    //PatientName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]), string.Empty) + "_" + ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LastName"]), string.Empty);
                }
                int ID = PatientId;
                DataTable dt = new DataTable();
                DataTable dtDoc = new DataTable();
                dt = ObjPatientsData.GetPatientImages(PatientId, null);
                dtDoc = ObjPatientsData.GetPatientDocumentsAll(PatientId, null);
                if (dt != null && dt.Rows.Count > 0)
                {
                    getFilenames.Columns.Add("FileName", System.Type.GetType("System.String"));
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        foreach (DataRow item in dt.Rows)
                        {
                            DataRow dr = getFilenames.NewRow();
                            dr["FileName"] = ConfigurationManager.AppSettings.Get("ImageBank") + item["Name"].ToString();
                            getFilenames.Rows.Add(dr);
                        }
                    }
                }
                if (dtDoc != null && dtDoc.Rows.Count > 0)
                {
                    if (!getFilenames.Columns.Contains("FileName"))
                    {
                        getFilenames.Columns.Add("FileName", System.Type.GetType("System.String"));
                    }
                    foreach (DataRow item in dtDoc.Rows)
                    {
                        DataRow dr = getFilenames.NewRow();
                        //RM-150
                        //dr["FileName"] = item["DocumentName"].ToString();
                        dr["FileName"] = ConfigurationManager.AppSettings.Get("ImageBank") + item["DocumentName"].ToString();
                        getFilenames.Rows.Add(dr);
                    }
                }
                if (getFilenames != null && getFilenames.Rows.Count > 0)
                {
                    ZipAllFiles(PatientId, PatientName);
                }
            }
            return RedirectToAction("PatientHistory", "Patients", new { PatientId = PatientId });

        }
        public void ZipAllFiles(int PatientId, string PatientName)
        {

            PatientName = PatientName.Trim().Replace(' ', '_');
            byte[] buffer = new byte[4096];
            // the path on the server where the temp file will be created! 
            var tempFileName = Server.MapPath(ConfigurationManager.AppSettings["TempUploadsFolderPath"]) + PatientName + ".zip";
            var zipOutputStream = new ZipOutputStream(System.IO.File.Create(tempFileName));
            var filePath = String.Empty;
            var fileName = String.Empty;
            var readBytes = 0;
            foreach (DataRow item in getFilenames.Rows)
            {
                if (item["FileName"].ToString() != "")
                {
                    fileName = item["FileName"].ToString();
                    filePath = Server.MapPath(fileName);
                    FileInfo fi = new FileInfo(filePath);
                    string PF_Name = String.Empty;
                    //if (fi.Exists)
                    //{
                    String[] History_Pat = fileName.Split('$');
                    if (History_Pat[0].Length > 0)
                    {
                        if (History_Pat[0].Equals(ConfigurationManager.AppSettings.Get("ImageBank")))
                        {
                            //PF_Name = "../Patient_Documents/" + History_Pat[2].ToString();
                            PF_Name = History_Pat[2].ToString();
                        }
                        else
                        {
                            PF_Name = fileName;
                        }
                    }
                    var zipEntry = new ZipEntry(PF_Name.Replace(" ", ""));
                    zipOutputStream.PutNextEntry(zipEntry);
                    using (var fs = System.IO.File.OpenRead(filePath))
                    {
                        do
                        {
                            readBytes = fs.Read(buffer, 0, buffer.Length);
                            zipOutputStream.Write(buffer, 0, readBytes);
                        } while (readBytes > 0);
                    }
                    //}
                }
            }
            if (zipOutputStream.Length == 0)
            {
                return;
            }
            zipOutputStream.Finish();
            zipOutputStream.Close();
            Response.ContentType = "application/x-zip-compressed";
            string _patientName = PatientName.Trim();

            //Response.AppendHeader("Content-Disposition", "attachment; filename=Patient_" + PatientId + "_" + PatientName + "_Records.zip");
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + _patientName + "_Records.zip");
            Response.WriteFile(tempFileName);
            Response.Flush();
            Response.Close();

            //delete the temp file  
            if (System.IO.File.Exists(tempFileName))
                System.IO.File.Delete(tempFileName);

        }
        #endregion
        #region Import Patient
        //public ActionResult ImportPatient()
        //{
        //    if (SessionManagement.UserId != null && SessionManagement.UserId != "")
        //    {
        //        return PartialView("PartialImportPatient");
        //    }
        //    else
        //    {
        //        return RedirectToAction("Index", "User");
        //    }


        //}
        [CustomAuthorize]
        public ActionResult ImportPatient()
        {
            return View("ImportPatients");
        }
        // Import Step 2
        [HttpPost]
        public JsonResult GetImportPatientStep2(string UplaodeFileName)
        {
            return Json(ImportPatientStep2(UplaodeFileName), JsonRequestBehavior.AllowGet);
        }

        public ActionResult NewGetImportPatientStep2(string UplaodeFileName)
        {
            int status = 0;
            DataSet ds = new DataSet();
            DataTable dtExcelRecords = new DataTable();
            string fileName = Path.GetFileName(UplaodeFileName);
            string fileLocation = Server.MapPath("../CSVLoad/" + fileName);
            //string fileLocation = "D:\\Project\\RL-BitBucket\\NewRecordlinc\\CSVLoad\\$636467804035586506$Patient_Import(8).xlsx";
            FileInfo fi = new FileInfo(fileLocation);
            List<BO.Models.MatchFields> ListMatch = new List<BO.Models.MatchFields>();
            if (!string.IsNullOrEmpty(UplaodeFileName))
            {
                try
                {
                    ds = ReadExcel(fi.FullName, fi.Extension, true);
                    DataTable dt = ds.Tables[0];
                    var my = dt.Rows.Cast<DataRow>().Where(row => !row.ItemArray.All(field =>
                            field is System.DBNull || string.Compare((field as string).Trim(),
                            string.Empty) == 0)).CopyToDataTable();

                    dtExcelRecords = ds.Tables[0];
                    Session["dtMapping"] = dtExcelRecords;
                    BO.Models.MatchFields match = new BO.Models.MatchFields();
                    ListMatch.Add(new BO.Models.MatchFields
                    {
                        id = "0",
                        text = "--Select--"
                    });
                    if (dtExcelRecords != null && dtExcelRecords.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtExcelRecords.Columns.Count; i++)
                        {
                            ListMatch.Add(new BO.Models.MatchFields
                            {
                                id = Convert.ToString(dtExcelRecords.Columns[i]),
                                text = Convert.ToString(dtExcelRecords.Columns[i])
                            });
                        }
                        ViewBag.list = ListMatch;
                    }
                    foreach (DataRow item in dt.Rows)
                    {
                        BO.Models.MappedColumn MappedColumn = new BO.Models.MappedColumn();
                        MappedColumn.FirstName = item["FirstName"].ToString();
                        MappedColumn.LastName = item["LastName"].ToString();
                        MappedColumn.Gender = item["Gender"].ToString();
                        MappedColumn.Email = item["Email"].ToString();
                        MappedColumn.DateOfBirth = Convert.ToDateTime(item["DateOfBirth"]);

                        //Numeric first name and last name.
                        Regex regexname = new Regex(@"^[a-zA-Z]+$");
                        Match fnameMatched = regexname.Match((MappedColumn.FirstName));
                        Match lnameMatched = regexname.Match((MappedColumn.LastName));
                        if (!fnameMatched.Success || !lnameMatched.Success)
                        {
                            int FL = 2;
                            return Json(FL);
                        }

                        DateTime currentDate = DateTime.Now;
                        string message = string.Empty;
                        if (MappedColumn.DateOfBirth > currentDate)
                        {
                            int dob = 3;
                            return Json(dob);
                        }

                        string email = MappedColumn.Email;
                        if (!String.IsNullOrEmpty(email))
                        {
                            Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
                            Match matched = regex.Match(email);
                            if (!matched.Success)
                            {
                                int mail = 1;
                                return Json(mail);
                            }
                        }


                        if (string.IsNullOrEmpty(MappedColumn.FirstName) || string.IsNullOrEmpty(MappedColumn.LastName) || string.IsNullOrEmpty(MappedColumn.Gender) || string.IsNullOrEmpty(MappedColumn.Email))
                        {
                            string empty = "empty";
                            return Json(empty);
                        }
                    }
                    return View("_PartialMatchExcelFields");

                }
                catch (Exception Ex)
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public string ImportPatientStep2(string UplaodeFileName)
        {

            StringBuilder sb = new StringBuilder();
            try
            {
                DataSet ds = new DataSet();
                DataTable dtExcelRecords = new DataTable();
                string fileName = Path.GetFileName(UplaodeFileName);
                string fileLocation = Server.MapPath("../CSVLoad/" + fileName);

                FileInfo fi = new FileInfo(fileLocation);
                if (!string.IsNullOrEmpty(UplaodeFileName))
                {


                    ds = ReadExcel(fi.FullName, fi.Extension, true);
                    dtExcelRecords = ds.Tables[0];
                    Session["dtMapping"] = dtExcelRecords;
                    string ddlMaping = GetDDLForMappingColumn(dtExcelRecords);

                    sb.Append("<ul class=\"tabbed_form_content tabbed_form_content1 match_field\">");



                    sb.Append("<li>");
                    sb.Append("<label>First Name<span class=\"required\">*</span></label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlFirstName\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping); //FirstName Dropdownlist
                    sb.Append("</select></div></li>");


                    sb.Append("<li>");
                    sb.Append("<label>Middle Name</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlMiddelName\"  class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//MiddelName Dropdownlist
                    sb.Append("</select></div></li>");


                    sb.Append("<li>");
                    sb.Append("<label>Last Name<span class=\"required\">*</span></label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlLastName\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//LastName Dropdownlist
                    sb.Append("</select></div></li>");



                    sb.Append("<li>");
                    sb.Append("<label>Date Of Birth</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlDateOfBirth\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping); //DateOfBirth DropDownlist
                    sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>Gender<span class=\"required\">*</span></label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlGender\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Gender Dropdownlist
                    sb.Append("</select></div></li>");


                    sb.Append("<li>");
                    sb.Append("<label>Email<span class=\"required\">*</span></label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append(" <select name=\"standard-dropdown\" id=\"ddlEmail\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Email Dropdownlist
                    sb.Append("</select></div></li>");



                    sb.Append("<li>");
                    sb.Append("<label>Password</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlPassword\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Password Dropdownlist
                    sb.Append("</select></div></li>");





                    sb.Append("<li>");
                    sb.Append("<label>Address</label>");
                    sb.Append(" <div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlAddress\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Address Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>City</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlCity\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//City Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append(" <li>");
                    sb.Append("<label>State</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlState\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//State Dropdownlist
                    sb.Append("</select></div></li>");


                    sb.Append("<li>");
                    sb.Append("<label>Country</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlCountry\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;\">");
                    sb.Append(ddlMaping);//Country Dropdownlist
                    sb.Append("</select></div></li>");

                    sb.Append("<li>");
                    sb.Append("<label>Zip</label><div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlZip\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%; z-index: 1111;\">");
                    sb.Append(ddlMaping);//Zip Dropdownlist
                    sb.Append("</select></div></li>");


                    sb.Append(" <li>");
                    sb.Append("<label>Phone</label>");
                    sb.Append("<div class=\"grey_frmbg\">");
                    sb.Append("<select name=\"standard-dropdown\" id=\"ddlPhone\" class=\"custom-class1 custom-class2 right\" style=\"width: 100%;z-index: 1111;\">");
                    sb.Append(ddlMaping);//Phone Dropdownlist
                    sb.Append("</select></div></li>");



                    sb.Append("</ul>");
                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }
        public string GetDDLForMappingColumn(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();
            try
            {

                if (dt != null && dt.Rows.Count > 0)
                {
                    sb.Append("<option value=\"0\">--Select--</option>");

                    for (int i = 0; i < dt.Columns.Count; i++)
                    {
                        sb.Append("<option value=\"" + dt.Columns[i].ToString() + "\">" + dt.Columns[i].ToString() + "</option>");
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }

            return sb.ToString();
        }



        // Import Step 4
        [HttpPost]
        public JsonResult GetImportPatientStep4(string Mapping)
        {
            string[] ArrayMapping = Mapping.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            DataTable dtExcelRecord = new DataTable();
            dtExcelRecord = (DataTable)Session["dtMapping"];


            StringBuilder PatientIds = new StringBuilder();
            // Code for insert data in Database as per columns map

            foreach (DataRow item in dtExcelRecord.Rows)
            {

                if (ArrayMapping[5] != "0")
                {
                    string Email = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[5]]), string.Empty);
                    if (!string.IsNullOrEmpty(Email))
                    {
                        if (ObjCommon.IsEmail(Email))
                        {
                            bool CheckEmail = false;
                            CheckEmail = ObjPatientsData.CheckPatientEmail(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[5]]), string.Empty));
                            if (CheckEmail == false)
                            {
                                int Gender = 0; string Phone = string.Empty; string City = string.Empty;
                                string Address = string.Empty; string State = string.Empty;
                                string Country = string.Empty; string DateOfBirth = string.Empty;
                                string ZipCode = string.Empty; string MiddleName = string.Empty;


                                if (ArrayMapping[1] != "0")
                                {
                                    MiddleName = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[1]]), string.Empty);
                                }


                                if (ArrayMapping[3] != "0")
                                {
                                    DateOfBirth = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[3]]), string.Empty);

                                }
                                if (ArrayMapping[4] != "0")
                                {
                                    Gender = ObjCommon.GetGender(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[4]]), string.Empty));
                                }




                                if (ArrayMapping[7] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[7]]), string.Empty)))
                                    {
                                        Address = Convert.ToString(item[ArrayMapping[7]]);
                                    }

                                }

                                if (ArrayMapping[8] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[8]]), string.Empty)))
                                    {
                                        City = Convert.ToString(item[ArrayMapping[8]]);
                                    }

                                }




                                if (ArrayMapping[9] != "0")
                                {
                                    DataTable dtState = new DataTable();
                                    string qry = "select * from StateMaster where StateName='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[9]]), string.Empty) + "' or StateCode='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[9]]), string.Empty) + "'";
                                    dtState = ObjCommon.DataTable(qry);
                                    if (dtState != null && dtState.Rows.Count > 0)
                                    {
                                        State = dtState.Rows[0]["StateCode"].ToString();
                                    }
                                }


                                if (ArrayMapping[10] != "0")
                                {
                                    DataTable dtCountry = new DataTable();
                                    string qry = "select * from CountryMaster where CountryName='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[10]]), string.Empty) + "' or CountryCode='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[10]]), string.Empty) + "'";
                                    dtCountry = ObjCommon.DataTable(qry);
                                    if (dtCountry != null && dtCountry.Rows.Count > 0)
                                    {
                                        Country = dtCountry.Rows[0]["CountryCode"].ToString();
                                    }
                                }


                                if (ArrayMapping[11] != "0")
                                {
                                    bool CheckZip = false;
                                    CheckZip = ObjCommon.IsValidZipCode(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[11]]), string.Empty));
                                    if (CheckZip)
                                    {
                                        ZipCode = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[11]]), string.Empty);
                                    }
                                }


                                if (ArrayMapping[12] != "0")
                                {
                                    bool CheckPhone = false;
                                    CheckPhone = ObjCommon.IsValidPhone(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[12]]), string.Empty));
                                    if (CheckPhone)
                                    {
                                        Phone = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[12]]), string.Empty);
                                    }
                                }

                                //password
                                string passwd = null;
                                if (ArrayMapping[6] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[6]]), string.Empty)))
                                    {
                                        passwd = Convert.ToString(item[ArrayMapping[6]]);
                                    }
                                }
                                else
                                {
                                    passwd = ObjCommon.CreateRandomPassword(8);
                                }

                                int InsertPatient = 0;
                                InsertPatient = ObjPatientsData.PatientInsert(0, null, ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[0].ToString()]), string.Empty),
                                    MiddleName,
                                    ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[2]]), string.Empty),
                                    DateOfBirth, Gender, null, Phone,
                                    ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[5]]), string.Empty), Address, City, State, ZipCode, Country, Convert.ToInt32(Session["UserId"]), 0, null,
                                    passwd, null);


                                if (InsertPatient != 0)
                                {
                                    if (string.IsNullOrEmpty(PatientIds.ToString()))
                                    {
                                        PatientIds.Append(Convert.ToString(InsertPatient));
                                    }
                                    else
                                    {
                                        PatientIds.Append("," + Convert.ToString(InsertPatient));
                                    }
                                }
                            }
                        }
                    }


                }
            }


            return Json(ImportPatientStep4(PatientIds.ToString()), JsonRequestBehavior.AllowGet);
        }



        public string ImportPatientStep4(string PatientIds)
        {
            StringBuilder sb = new StringBuilder();
            try
            {

                string[] ArrayPatientIds = PatientIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (ArrayPatientIds.Length > 0)
                {


                    sb.Append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"patientTableImport\">");
                    sb.Append("<thead><tr>");
                    sb.Append("<th>First Name</th>");
                    sb.Append("<th>Middle Name</th>");
                    sb.Append("<th>Last Name</th>");
                    sb.Append("<th>Date Of Birth</th>");
                    sb.Append("<th>Gender</th>");
                    sb.Append("<th>Email</th>");
                    sb.Append("<th>Password</th>");
                    sb.Append("<th>Address</th>");
                    sb.Append("<th>City</th>");
                    sb.Append("<th>State</th>");
                    sb.Append("<th>Country</th>");
                    sb.Append("<th>Zip</th>");
                    sb.Append("<th>Phone</th>");
                    sb.Append("</tr></thead>");
                    sb.Append("<tbody>");

                    for (int i = 0; i < ArrayPatientIds.Length; i++)
                    {
                        DataSet dtImported = new DataSet();
                        dtImported = ObjPatientsData.GetPateintDetailsByPatientId(Convert.ToInt32(ArrayPatientIds[i]));
                        if (dtImported != null && dtImported.Tables.Count > 0)
                        {

                            for (int j = 0; j < dtImported.Tables[0].Rows.Count; j++)
                            {
                                sb.Append("<tr>");





                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["FirstName"]), string.Empty) + "</td>");
                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["MiddelName"]), string.Empty) + "</td>");
                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["LastName"]), string.Empty) + "</td>");
                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["DateOfBirth"]), string.Empty) + "</td>");
                                string Gender = ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["Gender"]), string.Empty);
                                if (Gender == "1")
                                {
                                    Gender = "Female";
                                }
                                else
                                {
                                    Gender = "Male";
                                }
                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Gender, string.Empty) + "</td>");
                                sb.Append("<td><div class=\"block_data\"  onmouseout=\"divonmouseout()\"  onmouseover=\"divonmouseover()\" ><a href=\"javascript:;\" class=\"message_icon_import\"><div class=\"tooltip_import\">" + ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["Email"]), string.Empty) + "</div></a></div></td>");
                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["ExactPassword"]), string.Empty) + "</td>");


                                sb.Append("<td class=\"numeric\"><div class=\"block_data\"  onmouseout=\"divonmouseout()\"  onmouseover=\"divonmouseover()\"><a href=\"javascript:;\" class=\"location_icon_import\"><div class=\"tooltip_import\">" + ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["ExactAddress"]), string.Empty) + "</div></a></div></td>");
                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["City"]), string.Empty) + "</td>");
                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["State"]), string.Empty) + "</td>");
                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["Country"]), string.Empty) + "</td>");
                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["ZipCode"]), string.Empty) + "</td>");
                                sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["Phone"]), string.Empty) + "</td>");


                                sb.Append("</tr>");
                            }


                        }
                    }


                    sb.Append("</tbody></table>");

                }
                else
                {
                    sb.Append("<br/><center>No record imported</center>");
                }



            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        //Step 4 
        public ActionResult NewGetImportPatientStep4(string Mapping)
        {
            string[] ArrayMapping = Mapping.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            DataTable dtExcelRecord = new DataTable();
            dtExcelRecord = (DataTable)Session["dtMapping"];
            StringBuilder PatientIds = new StringBuilder();
            StringBuilder UnImportedEmails = new StringBuilder();
            List<BO.Models.MappedColumn> ImportedList = new List<BO.Models.MappedColumn>();
            List<BO.Models.MappedColumn> UnImportedList = new List<BO.Models.MappedColumn>();
            foreach (DataRow item in dtExcelRecord.Rows)
            {
                BO.Models.MappedColumn MappedColumn = new BO.Models.MappedColumn();
                if (ArrayMapping[5] != "0")
                {
                    MappedColumn.Email = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[5]]).Trim(), string.Empty);
                    if (!string.IsNullOrEmpty(MappedColumn.Email.Trim()))
                    {
                        if (ObjCommon.IsEmail(MappedColumn.Email))
                        {
                            bool CheckEmail = false;
                            CheckEmail = ObjPatientsData.CheckPatientEmail(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[5]]).Trim(), string.Empty));
                            if (CheckEmail == false)
                            {
                                MappedColumn.Gender = string.Empty;
                                MappedColumn.Phone = string.Empty;
                                MappedColumn.City = string.Empty;
                                MappedColumn.Address = string.Empty;
                                MappedColumn.State = string.Empty;
                                MappedColumn.Country = string.Empty;
                                MappedColumn.DateOfBirth = null;
                                MappedColumn.Zip = string.Empty;
                                MappedColumn.MiddleName = string.Empty;

                                if (ArrayMapping[0] != "0")
                                {
                                    MappedColumn.FirstName = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[0]]).Trim(), string.Empty);
                                }

                                if (ArrayMapping[2] != "0")
                                {
                                    MappedColumn.LastName = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[2]]).Trim(), string.Empty);
                                }

                                if (ArrayMapping[1] != "0")
                                {
                                    MappedColumn.MiddleName = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[1]]).Trim(), string.Empty);
                                }

                                if (ArrayMapping[3] != "0")
                                {
                                    DateTime dtValue;
                                    DateTime.TryParse(item[ArrayMapping[3]].ToString(), out dtValue);
                                    string date1 = ObjCommon.CheckNull(Convert.ToString(dtValue.ToShortDateString()), string.Empty);
                                    string datetrim1 = date1.Replace("00:00:00", "").Trim();
                                    // MappedColumn.DateOfBirth = dtValue.Date;
                                    MappedColumn.DateOfBirth = (dtValue.Date == DateTime.MinValue) ? (DateTime?)null : dtValue.Date;
                                }
                                else
                                {
                                    MappedColumn.DateOfBirth = null;
                                }

                                //Gender
                                if (ArrayMapping[4] != "0")
                                {
                                    int Gender = 0;
                                    Gender = ObjCommon.GetGender(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[4]]).Trim(), string.Empty));
                                    MappedColumn.Gender = Convert.ToString(Gender);
                                }

                                if (ArrayMapping[7] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[7]]).Trim(), string.Empty)))
                                    {
                                        MappedColumn.Address = Convert.ToString(item[ArrayMapping[7]]).Trim();
                                    }
                                }

                                if (ArrayMapping[8] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[8]]).Trim(), string.Empty)))
                                    {
                                        MappedColumn.City = Convert.ToString(item[ArrayMapping[8]]).Trim();
                                    }
                                }

                                if (ArrayMapping[9] != "0")
                                {
                                    DataTable dtState = new DataTable();
                                    string qry = "select * from StateMaster where StateName='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[9]]), string.Empty) + "' or StateCode='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[9]]), string.Empty) + "'";
                                    dtState = ObjCommon.DataTable(qry);
                                    if (dtState != null && dtState.Rows.Count > 0)
                                    {
                                        MappedColumn.State = dtState.Rows[0]["StateCode"].ToString();
                                    }
                                }

                                if (ArrayMapping[10] != "0")
                                {
                                    DataTable dtCountry = new DataTable();
                                    string qry = "select * from CountryMaster where CountryName='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[10]]), string.Empty) + "' or CountryCode='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[10]]), string.Empty) + "'";
                                    dtCountry = ObjCommon.DataTable(qry);
                                    if (dtCountry != null && dtCountry.Rows.Count > 0)
                                    {
                                        MappedColumn.Country = dtCountry.Rows[0]["CountryCode"].ToString();
                                    }
                                }

                                if (ArrayMapping[11] != "0")
                                {
                                    bool CheckZip = false;
                                    CheckZip = ObjCommon.IsValidZipCode(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[11]]).Trim(), string.Empty));
                                    if (CheckZip)
                                    {
                                        MappedColumn.Zip = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[11]]).Trim(), string.Empty);
                                    }
                                }

                                if (ArrayMapping[12] != "0")
                                {
                                    bool CheckPhone = false;
                                    CheckPhone = ObjCommon.IsValidPhone(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[12]]).Trim(), string.Empty));
                                    if (CheckPhone)
                                    {
                                        MappedColumn.Phone = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[12]]).Trim(), string.Empty);
                                    }
                                }

                                if (ArrayMapping[6] != "0")
                                {
                                    MappedColumn.Password = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[6]]).Trim(), string.Empty);
                                }
                                else
                                {
                                    MappedColumn.Password = ObjCommon.CreateRandomPassword(8);
                                }
                                int InsertPatient = 0;
                                InsertPatient = ObjPatientsData.NewPatientInsert(MappedColumn, Convert.ToInt32(Session["UserId"]), 0, 0, null, null, 0, null, null);
                                if (InsertPatient != 0)
                                {
                                    if (string.IsNullOrEmpty(PatientIds.ToString()))
                                    {
                                        PatientIds.Append(Convert.ToString(InsertPatient));
                                    }
                                    else
                                    {
                                        PatientIds.Append("," + Convert.ToString(InsertPatient));
                                    }
                                }
                            }
                            else
                            {
                                //Code for Unimported Patients                              
                                BO.Models.MappedColumn UnMappedColumn = new BO.Models.MappedColumn();
                                UnMappedColumn.Email = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[5]]).Trim(), string.Empty);
                                int Gender = 0;
                                Gender = ObjCommon.GetGender(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[4]]), string.Empty));
                                UnMappedColumn.Gender = Convert.ToString(Gender);
                                UnMappedColumn.LastName = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[2]]).Trim(), string.Empty);
                                UnMappedColumn.FirstName = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[0]]).Trim(), string.Empty);
                                if (ArrayMapping[1] != "0")
                                {
                                    UnMappedColumn.MiddleName = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[1]]).Trim(), string.Empty);
                                }

                                if (ArrayMapping[3] != "0")
                                {
                                    DateTime dtValue;
                                    DateTime.TryParse(item[ArrayMapping[3]].ToString(), out dtValue);
                                    string date1 = ObjCommon.CheckNull(Convert.ToString(dtValue.ToShortDateString()), string.Empty);
                                    string datetrim1 = date1.Replace("00:00:00", "").Trim();
                                    //UnMappedColumn.DateOfBirth = dtValue.Date;
                                    UnMappedColumn.DateOfBirth = (dtValue.Date == DateTime.MinValue) ? (DateTime?)null : dtValue.Date;
                                }

                                if (ArrayMapping[7] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[7]]).Trim(), string.Empty)))
                                    {
                                        UnMappedColumn.Address = Convert.ToString(item[ArrayMapping[7]]);
                                    }

                                }

                                if (ArrayMapping[8] != "0")
                                {
                                    if (!string.IsNullOrEmpty(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[8]]).Trim(), string.Empty)))
                                    {
                                        UnMappedColumn.City = Convert.ToString(item[ArrayMapping[8]]);
                                    }

                                }

                                if (ArrayMapping[9] != "0")
                                {
                                    DataTable dtState = new DataTable();
                                    string qry = "select * from StateMaster where StateName='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[9]]), string.Empty) + "' or StateCode='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[9]]), string.Empty) + "'";
                                    dtState = ObjCommon.DataTable(qry);
                                    if (dtState != null && dtState.Rows.Count > 0)
                                    {
                                        UnMappedColumn.State = dtState.Rows[0]["StateCode"].ToString();
                                    }
                                }

                                if (ArrayMapping[10] != "0")
                                {
                                    DataTable dtCountry = new DataTable();
                                    string qry = "select * from CountryMaster where CountryName='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[10]]), string.Empty) + "' or CountryCode='" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[10]]), string.Empty) + "'";
                                    dtCountry = ObjCommon.DataTable(qry);
                                    if (dtCountry != null && dtCountry.Rows.Count > 0)
                                    {
                                        UnMappedColumn.Country = dtCountry.Rows[0]["CountryCode"].ToString();
                                    }
                                }

                                if (ArrayMapping[11] != "0")
                                {
                                    bool CheckZip = false;
                                    CheckZip = ObjCommon.IsValidZipCode(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[11]]).Trim(), string.Empty));
                                    if (CheckZip)
                                    {
                                        UnMappedColumn.Zip = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[11]]), string.Empty);
                                    }
                                }

                                if (ArrayMapping[12] != "0")
                                {
                                    bool CheckPhone = false;
                                    CheckPhone = ObjCommon.IsValidPhone(ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[12]]).Trim(), string.Empty));
                                    if (CheckPhone)
                                    {
                                        UnMappedColumn.Phone = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[12]]), string.Empty);
                                    }
                                }
                                if (ArrayMapping[6] != "0")
                                {
                                    UnMappedColumn.Password = ObjCommon.CheckNull(Convert.ToString(item[ArrayMapping[6]]).Trim(), string.Empty);
                                }
                                else
                                {
                                    UnMappedColumn.Password = string.Empty;
                                }
                                bool isAnyPropEmpty = UnMappedColumn.GetType().GetProperties()
    .Where(pi => pi.GetValue(UnMappedColumn) is string)
    .Select(pi => (string)pi.GetValue(UnMappedColumn))
    .All(value => String.IsNullOrEmpty(value));
                                if (!isAnyPropEmpty)
                                {
                                    UnImportedList.Add(UnMappedColumn);
                                }
                            }
                        }
                    }
                }
            }
            List<BO.Models.MappedColumn> importedList = NewImportPatientStep4(PatientIds.ToString());
            ViewBag.icnt = importedList.Count();
            ViewBag.ucnt = UnImportedList.Count();
            ViewBag.UnimportedList = UnImportedList;
            return View("_PartialImportedExcelRows", importedList);
        }

        //New 
        public List<BO.Models.MappedColumn> NewImportPatientStep4(string PatientIds)
        {
            StringBuilder sb = new StringBuilder();
            List<BO.Models.MappedColumn> ImportList = new List<BO.Models.MappedColumn>();
            try
            {
                string[] ArrayPatientIds = PatientIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                if (ArrayPatientIds.Length > 0)
                {
                    for (int i = 0; i < ArrayPatientIds.Length; i++)
                    {
                        DataSet dtImported = new DataSet();
                        dtImported = ObjPatientsData.GetPateintDetailsByPatientId(Convert.ToInt32(ArrayPatientIds[i]));
                        if (dtImported != null && dtImported.Tables.Count > 0)
                        {
                            BO.Models.MappedColumn uip = new BO.Models.MappedColumn();
                            for (int j = 0; j < dtImported.Tables[0].Rows.Count; j++)
                            {
                                uip.FirstName = ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["FirstName"]), string.Empty);
                                uip.MiddleName = ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["MiddelName"]), string.Empty);
                                uip.LastName = ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["LastName"]), string.Empty);

                                string date = ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["DateOfBirth"]), string.Empty);
                                if (date == "")
                                {
                                    uip.DateOfBirth = null;
                                }
                                else
                                {
                                    string datetrim = date.Replace("00:00:00.000", "").Trim();
                                    DateTime dtValue;
                                    DateTime.TryParse(dtImported.Tables[0].Rows[j]["DateOfBirth"].ToString(), out dtValue);
                                    uip.DateOfBirth = dtValue.Date;
                                }
                                uip.Gender = ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["Gender"]), string.Empty);
                                uip.Email = ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["Email"]), string.Empty);
                                uip.Password = ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["ExactPassword"]), string.Empty);
                                uip.Address = ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["ExactAddress"]), string.Empty);
                                uip.City = ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["City"]), string.Empty);
                                uip.State = ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["State"]), string.Empty);
                                uip.Country = ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["Country"]), string.Empty);
                                uip.Zip = ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["ZipCode"]), string.Empty);
                                uip.Phone = ObjCommon.CheckNull(Convert.ToString(dtImported.Tables[0].Rows[j]["Phone"]), string.Empty);
                                ImportList.Add(uip);
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return ImportList;
        }


        [HttpPost]
        public JsonResult ImportPatientShowExcelMappedData(string Mapping)
        {
            return Json(ImportPatientShowExcel(Mapping), JsonRequestBehavior.AllowGet);
        }

        //Pankaj
        [HttpPost]
        public ActionResult NewImportPatientShowExcelMappedData(string Mapping)
        {
            DataTable dtExcelRecord = new DataTable();
            dtExcelRecord = (DataTable)Session["dtMapping"];
            string[] ArrayMappedColumns = Mapping.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            List<BO.Models.MappedColumn> mappedColumnList = new List<BO.Models.MappedColumn>();
            foreach (DataRow item in dtExcelRecord.Rows)
            {

                BO.Models.MappedColumn MappedColumn = new BO.Models.MappedColumn();
                if (ArrayMappedColumns[0] != "0")
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(item[ArrayMappedColumns[0]])))
                    {
                        MappedColumn.FirstName = ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[0]]), string.Empty);
                    }
                    else
                    {
                        MappedColumn.FirstName = null;
                    }
                }

                if (ArrayMappedColumns[1] != "0")
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(item[ArrayMappedColumns[1]])))
                    {
                        MappedColumn.MiddleName = ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[1]]), string.Empty);
                    }
                    else
                    {
                        MappedColumn.MiddleName = null;
                    }
                }

                if (ArrayMappedColumns[2] != "0")
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(item[ArrayMappedColumns[2]])))
                    {
                        MappedColumn.LastName = ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[2]]), string.Empty);
                    }
                    else
                    {
                        MappedColumn.LastName = null;
                    }
                }

                if (ArrayMappedColumns[3] != "0")
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(item[ArrayMappedColumns[3]])))
                    {
                        DateTime dtValue;
                        DateTime.TryParse(item[ArrayMappedColumns[3]].ToString(), out dtValue);
                        string date = ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[3]]), string.Empty);
                        string trimdate = date.Replace("00:00:00", "").Trim();
                        //MappedColumn.DateOfBirth = dtValue.Date;
                        MappedColumn.DateOfBirth = (dtValue.Date == DateTime.MinValue) ? (DateTime?)null : dtValue.Date;
                    }
                    else
                    {
                        MappedColumn.DateOfBirth = null;
                    }
                }

                if (ArrayMappedColumns[4] != "0")
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(item[ArrayMappedColumns[4]])))
                    {
                        MappedColumn.Gender = ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[4]]), string.Empty);
                    }
                    else
                    {
                        MappedColumn.Gender = null;
                    }
                }

                if (ArrayMappedColumns[5] != "0")
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(item[ArrayMappedColumns[5]])))
                    {
                        MappedColumn.Email = ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[5]]), string.Empty);
                    }
                    else
                    {
                        MappedColumn.Email = null;
                    }
                }

                if (ArrayMappedColumns[6] != "0")
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(item[ArrayMappedColumns[6]])))
                    {
                        MappedColumn.Password = ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[6]]), string.Empty);
                    }
                    else
                    {
                        MappedColumn.Password = null;
                    }
                }
                //Address
                if (ArrayMappedColumns[7] != "0")
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(item[ArrayMappedColumns[7]])))
                    {
                        MappedColumn.Address = ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[7]]), string.Empty);
                    }
                    else
                    {
                        MappedColumn.Address = null;
                    }
                }
                //City
                if (ArrayMappedColumns[8] != "0")
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(item[ArrayMappedColumns[8]])))
                    {
                        MappedColumn.City = ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[8]]), string.Empty);
                    }
                    else
                    {
                        MappedColumn.City = null;
                    }
                }
                //State
                if (ArrayMappedColumns[9] != "0")
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(item[ArrayMappedColumns[9]])))
                    {
                        MappedColumn.State = ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[9]]), string.Empty);
                    }
                    else
                    {
                        MappedColumn.State = null;
                    }
                }
                //Country
                if (ArrayMappedColumns[10] != "0")
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(item[ArrayMappedColumns[10]])))
                    {
                        MappedColumn.Country = ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[10]]), string.Empty);
                    }
                    else
                    {
                        MappedColumn.Country = null;
                    }
                }
                //Zip
                if (ArrayMappedColumns[11] != "0")
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(item[ArrayMappedColumns[11]])))
                    {
                        MappedColumn.Zip = ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[11]]), string.Empty);
                    }
                    else
                    {
                        MappedColumn.Zip = null;
                    }
                }
                //Phone
                if (ArrayMappedColumns[12] != "0")
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(item[ArrayMappedColumns[12]])))
                    {
                        MappedColumn.Phone = ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[12]]), string.Empty);
                    }
                    else
                    {
                        MappedColumn.Phone = null;
                    }
                }
                bool isAnyPropEmpty = MappedColumn.GetType().GetProperties()
    .Where(pi => pi.GetValue(MappedColumn) is string)
    .Select(pi => (string)pi.GetValue(MappedColumn))
    .All(value => String.IsNullOrEmpty(value));
                if (!isAnyPropEmpty)
                {
                    mappedColumnList.Add(MappedColumn);
                }
            }
            return View("_PartialImportedExcelRows", mappedColumnList);
        }


        public string ImportPatientShowExcel(string MappedColumns)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dtExcelRecord = new DataTable();
                dtExcelRecord = (DataTable)Session["dtMapping"];

                string[] ArrayMappedColumns = MappedColumns.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);


                sb.Append("<table border=\"0\" cellspacing=\"0\" cellpadding=\"0\" class=\"patientTableImport\">");
                sb.Append("<thead><tr>");
                sb.Append("<th>First Name</th>");
                sb.Append("<th>Middle Name</th>");
                sb.Append("<th>Last Name</th>");
                sb.Append("<th>Date Of Birth</th>");
                sb.Append("<th>Gender</th>");
                sb.Append("<th>Email</th>");
                sb.Append("<th>Password</th>");
                sb.Append("<th>Address</th>");
                sb.Append("<th>City</th>");
                sb.Append("<th>State</th>");
                sb.Append("<th>Country</th>");
                sb.Append("<th>Zip</th>");
                sb.Append("<th>Phone</th>");
                sb.Append("</tr></thead>");
                sb.Append("<tbody>");

                foreach (DataRow item in dtExcelRecord.Rows)
                {
                    sb.Append("<tr>");

                    #region Firstname


                    if (ArrayMappedColumns[0] != "0")
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(item[ArrayMappedColumns[0]])))
                        {
                            return "First name cannot be null or empty.<br/>Please click on the upload excel file section to upload new excel file.";
                        }
                        else
                        {
                            sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[0]]), string.Empty) + "</td>");
                        }
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region MiddleName


                    if (ArrayMappedColumns[1] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[1]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region LastName
                    if (ArrayMappedColumns[2] != "0")
                    {
                        if (string.IsNullOrEmpty(Convert.ToString(item[ArrayMappedColumns[2]])))
                        {
                            return "Last name cannot be null or empty.<br/>Please click on the upload excel file section to upload new excel file.";
                        }
                        else
                        {
                            sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[2]]), string.Empty) + "</td>");
                        }
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region DateOfBirth
                    if (ArrayMappedColumns[3] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[3]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion


                    #region Gender
                    if (ArrayMappedColumns[4] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[4]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region Email
                    if (ArrayMappedColumns[5] != "0")
                    {
                        sb.Append("<td><div class=\"block_data\" onmouseout=\"divonmouseout()\"  onmouseover=\"divonmouseover()\"><a href=\"javascript:;\" class=\"message_icon_import\"><div class=\"tooltip_import\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[5]]), string.Empty) + "</div></a></div></td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region Password
                    if (ArrayMappedColumns[6] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[6]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }

                    #endregion

                    #region Address
                    if (ArrayMappedColumns[7] != "0")
                    {
                        sb.Append("<td class=\"numeric\"><div class=\"block_data\"  onmouseout=\"divonmouseout()\"  onmouseover=\"divonmouseover()\" ><a href=\"javascript:;\" class=\"location_icon_import\"><div class=\"tooltip_import\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[7]]), string.Empty) + "</div></a></div></td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region City
                    if (ArrayMappedColumns[8] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[8]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region State
                    if (ArrayMappedColumns[9] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[9]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion


                    #region Country
                    if (ArrayMappedColumns[10] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[10]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion

                    #region Zip
                    if (ArrayMappedColumns[11] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[11]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion


                    #region Phone


                    if (ArrayMappedColumns[12] != "0")
                    {
                        sb.Append("<td class=\"numeric\">" + ObjCommon.CheckNull(Convert.ToString(item[ArrayMappedColumns[12]]), string.Empty) + "</td>");
                    }
                    else
                    {
                        sb.Append("<td class=\"numeric\"></td>");
                    }
                    #endregion



                    sb.Append("</tr>");
                }


                sb.Append("</tbody></table>");

            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }



        public DataTable getExcelColumn()
        {
            DataTable dt1 = new DataTable();
            try
            {
                OleDbConnection objConnection = new OleDbConnection(ConnectionString());
                objConnection.Open();

                DataTable tbl = objConnection.GetSchema("Tables");




                string sheetName = (string)tbl.Rows[0]["TABLE_NAME"];

                OleDbCommand objCmdSelect = new OleDbCommand("SELECT TOP 1 * FROM [" + sheetName + "]", objConnection);
                OleDbDataAdapter objAdapter = new OleDbDataAdapter(objCmdSelect);
                DataTable Dt = new DataTable();
                objAdapter.Fill(Dt);
                dt1 = new DataTable("Excel");


                dt1.Columns.Add("ExcelColumn", typeof(String));





                foreach (DataColumn dc in Dt.Columns)
                {
                    dt1.Rows.Add(new object[] { dc.ColumnName });

                }
                objConnection.Close();
                return dt1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private DataSet ReadExcel(string filePath, string Extension, bool isHDR)
        {
            FileStream stream = System.IO.File.Open(filePath, FileMode.Open, FileAccess.Read);
            IExcelDataReader excelReader = null;

            switch (Extension)
            {
                case ".xls":
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                    break;
                case ".xlsx": //Excel 07
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    break;
            }

            excelReader.IsFirstRowAsColumnNames = isHDR;
            DataSet result = excelReader.AsDataSet();


            while (excelReader.Read())
            {

            }

            excelReader.Close();

            return result;

        }
        private String ConnectionString()
        {
            return "Provider=Microsoft.ACE.OLEDB.12.0;" +
                "Data Source=" + FileNameNew + "; " +
                "Extended Properties=\"Excel 12.0;HDR=YES;\"";
        }
        public DataTable getExcelColumnData()
        {

            try
            {
                OleDbConnection objConnection = new OleDbConnection(ConnectionString());
                objConnection.Open();

                DataTable Table2 = objConnection.GetSchema("Tables");


                string sheetName = (string)Table2.Rows[0]["TABLE_NAME"];

                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + sheetName + "]", objConnection);
                OleDbDataAdapter objAdapter = new OleDbDataAdapter(objCmdSelect);
                DataTable Dt = new DataTable();
                objAdapter.Fill(Dt);

                objConnection.Close();
                return Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region Remove Patient Profile Image


        public ActionResult RemovePationtProfileImage(int PatientId)
        {
            if (PatientId > 0)
            {
                ObjPatientsData = new clsPatientsData();
                ObjPatientsData.RemovePatientProfileImage(PatientId);
            }
            return RedirectToAction("PatientHistory", new { PatientId = PatientId });
        }


        #endregion

        //DD Chnanges
        public string GetLocationColleagues(int ColleagueId, int PatientId)
        {

            List<BO.ViewModel.AddressDetails> lstDoctorAddressDetails = new List<BO.ViewModel.AddressDetails>();
            clsColleaguesData objColleaguesData = new clsColleaguesData();
            clsCommon objCommon = new clsCommon();
            DataSet ds = new DataSet();
            ds = objColleaguesData.GetDoctorDetailsById(ColleagueId);
            string GetString = string.Empty;
            if (ds != null && ds.Tables.Count > 0)
            {
                //For Address details of Doctor

                lstDoctorAddressDetails = (from p in ds.Tables[2].AsEnumerable()
                                           select new BO.ViewModel.AddressDetails
                                           {
                                               AddressInfoID = Convert.ToInt32(p["AddressInfoID"]),

                                               ExactAddress = objCommon.CheckNull(Convert.ToString(p["ExactAddress"]), string.Empty),
                                               Address2 = objCommon.CheckNull(Convert.ToString(p["Address2"]), string.Empty),
                                               City = objCommon.CheckNull(Convert.ToString(p["City"]), string.Empty),
                                               State = objCommon.CheckNull(Convert.ToString(p["State"]), string.Empty),
                                               Country = objCommon.CheckNull(Convert.ToString(p["Country"]), string.Empty),
                                               ZipCode = objCommon.CheckNull(Convert.ToString(p["ZipCode"]), string.Empty),
                                               Phone = objCommon.CheckNull(Convert.ToString(p["Phone"]), string.Empty),
                                               ContactType = Convert.ToInt32(p["ContactType"]),
                                               Location = objCommon.CheckNull(Convert.ToString(p["LocationName"]), string.Empty)
                                           }).ToList();
                ViewBag.ColleagueId = ColleagueId;
                ViewData.Model = lstDoctorAddressDetails;
                if (lstDoctorAddressDetails.Count > 1)
                {
                    string viewName = "PartialPatientColleaguesLocation";

                    using (var sw = new StringWriter())
                    {
                        var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                        var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                        viewResult.View.Render(viewContext, sw);
                        viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                        GetString = sw.GetStringBuilder().ToString();
                    }
                    return GetString;
                }
                return "1";
            }


            return "2";
        }

        // [CustomAuthorizeAttribute]
        public ActionResult SentandReceivedPatient(string flag)
        {
            ViewBag.Flag = flag;
            //if (Request["Referral"] != null)
            //{
            //    ViewBag.IsReferralSent = Request["Referral"].ToString();
            //}
            //if (Request["Email"] != null)
            //{
            //    ViewBag.Searchsentbox = Request["Email"].ToString();
            //}
            if (Request["ColleagueId"] != null)
            {
                ViewBag.ColleagueId = Request["ColleagueId"].ToString();
            }
            return View();
        }
        public ActionResult PartialSentandReceivedPatient(string Flag, string SortColumn, string SortDirection, string SearchText, string NewSearchText, string FilterBy, string ColleagueId)
        {
            if (SearchText == "undefined" || SearchText == "Search" || SearchText == "")
            {
                SearchText = null;
            }
            if (NewSearchText == "null" || NewSearchText == "ALL")
            {
                NewSearchText = null;
            }
            if (ColleagueId == "undefined")
            {
                ColleagueId = null;
            }
            mdlPatient MdlPatient = new mdlPatient();
            return View(MdlPatient.GetPatientSentAndReceivedList(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(Flag), Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection), SearchText, 1, 50, NewSearchText, Convert.ToInt32(FilterBy), ColleagueId));
        }
        [HttpPost]
        public JsonResult GetPatientAutocompleteList(string text)
        {
            MdlPatient = new mdlPatient();
            MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), 1, 8, text, 1, 1, null, 0);
            return Json(MdlPatient.lstPatientDetailsOfDoctor, JsonRequestBehavior.AllowGet);
            //return Json(1);
        }
        [HttpPost]
        public ActionResult SearchPatientFromPopUp(string SearchText, int? PatientId)
        {
            MdlPatient = new mdlPatient();
            MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), 1, 30, SearchText, 11, 2, null, 0).Where(t => t.PatientId != PatientId).OrderBy(t => t.LastName).ToList();

            List<FamilyReleationHistory> ReleationHistory = new List<FamilyReleationHistory>();
            int p = PatientId ?? 0;
            ReleationHistory = MdlPatient.GetFamilyHistoryDetails(p);
            //RM-444
            //Var result = MsgList.Except(MsgList.Where(o => SentList.Select(s => s.MsgID).ToList().Contains(o.MsgID))).ToList();
            MdlPatient.lstPatientDetailsOfDoctorSorted = MdlPatient.lstPatientDetailsOfDoctor.Except(MdlPatient.lstPatientDetailsOfDoctor.Where(o => ReleationHistory.Select(r => r.ToPatientId).ToList().Contains(o.PatientId))).ToList();


            if (MdlPatient.lstPatientDetailsOfDoctor.Count == 0)
            {
                return Json("no_record", JsonRequestBehavior.AllowGet);
            }
            return View("_patientFamilyPopup", MdlPatient.lstPatientDetailsOfDoctorSorted);
        }

        public string GetPatientListForSendReferralPopUp(string SearchText, int? PatientId)
        {
            StringBuilder sb = new StringBuilder();
            string searchValue = "Find patient to add as family member...";
            if (!string.IsNullOrEmpty(SearchText))
            {
                searchValue = SearchText;
            }
            // int PatientId = Convert.ToInt32(Request.QueryString["PatientId"]);
            try
            {
                MdlPatient = new mdlPatient();
                MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), 1, 30, SearchText, 11, 2, null, 0).Where(t => t.PatientId != PatientId).ToList();

                //sb.Append("<style>#Checkallarea input[type=checkbox]{display: none;}</style>");
                sb.Append(" <h2 id=\"TitleRefer\" class=\"title\">Select Patient to add as family member</h2>");
                sb.Append(" <div class=\"search_main_div\"><input style=\"display:none\" id=\"SearchBtnHide\" class=\"search_btn_popup\" name=\"Cancel\" type=\"button\" value=\"Cancel\" onclick=\"SearchPatient( " + PatientId + " )\"><div class=\"patient_searchbar\"><input  onfocus=\"if (this.value =='Find patient to add as family member...') {this.value = '';}\"  id=\"txtSearchPatient\" onblur=\"if (this.value == '') {this.value = 'Find patient to add as family member...';}\"  type=\"text\" value=\"" + searchValue + "\" name=\"\"></div></div>");
                sb.Append("<div id=\"ColleagueLocation\"></div>");
                sb.Append("<div id=\"Patientcallonscroll\" class=\"cntnt\"><input type=\"hidden\" id=\"LocationID\" value=\"\">");


                if (MdlPatient.lstPatientDetailsOfDoctor.Count > 0)
                {
                    sb.Append("<ul id=\"ScondIdScroll\" class=\"search_listing\" style=\"width:100%;\">");


                    foreach (var item in MdlPatient.lstPatientDetailsOfDoctor.Take(500))
                    {
                        sb.Append("<li><div class=\"clearfix\" data-val=\"" + item.Gender.ToString() + "\" id=\"" + item.PatientId + "\"   style=\"display:block !important\" > ");
                        sb.Append("<span class=\"check\"><input type=\"checkbox\" name=\"\" class=\"checkbox_add_patient\" id=\"chkPatient\" value=\"" + item.PatientId + "\"></span>");
                        sb.Append("<span class=\"thumb\"><img style=\"height: 95px;width: 85px;\" alt=\"\" src='" + ((!string.IsNullOrEmpty(item.ImageName) ? item.ImageName : (item.Gender.ToString() == "Female") ? System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"] : System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorImage"])) + "' ></span> <span class=\"description\"><h2><a  style=\"word-wrap: break-word;\" href=\"javascript:;\">" + item.FirstName + "&nbsp;" + item.LastName + "</a></h2></span>");
                        sb.Append("<span class=\"clear\"></span></div></li>");
                    }
                    //sb.Append("</ul><div class=\"clear\"></div><br /><div id=\"EmptyRecord\"></div><div class=\"actions\"><a href=\"JavaScript:;\" onclick=\"AddPatientsAsMemeber()\" ><img alt=\"\" class=\"btn-popup pull-right\" src=\"../../Content/images/btn-next.png\"/></a><input class=\"New_cancel_btn btn-popup\" name=\"Cancel\" type=\"submit\" onclick=\"CloseEditPatientListpop()\" value=\"Cancel\" style=\"width:155px; height:49px;float:left;\"><div class=\"clear\">");
                    sb.Append("<div id=\"EmptyRecord\"></div></ul><div class=\"clear\"></div><br /><div class=\"actions\"><a href=\"JavaScript:;\" onclick=\"AddPatientsAsMemeber()\" ><img alt=\"\" class=\"btn-popup pull-right\" src=\"../../Content/images/btn-next.png\"/></a><input class=\"New_cancel_btn btn-popup\" name=\"Cancel\" type=\"submit\" onclick=\"CloseEditPatientListpop()\" value=\"Cancel\" style=\"width:155px; height:49px;float:left;\"><div class=\"clear\">");
                    //sb.Append("</ul><div class=\"clear\"></div><br /><div id=\"EmptyRecord\"></div><div class=\"actions\"><a href=\"JavaScript:;\" onclick=\"ReferralSend()\" ><img alt=\"\" style=\"width:140px; height:41px;margin-bottom: 3px;\" src=\"../../Content/images/btn-next.png\"/></a><input class=\"New_cancel_btn\" name=\"Cancel\" type=\"submit\" onclick=\"CloseEditPatientListpop()\" value=\"Cancel\" style=\"width:140px; height:38px;\"><div class=\"clear\">");
                }
                else
                {
                    sb.Append("<center>Patient not found. Please<a href=\"" + Url.Action("AddPatient", "Patients") + "\" style=\"color: #0890c4;\"> click here</a> to add new patient.</center>");
                }

                sb.Append("</div><button class=\"mfp-close\" type=\"button\" title=\"Close (Esc)\">×</button>");
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }
        public ActionResult SearchPatientListForSendReferralPopUpLI(string SearchText, string pageindex, string pagesize, int? PatientId)
        {
            MdlPatient = new mdlPatient();
            MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(pageindex), Convert.ToInt32(pagesize), SearchText, 11, 2, null, 0).Where(t => t.PatientId != PatientId).ToList();

            List<FamilyReleationHistory> ReleationHistory = new List<FamilyReleationHistory>();
            int p = PatientId ?? 0;
            ReleationHistory = MdlPatient.GetFamilyHistoryDetails(p);
            //RM-444 : Filter the patients which are added as family member
            MdlPatient.lstPatientDetailsOfDoctorSorted = MdlPatient.lstPatientDetailsOfDoctor.Except(MdlPatient.lstPatientDetailsOfDoctor.Where(o => ReleationHistory.Select(r => r.ToPatientId).ToList().Contains(o.PatientId))).ToList();
            return PartialView("_patientFamilyListPopup", MdlPatient.lstPatientDetailsOfDoctorSorted); //GetPatientListForSendReferralPopUpLI(SearchText, int.Parse(pageindex), int.Parse(pagesize), PatientId);
        }

        //Get Only LI Containt 
        public string GetPatientListForSendReferralPopUpLI(string SearchText, int pageindex, int pagesize, int? PatientId)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                MdlPatient = new mdlPatient();
                MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), pageindex, pagesize, SearchText, 11, 2, null, 0).Where(t => t.PatientId != PatientId).ToList();

                if (MdlPatient.lstPatientDetailsOfDoctor.Count > 0)
                {
                    foreach (var item in MdlPatient.lstPatientDetailsOfDoctor)
                    {
                        sb.Append("<li><div class=\"clearfix\" data-val=\"" + item.Gender.ToString() + "\" id=\"" + item.PatientId + "\">");
                        sb.Append("<span class=\"check\"><input type=\"checkbox\" name=\"\" class=\"checkbox_add_patient\" id=\"chkPatient\" value=\"" + item.PatientId + "\"></span>");
                        sb.Append("<span class=\"thumb\"><img style=\"height: 95px;width: 85px;\" alt=\"\" src='" + ((!string.IsNullOrEmpty(item.ImageName) ? item.ImageName : (item.Gender.ToString() == "Female") ? System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"] : System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorImage"])) + "' ></span> <span class=\"description\"><h2><a  style=\"word-wrap: break-word;\" href=\"javascript:;\">" + item.FirstName + "&nbsp;" + item.LastName + "</a></h2></span>");
                        sb.Append("<span class=\"clear\"></span></div></li>");
                    }
                }
                else if (pageindex == 1)
                {
                    sb.Append("<center>No Record Found</center>");
                }
                else
                {
                    sb.Append("<center>No More Record Found</center>");
                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }
        public ActionResult PartialFamilyMembers(int PatientId)
        {
            try
            {
                List<FamilyReleation> FamilyDetails = new List<FamilyReleation>();
                List<FamilyReleationHistory> ReleationHistory = new List<FamilyReleationHistory>();
                MdlPatient = new mdlPatient();
                FamilyDetails = MdlPatient.GetFamilyDetails().ToList();
                ReleationHistory = MdlPatient.GetFamilyHistoryDetails(PatientId).Where(p => p.ReletaiveId == (int)EnumFamilyRelation.Father || p.ReletaiveId == (int)EnumFamilyRelation.Mother || p.ReletaiveId == (int)EnumFamilyRelation.Husband || p.ReletaiveId == (int)EnumFamilyRelation.Wife).ToList();
                var familylist =
                (from FD in FamilyDetails
                 where !ReleationHistory.Any(x => x.ReletaiveId == FD.RelativeId)
                 select FD).ToList();
                return PartialView("PartialFamilyMembers", familylist.OrderBy(p => p.RelativeId));
            }
            catch (Exception)
            {
                throw;
            }

        }
        [HttpPost]
        public JsonResult SaveFamilyDetails(int PatientId, int Selectedpatient, int RelativeId, string Gender)
        {
            try
            {
                int status = 0;
                MdlPatient = new mdlPatient();
                if (MdlPatient.CheckReleationIsExist(PatientId, Selectedpatient) > 0)
                {
                    status = (int)FamilyStatus.IsExist;
                }
                else
                {
                    status = MdlPatient.InsertFamilyRelationDetails(PatientId, Selectedpatient, RelativeId) == true ? (int)FamilyStatus.IsSuccess : (int)FamilyStatus.IsFailed;
                    if (status > 0)
                    {
                        status = 0;
                        int CrossRelativeId = MdlPatient.GetCrossReletiveId(RelativeId, Gender);
                        status = MdlPatient.InsertFamilyRelationDetails(Selectedpatient, PatientId, CrossRelativeId) == true ? (int)FamilyStatus.IsSuccess : (int)FamilyStatus.IsFailed;

                    }

                }


                return Json(status);
            }
            catch (Exception)
            {
                throw;
            }

        }

        public ActionResult PartialReleationHistory(int PatientId)
        {
            try
            {
                List<FamilyReleationHistory> ReleationHistory = new List<FamilyReleationHistory>();
                MdlPatient = new mdlPatient();
                DataTable ds = new DataTable();
                ReleationHistory = MdlPatient.GetFamilyHistoryDetails(PatientId);
                return PartialView("_PartialRelationHistory", ReleationHistory);
            }
            catch (Exception)
            {
                throw;
            }

        }
        [HttpPost]
        public JsonResult DeleteReleation(int PatientId, int ToPatientId)
        {
            try
            {
                int status = 0;
                MdlPatient = new mdlPatient();
                status = MdlPatient.DeleteRelation(PatientId, ToPatientId) == true ? (int)FamilyStatus.IsSuccess : (int)FamilyStatus.IsFailed;
                if (status > 0)
                {
                    status = 0;
                    status = MdlPatient.DeleteRelation(ToPatientId, PatientId) == true ? (int)FamilyStatus.IsSuccess : (int)FamilyStatus.IsFailed;

                }
                return Json(status);
            }
            catch (Exception)
            {
                throw;
            }

        }

        #region 
        public List<ColleaguesMessagesOfDoctor> GetCommunicationHistoryForPatient(int UserId, int PatientId, int PageIndex, int PageSize)
        {
            MdlColleagues = new mdlColleagues();
            List<ColleaguesMessagesOfDoctor> objColleaguesMessagesDoctorList = new List<ColleaguesMessagesOfDoctor>();
            objColleaguesMessagesDoctorList = MdlColleagues.GetCommunicationHistoryForPatient(UserId, PatientId, PageIndex, PageSize, 1, 2, SessionManagement.TimeZoneSystemName);
            return objColleaguesMessagesDoctorList;
        }
        #endregion

        public ActionResult PatientHistory(int PatientId = 0, string Isfrom = "")
        {
            string EncryptesPId = null;
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                if (Request.Form["hdnViewPatientHistory"] != null)
                {
                    string Pid = Request["hdnViewPatientHistory"].ToString() + "|" + "true";
                    PatientId = Convert.ToInt32(Request["hdnViewPatientHistory"].ToString());
                    EncryptesPId = ObjTripleDESCryptoHelper.encryptText(Pid);
                    TempData["CurrentPatientId"] = PatientId;
                    TempData["Identify"] = "Patients";
                }
                if (Request.Form["hdnPatientHistory"] != null)
                {
                    string Pid = Request["hdnPatientHistory"].ToString() + "|" + "true";
                    PatientId = Convert.ToInt32(Request["hdnPatientHistory"].ToString());
                    EncryptesPId = ObjTripleDESCryptoHelper.encryptText(Pid);
                    TempData["CurrentPatientId"] = PatientId;
                    TempData["Identify"] = "Patients";
                }
                if (Request["hdnAttachPatientId"] != null)
                {
                    string Pid = Request["hdnAttachPatientId"].ToString() + "|" + "false";
                    EncryptesPId = ObjTripleDESCryptoHelper.encryptText(Pid);
                    PatientId = Convert.ToInt32(Request["hdnAttachPatientId"]);
                    TempData["AttachPatient"] = PatientId;
                    TempData.Keep("AttachPatient");
                    TempData["Identify"] = "Inbox";
                }
                if (Convert.ToString(TempData["Identify"]) == ("Patients"))
                {
                    PatientId = Convert.ToInt32(TempData["CurrentPatientId"]);
                    TempData["CurrentPatientId"] = PatientId;
                    string Pid = PatientId.ToString() + "|" + "true";
                    EncryptesPId = ObjTripleDESCryptoHelper.encryptText(Pid);
                    TempData["Identify"] = "Patients";
                }
                if(Convert.ToString(TempData["OCR"]) == "PatientHistory")
                {
                    TempData["AttachPatient"] = PatientId;
                }
                else
                {
                    //priya 
                    if (Convert.ToString(TempData["Identify"]) != ("Patients/AddPatient"))
                    {
                        //RM-283 Changes for RM-283
                        if (Isfrom == "mdf")
                        {
                            TempData.Remove("AttachPatient");
                        }
                        //else if (!string.IsNullOrEmpty(Convert.ToString(TempData["AttachPatient"])))
                        //{
                        //    PatientId = Convert.ToInt32(TempData["AttachPatient"]);
                        //}
                        TempData["AttachPatient"] = PatientId;
                        string Pid = PatientId.ToString() + "|" + "false";
                        MdlPatient.IsAuthorize = "false";
                        MdlPatient.EncryptesPId = ObjTripleDESCryptoHelper.encryptText(Pid);
                        TempData["Identify"] = "Inbox";
                    }
                    else
                    {
                        if (Convert.ToString(TempData["Identify"]) == ("Patients/AddPatient"))
                        {
                            TempData["PatientId"] = PatientId;
                        }
                    }
                }

                // used for print all document method.
                TempData["PatientId"] = PatientId;
                TempData["PatientHistoryPatientId"] = PatientId;
                MdlColleagues = new mdlColleagues();
                DataSet ds = new DataSet();
                ds = ObjPatientsData.GetPateintDetailsByPatientId(PatientId, Convert.ToInt32(SessionManagement.UserId));
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    PatientHistory objPatientHistory = new PatientHistory();
                    DataTable dtTable = ds.Tables[0];
                    List<PatientHistory> _patientHistory = objPatientMethodCall.GetPatientHistorys(dtTable, SessionManagement.TimeZoneSystemName);
                    //List<PatientHistory> _patientHistory = dtTable.DataTableToList<PatientHistory>();
                    objPatientHistory = _patientHistory.FirstOrDefault();
                    string DOB = objPatientHistory.DateOfBirth;
                    if (!string.IsNullOrEmpty(DOB))
                    {
                        DateTime? data = Convert.ToDateTime(DOB);
                        TimeSpan timeofDay = data.Value.TimeOfDay;
                        if (timeofDay != TimeSpan.Zero)
                        {
                            DOB = DOB != string.Empty ? Convert.ToString(DataAccessLayer.Common.clsHelper.ConvertFromUTC(Convert.ToDateTime(DOB), SessionManagement.TimeZoneSystemName)) : string.Empty;
                        }
                    }
                    objPatientHistory.DateOfBirth = DOB;
                    objPatientHistory.EncryptesPId = EncryptesPId;
                    if (objPatientHistory.Gender != null && objPatientHistory.Gender == "Female")
                    {
                        objPatientHistory.ImageName =
                            objPatientHistory.ProfileImage == "" ?
                            ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"]), null) :
                            ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["ImageBank"]), null) + objPatientHistory.ProfileImage;

                    }
                    else
                    {
                        objPatientHistory.ImageName =
                             objPatientHistory.ProfileImage == "" ?
                            ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), null) :
                            ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["ImageBank"]), null) +
                            objPatientHistory.ProfileImage;
                    }

                    objPatientHistory.lstColleaguesMessagesDoctor = MdlColleagues.GetCommunicationHistoryForPatient(Convert.ToInt32(Session["UserId"]), PatientId, 1, 20, 1, 2, SessionManagement.TimeZoneSystemName);
                    if (objPatientHistory.IsAuthorize != "false")
                    {
                        objPatientHistory.lstUplodedImages = GetPatientImagesAll(PatientId);
                        objPatientHistory.lstUplodedDocuments = GetAllPatientDocument(PatientId);
                    }
                    ViewBag.ColleagueListForSendReferralPopUp = GetColleagueListForSendReferralPopUp(null);
                    //ViewBag.state = StateScriptNew("", "0");
                    ViewBag.state = StateScriptNew("", Convert.ToString(objPatientHistory.State));
                    return View("oldPatientDetails", objPatientHistory);
                }
                else
                {
                    TempData["ErrorMessage"] = "Invalid Patient you are trying to access.";
                    return RedirectToAction("Index", "Patients");
                }
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }



        public List<UplodedImages> GetPatientImagesAll(int PatientId)
        {
            List<UplodedImages> objuplodedImageList = new List<UplodedImages>();
            int? TempOwnerID = Convert.ToInt32(SessionManagement.UserId);
            //objuplodedImageList = ObjPatientsData.GetPatientImages(PatientId, (TempOwnerID == 0 ? null : TempOwnerID)).DataTableToList<UplodedImages>();
            DataTable dt = ObjPatientsData.GetPatientImages(PatientId, (TempOwnerID == 0 ? null : TempOwnerID));
            if (dt != null && dt.Rows.Count > 0)
            {
                objuplodedImageList = objPatientMethodCall.PatientImages(dt, SessionManagement.TimeZoneSystemName);
            }
            return objuplodedImageList;


        }
        public ActionResult GetAllPatientImagesByID(int PatientId)
        {
            List<UplodedImages> objuplodedImageList = new List<UplodedImages>();
            objuplodedImageList = GetPatientImagesAll(PatientId);
            return PartialView("_uplodedImages", objuplodedImageList);
            //int? TempOwnerID = Convert.ToInt32(SessionManagement.UserId);
            //objuplodedImageList = ObjPatientsData.GetPatientImages(PatientId, (TempOwnerID == 0 ? null : TempOwnerID)).DataTableToList<UplodedImages>();
            //DataTable dt = ObjPatientsData.GetPatientImages(PatientId, (TempOwnerID == 0 ? null : TempOwnerID));
            //if (dt != null && dt.Rows.Count > 0)
            //{
            //    objuplodedImageList = (from p in dt.AsEnumerable()
            //                           select new UplodedImages
            //                           {
            //                               ImageId = Convert.ToInt32(p["ImageId"]),
            //                               PatientId = Convert.ToInt32(p["PatientId"]),
            //                               Name = Convert.ToString(p["Name"]),
            //                               ImagePath = Convert.ToString(p["FullPath"]),
            //                               RelativePath = Convert.ToString(p["RelativePath"]),
            //                               CreationDate = Convert.ToString(p["CreationDate"]),
            //                               LastModifiedDate = Convert.ToString(p["LastModifiedDate"]),
            //                               MontageId = Convert.ToInt32(p["MontageId"]),
            //                               Timepoint = Convert.ToInt32(p["Timepoint"]),
            //                               FullPath = Convert.ToString(p["FullPath"]),
            //                               Height = Convert.ToInt32(p["Height"]),
            //                               Weight = Convert.ToInt32(p["Width"]),
            //                               ImageTypeId = Convert.ToInt32(p["ImageType"]),
            //                               ImageFormat = Convert.ToString(p["ImageFormat"]),
            //                               Description = Convert.ToString(p["Description"]),
            //                               Uploadby = Convert.ToString(p["Uploadby"]),
            //                               ImageStatus = Convert.ToBoolean(p["ImageStatus"]),
            //                               ImagePosition = Convert.ToInt32(p["ImagePosition"]),
            //                               ImageTypeName = Convert.ToString(p["ImageTypeName"]),
            //                               DateForApi = Convert.ToString(p["DateForApi"]),
            //                               DocumentName = string.Empty,
            //                               DocumentId = 0,
            //                           }).ToList();
            //}


        }
        public List<UplodedImages> GetAllPatientDocument(int PatientId)
        {
            List<UplodedImages> objuplodedImageList = new List<UplodedImages>();
            int? TempOwnerID = Convert.ToInt32(SessionManagement.UserId);
            //objuplodedImageList = ObjPatientsData.GetPatientDocumentsAll(PatientId, (TempOwnerID == 0 ? null : TempOwnerID)).DataTableToList<UplodedImages>();
            DataTable dt = ObjPatientsData.GetPatientDocumentsAll(PatientId, (TempOwnerID == 0 ? null : TempOwnerID));
            if (dt != null && dt.Rows.Count > 0)
            {
                objuplodedImageList = objPatientMethodCall.PatientDocument(dt, SessionManagement.TimeZoneSystemName);
            }
            return objuplodedImageList;
        }
        public ActionResult GetAllPatientDocumentByID(int PatientId)
        {
            List<UplodedImages> objuplodedImageList = new List<UplodedImages>();
            //int? TempOwnerID = Convert.ToInt32(SessionManagement.UserId);
            //objuplodedImageList = ObjPatientsData.GetPatientDocumentsAll(PatientId, (TempOwnerID == 0 ? null : TempOwnerID)).DataTableToList<UplodedImages>();
            objuplodedImageList = GetAllPatientDocument(PatientId);
            return PartialView("_uplodedImages", objuplodedImageList);
        }

        public ActionResult LoadPatientscrolling(int PatientId, int PageIndex, int SortColumn, int SortDirection)
        {
            MdlColleagues = new mdlColleagues();
            int pagesize = 20;
            int UserId = Convert.ToInt32(Session["UserId"]);
            List<ColleaguesMessagesOfDoctor> objColleaguesMessagesDoctorList = new List<ColleaguesMessagesOfDoctor>();
            objColleaguesMessagesDoctorList = MdlColleagues.GetCommunicationHistoryForPatient(UserId, PatientId, PageIndex, pagesize, SortColumn, SortDirection, SessionManagement.TimeZoneSystemName);
            return PartialView("_PartialCommunicationListPortion", objColleaguesMessagesDoctorList);
        }
        public ActionResult GetPatientHostoryCommunicationOnSorting(int PatientId, int PageIndex, int SortColumn, int SortDirection)
        {
            MdlColleagues = new mdlColleagues();
            int UserId = Convert.ToInt32(Session["UserId"]);
            List<ColleaguesMessagesOfDoctor> objColleaguesMessagesDoctorList = new List<ColleaguesMessagesOfDoctor>();
            objColleaguesMessagesDoctorList = MdlColleagues.GetCommunicationHistoryForPatient(UserId, PatientId, PageIndex, 20, SortColumn, SortDirection, SessionManagement.TimeZoneSystemName);
            return PartialView("_PartialCommunicationListPortion", objColleaguesMessagesDoctorList);
        }

        [HttpPost]
        public ActionResult FillNotesPopUpById(int PatientId, int Id, string Type)
        {
            List<EditFiles> objFiles = new List<EditFiles>();
            EditFiles ObjFile = new EditFiles();
            DataTable dt = new DataTable();
            if (Type == "Image")
            {
                dt = ObjPatientsData.GetDetailByImageId(Id);
                objFiles = objPatientMethodCall.GetImageFileDetailById(dt, Type, SessionManagement.TimeZoneSystemName);
            }
            else
            {
                dt = ObjPatientsData.GetDescriptionofDocumentById(PatientId, Id);
                objFiles = objPatientMethodCall.GetImageFileDetailById(dt, Type, SessionManagement.TimeZoneSystemName);
            }

            //objFiles = dt.DataTableToList<EditFiles>();
            if (objFiles.Count > 0)
            {
                ObjFile = objFiles.FirstOrDefault();
            }
            return PartialView("_patientFileEdit", ObjFile);
        }

        public ActionResult PatientComposeMessage(int PatientId = 0)
        {
            BO.ViewModel.ComposeMessage objcomposedetails = new BO.ViewModel.ComposeMessage();
            if (PatientId > 0)
            {
                MessagesBLL objMessageBLL = new MessagesBLL();
                objcomposedetails.PatientId = Convert.ToString(PatientId);
                objcomposedetails.PatientList = objMessageBLL.GetPatientList(Convert.ToInt32(SessionManagement.UserId), null, objcomposedetails.PatientId);
            }
            TempData["objcomposedetails"] = objcomposedetails;
            return RedirectToAction("PatientMessageCompose", "Patients");
        }
        [CustomAuthorize]
        public ActionResult PatientMessageCompose()
        {
            BO.ViewModel.ComposeMessage objcomposedetails = new BO.ViewModel.ComposeMessage();
            if (TempData["objcomposedetails"] != null)
            {
                TempData.Keep();
                objcomposedetails = (BO.ViewModel.ComposeMessage)TempData["objcomposedetails"];
            }
            return View(objcomposedetails);
        }
        [CustomAuthorize]
        [HttpPost]
        public ActionResult PatientMessageCompose(BO.ViewModel.ComposeMessage objcomposedetails)
        {
            MessagesBLL objMessageBLL = new MessagesBLL();
            objcomposedetails.PatientId = Convert.ToString(objcomposedetails.PatientId);
            objcomposedetails.PatientList = objMessageBLL.GetPatientList(Convert.ToInt32(SessionManagement.UserId), null, objcomposedetails.PatientId);
            TempData["objcomposedetails"] = objcomposedetails;
            return RedirectToAction("PatientMessageCompose", "Patients");
        }
        [CustomAuthorize]
        public ActionResult PartialPatientComposeMessage(BO.ViewModel.ComposeMessage objcomposedetails)
        {
            switch (objcomposedetails.MessageTypeId)
            {
                case (int)BO.Enums.Common.ComposeMessageType.Replay:
                    ViewBag.Title = "Reply";
                    MessagesBLL Message = new MessagesBLL();
                    objcomposedetails = Message.GetPatientCommopseMessgeDetails(objcomposedetails.MessageId, (int)BO.Enums.Common.ComposeMessageType.Replay, Convert.ToInt32(SessionManagement.UserId));
                    TempData["objcomposedetails"] = objcomposedetails;
                    break;
                case (int)BO.Enums.Common.ComposeMessageType.Compose:
                    ViewBag.Title = "Compose Message";
                    objcomposedetails.ComposeType = (int)BO.Enums.Common.ComposeType.PatientCompose;
                    break;
                case (int)BO.Enums.Common.ComposeMessageType.Draft:
                    ViewBag.Title = "Compose Message";
                    objcomposedetails.ComposeType = (int)BO.Enums.Common.ComposeType.PatientCompose;
                    break;


            }
            return View("PartialPatientComposeMessage", objcomposedetails);
        }
        [CustomAuthorize]
        public JsonResult SaveAsDraft(BO.ViewModel.ComposeMessage objcomposedetail)
        {
            MessagesBLL objmessagebll = new MessagesBLL();
            CommonBLL ObjCommonbll = new CommonBLL();
            int RecordId = 0;
            objcomposedetail.ComposeType = (int)BO.Enums.Common.ComposeType.PatientCompose;
            RecordId = objmessagebll.InsertUpdateDraftMessage(objcomposedetail, Convert.ToInt32(SessionManagement.UserId));
            List<BO.ViewModel.TempFileAttacheMents> objlistofattachements = new List<BO.ViewModel.TempFileAttacheMents>();
            objlistofattachements = ObjCommonbll.GetTempAttachments(objcomposedetail.FileIds);
            foreach (var item in objlistofattachements)
            {
                if (System.IO.File.Exists(NewTempFileUpload + item.FilePath))
                {
                    System.IO.File.Move(NewTempFileUpload + item.FilePath, DraftFileUpload + item.FilePath);
                    objmessagebll.InsertDraftAttachments(RecordId, item.FilePath, Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item.FileExtension), (int)BO.Enums.Common.ComposeType.PatientCompose);
                }

            }
            ObjCommonbll.DeleteAllAttachements(objcomposedetail.FileIds);
            return Json(RecordId > 0 ? true : false, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorize]
        public JsonResult ForwardSaveAsDraft(BO.ViewModel.ComposeMessage objcomposedetail)
        {
            MessagesBLL objmessagebll = new MessagesBLL();
            CommonBLL ObjCommonbll = new CommonBLL();
            int RecordId = 0;
            objcomposedetail.ComposeType = (int)BO.Enums.Common.ComposeType.PatientCompose;
            objcomposedetail.MessageId = 0;
            RecordId = objmessagebll.InsertUpdateDraftMessage(objcomposedetail, Convert.ToInt32(SessionManagement.UserId));
            List<BO.ViewModel.TempFileAttacheMents> objlistofattachements = new List<BO.ViewModel.TempFileAttacheMents>();
            objlistofattachements = ObjCommonbll.GetTempAttachments(objcomposedetail.FileIds);
            foreach (var item in objlistofattachements)
            {
                if (System.IO.File.Exists(NewTempFileUpload + item.FilePath))
                {
                    System.IO.File.Move(NewTempFileUpload + item.FilePath, DraftFileUpload + item.FilePath);
                    objmessagebll.InsertDraftAttachments(RecordId, item.FilePath, Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item.FileExtension), (int)BO.Enums.Common.ComposeType.PatientCompose);
                }

            }
            List<BO.ViewModel.TempFileAttacheMents> objlistofforwardattachements = new List<BO.ViewModel.TempFileAttacheMents>();
            objlistofforwardattachements = ObjCommonbll.GetForwardAttachMentsById(objcomposedetail.ForwardFileId == null ? string.Empty : objcomposedetail.ForwardFileId, (int)BO.Enums.Common.ComposeType.PatientCompose);
            foreach (var item in objlistofforwardattachements)
            {
                string NewFileName = Convert.ToString(item.FileName);
                NewFileName = NewFileName.Substring(NewFileName.ToString().LastIndexOf("$") + 1);
                NewFileName = Convert.ToString("$" + System.DateTime.Now.Ticks + "$" + NewFileName);
                if (System.IO.File.Exists(ImageBankFileUpload + item.FilePath))
                {
                    if (!new CommonBLL().CheckFileIsValidImage(item.FilePath))
                    {
                        item.FileExtension = 1;
                    }
                    else
                    {
                        item.FileExtension = 0;
                    }
                    System.IO.File.Copy(ImageBankFileUpload + item.FilePath, DraftFileUpload + NewFileName);
                    objmessagebll.InsertDraftAttachments(RecordId, NewFileName, Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item.FileExtension), (int)BO.Enums.Common.ComposeType.PatientCompose);
                }

            }
            ObjCommonbll.DeleteAllAttachements(objcomposedetail.FileIds);
            return Json(RecordId > 0 ? true : false, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorize]
        public JsonResult SendMessageToPatient(BO.ViewModel.ComposeMessage objcomposedetail)
        {
            bool Result = false;
            object obj = string.Empty;
            string NotEmail = string.Empty;
            MessagesBLL objmessagebll = new MessagesBLL();
            CommonBLL ObjCommonbll = new CommonBLL();
            List<BO.ViewModel.TempFileAttacheMents> objlistofattachements = new List<BO.ViewModel.TempFileAttacheMents>();
            objlistofattachements = ObjCommonbll.GetTempAttachments(objcomposedetail.FileIds, objcomposedetail.MessageId, (int)BO.Enums.Common.ComposeType.PatientCompose);
            BO.ViewModel.DoctorDetail objDoctordetail = objmessagebll.GetDoctorDetail(Convert.ToInt32(SessionManagement.UserId));
            string[] PatientIds = objcomposedetail.PatientId.ToString().Split(new char[] { ',' });
            if (PatientIds.Length > 0)
            {
                if (objcomposedetail.MessageId > 0)
                {
                    switch (objcomposedetail.MessageTypeId)
                    {
                        case (int)BO.Enums.Common.ComposeMessageType.Draft:
                            int i = 0;
                            foreach (var item in PatientIds)
                            {
                                int RecordId = 0;
                                BO.ViewModel.PatientDetail objpatientdetail = objmessagebll.GetPatientDetail(Convert.ToInt32(item), Convert.ToInt32(SessionManagement.UserId));
                                if (string.IsNullOrWhiteSpace(Convert.ToString(objpatientdetail.Email)))
                                {
                                    NotEmail += objpatientdetail.PatientName + "<br/>";
                                }
                                if (i == 0)
                                {
                                    i++;
                                    RecordId = objmessagebll.InsertUpdatePatientMessages(objDoctordetail.DoctorName, objpatientdetail.PatientName, "Subject", objcomposedetail.MessageBody, Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item), false, true, false, objcomposedetail.MessageId);
                                    ObjTemplate.SendMessageToPatient(Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item), objcomposedetail.MessageBody, RecordId);
                                    ManageAttachement(objlistofattachements, RecordId, Convert.ToInt32(item));
                                    Result = true;
                                }
                                else
                                {
                                    RecordId = objmessagebll.InsertUpdatePatientMessages(objDoctordetail.DoctorName, objpatientdetail.PatientName, "Subject", objcomposedetail.MessageBody, Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item), false, true, false);
                                    ObjTemplate.SendMessageToPatient(Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item), objcomposedetail.MessageBody, RecordId);
                                    ManageAttachement(objlistofattachements, RecordId, Convert.ToInt32(item));
                                    Result = true;
                                }

                            }
                            break;
                        case (int)BO.Enums.Common.ComposeMessageType.Replay:
                            foreach (var item in PatientIds)
                            {
                                int RecordId = 0;
                                BO.ViewModel.PatientDetail objpatientdetail = objmessagebll.GetPatientDetail(Convert.ToInt32(item), Convert.ToInt32(SessionManagement.UserId));
                                if (string.IsNullOrWhiteSpace(Convert.ToString(objpatientdetail.Email)))
                                {
                                    NotEmail += objpatientdetail.PatientName + "<br/>";
                                }
                                RecordId = objmessagebll.InsertUpdatePatientMessages(objDoctordetail.DoctorName, objpatientdetail.PatientName, "Subject", objcomposedetail.MessageBody, Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item), false, true, false);
                                objmessagebll.ManageReplyForwardMessage(objcomposedetail.MessageId, RecordId, (int)BO.Enums.Common.ComposeMessageType.Replay, (int)BO.Enums.Common.ComposeType.PatientCompose);
                                ObjTemplate.SendMessageToPatient(Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item), objcomposedetail.MessageBody, RecordId);
                                ManageAttachement(objlistofattachements, RecordId, Convert.ToInt32(item));
                                Result = true;
                            }
                            break;

                    }
                }
                else
                {
                    foreach (var item in PatientIds)
                    {
                        int RecordId = 0;
                        BO.ViewModel.PatientDetail objpatientdetail = objmessagebll.GetPatientDetail(Convert.ToInt32(item), Convert.ToInt32(SessionManagement.UserId));
                        if (string.IsNullOrWhiteSpace(Convert.ToString(objpatientdetail.Email)))
                        {
                            NotEmail += objpatientdetail.PatientName + "<br/>";
                        }
                        RecordId = objmessagebll.InsertUpdatePatientMessages(objDoctordetail.DoctorName, objpatientdetail.PatientName, "Subject", objcomposedetail.MessageBody, Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item), false, true, false);
                        ObjTemplate.SendMessageToPatient(Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item), objcomposedetail.MessageBody, RecordId);
                        ManageAttachement(objlistofattachements, RecordId, Convert.ToInt32(item));
                        Result = true;
                    }
                }
                ObjCommonbll.DeleteAllAttachements(objcomposedetail.FileIds, objcomposedetail.MessageId);
                DeleteAttachmentFiles(objlistofattachements);
            }
            if (!string.IsNullOrWhiteSpace(NotEmail))
            {
                obj = NotEmail;
            }
            else
            {
                obj = "1";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        [CustomAuthorize]
        public JsonResult ForwardMessageToPatient(BO.ViewModel.ComposeMessage objcomposedetail)
        {
            bool Result = false;
            object obj = string.Empty;
            string NotEmail = string.Empty;
            MessagesBLL objmessagebll = new MessagesBLL();
            CommonBLL ObjCommonbll = new CommonBLL();
            List<BO.ViewModel.TempFileAttacheMents> objlistofattachements = new List<BO.ViewModel.TempFileAttacheMents>();
            objlistofattachements = ObjCommonbll.GetTempAttachments(objcomposedetail.FileIds, 0, (int)BO.Enums.Common.ComposeType.PatientCompose);
            List<BO.ViewModel.TempFileAttacheMents> objlistofforwardattachements = new List<BO.ViewModel.TempFileAttacheMents>();
            objlistofforwardattachements = ObjCommonbll.GetForwardAttachMentsById(objcomposedetail.ForwardFileId == null ? string.Empty : objcomposedetail.ForwardFileId, (int)BO.Enums.Common.ComposeType.PatientCompose);
            BO.ViewModel.DoctorDetail objDoctordetail = objmessagebll.GetDoctorDetail(Convert.ToInt32(SessionManagement.UserId));
            string[] PatientIds = objcomposedetail.PatientId.ToString().Split(new char[] { ',' });
            if (PatientIds.Length > 0)
            {
                if (objcomposedetail.MessageId > 0)
                {
                    switch (objcomposedetail.MessageTypeId)
                    {
                        case (int)BO.Enums.Common.ComposeMessageType.Forward:

                            foreach (var item in PatientIds)
                            {
                                int RecordId = 0;
                                BO.ViewModel.PatientDetail objpatientdetail = objmessagebll.GetPatientDetail(Convert.ToInt32(item), Convert.ToInt32(SessionManagement.UserId));
                                if (string.IsNullOrWhiteSpace(Convert.ToString(objpatientdetail.Email)))
                                {
                                    NotEmail += objpatientdetail.PatientName + "<br/>";
                                }
                                RecordId = objmessagebll.InsertUpdatePatientMessages(objDoctordetail.DoctorName, objpatientdetail.PatientName, "Subject", objcomposedetail.MessageBody, Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item), false, true, false);
                                objmessagebll.ManageReplyForwardMessage(objcomposedetail.MessageId, RecordId, (int)BO.Enums.Common.ComposeMessageType.Forward, (int)BO.Enums.Common.ComposeType.PatientCompose);
                                ObjTemplate.SendMessageToPatient(Convert.ToInt32(SessionManagement.UserId), Convert.ToInt32(item), objcomposedetail.MessageBody, RecordId);
                                ManageAttachement(objlistofattachements, RecordId, Convert.ToInt32(item));
                                ManageForwardAttachement(objlistofforwardattachements, RecordId, Convert.ToInt32(item));
                                Result = true;


                            }
                            break;
                    }
                }

                ObjCommonbll.DeleteAllAttachements(objcomposedetail.FileIds, objcomposedetail.MessageId);
                DeleteAttachmentFiles(objlistofattachements);
            }
            if (!string.IsNullOrWhiteSpace(NotEmail))
            {
                obj = NotEmail;
            }
            else
            {
                obj = "1";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public bool ManageAttachement(List<BO.ViewModel.TempFileAttacheMents> objlistofattachements, int RecordId = 0, int PatientId = 0)
        {
            CommonBLL ObjCommonbll = new CommonBLL();
            MessagesBLL objmessagebll = new MessagesBLL();
            bool Result = false;
            if (objlistofattachements.Count > 0 && RecordId > 0)
            {
                foreach (var item in objlistofattachements)
                {
                    string NewFileName = Convert.ToString(item.FilePath);
                    NewFileName = NewFileName.Substring(NewFileName.ToString().LastIndexOf("$") + 1);
                    NewFileName = Convert.ToString("$" + System.DateTime.Now.Ticks + "$" + NewFileName);
                    //string MontageName = PatientId + "Montage";
                    //int MontageId = Convert.ToInt32(objmessagebll.AddMontage(MontageName, 0, Convert.ToInt32(PatientId), Convert.ToInt32(SessionManagement.UserId), 0));
                    switch (item.FileFrom)
                    {
                        case (int)BO.Enums.Common.FileFromFolder.Draft_DraftAttachments:

                            switch (item.FileExtension)
                            {
                                case (int)BO.Enums.Common.FileTypeExtension.File:
                                    if (System.IO.File.Exists(DraftFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Copy(DraftFileUpload + item.FilePath, ImageBankFileUpload + NewFileName);
                                        //int Docstatus = objmessagebll.AddDocumenttoMontage(MontageId, NewFileName, "Doctor");
                                        objmessagebll.InsertMessageAttachemntsForPatient(RecordId, NewFileName);
                                        Result = true;
                                    }
                                    break;
                                case (int)BO.Enums.Common.FileTypeExtension.Image:
                                    if (System.IO.File.Exists(DraftFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Copy(DraftFileUpload + item.FilePath, ImageBankFileUpload + NewFileName);
                                        using (Image image = Image.FromFile(DraftFileUpload + item.FilePath))
                                        {
                                            Size thumbnailSize = ObjCommon.GetThumbnailSize(image);
                                            using (System.Drawing.Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width, thumbnailSize.Height, null, IntPtr.Zero))
                                            {

                                                thumbnail.Save(ThumbFileUpload + NewFileName);
                                            }
                                        }
                                        int ImageId = objmessagebll.AddImagetoPatient(Convert.ToInt32(PatientId), 1, NewFileName, 1000, 1000, 0, 500, DraftFileUpload + item.FilePath, ImageBankFileUpload + NewFileName, Convert.ToInt32(Session["UserId"]), 0, 1, "Patient");
                                        //if (ImageId > 0)
                                        //{
                                        //    objmessagebll.AddImagetoMontage(ImageId, 500, MontageId);
                                        //}
                                        objmessagebll.InsertMessageAttachemntsForPatient(RecordId, NewFileName);
                                        Result = true;
                                    }
                                    break;
                            }
                            break;
                        case (int)BO.Enums.Common.FileFromFolder.TempFile_TempFolder:
                        case (int)BO.Enums.Common.FileFromFolder.TempImage_TempFolder:
                            switch (item.FileExtension)
                            {
                                case (int)BO.Enums.Common.FileTypeExtension.File:
                                    if (System.IO.File.Exists(NewTempFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Copy(NewTempFileUpload + item.FilePath, ImageBankFileUpload + NewFileName);
                                        //int Docstatus = objmessagebll.AddDocumenttoMontage(MontageId, NewFileName, "Doctor");
                                        objmessagebll.InsertMessageAttachemntsForPatient(RecordId, NewFileName);
                                        Result = true;
                                    }
                                    break;
                                case (int)BO.Enums.Common.FileTypeExtension.Image:
                                    if (System.IO.File.Exists(NewTempFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Copy(NewTempFileUpload + item.FilePath, ImageBankFileUpload + NewFileName);
                                        using (Image image = Image.FromFile(NewTempFileUpload + item.FilePath))
                                        {
                                            Size thumbnailSize = ObjCommon.GetThumbnailSize(image);
                                            using (System.Drawing.Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width, thumbnailSize.Height, null, IntPtr.Zero))
                                            {
                                                thumbnail.Save(ThumbFileUpload + NewFileName);
                                            }
                                        }
                                        int ImageId = objmessagebll.AddImagetoPatient(Convert.ToInt32(PatientId), 1, NewFileName, 1000, 1000, 0, 500, DraftFileUpload + item.FilePath, ImageBankFileUpload + NewFileName, Convert.ToInt32(Session["UserId"]), 0, 1, "Patient");
                                        //if (ImageId > 0)
                                        //{
                                        //    objmessagebll.AddImagetoMontage(ImageId, 500, MontageId);
                                        //}
                                        objmessagebll.InsertMessageAttachemntsForPatient(RecordId, NewFileName);
                                        Result = true;
                                    }
                                    break;
                            }

                            break;
                    }
                }
            }
            return Result;
        }
        public bool ManageForwardAttachement(List<BO.ViewModel.TempFileAttacheMents> objlistofattachements, int RecordId = 0, int PatientId = 0)
        {
            CommonBLL ObjCommonbll = new CommonBLL();
            MessagesBLL objmessagebll = new MessagesBLL();
            bool Result = false;
            if (objlistofattachements.Count > 0 && RecordId > 0)
            {
                foreach (var item in objlistofattachements)
                {
                    string MontageName = PatientId + "Montage";
                    // int MontageId = Convert.ToInt32(objmessagebll.AddMontage(MontageName, 0, Convert.ToInt32(PatientId), Convert.ToInt32(SessionManagement.UserId), 0));
                    if (!new CommonBLL().CheckFileIsValidImage(item.FilePath))
                    {
                        item.FileExtension = 1;
                    }
                    else
                    {
                        item.FileExtension = 0;
                    }
                    switch (item.FileExtension)
                    {

                        case (int)BO.Enums.Common.FileTypeExtension.File:
                            if (System.IO.File.Exists(Server.MapPath(ImageBankFolder + item.FilePath)))
                            {
                                //int Docstatus = objmessagebll.AddDocumenttoMontage(MontageId, item.FilePath, "Doctor");
                                objmessagebll.InsertMessageAttachemntsForPatient(RecordId, item.FilePath);
                                Result = true;
                            }
                            break;
                        case (int)BO.Enums.Common.FileTypeExtension.Image:
                            if (System.IO.File.Exists(Server.MapPath(ImageBankFolder + item.FilePath)))
                            {

                                int ImageId = objmessagebll.AddImagetoPatient(Convert.ToInt32(PatientId), 1, item.FilePath, 1000, 1000, 0, 500, (DraftFileUpload + item.FilePath), ImageBankFileUpload + item.FilePath, Convert.ToInt32(Session["UserId"]), 0, 1, "Patient");
                                if (ImageId > 0)
                                {
                                    ////  objmessagebll.AddImagetoMontage(ImageId, 500, MontageId);
                                }
                                objmessagebll.InsertMessageAttachemntsForPatient(RecordId, item.FilePath);
                                Result = true;
                            }
                            break;
                    }

                }
            }
            return Result;
        }
        public bool DeleteAttachmentFiles(List<BO.ViewModel.TempFileAttacheMents> objlistofattachements)
        {
            bool Result = false;
            if (objlistofattachements.Count > 0)
            {
                foreach (var item in objlistofattachements)
                {
                    switch (item.FileFrom)
                    {
                        case (int)BO.Enums.Common.FileFromFolder.Draft_DraftAttachments:
                            switch (item.FileExtension)
                            {
                                case (int)BO.Enums.Common.FileTypeExtension.File:
                                    if (System.IO.File.Exists(DraftFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Delete(DraftFileUpload + item.FilePath);
                                        Result = true;
                                    }
                                    break;
                                case (int)BO.Enums.Common.FileTypeExtension.Image:
                                    if (System.IO.File.Exists(DraftFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Delete(DraftFileUpload + item.FilePath);
                                        Result = true;
                                    }
                                    break;
                            }
                            break;
                        case (int)BO.Enums.Common.FileFromFolder.TempFile_TempFolder:
                        case (int)BO.Enums.Common.FileFromFolder.TempImage_TempFolder:
                            switch (item.FileExtension)
                            {
                                case (int)BO.Enums.Common.FileTypeExtension.File:
                                    if (System.IO.File.Exists(NewTempFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Delete(NewTempFileUpload + item.FilePath);
                                        Result = true;
                                    }
                                    break;
                                case (int)BO.Enums.Common.FileTypeExtension.Image:
                                    if (System.IO.File.Exists(NewTempFileUpload + item.FilePath))
                                    {
                                        System.IO.File.Delete(NewTempFileUpload + item.FilePath);
                                        Result = true;
                                    }
                                    break;
                            }

                            break;
                    }
                }
            }
            return Result;
        }
        [CustomAuthorize]
        public ActionResult PartialPatientForwardMessage(BO.ViewModel.ComposeMessage objcomposedetails)
        {

            MessagesBLL Message = new MessagesBLL();
            ViewBag.Title = "Forward";
            objcomposedetails = Message.GetPatientCommopseMessgeDetails(objcomposedetails.MessageId, (int)BO.Enums.Common.ComposeMessageType.Forward, Convert.ToInt32(SessionManagement.UserId));
            TempData["objcomposedetails"] = objcomposedetails;
            return View("PartialPatientForwardMessage", objcomposedetails);
        }
        public ActionResult GetMessageBodyOfInboxReferrals(int MessageId, int MessageTypeId)
        {
            try
            {
                MdlPatient = new mdlPatient();
                ViewBag.MessageTypeId = MessageTypeId;
                MdlColleagues = new mdlColleagues();
                List<ReferralDetails> lstRefferalDetails = new List<ReferralDetails>();
                MdlColleagues.lstRefferalDetails = MdlPatient.GetRefferalDetailsById(Convert.ToInt32(Session["UserId"]), MessageId, "Inbox", SessionManagement.TimeZoneSystemName);
                ViewBag.ReferralCardId = MdlPatient.ReferralCardId;
                ViewBag.EndMessageTime = DateTime.Now;
                return View("GetMessageBodyOfInboxReferralsNew", MdlColleagues.lstRefferalDetails);

            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                throw;
            }
        }
        public ActionResult PartialViewForReferralcategoryvalues(int RefCardId)
        {
            try
            {
                MdlColleagues = new mdlColleagues();
                MdlColleagues.NewReferralView = MdlColleagues.GetNewReferralViewByRefCardId(Convert.ToInt32(RefCardId));
                return View("PartialNewReferral", MdlColleagues.NewReferralView);

            }
            catch (Exception EX)
            {
                ObjCommon.InsertErrorLog(Request.Url.ToString(), EX.Message, EX.StackTrace);
                throw;
            }

        }
        public bool DeleteMessageByMessageId(int MessageId, int MessageType)
        {
            bool Result = false;
            clsColleaguesData CLScolleagues = new clsColleaguesData();
            if (MessageType != 1)
            {
                Result = CLScolleagues.RemoveMessageFromInboxOfDoctor(Convert.ToInt32(SessionManagement.UserId), MessageId);
            }
            else
            {
                Result = CLScolleagues.RemoveMessageFromSentBoxOfDoctor(Convert.ToInt32(SessionManagement.UserId), MessageId);
            }

            return Result;
        }

        [HttpPost]
        public string getPatientDetailsById(int Id)
        {
            var PatientName = string.Empty;
            DataSet des = ObjPatientsData.GetPateintDetailsByPatientId(Id);
            if (des != null && des.Tables.Count > 0)
            {
                PatientName = des.Tables[0].Rows[0]["FullName"].ToString();
            }
            return PatientName;
        }


        public List<SelectListItem> CountryScriptNew(string countryId)
        {
            if (countryId == "")
            {
                countryId = "US";
            }
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.Country();
            return lst;
        }

        //Get Colleagues List for send Referrals--Start
        [HttpPost]
        public JsonResult GetUserList(string selected)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();

                dt = ObjColleaguesData.GetColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), 1, int.MaxValue, null, 11, 2);
                if (dt != null && dt.Rows.Count > 0)
                {
                    sb.Append("<select id=\"select3\" name=\"select3\" style=\"width:100%\">");

                    sb.Append("<option></option>");
                    foreach (DataRow item in dt.Rows)
                    {
                        string Id = Convert.ToString(item["ColleagueId"]);
                        if (selected.Split(',').Contains(Id))
                        {
                            sb.Append("<option value=" + Convert.ToInt32(item["ColleagueId"]) + " class=\"selected\" selected=\"selected\">" + item["FirstName"] + "&nbsp;" + item["LastName"] + "</option>");
                        }
                        else
                        {
                            sb.Append("<option value=" + Convert.ToInt32(item["ColleagueId"]) + ">" + item["FirstName"] + "&nbsp;" + item["LastName"] + "</option>");
                        }
                    }

                    sb.Append("</select>");

                    sb.ToString();
                }

            }
            catch (Exception)
            {

                throw;
            }

            return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
        }



        public List<SelectListItem> StateScriptNew(string countryId, string stateId)
        {
            if (countryId == "")
            {
                countryId = "US";
            }
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.StateByCountryCode(countryId);
            foreach (var item in lst)
            {
                if (item.Text == "[Select State]")
                {
                    lst.Remove(item);
                    break;
                }
            }
            foreach (var item in lst)
            {
                if (item.Value == stateId)
                {
                    item.Selected = true;
                }
            }

            return lst;
        }
        //ViewSide Code
        [CustomAuthorize]
        public ActionResult AddPatient(string PatientId)
        {
            BO.Models.Patients mdlNewPatient = new BO.Models.Patients();
            bool Result = ObjCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
            // Check permission & Membersip
            int intPatientId = 0;
            if (PatientId != null && PatientId != "")
            {
                intPatientId = Convert.ToInt32(PatientId);
                mdlNewPatient = PatientBLL.GetPatientDetailsById(intPatientId, null);
                ViewBag.state = StateScriptNew("", Convert.ToString(mdlNewPatient.State));
                ViewBag.ColleaguesId = GetColleaguesList(ObjCommon.CheckNull(Convert.ToString(mdlNewPatient.ColleagueId), string.Empty));
                ViewBag.LocationsId = NewGetLocationList(ObjCommon.CheckNull(Convert.ToString(mdlNewPatient.LocationId), string.Empty));
            }
            else
            {
                mdlNewPatient.PatientId = 0;
                ViewBag.state = StateScriptNew("", "0");
                ViewBag.ColleaguesId = GetColleaguesList("");
                ViewBag.LocationsId = NewGetLocationList("");
            }
            return View("NewAddPatient", mdlNewPatient);
        }



        [CustomAuthorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        // Add NewPatient 
        public ActionResult AddNewPatient(BO.Models.Patients newPatientObj)
        {
            try
            {

                int status = 0;
                newPatientObj.OwnerId = Convert.ToInt32(SessionManagement.UserId);   // Convert.ToInt32(Session["UserId"]);
                newPatientObj.PatientStatus = 0;
                if (string.IsNullOrEmpty(newPatientObj.AssignedPatientId))
                {
                    newPatientObj.AssignedPatientId = "0";
                }
                newPatientObj.ReferredBy = newPatientObj.ColleagueId;
                newPatientObj.State = newPatientObj.StateId;
                status = PatientBLL.PatientInsertUpdateNew(newPatientObj);
                if (status > 0)
                {
                    FileUploadController objFileUpload = new FileUploadController();
                    objFileUpload.TempToImageBankUpload(status, newPatientObj.OwnerId, false);

                    TempData["Identify"] = "Patients/AddPatient";
                    return RedirectToAction("PatientHistory", "Patients", new { PatientId = status });
                }
                else
                {
                    newPatientObj.PatientId = 0;
                    ViewBag.country = CountryScriptNew("");
                    ViewBag.state = StateScriptNew("", "0");
                    ViewBag.ColleaguesId = GetColleaguesList("");
                    ViewBag.LocationsId = NewGetLocationList("");
                    return PartialView("NewAddPatient", newPatientObj);
                }

            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                throw;
            }
        }
        //Get Location List for Add or Edit Patient--Start
        [HttpPost]
        public JsonResult GetLocationList(string selected)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                DataTable dt = new DataTable();
                dt = ObjColleaguesData.GetDoctorLocaitonById(Convert.ToInt32(Session["UserId"]));
                if (dt != null && dt.Rows.Count > 0)
                {
                    sb.Append("<select id=\"select4\" name=\"select4\" style=\"width:100%\">");

                    sb.Append("<option></option>");
                    foreach (DataRow item in dt.Rows)
                    {
                        string Id = Convert.ToString(item["AddressInfoId"]);
                        if (selected == Id)
                        {
                            if (Id != null)
                            {
                                sb.Append("<option value=" + Convert.ToInt32(item["AddressInfoId"]) + " class=\"selected\" selected=\"selected\">" + item["Location"]);
                            }

                        }
                        else
                        {
                            sb.Append("<option value=" + Convert.ToInt32(item["AddressInfoId"]) + ">" + item["Location"]);
                        }
                    }

                    sb.Append("</select>");

                    sb.ToString();
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(sb.ToString(), JsonRequestBehavior.AllowGet);
        }


        public ActionResult PatientDetails(int _patientId)
        {
            return View();
        }

        public ActionResult GetTreatmentPlanDetails(int PatientId)
        {
            try
            {
                var treatmentlist = PlanTreatmentBLL.GetCaseByPatientId(PatientId, SessionManagement.UserId);
                return View("_patientPlanSection", treatmentlist);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public JsonResult CheckAssignPatientID(int AssignedPatientId, int PatientId, int UserId)
        {
            UserId = Convert.ToInt32(SessionManagement.UserId);
            bool result = PatientBLL.CheckAssignPatientId(AssignedPatientId, PatientId, UserId);
            return Json(result);
        }

        public ActionResult UpdateEditablePatientDetails(PatientEditableVM obj)
        {
            try
            {
                if (PatientBLL.UpdatePatientDetails(obj))
                {
                    return Json(new { Success = true, Message = "OK" });
                }
                else
                {
                    return Json(new { Success = false, Message = "OK" });
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        public ActionResult ShowDeletePopup()
        {
            return PartialView("_patientFileDelete");
        }

        public ActionResult SendForm(int PatientId, string Email, string PatientName)
        {
            try
            {
                bool result = PatientBLL.SendForm(PatientId, Convert.ToInt32(SessionManagement.UserId), Email, SessionManagement.FirstName, PatientName);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("PatientsController-Patient Send Form", ex.Message, ex.StackTrace);
                throw;
            }
        }
        public ActionResult ReferPaymentFromPopUp(int PatientId)
        {
            TempData["PatientId"] = PatientId;
            return PartialView("_PartialRequestPayment");
        }

        public ActionResult SubmitReferPayment(int PatientId, string Amount)
        {
            try
            {
                bool result = PatientBLL.RequestPayment(PatientId, Convert.ToInt32(SessionManagement.UserId), Amount);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("Request payment to patient Time", Ex.Message, Ex.StackTrace);
                throw Ex;
            }
        }
        [HttpPost]
        public ActionResult RequestReviews(int PatientId, string Email, string FirstName, string LastName)
        {
            clsColleaguesData objColleaguesData = new clsColleaguesData();
            clsCommon objCommon = new clsCommon();
            Member MdlMember = null;
            clsTemplate ObjTemplate = new clsTemplate();
            TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();

            try
            {
                #region Public Profile
                SectionPublicProfile objprofiles = new SectionPublicProfile();
                if (SessionManagement.UserId != null && SessionManagement.UserId != "")
                {
                    //bool Result = objCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
                    string DoctoreId = Request.QueryString["DoctorId"];
                    int ViewProfileId;
                    int DoctId = 0;
                    if (Request["hdnViewProfile"] != null)
                    {
                        DoctId = Convert.ToInt32(Request["hdnViewProfile"]);
                    }

                    if (!string.IsNullOrEmpty(DoctoreId))
                    {
                        ViewBag.hdnViewProfile = true;
                        ViewProfileId = int.Parse(DoctoreId);
                        if (ViewProfileId == Convert.ToInt32(Session["UserId"]))
                        {
                            ViewBag.hdnViewProfile = false;
                        }
                    }
                    else if (DoctId > 0)
                    {
                        ViewBag.hdnViewProfile = true;
                        ViewProfileId = DoctId;
                        if (ViewProfileId == Convert.ToInt32(Session["UserId"]))
                        {
                            ViewBag.hdnViewProfile = false;
                        }
                    }
                    else
                    {
                        ViewBag.hdnViewProfile = false;
                        ViewProfileId = Convert.ToInt32(Session["UserId"]);
                    }

                    string ImageName = null;
                    MdlMember = new Member();

                    DataSet ds = new DataSet();
                    ds = MdlMember.GetProfileDetailsOfDoctorByID(ViewProfileId);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string Desciption = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Description"]), string.Empty);
                        MdlMember.UserId = Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]);
                        MdlMember.FirstName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]), string.Empty);
                        MdlMember.LastName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LastName"]), string.Empty);
                        MdlMember.Salutation = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Salutation"]), string.Empty);
                        if (!string.IsNullOrEmpty(Desciption))
                        {
                            MdlMember.Description = Desciption.Replace("\n", "<br/>");
                        }

                        MdlMember.Title = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Title"]), string.Empty);
                        MdlMember.MiddleName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["MiddleName"]), string.Empty);
                        MdlMember.PublicProfile = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["PublicProfileUrl"]), string.Empty);
                        MdlMember.WebsiteURL = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["WebsiteURL"]), string.Empty);
                    }
                }
                #endregion

                bool result = PatientBLL.RequestReviews(PatientId, Email, FirstName, LastName, MdlMember.PublicProfile, MdlMember.FirstName, MdlMember.LastName);
                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("PatientsController-Patient Request Reviews", ex.Message, ex.StackTrace);
                throw;
            }
        }
        
    }
}

