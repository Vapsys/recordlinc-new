﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewRecordlinc.App_Start;
using NewRecordlinc.Models.Patients;
using NewRecordlinc.Models.Colleagues;
using NewRecordlinc.Models.Common;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.PatientsData;
using DataAccessLayer.Common;
using System.Data;
using System.Configuration;
using System.Text;
using System.Web.Script.Serialization;
using System.Web.Configuration;
using ICSharpCode.SharpZipLib.Zip;
using System.IO;
using System.Security;
using BO.Models;
using BusinessLogicLayer;
using BO.ViewModel;

namespace NewRecordlinc.Controllers
{
    public class ReferralsController : Controller
    {

        mdlPatient MdlPatient = null;
        string TempFolder = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("TempUploadsFolderPath"));
        string ImageBankFolder = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("ImageBank"));
        string DefaultDoctorImage = System.Configuration.ConfigurationManager.AppSettings.Get("DefaultDoctorImage");
        string DefaultDoctorFemaleImage = System.Configuration.ConfigurationManager.AppSettings.Get("DefaultDoctorFemaleImage");
        int PageSizeMaster = Convert.ToInt32(WebConfigurationManager.AppSettings["PageSize"]);

        clsPatientsData ObjPatientsData = new clsPatientsData();
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        mdlColleagues MdlColleagues = new mdlColleagues();
        clsCommon ObjCommon = new clsCommon();
        clsTemplate ObjTemplate = new clsTemplate();
        [CustomAuthorize]
        public ActionResult Index()
        {
            int PatientId = (int)TempData["RePatientId"];
            string ColleagueIds = Convert.ToString(TempData["ReColleagueIds"]);
            int AddressInfoId = (int)TempData["ReAddressInfoId"];
            TempData.Keep();
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                SessionManagement.PMSSendReferralDetails = null;
                MdlPatient = new mdlPatient();

                DataSet ds = new DataSet();
                DataSet dsuser = new DataSet();

                if (PatientId != 0)
                {
                    int DoctorId = Convert.ToInt32(SessionManagement.UserId);
                    ds = ObjPatientsData.GetPateintDetailsByPatientId(PatientId, DoctorId);
                    dsuser = ObjColleaguesData.GetDoctorDetailsById(Convert.ToInt32(Session["UserId"]));

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        MdlPatient.PatientId = Convert.ToInt32(ds.Tables[0].Rows[0]["PatientId"]);
                        MdlPatient.AssignedPatientId = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["AssignedPatientId"]), string.Empty);
                        MdlPatient.FirstName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]), string.Empty);
                        MdlPatient.LastName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LastName"]), string.Empty);
                        MdlPatient.DateOfBirth = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["DateOfBirth"]), string.Empty);
                        MdlPatient.Email = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Email"]), string.Empty);
                        MdlPatient.Gender = ObjCommon.CheckNull((Convert.ToString(ds.Tables[0].Rows[0]["Gender"]) == "0") ? "Male" : "Female", string.Empty);
                        MdlPatient.Age = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Age"]), string.Empty);
                        MdlPatient.ExactAddress = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["ExactAddress"]), string.Empty);
                        MdlPatient.Address2 = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Address2"]), string.Empty);
                        MdlPatient.City = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["City"]), string.Empty);
                        MdlPatient.State = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["State"]), string.Empty);
                        MdlPatient.ZipCode = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["ZipCode"]), string.Empty);
                        MdlPatient.Phone = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Phone"]), string.Empty);

                        MdlPatient.ImageName = ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["ProfileImage"]), string.Empty) == string.Empty ? ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty) : ObjCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["ImageBank"]), string.Empty) + ObjCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["ProfileImage"]), string.Empty);
                        if (dsuser != null && dsuser.Tables.Count > 0 && dsuser.Tables[0].Rows.Count > 0)
                        {
                            MdlPatient.DoctorFullName = ObjCommon.CheckNull(Convert.ToString(dsuser.Tables[0].Rows[0]["FullName"]), string.Empty);
                        }

                        MdlPatient.lstPatientImages = MdlPatient.GetPatientImagesAll(PatientId);
                        MdlPatient.lstPateintDocuments = MdlPatient.GetPatientDocumetnsAll(PatientId);
                    }
                }
                string ColleagueName = string.Empty;
                string LocationName = string.Empty;
                if (AddressInfoId == 0)
                {
                    if (!string.IsNullOrEmpty(ColleagueIds))
                    {

                        string[] arryColleagueIds = ColleagueIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < arryColleagueIds.Count(); i++)
                        {
                            DataSet dscolleague = new DataSet();
                            dscolleague = ObjColleaguesData.GetDoctorDetailsById(Convert.ToInt32(arryColleagueIds[i]));
                            if (dscolleague != null && dscolleague.Tables.Count > 0)
                            {
                                if (string.IsNullOrEmpty(ColleagueName))
                                {

                                    ColleagueName = (dscolleague.Tables[0].Rows.Count > 0) ? ObjCommon.CheckNull(Convert.ToString(dscolleague.Tables[0].Rows[0]["FullName"]), string.Empty) : ColleagueName;

                                    LocationName = (dscolleague.Tables[2].Rows.Count > 0) ? ObjCommon.CheckNull(Convert.ToString(dscolleague.Tables[2].Rows[0]["LocationName"]), string.Empty) : LocationName;
                                }
                                else if (dscolleague.Tables[0].Rows.Count > 0)
                                {
                                    ColleagueName = ColleagueName + " <br/><label>&nbsp;</label>&nbsp;" + ObjCommon.CheckNull(Convert.ToString(dscolleague.Tables[0].Rows[0]["FullName"]), string.Empty);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(ColleagueIds))
                    {

                        string[] arryColleagueIds = ColleagueIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < arryColleagueIds.Count(); i++)
                        {
                            DataTable dscolleaguelocation = new DataTable();
                            dscolleaguelocation = ObjColleaguesData.GetLocationOfColleagues(Convert.ToInt32(AddressInfoId));
                            if (dscolleaguelocation != null && dscolleaguelocation.Rows.Count > 0)
                            {
                                if (string.IsNullOrEmpty(ColleagueName))
                                {
                                    LocationName = ObjCommon.CheckNull(Convert.ToString(dscolleaguelocation.Rows[0]["Location"].ToString()), string.Empty);
                                    ColleagueIds = ObjCommon.CheckNull(Convert.ToString(dscolleaguelocation.Rows[0]["UserId"].ToString()), string.Empty);
                                    ColleagueName = ObjCommon.CheckNull(Convert.ToString(dscolleaguelocation.Rows[0]["ColleagueName"].ToString()), string.Empty);
                                }
                                else
                                {
                                    LocationName = LocationName + " <br/><label>&nbsp;</label>&nbsp;" + ObjCommon.CheckNull(Convert.ToString(dscolleaguelocation.Rows[0]["Location"].ToString()), string.Empty);
                                    ColleagueName = LocationName + " <br/><label>&nbsp;</label>&nbsp;" + ObjCommon.CheckNull(Convert.ToString(dscolleaguelocation.Rows[0]["UserId"].ToString()), string.Empty);
                                    LocationName = LocationName + " <br/><label>&nbsp;</label>&nbsp;" + ObjCommon.CheckNull(Convert.ToString(dscolleaguelocation.Rows[0]["ColleagueName"].ToString()), string.Empty);

                                }
                            }
                        }
                    }
                }
                ViewBag.ColleagueName = ColleagueName;
                MdlPatient.ColleagueIds = ColleagueIds;
                ViewBag.LocationName = LocationName;
                ViewBag.LocationId = AddressInfoId;
                //ViewBag.list=MdlPatient.GetVisibleDetails(0,0);
                MainReferralForm rfobj = new MainReferralForm();
                int UserId = Convert.ToInt32(ColleagueIds.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries)[0]);
                rfobj = ReferralFormBLL.GetDentistSpeciality(UserId);
                if (rfobj != null)
                {
                    MdlPatient.lstDentistSpeciality = new List<SelectListItem>();
                    MdlPatient.lstSpecialityService = new List<DentistSpecialityService>();
                    foreach (var item in rfobj.RefFormSetting)
                    {
                        MdlPatient.lstDentistSpeciality.Add(new SelectListItem()
                        {
                            Text = item.Name,
                            Value = Convert.ToString(item.SpecialityId),
                            Selected = Convert.ToBoolean(item.IsEnable)
                        });

                        foreach (var item1 in item.RefService)
                        {
                            MdlPatient.lstSpecialityService.Add(new DentistSpecialityService()
                            {
                                ServiceId = item1.FieldId,
                                SpecialityId = item.SpecialityId,
                                SpecialityName = item.Name,
                                NameFor = item1.NameFor,
                                ServiceName = item1.Name,
                                IsEnable = Convert.ToBoolean(item1.IsEnable)
                            });
                        }
                        //List<ReferralFormServices> ServiceObj = new List<ReferralFormServices>();
                        //ServiceObj = ReferralFormBLL.GetDentistServices(UserId, item.SpecialityId);
                        //if (ServiceObj != null)
                        //{
                        //    foreach (var S_Obj in ServiceObj)
                        //    {
                        //        MdlPatient.lstSpecialityService.Add(new DentistSpecialityService()
                        //        {
                        //            ServiceId = S_Obj.FieldId,
                        //            SpecialityId = item.SpecialityId,
                        //            SpecialityName = item.Name,
                        //            ServiceName = S_Obj.Name,
                        //            IsEnable = Convert.ToBoolean(S_Obj.IsEnable)
                        //        });
                        //    }
                        //}
                    }
                }
                return View("Index2", MdlPatient);
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }
        [CustomAuthorize]
        public ActionResult Index1()
        {
            int PatientId = (int)TempData["RePatientId"];
            string ColleagueIds = Convert.ToString(TempData["ReColleagueIds"]);
            int AddressInfoId = (int)TempData["ReAddressInfoId"];
            TempData.Keep();
            return View("ComposeReferral");
        }
        public ActionResult GetPatientImages(int PatientId)
        {
            List<BO.ViewModel.PatientImages> Lst = ReferralFormBLL.GetPatientImagesAll(PatientId);
            return View(Lst);
        }
        public ActionResult GetPatientDocument(int PatientId)
        {
            List<BO.ViewModel.PateintDocuments> Lst = ReferralFormBLL.GetPatientDocumetnsAll(PatientId);
            return View(Lst);
        }
        public PartialViewResult PatientListPopUp()
        {
            return PartialView("PartialPatientListPopUp");
        }

        [HttpPost]
        public ActionResult ComposeReferral(FormCollection Frm)
        {
            if (Request.Form["hdnsavereferral"] != null && Convert.ToString(Request.Form["hdnsavereferral"]) == "0")
            {
                return null;
            }
            string PatientId = string.Empty; string ImagesId = string.Empty;
            string ColleagueId = string.Empty; string DocumentId = string.Empty;
            string LocationId = string.Empty; string TeethIds = string.Empty;
            string comment = string.Empty; string XMLstring = "<NewDataSet>";
            string FromFiled = string.Empty; string TextValue = string.Empty;
            foreach (var item in Request.Form.Keys)
            {

                if (!String.IsNullOrEmpty(Convert.ToString(Request.Form[item.ToString()])))
                {
                    if (item.ToString().StartsWith("PatientId"))
                    {
                        PatientId = Convert.ToString(Request.Form[item.ToString()]).Replace("PaitentId_", "");
                    }
                    if (item.ToString().StartsWith("ColleagueId"))
                    {
                        ColleagueId = Convert.ToString(Request.Form[item.ToString()]).Replace("ColleagueId_", "");
                    }
                    if (item.ToString().StartsWith("LocationId"))
                    {
                        LocationId = Convert.ToString(Request.Form[item.ToString()]).Replace("LocationId_", "");
                    }
                    if (item.ToString().StartsWith("FromFiled"))
                    {
                        FromFiled = Convert.ToString(Request.Form[item.ToString()]).Replace("FromFiled_", "");
                    }
                    if (item.ToString().StartsWith("comment"))
                    {
                        comment = Convert.ToString(Request.Form[item.ToString()]).Replace("comment_", "");
                    }
                    //if (item.ToString().StartsWith("ImagesId"))
                    //{
                    //    ImagesId = Convert.ToString(Request.Form[item.ToString()]).Replace("ImagesId_", "");
                    //    ImagesId = ImagesId.TrimEnd(',');
                    //}
                    //if (item.ToString().StartsWith("DocId"))
                    //{
                    //    DocumentId = Convert.ToString(Request.Form[item.ToString()]).Replace("DocId_", "");
                    //    DocumentId = DocumentId.TrimEnd(',');
                    //}
                    if (item.ToString().StartsWith("sub"))
                    {
                        string[] SplitString; string CategoryID = string.Empty;
                        string SubCatId = string.Empty;
                        string SubSubCatId = string.Empty;

                        SplitString = item.ToString().Split('_');
                        CategoryID = SplitString[1];
                        SubCatId = SplitString[2];
                        if (SplitString[3] == "0")
                        {
                            SplitString[3] = null;
                        }
                        SubSubCatId = ((SplitString[3] != "" && SplitString[3] != null) ? SplitString[3] : null);
                        TextValue = Convert.ToString(Request.Form[item.ToString()]);
                        XMLstring += "<Table><catId>" + SecurityElement.Escape(CategoryID) + "</catId>";
                        XMLstring += "<SubCatId>" + SecurityElement.Escape(SubCatId) + "</SubCatId>";
                        XMLstring += "<SubSubCatId>" + SecurityElement.Escape(SubSubCatId) + "</SubSubCatId>";
                        XMLstring += "<Value>" + SecurityElement.Escape(TextValue) + "</Value>";
                        XMLstring += "<ReferrelCardId>" + null + "</ReferrelCardId></Table>";
                    }
                    if (item.ToString().StartsWith("teeth"))
                    {
                        TeethIds += Convert.ToString(Request.Form[item.ToString()]).Replace("teeth_", "") + ",";

                    }

                }

            }

            FileUploadController objFileUpload = new FileUploadController();
            IDictionary<string,bool> img_result = objFileUpload.TempToImageBankUpload(Convert.ToInt32(PatientId), Convert.ToInt32(SessionManagement.UserId),true);
            foreach (var item in img_result)
            {
                if(item.Value == false)
                {
                    ImagesId = ImagesId + item.Key + ",";
                }
                else
                {
                    DocumentId = DocumentId + item.Key + ",";
                }
            }
           

            TeethIds = TeethIds.TrimEnd(',');
            XMLstring += "</NewDataSet>";



            object obj = string.Empty;

            int[] ReferralId = new int[2];

            try
            {
                if (!string.IsNullOrEmpty(ColleagueId))
                {
                    string[] arryColleagueIds = ColleagueId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < arryColleagueIds.Count(); i++)
                    {
                        string ToField = string.Empty;

                        string ToColleagueEmail = string.Empty;
                        DataSet dscolleague = new DataSet();
                        dscolleague = ObjColleaguesData.GetDoctorDetailsById(Convert.ToInt32(arryColleagueIds[i]));
                        if (dscolleague != null && dscolleague.Tables.Count > 0)
                        {
                            ToField = ObjCommon.CheckNull(Convert.ToString(dscolleague.Tables[0].Rows[0]["FullName"]), string.Empty);
                            ToColleagueEmail = ObjCommon.CheckNull(Convert.ToString(dscolleague.Tables[0].Rows[0]["Username"]), string.Empty);
                            //ReferralId = ObjColleaguesData.InsertReferral(FromField, ToField, "Referral Card", "Refer Patient", 2, Convert.ToInt32(Session["UserId"]), PatientId, RegardOption, RequestingOption, 0, Convert.ToInt32(arryColleagueIds[i]), Comments, RegardingOtherComments, RequestOtherComments, Teethlist, SelectedImagesIds, SelectedDocumentsIds,LocationName);

                            ReferralId = ObjColleaguesData.InsertReferral(FromFiled, ToField, "Referral Card", "Refer Patient", 2, Convert.ToInt32(Session["UserId"]), Convert.ToInt32(PatientId), null, null, 0, Convert.ToInt32(arryColleagueIds[i]), comment, null, null, TeethIds, ImagesId, DocumentId, LocationId, XMLstring);

                            if (ReferralId[0] > 0)
                            {
                                ObjTemplate.SendPatientReferral(Convert.ToInt32(Session["UserId"]), ToColleagueEmail, ToField, comment, ReferralId[0], ReferralId[1], 2);
                                obj = "1";
                            }
                            ObjColleaguesData.AddPatientToReferredDoctor(Convert.ToInt32(arryColleagueIds[i]), Convert.ToInt32(PatientId));
                            if (PatientId != null)
                            {
                                ObjTemplate.SendReferralDetailsToPatients(Convert.ToInt32(PatientId), Convert.ToInt32(arryColleagueIds[i]), FromFiled, Convert.ToInt32(LocationId),Convert.ToInt32(SessionManagement.UserId));
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                bool status = ObjCommon.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                //obj = ex.Message.ToString();
            }
            TempData["Redirection"] = "ComposeReferral";
            TempData.Remove("RePatientId");
            TempData.Remove("ReColleagueIds");
            TempData.Remove("ReAddressInfoId");
            return RedirectToAction("Index", "Patients", new { PatientId = PatientId, DoctorId = ColleagueId, Redir = 1 });

        }
        //public JsonResult ComposeReferral(int PatientId, string ColleagueId, string FromField, string RegardOption, string RequestingOption, string Comments, string RegardingOtherComments, string RequestOtherComments, string Teethlist, string SelectedImagesIds, string SelectedDocumentsIds,string LocationName)
        //{
        //    object obj = string.Empty;

        //    int[] ReferralId = new int[2];
        //    // Subject: for referall is Referral Card and Body: for referral is Refer Patient
        //    // Need to remove OwnerType,pTeethMap
        //    // Refer: is 1 for message and 2 for referral
        //    // ReferStatus: is 1 for refer,2 for accept and 3 for denied
        //    // ReferedUserId: is Selected colleague id for send referral
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(ColleagueId))
        //        {
        //            string[] arryColleagueIds = ColleagueId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
        //            for (int i = 0; i < arryColleagueIds.Count(); i++)
        //            {
        //                string ToField = string.Empty;

        //                string ToColleagueEmail = string.Empty;
        //                DataSet dscolleague = new DataSet();
        //                dscolleague = ObjColleaguesData.GetDoctorDetailsById(Convert.ToInt32(arryColleagueIds[i]));
        //                if (dscolleague != null && dscolleague.Tables.Count > 0)
        //                {
        //                    ToField = ObjCommon.CheckNull(Convert.ToString(dscolleague.Tables[0].Rows[0]["FullName"]), string.Empty);
        //                    ToColleagueEmail = ObjCommon.CheckNull(Convert.ToString(dscolleague.Tables[0].Rows[0]["Username"]), string.Empty);

        //                    ReferralId = ObjColleaguesData.InsertReferral(FromField, ToField, "Referral Card", "Refer Patient", 2, Convert.ToInt32(Session["UserId"]), PatientId, RegardOption, RequestingOption, 0, Convert.ToInt32(arryColleagueIds[i]), Comments, RegardingOtherComments, RequestOtherComments, Teethlist, SelectedImagesIds, SelectedDocumentsIds,LocationName);
        //                    if (ReferralId[0] > 0)
        //                    {
        //                        ObjTemplate.SendPatientReferral(Convert.ToInt32(Session["UserId"]), ToColleagueEmail, ToField, Comments, ReferralId[0],ReferralId[1],2);
        //                        obj = "1";
        //                    }
        //                    ObjColleaguesData.AddPatientToReferredDoctor(Convert.ToInt32(arryColleagueIds[i]), PatientId);
        //                    ObjTemplate.SendReferralDetailsToPatients(PatientId, Convert.ToInt32(arryColleagueIds[i]), FromField,Convert.ToInt32(LocationName));
        //                }
        //            }
        //        }

        //        if (ReferralId[0] > 0)
        //        {
        //            obj = "1";
        //        }
        //        else
        //        {
        //            obj = "";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        bool status = ObjCommon.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
        //        //obj = ex.Message.ToString();
        //    }
        //    return Json(obj, JsonRequestBehavior.AllowGet);
        //}
        public ActionResult SendReferralDetails()
        {
            try
            {
                if (SessionManagement.UserId != null && SessionManagement.UserId != "")
                {
                    MdlColleagues = new mdlColleagues();
                    MdlColleagues.lstReferredPatient = SendReferral(Convert.ToInt32(Session["UserId"]), 1, PageSizeMaster, 1, 2, null, SessionManagement.TimeZoneSystemName);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return View(MdlColleagues);
        }

        public List<ReferredPatient> SendReferral(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirection, string SearchText, string TimeZoneSystemName)
        {

            List<ReferredPatient> lst = new List<ReferredPatient>();
            lst = MdlColleagues.GetSendReferredPatientList(UserId, PageIndex, PageSize, SortColumn, SortDirection, SearchText, TimeZoneSystemName);
            ViewBag.PageIndex = PageIndex;
            ViewBag.OrderByID = SortColumn;
            ViewBag.OrderBy = SortDirection;
            ViewBag.Searchtext = SearchText;
            if (lst.Count > 0)
                ViewBag.MessageId = lst[0].MessageId;
            else
                ViewBag.MessageId = 0;
            if (SortColumn == 1)
            {
                ViewBag.OrderbyTest = 2;
            }
            else
            {
                ViewBag.OrderbyTest = 1;
            }
            return lst;
        }
        public ActionResult RecivedReferralDetails()
        {
            try
            {
                if (SessionManagement.UserId != null && SessionManagement.UserId != "")
                {
                    MdlColleagues = new mdlColleagues();
                    MdlColleagues.lstReferredPatient = ReceivedReferral(Convert.ToInt32(Session["UserId"]), 1, PageSizeMaster, 1, 2, null);
                }
            }
            catch (Exception)
            {

                throw;
            }

            return View(MdlColleagues);
        }

        public List<ReferredPatient> ReceivedReferral(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirection, string SearchText)
        {
            List<ReferredPatient> lst = new List<ReferredPatient>();
            lst = MdlColleagues.GetReceivedPatientList(UserId, PageIndex, PageSize, SortColumn, SortDirection, SearchText, SessionManagement.TimeZoneSystemName);
            ViewBag.PageIndex = PageIndex;
            ViewBag.OrderByID = SortColumn;
            ViewBag.OrderBy = SortDirection;
            ViewBag.Searchtext = SearchText;
            if (lst.Count > 0)
                ViewBag.MessageId = lst[0].MessageId;
            else
                ViewBag.MessageId = 0;
            if (SortColumn == 1)
            {
                ViewBag.OrderbyTest = 2;
            }
            else
            {
                ViewBag.OrderbyTest = 1;
            }
            return lst;
        }

        public ActionResult GetMessageBodayOfSentReferrals(int MessageId)
        {
            try
            {
                MdlPatient = new mdlPatient();
                MdlColleagues = new mdlColleagues();
                List<ReferralDetails> lstRefferalDetails = new List<ReferralDetails>();
                MdlColleagues.lstRefferalDetails = MdlPatient.GetRefferalDetailsById(Convert.ToInt32(Session["UserId"]), MessageId, "Sentbox", SessionManagement.TimeZoneSystemName);
                ViewBag.ReferralCardId = MdlPatient.ReferralCardId;
                ViewBag.EndMessageTime = DateTime.Now;
                return View(MdlColleagues.lstRefferalDetails);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public ActionResult GetMessageBodyOfReceiveReferrals(int MessageId)
        {
            try
            {
                MdlPatient = new mdlPatient();
                MdlColleagues = new mdlColleagues();
                List<ReferralDetails> lstRefferalDetails = new List<ReferralDetails>();
                MdlColleagues.lstRefferalDetails = MdlPatient.GetRefferalDetailsById(Convert.ToInt32(Session["UserId"]), MessageId, "Inbox", SessionManagement.TimeZoneSystemName);
                ViewBag.ReferralCardId = MdlPatient.ReferralCardId;
                ViewBag.EndMessageTime = DateTime.Now;
                return View(MdlColleagues.lstRefferalDetails);

            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog(Request.Url.ToString(), ex.Message, ex.StackTrace);
                throw;
            }
        }
        public ActionResult PartialViewForReferralcategoryvalues(int RefCardId)
        {
            try
            {
                MdlColleagues = new mdlColleagues();
                MdlColleagues.NewReferralView = MdlColleagues.GetNewReferralViewByRefCardId(Convert.ToInt32(RefCardId));
                return View("PartialNewReferral", MdlColleagues.NewReferralView);

            }
            catch (Exception EX)
            {
                ObjCommon.InsertErrorLog(Request.Url.ToString(), EX.Message, EX.StackTrace);
                throw;
            }

        }
        public ActionResult GetSentReferralOnScroll(int PageIndex, int OrderByID, int OrderBy, string Searchtext, string TimeZoneSystemName)
        {

            MdlColleagues = new mdlColleagues();
            MdlPatient = new mdlPatient();
            List<ReferredPatient> lst = new List<ReferredPatient>();
            ViewBag.PageIndex = PageIndex;
            ViewBag.OrderByID = OrderByID;
            ViewBag.OrderBy = OrderBy;
            ViewBag.Searchtext = Searchtext;
            lst = MdlColleagues.GetSendReferredPatientList(Convert.ToInt32(Session["UserId"]), PageIndex, PageSizeMaster, OrderByID, OrderBy, Searchtext, TimeZoneSystemName);

            return View("PartialSentReferral", lst);

        }

        public ActionResult SentCasesPageing(int PageIndex, int OrderByID, int OrderBy, string Searchtext)
        {
            MdlColleagues = new mdlColleagues();
            List<ReferredPatient> lst = new List<ReferredPatient>();
            ViewBag.Sorting = "true";
            lst = SendReferral(Convert.ToInt32(Session["UserId"]), PageIndex, PageSizeMaster, OrderByID, OrderBy, Searchtext, SessionManagement.TimeZoneSystemName);
            return View("PartialSentReferral", lst);
        }

        public ActionResult GetReceivedReferralOnScroll(int PageIndex, int OrderByID, int OrderBy, string Searchtext)
        {

            MdlColleagues = new mdlColleagues();
            MdlPatient = new mdlPatient();
            List<ReferredPatient> lst = new List<ReferredPatient>();
            ViewBag.PageIndex = PageIndex;
            ViewBag.OrderByID = OrderByID;
            ViewBag.OrderBy = OrderBy;
            ViewBag.Searchtext = Searchtext;
            lst = MdlColleagues.GetReceivedPatientList(Convert.ToInt32(Session["UserId"]), PageIndex, PageSizeMaster, OrderByID, OrderBy, Searchtext, SessionManagement.TimeZoneSystemName);

            return View("PartialReceviedReferral", lst);

        }
        public ActionResult ReceivedCasesPageing(int PageIndex, int OrderByID, int OrderBy, string Searchtext)
        {
            MdlColleagues = new mdlColleagues();
            List<ReferredPatient> lst = new List<ReferredPatient>();
            ViewBag.Sorting = "true";
            lst = ReceivedReferral(Convert.ToInt32(Session["UserId"]), PageIndex, PageSizeMaster, OrderByID, OrderBy, Searchtext);
            return View("PartialReceviedReferral", lst);
        }

        public bool RemoveReferralReceived(int MessageId)
        {
            bool result = false;
            try
            {
                result = ObjColleaguesData.RemovereferralFromDoctorInbox(Convert.ToInt32(Session["UserId"]), MessageId);
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }
        public bool RemoveReferralSent(int MessageId)
        {

            bool result = false;
            try
            {
                result = ObjColleaguesData.RemoveReferralFromSentBox(Convert.ToInt32(Session["UserId"]), MessageId);
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }


        public JsonResult ResendMessage(int MessageId, int ReferralCardId, int PatientId, string SenderName)
        {

            bool result = false;
            try
            {
                int[] ReferralId = new int[2];
                ReferralId = ObjColleaguesData.ResendMessage(MessageId, ReferralCardId);
                DataTable dtmessage = new DataTable();
                if (ReferralId.Count() > 0)
                {
                    string ToField = string.Empty;
                    string ToColleagueEmail = string.Empty;
                    string Comments = string.Empty;
                    int LocationId = 0;
                    int ReferedUserId = 0;
                    dtmessage = ObjColleaguesData.GetMessageDetailByMessageId(ReferralId[1]);
                    if (dtmessage != null && dtmessage.Rows.Count > 0)
                    {
                        ToField = ObjCommon.CheckNull(Convert.ToString(dtmessage.Rows[0]["ToField"]), string.Empty);
                        ToColleagueEmail = ObjCommon.CheckNull(Convert.ToString(dtmessage.Rows[0]["ToColleagueEmail"]), string.Empty);
                        Comments = ObjCommon.CheckNull(Convert.ToString(dtmessage.Rows[0]["Comments"]), string.Empty);
                        LocationId = Convert.ToInt32(dtmessage.Rows[0]["LocationId"]);
                        ReferedUserId = Convert.ToInt32(dtmessage.Rows[0]["ReferedUserId"]);
                        if (ReferralId[0] > 0)
                        {
                            ObjTemplate.SendPatientReferral(Convert.ToInt32(Session["UserId"]), ToColleagueEmail, ToField, Comments, ReferralId[0], ReferralId[1], 2);
                        }
                        if (PatientId > 0)
                        {
                            ObjTemplate.SendReferralDetailsToPatients(Convert.ToInt32(PatientId), Convert.ToInt32(ReferedUserId), SenderName, Convert.ToInt32(LocationId), Convert.ToInt32(Session["UserId"]));
                        }
                    }

                }
                result = true;
            }
            catch (Exception ex)
            {


            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// This method use for hide the query string values created by Ankit Solanki.
        /// </summary>
        /// <param name="PatientId"></param>
        /// <param name="ColleagueIds"></param>
        /// <param name="AddressInfoId"></param>
        /// <returns></returns>
        public ActionResult Referral(int PatientId = 0, string ColleagueIds = null, int AddressInfoId = 0)
        {
            TempData["RePatientId"] = PatientId;
            TempData["ReColleagueIds"] = ColleagueIds;
            TempData["ReAddressInfoId"] = AddressInfoId;
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult GetVisibleDetails(int regardId, int ColleagueId)
        {
            MdlPatient = new mdlPatient();
            List<mdlPatient.SpecialityService> spList = new List<mdlPatient.SpecialityService>();
            spList = MdlPatient.GetVisibleDetails(regardId, ColleagueId);
            ViewBag.list = spList;
            return View("PartialServices", spList);
        }
        [HttpPost]
        public ActionResult GetVisibleSpeciality(int ColleagueId)
        {
            MdlPatient = new mdlPatient();
            List<mdlPatient.SpecialityService> spList = new List<mdlPatient.SpecialityService>();
            spList = MdlPatient.GetVisibleSpeciality(ColleagueId);
            ViewBag.list = spList;
            return View("PartialSpeciality", spList);
        }
    }
}
