﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.IO;
using System.Text;
using NewRecordlinc.Models.Common;
using DataAccessLayer.ColleaguesData;
using System.Data;
using DataAccessLayer.Common;
using System.Web.Helpers;
using DataAccessLayer.FlipTop;
using System.Configuration;
using NewRecordlinc.Models.Colleagues;
using NewRecordlinc.App_Start;
using NewRecordlinc.Models;
using ArbApiSample;
using System.Xml;
using BO.Models;
using BusinessLogicLayer;

namespace NewRecordlinc.Controllers
{
    public class FreeToPaidController : Controller
    {
        //
        // GET: /FreeToPaid
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        clsCommon objCommon = new clsCommon();
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        clsTemplate ObjTemplate = new clsTemplate();
        RecurringSubscription objRecurringSubscription = new RecurringSubscription();

        public ActionResult Index()
        {
            PyamentDetails objPyamentDetails = new PyamentDetails();
            if (Request["Invalid"] != null)
            {
                ViewBag.PaymentNotProceed = Request["Invalid"].ToString();
            }
            if (TempData["userdetails"] != null)
            {
                objPyamentDetails = TempData["userdetails"] as PyamentDetails;
            }
            if (Request["Amount"] != null && Request["Plane"] != null)
            {
                objPyamentDetails.Amount = Request["Amount"];
                objPyamentDetails.Planes = Request["Plane"];
            }
            else
            {
                objPyamentDetails.Amount = "$150";
                objPyamentDetails.Planes = "6";
            }
            return View(objPyamentDetails);
        }

        [HttpPost]
        public ActionResult Index(PyamentDetails objPyamentDetails)
        {
            Member mdlMember = new Member();
            if (objPyamentDetails.Planes != null)
            {
                mdlCommon objCommoncls = new mdlCommon();
                if (Request["Amount"] != null && Request["Plane"] != null)
                {
                    objPyamentDetails.Amount = Request["Amount"];
                    objPyamentDetails.Planes = Request["Plane"];
                }
                else
                {
                    objPyamentDetails.Amount = "$" + objCommoncls.GetAmount(objPyamentDetails.Planes.ToString());
                }
            }
            else
            {
                if (Convert.ToString(Request["Amount"]) != null)
                {
                    objPyamentDetails.Amount = Convert.ToString(Request["Amount"]);                    
                    if (objPyamentDetails.Amount == "$150")
                    {
                        objPyamentDetails.Planes = "6";                                               
                    }
                    if (objPyamentDetails.Amount == "$350")
                    {
                        objPyamentDetails.Planes = "7";                        
                    }
                    if (objPyamentDetails.Amount == "")
                    {
                        objPyamentDetails.Planes = "5";                        
                    }
                    var lst = mdlMember.GetMembershipPlan(Convert.ToInt32(objPyamentDetails.Planes));
                    List<SelectListItem> lstMembership = new List<SelectListItem>();
                    foreach (var item in lst)
                    {
                        SelectListItem li = new SelectListItem();
                        li.Text = item.Title;
                        li.Value = Convert.ToString(item.Membership_Id);
                        if (item.Membership_Id == Convert.ToInt32(objPyamentDetails.Planes))
                        {
                            li.Selected = true;
                        }
                        lstMembership.Add(li);                   
                    }
                    ViewBag.lstMembership = lstMembership;
                    
                }
            }

            return View(objPyamentDetails);
        }

        public ActionResult UpgradeAccount()
        {
            Member MdlMember = new Member();
            clsColleaguesData objcls = new clsColleaguesData();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            DataTable Dts = new DataTable();
            PyamentDetails objPyamentDetails = new PyamentDetails();
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                ds = MdlMember.GetProfileDetailsOfDoctorByID(Convert.ToInt32(Session["UserId"]));
                dt = MdlMember.GetPaymentHostoryOnUserId(Convert.ToInt32(Session["UserId"]));
                Membership_FeatureBLL ObjMembershipBLL = new Membership_FeatureBLL();
                List<Membership> lst = new List<Membership>();
                List<NewMembership> lst1 = new List<NewMembership>();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    objPyamentDetails.ListOfMembership = MdlMember.GetMembershipPlan(Convert.ToInt32(ds.Tables[0].Rows[0]["MembershipId"]));
                    objPyamentDetails.Planes = Convert.ToString(ds.Tables[0].Rows[0]["MembershipId"]);
                }

                if (dt.Rows.Count > 0)
                    ViewBag.CurrentPlan = "PAID";
                else
                    ViewBag.CurrentPlan = "FREE";
                //objPyamentDetails.Amount = "";
                //objPyamentDetails.Planes = "1";
                if (ds.Tables[0].Rows.Count > 0)
                {
                    objPyamentDetails.Firstname = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]), string.Empty);
                    objPyamentDetails.Lastname = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LastName"]), string.Empty);
                    objPyamentDetails.Email = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["UserName"]), string.Empty);
                    objPyamentDetails.Userid = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["UserId"]), string.Empty);
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    objPyamentDetails.City = objCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["City"]), string.Empty);
                    objPyamentDetails.State = objCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["State"]), string.Empty);
                    objPyamentDetails.Zip = objCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["ZipCode"]), string.Empty);
                    objPyamentDetails.Phone = objCommon.CheckNull(Convert.ToString(ds.Tables[2].Rows[0]["Phone"]), string.Empty);
                }
                DataTable dttbl = objcls.getNewPaymentHistoryOnUserId(Convert.ToInt32(Session["UserId"]));
                string membershipId = string.Empty;
                string amount = string.Empty;
                string subType = string.Empty;
                if (dttbl != null && dttbl.Rows.Count > 0)
                {
                    //membershipId = Convert.ToString(dttbl.Rows[0]["membershipId"]);
                    amount = Convert.ToString(dttbl.Rows[0]["amount"]);
                    subType = Convert.ToString(dttbl.Rows[0]["subscriptionType"]);                    
                }
                if (subType == "3")
                {
                    ViewBag.PlanDescription = "Quarterly Payment of $" + amount + " per quarter.";
                }
                if (subType == "1")
                {
                    ViewBag.PlanDescription = "Monthly Payment of $" + amount + " per month.";
                }
                if (subType == "6")
                {
                    ViewBag.PlanDescription = "Half-yearly Payment of $" + amount + " per half year.";
                }
                if (subType == "12")
                {
                    ViewBag.PlanDescription = "Annually Payment of $" + amount + " per year.";
                }
                if (subType == "")
                {
                    ViewBag.PlanDescription = "Free Package";
                }
                return View(objPyamentDetails);

            }
            return RedirectToAction("Index", "User");
        }

        [HttpPost]
        public ActionResult MakePayment(PyamentDetails objPyamentDetails)
        {
            try
            {
                if (Request["Amount"] != null && Request["Plane"] != null)
                {
                    objPyamentDetails.Amount = Request["Amount"];
                    objPyamentDetails.Planes = Request["Plane"];
                }
                clsCommon objCommonerrorlog = new clsCommon();
                mdlCommon objCommoncls = new mdlCommon();
                clsColleaguesData ObjColleaguesData = new clsColleaguesData();
                // By default, this sample code is designed to post to our test server for
                // developer accounts: https://test.authorize.net/gateway/transact.dll
                // for real accounts (even in test mode), please make sure that you are
                // posting to: https://secure.authorize.net/gateway/transact.dll
                String post_url = ConfigurationManager.AppSettings["post_url"];
                string amount = string.Empty;
                string strRecurring = string.Empty;
                Dictionary<string, string> post_values = new Dictionary<string, string>();

                post_values.Add("x_login", ConfigurationManager.AppSettings["x_login"]);
                post_values.Add("x_tran_key", ConfigurationManager.AppSettings["x_tran_key"]);
                post_values.Add("x_delim_data", ConfigurationManager.AppSettings["x_delim_data"]);
                post_values.Add("x_delim_char", ConfigurationManager.AppSettings["x_delim_char"]);
                post_values.Add("x_relay_response", ConfigurationManager.AppSettings["x_relay_response"]);
                post_values.Add("x_response_format", ConfigurationManager.AppSettings["x_response_format"]);
                post_values.Add("x_version", ConfigurationManager.AppSettings["x_version"]);
                post_values.Add("x_type", ConfigurationManager.AppSettings["x_type"]);
                post_values.Add("x_method", ConfigurationManager.AppSettings["x_method"]);


                post_values.Add("x_card_type", objPyamentDetails.CardType);
                post_values.Add("x_card_num", objPyamentDetails.CardNumber);
                post_values.Add("x_exp_date", objPyamentDetails.expdate);
                Member objMember = new Member();
               
                objPyamentDetails.ListOfMembership = objMember.GetMembershipById(Convert.ToInt32(objPyamentDetails.Planes));
               
                string firstAmount = objCommoncls.GetAmount(objPyamentDetails.Planes);
                foreach (var item in objPyamentDetails.ListOfMembership)
                {
                    string ActualPrice = string.Empty;
                    if (objPyamentDetails.payInterval == "1")
                    {
                        ActualPrice = Convert.ToString(item.MonthlyPrice);
                        if (!string.IsNullOrEmpty(ActualPrice))
                        {
                            amount = ActualPrice;
                            objPyamentDetails.Amount = ActualPrice;
                        }
                        else
                        {
                            amount = firstAmount;
                            objPyamentDetails.Amount = amount;
                        }
                    }
                    if (objPyamentDetails.payInterval == "3")
                    {
                        ActualPrice = Convert.ToString(item.QuaterlyPrice);
                        if (!string.IsNullOrEmpty(ActualPrice))
                        {
                            amount = ActualPrice;
                            objPyamentDetails.Amount = ActualPrice;
                        }
                        else
                        {
                            var amt = Convert.ToString(Convert.ToInt32(firstAmount) * 3);
                            amount = amt;
                            objPyamentDetails.Amount = amount;
                        }
                    }
                    if (objPyamentDetails.payInterval == "6")
                    {
                        ActualPrice = Convert.ToString(item.HalfYearlyPrice);
                        if (!string.IsNullOrEmpty(ActualPrice))
                        {
                            amount = ActualPrice;
                            objPyamentDetails.Amount = ActualPrice;
                        }
                        else
                        {
                            var amt = Convert.ToString(Convert.ToInt32(firstAmount) * 6);
                            amount = amt;
                            objPyamentDetails.Amount = amount;
                        }
                    }
                    if (objPyamentDetails.payInterval == "12")
                    {
                        ActualPrice = Convert.ToString(item.AnnualPrice);
                        if (!string.IsNullOrEmpty(ActualPrice))
                        {
                            amount = ActualPrice;
                            objPyamentDetails.Amount = ActualPrice;
                        }
                        else
                        {
                            var amt = Convert.ToString(Convert.ToInt32(firstAmount) * 12);
                            amount = amt;
                            objPyamentDetails.Amount = amount;
                        }
                    }
                }
                //amount = objCommoncls.GetAmount(objPyamentDetails.Planes.ToString());
                post_values.Add("x_amount", amount);
                post_values.Add("x_description", objPyamentDetails.Description);
                post_values.Add("x_first_name", objPyamentDetails.Firstname);
                post_values.Add("x_last_name", objPyamentDetails.Lastname);
                post_values.Add("x_address", objPyamentDetails.Lastname);
                post_values.Add("x_state", objPyamentDetails.State);
                post_values.Add("x_zip", objPyamentDetails.Zip);

                String post_string = "";

                foreach (KeyValuePair<string, string> post_value in post_values)
                {
                    post_string += post_value.Key + "=" + HttpUtility.UrlEncode(post_value.Value) + "&";
                }
                post_string = post_string.TrimEnd('&');


                objCommonerrorlog.InsertErrorLog("PaymentMode", "Invalid Transaction", post_string);
                // create an HttpWebRequest object to communicate with Authorize.net
                HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(post_url);
                objRequest.Method = "POST";
                objRequest.ContentLength = post_string.Length;
                objRequest.ContentType = "application/x-www-form-urlencoded";
                objRequest.KeepAlive = false;
                objRequest.ProtocolVersion = HttpVersion.Version10;
                objRequest.ServicePoint.ConnectionLimit = 1;
                System.Net.ServicePointManager.Expect100Continue = false;
                SecurityProtocolType origSecurityProtocol = System.Net.ServicePointManager.SecurityProtocol;
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                // post data is sent as a stream
                StreamWriter myWriter = null;
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(post_string);
                myWriter.Close();

                // returned values are returned as a stream, then read into a string
                String post_response;
                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
                {
                    post_response = responseStream.ReadToEnd();
                    responseStream.Close();
                }
                objCommonerrorlog.InsertErrorLog("PaymentMode", "Invalid Transaction", post_response);
                // the response string is broken into an array
                // The split character specified here must match the delimiting character specified above
                string[] response_array = post_response.Split('|');
                string transactionId = Convert.ToString(response_array[6]);
                if (objPyamentDetails.isDoctorInSystem == true)
                {
                    if (response_array != null && response_array[0] == "1")
                    {
                        string resp = CallSubscription(objPyamentDetails, transactionId);
                        if (!string.IsNullOrEmpty(resp) && resp == "Done")
                        {
                            strRecurring = "Recurring is done successfully.";
                        }
                        
                        DataTable dt = ObjColleaguesData.GetDoctorDetailsByEmail(objPyamentDetails.Email);
                        int userid = Convert.ToInt32(dt.Rows[0]["UserId"]);
                        mdlCommon objCommon = new mdlCommon();                        
                        objPyamentDetails.PackageStartDate = System.DateTime.Now;
                        objPyamentDetails.PackageEndDate = objCommoncls.GetPackageEndDate(objPyamentDetails.Planes);

                        ObjColleaguesData.InsertTransactionDetails(Convert.ToInt32(userid), objPyamentDetails.PackageStartDate, objPyamentDetails.PackageEndDate, Convert.ToDecimal(amount), true, objPyamentDetails.CardType, objPyamentDetails.CardNumber, objPyamentDetails.expdate, response_array[6], objPyamentDetails.Cvv, true);
                        ObjColleaguesData.UpdatePaymentStatusOfExistingUser(Convert.ToInt32(userid), true);
                        string CardNumberWithMask = objPyamentDetails.CardNumber;

                        CardNumberWithMask = CardNumberWithMask.Substring(CardNumberWithMask.Length - 4).PadLeft(CardNumberWithMask.Length, '*');

                        string CardType = objCommon.GetCardType(objPyamentDetails.CardType);
                        ObjTemplate.AccountUpgradedSuccessfully(Convert.ToInt32(userid), CardNumberWithMask, CardType, response_array[6], objPyamentDetails.Amount);
                        SessionManagement.UserId = Convert.ToString(dt.Rows[0]["UserId"]);
                        SessionManagement.FirstName = Convert.ToString(dt.Rows[0]["FirstName"]);
                        SessionManagement.UserPaymentVerified = PaymentBLL.CheckUserPayment(Convert.ToInt32(SessionManagement.UserId));
                        string ImageName = "";
                        ImageName = Convert.ToString(dt.Rows[0]["ImageName"]);
                        if (string.IsNullOrEmpty(ImageName))
                        {
                            SessionManagement.DoctorImage = Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]);
                        }
                        else
                        {
                            SessionManagement.DoctorImage = Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]) + ImageName;
                        }
                        Session["DoctorImage"] = SessionManagement.DoctorImage;


                        TempData["Paymentstatus"] = response_array[3];
                        if (response_array[0] == "1")
                        {
                            TempData["TransactionId"] = response_array[6];
                        }

                    }
                    else
                    {
                        ObjTemplate.PaymentFailed(objPyamentDetails.Email, objPyamentDetails.Firstname, objPyamentDetails.Lastname, objPyamentDetails.Phone, response_array[3], Request.Url.AbsoluteUri);
                        TempData["Paymentstatus"] = response_array[3];
                    }
                    TempData["userdetails"] = objPyamentDetails;
                    return RedirectToAction("Index", new { });
                }

                if (SessionManagement.UserId != null && SessionManagement.UserId != "")
                {
                    if (response_array != null && response_array[0] == "1")
                    {

                        mdlCommon objCommon = new mdlCommon();                        
                        string resp = CallSubscription(objPyamentDetails, transactionId);
                        if (!string.IsNullOrEmpty(resp) && resp == "Done")
                        {
                            strRecurring = "Recurring is done successfully.";
                            TempData["strRecurring"] = strRecurring;
                        }
                        SessionManagement.UserPaymentVerified = PaymentBLL.CheckUserPayment(Convert.ToInt32(SessionManagement.UserId));
                        objPyamentDetails.PackageStartDate = System.DateTime.Now;
                        objPyamentDetails.PackageEndDate = objCommoncls.GetPackageEndDate(objPyamentDetails.Planes);

                        ObjColleaguesData.InsertTransactionDetails(Convert.ToInt32(Session["UserId"]), objPyamentDetails.PackageStartDate, objPyamentDetails.PackageEndDate, Convert.ToDecimal(amount), true, objPyamentDetails.CardType, objPyamentDetails.CardNumber, objPyamentDetails.expdate, response_array[6], objPyamentDetails.Cvv, true);
                        ObjColleaguesData.UpdatePaymentStatusOfExistingUser(Convert.ToInt32(Session["UserId"]), true);
                        string CardNumberWithMask = objPyamentDetails.CardNumber;

                        CardNumberWithMask = CardNumberWithMask.Substring(CardNumberWithMask.Length - 4).PadLeft(CardNumberWithMask.Length, '*');

                        string CardType = objCommon.GetCardType(objPyamentDetails.CardType);
                        ObjTemplate.AccountUpgradedSuccessfully(Convert.ToInt32(Session["UserId"]), CardNumberWithMask, CardType, response_array[6], objPyamentDetails.Amount);
                        TempData["Paymentstatus"] = response_array[3];
                        if (response_array[0] == "1")
                        {
                            TempData["TransactionId"] = response_array[6];
                        }
                    }
                    else
                    {
                        ObjTemplate.PaymentFailed(objPyamentDetails.Email, objPyamentDetails.Firstname, objPyamentDetails.Lastname, objPyamentDetails.Phone, response_array[3], Request.Url.AbsoluteUri);
                        TempData["Paymentstatus"] = response_array[3];
                    }
                    return RedirectToAction("UpgradeAccount", "FreeToPaid");
                }
                else if (response_array != null)
                {
                    if (response_array[0] == "1")
                    {


                        string Email = objPyamentDetails.Email;
                        string Name = objPyamentDetails.Firstname + " " + objPyamentDetails.Lastname;
                        // string Phone = string.Empty;



                        #region Doctor sign up in system


                        string Password = objCommon.CreateRandomPassword(8);
                        string EncPassword = ObjTripleDESCryptoHelper.encryptText(Password);
                        string Accountkey = Crypto.SHA1(Convert.ToString(DateTime.Now.Ticks));
                        //Doctor sign up

                        Person person = GetSocialMediaPerson(Email); //Get Social Media detail


                        string SocialMedia = "";
                        string NameforTemplate = "";
                        string PhoneForTemplate = "";

                        string fullName = Name;
                        if (!string.IsNullOrEmpty(fullName))
                        {
                            if (fullName.Contains(" "))
                            {
                                var names = fullName.Split(' ');
                                string firstName = names[0];
                                string lastName = names[1];

                                person.FirstName = firstName;
                                person.LastName = lastName;
                            }
                            else
                            {
                                person.FirstName = fullName;
                            }
                            NameforTemplate = "Name: " + fullName;
                        }
                        else
                        {
                            person.FirstName = fullName;
                        }

                        if (!string.IsNullOrEmpty(objPyamentDetails.Phone))
                        {
                            PhoneForTemplate = "Phone: " + objPyamentDetails.Phone;
                        }


                        SocialMedia = NameforTemplate + "<br/>" + PhoneForTemplate + "<br/>" + person.FacebookURL + "<br/>" + person.TwitterURL + "<br/>" + person.LinkedInURL;


                        string companywebsite = "";
                        if (Session["CompanyWebsite"] != null)
                        {
                            companywebsite = Session["CompanyWebsite"].ToString();
                        }

                        int NewUserId = 0;
                        // Here we put code for sign up doctor in sytem or Temp table
                        //string resp = CallSubscription(objPyamentDetails, response_array[6]);
                        //if (!string.IsNullOrEmpty(resp))
                        //{
                        //    strRecurring = "Recurring is done successfully.";
                        //    TempData["strRecurring"] = strRecurring;                            
                        //}
                        DataTable dttemp = ObjColleaguesData.GetDoctorFromTempByEmail(Email);

                        if (dttemp.Rows.Count > 0)
                        {

                        }
                        else
                        {
                            NewUserId = ObjColleaguesData.Insert_Temp_Signup(Email, EncPassword, false, person.ImageUrl, person.FirstName, person.LastName, person.Age, person.Gender, person.Location, person.Company, person.Title, person.FacebookURL, person.TwitterURL, person.LinkedInURL, person.InfluenceScore, objPyamentDetails.Phone, person.Description);
                            objPyamentDetails.Userid = Convert.ToString(NewUserId);
                            string resp = CallSubscription(objPyamentDetails, response_array[6]);
                            if (!string.IsNullOrEmpty(resp))
                            {
                                strRecurring = "Recurring is done successfully.";
                                TempData["strRecurring"] = strRecurring;
                            }
                        }
                        objPyamentDetails.PackageStartDate = System.DateTime.Now;



                        objPyamentDetails.PackageEndDate = objCommoncls.GetPackageEndDate(objPyamentDetails.Planes);
                        ObjColleaguesData.InsertTransactionDetails((NewUserId == 0 ? Convert.ToInt32(dttemp.Rows[0]["id"]) : NewUserId), objPyamentDetails.PackageStartDate, objPyamentDetails.PackageEndDate, Convert.ToDecimal(amount), true, objPyamentDetails.CardType, objPyamentDetails.CardNumber, objPyamentDetails.expdate, response_array[6], objPyamentDetails.Cvv, false);
                        if (dttemp.Rows.Count > 0)
                        {
                            return RedirectToAction("Index", "User", new { UserName = Email, Password = EncPassword });
                        }

                        if (NewUserId != 0)
                        {

                            string CardNumberWithMask = objPyamentDetails.CardNumber;

                            CardNumberWithMask = CardNumberWithMask.Substring(CardNumberWithMask.Length - 4).PadLeft(CardNumberWithMask.Length, '*');

                            string CardType = objCommoncls.GetCardType(objPyamentDetails.CardType);
                            ObjTemplate.SignUpWithPaidAccountMail(Email, Name, companywebsite + "/User/Index?UserName=" + Email + "&Password=" + EncPassword, Password, companywebsite, CardNumberWithMask, CardType, response_array[6], objPyamentDetails.Amount); // send tempate to admin if doctor sing up in system

                            ObjTemplate.SignUpEMail(Email, Name, objPyamentDetails.Phone, person.FacebookURL, person.TwitterURL, person.LinkedInURL);// send tempate to admin if doctor sing up in system

                        }
                        TempData["TransactionId"] = response_array[6];
                        TempData["Paymentstatus"] = response_array[3];
                        TempData["userdetails"] = objPyamentDetails;
                        return RedirectToAction("Index");
                        //  return RedirectToAction("Index", "User", new { valid = "Signup" });

                        #endregion

                    }
                    else
                    {
                        ObjTemplate.PaymentFailed(objPyamentDetails.Email, objPyamentDetails.Firstname, objPyamentDetails.Lastname, objPyamentDetails.Phone, response_array[3], Request.Url.AbsoluteUri);
                        TempData["userdetails"] = objPyamentDetails;
                        TempData["Paymentstatus"] = response_array[3];
                        return RedirectToAction("Index", "FreeToPaid", new { Invalid = "Invalid" });
                    }
                }


                // individual elements of the array could be accessed to read certain response
                // fields.  For example, response_array[0] would return the Response Code,
                // response_array[2] would return the Response Reason Code.
                // for a list of response fields, please review the AIM Implementation Guide
            }
            catch(Exception ex)
            {
                throw;
            }
            return RedirectToAction("Index", "FreeToPaid", new { Invalid = "Invalid" });
        }
        protected Person GetSocialMediaPerson(string email)
        {
            try
            {
                //connect to fliptop and pull the information down
                SocialMedia sm = new SocialMedia();
                Person person = sm.LookUpPersonfullcontact(email);

                return person;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string CallSubscription(PyamentDetails objPyamentDetails, string transactionId)
        {
            RecurringSubscription objRecurringSubscription = new RecurringSubscription();
            clsColleaguesData objcls = new clsColleaguesData();
            TripleDESCryptoHelper objtriple = new TripleDESCryptoHelper();
            mdlCommon objCommon = new mdlCommon();
            int memberId = Convert.ToInt32(objPyamentDetails.Userid);
            int subId = 0;
            int membershipId = 0;
            bool hasSubscription = false;
            string createResponse = string.Empty;
            string updateResponse = string.Empty;
            string cancelResponse = string.Empty;
            string strReturn = string.Empty;            
            DataTable dt = objcls.getNewPaymentHistoryOnUserId(memberId);            
            if (dt != null && dt.Rows.Count > 0)
            {
                membershipId = Convert.ToInt32(dt.Rows[0]["membershipId"]);
                subId = Convert.ToInt32(dt.Rows[0]["subscriptionId"]);
                hasSubscription = true;
            }

            if (hasSubscription)
            {
                //if (Convert.ToInt32(objPyamentDetails.Planes) > membershipId || Convert.ToInt32(objPyamentDetails.Planes) < membershipId)
                //{
                    cancelResponse = objRecurringSubscription.cancelSubscriptionBySystem(Convert.ToInt32(subId));
                    bool isCancelUpdate = false;
                    if (!string.IsNullOrEmpty(cancelResponse))
                    {
                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.LoadXml(cancelResponse);

                        XmlNodeList grandelemList = xmlDoc.GetElementsByTagName("messages");                        
                        if (grandelemList.Count > 0)
                        {
                            string code = grandelemList[0]["resultCode"].InnerText;
                            if (code == "Ok")
                            {
                                clsColleaguesData objCCData = new clsColleaguesData();
                                var isUpdate = objCCData.UpdateSubscriptionStatus(memberId, Convert.ToString(subId));
                                isCancelUpdate = true;
                            }
                        }
                    }
                    if (isCancelUpdate)
                    {
                        createResponse = objRecurringSubscription.createSubscriptionBySystem(objPyamentDetails);                        
                    }
                //}
                //else
                //{
                //    updateResponse = objRecurringSubscription.UpdateSubscriptionBySystem(objPyamentDetails, Convert.ToString(subId));
                //    if (!string.IsNullOrEmpty(updateResponse))
                //    {
                //        XmlDocument xmlDoc = new XmlDocument();
                //        xmlDoc.LoadXml(updateResponse);

                //        XmlNodeList grandelemList = xmlDoc.GetElementsByTagName("messages");
                //        XmlNodeList elemList = xmlDoc.GetElementsByTagName("message");
                //        XmlNodeList getSubscriptionName = xmlDoc.GetElementsByTagName("subscriptionId");
                //        XmlNodeList getCustomerProfile = xmlDoc.GetElementsByTagName("profile");
                //        if (grandelemList.Count > 0)
                //        {

                //            string code = grandelemList[0]["resultCode"].InnerText;
                //            if (code != "Ok")
                //            {
                //                ObjTemplate.RecurringPaymentFailed(objPyamentDetails.Email, objPyamentDetails.Firstname, objPyamentDetails.Lastname, objPyamentDetails.Phone, elemList[0]["text"].InnerText);
                //            }
                //            else
                //            {
                //                if (getSubscriptionName.Count > 0 && getCustomerProfile.Count > 0)
                //                {
                //                    string customerId = getCustomerProfile[0]["customerProfileId"].InnerText;
                //                    string subscriptionId = getSubscriptionName[0].InnerText;
                //                    clsColleaguesData objCCData = new clsColleaguesData();
                //                    string amount = objPyamentDetails.Amount.Replace('$', ' ');
                //                    string suscriptionType = objPyamentDetails.payInterval;
                //                    var isInsert = objCCData.NewInsertUsertransactionDetails(memberId, amount, transactionId, Convert.ToString(membershipId), customerId, DateTime.Now, suscriptionType, subscriptionId);
                //                    objCCData.UpdateMembershipByRecurring(objPyamentDetails.Userid, objPyamentDetails.Planes);
                //                    strReturn = "Done";
                //                }
                //            }
                //        }
                //    }
                //}
            }
            else
            {
                createResponse = objRecurringSubscription.createSubscriptionBySystem(objPyamentDetails);
            }
            if (!string.IsNullOrEmpty(createResponse))
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(createResponse);

                XmlNodeList grandelemList = xmlDoc.GetElementsByTagName("messages");
                XmlNodeList elemList = xmlDoc.GetElementsByTagName("message");
                XmlNodeList getSubscriptionName = xmlDoc.GetElementsByTagName("subscriptionId");
                XmlNodeList getCustomerProfile = xmlDoc.GetElementsByTagName("profile");
                if (grandelemList.Count > 0)
                {
                    string code = grandelemList[0]["resultCode"].InnerText;
                    if (code != "Ok")
                    {
                        ObjTemplate.RecurringPaymentFailed(objPyamentDetails.Email, objPyamentDetails.Firstname, objPyamentDetails.Lastname, objPyamentDetails.Phone, elemList[0]["text"].InnerText);
                    }
                    else
                    {
                        if (getSubscriptionName.Count > 0 && getCustomerProfile.Count > 0)
                        {
                            string customerId = getCustomerProfile[0]["customerProfileId"].InnerText;
                            string subscriptionId = getSubscriptionName[0].InnerText;
                            clsColleaguesData objCCData = new clsColleaguesData();
                            string amount = objPyamentDetails.Amount;
                            string suscriptionType = objPyamentDetails.payInterval;
                            var isInsert = objCCData.NewInsertUsertransactionDetails(memberId, amount, transactionId, objPyamentDetails.Planes, customerId, DateTime.Now, suscriptionType, subscriptionId);
                            objCCData.UpdateMembershipByRecurring(objPyamentDetails.Userid, objPyamentDetails.Planes);
                            PaymentBLL.AddPaymentBlockingHistory(Convert.ToInt32(objPyamentDetails.Userid), false, null);
                            strReturn = "Done";
                        }
                    }
                }                
            }
            return strReturn;
        }       

        public string CheckUserExistInSystem(string Email)
        {
            string Status = "";
            DataTable dtCheckEmail = new DataTable();
            dtCheckEmail = ObjColleaguesData.CheckEmailInSystem(Email);
            DataTable dtCheck = ObjColleaguesData.CheckEmailExistsAsDoctor(Email);
            if (dtCheckEmail.Rows.Count > 0 || dtCheck.Rows.Count > 0)
            {
                Status = "1";
            }
            return Status.ToString();
        }

        public JsonResult GetDoctorDetailsByEmail(string email)
        {
            DataTable dt = ObjColleaguesData.GetDoctorDetailsByEmail(email);
            string fname = string.Empty;
            string lname = string.Empty;
            string address = string.Empty;
            string phoneNo = string.Empty;
            string city = string.Empty;
            string state = string.Empty;
            string zip = string.Empty;

            if (dt.Rows.Count > 0)
            {
                fname = Convert.ToString(dt.Rows[0]["FirstName"]);
                lname = Convert.ToString(dt.Rows[0]["LastName"]);
                address = Convert.ToString(dt.Rows[0]["ExactAddress"]);
                phoneNo = Convert.ToString(dt.Rows[0]["Phone"]);
                city = Convert.ToString(dt.Rows[0]["City"]);
                state = Convert.ToString(dt.Rows[0]["State"]);
                zip = Convert.ToString(dt.Rows[0]["ZipCode"]);

                return Json(new { FName = fname, LName = lname, Phone = phoneNo, Add = address, City = city, State = state, Zip = zip, isDoctorInsystem = true, status = "1" }, JsonRequestBehavior.AllowGet);
            }
            else
            {

                dt = ObjColleaguesData.GetDoctorFromTempByEmail(email);
                if (dt.Rows.Count > 0)
                {
                    fname = Convert.ToString(dt.Rows[0]["first_name"]);
                    lname = Convert.ToString(dt.Rows[0]["last_name"]);
                    phoneNo = Convert.ToString(dt.Rows[0]["phoneno"]);

                    return Json(new { FName = fname, LName = lname, Phone = phoneNo, status = "2" }, JsonRequestBehavior.AllowGet);
                }
            }
            return Json("", JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult GetPackagesAd(string packageid)
        {
            Features objFeatures = new Features();
            PaymentPackage objPayPack = new PaymentPackage();
            MemberShipBLL objMembll = new MemberShipBLL();
            Membership_FeatureBLL objMemFeature = new Membership_FeatureBLL();

            Membership objMembership = objMembll.GetById(Convert.ToInt32(packageid));
            objPayPack.memshipamt = Convert.ToString(objMembership.MonthlyPrice);
            objPayPack.memshipid = Convert.ToString(objMembership.Membership_Id);
            objPayPack.memshipname = Convert.ToString(objMembership.Title);
            var lst = objMemFeature.GetMembershipFeaturesDetails(Convert.ToInt32(packageid));
            objPayPack.lstfeatures = lst.lstFeatures;
            return View(objPayPack);
        }

        [HttpPost]
        public ActionResult ShowUpgradeAccount()
        {
            return RedirectToAction("UpgradeAccount", "FreeToPaid");
        }
    }
}
