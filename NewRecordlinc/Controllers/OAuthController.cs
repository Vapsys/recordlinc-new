﻿using BO.Models;
using BusinessLogicLayer;
using MailBee;
using MailBee.SmtpMail;
using NewRecordlinc.App_Start;
using System;
using System.Web.Mvc;
using NewRecordlinc.Models.Config;


namespace NewRecordlinc.Controllers
{
    public class OAuthController : Controller
    {       
        // GET: OAuth
        public ActionResult Index()
        {
            try
            {
                var strToken = GetAccessTokenBLL.GoogleAccessToken(Convert.ToString(Request["code"]),Configurations.gclientID,Configurations.gclientSecret,Configurations.gredirectUri);
                string user = (string)Session["AuthEmail"];
                int pid = (int)Session["providerId"];
                bool IsInserted = false;
                var IsValid = EmailProviderBLL.CheckEmailAlreadyExists(user, pid, SessionManagement.UserId);
                if (!IsValid)
                {
                    EmailProviderDetails epd = new EmailProviderDetails();
                    epd.accessToken = Convert.ToString(strToken[0]);
                    if (strToken.Count > 1)
                    {
                        epd.refreshToken = Convert.ToString(strToken[1]);
                    }
                    else {
                        epd.refreshToken = string.Empty;
                    }
                    epd.createdAt = DateTime.Now;
                    epd.emailAddress = user;
                    epd.isDefault = false;
                    epd.memberId = Convert.ToInt32(SessionManagement.UserId);
                    epd.password = string.Empty;
                    epd.providerId = (int)Session["providerId"];
                    epd.requireSSL = false;
                    epd.sendEnabled = true;
                    epd.smtpPortNumber = 0;
                    epd.smtpServerName = string.Empty;
                    epd.syncEnabled = false;
                    IsInserted = EmailProviderBLL.InsertEmailProviderDetails(epd);
                    if (IsInserted)
                    {
                        TempData["OAuthDone"] = true;
                        TempData["OauthMsg"] = "Record is inserted successfully. OAuth process is completed.";
                        return RedirectToAction("Index", "EmailOauth");
                    }
                    else
                    {
                        TempData["OAuthDone"] = false;
                        TempData["OauthMsg"] = "Record is not inserted successfully. Please try again.";
                        return RedirectToAction("Index", "EmailOauth");
                    }
                }
                else
                {
                    TempData["OAuthDone"] = false;
                    TempData["OauthMsg"] = "Same account information is already exists in the system. Please check details and try again.";
                    return RedirectToAction("Index", "EmailOauth");
                }
                                                                                           
            }
            catch (Exception ex)
            {
                throw;
            }            
        }             

        [Route("signin-microsoft")]
        public ActionResult OAuthRedirectURL()
        {
            try
            {
                string user = (string)Session["AuthEmail"];
                var accessToken = GetAccessTokenBLL.OutlookAccessToken(Configurations.oclientID, Configurations.oclientSecret, Convert.ToString(Request["code"]), Configurations.oredirectUri);

                EmailProviderDetails epd = new EmailProviderDetails();
                epd.accessToken = accessToken[0];
                if (accessToken.Count > 1)
                {
                    epd.refreshToken = Convert.ToString(accessToken[1]);
                }
                else {
                    epd.refreshToken = string.Empty;
                }
                epd.createdAt = DateTime.Now;
                epd.emailAddress = user;
                epd.isDefault = false;
                epd.memberId = Convert.ToInt32(SessionManagement.UserId);
                epd.password = string.Empty;
                epd.providerId = (int)Session["providerId"];
                epd.requireSSL = false;
                epd.sendEnabled = true;
                epd.smtpPortNumber = 0;
                epd.smtpServerName = string.Empty;
                epd.syncEnabled = false;
                bool IsInserted = EmailProviderBLL.InsertEmailProviderDetails(epd);
                if (IsInserted)
                {
                    TempData["OAuthDone"] = true;
                    TempData["OauthMsg"] = "Record is inserted successfully. OAuth process is completed.";
                    return RedirectToAction("Index", "EmailOauth");
                }
                else
                {
                    TempData["OAuthDone"] = false;
                    TempData["OauthMsg"] = "Record is not inserted successfully. Please try again.";
                    return RedirectToAction("Index", "EmailOauth");
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }               
    }
}