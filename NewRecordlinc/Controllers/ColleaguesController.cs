using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NewRecordlinc.Models.Colleagues;
using NewRecordlinc.App_Start;
using System.Text;
using NewRecordlinc.Models.Common;
using NewRecordlinc.Models.Patients;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.PatientsData;
using DataAccessLayer.Common;
using System.Web.Configuration;
using System.IO;
using System.Data;
using System.Data.OleDb;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using Excel;
using BusinessLogicLayer;
using BO.Models;
using BO.ViewModel;
namespace NewRecordlinc.Controllers
{
    public class ColleaguesController : Controller
    {
        //
        // GET: /Colleagues/

        mdlCommon MdlCommon = null;
        mdlColleagues MdlColleagues = null;
        mdlPatient MdlPatient = null;
        clsTemplate ObjTemplate = new clsTemplate();
        string FileNameNew = "";
        clsColleaguesData objColleaguesData = new clsColleaguesData();
        clsCommon ObjCommon = new clsCommon();

        int PageSize = Convert.ToInt32(WebConfigurationManager.AppSettings["PageSize"]);

        public ActionResult Index(int PageIndex = 1)
        {
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {

                bool Result = ObjCommon.Temp_RemoveAllByUserId(Convert.ToInt32(Session["UserId"]), clsCommon.GetUniquenumberForAttachments());
                clsRestriction objRestriction = new clsRestriction();
                int Count = objRestriction.GetCountOfColleaguesByUserId(Convert.ToInt32(SessionManagement.UserId));
                ViewBag.Count = Count;
                return View("Index2");
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        [HttpPost]
        public string SearchPatientFromPopUp(string SearchText)
        {
            return GetPatientListForSendReferralPopUp(SearchText);
        }

        [HttpPost]
        public ActionResult NewSearchPatientFromPopUp(string SearchText)
        {
            string searchValue = "Find patient to refer...";
            if (!string.IsNullOrEmpty(SearchText))
            {
                searchValue = SearchText;
            }
            MdlPatient = new mdlPatient();
            MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), 1, 30, SearchText, 11, 2, null, 0);
            return View("_PartialPatientItem", MdlPatient.lstPatientDetailsOfDoctor);
        }

        public ActionResult NewSearchPatientFromItem(string SearchText)
        {
            string searchValue = "Find patient to refer...";
            if (!string.IsNullOrEmpty(SearchText))
            {
                searchValue = SearchText;
            }
            MdlPatient = new mdlPatient();
            MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), 1, 30, SearchText, 11, 2, null, 0);
            return View("_PartialSearchColleagueItem", MdlPatient.lstPatientDetailsOfDoctor);
        }


        public string GetPatientListForSendReferralPopUp(string SearchText)
        {
            StringBuilder sb = new StringBuilder();
            string searchValue = "Find patient to refer...";
            if (!string.IsNullOrEmpty(SearchText))
            {
                searchValue = SearchText;
            }
            try
            {
                MdlPatient = new mdlPatient();
                MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), 1, 30, SearchText, 11, 2, null, 0);

                //sb.Append("<style>#Checkallarea input[type=checkbox]{display: none;}</style>");
                sb.Append(" <h2 id=\"TitleRefer\" class=\"title\">Select Patient to Refer</h2>");
                sb.Append(" <div class=\"search_main_div\"><input style=\"display:none\" id=\"SearchBtnHide\" class=\"search_btn_popup\" name=\"Cancel\" type=\"button\" value=\"Cancel\" onclick=\"SearchPatient()\"><div class=\"patient_searchbar\"><input  onfocus=\"if (this.value =='Find patient to refer...') {this.value = '';}\"  id=\"txtSearchPatient\" onkeyup=\"SearchPatientOnKeyup()\" onblur=\"if (this.value == '') {this.value = 'Find patient to refer...';}\"  type=\"text\" value=\"" + searchValue + "\" name=\"\"></div></div>");
                sb.Append("<div id=\"ColleagueLocation\"></div>");
                sb.Append("<div id=\"Patientcallonscroll\" class=\"cntnt\"><input type=\"hidden\" id=\"LocationID\" value=\"\">");


                if (MdlPatient.lstPatientDetailsOfDoctor.Count > 0)
                {
                    sb.Append("<ul id=\"ScondIdScroll\" class=\"search_listing\" style=\"width:100%;\">");


                    foreach (var item in MdlPatient.lstPatientDetailsOfDoctor.Take(500))
                    {
                        sb.Append("<li><div class=\"clearfix\" id=\"" + item.PatientId + "\" style=\"display:block !important\" > ");
                        sb.Append("<span class=\"check\"><input type=\"checkbox\" name=\"\" class=\"checkbox_add_patient\" id=\"chkPatient\" value=\"" + item.PatientId + "\"></span>");
                        sb.Append("<span class=\"thumb\"><img style=\"height: 95px;width: 85px;\" alt=\"\" src='" + ((!string.IsNullOrEmpty(item.ImageName) ? item.ImageName : (item.Gender.ToString() == "Female") ? System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"] : System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorImage"])) + "' ></span> <span class=\"description\"><h2><a  style=\"word-wrap: break-word;display:list-item;\" href=\"javascript:;\" onclick=\"ViewPatientHistory('" + item.PatientId + "');\">" + item.FirstName + "&nbsp;" + item.LastName + "</a></h2></span>");
                        sb.Append("<span class=\"clear\"></span></div></li>");
                    }
                    sb.Append("</ul><div class=\"clear\"></div><br /><div id=\"EmptyRecord\"></div><div class=\"actions\"><a href=\"JavaScript:;\" onclick=\"ReferralSend()\" ><img alt=\"\" class=\"btn-popup pull-right\" src=\"../../Content/images/btn-next.png\"/></a><input class=\"New_cancel_btn btn-popup\" name=\"Cancel\" type=\"submit\" onclick=\"CloseEditPatientListpop()\" value=\"Cancel\" style=\"width:155px; height:49px;float:left;\"><div class=\"clear\">");
                    //sb.Append("</ul><div class=\"clear\"></div><br /><div id=\"EmptyRecord\"></div><div class=\"actions\"><a href=\"JavaScript:;\" onclick=\"ReferralSend()\" ><img alt=\"\" style=\"width:140px; height:41px;margin-bottom: 3px;\" src=\"../../Content/images/btn-next.png\"/></a><input class=\"New_cancel_btn\" name=\"Cancel\" type=\"submit\" onclick=\"CloseEditPatientListpop()\" value=\"Cancel\" style=\"width:140px; height:38px;\"><div class=\"clear\">");
                }
                else
                {
                    sb.Append("<center>Patient not found. Please<a href=\"" + Url.Action("AddPatient", "Patients") + "\" style=\"color: #0890c4;\"> click here</a> to add new patient.</center>");
                }

                sb.Append("</div><button class=\"mfp-close\" type=\"button\" title=\"Close (Esc)\">×</button>");
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }


        public string SearchPatientListForSendReferralPopUpLI(string SearchText, string pageindex, string pagesize)
        {

            return GetPatientListForSendReferralPopUpLI(SearchText, int.Parse(pageindex), int.Parse(pagesize));
        }

        //Get Only LI Containt 
        public string GetPatientListForSendReferralPopUpLI(string SearchText, int pageindex, int pagesize)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                MdlPatient = new mdlPatient();
                MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), pageindex, pagesize, SearchText, 11, 2, null, 0);

                if (MdlPatient.lstPatientDetailsOfDoctor.Count > 0)
                {
                    foreach (var item in MdlPatient.lstPatientDetailsOfDoctor)
                    {
                        sb.Append("<li><div class=\"clearfix\">");
                        sb.Append("<span class=\"check\"><input type=\"checkbox\" name=\"\" class=\"checkbox_add_patient\" id=\"chkPatient\" value=\"" + item.PatientId + "\"></span>");
                        sb.Append("<span class=\"thumb\"><img style=\"height: 95px;width: 85px;\" alt=\"\" src='" + ((!string.IsNullOrEmpty(item.ImageName) ? item.ImageName : (item.Gender.ToString() == "Female") ? System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorFemaleImage"] : System.Configuration.ConfigurationManager.AppSettings["DefaultDoctorImage"])) + "' ></span> <span class=\"description\"><h2><a  style=\"word-wrap: break-word;\" href=\"javascript:;\" onclick=\"ViewPatientHistory('" + item.PatientId + "');\">" + item.FirstName + "&nbsp;" + item.LastName + "</a></h2></span>");
                        sb.Append("<span class=\"clear\"></span></div></li>");
                    }
                }
                else if (pageindex == 1)
                {
                    sb.Append("<center>No Record Found</center>");
                }
                else
                {
                    sb.Append("<center>No More Record Found</center>");
                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }
        // Get Patient list on scroll
        public ActionResult NewGetPatientListForSendReferralPopUpLI(string SearchText, int pageindex, int pagesize)
        {
            MdlPatient = new mdlPatient();
            MdlPatient.lstPatientDetailsOfDoctor = MdlPatient.GetPatientDetailsOfDoctor(Convert.ToInt32(Session["UserId"]), pageindex, pagesize, SearchText, 11, 2, null, 0);
            return View("_PartialSearchColleagueItem", MdlPatient.lstPatientDetailsOfDoctor);
        }


        #region Code for Colleague list
        //[HttpPost]
        public JsonResult ColleaguesList(string PageIndex, string SearchText, string SortColumn, string SortDirection, string NewSearchText, string FilterBy)
        {
            if (NewSearchText == "ALL")
            {
                NewSearchText = string.Empty;
            }
            return Json(ColleagueListOfDoctor(Convert.ToInt32(PageIndex), SearchText, Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection), NewSearchText, Convert.ToInt32(FilterBy)), JsonRequestBehavior.AllowGet);
        }


        public string ColleagueListOfDoctor(int PageIndex, string Searchtext, int SortColumn, int SortDirection, string NewSearchText, int FilterBy)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                MdlColleagues = new mdlColleagues();
                MdlColleagues.lstColleaguesDetails = MdlColleagues.ColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, PageSize, Searchtext, SortColumn, SortDirection, NewSearchText, FilterBy);


                if (MdlColleagues.lstColleaguesDetails.Count > 0)
                {
                    double TotalPages = Math.Ceiling((double)MdlColleagues.lstColleaguesDetails.ElementAt(0).TotalRecord / PageSize);
                    sb.Append("<div class=\"inner ohidden\">");
                    sb.Append("<div class=\"buttons_holder clearfix\">");
                    sb.Append("<input class=\"search_colleague\" onclick=\"location.href='" + Url.Action("SearchColleagues", "Colleagues") + "'\" name=\"Search Colleague\" type=\"button\" value=\"Search Colleague\">");
                    sb.Append("<input class=\"invite_email2\" onclick=\"location.href='" + Url.Action("InviteColleagues", "Colleagues") + "'\" name=\"Invite by Email\" type=\"button\" value=\"Invite by Email\">");
                    sb.Append("</div>");
                    sb.Append("<div><section>");
                    sb.Append("<select id=\"Listing\" class=\"cs-select cs-skin-elastic\">");
                    if (FilterBy == 1)
                        sb.Append("<option value=\"1\" selected>First Name</option>");
                    else
                        sb.Append("<option value=\"1\">First Name</option>");
                    if (FilterBy == 12)
                        sb.Append("<option value=\"12\" selected>Last Name</option>");
                    else
                        sb.Append("<option value=\"12\">Last Name</option>");
                    if (FilterBy == 10)
                        sb.Append("<option value=\"10\" selected>Email</option>");
                    else
                        sb.Append("<option value=\"10\">Email</option>");

                    sb.Append("</select>");
                    sb.Append("</section>");

                    //THis is A-Z button search result
                    sb.Append("<div class=\"alphabet\">");
                    sb.Append("<a class=\"first ALL\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "ALL" + "')\">ALL</a>");
                    sb.Append("<a class=\"A\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "A" + "')\">A</a>");
                    sb.Append("<a class=\"B\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "B" + "')\">B</a>");
                    sb.Append("<a class=\"C\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "C" + "')\">C</a>");
                    sb.Append("<a class=\"D\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "D" + "')\">D</a>");
                    sb.Append("<a class=\"E\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "E" + "')\">E</a>");
                    sb.Append("<a class=\"F\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "F" + "')\">F</a>");
                    sb.Append("<a class=\"G\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "G" + "')\">G</a>");
                    sb.Append("<a class=\"H\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "H" + "')\">H</a>");
                    sb.Append("<a class=\"I\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "I" + "')\">I</a>");
                    sb.Append("<a class=\"J\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "J" + "')\">J</a>");
                    sb.Append("<a class=\"K\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "K" + "')\">K</a>");
                    sb.Append("<a class=\"L\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "L" + "')\">L</a>");
                    sb.Append("<a class=\"M\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "M" + "')\">M</a>");
                    sb.Append("<a class=\"N\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "N" + "')\">N</a>");
                    sb.Append("<a class=\"O\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "O" + "')\">O</a>");
                    sb.Append("<a class=\"P\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "P" + "')\">P</a>");
                    sb.Append("<a class=\"Q\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "Q" + "')\">Q</a>");
                    sb.Append("<a class=\"R\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "R" + "')\">R</a>");
                    sb.Append("<a class=\"S\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "S" + "')\">S</a>");
                    sb.Append("<a class=\"T\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "T" + "')\">T</a>");
                    sb.Append("<a class=\"U\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "U" + "')\">U</a>");
                    sb.Append("<a class=\"V\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "V" + "')\">V</a>");
                    sb.Append("<a class=\"W\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "W" + "')\">W</a>");
                    sb.Append("<a class=\"X\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "X" + "')\">X</a>");
                    sb.Append("<a class=\"Y\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "Y" + "')\">Y</a>");
                    sb.Append("<a class=\"last Z\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "Z" + "')\">Z</a></div></div>");

                    sb.Append("<div class=\"table_data colleagues\" id=\"divMain\" style=\"display: block;\">");



                    sb.Append("<div style='display:inline-table;width:100%;height:56px;'><div style='width:40%;height:35px;'><h5 style=\"padding-top:35px !important;\">Colleagues</h5></div><div style='width:40%;float:right;'><span class=\"buttons\"><input name=\"Send to Selected\" id=\"editform_5\" class=\"send_to_selected edit popup-with-form\"  type=\"submit\"><input class=\"delete_selected\" name=\"\" type=\"submit\" id=\"btnMultipleDelete\" /></span></div></div>");


                    sb.Append("<div class=\"tbody word-wrap\">");
                    foreach (var item in MdlColleagues.lstColleaguesDetails)
                    {

                        DataTable dtReferralRece = new DataTable();
                        DataTable dtReferralSent = new DataTable();

                        dtReferralRece = objColleaguesData.GetReferralReceivedCountOfDoctor(Convert.ToInt32(Session["UserId"]), item.ColleagueId);
                        dtReferralSent = objColleaguesData.GetReferralSentCountOfDoctor(Convert.ToInt32(Session["UserId"]), item.ColleagueId);



                        sb.Append("<div class=\"trow\" id=\"row" + item.ColleagueId + "\">");
                        sb.Append("<div class=\"dataTableRow\">");
                        sb.Append("<input name=\"checkcolleague\" id=\"checkcolleague\" type=\"checkbox\" value=" + item.ColleagueId + ">");
                        sb.Append("<a data-role=\"ViewProfile\" data-value=\"" + item.ColleagueId + "\"> <figure class=\"thumb\"><img src='" + item.ImageName + "' alt='" + (item.FirstName != string.Empty && item.FirstName != null ? item.FirstName + " " : string.Empty) + (item.LastName != string.Empty && item.LastName != null ? item.LastName : string.Empty) + "'></figure></a> ");

                        sb.Append("<div class=\"dataTableCell cell1\"><a data-role=\"ViewProfile\" data-value=\"" + item.ColleagueId + "\" style=\"text-decoration:none;\"><div class=\"data\">");
                        sb.Append("<h3>" + item.LastName + ", " + (!string.IsNullOrWhiteSpace(item.MiddleName) ? " " + item.MiddleName + " " : " ") + "" + item.FirstName);
                        for (int j = 0; j < item.lstSpeacilitiesOfDoctor.Count; j++)
                        {
                            sb.Append("<span>" + item.lstSpeacilitiesOfDoctor.ElementAt(j).SpecialtyDescription + "</span>");
                        }
                        sb.Append("</h3></div></a></div>");




                        sb.Append("<div class=\"dataTableInner\">");
                        sb.Append("<div class=\"dataTableCell cell2\">");
                        sb.Append("<ul class=\"list list_ab\">");
                        if (item.Phone != "")
                        {
                            sb.Append("<li><i class=\"wphone\"></i>" + item.Phone + "</li>");
                        }
                        if (item.Fax != "")
                        {
                            sb.Append("<li><i class=\"fax\"></i>" + item.Fax + "</li>");
                        }
                        sb.Append("<li>");
                        if (item.ExactAddress != "" || item.Address2 != "" || item.City != "" && item.City != null || !string.IsNullOrWhiteSpace(item.State) || !string.IsNullOrWhiteSpace(item.ZipCode))
                        {
                            sb.Append("<i class=\"address\"></i> ");
                        }
                        if (item.ExactAddress != "" || item.Address2 != "")
                        {
                            sb.Append(item.ExactAddress);

                            sb.Append("," + item.Address2 + "<z>&nbsp;</z>");
                        }
                        if (item.City != "" && item.City != null)
                        {
                            sb.Append(item.City + "<z>,&nbsp;</z>");
                        }
                        if (!string.IsNullOrWhiteSpace(item.State) || !string.IsNullOrWhiteSpace(item.ZipCode))
                        {
                            sb.Append(item.State + "&nbsp;" + item.ZipCode + "&nbsp; </li>");
                        }

                        sb.Append("<li>");

                        sb.Append("<i class=\"email\"></i>" + item.EmailAddress);
                        if (item.SecondaryEmail != null && item.SecondaryEmail != "")
                        {
                            sb.Append(", " + item.SecondaryEmail);
                        }

                        sb.Append("</li>");

                        //   sb.Append("<li><a class=\"edit popup-with-form showlightbox\" href=\"#suggestion_box\" data-role=\"OpenSuggestionBox\" data-value=\"" + item.ColleagueId + "\"> <i class=\"edit\"></i>Suggest a change </a></li>");
                        sb.Append("</ul></div>");

                        sb.Append("<div class=\"dataTableCell cell3\">");
                        sb.Append("<ul class=\"list\">");
                        sb.Append("<li>Referrals Received (<a href=\"Javascript:;\"  data-role=\"SearchInboxFromColleageReferrals\"data-firstname=\"" + item.FirstName + "\" data-ColleagueId=\"" + item.ColleagueId + "\" data-email=\"" + item.EmailAddress + "\">" + dtReferralRece.Rows[0]["TotalRecord"] + "</a>)</li>");
                        sb.Append("<li>Referrals Sent (<a  href=\"Javascript:;\"  data-role=\"SearchSentboxFromColleageReferrals\" data-firstname=\"" + item.FirstName + "\" data-ColleagueId=\"" + item.ColleagueId + "\" data-email=\"" + item.EmailAddress + "\">" + dtReferralSent.Rows[0]["TotalRecord"] + "</a>)</li>");
                        sb.Append("</ul></div>");


                        sb.Append(" <div class=\"dataTableCell cell4\">");
                        sb.Append("  <a href=\"Javascript:;\"  title=\"Referral\" class=\"frmSendRefferal\" data-role=\"OpenPopup\" data-value=\"" + item.ColleagueId + "\" data-location=\"" + item.LocationId + "\"></a>");
                        sb.Append(" <a href=\"javascript:;\" data-role=\"SendMessage\" data-value=\"" + item.ColleagueId + "\" title=\"Mail\" class=\"frmSendEmail\"></a>");
                        sb.Append("  <a href=\"javascript:;\" title=\"Delete\" style=\"display: inline-block;float: right;margin: -36px 23px 0 0;\" class=\"frmdltbtn12\" data-pageindex=\"" + PageIndex + "\" data-role=\"RemoveColleague\" data-value=\"" + item.ColleagueId + "\"></a>");
                        sb.Append(" </div></div></div></div>  ");





                    }
                    sb.Append("</div>");
                    sb.Append("</div>");
                    sb.Append("</div>");


                }
                else
                {
                    //// code for no result |||| <d>
                    sb.Append("<div class=\"inner\">");
                    sb.Append("<div class=\"buttons_holder clearfix\">");
                    sb.Append("<input class=\"search_colleague\" onclick=\"location.href='" + Url.Action("SearchColleagues", "Colleagues") + "'\" name=\"Search Colleague\" type=\"button\" value=\"Search Colleague\">");
                    sb.Append("<input class=\"invite_email2\" onclick=\"location.href='" + Url.Action("InviteColleagues", "Colleagues") + "'\" name=\"Invite by Email\" type=\"button\" value=\"Invite by Email\">");
                    sb.Append("</div>");
                    sb.Append("<section>");
                    sb.Append("<select id=\"Listing\" class=\"cs-select cs-skin-elastic\">");
                    if (FilterBy == 1)
                        sb.Append("<option value=\"1\" selected>First Name</option>");
                    else
                        sb.Append("<option value=\"1\">First Name</option>");
                    if (FilterBy == 12)
                        sb.Append("<option value=\"12\" selected>Last Name</option>");
                    else
                        sb.Append("<option value=\"12\">Last Name</option>");
                    if (FilterBy == 10)
                        sb.Append("<option value=\"10\" selected>Email</option>");
                    else
                        sb.Append("<option value=\"10\">Email</option>");
                    //sb.Append("<option value=\"11\" data-class=\"flag-argentina\">Referred By</option>");

                    //sb.Append("<option value=\"5\" data-class=\"flag-safrica\">Location</option>");

                    sb.Append("</select>");
                    sb.Append("</section>");

                    //THis is A-Z button search result
                    sb.Append("<div class=\"alphabet\">");
                    sb.Append("<a class=\"first ALL\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "ALL" + "')\">ALL</a>");
                    sb.Append("<a class=\"A\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "A" + "')\">A</a>");
                    sb.Append("<a class=\"B\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "B" + "')\">B</a>");
                    sb.Append("<a class=\"C\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "C" + "')\">C</a>");
                    sb.Append("<a class=\"D\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "D" + "')\">D</a>");
                    sb.Append("<a class=\"E\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "E" + "')\">E</a>");
                    sb.Append("<a class=\"F\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "F" + "')\">F</a>");
                    sb.Append("<a class=\"G\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "G" + "')\">G</a>");
                    sb.Append("<a class=\"H\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "H" + "')\">H</a>");
                    sb.Append("<a class=\"I\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "I" + "')\">I</a>");
                    sb.Append("<a class=\"J\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "J" + "')\">J</a>");
                    sb.Append("<a class=\"K\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "K" + "')\">K</a>");
                    sb.Append("<a class=\"L\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "L" + "')\">L</a>");
                    sb.Append("<a class=\"M\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "M" + "')\">M</a>");
                    sb.Append("<a class=\"N\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "N" + "')\">N</a>");
                    sb.Append("<a class=\"O\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "O" + "')\">O</a>");
                    sb.Append("<a class=\"P\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "P" + "')\">P</a>");
                    sb.Append("<a class=\"Q\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "Q" + "')\">Q</a>");
                    sb.Append("<a class=\"R\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "R" + "')\">R</a>");
                    sb.Append("<a class=\"S\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "S" + "')\">S</a>");
                    sb.Append("<a class=\"T\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "T" + "')\">T</a>");
                    sb.Append("<a class=\"U\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "U" + "')\">U</a>");
                    sb.Append("<a class=\"V\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "V" + "')\">V</a>");
                    sb.Append("<a class=\"W\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "W" + "')\">W</a>");
                    sb.Append("<a class=\"X\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "X" + "')\">X</a>");
                    sb.Append("<a class=\"Y\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "Y" + "')\">Y</a>");
                    sb.Append("<a class=\"last Z\" href=\"Javascript:;\" onclick=\"ColleaguesListByFiltering('" + "Z" + "')\">Z</a></div>");

                    //sb.Append("<div style='display:inline-table;width:100%;height:56px;'>< div style = 'width:40%;height:35px;' >< h5 style=\"padding-top:35px !important;\">Colleagues<span class=\"buttons\"><input name=\"Send to Selected\" id=\"editform_5\" class=\"send_to_selected edit popup-with-form showlightbox\"  type=\"submit\"><input class=\"delete_selected\" name=\"\" type=\"submit\" id=\"btnMultipleDelete\" /></span></h5>");
                    sb.Append("<div style='display:inline-table;width:100%;height:56px;'><div style='width:40%;height:35px;'><h5 style=\"padding-top:35px !important;\">Colleagues</h5></div><div style='width:40%;float:right;'><span class=\"buttons\"><input name=\"Send to Selected\" id=\"editform_5\" class=\"send_to_selected edit popup-with-form showlightbox\"  type=\"submit\"><input class=\"delete_selected\" name=\"\" type=\"submit\" id=\"btnMultipleDelete\" /></span></div></div>");
                    sb.Append("<hr/><br/><center>No Record Found</center>");



                    sb.Append("</div>");
                    sb.Append("</div>");

                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }
        public List<ReferralDetails> GetRefferralsentDetailsofDoctor(int DoctorId, int ColleagueId)
        {
            try
            {
                clsColleaguesData clscolleaguesData = new clsColleaguesData();
                DataTable dt = new DataTable();
                List<ReferralDetails> lstreferral = new List<ReferralDetails>();
                dt = clscolleaguesData.GetRefferalsentCountandReferralDetails(DoctorId, ColleagueId);
                foreach (DataRow item in dt.Rows)
                {
                    lstreferral.Add(new ReferralDetails()
                    {
                        MessageId = Convert.ToInt32(item["MessageId"]),
                        PatientId = Convert.ToInt32(item["PatientId"]),
                        PatientName = Convert.ToString(item["PatientName"])
                    });

                }
                return lstreferral;

            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<ReferralDetails> GetReferralReceiveDetailsofDoctor(int DoctorId, int ColleagueId)
        {
            try
            {
                clsColleaguesData clscolleaguesData = new clsColleaguesData();
                DataTable dt = new DataTable();
                List<ReferralDetails> lstreferral = new List<ReferralDetails>();
                dt = clscolleaguesData.GetRefferalReceivedCountandReferralDetails(DoctorId, ColleagueId);
                foreach (DataRow item in dt.Rows)
                {
                    lstreferral.Add(new ReferralDetails()
                    {
                        MessageId = Convert.ToInt32(item["MessageId"]),
                        PatientId = Convert.ToInt32(item["PatientId"]),
                        PatientName = Convert.ToString(item["PatientName"])
                    });

                }
                return lstreferral;

            }
            catch (Exception)
            {

                throw;
            }
        }
        public ActionResult PatientList(int ColleagueId, int item)
        {
            try
            {
                MdlColleagues = new mdlColleagues();
                if (item == 1)
                {
                    MdlColleagues.lstRefferalDetails = GetReferralReceiveDetailsofDoctor(Convert.ToInt32(Session["UserId"]), ColleagueId);
                }
                else
                {
                    MdlColleagues.lstRefferalDetails = GetRefferralsentDetailsofDoctor(Convert.ToInt32(Session["UserId"]), ColleagueId);
                }

            }
            catch (Exception)
            {

                throw;
            }
            return View(MdlColleagues);
        }


        public string ColleagueListOfDoctorOnScroll(int PageIndex, string searchtext, string NewSearchText)
        {
            StringBuilder sb = new StringBuilder();

            MdlColleagues = new mdlColleagues();
            MdlColleagues.lstColleaguesDetails = MdlColleagues.ColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, PageSize, searchtext, 11, 2, NewSearchText, 0);
            if (MdlColleagues.lstColleaguesDetails.Count > 0)
            {
                foreach (var item in MdlColleagues.lstColleaguesDetails)
                {

                    DataTable dtReferralRece = new DataTable();
                    DataTable dtReferralSent = new DataTable();

                    dtReferralRece = objColleaguesData.GetReferralReceivedCountOfDoctor(Convert.ToInt32(Session["UserId"]), item.ColleagueId);
                    dtReferralSent = objColleaguesData.GetReferralSentCountOfDoctor(Convert.ToInt32(Session["UserId"]), item.ColleagueId);



                    sb.Append("<div class=\"trow\" id=\"row" + item.ColleagueId + "\">");
                    sb.Append("<div class=\"dataTableRow\">");
                    sb.Append("<input name=\"checkcolleague\" id=\"checkcolleague\" type=\"checkbox\" value=" + item.ColleagueId + ">");
                    sb.Append("<a data-role=\"ViewProfile\" data-value=\"" + item.ColleagueId + "\"> <figure class=\"thumb\"><img src='" + item.ImageName + "' alt='" + (item.FirstName != string.Empty && item.FirstName != null ? item.FirstName + " " : string.Empty) + (item.LastName != string.Empty && item.LastName != null ? item.LastName : string.Empty) + "'></figure></a> ");

                    sb.Append("<div class=\"dataTableCell cell1\"><a data-role=\"ViewProfile\" data-value=\"" + item.ColleagueId + "\" style=\"text-decoration:none;\"><div class=\"data\">");
                    sb.Append("<h3>" + item.LastName + ", " + item.FirstName);
                    for (int j = 0; j < item.lstSpeacilitiesOfDoctor.Count; j++)
                    {
                        sb.Append("<span>" + item.lstSpeacilitiesOfDoctor.ElementAt(j).SpecialtyDescription + "</span>");
                    }
                    sb.Append("</h3></div></a></div>");




                    sb.Append("<div class=\"dataTableInner\">");
                    sb.Append("<div class=\"dataTableCell cell2\">");
                    sb.Append("<ul class=\"list list_ab\">");
                    if (item.Phone != "")
                    {
                        sb.Append("<li><i class=\"wphone\"></i>" + item.Phone + "</li>");
                    }
                    if (item.Fax != "")
                    {
                        sb.Append("<li><i class=\"fax\"></i>" + item.Fax + "</li>");
                    }
                    sb.Append("<li>");
                    if (item.ExactAddress != "" || item.Address2 != "")
                    {
                        sb.Append("<i class=\"address\"></i> " + item.ExactAddress);

                        sb.Append("<z>&nbsp;</z> " + item.Address2 + "<z>&nbsp;</z>");
                    }
                    if (item.City != "" && item.City != null)
                    {
                        sb.Append(item.City + "<z>,&nbsp;</z>");
                    }

                    sb.Append(item.State + "&nbsp;" + item.ZipCode + "&nbsp; </li>");


                    sb.Append("<li>");

                    sb.Append("<i class=\"email\"></i>" + item.EmailAddress);
                    if (item.SecondaryEmail != null && item.SecondaryEmail != "")
                    {
                        sb.Append(", " + item.SecondaryEmail);
                    }

                    sb.Append("</li>");

                    // sb.Append("<li><a class=\"edit popup-with-form showlightbox\" href=\"#suggestion_box\" data-role=\"OpenSuggestionBox\" data-value=\"" + item.ColleagueId + "\"> <i class=\"edit\"></i>Suggest a change </a></li>");
                    sb.Append("</ul></div>");

                    sb.Append("<div class=\"dataTableCell cell3\">");
                    sb.Append("<ul class=\"list\">");
                    sb.Append("<li>Referrals Received (<a href=\"Javascript:;\"  data-role=\"SearchInboxFromColleageReferrals\" data-firstname=\"" + item.FirstName + "\" data-ColleagueId=\"" + item.ColleagueId + "\"  data-email=\"" + item.EmailAddress + "\">" + dtReferralRece.Rows[0]["TotalRecord"] + "</a>)</li>");
                    sb.Append("<li>Referrals Sent (<a  href=\"Javascript:;\"   data-role=\"SearchSentboxFromColleageReferrals\" data-firstname=\"" + item.FirstName + "\" data-ColleagueId=\"" + item.ColleagueId + "\" data-email=\"" + item.EmailAddress + "\">" + dtReferralSent.Rows[0]["TotalRecord"] + "</a>)</li>");
                    sb.Append("</ul></div>");


                    sb.Append(" <div class=\"dataTableCell cell4\">");
                    sb.Append("  <a href=\"Javascript:;\"  title=\"Referral\" class=\"frmSendRefferal\" data-role=\"OpenPopup\" data-value=\"" + item.ColleagueId + "\"></a>");
                    sb.Append(" <a href=\"javascript:;\" data-role=\"SendMessage\" data-value=\"" + item.ColleagueId + "\" title=\"Mail\" class=\"frmSendEmail\"></a>");
                    sb.Append("  <a href=\"javascript:;\" title=\"Delete\" style=\"display: inline-block;float: right;margin: -36px 23px 0 0;\" class=\"frmdltbtn12\" data-pageindex=\"" + PageIndex + "\" data-role=\"RemoveColleague\" data-value=\"" + item.ColleagueId + "\"></a>");
                    sb.Append(" </div></div></div></div>  ");





                }
            }
            else
            {
                sb.Append("<div class=\"trow\"><center>No more record found</center></div>");
            }
            return sb.ToString();
        }

        #endregion

        #region Code For Search Colleagues
        public ActionResult NewSearchColleagues(int PageIndex = 1)
        {
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                MdlColleagues = new mdlColleagues();
                ViewBag.SearchColleaguesResult = SearchColleaguesResult(Session["UserId"].ToString(), PageIndex, PageSize, null, null, null, null, null, null, null, null, null, null);
                ViewBag.SpecialitiesCheckBoxScript = SpecialitiesCheckBoxScript("");

                return PartialView("SearchColleagues", MdlColleagues);
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        [HttpPost]
        public JsonResult SearchColleaguesOption(string UserId, int PageIndex, int PageSize, string Name, string Email, string phone, string City, string State, string Zipcode, string Institute, string SpecialtityList, string Keywords)
        {
            string Search = string.Empty;
            string PhoneSearch = string.Empty;
            if (!string.IsNullOrEmpty(Keywords))
            {
                Search = "Keywords";
            }
            else
            {
                Search = "Search";
            }

            if (!string.IsNullOrEmpty(phone))
            {
                PhoneSearch = phone.Replace(" ", "").Replace("(", "");
                PhoneSearch = PhoneSearch.Replace(")", "").Replace("-", "");
            }
            Regex phoneNumber = new Regex(@"^\([0-9]{3}\)\s[0-9]{3}-[0-9]{4}$");
            Regex phoneNumberWithoutSpace = new Regex(@"^\([0-9]{3}\)[0-9]{3}-[0-9]{4}$");
            if (phoneNumber.IsMatch(Keywords) || phoneNumberWithoutSpace.IsMatch(Keywords))
            {
                Keywords = Keywords.Replace("(", "").Replace(")", "").Replace("-", "");
                Keywords = Keywords.Replace(" ", "");
            }
            return Json(SearchColleaguesResult(Session["UserId"].ToString(), PageIndex, PageSize, Name, Email, PhoneSearch, City, State, Zipcode, Institute, SpecialtityList, Search, Keywords), JsonRequestBehavior.AllowGet);
        }

        public string SearchColleaguesResult(string UserId, int PageIndex, int PageSize, string Name, string Email, string phone, string City, string State, string Zipcode, string Institute, string SpecialtityList, string Search, string Keywords)
        {
            objColleaguesData = new clsColleaguesData();
            MdlColleagues = new mdlColleagues();
            DataTable dtstate = objColleaguesData.GetStateCode(State);
            if (dtstate.Rows.Count > 0)
            {
                State = dtstate.Rows[0]["StateCode"].ToString();
            }

            MdlColleagues.lstSearchColleagueForDoctor = MdlColleagues.GetSearchColleagueForDoctor(int.Parse(UserId), PageIndex, PageSize, Name, Email, phone, City, State, Zipcode, Institute, SpecialtityList, Keywords);
            StringBuilder sb = new StringBuilder();
            if (MdlColleagues.lstSearchColleagueForDoctor.Count > 0)
            {
                double TotalPages = Math.Ceiling((double)MdlColleagues.lstSearchColleagueForDoctor.ElementAt(0).TotalRecord / PageSize);
                sb.Append("<ul class=\"search_listing\"  style=\"width:100%;\" data-name=\"" + Name + "\" data-email=\"" + Email + "\" data-phone=\"" + phone + "\" data-city=\"" + City + "\" data-state=\"" + State + "\" data-zipcode=\"" + Zipcode + "\" data-institute=\"" + Institute + "\" data-specialtitylist=\"" + SpecialtityList + "\" data-search=\"" + Search + "\" data-keywords=\"" + Keywords + "\">");
                for (int i = 0; i < MdlColleagues.lstSearchColleagueForDoctor.Count; i++)
                {

                    sb.Append("<li id=\"row" + MdlColleagues.lstSearchColleagueForDoctor[i].UserId + "\"><div class=\"dyheight\">");
                    sb.Append("<span class=\"\"><a target=\"_blank\" href=\"/Profile/Index?DoctorId=" + MdlColleagues.lstSearchColleagueForDoctor[i].UserId + "\"> <figure   class=\"thumb\"><img src=\"" + MdlColleagues.lstSearchColleagueForDoctor[i].ImageName + "\" alt=\"" + (MdlColleagues.lstSearchColleagueForDoctor[i].FirstName != string.Empty && MdlColleagues.lstSearchColleagueForDoctor[i].FirstName != null ? MdlColleagues.lstSearchColleagueForDoctor[i].FirstName + " " : string.Empty) + (MdlColleagues.lstSearchColleagueForDoctor[i].LastName != string.Empty && MdlColleagues.lstSearchColleagueForDoctor[i].LastName != null ? MdlColleagues.lstSearchColleagueForDoctor[i].LastName : string.Empty) + "\"></figure></a></span>");
                    sb.Append("<span class=\"description\">");
                    sb.Append("<h2><a class=\"whichkey\" target=\"_blank\" href=\"/Profile/Index?DoctorId=" + MdlColleagues.lstSearchColleagueForDoctor[i].UserId + "\">" + MdlColleagues.lstSearchColleagueForDoctor[i].FirstName + " " + MdlColleagues.lstSearchColleagueForDoctor[i].LastName + "<span class=\"sub_heading\">" + MdlColleagues.lstSearchColleagueForDoctor[i].OfficeName + "</span></a></h2>");
                    sb.Append("<ul class=\"list\"><li>" + MdlColleagues.lstSearchColleagueForDoctor[i].Specialities + "</li><li>" + (MdlColleagues.lstSearchColleagueForDoctor[i].City != string.Empty && MdlColleagues.lstSearchColleagueForDoctor[i].City != null ? MdlColleagues.lstSearchColleagueForDoctor[i].City + ", " : string.Empty) + MdlColleagues.lstSearchColleagueForDoctor[i].State + " (" + MdlColleagues.lstSearchColleagueForDoctor[i].Miles + " Miles)</li></ul>");
                    sb.Append("</span>");
                    sb.Append("<span class=\"action\"><input onclick=\"AddColleagues('" + MdlColleagues.lstSearchColleagueForDoctor[i].UserId + "')\" name=\"Add Dentist\" type=\"button\" value=\"Add Dentist\" class=\"add_dentist\"></span>");
                    sb.Append("<span class=\"clear\"></span>");
                    sb.Append("</div></li>");
                }
                sb.Append("</ul>");
                if (MdlColleagues.lstSearchColleagueForDoctor.Count < PageSize)
                {

                }
                else
                {
                    sb.Append("<button type=\"button\" style=\"width: 18 % \" id=\"showmore\" onclick=\"SearchColleaguesResultOnscroll()\" class=\"btn btn - primary\">Show More...</button>");
                }
            }
            else
            {
                sb.Append("<center>No record found. Please refine your search or visit <a href=" + Url.Action("InviteColleagues", "Colleagues") + " >Invite Colleague</a> page to invite colleague via email.");
                sb.Append("</center>");
            }
            return sb.ToString();
        }

        public List<SearchColleagueForDoctor> NewSearchColleaguesResult(string UserId, int PageIndex, int PageSize, string Name, string Email, string phone, string City, string State, string Zipcode, string Institute, string SpecialtityList, string Search, string Keywords)
        {
            objColleaguesData = new clsColleaguesData();
            MdlColleagues = new mdlColleagues();
            DataTable dtstate = objColleaguesData.GetStateCode(State);
            if (dtstate.Rows.Count > 0)
            {
                State = dtstate.Rows[0]["StateCode"].ToString();
            }

            MdlColleagues.lstSearchColleagueForDoctor = MdlColleagues.GetSearchColleagueForDoctor(int.Parse(UserId), PageIndex, PageSize, Name, Email, phone, City, State, Zipcode, Institute, SpecialtityList, Keywords);
            return MdlColleagues.lstSearchColleagueForDoctor;
        }

        public ActionResult NewSearchColleaguesOption(string UserId, int PageIndex, int PageSize, string Name, string Email, string phone, string City, string State, string Zipcode, string Institute, string SpecialtityList, string Search, string Keywords)
        {
            UserId = Session["UserId"].ToString();
            Search = string.Empty;
            if (!string.IsNullOrEmpty(Keywords))
            {
                Search = "Keywords";
            }
            else
            {
                Search = "Search";
            }
            objColleaguesData = new clsColleaguesData();
            MdlColleagues = new mdlColleagues();
            DataTable dtstate = objColleaguesData.GetStateCode(State);
            if (dtstate.Rows.Count > 0)
            {
                State = dtstate.Rows[0]["StateCode"].ToString();
            }

            MdlColleagues.lstSearchColleagueForDoctor = MdlColleagues.GetSearchColleagueForDoctor(int.Parse(UserId), PageIndex, PageSize, Name, Email, phone, City, State, Zipcode, Institute, SpecialtityList, Keywords);
            return View("_PartialSearchColleague", MdlColleagues.lstSearchColleagueForDoctor);
        }



        public string SearchColleaguesResultOnscroll(int PageIndex, string Name, string Email, string phone, string City, string State, string Zipcode, string Institute, string SpecialtityList, string Search, string Keywords)
        {
            StringBuilder sb = new StringBuilder();


            MdlColleagues = new mdlColleagues();
            MdlColleagues.lstSearchColleagueForDoctor = MdlColleagues.GetSearchColleagueForDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, PageSize, Name, Email, phone, City, State, Zipcode, Institute, SpecialtityList, Keywords);

            if (MdlColleagues.lstSearchColleagueForDoctor.Count > 0)
            {
                for (int i = 0; i < MdlColleagues.lstSearchColleagueForDoctor.Count; i++)
                {

                    sb.Append("<li id=\"row" + MdlColleagues.lstSearchColleagueForDoctor[i].UserId + "\"><div class=\"dyheight\">");
                    sb.Append("<span class=\"\"><a target=\"_blank\" href=\"/Profile/Index?DoctorId=" + MdlColleagues.lstSearchColleagueForDoctor[i].UserId + "\"> <figure   class=\"thumb\"><img src=\"" + MdlColleagues.lstSearchColleagueForDoctor[i].ImageName + "\" alt=\"" + (MdlColleagues.lstSearchColleagueForDoctor[i].FirstName != string.Empty && MdlColleagues.lstSearchColleagueForDoctor[i].FirstName != null ? MdlColleagues.lstSearchColleagueForDoctor[i].FirstName + " " : string.Empty) + (MdlColleagues.lstSearchColleagueForDoctor[i].LastName != string.Empty && MdlColleagues.lstSearchColleagueForDoctor[i].LastName != null ? MdlColleagues.lstSearchColleagueForDoctor[i].LastName : string.Empty) + "\"></figure></a></span>");
                    sb.Append("<span class=\"description\">");
                    sb.Append("<h2><a class=\"whichkey\" target=\"_blank\" href=\"/Profile/Index?DoctorId=" + MdlColleagues.lstSearchColleagueForDoctor[i].UserId + "\">" + MdlColleagues.lstSearchColleagueForDoctor[i].FirstName + " " + MdlColleagues.lstSearchColleagueForDoctor[i].LastName + "<span class=\"sub_heading\">" + MdlColleagues.lstSearchColleagueForDoctor[i].OfficeName + "</span></a></h2>");
                    sb.Append("<ul class=\"list\"><li>" + MdlColleagues.lstSearchColleagueForDoctor[i].Specialities + "</li><li>" + (MdlColleagues.lstSearchColleagueForDoctor[i].City != string.Empty && MdlColleagues.lstSearchColleagueForDoctor[i].City != null ? MdlColleagues.lstSearchColleagueForDoctor[i].City + ", " : string.Empty) + MdlColleagues.lstSearchColleagueForDoctor[i].State + " (" + MdlColleagues.lstSearchColleagueForDoctor[i].Miles + " Miles)</li></ul>");
                    sb.Append("</span>");
                    sb.Append("<span class=\"action\"><input onclick=\"AddColleagues('" + MdlColleagues.lstSearchColleagueForDoctor[i].UserId + "')\" name=\"Add Dentist\" type=\"button\" value=\"Add Dentist\" class=\"add_dentist\"></span>");
                    sb.Append("<span class=\"clear\"></span>");
                    sb.Append("</div></li>");
                }
            }
            else
            {
                sb.Append("<center>No more record found.</center>");
            }
            return sb.ToString();
        }

        public ActionResult NewSearchColleaguesResultOnscroll(int PageIndex, string Name, string Email, string phone, string City, string State, string Zipcode, string Institute, string SpecialtityList, string Search, string Keywords)
        {
            StringBuilder sb = new StringBuilder();
            MdlColleagues = new mdlColleagues();
            MdlColleagues.lstSearchColleagueForDoctor = MdlColleagues.GetSearchColleagueForDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, PageSize, Name, Email, phone, City, State, Zipcode, Institute, SpecialtityList, Keywords);
            return View("_PartialSearchColleague", MdlColleagues.lstSearchColleagueForDoctor);
        }

        public string SpecialitiesCheckBoxScript(string SpecialitiId)
        {
            StringBuilder sb = new StringBuilder();
            List<SelectListItem> lst = new List<SelectListItem>();
            MdlCommon = new mdlCommon();
            lst = MdlCommon.DoctorSpecialities();



            for (int i = 0; i < lst.Count; i++)
            {
                if (lst[i].Value == "0")
                {

                }
                else if (SpecialitiId != "" && SpecialitiId.Contains(lst[i].Value))
                {
                    sb.Append("<li><label><input class=\"checkallboxSearchColleague\" name=\"chkSpecialities\" checked=\"checked\" type=\"checkbox\" value=\"" + lst[i].Value + "\" >" + lst[i].Text + "</label></li>");
                }
                else
                {
                    sb.Append("<li><label><input class=\"checkallboxSearchColleague\" name=\"chkSpecialities\" type=\"checkbox\" value=\"" + lst[i].Value + "\">" + lst[i].Text + "</label></li>");
                }
            }


            return sb.ToString();
        }

        #endregion

        #region Code For Colleague SocialMedia
        [HttpGet]
        public ActionResult ColleagueSocialMedia()
        {
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {


                ViewBag.ColleagueSocialMediaList = GetColleagueSocialMedia(1, null, 10, 2);
                return PartialView("PartialColleagueSocialMedia");
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        [HttpPost]
        public JsonResult GetColleagueSocialMediaList(string PageIndex, string SearchText, string SortColumn, string SortDirection)
        {
            return Json(GetColleagueSocialMedia(Convert.ToInt32(PageIndex), SearchText, Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection)), JsonRequestBehavior.AllowGet);
        }
        public DateTime BeforCallStoredProcedure;
        public DateTime AfterCallStoredProcedure;
        public string GetColleagueSocialMedia(int PageIndex, string SearchText, int SortColumn, int SortDirection)
        {
            StringBuilder sb = new StringBuilder();
            HttpContext.Server.ScriptTimeout = 60 * 10; // Ten minutes..
            try
            {
                MdlColleagues = new mdlColleagues();

                MdlColleagues.lstGetSocialMediaDetailsForColleagues = MdlColleagues.GetSocialMediaDetailsForColleagues(Convert.ToInt32(Session["UserId"]), PageIndex, PageSize, SearchText, SortColumn, SortDirection);

                if (MdlColleagues.lstGetSocialMediaDetailsForColleagues.Count > 0)
                {
                    BeforCallStoredProcedure = DateTime.Now;
                    double TotalPages = Math.Ceiling(Convert.ToDouble(MdlColleagues.lstGetSocialMediaDetailsForColleagues[0].TotalRecord) / PageSize);
                    sb.Append("<div class=\"inner\">");

                    sb.Append("<div class=\"social_media\">");
                    sb.Append("<h5>Colleague Social Media</h5>");
                    sb.Append("<div class=\"clear\"></div>");

                    sb.Append("<table class=\"patient_social_media\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">");
                    sb.Append(" <thead><tr>");

                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 1 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Name</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 2 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">FACEBOOK</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 3 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">LINKEDIN</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 4 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">TWITTER</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 5 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">GOOGLE</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 6 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">YOUTUBE</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\"onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 7 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">PINTEREST</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 8 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">BLOG</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 11 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Yelp</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 9 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Klout Score</a></th>");

                    sb.Append(" </tr></thead><tbody id=\"ColleageSciaMedai\" data-sortcolumn=\"" + SortColumn + "\" data-sortirection=\"" + SortDirection + "\">");
                    for (int i = 0; i < MdlColleagues.lstGetSocialMediaDetailsForColleagues.Count; i++)
                    {
                        sb.Append("<tr>");

                        sb.Append("<td><a style=\"margin-right:0px !important;\" onclick=\"ViewProfile(" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].UserId + ")\">" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].FirstName + "&nbsp;" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].LastName + "</a></td>");
                        sb.Append("<td>");
                        if (MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].FacebookUrl != null && MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].FacebookUrl != "")
                        {

                            sb.Append("<a class=\"fb\" href=\"Javascript:;\" onclick=\"OpenInNewTab('" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].FacebookUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].LinkedinUrl != null && MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].LinkedinUrl != "")
                        {
                            sb.Append("<a class=\"in\"   href=\"Javascript:;\" onclick=\"OpenInNewTab('" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].LinkedinUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].TwitterUrl != null && MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].TwitterUrl != "")
                        {
                            sb.Append("<a class=\"tw\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].TwitterUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append(" <td>");
                        if (MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].GoogleplusUrl != null && MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].GoogleplusUrl != "")
                        {
                            sb.Append("<a class=\"gp\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].GoogleplusUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].YoutubeUrl != null && MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].YoutubeUrl != "")
                        {
                            sb.Append("<a class=\"yt\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].YoutubeUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].PinterestUrl != null && MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].PinterestUrl != "")
                        {
                            sb.Append("<a class=\"pn\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].PinterestUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].BlogUrl != null && MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].BlogUrl != "")
                        {
                            sb.Append("<a class=\"bl\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].BlogUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].YelpUrl != null && MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].YelpUrl != "")
                        {
                            sb.Append("<a class=\"ye\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].YelpUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (Convert.ToInt32(MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].cloudscore) > 0)
                        {
                            //sb.Append("<a class=\"ot\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].OtherUrl + "')\"></a>");
                            sb.Append(MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].cloudscore);
                        }
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                    sb.Append("</tbody></table><br />");

                    sb.Append("</div><div class=\"clear\"></div></div>");

                    AfterCallStoredProcedure = DateTime.Now;
                }
                else
                {
                    BeforCallStoredProcedure = DateTime.Now;
                    sb.Append("<div class=\"inner\">");

                    sb.Append("<div class=\"social_media\">");
                    sb.Append("<h5>Colleague Social Media</h5>");
                    sb.Append("<div class=\"clear\"></div>");

                    sb.Append("<table class=\"patient_social_media\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">");
                    sb.Append(" <thead><tr>");

                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 1 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Name</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 2 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">FACEBOOK</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 3 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">LINKEDIN</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 4 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">TWITTER</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 5 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">GOOGLE</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 6 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">YOUTUBE</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\"onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 7 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">PINTEREST</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 8 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">BLOG</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 11 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Yelp</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleagueSocialMediaList('" + PageIndex + "','" + SearchText + "','" + 9 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">Klout Score</a></th>");

                    sb.Append(" </tr></thead><tbody id=\"ColleageSciaMedai\" data-sortcolumn=\"" + SortColumn + "\" data-sortirection=\"" + SortDirection + "\">");
                    sb.Append("<tr>");
                    sb.Append("<td colspan=\"10\">");
                    sb.Append("<center>No Record Found</center>");
                    sb.Append("</td>");
                    sb.Append("</tr>");
                    sb.Append("</tbody></table><br />");

                    sb.Append("</div><div class=\"clear\"></div></div>");
                    AfterCallStoredProcedure = DateTime.Now;
                }
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("Before Call StoredProcedure :" + BeforCallStoredProcedure + " After Call StoredProcedure :" + AfterCallStoredProcedure, Ex.Message, Ex.StackTrace);
                throw;
            }
            return sb.ToString();
        }

        public string GetColleagueSocialMediaListOnScroll(int PageIndex, string searchtext, int strSortColumn, int strSortDirection)
        {
            StringBuilder sb = new StringBuilder();
            MdlColleagues = new mdlColleagues();
            try
            {
                MdlColleagues.lstGetSocialMediaDetailsForColleagues = MdlColleagues.GetSocialMediaDetailsForColleagues(Convert.ToInt32(Session["UserId"]), PageIndex, PageSize, searchtext, 1, 2);

                if (MdlColleagues.lstGetSocialMediaDetailsForColleagues.Count > 0)
                {
                    for (int i = 0; i < MdlColleagues.lstGetSocialMediaDetailsForColleagues.Count; i++)
                    {
                        sb.Append("<tr>");

                        sb.Append("<td><a style=\"margin-right:0px !important;\" onclick=\"ViewProfile(" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].UserId + ")\">" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].FirstName + "&nbsp;" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].LastName + "</a></td>");
                        sb.Append("<td>");
                        if (MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].FacebookUrl != null && MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].FacebookUrl != "")
                        {

                            sb.Append("<a class=\"fb\" href=\"Javascript:;\" onclick=\"OpenInNewTab('" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].FacebookUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].LinkedinUrl != null && MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].LinkedinUrl != "")
                        {
                            sb.Append("<a class=\"in\"   href=\"Javascript:;\" onclick=\"OpenInNewTab('" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].LinkedinUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].TwitterUrl != null && MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].TwitterUrl != "")
                        {
                            sb.Append("<a class=\"tw\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].TwitterUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append(" <td>");
                        if (MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].GoogleplusUrl != null && MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].GoogleplusUrl != "")
                        {
                            sb.Append("<a class=\"gp\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].GoogleplusUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].YoutubeUrl != null && MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].YoutubeUrl != "")
                        {
                            sb.Append("<a class=\"yt\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].YoutubeUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].PinterestUrl != null && MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].PinterestUrl != "")
                        {
                            sb.Append("<a class=\"pn\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].PinterestUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].BlogUrl != null && MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].BlogUrl != "")
                        {
                            sb.Append("<a class=\"bl\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].BlogUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].YelpUrl != null && MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].YelpUrl != "")
                        {
                            sb.Append("<a class=\"ye\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].YelpUrl + "')\"></a>");
                        }
                        sb.Append("</td>");
                        sb.Append("<td>");
                        if (Convert.ToInt32(MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].cloudscore) > 0)
                        {
                            //sb.Append("<a class=\"ot\"  href=\"Javascript:;\" onclick=\"OpenInNewTab('" + MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].OtherUrl + "')\"></a>");
                            sb.Append(MdlColleagues.lstGetSocialMediaDetailsForColleagues[i].cloudscore);
                        }
                        sb.Append("</td>");
                        sb.Append("</tr>");
                    }
                }
                else
                {
                    sb.Append("<tr><td colspan=\"9\"><center>No More Record Found</center></td></tr>");
                }
                return sb.ToString();
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("GetColleagueSocialMediaListOnScroll", Ex.Message, Ex.StackTrace);
                throw;
            }

        }

        #endregion

        #region Colleague ContactList
        [HttpGet]
        public ActionResult ColleagueContactList()
        {
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {

                ViewBag.ColleaguesContactList = GetColleaguesContact(1, null, 11, 2);
                return PartialView("PartialColleagueContactList");
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        [HttpPost]
        public JsonResult GetColleaguesContactList(string PageIndex, string SearchText, string SortColumn, string SortDirection)
        {
            return Json(GetColleaguesContact(Convert.ToInt32(PageIndex), SearchText, Convert.ToInt32(SortColumn), Convert.ToInt32(SortDirection)), JsonRequestBehavior.AllowGet);
        }





        public string GetColleaguesContact(int PageIndex, string SearchText, int SortColumn, int SortDirection)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                MdlColleagues = new mdlColleagues();
                MdlColleagues.lstColleaguesDetails = MdlColleagues.ColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, PageSize, SearchText, SortColumn, SortDirection, null, 0);

                if (MdlColleagues.lstColleaguesDetails.Count > 0)
                {

                    double TotalPages = Math.Ceiling(Convert.ToDouble(MdlColleagues.lstColleaguesDetails[0].TotalRecord) / PageSize);
                    sb.Append("<div class=\"inner\">");

                    sb.Append("<div class=\"table_data reports_ref_received clearfix\">");
                    sb.Append(" <h5>Colleague Contact List<span class=\"buttons\"><input class=\"export_report\" name=\"Export Report\"  type=\"submit\" onclick=\"location.href='" + Url.Action("ExportToExcel", "Colleagues") + "'\"></span></h5>");
                    sb.Append(" <div class=\"clear\"></div>");
                    sb.Append("<table class=\"patientContactList\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">");
                    sb.Append("<thead>");
                    sb.Append("<tr>");
                    //RM-199 Changes for FirstName,LastName,Email
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleaguesContactList('" + PageIndex + "','" + SearchText + "','" + 2 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">FIRST NAME</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleaguesContactList('" + PageIndex + "','" + SearchText + "','" + 12 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">LAST NAME</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleaguesContactList('" + PageIndex + "','" + SearchText + "','" + 3 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">OFFICE</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleaguesContactList('" + PageIndex + "','" + SearchText + "','" + 4 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">ADDRESS</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleaguesContactList('" + PageIndex + "','" + SearchText + "','" + 5 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">CITY</a></th>");

                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleaguesContactList('" + PageIndex + "','" + SearchText + "','" + 6 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">STATE</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleaguesContactList('" + PageIndex + "','" + SearchText + "','" + 7 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">ZIP</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleaguesContactList('" + PageIndex + "','" + SearchText + "','" + 8 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">PHONE</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleaguesContactList('" + PageIndex + "','" + SearchText + "','" + 9 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">FAX</a></th>");
                    sb.Append("<th><a href=\"javascript:;\" class=\"sortable\" onclick=\"GetColleaguesContactList('" + PageIndex + "','" + SearchText + "','" + 15 + "','" + (SortDirection == 1 ? 2 : 1) + "')\">EMAIL</a></th>");

                    sb.Append("</tr></thead>");
                    sb.Append("<tbody id=\"ColleageuContactList\" data-sortcolumn=\"" + SortColumn + "\" data-sortirection=\"" + SortDirection + "\">");
                    foreach (var item in MdlColleagues.lstColleaguesDetails)
                    {

                        sb.Append("<tr>");
                        sb.Append("<td><a style=\"margin-right:0px !important;\" onclick=\"ViewProfile(" + item.ColleagueId + ")\">" + item.FirstName + "</a></td>");
                        sb.Append("<td><a style=\"margin-right:0px !important;\" onclick=\"ViewProfile(" + item.ColleagueId + ")\">" + item.LastName + "</a></td>");
                        sb.Append("<td>" + item.Officename + "</td>");
                        sb.Append("<td>" + item.ExactAddress + "&nbsp;" + item.Address2 + "</td>");
                        sb.Append("<td>" + item.City + "</td>");
                        sb.Append("<td>" + item.State + "</td>");
                        sb.Append("<td>" + item.ZipCode + "</td>");


                        sb.Append("<td>" + item.Phone + "</td>");
                        sb.Append(" <td>" + item.Fax + "</td>");
                        sb.Append("<td><a href=\"mailto:" + item.EmailAddress + "\">" + item.EmailAddress + "</a></td>");
                        sb.Append("</tr>");
                    }
                    sb.Append("</tbody>");
                    sb.Append("</table>");
                    sb.Append("</div> <br /><div class=\"clear\"></div></div>");



                }
                else
                {
                    sb.Append("<center>No Record Found</center>");
                }
            }
            catch (Exception)
            {

                throw;
            }
            return sb.ToString();
        }

        public string GetColleaguesContactOnScroll(int PageIndex, string searchtext, int strSortColumn, int strSortDirection)
        {
            StringBuilder sb = new StringBuilder();
            MdlColleagues = new mdlColleagues();
            MdlColleagues.lstColleaguesDetails = MdlColleagues.ColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), PageIndex, PageSize, searchtext, strSortColumn, strSortDirection, null, 0);

            if (MdlColleagues.lstColleaguesDetails.Count > 0)
            {
                foreach (var item in MdlColleagues.lstColleaguesDetails)
                {

                    sb.Append("<tr>");
                    sb.Append("<td><a style=\"margin-right:0px !important;\" onclick=\"ViewProfile(" + item.ColleagueId + ")\">" + item.FirstName + "</a></td>");
                    sb.Append("<td><a style=\"margin-right:0px !important;\" onclick=\"ViewProfile(" + item.ColleagueId + ")\">" + item.LastName + "</a></td>");
                    sb.Append("<td>" + item.Officename + "</td>");
                    sb.Append("<td>" + item.ExactAddress + "&nbsp;" + item.Address2 + "</td>");
                    sb.Append("<td>" + item.City + "</td>");
                    sb.Append("<td>" + item.State + "</td>");
                    sb.Append("<td>" + item.ZipCode + "</td>");


                    sb.Append("<td>" + item.Phone + "</td>");
                    sb.Append(" <td>" + item.Fax + "</td>");
                    sb.Append("<td><a href=\"mailto:" + item.EmailAddress + "\">" + item.EmailAddress + "</a></td>");
                    sb.Append("</tr>");
                }
            }
            else
            {
                sb.Append("<tr><td colspan=\"10\"><center> No More Record Found</center></td></tr>");
            }
            return sb.ToString();
        }
        #endregion

        #region Code For Add,remove Colleague
        public PartialViewResult AddColleague()
        {
            return PartialView("PartialAddColleague");
        }
        public ActionResult AddAsColleague(string ColleagueId)
        {
            //BO.Models.PermissionResult objPermissionResult = new BO.Models.PermissionResult();
            //objPermissionResult = objpermissionbll.CheckPermissionAndMemberShip(Convert.ToInt32(SessionManagement.UserId), (int)BO.Enums.Common.Features.New_Patient_Leads, SessionManagement.GetMemberPlanDetails);
            //if (objPermissionResult.HasPermission || !objPermissionResult.DoesMembershipAllow)
            //{
            //    // TODO
            //    return View("~/Views/Shared/UpgradeMembership.cshtml", objPermissionResult);
            //    // Show message "You do not have permission to add patient. Please upgrade your membership to add more patients.
            //}
            MdlColleagues = new mdlColleagues();
            ObjTemplate = new clsTemplate();
            bool status = MdlColleagues.AddAsColleague(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(ColleagueId));
            string strReturnResult = "";
            if (status)
            {
                strReturnResult = "1";
                //Send Template to colleague
                ObjTemplate.ColleagueInviteDoctor(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(ColleagueId), "");

            }
            else
            {
                strReturnResult = "0";
            }
            return Json(strReturnResult, JsonRequestBehavior.AllowGet);

        }


        public string RemoveColleague(int ColleagueId, int PageIndex)
        {
            bool Result = true;
            string obj = "";
            MdlColleagues = new mdlColleagues();
            Result = MdlColleagues.RemoveColleague(Convert.ToInt32(Session["UserId"]), ColleagueId);
            if (Result)
            {
                obj = ColleagueListOfDoctor(PageIndex, null, 11, 2, null, 0);
            }
            else
            {
                obj = "0";
            }
            return obj.ToString();
        }

        public ActionResult NewRemoveColleague(int ColleagueId)
        {
            bool Result = true;
            string obj = "";
            MdlColleagues = new mdlColleagues();
            Result = MdlColleagues.RemoveColleague(Convert.ToInt32(Session["UserId"]), ColleagueId);
            if (Result)
            {
                obj = ColleagueListOfDoctor(1, null, 11, 2, null, 0);
            }
            else
            {
                obj = "0";
            }
            return Json(obj);
        }



        [HttpPost]
        public JsonResult RemoveMutipleColleague(string MultipleColleagueId)
        {
            bool Result = false;
            object obj = "";

            MdlColleagues = new mdlColleagues();

            string[] ColleagueIds = MultipleColleagueId.Split(',');
            foreach (string ColleagueId in ColleagueIds)
            {
                Result = MdlColleagues.RemoveColleague(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(ColleagueId));
            }

            if (Result)
            {
                obj = "1";
            }
            else
            {
                obj = "0";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Code For Invite Colleague
        public ActionResult InviteColleagues()
        {
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                return View("NewInviteColleagues");
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        [HttpPost]
        public JsonResult InviteColleagueOld(string email, string msg, HttpPostedFileBase file)
        {
            MdlColleagues = new mdlColleagues();

            int status = MdlColleagues.GetExistingEmail(Convert.ToInt32(Session["UserId"]), email, msg);
            string strReturnResult = "0";
            if (status == 1)
            {
                strReturnResult = "1";
            }


            return Json(strReturnResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public ActionResult InviteColleague(string txtEmail, string txtMessage, string pathfilename)
        {
            ResponseMessages objResponseMessages = new ResponseMessages();
            List<ResponseEmails> objListResponseEmail = new List<ResponseEmails>();
            StringBuilder EmailCountAsStaffMember = new StringBuilder();
            object obj = string.Empty;
            try
            {
                MdlColleagues = new mdlColleagues();
                string HasDoctorEmail = string.Empty;
                int status;
                StringBuilder EmailCount = new StringBuilder();
                DataTable Dtsendmail = new DataTable();
                string CurrentUserEmail = string.Empty;
                string CurrentUserSecondaryEmail = string.Empty;
                DataSet dsUser = new DataSet();
                dsUser = objColleaguesData.GetDoctorDetailsById(Convert.ToInt32(Session["UserId"]));
                if (dsUser.Tables.Count > 0)
                {
                    CurrentUserEmail = ObjCommon.CheckNull(Convert.ToString(dsUser.Tables[0].Rows[0]["Username"]), string.Empty);
                    CurrentUserSecondaryEmail = ObjCommon.CheckNull(Convert.ToString(dsUser.Tables[0].Rows[0]["SecondaryEmail"]), string.Empty);
                }
                
                string UplaodeFileName = pathfilename;
                if (!string.IsNullOrEmpty(UplaodeFileName))
                {
                    string fileExtension = Path.GetExtension(UplaodeFileName);
                    if (fileExtension.ToLower() == ".xls" || fileExtension.ToLower() == ".xlsx")
                    {
                        string fileName = Path.GetFileName(UplaodeFileName);
                        string fileLocation = Server.MapPath("../CSVLoad/" + fileName);


                        string connectionString = string.Empty;
                        FileNameNew = fileLocation;

                        //Check whether file extension is xls or xslx
                        if (fileExtension.ToLower() == ".xls")
                        {
                            connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 8.0;HDR=Yes;\"";
                        }
                        else if (fileExtension.ToLower() == ".xlsx")
                        {
                            connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileLocation + ";Extended Properties=\"Excel 12.0;HDR=Yes;\"";
                        }

                        DataSet ds = new DataSet();
                        DataTable dtExcelRecords = new DataTable();


                        FileInfo fi = new FileInfo(fileLocation);
                        ds = ReadExcel(fi.FullName, fi.Extension, true);
                        dtExcelRecords = ds.Tables[0];


                        if (dtExcelRecords != null && dtExcelRecords.Rows.Count > 0)
                        {
                            string Columnname = "";

                            foreach (DataColumn item in dtExcelRecords.Columns)
                            {
                                var dtcolumn = Convert.ToString(item);
                                Dtsendmail.Columns.Add(dtcolumn);
                                if (string.IsNullOrEmpty(Columnname))
                                {
                                    Columnname = Convert.ToString(item);
                                }
                                else
                                {
                                    Columnname = Columnname + "," + Convert.ToString(item);
                                }
                            }


                            // Uploaded File Must Contain one Column Name :Email
                            if (Columnname.Contains("Email"))
                            {
                                // Call for File Uploaded 
                                DataTable Data = new DataTable();
                                Data = ds.Tables[0];

                                //Check Doctor not Exists then send mail to justin and melisa.
                                DataTable chek = new DataTable();
                                // Dtsendmail.Columns.Add("Email");
                                foreach (DataRow item in Data.Rows)
                                {
                                    chek = objColleaguesData.CheckEmailInSystem(Convert.ToString(item["Email"]));
                                    if (chek.Rows.Count == 0)
                                    {
                                        Dtsendmail.Rows.Add(item.ItemArray);
                                    }
                                }
                                if (Dtsendmail.Rows.Count > 0)
                                {
                                    bool done = CheckUserExistorNot(Dtsendmail, Convert.ToInt32(SessionManagement.UserId));
                                }
                                foreach (DataRow item in Data.Rows)
                                {
                                    ResponseEmails objResponseEmails = new ResponseEmails();

                                    bool valid = false;
                                    string Email = string.Empty;
                                    Email = Convert.ToString(item["Email"]);
                                    if (Email != "" && Email != null)
                                    {
                                        valid = isEmail(Email);
                                        if (valid)
                                        {
                                            if (CurrentUserEmail != Email)
                                            {
                                                bool checkEmailStaff = new clsColleaguesData().CheckEmailAsStaffMember(Email);
                                                if (!checkEmailStaff)
                                                {
                                                    if (!string.IsNullOrEmpty(CurrentUserSecondaryEmail))
                                                    {

                                                        if (CurrentUserSecondaryEmail != Email)
                                                        {
                                                            status = MdlColleagues.GetExistingEmail(Convert.ToInt32(Session["UserId"]), Email, txtMessage);
                                                            if (status ==0)
                                                            {
                                                                objResponseEmails.EmailId = Email;
                                                                objResponseEmails.Id = 2;
                                                            }
                                                            else if (status == 1)
                                                            {
                                                                objResponseEmails.EmailId = Email;
                                                                objResponseEmails.Id = 1;
                                                            }
                                                            else if (status == 2)
                                                            {
                                                                objResponseEmails.EmailId = Email;
                                                                objResponseEmails.Id = 5;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        status = MdlColleagues.GetExistingEmail(Convert.ToInt32(Session["UserId"]), Email, txtMessage);
                                                        if (status==0)
                                                        {
                                                            objResponseEmails.EmailId = Email;
                                                            objResponseEmails.Id = 2;
                                                        }
                                                        else if(status == 1)
                                                        {
                                                            objResponseEmails.EmailId = Email;
                                                            objResponseEmails.Id = 1;
                                                        }
                                                        if (status == 2)
                                                        {
                                                            objResponseEmails.EmailId = Email;
                                                            objResponseEmails.Id = 5;
                                                        }

                                                    }
                                                }
                                                else
                                                {
                                                    EmailCountAsStaffMember = EmailCountAsStaffMembers(Email);
                                                    objResponseEmails.EmailId = Email;
                                                    objResponseEmails.Id = 3;
                                                }

                                            }
                                            else
                                            {
                                                HasDoctorEmail = Email;
                                                objResponseEmails.EmailId = Email;
                                                objResponseEmails.Id = 4;
                                            }
                                        }
                                        objResponseMessages.EmailRequired = "Please Enter Valid Email";
                                    }
                                    else
                                    {
                                        objResponseMessages.EmailRequired = "Please Enter Email";

                                    }
                                    objListResponseEmail.Add(objResponseEmails);
                                }

                                // Call if there is Textbox aslo contain emails  
                                if (txtEmail != null && txtEmail != "")
                                {
                                    string EmailAddress = Convert.ToString(txtEmail);
                                    string[] Email = EmailAddress.Split(',');

                                    foreach (var item in Email)
                                    {
                                        ResponseEmails objResponseEmails = new ResponseEmails();
                                        bool valid = false;
                                        string ValidEmail = string.Empty;
                                        ValidEmail = Convert.ToString(item);

                                        if (!string.IsNullOrEmpty(ValidEmail))
                                        {
                                            valid = isEmail(ValidEmail);
                                            if (valid)
                                            {
                                                if (CurrentUserEmail != ValidEmail)
                                                {
                                                    bool checkEmailStaff = new clsColleaguesData().CheckEmailAsStaffMember(ValidEmail);
                                                    if (!checkEmailStaff)
                                                    {
                                                        if (!string.IsNullOrEmpty(CurrentUserSecondaryEmail))
                                                        {
                                                            if (CurrentUserSecondaryEmail != ValidEmail)
                                                            {
                                                                status = MdlColleagues.GetExistingEmail(Convert.ToInt32(Session["UserId"]), ValidEmail, txtMessage);
                                                                if (status  == 0)
                                                                {
                                                                    objResponseEmails.EmailId = ValidEmail;
                                                                    objResponseEmails.Id = 2;
                                                                }
                                                                else if (status == 1)
                                                                {
                                                                    objResponseEmails.EmailId = ValidEmail;
                                                                    objResponseEmails.Id = 1;
                                                                }
                                                                if (status == 2)
                                                                {
                                                                    objResponseEmails.EmailId = ValidEmail;
                                                                    objResponseEmails.Id = 5;
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            status = MdlColleagues.GetExistingEmail(Convert.ToInt32(Session["UserId"]), ValidEmail, txtMessage);
                                                            if (status == 0)
                                                            {
                                                                objResponseEmails.EmailId = ValidEmail;
                                                                objResponseEmails.Id = 2;
                                                            }
                                                            else if (status == 1)
                                                            {
                                                                objResponseEmails.EmailId = ValidEmail;
                                                                objResponseEmails.Id = 1;
                                                            }
                                                            if (status == 2)
                                                            {
                                                                objResponseEmails.EmailId = ValidEmail;
                                                                objResponseEmails.Id = 5;
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        EmailCountAsStaffMember = EmailCountAsStaffMembers(ValidEmail);
                                                        objResponseEmails.EmailId = ValidEmail;
                                                        objResponseEmails.Id = 3;
                                                    }
                                                }

                                                else
                                                {
                                                    HasDoctorEmail = ValidEmail;
                                                    objResponseEmails.EmailId = ValidEmail;
                                                    objResponseEmails.Id = 4;
                                                }
                                            }
                                        }
                                        objListResponseEmail.Add(objResponseEmails);
                                    }

                                    //Make txtEmail null because loop excute next 
                                    txtEmail = string.Empty;
                                }

                                
                            }
                            else
                            {
                                obj = "2";
                            }
                        }

                        // Call if there is Textbox aslo contain emails  
                        if (txtEmail != null && txtEmail != "")
                        {
                            string EmailAddress = Convert.ToString(txtEmail);
                            string[] Email = EmailAddress.Split(',');
                            //Check Doctor not Exists then send mail to justin and melisa.
                            DataTable chek = new DataTable();
                            ResponseEmails objResponseEmails = new ResponseEmails();

                            Dtsendmail.Columns.Add("Email");
                            foreach (var item in Email)
                            {
                                chek = objColleaguesData.CheckEmailInSystem(Convert.ToString(item));
                                if (chek.Rows.Count == 0)
                                {
                                    Dtsendmail.Rows.Add(item);
                                }
                            }
                            if (Dtsendmail.Rows.Count > 0)
                            {
                                bool done = CheckUserExistorNot(Dtsendmail, Convert.ToInt32(SessionManagement.UserId));
                            }
                            for (int i = 0; i < Email.Length; i++)
                            {
                                bool valid = false;


                                string ValidEmail = string.Empty;
                                ValidEmail = Convert.ToString(Email[i]);

                                if (!string.IsNullOrEmpty(ValidEmail))
                                {

                                    valid = isEmail(ValidEmail);
                                    if (valid)
                                    {
                                        if (CurrentUserEmail != ValidEmail)
                                        {
                                            if (!string.IsNullOrEmpty(CurrentUserSecondaryEmail))
                                            {
                                                if (CurrentUserSecondaryEmail != ValidEmail)
                                                {
                                                    status = MdlColleagues.GetExistingEmail(Convert.ToInt32(Session["UserId"]), ValidEmail, txtMessage);
                                                    if (status == 0)
                                                    {
                                                        objResponseEmails.EmailId = ValidEmail;
                                                        objResponseEmails.Id = 2;
                                                    }
                                                    else if (status == 1)
                                                    {
                                                        objResponseEmails.EmailId = ValidEmail;
                                                        objResponseEmails.Id = 1;
                                                    }
                                                    if (status == 2)
                                                    {
                                                        objResponseEmails.EmailId = ValidEmail;
                                                        objResponseEmails.Id = 5;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                status = MdlColleagues.GetExistingEmail(Convert.ToInt32(Session["UserId"]), ValidEmail, txtMessage);
                                                if (status == 0)
                                                {
                                                    objResponseEmails.EmailId = ValidEmail;
                                                    objResponseEmails.Id = 2;
                                                }
                                                else if (status == 1)
                                                {
                                                    objResponseEmails.EmailId = ValidEmail;
                                                    objResponseEmails.Id = 1;
                                                }
                                                if (status == 2)
                                                {
                                                    objResponseEmails.EmailId = ValidEmail;
                                                    objResponseEmails.Id = 5;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            HasDoctorEmail = ValidEmail;
                                            objResponseEmails.EmailId = ValidEmail;
                                            objResponseEmails.Id = 4;
                                        }
                                    }
                                }
                                objListResponseEmail.Add(objResponseEmails);

                            }
                           // obj = "1";
                        }

                    }
                    else
                    {
                        obj = "2";
                    }



                }
                else
                {
                    // Call if there is Textbox aslo contain emails  
                    if (txtEmail != null && txtEmail != "")
                    {
                        string EmailAddress = Convert.ToString(txtEmail);
                        string[] Email = EmailAddress.Split(',');
                        //Check Doctor not Exists then send mail to justin and melisa.
                        DataTable chek = new DataTable();
                        Dtsendmail.Columns.Add("Email");
                        foreach (var item in Email)
                        {
                            chek = objColleaguesData.CheckEmailInSystem(Convert.ToString(item));
                            if (chek.Rows.Count == 0)
                            {
                                Dtsendmail.Rows.Add(item);
                            }
                        }
                        if (Dtsendmail.Rows.Count > 0)
                        {
                            bool done = CheckUserExistorNot(Dtsendmail, Convert.ToInt32(SessionManagement.UserId));
                        }
                        foreach (var item in Email)
                        {
                            ResponseEmails objResponseEmails = new ResponseEmails();

                            bool valid = false;
                            string ValidEmail = string.Empty;
                            ValidEmail = Convert.ToString(item);

                            if (!string.IsNullOrEmpty(ValidEmail))
                            {
                                valid = isEmail(ValidEmail);
                                if (valid)
                                {
                                    if (CurrentUserEmail != ValidEmail)
                                    {
                                        bool checkEmailStaff = new clsColleaguesData().CheckEmailAsStaffMember(ValidEmail);
                                        if (!checkEmailStaff)
                                        {
                                            if (!string.IsNullOrEmpty(CurrentUserSecondaryEmail))
                                            {
                                                if (CurrentUserSecondaryEmail != ValidEmail)
                                                {
                                                    status = MdlColleagues.GetExistingEmail(Convert.ToInt32(Session["UserId"]), ValidEmail, txtMessage);
                                                    if (status == 0)
                                                    {
                                                        objResponseEmails.EmailId = ValidEmail;
                                                        objResponseEmails.Id = 2;
                                                    }
                                                    else if (status == 1)
                                                    {
                                                        objResponseEmails.EmailId = ValidEmail;
                                                        objResponseEmails.Id = 1;
                                                    }
                                                    if (status == 2)
                                                    {
                                                        objResponseEmails.EmailId = ValidEmail;
                                                        objResponseEmails.Id = 5;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                status = MdlColleagues.GetExistingEmail(Convert.ToInt32(Session["UserId"]), ValidEmail, txtMessage);
                                                if (status == 0)
                                                {
                                                    objResponseEmails.EmailId = ValidEmail;
                                                    objResponseEmails.Id = 2;
                                                }
                                                else if (status == 1)
                                                {
                                                    objResponseEmails.EmailId = ValidEmail;
                                                    objResponseEmails.Id = 1;
                                                }
                                                if (status == 2)
                                                {
                                                    objResponseEmails.EmailId = ValidEmail;
                                                    objResponseEmails.Id = 5;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            EmailCountAsStaffMember = EmailCountAsStaffMembers(ValidEmail);
                                            objResponseEmails.EmailId = ValidEmail;
                                            objResponseEmails.Id = 3;
                                        }
                                    }
                                    else
                                    {
                                        HasDoctorEmail = ValidEmail;
                                        objResponseEmails.EmailId = ValidEmail;
                                        objResponseEmails.Id = 4;
                                    }

                                }
                            }
                            objListResponseEmail.Add(objResponseEmails);
                        }

                        //obj = "1";
                    }
                }

                //if (obj != null)
                //{
                //    if (!string.IsNullOrEmpty(HasDoctorEmail))
                //    {
                //        if (obj.ToString() == "1")
                //        {
                //            obj = "3";
                //        }
                //    }
                //}
                //if (!string.IsNullOrWhiteSpace(Convert.ToString(EmailCount)))
                //{
                //    var numberList = EmailCount.ToString().Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                //    if (numberList.Count > 1)
                //    {
                //        obj = "Email id " + EmailCount.ToString() + " already exist in colleague list. So, cannot send request to these colleagues.";
                //    }
                //    else
                //    {
                //        obj = "Email id " + EmailCount.ToString() + " already exists in colleague list. So, cannot send request to this colleague.";
                //    }
                //}

                if (!string.IsNullOrWhiteSpace(Convert.ToString(EmailCountAsStaffMember)))
                {
                    // send Email to travis when someone try to add staff member as colleagues
                    //  new clsTemplate().StaffMemberInviteDoctor(UserId, SafeValue<string>(EmailCountAsStaffMember));

                    var numberList = Convert.ToString(EmailCountAsStaffMember).Split(',').Select(x => x.Trim()).Where(x => !string.IsNullOrWhiteSpace(x)).ToList();
                    if (numberList.Count > 0)
                    {
                        new clsTemplate().StaffMemberInviteDoctor(Convert.ToInt32(SessionManagement.UserId), numberList);
                    }
                    //if (numberList.Count > 1)
                    //{

                    //    Objs += "Email id " + SafeValue<string>(EmailCountAsStaffMember) + "is register as a staff. So, cannot send request to this colleague.";
                    //}
                    //else
                    //{
                    //    Objs += "Email id " + SafeValue<string>(EmailCountAsStaffMember) + "is register as a staff. So, cannot send request to this colleague.";
                    //}
                }
            }
            catch (Exception)
            {

                throw;
            }

            objResponseMessages.objListResponseEmail = objListResponseEmail;
            return PartialView("_PartialInviteColleagueMessage", objResponseMessages);

        }

        #region Code For Suggest Change
        public PartialViewResult SuggestionBox(int ColleagueId)
        {
            MdlColleagues = new mdlColleagues();
            MdlColleagues.ColleagueId = ColleagueId;
            return PartialView("PartialSuggestion", MdlColleagues);
        }
        [HttpPost]
        public JsonResult SuggestChange(string ColleagueId, string Subject, string Suggestion)
        {

            string Obj = "";
            MdlColleagues = new mdlColleagues();
            bool status = false;

            string[] ColleagueIds = ColleagueId.ToString().Split(new char[] { ',' });
            if (ColleagueIds.Length != 0)
            {

                foreach (var item in ColleagueIds)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        status = MdlColleagues.SuggestChange(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(item), Subject, Suggestion);
                    }
                }
            }

            if (status)
            {
                Obj = "1";
            }
            else
            {
                Obj = "0";
            }

            return Json(Obj);

        }

        #endregion


        #region StaffMemberList
        public static StringBuilder EmailCountAsStaffMembers(string EmailAddress)
        {
            StringBuilder EmailCountAsStaffMembers = new StringBuilder();
            if (!string.IsNullOrWhiteSpace(Convert.ToString(EmailCountAsStaffMembers)))
            {
                EmailCountAsStaffMembers.Append(" , " + EmailAddress);
            }
            else
            {
                EmailCountAsStaffMembers.Append(EmailAddress);
            }
            return EmailCountAsStaffMembers;
        }
        #endregion

        public DataTable getExcelColumn()
        {
            DataTable dt1 = new DataTable();
            try
            {
                OleDbConnection objConnection = new OleDbConnection(ConnectionString());
                objConnection.Open();

                DataTable tbl = objConnection.GetSchema("Tables");





                string sheetName = (string)tbl.Rows[0]["TABLE_NAME"];

                OleDbCommand objCmdSelect = new OleDbCommand("SELECT TOP 1 * FROM [" + sheetName + "]", objConnection);
                OleDbDataAdapter objAdapter = new OleDbDataAdapter(objCmdSelect);
                DataTable Dt = new DataTable();
                objAdapter.Fill(Dt);
                dt1 = new DataTable("Excel");


                dt1.Columns.Add("ExcelColumn", typeof(String));





                foreach (DataColumn dc in Dt.Columns)
                {
                    dt1.Rows.Add(new object[] { dc.ColumnName });

                }
                objConnection.Close();
                return dt1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private DataSet ReadExcel(string filePath, string Extension, bool isHDR)
        {
            FileStream stream = System.IO.File.Open(filePath, FileMode.Open, FileAccess.Read);
            IExcelDataReader excelReader = null;

            switch (Extension)
            {
                case ".xls":
                    excelReader = ExcelReaderFactory.CreateBinaryReader(stream);
                    break;
                case ".xlsx": //Excel 07
                    excelReader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                    break;
            }

            excelReader.IsFirstRowAsColumnNames = isHDR;
            DataSet result = excelReader.AsDataSet();



            while (excelReader.Read())
            {

            }

            //Priya: 
            //Comment this code because throw Exception when we upload .xlsx file 
            //w.r.t : http://techbrij.com/read-excel-xls-xlsx-asp-net-mvc-upload
            //foreach (DataColumn dc in result.Tables[0].Columns)
            //{
            //    result.Tables[0].Rows.Add(new object[] { dc.ColumnName });

            //}



            excelReader.Close();

            return result;

        }

        private String ConnectionString()
        {

            return "Provider=Microsoft.ACE.OLEDB.12.0;" +
                "Data Source=" + FileNameNew + "; " +
                "Extended Properties=\"Excel 12.0;HDR=YES;\"";

        }

        public DataTable getExcelColumnData()
        {

            try
            {
                OleDbConnection objConnection = new OleDbConnection(ConnectionString());
                objConnection.Open();

                DataTable Table2 = objConnection.GetSchema("Tables");


                string sheetName = (string)Table2.Rows[0]["TABLE_NAME"];

                OleDbCommand objCmdSelect = new OleDbCommand("SELECT * FROM [" + sheetName + "]", objConnection);
                OleDbDataAdapter objAdapter = new OleDbDataAdapter(objCmdSelect);
                DataTable Dt = new DataTable();
                objAdapter.Fill(Dt);

                objConnection.Close();
                return Dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool isEmail(string inputEmail)
        {
            string strRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                  @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                  @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            Regex re = new Regex(strRegex);
            if (re.IsMatch(inputEmail))
                return (true);
            else
                return false;
        }


        #region Code For Export Colleague list
        public ActionResult ExportToExcel()
        {
            DataTable dt = new DataTable();
            dt = objColleaguesData.GetColleaguesDetailsForDoctor(Convert.ToInt32(Session["UserId"]), 1, int.MaxValue, null, 1, 2);
            if (dt != null && dt.Rows.Count > 0)
            {
                dt.AsEnumerable()
    .Select(row => row["ImageName"] = (String.IsNullOrEmpty(Convert.ToString(row["ImageName"]))) ? row["ImageName"] : Convert.ToString(row["ImageName"]).Contains("$") ? Convert.ToString(row["ImageName"]).Split('$')[2] : Convert.ToString(row["ImageName"]))
    .ToList();
                dt.Columns.Remove("ColleagueId");
                //dt.Columns.Remove("RowNumber");
                dt.Columns.Remove("TotalRecord");
                dt.AcceptChanges();
                ObjCommon.ExportToExcel(dt, "Colleagues_Record");
            }
            return null;
        }
        #endregion


        public string GetLocationColleagues(int ColleagueId)
        {
            try
            {

                List<Models.Colleagues.AddressDetails> AddressDetails = new List<Models.Colleagues.AddressDetails>();

                clsColleaguesData objColleaguesData = new clsColleaguesData();
                clsCommon objCommon = new clsCommon();
                DataTable ds = new DataTable();
                ds = objColleaguesData.GetDoctorLocaitonById(ColleagueId);
                string GetString = string.Empty;
                if (ds != null && ds.Rows.Count > 0)
                {
                    //For Address details of Doctor

                    AddressDetails = (from p in ds.AsEnumerable()
                                      select new Models.Colleagues.AddressDetails
                                      {
                                          AddressInfoID = Convert.ToInt32(p["AddressInfoID"]),

                                          ExactAddress = objCommon.CheckNull(Convert.ToString(p["ExactAddress"]), string.Empty),
                                          Address2 = objCommon.CheckNull(Convert.ToString(p["Address2"]), string.Empty),
                                          City = objCommon.CheckNull(Convert.ToString(p["City"]), string.Empty),
                                          State = objCommon.CheckNull(Convert.ToString(p["State"]), string.Empty),
                                          Country = objCommon.CheckNull(Convert.ToString(p["Country"]), string.Empty),
                                          ZipCode = objCommon.CheckNull(Convert.ToString(p["ZipCode"]), string.Empty),
                                          Phone = objCommon.CheckNull(Convert.ToString(p["Phone"]), string.Empty),
                                          ContactType = Convert.ToInt32(p["ContactType"]),
                                          Location = objCommon.CheckNull(Convert.ToString(p["Location"]), string.Empty)
                                      }).ToList();
                    ViewBag.ColleagueId = ColleagueId;
                    ViewData.Model = AddressDetails;
                    if (AddressDetails.Count > 1)
                    {
                        string viewName = "PartialColleaguesLocation";

                        using (var sw = new StringWriter())
                        {
                            var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                            var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                            viewResult.View.Render(viewContext, sw);
                            viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                            GetString = sw.GetStringBuilder().ToString();
                        }
                        return GetString;
                    }
                    return "1";
                }

            }
            catch (Exception)
            {
                throw;
            }
            return "2";
        }
        public bool CheckUserExistorNot(DataTable dt, int Userid)
        {
            var html = GetHTMLofPatinetRecordw(dt);
            ObjTemplate.InviteColleaguesMailForSupport(html, Userid);
            return true;
        }
        public string GetHTMLofPatinetRecordw(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(@"<table><thead><tr style='border: 1px solid #ddd;'>");
            foreach (DataColumn item in dt.Columns)
            {
                if (Convert.ToString(item) == "Email" || Convert.ToString(item) == "First Name" || Convert.ToString(item) == "Last Name" || Convert.ToString(item) == "Phone")
                {
                    sb.Append(@"<td><strong>" + item.ToString() + "</strong></td>");
                }
            }
            sb.Append(@"</tr></thead><tbody>");
            foreach (DataRow item in dt.Rows)
            {
                sb.Append(@"<tr style='border: 1px solid #ddd;'>");
                foreach (DataColumn item1 in dt.Columns)
                {
                    if (Convert.ToString(item1) == "Email" || Convert.ToString(item1) == "First Name" || Convert.ToString(item1) == "Last Name" || Convert.ToString(item1) == "Phone")
                    {
                        sb.Append(@"<td>" + item[item1].ToString() + "</td>");
                    }
                }
                sb.Append(@"</tr>");
            }
            sb.Append(@"</tbody></table>");
            return sb.ToString();
        }
        [CustomAuthorize]
        public ActionResult Index2()
        {

            clsRestriction objRestriction = new clsRestriction();
            int Count = objRestriction.GetCountOfColleaguesByUserId(Convert.ToInt32(SessionManagement.UserId));

            return View();
        }

        public ActionResult ColleaguesListing(FilterColleagues Obj)
        {
            //Sorting Obj = new Sorting();
            if (Obj.NewSearchText == "All")
            {
                Obj.NewSearchText = null;
                Obj.SortColumn = 12;
                Obj.FilterBy = 0;
            }
            List<ColleagueDetails> List = new List<ColleagueDetails>();
            Obj.Id = Convert.ToInt32(SessionManagement.UserId);
            List = ColleaguesBLL.GetColleaguesForOCR(Obj, Obj.Id);
            foreach (var item in List)
            {
                DataTable dtReferralRece = new DataTable();
                DataTable dtReferralSent = new DataTable();
                dtReferralRece = objColleaguesData.GetReferralReceivedCountOfDoctor(Convert.ToInt32(Session["UserId"]), item.ColleagueId);
                dtReferralSent = objColleaguesData.GetReferralSentCountOfDoctor(Convert.ToInt32(Session["UserId"]), item.ColleagueId);
                item.refferralreceived = Convert.ToInt32(dtReferralRece.Rows[0]["TotalRecord"]);
                item.referralsent = Convert.ToInt32(dtReferralSent.Rows[0]["TotalRecord"]);
            }
            if (Obj.IsDashboard)
            {
                return View("_PartialDashboardColleagueItems", List);
            }
            else
            {
                return View("ColleaguesListing", List);
            }
        }

        //New 
        public ActionResult SearchColleagues(int PageIndex = 1)
        {
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                MdlColleagues = new mdlColleagues();
                List<SearchColleagueForDoctor> lst = new List<SearchColleagueForDoctor>();
                lst = NewSearchColleaguesResult(Session["UserId"].ToString(), PageIndex, PageSize, null, null, null, null, null, null, null, null, null, null);

                //ViewBag.SpecialitiesCheckBoxScript = SpecialitiesCheckBoxScript("");

                return View("NewSearchColleagues", lst);
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        public ActionResult NewInviteColleagues()
        {
            if (SessionManagement.UserId != null && SessionManagement.UserId != "")
            {
                return PartialView("PartialInviteColleagues");
            }
            else
            {
                return RedirectToAction("Index", "User");
            }
        }

        public ActionResult Inserthasbuttonval(List<ColleagueDetails> hasbuttonList)
        {
            bool res = true;
            res = ColleaguesBLL.Inserthasbuttonval(hasbuttonList, Convert.ToInt32(SessionManagement.UserId));
            return Json(res);
        }
        [HttpPost]
        public ActionResult EnableDisablehasbuttonval(int ReceiverId)
        {
            bool res = true;
            res = ColleaguesBLL.EnableDisablehasbuttonval(ReceiverId, Convert.ToInt32(SessionManagement.UserId));
            return Json(res);
        }
        public ActionResult CheckColleagueStatusInForInvite(string txtemail)
        {
            //Check Email in system.
            string response = "";
            int UserId = Convert.ToInt32(Convert.ToInt32(Session["UserId"]));
            DataSet dsUser = new DataSet();
            dsUser = objColleaguesData.GetDoctorDetailsById(Convert.ToInt32(Session["UserId"]));
            if (dsUser.Tables.Count > 0)
            {
                string CurrentUserEmail = ObjCommon.CheckNull(Convert.ToString(dsUser.Tables[0].Rows[0]["Username"]), string.Empty);
                if (CurrentUserEmail == txtemail)
                {
                    response = "Self";
                    return Json(response);
                }
            }
            DataTable CheckEmail = new DataTable();
            CheckEmail = objColleaguesData.CheckEmailInSystem(Convert.ToString(txtemail));
            if (CheckEmail.Rows.Count == 0)
            {
                //Invite
                DataTable Dtsendmail = new DataTable();
                Dtsendmail.Columns.Add("Email");
                Dtsendmail.Rows.Add(txtemail);
                response = (CheckUserExistorNot(Dtsendmail, Convert.ToInt32(SessionManagement.UserId))) == true ? "Invite" : "0";
                return Json(response);
            }
            else
            {
                //Check colleague or not
                DataTable ColleagueExist = new DataTable();
                ColleagueExist = objColleaguesData.GetColleagueExistByEmail(UserId, txtemail);
                if (ColleagueExist.Rows.Count > 0)
                {
                    response = "Exist";//Colleague Exist
                    return Json(response);
                }
                else
                {
                    //Add Colleauge
                    int ColleagueId = Convert.ToInt32(CheckEmail.Rows[0]["UserId"]);
                    MdlColleagues = new mdlColleagues();
                    bool status = MdlColleagues.AddAsColleague(UserId, ColleagueId);
                    if (status)
                    {
                        response = "Added";
                        //Send Template to colleague
                        ObjTemplate.ColleagueInviteDoctor(Convert.ToInt32(Session["UserId"]), Convert.ToInt32(ColleagueId), "");
                        return Json(response);
                    }
                    else
                    {
                        response = "Failed";
                        return Json(response);

                    }
                }
            }
        }

        //Location partial view
        public ActionResult LocationList(string UserId)
        {
            List<BO.ViewModel.AddressDetails> lst = new List<BO.ViewModel.AddressDetails>();
            lst = ColleaguesBLL.GetDentistLocationsById(UserId);
            return View("_PartialReferPatientPopUp", lst);
        }
        public ActionResult UpdateHasButton(int[] ColleaguesId, bool IsEnable)
        {
            return Json(ColleaguesBLL.UpdateHasButton(ColleaguesId, Convert.ToInt32(SessionManagement.UserId), IsEnable), JsonRequestBehavior.AllowGet);
        }
    }
}
