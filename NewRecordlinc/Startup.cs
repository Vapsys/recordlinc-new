﻿using Hangfire;
using Hangfire.Logging;
using Hangfire.Dashboard;
using Hangfire.SqlServer;
using Microsoft.Owin;
using NewRecordlinc.Models;
using Owin;
using System.Web.Security;

[assembly: OwinStartupAttribute(typeof(NewRecordlinc.Startup))]
namespace NewRecordlinc
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configuration.UseSqlServerStorage("CONNECTIONSTRING");
            app.UseHangfireDashboard("/hangfire", new DashboardOptions()
            {
                Authorization = new[] { new HangFireAuthorizationFilter() }
            });
            app.UseHangfireServer();
        }
    }
}