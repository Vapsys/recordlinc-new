﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Optimization;
using NewRecordlinc.Models;
using DataAccessLayer.Common;
using System.Data;
using NewRecordlinc.Controllers;
using NewRecordlinc.App_Start;
using System.Web.Http;
using SampleApplication;

namespace NewRecordlinc
{

    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        clsCommon OnjCommon = new clsCommon();


        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new PayCheckAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapMvcAttributeRoutes();
            routes.MapRoute(
                   "p",
                   "p/{username}/{action}",
                   new { controller = "p", action = "AboutDoctor", test = "hello", isWidget = false }
                   );
            routes.MapRoute(
                 "widget",
                 "widget/{username}/{action}",
                 new { controller = "p", action = "AboutDoctor", isWidget = true }
                 );

            routes.MapRoute(
                   "r",
                   "Dashboard/",
                   new { controller = "dashboard", action = "dashboard", }
                   );

            //routes.MapRoute(
            //       "r",
            //       "r/{username}/{action}",
            //       new { controller = "r", action = "AboutDoctor" }
            //       );

            routes.MapRoute(
                name: "Landing1",
                url: "A-Dentists-Guide-to-Referrals",
                defaults: new { controller = "Landing", action = "A-Dentists-Guide-to-Referrals" }
                );

            routes.MapRoute(
                name: "Landing2",
                url: "Referral-Machine-for-Dentists",
                defaults: new { controller = "Landing", action = "Referral-Machine-for-Dentists" }
                );

            routes.MapRoute(
                name: "Landing3",
                url: "dos-and-donts-for-online-dentist-reviews",
                defaults: new { controller = "Landing", action = "dos-and-donts-for-online-dentist-reviews" }
                );

            routes.MapRoute(
                name: "Landing4",
                url: "Referral-Autopilot-for-dentist",
                defaults: new { controller = "Landing", action = "Referral-Autopilot-for-dentist" }
                );

            //When You are Published Recordlinc Please uncomment this code and published it.
            routes.MapRoute(
               name: "Default", // Route name
               url: "{controller}/{action}/{id}", // URL with parameters
               defaults: new { controller = "User", action = "Index", id = UrlParameter.Optional } // Parameter defaults
           );

            //When You are Published WeaveDental Please uncomment this code and published it.
            // routes.MapRoute(
            //     "Default", // Route name for WeaveDental
            //     "{controller}/{action}/{id}", // URL with parameters
            //     new { controller = "Login", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            // );

        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            System.Web.Helpers.AntiForgeryConfig.SuppressXFrameOptionsHeader = true;
        }

        public static void RegisterBundles(BundleCollection Bundle)
        {

            Bundle.Add(new StyleBundle("~/StyleBundle").Include(
                        "~/content/css/toolbar.css",
                        "~/content/css/backend.css",
                        "~/content/css/media-query.css"));

            Bundle.Add(new ScriptBundle("~/ScriptBundle").Include(
                        "~/content/js/jquery-1.8.3.min.js",
                        "~/content/js/html5.js",
                        "~/content/js/modernizr.custom.js",
                        "~/Content/js/Common.js"));
        }

        protected void Application_AcquireRequestState(object sender, EventArgs e)
        {

        }

        void Application_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError().GetBaseException();
            var strRequestUrl = Request.Url.ToString();
            if (strRequestUrl.IndexOf("/images/") < 0
                    && strRequestUrl.IndexOf("/DentistImages/") < 0
                    && strRequestUrl.IndexOf("/ImageBank/") < 0
                    && strRequestUrl.IndexOf("favicon.ico") < 0)
            {
                bool status = OnjCommon.InsertErrorLog(Request.Url.ToString(), exc.Message, exc.StackTrace);
            }
            bool isAjaxCall = string.Equals("XMLHttpRequest", Context.Request.Headers["x-requested-with"], StringComparison.OrdinalIgnoreCase);
            bool isAjaxCalls = new HttpRequestWrapper(Context.Request).IsAjaxRequest();
            Server.ClearError();
            if (!isAjaxCall)
            {
                if (exc is HttpException)
                {
                    Response.Redirect("/Error");
                }
            }
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

            try
            {

                Response.Cache.SetNoStore();
                //Code for set www by default


                if (!HttpContext.Current.Request.Url.ToString().ToLower().Contains("http://localhost") && !HttpContext.Current.Request.Url.ToString().ToLower().Contains("qa.") && !HttpContext.Current.Request.Url.ToString().ToLower().Contains("http://192.168") && !HttpContext.Current.Request.Url.ToString().ToLower().Contains("variance") && !HttpContext.Current.Request.Url.ToString().ToLower().Contains("variance-20") && !HttpContext.Current.Request.Url.ToString().ToLower().Contains("162.42.247.91"))
                {
                    if (HttpContext.Current.Request.Url.Scheme.ToLower() != "https")
                    {

                        //if (HttpContext.Current.Request.Url.ToString().ToLower().Contains("www"))
                        //{
                        //    HttpContext.Current.Response.Redirect(HttpContext.Current.Request.Url.ToString().ToLower().Replace("http://", "https://"));
                        //}
                        //else
                        //{
                        //    HttpContext.Current.Response.Redirect(HttpContext.Current.Request.Url.ToString().ToLower().Replace("http://", "https://www."));

                        //}

                    }
                }



                //remove
            }
            catch (Exception)
            {
            }
        }

        public static bool HasSpecialCharacters(string str)
        {
            string specialCharacters = @"%!@#$%^&*()/>.<,:;'\|}]{[_~`+=-" + "\"";
            char[] specialCharactersArray = specialCharacters.ToCharArray();

            int index = str.IndexOfAny(specialCharactersArray);
            if (index == -1)
                return false;
            else
                return true;
        }


    }
}















