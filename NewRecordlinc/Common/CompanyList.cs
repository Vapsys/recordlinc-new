﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace NewRecordlinc.Common
{
    public class CompanyList
    {

        public static SqlConnection cn = new SqlConnection();

        public static DataSet ds = new DataSet();
        public static SqlDataAdapter da = new SqlDataAdapter();
        public static DataSet ChechActiveCompany()
        {
            try
            {
                SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["CONNECTIONSTRING"].ConnectionString);
                cn.Open();
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = " SELECT *,CompantList_Address.CompanyAddress1+' '+CompantList_Address.CompanyAddress2 as CompanyAddress FROM CompantList_Address INNER JOIN CompanyList ON CompantList_Address.CompanyId = CompanyList.CompanyId INNER JOIN " +
                        " CompanyList_Logo ON CompanyList.CompanyId = CompanyList_Logo.CompanyId INNER JOIN " +
                         " CompanyList_NetworkCredential ON CompanyList.CompanyId = CompanyList_NetworkCredential.CompanyId  and (CompanyList.Isactive = 1) order by isprimary desc ";


                da.SelectCommand = cmd;
                cmd.Connection = cn;
                cmd.ExecuteNonQuery();
                da.Fill(ds);
                cn.Close();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ds;
        }



        public static void GetActiveCompany()
        {
            if (HttpContext.Current.Session["CompanyId"] == null)
            {
                ds = ChechActiveCompany();
                if (ds != null && ds.Tables[0].Rows.Count > 0)
                {
                    if (HttpContext.Current.Session["CompanyName"] == null)
                        HttpContext.Current.Session["CompanyName"] = ds.Tables[0].Rows[0]["CompanyName"].ToString();
                    if (HttpContext.Current.Session["CompanyAddress"] == null)
                        HttpContext.Current.Session["CompanyAddress"] = ds.Tables[0].Rows[0]["CompanyAddress"].ToString();
                    if (HttpContext.Current.Session["CompanyCity"] == null)
                        HttpContext.Current.Session["CompanyCity"] = ds.Tables[0].Rows[0]["CompanyCity"].ToString();
                    if (HttpContext.Current.Session["CompanyState"] == null)
                        HttpContext.Current.Session["CompanyState"] = ds.Tables[0].Rows[0]["CompanyState"].ToString();
                    if (HttpContext.Current.Session["CompanyZipCode"] == null)
                        HttpContext.Current.Session["CompanyZipCode"] = ds.Tables[0].Rows[0]["CompanyZipCode"].ToString();
                    if (HttpContext.Current.Session["CompanyId"] == null)
                        HttpContext.Current.Session["CompanyId"] = Convert.ToInt32(ds.Tables[0].Rows[0]["CompanyId"]);
                    if (HttpContext.Current.Session["LogoPath"] == null)
                        HttpContext.Current.Session["LogoPath"] = ds.Tables[0].Rows[0]["LogoPath"].ToString();
                    if (HttpContext.Current.Session["CompanyWebsite"] == null)
                        HttpContext.Current.Session["CompanyWebsite"] = ds.Tables[0].Rows[0]["CompanyWebsite"].ToString();
                    if (HttpContext.Current.Session["CompanySalesEmail"] == null)
                        HttpContext.Current.Session["CompanySalesEmail"] = ds.Tables[0].Rows[0]["SalesEmail"].ToString();
                    if (HttpContext.Current.Session["CompanySupportEmail"] == null)
                        HttpContext.Current.Session["CompanySupportEmail"] = ds.Tables[0].Rows[0]["SupportEmail"].ToString();
                    if (HttpContext.Current.Session["PhoneNo"] == null)
                        HttpContext.Current.Session["PhoneNo"] = ds.Tables[0].Rows[0]["PhoneNo"].ToString();
                    if (HttpContext.Current.Session["CompanyOwnerName"] == null)
                        HttpContext.Current.Session["CompanyOwnerName"] = ds.Tables[0].Rows[0]["CompanyOwnerName"].ToString();
                    if (HttpContext.Current.Session["CompanyOwnerTitle"] == null)
                        HttpContext.Current.Session["CompanyOwnerTitle"] = ds.Tables[0].Rows[0]["CompanyOwnerTitle"].ToString();
                    if (HttpContext.Current.Session["CompanyOwnerEmail"] == null)
                        HttpContext.Current.Session["CompanyOwnerEmail"] = ds.Tables[0].Rows[0]["CompanyOwnerEmail"].ToString();

                    if (HttpContext.Current.Session["CompanyFacebook"] == null)
                        HttpContext.Current.Session["CompanyFacebook"] = ds.Tables[0].Rows[0]["CompanyFacebook"].ToString();
                    if (HttpContext.Current.Session["CompanyBlog"] == null)
                        HttpContext.Current.Session["CompanyBlog"] = ds.Tables[0].Rows[0]["CompanyBlog"].ToString();
                    if (HttpContext.Current.Session["CompanyYoutube"] == null)
                        HttpContext.Current.Session["CompanyYoutube"] = ds.Tables[0].Rows[0]["CompanyYoutube"].ToString();
                    if (HttpContext.Current.Session["CompanyLinkedin"] == null)
                        HttpContext.Current.Session["CompanyLinkedin"] = ds.Tables[0].Rows[0]["CompanyLinkedin"].ToString();
                    if (HttpContext.Current.Session["CompanyTwitter"] == null)
                        HttpContext.Current.Session["CompanyTwitter"] = ds.Tables[0].Rows[0]["CompanyTwitter"].ToString();
                    if (HttpContext.Current.Session["CompanyGooglePlus"] == null)
                        HttpContext.Current.Session["CompanyGooglePlus"] = ds.Tables[0].Rows[0]["CompanyGooglePlus"].ToString();
                    if (HttpContext.Current.Session["CompanyFacebookLike"] == null)
                        HttpContext.Current.Session["CompanyFacebookLike"] = ds.Tables[0].Rows[0]["CompanyFacebookLike"].ToString();
                    if (HttpContext.Current.Session["CompanyAlexa"] == null)
                        HttpContext.Current.Session["CompanyAlexa"] = ds.Tables[0].Rows[0]["CompanyAlexa"].ToString();

                    if (HttpContext.Current.Session["CompanyPinterest"] == null)
                        HttpContext.Current.Session["CompanyPinterest"] = ds.Tables[0].Rows[0]["CompanyPinterest"].ToString();

                    if (HttpContext.Current.Session["CompanyVimeo"] == null)
                        HttpContext.Current.Session["CompanyVimeo"] = ds.Tables[0].Rows[0]["CompanyVimeo"].ToString();

                    if (HttpContext.Current.Session["CompanyGoogle"] == null)
                        HttpContext.Current.Session["CompanyGoogle"] = ds.Tables[0].Rows[0]["CompanyGoogle"].ToString();

                    }
            }

        }


        public string EmailSubject(string TempSubject)
        {
            try
            {
                DataSet dsCompany = new DataSet();
                dsCompany = ChechActiveCompany();
                if (dsCompany != null && dsCompany.Tables.Count > 0 && dsCompany.Tables[0].Rows.Count > 0)
                {
                    string CompanyName = dsCompany.Tables[0].Rows[0]["CompanyName"].ToString();
                    string CompanyCity = dsCompany.Tables[0].Rows[0]["CompanyCity"].ToString();
                    string CompanyState = dsCompany.Tables[0].Rows[0]["CompanyState"].ToString();
                    string CompanyCountry = dsCompany.Tables[0].Rows[0]["CompanyCountry"].ToString();
                    string CompanyAddress = "@ " + DateTime.Now.Year.ToString() + " " + CompanyName + " Inc." + " " + CompanyCity + " " + CompanyState + ", " + CompanyCountry + ".";
                    string CompanyWebsite = dsCompany.Tables[0].Rows[0]["CompanyWebsite"].ToString();
                    string CompanyLogo = dsCompany.Tables[0].Rows[0]["LogoPath"].ToString();
                    CompanyLogo = CompanyLogo.Replace("~/", "/");
                    CompanyLogo = CompanyWebsite + CompanyLogo;
                    if (HttpContext.Current.Session["DonateDentist.org"] != null && Convert.ToString(HttpContext.Current.Session["DonateDentist.org"]) == "DonateDentist.org")
                    {
                        TempSubject = TempSubject.Replace("#CompanyName#", "DonateDentist.org");
                    }
                    else
                    {
                        TempSubject = TempSubject.Replace("#CompanyName#", CompanyName);
                    }

                    TempSubject = TempSubject.Replace("#CompanyCity#", CompanyCity);
                    TempSubject = TempSubject.Replace("#CompanyState#", CompanyState);
                    TempSubject = TempSubject.Replace("#CompanyCountry#", CompanyCountry);

                }
                return TempSubject;
            }
            catch (Exception )
            {
                return TempSubject;
            }
        }


        public string EmailBodyContent(string strEmailBody)
        {
            try
            {
                DataSet dsCompany = new DataSet();
                dsCompany = ChechActiveCompany();
                if (dsCompany != null && dsCompany.Tables.Count > 0 && dsCompany.Tables[0].Rows.Count > 0)
                {
                    string CompanyName = dsCompany.Tables[0].Rows[0]["CompanyName"].ToString();
                    string CompanyCity = dsCompany.Tables[0].Rows[0]["CompanyCity"].ToString();
                    string CompanyState = dsCompany.Tables[0].Rows[0]["CompanyState"].ToString();
                    string CompanyCountry = dsCompany.Tables[0].Rows[0]["CompanyCountry"].ToString();
                    string CompanyZipCode = dsCompany.Tables[0].Rows[0]["CompanyZipCode"].ToString();
                    string CompanyPhone = dsCompany.Tables[0].Rows[0]["PhoneNo"].ToString();
                    string CompanyOwnerName = dsCompany.Tables[0].Rows[0]["CompanyOwnerName"].ToString();
                    string CompanyOwnerTitle = dsCompany.Tables[0].Rows[0]["CompanyOwnerTitle"].ToString();
                    string CompanyOwnerEmail = dsCompany.Tables[0].Rows[0]["CompanyOwnerEmail"].ToString();

                    string CompanyAddress = "@ " + DateTime.Now.Year.ToString() + " " + CompanyName + " Inc." + " " + CompanyCity + " " + CompanyState + ", " + CompanyCountry + ".";
                    string CompanyWebsite = dsCompany.Tables[0].Rows[0]["CompanyWebsite"].ToString();
                    string CompanyLogo = dsCompany.Tables[0].Rows[0]["LogoPath"].ToString();
                    string CompanySupportEmail = dsCompany.Tables[0].Rows[0]["SupportEmail"].ToString();
                    CompanyLogo = CompanyLogo.Replace("~/", "/");
                    CompanyLogo = CompanyLogo.Replace("../", "/");
                    CompanyLogo = CompanyWebsite + CompanyLogo;


                    strEmailBody = strEmailBody.Replace("#CompanyLogo#", CompanyLogo);
                    strEmailBody = strEmailBody.Replace("#CompanyJoinUrl#", CompanyWebsite);
                    strEmailBody = strEmailBody.Replace("#loginlink#", CompanyWebsite);
                    strEmailBody = strEmailBody.Replace("#CompanyName#", CompanyName);
                    strEmailBody = strEmailBody.Replace("#CompanyCity#", CompanyCity);
                    strEmailBody = strEmailBody.Replace("#CompanyState#", CompanyState);
                    strEmailBody = strEmailBody.Replace("#CompanyCountry#", CompanyCountry);
                    strEmailBody = strEmailBody.Replace("#CompanyZipCode#", CompanyZipCode);
                    strEmailBody = strEmailBody.Replace("#CompanyPhone#", CompanyPhone);
                    strEmailBody = strEmailBody.Replace("#CompanyWebsite#", CompanyWebsite);
                    strEmailBody = strEmailBody.Replace("#CompanySupportEmail#", CompanySupportEmail);
                    strEmailBody = strEmailBody.Replace("#CompanyOwnerEmail#", CompanyOwnerEmail);
                    strEmailBody = strEmailBody.Replace("#CompanyOwnerName#", CompanyOwnerName);
                    strEmailBody = strEmailBody.Replace("#CompanyOwnerTitle#", CompanyOwnerTitle);

                    strEmailBody = strEmailBody.Replace("#CompanyYear#", DateTime.Now.Year.ToString());
                }
                return strEmailBody;
            }
            catch (Exception )
            {
                return strEmailBody;
            }
        }
    }
}
