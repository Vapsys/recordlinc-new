﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Drawing.Imaging;

namespace NewRecordlinc.Common
{
    public class Common
    {
        public static Bitmap ImageThumbnail(string lcFilename, int lnWidth, int lnHeight)
        {
            System.Drawing.Bitmap bmpOut = null;
            try
            {


                Bitmap loBMP = new Bitmap(lcFilename);
                ImageFormat loFormat = loBMP.RawFormat;

                decimal lnRatio;
                int lnNewWidth = 0;
                int lnNewHeight = 0;



                /////*** If the image is smaller than a thumbnail just return it
                if (loBMP.Width < lnWidth && loBMP.Height < lnHeight)
                    return loBMP;

                if (loBMP.Width > loBMP.Height)
                {
                    lnRatio = (decimal)lnWidth / loBMP.Width;
                    lnNewWidth = lnWidth;
                    decimal lnTemp = loBMP.Height * lnRatio;
                    lnNewHeight = (int)lnTemp;
                }
                else
                {
                    lnRatio = (decimal)lnHeight / loBMP.Height;
                    lnNewHeight = lnHeight;
                    decimal lnTemp = loBMP.Width * lnRatio;
                    lnNewWidth = (int)lnTemp;
                }

                bmpOut = new Bitmap(lnNewWidth, lnNewHeight);
                Graphics g = Graphics.FromImage(bmpOut);
                g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                g.FillRectangle(Brushes.White, 0, 0, lnNewWidth, lnNewHeight);
                g.DrawImage(loBMP, 0, 0, lnNewWidth, lnNewHeight);

                loBMP.Dispose();
            }
            catch
            {
                return null;
            }

            return bmpOut;
        }



    }

    public class ImageClass
    {
        public static string imageToByteArray(HttpPostedFileBase imageurl)
        {
            Image image = null;
            if (imageurl.ContentLength > 0)
            {
                var filename = Path.GetFileName(imageurl.FileName);
                image = Image.FromStream(imageurl.InputStream);
            }

            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, ImageFormat.Png);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

        public static string byteArrayToImage(string img)
        {
            return "data:image/png;base64," + EncryptDecrypt.Decrypt(img);
        }

    }

    public static class FileExtension
    {

        public static string CheckFileExtenssion(string DocName)
        {
            try
            {
                string Ext = string.IsNullOrEmpty(DocName) ? "" : Path.GetExtension(DocName);
                switch (Ext.ToLower())
                {
                    case ".doc": return "../../Content/images/word.png";
                    case ".docx": return "../../Content/images/word.png";
                    case ".xls": return "../../Content/images/excel.png";
                    case ".csv": return "../../Content/images/excel.png";
                    case ".xlsx": return "../../Content/images/excel.png";
                    case ".pdf": return "../../Content/images/pdf.png";
                    case ".dex": return "../../Content/images/dex.png";
                    case ".dcm": return "../../Content/images/dcm.png";
                    case ".tif": return "../../Content/images/img_1.jpg";
                    case ".TIF": return "../../Content/images/img_1.jpg";
                    case ".pspimage": return "../../Content/images/img_1.jpg";
                    case ".thm": return "../../Content/images/img_1.jpg";
                    default: return "../../Content/images/img_1.jpg";
                }
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string CheckFileExtenssionOnReferral(string DocName)
        {
            try
            {
                string Ext = string.IsNullOrEmpty(DocName) ? "" : Path.GetExtension(DocName);
                switch (Ext.ToLower())
                {
                    case ".doc": return "../../Content/images/word.png";
                    case ".docx": return "../../Content/images/word.png";
                    case ".xls": return "../../Content/images/excel.png";
                    case ".csv": return "../../Content/images/excel.png";
                    case ".xlsx": return "../../Content/images/excel.png";
                    case ".dex": return "../../Content/images/dex.png";
                    case ".pdf": return "../../Content/images/pdf.png";
                    case ".dcm": return "../../Content/images/dcm.png";
                    case ".yuv": return "../ImageBank/" + DocName + "";
                    default: return "../ImageBank/"+ DocName + "";
                }
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static string CheckFileExtenssionOnlyThumbName(string DocName)
        {
            try
            {
                string Ext = string.IsNullOrEmpty(DocName) ? "" : Path.GetExtension(DocName);
                switch (Ext.ToLower())
                {
                    case ".doc": return "word.png";
                    case ".docx": return "word.png";
                    case ".xls": return "excel.png";
                    case ".csv": return "excel.png";
                    case ".xlsx": return "excel.png";
                    case ".dex": return "dex.png";
                    case ".pdf": return "pdf.png";
                    case ".dcm": return "dcm.png";
                    case ".yuv": return "img_1.jpg";
                    case ".tif": return "img_1.jpg";
                    case ".TIF": return "img_1.jpg";
                    case ".thm": return "img_1.jpg";
                    case ".pspimage": return "img_1.jpg";                    
                    default: return "img_1.jpg";
                }
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}