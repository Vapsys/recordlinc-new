﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

namespace NewRecordlinc.Common
{
    public class EncryptDecrypt
    {
        public static string strUserKey = "asjieJgyweiKHshgfksjdf";

        public static string Encrypt(string strToEncrypt)
        {
            try
            {
                if (strToEncrypt != "")
                {


                    TripleDESCryptoServiceProvider objDESCrypto = new TripleDESCryptoServiceProvider();
                    MD5CryptoServiceProvider objHashMD5 = new MD5CryptoServiceProvider();
                    byte[] byteHash, byteBuff;
                    string strTempKey = strUserKey;
                    byteHash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
                    objHashMD5 = null;
                    objDESCrypto.Key = byteHash;
                    objDESCrypto.Mode = CipherMode.ECB; 
                    byteBuff = ASCIIEncoding.ASCII.GetBytes(strToEncrypt);
                    return Convert.ToBase64String(objDESCrypto.CreateEncryptor().TransformFinalBlock(byteBuff, 0, byteBuff.Length));
                }
                else
                {
                    return "";

                }
            }
            catch (Exception ex)
            {
                return "Wrong Input. " + ex.Message;
            }
        }

        public static string Decrypt(string strEncrypted)
        {
            try
            {
                if (strEncrypted != "")
                {

                    TripleDESCryptoServiceProvider objDESCrypto =
                         new TripleDESCryptoServiceProvider();
                    MD5CryptoServiceProvider objHashMD5 = new MD5CryptoServiceProvider();
                    byte[] byteHash, byteBuff;
                    string strTempKey = strUserKey;
                    byteHash = objHashMD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(strTempKey));
                    objHashMD5 = null;
                    objDESCrypto.Key = byteHash;
                    objDESCrypto.Mode = CipherMode.ECB; 
                    byteBuff = Convert.FromBase64String(strEncrypted);
                    string strDecrypted = ASCIIEncoding.ASCII.GetString
                    (objDESCrypto.CreateDecryptor().TransformFinalBlock
                    (byteBuff, 0, byteBuff.Length));
                    objDESCrypto = null;
                    return strDecrypted;
                }

                else
                {
                    return "";
                }

            }
            catch (Exception ex)
            {
                return "Wrong Input. " + ex.Message;
            }
        }



    }
}