﻿
//For Email Validation - Start
function IsEmail(email) {
    var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regex.test(email);
}
//For Email Validation - End


//For Email Validation with comma separated- Start
function IsEmailwithcomma(email) {
    var regex = /^([a-zA-Z0-9][a-zA-Z0-9_.+-]+@[\w-.]+\.[A-Za-z]{2,4},?)+$/;
    return regex.test(email);
}
//For Email Validation with comma separated - End


//For Text Counter  - Start
function textCounter(field, cnt, maxlimit) {
    
        var cntfield = document.getElementById(cnt)
        if (field.value.length > maxlimit) // if too long...trim it!
            field.value = field.value.substring(0, maxlimit);
        // otherwise, update 'characters left' counter
        else
            cntfield.value = maxlimit - field.value.length;
}
//For Text Counter  - End


//For CharactersOnly - Start
function IsCharactersOnly(data) {
    //data = data.replace(/'/g, "''");
    // var regex = /^[a-zA-Z]*$/;
    var regex = /^[a-zA-Z\/\s\.'-]*$/;
    //var regex = /^[a-zA-Z*]$/;
    // var regex = /^[A-Za-z\/\s\.-]+$/;
    // var regex = /^[a-z A-Z._]{1,15}$/;
    //var regex =  /^([^']([a-zA-Z])+(\s){0,1})+('){0,1}([a-zA-Z][^'])+$/;
    return regex.test(data);
}
//For CharactersOnly - End

//For Character And Special Character - Start
function CheckFirstAndLastName(data) {
    var regex = /^([^0-9]*)$/;
    return regex.test(data);
}

//For Character And Special Character - End


// For Show Processbar - start
function ShowProcessbar() {
    // this is the code for Mouse follow Loding Image 
    $("#divLoading").show();
}
// For Show Processbar - End

// For ZipCode Validation - Start
function ValidateZipCode(data) {
    var regx = /^([FG]?\d{5}|\d{5}[AB])$/;
    return regx.test(data);
}

// For ZipCode Validation - End


// For Phone Validation - Start
function ValidationPhone(data) {
    var RegExPhone = /^(?:\([0-9]\d{2}\)\ ?|[0-9]\d{2}(?:\-?|\ ?))[0-9]\d{2}[- ]?\d{4}$/;
    return RegExPhone.test(data);

}
//For Phone Validation - End


// Funcaion For get query string values from url we just Pass query string name as name - start
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
// Funcaion For get query string values from url - End


function isUrl(s) {
    var regexp = /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/|www\.)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
    return regexp.test(s);
}