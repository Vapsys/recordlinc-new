jQuery(document).ready(function() {	
	
	//Member Login PopUp Open
	jQuery(".MemberLogin a").click(function(){
		jQuery(".MemberLoginDropDown").fadeIn();
		jQuery(".MemberLogin").addClass("active");
	});	
	//Member Login PopUp Close
	jQuery(".MemberLoginCloseBtn").click(function(){
		jQuery(".MemberLoginDropDown").fadeOut();
		jQuery(".MemberLogin").removeClass("active");
	});
	
	jQuery("body *").click(function(e){			
		if(!jQuery(e.target).hasClass('loginbtn')  && !jQuery(e.target).closest('.MemberLoginDropDown').length>0 ){
				jQuery(".MemberLogin").removeClass("active");
				jQuery('.MemberLoginDropDown').hide(); 
			
	       };				
			});
			
});


