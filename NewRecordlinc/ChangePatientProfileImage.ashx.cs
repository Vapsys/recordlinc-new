﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Web.Script.Serialization;
using NewRecordlinc.Models;
using System.IO;

namespace NewRecordlinc
{
    /// <summary>
    /// Summary description for ChangePatientProfileImage
    /// </summary>
    public class ChangePatientProfileImage : IHttpHandler
    {
        public class ViewDataUploadFilesResult
        {
            public string Thumbnail_url { get; set; }
            public string Name { get; set; }
            public int Length { get; set; }
            public string Type { get; set; }
        }
        UpdatePatientImage objCommonDAL = new UpdatePatientImage();

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";

            int userid = Convert.ToInt32(context.Request["PatientId"]);


            try
            {
                HttpPostedFile postedFile = context.Request.Files["fuImage"];
                string Sesionid = context.Request["PatientId"].ToString();

                var r = new System.Collections.Generic.List<ViewDataUploadFilesResult>();
                JavaScriptSerializer js = new JavaScriptSerializer();

                for (int filecount = 0; filecount < context.Request.Files.Count; filecount++)
                {
                    HttpPostedFile hpf = context.Request.Files[filecount] as HttpPostedFile;
                    string FileName = string.Empty;
                    if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
                    {
                        string[] files = hpf.FileName.Split(new char[] { '\\' });
                        FileName = files[files.Length - 1];
                    }
                    else
                    {
                        FileName = hpf.FileName;
                        string[] files = hpf.FileName.Split(new char[] { '\\' });
                        FileName = files[files.Length - 1];
                    }


                    string imgName = FileName + "_" + System.DateTime.Now.ToString("dd-MM-yyyy_HH-mm-ss") + ".jpeg";
                    string OrgImagePath = ConfigurationManager.AppSettings.Get("imagebankpathPatient") + imgName;
                    string ImagePath = imgName;









                    string filename1 = "$" + System.DateTime.Now.Ticks + "$" + FileName;

                    string thambnailimage = filename1.Replace(System.Configuration.ConfigurationManager.AppSettings.Get("imagebankpathPatient"), System.Configuration.ConfigurationManager.AppSettings.Get("imagebankpathThumbPatient"));
                    string extension;
                    if (hpf.ContentLength == 0)
                        continue;


                    extension = Path.GetExtension(FileName);


                    if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd" && extension.ToLower() != ".pspimage" && extension.ToLower() != ".thm" && extension.ToLower() != ".tif")
                    {
                    }
                    else
                    {

                        objCommonDAL.UpdatePatientProfileImg(userid, filename1);
                        string savepath = "";
                        string tempPath = "";
                        tempPath = System.Configuration.ConfigurationManager.AppSettings["FolderPath"];
                        savepath = context.Server.MapPath(tempPath);
                        savepath = savepath.Replace("PublicProfile\\", "");
                        hpf.SaveAs(savepath + @"\" + thambnailimage);
                    }
                }


            }
            catch { }



        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}