﻿using DataAccessLayer.Common;
using NewRecordlinc.Models.Colleagues;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace NewRecordlinc.Models
{
    public partial class PatientHistory
    {
        public int PatientId { get; set; }
        public string AssignedPatientId { get; set; }
        public string EncryptesPId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }

        public string Email { get; set; }
        public string SecondryEmail { get; set; }
        public string AddressInfoEmail { get; set; }

        public string Gender { get; set; }
        public string Age { get; set; }
        public string ExactAddress { get; set; }
        public string Address2 { get; set; }
        public string Phone { get; set; }

        public string Mobile { get; set; }
        public string Workphone { get; set; }
        public string Fax { get; set; }

        public string State { get; set; }
        public string City { get; set; }
        public string ImageName { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string DoctorFullName { get; set; } // this field use in send referral
        public string Notes { get; set; }
        public string FirstExamDate { get; set; }
        public string Location { get; set; }
        public string ReferredBy { get; set; }
        public string ReferredByName { get; set; }
        public string IsAuthorize { get; set; }
        public string ProfileImage { get; set; }
        public string FullName { get; set; }
        public string MiddelName { get; set; }
        public string ExactPassword { get; set; }
        public string Password { get; set; }
        public string ReferredById { get; set; }
        public int OwnerId { get; set; }
        public List<ColleaguesMessagesOfDoctor> lstColleaguesMessagesDoctor { get; set; }
        public List<UplodedImages> lstUplodedImages { get; set; }
        public List<UplodedImages> lstUplodedDocuments { get; set; }
    }
    public partial class MessageAttachments
    {
        public int Id { get; set; }
        public int MessageId { get; set; }
        public string AttachmentName { get; set; }
    }
    public partial class UplodedImages
    {
        public int ImageId { get; set; }
        public int PatientId { get; set; }
        public string Name { get; set; }
        public string ImagePath { get; set; }
        public string RelativePath { get; set; }
        public string CreationDate { get; set; }
        public string LastModifiedDate { get; set; }
        public int MontageId { get; set; }
        public int Timepoint { get; set; }
        public string FullPath { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        public int ImageTypeId { get; set; }
        public string ImageFormat { get; set; }
        public string Description { get; set; }
        public string Uploadby { get; set; }
        public bool ImageStatus { get; set; }
        public int ImagePosition { get; set; }
        public string ImageTypeName { get; set; }
        public string DateForApi { get; set; }
        public string DocumentName { get; set; }
        public int DocumentId { get; set; }

    }
    public partial class EditFiles
    {
        public int ImageId { get; set; }
        public int PatientId { get; set; }
        public string Name { get; set; }
        public string CreationDate { get; set; }
        public string LastModifiedDate { get; set; }
        public int MontageId { get; set; }
        public int Timepoint { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        public int ImageTypeId { get; set; }
        public string ImageFormat { get; set; }
        public string Description { get; set; }
        public string Uploadby { get; set; }
        public bool ImageStatus { get; set; }
        public int PositionInMontage { get; set; }
        public int ImageFormatTypeId { get; set; }
        public string FullPathToImage { get; set; }
        public string RelativePathToImage { get; set; }
        public int LastModifiedBy { get; set; }
        public string DocumentName { get; set; }
        public int DocumentId { get; set; }
        public int OwnerId { get; set; }
        public string CreationDateForApi { get; set; }
        public string LastModifiedDateForApi { get; set; }
        public string CreationDateNew { get; set; }
        public string LastModifiedDateNew { get; set; }

    }

    public partial class PatientHistoryMethod
    {
        public List<PatientHistory> GetPatientHistorys(DataTable dtTable, string TimeZoneSystemName)
        {
            List<PatientHistory> objpatientMessage = new List<PatientHistory>();
            if (dtTable != null && dtTable.Rows.Count > 0)
            {
                objpatientMessage = (from p in dtTable.AsEnumerable()
                                     select new PatientHistory
                                     {
                                         PatientId = Convert.ToInt32(p["PatientId"]),
                                         Gender = Convert.ToString(p["Gender"]) == "0" ? "Male" : "Female",
                                         Age = Convert.ToString(p["Age"]),
                                         FirstName = Convert.ToString(p["FirstName"]),
                                         LastName = Convert.ToString(p["LastName"]),
                                         //DateOfBirth = Convert.ToString(p["DateOfBirth"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["DateOfBirth"]), TimeZoneSystemName)) : string.Empty,//Convert.ToString(p["DateOfBirth"]),
                                         DateOfBirth = Convert.ToString(p["DateOfBirth"]), // SUP-119 Removing UTC for DOB 
                                         AssignedPatientId = Convert.ToString(p["AssignedPatientId"]),
                                         OwnerId = Convert.ToInt32(p["OwnerId"]),
                                         Email = Convert.ToString(p["Email"]),
                                         SecondryEmail = Convert.ToString(p["SecondaryEmail"]),
                                         AddressInfoEmail = Convert.ToString(p["EmailAddress"]),
                                         ProfileImage = Convert.ToString(p["ProfileImage"]),
                                         Notes = Convert.ToString(p["Notes"]),
                                         ExactAddress = Convert.ToString(p["ExactAddress"]),
                                         Address2 = Convert.ToString(p["Address2"]),
                                         City = Convert.ToString(p["City"]),
                                         State = Convert.ToString(p["STATE"]),
                                         Country = Convert.ToString(p["Country"]),
                                         ZipCode = Convert.ToString(p["ZipCode"]),
                                         Phone = Convert.ToString(p["Phone"]),
                                         Workphone = Convert.ToString(p["WorkPhone"]),
                                         Mobile = Convert.ToString(p["Mobile"]),
                                         Fax = Convert.ToString(p["Fax"]),
                                         FullName = Convert.ToString(p["FullName"]),
                                         MiddelName = Convert.ToString(p["MiddelName"]),
                                         //ImageName = Convert.ToString(p["ImageName"]),
                                         ExactPassword = Convert.ToString(p["ExactPassword"]),
                                         ReferredById = Convert.ToString(p["ReferredById"]),
                                         ReferredBy = Convert.ToString(p["ReferredBy"]),
                                         Location = Convert.ToString(p["Location"]),
                                         FirstExamDate = Convert.ToString(p["FirstExamDate"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["FirstExamDate"]), TimeZoneSystemName)) : string.Empty,//Convert.ToString(p["FirstExamDate"]),
                                         EncryptesPId = string.Empty,
                                         DoctorFullName = string.Empty,
                                         Password = Convert.ToString(p["Password"]),
                                         //IsAuthorize = Convert.ToString(p["IsAuthorize"]),                                      

                                     }).ToList();
            }

            return objpatientMessage;
        }

        public List<UplodedImages> PatientImages(DataTable dt, string TimeZoneSystemName)
        {
            List<UplodedImages> objpatientMessage = new List<UplodedImages>();
            if (dt != null && dt.Rows.Count > 0)
            {
                objpatientMessage = (from p in dt.AsEnumerable()
                                     select new UplodedImages
                                     {
                                         ImageId = Convert.ToInt32(p["ImageId"]),
                                         PatientId = Convert.ToInt32(p["PatientId"]),
                                         Name = Convert.ToString(p["Name"]),
                                         ImagePath = Convert.ToString(p["FullPath"]),
                                         RelativePath = Convert.ToString(p["RelativePath"]),
                                         CreationDate = Convert.ToString(p["CreationDate"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["CreationDate"]), TimeZoneSystemName)) : string.Empty,
                                         LastModifiedDate = Convert.ToString(p["LastModifiedDate"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["LastModifiedDate"]), TimeZoneSystemName)) : string.Empty,
                                        // MontageId = Convert.ToInt32(p["MontageId"]),
                                         Timepoint = Convert.ToInt32(p["Timepoint"]),
                                         FullPath = Convert.ToString(p["FullPath"]),
                                         Height = Convert.ToInt32(p["Height"]),
                                         Weight = Convert.ToInt32(p["Width"]),
                                         ImageTypeId = Convert.ToInt32(p["ImageType"]),
                                         ImageFormat = Convert.ToString(p["ImageFormat"]),
                                         Description = Convert.ToString(p["Description"]),
                                         Uploadby = Convert.ToString(p["Uploadby"]),
                                         ImageStatus = Convert.ToBoolean(p["ImageStatus"]),
                                         //ImagePosition = Convert.ToInt32(p["ImagePosition"]),
                                         ImageTypeName = Convert.ToString(p["ImageTypeName"]),
                                         //DateForApi = Convert.ToString(p["DateForApi"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["DateForApi"]), TimeZoneSystemName)) : string.Empty,
                                         DocumentName = string.Empty,
                                         DocumentId = 0,
                                     }).ToList();
            }
            return objpatientMessage;
        }

        public List<UplodedImages> PatientDocument(DataTable dt, string TimeZoneSystemName)
        {
            List<UplodedImages> objuplodedDocumentList = new List<UplodedImages>();
            if (dt != null && dt.Rows.Count > 0)
            {
                objuplodedDocumentList = (from p in dt.AsEnumerable()
                                          select new UplodedImages
                                          {
                                              ImageId = 0,
                                              PatientId = Convert.ToInt32(p["PatientId"]),
                                              CreationDate = Convert.ToString(p["CreationDate"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["CreationDate"]), TimeZoneSystemName)) : string.Empty,
                                              LastModifiedDate = Convert.ToString(p["LastModifiedDate"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["LastModifiedDate"]), TimeZoneSystemName)) : string.Empty,
                                              MontageId = Convert.ToInt32(p["MontageId"]),
                                              Description = Convert.ToString(p["Description"]),
                                              Uploadby = Convert.ToString(p["Uploadby"]),
                                              DateForApi = Convert.ToString(p["DateForApi"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["DateForApi"]), TimeZoneSystemName)) : string.Empty,
                                              DocumentName = Convert.ToString(p["DocumentName"]),
                                              DocumentId = Convert.ToInt32(p["DocumentId"])
                                          }).ToList();
            }
            return objuplodedDocumentList;
        }
        public List<EditFiles> GetImageFileDetailById(DataTable dtTable, string Type, string TimeZoneSystemName)
        {
            List<EditFiles> objpatientFiles = new List<EditFiles>();
            if (Type == "Image")
            {
                if (dtTable != null && dtTable.Rows.Count > 0)
                {
                    objpatientFiles = (from p in dtTable.AsEnumerable()
                                       select new EditFiles
                                       {
                                           ImageId = Convert.ToInt32(p["ImageId"]),
                                           PatientId = Convert.ToInt32(p["PatientId"]),
                                           Name = Convert.ToString(p["Name"]),
                                           CreationDate = Convert.ToString(p["CreationDate"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["CreationDate"]), TimeZoneSystemName)) : string.Empty,
                                           LastModifiedDate = Convert.ToString(p["LastModifiedDate"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["LastModifiedDate"]), TimeZoneSystemName)) : string.Empty,
                                          // MontageId = Convert.ToInt32(p["MontageId"]),
                                           Timepoint = Convert.ToInt32(p["Timepoint"]),
                                           Height = Convert.ToInt32(p["Height"]),
                                           Weight = Convert.ToInt32(p["Width"]),
                                           Description = Convert.ToString(p["Description"]),
                                           Uploadby = Convert.ToString(p["Uploadby"]),
                                           ImageStatus = Convert.ToBoolean(p["ImageStatus"]),
                                           FullPathToImage = Convert.ToString(p["FullPathToImage"]),
                                           RelativePathToImage = Convert.ToString(p["RelativePathToImage"]),
                                           LastModifiedBy = Convert.ToInt32(p["LastModifiedBy"]),
                                           ImageTypeId = Convert.ToInt32(p["ImageTypeId"]),
                                           OwnerId = Convert.ToInt32(p["OwnerId"]),
                                           ImageFormatTypeId = Convert.ToInt32(p["ImageFormatTypeId"]),
                                          // PositionInMontage = Convert.ToInt32(p["PositionInMontage"]),
                                           CreationDateForApi = Convert.ToString(p["CreationDateForApi"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["CreationDateForApi"]), TimeZoneSystemName)) : string.Empty,
                                           LastModifiedDateForApi = Convert.ToString(p["LastModifiedDateForApi"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["LastModifiedDateForApi"]), TimeZoneSystemName)) : string.Empty,
                                           CreationDateNew = Convert.ToString(p["CreationDateNew"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["CreationDateNew"]), TimeZoneSystemName)) : string.Empty,
                                           LastModifiedDateNew = Convert.ToString(p["LastModifiedDateNew"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["LastModifiedDateNew"]), TimeZoneSystemName)) : string.Empty,
                                           DocumentName = string.Empty,
                                           DocumentId = 0,
                                       }).ToList();
                }
                return objpatientFiles;
            }
            else
            {
                if (dtTable != null && dtTable.Rows.Count > 0)
                {
                    objpatientFiles = (from p in dtTable.AsEnumerable()
                                       select new EditFiles
                                       {
                                           MontageId = Convert.ToInt32(p["MontageId"]),
                                           PatientId = Convert.ToInt32(p["PatientId"]),
                                           DocumentName = Convert.ToString(p["DocumentName"]),
                                           CreationDate = Convert.ToString(p["CreationDate"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["CreationDate"]), TimeZoneSystemName)) : string.Empty,
                                           LastModifiedDate = Convert.ToString(p["LastModifiedDate"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["LastModifiedDate"]), TimeZoneSystemName)) : string.Empty,
                                           CreationDateNew = Convert.ToString(p["CreationDateNew"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["CreationDateNew"]), TimeZoneSystemName)) : string.Empty,
                                           LastModifiedDateNew = Convert.ToString(p["LastModifiedDateNew"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["LastModifiedDateNew"]), TimeZoneSystemName)) : string.Empty,
                                           Description = Convert.ToString(p["Description"]),
                                           DocumentId = Convert.ToInt32(p["DocumentId"]),
                                           CreationDateForApi = Convert.ToString(p["CreationDateForApi"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["CreationDateForApi"]), TimeZoneSystemName)) : string.Empty,
                                           LastModifiedDateForApi = Convert.ToString(p["LastModifiedDateForApi"]) != string.Empty ? Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(p["LastModifiedDateForApi"]), TimeZoneSystemName)) : string.Empty,
                                       }).ToList();
                }
                return objpatientFiles;
            }
        }
    }
}