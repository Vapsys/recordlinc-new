﻿using Hangfire.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hangfire.Annotations;

namespace NewRecordlinc.Models
{
    public class HangFireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize([NotNull] DashboardContext context)
        {
           if (HttpContext.Current.Session != null)
            {
                if (HttpContext.Current.Session["AdminUserId"] != null && HttpContext.Current.Session["AdminUserId"].ToString() != "")
                {
                    return true;
                }                
                else
                    return false;
            }        
            else
                return true;           

        }
    }
}