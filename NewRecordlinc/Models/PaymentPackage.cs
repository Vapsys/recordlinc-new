﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BO.Models;

namespace NewRecordlinc.Models
{
    public class PaymentPackage
    {
        public string memshipid { get; set; }
        public string memshipname { get; set; }
        public string memshipamt { get; set; }
        public List<Features> lstfeatures { get; set; }
    }
}