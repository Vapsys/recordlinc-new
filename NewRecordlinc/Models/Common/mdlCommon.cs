﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer.Common;
using DataAccessLayer.ColleaguesData;
using System.Data;
using NewRecordlinc.Models.Appointment;
using System.Globalization;
using NewRecordlinc.Common;
using System.ComponentModel.DataAnnotations;

namespace NewRecordlinc.Models.Common
{
    public class mdlCommon
    {


        public List<SelectListItem> Country()
        {
            DataTable dt = new DataTable();
            clsCommon objCommon = new clsCommon();
            dt = objCommon.GetAllCountry();
            List<SelectListItem> lst = new List<SelectListItem>();

            lst.Add(new SelectListItem { Text = "Select Country", Value = "0" });


            foreach (DataRow item in dt.Rows)
            {
                lst.Add(new SelectListItem { Text = item["CountryName"].ToString(), Value = item["CountryCode"].ToString() });
            }

            return lst;
        }


        public List<SelectListItem> StateByCountryCode(string CountryCode)
        {
            DataTable dt = new DataTable();
            clsCommon objCommon = new clsCommon();
            dt = objCommon.GetAllStateByCountryCode(CountryCode);
            List<SelectListItem> lst = new List<SelectListItem>();

            lst.Add(new SelectListItem { Text = "Select State", Value = "0" });


            foreach (DataRow item in dt.Rows)
            {
                lst.Add(new SelectListItem { Text = item["StateName"].ToString(), Value = item["StateCode"].ToString() });
            }

            return lst;
        }


        public List<SelectListItem> YearList()
        {
            List<SelectListItem> lst = new List<SelectListItem>();

            lst.Add(new SelectListItem { Text = "Select Year", Value = "0" });


            for (int i = DateTime.Now.Year; i > 1970; i--)
            {
                lst.Add(new SelectListItem { Text = i.ToString(), Value = i.ToString() });
            }

            return lst;
        }

        public List<SelectListItem> DoctorSpecialities()
        {
            DataTable dt = new DataTable();
            clsCommon objCommon = new clsCommon();
            dt = objCommon.GetAllSpeciality();
            List<SelectListItem> lst = new List<SelectListItem>();

            lst.Add(new SelectListItem { Text = "Select Speciality", Value = "0" });


            foreach (DataRow item in dt.Rows)
            {
                lst.Add(new SelectListItem { Text = item["Description"].ToString(), Value = item["SpecialityId"].ToString() });
            }

            return lst;
        }

        public List<SelectListItem> GetImageTypeById()
        {
            DataTable dt = new DataTable();
            clsCommon objCommon = new clsCommon();
            dt = objCommon.GetImageType();
            List<SelectListItem> lst = new List<SelectListItem>();




            foreach (DataRow item in dt.Rows)
            {
                lst.Add(new SelectListItem { Text = item["Description"].ToString(), Value = item["ImageTypeId"].ToString() });
            }

            return lst;



        }
        public List<SelectListItem> NewDoctorSpecialities(string selected = "")
        {
            DataTable dt = new DataTable();
            clsCommon objCommon = new clsCommon();
            dt = objCommon.GetAllSpeciality();
            List<SelectListItem> lst = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    string Id = Convert.ToString(item["SpecialityId"]);
                    if (selected.Split(',').Contains(Id))
                    {
                        lst.Add(new SelectListItem { Text = item["Description"].ToString(), Value = item["SpecialityId"].ToString(), Selected = true });
                    }
                    else
                    {
                        lst.Add(new SelectListItem { Text = item["Description"].ToString(), Value = item["SpecialityId"].ToString() });
                    }
                }
            }


            return lst;
        }

        public List<SelectListItem> CustomPageNames()
        {
            DataTable dt = new DataTable();
            clsAdmin ObjAdmin = new clsAdmin();
            dt = ObjAdmin.GetAllCustomPageDetails();
            List<SelectListItem> lst = new List<SelectListItem>();

            lst.Add(new SelectListItem { Text = "Select Page", Value = "0" });


            foreach (DataRow item in dt.Rows)
            {
                lst.Add(new SelectListItem { Text = item["PageName"].ToString(), Value = item["Id"].ToString() });
            }

            return lst;
        }
        public string GetCardType(string CardCode)
        {
            switch (CardCode)
            {
                case "A": return "American Express";
                case "C": return "Diners Club";
                case "D": return "Discover";
                case "J": return "JCB";
                case "M": return "Mastercard";
                case "V": return "Visa";
                default: return "";
            }


        }

        public string GetAmount(string PlaneId)
        {
            switch (PlaneId)
            {
                case "1": return "47";
                case "2": return "50";
                case "3": return "888";
                case "4": return "1188";
                case "5": return "147";
                case "6": return "150";
                case "7": return "350";
                case "8":return "173";
                case "9":return "150";
                case "10":return "350";
                default: return "0";
            }

        }
        public DateTime GetPackageEndDate(string PlaneId)
        {
            DateTime dttm = new DateTime();
            dttm = System.DateTime.Now;
            switch (PlaneId)
            {

                case "1": return dttm.AddMonths(1);
                case "2": return dttm.AddMonths(12);
                case "3": return dttm.AddMonths(24);
                case "4": return dttm.AddMonths(36);
                case "5": return dttm.AddMonths(1);
                case "6": return dttm.AddMonths(1);
                case "7": return dttm.AddMonths(1);
                case "8":return dttm.AddMonths(1);
                case "9":return dttm.AddMonths(1);
                case "10": return dttm.AddMonths(1);
                default: return dttm;
            }
        }
    }


    public class PyamentDetails
    {
        public string CardType { get; set; }       
        public string Cvv { get; set; }
        public string CardNumber { get; set; }
        public string expdate { get; set; }
        public string Amount { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool isDoctorInSystem { get; set; }
        public string Userid { get; set; }
        public string Description { get; set; }
        public string Planes { get; set; }
        public DateTime PackageStartDate { get; set; }
        public DateTime PackageEndDate { get; set; }
        public List<NewMembership> ListOfMembership = new List<NewMembership>();
        public string payInterval { get; set; }

    }

    public class DynamicGrid
    {
        public string TableScript { get; set; }
        public int Pageindex { get; set; }
        public int PageSize { get; set; }
        public string SortColumn { get; set; }
        public string SortBy { get; set; }
        public string Other { get; set; }
    }

    public class SerielizeObjForDoctorImportInAdmin
    {
        public string NewDoctor { get; set; }
        public string ExistsDoctor { get; set; }
    }
    public class ViewDataUploadFilesResult
    {
        public string Thumbnail_url { get; set; }
        public string Name { get; set; }
        public int Length { get; set; }
        public string Type { get; set; }
    }
    public class SerializeObjForInbox
    {
        public string MessageBody { get; set; }
        public string InboxList { get; set; }
    }
    public class SerializeObjForSentBox
    {
        public string MessageBody { get; set; }
        public string SentboxList { get; set; }
    }
    public class ResultType
    {
        public int MsgType { get; set; }
        public string Message { get; set; }
        
    }
    public class NewMembership
    {
        public int Membership_Id { get; set; }
        public string Title { get; set; }
        public decimal MonthlyPrice { get; set; }
        public decimal AnnualPrice { get; set; }
        public decimal QuaterlyPrice { get; set; }
        public decimal HalfYearlyPrice { get; set; }
    }
    public class NewMembership_Feathures
    {
        public int MembershipFeaturesId { get; set; }
        public int MembershipId { get; set; }
        public string MembershipName { get; set; }
        public string FeatureId { get; set; }
        public string FeatureName { get; set; }
        public bool cansee { get; set; }
        public string MaxCount { get; set; }
        public string CreatedOn { get; set; }
        public string ModifiedOn { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
    }
}