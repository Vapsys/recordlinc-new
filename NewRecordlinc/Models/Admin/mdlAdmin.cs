﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Web.Mvc;
using NewRecordlinc.Models.Patients;
using System.Text.RegularExpressions;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.PatientsData;
using DataAccessLayer.Common;
using System.ComponentModel.DataAnnotations;
using BO.Models;

namespace NewRecordlinc.Models.Admin
{

    public class mdlAdmin
    {
        clsAdmin ObjAdmin = new clsAdmin();
        clsCommon ObjCommon = new clsCommon();
        public List<EmailTemplate> lstEmailTemplate = new List<EmailTemplate>();
        public List<Insurance> lstInsurance = new List<Insurance>();
        public int TotalInsurance { get; set;}
        public int CurrentPageNo { get; set; }
        #region Insurance Detail

        public List<Insurance> GetAllInsuranceDetail(int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext)
        {
            lstInsurance = new List<Insurance>();
            try
            {
                DataTable dt = new DataTable();
                dt = ObjAdmin.GetAllInsuranceDetail(PageIndex,PageSize,SortColumn,SortDirection,Searchtext);
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        lstInsurance.Add(new Insurance
                        {
                            InsuranceId = Convert.ToInt32(dt.Rows[i]["InsuranceId"]),
                            Name = ObjCommon.CheckNull(Convert.ToString(dt.Rows[i]["Name"]), string.Empty),
                            LogoPath = ObjCommon.CheckNull(Convert.ToString(dt.Rows[i]["Logo"]), string.Empty),
                            Link = ObjCommon.CheckNull(Convert.ToString(dt.Rows[i]["Link"]), string.Empty),
                            Description = ObjCommon.CheckNull(Convert.ToString(dt.Rows[i]["Description"]), string.Empty),
                        });
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstInsurance;
        }

        public Insurance GetInsuranceDetailById(int InsuranceId)
        {
            Insurance insurance = new Insurance();
            try
            {
                DataTable dt = new DataTable();
                dt = ObjAdmin.GetInsuranceDetailById(InsuranceId);
                if (dt.Rows.Count > 0)
                {
                    insurance.InsuranceId = Convert.ToInt32(dt.Rows[0]["InsuranceId"]);
                    insurance.Name = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["Name"]), string.Empty);
                    insurance.LogoPath = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["Logo"]), string.Empty);
                    insurance.Description = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["Description"]), string.Empty);
                    insurance.Link = ObjCommon.CheckNull(Convert.ToString(dt.Rows[0]["Link"]), string.Empty);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return insurance;
        }
        #endregion

        #region Edit Email Template
        public List<EmailTemplate> GetEmailTemplateById(int Id)
        {
            lstEmailTemplate = new List<EmailTemplate>();
            try
            {
                DataTable dt = new DataTable();
                dt = ObjAdmin.GetEmailTemplateById(Id);
                if (dt != null && dt.Rows.Count > 0)
                {

                    foreach (DataRow row in dt.Rows)
                    {
                        lstEmailTemplate.Add(new EmailTemplate
                        {
                            TemplateId = Convert.ToInt32(row["TemplateId"]),
                            TemplateName = ObjCommon.CheckNull(Convert.ToString(row["TemplateName"]), string.Empty),
                            TemplateDesc = ObjCommon.CheckNull(Convert.ToString(row["TemplateDesc"]), string.Empty),
                            TemplateSubject = ObjCommon.CheckNull(Convert.ToString(row["TemplateSubject"]), string.Empty),
                        });
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstEmailTemplate;
        }

        #endregion
    }


    public class EmailTemplate
    {
        public int TemplateId { get; set; }
        public string TemplateName { get; set; }
        public string TemplateDesc { get; set; }
        public string TemplateSubject { get; set; }

    }


    public class mdlCompany
    {
        public int CompanyId { get; set; }
        public string CompanyName { get; set; }
        public string Connectionstring { get; set; }
        public string CompanyWebsite { get; set; }
        public string SalesEmail { get; set; }
        public string SupportEmail { get; set; }

        public string PhoneNo { get; set; }
        public string CompanyOwnerName { get; set; }
        public string CompanyOwnerTitle { get; set; }
        public string CompanyOwnerEmail { get; set; }
        public string CompanyFacebook { get; set; }

        public string CompanyBlog { get; set; }
        public string CompanyYoutube { get; set; }
        public string CompanyLinkedin { get; set; }
        public string CompanyTwitter { get; set; }
        public string CompanyGooglePlus { get; set; }

        public string CompanyFacebookLike { get; set; }
        public string CompanyAlexa { get; set; }
        public string LogoPath { get; set; }
        public string SMTPServer { get; set; }
        public string SMTPPort { get; set; }
        public string SMTPLogin { get; set; }

        public string SMTPPassword { get; set; }
        public bool SMTPSsl { get; set; }
        public string CompanyAddress1 { get; set; }
        public string CompanyAddress2 { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyState { get; set; }
        public string CompanyZipCode { get; set; }
        public string CompanyCountry { get; set; }
        public bool Isactive { get; set; }
        public string oldPath { get; set; }
    }

   //public class EmailVerification
   // {
   //     public int UserId { get; set; }
   //     public string FullName { get; set; }     
   //     public string Email { get; set; }     
   //     public string Emailtype { get; set; }     
   //     public int STATUS { get; set; }
   //     public int ProcessDate { get; set; }
   //     public string Phone { get; set; }
   // }

   //public class EmailFilter
   // {
   //     public int PageIndex { get; set; }
   //     public int PageSize { get; set; }
   //     public string Searchtext { get; set; }
   //     public int SortColumn { get; set; }
   //     public int SortDirection { get; set; }
   // } 
         
}
