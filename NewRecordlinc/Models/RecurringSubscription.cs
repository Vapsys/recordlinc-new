﻿using ArbApiSample;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using NewRecordlinc.Models.Colleagues;
using NewRecordlinc.Models.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;
using BO.Models;

namespace NewRecordlinc.Models
{
    public class RecurringSubscription
    {
        static string _apiUrl = ConfigurationManager.AppSettings["recurring_apiurl"];
        static string _userLoginName = ConfigurationManager.AppSettings["x_login"];
        static string _transactionKey = ConfigurationManager.AppSettings["x_tran_key"];
        static string _subscriptionId;
        static bool _dumpXml;
        clsTemplate ObjTemplate = new clsTemplate();

        public string CreateSubscription(PyamentDetails objPyamentDetails)
        {
            ARBCreateSubscriptionRequest createSubscriptionRequest = new ARBCreateSubscriptionRequest();
            PopulateSubscription(createSubscriptionRequest, objPyamentDetails);
            // createSubscriptionRequest.merchantAuthentication = null;
            object response = null;
            XmlDocument xmldoc = null;
            bool bResult = true;
           string resp = PostRequest(createSubscriptionRequest, out xmldoc);

            //if (bResult) bResult = ProcessXmlResponse(xmldoc, out response);

            //if (bResult) ProcessResponse(response);

            return resp;

        }
        private static void PopulateSubscription(ARBCreateSubscriptionRequest request, PyamentDetails objPyamentDetails)
        {
            clsColleaguesData ObjColleaguesData = new clsColleaguesData();
            ARBSubscriptionType sub = new ARBSubscriptionType();
            creditCardType creditCard = new creditCardType();
            DataTable dt = ObjColleaguesData.GetDoctorDetailsByEmail(objPyamentDetails.Email);
            if (dt.Rows.Count > 0)
            {
                objPyamentDetails.Firstname = Convert.ToString(dt.Rows[0]["FirstName"]);
                objPyamentDetails.Lastname = Convert.ToString(dt.Rows[0]["LastName"]);
                objPyamentDetails.Address = Convert.ToString(dt.Rows[0]["ExactAddress"]);
                objPyamentDetails.City = Convert.ToString(dt.Rows[0]["City"]);
                objPyamentDetails.State = Convert.ToString(dt.Rows[0]["State"]);
                objPyamentDetails.Zip = Convert.ToString(dt.Rows[0]["ZipCode"]);
                objPyamentDetails.Phone = Convert.ToString(dt.Rows[0]["Phone"]);
            }
            sub.name = "Recordlinc Subscription";

            creditCard.cardNumber = objPyamentDetails.CardNumber;
            creditCard.expirationDate = objPyamentDetails.expdate;  // required format for API is YYYY-MM
            sub.payment = new paymentType();
            sub.payment.Item = creditCard;

            sub.billTo = new nameAndAddressType();
            sub.billTo.firstName = objPyamentDetails.Firstname;
            sub.billTo.lastName = objPyamentDetails.Lastname;
            sub.billTo.address = objPyamentDetails.Address;
            sub.billTo.city = objPyamentDetails.City;
            sub.billTo.state = objPyamentDetails.State;
            sub.billTo.zip = objPyamentDetails.Zip;

            // Create a subscription that is 12 monthly payments starting on Jan 1, 2019
            // with 1 month free (trial period)

            sub.paymentSchedule = new paymentScheduleType();
            DateTime date = System.DateTime.Now;
            sub.paymentSchedule.startDate = date.AddMonths(1);// dt.AddMonths(1);
            sub.paymentSchedule.startDateSpecified = true;
            sub.paymentSchedule.totalOccurrences = 12;
            sub.paymentSchedule.totalOccurrencesSpecified = true;

            //  free 1 month trial
            //sub.paymentSchedule.trialOccurrences = 1;
            //sub.paymentSchedule.trialOccurrencesSpecified = true;
            //sub.trialAmount = 0.00M;
            //sub.trialAmountSpecified = true;

            sub.amount = 47.00M;
            sub.amountSpecified = true;

            sub.paymentSchedule.interval = new paymentScheduleTypeInterval();
            sub.paymentSchedule.interval.length = 1;
            sub.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.months;

            //sub.order = new orderType();
            //sub.order.invoiceNumber = "invoice111";

            sub.customer = new customerType();
            sub.customer.email = objPyamentDetails.Email;
            sub.customer.phoneNumber = objPyamentDetails.Phone;


            // Include authentication information
            PopulateMerchantAuthentication((ANetApiRequest)request);

            request.subscription = sub;
        }
        private static string PostRequest(object apiRequest, out XmlDocument xmldoc)
        {
           // bool bResult = false;
            XmlSerializer serializer;

            xmldoc = null;
            String post_response= null;
            try            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(_apiUrl);
                webRequest.Method = "POST";
                webRequest.ContentType = "text/xml";
                webRequest.KeepAlive = true;

                // Serialize the request
                serializer = new XmlSerializer(apiRequest.GetType());
                XmlWriter writer = new XmlTextWriter(webRequest.GetRequestStream(), Encoding.UTF8);
                serializer.Serialize(writer, apiRequest);
                writer.Close();



                // Get the response
                
                WebResponse webResponse = webRequest.GetResponse();
                using (StreamReader responseStream = new StreamReader(webResponse.GetResponseStream()))
                {
                    post_response = responseStream.ReadToEnd();
                    responseStream.Close();
                }

                //bResult = true;
            }
            catch (Exception ex)
            {
                post_response = null;
                //bResult = false;
            }

            return post_response;
        }
        //private static bool ProcessXmlResponse(XmlDocument xmldoc, out object apiResponse)
        //{
        //    bool bResult = true;
        //    XmlSerializer serializer;

        //    apiResponse = null;

        //    try
        //    {
        //        // Use the root node to determine the type of response object to create
        //        switch (xmldoc.DocumentElement.Name)
        //        {
        //            case "ARBCreateSubscriptionResponse":
        //                serializer = new XmlSerializer(typeof(ARBCreateSubscriptionResponse));
        //                apiResponse = (ARBCreateSubscriptionResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));
        //                break;

        //            case "ARBUpdateSubscriptionResponse":
        //                serializer = new XmlSerializer(typeof(ARBUpdateSubscriptionResponse));
        //                apiResponse = (ARBUpdateSubscriptionResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));
        //                break;

        //            case "ARBCancelSubscriptionResponse":
        //                serializer = new XmlSerializer(typeof(ARBCancelSubscriptionResponse));
        //                apiResponse = (ARBCancelSubscriptionResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));
        //                break;

        //            case "ARBGetSubscriptionStatusResponse":
        //                serializer = new XmlSerializer(typeof(ARBGetSubscriptionStatusResponse));
        //                apiResponse = (ARBGetSubscriptionStatusResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));
        //                break;

        //            case "ErrorResponse":
        //                serializer = new XmlSerializer(typeof(ANetApiResponse));
        //                apiResponse = (ANetApiResponse)serializer.Deserialize(new StringReader(xmldoc.DocumentElement.OuterXml));
        //                break;

        //            default:
        //                Console.WriteLine("Unexpected type of object: " + xmldoc.DocumentElement.Name);
        //                bResult = false;
        //                break;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        bResult = false;
        //        apiResponse = null;
        //        Console.WriteLine(ex.GetType().ToString() + ": " + ex.Message);
        //    }

        //    return bResult;
        //}
        //private static void ProcessResponse(object response)
        //{
        //    // Every response is based on ANetApiResponse so you can always do this sort of type casting.
        //    ANetApiResponse baseResponse = (ANetApiResponse)response;

        //    // Write the results to the console window
        //    Console.Write("Result: ");
        //    Console.WriteLine(baseResponse.messages.resultCode.ToString());

        //    // If the result code is "Ok" then the request was successfully processed.
        //    if (baseResponse.messages.resultCode == messageTypeEnum.Ok)
        //    {
        //        // CreateSubscription is the only method that returns additional data
        //        if (response.GetType() == typeof(ARBCreateSubscriptionResponse))
        //        {
        //            ARBCreateSubscriptionResponse createResponse = (ARBCreateSubscriptionResponse)response;
        //            _subscriptionId = createResponse.subscriptionId;

        //            Console.WriteLine("Subscription ID: " + _subscriptionId);
        //        }
        //    }
        //    else
        //    {
        //        // Write error messages to console window
        //        for (int i = 0; i < baseResponse.messages.message.Length; i++)
        //        {
        //            Console.WriteLine("[" + baseResponse.messages.message[i].code
        //                    + "] " + baseResponse.messages.message[i].text);
        //        }
        //    }
        //}
        private static void PopulateMerchantAuthentication(ANetApiRequest request)
        {
            request.merchantAuthentication = new merchantAuthenticationType();
            request.merchantAuthentication.name = _userLoginName;
            request.merchantAuthentication.transactionKey = _transactionKey;
            request.refId = ConfigurationManager.AppSettings["refid"];
        }        

        public string createSubscriptionBySystem(PyamentDetails objPyamentDetails)
        {
            ARBCreateSubscriptionRequest createSubscriptionRequest = new ARBCreateSubscriptionRequest();
            PopulateSubscriptionBySystem(createSubscriptionRequest, objPyamentDetails);                   
            XmlDocument xmldoc = null;            

            string resp = PostRequestBySystem(createSubscriptionRequest, out xmldoc);           
            return resp;

        }

        //Cancel subscription
        public string cancelSubscriptionBySystem(int subId)
        {
            ARBCancelSubscriptionRequest cancelARBCancelSubscriptionRequest = new ARBCancelSubscriptionRequest();
            cancelARBCancelSubscriptionRequest.subscriptionId = Convert.ToString(subId);
            PopulateMerchantAuthenticationBySystem(cancelARBCancelSubscriptionRequest);
            XmlDocument xmldoc = null;
            string resp = PostRequestBySystem(cancelARBCancelSubscriptionRequest, out xmldoc);
            return resp;
        }

        //update subscription
        public string UpdateSubscriptionBySystem(PyamentDetails objPaymentDetails, string subId)
        {
            ARBUpdateSubscriptionRequest updateARBUpdateSubscriptionRequest = new ARBUpdateSubscriptionRequest();
            PopulateMerchantAuthentication(updateARBUpdateSubscriptionRequest);            
            updateARBUpdateSubscriptionRequest.subscriptionId = Convert.ToString(subId);
            PopulateUpdateSubscriptionBySystem(updateARBUpdateSubscriptionRequest, objPaymentDetails);
            XmlDocument xmldoc = null;
            PostRequestBySystem(updateARBUpdateSubscriptionRequest, out xmldoc);                       
            string resp = PostRequestBySystem(updateARBUpdateSubscriptionRequest, out xmldoc);
            return resp;
        }

        public string PostRequestBySystem(object apiRequest, out XmlDocument xmldoc)
        {
            // bool bResult = false;
            XmlSerializer serializer;

            xmldoc = null;
            String post_response = null;
            try
            {                
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(_apiUrl);
                webRequest.Method = "POST";
                webRequest.ContentType = "text/xml";
                webRequest.KeepAlive = false;
                webRequest.ProtocolVersion = HttpVersion.Version10;
                webRequest.ServicePoint.ConnectionLimit = 1;
                System.Net.ServicePointManager.Expect100Continue = false;
                SecurityProtocolType origSecurityProtocol = System.Net.ServicePointManager.SecurityProtocol;
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                // Serialize the request
                serializer = new XmlSerializer(apiRequest.GetType());
                XmlWriter writer = new XmlTextWriter(webRequest.GetRequestStream(), Encoding.UTF8);
                serializer.Serialize(writer, apiRequest);
                writer.Close();



                // Get the response                
                //ARBGetSubscriptionResponse getsub = new ARBGetSubscriptionResponse();
                WebResponse webResponse = webRequest.GetResponse();
                using (StreamReader responseStream = new StreamReader(webResponse.GetResponseStream()))
                {
                    post_response = responseStream.ReadToEnd();
                    responseStream.Close();
                }

                //bResult = true;
            }
            catch (Exception ex)
            {
                post_response = null;
                //bResult = false;
            }

            return post_response;
        }

        //Create Subscription
        private static void PopulateSubscriptionBySystem(ARBCreateSubscriptionRequest request, PyamentDetails objPyamentDetails)
        {
            try
            {

                clsColleaguesData ObjColleaguesData = new clsColleaguesData();
                ARBSubscriptionType sub = new ARBSubscriptionType();
                creditCardType creditCard = new creditCardType();
                NewRecordlinc.Models.Colleagues.Member MdlMember = new NewRecordlinc.Models.Colleagues.Member();
                DataSet ds = MdlMember.GetProfileDetailsOfDoctorByID(Convert.ToInt32(objPyamentDetails.Userid));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    objPyamentDetails.Firstname = Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]);
                    objPyamentDetails.Lastname = Convert.ToString(ds.Tables[0].Rows[0]["LastName"]);
                    objPyamentDetails.Userid = Convert.ToString(ds.Tables[0].Rows[0]["UserId"]);
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    objPyamentDetails.Address = Convert.ToString(ds.Tables[2].Rows[0]["ExactAddress"]);
                    objPyamentDetails.City = Convert.ToString(ds.Tables[2].Rows[0]["City"]);
                    objPyamentDetails.State = Convert.ToString(ds.Tables[2].Rows[0]["State"]);
                    objPyamentDetails.Zip = Convert.ToString(ds.Tables[2].Rows[0]["ZipCode"]);
                    objPyamentDetails.Phone = Convert.ToString(ds.Tables[2].Rows[0]["Phone"]);
                }
                sub.name = "Recordlinc Subscription";

                creditCard.cardNumber = objPyamentDetails.CardNumber;
                creditCard.expirationDate = objPyamentDetails.expdate;  // required format for API is YYYY-MM
                sub.payment = new paymentType();
                sub.payment.Item = creditCard;

                sub.billTo = new nameAndAddressType();
                sub.billTo.firstName = objPyamentDetails.Firstname;
                sub.billTo.lastName = objPyamentDetails.Lastname;
                sub.billTo.address = objPyamentDetails.Address;
                sub.billTo.city = objPyamentDetails.City;
                sub.billTo.state = objPyamentDetails.State;
                sub.billTo.zip = objPyamentDetails.Zip;

                mdlCommon objmdlCommon = new mdlCommon();
                string firstAmount = objmdlCommon.GetAmount(objPyamentDetails.Planes);
                decimal totalAmount = 0.0M;
                if (objPyamentDetails.payInterval == "1")
                {
                    totalAmount = Convert.ToDecimal(firstAmount + ".00");
                }
                if (objPyamentDetails.payInterval == "3")
                {
                    var amt = Convert.ToString(Convert.ToInt32(firstAmount) * 3);
                    totalAmount = Convert.ToDecimal(amt + ".00");
                }
                if (objPyamentDetails.payInterval == "6")
                {
                    var amt = Convert.ToString(Convert.ToInt32(firstAmount) * 6);
                    totalAmount = Convert.ToDecimal(amt + ".00");
                }
                if (objPyamentDetails.payInterval == "12")
                {
                    var amt = Convert.ToString(Convert.ToInt32(firstAmount) * 12);
                    totalAmount = Convert.ToDecimal(amt + ".00");
                }
                sub.amount = Convert.ToDecimal(objPyamentDetails.Amount); //Convert.ToDecimal(objPyamentDetails.Amount); //47.00M;
                sub.amountSpecified = true;

                // Create a subscription that is 12 monthly payments starting on Jan 1, 2019
                // with 1 month free (trial period)

                sub.paymentSchedule = new paymentScheduleType();
                DateTime date = System.DateTime.Now;
                sub.paymentSchedule.startDate = date.AddMonths(0);
                sub.paymentSchedule.startDateSpecified = true;
                if (objPyamentDetails.payInterval == "12")
                {
                    sub.paymentSchedule.totalOccurrences = 3;
                }
                if (objPyamentDetails.payInterval == "6")
                {
                    sub.paymentSchedule.totalOccurrences = 6;
                }
                if (objPyamentDetails.payInterval == "3")
                {
                    sub.paymentSchedule.totalOccurrences = 12;
                }
                if (objPyamentDetails.payInterval == "1")
                {
                    sub.paymentSchedule.totalOccurrences = 36;
                }
                sub.paymentSchedule.totalOccurrencesSpecified = true;

                sub.paymentSchedule.interval = new paymentScheduleTypeInterval();
                if (objPyamentDetails.payInterval == "1")
                {
                    sub.paymentSchedule.interval.length = 1;
                    sub.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.months;
                }
                if (objPyamentDetails.payInterval == "3")
                {
                    sub.paymentSchedule.interval.length = 3;
                    sub.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.months;
                }
                if (objPyamentDetails.payInterval == "6")
                {
                    sub.paymentSchedule.interval.length = 6;
                    sub.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.months;
                }
                if (objPyamentDetails.payInterval == "12")
                {
                    sub.paymentSchedule.interval.length = 12;
                    sub.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.months;
                }

                //sub.order = new orderType();
                //sub.order.invoiceNumber = "invoice111";

                sub.customer = new customerType();
                sub.customer.email = objPyamentDetails.Email;
                sub.customer.phoneNumber = objPyamentDetails.Phone;


                // Include authentication information
                PopulateMerchantAuthenticationBySystem((ANetApiRequest)request);

                request.subscription = sub;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        //Update Subscription
        private static void PopulateUpdateSubscriptionBySystem(ARBUpdateSubscriptionRequest request, PyamentDetails objPyamentDetails)
        {
            try
            {

                clsColleaguesData ObjColleaguesData = new clsColleaguesData();
                ARBSubscriptionType sub = new ARBSubscriptionType();
                creditCardType creditCard = new creditCardType();

                DataTable dt = ObjColleaguesData.GetDoctorDetailsByEmail(objPyamentDetails.Email);
                if (dt.Rows.Count > 0)
                {
                    objPyamentDetails.Firstname = Convert.ToString(dt.Rows[0]["FirstName"]);
                    objPyamentDetails.Lastname = Convert.ToString(dt.Rows[0]["LastName"]);
                    objPyamentDetails.Address = Convert.ToString(dt.Rows[0]["ExactAddress"]);
                    objPyamentDetails.City = Convert.ToString(dt.Rows[0]["City"]);
                    objPyamentDetails.State = Convert.ToString(dt.Rows[0]["State"]);
                    objPyamentDetails.Zip = Convert.ToString(dt.Rows[0]["ZipCode"]);
                    objPyamentDetails.Phone = Convert.ToString(dt.Rows[0]["Phone"]);
                }
                sub.name = "Recordlinc Subscription";

                creditCard.cardNumber = objPyamentDetails.CardNumber;
                creditCard.expirationDate = objPyamentDetails.expdate;  // required format for API is YYYY-MM
                sub.payment = new paymentType();
                sub.payment.Item = creditCard;

                sub.billTo = new nameAndAddressType();
                sub.billTo.firstName = objPyamentDetails.Firstname;
                sub.billTo.lastName = objPyamentDetails.Lastname;
                sub.billTo.address = objPyamentDetails.Address;
                sub.billTo.city = objPyamentDetails.City;
                sub.billTo.state = objPyamentDetails.State;
                sub.billTo.zip = objPyamentDetails.Zip;

                mdlCommon objmdlCommon = new mdlCommon();
                string firstAmount = objmdlCommon.GetAmount(objPyamentDetails.Planes);
                decimal totalAmount = 0.0M;
                if (objPyamentDetails.payInterval == "1")
                {
                    totalAmount = Convert.ToDecimal(firstAmount + ".00");
                }
                if (objPyamentDetails.payInterval == "3")
                {
                    var amt = Convert.ToString(Convert.ToInt32(firstAmount) * 3);
                    totalAmount = Convert.ToDecimal(amt + ".00");
                }
                if (objPyamentDetails.payInterval == "6")
                {
                    var amt = Convert.ToString(Convert.ToInt32(firstAmount) * 6);
                    totalAmount = Convert.ToDecimal(amt + ".00");
                }
                if (objPyamentDetails.payInterval == "12")
                {
                    var amt = Convert.ToString(Convert.ToInt32(firstAmount) * 12);
                    totalAmount = Convert.ToDecimal(amt + ".00");
                }
                sub.amount = totalAmount; //47.00M;
                sub.amountSpecified = true;

                // Create a subscription that is 12 monthly payments starting on Jan 1, 2019
                // with 1 month free (trial period)

                sub.paymentSchedule = new paymentScheduleType();
                DateTime date = System.DateTime.Now;
                sub.paymentSchedule.startDate = date.AddMonths(0);
                sub.paymentSchedule.startDateSpecified = true;
                if (objPyamentDetails.payInterval == "12")
                {
                    sub.paymentSchedule.totalOccurrences = 3;
                }
                if (objPyamentDetails.payInterval == "6")
                {
                    sub.paymentSchedule.totalOccurrences = 6;
                }
                if (objPyamentDetails.payInterval == "3")
                {
                    sub.paymentSchedule.totalOccurrences = 12;
                }
                if (objPyamentDetails.payInterval == "1")
                {
                    sub.paymentSchedule.totalOccurrences = 36;
                }
                sub.paymentSchedule.totalOccurrencesSpecified = true;

                sub.paymentSchedule.interval = new paymentScheduleTypeInterval();
                if (objPyamentDetails.payInterval == "1")
                {
                    sub.paymentSchedule.interval.length = 1;
                    sub.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.months;
                }
                if (objPyamentDetails.payInterval == "3")
                {
                    sub.paymentSchedule.interval.length = 3;
                    sub.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.months;
                }
                if (objPyamentDetails.payInterval == "6")
                {
                    sub.paymentSchedule.interval.length = 6;
                    sub.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.months;
                }
                if (objPyamentDetails.payInterval == "12")
                {
                    sub.paymentSchedule.interval.length = 12;
                    sub.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.months;
                }

                //sub.order = new orderType();
                //sub.order.invoiceNumber = "invoice111";

                sub.customer = new customerType();
                sub.customer.email = objPyamentDetails.Email;
                sub.customer.phoneNumber = objPyamentDetails.Phone;


                // Include authentication information
                PopulateMerchantAuthenticationBySystem((ANetApiRequest)request);

                request.subscription = sub;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        private static void PopulateMerchantAuthenticationBySystem(ANetApiRequest request)
        {
            request.merchantAuthentication = new merchantAuthenticationType();
            request.merchantAuthentication.name = _userLoginName;
            request.merchantAuthentication.transactionKey = _transactionKey;
            request.refId = ConfigurationManager.AppSettings["refid"];
        }

        //Cancel current subscription
        public string MakePayment(PyamentDetails objPyamentDetails, bool IsPlanPrice, string requestUrl)
        {
            try
            {

                mdlCommon objCommoncls = new mdlCommon();
                clsColleaguesData ObjColleaguesData = new clsColleaguesData();
                // By default, this sample code is designed to post to our test server for
                // developer accounts: https://test.authorize.net/gateway/transact.dll
                // for real accounts (even in test mode), please make sure that you are
                // posting to: https://secure.authorize.net/gateway/transact.dll
                String post_url = ConfigurationManager.AppSettings["post_url"];
                string amount = string.Empty;
                string strRecurring = string.Empty;
                string strPaymentstatus = string.Empty;
                string strTransactionId = string.Empty;
                string strReturn = string.Empty;
                Dictionary<string, string> post_values = new Dictionary<string, string>();

                post_values.Add("x_login", ConfigurationManager.AppSettings["x_login"]);
                post_values.Add("x_tran_key", ConfigurationManager.AppSettings["x_tran_key"]);
                post_values.Add("x_delim_data", ConfigurationManager.AppSettings["x_delim_data"]);
                post_values.Add("x_delim_char", ConfigurationManager.AppSettings["x_delim_char"]);
                post_values.Add("x_relay_response", ConfigurationManager.AppSettings["x_relay_response"]);
                post_values.Add("x_response_format", ConfigurationManager.AppSettings["x_response_format"]);
                post_values.Add("x_version", ConfigurationManager.AppSettings["x_version"]);
                post_values.Add("x_type", ConfigurationManager.AppSettings["x_type"]);
                post_values.Add("x_method", ConfigurationManager.AppSettings["x_method"]);


                post_values.Add("x_card_type", objPyamentDetails.CardType);
                post_values.Add("x_card_num", objPyamentDetails.CardNumber);
                post_values.Add("x_exp_date", objPyamentDetails.expdate);
                Member objMember = new Member();
                TripleDESCryptoHelper objTripleDESCryptoHelper = new TripleDESCryptoHelper();
                objPyamentDetails.ListOfMembership = objMember.GetMembershipById(Convert.ToInt32(objPyamentDetails.Planes));

                if (IsPlanPrice)
                {
                    amount = objPyamentDetails.Amount;
                }
                else
                {
                    string firstAmount = objCommoncls.GetAmount(objPyamentDetails.Planes);
                    foreach (var item in objPyamentDetails.ListOfMembership)
                    {
                        string ActualPrice = string.Empty;
                        if (objPyamentDetails.payInterval == "1")
                        {
                            ActualPrice = Convert.ToString(item.MonthlyPrice);
                            if (!string.IsNullOrEmpty(ActualPrice))
                            {
                                amount = ActualPrice;
                                objPyamentDetails.Amount = ActualPrice;
                            }
                            else
                            {
                                amount = firstAmount;
                                objPyamentDetails.Amount = amount;
                            }
                        }
                        if (objPyamentDetails.payInterval == "3")
                        {
                            ActualPrice = Convert.ToString(item.QuaterlyPrice);
                            if (!string.IsNullOrEmpty(ActualPrice))
                            {
                                amount = ActualPrice;
                                objPyamentDetails.Amount = ActualPrice;
                            }
                            else
                            {
                                var amt = Convert.ToString(Convert.ToInt32(firstAmount) * 3);
                                amount = amt;
                                objPyamentDetails.Amount = amount;
                            }
                        }
                        if (objPyamentDetails.payInterval == "6")
                        {
                            ActualPrice = Convert.ToString(item.HalfYearlyPrice);
                            if (!string.IsNullOrEmpty(ActualPrice))
                            {
                                amount = ActualPrice;
                                objPyamentDetails.Amount = ActualPrice;
                            }
                            else
                            {
                                var amt = Convert.ToString(Convert.ToInt32(firstAmount) * 6);
                                amount = amt;
                                objPyamentDetails.Amount = amount;
                            }
                        }
                        if (objPyamentDetails.payInterval == "12")
                        {
                            ActualPrice = Convert.ToString(item.AnnualPrice);
                            if (!string.IsNullOrEmpty(ActualPrice))
                            {
                                amount = ActualPrice;
                                objPyamentDetails.Amount = ActualPrice;
                            }
                            else
                            {
                                var amt = Convert.ToString(Convert.ToInt32(firstAmount) * 12);
                                amount = amt;
                                objPyamentDetails.Amount = amount;
                            }
                        }
                    }
                }
                

                post_values.Add("x_amount", amount);
                post_values.Add("x_description", objPyamentDetails.Description);
                post_values.Add("x_first_name", objPyamentDetails.Firstname);
                post_values.Add("x_last_name", objPyamentDetails.Lastname);
                post_values.Add("x_address", objPyamentDetails.Lastname);
                post_values.Add("x_state", objPyamentDetails.State);
                post_values.Add("x_zip", objPyamentDetails.Zip);

                String post_string = "";

                foreach (KeyValuePair<string, string> post_value in post_values)
                {
                    post_string += post_value.Key + "=" + HttpUtility.UrlEncode(post_value.Value) + "&";
                }
                post_string = post_string.TrimEnd('&');



                // create an HttpWebRequest object to communicate with Authorize.net
                HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(post_url);
                objRequest.Method = "POST";
                objRequest.ContentLength = post_string.Length;
                objRequest.ContentType = "application/x-www-form-urlencoded";
                objRequest.KeepAlive = false;
                objRequest.ProtocolVersion = HttpVersion.Version10;
                objRequest.ServicePoint.ConnectionLimit = 1;
                System.Net.ServicePointManager.Expect100Continue = false;
                SecurityProtocolType origSecurityProtocol = System.Net.ServicePointManager.SecurityProtocol;
                System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;

                // post data is sent as a stream
                StreamWriter myWriter = null;
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(post_string);
                myWriter.Close();

                // returned values are returned as a stream, then read into a string
                String post_response;
                HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
                {
                    post_response = responseStream.ReadToEnd();
                    responseStream.Close();
                }

                // the response string is broken into an array
                // The split character specified here must match the delimiting character specified above
                string[] response_array = post_response.Split('|');
                if (response_array != null && response_array[0] == "1")
                {
                    mdlCommon objCommon = new mdlCommon();
                    string transactionId = Convert.ToString(response_array[6]);
                    //objPyamentDetails.Userid = objTripleDESCryptoHelper.decryptText(objPyamentDetails.Userid);
                    string resp = CallSubscription(objPyamentDetails, transactionId);

                    if (!string.IsNullOrEmpty(resp) && resp == "Done")
                    {
                        strRecurring = "Recurring is done successfully.";                       
                    }
                    objPyamentDetails.PackageStartDate = System.DateTime.Now;
                    objPyamentDetails.PackageEndDate = objCommoncls.GetPackageEndDate(objPyamentDetails.Planes);

                    ObjColleaguesData.InsertTransactionDetails(Convert.ToInt32(objPyamentDetails.Userid), objPyamentDetails.PackageStartDate, objPyamentDetails.PackageEndDate, Convert.ToDecimal(amount), true, objPyamentDetails.CardType, objPyamentDetails.CardNumber, objPyamentDetails.expdate, response_array[6], objPyamentDetails.Cvv, true);
                    ObjColleaguesData.UpdatePaymentStatusOfExistingUser(Convert.ToInt32(objPyamentDetails.Userid), true);
                    string CardNumberWithMask = objPyamentDetails.CardNumber;

                    CardNumberWithMask = CardNumberWithMask.Substring(CardNumberWithMask.Length - 4).PadLeft(CardNumberWithMask.Length, '*');

                    string CardType = objCommon.GetCardType(objPyamentDetails.CardType);
                    ObjTemplate.AccountUpgradedSuccessfully(Convert.ToInt32(objPyamentDetails.Userid), CardNumberWithMask, CardType, response_array[6], objPyamentDetails.Amount);
                    strPaymentstatus = response_array[3];                    
                    if (response_array[0] == "1")
                    {
                        strTransactionId = response_array[6];
                        strReturn = "Done";
                    }
                }
                else
                {
                    ObjTemplate.PaymentFailed(objPyamentDetails.Email, objPyamentDetails.Firstname, objPyamentDetails.Lastname, objPyamentDetails.Phone, response_array[3], requestUrl);
                    strPaymentstatus = response_array[3];
                }
                //objPyamentDetails.Userid = objTripleDESCryptoHelper.encryptText(objPyamentDetails.Userid);
                return strReturn;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        #region Account Upgrade For Recurring Payment
        public string CallSubscription(PyamentDetails objPyamentDetails, string transactionId)
        {
            RecurringSubscription objRecurringSubscription = new RecurringSubscription();
            clsColleaguesData objcls = new clsColleaguesData();
            TripleDESCryptoHelper objtriple = new TripleDESCryptoHelper();
            mdlCommon objCommon = new mdlCommon();
            int memberId = Convert.ToInt32(objPyamentDetails.Userid);
            DataTable dt = objcls.getNewPaymentHistoryOnUserId(memberId);
            int subId = 0;
            bool hasSubscription = false;
            string createResponse = string.Empty;
            string updateResponse = string.Empty;
            string cancelResponse = string.Empty;
            int membershipId = 0;
            string strReturn = string.Empty;
            if (dt != null && dt.Rows.Count > 0)
            {
                membershipId = Convert.ToInt32(dt.Rows[0]["membershipId"]);
                subId = Convert.ToInt32(dt.Rows[0]["subscriptionId"]);
                hasSubscription = true;
            }

            if (hasSubscription)
            {
                //if (Convert.ToInt32(objPyamentDetails.Planes) > membershipId || Convert.ToInt32(objPyamentDetails.Planes) < membershipId)
                //{
                cancelResponse = cancelSubscriptionBySystem(Convert.ToInt32(subId));
                bool isCancelUpdate = false;
                if (!string.IsNullOrEmpty(cancelResponse))
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(cancelResponse);

                    XmlNodeList grandelemList = xmlDoc.GetElementsByTagName("messages");
                    if (grandelemList.Count > 0)
                    {
                        string code = grandelemList[0]["resultCode"].InnerText;
                        if (code == "Ok")
                        {
                            clsColleaguesData objCCData = new clsColleaguesData();
                            var isUpdate = objCCData.UpdateSubscriptionStatus(memberId, Convert.ToString(subId));
                            isCancelUpdate = true;
                        }
                    }
                }
                if (isCancelUpdate)
                {
                    createResponse = createSubscriptionBySystem(objPyamentDetails);
                }
                //}
                //else
                //{
                //    updateResponse = objRecurringSubscription.UpdateSubscriptionBySystem(objPyamentDetails, Convert.ToString(subId));
                //    if (!string.IsNullOrEmpty(updateResponse))
                //    {
                //        XmlDocument xmlDoc = new XmlDocument();
                //        xmlDoc.LoadXml(updateResponse);

                //        XmlNodeList grandelemList = xmlDoc.GetElementsByTagName("messages");
                //        XmlNodeList elemList = xmlDoc.GetElementsByTagName("message");
                //        XmlNodeList getSubscriptionName = xmlDoc.GetElementsByTagName("subscriptionId");
                //        XmlNodeList getCustomerProfile = xmlDoc.GetElementsByTagName("profile");
                //        if (grandelemList.Count > 0)
                //        {

                //            string code = grandelemList[0]["resultCode"].InnerText;
                //            if (code != "Ok")
                //            {
                //                ObjTemplate.RecurringPaymentFailed(objPyamentDetails.Email, objPyamentDetails.Firstname, objPyamentDetails.Lastname, objPyamentDetails.Phone, elemList[0]["text"].InnerText);
                //            }
                //            else
                //            {
                //                if (getSubscriptionName.Count > 0 && getCustomerProfile.Count > 0)
                //                {
                //                    string customerId = getCustomerProfile[0]["customerProfileId"].InnerText;
                //                    string subscriptionId = getSubscriptionName[0].InnerText;
                //                    clsColleaguesData objCCData = new clsColleaguesData();
                //                    string amount = objPyamentDetails.Amount.Replace('$', ' ');
                //                    string suscriptionType = objPyamentDetails.payInterval;
                //                    var isInsert = objCCData.NewInsertUsertransactionDetails(memberId, amount, transactionId, Convert.ToString(membershipId), customerId, DateTime.Now, suscriptionType, subscriptionId);
                //                    strReturn = "Done";
                //                }
                //            }
                //        }
                //    }
                //}
            }
            else
            {
                createResponse = objRecurringSubscription.createSubscriptionBySystem(objPyamentDetails);
            }
            if (!string.IsNullOrEmpty(createResponse))
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(createResponse);

                XmlNodeList grandelemList = xmlDoc.GetElementsByTagName("messages");
                XmlNodeList elemList = xmlDoc.GetElementsByTagName("message");
                XmlNodeList getSubscriptionName = xmlDoc.GetElementsByTagName("subscriptionId");
                XmlNodeList getCustomerProfile = xmlDoc.GetElementsByTagName("profile");
                if (grandelemList.Count > 0)
                {
                    string code = grandelemList[0]["resultCode"].InnerText;
                    if (code != "Ok")
                    {
                        ObjTemplate.RecurringPaymentFailed(objPyamentDetails.Email, objPyamentDetails.Firstname, objPyamentDetails.Lastname, objPyamentDetails.Phone, elemList[0]["text"].InnerText);
                    }
                    else
                    {
                        if (getSubscriptionName.Count > 0 && getCustomerProfile.Count > 0)
                        {
                            string customerId = getCustomerProfile[0]["customerProfileId"].InnerText;
                            string subscriptionId = getSubscriptionName[0].InnerText;
                            clsColleaguesData objCCData = new clsColleaguesData();
                            string amount = objPyamentDetails.Amount;
                            string suscriptionType = objPyamentDetails.payInterval;
                            var isInsert = objCCData.NewInsertUsertransactionDetails(memberId, amount, transactionId, objPyamentDetails.Planes, customerId, DateTime.Now, suscriptionType, subscriptionId);
                            objCCData.UpdateMembershipByRecurring(objPyamentDetails.Userid, objPyamentDetails.Planes);
                            strReturn = "Done";
                        }
                    }
                }
            }
            return strReturn;
        }
#endregion
    }
}