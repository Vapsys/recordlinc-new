﻿using BO.Models;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace NewRecordlinc.Models.Colleagues
{


    public class DentistProfileDetails
    {
        public int UserId { get; set; }
        public string EncryptUserId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string ImageName { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string WebsiteURL { get; set; }
        public string OfficeName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public int LocationId { get; set; }

        public List<WebsiteDetails> lstsecondarywebsitelist = new List<WebsiteDetails>();
        public List<SocialMediaForDoctor> lstGetSocialMediaDetailByUserId = new List<SocialMediaForDoctor>();
        public List<EducationandTraining> lstEducationandTraining = new List<EducationandTraining>();
        public List<ProfessionalMemberships> lstProfessionalMemberships = new List<ProfessionalMemberships>();
        public List<TeamMemberDetailsForDoctor> lstTeamMemberDetailsForDoctor = new List<TeamMemberDetailsForDoctor>();
        public List<SpeacilitiesOfDoctor> lstSpeacilitiesOfDoctor = new List<SpeacilitiesOfDoctor>();
        public List<AddressDetails> lstDoctorAddressDetails = new List<AddressDetails>();
        public List<Banner> lstBannerDetails = new List<Banner>();
        public List<string> lstaddressstr = new List<string>();

    }
    //public class PatientLogin
    //{
    //    [Required(ErrorMessage = "Please enter email address.")]
    //    [Display(Name = "Email")]
    //    [EmailAddress(ErrorMessage = "Invalid Email Address")]
    //    public string Email { get; set; }

    //    [Required(ErrorMessage = "Please enter password.")]
    //    [DataType(DataType.Password)]
    //    [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
    //    [Display(Name = "Password")]
    //    public string Password { get; set; }
    //}
   
    public partial class DentistProfileMethod
    {
        clsCommon objCommon = new clsCommon();
        clsColleaguesData objColleaguesData = new clsColleaguesData();
        string DoctorImage = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("DoctorImage"));
        string DefaultDoctorImage = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("DefaultDoctorImage"));

        public DentistProfileDetails DoctorPublicProfile(DataSet ds)
        {
            DentistProfileDetails objProfileDetails = new DentistProfileDetails();
            TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
            try
            {
                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        objProfileDetails.UserId = Convert.ToInt32(ds.Tables[0].Rows[0]["UserId"]);
                        objProfileDetails.EncryptUserId = ObjTripleDESCryptoHelper.encryptText(Convert.ToString(ds.Tables[0].Rows[0]["UserId"]));
                        objProfileDetails.FirstName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["FirstName"]), string.Empty);
                        objProfileDetails.MiddleName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["MiddleName"]), string.Empty);
                        objProfileDetails.LastName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["LastName"]), string.Empty);
                        string ImageName = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["ImageName"]), string.Empty);
                        if (string.IsNullOrEmpty(ImageName))
                        {
                            objProfileDetails.ImageName = DefaultDoctorImage;
                        }
                        else
                        {
                            objProfileDetails.ImageName = DoctorImage + ImageName;
                        }
                        objProfileDetails.Title = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Title"]), string.Empty);
                        objProfileDetails.Description = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["Description"]), string.Empty);
                        objProfileDetails.WebsiteURL = objCommon.CheckNull(Convert.ToString(ds.Tables[0].Rows[0]["WebsiteURL"]), string.Empty);
                        objProfileDetails.LocationId = Convert.ToInt32(ds.Tables[0].Rows[0]["LocationId"]);
                    }

                    if (ds.Tables[1].Rows.Count > 0)
                    {
                        objProfileDetails.OfficeName = objCommon.CheckNull(Convert.ToString(ds.Tables[1].Rows[0]["AccountName"]), string.Empty);
                    }


                    objProfileDetails.lstDoctorAddressDetails = GetDoctorAddressDetailsById(objProfileDetails.UserId);
                    if (objProfileDetails.lstDoctorAddressDetails.Count > 0)
                    {
                        objProfileDetails.City = objProfileDetails.lstDoctorAddressDetails[0].City;
                        objProfileDetails.State = objProfileDetails.lstDoctorAddressDetails[0].State;
                        objProfileDetails.ZipCode = objProfileDetails.lstDoctorAddressDetails[0].ZipCode;
                    }
                    objProfileDetails.lstsecondarywebsitelist = GetSecondaryWebsitelistByUserId(objProfileDetails.UserId);
                    objProfileDetails.lstGetSocialMediaDetailByUserId = GetSocialMediaDetailByUserId(objProfileDetails.UserId);
                    objProfileDetails.lstEducationandTraining = GetEducationandTrainingDetailsForDoctor(objProfileDetails.UserId);
                    objProfileDetails.lstProfessionalMemberships = GetProfessionalMembershipsByUserId(objProfileDetails.UserId);
                    objProfileDetails.lstTeamMemberDetailsForDoctor = GetTeamMemberDetailsOfDoctor(objProfileDetails.UserId, 1, 15);
                    objProfileDetails.lstSpeacilitiesOfDoctor = GetSpeacilitiesOfDoctorById(objProfileDetails.UserId);
                    objProfileDetails.lstBannerDetails = GetBannersByUserId(objProfileDetails.UserId);
                    objProfileDetails.Phone = objCommon.ConvertPhoneNumber(objProfileDetails.lstDoctorAddressDetails.Where(p => (int)p.AddressInfoID == (int)objProfileDetails.LocationId).FirstOrDefault().Phone);
                    
                }
            }
            catch (Exception ex)
            {


            }
            finally
            {

            }

            return objProfileDetails;

        }
        #region Code For WebSite Details
        public List<WebsiteDetails> GetSecondaryWebsitelistByUserId(int UserId)
        {
            List<WebsiteDetails> lstsecondarywebsitelist = new List<WebsiteDetails>();
            try
            {

                clsCommon objCommon = new clsCommon();
                DataTable dt = new DataTable();
                dt = objColleaguesData.GetSecondaryWebsiteDetailsbyId(UserId);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstsecondarywebsitelist.Add(new WebsiteDetails()
                        {
                            SecondaryWebsiteId = Convert.ToInt32(row["SecondaryId"]),
                            SecondaryWebsiteurl = objCommon.CheckNull(Convert.ToString(row["WebSite"]), string.Empty),
                        });
                    }
                }
                return lstsecondarywebsitelist;

            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion
        #region Code For Social Media Details
        public List<SocialMediaForDoctor> GetSocialMediaDetailByUserId(int UserId)
        {
            List<SocialMediaForDoctor> lstGetSocialMediaDetailByUserId = new List<SocialMediaForDoctor>();
            try
            {
                #region Start New
                DataTable dt = new DataTable();

                dt = objColleaguesData.GetSocialMediaDetailByUserId(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {

                    foreach (DataRow row in dt.Rows)
                    {
                        lstGetSocialMediaDetailByUserId.Add(new SocialMediaForDoctor()
                        {
                            FacebookUrl = objCommon.CheckNull(Convert.ToString(row["FacebookUrl"]), string.Empty) != string.Empty ? (Convert.ToString(row["FacebookUrl"]).Contains("http://") || Convert.ToString(row["FacebookUrl"]).Contains("https://") ? Convert.ToString(row["FacebookUrl"]) : "http://" + Convert.ToString(row["FacebookUrl"])) : Convert.ToString(row["FacebookUrl"]),
                            LinkedinUrl = objCommon.CheckNull(Convert.ToString(row["LinkedinUrl"]), string.Empty) != string.Empty ? (Convert.ToString(row["LinkedinUrl"]).Contains("http://") || Convert.ToString(row["LinkedinUrl"]).Contains("https://") ? Convert.ToString(row["LinkedinUrl"]) : "http://" + Convert.ToString(row["LinkedinUrl"])) : Convert.ToString(row["LinkedinUrl"]),
                            TwitterUrl = objCommon.CheckNull(Convert.ToString(row["TwitterUrl"]), string.Empty) != string.Empty ? (Convert.ToString(row["TwitterUrl"]).Contains("http://") || Convert.ToString(row["TwitterUrl"]).Contains("https://") ? Convert.ToString(row["TwitterUrl"]) : "http://" + Convert.ToString(row["TwitterUrl"])) : Convert.ToString(row["TwitterUrl"]),
                            GoogleplusUrl = objCommon.CheckNull(Convert.ToString(row["GoogleplusUrl"]), string.Empty) != string.Empty ? (Convert.ToString(row["GoogleplusUrl"]).Contains("http://") || Convert.ToString(row["GoogleplusUrl"]).Contains("https://") ? Convert.ToString(row["GoogleplusUrl"]) : "http://" + Convert.ToString(row["GoogleplusUrl"])) : Convert.ToString(row["GoogleplusUrl"]),
                            YoutubeUrl = objCommon.CheckNull(Convert.ToString(row["YoutubeUrl"]), string.Empty) != string.Empty ? (Convert.ToString(row["YoutubeUrl"]).Contains("http://") || Convert.ToString(row["YoutubeUrl"]).Contains("https://") ? Convert.ToString(row["YoutubeUrl"]) : "http://" + Convert.ToString(row["YoutubeUrl"])) : Convert.ToString(row["YoutubeUrl"]),
                            PinterestUrl = objCommon.CheckNull(Convert.ToString(row["PinterestUrl"]), string.Empty) != string.Empty ? (Convert.ToString(row["PinterestUrl"]).Contains("http://") || Convert.ToString(row["PinterestUrl"]).Contains("https://") ? Convert.ToString(row["PinterestUrl"]) : "http://" + Convert.ToString(row["PinterestUrl"])) : Convert.ToString(row["PinterestUrl"]),
                            BlogUrl = objCommon.CheckNull(Convert.ToString(row["BlogUrl"]), string.Empty) != string.Empty ? (Convert.ToString(row["BlogUrl"]).Contains("http://") || Convert.ToString(row["BlogUrl"]).Contains("https://") ? Convert.ToString(row["BlogUrl"]) : "http://" + Convert.ToString(row["BlogUrl"])) : Convert.ToString(row["BlogUrl"]),
                            YelpUrl = objCommon.CheckNull(Convert.ToString(row["YelpUrl"]), string.Empty) != string.Empty ? (Convert.ToString(row["YelpUrl"]).Contains("http://") || Convert.ToString(row["YelpUrl"]).Contains("https://") ? Convert.ToString(row["YelpUrl"]) : "http://" + Convert.ToString(row["YelpUrl"])) : Convert.ToString(row["YelpUrl"]),

                        });
                    }
                }
                #endregion
            }
            catch (Exception)
            {

                throw;
            }

            return lstGetSocialMediaDetailByUserId;
        }
        #endregion
        #region Code For EducationandTraining Details
        public List<EducationandTraining> GetEducationandTrainingDetailsForDoctor(int UserId)
        {
            List<EducationandTraining> lstEducationandTraining = new List<EducationandTraining>();
            try
            {
                DataTable dt = new DataTable();
                dt = objColleaguesData.GetEducationTrainingByUserId(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[i]["Institute"])))
                        {
                            lstEducationandTraining.Add(new EducationandTraining { Id = Convert.ToInt32(dt.Rows[i]["Id"]), Institute = objCommon.CheckNull(dt.Rows[i]["Institute"].ToString(), string.Empty), Specialisation = objCommon.CheckNull(dt.Rows[i]["Specialisation"].ToString(), string.Empty), YearAttended = objCommon.CheckNull(dt.Rows[i]["YearAttended"].ToString(), string.Empty) });
                        }
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
            return lstEducationandTraining;
        }
        #endregion
        #region Code For SpeacilitiesOfDoctor Details
        public List<SpeacilitiesOfDoctor> GetSpeacilitiesOfDoctorById(int UserId)
        {
            DataTable dtSpeacilities = new DataTable();
            dtSpeacilities = objColleaguesData.GetMemberSpecialities(UserId);
            List<SpeacilitiesOfDoctor> lstSpeacilities = new List<SpeacilitiesOfDoctor>();
            if (dtSpeacilities.Rows.Count > 0)
            {
                lstSpeacilities = (from p in dtSpeacilities.AsEnumerable()
                                   select new SpeacilitiesOfDoctor
                                   {
                                       SpecialtyId = int.Parse(objCommon.CheckNull(p["SpecialityId"].ToString(), "0")),
                                       SpecialtyDescription = objCommon.CheckNull(p["Specialities"].ToString(), ""),
                                   }).ToList();


            }

            return lstSpeacilities;

        }
        #endregion
        #region Code For TeamMemberDetailsOfDoctor Details
        public List<TeamMemberDetailsForDoctor> GetTeamMemberDetailsOfDoctor(int ParentUserId, int PageIndex, int PageSize)
        {
            List<TeamMemberDetailsForDoctor> lstTeamMemberDetailsForDoctor = new List<TeamMemberDetailsForDoctor>();
            try
            {
                int UserId = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                DataTable dt = new DataTable();
                dt = objColleaguesData.GetTeamMemberDetailsOfDoctor(ParentUserId, PageIndex, PageSize);
                if (dt != null && dt.Rows.Count > 0)
                {
                    lstTeamMemberDetailsForDoctor = (from p in dt.AsEnumerable()
                                                     select new TeamMemberDetailsForDoctor
                                                     {
                                                         UserId = Convert.ToInt32(p["UserId"]),
                                                         FirstName = objCommon.CheckNull(Convert.ToString(p["FirstName"]), string.Empty),
                                                         LastName = objCommon.CheckNull(Convert.ToString(p["LastName"]), string.Empty),
                                                         SpecialtyDescription = objCommon.CheckNull(Convert.ToString(p["Speciality"]), string.Empty),
                                                         PublicProfileUrl = objCommon.CheckNull(Convert.ToString(p["PublicProfileUrl"]), string.Empty),
                                                         AccountName = objCommon.CheckNull(Convert.ToString(p["AccountName"]), string.Empty),
                                                     }).Where(x => x.UserId != UserId).ToList();
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstTeamMemberDetailsForDoctor;
        }
        #endregion
        #region Code For ProfessionalMemberships Details
        public List<ProfessionalMemberships> GetProfessionalMembershipsByUserId(int UserId)
        {
            List<ProfessionalMemberships> lstProfessionalMemberships = new List<ProfessionalMemberships>();
            try
            {
                DataTable dt = new DataTable();

                dt = objColleaguesData.GetProfessionalMembershipsByUserId(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[i]["Membership"])))
                        {
                            lstProfessionalMemberships.Add(new ProfessionalMemberships { Id = Convert.ToInt32(dt.Rows[i]["Id"]), Membership = objCommon.CheckNull(Convert.ToString(dt.Rows[i]["Membership"]), string.Empty), });
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }


            return lstProfessionalMemberships;
        }
        #endregion
        #region Code For DoctorAddressDetails Details
        public List<AddressDetails> GetDoctorAddressDetailsById(int UserId)
        {
            List<AddressDetails> lstDoctorAddressDetails = new List<AddressDetails>();
            try
            {

                objColleaguesData = new clsColleaguesData();
                DataSet ds = new DataSet();
                ds = objColleaguesData.GetDoctorDetailsById(UserId);
                if (ds != null && ds.Tables.Count > 0)
                {
                    //For Address details of Doctor

                    lstDoctorAddressDetails = (from p in ds.Tables[2].AsEnumerable()
                                               select new AddressDetails
                                               {
                                                   AddressInfoID = Convert.ToInt32(p["AddressInfoID"]),
                                                   Location = objCommon.CheckNull(Convert.ToString(p["LocationName"]), string.Empty),
                                                   ExactAddress = objCommon.CheckNull(Convert.ToString(p["ExactAddress"]), string.Empty),
                                                   Address2 = objCommon.CheckNull(Convert.ToString(p["Address2"]), string.Empty),
                                                   City = objCommon.CheckNull(Convert.ToString(p["City"]), string.Empty),
                                                   State = objCommon.CheckNull(Convert.ToString(p["State"]), string.Empty),
                                                   Country = objCommon.CheckNull(Convert.ToString(p["Country"]), string.Empty),
                                                   ZipCode = objCommon.CheckNull(Convert.ToString(p["ZipCode"]), string.Empty),
                                                   Phone = objCommon.ConvertPhoneNumber(objCommon.CheckNull(Convert.ToString(p["Phone"]), string.Empty)),
                                                   Fax = objCommon.CheckNull(Convert.ToString(p["Fax"]), string.Empty),
                                                   EmailAddress = objCommon.CheckNull(Convert.ToString(p["EmailAddress"]), string.Empty),
                                                   ContactType = Convert.ToInt32(p["ContactType"]),
                                               }).ToList();

                }
            }
            catch (Exception)
            {

                throw;
            }

            return lstDoctorAddressDetails;
        }
        #endregion
        #region Code For Social Media Details
        public List<Banner> GetBannersByUserId(int UserId)
        {
            List<Banner> lstBanner = new List<Banner>();
            try
            {
                #region Start New
                DataTable dt = new DataTable();

                dt = objColleaguesData.GetBannerDetailByUserId(UserId);
                if (dt.Rows.Count > 0)
                {
                    lstBanner = (from p in dt.AsEnumerable()
                                 select new Banner
                                 {
                                     BannerId = Convert.ToInt32(p["BannerId"]),
                                     UserId = Convert.ToInt32((p["UserId"])),
                                     Title = objCommon.CheckNull(Convert.ToString(p["Title"]), string.Empty),
                                     Path = objCommon.CheckNull(Convert.ToString(p["Path"]), string.Empty) == string.Empty ? objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultBannerImage"]), string.Empty) : objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["BannerImage"]), string.Empty) + UserId + "/" + Convert.ToString(p["Path"]),
                                     ColorCode = objCommon.CheckNull(Convert.ToString(p["ColorCode"]), string.Empty)

                                 }).ToList();

                }

                #endregion
            }
            catch (Exception)
            {

                throw;
            }
            return lstBanner;
        }
        #endregion
       
}
}