﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccessLayer.ColleaguesData;
using System.Data;
using DataAccessLayer.Common;
using System.Configuration;
using System.Web.Mvc;
using NewRecordlinc.Models.Patients;
using System.Text.RegularExpressions;
using System.Text;
using System.Xml;
using HtmlAgilityPack;
using BusinessLogicLayer;
using DataAccessLayer.AppointmentData;
using DataAccessLayer.Appointment;

namespace NewRecordlinc.Models.Colleagues
{
    public class mdlColleagues
    {
        clsColleaguesData objColleaguesData = new clsColleaguesData();
        clsCommon objCommon = new clsCommon();
        clsTemplate ObjTemplate = new clsTemplate();
        public int ColleagueId { get; set; }
        public string SearchInbox { get; set; }
        public List<ColleaguesDetails> lstColleaguesDetails = null;

        public List<ColleaguesDetails> lstColleaguesDetailsfordashboard = new List<ColleaguesDetails>();
        public List<MessageAttachment> lstImages = new List<MessageAttachment>();
        public List<SearchColleagueForDoctor> lstSearchColleagueForDoctor = null;
        public List<PatientDetailsOfDoctor> lstPatientDetailsOfDoctor = new List<PatientDetailsOfDoctor>();

        public List<MessageListOfDoctor> lstInboxOfDoctor = new List<MessageListOfDoctor>();
        public List<MessageListOfDoctor> lstSentboxOfDoctor = new List<MessageListOfDoctor>();
        public List<ColleaguesMessagesOfDoctor> lstColleaguesMessagesDoctor = new List<ColleaguesMessagesOfDoctor>();
        public List<ReferralDetails> lstRefferalDetails { get; set; }
        public List<MessageDetails> lstMessageDetails = new List<MessageDetails>();
        public List<ReferredPatient> lstReferredPatient = new List<ReferredPatient>();
        public List<NewReferralView> NewReferralView = new List<NewReferralView>();
        List<DoctorLocation> doclst = new List<DoctorLocation>();
        public string[] ReferralRegarding = { "Orthodontics", "Periodontist", "Oral Surgery", "Prosthodontics", "Radiology", "General Dentistry", "Endodontics", "Lab Work" };
        public string[] ReferralRequest = { "Extractions", "Pathology", "Caries", "Check Periodontal", "Oral Surgery", "Orthodontic Consultation", "Orthognathic Surgery", "Implants" };

        public bool AddAsColleague(int UserID, int ColleagueId)
        {
            objColleaguesData = new clsColleaguesData();
            return objColleaguesData.AddAsColleague(UserID, ColleagueId);
        }
        public List<ColleaguesDetails> ColleaguesDetailsForDoctor(int UserId, int StartIndex, int EndSize, string Searchtext, int SortColumn, int SortDirection, string NewSearchText, int FilterBy)
        {
            objCommon = new clsCommon();

            List<ColleaguesDetails> lst = new List<ColleaguesDetails>();
            objColleaguesData = new clsColleaguesData();
            DataSet ds = new DataSet();
            ds = objColleaguesData.GetColleaguesDetailsForDoctorWithSpeacilities(UserId, StartIndex, EndSize, Searchtext, SortColumn, SortDirection, NewSearchText, FilterBy);

            foreach (DataRow item in ds.Tables[0].Rows)
            {
                string ImageName = "";
                ColleaguesDetails ObjColleaguesDetails = new ColleaguesDetails();


                ObjColleaguesDetails.ColleagueId = Convert.ToInt32(item["ColleagueId"]);
                ObjColleaguesDetails.FirstName = objCommon.CheckNull(Convert.ToString(item["FirstName"]), string.Empty);
                ObjColleaguesDetails.LastName = objCommon.CheckNull(Convert.ToString(item["LastName"]), string.Empty);
                ObjColleaguesDetails.MiddleName = objCommon.CheckNull(Convert.ToString(item["MiddleName"]), string.Empty);
                ImageName = objCommon.CheckNull(Convert.ToString(item["ImageName"]), string.Empty);
                if (string.IsNullOrEmpty(ImageName))
                {
                    ObjColleaguesDetails.ImageName = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty);
                }
                else
                {
                    ObjColleaguesDetails.ImageName = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + ImageName;
                }

                ObjColleaguesDetails.ExactAddress = objCommon.CheckNull(Convert.ToString(item["ExactAddress"]), string.Empty);
                ObjColleaguesDetails.SecondaryEmail = objCommon.CheckNull(Convert.ToString(item["SecondaryEmail"]), string.Empty);

                ObjColleaguesDetails.Address2 = objCommon.CheckNull(Convert.ToString(item["Address2"]), string.Empty);
                ObjColleaguesDetails.City = objCommon.CheckNull(Convert.ToString(item["City"]), string.Empty);
                ObjColleaguesDetails.State = objCommon.CheckNull(Convert.ToString(item["State"]), string.Empty);
                ObjColleaguesDetails.Country = objCommon.CheckNull(Convert.ToString(item["Country"]), string.Empty);
                ObjColleaguesDetails.ZipCode = objCommon.CheckNull(Convert.ToString(item["ZipCode"]), string.Empty);
                ObjColleaguesDetails.EmailAddress = objCommon.CheckNull(Convert.ToString(item["EmailAddress"]), string.Empty);
                ObjColleaguesDetails.Phone = objCommon.CheckNull(Convert.ToString(item["Phone"]), string.Empty);
                ObjColleaguesDetails.Fax = objCommon.CheckNull(Convert.ToString(item["fax"]), string.Empty);
                ObjColleaguesDetails.Officename = objCommon.CheckNull(Convert.ToString(item["OfficeName"]), string.Empty);
                ObjColleaguesDetails.TotalRecord = Convert.ToInt32(item["TotalRecord"]);
                //  ObjColleaguesDetails.ColleageReferralCount = Convert.ToInt32(item["ColleageReferralCount"]);
                ObjColleaguesDetails.Location = objCommon.CheckNull(Convert.ToString(item["Location"]), string.Empty);
                ObjColleaguesDetails.LocationId = Convert.ToInt32(item["LocationId"]);


                if (ds.Tables.Count > 1)
                {

                    DataTable dtSpeacilities = new DataTable();
                    var rows = ds.Tables[1].AsEnumerable()
                        .Where(x => ((int)x["Userid"]) == Convert.ToInt32(item["ColleagueId"].ToString()));

                    if (rows.Any())
                        dtSpeacilities = rows.CopyToDataTable();


                    // Datatable 
                    //dtSpeacilities = objColleaguesData.GetMemberSpecialities(int.Parse(objCommon.CheckNull(item["ColleagueId"].ToString(), "0")));
                    List<SpeacilitiesOfDoctor> lstSpeacilities = new List<SpeacilitiesOfDoctor>();
                    foreach (DataRow Specialities in dtSpeacilities.Rows)
                    {
                        SpeacilitiesOfDoctor clsSpe = new SpeacilitiesOfDoctor();
                        clsSpe.SpecialtyId = int.Parse(objCommon.CheckNull(Specialities["Id"].ToString(), "0"));
                        clsSpe.SpecialtyDescription = objCommon.CheckNull(Specialities["Specialities"].ToString(), "");
                        lstSpeacilities.Add(clsSpe);

                    }

                    ObjColleaguesDetails.lstSpeacilitiesOfDoctor = lstSpeacilities;
                }

                lst.Add(ObjColleaguesDetails);
            }


            return lst;

        }

        #region Code For Social Media details For Doctor's Colleagues
        public List<SocialMediaForColleagues> lstGetSocialMediaDetailsForColleagues = new List<SocialMediaForColleagues>();
        public DateTime startBeforCallSP;
        public DateTime AfterCallSP;
        public List<SocialMediaForColleagues> GetSocialMediaDetailsForColleagues(int id, int index, int size, string Searchtext, int SortColumn, int SortDirection)
        {
            try
            {
                #region Start New
                DataTable dt = new DataTable();
                lstGetSocialMediaDetailsForColleagues = new List<SocialMediaForColleagues>();
                startBeforCallSP = DateTime.Now;
                dt = objColleaguesData.GetSocialMediaDetailsForColleagues(id, index, size, Searchtext, SortColumn, SortDirection);
                AfterCallSP = DateTime.Now;
                if (dt != null && dt.Rows.Count > 0)
                {

                    foreach (DataRow row in dt.Rows)
                    {
                        lstGetSocialMediaDetailsForColleagues.Add(new SocialMediaForColleagues()
                        {
                            UserId = Convert.ToInt32(row["userid"]),
                            FirstName = objCommon.CheckNull(Convert.ToString(row["FirstName"]), string.Empty),
                            LastName = objCommon.CheckNull(Convert.ToString(row["LastName"]), string.Empty),
                            FacebookUrl = objCommon.CheckNull(Convert.ToString(row["FacebookUrl"]), string.Empty),
                            LinkedinUrl = objCommon.CheckNull(Convert.ToString(row["LinkedinUrl"]), string.Empty),
                            TwitterUrl = objCommon.CheckNull(Convert.ToString(row["TwitterUrl"]), string.Empty),
                            GoogleplusUrl = objCommon.CheckNull(Convert.ToString(row["GoogleplusUrl"]), string.Empty),
                            YoutubeUrl = objCommon.CheckNull(Convert.ToString(row["YoutubeUrl"]), string.Empty),
                            PinterestUrl = objCommon.CheckNull(Convert.ToString(row["BlogUrl"]), string.Empty),
                            BlogUrl = objCommon.CheckNull(Convert.ToString(row["PinterestUrl"]), string.Empty),
                            YelpUrl = objCommon.CheckNull(Convert.ToString(row["YelpUrl"]), string.Empty),
                            OtherUrl = objCommon.CheckNull(Convert.ToString(row["OtherUrl"]), string.Empty),
                            TotalRecord = Convert.ToInt32(row["TotalRecord"]),
                            cloudscore = Convert.ToInt32(row["cloudscore"]),

                        });
                    }
                }
                #endregion
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("mdlColleagues---GetSocialMediaDetailsForColleagues--startBeforCallSP :"+ startBeforCallSP+" EndAfterCallSP :"+ AfterCallSP, Ex.Message, Ex.StackTrace);
                throw;
            }

            return lstGetSocialMediaDetailsForColleagues;
        }
        #endregion

        #region  Code For Colleague Remove
        public bool RemoveColleague(int UserId, int ColleagueId)
        {
            bool Result = false;
            Result = objColleaguesData.RemoveColleagues(UserId, ColleagueId);
            return Result;

        }
        #endregion

        #region Get Colleague Message by MessageId
        public ColleaguesMessagesOfDoctor ColleagueMessage(int MessageId, string TimeZoneSystemName)
        {
            ColleaguesMessagesOfDoctor CMD = new ColleaguesMessagesOfDoctor();
            clsAppointmentData objclsAppointmentData = new clsAppointmentData();
            clsColleaguesData ObjColleaguesData = new clsColleaguesData();
            DataTable dt = new DataTable();
            string Message = string.Empty;
            DataSet dsMessage = ObjColleaguesData.GetColleagueMessageFormDoctorbyMessageId(MessageId);
            if (dsMessage.Tables.Count > 0)
                dt = dsMessage.Tables[0];
            if (dt.Rows.Count > 0)
            {
                string ReceverIds = string.Empty;
                CMD.lstReceiverDetails = new List<ReceiversDetails>();
                for (int i = 0; i < dsMessage.Tables[0].Rows.Count; i++)
                {
                    ReceverIds += Convert.ToString(objCommon.CheckNull(dsMessage.Tables[0].Rows[i]["reciveruserid"].ToString(), "0"))+ ',';
                }
                ReceverIds = ReceverIds.TrimEnd(',');
                for (int i = 0; i < dsMessage.Tables[0].Rows.Count; i++)
                {
                    CMD.lstReceiverDetails.Add(new ReceiversDetails()
                    {
                        ReceiverEmail = objCommon.CheckNull(dsMessage.Tables[0].Rows[i]["ReciverEmail"].ToString(), string.Empty),
                        ReceiverId = Convert.ToString(objCommon.CheckNull(ReceverIds, "0")),
                        ReceiverName = objCommon.CheckNull(dsMessage.Tables[0].Rows[i]["ReciverName"].ToString(), string.Empty)
                    });
                }
                string NewMessage = Uri.UnescapeDataString(Convert.ToString(dt.Rows[0]["Messages"]));
                CMD.CreationDate = dt.Rows[0]["CreationDate"].ToString();
                //-- convert date as per timezone
                if(!string.IsNullOrEmpty(CMD.CreationDate))
                {
                    DateTime dtCreationDate = Convert.ToDateTime(CMD.CreationDate);
                    dtCreationDate = clsHelper.ConvertFromUTC(dtCreationDate, TimeZoneSystemName);
                    CMD.CreationDate = dtCreationDate.ToString();
                }
                CMD.Message = NewMessage;
                CMD.SenderName = dt.Rows[0]["SenderName"].ToString();
                CMD.ReciverName = dt.Rows[0]["ReciverName"].ToString();
                CMD.SenderEmail = dt.Rows[0]["SenderEmail"].ToString();
                CMD.ReciverEmail = dt.Rows[0]["ReciverEmail"].ToString();
                CMD.SenderId = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["senderuserid"])) ? 0 : dt.Rows[0]["senderuserid"]);
                CMD.ReciverId = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["reciveruserid"])) ? 0 : dt.Rows[0]["reciveruserid"]);
                List<AttachedPatientDetails> lst = new List<AttachedPatientDetails>();
                if (!string.IsNullOrEmpty(dt.Rows[0]["AttachedPatientIds"].ToString()))
                {
                    CMD.AttachedPatientsId = Convert.ToString(dt.Rows[0]["AttachedPatientIds"]);
                    string[] attch = CMD.AttachedPatientsId.Split(',');
                    foreach (var item in attch)
                    {
                        
                        DataTable dtpatient = new DataTable();
                        dtpatient = objclsAppointmentData.GetPateintDetailsByPatientId(Convert.ToInt64(item),null);
                        if(dtpatient.Rows.Count > 0)
                        {
                            foreach (DataRow pat in dtpatient.Rows)
                            {
                                lst.Add(new AttachedPatientDetails()
                                {
                                    AttachedPatientId = Convert.ToInt32(pat["PatientId"]),
                                    AttachedPatientFirstName = Convert.ToString(pat["FirstName"]),
                                    AttachedPatientLastName = Convert.ToString(pat["LastName"])
                                });
                            }
                        }
                    }
                }
                CMD.LstPatient = lst;
                CMD.lstImages = new List<MessageAttachment>();
                //var message = CMD.Message.Split('\n');
                //CMD.Message = "";                
                //foreach (var item in message)
                //{                    
                //    if(!string.IsNullOrEmpty(item)||!string.IsNullOrWhiteSpace(item))
                //        CMD.Message += "<li>" + item + "</li>";
                //}
            }

            if (dsMessage.Tables.Count > 1)
            {
                foreach (DataRow item in dsMessage.Tables[1].Rows)
                {

                    CMD.lstImages.Add(new MessageAttachment()
                    {
                        AttachementFullName = item["AttachmentName"].ToString(),
                        AttachmentId = Convert.ToInt32(item["Id"]),
                        AttachmentName = item["AttachmentName"].ToString().Substring(item["AttachmentName"].ToString().LastIndexOf("$") + 1)
                    });
                }
            }
            return CMD;
        }

        #endregion
        public List<MessageListOfDoctor> GetRecivedReferralListOfDoctor(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                lstInboxOfDoctor = new List<MessageListOfDoctor>();
                dt = objColleaguesData.GetRecivedReferralDetailsbyUserId(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lstInboxOfDoctor.Add(new MessageListOfDoctor()
                        {
                            MessageId = Convert.ToInt32(item["MessageId"]),
                            Field = objCommon.CheckNull(Convert.ToString(item["FromField"]), string.Empty),
                            Body = Regex.Replace(Regex.Replace(objCommon.RemoveHTML(objCommon.CheckNull(Convert.ToString(item["Body"]), string.Empty)), @"<[^>]+>|&nbsp;", string.Empty), @"[\r\n\t]+", string.Empty),
                            CreationDate = Convert.ToDateTime(item["CreationDate"]),
                            MessageTypeId = Convert.ToInt32(item["MessageTypeId"]),
                            AttachmentId = objCommon.CheckNull(Convert.ToString(item["AttachmentId"]), string.Empty),
                            IsRead = objCommon.CheckNull(Convert.ToString(item["IsRead"]), string.Empty),

                        });
                    }
                }
            }
            catch (Exception ex)
            {
                bool status = objCommon.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                throw;
            }
            return lstInboxOfDoctor;

        }
        public List<MessageListOfDoctor> GetSendReferralListOfDoctor(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                lstInboxOfDoctor = new List<MessageListOfDoctor>();
                dt = objColleaguesData.GetSendReferralDetailsbyUserId(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lstInboxOfDoctor.Add(new MessageListOfDoctor()
                        {
                            MessageId = Convert.ToInt32(item["MessageId"]),
                            Field = objCommon.CheckNull(Convert.ToString(item["ToField"]), string.Empty),
                            Body = Regex.Replace(Regex.Replace(objCommon.RemoveHTML(objCommon.CheckNull(Convert.ToString(item["Body"]), string.Empty)), @"<[^>]+>|&nbsp;", string.Empty), @"[\r\n\t]+", string.Empty),
                            CreationDate = Convert.ToDateTime(item["CreationDate"]),
                            MessageTypeId = Convert.ToInt32(item["MessageTypeId"]),
                            AttachmentId = objCommon.CheckNull(Convert.ToString(item["AttachmentId"]), string.Empty),
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                bool status = objCommon.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                throw;
            }
            return lstInboxOfDoctor;
        }

        public bool GetCommunicationHistoryForPatientByDoctorID(int UserId, int PatientId)
        {
            bool dt = objColleaguesData.GetCommunicationHistoryForPatientByDocID(UserId, PatientId);
            return dt;
        }

        public List<ColleaguesMessagesOfDoctor> GetCommunicationHistoryForPatient(int UserId, int PatientId, int PageIndex, int PageSize, int SortColumn, int SortDirection, string TimeZoneSystemName)
        {
            DataTable dt = objColleaguesData.GetCommunicationHistoryForPatient(UserId, PatientId, PageIndex, PageSize, SortColumn, SortDirection);
            lstColleaguesMessagesDoctor = new List<ColleaguesMessagesOfDoctor>();
            if (dt != null && dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    ColleaguesMessagesOfDoctor objcolleaguesdata = new ColleaguesMessagesOfDoctor();
                    objcolleaguesdata.lstImages = new List<MessageAttachment>();
                    objcolleaguesdata.MessageId = Convert.ToInt32(item["MessageId"]);
                    objcolleaguesdata.MessageTypeId = Convert.ToInt32(item["MessageTypeId"].ToString());
                    objcolleaguesdata.ColleagueName = item["ColleagueName"].ToString();
                    string Body = HttpUtility.UrlDecode(item["Messages"].ToString());
                    //objcolleaguesdata.Message = Regex.Replace(Regex.Replace(objCommon.RemoveHTML(objCommon.CheckNull(Body, string.Empty)), @"<[^>]+>|&nbsp;", string.Empty), @"[\r\n\t]+", string.Empty);
                    HtmlDocument htmlDoc = new HtmlDocument();
                    htmlDoc.LoadHtml(Body);
                    //string Message = htmlDoc.DocumentNode.InnerText;
                    //objcolleaguesdata.Message = Convert.ToString(item["Messages"].ToString());
                    string BOdy = System.Uri.UnescapeDataString(Convert.ToString(item["Messages"]));
                    objcolleaguesdata.Message = Regex.Replace(BOdy, @"<[^>]+>|&nbsp;", "").Trim();
                    //  objcolleaguesdata.CreationDate = string.IsNullOrEmpty(item["CreationDate"].ToString()) ? string.Empty : Convert.ToDateTime(item["CreationDate"].ToString()).ToString("MM/dd/yyyy hh:mm tt");
                    objcolleaguesdata.CreationDate = Convert.ToString(item["CreationDate"]);
                    if (!string.IsNullOrEmpty(item["ColleagueId"].ToString()))
                    {
                        objcolleaguesdata.ColleagueId = Convert.ToInt32(item["ColleagueId"]);
                    }
                    //if (!string.IsNullOrEmpty(item["Id"].ToString()))
                    //{
                    //    objcolleaguesdata.lstImages.Add(new MessageAttachment()
                    //    {
                    //        AttachmentId = Convert.ToInt32(item["Id"]),
                    //        AttachmentName = Convert.ToString(item["AttchementName"])
                    //    });
                    //}
                    int PatientId1 = PatientId > 0 ? PatientId : 0;
                    objcolleaguesdata.PatientId = PatientId1;

                    if (!string.IsNullOrEmpty(objcolleaguesdata.CreationDate))
                    {
                        DateTime dtCreationDate = clsHelper.ConvertFromUTC(Convert.ToDateTime(objcolleaguesdata.CreationDate), TimeZoneSystemName);
                        objcolleaguesdata.CreationDate = new CommonBLL().ConvertToDateTime(Convert.ToDateTime(dtCreationDate), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL));
                    }
                        

                    lstColleaguesMessagesDoctor.Add(objcolleaguesdata);
                }
            }
            return lstColleaguesMessagesDoctor;
        }
        public List<ColleaguesMessagesOfDoctor> GetColleaguesMessagesForDoctor(int UserId, int PatientId)
        {
            try
            {
                lstColleaguesMessagesDoctor = new List<ColleaguesMessagesOfDoctor>();
                DataSet ds = new DataSet();
                ds = objColleaguesData.GetColleaguesCommunicationbyDoctor(UserId, PatientId);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        ColleaguesMessagesOfDoctor objcolleaguesdata = new ColleaguesMessagesOfDoctor();
                        objcolleaguesdata.MessageId = Convert.ToInt32(item["MessageId"]);
                        objcolleaguesdata.Message = item["Messages"].ToString();
                        objcolleaguesdata.ColleagueName = item["ColleagueName"].ToString();
                        objcolleaguesdata.MessageTypeId = Convert.ToInt32(item["MessageTypeId"]);
                        objcolleaguesdata.CreationDate = item["CreationDate"].ToString();
                        objcolleaguesdata.SenderId = Convert.ToInt32(item["SenderId"]);
                        objcolleaguesdata.ReciverId = Convert.ToInt32(item["ReferedUserId"]);
                        lstColleaguesMessagesDoctor.Add(objcolleaguesdata);
                    }
                    foreach (DataRow item in ds.Tables[1].Rows)
                    {

                        ColleaguesMessagesOfDoctor objcolleaguesdata = new ColleaguesMessagesOfDoctor();
                        objcolleaguesdata.MessageId = Convert.ToInt32(item["MessageId"]);
                        objcolleaguesdata.Message = item["Messages"].ToString();
                        objcolleaguesdata.MessageTypeId = Convert.ToInt32(item["MessageTypeId"]);
                        objcolleaguesdata.CreationDate = item["CreationDate"].ToString();
                        objcolleaguesdata.SenderId = Convert.ToInt32(item["SenderId"]);
                        objcolleaguesdata.ReciverId = Convert.ToInt32(item["ReciverId"]);
                        objcolleaguesdata.SenderName = Convert.ToString(item["SenderName"]);
                        objcolleaguesdata.ReciverName = Convert.ToString(item["ReciverName"]);
                        var AttachedPatientId = objcolleaguesdata.AttachedPatientsId = item["AttachedPatientIds"].ToString();

                        var attachlst = AttachedPatientId.Split(',').ToList();
                        var CheckPatientId = attachlst.Any(x => x.Contains(PatientId.ToString()));
                        if (CheckPatientId)
                        {
                            objcolleaguesdata.PatientName = objcolleaguesdata.AttachedPatientsId;
                            lstColleaguesMessagesDoctor.Add(objcolleaguesdata);
                        }

                    }

                }
                return lstColleaguesMessagesDoctor;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public List<SearchColleagueForDoctor> GetSearchColleagueForDoctor(int UserId, int PageIndex, int PageSize, string Name, string Email, string phone, string City, string State, string Zipcode, string Institute, string SpecialtityList, string Keywords)
        {

            objColleaguesData = new clsColleaguesData();
            DataTable dt = new DataTable();
            dt = objColleaguesData.GetSearchColleagueForDoctor(UserId, PageIndex, PageSize, Name, Email, phone, City, State, Zipcode, Institute, SpecialtityList, Keywords);
            List<SearchColleagueForDoctor> lst1 = new List<SearchColleagueForDoctor>();
            if (dt!=null && dt.Rows.Count > 0)
            {
                
                foreach (DataRow item in dt.Rows)
                {
                    string ImagPath = string.Empty;
                    if (string.IsNullOrWhiteSpace(Convert.ToString(item["ImageName"])))
                    {
                        ImagPath = Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]);
                    }
                    else
                    {
                        ImagPath = Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]) + Convert.ToString(item["ImageName"]);
                    }
                    decimal Miles = 0;
                    if (string.IsNullOrWhiteSpace(Convert.ToString(item["Miles"])))
                    {
                        Miles = 0;
                    }
                    else
                    {
                        Miles = Convert.ToDecimal(item["Miles"]);
                    }
                    lst1.Add(new SearchColleagueForDoctor()
                    {
                        UserId = Convert.ToInt32(item["UserId"]),
                        FirstName =Convert.ToString(item["FirstName"]),
                        LastName = Convert.ToString(item["LastName"]),
                        ImageName = ImagPath,
                        ExactAddress = Convert.ToString(item["ExactAddress"]),
                        Address2 = Convert.ToString(item["Address2"]),
                        City = Convert.ToString(item["City"]),
                        State = Convert.ToString(item["State"]),
                        Miles = Miles,
                        Specialities = Convert.ToString(item["Specialities"]),
                        OfficeName = Convert.ToString(item["OfficeName"]),
                        TotalRecord = Convert.ToInt32(item["TotalRecord"]),
                        ZipCode = Convert.ToString(item["ZipCode"]),
                    });

                }
            }

            //List<SearchColleagueForDoctor> lst = (from c in dt.AsEnumerable()
            //                                      select new SearchColleagueForDoctor
            //                                      {
            //                                          UserId = c.Field<int>("UserId"),
            //                                          FirstName = c.Field<string>("FirstName"),
            //                                          LastName = c.Field<string>("LastName"),
            //                                          ImageName = c.Field<string>("ImageName") == null ? objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty) : objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + c.Field<string>("ImageName"),
            //                                          ExactAddress = c.Field<string>("ExactAddress"),
            //                                          Address2 = c.Field<string>("Address2"),
            //                                          City = c.Field<string>("City"),
            //                                          State = c.Field<string>("State"),
            //                                          Miles = c.Field<object>("Miles") == null ? 0 : c.Field<decimal>("Miles"),
            //                                          Specialities = c.Field<string>("Specialities"),
            //                                          OfficeName = c.Field<string>("OfficeName"),
            //                                          TotalRecord = c.Field<int>("TotalRecord"),
            //                                          ZipCode = c.Field<string>("ZipCode")
            //                                      }).ToList();
            return lst1;
        }


        public int GetExistingEmail(int UserID, string Email, string PersonalNotes)
        {
            try
            {
                objColleaguesData = new clsColleaguesData();

                string[] arryEmail = Email.Split(',');
                for (int i = 0; i < arryEmail.Count(); i++)
                {
                    DataTable dt = new DataTable();
                    dt = objColleaguesData.CheckEmailInSystem(arryEmail[i]);
                    DataTable dtCheckAsColleague = objColleaguesData.GetColleaguesDetailsForDoctor(UserID, 1, int.MaxValue, Email, 1, 2);

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        if (dtCheckAsColleague.Rows.Count == 0)
                        {
                            AddAsColleague(UserID, Convert.ToInt32(dt.Rows[i]["UserId"]));
                            //Send Added Colleague Templet
                            ObjTemplate.ColleagueInviteDoctor(UserID, Convert.ToInt32(dt.Rows[i]["UserId"]), PersonalNotes);
                        }
                        else
                        {
                            return 0;
                        }

                    }
                    else
                    {
                        //Send Invite Colleague Templet
                        // ObjTemplate.InviteDoctor(Email, UserID, PersonalNotes);
                        return 2;
                    }

                }
            }
            catch (Exception)
            {

                return 0;
            }

            return 1;
        }
       

        #region Code for Send Suggest a Change
        public bool SuggestChange(int UserId, int ColleagueId, string Subject, string Suggestion)
        {
            return ObjTemplate.SuggestChange(UserId, ColleagueId, Subject, Suggestion);
        }
        #endregion


        // Nimesh

        public List<MessageListOfDoctor> GetInboxOfDoctor(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext, string IsRefarral, string TimeZoneSystemName)
        {

            lstInboxOfDoctor = new List<MessageListOfDoctor>();
            objColleaguesData = new clsColleaguesData();
            DataTable dt = new DataTable();
            dt = objColleaguesData.InboxOfDoctor(UserId, PageIndex, PageSize, SortColumn, SortDirection, Searchtext, IsRefarral);

            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {

                    foreach (DataRow row in dt.Rows)
                    {
                        string Body = Uri.UnescapeDataString(Convert.ToString(row["Body"]));
                        lstInboxOfDoctor.Add(new MessageListOfDoctor()
                        {
                            MessageId = Convert.ToInt32(row["MessageId"]),
                            PatientName = objCommon.CheckNull(Convert.ToString(row["PatientName"]), string.Empty),
                            Field = objCommon.CheckNull(Convert.ToString(row["FromField"]), string.Empty),
                            Body = Regex.Replace(Regex.Replace(objCommon.RemoveHTML(objCommon.CheckNull(Body, string.Empty)), @"<[^>]+>|&nbsp;", string.Empty), @"[\r\n\t]+", string.Empty),
                            CreationDate = clsHelper.ConvertFromUTC(Convert.ToDateTime(row["CreationDate"]), TimeZoneSystemName),
                            TotalRecord = Convert.ToInt32(row["TotalRecord"]),
                            MessageTypeId = Convert.ToInt32(row["MessageTypeId"]),
                            AttachmentId = objCommon.CheckNull(Convert.ToString(row["AttachmentId"]), string.Empty),
                            IsRead = objCommon.CheckNull(Convert.ToString(row["IsRead"]), string.Empty),

                        });
                    }
                }
            }
            catch (Exception ex)
            {
                bool status = objCommon.InsertErrorLog(HttpContext.Current.Request.Url.ToString(), ex.Message, ex.StackTrace);
                throw;
            }

            return lstInboxOfDoctor;
        }

        public List<NewReferralView> GetNewReferralViewByRefCardId(int ReferralCardId)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = objColleaguesData.GetNewReferralMessageViewByRefCardId(ReferralCardId);
                List<NewReferralView> LstNewrefView = new List<NewReferralView>();
                if (dt != null && dt.Rows.Count != 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (Convert.ToInt32(dt.Rows[i]["Sub_Sub_CatId"]) != 0)
                        {
                            LstNewrefView.Add(new NewReferralView()
                            {
                                CategoryId = Convert.ToInt32(objCommon.CheckNull(Convert.ToString(dt.Rows[i]["CategoryId"]), string.Empty)),
                                CategoryName = objCommon.CheckNull(Convert.ToString(dt.Rows[i]["CategoryName"]), string.Empty),
                                SubCategoryId = Convert.ToInt32(objCommon.CheckNull(Convert.ToString(dt.Rows[i]["Sub_CatId"]), string.Empty)),
                                SubCategoryName = objCommon.CheckNull(Convert.ToString(dt.Rows[i]["SubCatName"]), string.Empty),
                                SubCategoryHeaderText = objCommon.CheckNull(Convert.ToString(dt.Rows[i]["HeaderText"]), string.Empty),
                                SubSubCategoryId = Convert.ToInt32(objCommon.CheckNull(Convert.ToString(dt.Rows[i]["Sub_Sub_CatId"]), string.Empty)),
                                SubSubCategoryName = objCommon.CheckNull(Convert.ToString(dt.Rows[i]["Sub_Sub_CatName"]), string.Empty),
                                SubSubCategoryHeaderText = objCommon.CheckNull(Convert.ToString(dt.Rows[i]["SubSubCatHeaderText"]), string.Empty),
                                SubSubCategoryValue = objCommon.CheckNull(Convert.ToString(dt.Rows[i]["Value"]), string.Empty),
                            });
                        }
                        else
                        {
                            LstNewrefView.Add(new NewReferralView()
                            {
                                CategoryId = Convert.ToInt32(objCommon.CheckNull(Convert.ToString(dt.Rows[i]["CategoryId"]), string.Empty)),
                                CategoryName = objCommon.CheckNull(Convert.ToString(dt.Rows[i]["CategoryName"]), string.Empty),
                                SubCategoryId = Convert.ToInt32(objCommon.CheckNull(Convert.ToString(dt.Rows[i]["Sub_CatId"]), string.Empty)),
                                SubCategoryName = objCommon.CheckNull(Convert.ToString(dt.Rows[i]["SubCatName"]), string.Empty),
                                SubCategoryHeaderText = objCommon.CheckNull(Convert.ToString(dt.Rows[i]["HeaderText"]), string.Empty),
                                SubCategoryValue = objCommon.CheckNull(Convert.ToString(dt.Rows[i]["Value"]), string.Empty),
                                SubSubCategoryId = Convert.ToInt32(objCommon.CheckNull(Convert.ToString(dt.Rows[i]["Sub_Sub_CatId"]), string.Empty)),
                                SubSubCategoryName = objCommon.CheckNull(Convert.ToString(dt.Rows[i]["Sub_Sub_CatName"]), string.Empty),
                                SubSubCategoryHeaderText = objCommon.CheckNull(Convert.ToString(dt.Rows[i]["SubSubCatHeaderText"]), string.Empty),
                            });
                        }
                    }
                }
                return LstNewrefView;
            }
            catch (Exception EX)
            {
                objCommon.InsertErrorLog("mdlcolleagues--GetNewReferralViewByRefCardId", EX.Message, EX.StackTrace);
                throw;
            }
        }
        public List<MessageListOfDoctor> GetSentboxOfDoctor(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext, string IsReferral, string TimeZoneSystemName)
        {

            lstInboxOfDoctor = new List<MessageListOfDoctor>();
            objColleaguesData = new clsColleaguesData();
            DataTable dt = new DataTable();
            dt = objColleaguesData.SentboxOfDoctor(UserId, PageIndex, PageSize, SortColumn, SortDirection, Searchtext, IsReferral);

            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    string BOdy = System.Uri.UnescapeDataString(Convert.ToString(row["Body"]));
                    DateTime CreationDate = DateTime.MinValue;
                    if (!string.IsNullOrEmpty(Convert.ToString(row["CreationDate"])))
                        CreationDate = clsHelper.ConvertFromUTC(Convert.ToDateTime(row["CreationDate"]), TimeZoneSystemName);
                    lstInboxOfDoctor.Add(new MessageListOfDoctor()
                    {
                        
                        MessageId = Convert.ToInt32(row["MessageId"]),
                        Field = objCommon.CheckNull(Convert.ToString(row["ToField"]), string.Empty),
                        Body = Regex.Replace(Regex.Replace(objCommon.RemoveHTML(objCommon.CheckNull(Convert.ToString(BOdy), string.Empty)), @"<[^>]+>|&nbsp;", string.Empty), @"[\r\n\t]+", string.Empty),
                        CreationDate = CreationDate,
                        TotalRecord = Convert.ToInt32(row["TotalRecord"]),
                        MessageTypeId = Convert.ToInt32(row["MessageTypeId"]),
                        AttachmentId = objCommon.CheckNull(Convert.ToString(row["AttachmentId"]), string.Empty),
                        PatientName = objCommon.CheckNull(Convert.ToString(row["PatientName"]), string.Empty),

                    });
                }
            }


            return lstInboxOfDoctor;
        }
        //AK changes for Getting Draft Message List functionality 02-28-2017
        public List<MessageListOfDoctor> GetDraftofDoctor(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext, bool IsReferral)
        {
            try
            {
                lstInboxOfDoctor = new List<MessageListOfDoctor>();
                objColleaguesData = new clsColleaguesData();
                DataTable dt = new DataTable();
                dt = objColleaguesData.GetDraftMessagesofDoctor(UserId, PageIndex, PageSize, SortColumn, SortDirection, Searchtext, IsReferral);
                if(dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        string BOdy = System.Uri.UnescapeDataString(Convert.ToString(row["Body"]));
                        lstInboxOfDoctor.Add(new MessageListOfDoctor() {
                            MessageId = Convert.ToInt32(row["MessageId"]),
                            Field = objCommon.CheckNull(Convert.ToString(row["ToField"]), string.Empty),
                            Body = Regex.Replace(Regex.Replace(objCommon.RemoveHTML(objCommon.CheckNull(Convert.ToString(BOdy), string.Empty)), @"<[^>]+>|&nbsp;", string.Empty), @"[\r\n\t]+", string.Empty),
                            CreationDate = Convert.ToDateTime(row["CreationDate"]),
                            MessageTypeId = Convert.ToInt32(row["MessageTypeId"]),
                            AttachmentId = objCommon.CheckNull(Convert.ToString(row["AttachmentId"]), string.Empty),
                            PatientName = objCommon.CheckNull(Convert.ToString(row["PatientName"]), string.Empty),
                        });
                    }
                }
                return lstInboxOfDoctor;
            }
            catch (Exception Ex)
            {
                objCommon.InsertErrorLog("MDLColleagues--GetDraftofDoctor", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        public List<MessageDetails> GetMessageDetailsById(int UserId, int MessageId, int MessageTypeId, string TimeZoneSystemName)
        {
            lstMessageDetails = new List<MessageDetails>();

            try
            {
                DataTable dt = new DataTable();
                DataSet ds = new DataSet();
                if (MessageTypeId == 0)
                {
                    dt = objColleaguesData.GetInBoxPatientMessageDetailsById(UserId, MessageId);
                    foreach (DataRow item in dt.Rows)
                    {
                        string Body = Uri.UnescapeDataString(Convert.ToString(item["Body"]));
                        string CreationDate = string.Empty;
                        if(!string.IsNullOrEmpty(Convert.ToString(item["CreationDate"]))) 
                            CreationDate =  Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(item["CreationDate"]), TimeZoneSystemName));

                        MessageDetails ObjMessageDetails = new MessageDetails();
                        DataTable dts = new DataTable();
                        ObjMessageDetails.MessageId = Convert.ToInt32(item["MessageId"]);
                        ObjMessageDetails.SenderId = Convert.ToInt32(item["Senderid"]);
                        ObjMessageDetails.From = objCommon.CheckNull(Convert.ToString(item["FromField"]), string.Empty);
                        ObjMessageDetails.To = objCommon.CheckNull(Convert.ToString(item["ToField"]), string.Empty);
                        ObjMessageDetails.CreationDate = CreationDate;
                        ObjMessageDetails.Body = objCommon.CheckNull(Body, string.Empty);
                        ObjMessageDetails.SenderEmail = objCommon.CheckNull(Convert.ToString(item["SenderEmail"]), string.Empty);
                        ObjMessageDetails.ReceiverEmail = objCommon.CheckNull(Convert.ToString(item["ReceiverEmail"]), string.Empty);
                        DataTable dtAttachments = new DataTable();

                        dtAttachments = objColleaguesData.GetPatientMessageAttachemntsById(Convert.ToInt32(item["MessageId"]));

                        List<MessageAttachment> lstAttachment = new List<MessageAttachment>();
                        foreach (DataRow Attachment in dtAttachments.Rows)
                        {
                            MessageAttachment ObjMessageAttachment = new MessageAttachment();
                            ObjMessageAttachment.AttachmentId = int.Parse(objCommon.CheckNull(Attachment["Id"].ToString(), "0"));
                            ObjMessageAttachment.AttachmentName = objCommon.CheckNull(Attachment["AttachmentName"].ToString(), string.Empty);
                            lstAttachment.Add(ObjMessageAttachment);
                        }

                        ObjMessageDetails.lstMessageAttachment = lstAttachment;

                        lstMessageDetails.Add(ObjMessageDetails);
                    }
                }
                else
                {
                    ds = objColleaguesData.GetInboxMessageDetailsById(UserId, MessageId);
                }

                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        string Body = Uri.UnescapeDataString(Convert.ToString(item["Body"]));
                        string CreationDate = string.Empty;
                        if (!string.IsNullOrEmpty(Convert.ToString(item["CreationDate"])))
                            CreationDate = Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(item["CreationDate"]), TimeZoneSystemName));
                        MessageDetails ObjMessageDetails = new MessageDetails();
                        ObjMessageDetails.MessageId = Convert.ToInt32(item["MessageId"]);
                        ObjMessageDetails.SenderId = Convert.ToInt32(item["Senderid"]);
                        ObjMessageDetails.From = objCommon.CheckNull(Convert.ToString(item["FromField"]), string.Empty);
                        ObjMessageDetails.To = objCommon.CheckNull(Convert.ToString(item["ToField"]), string.Empty);
                        ObjMessageDetails.CreationDate = CreationDate;
                        ObjMessageDetails.Body = objCommon.CheckNull(Body, string.Empty);
                        ObjMessageDetails.SenderEmail = objCommon.CheckNull(Convert.ToString(item["SenderEmail"]), string.Empty);
                        ObjMessageDetails.ReceiverEmail = objCommon.CheckNull(Convert.ToString(item["ReceiverEmail"]), string.Empty);
                        if (!string.IsNullOrEmpty(item["AttachedPatientIds"].ToString()))
                        {
                            ObjMessageDetails.AttachedPatientsId = item["AttachedPatientIds"].ToString();
                            if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                            {
                                ObjMessageDetails.lstAttachedPatient.Add(new AttachedPatientDetails()
                                {
                                    AttachedPatientId = Convert.ToInt32(ds.Tables[1].Rows[0]["PatientId"]),
                                    AttachedPatientFirstName = Convert.ToString(ds.Tables[1].Rows[0]["FirstName"]),
                                    AttachedPatientLastName = Convert.ToString(ds.Tables[1].Rows[0]["LastName"])
                                });
                            }
                            else
                            {
                                ObjMessageDetails.lstAttachedPatient = new List<AttachedPatientDetails>();
                            }
                        }

                        DataTable dtAttachments = new DataTable();
                        // Datatable 
                        if (MessageTypeId == 0)
                        {
                            dtAttachments = objColleaguesData.GetPatientMessageAttachemntsById(Convert.ToInt32(item["MessageId"]));
                        }
                        else
                        {
                            dtAttachments = objColleaguesData.GetColleageMessageAttachemntsById(Convert.ToInt32(item["MessageId"]));
                        }

                        List<MessageAttachment> lstAttachment = new List<MessageAttachment>();
                        foreach (DataRow Attachment in dtAttachments.Rows)
                        {
                            MessageAttachment ObjMessageAttachment = new MessageAttachment();
                            ObjMessageAttachment.AttachmentId = int.Parse(objCommon.CheckNull(Attachment["Id"].ToString(), "0"));
                            ObjMessageAttachment.AttachmentName = objCommon.CheckNull(Attachment["AttachmentName"].ToString(), string.Empty);
                            lstAttachment.Add(ObjMessageAttachment);

                        }

                        ObjMessageDetails.lstMessageAttachment = lstAttachment;

                        lstMessageDetails.Add(ObjMessageDetails);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstMessageDetails;
        }

        public List<ReferredPatient> GetSendReferredPatientList(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirection, string SearchText, string TimeZoneSystemName)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = objColleaguesData.GetSendReferredPatientList(UserId, PageIndex, PageSize, SortColumn, SortDirection, SearchText);

                List<ReferredPatient> refpatient = new List<ReferredPatient>();
                if (dt != null && dt.Rows.Count > 0)
                {
                    string CreationDate;
                    foreach (DataRow item in dt.Rows)
                    {

                        CreationDate = string.Empty;
                        if (!string.IsNullOrEmpty(Convert.ToString(item["CreationDate"])))
                            CreationDate = Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(item["CreationDate"]), TimeZoneSystemName));
                        refpatient.Add(new ReferredPatient()
                        {
                            MessageId = Convert.ToInt32(item["MessageId"]),
                            PatientId = Convert.ToInt32(item["PatientId"]),
                            SenderId = Convert.ToInt32(item["SenderId"]),
                            ReceiveId = Convert.ToInt32(item["ReferedUserId"]),
                            SenderName = Convert.ToString(item["SenderName"]),
                            ReceiverName = Convert.ToString(item["ReceiverName"]),
                            PatientName = Convert.ToString(item["PatientName"]),
                            MessageBody = Convert.ToString(item["Body"]),
                            CreationDate = CreationDate

                        });
                    }
                }
                return refpatient;
            }
            catch (Exception)
            {

                throw;
            }

        }
        public List<DoctorLocation> GetDoctorList(int UserId)
        {
            try
            {
                List<DoctorLocation> doclst = new List<DoctorLocation>();
                DataTable dt = new DataTable();
                dt = objColleaguesData.GetDoctorLocaitonById(UserId);
                if (dt != null)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        doclst.Add(new DoctorLocation()
                        {
                            LocationId = Convert.ToInt32(item["AddressInfoId"]),
                            LocationName = Convert.ToString(item["Location"])
                        });
                    }
                }
                return doclst;
            }
            catch (Exception EX)
            {

                throw;
            }
        }
        public List<ReferredPatient> GetReceivedPatientList(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirection, string SearchText, string TimeZoneSystemName)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = objColleaguesData.GetReceivedReferredPatientList(UserId, PageIndex, PageSize, SortColumn, SortDirection, SearchText);
                List<ReferredPatient> refpatient = new List<ReferredPatient>();
                string CreationDate = string.Empty;
                
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        CreationDate = string.Empty;
                        if (!string.IsNullOrEmpty(Convert.ToString(item["CreationDate"])))
                            CreationDate = Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(item["CreationDate"]), TimeZoneSystemName));
                        
                        refpatient.Add(new ReferredPatient()
                        {
                            MessageId = Convert.ToInt32(item["MessageId"]),
                            PatientId = Convert.ToInt32(item["PatientId"]),
                            SenderId = Convert.ToInt32(item["SenderId"]),
                            ReceiveId = Convert.ToInt32(item["ReferedUserId"]),
                            SenderName = Convert.ToString(item["SenderName"]),
                            ReceiverName = Convert.ToString(item["ReceiverName"]),
                            PatientName = Convert.ToString(item["PatientName"]),
                            MessageBody = Convert.ToString(item["Body"]),
                            CreationDate = CreationDate
                        });
                    }
                }
                return refpatient;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public List<MessageDetails> GetSentMessageDetailsById(int UserId, int MessageId, int MessageTypeId, string TimeZoneSystemName)
        {
            lstMessageDetails = new List<MessageDetails>();

            try
            {
                DataSet ds = new DataSet();
                DataTable dt = new DataTable();
                if (MessageTypeId == 3)
                {
                    dt = objColleaguesData.GetSentboxPatientMessageDetailsById(UserId, MessageId);
                    foreach (DataRow item in dt.Rows)
                    {
                        string Body = Uri.UnescapeDataString(Convert.ToString(item["Body"]));
                        MessageDetails ObjMessageDetails = new MessageDetails();
                        DataTable dts = new DataTable();
                        ObjMessageDetails.MessageId = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(item["MessageId"])) ? 0 : item["MessageId"]);
                        ObjMessageDetails.SenderId = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(item["Senderid"])) ? 0 : item["Senderid"]);
                        ObjMessageDetails.From = objCommon.CheckNull(Convert.ToString(item["FromField"]), string.Empty);
                        ObjMessageDetails.To = objCommon.CheckNull(Convert.ToString(item["ToField"]), string.Empty);
                        ObjMessageDetails.CreationDate = objCommon.CheckNull(Convert.ToString(item["CreationDate"]), string.Empty);
                        ObjMessageDetails.Body = objCommon.CheckNull(Body, string.Empty);
                        ObjMessageDetails.SenderEmail = objCommon.CheckNull(Convert.ToString(item["SenderEmail"]), string.Empty);
                        ObjMessageDetails.ReceiverEmail = objCommon.CheckNull(Convert.ToString(item["ReceiverEmail"]), string.Empty);
                        ObjMessageDetails.Status = Convert.ToBoolean(item["Status"]);
                        DataTable dtAttachments = new DataTable();

                        dtAttachments = objColleaguesData.GetPatientMessageAttachemntsById(Convert.ToInt32(item["MessageId"]));

                        List<MessageAttachment> lstAttachment = new List<MessageAttachment>();
                        foreach (DataRow Attachment in dtAttachments.Rows)
                        {
                            MessageAttachment ObjMessageAttachment = new MessageAttachment();
                            ObjMessageAttachment.AttachmentId = int.Parse(objCommon.CheckNull(Attachment["Id"].ToString(), "0"));
                            ObjMessageAttachment.AttachmentName = objCommon.CheckNull(Attachment["AttachmentName"].ToString(), string.Empty);
                            ObjMessageAttachment.AttachementFullName = objCommon.CheckNull(Attachment["AttachmentName"].ToString(), string.Empty);
                            //ObjMessageAttachment.AttachmentName = 
                            lstAttachment.Add(ObjMessageAttachment);
                        }

                        ObjMessageDetails.lstMessageAttachment = lstAttachment;

                        if(!string.IsNullOrEmpty(ObjMessageDetails.CreationDate))
                            ObjMessageDetails.CreationDate = clsHelper.ConvertFromUTC(Convert.ToDateTime(ObjMessageDetails.CreationDate),TimeZoneSystemName).ToString("MM/dd/yyy hh:mm tt");

                        lstMessageDetails.Add(ObjMessageDetails);
                    }
                }
                else
                {
                    ds = objColleaguesData.GetSentboxMessageDetailsById(UserId, MessageId);
                }

                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataRow item in ds.Tables[0].Rows)
                    {
                        string body = Uri.UnescapeDataString(Convert.ToString(item["Body"]));
                        MessageDetails ObjMessageDetails = new MessageDetails();
                        ObjMessageDetails.MessageId = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(item["MessageId"])) ? 0 : item["MessageId"]);
                        ObjMessageDetails.SenderId = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(item["Senderid"])) ? 0 : item["Senderid"]);
                        ObjMessageDetails.From = objCommon.CheckNull(Convert.ToString(item["FromField"]), string.Empty);
                        ObjMessageDetails.To = objCommon.CheckNull(Convert.ToString(item["ToField"]), string.Empty);
                        ObjMessageDetails.CreationDate = objCommon.CheckNull(Convert.ToString(item["CreationDate"]), string.Empty);
                        ObjMessageDetails.Body = objCommon.CheckNull(body, string.Empty);
                        ObjMessageDetails.SenderEmail = objCommon.CheckNull(Convert.ToString(item["SenderEmail"]), string.Empty);
                        ObjMessageDetails.ReceiverEmail = objCommon.CheckNull(Convert.ToString(item["ReceiverEmail"]), string.Empty);
                        ObjMessageDetails.Status = Convert.ToBoolean(item["Status"]);
                        if (!string.IsNullOrEmpty(item["AttachedPatientIds"].ToString()))
                        {
                            ObjMessageDetails.AttachedPatientsId = item["AttachedPatientIds"].ToString();
                            if (ds.Tables.Count > 1 && ds.Tables[1].Rows.Count > 0)
                            {
                                foreach (DataRow item1 in ds.Tables[1].Rows)
                                {
                                    ObjMessageDetails.lstAttachedPatient.Add(new AttachedPatientDetails()
                                    {
                                        AttachedPatientId = Convert.ToInt32(item1["PatientId"]),
                                        AttachedPatientFirstName = Convert.ToString(item1["FirstName"]),
                                        AttachedPatientLastName = Convert.ToString(item1["LastName"])
                                    });
                                }
                            }
                            else
                            {
                                ObjMessageDetails.lstAttachedPatient = new List<AttachedPatientDetails>();
                            }
                        }

                        DataTable dtAttachments = new DataTable();
                        // Datatable 
                        if (MessageTypeId == 0)
                        {
                            dtAttachments = objColleaguesData.GetPatientMessageAttachemntsById(Convert.ToInt32(item["MessageId"]));
                        }
                        else
                        {
                            dtAttachments = objColleaguesData.GetColleageMessageAttachemntsById(Convert.ToInt32(item["MessageId"]));
                        }

                        List<MessageAttachment> lstAttachment = new List<MessageAttachment>();
                        foreach (DataRow Attachment in dtAttachments.Rows)
                        {
                            MessageAttachment ObjMessageAttachment = new MessageAttachment();
                            ObjMessageAttachment.AttachmentId = int.Parse(objCommon.CheckNull(Attachment["Id"].ToString(), "0"));
                            ObjMessageAttachment.AttachmentName = objCommon.CheckNull(Attachment["AttachmentName"].ToString(), string.Empty);
                            lstAttachment.Add(ObjMessageAttachment);

                        }

                        ObjMessageDetails.lstMessageAttachment = lstAttachment;
                        if (!string.IsNullOrEmpty(ObjMessageDetails.CreationDate))
                            ObjMessageDetails.CreationDate = clsHelper.ConvertFromUTC(Convert.ToDateTime(ObjMessageDetails.CreationDate), TimeZoneSystemName).ToString("MM/dd/yyy hh:mm tt");
                        lstMessageDetails.Add(ObjMessageDetails);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstMessageDetails;
        }

    }


    public class SearchColleagueForDoctor
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImageName { get; set; }
        public string ExactAddress { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public decimal Miles { get; set; }
        public string Specialities { get; set; }
        public string OfficeName { get; set; }
        public int TotalRecord { get; set; }
        public string ZipCode { get; set; }
    }
    public class ColleaguesDetails
    {


        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string ImageName { get; set; }
        public int ColleagueId { get; set; }
        public string ExactAddress { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string EmailAddress { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Officename { get; set; }
        public List<SpeacilitiesOfDoctor> lstSpeacilitiesOfDoctor = new List<SpeacilitiesOfDoctor>();
        public int TotalRecord { get; set; }
        public string SecondaryEmail { get; set; }
        public int ColleageReferralCount { get; set; }
        public string Location { get; set; }
        public int LocationId { get; set; }
     }
    public class SocialMediaForColleagues
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FacebookUrl { get; set; }
        public string LinkedinUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string GoogleplusUrl { get; set; }
        public string YoutubeUrl { get; set; }
        public string PinterestUrl { get; set; }
        public string BlogUrl { get; set; }
        public string YelpUrl { get; set; }
        public string OtherUrl { get; set; }
        public int TotalRecord { get; set; }
        public int cloudscore { get; set; }
    }


    public class MessageListOfDoctor
    {
        public int MessageId { get; set; }
        public string Field { get; set; }
        public string Body { get; set; }
        public DateTime CreationDate { get; set; }
        public int TotalRecord { get; set; }
        public int MessageTypeId { get; set; }
        public string IsRead { get; set; }
        public string AttachmentId { get; set; }
        public string PatientName { get; set; }

    }

    public class MessageDetails
    {
        public int MessageId { get; set; }
        public int SenderId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string CreationDate { get; set; }

        public bool Status { get; set; }
        public string Body { get; set; }
        public string SenderEmail { get; set; }

        public string ReceiverEmail { get; set; }
        public string AttachedPatientsId { get; set; }
        public List<MessageAttachment> lstMessageAttachment = new List<MessageAttachment>();
        public List<AttachedPatientDetails> lstAttachedPatient = new List<AttachedPatientDetails>();
    }
    public class AttachedPatientDetails
    {
        public int AttachedPatientId { get; set; }
        public string AttachedPatientFirstName { get; set; }
        public string AttachedPatientLastName { get; set; }
    }

    public class MessageAttachment
    {
        public int AttachmentId { get; set; }
        public string AttachmentName { get; set; }
        public string AttachementFullName { get; set; }
    }

    public class ReferralDetails
    {
        public int ReferralCardId { get; set; }
        public int MessageId { get; set; }
        public int SenderId { get; set; }
        public int ReceiverID { get; set; }

        public int PatientId { get; set; }
        public string ProfileImage { get; set; }
        public string FullName { get; set; }
        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string SenderName { get; set; }
        public string ReceiverName { get; set; }
        public string FullAddress { get; set; }
        public string Phone { get; set; }
        public string EmailAddress { get; set; }
        public string RegardOption { get; set; }
        public string RequestingOption { get; set; }
        public string OtherComments { get; set; }
        public string RequestComments { get; set; }
        public string Comments { get; set; }
        public string CreationDate { get; set; }
        public string PatientName { get; set; }
        public string Location { get; set; }
        public string AssignedPatientId { get; set; }
        public List<MessageAttachment> lstImages = new List<MessageAttachment>();
        public List<MessageAttachment> lstDocuments = new List<MessageAttachment>();
        public List<ToothList> lstToothList { get; set; }
        public List<ReferralDetails> lstRefferalDetails { get; set; }

        public List<Int32> lstSelectedTeeths { get; set; }
        public List<Int32> lstSelectedPermanentTeeths { get; set; }
        public List<Int32> lstSelectedPrimaryTeeths { get; set; }


        public string[] ReferralRegarding = { "Orthodontics", "Periodontist", "Oral Surgery", "Prosthodontics", "Radiology", "General Dentistry", "Endodontics", "Lab Work" };
        public string[] ReferralRequest = { "Extractions", "Pathology", "Caries", "Check Periodontal", "Oral Surgery", "Orthodontic Consultation", "Orthognathic Surgery", "Implants" };

    }

    public class ToothList
    {
        public int ID { get; set; }
        public int ToothType { get; set; }
    }
    public class ColleaguesMessagesOfDoctor
    {
        public int UserId { get; set; }
        public int MessageId { get; set; }
        public string Message { get; set; }
        public int MessageTypeId { get; set; }
        public int ColleagueId { get; set; }
        public string ColleagueName { get; set; }
        public string AttachedPatientsId { get; set; }
        public string CreationDate { get; set; }
        public string PatientName { get; set; }
        public string SenderName { get; set; }
        public string ReciverName { get; set; }
        public string SenderEmail { get; set; }
        public string ReciverEmail { get; set; }
        public int SenderId { get; set; }
        public int ReciverId { get; set; }
        public int PatientId { get; set; }

        public List<MessageAttachment> lstImages { get; set; }
        public List<AttachedPatientDetails> LstPatient { get; set; }
        public List<ReceiversDetails> lstReceiverDetails { get; set; }
        public List<MessageAttachments> MessageAttachments { get; set; }
    }
    public class ReceiversDetails
    {
        public string ReceiverId { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverEmail { get; set; }
    }
    public class ReferredPatient
    {
        public int PatientId { get; set; }
        public string PatientName { get; set; }
        public int SenderId { get; set; }
        public string SenderName { get; set; }
        public int ReceiveId { get; set; }
        public string ReceiverName { get; set; }
        public int MessageId { get; set; }
        public string MessageBody { get; set; }
        public string CreationDate { get; set; }
    }
    public class NewReferralView
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int SubCategoryId { get; set; }
        public string SubCategoryName { get; set; }
        public string SubCategoryHeaderText { get; set; }
        public string SubCategoryValue { get; set; }
        public int SubSubCategoryId { get; set; }
        public string SubSubCategoryName { get; set; }
        public string SubSubCategoryHeaderText { get; set; }
        public string SubSubCategoryValue { get; set; }
    }
    public class DoctorLocation
    {
        public int LocationId { get; set; }
        public string LocationName { get; set; }
    }

}