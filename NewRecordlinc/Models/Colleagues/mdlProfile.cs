﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.PatientsData;
using DataAccessLayer.Common;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Configuration;
using NewRecordlinc.Models.Patients;
using NewRecordlinc.Models;
using System.ComponentModel.DataAnnotations;
using BO.Models;
using BusinessLogicLayer;
using NewRecordlinc.Models.Common;

namespace NewRecordlinc.Models.Colleagues
{
    public class TimeZones
    {
        public int TimeZoneId { get; set; }
        public string TimeZoneText { get; set; }
    }
    public class Member
    {
        clsColleaguesData objColleaguesData = new clsColleaguesData();
        clsCommon objCommon = new clsCommon();
        DataSet ds = new DataSet();
        TripleDESCryptoHelper ObjCryptoHelper = new TripleDESCryptoHelper();
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }
        public string OfficeName { get; set; }
        public string Title { get; set; }
        public string ImageName { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string DentrixProviderId { get; set; }
        public string PublicProfile { get; set; }
        public string WebsiteURL { get; set; }
        public int TeamMemberUserId { get; set; }
        public string Location { get; set; }
        public int ProfileComplete { get; set; }
        public string Profilepercentage { get; set; }
        public string RemainList { get; set; }
        public string Institute { get; set; }
        public string MemberShip { get; set; }
        public string EncryptUserId { get; set; }
        public int LocationId { get; set; }
        public bool provtype { get; set; }
        public string Salutation { get; set; }
        public string specialtyIds { get; set; }
        public List<Banner> lstBanner { get; set; }
        public List<mdlGallary> lstGallary { get; set; }
        public List<Insurance> lstInsurance { get; set; }
        public string GallaryPath { get; set; }
        public List<AddressDetails> lstDoctorAddressDetails = new List<AddressDetails>();
        public List<AddressDetails> lstDoctorSortedAddressDetails = new List<AddressDetails>();
        public List<EducationandTraining> lstEducationandTraining = new List<EducationandTraining>();
        public List<ProfessionalMemberships> lstProfessionalMemberships = new List<ProfessionalMemberships>();
        public List<SpeacilitiesOfDoctor> lstSpeacilitiesOfDoctor = new List<SpeacilitiesOfDoctor>();
        public List<Procedure> lstProcedure = new List<Procedure>();
        public List<LicenseDetailsForDoctor> licensedetails = new List<LicenseDetailsForDoctor>();
        public LicenseDetailsForDoctor objLicense { get; set; }
        public SectionPublicProfile ObjProfileSection { get; set; }
        public List<AddressDetails> lstDoctorAddressDetailsByAddressInfoID = new List<AddressDetails>();
        public List<SocialMediaForDoctor> lstGetSocialMediaDetailByUserId = new List<SocialMediaForDoctor>();
        public List<EducationandTraining> lstEducationandTrainingForDoctorById = new List<EducationandTraining>();
        public List<ProfessionalMemberships> lstProfessionalMembershipForDoctorById = new List<ProfessionalMemberships>();
        public List<WebsiteDetails> lstsecondarywebsitelist = new List<WebsiteDetails>();
        public List<TimeZones> lstTimeZone { get; set; }
        public List<LocationDetails> LocationList { get; set; }

        public List<MessageListOfDoctor> lstInboxOfDoctor = new List<MessageListOfDoctor>();
        public List<MessageListOfDoctor> GetInboxOfDoctor(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirection, string Searchtext, string IsReferral)
        {
            lstInboxOfDoctor = new List<MessageListOfDoctor>();
            DataTable dt = new DataTable();

            try
            {
                dt = objColleaguesData.InboxOfDoctor(UserId, PageIndex, PageSize, SortColumn, SortDirection, Searchtext, IsReferral);

                if (dt != null && dt.Rows.Count > 0)
                {

                    foreach (DataRow row in dt.Rows)
                    {
                        lstInboxOfDoctor.Add(new MessageListOfDoctor()
                        {
                            MessageId = Convert.ToInt32(row["MessageId"]),
                            Field = objCommon.CheckNull(Convert.ToString(row["FromField"]), string.Empty),
                            Body = Regex.Replace(Regex.Replace(objCommon.RemoveHTML(objCommon.CheckNull(Convert.ToString(row["Body"]), string.Empty)), @"<[^>]+>|&nbsp;", string.Empty), @"[\r\n\t]+", string.Empty),
                            CreationDate = Convert.ToDateTime(row["CreationDate"]),
                            TotalRecord = Convert.ToInt32(row["TotalRecord"]),
                            MessageTypeId = Convert.ToInt32(row["MessageTypeId"]),
                            AttachmentId = objCommon.CheckNull(Convert.ToString(row["AttachmentId"]), string.Empty),

                        });
                    }
                }




            }
            catch (Exception)
            {

                throw;
            }
            return lstInboxOfDoctor;
        }

        public List<TeamMemberDetailsForDoctor> lstTeamMemberDetailsForDoctor = new List<TeamMemberDetailsForDoctor>();

        public List<PatientDetailsOfDoctor> lstPatientDetailsOfDoctor = new List<PatientDetailsOfDoctor>();
        public List<SearchColleagueForDoctor> lstSearchColleagueForDoctor = new List<SearchColleagueForDoctor>();
        public List<ColleaguesDetails> lstColleaguesDetails = new List<ColleaguesDetails>();
        public EducationandTraining clsEducationandTrainingForDoctorById { get; set; }
        public AddressDetails clsAddressDetailsById { get; set; }
        public SocialMediaForDoctor clsSocialMediaForDoctor { get; set; }
        public DataSet GetProfileDetailsOfDoctorByID(int UserId)
        {
            objColleaguesData = new clsColleaguesData();
            DataSet ds = new DataSet();


            return ds = objColleaguesData.GetDoctorDetailsById(UserId);
        }
        public DataTable GetPaymentHostoryOnUserId(int userId)
        {
            objColleaguesData = new clsColleaguesData();

            return objColleaguesData.getPaymentHistoryOnUserId(userId);
        }

        #region Code For Address
        public List<AddressDetails> GetDoctorAddressDetailsById(int UserId)
        {
            try
            {
                lstDoctorAddressDetails = new List<AddressDetails>();
                objColleaguesData = new clsColleaguesData();
                DataSet ds = new DataSet();
                ds = objColleaguesData.GetDoctorDetailsById(UserId);
                if (ds != null && ds.Tables.Count > 0)
                {
                    //For Address details of Doctor

                    lstDoctorAddressDetails = (from p in ds.Tables[2].AsEnumerable()
                                               select new AddressDetails
                                               {
                                                   AddressInfoID = Convert.ToInt32(p["AddressInfoID"]),
                                                   Location = objCommon.CheckNull(Convert.ToString(p["LocationName"]), string.Empty),
                                                   ExactAddress = objCommon.CheckNull(Convert.ToString(p["ExactAddress"]), string.Empty),
                                                   Address2 = objCommon.CheckNull(Convert.ToString(p["Address2"]), string.Empty),
                                                   City = objCommon.CheckNull(Convert.ToString(p["City"]), string.Empty),
                                                   State = objCommon.CheckNull(Convert.ToString(p["State"]), string.Empty),
                                                   Country = objCommon.CheckNull(Convert.ToString(p["Country"]), string.Empty),
                                                   ZipCode = objCommon.CheckNull(Convert.ToString(p["ZipCode"]), string.Empty),
                                                   Phone = objCommon.CheckNull(Convert.ToString(p["Phone"]), string.Empty),
                                                   Fax = objCommon.CheckNull(Convert.ToString(p["Fax"]), string.Empty),
                                                   EmailAddress = objCommon.CheckNull(Convert.ToString(p["EmailAddress"]), string.Empty),
                                                   ContactType = Convert.ToInt32(p["ContactType"]),
                                                   Mobile = objCommon.CheckNull(Convert.ToString(p["Mobile"]), string.Empty)
                                               }).ToList();

                }
            }
            catch (Exception)
            {

                throw;
            }

            return lstDoctorAddressDetails;
        }

        internal List<TimeZones> GetTimeZoneList()
        {
            List<TimeZones> list = new List<TimeZones>();
            DataTable dt = objColleaguesData.GetTimeZones();
            if (dt != null && dt.Rows.Count > 0)
            {
                list = (from p in dt.AsEnumerable()
                        select new TimeZones
                        {
                            TimeZoneId = Convert.ToInt32(p["TimeZoneId"]),
                            TimeZoneText = Convert.ToString(p["TimeZoneText"])
                        }).ToList();
            }
            return list;
        }

        public List<AddressDetails> GetDoctorAddressDetailsByAddressInfoID(int AddressInfoID)
        {
            lstDoctorAddressDetailsByAddressInfoID = new List<AddressDetails>();
            DataTable dt = new DataTable();
            dt = objColleaguesData.GetAddressDetailsByAddressInfoID(AddressInfoID);

            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    lstDoctorAddressDetailsByAddressInfoID = (from p in dt.AsEnumerable()
                                                              select new AddressDetails
                                                              {
                                                                  AddressInfoID = Convert.ToInt32(p["AddressInfoID"]),

                                                                  ExactAddress = objCommon.CheckNull(Convert.ToString(p["ExactAddress"]), string.Empty),
                                                                  Address2 = objCommon.CheckNull(Convert.ToString(p["Address2"]), string.Empty),
                                                                  City = objCommon.CheckNull(Convert.ToString(p["City"]), string.Empty),
                                                                  State = objCommon.CheckNull(Convert.ToString(p["State"]), string.Empty),
                                                                  Country = objCommon.CheckNull(Convert.ToString(p["Country"]), string.Empty),
                                                                  ZipCode = objCommon.CheckNull(Convert.ToString(p["ZipCode"]), string.Empty),
                                                                  EmailAddress = objCommon.CheckNull(Convert.ToString(p["EmailAddress"]), string.Empty),
                                                                  Phone = objCommon.CheckNull(Convert.ToString(p["Phone"]), string.Empty),
                                                                  Fax = objCommon.CheckNull(Convert.ToString(p["Fax"]), string.Empty),
                                                                  ContactType = Convert.ToInt32(p["ContactType"]),
                                                                  Location = objCommon.CheckNull(Convert.ToString(p["Location"]), string.Empty),
                                                                  TimeZoneId = Convert.ToInt32(p["TimeZoneId"]),
                                                                  Website = objCommon.CheckNull(Convert.ToString(p["Website"]), string.Empty),
                                                                  Mobile = objCommon.CheckNull(Convert.ToString(p["Mobile"]), string.Empty),
                                                                  ExternalPMSId = objCommon.CheckNull(Convert.ToString(p["ExternalPMSId"]), string.Empty),
                                                                  //Olivia changes on 10-04-2018 for Dr. Lan Allan's website. - DLADCETK-1
                                                                  SchedulingLink = objCommon.CheckNull(Convert.ToString(p["SchedulingLink"]), string.Empty)
                                                              }).ToList();//DD Changes

                    //foreach (DataRow row in dt.Rows)
                    //{
                    //    lstDoctorAddressDetailsByAddressInfoID.Add(new AddressDetails()
                    //    {
                    //        AddressInfoID = Convert.ToInt32(row["AddressInfoID"]),

                    //        ExactAddress = objCommon.CheckNull(Convert.ToString(row["ExactAddress"]), string.Empty),
                    //        Address2 = objCommon.CheckNull(Convert.ToString(row["Address2"]), string.Empty),
                    //        City = objCommon.CheckNull(Convert.ToString(row["City"]), string.Empty),
                    //        State = objCommon.CheckNull(Convert.ToString(row["State"]), string.Empty),
                    //        Country = objCommon.CheckNull(Convert.ToString(row["Country"]), string.Empty),
                    //        ZipCode = objCommon.CheckNull(Convert.ToString(row["ZipCode"]), string.Empty),
                    //        EmailAddress = objCommon.CheckNull(Convert.ToString(row["EmailAddress"]), string.Empty),
                    //        Phone = objCommon.CheckNull(Convert.ToString(row["Phone"]), string.Empty),
                    //        Fax = objCommon.CheckNull(Convert.ToString(row["Fax"]), string.Empty),
                    //        ContactType = Convert.ToInt32(row["ContactType"]),

                    //    });
                    //}
                }
            }
            catch (Exception)
            {

                throw;
            }



            return lstDoctorAddressDetailsByAddressInfoID;
        }

        internal string GetTimeZoneValue(int timezoneId)
        {
            return objColleaguesData.GetTimeZoneValue(timezoneId);
        }
        //Olivia changes on 10-04-2018 for Dr. Lan Allan's website. - DLADCETK-1
        public int UpdateAddressForDoctor(int AddressInfoID, string ExactAddress, string Address2, string City, string State, string Country, string Zipcode, string IsPrimary, string Email, string Phone, string Fax, string Location, int TimezoneId, string Mobile, string ExternalPMSId, string SchedulingLink, int? userid = 0)
        {//DD Changes
            int Result = 0;

            int MakePrimary = 1;//Change for address Info
            if (IsPrimary == "false")
            {
                MakePrimary = 2;
            }

            // If location left blank then city will set as location name.
            if (string.IsNullOrEmpty(Location))
            {
                Location = City;
            }
            if (userid == null || userid == 0)
            {
                int UserId = Convert.ToInt32(HttpContext.Current.Session["ParentUserId"]);
                Result = objColleaguesData.UpdateAddressDetailsByAddressInfoID(AddressInfoID, ExactAddress, Address2, City, State, Country, Zipcode, UserId, MakePrimary, Email, Phone, Fax, Location, TimezoneId, Mobile, ExternalPMSId, SchedulingLink);
            }
            else
            {
                Result = objColleaguesData.UpdateAddressDetailsByAddressInfoID(AddressInfoID, ExactAddress, Address2, City, State, Country, Zipcode, Convert.ToInt32(userid), MakePrimary, Email, Phone, Fax, Location, TimezoneId, Mobile, ExternalPMSId, SchedulingLink);
            }



            return Result;
        }

        public bool RemoveDoctorAddressDetailsByAddressInfoID(int Id)
        {
            bool Result = false;
            Result = objColleaguesData.RemoveDoctorAddressDetailsByAddressInfoID(Id);
            return Result;
        }
        #endregion

        #region Code For Social Media Details
        public List<SocialMediaForDoctor> GetSocialMediaDetailByUserId(int UserId)
        {
            try
            {
                #region Start New
                DataTable dt = new DataTable();
                lstGetSocialMediaDetailByUserId = new List<SocialMediaForDoctor>();
                dt = objColleaguesData.GetSocialMediaDetailByUserId(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {

                    foreach (DataRow row in dt.Rows)
                    {
                        lstGetSocialMediaDetailByUserId.Add(new SocialMediaForDoctor()
                        {
                            FacebookUrl = objCommon.CheckNull(Convert.ToString(row["FacebookUrl"]), string.Empty) != string.Empty ? (Convert.ToString(row["FacebookUrl"]).Contains("http://") || Convert.ToString(row["FacebookUrl"]).Contains("https://") ? Convert.ToString(row["FacebookUrl"]) : "http://" + Convert.ToString(row["FacebookUrl"])) : Convert.ToString(row["FacebookUrl"]),
                            LinkedinUrl = objCommon.CheckNull(Convert.ToString(row["LinkedinUrl"]), string.Empty) != string.Empty ? (Convert.ToString(row["LinkedinUrl"]).Contains("http://") || Convert.ToString(row["LinkedinUrl"]).Contains("https://") ? Convert.ToString(row["LinkedinUrl"]) : "http://" + Convert.ToString(row["LinkedinUrl"])) : Convert.ToString(row["LinkedinUrl"]),
                            TwitterUrl = objCommon.CheckNull(Convert.ToString(row["TwitterUrl"]), string.Empty) != string.Empty ? (Convert.ToString(row["TwitterUrl"]).Contains("http://") || Convert.ToString(row["TwitterUrl"]).Contains("https://") ? Convert.ToString(row["TwitterUrl"]) : "http://" + Convert.ToString(row["TwitterUrl"])) : Convert.ToString(row["TwitterUrl"]),
                            GoogleplusUrl = objCommon.CheckNull(Convert.ToString(row["GoogleplusUrl"]), string.Empty) != string.Empty ? (Convert.ToString(row["GoogleplusUrl"]).Contains("http://") || Convert.ToString(row["GoogleplusUrl"]).Contains("https://") ? Convert.ToString(row["GoogleplusUrl"]) : "http://" + Convert.ToString(row["GoogleplusUrl"])) : Convert.ToString(row["GoogleplusUrl"]),
                            YoutubeUrl = objCommon.CheckNull(Convert.ToString(row["YoutubeUrl"]), string.Empty) != string.Empty ? (Convert.ToString(row["YoutubeUrl"]).Contains("http://") || Convert.ToString(row["YoutubeUrl"]).Contains("https://") ? Convert.ToString(row["YoutubeUrl"]) : "http://" + Convert.ToString(row["YoutubeUrl"])) : Convert.ToString(row["YoutubeUrl"]),
                            PinterestUrl = objCommon.CheckNull(Convert.ToString(row["PinterestUrl"]), string.Empty) != string.Empty ? (Convert.ToString(row["PinterestUrl"]).Contains("http://") || Convert.ToString(row["PinterestUrl"]).Contains("https://") ? Convert.ToString(row["PinterestUrl"]) : "http://" + Convert.ToString(row["PinterestUrl"])) : Convert.ToString(row["PinterestUrl"]),
                            BlogUrl = objCommon.CheckNull(Convert.ToString(row["BlogUrl"]), string.Empty) != string.Empty ? (Convert.ToString(row["BlogUrl"]).Contains("http://") || Convert.ToString(row["BlogUrl"]).Contains("https://") ? Convert.ToString(row["BlogUrl"]) : "http://" + Convert.ToString(row["BlogUrl"])) : Convert.ToString(row["BlogUrl"]),
                            YelpUrl = objCommon.CheckNull(Convert.ToString(row["YelpUrl"]), string.Empty) != string.Empty ? (Convert.ToString(row["YelpUrl"]).Contains("http://") || Convert.ToString(row["YelpUrl"]).Contains("https://") ? Convert.ToString(row["YelpUrl"]) : "http://" + Convert.ToString(row["YelpUrl"])) : Convert.ToString(row["YelpUrl"]),
                            InstagramUrl = objCommon.CheckNull(Convert.ToString(row["InstagramUrl"]), string.Empty) != string.Empty ? (Convert.ToString(row["InstagramUrl"]).Contains("http://") || Convert.ToString(row["InstagramUrl"]).Contains("https://") ? Convert.ToString(row["InstagramUrl"]) : "http://" + Convert.ToString(row["InstagramUrl"])) : Convert.ToString(row["InstagramUrl"]),//RM-355
                        });
                    }
                }
                #endregion
            }
            catch (Exception)
            {

                throw;
            }

            return lstGetSocialMediaDetailByUserId;
        }
        public bool UpdateSocialMediaForDoctor(int UserId, string FacebookUrl, string LinkedinUrl, string TwitterUrl,string InstagramUrl, string GoogleplusUrl, string YoutubeUrl, string PinterestUrl, string BlogUrl, string YelpUrl)
        {
            bool Result = false;
            Result = objColleaguesData.UpdateSocialMediaDetailsByUserId(UserId, FacebookUrl, LinkedinUrl, TwitterUrl, InstagramUrl, GoogleplusUrl, YoutubeUrl, PinterestUrl, BlogUrl, YelpUrl);
            return Result;
        }
        #endregion

        #region Code For Education and Training



        public List<EducationandTraining> GetEducationandTrainingDetailsForDoctor(int UserId)
        {
            try
            {

                DataTable dt = new DataTable();
                lstEducationandTraining = new List<EducationandTraining>();
                dt = objColleaguesData.GetEducationTrainingByUserId(UserId);

                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[i]["Institute"])))
                        {
                            lstEducationandTraining.Add(new EducationandTraining { Id = Convert.ToInt32(dt.Rows[i]["Id"]), Institute = objCommon.CheckNull(dt.Rows[i]["Institute"].ToString(), string.Empty), Specialisation = objCommon.CheckNull(dt.Rows[i]["Specialisation"].ToString(), string.Empty), YearAttended = objCommon.CheckNull(dt.Rows[i]["YearAttended"].ToString(), string.Empty) });
                        }
                    }
                }

                //if (dt != null && dt.Rows.Count > 0)
                //{
                //    if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[0]["Institute"])) )
                //    {
                //        lstEducationandTraining = (from p in dt.AsEnumerable()
                //                                   select new EducationandTraining
                //                                   {
                //                                       Id = Convert.ToInt32(p["id"]),
                //                                       Institute = objCommon.CheckNull(Convert.ToString(p["Institute"]), string.Empty),
                //                                       Specialisation = objCommon.CheckNull(Convert.ToString(p["Specialisation"]), string.Empty),
                //                                       YearAttended = objCommon.CheckNull(Convert.ToString(p["YearAttended"]), string.Empty),
                //                                   }).ToList();


                //    }

                //}
            }
            catch (Exception)
            {

                throw;
            }
            return lstEducationandTraining;
        }
        public List<EducationandTraining> GetEducationandTrainingDetailsForDoctorById(int Id)
        {
            try
            {

                DataTable dt = new DataTable();
                lstEducationandTrainingForDoctorById = new List<EducationandTraining>();
                dt = objColleaguesData.GetEducationTrainingByUniqueId(Id);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {

                        lstEducationandTrainingForDoctorById.Add(new EducationandTraining()
                        {
                            Id = Convert.ToInt32(row["id"]),
                            Institute = objCommon.CheckNull(Convert.ToString(row["Institute"]), string.Empty),
                            Specialisation = objCommon.CheckNull(Convert.ToString(row["Specialisation"]), string.Empty),
                            YearAttended = objCommon.CheckNull(Convert.ToString(row["YearAttended"]), string.Empty),

                        });
                    }
                }
                else
                {
                    lstEducationandTrainingForDoctorById.Add(new EducationandTraining()
                    {
                        Id = Convert.ToInt32(0),
                        Institute = string.Empty,
                        Specialisation = string.Empty,
                        YearAttended = string.Empty,

                    });
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstEducationandTrainingForDoctorById;
        }
        public bool EducationAndTrainingInsertAndUpdate(int Id, int UserId, string Institute, string Specialisation, string YearAttended)
        {
            bool Result = false;
            Result = objColleaguesData.EducationAndTrainingInsertAndUpdate(Id, UserId, Institute, Specialisation, YearAttended);
            return Result;
        }

        public bool RemoveEducationAndTraining(int Id)
        {
            bool Result = false;
            Result = objColleaguesData.RemoveEducationTrainingByUniqueId(Id);
            return Result;
        }
        #endregion

        #region GetSecondaryWebsitelist
        public List<WebsiteDetails> GetSecondaryWebsitelistByUserId(int UserId)
        {

            try
            {
                DataTable dt = new DataTable();
                lstsecondarywebsitelist = new List<WebsiteDetails>();
                dt = objColleaguesData.GetSecondaryWebsiteDetailsbyId(UserId);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstsecondarywebsitelist.Add(new WebsiteDetails()
                        {
                            SecondaryWebsiteId = Convert.ToInt32(row["SecondaryId"]),
                            SecondaryWebsiteurl = objCommon.CheckNull(Convert.ToString(row["WebSite"]), string.Empty),
                        });
                    }
                }
                return lstsecondarywebsitelist;

            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Code For Professional Memberships


        public List<ProfessionalMemberships> GetProfessionalMembershipsByUserId(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                lstProfessionalMemberships = new List<ProfessionalMemberships>();
                dt = objColleaguesData.GetProfessionalMembershipsByUserId(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[i]["Membership"])))
                        {
                            lstProfessionalMemberships.Add(new ProfessionalMemberships { Id = Convert.ToInt32(dt.Rows[i]["Id"]), Membership = objCommon.CheckNull(Convert.ToString(dt.Rows[i]["Membership"]), string.Empty), });
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }


            return lstProfessionalMemberships;
        }
        public List<ProfessionalMemberships> GetProfessionalMembershipsByUniqueId(int Id)
        {
            try
            {
                DataTable dt = new DataTable();
                lstProfessionalMembershipForDoctorById = new List<ProfessionalMemberships>();
                dt = objColleaguesData.GetProfessionalMembershipsByUniqueId(Id);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstProfessionalMembershipForDoctorById.Add(new ProfessionalMemberships()
                        {
                            Id = Convert.ToInt32(row["Id"]),
                            Membership = objCommon.CheckNull(Convert.ToString(row["Membership"]), string.Empty),
                        });
                    }

                }
            }
            catch (Exception)
            {

                throw;
            }


            return lstProfessionalMembershipForDoctorById;
        }
        public bool ProfessionalMembershipsInsertAndUpdate(int Id, int UserId, string Membership)
        {
            bool Result = false;
            Result = objColleaguesData.ProfessionalMembershipsInsertAndUpdate(Id, UserId, Membership);
            return Result;
        }
        public bool RemoveProfessionalMemberships(int Id)
        {
            bool Result = false;
            Result = objColleaguesData.RemoveProfessionalMembershipsByUniqueId(Id);
            return Result;
        }
        #endregion
        public List<LicenseDetailsForDoctor> GetLicensedetailsByUserId(int Userid, string TimeZoneSystemName)
        {
            try
            {
                licensedetails = new List<LicenseDetailsForDoctor>();
                objColleaguesData = new clsColleaguesData();
                DataTable dt = new DataTable();
                dt = objColleaguesData.GetLicenseDetialsByUserId(Userid);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["LicenseExpiration"] != DBNull.Value)
                    {
                        DateTime LicenseExpiration = Convert.ToDateTime(dt.Rows[0]["LicenseExpiration"]);
                        LicenseExpiration = clsHelper.ConvertFromUTC(LicenseExpiration, TimeZoneSystemName);
                        licensedetails.Add(new LicenseDetailsForDoctor()
                        {

                            UserId = Convert.ToInt32(dt.Rows[0]["UserId"]),
                            LicenseNumber = Convert.ToString(dt.Rows[0]["LicenseNumber"]),
                            LicenseState = Convert.ToString(dt.Rows[0]["LicenseState"]),
                            LicenseExpiration = LicenseExpiration,
                            LicenseExpirationDisplay = Convert.ToDateTime(dt.Rows[0]["LicenseExpiration"]).ToString("MM/dd/yyyy"),

                        });
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return licensedetails;
        }
        public List<NewMembership> GetMembershipPlan(int MembershipId)
        {
            try
            {
                DataTable dt = new DataTable();
                List<NewMembership> lstmember = new List<NewMembership>();
                dt = objColleaguesData.GetUpgreadeMembership(MembershipId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lstmember.Add(new NewMembership()
                        {
                            Membership_Id = Convert.ToInt32(item["Membership_Id"]),
                            Title = Convert.ToString(item["Title"]),
                            MonthlyPrice = Convert.ToDecimal(item["MonthlyPrice"]),
                            AnnualPrice = Convert.ToDecimal(item["AnnualPrice"]),
                            QuaterlyPrice = Convert.ToDecimal(item["QuaterlyPrice"]),
                            HalfYearlyPrice = Convert.ToDecimal(item["HalfYearlyPrice"]),
                        });
                    }
                }
                return lstmember;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }
        public LicenseDetailsForDoctor LicensedetailsByUserId(int Userid)
        {
            try
            {
                objLicense = new LicenseDetailsForDoctor();
                objColleaguesData = new clsColleaguesData();
                DataTable dt = new DataTable();
                dt = objColleaguesData.GetLicenseDetialsByUserId(Userid);
                if (dt != null && dt.Rows.Count > 0)
                {
                    objLicense.UserId = Convert.ToInt32(dt.Rows[0]["UserId"]);
                    objLicense.LicenseNumber = Convert.ToString(dt.Rows[0]["LicenseNumber"]);
                    objLicense.LicenseState = Convert.ToString(dt.Rows[0]["LicenseState"]);
                    if (dt.Rows[0]["LicenseExpiration"] != DBNull.Value)
                    {
                        //objLicense.LicenseExpirationDisplay = Convert.ToDateTime(dt.Rows[0]["LicenseExpiration"]).ToString("MM/dd/yyyy");
                        objLicense.LicenseExpirationDisplay = new CommonBLL().ConvertToDate(Convert.ToDateTime(dt.Rows[0]["LicenseExpiration"]), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL));
                    }


                }
            }
            catch (Exception ex)
            {

                throw;
            }
            return objLicense;
        }

        #region Code For Description Pop

        public string GetGetDescriptionForDoctor(int UserId)
        {
            string DescriptionData = "";
            try
            {

                DataSet ds = new DataSet();
                ds = GetProfileDetailsOfDoctorByID(UserId);

                if (ds != null && ds.Tables.Count > 0)
                {
                    DescriptionData = objCommon.CheckNull((Convert.ToString(ds.Tables[0].Rows[0]["Description"])), string.Empty);
                }
            }
            catch (Exception)
            {

                throw;
            }

            return DescriptionData;
        }
        #endregion

        #region Code For Team member
        public bool TeamMemberInsertAndUpdate(int UserId, int ParentUserId, string Email, string FirstName, string LastName, string OfficeName, string ExactAddress, string Address2, string City, string State, string Country, string ZipCode, string Specialty, int LocationId, string ProviderId, bool provtype)
        {
            bool Result = false;
            string Password = objCommon.CreateRandomPassword(8);
            Password = ObjCryptoHelper.encryptText(Password);


            Result = objColleaguesData.TeamMemberInsertAndUpdate(UserId, ParentUserId, Email, FirstName, LastName, OfficeName, ExactAddress, Address2, City, State, Country, ZipCode, Specialty, Password, ProviderId, LocationId, provtype);
            return Result;
        }
        public List<TeamMemberDetailsForDoctor> GetTeamMemberDetailsOfDoctor(int ParentUserId, int PageIndex, int PageSize)
        {
            try
            {
                int UserId = Convert.ToInt32(HttpContext.Current.Session["UserId"]);
                DataTable dt = new DataTable();
                lstTeamMemberDetailsForDoctor = new List<TeamMemberDetailsForDoctor>();
                dt = objColleaguesData.GetTeamMemberDetailsOfDoctor(ParentUserId, PageIndex, PageSize);
                if (dt != null && dt.Rows.Count > 0)
                {
                    lstTeamMemberDetailsForDoctor = (from p in dt.AsEnumerable()
                                                     select new TeamMemberDetailsForDoctor
                                                     {
                                                         UserId = Convert.ToInt32(p["UserId"]),
                                                         FirstName = objCommon.CheckNull(Convert.ToString(p["FirstName"]), string.Empty),
                                                         LastName = objCommon.CheckNull(Convert.ToString(p["LastName"]), string.Empty),
                                                     }).Where(x => x.UserId != UserId).ToList();


                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstTeamMemberDetailsForDoctor;
        }

        public bool RemoveTeamMemberOfDoctor(int UserId, int ParentUserId)
        {
            bool Result = false;
            Result = objColleaguesData.RemoveTeamMemberOfDoctor(UserId, ParentUserId);
            return Result;
        }
        #endregion

        #region Code For Banner
        public List<Banner> GetBannerDetailByUserId(int UserId)
        {

            try
            {
                DataTable dt = new DataTable();
                lstBanner = new List<Banner>();
                dt = objColleaguesData.GetBannerDetailByUserId(UserId);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstBanner.Add(new Banner()
                        {
                            BannerId = Convert.ToInt32(row["BannerId"]),
                            UserId = Convert.ToInt32(row["UserId"]),
                            Title = objCommon.CheckNull(Convert.ToString(row["Title"]), string.Empty),
                            Path = objCommon.CheckNull(Convert.ToString(row["Path"]), string.Empty),
                            ColorCode = objCommon.CheckNull(Convert.ToString(row["ColorCode"]), string.Empty),
                            ImagePath = ConfigurationManager.AppSettings["BannerImage"].Replace('~', ' ')
                        });
                    }
                }
                return lstBanner;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public Banner GetBannerDetailById(int BannerId)
        {
            try
            {
                DataTable dt = new DataTable();
                Banner banner = new Banner();
                dt = objColleaguesData.GetBannerDetailById(BannerId);
                if (dt.Rows.Count > 0)
                {
                    banner.BannerId = Convert.ToInt32(dt.Rows[0]["BannerId"]);
                    banner.UserId = Convert.ToInt32(dt.Rows[0]["UserId"]);
                    banner.Title = objCommon.CheckNull(Convert.ToString(dt.Rows[0]["Title"]), string.Empty);
                    banner.Path = objCommon.CheckNull(Convert.ToString(dt.Rows[0]["Path"]), string.Empty);
                    banner.ColorCode = objCommon.CheckNull(Convert.ToString(dt.Rows[0]["ColorCode"]), string.Empty);
                }
                return banner;

            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        public List<SpeacilitiesOfDoctor> GetSpeacilitiesOfDoctorById(int UserId)
        {
            DataTable dtSpeacilities = new DataTable();
            dtSpeacilities = objColleaguesData.GetMemberSpecialities(UserId);
            List<SpeacilitiesOfDoctor> lstSpeacilities = new List<SpeacilitiesOfDoctor>();
            if (dtSpeacilities.Rows.Count > 0)
            {
                lstSpeacilities = (from p in dtSpeacilities.AsEnumerable()
                                   select new SpeacilitiesOfDoctor
                                   {
                                       SpecialtyId = int.Parse(objCommon.CheckNull(p["SpecialityId"].ToString(), "0")),
                                       SpecialtyDescription = objCommon.CheckNull(p["Specialities"].ToString(), ""),
                                   }).ToList();


            }

            return lstSpeacilities;

        }
        public List<LocationDetails> GetLocationDetailsByDoctorId(int UserId)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = objColleaguesData.GetDoctorLocaitonById(UserId);
                List<LocationDetails> lst = new List<LocationDetails>();
                if (dt.Rows.Count > 0)
                {
                    lst = (from p in dt.AsEnumerable()
                           select new LocationDetails
                           {
                               LocationName = Convert.ToString(p["Location"]),
                               AddressInfoId = Convert.ToInt32(p["AddressInfoId"])

                           }).ToList();
                }
                return lst;
            }
            catch (Exception)
            {

                throw;
            }

        }

        public List<NewMembership> GetMembershipById(int MembershipId)
        {
            try
            {
                DataTable dt = new DataTable();
                List<NewMembership> lstmember = new List<NewMembership>();
                dt = objColleaguesData.GetUpgradeMembershipById(MembershipId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        lstmember.Add(new NewMembership()
                        {
                            Membership_Id = Convert.ToInt32(item["Membership_Id"]),
                            Title = Convert.ToString(item["Title"]),
                            MonthlyPrice = Convert.ToInt32(item["MonthlyPrice"]),
                            AnnualPrice = Convert.ToInt32(item["AnnualPrice"]),
                            QuaterlyPrice = Convert.ToInt32(item["QuaterlyPrice"]),
                            HalfYearlyPrice = Convert.ToInt32(item["HalfYearlyPrice"]),
                        });
                    }
                }
                return lstmember;
            }
            catch (Exception Ex)
            {

                throw;
            }
        }
        public List<Procedure> GetUserProcedureList(int UserId)
        {
            List<Procedure> lst = new List<Procedure>();
            lst = ProcedureBLL.GetProcedureList(0, UserId);
            return lst;
        }
        public LicenseDetailsForDoctor NewGetLicensedetailsByUserId(int Userid, string TimeZoneSystemName)
        {
            try
            {
                objLicense = new LicenseDetailsForDoctor();
                objColleaguesData = new clsColleaguesData();
                DataTable dt = new DataTable();
                dt = objColleaguesData.GetLicenseDetialsByUserId(Userid);
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["LicenseExpiration"] != DBNull.Value)
                    {
                        DateTime LicenseExpiration = Convert.ToDateTime(dt.Rows[0]["LicenseExpiration"]);
                        LicenseExpiration = clsHelper.ConvertFromUTC(LicenseExpiration, TimeZoneSystemName);
                        UserId = Convert.ToInt32(dt.Rows[0]["UserId"]);
                        objLicense.LicenseNumber = Convert.ToString(dt.Rows[0]["LicenseNumber"]);
                        objLicense.LicenseState = Convert.ToString(dt.Rows[0]["LicenseState"]);
                        objLicense.LicenseExpiration = LicenseExpiration;
                        objLicense.LicenseExpirationDisplay = Convert.ToDateTime(dt.Rows[0]["LicenseExpiration"]).ToString("MM/dd/yyyy");
                    }

                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return objLicense;
        }
    }



    public class SpeacilitiesOfDoctor
    {
        public int SpecialtyId { set; get; }
        public string SpecialtyDescription { set; get; }
    }

    public class EducationandTraining
    {
        public int Id { get; set; }
        public string Institute { get; set; }
        public string Specialisation { get; set; }
        public string YearAttended { get; set; }
    }
    public class ProfessionalMemberships
    {
        public int Id { get; set; }
        public string Membership { get; set; }
    }
    public class AddressDetails
    {
        public int AddressInfoID { get; set; }
        public string ExactAddress { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string EmailAddress { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public int ContactType { get; set; }
        public string Location { get; set; }
        public int TimeZoneId { get; set; }
        public string Website { get; set; }
        public string Mobile { get; set; }
        public string ExternalPMSId { get; set; }
        //Olivia changes on 10-04-2018 for Dr. Lan Allan's website. - DLADCETK-1
        public string SchedulingLink { get; set; }
    }

    public class SocialMediaForDoctor
    {

        public string FacebookUrl { get; set; }
        public string LinkedinUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string GoogleplusUrl { get; set; }
        public string YoutubeUrl { get; set; }
        public string PinterestUrl { get; set; }
        public string BlogUrl { get; set; }
        public string InstagramUrl { get; set; }
        public string YelpUrl { get; set; }

    }

    public class TeamMemberDetailsForDoctor
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SpecialtyDescription { get; set; }
        public string PublicProfileUrl { get; set; }
        public string AccountName { get; set; }

    }
    public class LicenseDetailsForDoctor
    {

        public int UserId { get; set; }
        public string LicenseNumber { get; set; }
        public string LicenseState { get; set; }
        public DateTime? LicenseExpiration { get; set; }
        public string LicenseExpirationDisplay { get; set; }


    }
    public class WebsiteDetails
    {
        public int SecondaryWebsiteId { get; set; }
        public int UserId { get; set; }
        public string SecondaryWebsiteurl { get; set; }
    }

    public class LocationDetails
    {
        public string LocationName { get; set; }
        public int AddressInfoId { get; set; }
    }
    public class SectionPublicProfile
    {
        public int Userid { get; set; }
        public bool PatientForms { get; set; }
        public bool SpecialOffers { get; set; }
        public bool ReferPatient { get; set; }
        public bool Reviews { get; set; }
        public bool AppointmentBooking { get; set; }
        public bool PatientLogin { get; set; }
    }
    //public class Banner
    //{

    //    public int BannerId { get; set; }
    //    public int UserId { get; set; }
    //    [Display(Name ="Title")]
    //    [Required(ErrorMessage = "Please enter title")]
    //    public string Title { get; set; }
    //    [Display(Name = "Banner Image")]
    //    [Required(ErrorMessage = "Please choose image")]
    //    public string Path { get; set; }
    //    public string ImagePath { get; set; }
    //    public HttpPostedFileBase BannerImage { get; set; }
    //    [Display(Name = "Color")]
    //    [Required(ErrorMessage = "Please choose color")]
    //    public string ColorCode { get; set; }
    //    public int Position { get; set; }        
    //}
}