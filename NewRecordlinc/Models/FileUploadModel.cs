﻿using BusinessLogicLayer;
using DataAccessLayer.Common;
using DataAccessLayer.PatientsData;
using NewRecordlinc.App_Start;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Web;

namespace NewRecordlinc.Models
{
    public class FileUploadModel
    {
        #region Global Declaration start
        public static string TempFile = System.Configuration.ConfigurationManager.AppSettings["TempFileUpload"];
        public static string ImageBankFiles = System.Configuration.ConfigurationManager.AppSettings["ImageBankFileUpload"];
        public static string ThumbFiles = System.Configuration.ConfigurationManager.AppSettings["ThumbFileUpload"];
        public static string DraftFiles = System.Configuration.ConfigurationManager.AppSettings["DraftFileUpload"];
        public static clsCommon ObjCommon = new clsCommon();
        #endregion

        /// <summary>
        /// Insert Image or files into temp folder
        /// </summary>
        /// <param name="files"></param>
        /// <param name="SesionId"></param>
        /// <param name="BrowserType"></param>
        /// <returns></returns>
        public static string TempFileUploads(HttpFileCollectionBase files, int UserId, string SesionId = "", string BrowserType = null, bool isFromReferral = false)
        {
            string UploadedFileIds = string.Empty;
            List<string> UploadedFile = new List<string>();
            try
            {
                List<FileUpload> lstFileUpload = new List<FileUpload>();
                if (UserId == 0)
                {
                    UserId = Convert.ToInt32(SessionManagement.UserId);
                }

                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFileBase file = files[i];
                    int RecordId = 0; string Filename = string.Empty; string FileExtension = string.Empty; string NewFileName = string.Empty, ShortFilename = string.Empty, ThumbUrl = string.Empty;
                    if (BrowserType == "IE" || BrowserType == "INTERNETEXPLORER")
                    {
                        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        Filename = testfiles[testfiles.Length - 1];
                    }
                    else
                    {
                        Filename = file.FileName;
                    }
                    Filename = Regex.Replace(Filename, @"[^0-9a-zA-Z\._]", string.Empty);
                    NewFileName = "$" + DateTime.Now.Ticks + "$" + Filename;
                    FileExtension = Path.GetExtension(Filename).ToLower();

                    if (!new CommonBLL().CheckFileIsValidImage(Filename))
                    {
                        RecordId = new CommonBLL().Insert_Temp_UploadedFiles(Convert.ToInt32(UserId), NewFileName, new CommonBLL().GetUniquenumberForAttachments());
                        UploadedFile.Add("F_" + RecordId);
                    }
                    else
                    {
                        RecordId = new CommonBLL().Insert_Temp_UploadedImages(Convert.ToInt32(UserId), NewFileName, new CommonBLL().GetUniquenumberForAttachments());
                        UploadedFile.Add("I_" + RecordId);
                    }

                    string UplodedFileType = string.Empty;
                    var extension = Path.GetExtension(Filename);
                    int FileFrom = 0;
                    if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd")
                    {
                        ThumbUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/Content/images/" + NewRecordlinc.Common.FileExtension.CheckFileExtenssionOnlyThumbName("unknown" + extension);
                        UplodedFileType = "Document";
                        FileFrom = 1;
                    }
                    else
                    {
                        ThumbUrl = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/" + "TempUploads/" + NewFileName;
                        UplodedFileType = "Image";
                        FileFrom = 2;
                    }
                    string delete_filename = HttpUtility.UrlEncode(NewFileName);
                    lstFileUpload.Add(new FileUpload()
                    {
                        progress = Convert.ToString(RecordId),
                        name = Filename,
                        shortfilename = ShortFilename,
                        size = file.ContentLength,
                        type = UplodedFileType,
                        thumbnail_url = ThumbUrl,
                        //delete_url = HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + "/PatientFileUpload.ashx?DeleteId=" + RecordId + "&FileExtension=" + extension.ToLower() + "&PatientId=" + PatientId,
                        delete_url = "/FileUpload/DeleteAttachedFile?FileId=" + RecordId + "&FileFrom="+ FileFrom + "&FileName=" + delete_filename,
                        delete_type = "DELETE"
                    });
                    NewFileName = Path.Combine(TempFile, NewFileName);



                    file.SaveAs(NewFileName);
                    if (Filename.Contains("$"))
                    {
                        ShortFilename = Convert.ToString(Filename.Split('$')[2]);
                    }
                    else
                    {
                        ShortFilename = Convert.ToString(Filename);
                    }


                }
                UploadedFileIds = string.Join(",", UploadedFile);
                if (isFromReferral)
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject(lstFileUpload);
                }
                else
                {
                    return UploadedFileIds;
                }

            }
            catch (Exception Ex)
            {
                return UploadedFileIds = "Error occurred. Error details: " + Ex.Message;
            }
        }

        /// <summary>
        /// Insert image or file into Image bank
        /// </summary>
        /// <param name="Flag"></param>
        /// <param name="context"></param>
        /// <param name="DoctorId"></param>
        /// <param name="path"></param>
        /// <param name="PatientId"></param>
        /// <param name="MessageId"></param>
        /// <returns></returns>
        public static IDictionary<string,bool> FileUploadOnImageBank(Fup_Request model)
        {
            try
            {
                // 1 - Only upload files in image bank
                // 2 - Get the image and files from the temp and add into Image 
                Dictionary<string,bool> return_result = new Dictionary<string, bool>();
                bool result = false;
                string strResult = string.Empty;
                switch (model.Request_Type)
                {
                    case BO.Enums.Common.FileUploadType.TempUpload:
                        strResult = TempFileUploads(model.files, model.UserId, model.SessionId, model.BrowserType, model.isFromReferral);
                        break;
                    case BO.Enums.Common.FileUploadType.ImageBankUpload:
                        result = UploadFiles(model.Context, model.DoctorId, model.PatientId);
                        break;
                    case BO.Enums.Common.FileUploadType.MoveToImageBank:
                        return_result = UploadFilesFromTemp(model.DoctorId, model.PatientId,model.isFromReferral);
                        break;
                    case BO.Enums.Common.FileUploadType.DeleteFromTempUpload:
                        result = RemoveTempFiles(model.FileId, model.FileFrom, model.FileName, model.ComposeType);
                        //RM-481 : User is not able to delete files from cummunication section.
                        strResult = (result) ? "true" : "false";
                        break;
                    default:
                        break;
                }
                if (return_result.Count == 0)
                {
                    return_result.Add(strResult, result);
                }
                return return_result;
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("FileUpload Model - FileUploadOnImageBank", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// Upload file into image bank when patient attach file from patient detail page
        /// </summary>
        /// <param name="context"></param>
        /// <param name="DoctorId"></param>
        /// <param name="PatientId"></param>
        /// <param name="NewMessageId"></param>
        /// <returns></returns>
        public static bool UploadFiles(HttpContext context, int DoctorId = 0, int PatientId = 0)
        {
            try
            {
                var lstFileUpload = new List<FileUploadModel>();
                string UploadfromInviteColleague = string.Empty; string FileName = string.Empty; string NewFileName = string.Empty;
                string extension = string.Empty;
                HttpPostedFile postedFile = context.Request.Files["Filedata"];
                if (context.Request.QueryString["InviteColleague"] != null && context.Request.QueryString["InviteColleague"] != "")
                {
                    UploadfromInviteColleague = Convert.ToString(context.Request.QueryString["InviteColleague"]);
                }
                if (!Directory.Exists(ImageBankFiles))
                {
                    Directory.CreateDirectory(ImageBankFiles);
                    SetPermissions(ImageBankFiles);
                }
                for (int filecount = 0; filecount < context.Request.Files.Count; filecount++)
                {
                    HttpPostedFile hpf = context.Request.Files[filecount] as HttpPostedFile;
                    if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
                    {
                        string[] files = hpf.FileName.Split(new char[] { '\\' });
                        FileName = files[files.Length - 1];
                    }
                    else
                    {
                        FileName = hpf.FileName;
                        string[] files = hpf.FileName.Split(new char[] { '\\' });
                        FileName = files[files.Length - 1];
                    }

                    if (FileName.Contains("/"))
                    {
                        string[] files = hpf.FileName.Split(new char[] { '/' });
                        FileName = files[files.Length - 1];
                    }
                    NewFileName = "$" + System.DateTime.Now.Ticks + "$" + FileName;
                    string filename1 = (ImageBankFiles + NewFileName);
                    extension = Path.GetExtension(FileName);
                    hpf.SaveAs(filename1);
                    if (extension.ToLower() != ".jpg" && extension.ToLower() != ".jpeg" && extension.ToLower() != ".gif" && extension.ToLower() != ".png" && extension.ToLower() != ".bmp" && extension.ToLower() != ".psd")
                    {
                        int docid = new clsPatientsData().UploadPatientDocument(0, NewFileName, "Doctor", PatientId, DoctorId);
                    }
                    else
                    {
                        // Add image to Image Table
                        int imageid = new clsPatientsData().AddImagetoPatient(PatientId, 1, NewFileName, 1000, 1000, 0, 0, ImageBankFiles, filename1, DoctorId, 0, 1, "Doctor");
                        if (!Directory.Exists(ThumbFiles))
                        {
                            Directory.CreateDirectory(ThumbFiles);
                            SetPermissions(ThumbFiles);
                        }

                        // Load image.
                        Image image = Image.FromFile(filename1);

                        // Compute thumbnail size.
                        Size thumbnailSize = ObjCommon.GetThumbnailSize(image);

                        // Get thumbnail.
                        Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                            thumbnailSize.Height, null, IntPtr.Zero);

                        // Save thumbnail.
                        thumbnail.Save(ThumbFiles + NewFileName);
                    }

                    //Here is the code for send file upload notification to doctor which one is associated with that patient.
                    clsPatientsData ObjPatientsData = new clsPatientsData();
                    DataTable dt = ObjPatientsData.GetSelectedTeamMemberForPatientHistory(PatientId, DoctorId);
                    bool result = BusinessLogicLayer.CommonBLL.SendFileUploadNotification(dt, PatientId, DoctorId);
                }
                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        /// <summary>
        /// Transfer file from temp folder to image when click on send communication
        /// </summary>
        /// <param name="DoctorId"></param>
        /// <param name="PatientId"></param>
        /// <param name="NewMessageId"></param>
        /// <returns></returns>
        public static Dictionary<string,bool> UploadFilesFromTemp(int DoctorId = 0, int PatientId = 0,bool IsReferralForm = false)
        {
            try
            {
                Dictionary<string, bool> return_result = new Dictionary<string, bool>();
                string NewFileName = string.Empty;
                Dictionary<string, bool> referral_result = new Dictionary<string, bool>();
                DataTable dtTempUploadedDocs = ObjCommon.GetRecordsFromTempTablesForDoctor(DoctorId, clsCommon.GetUniquenumberForAttachments());
                if (dtTempUploadedDocs != null && dtTempUploadedDocs.Rows.Count > 0)
                {

                    foreach (DataRow itemTempUploadedDocs in dtTempUploadedDocs.Rows)
                    {
                        clsPatientsData ObjPatientsData = new clsPatientsData();
                        string DocName = Convert.ToString(itemTempUploadedDocs["DocumentName"]);
                        NewFileName = Convert.ToString("$" + System.DateTime.Now.Ticks + "$" + DocName.Substring(DocName.ToString().LastIndexOf("$") + 1));
                        string Extension = Path.GetExtension(NewFileName);
                        string sourceFile = TempFile + DocName;
                        string destinationFile = ImageBankFiles + NewFileName;
                        System.IO.File.Copy(sourceFile, destinationFile);
                        if (Extension.ToLower() == ".jpg" || Extension.ToLower() == ".jpeg" || Extension.ToLower() == ".gif" || Extension.ToLower() == ".png" || Extension.ToLower() == ".bmp" || Extension.ToLower() == ".psd" || Extension.ToLower() == ".pspimage" || Extension.ToLower() == ".thm" || Extension.ToLower() == ".tif")
                        {
                            if (!Directory.Exists(ThumbFiles))
                            {
                                Directory.CreateDirectory(ThumbFiles);
                                SetPermissions(ThumbFiles);
                            }

                            // Load image.
                            Image image = Image.FromFile(destinationFile);

                            // Compute thumbnail size.
                            Size thumbnailSize = ObjCommon.GetThumbnailSize(image);

                            // Get thumbnail.
                            Image thumbnail = image.GetThumbnailImage(thumbnailSize.Width,
                                thumbnailSize.Height, null, IntPtr.Zero);

                            // Save thumbnail.
                            thumbnail.Save(ThumbFiles + NewFileName);

                            // Add the data in image table
                            // In referral Result : 0- Image, 1- Document
                            int imageid = ObjPatientsData.AddImagetoPatient(PatientId, 1, NewFileName, 1000, 1000, 0, 0, ImageBankFiles, NewFileName, DoctorId, 0, 1, "Doctor");
                            referral_result.Add(Convert.ToString(imageid), true);
                        }
                        else
                        {
                            int RecordId = ObjPatientsData.UploadPatientDocument(0, NewFileName, "Doctor", PatientId, DoctorId);
                            referral_result.Add(Convert.ToString(RecordId),true);
                        }
                        if (System.IO.File.Exists(sourceFile))
                        {
                            System.IO.File.Delete(sourceFile);
                        }
                    }
                }

                bool Result = ObjCommon.Temp_RemoveAllByUserId(DoctorId, clsCommon.GetUniquenumberForAttachments()); // For Remove All Uplaoded Temp Images and Files
                return_result.Add( NewFileName, Result);
                if (IsReferralForm)
                {
                    return referral_result;
                }
                else
                {
                    return return_result;
                }
            }
            catch (Exception ex)
            {
                new clsCommon().InsertErrorLog("FileUpload Model - UploadFilesFromTemp", ex.Message, ex.StackTrace);
                throw ex;
            }
        }

        /// <summary>
        /// Remove the file from compose message
        /// </summary>
        /// <param name="FileId"></param>
        /// <param name="FileFrom"></param>
        /// <param name="FileName"></param>
        /// <param name="ComposeType"></param>
        /// <returns></returns>
        public static bool RemoveTempFiles(int FileId, int FileFrom, string FileName, int ComposeType = 0)
        {
            try
            {
                int formid = 0;
                CommonBLL ObjCommonbll = new CommonBLL();
                bool Status = false;
                if (!string.IsNullOrWhiteSpace(FileName) && FileId > 0 && FileFrom > 0)
                {
                    string FilePath = string.Empty;
                    switch (FileFrom)
                    {
                        case (int)BO.Enums.Common.FileFromFolder.TempFile_TempFolder:
                            FilePath = TempFile + FileName;
                            formid = 1;
                            break;
                        case (int)BO.Enums.Common.FileFromFolder.TempImage_TempFolder:
                            FilePath = TempFile + FileName;
                            formid = 2;
                            break;
                        case (int)BO.Enums.Common.FileFromFolder.Draft_DraftAttachments:
                            FilePath = DraftFiles + FileName;
                            formid = 3;
                            break;
                        default:
                            break;
                    }
                    if (ObjCommonbll.Temp_AttachedFileDeleteById(FileId, formid, ComposeType))
                    {
                        Status = true;
                        if (System.IO.File.Exists(FilePath))
                            System.IO.File.Delete(FilePath);
                    }
                }
                return Status;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Delete the image from User_UploadedImages
        /// </summary>
        /// <param name="ImageId"></param>
        /// <returns></returns>
        public static bool DeleteImage(int ImageId)
        {
            try
            {
                clsPatientsData objclsPatients = new clsPatientsData();
                return objclsPatients.RemovePatientImage(ImageId, 0);
            }
            catch (Exception)
            {

                throw;
            }
        }

        /// <summary>
        /// Delete the image from User_UploadedFiles
        /// </summary>
        /// <param name="ImageId"></param>
        /// <returns></returns>
        public static bool RemoveDocument(int DocumentId)
        {
            try
            {
                clsPatientsData ObjPatientsData = new clsPatientsData();
                bool result = ObjPatientsData.RemovePatientDocumentById(DocumentId);
                return result;

            }
            catch (Exception)
            {

                throw;
            }
        }

        public static void SetPermissions(string savepath)
        {
            try
            {
                DirectoryInfo info = new DirectoryInfo(savepath);
                WindowsIdentity self = System.Security.Principal.WindowsIdentity.GetCurrent();
                DirectorySecurity ds = info.GetAccessControl();
                ds.AddAccessRule(new FileSystemAccessRule(self.Name,
                FileSystemRights.FullControl,
                InheritanceFlags.ObjectInherit |
                InheritanceFlags.ContainerInherit,
                PropagationFlags.None,
                AccessControlType.Allow));
                info.SetAccessControl(ds);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void ReturnOptions(HttpContext context)
        {
            context.Response.StatusCode = 200;
            context.Response.AddHeader("Allow", "DELETE,GET,HEAD,POST,PUT,OPTIONS");
        }
    }

    public class Fup_Request
    {
        public BO.Enums.Common.FileUploadType Request_Type { get; set; }
        public HttpFileCollectionBase files { get; set; }
        public HttpContext Context { get; set; }
        public int DoctorId { get; set; }
        public int PatientId { get; set; }
        public string SessionId { get; set; }
        public string BrowserType { get; set; }
        public int FileId { get; set; }
        public int FileFrom { get; set; }
        public string FileName { get; set; }
        public int ComposeType { get; set; }
        public int UserId { get; set; }
        public bool isFromReferral { get; set; }
    }
}
