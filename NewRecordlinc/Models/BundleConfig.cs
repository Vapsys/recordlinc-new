﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace NewRecordlinc.Models
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection Bundle)
        {

            Bundle.Add(new StyleBundle("~/StyleBundle").Include(
                         "~/content/css/recordlinc.css",
                         "~/content/css/media-query.css",
                        "~/content/css/menu.css",
                         "~/content/css/banner.css",
                         "~/content/css/accordian.css"
                        ));

            Bundle.Add(new ScriptBundle("~/ScriptBundle").Include(
                        "~/content/js/jquery-1.8.3.min.js",
                        "~/content/js/html5.js",
                        "~/content/js/scroll_top.js",
                        "~/Content/js/Common.js",
                        "~/content/js/menu.js",
                        "~/content/js/modernizr.custom.28468.js",
                        "~/content/js/jquery.cslider.js",
                        "~/Content/js/accordianoutter.js",
                        "~/content/js/jquery.ui.core.js",
                        "~/content/js/jquery.ui.widget.js",
                        "~/content/js/jquery.ui.mask.js"
                        ));

            Bundle.Add(new StyleBundle("~/NewStyleBundle").Include(
                       "~/Content/css/select2.min.css",
                       "~/Content/Appointment/Css/bootstrap.css",
                       "~/Content/css/fcbk.css",
                       "~/Content/css/DatePicker.css",
                       "~/Content/JCrop/jquery.Jcrop.css",
                      "~/Content/css/PatientHistory.css"
                      ));

            Bundle.Add(new ScriptBundle("~/NewScriptBundle").Include(
                     "~/Content/Appointment/Js/bootstrap.min.js",
                     "~/Content/JCrop/jquery.Jcrop.js",
                     "~/Content/js/jquery-ui-datepicker.js",
                     "~/Scripts/select2.min.js",
                     "~/Scripts/Default/jquery.iframe-transport.js",
                     "~/Scripts/Default/jquery.fileupload.js",
                     "~/Scripts/Default/jquery.fileupload-ui.js",
                     "~/Scripts/Default/application.js"
                     //"~/Content/js/jquery-1.10.2.js"
                     //"~/Content/js/lightbox.js"
                     ));
            Bundle.Add(new StyleBundle("~/CommonCSS").Include(
            "~/Content/css/Main/css/bootstrap.css",
            "~/Content/css/SweetAlert/sweetalert.css",
            "~/Content/css/Main/css/validation.css",
            "~/Content/css/DatePicker.css"
           ));
            Bundle.Add(new ScriptBundle("~/CommonJS").Include(
                 "~/Scripts/JS/jquery-1.11.2.min.js",
                "~/Scripts/JS/bootstrap.min.js",
                "~/Content/js/SweetAlert/sweetalert.min.js",
                "~/Scripts/jquery.validate.js",
                "~/Scripts/jquery.validate.unobtrusive.js",
                "~/Content/js/jquery-ui-datepicker.js",
                "~/Scripts/common.js"

                 ));
            Bundle.Add(new StyleBundle("~/PublicProfileCSS").Include(
              "~/Content/css/Main/css/main.css"
             ));
            Bundle.Add(new ScriptBundle("~/PublicProfileJS").Include(
                 "~/Scripts/JS/jquery.main.js"    
                 ));

            Bundle.Add(new StyleBundle("~/NewSiteLayoutCSS").Include(
             "~/Content/css/NewAdmin/css/bootstrap.min.css",
             "~/Content/css/Main/css/font-awesome.min.css",
             "~/Content/css/NewAdmin/css/dashboard.css",
             "~/Content/css/SweetAlert/sweetalert.css"
              //"~/Content/css/jquery-ui.css" //Commented due to UI Issue in Patient Hisory
            // "~/Content/css/NewAdmin/css/jcf.css"
            ));

            Bundle.Add(new ScriptBundle("~/NewSiteLayoutJS").Include(
                "~/Scripts/JS/bootstrap.min.js",
                 "~/Scripts/JS/sitescript.js",
                 "~/Scripts/JS/jquery.toaster.js",
                  "~/Scripts/jquery.validate.js",
                "~/Scripts/jquery.validate.unobtrusive.js",
                "~/Content/js/SweetAlert/sweetalert.min.js",
                "~/Scripts/jquery-ui-1.11.2.min.js"
                //"~/Content/css/NewAdmin/js/jquery-3.1.1.js"
                 //  "~/Scripts/JS/jcf.js"
                 ));
            Bundle.Add(new ScriptBundle("~/ComposeMessageJs").Include(
                "~/Content/ckeditor/ckeditor.js",
                "~/Content/Plugin/ckeditor/samples/js/sample.js"
                ));
            Bundle.Add(new ScriptBundle("~/_NewSiteLayoutInnerScripts").Include(
                    "~/Scripts/ControllerJS/Shared/NewSiteLauout.js",
                    "~/Scripts/common.js",
                    "~/Scripts/commonfileupload.js"
                ));
        }
    }
}