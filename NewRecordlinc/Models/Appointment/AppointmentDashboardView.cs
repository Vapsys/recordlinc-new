﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewRecordlinc.Models.Appointment
{
    public class AppointmentDashboardView
    {
        public DateTime Date { get; set; }
        public string PatientName { get; set; }
        public String ServiceType { get; set; }
        public String Status { get; set; }
    }
}