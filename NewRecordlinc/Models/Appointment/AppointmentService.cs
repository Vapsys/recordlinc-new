﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewRecordlinc.Models.Appointment
{
    public class AppointmentService
    {
        public int AppointmentServiceId { get; set; }

        public int DoctorId { get; set; }

        public string ServiceType { get; set; }

        public string TimeDuration { get; set; }

        public string Price { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
    public class AppointmentViewModel
    {
        public AppointmentViewModel()
        {
            AppointmentServices = new List<AppointmentService>();
        }
       public List<AppointmentService> AppointmentServices { get; set; }
    }
}