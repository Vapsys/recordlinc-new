﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NewRecordlinc.Models.Appointment
{
    public class OperationTheatreModel
    {
        public OperationTheatreModel()
        {
            TheatreList = new List<Appointment.OperationTheatre>();
            
        }
        public List<OperationTheatre> TheatreList { get; set; }
        public List<LocationForAppointment> LocationList { get; set; }
    }

    public class OperationTheatre
    {

        public int ApptResourceId { get; set; }
        [Required(ErrorMessage = "Please enter Operatory")]
        [Display(Name = "Operatory")]
        [RegularExpression(@"^[a-zA-Z\d\-_\s]+$",ErrorMessage = "Special characters are not allowed")]
        public string ResourceName { get; set; }

        [Display(Name = "Event Color")]
        public string EventColor { get; set; }

        [Display(Name = "Event Background Color")]
        public string EventBackgroundColor { get; set; }

        [Display(Name = "Event Border Color")]
        public string EventBorderColor { get; set; }

        [Display(Name = "Event Text Color")]
        public string EventTextColor { get; set; }

        [Display(Name = "Location")]
        public int LocationId { get; set; }

        public List<LocationForAppointment> LocationList { get; set; }



    }

    public class LocationForAppointment
    {
        public int locationId { get; set; }
        public string locationName { get; set; }
    }
    public class LocationListForSync
    {
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public string ProcessDate { get; set; }
    }
    public class AppointmentStatus
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }
}