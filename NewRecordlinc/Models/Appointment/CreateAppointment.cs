﻿using DataAccessLayer.Appointment;
using DataAccessLayer.Common;
using NewRecordlinc.Models.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using BusinessLogicLayer;

namespace NewRecordlinc.Models.Appointment
{

    public class CreateAppointment
    {

        public CreateAppointment()
        {
            AppointmentDayViewList = new List<AppointmentDayView>();
            AppointmentServiceList = new List<AppointmentService>();
            PatientlistCustom = new List<PatientList>();
            locationList = new List<LocationForAppointment>();
            DentistListCustom = new List<DentistList>();
            TheatreListCustom = new List<AppointmentResource>();
            ServiceListCustom = new List<ServiceList>();
            lstLocationForSync = new List<LocationListForSync>();
            lstInsurance = new List<InsuranceListForAppointment>();
        }
        public string FromTime { get; set; }
        public string ToTime { get; set; }
        public string PatientCercleName { get; set; }
        public string DoctorCercleName { get; set; }
        public string DoctorImage { get; set; }
        public string Note { get; set; }
        public int PatientId { get; set; }
        public int DoctorId { get; set; }
        public string PatientName { get; set; }
        public string DoctorName { get; set; }
        public string ServiceId { get; set; }
        public string ServiceType { get; set; }
        public string PrevDate { get; set; }
        public string NextDate { get; set; }
        public bool IsToday { get; set; }
        public int Servicelenght { get; set; }
        public int ServiceCount { get; set; }
        public DateTime CurrentDate
        {
            get; set;
        }
        public string LocationcheckedForSync { get; set; }
        public string LastSyncdateforLocation { get; set; }
        public List<SelectListItem> DoctorServices { get; set; }
        public List<PatientList> PatientlistCustom { get; set; }
        public List<SelectListItem> PatientList { get; set; }
        public List<LocationForAppointment> locationList { get; set; }

        public List<LocationListForSync> lstLocationForSync { get; set; }
        public List<AppointmentDayView> AppointmentDayViewList { get; set; }
        public List<AppointmentService> AppointmentServiceList { get; set; }
        public List<DentistList> DentistListCustom { get; set; }
        public List<AppointmentResource> TheatreListCustom { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public int locationId { get; set; }
        public int ApptResourceId { get; set; }
        public string PatientImageName { get; set; }
        public string PatientPhoneNumber { get; set; }
        public string PatientNumber { get; set; }
        public List<ServiceList> ServiceListCustom { get; set; }
        public int AppointmentLength { get; set; }

        public int AppointmentId { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int ParentUserId { get; set; }
        public List<InsuranceListForAppointment> lstInsurance { get; set; }
    }
    public class ServiceList
    {
        public int ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string TimeDuration { get; set; }
    }
    public class DentistList
    {
        public int UserId { get; set; }
        public string FullName { get; set; }
    }
    public class PatientList
    {
        public int PatientId { get; set; }
        public string PatientName { get; set; }
        public string PateintEmail { get; set; }
        public string PatientCercleName { get; set; }
        public int locationId { get; set; }
        public string ImageName { get; set; }
        public string PatientNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string Gender { get; set; }

    }
    public class AppointmentDayView
    {
        public AppointmentDayView()
        {
            AppointmentTimeSlotViewlst = new List<AppointmentTimeSlotView>();
        }
        public string DayLable { get; set; }
        public string IsOffDay { get; set; }
        public bool IsTodayDate { get; set; }
        public DateTime CurrentDate { get; set; }
        public List<AppointmentTimeSlotView> AppointmentTimeSlotViewlst { get; set; }
    }
    public class AppointmentTimeSlotView
    {
        public string TimeSlot { get; set; }
        public string IsBookedTimeslot { get; set; }
        public string Date { get; set; }
        public TimeSpan ActullTime { get; set; }
    }
    public class CreateAppointmentDataManagement
    {
        public CreateAppointmentDataManagement()
        {
            WeekSchedulesData = new List<WeekScheduleData>();
            SpecialDayChedulesData = new List<SpecialDayCheduleData>();
            AppointmentsData = new List<AppointmentData>();
        }
        public List<WeekScheduleData> WeekSchedulesData { get; set; }
        public List<SpecialDayCheduleData> SpecialDayChedulesData { get; set; }
        public List<AppointmentData> AppointmentsData { get; set; }
    }
    public class WeekScheduleData
    {
        public int AppointmentWeekScheduleId { get; set; }
        public int Day { get; set; }
        public string IsOffDay { get; set; }
        public int AppointmentWorkingTimeAllocationId { get; set; }
        public TimeSpan? MorningFromTime { get; set; }
        public TimeSpan? MorningToTime { get; set; }
        public TimeSpan? EveningFromTime { get; set; }
        public TimeSpan? EveningToTime { get; set; }

    }
    public class SpecialDayCheduleData
    {
        public int AppointmentSpecailDayScheduleId { get; set; }
        public DateTime Date { get; set; }
        public string IsOffDay { get; set; }
        public int AppointmentWorkingTimeAllocationId { get; set; }
        public TimeSpan? MorningFromTime { get; set; }
        public TimeSpan? MorningToTime { get; set; }

    }
    public class AppointmentData
    {
        public int AppointmentId { get; set; }
        public int DoctorId { get; set; }
        public int PatientId { get; set; }
        public string PatientName { get; set; }
        public string PatientCercleName { get; set; }
        public string TimeLable { get; set; }
        public string ServiceName { get; set; }
        public DateTime AppointmentDate { get; set; }
        public TimeSpan AppointmentTime { get; set; }
        public int AppointmentLength { get; set; }
        public string ServiceTypeId { get; set; }
        public string Status { get; set; }
        public int InsuranceId { get; set; }

        public string AppointmentNote
        {
            get; set;
        }

        //For Public Profile
        public string SignInEmail { get; set; }
        public string Password { get; set; }
        public string PFirstName { get; set; }
        public string PLastName { get; set; }
        public string PEmail { get; set; }
        public string PPhone { get; set; }
        public string PNote { get; set; }
        public int ApptResourceId { get; set; }
        public string PatientsImage { get; set; }
        public string DoctorImage { get; set; }
        public string PatientPhoneNumber { get; set; }
        public string Gender { get; set; }
        public string Location { get; set; }
        public string PatientNumber { get; set; }
        public string ResourceName { get; set; }
        public string DoctorName { get; set; }
        public string CreatedByName { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int Age { get; set; }
        public int LocationId { get; set; }
        public string strAppointmentDate { get; set; }
        public int CreatedBy { get; set; }
        public string AppointmentTimeSlot { get; set; }
        public List<AppointmentStatus> AppointmentStatusList { get; set; }
        public int AppointmentStatusId { get; set; }

    }
    public class CalendarView
    {
        public int AppointmentId { get; set; }
        public int PatientId { get; set; }
        public string PatientName { get; set; }
        public string ServiceId { get; set; }
        public string ServiceName { get; set; }
        public string Note { get; set; }
        public string FromDateTime { get; set; }
        public string ToDateTime { get; set; }
        public int ApptResourceId { get; set; }
        public string DoctorName { get; set; }
        public string PatientsPhone { get; set; }
        public string ResourceName { get; set; }
        public string EventBackgroundColor { get; set; }
        public string EventTextColor { get; set; }
        public string EventBorderColor { get; set; }
        public int DoctorId { get; set; }

    }
    public class CalendarResource
    {
        public string id { get; set; }
        public string title { get; set; }

        public string eventBackgroundColor { get; set; }
        public string eventBorderColor { get; set; }
        public string eventTextColor { get; set; }
    }
    public class AppointmentResource
    {
        public int ApptResourceId { get; set; }
        public string ResourceName { get; set; }
    }
    public class InsuranceListForAppointment
    {
        public int InsuranceId { get; set; }
        public string InsuranceName { get; set; }
    }
    public static class CommonAppointment
    {
        static int CONSTTIME = AppointmentSetting.CONSTTIME;
        static string CONSTDATEFORMATE = AppointmentSetting.CONSTDATEFORMATE;
        static int CONSTNOOFDAY = AppointmentSetting.CONSTNOOFDAY;

        public static CreateAppointment TimeSlotAllocation(CreateAppointmentDataManagement objCreateAppointmentDataManagement, DataTable dtAppService, string StartDate, int AppointmentId, string TimeZoneSystemName)
        {
            var objCreateAppointment = new CreateAppointment();
            //List of one month days
            var listofmonth = AppointmentSetting.GetOneMonthList(StartDate, objCreateAppointmentDataManagement);
            var TodayDateTime = DateTime.Now;
            TodayDateTime = clsHelper.ConvertFromUTC(Convert.ToDateTime(TodayDateTime.ToString()), TimeZoneSystemName);
            if (!string.IsNullOrEmpty(StartDate))
            {
                objCreateAppointment.CurrentDate = Convert.ToDateTime(StartDate).Date;
            }
            if (!string.IsNullOrEmpty(StartDate))
            {
                if (TodayDateTime.Date == Convert.ToDateTime(StartDate).Date)
                {
                    objCreateAppointment.PrevDate = TodayDateTime.ToString(CONSTDATEFORMATE);
                    objCreateAppointment.NextDate = TodayDateTime.AddDays(CONSTNOOFDAY).ToString(CONSTDATEFORMATE);
                }
                else if (TodayDateTime.AddDays(1).Date == Convert.ToDateTime(StartDate).Date)
                {

                    objCreateAppointment.PrevDate = Convert.ToDateTime(StartDate).AddDays(-1).ToString(CONSTDATEFORMATE);
                    objCreateAppointment.NextDate = Convert.ToDateTime(StartDate).AddDays(CONSTNOOFDAY).ToString(CONSTDATEFORMATE);
                }
                else if (TodayDateTime.AddDays(2).Date == Convert.ToDateTime(StartDate).Date)
                {
                    objCreateAppointment.PrevDate = Convert.ToDateTime(StartDate).AddDays(-2).ToString(CONSTDATEFORMATE);
                    objCreateAppointment.NextDate = Convert.ToDateTime(StartDate).AddDays(CONSTNOOFDAY).ToString(CONSTDATEFORMATE);
                }
                else
                {
                    objCreateAppointment.PrevDate = Convert.ToDateTime(StartDate).AddDays(-CONSTNOOFDAY).ToString(CONSTDATEFORMATE);
                    objCreateAppointment.NextDate = Convert.ToDateTime(StartDate).AddDays(CONSTNOOFDAY).ToString(CONSTDATEFORMATE);
                }

            }
            else
            {
                objCreateAppointment.PrevDate = TodayDateTime.ToString(CONSTDATEFORMATE);
                objCreateAppointment.NextDate = TodayDateTime.AddDays(CONSTNOOFDAY).ToString(CONSTDATEFORMATE);
            }
             List<DateTime> slotList = AppointmentSetting.GetPreviousSlot(StartDate, objCreateAppointmentDataManagement);
            if(slotList.Count > 0)
            {
                objCreateAppointment.PrevDate = slotList.FirstOrDefault().ToString("MM/dd/yyyy");
            }
            //Possible time slot allocation count base on 15 minute for 24 hours
            List<int> TimeDivision = AppointmentSetting.TimeSlotwithStaticTimeMin();

            //Loop Will run for one month
            foreach (var itemMonthsdays in listofmonth)
            {
                //DayNo. to find day wise data of week schedule
                int StartDay = AppointmentSetting.GetWeekSetting().Where(t => t.DayName == itemMonthsdays.DayOfWeek.ToString()).Select(t => t.Day).FirstOrDefault();

                //Found week schedule by DayNo.
                var drWeekSchedule = objCreateAppointmentDataManagement.WeekSchedulesData.Where(t => t.Day == StartDay).FirstOrDefault();
                if (drWeekSchedule != null)
                {
                    //Found specailday schedule by Date in "MM-dd-yyyy" formate.
                    var drSpacialDaySchedule = objCreateAppointmentDataManagement.SpecialDayChedulesData.Where(t => t.Date.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE)).FirstOrDefault();

                    //Found Booked appointment by Date in "MM-dd-yyyy" formate.
                    var drBookedAppointment = objCreateAppointmentDataManagement.AppointmentsData.Where(t => t.AppointmentDate.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE)).ToList();

                    var tempAppointmentDayView = new AppointmentDayView() { DayLable = string.Format("{2} <br>{1} {0}", itemMonthsdays.Day, itemMonthsdays.ToString("MMM"), itemMonthsdays.ToString("ddd")), IsOffDay = drWeekSchedule.IsOffDay, CurrentDate = itemMonthsdays };
                    if (itemMonthsdays.Date == DateTime.Now.Date)
                    {
                        tempAppointmentDayView.IsTodayDate = true;
                    }

                    //Check Specail day shedule is created on Date
                    if (drSpacialDaySchedule != null)
                    {
                        //If Spcail Day schdule is off day
                        if (drSpacialDaySchedule.IsOffDay.Equals("N"))
                        {
                            //Get time of spcial day schedule
                            TimeSpan MorFromTime = TimeSpan.Parse(Convert.ToString(drSpacialDaySchedule.MorningFromTime));
                            TimeSpan MorToTime = TimeSpan.Parse(Convert.ToString(drSpacialDaySchedule.MorningToTime));

                            //Start Slot Division with Maximum possiblity for selected service
                            foreach (var itemMinutes in TimeDivision)
                            {
                                //Check Morning From time should greater then To time
                                if (MorFromTime < MorToTime)
                                {
                                    //Check is there any booked appointment for the day
                                    if (drBookedAppointment.Count > 0)
                                    {
                                        //if appointment is booked then start checking one by one because there can me multiple appointment booked on the day
                                        foreach (var itemBookedApp in drBookedAppointment)
                                        {
                                            //Check where booked time is started from
                                            if (itemBookedApp.AppointmentTime == MorFromTime)
                                            {
                                                for (int i = 0; i < AppointmentSetting.TimeSlotForBookedTimeDivsion(itemBookedApp.AppointmentLength); i++)
                                                {
                                                    if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= MorFromTime)
                                                    {
                                                        tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = MorFromTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.TimeveOver.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(MorFromTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                                        MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                                    }
                                                    else
                                                    {
                                                        tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = MorFromTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.Booked.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(MorFromTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                                        MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                                    }
                                                }
                                            }//Check last slote should not excide To time and also check if appointment is booked on the day so rest time will be added there
                                            else if (MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= MorToTime)
                                            {
                                                if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= MorFromTime)
                                                {
                                                    tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = MorFromTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.TimeveOver.ToString(), TimeSlot = string.Format("{0} ", DateTime.Today.Add(MorFromTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                                    MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                                }
                                                else
                                                {
                                                    tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = MorFromTime, Date = itemMonthsdays.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(MorFromTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                                    MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                                }
                                            }
                                        }
                                    }//Check last slote should not excide To time
                                    else if (MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= MorToTime)
                                    {
                                        if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= MorFromTime)
                                        {
                                            tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = MorFromTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.TimeveOver.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(MorFromTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                            MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                        }
                                        else
                                        {
                                            tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = MorFromTime, Date = itemMonthsdays.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(MorFromTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                            MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                        }
                                    }
                                }
                            }
                            //-- Removing all booked slots from list
                            tempAppointmentDayView.AppointmentTimeSlotViewlst = tempAppointmentDayView.AppointmentTimeSlotViewlst.Where(x => x.IsBookedTimeslot != AppointmentSetting.ApoointmentStatus.Booked.ToString()).ToList();

                            objCreateAppointment.AppointmentDayViewList.Add(tempAppointmentDayView);
                        }
                    }
                    else
                    {
                        //Check week schedule day should be not Off day
                        if (drWeekSchedule.IsOffDay.Equals("N"))
                        {
                            TimeSpan MorFromTime;
                            TimeSpan MorToTime;
                            TimeSpan EveFromTime;
                            TimeSpan EveToTime;
                            //TimeSpan MorFromTime = TimeSpan.Parse(Convert.ToString(drWeekSchedule.MorningFromTime));
                            //TimeSpan MorToTime = TimeSpan.Parse(Convert.ToString(drWeekSchedule.MorningToTime));
                            if (string.IsNullOrEmpty(Convert.ToString(drWeekSchedule.MorningFromTime)) && string.IsNullOrEmpty(Convert.ToString(drWeekSchedule.MorningToTime)))
                            {
                                EveFromTime = Convert.ToDateTime(Convert.ToString(drWeekSchedule.EveningFromTime)).AddMinutes(-10).TimeOfDay;
                                MorFromTime = EveFromTime.Add(Convert.ToDateTime("00:05:00").TimeOfDay);
                                MorToTime = MorFromTime.Add(Convert.ToDateTime("00:05:00").TimeOfDay);
                                EveFromTime = EveFromTime;
                            }
                            else
                            {
                                MorFromTime = TimeSpan.Parse(Convert.ToString(drWeekSchedule.MorningFromTime));
                                MorToTime = TimeSpan.Parse(Convert.ToString(drWeekSchedule.MorningToTime));
                            }


                            if (string.IsNullOrEmpty(Convert.ToString(drWeekSchedule.EveningFromTime)) && string.IsNullOrEmpty(Convert.ToString(drWeekSchedule.EveningFromTime)))
                            {
                                MorToTime = Convert.ToDateTime(Convert.ToString(drWeekSchedule.MorningToTime)).AddMinutes(-10).TimeOfDay;
                                EveFromTime = MorToTime.Add(Convert.ToDateTime("00:05:00").TimeOfDay);
                                EveToTime = EveFromTime.Add(Convert.ToDateTime("00:05:00").TimeOfDay);
                                MorToTime = EveToTime;
                            }
                            else
                            {
                                EveFromTime = TimeSpan.Parse(Convert.ToString(drWeekSchedule.EveningFromTime));
                                EveToTime = TimeSpan.Parse(Convert.ToString(drWeekSchedule.EveningToTime));
                            }


                            TimeSpan TempTime = TimeSpan.Parse("00:00:00");
                            TimeSpan TempEndTime = TimeSpan.Parse("23:45:00");
                            foreach (var itemMinutes in TimeDivision)
                            {
                                if (TempTime < MorFromTime)
                                {
                                    //tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = TempTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.NotAvailable.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(TempTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                    TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                }
                                else if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= MorFromTime)
                                {
                                    if (MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= MorToTime)
                                    {
                                        //tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = MorFromTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.TimeveOver.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(MorFromTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                        MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                        TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                    }
                                    else if (TempTime >= MorToTime && TempTime < EveFromTime)
                                    {
                                        //tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = TempTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.NotAvailable.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(TempTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                        TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                    }
                                }
                                else if (drBookedAppointment.Count > 0)
                                {
                                    bool flag = false;
                                    foreach (var itemBookedApp in drBookedAppointment)
                                    {
                                        if (itemBookedApp.AppointmentTime == MorFromTime)
                                        {
                                            flag = true;
                                            if (itemBookedApp.AppointmentId == AppointmentId)
                                            {
                                                //Divide booked slot with 15 min as we are showing booking with 15 of difference
                                                for (int i = 0; i < AppointmentSetting.TimeSlotForBookedTimeDivsion(itemBookedApp.AppointmentLength); i++)
                                                {
                                                    tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = MorFromTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.Current.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(MorFromTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                                    MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                                    TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                                }
                                            }
                                            else
                                            {
                                                //Divide booked slot with 15 min as we are showing booking with 15 of difference
                                                for (int i = 0; i < AppointmentSetting.TimeSlotForBookedTimeDivsion(itemBookedApp.AppointmentLength); i++)
                                                {
                                                    tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = MorFromTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.Booked.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(MorFromTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                                    MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                                    TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                                }
                                            }

                                        }
                                    }
                                    if (!flag)
                                    {
                                        if (MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= MorToTime)
                                        {
                                            tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = MorFromTime, Date = itemMonthsdays.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(MorFromTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                            MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                            TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                        }
                                        else if (TempTime >= MorToTime && TempTime < EveFromTime)
                                        {
                                            // tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = TempTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.NotAvailable.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(TempTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                            TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                        }
                                    }

                                }
                                else if (MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= MorToTime)
                                {
                                    tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = MorFromTime, Date = itemMonthsdays.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(MorFromTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                    MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                    TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                }
                                else if (TempTime >= MorToTime && TempTime < EveFromTime)
                                {
                                    //tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = TempTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.NotAvailable.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(TempTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                    TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                }
                            }

                            //Evening Time calculation

                            foreach (var itemMinutes in TimeDivision)
                            {
                                if (TempTime < EveFromTime)
                                {
                                    //tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = TempTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.NotAvailable.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(TempTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                    // tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = TempTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.NotAvailable.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(TempTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                    TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                }
                                else if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= EveFromTime)
                                {
                                    if (EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= EveToTime)
                                    {
                                        //tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = EveFromTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.TimeveOver.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(EveFromTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                        EveFromTime = EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                        TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                    }
                                    else if (TempTime >= EveToTime && TempTime <= TempEndTime)
                                    {
                                        tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = TempTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.NotAvailable.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(TempTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                        // tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = TempTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.NotAvailable.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(TempTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                        TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                    }
                                }
                                else if (drBookedAppointment.Count > 0)
                                {
                                    bool flag = false;
                                    foreach (var itemBookedApp in drBookedAppointment)
                                    {
                                        if (itemBookedApp.AppointmentTime == EveFromTime)
                                        {
                                            flag = true;
                                            if (itemBookedApp.AppointmentId == AppointmentId)
                                            {
                                                for (int i = 0; i < AppointmentSetting.TimeSlotForBookedTimeDivsion(itemBookedApp.AppointmentLength); i++)
                                                {
                                                    tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = EveFromTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.Current.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(EveFromTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                                    EveFromTime = EveFromTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                                    TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                                }
                                            }
                                            else
                                            {
                                                for (int i = 0; i < AppointmentSetting.TimeSlotForBookedTimeDivsion(itemBookedApp.AppointmentLength); i++)
                                                {
                                                    tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = EveFromTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.Booked.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(EveFromTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                                    EveFromTime = EveFromTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                                    TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                                }

                                            }


                                        }
                                    }
                                    if (!flag)
                                    {
                                        if (EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= EveToTime)
                                        {
                                            tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = EveFromTime, Date = itemMonthsdays.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(EveFromTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                            EveFromTime = EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                            TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                        }
                                        else if (TempTime >= EveToTime && TempTime <= TempEndTime)
                                        {
                                            //  tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = TempTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.NotAvailable.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(TempTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                            TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                        }
                                    }

                                }
                                else if (EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= EveToTime)
                                {
                                    tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = EveFromTime, Date = itemMonthsdays.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(EveFromTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                    EveFromTime = EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
                                    TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                }
                                else if (TempTime >= EveToTime && TempTime <= TempEndTime)
                                {
                                    // tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = TempTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.NotAvailable.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(TempTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                                    TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                                }
                            }

                            //-- Removing all booked slots from list
                            tempAppointmentDayView.AppointmentTimeSlotViewlst = tempAppointmentDayView.AppointmentTimeSlotViewlst.Where(x => x.IsBookedTimeslot != AppointmentSetting.ApoointmentStatus.Booked.ToString()).ToList();

                            objCreateAppointment.AppointmentDayViewList.Add(tempAppointmentDayView);
                        }
                        //else
                        //{

                        //    TimeSpan TempTime = TimeSpan.Parse("00:00:00");
                        //    TimeSpan TempEndTime = TimeSpan.Parse("23:45:00");
                        //    foreach (var itemMinutes in TimeDivision)
                        //    {
                        //        tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { ActullTime = TempTime, Date = itemMonthsdays.ToString(), IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.NotAvailable.ToString(), TimeSlot = string.Format("{0}", DateTime.Today.Add(TempTime).ToString(AppointmentSetting.CONSTTIMEFORMATE)) });
                        //        TempTime = TempTime.Add(TimeSpan.FromMinutes(CONSTTIME));
                        //    }
                        //}
                       
                    }

                    
                }
            }

            return objCreateAppointment;
        }



        //public static CreateAppointment TimeSlotAllocation(CreateAppointmentDataManagement objCreateAppointmentDataManagement, DataTable dtAppService)
        //{
        //    var objCreateAppointment = new CreateAppointment();
        //    //List of one month days
        //    var listofmonth = AppointmentSetting.GetOneMonthList();
        //    var TodayDateTime = DateTime.Now;
        //    //Possible time slot allocation count base on 15 minute for 24 hours
        //    List<int> TimeDivision = AppointmentSetting.TimeSlotwithStaticTimeMin();

        //    //Loop Will run for one month
        //    foreach (var itemMonthsdays in listofmonth)
        //    {
        //        //DayNo. to find day wise data of week schedule
        //        int StartDay = AppointmentSetting.GetWeekSetting().Where(t => t.DayName == itemMonthsdays.DayOfWeek.ToString()).Select(t => t.Day).FirstOrDefault();

        //        //Found week schedule by DayNo.
        //        var drWeekSchedule = objCreateAppointmentDataManagement.WeekSchedulesData.Where(t => t.Day == StartDay).FirstOrDefault();

        //        //Found specailday schedule by Date in "MM-dd-yyyy" formate.
        //        var drSpacialDaySchedule = objCreateAppointmentDataManagement.SpecialDayChedulesData.Where(t => t.Date.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE)).FirstOrDefault();

        //        //Found Booked appointment by Date in "MM-dd-yyyy" formate.
        //        var drBookedAppointment = objCreateAppointmentDataManagement.AppointmentsData.Where(t => t.AppointmentDate.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE)).ToList();

        //        var tempAppointmentDayView = new AppointmentDayView() { DayLable = string.Format("{0} {1}, {2}", itemMonthsdays.Day, itemMonthsdays.ToString("MMM"), itemMonthsdays.ToString("ddd")), IsOffDay = drWeekSchedule.IsOffDay };

        //        //Check Specail day shedule is created on Date
        //        if (drSpacialDaySchedule != null)
        //        {
        //            //If Spcail Day schdule is off day
        //            if (drSpacialDaySchedule.IsOffDay.Equals("N"))
        //            {
        //                //Get time of spcial day schedule
        //                TimeSpan MorFromTime = TimeSpan.Parse(Convert.ToString(drSpacialDaySchedule.MorningFromTime));
        //                TimeSpan MorToTime = TimeSpan.Parse(Convert.ToString(drSpacialDaySchedule.MorningToTime));

        //                //Start Slot Division with Maximum possiblity for selected service
        //                foreach (var itemMinutes in TimeDivision)
        //                {
        //                    //Check Morning From time should greater then To time
        //                    if (MorFromTime < MorToTime)
        //                    {
        //                        //Check is there any booked appointment for the day
        //                        if (drBookedAppointment.Count > 0)
        //                        {
        //                            //if appointment is booked then start checking one by one because there can me multiple appointment booked on the day
        //                            foreach (var itemBookedApp in drBookedAppointment)
        //                            {
        //                                //Check where booked time is started from
        //                                if (itemBookedApp.AppointmentTime == MorFromTime)
        //                                {
        //                                    for (int i = 0; i < AppointmentSetting.TimeSlotForBookedTimeDivsion(itemBookedApp.AppointmentLength); i++)
        //                                    {
        //                                        if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= MorFromTime)
        //                                        {
        //                                            tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.TimeveOver.ToString(), TimeSlot = string.Format("{0} - {1}", MorFromTime.ToString(@"hh\:mm"), MorFromTime.Add(TimeSpan.FromMinutes(CONSTTIME)).ToString(@"hh\:mm")) });
        //                                            MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(CONSTTIME));
        //                                        }
        //                                        else
        //                                        {
        //                                            tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.Booked.ToString(), TimeSlot = string.Format("{0} - {1}", MorFromTime.ToString(@"hh\:mm"), MorFromTime.Add(TimeSpan.FromMinutes(CONSTTIME)).ToString(@"hh\:mm")) });
        //                                            MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(CONSTTIME));
        //                                        }
        //                                    }
        //                                }//Check last slote should not excide To time and also check if appointment is booked on the day so rest time will be added there
        //                                else if (MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= MorToTime)
        //                                {
        //                                    if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= MorFromTime)
        //                                    {
        //                                        tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.TimeveOver.ToString(), TimeSlot = string.Format("{0} - {1}", MorFromTime.ToString(@"hh\:mm"), MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)).ToString(@"hh\:mm")) });
        //                                        MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
        //                                    }
        //                                    else
        //                                    {
        //                                        tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { TimeSlot = string.Format("{0} - {1}", MorFromTime.ToString(@"hh\:mm"), MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)).ToString(@"hh\:mm")) });
        //                                        MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
        //                                    }
        //                                }
        //                            }
        //                        }//Check last slote should not excide To time
        //                        else if (MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= MorToTime)
        //                        {
        //                            if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= MorFromTime)
        //                            {
        //                                tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.TimeveOver.ToString(), TimeSlot = string.Format("{0} - {1}", MorFromTime.ToString(@"hh\:mm"), MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)).ToString(@"hh\:mm")) });
        //                                MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
        //                            }
        //                            else
        //                            {
        //                                tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { TimeSlot = string.Format("{0} - {1}", MorFromTime.ToString(@"hh\:mm"), MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)).ToString(@"hh\:mm")) });
        //                                MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        else
        //        {
        //            //Check week schedule day should be not Off day
        //            if (drWeekSchedule.IsOffDay.Equals("N"))
        //            {
        //                TimeSpan MorFromTime = TimeSpan.Parse(Convert.ToString(drWeekSchedule.MorningFromTime));
        //                TimeSpan MorToTime = TimeSpan.Parse(Convert.ToString(drWeekSchedule.MorningToTime));

        //                foreach (var itemMinutes in TimeDivision)
        //                {
        //                    if (MorFromTime < MorToTime)
        //                    {
        //                        if (drBookedAppointment.Count > 0)
        //                        {

        //                            foreach (var itemBookedApp in drBookedAppointment)
        //                            {
        //                                if (itemBookedApp.AppointmentTime == MorFromTime)
        //                                {
        //                                    //Divide booked slot with 15 min as we are showing booking with 15 of difference
        //                                    for (int i = 0; i < AppointmentSetting.TimeSlotForBookedTimeDivsion(itemBookedApp.AppointmentLength); i++)
        //                                    {
        //                                        //Check if time is Over for today date
        //                                        if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= MorFromTime)
        //                                        {
        //                                            tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.TimeveOver.ToString(), TimeSlot = string.Format("{0} - {1}", MorFromTime.ToString(@"hh\:mm"), MorFromTime.Add(TimeSpan.FromMinutes(CONSTTIME)).ToString(@"hh\:mm")) });
        //                                            MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(CONSTTIME));
        //                                        }
        //                                        else
        //                                        {//Remove this if dont need to show booked time if first slot of booked time is excide in today time
        //                                            tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.Booked.ToString(), TimeSlot = string.Format("{0} - {1}", MorFromTime.ToString(@"hh\:mm"), MorFromTime.Add(TimeSpan.FromMinutes(CONSTTIME)).ToString(@"hh\:mm")) });
        //                                            MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(CONSTTIME));
        //                                        }
        //                                    }

        //                                }
        //                                else if (MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= MorToTime)
        //                                {

        //                                    if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= MorFromTime)
        //                                    {
        //                                        tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.TimeveOver.ToString(), TimeSlot = string.Format("{0} - {1}", MorFromTime.ToString(@"hh\:mm"), MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)).ToString(@"hh\:mm")) });
        //                                        MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
        //                                    }
        //                                    else {
        //                                        tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { TimeSlot = string.Format("{0} - {1}", MorFromTime.ToString(@"hh\:mm"), MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)).ToString(@"hh\:mm")) });
        //                                        MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        else
        //                            //Check last slote should not excide To time
        //                            if (MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= MorToTime)
        //                        {
        //                            if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= MorFromTime)
        //                            {
        //                                tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.TimeveOver.ToString(), TimeSlot = string.Format("{0} - {1}", MorFromTime.ToString(@"hh\:mm"), MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)).ToString(@"hh\:mm")) });
        //                                MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
        //                            }
        //                            else {
        //                                tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { TimeSlot = string.Format("{0} - {1}", MorFromTime.ToString(@"hh\:mm"), MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)).ToString(@"hh\:mm")) });
        //                                MorFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
        //                            }
        //                        }
        //                    }
        //                }
        //                //Evening Time calculation
        //                TimeSpan EveFromTime = TimeSpan.Parse(Convert.ToString(drWeekSchedule.EveningFromTime));
        //                TimeSpan EveToTime = TimeSpan.Parse(Convert.ToString(drWeekSchedule.EveningToTime));
        //                foreach (var itemMinutes in TimeDivision)
        //                {
        //                    //Check if MorningTo time and EveningFrom time are starting at same time
        //                    //if (MorToTime == EveFromTime)
        //                    //{
        //                    //    //Check booked appointment
        //                    //    if (drBookedAppointment.Count > 0)
        //                    //    {
        //                    //        foreach (var itemBookedApp in drBookedAppointment)
        //                    //        {

        //                    //            if (itemBookedApp.AppointmentTime == MorFromTime)
        //                    //            {
        //                    //                for (int i = 0; i < AppointmentSetting.TimeSlotForBookedTimeDivsion(itemBookedApp.AppointmentLength); i++)
        //                    //                {
        //                    //                    if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= EveFromTime)
        //                    //                    {
        //                    //                        tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.TimeveOver.ToString(), TimeSlot = string.Format("{0} - {1}", MorFromTime.ToString(@"hh\:mm"), MorFromTime.Add(TimeSpan.FromMinutes(CONSTTIME)).ToString(@"hh\:mm")) });
        //                    //                        EveFromTime = MorFromTime.Add(TimeSpan.FromMinutes(CONSTTIME));
        //                    //                    }//Remove this if dont need to show booked time if first slot of booked time is excide in today time
        //                    //                    else {
        //                    //                        tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.Booked.ToString(), TimeSlot = string.Format("{0} - {1}", MorFromTime.ToString(@"hh\:mm"), MorFromTime.Add(TimeSpan.FromMinutes(CONSTTIME)).ToString(@"hh\:mm")) });
        //                    //                        EveFromTime = MorFromTime.Add(TimeSpan.FromMinutes(CONSTTIME));
        //                    //                    }
        //                    //                }
        //                    //            }
        //                    //            else if (MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= EveToTime)
        //                    //            {
        //                    //                if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= EveFromTime)
        //                    //                {
        //                    //                    tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() {IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.TimeveOver.ToString() ,TimeSlot = string.Format("{0} - {1}", MorFromTime.ToString(@"hh\:mm"), MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)).ToString(@"hh\:mm")) });
        //                    //                    EveFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
        //                    //                }
        //                    //                else {
        //                    //                    tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { TimeSlot = string.Format("{0} - {1}", MorFromTime.ToString(@"hh\:mm"), MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)).ToString(@"hh\:mm")) });
        //                    //                    EveFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
        //                    //                }
        //                    //            }
        //                    //        }
        //                    //    }
        //                    //    else if (MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= EveToTime)
        //                    //    {
        //                    //        if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= EveFromTime)
        //                    //        {
        //                    //            tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() {IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.TimeveOver.ToString() ,TimeSlot = string.Format("{0} - {1}", MorFromTime.ToString(@"hh\:mm"), MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)).ToString(@"hh\:mm")) });
        //                    //            EveFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
        //                    //        }
        //                    //        else
        //                    //        {
        //                    //            tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { TimeSlot = string.Format("{0} - {1}", MorFromTime.ToString(@"hh\:mm"), MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes)).ToString(@"hh\:mm")) });
        //                    //            EveFromTime = MorFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
        //                    //        }
        //                    //    }

        //                    //}
        //                    if (EveFromTime < EveToTime)
        //                    {
        //                        if (drBookedAppointment.Count > 0)
        //                        {
        //                            foreach (var itemBookedApp in drBookedAppointment)
        //                            {
        //                                //This will check if morning time where end evening time start from there like if moorning time start from 09 and end on 12 and evening time start from 12 and end on 16
        //                                if (itemBookedApp.AppointmentTime == EveFromTime)
        //                                {
        //                                    for (int i = 0; i < AppointmentSetting.TimeSlotForBookedTimeDivsion(itemBookedApp.AppointmentLength); i++)
        //                                    {
        //                                        if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= EveFromTime)
        //                                        {
        //                                            tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.TimeveOver.ToString(), TimeSlot = string.Format("{0} - {1}", EveFromTime.ToString(@"hh\:mm"), EveFromTime.Add(TimeSpan.FromMinutes(CONSTTIME)).ToString(@"hh\:mm")) });
        //                                            EveFromTime = EveFromTime.Add(TimeSpan.FromMinutes(CONSTTIME));
        //                                        }
        //                                        else {
        //                                            tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.Booked.ToString(), TimeSlot = string.Format("{0} - {1}", EveFromTime.ToString(@"hh\:mm"), EveFromTime.Add(TimeSpan.FromMinutes(CONSTTIME)).ToString(@"hh\:mm")) });
        //                                            EveFromTime = EveFromTime.Add(TimeSpan.FromMinutes(CONSTTIME));
        //                                        }
        //                                    }
        //                                }
        //                                else if (EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= EveToTime)
        //                                {
        //                                    if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= EveFromTime)
        //                                    {
        //                                        tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.TimeveOver.ToString(), TimeSlot = string.Format("{0} - {1}", EveFromTime.ToString(@"hh\:mm"), EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes)).ToString(@"hh\:mm")) });
        //                                        EveFromTime = EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
        //                                    }
        //                                    else {
        //                                        tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { TimeSlot = string.Format("{0} - {1}", EveFromTime.ToString(@"hh\:mm"), EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes)).ToString(@"hh\:mm")) });
        //                                        EveFromTime = EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
        //                                    }
        //                                }
        //                            }
        //                        }
        //                        else if (EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes)) <= EveToTime)
        //                        {
        //                            if (TodayDateTime.ToString(CONSTDATEFORMATE) == itemMonthsdays.ToString(CONSTDATEFORMATE) && TodayDateTime.TimeOfDay >= EveFromTime)
        //                            {
        //                                tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { IsBookedTimeslot = AppointmentSetting.ApoointmentStatus.TimeveOver.ToString(), TimeSlot = string.Format("{0} - {1}", EveFromTime.ToString(@"hh\:mm"), EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes)).ToString(@"hh\:mm")) });
        //                                EveFromTime = EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
        //                            }
        //                            else {
        //                                tempAppointmentDayView.AppointmentTimeSlotViewlst.Add(new AppointmentTimeSlotView() { TimeSlot = string.Format("{0} - {1}", EveFromTime.ToString(@"hh\:mm"), EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes)).ToString(@"hh\:mm")) });
        //                                EveFromTime = EveFromTime.Add(TimeSpan.FromMinutes(itemMinutes));
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        objCreateAppointment.AppointmentDayViewList.Add(tempAppointmentDayView);
        //    }
        //    return objCreateAppointment;
        //}
        public static CreateAppointmentDataManagement GetAppointmentDataIntoList(DataSet ds, string TimeZoneSystemName)
        {

            var objCreateAppointmentDataManagement = new CreateAppointmentDataManagement();

            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow item in ds.Tables[0].Rows)
                {
                    var tempWeekScheduleData = new WeekScheduleData();
                    tempWeekScheduleData.AppointmentWeekScheduleId = Convert.ToInt32(item["AppointmentWeekScheduleId"]);
                    tempWeekScheduleData.Day = Convert.ToInt32(item["Day"]);
                    tempWeekScheduleData.IsOffDay = Convert.ToString(item["IsOffDay"]);
                    tempWeekScheduleData.AppointmentWorkingTimeAllocationId = Convert.ToInt32(item["AppointmentWorkingTimeAllocationId"]);
                    if (!string.IsNullOrEmpty(Convert.ToString(item["MorningFromTime"])))
                    {
                        tempWeekScheduleData.MorningFromTime = TimeSpan.Parse(Convert.ToString(item["MorningFromTime"]));
                      //  tempWeekScheduleData.MorningFromTime =TimeSpan.Parse(Convert.ToString(new CommonBLL().ConvertToTime(Convert.ToDateTime(item["MorningFromTime"]),Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL))));
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(item["MorningToTime"])))
                    {
                        tempWeekScheduleData.MorningToTime = TimeSpan.Parse(Convert.ToString(item["MorningToTime"]));
                      //  tempWeekScheduleData.MorningToTime = TimeSpan.Parse(new CommonBLL().ConvertToTime(Convert.ToDateTime(item["MorningToTime"]), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL)));
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(item["EveningFromTime"])))
                    {
                        tempWeekScheduleData.EveningFromTime = TimeSpan.Parse(Convert.ToString(item["EveningFromTime"]));
                       // tempWeekScheduleData.EveningFromTime = TimeSpan.Parse(new CommonBLL().ConvertToTime(Convert.ToDateTime(item["EveningFromTime"]), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL)));
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(item["EveningToTime"])))
                    {
                        tempWeekScheduleData.EveningToTime = TimeSpan.Parse(Convert.ToString(item["EveningToTime"]));
                       // tempWeekScheduleData.EveningToTime = TimeSpan.Parse(new CommonBLL().ConvertToTime(Convert.ToDateTime(item["EveningToTime"]), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL)));
                    }
                    objCreateAppointmentDataManagement.WeekSchedulesData.Add(tempWeekScheduleData);
                }
            }

            if (ds.Tables[1].Rows.Count > 0)
            {
                foreach (DataRow item in ds.Tables[1].Rows)
                {
                    var tempSpecialDayCheduleData = new SpecialDayCheduleData();
                    tempSpecialDayCheduleData.AppointmentSpecailDayScheduleId = Convert.ToInt32(item["AppointmentSpecialDayScheduleId"]);
                    tempSpecialDayCheduleData.Date = Convert.ToDateTime(item["Date"]);
                    tempSpecialDayCheduleData.IsOffDay = Convert.ToString(item["IsOffDay"]);
                    tempSpecialDayCheduleData.AppointmentWorkingTimeAllocationId = Convert.ToInt32(item["AppointmentWorkingTimeAllocationId"]);

                    if (!string.IsNullOrEmpty(Convert.ToString(item["MorningFromTime"])))
                    {
                        tempSpecialDayCheduleData.MorningFromTime = TimeSpan.Parse(Convert.ToString(item["MorningFromTime"]));
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(item["MorningToTime"])))
                    {
                        tempSpecialDayCheduleData.MorningToTime = TimeSpan.Parse(Convert.ToString(item["MorningToTime"]));
                    }
                    objCreateAppointmentDataManagement.SpecialDayChedulesData.Add(tempSpecialDayCheduleData);
                }
            }

            if (ds.Tables[2].Rows.Count > 0)
            {
                objCreateAppointmentDataManagement.AppointmentsData = (from p in ds.Tables[2].AsEnumerable()
                                                                       select new AppointmentData
                                                                       {
                                                                           AppointmentId = Convert.ToInt32(p["AppointmentId"]),
                                                                           DoctorId = Convert.ToInt32(p["DoctorId"]),
                                                                           PatientId = Convert.ToInt32(p["PatientId"]),
                                                                           AppointmentDate = Convert.ToDateTime(p["AppointmentDate"]),
                                                                           AppointmentTime = TimeSpan.Parse(Convert.ToString(p["AppointmentTime"])),
                                                                           AppointmentLength = Convert.ToInt32(Convert.ToString(p["AppointmentLength"])),
                                                                           ServiceTypeId = Convert.ToString(p["ServiceTypeId"]),
                                                                           //Status = Convert.ToString(p["Status"])
                                                                       }).ToList();
                //-- convert appointment time to appropriate format
                foreach (var item in objCreateAppointmentDataManagement.AppointmentsData)
                {
                    var AppointmentDateTime = Convert.ToDateTime(item.AppointmentDate.ToString("yyyy-MM-dd") + " " + DateTime.Today.Add(item.AppointmentTime).ToString("HH:mm:ss"));
                    AppointmentDateTime = clsHelper.ConvertFromUTC(AppointmentDateTime, TimeZoneSystemName);
                    item.AppointmentDate = AppointmentDateTime.Date;
                    item.AppointmentTime = AppointmentDateTime.TimeOfDay;
                }
                {

                }
            }
            return objCreateAppointmentDataManagement;
        }
        public static List<CalendarView> CalenderViewIntoList(DataTable dt, string TimeZoneSystemName)
        {
           
           
            const string CONSTDATETIMEFORMATE = "yyyy-MM-dd HH:mm:ss";
            clsCommon ObjCommon = new clsCommon();
            var CalList = new List<CalendarView>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    DateTime AppFromdate,AppToDate;
                    TimeSpan AppTime;

                    // AppointmentDate = Convert.ToDateTime(objCreateAppointment.FromTime);
                    AppFromdate = Convert.ToDateTime(Convert.ToDateTime(item["AppointmentDate"]).ToString("yyyy-MM-dd") + " " + Convert.ToString(item["AppointmentTime"]));
                    AppFromdate = clsHelper.ConvertFromUTC(AppFromdate, TimeZoneSystemName);
                    if (!string.IsNullOrEmpty(Convert.ToString(item["AppointmentLength"])))
                        AppToDate = AppFromdate.AddMinutes(Convert.ToInt32(item["AppointmentLength"]));
                    else
                        AppToDate = AppFromdate;

                    AppTime = AppFromdate.TimeOfDay;
                    double Applength = 0;
                    var tempCalList = new CalendarView();
                    tempCalList.AppointmentId = Convert.ToInt32(item["AppointmentId"]);
                    tempCalList.PatientId = Convert.ToInt32(item["PatientId"]);
                    tempCalList.PatientName = Convert.ToString(item["PatientName"]);
                    tempCalList.ServiceId = Convert.ToString(item["ServiceTypeId"]);
                    tempCalList.ServiceName = Convert.ToString(item["ServiceType"]);
                    tempCalList.Note = Convert.ToString(item["Note"]);
                    tempCalList.ApptResourceId = Convert.ToInt32(item["ApptResourceId"]);
                    //Appdate = Convert.ToDateTime(item["AppointmentDate"]);
                    //AppTime = TimeSpan.Parse(Convert.ToString(item["AppointmentTime"]));
                    Applength = Convert.ToInt64(item["AppointmentLength"]);
                    tempCalList.FromDateTime = Convert.ToString(AppFromdate.ToString(CONSTDATETIMEFORMATE));
                    AppTime = AppTime.Add(TimeSpan.FromMinutes(Applength));
                    tempCalList.ToDateTime = Convert.ToString(AppToDate.ToString(CONSTDATETIMEFORMATE));
                    tempCalList.DoctorName = Convert.ToString(item["DoctorName"]);
                    tempCalList.PatientsPhone = ObjCommon.ConvertPhoneNumber(ObjCommon.CheckNull(Convert.ToString(item["PatientsPhone"]), string.Empty));
                    tempCalList.EventBackgroundColor = Convert.ToString(item["EventBackgroundColor"]);
                    tempCalList.ResourceName = Convert.ToString(item["ResourceName"]);
                    tempCalList.EventBorderColor = Convert.ToString(item["EventBorderColor"]);
                    tempCalList.EventTextColor = Convert.ToString(item["EventTextColor"]);
                    tempCalList.DoctorId = Convert.ToInt32(item["DoctorId"]);
                    CalList.Add(tempCalList);

                }
            }
            return CalList;
        }
        public static List<WeekScheduleData> WeekScheduleIntoList(DataTable dt)
        {
            List<WeekScheduleData> objlst = new List<WeekScheduleData>();
            foreach (DataRow item in dt.Rows)
            {
                var tempWeekScheduleData = new WeekScheduleData();
                tempWeekScheduleData.AppointmentWeekScheduleId = 0;
                tempWeekScheduleData.Day = Convert.ToInt32(item["Day"]);
                if (string.IsNullOrEmpty(Convert.ToString(item["MorningFromTime"])) && string.IsNullOrEmpty(Convert.ToString(item["MorningToTime"])) && string.IsNullOrEmpty(Convert.ToString(item["EveningFromTime"])) && string.IsNullOrEmpty(Convert.ToString(item["EveningToTime"])))
                {
                    tempWeekScheduleData.IsOffDay = "Y";
                }
                else {
                    tempWeekScheduleData.IsOffDay = "N";
                }
                    tempWeekScheduleData.AppointmentWorkingTimeAllocationId = 0;
                if (!string.IsNullOrEmpty(Convert.ToString(item["MorningFromTime"])))
                {
                    tempWeekScheduleData.MorningFromTime = TimeSpan.Parse(Convert.ToString(item["MorningFromTime"]));
                }
                if (!string.IsNullOrEmpty(Convert.ToString(item["MorningToTime"])))
                {
                    tempWeekScheduleData.MorningToTime = TimeSpan.Parse(Convert.ToString(item["MorningToTime"]));
                }
                if (!string.IsNullOrEmpty(Convert.ToString(item["EveningFromTime"])))
                {
                    tempWeekScheduleData.EveningFromTime = TimeSpan.Parse(Convert.ToString(item["EveningFromTime"]));
                }
                if (!string.IsNullOrEmpty(Convert.ToString(item["EveningToTime"])))
                {
                    tempWeekScheduleData.EveningToTime = TimeSpan.Parse(Convert.ToString(item["EveningToTime"]));
                }
                objlst.Add(tempWeekScheduleData);
            }
            return objlst;
        }

        public static List<AppointmentResource> AppointmentResourceIntoList(DataTable dt)
        {

            var CalList = new List<AppointmentResource>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    var tempCalList = new AppointmentResource();
                    tempCalList.ApptResourceId = Convert.ToInt32(item["ApptResourceId"]);
                    tempCalList.ResourceName = Convert.ToString(item["ResourceName"]);
                    CalList.Add(tempCalList);
                }
            }
            return CalList;
        }
        public static List<LocationListForSync> GetLastSyncDateforLocation(int UserId)
        {
            List<LocationListForSync> lstLocationforlist = new List<LocationListForSync>();
            try
            {
                clsCommon objcommon = new clsCommon();

                var ObjColleaguesData = new clsColleaguesData();
                DataTable dt = new DataTable();
                dt = ObjColleaguesData.GetLocationSyncStatus(UserId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        LocationListForSync Obj = new LocationListForSync();
                        Obj.LocationId = Convert.ToInt32(item["LocationId"]);
                        Obj.LocationName = objcommon.CheckNull(Convert.ToString(item["Location"]), string.Empty);
                        if (Convert.ToString(item["IsProcessed"]) == "True")
                        {
                            if (Convert.ToString(item["ProcessDate"]) == "1900-01-01 00:00:00.000")
                            {
                                Obj.ProcessDate = string.Empty;
                            }
                            else
                            {
                                Obj.ProcessDate = "Last sync date is : " + objcommon.CheckNull(Convert.ToString(item["ProcessDate"]), string.Empty);
                            }
                        }
                        else if (Convert.ToString(item["IsProcessed"]) == "False")
                        {
                            Obj.ProcessDate = "Your sync is in process";
                        }
                        else
                        {
                            Obj.ProcessDate = "You have naver sync";
                        }
                        lstLocationforlist.Add(Obj);
                    }
                    return lstLocationforlist;
                }

            }
            catch (Exception)
            {
                return lstLocationforlist;
            }
            return lstLocationforlist;
        }
        public static List<CalendarResource> CalendarResourceIntoList(DataTable dt)
        {

            var CalList = new List<CalendarResource>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow item in dt.Rows)
                {
                    var tempCalList = new CalendarResource();
                    tempCalList.id = Convert.ToString(item["ApptResourceId"]);
                    tempCalList.title = Convert.ToString(item["ResourceName"]);
                    tempCalList.eventBackgroundColor = Convert.ToString(item["EventBackgroundColor"]);
                    tempCalList.eventBorderColor = Convert.ToString(item["eventBorderColor"]);
                    tempCalList.eventTextColor = Convert.ToString(item["EventTextColor"]);
                    CalList.Add(tempCalList);
                }
            }
            return CalList;
        }
        public static string CheckOverLappingAndDoctorAvailability(CreateAppointment objCreateAppointment, int DoctorId, string TimeZoneSystemName)
        {

            var objclsAppointmentData = new clsAppointmentData();
            DateTime OrigionalFromTime = Convert.ToDateTime(objCreateAppointment.FromTime);
            DateTime FromTime = OrigionalFromTime;
            if (!string.IsNullOrEmpty(TimeZoneSystemName))
                FromTime = clsHelper.ConvertToUTC(FromTime, TimeZoneSystemName);

            DateTime OrigionalToTime = Convert.ToDateTime(objCreateAppointment.ToTime);
            DateTime ToTime = OrigionalToTime;
            if (!string.IsNullOrEmpty(TimeZoneSystemName))
                ToTime = clsHelper.ConvertToUTC(ToTime, TimeZoneSystemName);

            if (FromTime < DateTime.Now || ToTime < DateTime.Now)
            {
                return "Can not select past time!";
            }
            if (Convert.ToInt32(objCreateAppointment.AppointmentId) > 0)
            {
                objCreateAppointment.AppointmentId = objCreateAppointment.AppointmentId;
            }
            else
            {
                objCreateAppointment.AppointmentId = 0;
            }
            var timeSlotData = GetAppointmentDataIntoList(objclsAppointmentData.GetCreateAppointmentTimeSlotViewData(DoctorId, objCreateAppointment.ApptResourceId, objCreateAppointment.AppointmentId),TimeZoneSystemName);

            int StartDay = AppointmentSetting.GetWeekSetting().Where(t => t.DayName == FromTime.DayOfWeek.ToString()).Select(t => t.Day).FirstOrDefault();

            var drSpacialDaySchedule = timeSlotData.SpecialDayChedulesData.Where(t => t.Date.ToString(CONSTDATEFORMATE) == FromTime.ToString(CONSTDATEFORMATE)).FirstOrDefault();
            var drWeekSchedule = timeSlotData.WeekSchedulesData.Where(t => t.Day == StartDay).FirstOrDefault();
            var drBookedAppointment = timeSlotData.AppointmentsData.Where(t => t.AppointmentDate.ToString(CONSTDATEFORMATE) == FromTime.ToString(CONSTDATEFORMATE)).ToList();

            var checkbookin = drBookedAppointment.Where(t => t.AppointmentTime <= FromTime.AddMinutes(+1).TimeOfDay && t.AppointmentTime.Add(TimeSpan.FromMinutes(t.AppointmentLength)) >= FromTime.AddMinutes(+1).TimeOfDay).ToList();
            if (checkbookin.Count > 0)
            {
                return "You have booked appointment on this time";
            }
            else
            {
                var totimecheck = drBookedAppointment.Where(t => t.AppointmentTime <= ToTime.AddMinutes(-1).TimeOfDay && t.AppointmentTime.Add(TimeSpan.FromMinutes(t.AppointmentLength)) >= ToTime.AddMinutes(-1).TimeOfDay).ToList();
                if (totimecheck.Count > 0)
                {
                    return "You have booked appointment on this time";
                }
            }

            var CheckBetn = drBookedAppointment.Where(t => t.AppointmentTime >= FromTime.AddMinutes(+1).TimeOfDay && t.AppointmentTime.Add(TimeSpan.FromMinutes(15)) <= ToTime.AddMinutes(-1).TimeOfDay).ToList();

            if (CheckBetn.Count > 0)
            {
                return "You have booked appointment on this time";
            }
            if (drSpacialDaySchedule?.IsOffDay == "Y")
            {
                return "Off day on special day schedule, can not create appointment";
            }
            else if (drSpacialDaySchedule != null)
            {
                if (!(drSpacialDaySchedule?.MorningFromTime <= FromTime.TimeOfDay && drSpacialDaySchedule?.MorningToTime >= ToTime.TimeOfDay))
                {
                    return "your are not available on that time,Specail day schedule is created for this date";
                }
            }

            if (drWeekSchedule?.IsOffDay == "Y")
            {
                return "Your have off day on this date!";
            }
            else if (drWeekSchedule != null)
            {
                DateTime MorningFromTime = OrigionalFromTime.Date, EveningFromTime = OrigionalFromTime.Date, EveningToTime = OrigionalToTime.Date, MorningToTime = OrigionalToTime.Date;
                MorningFromTime = drWeekSchedule.MorningFromTime.HasValue ? Convert.ToDateTime(OrigionalFromTime.Date.Add(drWeekSchedule.MorningFromTime.GetValueOrDefault())) : MorningFromTime;
                MorningFromTime = clsHelper.ConvertToUTC(MorningFromTime, TimeZoneSystemName);

                EveningFromTime = drWeekSchedule.EveningFromTime.HasValue ? Convert.ToDateTime(OrigionalFromTime.Date.Add(drWeekSchedule.EveningFromTime.GetValueOrDefault())) : EveningFromTime;
                EveningFromTime = clsHelper.ConvertToUTC(EveningFromTime, TimeZoneSystemName);

                EveningToTime = drWeekSchedule.EveningToTime.HasValue ? Convert.ToDateTime(OrigionalToTime.Date.Add(drWeekSchedule.EveningToTime.GetValueOrDefault())) : EveningToTime;
                EveningToTime = clsHelper.ConvertToUTC(EveningToTime, TimeZoneSystemName);

                MorningToTime = drWeekSchedule.MorningToTime.HasValue ? Convert.ToDateTime(OrigionalToTime.Date.Add(drWeekSchedule.MorningToTime.GetValueOrDefault())) : MorningToTime;
                MorningToTime = clsHelper.ConvertToUTC(MorningToTime, TimeZoneSystemName);

                if ((MorningFromTime <= FromTime && MorningToTime >= ToTime) || (EveningFromTime <= FromTime && EveningToTime >= ToTime))
                {
                    return "";
                }
                else
                {
                    return "your are not available on that time!";
                }
            }
            return "";
        }
        public static int CheckAvalibleDoctorByDateTimeTheatreDoctor(CreateAppointment objCreateAppointment, int DoctorId, string TimeZoneSystemName)
        {
            DateTime FromTime = Convert.ToDateTime(objCreateAppointment.FromTime);
            var objclsAppointmentData = new clsAppointmentData();
            DateTime AppointmentDate;
            TimeSpan AppointmentFromTime;
            AppointmentDate = clsHelper.ConvertToUTC(Convert.ToDateTime(objCreateAppointment.FromTime), TimeZoneSystemName);
            AppointmentFromTime = AppointmentDate.TimeOfDay;
            TimeSpan AppointmentToTime = AppointmentFromTime.Add(TimeSpan.FromMinutes(Convert.ToInt32(objCreateAppointment.AppointmentLength)));
            if (Convert.ToInt32(objCreateAppointment.AppointmentId) > 0)
            {
                objCreateAppointment.AppointmentId = objCreateAppointment.AppointmentId;
            }
            else
            {
                objCreateAppointment.AppointmentId = 0;
            }
            int Result = (objclsAppointmentData.CheckAvalibleDoctorByDateTimeTheatreDoctor(objCreateAppointment.DoctorId, AppointmentFromTime, AppointmentToTime, objCreateAppointment.CurrentDate, objCreateAppointment.AppointmentId)).Rows.Count;
            return Result;

        }

        public static int CheckAvalibleDoctorByDateTimeTheatrePatient(CreateAppointment objCreateAppointment, int DoctorId, string TimeZoneSystemName)
        {
            DateTime FromTime = Convert.ToDateTime(objCreateAppointment.FromTime);
            var objclsAppointmentData = new clsAppointmentData();
            DateTime AppointmentDate;
            TimeSpan AppointmentFromTime;
            AppointmentDate = clsHelper.ConvertToUTC(Convert.ToDateTime(objCreateAppointment.FromTime), TimeZoneSystemName);
            AppointmentFromTime = AppointmentDate.TimeOfDay;
            TimeSpan AppointmentToTime = AppointmentFromTime.Add(TimeSpan.FromMinutes(Convert.ToInt32(objCreateAppointment.AppointmentLength)));
            if (Convert.ToInt32(objCreateAppointment.AppointmentId) > 0)
            {
                objCreateAppointment.AppointmentId = objCreateAppointment.AppointmentId;
            }
            else
            {
                objCreateAppointment.AppointmentId = 0;
            }
            int Result = (objclsAppointmentData.CheckAvalibleDoctorByDateTimeTheatrePatientId(objCreateAppointment.PatientId, AppointmentFromTime, AppointmentToTime, objCreateAppointment.CurrentDate, objCreateAppointment.AppointmentId)).Rows.Count;
            return Result;

        }
    }
    public static class AppointmentSetting
    {
        public const int CONSTTIME = 15;
        public const int CONSTNOOFDAY = 4;
        public const string CONSTDATEFORMATE = "MM-dd-yyyy";
        public const string CONSTDATEFORMATE1 = "MM/dd/yyyy";
        public const string CONSTTIMEFORMATE = "hh:mm tt";
        public const string CONSTDATETIMEFORMATE = "MM/dd/yyyy hh:mm tt";
        public static List<SelectListItem> GetAllTime()
        {

            List<SelectListItem> lst = new List<SelectListItem>();
            lst.Add(new SelectListItem { Text = "00", Value = "00:00:00" });
            lst.Add(new SelectListItem { Text = "01", Value = "01:00:00" });
            lst.Add(new SelectListItem { Text = "02", Value = "02:00:00" });
            lst.Add(new SelectListItem { Text = "03", Value = "03:00:00" });
            lst.Add(new SelectListItem { Text = "04", Value = "04:00:00" });
            lst.Add(new SelectListItem { Text = "05", Value = "05:00:00" });
            lst.Add(new SelectListItem { Text = "06", Value = "06:00:00" });
            lst.Add(new SelectListItem { Text = "07", Value = "07:00:00" });
            lst.Add(new SelectListItem { Text = "08", Value = "08:00:00" });
            lst.Add(new SelectListItem { Text = "09", Value = "09:00:00" });
            lst.Add(new SelectListItem { Text = "10", Value = "10:00:00" });
            lst.Add(new SelectListItem { Text = "11", Value = "11:00:00" });
            lst.Add(new SelectListItem { Text = "12", Value = "12:00:00" });
            lst.Add(new SelectListItem { Text = "13", Value = "13:00:00" });
            lst.Add(new SelectListItem { Text = "14", Value = "14:00:00" });
            lst.Add(new SelectListItem { Text = "15", Value = "15:00:00" });
            lst.Add(new SelectListItem { Text = "16", Value = "16:00:00" });
            lst.Add(new SelectListItem { Text = "17", Value = "17:00:00" });
            lst.Add(new SelectListItem { Text = "18", Value = "18:00:00" });
            lst.Add(new SelectListItem { Text = "19", Value = "19:00:00" });
            lst.Add(new SelectListItem { Text = "20", Value = "20:00:00" });
            lst.Add(new SelectListItem { Text = "21", Value = "21:00:00" });
            lst.Add(new SelectListItem { Text = "22", Value = "22:00:00" });
            lst.Add(new SelectListItem { Text = "23", Value = "23:00:00" });
            return lst;
        }
        public static List<WeekDay> GetWeekSetting()
        {

            return new List<WeekDay>()
            {
                new WeekDay(){Day=0,DayName="Monday"},
                new WeekDay(){Day=1,DayName="Tuesday"},
                new WeekDay(){Day=2,DayName="Wednesday"},
                new WeekDay(){Day=3,DayName="Thursday"},
                new WeekDay(){Day=4,DayName="Friday"},
                new WeekDay(){Day=5,DayName="Saturday"},
                new WeekDay(){Day=6,DayName="Sunday"},
            };
        }

        public static List<DateTime> GetOneMonthList(string SartDate, CreateAppointmentDataManagement objCreateAppointmentDataManagement)
        {
            var dates = new List<DateTime>();
            var onemonth = DateTime.Now.AddDays(30);
            var startingdate = DateTime.Now;
            if (!string.IsNullOrEmpty(SartDate))
            {
                startingdate = Convert.ToDateTime(SartDate);
                onemonth = startingdate.AddDays(30);
            }


            int i = 1;
            for (var date = startingdate; date < onemonth && i <= CONSTNOOFDAY; date = date.AddDays(1))
            {
                int StartDay = AppointmentSetting.GetWeekSetting().Where(t => t.DayName == date.DayOfWeek.ToString()).Select(t => t.Day).FirstOrDefault();

                //Found week schedule by DayNo.
                var drWeekSchedule = objCreateAppointmentDataManagement.WeekSchedulesData.Where(t => t.Day == StartDay).FirstOrDefault();
                if(drWeekSchedule.IsOffDay.Equals("N",StringComparison.InvariantCultureIgnoreCase))
                {
                    dates.Add(date);
                    i++;
                }
            }
            return dates;
        }
        public static List<DateTime> GetPreviousSlot(string SartDate, CreateAppointmentDataManagement objCreateAppointmentDataManagement)
        {
            var dates = new List<DateTime>();
            var onemonth = DateTime.Now.AddDays(-30);
            var startingdate = DateTime.Now;
            if (!string.IsNullOrEmpty(SartDate))
            {
                startingdate = Convert.ToDateTime(SartDate);
                startingdate = startingdate.AddDays(-1);
                onemonth = startingdate.AddDays(-30);
            }


            int i = 1;
            for (var date = startingdate; date > onemonth && i <= CONSTNOOFDAY; date = date.AddDays(-1))
            {
                int StartDay = AppointmentSetting.GetWeekSetting().Where(t => t.DayName == date.DayOfWeek.ToString()).Select(t => t.Day).FirstOrDefault();

                //Found week schedule by DayNo.
                var drWeekSchedule = objCreateAppointmentDataManagement.WeekSchedulesData.Where(t => t.Day == StartDay).FirstOrDefault();
                if (drWeekSchedule.IsOffDay.Equals("N", StringComparison.InvariantCultureIgnoreCase))
                {
                    dates.Add(date);
                    i++;
                }
            }
            dates.Reverse();
            return dates;
        }
        public static List<int> HoursDevision(TimeSpan Servicetime)
        {
            int minutes = (int)Servicetime.TotalMinutes;
            int slotcount = 24 * 60 / minutes;
            List<int> lst = new List<int>();
            for (int i = 0; i < slotcount; i++)
            {
                lst.Add(minutes);
            }
            return lst;
        }

        public static List<int> TimeSlotwithStaticTimeMin()
        {

            int slotcount = 24 * 60 / CONSTTIME;
            List<int> lst = new List<int>();
            for (int i = 0; i < slotcount; i++)
            {
                lst.Add(CONSTTIME);
            }
            return lst;
        }
        public static int TimeSlotForBookedTimeDivsion(int Servicetime)
        {
            return Servicetime / CONSTTIME;
        }
        public enum ApoointmentStatus
        {
            Booked,
            TimeveOver,
            NotAvailable,
            Current
        }
    }
}