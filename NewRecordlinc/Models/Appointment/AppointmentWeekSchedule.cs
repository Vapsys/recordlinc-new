﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NewRecordlinc.Models.Appointment
{
    public class AppointmentWeekSchedule
    {
        public AppointmentWeekSchedule()
        {
            WeekDay = new List<WeekDay>();
        }
        public int AppointmentWeekScheduleId { get; set; }

        public int DoctorId { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }
        public List<SelectListItem> Times { get; set; }

        public List<WeekDay> WeekDay { get; set; }

    }

    public class WeekDay
    {
        public int Day { get; set; }

        public string DayName { get; set; }

        public bool IsOffDay { get; set; }

        public int AppointmentWorkingTimeAllocationId { get; set; }

        public int? AppointmentWeekScheduleId { get; set; }

        public int? AppintmentSpecialDayScheduleId { get; set; }

        public string MorningFromTime { get; set; }

        public string MorningToTime { get; set; }

        public string EveningFromTime { get; set; }

        public string EveningToTime { get; set; }

        public List<SelectListItem> Times { get; set; }
    }
}