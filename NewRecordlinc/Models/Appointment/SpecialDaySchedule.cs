﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NewRecordlinc.Models.Appointment
{
    public class SpecialDaySchedule
    {
        public SpecialDaySchedule()
        {
            DateOfday = new Date();
        }
       public int AppointmentSpecialDayScheduleId { get; set; }
       public int DoctorId { get; set; }
       public DateTime Date { get; set; }
       public bool IsoffDay { get; set; }
       public Date DateOfday { get; set; }
    }
    public class Date
    {
        public int AppointmentWorkingTimeAllocationId { get; set; }
        public int? AppintmentSpecialDayScheduleId { get; set; }
        public string MorningFromTime { get; set; }
        public string MorningToTime { get; set; }
        public List<SelectListItem> Times { get; set; }
    }
}