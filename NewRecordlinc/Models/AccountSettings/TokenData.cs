﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewRecordlinc.Models.AccountSettings
{
    public class TokenData
    {
        public string Access_Token { get; set; }
        public string Refresh_Token { get; set; }
        public string Expires_In { get; set; }
        public string Token_Type { get; set; }
    }
    public class Location
    {
        public int locationId { get; set; }
        public string locationName { get; set; }
    }
}