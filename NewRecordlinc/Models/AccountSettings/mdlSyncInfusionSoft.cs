﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewRecordlinc.Models.AccountSettings
{
    public class mdlSyncInfusionSoft
    {
        public long Id { get; set; }
        public int MemberId { get; set; }
        public string APIName { get; set; }
        public string APIURL { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string APIKey { get; set; }
        public int SyncPerDay { get; set; }
        public bool IsActive { get; set; }
        public DateTime OnCreated { get; set; }
        public DateTime OnUpdated { get; set; }
    }
}