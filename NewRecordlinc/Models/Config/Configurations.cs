﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace NewRecordlinc.Models.Config
{
    public class Configurations
    {
        public static string gclientID = Convert.ToString(ConfigurationManager.AppSettings["google_clientID"]); //"299046596018-f5j343nvbnp6t4h36g1dsgsr79li0bps.apps.googleusercontent.com";
        public static string gclientSecret = Convert.ToString(ConfigurationManager.AppSettings["google_clientsecret"]); //"KoRx8NANfuFIARIBFPRRt5qE";
        public static string gredirectUri = Convert.ToString(ConfigurationManager.AppSettings["google_redirectUri"]);
        public static string oclientID = Convert.ToString(ConfigurationManager.AppSettings["outlook_clientID"]);
        public static string oclientSecret = Convert.ToString(ConfigurationManager.AppSettings["outlook_clientsecret"]);
        public static string oredirectUri = Convert.ToString(ConfigurationManager.AppSettings["outlook_redirectUri"]);
        public static string mailbeeKey = Convert.ToString(ConfigurationManager.AppSettings["mailbee_licensekey"]);
        
    }
}