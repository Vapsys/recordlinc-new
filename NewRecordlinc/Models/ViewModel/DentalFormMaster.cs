﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Configuration;
using DataAccessLayer.PatientsData;
using NewRecordlinc.Models.Patients;
using System.ComponentModel.DataAnnotations;

namespace NewRecordlinc.Models.ViewModel
{
    public class DentalFormMaster
    {

        public List<BasicInfo> BasicInfo { get; set; }
        public List<RegistrationForm> RegistrationForm { get; set; }
        public List<DentalHistory> DentalHistory { get; set; }
        public List<MedicalHistory> MedicalHistory { get; set; }
    }



    public class BasicInfo
    {
        DataRepository objDataRepository = new DataRepository();

        public int PatientId { get; set; }

        public string AssignedPatientId { set; get; }

        [Required(ErrorMessage = "Required")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Required")]
        public string LastName { get; set; }

        public string MiddelName { set; get; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$", ErrorMessage = "Invalid")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [System.Web.Mvc.Compare("Password", ErrorMessage = "Confirmation Password Do Not Match.")]
        public string ConfirmPassword { get; set; }

        public string GuardianFirstName { get; set; }
        public string GuardianLastName { get; set; }
        public string GuardianPhone { get; set; }
        public string GuardianEmail { get; set; }

        public string Country { set; get; }
        public string DateOfBirth { set; get; }
        public int Gender { set; get; }
        public string ProfileImage { set; get; }

        public string Phone { set; get; }
        public string txtWorkPhone { set; get; }
        public string txtFax { set; get; }
        public string Address { set; get; }
        
        public string City { set; get; }
        public string State { set; get; }
        public string Zip { set; get; }
        public string Comments { set; get; }
        public string StartDate { set; get; }
        public string Address2 { set; get; }
        public string GuarFirstName { set; get; }
        public string GuarLastName { set; get; }
        public string GuarPhone { set; get; }
        public string GuarEmail { set; get; }
        public string FacebookUrl { set; get; }
        public string TwitterUrl { set; get; }
        public string LinkedinUrl { set; get; }
        public string Status { set; get; }
        public int OwnerId { set; get; }
        public int Limit { get; set; }
        public int FromRowNumber { get; set; }
        public string ContainerId { get; set; }
        public string AjaxActionUrl { get; set; }
        public string StayloggedMins { get; set; }
        public string SecondaryEmail { get; set; }

        public IDictionary<string, object> Parameters { get; set; }



        public virtual List<RegistrationForm> RegistrationForm
        {
            get { return objDataRepository.GetRegistration(PatientId); }
        }


    }
    public class RegistrationForm
    {
        public string txtPatientParentEmployedBy { get; set; }
        public string txtSpouseParentName { get; set; }
        public string txtSpouseParentEmployedBy { get; set; }
        public string txtWhoisResponsible { get; set; }



        public string txtResponsiblepartyFname { get; set; }
        public string txtResponsiblepartyLname { get; set; }
        
        public string txtemailaddress { get; set; }
        public string txtResponsibleRelationship { get; set; }
        public string txtResponsibleAddress { get; set; }
        public string txtResponsibleCity { get; set; }
        public string txtResponsibleState { get; set; }
        public string txtResponsibleZipCode { get; set; }
        public string txtResponsibleDOB { get; set; }
        public string txtResponsibleContact { get; set; }









        public string chkMethodOfPayment { get; set; }
        public string txtWhommay { get; set; }
        public string txtSomeonetonotify { get; set; }
        public string txtemergencyname { get; set; }
        public string txtemergency { get; set; }
        public bool Insurance { get; set; }
        public bool Cash { get; set; }
        public bool CrediteCard { get; set; }

        public string txtEmployeeName1 { get; set; }

        public string Insurance_Phone1 { get; set; }
        public string txtEmployeeDob1 { get; set; }
        public string txtEmployerName1 { get; set; }
        public string txtYearsEmployed1 { get; set; }
        public string txtNameofInsurance1 { get; set; }
        public string txtInsuranceAddress1 { get; set; }
        public string txtInsuranceTelephone1 { get; set; }

        public string txtEmployeeName2 { get; set; }
        public string Insurance_Phone2 { get; set; }
        public string txtEmployeeDob2 { get; set; }
        public string txtEmployerName2 { get; set; }
        public string txtYearsEmployed2 { get; set; }
        public string txtNameofInsurance2 { get; set; }
        public string txtInsuranceAddress2 { get; set; }
        public string txtInsuranceTelephone2 { get; set; }
        public string txtDigiSignreg { get; set; }
    }
    public class DentalHistory
    {
        public string txtQue1 { get; set; }
        public string txtQue2 { get; set; }
        public string txtQue3 { get; set; }
        public string txtQue4 { get; set; }
        public string txtQue5 { get; set; }
        public string txtQue5a { get; set; }
        public string txtQue5b { get; set; }
        public string txtQue5c { get; set; }
        public string txtQue6 { get; set; }
        public string txtQue7 { get; set; }
        public bool rdQue7a { get; set; }
        public bool rdQue8 { get; set; }
        public bool rdQue9 { get; set; }
        public string txtQue9a { get; set; }
        public bool rdQue10 { get; set; }
        public bool rdoQue11aFixedbridge { get; set; }

        public bool rdoQue11bRemoveablebridge { get; set; }

        public bool rdoQue11cDenture { get; set; }

        public bool rdQue11dImplant { get; set; }

        public string txtQue12 { get; set; }



        public bool rdQue15 { get; set; }
        public bool rdQue16 { get; set; }
        public bool rdQue17 { get; set; }
        public bool rdQue18 { get; set; }
        public bool rdQue19 { get; set; }
        public string chkQue20 { get; set; }
        public bool chkQue20_1 { get; set; }
        public bool chkQue20_2 { get; set; }
        public bool chkQue20_3 { get; set; }
        public bool chkQue20_4 { get; set; }
        public bool rdQue21 { get; set; }
        public string txtQue21a { get; set; }
        public string txtQue22 { get; set; }


        public string txtQue23 { get; set; }
        public bool rdQue24 { get; set; }

        public string txtQue26 { get; set; }

        public bool rdQue28 { get; set; }
        public string txtQue28a { get; set; }
        public string txtQue28b { get; set; }
        public string txtQue28c { get; set; }
        public string txtQue29 { get; set; }
        public string txtQue29a { get; set; }
        public bool rdQue30 { get; set; }

        public string txtComments { get; set; }

        public string txtDateoffirstvisit { get; set; }
        public string Reasonforfirstvisit { get; set; }
        public string Dateoflastvisit { get; set; }
        public string Reasonforlastvisit { get; set; }
        public string Dateofnextvisit { get; set; }
        public string Reasonfornextvisit { get; set; }


    }

    public class MedicalHistory
    {
        //section-1
        public bool MrdQue1 { get; set; }
        public bool MrnQue1 { get; set; }
        public string Mtxtphysicians { get; set; }
        public bool MrdQue2 { get; set; }
        public bool MrnQue2 { get; set; }
        public string Mtxthospitalized { get; set; }
        public bool MrdQue3 { get; set; }
        public bool MrnQue3 { get; set; }
        public string Mtxtserious { get; set; }
        public bool MrnQue4 { get; set; }
        public bool MrdQue4 { get; set; }
        public string Mtxtmedications { get; set; }
        public bool MrdQue5 { get; set; }
        public bool MrnQue5 { get; set; }
        public string MtxtRedux { get; set; }
        public bool MrdQue6 { get; set; }
        public bool MrnQue6 { get; set; }
        public string MtxtFosamax { get; set; }
        public bool MrdQuediet7 { get; set; }
        public bool MrnQuediet7 { get; set; }
        public string Mtxt7 { get; set; }
        public bool Mrdotobacco8 { get; set; }
        public bool Mrnotobacco8 { get; set; }
        public string Mtxt8 { get; set; }
        public bool Mrdosubstances { get; set; }
        public bool Mrnosubstances { get; set; }
        public string Mtxt9 { get; set; }
        public bool Mrdopregnant { get; set; }
        public bool Mrnopregnant { get; set; }
        public string Mtxt10 { get; set; }
        public bool Mrdocontraceptives { get; set; }
        public bool Mrnocontraceptives { get; set; }

        public string Mtxt11 { get; set; }
        public bool MrdoNursing { get; set; }
        public bool MrnoNursing { get; set; }
        public string Mtxt12 { get; set; }
        public bool Mrdotonsils { get; set; }
        public bool Mrnotonsils { get; set; }
        public string Mtxt13 { get; set; }
        public string Mtxtallergic { get; set; }
        public bool MchkQue_1 { get; set; }
        public bool MchkQueN_1 { get; set; }
        public bool MchkQue_2 { get; set; }
        public bool MchkQueN_2 { get; set; }
        public bool MchkQue_3 { get; set; }
        public bool MchkQueN_3 { get; set; }
        public bool MchkQue_4 { get; set; }
        public bool MchkQueN_4 { get; set; }
        public bool MchkQue_5 { get; set; }
        public bool MchkQueN_5 { get; set; }
        public bool MchkQue_6 { get; set; }
        public bool MchkQueN_6 { get; set; }
        public bool MchkQue_7 { get; set; }
        public bool MchkQueN_7 { get; set; }
        public bool MchkQue_8 { get; set; }
        public bool MchkQueN_8 { get; set; }
        public bool MchkQue_9 { get; set; }
        public bool MchkQueN_9 { get; set; }
        public string MtxtchkQue_9 { get; set; }
        
        //section-2
        public bool MrdQueAIDS_HIV_Positive { get; set; }
        public bool MrdQueAlzheimer { get; set; }
        public bool MrdQueAnaphylaxis { get; set; }
        public bool MrdQueBoneDisorders { get; set; }
        public bool MrdQueChemicalDependancy { get; set; }
        public bool MrdQueCortisoneTreatments { get; set; }
        public bool MrdQueEndocrineProblems { get; set; }
        public bool MrdQueExcessiveUrination { get; set; }
        public bool MrdQueNervousDisorder { get; set; }
        public bool MrdQuePneumonia { get; set; }
        public bool MrdQueProlongedBleending { get; set; }
        public bool MrdQueAnemia { get; set; }
        public bool MrdQueAngina { get; set; }
        public bool MrdQueArthritis_Gout { get; set; }
        public bool MrdQueArtificialHeartValve { get; set; }
        public bool MrdQueArtificialJoint { get; set; }
        public bool MrdQueAsthma { get; set; }
        public bool MrdQueBloodDisease { get; set; }
        public bool MrdQueBloodTransfusion { get; set; }
        public bool MrdQueBreathing { get; set; }
        public bool MrdQueBruise { get; set; }
        public bool MrdQueCancer { get; set; }
        public bool MrdQueChemotherapy { get; set; }
        public bool MrdQueChest { get; set; }
        public bool MrdQueCold_Sores_Fever { get; set; }
        public bool MrdQueCongenital { get; set; }
        public bool MrdQueConvulsions { get; set; }
        public bool MrdQueCortisone { get; set; }
        public bool MrdQueDiabetes { get; set; }
        public bool MrdQueDrug { get; set; }
        public bool MrdQueEasily { get; set; }
        public bool MrdQueEmphysema { get; set; }
        public bool MrdQueEpilepsy { get; set; }
        public bool MrdQueExcessiveBleeding { get; set; }
        public bool MrdQueExcessiveThirst { get; set; }
        public bool MrdQueFainting { get; set; }
        public bool MrdQueFrequentCough { get; set; }
        public bool MrdQueFrequentDiarrhea { get; set; }
        public bool MrdQueFrequentHeadaches { get; set; }
        public bool MrdQueGenital { get; set; }
        public bool MrdQueGlaucoma { get; set; }
        public bool MrdQueHay { get; set; }
        public bool MrdQueHeartAttack_Failure { get; set; }
        public bool MrdQueHeartMurmur { get; set; }
        public bool MrdQueHeartPacemaker { get; set; }
        public bool MrdQueHeartTrouble_Disease { get; set; }
        public bool MrdQueHemophilia { get; set; }
        public bool MrdQueHepatitisA { get; set; }
        public bool MrdQueHepatitisBorC { get; set; }
        public bool MrdQueHerpes { get; set; }
        public bool MrdQueHighBloodPressure { get; set; }
        public bool MrdQueHighCholesterol { get; set; }
        public bool MrdQueHives { get; set; }
        public bool MrdQueHypoglycemia { get; set; }
        public bool MrdQueIrregular { get; set; }
        public bool MrdQueKidney { get; set; }
        public bool MrdQueLeukemia { get; set; }
        public bool MrdQueLiver { get; set; }
        public bool MrdQueLow { get; set; }
        public bool MrdQueLung { get; set; }
        public bool MrdQueMitral { get; set; }
        public bool MrdQueOsteoporosis { get; set; }
        public bool MrdQuePain { get; set; }
        public bool MrdQueParathyroid { get; set; }
        public bool MrdQuePsychiatric { get; set; }
        public bool MrdQueRadiation { get; set; }
        public bool MrdQueRecent { get; set; }
        public bool MrdQueRenal { get; set; }
        public bool MrdQueRheumatic { get; set; }
        public bool MrdQueRheumatism { get; set; }
        public bool MrdQueScarlet { get; set; }
        public bool MrdQueShingles { get; set; }
        public bool MrdQueSickle { get; set; }
        public bool MrdQueSinus { get; set; }
        public bool MrdQueSpina { get; set; }
        public bool MrdQueStomach { get; set; }
        public bool MrdQueStroke { get; set; }
        public bool MrdQueSwelling { get; set; }
        public bool MrdQueThyroid { get; set; }
        public bool MrdQueTonsillitis { get; set; }
        public bool MrdQueTuberculosis { get; set; }
        public bool MrdQueTumors { get; set; }
        public bool MrdQueUlcers { get; set; }
        public bool MrdQueVenereal { get; set; }
        public bool MrdQueYellow { get; set; }

        public string Mtxtillness { get; set; }
        public string MtxtComments { get; set; }
        public bool MrdQueAbnormal { get; set; }
        public bool MrdQueThinners { get; set; }
        public bool MrdQueFibrillation { get; set; }
        public bool MrdQueCigarette { get; set; }
        public bool MrdQueCirulation { get; set; }
        public bool MrdQuePersistent { get; set; }
        public bool MrdQuefillers { get; set; }
        public bool MrdQueDigestive { get; set; }
        public bool MrdQueDizziness { get; set; }
        public bool MrdQueSubstances { get; set; }
        public bool MrdQueAlcoholic { get; set; }
        public bool MrdQueHeart_defect { get; set; }
        public bool MrdQueHiatal { get; set; }
        public bool MrdQueBlood_pressure { get; set; }
        public bool MrdQueHow_much { get; set; }
        public bool MrdQueHow_often { get; set; }
        public bool MrdQueReplacement { get; set; }
        public bool MrdQueNeck_BackProblem { get; set; }
        public bool MrdQuePainJaw_Joints { get; set; }
        public bool MrdQueEndocarditis { get; set; }
        public bool MrdQueShortness { get; set; }
        public bool MrdQueSjorgren { get; set; }
        public bool MrdQueSwollen { get; set; }
        public bool MrdQueValve { get; set; }
        public bool MrdQueVision { get; set; }
        public bool MrdQueUnexplained { get; set; }
        public bool MrdQueArthritis { get; set; }
    }


    public class DataRepository
    {
        clsPatientsData ObjPatientsData = new clsPatientsData();
        clsCommon ObjCommon = new clsCommon();

        static List<RegistrationForm> PatientListRegistrationForm = new List<RegistrationForm>();
        static List<BasicInfo> PatientListBasicInfo = new List<BasicInfo>();
        static List<MedicalHistory> PatientListMedicalHistoryForm = new List<MedicalHistory>();
        static List<DentalHistory> PatientListDentalHistoryForm = new List<DentalHistory>();




        public List<BasicInfo> GetPatientBasicInfo(int PatientId, string TimeZoneSystemName)
        {
            PatientListBasicInfo = new List<BasicInfo>();
            DataTable dt = new DataTable();
            dt = ObjPatientsData.GetPatientsDetails(PatientId);
            string strDateOfBirth = null;
            DateTime TempDateOfBirth;
            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    if(!string.IsNullOrEmpty(Convert.ToString(row["DateOfBirth"])))
                    {
                        //TempDateOfBirth = Convert.ToDateTime(row["DateOfBirth"]);
                        //TempDateOfBirth = clsHelper.ConvertFromUTC(TempDateOfBirth, TimeZoneSystemName);

                        //strDateOfBirth = TempDateOfBirth.ToString("MM/dd/yyyy");
                        // SUP-119 Removing UTC for DOB 
                        strDateOfBirth = Convert.ToString(row["DateOfBirth"]);
                    }
                    else
                    {
                        strDateOfBirth = null;
                    }
                    PatientListBasicInfo.Add(new BasicInfo()
                    { 
                        PatientId = Convert.ToInt32(row["PatientId"]),
                        Password = Convert.ToString(row["Password"]),
                        ConfirmPassword = Convert.ToString(row["Password"]),
                        AssignedPatientId = Convert.ToString(row["AssignedPatientId"]),
                        FirstName = Convert.ToString(row["FirstName"]),
                        LastName = Convert.ToString(row["LastName"]),
                        MiddelName = Convert.ToString(row["MiddelName"]),
                        Country = Convert.ToString(row["Country"]),
                        DateOfBirth = strDateOfBirth,
                        Gender = Convert.ToInt32(row["Gender"]),
                        ProfileImage = Convert.ToString(row["ProfileImage"]),
                        Phone = Convert.ToString(row["Phone"]),
                        Email = ObjCommon.CheckNull(Convert.ToString(row["Email"]),string.Empty),
                        Address = Convert.ToString(row["Address"]),
                        City = Convert.ToString(row["City"]),
                        State = Convert.ToString(row["State"]),
                        Zip = Convert.ToString(row["Zip"]),
                        Comments = Convert.ToString(row["Comments"]),
                        StartDate = Convert.ToString(row["StartDate"]),
                        Address2 = Convert.ToString(row["Address2"]),
                        GuarFirstName = Convert.ToString(row["GuarFirstName"]),
                        GuarLastName = Convert.ToString(row["GuarLastName"]),
                        GuarPhone = Convert.ToString(row["GuarPhone"]),
                        GuarEmail = Convert.ToString(row["GuarEmail"]),
                        FacebookUrl = Convert.ToString(row["FacebookUrl"]),
                        TwitterUrl = Convert.ToString(row["TwitterUrl"]),
                        LinkedinUrl = Convert.ToString(row["LinkedinUrl"]),
                        OwnerId = Convert.ToInt32(row["OwnerId"]),
                        Status = Convert.ToString(row["Status"]),
                        StayloggedMins = (Convert.ToString(row["StayloggedMins"]) == null || Convert.ToString(row["StayloggedMins"]) == "") ? "60" : Convert.ToString(row["StayloggedMins"]),
                        SecondaryEmail = Convert.ToString(row["SecondaryEmail"]),
                        txtWorkPhone = Convert.ToString(row["WorkPhone"]),

                    });
                }
            }
            return PatientListBasicInfo;
        }
        public List<RegistrationForm> GetRegistration(int PatientId)
        {
            PatientListRegistrationForm = new List<RegistrationForm>();
            RegistrationForm objRf = new RegistrationForm();
            DataTable dt = new DataTable();
            dt = ObjPatientsData.GetRegistration(PatientId, "GetRegistrationform");

            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    objRf.txtResponsiblepartyFname = Convert.ToString(row["txtResponsiblepartyFname"]);
                    objRf.txtResponsiblepartyLname = Convert.ToString(row["txtResponsiblepartyLname"]);
                    objRf.txtResponsibleRelationship = Convert.ToString(row["txtResponsibleRelationship"]);
                    objRf.txtResponsibleAddress = Convert.ToString(row["txtResponsibleAddress"]);
                    objRf.txtResponsibleCity = Convert.ToString(row["txtResponsibleCity"]);
                    objRf.txtResponsibleState = Convert.ToString(row["txtResponsibleState"]);
                    objRf.txtResponsibleZipCode = Convert.ToString(row["txtResponsibleZipCode"]);
                    objRf.txtResponsibleDOB = Convert.ToString(row["txtResponsibleDOB"]);
                    objRf.txtResponsibleContact = Convert.ToString(row["txtResponsibleContact"]);



                    objRf.chkMethodOfPayment = Convert.ToString(row["chkMethodOfPayment"]);
                    objRf.txtWhommay = Convert.ToString(row["txtWhommay"]);
                    objRf.txtSomeonetonotify = Convert.ToString(row["txtSomeonetonotify"]);
                    objRf.txtemergencyname = Convert.ToString(row["txtemergencyname"]);
                    objRf.txtemergency = Convert.ToString(row["txtemergency"]);
                    objRf.txtEmployeeName1 = Convert.ToString(row["txtEmployeeName1"]);
                    objRf.Insurance_Phone1 = Convert.ToString(row["txtInsurancePhone1"]);
                    objRf.txtEmployeeDob1 = Convert.ToString(row["txtEmployeeDob1"]);
                    objRf.txtEmployerName1 = Convert.ToString(row["txtEmployerName1"]);
                    objRf.txtYearsEmployed1 = Convert.ToString(row["txtYearsEmployed1"]);
                    objRf.txtNameofInsurance1 = Convert.ToString(row["txtNameofInsurance1"]);
                    objRf.txtInsuranceAddress1 = Convert.ToString(row["txtInsuranceAddress1"]);
                    objRf.txtInsuranceTelephone1 = Convert.ToString(row["txtInsuranceTelephone1"]);
                    objRf.txtEmployeeName2 = Convert.ToString(row["txtEmployeeName2"]);
                    objRf.Insurance_Phone2 = Convert.ToString(row["txtInsurancePhone2"]);
                    objRf.txtEmployeeDob2 = Convert.ToString(row["txtEmployeeDob2"]);
                    objRf.txtEmployerName2 = Convert.ToString(row["txtEmployerName2"]);
                    objRf.txtYearsEmployed2 = Convert.ToString(row["txtYearsEmployed2"]);
                    objRf.txtNameofInsurance2 = Convert.ToString(row["txtNameofInsurance2"]);
                    objRf.txtInsuranceAddress2 = Convert.ToString(row["txtInsuranceAddress2"]);
                    objRf.txtInsuranceTelephone2 = Convert.ToString(row["txtInsuranceTelephone2"]);
                    objRf.txtemailaddress = ObjCommon.CheckNull(Convert.ToString(row["txtemailaddress"]),string.Empty);

                    if (objRf.chkMethodOfPayment != null && objRf.chkMethodOfPayment != "")
                    {
                        string Values = objRf.chkMethodOfPayment;
                        if (Values.Contains("Insurance"))
                        {
                            objRf.Insurance = true;
                        }
                        else
                        {
                            objRf.Insurance = false;
                        }
                        if (Values.Contains("Cash"))
                        {
                            objRf.Cash = true;
                        }
                        else
                        {
                            objRf.Cash = false;
                        }

                        if (Values.Contains("Credit"))
                        {
                            objRf.CrediteCard = true;
                        }
                        else
                        {
                            objRf.CrediteCard = false;
                        }

                    }
                }
            }
            PatientListRegistrationForm.Add(objRf);
            return PatientListRegistrationForm;
        }
        public List<MedicalHistory> GetMedicalHistory(int PatientId)
        {
            try
            {
                PatientListMedicalHistoryForm = new List<MedicalHistory>();
                MedicalHistory objDH = new MedicalHistory();
                DataTable dt = new DataTable();
                dt = ObjPatientsData.GetMedicalHistory(PatientId, "GetMedicalHistory");
                if (dt != null && dt.Rows.Count > 0)
                {

                    foreach (DataRow row in dt.Rows)
                    {
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQue1"])))
                        {
                            string Values = Convert.ToString(row["MrdQue1"]);
                            objDH.MrdQue1 = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrNQue1"])))
                        {
                            string Values = Convert.ToString(row["MrNQue1"]);
                            objDH.MrnQue1 = (Values.Contains("Y")) ? true : false;
                        }
                        objDH.Mtxtphysicians = Convert.ToString(row["Mtxtphysicians"]);

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQue2"])))
                        {
                            string Values = Convert.ToString(row["MrdQue2"]);
                            objDH.MrdQue2 = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrnQue2"])))
                        {
                            string Values = Convert.ToString(row["MrnQue2"]);
                            objDH.MrnQue2 = (Values.Contains("Y")) ? true : false;
                        }
                        objDH.Mtxthospitalized = Convert.ToString(row["Mtxthospitalized"]);

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQue3"])))
                        {
                            string Values = Convert.ToString(row["MrdQue3"]);
                            objDH.MrdQue3 = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrnQue3"])))
                        {
                            string Values = Convert.ToString(row["MrnQue3"]);
                            objDH.MrnQue3 = (Values.Contains("Y")) ? true : false;
                        }
                        objDH.Mtxtserious = Convert.ToString(row["Mtxtserious"]);

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQue4"])))
                        {
                            string Values = Convert.ToString(row["MrdQue4"]);
                            objDH.MrdQue4 = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrnQue4"])))
                        {
                            string Values = Convert.ToString(row["MrnQue4"]);
                            objDH.MrnQue4 = (Values.Contains("Y")) ? true : false;
                        }
                        objDH.Mtxtmedications = Convert.ToString(row["Mtxtmedications"]);

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQue5"])))
                        {
                            string Values = Convert.ToString(row["MrdQue5"]);
                            objDH.MrdQue5 = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrnQue5"])))
                        {
                            string Values = Convert.ToString(row["MrnQue5"]);
                            objDH.MrnQue5 = (Values.Contains("Y")) ? true : false;
                        }
                        objDH.MtxtRedux = Convert.ToString(row["MtxtRedux"]);

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQue6"])))
                        {
                            string Values = Convert.ToString(row["MrdQue6"]);
                            objDH.MrdQue6 = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrnQue6"])))
                        {
                            string Values = Convert.ToString(row["MrnQue6"]);
                            objDH.MrnQue6 = (Values.Contains("Y")) ? true : false;
                        }
                        objDH.MtxtFosamax = Convert.ToString(row["MtxtFosamax"]);

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQuediet7"])))
                        {
                            string Values = Convert.ToString(row["MrdQuediet7"]);
                            objDH.MrdQuediet7 = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrnQuediet7"])))
                        {
                            string Values = Convert.ToString(row["MrnQuediet7"]);
                            objDH.MrnQuediet7 = (Values.Contains("Y")) ? true : false;
                        }
                        objDH.Mtxt7 = Convert.ToString(row["Mtxt7"]);

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["Mrdotobacco8"])))
                        {
                            string Values = Convert.ToString(row["Mrdotobacco8"]);
                            objDH.Mrdotobacco8 = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["Mrnotobacco8"])))
                        {
                            string Values = Convert.ToString(row["Mrnotobacco8"]);
                            objDH.Mrnotobacco8 = (Values.Contains("Y")) ? true : false;
                        }
                        objDH.Mtxt8 = Convert.ToString(row["Mtxt8"]);

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["Mrdosubstances"])))
                        {
                            string Values = Convert.ToString(row["Mrdosubstances"]);
                            objDH.Mrdosubstances = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["Mrnosubstances"])))
                        {
                            string Values = Convert.ToString(row["Mrnosubstances"]);
                            objDH.Mrnosubstances = (Values.Contains("Y")) ? true : false;
                        }
                        objDH.Mtxt9 = Convert.ToString(row["Mtxt9"]);

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["Mrdopregnant"])))
                        {
                            string Values = Convert.ToString(row["Mrdopregnant"]);
                            objDH.Mrdopregnant = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["Mrnopregnant"])))
                        {
                            string Values = Convert.ToString(row["Mrnopregnant"]);
                            objDH.Mrnopregnant = (Values.Contains("Y")) ? true : false;
                        }
                        objDH.Mtxt10 = Convert.ToString(row["Mtxt10"]);


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["Mrdocontraceptives"])))
                        {
                            string Values = Convert.ToString(row["Mrdocontraceptives"]);
                            objDH.Mrdocontraceptives = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["Mrnocontraceptives"])))
                        {
                            string Values = Convert.ToString(row["Mrnocontraceptives"]);
                            objDH.Mrnocontraceptives = (Values.Contains("Y")) ? true : false;
                        }
                        objDH.Mtxt11 = Convert.ToString(row["Mtxt11"]);

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdoNursing"])))
                        {
                            string Values = Convert.ToString(row["MrdoNursing"]);
                            objDH.MrdoNursing = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrnoNursing"])))
                        {
                            string Values = Convert.ToString(row["MrnoNursing"]);
                            objDH.MrnoNursing = (Values.Contains("Y")) ? true : false;
                        }
                        objDH.Mtxt12 = Convert.ToString(row["Mtxt12"]);

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["Mrdotonsils"])))
                        {
                            string Values = Convert.ToString(row["Mrdotonsils"]);
                            objDH.Mrdotonsils = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["Mrnotonsils"])))
                        {
                            string Values = Convert.ToString(row["Mrnotonsils"]);
                            objDH.Mrnotonsils = (Values.Contains("Y")) ? true : false;
                        }
                        objDH.Mtxt13 = Convert.ToString(row["Mtxt13"]);

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MchkQue_1"])))
                        {
                            string Values = Convert.ToString(row["MchkQue_1"]);
                            objDH.MchkQue_1 = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MchkQueN_1"])))
                        {
                            string Values = Convert.ToString(row["MchkQueN_1"]);
                            objDH.MchkQueN_1 = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MchkQue_2"])))
                        {
                            string Values = Convert.ToString(row["MchkQue_2"]);
                            objDH.MchkQue_2 = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MchkQueN_2"])))
                        {
                            string Values = Convert.ToString(row["MchkQueN_2"]);
                            objDH.MchkQueN_2 = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MchkQue_3"])))
                        {
                            string Values = Convert.ToString(row["MchkQue_3"]);
                            objDH.MchkQue_3 = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MchkQueN_3"])))
                        {
                            string Values = Convert.ToString(row["MchkQueN_3"]);
                            objDH.MchkQueN_3 = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MchkQue_4"])))
                        {
                            string Values = Convert.ToString(row["MchkQue_4"]);
                            objDH.MchkQue_4 = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MchkQueN_4"])))
                        {
                            string Values = Convert.ToString(row["MchkQueN_4"]);
                            objDH.MchkQueN_4 = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MchkQue_5"])))
                        {
                            string Values = Convert.ToString(row["MchkQue_5"]);
                            objDH.MchkQue_5 = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MchkQueN_5"])))
                        {
                            string Values = Convert.ToString(row["MchkQueN_5"]);
                            objDH.MchkQueN_5 = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MchkQue_6"])))
                        {
                            string Values = Convert.ToString(row["MchkQue_6"]);
                            objDH.MchkQue_6 = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MchkQueN_6"])))
                        {
                            string Values = Convert.ToString(row["MchkQueN_6"]);
                            objDH.MchkQueN_6 = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MchkQue_7"])))
                        {
                            string Values = Convert.ToString(row["MchkQue_7"]);
                            objDH.MchkQue_7 = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MchkQueN_7"])))
                        {
                            string Values = Convert.ToString(row["MchkQueN_7"]);
                            objDH.MchkQueN_7 = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MchkQue_8"])))
                        {
                            string Values = Convert.ToString(row["MchkQue_8"]);
                            objDH.MchkQue_8 = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MchkQueN_8"])))
                        {
                            string Values = Convert.ToString(row["MchkQueN_8"]);
                            objDH.MchkQueN_8 = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MchkQue_9"])))
                        {
                            string Values = Convert.ToString(row["MchkQue_9"]);
                            objDH.MchkQue_9 = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MchkQueN_9"])))
                        {
                            string Values = Convert.ToString(row["MchkQueN_9"]);
                            objDH.MchkQueN_9 = (Values.Contains("Y")) ? true : false;
                        }

                        objDH.MtxtchkQue_9 = Convert.ToString(row["MtxtchkQue_9"]);


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueAIDS_HIV_Positive"])))
                        {
                            string Values = Convert.ToString(row["MrdQueAIDS_HIV_Positive"]);
                            objDH.MrdQueAIDS_HIV_Positive = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueAlzheimer"])))
                        {
                            string Values = Convert.ToString(row["MrdQueAlzheimer"]);
                            objDH.MrdQueAlzheimer = (Values.Contains("Y")) ? true : false;
                        }



                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueAnaphylaxis"])))
                        {
                            string Values = Convert.ToString(row["MrdQueAnaphylaxis"]);
                            objDH.MrdQueAnaphylaxis = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueAnemia"])))
                        {
                            string Values = Convert.ToString(row["MrdQueAnemia"]);
                            objDH.MrdQueAnemia = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueAngina"])))
                        {
                            string Values = Convert.ToString(row["MrdQueAngina"]);
                            objDH.MrdQueAngina = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueArthritis_Gout"])))
                        {
                            string Values = Convert.ToString(row["MrdQueArthritis_Gout"]);
                            objDH.MrdQueArthritis_Gout = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueArtificialHeartValve"])))
                        {
                            string Values = Convert.ToString(row["MrdQueArtificialHeartValve"]);
                            objDH.MrdQueArtificialHeartValve = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueArtificialJoint"])))
                        {
                            string Values = Convert.ToString(row["MrdQueArtificialJoint"]);
                            objDH.MrdQueArtificialJoint = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueAsthma"])))
                        {
                            string Values = Convert.ToString(row["MrdQueAsthma"]);
                            objDH.MrdQueAsthma = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueBloodDisease"])))
                        {
                            string Values = Convert.ToString(row["MrdQueBloodDisease"]);
                            objDH.MrdQueBloodDisease = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueBloodTransfusion"])))
                        {
                            string Values = Convert.ToString(row["MrdQueBloodTransfusion"]);
                            objDH.MrdQueBloodTransfusion = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueBreathing"])))
                        {
                            string Values = Convert.ToString(row["MrdQueBreathing"]);
                            objDH.MrdQueBreathing = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueBruise"])))
                        {
                            string Values = Convert.ToString(row["MrdQueBruise"]);
                            objDH.MrdQueBruise = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueCancer"])))
                        {
                            string Values = Convert.ToString(row["MrdQueCancer"]);
                            objDH.MrdQueCancer = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueChemicalDependancy"])))
                        {
                            string Values = Convert.ToString(row["MrdQueChemicalDependancy"]);
                            objDH.MrdQueChemicalDependancy = (Values.Contains("Y")) ? true : false;
                        }




                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueChemotherapy"])))
                        {
                            string Values = Convert.ToString(row["MrdQueChemotherapy"]);
                            objDH.MrdQueChemotherapy = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueChest"])))
                        {
                            string Values = Convert.ToString(row["MrdQueChest"]);
                            objDH.MrdQueChest = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueCold_Sores_Fever"])))
                        {
                            string Values = Convert.ToString(row["MrdQueCold_Sores_Fever"]);
                            objDH.MrdQueCold_Sores_Fever = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueCongenital"])))
                        {
                            string Values = Convert.ToString(row["MrdQueCongenital"]);
                            objDH.MrdQueCongenital = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueConvulsions"])))
                        {
                            string Values = Convert.ToString(row["MrdQueConvulsions"]);
                            objDH.MrdQueConvulsions = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueCortisone"])))
                        {
                            string Values = Convert.ToString(row["MrdQueCortisone"]);
                            objDH.MrdQueCortisone = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueCortisoneTretments"])))
                        {
                            string Values = Convert.ToString(row["MrdQueCortisoneTretments"]);
                            objDH.MrdQueCortisoneTreatments = (Values.Contains("Y")) ? true : false;
                        }



                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueDiabetes"])))
                        {
                            string Values = Convert.ToString(row["MrdQueDiabetes"]);
                            objDH.MrdQueDiabetes = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueDrug"])))
                        {
                            string Values = Convert.ToString(row["MrdQueDrug"]);
                            objDH.MrdQueDrug = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueEasily"])))
                        {
                            string Values = Convert.ToString(row["MrdQueEasily"]);
                            objDH.MrdQueEasily = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueEmphysema"])))
                        {
                            string Values = Convert.ToString(row["MrdQueEmphysema"]);
                            objDH.MrdQueEmphysema = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueEndocrineProblems"])))
                        {
                            string Values = Convert.ToString(row["MrdQueEndocrineProblems"]);
                            objDH.MrdQueEndocrineProblems = (Values.Contains("Y")) ? true : false;
                        }



                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueEpilepsy"])))
                        {
                            string Values = Convert.ToString(row["MrdQueEpilepsy"]);
                            objDH.MrdQueEpilepsy = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueExcessiveBleeding"])))
                        {
                            string Values = Convert.ToString(row["MrdQueExcessiveBleeding"]);
                            objDH.MrdQueExcessiveBleeding = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueExcessiveThirst"])))
                        {
                            string Values = Convert.ToString(row["MrdQueExcessiveThirst"]);
                            objDH.MrdQueExcessiveThirst = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueExcessiveUrination"])))
                        {
                            string Values = Convert.ToString(row["MrdQueExcessiveUrination"]);
                            objDH.MrdQueExcessiveUrination = (Values.Contains("Y")) ? true : false;
                        }



                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueFainting"])))
                        {
                            string Values = Convert.ToString(row["MrdQueFainting"]);
                            objDH.MrdQueFainting = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueFrequentCough"])))
                        {
                            string Values = Convert.ToString(row["MrdQueFrequentCough"]);
                            objDH.MrdQueFrequentCough = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueFrequentDiarrhea"])))
                        {
                            string Values = Convert.ToString(row["MrdQueFrequentDiarrhea"]);
                            objDH.MrdQueFrequentDiarrhea = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueFrequentHeadaches"])))
                        {
                            string Values = Convert.ToString(row["MrdQueFrequentHeadaches"]);
                            objDH.MrdQueFrequentHeadaches = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueGenital"])))
                        {
                            string Values = Convert.ToString(row["MrdQueGenital"]);
                            objDH.MrdQueGenital = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueGlaucoma"])))
                        {
                            string Values = Convert.ToString(row["MrdQueGlaucoma"]);
                            objDH.MrdQueGlaucoma = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueHay"])))
                        {
                            string Values = Convert.ToString(row["MrdQueHay"]);
                            objDH.MrdQueHay = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueHeartAttack_Failure"])))
                        {
                            string Values = Convert.ToString(row["MrdQueHeartAttack_Failure"]);
                            objDH.MrdQueHeartAttack_Failure = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueHeartMurmur"])))
                        {
                            string Values = Convert.ToString(row["MrdQueHeartMurmur"]);
                            objDH.MrdQueHeartMurmur = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueHeartPacemaker"])))
                        {
                            string Values = Convert.ToString(row["MrdQueHeartPacemaker"]);
                            objDH.MrdQueHeartPacemaker = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueHeartTrouble_Disease"])))
                        {
                            string Values = Convert.ToString(row["MrdQueHeartTrouble_Disease"]);
                            objDH.MrdQueHeartTrouble_Disease = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueHemophilia"])))
                        {
                            string Values = Convert.ToString(row["MrdQueHemophilia"]);
                            objDH.MrdQueHemophilia = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueHepatitisA"])))
                        {
                            string Values = Convert.ToString(row["MrdQueHepatitisA"]);
                            objDH.MrdQueHepatitisA = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueHepatitisBorC"])))
                        {
                            string Values = Convert.ToString(row["MrdQueHepatitisBorC"]);
                            objDH.MrdQueHepatitisBorC = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueHerpes"])))
                        {
                            string Values = Convert.ToString(row["MrdQueHerpes"]);
                            objDH.MrdQueHerpes = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueHighBloodPressure"])))
                        {
                            string Values = Convert.ToString(row["MrdQueHighBloodPressure"]);
                            objDH.MrdQueHighBloodPressure = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueHighCholesterol"])))
                        {
                            string Values = Convert.ToString(row["MrdQueHighCholesterol"]);
                            objDH.MrdQueHighCholesterol = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueHives"])))
                        {
                            string Values = Convert.ToString(row["MrdQueHives"]);
                            objDH.MrdQueHives = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueHypoglycemia"])))
                        {
                            string Values = Convert.ToString(row["MrdQueHypoglycemia"]);
                            objDH.MrdQueHypoglycemia = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueIrregular"])))
                        {
                            string Values = Convert.ToString(row["MrdQueIrregular"]);
                            objDH.MrdQueIrregular = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueKidney"])))
                        {
                            string Values = Convert.ToString(row["MrdQueKidney"]);
                            objDH.MrdQueKidney = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueLeukemia"])))
                        {
                            string Values = Convert.ToString(row["MrdQueLeukemia"]);
                            objDH.MrdQueLeukemia = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueLiver"])))
                        {
                            string Values = Convert.ToString(row["MrdQueLiver"]);
                            objDH.MrdQueLiver = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueLow"])))
                        {
                            string Values = Convert.ToString(row["MrdQueLow"]);
                            objDH.MrdQueLow = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueLung"])))
                        {
                            string Values = Convert.ToString(row["MrdQueLung"]);
                            objDH.MrdQueLung = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueMitral"])))
                        {
                            string Values = Convert.ToString(row["MrdQueMitral"]);
                            objDH.MrdQueMitral = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueNervousDisorder"])))
                        {
                            string Values = Convert.ToString(row["MrdQueNervousDisorder"]);
                            objDH.MrdQueNervousDisorder = (Values.Contains("Y")) ? true : false;
                        }



                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueOsteoporosis"])))
                        {
                            string Values = Convert.ToString(row["MrdQueOsteoporosis"]);
                            objDH.MrdQueOsteoporosis = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQuePain"])))
                        {
                            string Values = Convert.ToString(row["MrdQuePain"]);
                            objDH.MrdQuePain = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueParathyroid"])))
                        {
                            string Values = Convert.ToString(row["MrdQueParathyroid"]);
                            objDH.MrdQueParathyroid = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQuePsychiatric"])))
                        {
                            string Values = Convert.ToString(row["MrdQuePsychiatric"]);
                            objDH.MrdQuePsychiatric = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueRadiation"])))
                        {
                            string Values = Convert.ToString(row["MrdQueRadiation"]);
                            objDH.MrdQueRadiation = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueRecent"])))
                        {
                            string Values = Convert.ToString(row["MrdQueRecent"]);
                            objDH.MrdQueRecent = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueRenal"])))
                        {
                            string Values = Convert.ToString(row["MrdQueRenal"]);
                            objDH.MrdQueRenal = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueRheumatic"])))
                        {
                            string Values = Convert.ToString(row["MrdQueRheumatic"]);
                            objDH.MrdQueRheumatic = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueRheumatism"])))
                        {
                            string Values = Convert.ToString(row["MrdQueRheumatism"]);
                            objDH.MrdQueRheumatism = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueScarlet"])))
                        {
                            string Values = Convert.ToString(row["MrdQueScarlet"]);
                            objDH.MrdQueScarlet = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueShingles"])))
                        {
                            string Values = Convert.ToString(row["MrdQueShingles"]);
                            objDH.MrdQueShingles = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueSickle"])))
                        {
                            string Values = Convert.ToString(row["MrdQueSickle"]);
                            objDH.MrdQueSickle = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueSinus"])))
                        {
                            string Values = Convert.ToString(row["MrdQueSinus"]);
                            objDH.MrdQueSinus = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueSpina"])))
                        {
                            string Values = Convert.ToString(row["MrdQueSpina"]);
                            objDH.MrdQueSpina = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueStomach"])))
                        {
                            string Values = Convert.ToString(row["MrdQueStomach"]);
                            objDH.MrdQueStomach = (Values.Contains("Y")) ? true : false;
                        }


                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueStroke"])))
                        {
                            string Values = Convert.ToString(row["MrdQueStroke"]);
                            objDH.MrdQueStroke = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueSwelling"])))
                        {
                            string Values = Convert.ToString(row["MrdQueSwelling"]);
                            objDH.MrdQueSwelling = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueThyroid"])))
                        {
                            string Values = Convert.ToString(row["MrdQueThyroid"]);
                            objDH.MrdQueThyroid = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueTonsillitis"])))
                        {
                            string Values = Convert.ToString(row["MrdQueTonsillitis"]);
                            objDH.MrdQueTonsillitis = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueTuberculosis"])))
                        {
                            string Values = Convert.ToString(row["MrdQueTuberculosis"]);
                            objDH.MrdQueTuberculosis = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueTumors"])))
                        {
                            string Values = Convert.ToString(row["MrdQueTumors"]);
                            objDH.MrdQueTumors = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueUlcers"])))
                        {
                            string Values = Convert.ToString(row["MrdQueUlcers"]);
                            objDH.MrdQueUlcers = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueVenereal"])))
                        {
                            string Values = Convert.ToString(row["MrdQueVenereal"]);
                            objDH.MrdQueVenereal = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueYellow"])))
                        {
                            string Values = Convert.ToString(row["MrdQueYellow"]);
                            objDH.MrdQueYellow = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueAbnormal"])))
                        {
                            string Values = Convert.ToString(row["MrdQueAbnormal"]);
                            objDH.MrdQueAbnormal = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueThinners"])))
                        {
                            string Values = Convert.ToString(row["MrdQueThinners"]);
                            objDH.MrdQueThinners = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueFibrillation"])))
                        {
                            string Values = Convert.ToString(row["MrdQueFibrillation"]);
                            objDH.MrdQueFibrillation = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueCigarette"])))
                        {
                            string Values = Convert.ToString(row["MrdQueCigarette"]);
                            objDH.MrdQueCigarette = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueCirulation"])))
                        {
                            string Values = Convert.ToString(row["MrdQueCirulation"]);
                            objDH.MrdQueCirulation = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQuePersistent"])))
                        {
                            string Values = Convert.ToString(row["MrdQuePersistent"]);
                            objDH.MrdQuePersistent = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQuefillers"])))
                        {
                            string Values = Convert.ToString(row["MrdQuefillers"]);
                            objDH.MrdQuefillers = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueDigestive"])))
                        {
                            string Values = Convert.ToString(row["MrdQueDigestive"]);
                            objDH.MrdQueDigestive = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueDizziness"])))
                        {
                            string Values = Convert.ToString(row["MrdQueDizziness"]);
                            objDH.MrdQueDizziness = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueSubstances"])))
                        {
                            string Values = Convert.ToString(row["MrdQueSubstances"]);
                            objDH.MrdQueSubstances = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueAlcoholic"])))
                        {
                            string Values = Convert.ToString(row["MrdQueAlcoholic"]);
                            objDH.MrdQueAlcoholic = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueHeart_defect"])))
                        {
                            string Values = Convert.ToString(row["MrdQueHeart_defect"]);
                            objDH.MrdQueHeart_defect = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueHiatal"])))
                        {
                            string Values = Convert.ToString(row["MrdQueHiatal"]);
                            objDH.MrdQueHiatal = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueBlood_pressure"])))
                        {
                            string Values = Convert.ToString(row["MrdQueBlood_pressure"]);
                            objDH.MrdQueBlood_pressure = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueHow_much"])))
                        {
                            string Values = Convert.ToString(row["MrdQueHow_much"]);
                            objDH.MrdQueHow_much = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueHow_often"])))
                        {
                            string Values = Convert.ToString(row["MrdQueHow_often"]);
                            objDH.MrdQueHow_often = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueReplacement"])))
                        {
                            string Values = Convert.ToString(row["MrdQueReplacement"]);
                            objDH.MrdQueReplacement = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueNeck_BackProblem"])))
                        {
                            string Values = Convert.ToString(row["MrdQueNeck_BackProblem"]);
                            objDH.MrdQueNeck_BackProblem = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQuePainJaw_Joints"])))
                        {
                            string Values = Convert.ToString(row["MrdQuePainJaw_Joints"]);
                            objDH.MrdQuePainJaw_Joints = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueEndocarditis"])))
                        {
                            string Values = Convert.ToString(row["MrdQueEndocarditis"]);
                            objDH.MrdQueEndocarditis = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueShortness"])))
                        {
                            string Values = Convert.ToString(row["MrdQueShortness"]);
                            objDH.MrdQueShortness = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueSjorgren"])))
                        {
                            string Values = Convert.ToString(row["MrdQueSjorgren"]);
                            objDH.MrdQueSjorgren = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueSwollen"])))
                        {
                            string Values = Convert.ToString(row["MrdQueSwollen"]);
                            objDH.MrdQueSwollen = (Values.Contains("Y")) ? true : false;
                        }

                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueValve"])))
                        {
                            string Values = Convert.ToString(row["MrdQueValve"]);
                            objDH.MrdQueValve = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueVision"])))
                        {
                            string Values = Convert.ToString(row["MrdQueVision"]);
                            objDH.MrdQueVision = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueUnexplained"])))
                        {
                            string Values = Convert.ToString(row["MrdQueUnexplained"]);
                            objDH.MrdQueUnexplained = (Values.Contains("Y")) ? true : false;
                        }
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(row["MrdQueArthritis"])))
                        {
                            string Values = Convert.ToString(row["MrdQueArthritis"]);
                            objDH.MrdQueArthritis = (Values.Contains("Y")) ? true : false;
                        }

                        objDH.Mtxtillness = Convert.ToString(row["Mtxtillness"]);
                        objDH.MtxtComments = Convert.ToString(row["MtxtComments"]);

                    }
                }
                PatientListMedicalHistoryForm.Add(objDH);
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("DentalFormMaster--GetMedicalHistory", Ex.Message, Ex.StackTrace);
            }
            return PatientListMedicalHistoryForm;
        }
        public List<DentalHistory> GetDentalHistory(int PatientId)
        {
            PatientListDentalHistoryForm = new List<DentalHistory>();
            DentalHistory objDH = new DentalHistory();
            DataTable dt = new DataTable();
            dt = ObjPatientsData.GetDentalHistory(PatientId, "GetDentalHistory");
            if (dt != null && dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    objDH.txtQue1 = Convert.ToString(row["txtQue1"]);
                    objDH.txtQue2 = Convert.ToString(row["txtQue2"]);
                    objDH.txtQue3 = Convert.ToString(row["txtQue3"]);
                    objDH.txtQue4 = Convert.ToString(row["txtQue4"]);
                    objDH.txtQue5 = Convert.ToString(row["txtQue5"]);
                    objDH.txtQue5a = Convert.ToString(row["txtQue5a"]);
                    objDH.txtQue5b = Convert.ToString(row["txtQue5b"]);
                    objDH.txtQue5c = Convert.ToString(row["txtQue5c"]);
                    objDH.txtQue6 = Convert.ToString(row["txtQue6"]);
                    objDH.txtQue7 = Convert.ToString(row["txtQue7"]);

                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["rdQue7a"])))
                    {
                        string Values = Convert.ToString(row["rdQue7a"]);
                        objDH.rdQue7a = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["rdQue8"])))
                    {
                        string Values = Convert.ToString(row["rdQue8"]);
                        objDH.rdQue8 = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["rdQue9"])))
                    {
                        string Values = Convert.ToString(row["rdQue9"]);
                        objDH.rdQue9 = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.txtQue9a = Convert.ToString(row["txtQue9a"]);

                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["rdQue10"])))
                    {
                        string Values = Convert.ToString(row["rdQue10"]);
                        objDH.rdQue10 = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["rdoQue11aFixedbridge"])))
                    {
                        string Values = Convert.ToString(row["rdoQue11aFixedbridge"]);
                        objDH.rdoQue11aFixedbridge = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["rdoQue11bRemoveablebridge"])))
                    {
                        string Values = Convert.ToString(row["rdoQue11bRemoveablebridge"]);
                        objDH.rdoQue11bRemoveablebridge = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["rdoQue11cDenture"])))
                    {
                        string Values = Convert.ToString(row["rdoQue11cDenture"]);
                        objDH.rdoQue11cDenture = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["rdQue11dImplant"])))
                    {
                        string Values = Convert.ToString(row["rdQue11dImplant"]);
                        objDH.rdQue11dImplant = (Values.Contains("Y")) ? true : false;
                    }

                    objDH.txtQue12 = Convert.ToString(row["txtQue12"]);

                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["rdQue15"])))
                    {
                        string Values = Convert.ToString(row["rdQue15"]);
                        objDH.rdQue15 = (Values.Contains("Y")) ? true : false;
                    }


                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["rdQue16"])))
                    {
                        string Values = Convert.ToString(row["rdQue16"]);
                        objDH.rdQue16 = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["rdQue17"])))
                    {
                        string Values = Convert.ToString(row["rdQue17"]);
                        objDH.rdQue17 = Values.Contains("Y") ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["rdQue18"])))
                    {
                        string Values = Convert.ToString(row["rdQue18"]);
                        objDH.rdQue18 = (Values.Contains("Y")) ? true : false;
                    }

                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["rdQue19"])))
                    {
                        string Values = Convert.ToString(row["rdQue19"]);
                        objDH.rdQue19 = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.chkQue20 = Convert.ToString(row["chkQue20"]);

                    if (objDH.chkQue20 != null && objDH.chkQue20 != "")
                    {
                        string Values = objDH.chkQue20;
                        objDH.chkQue20_1 = (Values.Contains("1")) ? true : false;
                        objDH.chkQue20_2 = (Values.Contains("2")) ? true : false;
                        objDH.chkQue20_3 = (Values.Contains("3")) ? true : false;
                        objDH.chkQue20_4 = (Values.Contains("4")) ? true : false;
                    }
                    

                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["rdQue21"])))
                    {
                        string Values = Convert.ToString(row["rdQue21"]);
                        objDH.rdQue21 = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.txtQue21a = Convert.ToString(row["txtQue21a"]);
                    objDH.txtQue22 = Convert.ToString(row["txtQue22"]);

                    objDH.txtQue23 = Convert.ToString(row["txtQue23"]);

                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["rdQue24"])))
                    {
                        string Values = Convert.ToString(row["rdQue24"]);
                        objDH.rdQue24 = (Values.Contains("Y")) ? true : false;
                    }


                    objDH.txtQue26 = Convert.ToString(row["txtQue26"]);


                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["rdQue28"])))
                    {
                        string Values = Convert.ToString(row["rdQue28"]);
                        objDH.rdQue28 = (Values.Contains("Y")) ? true : false;
                    }
                    objDH.txtQue28a = Convert.ToString(row["txtQue28a"]);
                    objDH.txtQue28b = Convert.ToString(row["txtQue28b"]);
                    objDH.txtQue28c = Convert.ToString(row["txtQue28c"]);
                    objDH.txtQue29 = Convert.ToString(row["txtQue29"]);
                    objDH.txtQue29a = Convert.ToString(row["txtQue29a"]);
                    if (!string.IsNullOrWhiteSpace(Convert.ToString(row["rdQue30"])))
                    {
                        string Values = Convert.ToString(row["rdQue30"]);
                        objDH.rdQue30 = (Values.Contains("Y")) ? true : false;
                    }

                    objDH.txtComments = Convert.ToString(row["txtComments"]);
                    objDH.Reasonforfirstvisit = ObjCommon.CheckNull(Convert.ToString(row["Reasonforfirstvisit"]),string.Empty);
                    objDH.Dateoflastvisit = Convert.ToString(row["Dateoflastvisit"]);
                    objDH.Reasonforlastvisit = Convert.ToString(row["Reasonforlastvisit"]);
                    objDH.Dateofnextvisit = Convert.ToString(row["Dateofnextvisit"]);
                    objDH.Reasonfornextvisit = Convert.ToString(row["Reasonfornextvisit"]);
                    objDH.txtDateoffirstvisit = Convert.ToString(row["firstvistdate"]); 
                }
            }
            PatientListDentalHistoryForm.Add(objDH);
            return PatientListDentalHistoryForm;
        }

    }
}