﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccessLayer.PatientsData;
using DataAccessLayer.Common;

namespace NewRecordlinc.Models.Patients
{

    public class mdlDentalForms
    {
        public int PatientId { get; set; }
        public List<PersonalInfomationForm> lstPersonalInfomationForm = new List<PersonalInfomationForm>();
        public List<InsuranceCoverageForm> lstInsuranceCoverageForm = new List<InsuranceCoverageForm>();
        public List<MedicalHistoryForm> lstMedicalHistoryForm = new List<MedicalHistoryForm>();
        public List<DentalHistoryForm> lstDentalHistoryForm = new List<DentalHistoryForm>();



        #region Code For Get Patient Information Form Details
        public List<PersonalInfomationForm> GetPersonalInfomationForm(int PatientId)
        {
            try
            {
                lstPersonalInfomationForm = new List<PersonalInfomationForm>();

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstPersonalInfomationForm;
        }
        #endregion
    }




    public class PersonalInfomationForm
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        public string DateofBirth { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PrimaryPhoneNumber { get; set; }

        public string SecondaryPhoneNumber { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyContactPhoneNumber { get; set; }
    }
    public class InsuranceCoverageForm
    {

        public string txtResponsiblepartyFname { get; set; }
        public string txtResponsiblepartyLname { get; set; }
        public string txtResponsibleRelationship { get; set; }
        public string txtResponsibleAddress { get; set; }
        public string txtResponsibleCity { get; set; }
        public string txtResponsibleState { get; set; }
        public string txtResponsibleZipCode { get; set; }
        public string txtResponsiblePhone { get; set; }


        public string txtInsuranceCompany1 { get; set; }
        public string txtNameofInsured1 { get; set; }
        public string txtDateofBirth1 { get; set; }
        public string txtMemberID1 { get; set; }
        public string txtGroupNumber1 { get; set; }

        public string txtInsuranceCompany2 { get; set; }
        public string txtNameofInsured2 { get; set; }
        public string txtDateofBirth2 { get; set; }
        public string txtMemberID2 { get; set; }
        public string txtGroupNumber2 { get; set; }

    }
    public class MedicalHistoryForm
    {
        //section-1
        public bool MrdQue1 { get; set; }
        public string Mtxtphysicians { get; set; }
        public bool MrdQue2 { get; set; }
        public string Mtxthospitalized { get; set; }
        public bool MrdQue3 { get; set; }
        public string Mtxtserious { get; set; }
        public bool MrdQue4 { get; set; }
        public string Mtxtmedications { get; set; }
        public bool MrdQue5 { get; set; }
        public string MtxtRedux { get; set; }
        public bool MrdQue6 { get; set; }
        public string MtxtFosamax { get; set; }

        public bool MrdQuediet7 { get; set; }
        public string Mtxt7 { get; set; }
        public bool Mrdotobacco8 { get; set; }
        public string Mtxt8 { get; set; }
        public bool Mrdosubstances { get; set; }
        public string Mtxt9 { get; set; }
        public bool Mrdopregnant { get; set; }
        public string Mtxt10 { get; set; }
        public bool Mrdocontraceptives { get; set; }
        public bool MrdoNursing { get; set; }

        public string Mtxtallergic { get; set; }
        public bool MchkQue_1 { get; set; }
        public bool MchkQue_2 { get; set; }
        public bool MchkQue_3 { get; set; }
        public bool MchkQue_4 { get; set; }
        public bool MchkQue_5 { get; set; }
        public bool MchkQue_6 { get; set; }
        public bool MchkQue_7 { get; set; }
        public bool MchkQue_8 { get; set; }
        public bool MchkQue_9 { get; set; }
        public string MtxtchkQue_9 { get; set; }

        //section-2

        public bool MrdQueAIDS_HIV_Positive { get; set; }
        public bool MrdQueAlzheimer { get; set; }
        public bool MrdQueAnaphylaxis { get; set; }
        public bool MrdQueAnemia { get; set; }
        public bool MrdQueAngina { get; set; }
        public bool MrdQueArthritis_Gout { get; set; }
        public bool MrdQueArtificialHeartValve { get; set; }
        public bool MrdQueArtificialJoint { get; set; }
        public bool MrdQueAsthma { get; set; }
        public bool MrdQueBloodDisease { get; set; }
        public bool MrdQueBloodTransfusion { get; set; }
        public bool MrdQueBreathing { get; set; }
        public bool MrdQueBruise { get; set; }
        public bool MrdQueCancer { get; set; }
        public bool MrdQueChemotherapy { get; set; }
        public bool MrdQueChest { get; set; }
        public bool MrdQueCold_Sores_Fever { get; set; }
        public bool MrdQueCongenital { get; set; }
        public bool MrdQueConvulsions { get; set; }
        public bool MrdQueCortisone { get; set; }
        public bool MrdQueDiabetes { get; set; }
        public bool MrdQueDrug { get; set; }
        public bool MrdQueEasily { get; set; }
        public bool MrdQueEmphysema { get; set; }
        public bool MrdQueEpilepsy { get; set; }
        public bool MrdQueExcessiveBleeding { get; set; }
        public bool MrdQueExcessiveThirst { get; set; }
        public bool MrdQueFainting { get; set; }
        public bool MrdQueFrequentCough { get; set; }
        public bool MrdQueFrequentDiarrhea { get; set; }
        public bool MrdQueFrequentHeadaches { get; set; }
        public bool MrdQueGenital { get; set; }
        public bool MrdQueGlaucoma { get; set; }
        public bool MrdQueHay { get; set; }
        public bool MrdQueHeartAttack_Failure { get; set; }
        public bool MrdQueHeartMurmur { get; set; }
        public bool MrdQueHeartPacemaker { get; set; }
        public bool MrdQueHeartTrouble_Disease { get; set; }
        public bool MrdQueHemophilia { get; set; }
        public bool MrdQueHepatitisA { get; set; }
        public bool MrdQueHepatitisBorC { get; set; }
        public bool MrdQueHerpes { get; set; }
        public bool MrdQueHighBloodPressure { get; set; }
        public bool MrdQueHighCholesterol { get; set; }
        public bool MrdQueHives { get; set; }
        public bool MrdQueHypoglycemia { get; set; }
        public bool MrdQueIrregular { get; set; }
        public bool MrdQueKidney { get; set; }
        public bool MrdQueLeukemia { get; set; }
        public bool MrdQueLiver { get; set; }
        public bool MrdQueLow { get; set; }
        public bool MrdQueLung { get; set; }
        public bool MrdQueMitral { get; set; }
        public bool MrdQueOsteoporosis { get; set; }
        public bool MrdQuePain { get; set; }
        public bool MrdQueParathyroid { get; set; }
        public bool MrdQuePsychiatric { get; set; }
        public bool MrdQueRadiation { get; set; }
        public bool MrdQueRecent { get; set; }
        public bool MrdQueRenal { get; set; }
        public bool MrdQueRheumatic { get; set; }
        public bool MrdQueRheumatism { get; set; }
        public bool MrdQueScarlet { get; set; }
        public bool MrdQueShingles { get; set; }
        public bool MrdQueSickle { get; set; }
        public bool MrdQueSinus { get; set; }
        public bool MrdQueSpina { get; set; }
        public bool MrdQueStomach { get; set; }
        public bool MrdQueStroke { get; set; }
        public bool MrdQueSwelling { get; set; }
        public bool MrdQueThyroid { get; set; }
        public bool MrdQueTonsillitis { get; set; }
        public bool MrdQueTuberculosis { get; set; }
        public bool MrdQueTumors { get; set; }
        public bool MrdQueUlcers { get; set; }
        public bool MrdQueVenereal { get; set; }
        public bool MrdQueYellow { get; set; }

        public string Mtxtillness { get; set; }
        public string MtxtComments { get; set; }
        public string MrdQueAbnormal { get; set; }
        public string MrdQueThinners { get; set; }
        public string MrdQueFibrillation { get; set; }
        public string MrdQueCigarette { get; set; }
        public string MrdQueCirulation { get; set; }
        public string MrdQuePersistent { get; set; }
        public string MrdQuefillers { get; set; }
        public string MrdQueDigestive { get; set; }
        public string MrdQueDizziness { get; set; }
        public string MrdQueSubstances { get; set; }
        public string MrdQueAlcoholic { get; set; }
        public string MrdQueHeart_defect { get; set; }
        public string MrdQueHiatal { get; set; }
        public string MrdQueBlood_pressure { get; set; }
        public string MrdQueHow_much { get; set; }
        public string MrdQueHow_often { get; set; }
        public string MrdQueReplacement { get; set; }
        public string MrdQueNeck_BackProblem { get; set; }
        public string MrdQuePainJaw_Joints { get; set; }
        public string MrdQueEndocarditis { get; set; }
        public string MrdQueShortness { get; set; }
        public string MrdQueSjorgren { get; set; }
        public string MrdQueSwollen { get; set; }
        public string MrdQueValve { get; set; }
        public string MrdQueVision { get; set; }
        public string MrdQueUnexplained { get; set; }
        public string MrdQueArthritis { get; set; }
    }
    public class DentalHistoryForm
    {
        public string txtQue1 { get; set; }
        public string txtQue2 { get; set; }
        public string txtQue3 { get; set; }
        public string txtQue4 { get; set; }
        public string txtQue5 { get; set; }
        public string txtQue5a { get; set; }
        public string txtQue5b { get; set; }
        public string txtQue5c { get; set; }
        public string txtQue6 { get; set; }
        public string txtQue7 { get; set; }
        public bool rdQue7a { get; set; }
        public bool rdQue8 { get; set; }
        public bool rdQue9 { get; set; }
        public string txtQue9a { get; set; }
        public bool rdQue10 { get; set; }
        public bool rdoQue11aFixedbridge { get; set; }

        public bool rdoQue11bRemoveablebridge { get; set; }

        public bool rdoQue11cDenture { get; set; }

        public bool rdQue11dImplant { get; set; }

        public string txtQue12 { get; set; }



        public bool rdQue15 { get; set; }
        public bool rdQue16 { get; set; }
        public bool rdQue17 { get; set; }
        public bool rdQue18 { get; set; }
        public bool rdQue19 { get; set; }
        public string chkQue20 { get; set; }
        public bool chkQue20_1 { get; set; }
        public bool chkQue20_2 { get; set; }
        public bool chkQue20_3 { get; set; }
        public bool chkQue20_4 { get; set; }
        public bool rdQue21 { get; set; }
        public string txtQue21a { get; set; }
        public string txtQue22 { get; set; }


        public string txtQue23 { get; set; }
        public bool rdQue24 { get; set; }

        public string txtQue26 { get; set; }

        public bool rdQue28 { get; set; }
        public string txtQue28a { get; set; }
        public string txtQue28b { get; set; }
        public string txtQue28c { get; set; }
        public string txtQue29 { get; set; }
        public string txtQue29a { get; set; }
        public bool rdQue30 { get; set; }

        public string txtComments { get; set; }
    }

    public class MediaclHisotry
    {
        public string MrdQue1 { get; set; }
        public string MrnQue1 { get; set; }
        public string Mtxtphysicians { get; set; }
        public string MrdQue2 { get; set; }
        public string MrnQue2 { get; set; }
        public string Mtxthospitalized { get; set; }
        public string MrdQue3 { get; set; }
        public string MrnQue3 { get; set; }
        public string Mtxtserious { get; set; }
        public string MrdQue4 { get; set; }
        public string MrnQue4 { get; set; }
        public string Mtxtmedications { get; set; }
        public string MrdQue5 { get; set; }
        public string MrnQue5 { get; set; }
        public string MtxtRedux { get; set; }
        public string MrdQue6 { get; set; }
        public string MrnQue6 { get; set; }
        public string MtxtFosamax { get; set; }
        public string MrdQuediet7 { get; set; }
        public string MrnQuediet7 { get; set; }
        public string Mtxt7 { get; set; }
        public string Mrdotobacco8 { get; set; }
        public string Mrnotobacco8 { get; set; }
        public string Mtxt8 { get; set; }
        public string Mrdosubstances { get; set; }
        public string Mrnosubstances { get; set; }
        public string Mtxt9 { get; set; }
        public string Mrdopregnant { get; set; }
        public string Mrnopregnant { get; set; }
        public string Mtxt10 { get; set; }
        public string Mtxt11 { get; set; }
        public string Mrdocontraceptives { get; set; }
        public string Mrnocontraceptives { get; set; }
        public string Mtxt12 { get; set; }
        public string MrdoNursing { get; set; }
        public string MrnoNursing { get; set; }
        public string Mrdotonsils { get; set; }
        public string Mrnotonsils { get; set; }
        public string Mtxt13 { get; set; }
        public string MchkQue_1 { get; set; }
        public string MchkQueN_1 { get; set; }
        public string MchkQue_2 { get; set; }
        public string MchkQueN_2 { get; set; }
        public string MchkQue_3 { get; set; }
        public string MchkQueN_3 { get; set; }
        public string MchkQue_4 { get; set; }
        public string MchkQueN_4 { get; set; }
        public string MchkQue_5 { get; set; }
        public string MchkQueN_5 { get; set; }
        public string MchkQue_6 { get; set; }
        public string MchkQueN_6 { get; set; }
        public string MchkQue_7 { get; set; }
        public string MchkQueN_7 { get; set; }
        public string MchkQue_8 { get; set; }
        public string MchkQueN_8 { get; set; }
        public string MchkQue_9 { get; set; }
        public string MchkQueN_9 { get; set; }
        public string MtxtchkQue_9 { get; set; }

        public string MrdQueAIDS_HIV_Positive { get; set; }
        public string MrdQueAlzheimer { get; set; }
        public string MrdQueAnaphylaxis { get; set; }
        public string MrdQueAnemia { get; set; }
        public string MrdQueAngina { get; set; }
        public string MrdQueArthritis_Gout { get; set; }
        public string MrdQueArtificialHeartValve { get; set; }
        public string MrdQueArtificialJoint { get; set; }
        public string MrdQueAsthma { get; set; }
        public string MrdQueBloodDisease { get; set; }
        public string MrdQueBloodTransfusion { get; set; }

        public string MrdQueBoneDisorder { get; set; }

        public string MrdQueBreathing { get; set; }

        public string MrdQueBruise { get; set; }
        public string MrdQueCancer { get; set; }

        public string MrdQueChemicalDepandancy { get; set; }

        public string MrdQueChemotherapy { get; set; }
        public string MrdQueChest { get; set; }
        public string MrdQueCold_Sores_Fever { get; set; }
        public string MrdQueCongenital { get; set; }
        public string MrdQueConvulsions { get; set; }

        public string MrdQueCortisone { get; set; }
        public string MrdQueCortisoneTretments { get; set; }
        public string MrdQueDiabetes { get; set; }
        public string MrdQueDrug { get; set; }
        public string MrdQueEasily { get; set; }
        public string MrdQueEmphysema { get; set; }

        public string MrdQueEndorcrineProblems { get; set; }

        public string MrdQueEpilepsy { get; set; }
        public string MrdQueExcessiveBleeding { get; set; }
        public string MrdQueExcessiveThirst { get; set; }

        public string MrdQueExcessiveUrination { get; set; }

        public string MrdQueFainting { get; set; }
        public string MrdQueFrequentCough { get; set; }
        public string MrdQueFrequentDiarrhea { get; set; }
        public string MrdQueFrequentHeadaches { get; set; }
        public string MrdQueGenital { get; set; }
        public string MrdQueGlaucoma { get; set; }

        public string MrdQueHay { get; set; }
        public string MrdQueHeartAttack_Failure { get; set; }
        public string MrdQueHeartMurmur { get; set; }
        public string MrdQueHeartPacemaker { get; set; }
        public string MrdQueHeartTrouble_Disease { get; set; }
        public string MrdQueHemophilia { get; set; }

        public string MrdQueHepatitisA { get; set; }
        public string MrdQueHepatitisBorC { get; set; }
        public string MrdQueHerpes { get; set; }
        public string MrdQueHighBloodPressure { get; set; }
        public string MrdQueHighCholesterol { get; set; }
        public string MrdQueHives { get; set; }

        public string MrdQueHypoglycemia { get; set; }
        public string MrdQueIrregular { get; set; }
        public string MrdQueKidney { get; set; }
        public string MrdQueLeukemia { get; set; }
        public string MrdQueLiver { get; set; }
        public string MrdQueLow { get; set; }
        public string MrdQueLung { get; set; }

        public string MrdQueMitral { get; set; }
        public string MrdQueNervousDisorder { get; set; }
        public string MrdQueOsteoporosis { get; set; }
        public string MrdQuePain { get; set; }
        public string MrdQueParathyroid { get; set; }
        public string MrdQuePsychiatric { get; set; }
        public string MrdQueRadiation { get; set; }
        public string MrdQueRecent { get; set; }

        public string MrdQueRenal { get; set; }
        public string MrdQueRheumatic { get; set; }
        public string MrdQueRheumatism { get; set; }
        public string MrdQueScarlet { get; set; }
        public string MrdQueShingles { get; set; }
        public string MrdQueSickle { get; set; }
        public string MrdQueSinus { get; set; }
        public string MrdQueSpina { get; set; }

        public string MrdQueStomach { get; set; }
        public string MrdQueStroke { get; set; }
        public string MrdQueSwelling { get; set; }
        public string MrdQueThyroid { get; set; }
        public string MrdQueTonsillitis { get; set; }
        public string MrdQueTuberculosis { get; set; }
        public string MrdQueTumors { get; set; }

        public string MrdQueUlcers { get; set; }
        public string MrdQueVenereal { get; set; }
        public string MrdQueYellow { get; set; }
        public string Mtxtillness { get; set; }
        public string MtxtComments { get; set; }
        public string MrdQueAbnormal { get; set; }
        public string MrdQueThinners { get; set; }
        public string MrdQueFibrillation { get; set; }
        public string MrdQueCigarette { get; set; }
        public string MrdQueCirulation { get; set; }
        public string MrdQuePersistent { get; set; }
        public string MrdQuefillers { get; set; }
        public string MrdQueDigestive { get; set; }
        public string MrdQueDizziness { get; set; }
        public string MrdQueSubstances { get; set; }
        public string MrdQueAlcoholic { get; set; }
        public string MrdQueHeart_defect { get; set; }
        public string MrdQueHiatal { get; set; }
        public string MrdQueBlood_pressure { get; set; }
        public string MrdQueHow_much { get; set; }
        public string MrdQueHow_often { get; set; }
        public string MrdQueReplacement { get; set; }
        public string MrdQueNeck_BackProblem { get; set; }
        public string MrdQuePainJaw_Joints { get; set; }
        public string MrdQueEndocarditis { get; set; }
        public string MrdQueShortness { get; set; }
        public string MrdQueSjorgren { get; set; }
        public string MrdQueSwollen { get; set; }
        public string MrdQueValve { get; set; }
        public string MrdQueVision { get; set; }
        public string MrdQueUnexplained { get; set; }
        public string MrdQueArthritis { get; set; }
    }
}