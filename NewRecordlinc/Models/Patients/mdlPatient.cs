﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DataAccessLayer;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Configuration;
using DataAccessLayer.PatientsData;
using NewRecordlinc.Models.ViewModel;
using NewRecordlinc.Models.Colleagues;
using System.Globalization;
using HtmlAgilityPack;
using BusinessLogicLayer;

namespace NewRecordlinc.Models.Patients
{
    public class mdlPatient
    {
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        clsPatientsData objPatientsData = new clsPatientsData();
        clsCommon objCommon = new clsCommon();

        public int PatientId { get; set; }
        public string AssignedPatientId { get; set; }
        public string EncryptesPId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DateOfBirth { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Age { get; set; }
        public string ExactAddress { get; set; }
        public string Address2 { get; set; }
        public string Phone { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string ImageName { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }

        public string DoctorFullName { get; set; } // this field use in send referral
        public string Notes { get; set; }
        public string FirstExamDate { get; set; }
        public string Location { get; set; }
        public string ReferredBy { get; set; }
        public string ReferredByName { get; set; }

        public string IsAuthorize { get; set; }

        public string ColleagueIds { get; set; }// this field use when request for send referral comes from colleague page.
        public string ColleagueFullName { get; set; }// this field use when request for send referral comes from colleague page.
        public List<PatientDetailsOfDoctor> lstPatientDetailsOfDoctor = new List<PatientDetailsOfDoctor>();
        public List<PatientDetailsOfDoctor> lstPatientDetailsOfDoctorSorted = new List<PatientDetailsOfDoctor>();
        public List<PatientNotes> lstPatientNotes = new List<PatientNotes>();
        public List<SocialMediaDetailsForPatients> lstSocialMediaDetailsForPatients = new List<SocialMediaDetailsForPatients>();
        public List<PatientReferralHistoryDetails> lstPatientReferralHistoryForDoctor = new List<PatientReferralHistoryDetails>();

        public List<ReferralHistoryOfPatient> lstReferralHistoryOfPatient = new List<ReferralHistoryOfPatient>();
        public List<PatientMessagesOfDoctor> lstPatientMessagesOfDoctor = new List<PatientMessagesOfDoctor>();
        public List<TempUpload> lstTempUpload = new List<TempUpload>();
        public List<DoctorLocation> lstDoctorLocation = new List<DoctorLocation>();

        public List<PatientImages> lstPatientImages = new List<PatientImages>();
        public List<PateintDocuments> lstPateintDocuments = new List<PateintDocuments>();

        public List<ReferralDetails> lstRefferalDetails = new List<ReferralDetails>();
        

        public string[] ReferralRegarding = { "Orthodontics", "Periodontist", "Oral Surgery", "Prosthodontics", "Radiology", "General Dentistry", "Endodontics", "Lab Work" };
        public string[] ReferralRequest = { "Extractions", "Pathology", "Caries", "Check Periodontal", "Oral Surgery", "Orthodontic Consultation", "Orthognathic Surgery", "Implants" };




        #region Code For Get Patients Documents
        public List<PateintDocuments> GetPatientDocumetnsAll(int PatientId)
        {
            lstPateintDocuments = new List<PateintDocuments>();
            DataTable dt = new DataTable();
            dt = objPatientsData.GetPatientDocumentsAll(PatientId, null);
            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstPateintDocuments.Add(new PateintDocuments
                        {
                            MontageId = Convert.ToInt32(row["MontageId"]),
                            DocumentId = Convert.ToInt32(row["DocumentId"]),
                            DocumentName = objCommon.CheckNull(Convert.ToString(row["DocumentName"]), string.Empty),
                            CreationDate = objCommon.CheckNull(Convert.ToString(row["CreationDate"]), string.Empty),
                            LastModifiedDate = objCommon.CheckNull(Convert.ToString(row["LastModifiedDate"]), string.Empty),
                            Description = objCommon.CheckNull(Convert.ToString(row["Description"]), string.Empty),

                        });
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstPateintDocuments;
        }
        #endregion
        #region Code For Get Pateints Images
        public List<PatientImages> GetPatientImagesAll(int PatientId)
        {
            lstPatientImages = new List<PatientImages>();
            DataTable dt = new DataTable();
            dt = objPatientsData.GetPatientImages(PatientId, null);
            try
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstPatientImages.Add(new PatientImages
                        {
                            ImageId = Convert.ToInt32(row["ImageId"]),
                            ImageType = Convert.ToInt32(row["ImageType"]),
                            CreationDate = objCommon.CheckNull(Convert.ToString(row["CreationDate"]), string.Empty),
                            LastModifiedDate = objCommon.CheckNull(Convert.ToString(row["LastModifiedDate"]), string.Empty),
                            Description = objCommon.CheckNull(Convert.ToString(row["Description"]), string.Empty),
                            //MontageId = Convert.ToInt32(row["MontageId"]),
                            Name = objCommon.CheckNull(Convert.ToString(row["Name"]), string.Empty),
                        });
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return lstPatientImages;
        }

        #endregion

        #region Code For Get Patient details by PatientId
        public DataSet GetPateintDetailsByPatientId(int PatientId)
        {
            objPatientsData = new clsPatientsData();
            DataSet ds = new DataSet();
            return ds = objPatientsData.GetPateintDetailsByPatientId(PatientId);
        }
        #endregion
        #region Code For Get Patient List Of Doctor

        public List<PatientDetailsOfDoctor> GetPatientDetailsOfDoctor(int UserId, int PageIndex, int PageSize, string SearchText, int SortColumn, int SortDirection, string NewSearchText, int FilterBy,int locationId =0)
        {
            try
            {
                DataTable dt = new DataTable();
                lstPatientDetailsOfDoctor = new List<PatientDetailsOfDoctor>();
                dt = objPatientsData.GetPatientDetailsOfDoctor(UserId, PageIndex, PageSize, SearchText, SortColumn, SortDirection, NewSearchText, FilterBy,locationId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    lstPatientDetailsOfDoctor = (from p in dt.AsEnumerable()
                                                 select new PatientDetailsOfDoctor
                                                 {
                                                     PatientId = Convert.ToInt32(p["PatientId"]),
                                                     AssignedPatientId = objCommon.CheckNull(Convert.ToString(p["AssignedPatientId"]), string.Empty),
                                                     FirstName = objCommon.CheckNull(Convert.ToString(p["FirstName"]), string.Empty),
                                                     LastName = objCommon.CheckNull(Convert.ToString(p["LastName"]), string.Empty),
                                                     Email = objCommon.CheckNull(Convert.ToString(p["Email"]), string.Empty),
                                                     Gender = objCommon.CheckNull(Convert.ToString(p["Gender"]), string.Empty),
                                                     Age = objCommon.CheckNull(Convert.ToString(p["Age"]), string.Empty),
                                                     Phone = objCommon.CheckNull(Convert.ToString(p["Phone"]), string.Empty),

                                                     ExactAddress = objCommon.CheckNull(Convert.ToString(p["ExactAddress"]), string.Empty),
                                                     Address2 = objCommon.CheckNull(Convert.ToString(p["Address2"]), string.Empty),
                                                     City = objCommon.CheckNull(Convert.ToString(p["City"]), string.Empty),
                                                     State = objCommon.CheckNull(Convert.ToString(p["State"]), string.Empty),
                                                     ZipCode = objCommon.CheckNull(Convert.ToString(p["ZipCode"]), string.Empty),
                                                     ImageName = objCommon.CheckNull(Convert.ToString(p["ProfileImage"]), string.Empty) != string.Empty ? Convert.ToString(ConfigurationManager.AppSettings["ImageBank"]) + Convert.ToString(p["ProfileImage"]) : "",
                                                     TotalRecord = Convert.ToInt32(p["TotalRecord"]),
                                                     OwnerId = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(p["OwnerId"]))?0:p["OwnerId"]),
                                                     ReferredBy = Convert.ToString(p["ReferredBy"]),
                                                     ReferredById = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(p["ReferredById"])) ? 0 : p["ReferredById"]),
                                                     Location = objCommon.CheckNull(Convert.ToString(p["Location"]), string.Empty),
                                                 }).ToList();
                }

                foreach (var items in lstPatientDetailsOfDoctor)
                {
                    if (string.IsNullOrEmpty(items.ImageName))
                    {
                        items.ImageName = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty);
                    }
                    else
                    {
                        items.ImageName = objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + items.ImageName;
                    }
                }
            }
            catch (Exception EX)
            {

                throw;
            }

            return lstPatientDetailsOfDoctor;

        }

        public List<PatientDetailsOfDoctor> GetAllPatientDetailsOfDoctor(int UserId, string SearchText, int SortColumn, int SortDirection, string NewSearchText, int FilterBy)
        {
            try
            {
                DataTable dt = new DataTable();
                lstPatientDetailsOfDoctor = new List<PatientDetailsOfDoctor>();
                dt = objPatientsData.GetAllPatientDetailsOfDoctor(UserId, SearchText, SortColumn, SortDirection, NewSearchText, FilterBy);
                if (dt != null && dt.Rows.Count > 0)
                {
                    lstPatientDetailsOfDoctor = (from p in dt.AsEnumerable()
                                                 select new PatientDetailsOfDoctor
                                                 {
                                                     PatientId = Convert.ToInt32(p["PatientId"]),
                                                     AssignedPatientId = objCommon.CheckNull(Convert.ToString(p["AssignedPatientId"]), string.Empty),
                                                     FirstName = objCommon.CheckNull(Convert.ToString(p["FirstName"]).Trim(), string.Empty),
                                                     LastName = objCommon.CheckNull(Convert.ToString(p["LastName"]).Trim(), string.Empty),
                                                     Email = objCommon.CheckNull(Convert.ToString(p["Email"]).Trim(), string.Empty),
                                                     Gender = objCommon.CheckNull(Convert.ToString(p["Gender"]), string.Empty),
                                                     Age = objCommon.CheckNull(Convert.ToString(p["Age"]), string.Empty),
                                                     Phone = objCommon.CheckNull(Convert.ToString(p["Phone"]), string.Empty),

                                                     ExactAddress = objCommon.CheckNull(Convert.ToString(p["ExactAddress"]), string.Empty),
                                                     Address2 = objCommon.CheckNull(Convert.ToString(p["Address2"]), string.Empty),
                                                     City = objCommon.CheckNull(Convert.ToString(p["City"]), string.Empty),
                                                     State = objCommon.CheckNull(Convert.ToString(p["State"]), string.Empty),
                                                     ZipCode = objCommon.CheckNull(Convert.ToString(p["ZipCode"]), string.Empty),
                                                     ImageName = objCommon.CheckNull(Convert.ToString(p["ProfileImage"]), string.Empty) != string.Empty ? Convert.ToString(ConfigurationManager.AppSettings["ImageBank"]) + Convert.ToString(p["ProfileImage"]) : "",
                                                     TotalRecord = Convert.ToInt32(p["TotalRecord"]),
                                                     OwnerId = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(p["OwnerId"])) ? 0 : p["OwnerId"]),
                                                     ReferredBy = Convert.ToString(p["ReferredBy"]).Trim(),
                                                     ReferredById = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(p["ReferredById"])) ? 0 : p["ReferredById"]),
                                                     Location = objCommon.CheckNull(Convert.ToString(p["Location"]), string.Empty),
                                                     Text = Convert.ToBoolean(string.IsNullOrEmpty(Convert.ToString(p["Text"])) ? 0 : p["Text"]),
                                                     Voice = Convert.ToBoolean(string.IsNullOrEmpty(Convert.ToString(p["Voice"])) ? 0 : p["Voice"]),
                                                     ChkEmail = Convert.ToBoolean(string.IsNullOrEmpty(Convert.ToString(p["ChkEmail"])) ? 0 : p["ChkEmail"]),
                                                     AccountId = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(p["Accountid"])) ? 0 : p["Accountid"]),
                                                     MailCount = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(p["MailCount"])) ? 0 : p["MailCount"]),
                                                     LastSentDate = objCommon.CheckNull(Convert.ToString(p["MailLastSentdate"]),string.Empty)
                                                 }).ToList();
                }

            }
            catch (Exception EX)
            {

                throw;
            }

            return lstPatientDetailsOfDoctor;

        }

        public List<FamilyReleation> GetFamilyDetails()
        {
            List<FamilyReleation> lstfamilyrelation = new List<FamilyReleation>();
            try
            {
                DataTable dt = new DataTable();

                dt = objPatientsData.GetFamilyDetails();
                if (dt != null && dt.Rows.Count > 0)
                {
                    lstfamilyrelation = (from p in dt.AsEnumerable()
                                         select new FamilyReleation
                                         {
                                             RelativeId = Convert.ToInt32(p["RelativeId"]),
                                             ReleationName = objCommon.CheckNull(Convert.ToString(p["ReleationName"]), string.Empty),
                                         }).ToList();
                }

            }
            catch (Exception EX)
            {

                throw;
            }

            return lstfamilyrelation;

        }
        public List<FamilyReleationHistory> GetFamilyHistoryDetails(int PatientId)
        {
            List<FamilyReleationHistory> lstfamilyrelation = new List<FamilyReleationHistory>();
            try
            {
                DataTable dt = new DataTable();

                dt = objPatientsData.GetFamilyHistoryDetails(PatientId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    lstfamilyrelation = (from p in dt.AsEnumerable()
                                         select new FamilyReleationHistory
                                         {
                                             ReletaiveId = Convert.ToInt32(p["ReleativeId"]),
                                             PatientId = Convert.ToInt32(p["PatientId"]),
                                             PatientName = Convert.ToString(p["PatientName"]),
                                             ReleationName = objCommon.CheckNull(Convert.ToString(p["ReleationName"]), string.Empty),
                                             ToPatientId = Convert.ToInt32(p["ToPatientId"])
                                         }).ToList();
                }

            }
            catch (Exception EX)
            {

                throw;
            }

            return lstfamilyrelation;

        }
        #endregion
        #region Code For Get Patient's Notes details
        public List<PatientNotes> GetPatientNotesDetails(int PatientId)
        {
            try
            {
                DataTable dt = new DataTable();
                lstPatientNotes = new List<PatientNotes>();
                dt = objPatientsData.GetPatientNotesDetails(PatientId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstPatientNotes.Add(new PatientNotes()
                        {
                            NoteId = Convert.ToInt32(row["Note_Id"]),
                            Note = objCommon.CheckNull(Convert.ToString(row["PatientNotes"]), string.Empty),
                            FirstName = objCommon.CheckNull(Convert.ToString(row["FirstName"]), string.Empty),
                            LastName = objCommon.CheckNull(Convert.ToString(row["LastName"]), string.Empty),
                            ImageName = objCommon.CheckNull(Convert.ToString(row["ImageName"]), string.Empty) == string.Empty ? objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DefaultDoctorImage"]), string.Empty) : objCommon.CheckNull(Convert.ToString(ConfigurationManager.AppSettings["DoctorImage"]), string.Empty) + objCommon.CheckNull(Convert.ToString(row["ImageName"]), string.Empty),
                            CreationDate = objCommon.CheckNull(Convert.ToString(row["CreatedDate"]), string.Empty),
                            UserId = Convert.ToInt32(row["UserId"]),
                        });
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstPatientNotes;
        }
        #endregion
        public int GetPatientCountOfDoctor(int DoctorId, string SearchText = null, string NewSearchText = null, int FilterBy = 0, int Location = 0)
        {
            try
            {
                DataTable dt = new DataTable();
                int Count = 0;
                dt = objPatientsData.GetPatientCountByDoctorId(DoctorId,SearchText,NewSearchText,FilterBy,Location);
                if (dt != null && dt.Rows.Count > 0)
                {
                    Count = Convert.ToInt32(dt.Rows[0]["Counting"]);
                }
                return Count;
            }
            catch (Exception EX)
            {
                objCommon.InsertErrorLog("GetPatientCountByDoctorId", EX.Message, EX.StackTrace);
                throw;
            }
        }
        public bool RemovePatient(int PatientId, int UserId)
        {
            clsPatientsData objPatientsData = new clsPatientsData();
            return objPatientsData.RemovePatientNew(PatientId, UserId);
        }


        public bool CheckPatientEmail(string PatientEmail)
        {
            clsPatientsData objPatientsData = new clsPatientsData();
            return objPatientsData.CheckPatientEmail(PatientEmail);
        }
        public DataTable CheckAssignedPatientId(string AssignedPatientId, int OwnerId, int PatientId)
        {
            clsPatientsData objPatientsData = new clsPatientsData();
            string CheckAssignId = null;
            if (!string.IsNullOrEmpty(AssignedPatientId))
            {
                CheckAssignId = AssignedPatientId;
            }
            return objPatientsData.CheckAssignedPatientId(CheckAssignId, OwnerId, PatientId);
        }

        public int PatientInsertAndUpdate(int PatientId, string AssignedPatientId, string FirstName,
                                           string MiddelName, string LastName, string DateOfBirth,
                                           int Gender, string ProfileImage, string Phone, string Email,
                                           string Address, string City, string State, string Zip, string Country,
                                           int OwnerId, int PatientStatus, string Address2, string Password, string Notes, int? ReferredBy, string Location, string FirstExamDate, string TimeZoneSystemName)
        {
            clsPatientsData objPatientsData = new clsPatientsData();
            if (AssignedPatientId == null)
            {
                AssignedPatientId = "";
            }

            DateTime? dt_DateOfBirth = null;
            if(!string.IsNullOrEmpty(DateOfBirth))
            {
                dt_DateOfBirth = Convert.ToDateTime(DateOfBirth);
                //dt_DateOfBirth = clsHelper.ConvertToUTC(Convert.ToDateTime(dt_DateOfBirth), TimeZoneSystemName);
            }
            return objPatientsData.PatientInsertAndUpdate(PatientId, AssignedPatientId, FirstName, MiddelName, LastName, dt_DateOfBirth, Gender, ProfileImage, Phone, Email,
                                           Address, City, State, Zip, Country,
                                           OwnerId, PatientStatus, Address2, Password, Notes, ReferredBy, Location, FirstExamDate);
        }

       
        public List<DoctorLocation> GetDoctorList(int UserId)
        {
            try
            {
                clsColleaguesData objColleaguesData = new clsColleaguesData();
                List<DoctorLocation> doclst = new List<DoctorLocation>();
                DataTable dt = new DataTable();
                dt = objColleaguesData.GetDoctorLocaitonById(UserId);
                if (dt != null)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        doclst.Add(new DoctorLocation()
                        {
                            LocationId = Convert.ToInt32(item["AddressInfoId"]),
                            LocationName = Convert.ToString(item["Location"])
                        });
                    }
                }
                return doclst;
            }
            catch (Exception EX)
            {

                throw;
            }
        }


        #region Code For Insert and Remove Patient's Note

        public bool AddPatientNote(int PatientId, int UserId, string Note)
        {
            bool Result = false;
            Result = objPatientsData.AddPatientNote(PatientId, UserId, Note);
            return Result;
        }

        public bool RemovePatientNote(int NoteId)
        {
            bool Result = false;
            Result = objPatientsData.RemovePatientNote(NoteId);
            return Result;
        }
        #endregion
        #region Code For Get Patient's Social Media details for Doctor
        public List<SocialMediaDetailsForPatients> GetSocialMediaDetailsForPatient(int id, int index, int size, string SearchText, int SortColumn, int SortDirection)
        {
            try
            {
                lstSocialMediaDetailsForPatients = new List<SocialMediaDetailsForPatients>();
                DataTable dt = new DataTable();
                dt = objPatientsData.GetSocialMediaDetailsForPatient(id, index, size, SearchText, SortColumn, SortDirection);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstSocialMediaDetailsForPatients.Add(new SocialMediaDetailsForPatients()
                        {
                            PatientId = Convert.ToInt32(row["PatientId"]),
                            FirstName = objCommon.CheckNull(Convert.ToString(row["FirstName"]), string.Empty),
                            LastName = objCommon.CheckNull(Convert.ToString(row["LastName"]), string.Empty),
                            FacebookUrl = objCommon.CheckNull(Convert.ToString(row["FacebookUrl"]), string.Empty),
                            LinkedinUrl = objCommon.CheckNull(Convert.ToString(row["LinkedinUrl"]), string.Empty),
                            TwitterUrl = objCommon.CheckNull(Convert.ToString(row["TwitterUrl"]), string.Empty),
                            GoogleplusUrl = objCommon.CheckNull(Convert.ToString(row["GoogleplusUrl"]), string.Empty),
                            YoutubeUrl = objCommon.CheckNull(Convert.ToString(row["YoutubeUrl"]), string.Empty),
                            PinterestUrl = objCommon.CheckNull(Convert.ToString(row["BlogUrl"]), string.Empty),
                            BlogUrl = objCommon.CheckNull(Convert.ToString(row["PinterestUrl"]), string.Empty),
                            cloudscore = Convert.ToInt32(objCommon.CheckNull(Convert.ToString(row["cloudscore"]),"0")),
                            OtherUrl = objCommon.CheckNull(Convert.ToString(row["OtherUrl"]), string.Empty),
                        });


                    }

                }


            }
            catch (Exception ex)
            {

                throw ex;
            }


            return lstSocialMediaDetailsForPatients;
        }
        #endregion
        #region Code For Get All Referral History details recevied Doctor
        public List<PatientReferralHistoryDetails> GetPatientReferralHistoryDetailsForDoctor(int UserId, int PageIndex, int PageSize, int SortColumn, int SortDirection, string SearchText)
        {
            try
            {
                lstPatientReferralHistoryForDoctor = new List<PatientReferralHistoryDetails>();
                DataTable dt = new DataTable();
                dt = objPatientsData.GetPatientReferralHistoryDetails(UserId, PageIndex, PageSize, SortColumn, SortDirection, SearchText);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstPatientReferralHistoryForDoctor.Add(new PatientReferralHistoryDetails()
                        {
                            FirstName = objCommon.CheckNull(Convert.ToString(row["FirstName"]), string.Empty),
                            LastName = objCommon.CheckNull(Convert.ToString(row["LastName"]), string.Empty),
                            CreationDate = objCommon.CheckNull(Convert.ToString(row["CreationDate"]), string.Empty),
                            SenderName = objCommon.CheckNull(Convert.ToString(row["SenderName"]), string.Empty),
                            ReceiverName = objCommon.CheckNull(Convert.ToString(row["ReceiverName"]), string.Empty),
                            Comments = objCommon.CheckNull(Convert.ToString(row["Comments"]), string.Empty) != string.Empty ? (Convert.ToString(row["Comments"]).Length > 25 ? Convert.ToString(row["Comments"]).Substring(0, 25) + "..." : Convert.ToString(row["Comments"])) : objCommon.CheckNull(Convert.ToString(row["Comments"]), string.Empty),
                            TotalRecord = Convert.ToInt32(row["TotalRecord"]),

                        });


                    }

                }


            }
            catch (Exception ex)
            {

                throw ex;
            }


            return lstPatientReferralHistoryForDoctor;
        }
        #endregion


        #region Code For Get Referral details of patient by Patient id
        public List<ReferralHistoryOfPatient> GetPatientReferralHistoryDetailsByPatientId(int UserId, int PatientId, int PageIndex, int PageSize)
        {
            try
            {
                lstReferralHistoryOfPatient = new List<ReferralHistoryOfPatient>();
                DataTable dt = new DataTable();
                dt = objPatientsData.GetPatientReferralHistoryDetailsByPatientId(UserId, PatientId, PageIndex, PageSize);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstReferralHistoryOfPatient.Add(new ReferralHistoryOfPatient()
                        {
                            CreationDate = objCommon.CheckNull(Convert.ToString(row["CreationDate"]), string.Empty),
                            FromField = objCommon.CheckNull(Convert.ToString(row["FromField"]), string.Empty),
                            ToField = objCommon.CheckNull(Convert.ToString(row["ToField"]), string.Empty),
                            Status = objCommon.CheckNull(Convert.ToString(row["Status"]), string.Empty),
                            Comments = objCommon.CheckNull(Convert.ToString(row["Comments"]), string.Empty),
                            TotalRecord = Convert.ToInt32(row["TotalRecord"]),
                            MessageId = Convert.ToInt32(row["MessageId"]),
                            ReferralCardId = Convert.ToInt32(row["ReferralCardId"]),
                            ReferedUserId = Convert.ToInt32(row["ReferedUserId"]),
                        });
                    }
                }
                return lstReferralHistoryOfPatient;

            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Code For Get All Messages Sent by Patients to Doctor
        public List<PatientMessagesOfDoctor> GetPatientMessagesForDoctor(int UserId, int PageIndex, int PageSize)
        {
            try
            {
                lstPatientMessagesOfDoctor = new List<PatientMessagesOfDoctor>();
                DataTable dt = new DataTable();
                dt = objPatientsData.GetPatientMessagesForDoctor(UserId, PageIndex, PageSize);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstPatientMessagesOfDoctor.Add(new PatientMessagesOfDoctor()
                        {
                            PatientmessageId = Convert.ToInt32(row["PatientmessageId"]),
                            FromField = objCommon.CheckNull(Convert.ToString(row["FromField"]), string.Empty),
                            CreationDate = objCommon.CheckNull(Convert.ToString(row["CreationDate"]), string.Empty),
                            Message = objCommon.CheckNull(Convert.ToString(row["Message"]), string.Empty),
                            PatientId = Convert.ToInt32(row["PatientId"]),
                            TotalRecord = Convert.ToInt32(row["TotalRecord"]),
                            PatientEmail = objCommon.CheckNull(Convert.ToString(row["PatientEmail"]), string.Empty),
                            DoctorEmail = objCommon.CheckNull(Convert.ToString(row["DoctorEmail"]), string.Empty),
                            ToField = objCommon.CheckNull(Convert.ToString(row["FromField"]), string.Empty),

                        });
                    }


                }
                return lstPatientMessagesOfDoctor;

            }
            catch (Exception Ex)
            {

                throw Ex;
            }
        }
        #endregion
        #region PatientNoteForbyDoctorId
        public List<PatientNotes> PatientNoteForDoctor(int UserId, int PageIndex, int PageSize, int ShortDirection, int ShortColumn, string TimeZoneSystemName)
        {
            try
            {
                lstPatientNotes = new List<PatientNotes>();
                DataTable dt = new DataTable();
                dt = objPatientsData.GetPatientNotesforDoctor(UserId, ShortColumn, ShortDirection, PageIndex, PageSize);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        var tmpPatientNotes = new PatientNotes()
                        {
                            PatientFullName = item["PatientName"].ToString(),
                            NoteId = Convert.ToInt32(item["Note_Id"]),
                            CreationDate = item["CreatedDate"].ToString(),
                            Note = item["Notes"].ToString(),
                            PatientId = Convert.ToInt32(item["PatientId"])
                        };
                        if(!string.IsNullOrEmpty(tmpPatientNotes.CreationDate))
                        {
                            var dtCreationDate = Convert.ToDateTime(tmpPatientNotes.CreationDate);
                            dtCreationDate = clsHelper.ConvertFromUTC(dtCreationDate, TimeZoneSystemName);
                            //tmpPatientNotes.CreationDate = dtCreationDate.ToString("M/d/yyyy hh:mm tt");
                            tmpPatientNotes.CreationDate = new CommonBLL().ConvertToDateTime(Convert.ToDateTime(dtCreationDate), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL));
                        }
                        lstPatientNotes.Add(tmpPatientNotes);
                        
                    }

                }
                return lstPatientNotes;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion
        public List<AppointmentReport> GetAppointmentReportList(int UserId, string SortColumn, string SortDirection, int PageIndex, int PageSize, string SearchText, string TimeZoneSystemName,string Name,int Age,string DoctorName,string Alert, DateTime? LastHygiene, string HygieneType, DateTime? NextAppointmentTime, DateTime? LastAppointment)
        {
            try
            {
                clsPatientsData cls = new clsPatientsData();
                DataTable dt = new DataTable();
                List<AppointmentReport> AppReport = new List<AppointmentReport>();
                dt = cls.GetAppointmentReportofdoctor(UserId, SortColumn, SortDirection, PageIndex, PageSize, SearchText, Name, Age, DoctorName, Alert, LastHygiene, HygieneType, NextAppointmentTime, LastAppointment);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        DateTime? LastHygieneDateTime = null;
                        if (Convert.ToString(item["HygineDate"]) != "" && Convert.ToString(item["HygineTime"]) != "")
                        {
                            LastHygieneDateTime = Convert.ToDateTime
                                                    (Convert.ToDateTime(Convert.ToString(item["HygineDate"])).ToString("yyyy-MM-dd") + " " +
                                                    (Convert.ToDateTime(Convert.ToString(item["HygineTime"])).ToString("hh:mm tt"))
                                                    );
                            LastHygieneDateTime = clsHelper.ConvertFromUTC(Convert.ToDateTime(LastHygieneDateTime), TimeZoneSystemName);
                        }
                      

                        DateTime? NextAppointmentDateTime = null;
                        string strNextAppointmentDate = string.Empty;
                        if (Convert.ToString(item["NextAppointmentDate"]) != "" && Convert.ToString(item["NextAppointmentTime"]) != "")
                        {
                            NextAppointmentDateTime = Convert.ToDateTime
                                                    (Convert.ToDateTime(Convert.ToString(item["NextAppointmentDate"])).ToString("yyyy-MM-dd") + " " +
                                                    (Convert.ToDateTime(Convert.ToString(item["NextAppointmentTime"])).ToString("hh:mm tt"))
                                                    );
                            NextAppointmentDateTime = clsHelper.ConvertFromUTC(Convert.ToDateTime(NextAppointmentDateTime), TimeZoneSystemName);
                            strNextAppointmentDate = new CommonBLL().ConvertToDateTime(Convert.ToDateTime(NextAppointmentDateTime), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL));
                        }
                        DateTime? LastAppointmentDateTime = null;
                        string strLastAppointmentDate = string.Empty;
                        if (Convert.ToString(item["PreviousAppointmentDate"]) != "" && Convert.ToString(item["PreviousAppointmentTime"]) != "")
                        {
                            LastAppointmentDateTime = Convert.ToDateTime
                                                   (Convert.ToDateTime(Convert.ToString(item["PreviousAppointmentDate"])).ToString("yyyy-MM-dd") + " " +
                                                   (Convert.ToDateTime(Convert.ToString(item["PreviousAppointmentTime"])).ToString("hh:mm tt"))
                                                   );
                            LastAppointmentDateTime = clsHelper.ConvertFromUTC(Convert.ToDateTime(LastAppointmentDateTime), TimeZoneSystemName);
                            strLastAppointmentDate = new CommonBLL().ConvertToDateTime(Convert.ToDateTime(LastAppointmentDateTime), Convert.ToInt32(BO.Enums.Common.DateFormat.GENERAL));
                        }


                        AppReport.Add(new AppointmentReport()
                        {

                            AppointmentId = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(item["AppointmentId"])) ? 0 : item["AppointmentId"]),
                            DoctorName = objCommon.CheckNull(Convert.ToString(item["DoctorName"]), string.Empty),
                            PatientFirstName = objCommon.CheckNull(Convert.ToString(item["FirstName"]), string.Empty),
                            PatientLastName = objCommon.CheckNull(Convert.ToString(item["LastName"]), string.Empty),
                            Phone = objCommon.CheckNull(Convert.ToString(item["Phone"]), string.Empty),
                            PatientId = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(item["PatientId"])) ? 0 : item["PatientId"]),
                            DoctorId = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(item["DoctorId"])) ? 0 : item["DoctorId"]),
                            AppointmentType = objCommon.CheckNull(Convert.ToString(item["ServiceType"]), string.Empty),
                            PatientAge = objCommon.CheckNull(Convert.ToString(item["Age"]), string.Empty),
                            Alert = objCommon.CheckNull(Convert.ToString(item["Alert"]), string.Empty),
                            AppointmentReason = objCommon.CheckNull(Convert.ToString(item["Age"]), string.Empty),
                            //NextAppointmentDate = (NextAppointmentDateTime.HasValue ? Convert.ToString(NextAppointmentDateTime.Value.ToString("MM/dd/yyyy")) : "") + " " + (NextAppointmentDateTime.HasValue ? NextAppointmentDateTime.Value.ToString("hh:mm tt") : ""),
                            NextAppointmentDate = strNextAppointmentDate,
                            LastHygieneDate = (LastHygieneDateTime.HasValue ? Convert.ToString(LastHygieneDateTime.Value.ToString("MM/dd/yyyy")) : "") + " " + (LastHygieneDateTime.HasValue ? Convert.ToString(LastHygieneDateTime.Value.ToString("hh:mm tt")) : ""),
                            HygieneType = objCommon.CheckNull(Convert.ToString(item["LastHygieneType"]), string.Empty),
                            //LastAppointmentDate = (LastAppointmentDateTime.HasValue ? Convert.ToString(LastAppointmentDateTime.Value.ToString("MM/dd/yyyy")) : "") + " " + (LastAppointmentDateTime.HasValue ? LastAppointmentDateTime.Value.ToString("hh:mm tt") : ""),
                            LastAppointmentDate = strLastAppointmentDate,
                            NextAppointmentType = objCommon.CheckNull(Convert.ToString(item["NextAppointmentType"]), string.Empty),
                        });
                    }
                }
                return AppReport;
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("", ex.Message, ex.StackTrace);
                throw;
            }

        }
        public List<PatientDetailsOfDoctor> GetPatientSentAndReceivedList(int UserId, int Flag, int SortColumn, int SortDirection, string SearchText, int PageIndex, int PageSize, string NewSearchText, int FilterBy, string ColleagueId)
        {
            try
            {
                clsPatientsData patientdata = new clsPatientsData();
                DataTable dt = new DataTable();
                List<PatientDetailsOfDoctor> lst = new List<PatientDetailsOfDoctor>();
                dt = patientdata.PatientSentAndReceivedList(UserId, Flag, SortColumn, SortDirection, SearchText, PageIndex, PageSize, NewSearchText, FilterBy, ColleagueId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        if (Convert.ToString(item["PatientId"]) != null && Convert.ToString(item["PatientId"]) != "")
                        {
                            lst.Add(new PatientDetailsOfDoctor()
                            {
                                FirstName = objCommon.CheckNull(Convert.ToString(item["FirstName"]), string.Empty),
                                LastName = objCommon.CheckNull(Convert.ToString(item["LastName"]), string.Empty),
                                AssignedPatientId = objCommon.CheckNull(Convert.ToString(item["AssignedPatientId"]), string.Empty),
                                Email = objCommon.CheckNull(Convert.ToString(item["Email"]), string.Empty),
                                PatientId = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(item["PatientId"])) ? 0 : item["PatientId"]),
                                Phone = objCommon.CheckNull(Convert.ToString(item["WorkPhone"]), string.Empty),
                                ReferredById = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(item["ReferredBy"])) ? 0 : item["ReferredBy"]),
                                ReferredBy = objCommon.CheckNull(Convert.ToString(item["DocFirstName"]) + ' ' + Convert.ToString(item["DocLastName"]), string.Empty),
                                OwnerId = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(item["OwnerId"])) ? 0 : item["OwnerId"]),
                                Location = objCommon.CheckNull(Convert.ToString(item["Location"]), string.Empty),
                            });
                        }
                    }
                }
                return lst;
            }
            catch (Exception ex)
            {
                objCommon.InsertErrorLog("", ex.Message, ex.StackTrace);
                throw;
            }
        }
        #region Code For Get All Messages Sent by Doctor to Patient
        public List<PatientMessagesOfDoctor> GetPatientMessagesSentByDoctorToPatientId(int UserId, int PageIndex, int PageSize, int PatientId)
        {
            try
            {
                lstPatientMessagesOfDoctor = new List<PatientMessagesOfDoctor>();
                DataTable dt = new DataTable();
                dt = objPatientsData.GetPatientMessagesSentByDoctorToPatientId(UserId, PageIndex, PageSize, PatientId);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstPatientMessagesOfDoctor.Add(new PatientMessagesOfDoctor()
                        {
                            PatientmessageId = Convert.ToInt32(row["PatientmessageId"]),
                            FromField = objCommon.CheckNull(Convert.ToString(row["FromField"]), string.Empty),
                            CreationDate = objCommon.CheckNull(Convert.ToString(row["CreationDate"]), string.Empty),
                            Message = objCommon.CheckNull(Convert.ToString(row["Message"]), string.Empty),
                            PatientId = Convert.ToInt32(row["PatientId"]),
                            TotalRecord = Convert.ToInt32(row["TotalRecord"]),
                        });
                    }


                }
                return lstPatientMessagesOfDoctor;

            }
            catch (Exception Ex)
            {

                throw Ex;
            }
        }
        #endregion
        #region Code For Remove PatientMessage By Doctor
        public bool RemovePatientMessageByDoctor(int PatientmessageId)
        {
            return (objPatientsData.RemovePatientMessageByDoctor(PatientmessageId));
        }
        #endregion


        #region Code For Get Temp Records Details Uplaoded By Doctor
        public List<TempUpload> GetRecordsFromTempTablesForDoctor(int UserId)
        {
            try
            {
                lstTempUpload = new List<TempUpload>();
                DataTable dt = new DataTable();
                dt = objCommon.GetRecordsFromTempTablesForDoctor(UserId, clsCommon.GetUniquenumberForAttachments());
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        lstTempUpload.Add(new TempUpload()
                        {
                            DocumentId = Convert.ToInt32(row["DocumentId"]),
                            DocumentName = objCommon.CheckNull(Convert.ToString(row["DocumentName"]), string.Empty),
                        });
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstTempUpload;
        }
        #endregion

        #region Get Message Details by MessageId
        public PatientMessagesOfDoctor PatientMessage(int MessageId, string TimeZoneSystemName)
        {
            PatientMessagesOfDoctor PMD = new PatientMessagesOfDoctor();
            clsPatientsData ObjPatientsData = new clsPatientsData();
            DataTable dt = new DataTable();
            List<Attachements> lstattachement = new List<Attachements>();
            dt = ObjPatientsData.GetPatientMessageofDoctor(MessageId);
            if (dt.Rows.Count > 0)
            {
                
                string NewMessage = Uri.UnescapeDataString(Convert.ToString(dt.Rows[0]["Message"]));
                HtmlDocument htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(NewMessage);
                string Message = htmlDoc.DocumentNode.InnerText;
                PMD.FromField = dt.Rows[0]["PatientName"].ToString();
                PMD.Message = Uri.UnescapeDataString(Convert.ToString(dt.Rows[0]["Message"]));
                PMD.CreationDate = dt.Rows[0]["CreationDate"].ToString();
                PMD.ToField = dt.Rows[0]["ToField"].ToString();
                PMD.PatientEmail = dt.Rows[0]["PatientEmail"].ToString();
                PMD.DoctorEmail = dt.Rows[0]["DoctorEmail"].ToString();
                PMD.PatientId = Convert.ToInt32(dt.Rows[0]["PatientId"]);
                foreach (DataRow item in dt.Rows)
                {
                    PMD.lstattachement = new List<Attachements>();
                    PMD.lstattachement.Add(new Attachements()
                    {
                        AttachementFullName = item["AttachmentName"].ToString(),
                        Attachementid = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(item["Id"])) ? 0 : item["Id"]),
                        AttachementName = item["AttachmentName"].ToString().Substring(item["AttachmentName"].ToString().ToString().LastIndexOf("$") + 1)
                    });
                }
                //-- Timezone changes
                if(!string.IsNullOrWhiteSpace(PMD.CreationDate))
                {
                    var dtCreationDate = Convert.ToDateTime(PMD.CreationDate);
                    dtCreationDate = clsHelper.ConvertFromUTC(dtCreationDate, TimeZoneSystemName);
                    PMD.CreationDate = dtCreationDate.ToString();

                }

            }
            return PMD;
        }

        #endregion

        #region Code For Get Refferal Details
        public string ReferralCardId { get; set; }
        public List<ReferralDetails> GetRefferalDetailsById(int UserId, int MessageId, string Type, string TimeZoneSystemName)
        {
            lstRefferalDetails = new List<ReferralDetails>();

            try
            {
                DataTable dt = new DataTable();
                dt = ObjColleaguesData.GetReferralViewByID(MessageId, Type);
                if (dt != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow item in dt.Rows)
                    {
                        ReferralDetails ObjRefferalDetails = new ReferralDetails();
                        string ReferralId = objCommon.CheckNull(Convert.ToString(item["ReferralCardId"]), string.Empty);
                        ReferralCardId = ReferralId;
                        if (!string.IsNullOrEmpty(ReferralId))
                        {
                            ObjRefferalDetails.ReferralCardId = Convert.ToInt32(item["ReferralCardId"]);
                        }


                        ObjRefferalDetails.MessageId = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(item["MessageId"])) ? 0 : item["MessageId"]);
                        ObjRefferalDetails.SenderId = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(item["SenderID"])) ? 0 : item["SenderID"]);
                        ObjRefferalDetails.ReceiverID = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(item["ReceiverID"])) ? 0 : item["ReceiverID"]);
                        ObjRefferalDetails.PatientId = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(item["PatientId"])) ? 0 : item["PatientId"]);
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(item["ProfileImage"])))
                        {
                            ObjRefferalDetails.ProfileImage = objCommon.CheckNull(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("ImageBank")) + Convert.ToString(item["ProfileImage"]), string.Empty);
                        }
                        else
                        {
                            if (Convert.ToString(item["Gender"]) == "Female")
                            {
                                ObjRefferalDetails.ProfileImage = objCommon.CheckNull(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("DefaultDoctorFemaleImage")), string.Empty);
                            }
                            else
                            {
                                ObjRefferalDetails.ProfileImage = objCommon.CheckNull(Convert.ToString(System.Configuration.ConfigurationManager.AppSettings.Get("DefaultDoctorImage")), string.Empty);
                            }
                        }



                        string CreationDate = string.Empty;
                        if (!string.IsNullOrEmpty(Convert.ToString(item["CreationDate"])))
                            CreationDate = Convert.ToString(clsHelper.ConvertFromUTC(Convert.ToDateTime(item["CreationDate"]), TimeZoneSystemName));
                        string strDateOfBirth = string.Empty;

                        ObjRefferalDetails.FullName = objCommon.CheckNull(Convert.ToString(item["FullName"]), string.Empty);
                        ObjRefferalDetails.Gender = objCommon.CheckNull(Convert.ToString(item["Gender"]), string.Empty);
                        ObjRefferalDetails.DateOfBirth = objCommon.CheckNull(Convert.ToString(item["DateOfBirth"]), string.Empty);
                        ObjRefferalDetails.SenderName = objCommon.CheckNull(Convert.ToString(item["SenderName"]), string.Empty);
                        ObjRefferalDetails.ReceiverName = objCommon.CheckNull(Convert.ToString(item["ReceiverName"]), string.Empty);
                        ObjRefferalDetails.FullAddress = objCommon.CheckNull(Convert.ToString(item["FullAddress"]), string.Empty);
                        ObjRefferalDetails.EmailAddress = objCommon.CheckNull(Convert.ToString(item["EmailAddress"]), string.Empty);
                        ObjRefferalDetails.Location = objCommon.CheckNull(Convert.ToString(item["ReferralLocation"]), string.Empty);
                        ObjRefferalDetails.RegardOption = objCommon.CheckNull(Convert.ToString(item["RegardOption"]), string.Empty);
                        ObjRefferalDetails.RequestingOption = objCommon.CheckNull(Convert.ToString(item["RequestingOption"]), string.Empty);
                        ObjRefferalDetails.OtherComments = objCommon.CheckNull(Convert.ToString(item["OtherComments"]), string.Empty);
                        ObjRefferalDetails.RequestComments = objCommon.CheckNull(Convert.ToString(item["RequestComments"]), string.Empty);
                        ObjRefferalDetails.Comments = objCommon.CheckNull(Convert.ToString(item["Comments"]), string.Empty);
                        ObjRefferalDetails.CreationDate = CreationDate;


                        if (!string.IsNullOrEmpty(ReferralId))
                        {



                            DataTable dtDocument = new DataTable();
                            // Datatable For Document
                            dtDocument = ObjColleaguesData.GetRefferalDocumentAttachemntsById(Convert.ToInt32(item["ReferralCardId"]), ObjRefferalDetails.PatientId);

                            List<MessageAttachment> lstDocument = new List<MessageAttachment>();
                            foreach (DataRow Document in dtDocument.Rows)
                            {
                                MessageAttachment ObjMessageAttachment = new MessageAttachment();
                                ObjMessageAttachment.AttachmentId = int.Parse(objCommon.CheckNull(Document["DocumentId"].ToString(), "0"));
                                ObjMessageAttachment.AttachmentName = objCommon.CheckNull(Document["DocumentName"].ToString(), string.Empty);
                                lstDocument.Add(ObjMessageAttachment);
                            }
                            ObjRefferalDetails.lstDocuments = lstDocument;

                            // Datatable For Image
                            DataTable dtImage = new DataTable();
                            dtImage = ObjColleaguesData.GetRefferalImageById(Convert.ToInt32(item["ReferralCardId"]), ObjRefferalDetails.PatientId);
                            List<MessageAttachment> lstImage = new List<MessageAttachment>();
                            foreach (DataRow Image in dtImage.Rows)
                            {
                                MessageAttachment ObjMessageAttachment = new MessageAttachment();
                                ObjMessageAttachment.AttachmentId = int.Parse(objCommon.CheckNull(Image["ImageId"].ToString(), "0"));
                                ObjMessageAttachment.AttachmentName = objCommon.CheckNull(Image["Name"].ToString(), string.Empty);
                                ObjMessageAttachment.AttachementFullName = System.Configuration.ConfigurationManager.AppSettings.Get("ImageBank") + objCommon.CheckNull(Image["Name"].ToString(), string.Empty);
                                lstImage.Add(ObjMessageAttachment);
                            }
                            ObjRefferalDetails.lstImages = lstImage;


                            //ToothType
                            DataTable dtToothTest = ObjColleaguesData.GetReferralToothDetailsByReferralCardId(ObjRefferalDetails.ReferralCardId);
                            List<ToothList> lstToothList = new List<ToothList>();
                            ObjRefferalDetails.lstSelectedTeeths = new List<Int32>();
                            ObjRefferalDetails.lstSelectedPermanentTeeths = new List<Int32>();
                            ObjRefferalDetails.lstSelectedPrimaryTeeths = new List<Int32>();
                            foreach (DataRow ToothTest in dtToothTest.Rows)
                            {
                                ToothList ObjToothList = new ToothList();
                                ObjToothList.ID = int.Parse(objCommon.CheckNull(ToothTest["ReferralDetailId"].ToString(), "0"));
                                ObjToothList.ToothType = Convert.ToInt32(ToothTest["ToothType"].ToString());
                                lstToothList.Add(ObjToothList);
                                if (Convert.ToInt32(ToothTest["ToothType"]) <= 32)
                                {
                                    ObjRefferalDetails.lstSelectedPermanentTeeths.Add(Convert.ToInt32(ToothTest["ToothType"].ToString()));
                                }
                                else
                                {
                                    ObjRefferalDetails.lstSelectedPrimaryTeeths.Add(Convert.ToInt32(ToothTest["ToothType"].ToString()));
                                }
                                ObjRefferalDetails.lstSelectedTeeths.Add(Convert.ToInt32(ToothTest["ToothType"].ToString()));
                            }
                            ObjRefferalDetails.lstToothList = lstToothList;
                        }

                        lstRefferalDetails.Add(ObjRefferalDetails);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstRefferalDetails;
        }

        public List<MessageAttachments> ColleagueMessageAttachments(int MessageId)
        {
            DataTable dt = new DataTable();
            List<MessageAttachments> _listMessageAttechment = new List<MessageAttachments>();
            string qry = "select * from ColleagueMessageAttachments where MessageId =" + MessageId;
            dt = objCommon.DataTable(qry);
            if (dt.Rows.Count > 0)
            {
                _listMessageAttechment = dt.DataTableToList<MessageAttachments>();                
            }
            return _listMessageAttechment;
        }
        public List<MessageAttachments> MessageAttachments(int MessageId)
        {
            DataTable dt = new DataTable();
            List<MessageAttachments> _listMessageAttechment = new List<MessageAttachments>();
            string qry = "select * from MessageAttachments where MessageId =" + MessageId;
            dt = objCommon.DataTable(qry);
            if (dt.Rows.Count > 0)
            {
                _listMessageAttechment = dt.DataTableToList<MessageAttachments>();
            }
            return _listMessageAttechment;
        }
        #endregion
        public bool InsertFamilyRelationDetails(int FromPatientId, int ToPatientId, int ReleativeId)
        {
            clsPatientsData objPatientsData = new clsPatientsData();
            return objPatientsData.InsertFamilyRelationDetails(FromPatientId, ToPatientId, ReleativeId);
        }
        public bool DeleteRelation(int FromPatientId, int ToPatientId)
        {
            clsPatientsData objPatientsData = new clsPatientsData();
            return objPatientsData.DeleteRelation(FromPatientId, ToPatientId);
        }
        public int CheckReleationIsExist(int FromPatientId, int ToPatientId)
        {
            clsPatientsData objPatientsData = new clsPatientsData();
            return objPatientsData.CheckReleationIsExist(FromPatientId, ToPatientId);
        }
        public int GetCrossReletiveId(int RelativeId, string Gender)
        {
            int CrossRelativeId = 0;
            if (RelativeId == (int)EnumFamilyRelation.GrandFather || RelativeId == (int)EnumFamilyRelation.GrandMother)
            {
                if (Gender == "Male") { CrossRelativeId = (int)EnumFamilyRelation.Grandson; } else { CrossRelativeId = (int)EnumFamilyRelation.Granddaughter; }
            }
            else if (RelativeId == (int)EnumFamilyRelation.Father || RelativeId == (int)EnumFamilyRelation.Mother)
            {
                if (Gender == "Male") { CrossRelativeId = (int)EnumFamilyRelation.Son; } else { CrossRelativeId = (int)EnumFamilyRelation.Daughter; }
            }
            else if (RelativeId == (int)EnumFamilyRelation.Son || RelativeId == (int)EnumFamilyRelation.Daughter)
            {
                if (Gender == "Male") { CrossRelativeId = (int)EnumFamilyRelation.Father; } else { CrossRelativeId = (int)EnumFamilyRelation.Mother; }
            }
            else if (RelativeId == (int)EnumFamilyRelation.Brother || RelativeId == (int)EnumFamilyRelation.Sister)
            {
                if (Gender == "Male") { CrossRelativeId = (int)EnumFamilyRelation.Brother; } else { CrossRelativeId = (int)EnumFamilyRelation.Sister; }
            }
            else if (RelativeId == (int)EnumFamilyRelation.Grandson || RelativeId == (int)EnumFamilyRelation.Granddaughter)
            {
                if (Gender == "Male") { CrossRelativeId = (int)EnumFamilyRelation.GrandFather; } else { CrossRelativeId = (int)EnumFamilyRelation.GrandMother; }
            }
            else if (RelativeId == (int)EnumFamilyRelation.Uncle || RelativeId == (int)EnumFamilyRelation.Aunt)
            {
                if (Gender == "Male") { CrossRelativeId = (int)EnumFamilyRelation.Nephew; } else { CrossRelativeId = (int)EnumFamilyRelation.Niece; }
            }
            else if (RelativeId == (int)EnumFamilyRelation.Nephew || RelativeId == (int)EnumFamilyRelation.Niece)
            {
                if (Gender == "Male") { CrossRelativeId = (int)EnumFamilyRelation.Uncle; } else { CrossRelativeId = (int)EnumFamilyRelation.Aunt; }
            }
            else if (RelativeId == (int)EnumFamilyRelation.Husband)
            {
                CrossRelativeId = (int)EnumFamilyRelation.Wife;
            }
            else if (RelativeId == (int)EnumFamilyRelation.Wife)
            {
                CrossRelativeId = (int)EnumFamilyRelation.Husband;
            }
            else if (RelativeId == (int)EnumFamilyRelation.Cousin)
            {
                CrossRelativeId = (int)EnumFamilyRelation.Cousin;
            }
            return CrossRelativeId;

        }
        public class SpecialityService
        {
            public int ServicesId { get; set; }
            public string ServiceName { get; set; }
            public int SpecialityId { get; set; }
            public int DentialServiceId { get; set; }
            public string SpecialityName { get; set; }
        }
        public List<SpecialityService> GetVisibleDetails(int regardId, int clgId)
        {
            List<SpecialityService> lstSp = new List<SpecialityService>();
            SqlParameter[] sqlpara = new SqlParameter[]
            {
                new SqlParameter{ParameterName="@regardId",Value=regardId},
                new SqlParameter{ParameterName="@ColleagueId",Value=clgId}
            };
            DataTable dt = new clsHelper().DataTable("Usp_GetReferralItemVisibility", sqlpara);
            foreach (DataRow item in dt.Rows)
            {
                lstSp.Add(new SpecialityService()
                {
                    ServiceName = Convert.ToString(item["ServiceName"]),
                    ServicesId = Convert.ToInt32(item["ServicesId"]),
                    SpecialityId = Convert.ToInt32(item["SpecialityId"]),
                    DentialServiceId = Convert.ToInt32(item["DentialServiceId"])
                });

            }
            return lstSp;
        }
        public List<SpecialityService> GetVisibleSpeciality(int clgId)
        {
            List<SpecialityService> lstSp = new List<SpecialityService>();
            DataTable dt = clsColleaguesData.GetUserReferrlaFormSetting(clgId);
            foreach (DataRow item in dt.Rows)
            {
                lstSp.Add(new SpecialityService()
                {
                    SpecialityName = Convert.ToString(item["SpecialityName"]),
                    SpecialityId = Convert.ToInt32(item["SpecialtyId"])
                });

            }
            return lstSp;
        }
        public List<SelectListItem> lstDentistService { get; set; }
        public List<SelectListItem> lstDentistSpeciality { get; set; }
        public List<DentistSpecialityService> lstSpecialityService { get; set; }
    }

    public class DentistSpecialityService
    {
        public int ServiceId { get; set; }
        public int SpecialityId { get; set; }
        public string SpecialityName { get; set; }
        public string ServiceName { get; set; }
        public string NameFor { get; set; }
        public bool IsEnable { get; set; }
    }

    public class PatientDetailsOfDoctor
    {
        public int PatientId { get; set; }
        public string AssignedPatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Gender { get; set; }
        public string Age { get; set; }
        public string Phone { get; set; }
        public string ExactAddress { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string ImageName { get; set; }
        public int OwnerId { get; set; }
        public int TotalRecord { get; set; }
        public int ColleaguesId { get; set; }
        public string ColleaguesName { get; set; }
        public string ReferredBy { get; set; }
        public int ReferredById { get; set; }
        public string Location { get; set; }
        public bool Voice { get; set; }
        public bool Text { get; set; }
        public bool ChkEmail { get; set; }
        public int AccountId { get; set; }
        public int MailCount { get; set; }
        public string LastSentDate { get; set; }
    }

    public class APIResponseBase
    {
        public string MethodName { get; set; }
        public int ResultCode { get; set; }
        public bool IsSuccess { get; set; }
        public string StatusMessage { get; set; }

    }

    public class PatientDetailResponse : APIResponseBase
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int TotalRecords { get; set; }
        public List<PatientDetails> PatientList { get; set; }
    }



    public class PatientDetails
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string BirthDate { get; set; }
        public DateTime? LastAppointmentDate { get; set; }
        public DateTime? AppointmentDate { get; set; }

    }
    public class PatientNotes
    {
        public int NoteId { get; set; }
        public string Note { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImageName { get; set; }
        public string CreationDate { get; set; }
        public int UserId { get; set; }
        public string PatientFullName { get; set; }
        public int PatientId { get; set; }
        public string DoctorName { get; set; }
        public string DoctorEmail { get; set; }
    }

    public class SocialMediaDetailsForPatients
    {
        public int PatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FacebookUrl { get; set; }
        public string LinkedinUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string GoogleplusUrl { get; set; }
        public string YoutubeUrl { get; set; }
        public string PinterestUrl { get; set; }
        public string BlogUrl { get; set; }
        public string OtherUrl { get; set; }
        public int TotalRecord { get; set; }
        public int cloudscore { get; set; }
    }

    public class PatientReferralHistoryDetails
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CreationDate { get; set; }
        public string SenderName { get; set; }
        public string ReceiverName { get; set; }
        public string Comments { get; set; }
        public int TotalRecord { get; set; }

    }



    public class ReferralHistoryOfPatient
    {
        public string FromField { get; set; }
        public string ToField { get; set; }
        public string CreationDate { get; set; }
        public string Status { get; set; }
        public string Comments { get; set; }
        public int TotalRecord { get; set; }
        public int MessageId { get; set; }
        public int ReferralCardId { get; set; }
        public int ReferedUserId { get; set; }
    }

    public class PatientMessagesOfDoctor
    {
        public int PatientmessageId { get; set; }
        public string CreationDate { get; set; }
        public string FromField { get; set; }
        public string ToField { get; set; }
        public int PatientId { get; set; }
        public string Message { get; set; }
        public int MessageId { get; set; }
        public int TotalRecord { get; set; }
        public string PatientEmail { get; set; }
        public string DoctorEmail { get; set; }

        public List<Attachements> lstattachement { get; set; }
    }

    public class TempUpload
    {
        public int DocumentId { get; set; }
        public string DocumentName { get; set; }
    }

    public class PatientImages
    {
        public int ImageId { get; set; }
        public int ImageType { get; set; }
        public int MontageId { get; set; }
        public string CreationDate { get; set; }
        public string LastModifiedDate { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }

    }
    public class AppointmentReport
    {
        public int AppointmentId { get; set; }
        public int DoctorId { get; set; }
        public int PatientId { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientLastName { get; set; }
        public string DoctorName { get; set; }
        public string AppointmentDate { get; set; }
        public string AppointmentTime { get; set; }
        public string AppointmentType { get; set; }
        public string PatientDOB { get; set; }
        public string NextAppointmentDate { get; set; }
        public string NextAppointmentType { get; set; }
        public string LastAppointmentDate { get; set; }
        public string LastHygieneDate { get; set; }
        public string LastHygieneTime { get; set; }
        public string HygieneType { get; set; }
        public string PatientAge { get; set; }
        public string AppointmentReason { get; set; }
        public string Alert { get; set; }
        public string Phone { get; set; }
    }
    public class PateintDocuments
    {
        public int DocumentId { get; set; }
        public int MontageId { get; set; }
        public string DocumentName { get; set; }
        public string Description { get; set; }
        public string CreationDate { get; set; }
        public string LastModifiedDate { get; set; }

    }
    public class Attachements
    {
        public int Attachementid { get; set; }
        public string AttachementName { get; set; }
        public string AttachementFullName { get; set; }
    }
    public class DoctorLocation
    {
        public int LocationId { get; set; }
        public string LocationName { get; set; }
    }

}