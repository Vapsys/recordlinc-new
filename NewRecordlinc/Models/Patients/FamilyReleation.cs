﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewRecordlinc.Models.Patients
{
    public class FamilyReleation
    {
        public int RelativeId { get; set; }
        public string ReleationName { get; set; }
    }
    public class FamilyReleationHistory
    {
        public int ReletaiveId { get; set; }
        public int PatientId { get; set; }

        public string PatientName { get; set; }
        public string ReleationName { get; set; }
        public int ToPatientId { get; set; }
    }
    public enum EnumFamilyRelation
    {
        GrandFather = 1,
        GrandMother = 2,
        Father = 3,
        Mother = 4,
        Son = 5,
        Daughter = 6,
        Husband = 7,
        Wife = 8,
        Brother = 9,
        Sister = 10,
        Grandson = 11,
        Granddaughter = 12,
        Uncle = 13,
        Aunt = 14,
        Nephew = 15,
        Niece = 16,
        Cousin = 17
    }
    public enum FamilyStatus
    {
        IsSuccess = 1,
        IsExist = 2,
        IsFailed = 3

    }
}