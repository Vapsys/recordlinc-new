﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using DataAccessLayer.Common;
using System.Data;

namespace NewRecordlinc.Models
{
    public class UpdatePatientImage
    {
        clsHelper clshelper = new clsHelper();
        public bool UpdatePatientProfileImg(int PatientId, string ProfileImage)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] addPatientParameters = new SqlParameter[2];

                addPatientParameters[0] = new SqlParameter("@PatientId", SqlDbType.Int);
                //addPatientParameters[0].ParameterName = "@PatientId";
                addPatientParameters[0].Value = PatientId;

                addPatientParameters[1] = new SqlParameter("@ProfileImage",SqlDbType.VarChar);
               // addPatientParameters[1].ParameterName = "@ProfileImage";
                addPatientParameters[1].Value = ProfileImage;

                UpdateStatus = clshelper.ExecuteNonQuery("UpdatePatientProfileImg", addPatientParameters);

                return UpdateStatus;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}