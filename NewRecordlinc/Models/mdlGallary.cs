﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccessLayer.ProfileData;
using System.Data;

namespace NewRecordlinc.Models
{
    public class mdlGallary
    {
        public int GallaryId { get; set; }
        public string VideoURL { get; set; }
        public HttpPostedFileBase File { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public int UserId { get; set; }

        public static mdlGallary getGallaryById(int UserId, int GallaryId, string defaultPath)
        {
            string filePath = System.Configuration.ConfigurationManager.AppSettings.Get("GallaryFilePath");
            if(string.IsNullOrEmpty(filePath))
            {
                filePath = defaultPath;
            }
            filePath = HttpContext.Current.Server.MapPath(filePath);
            var objProfileData = new clsProfileData();
            mdlGallary model = new mdlGallary();
            DataTable dt = new DataTable();
            dt = objProfileData.getGallaryById(UserId, GallaryId);
            if(dt != null && dt.Rows.Count > 0)
            {
                if(Convert.ToInt32(Convert.ToString(dt.Rows[0]["MediaType"])) == (int)GalleryConstants.IMAGE_URL)
                {
                    model.FilePath = filePath + Convert.ToString(dt.Rows[0]["MediaUrl"]);
                    model.FileName = Convert.ToString(dt.Rows[0]["MediaUrl"]);
                }
                else
                {
                    model.VideoURL = Convert.ToString(dt.Rows[0]["MediaUrl"]);
                }
                model.GallaryId = GallaryId;
                model.UserId = Convert.ToInt32(dt.Rows[0]["UserId"]);
            }
            return model;
        }
        public static List<mdlGallary> getGallaryList(int UserId, int GallaryId)
        {
           
            var objProfileData = new clsProfileData();
            List<mdlGallary> list = new List<mdlGallary>();
            mdlGallary model = new mdlGallary();
            DataTable dt = new DataTable();
            dt = objProfileData.getGallaryById(UserId, GallaryId);
            if (dt != null && dt.Rows.Count > 0)
            {
                list = (from p in dt.AsEnumerable()
                 select new mdlGallary
                 {
                     FileName = Convert.ToInt32(Convert.ToString(p["MediaType"])) == (int)GalleryConstants.IMAGE_URL ? Convert.ToString(p["MediaUrl"]) : null,
                     VideoURL = Convert.ToInt32(Convert.ToString(p["MediaType"])) == (int)GalleryConstants.VIDEO_URL ? Convert.ToString(p["MediaUrl"]) : null,
                     GallaryId = Convert.ToInt32(p["GallaryId"]),
                     UserId = Convert.ToInt32(p["UserId"])
                 }).ToList();

                
                //if (Convert.ToInt32(Convert.ToString(dt.Rows[0]["MediaType"])) == (int)GalleryConstants.IMAGE_URL)
                //{
                //    model.FileName = Convert.ToString(dt.Rows[0]["MediaUrl"]);
                //}
                //else
                //{
                //    model.VideoURL = Convert.ToString(dt.Rows[0]["MediaUrl"]);
                //}
                //model.GallaryId = GallaryId;
            }
            return list;
        }
        public enum GalleryConstants
        {
            IMAGE_URL = 0,
            VIDEO_URL
        }
    }
}