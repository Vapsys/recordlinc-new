﻿#pragma warning disable 1591
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace NewRecordlinc.Models.Demo
{
	using System.Data.Linq;
	using System.Data.Linq.Mapping;
	using System.Data;
	using System.Collections.Generic;
	using System.Reflection;
	using System.Linq;
	using System.Linq.Expressions;
	using System.ComponentModel;
	using System;
	
	
	[global::System.Data.Linq.Mapping.DatabaseAttribute(Name="Recordlinc")]
	public partial class dbDemoDataContext : System.Data.Linq.DataContext
	{
		
		private static System.Data.Linq.Mapping.MappingSource mappingSource = new AttributeMappingSource();
		
    #region Extensibility Method Definitions
    partial void OnCreated();
    #endregion
		
		public dbDemoDataContext() : 
				base(global::System.Configuration.ConfigurationManager.ConnectionStrings["RecordlincConnectionString"].ConnectionString, mappingSource)
		{
			OnCreated();
		}
		
		public dbDemoDataContext(string connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public dbDemoDataContext(System.Data.IDbConnection connection) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public dbDemoDataContext(string connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		public dbDemoDataContext(System.Data.IDbConnection connection, System.Data.Linq.Mapping.MappingSource mappingSource) : 
				base(connection, mappingSource)
		{
			OnCreated();
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.Demo_ImageStoredInsert")]
		public int Demo_ImageStoredInsert([global::System.Data.Linq.Mapping.ParameterAttribute(Name="RealImageSource", DbType="NVarChar(MAX)")] string realImageSource, [global::System.Data.Linq.Mapping.ParameterAttribute(Name="EncrypImageSource", DbType="NVarChar(MAX)")] string encrypImageSource)
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())), realImageSource, encrypImageSource);
			return ((int)(result.ReturnValue));
		}
		
		[global::System.Data.Linq.Mapping.FunctionAttribute(Name="dbo.Demo_ImageStoredSelect")]
		public ISingleResult<Demo_ImageStoredSelectResult> Demo_ImageStoredSelect()
		{
			IExecuteResult result = this.ExecuteMethodCall(this, ((MethodInfo)(MethodInfo.GetCurrentMethod())));
			return ((ISingleResult<Demo_ImageStoredSelectResult>)(result.ReturnValue));
		}
	}
	
	public partial class Demo_ImageStoredSelectResult
	{
		
		private long _id;
		
		private string _RealImageSource;
		
		private string _EncrypImageSource;
		
		public Demo_ImageStoredSelectResult()
		{
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_id", DbType="BigInt NOT NULL")]
		public long id
		{
			get
			{
				return this._id;
			}
			set
			{
				if ((this._id != value))
				{
					this._id = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_RealImageSource", DbType="NVarChar(MAX) NOT NULL", CanBeNull=false)]
		public string RealImageSource
		{
			get
			{
				return this._RealImageSource;
			}
			set
			{
				if ((this._RealImageSource != value))
				{
					this._RealImageSource = value;
				}
			}
		}
		
		[global::System.Data.Linq.Mapping.ColumnAttribute(Storage="_EncrypImageSource", DbType="NVarChar(MAX)")]
		public string EncrypImageSource
		{
			get
			{
				return this._EncrypImageSource;
			}
			set
			{
				if ((this._EncrypImageSource != value))
				{
					this._EncrypImageSource = value;
				}
			}
		}
	}
}
#pragma warning restore 1591
