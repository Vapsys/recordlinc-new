﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NewRecordlinc.Models.Demo
{
    public class mdlImageConvertl
    {

        #region Property

        readonly dbDemoDataContext db = new dbDemoDataContext();

        public string strImageRealSource { get; set; }
        public string strImageEncryptSource { get; set; }
        public string strTableScript { get; set; }

        #endregion


        #region Methods


        public List<Demo_ImageStoredSelectResult> GetImageList()
        {
            return db.Demo_ImageStoredSelect().ToList();
        }

        public void InsertImage(mdlImageConvertl obj)
        {
            db.Demo_ImageStoredInsert(obj.strImageRealSource, obj.strImageEncryptSource);
        }

        #endregion
    }


}