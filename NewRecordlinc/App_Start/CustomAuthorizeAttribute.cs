﻿using BusinessLogicLayer;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Mvc;
using System.Web.Routing;
namespace NewRecordlinc.App_Start
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (string.IsNullOrEmpty(SessionManagement.UserId) && string.IsNullOrEmpty(SessionManagement.AdminUserId))
            {

                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    var urlHelper = new UrlHelper(filterContext.RequestContext);
                    filterContext.HttpContext.Response.StatusCode = 403;
                    filterContext.Result = new JsonResult
                    {
                        Data = new
                        {
                            Error = "NotAuthorized",
                            LogOnUrl = urlHelper.Action("NewIndex", "User")
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                else
                {
                    filterContext.Result = new RedirectToRouteResult(new
                                           RouteValueDictionary(new { controller = "User", action = "Index", }));
                    filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
                  // base.OnAuthorization(filterContext);
                }
            }

        }
    }
    public class AdminAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (SessionManagement.AdminUserId == null || SessionManagement.AdminUserId == "")
            {
                if (filterContext.HttpContext.Request.IsAjaxRequest())
                {
                    var urlHelper = new UrlHelper(filterContext.RequestContext);
                    filterContext.HttpContext.Response.StatusCode = 403;
                    filterContext.Result = new JsonResult
                    {
                        Data = new
                        {
                            Error = "NotAuthorized",
                            LogOnUrl = urlHelper.Action("Index", "User")
                        },
                        JsonRequestBehavior = JsonRequestBehavior.AllowGet
                    };
                }
                else
                {
                    filterContext.Result = new RedirectToRouteResult(new
                                           RouteValueDictionary(new { controller = "User", action = "Index", }));
                    filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
                    // base.OnAuthorization(filterContext);
                }
            }

        }
    }

    public class PayCheckAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {            
            if (SessionManagement.UserPaymentVerified)
            {
                if (filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "FreeToPaid") return;
                if (filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Error") return;
                if (filterContext.ActionDescriptor.ControllerDescriptor.ControllerName == "Common") return;
                if (filterContext.ActionDescriptor.ActionName == "Logout") return;
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new
                {
                    controller = "FreeToPaid",
                    action = "UpgradeAccount"
                }));
                filterContext.Result.ExecuteResult(filterContext.Controller.ControllerContext);
            }                      
        }
    }
}