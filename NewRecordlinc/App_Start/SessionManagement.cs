﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using NewRecordlinc.Models;
using System.Configuration;
using DataAccessLayer.Common;
using BO.ViewModel;

namespace NewRecordlinc.App_Start
{
    public class SessionManagement
    {
        public static string UserId
        {
            get { return Convert.ToString(HttpContext.Current.Session["UserId"]); }
            set { HttpContext.Current.Session["UserId"] = value; }
        }
        public static string DoctorImage
        {
            get { return Convert.ToString(HttpContext.Current.Session["DoctorImage"]); }
            set { HttpContext.Current.Session["DoctorImage"] = value; }
        }
        public static string FirstName
        {
            get { return Convert.ToString(HttpContext.Current.Session["FirstName"]); }
            set { HttpContext.Current.Session["FirstName"] = value; }
        }


        public static string AdminUserId
        {
            get { return Convert.ToString(HttpContext.Current.Session["AdminUserId"]); }
            set { HttpContext.Current.Session["AdminUserId"] = value; }
        }
        public static string LocationId
        {
            get { return Convert.ToString(HttpContext.Current.Session["LocationId"]); }
            set { HttpContext.Current.Session["LocationId"] = value; }
        }
        public static string ParentUserId
        {
            get { return Convert.ToString(HttpContext.Current.Session["ParentUserId"]); }
            set { HttpContext.Current.Session["ParentUserId"] = value; }
        }

        public static string TimeZoneId
        {
            get { return Convert.ToString(HttpContext.Current.Session["TimeZoneId"]); }
            set { HttpContext.Current.Session["TimeZoneId"] = value; }
        }
        public static string TimeZoneSystemName
        {
            get { return Convert.ToString(HttpContext.Current.Session["TimeZoneSystemName"]); }
            set { HttpContext.Current.Session["TimeZoneSystemName"] = value; }
        }
        public static List<MemberPlanDetail> GetMemberPlanDetails
        {
            get
            {
                if (HttpContext.Current.Session["GetMemberPlanDetails"] == null)
                {
                    HttpContext.Current.Session["GetMemberPlanDetails"] = new List<MemberPlanDetail>();
                }
                return HttpContext.Current.Session["GetMemberPlanDetails"] as List<MemberPlanDetail>;
            }
            set { HttpContext.Current.Session["GetMemberPlanDetails"] = value; }
        }

        public static PMSSendReferralDetails PMSSendReferralDetails
        {
            get { return (PMSSendReferralDetails) HttpContext.Current.Session["PMSSendReferralDetails"]; }
            set { HttpContext.Current.Session["PMSSendReferralDetails"] = value; }
        }

        public static bool UserPaymentVerified
        {
            get { return Convert.ToBoolean(HttpContext.Current.Session["UserPaymentVerified"]); }
            set { HttpContext.Current.Session["UserPaymentVerified"] = value; }
        }

        public static bool PublicProfileReviews
        {
            get { return Convert.ToBoolean(HttpContext.Current.Session["PublicProfileReviews"]); }
            set { HttpContext.Current.Session["PublicProfileReviews"] = value; }
        }
    }
}