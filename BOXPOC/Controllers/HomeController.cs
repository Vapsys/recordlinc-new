﻿using Box.V2;
using Box.V2.Auth;
using Box.V2.Config;
using Box.V2.JWTAuth;
using Box.V2.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BOXPOC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult FileUpload(HttpPostedFileBase file)
        {
            try
            {
                var boxConfig = new BoxConfig("zb26m6aekk09asuveffr0ee7248lig0j",
                    "LCaux9uTJDo3Tdv3cHD3UcwnTrUFWsgj",
                    "121489751",
                    "-----BEGIN ENCRYPTED PRIVATE KEY-----\nMIIFDjBABgkqhkiG9w0BBQ0wMzAbBgkqhkiG9w0BBQwwDgQI6h7ABizx+M8CAggA\nMBQGCCqGSIb3DQMHBAjEVjKmHM34mQSCBMhSxspcFX06ecS5aAaKH2Exnx/V1o6W\njk5G/fNA0Z+PUceQXnpz+dHCl538nuDb1hz1liFvmhRgsyO7Cg/rARH7PXtv0Ao3\nCA5EfIdQsJKF2EskfZT3oTPQ62Sr2GIkXqvQYbDovEzLDsDvtHZKjE0FsfUGiDwz\nseVRRSY/+aUUYZkgIi42/tc50lIwdT07RZjY3hHqP3UGqbiXMQj9+sPSIlNAu3Qu\n6rT7gHGgW8ZGlUOXSFcdmenBF8BOt0oi4vcKqEDwh4E2uLNkf3lLEWaKNb7VnlyH\nHucXxii75OHG5v8rTj3Wm2LMQi1vTpjjNV6SH1ySeOHdcxwf7VZ2XWcAktjGeGWj\nMsDpFCIqFpbFFjpQ80XCsMZ/4Ghlw80EYfe+U3PALoaSfQ8eeD9T1h0D8AHmXnsI\ntHSDA6IeL2jd68s4d9FFdNC84+jdJjIE9KPaz2bH+8gV8H/xp7ePwStTdKy1eBqp\nv/xyyRh9smnwcCzGsucyYdbtTq6dtneY4tCgYMtcM8anKa7xKLYh9t42MOsB/Zjr\n0ibylcnMSbRRGTlsQZr3YM6dPxBxrPdsd3shoicaF9rc7gNycslHMDmjc7gEAdDQ\nRNevJsTcaI/jCXLb1hkNd00bwy+sX+ABrwXdnqcAyk0xe+HNpSoxgxGJ3Hk7wJ50\nWLo6YMfkDrLKyoIc/sD1de/5v2Yd9iPCosEIr0yNa+FGNmkvp7jXpIb/G741VUgR\nDc6S5I+mqeHN0W2474TJirk0QsuAE42czhe8qTgetKPJpESjcW0kMYhW3/hgksvR\niDpasqW6jHn4++xylcQlyVuCUSBzi00BkA/HvnDnrIy2hN8T8mbrxvHZKMf4SvFg\nRL7IJ0OKtiliceRnGDVBe8zMFMn9cfotnS9C9VGgFEIzzdbNmTvjgMDV6Lx7pMZw\nj6DV1TgtO1PG1t1szoMysgSMMotLzQcTK0pA67nNfpAOuM6aDFCTNsWz0YMkNx7j\nqJvMZAgycC4nH4cBkOsRo99N2jDZPtgfn2/yHVLiy8yDuNctWM2u+48qYuBzUjg0\nWA7+Y29bLwkO0biPqUrHiYM+/ckryROKfMi5f+SSr6lJWXCRWgHyPw9mKmplmgNX\n9Rwqk+qQgccjCx3dCxllhX6ycXmtfUWd86s0OQ/TigwTxkQan3POJyaO/8cFC386\n6EGdshBdDhmvrOFQAFmhwiovIL767khEh4Pa9qvxACN9kZ5EfAdqApeZBluRsUy9\nXcaJ16mccqkDzRXOyXazLWHQdQeKi2P9XmUy5QlJNxRUy6ughxtpqC//iAGxbdlz\ncjmpxedb1eNV9JO+pycMuG0MCeOgD5HyQTv/ad3yvRpMP35J1k8NuHAR1rJGsRSg\necDs+2dg2HbePaYMKgzEzxFIa9OGCydv28+8o5mVZ2r7gQel5/3irJJ0rlu24Y/q\nyAjTz0xLkzuQfLk5la+USKGSZs60NX8qo70IP9WLozgE2pEW3aMQwRDz5qhSNw9F\ng7tNvBKfYItEVuNmYzgRfC+IxWXokTN2IdjvosM8c/8FE8Xn29gAsfcoVzIjrTP3\nqW3WQZjG2XwALVZRSydBC1HJ26Q96vpjlu/m8E2u9mnfhrg5/51OUOqv9Ak2yIAD\nUyk=\n-----END ENCRYPTED PRIVATE KEY-----\n",
                    "3025581380b6d8345a7aa309548967c6",
                    "ihmb7hh8");
                var boxJWT = new BoxJWTAuth(boxConfig);
                var userToken = boxJWT.UserToken("6446395751");
                var client = boxJWT.UserClient(userToken, "6446395751");
                BoxFileRequest req = new BoxFileRequest()
                {
                    Name = file.FileName,
                    Parent = new BoxRequestEntity() { Id = "64118254178" }
                };
                var newFile = client.FilesManager.UploadAsync(req, file.InputStream, timeout: new TimeSpan(1, 0, 0)).Result;
                return Json(new { success = true, data = true });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, data = ex.Message });
            }
        }
        [HttpPost]
        public ActionResult Delete(string id)
        {
            try
            {
                var boxConfig = new BoxConfig("zb26m6aekk09asuveffr0ee7248lig0j",
                  "LCaux9uTJDo3Tdv3cHD3UcwnTrUFWsgj",
                  "121489751",
                  "-----BEGIN ENCRYPTED PRIVATE KEY-----\nMIIFDjBABgkqhkiG9w0BBQ0wMzAbBgkqhkiG9w0BBQwwDgQI6h7ABizx+M8CAggA\nMBQGCCqGSIb3DQMHBAjEVjKmHM34mQSCBMhSxspcFX06ecS5aAaKH2Exnx/V1o6W\njk5G/fNA0Z+PUceQXnpz+dHCl538nuDb1hz1liFvmhRgsyO7Cg/rARH7PXtv0Ao3\nCA5EfIdQsJKF2EskfZT3oTPQ62Sr2GIkXqvQYbDovEzLDsDvtHZKjE0FsfUGiDwz\nseVRRSY/+aUUYZkgIi42/tc50lIwdT07RZjY3hHqP3UGqbiXMQj9+sPSIlNAu3Qu\n6rT7gHGgW8ZGlUOXSFcdmenBF8BOt0oi4vcKqEDwh4E2uLNkf3lLEWaKNb7VnlyH\nHucXxii75OHG5v8rTj3Wm2LMQi1vTpjjNV6SH1ySeOHdcxwf7VZ2XWcAktjGeGWj\nMsDpFCIqFpbFFjpQ80XCsMZ/4Ghlw80EYfe+U3PALoaSfQ8eeD9T1h0D8AHmXnsI\ntHSDA6IeL2jd68s4d9FFdNC84+jdJjIE9KPaz2bH+8gV8H/xp7ePwStTdKy1eBqp\nv/xyyRh9smnwcCzGsucyYdbtTq6dtneY4tCgYMtcM8anKa7xKLYh9t42MOsB/Zjr\n0ibylcnMSbRRGTlsQZr3YM6dPxBxrPdsd3shoicaF9rc7gNycslHMDmjc7gEAdDQ\nRNevJsTcaI/jCXLb1hkNd00bwy+sX+ABrwXdnqcAyk0xe+HNpSoxgxGJ3Hk7wJ50\nWLo6YMfkDrLKyoIc/sD1de/5v2Yd9iPCosEIr0yNa+FGNmkvp7jXpIb/G741VUgR\nDc6S5I+mqeHN0W2474TJirk0QsuAE42czhe8qTgetKPJpESjcW0kMYhW3/hgksvR\niDpasqW6jHn4++xylcQlyVuCUSBzi00BkA/HvnDnrIy2hN8T8mbrxvHZKMf4SvFg\nRL7IJ0OKtiliceRnGDVBe8zMFMn9cfotnS9C9VGgFEIzzdbNmTvjgMDV6Lx7pMZw\nj6DV1TgtO1PG1t1szoMysgSMMotLzQcTK0pA67nNfpAOuM6aDFCTNsWz0YMkNx7j\nqJvMZAgycC4nH4cBkOsRo99N2jDZPtgfn2/yHVLiy8yDuNctWM2u+48qYuBzUjg0\nWA7+Y29bLwkO0biPqUrHiYM+/ckryROKfMi5f+SSr6lJWXCRWgHyPw9mKmplmgNX\n9Rwqk+qQgccjCx3dCxllhX6ycXmtfUWd86s0OQ/TigwTxkQan3POJyaO/8cFC386\n6EGdshBdDhmvrOFQAFmhwiovIL767khEh4Pa9qvxACN9kZ5EfAdqApeZBluRsUy9\nXcaJ16mccqkDzRXOyXazLWHQdQeKi2P9XmUy5QlJNxRUy6ughxtpqC//iAGxbdlz\ncjmpxedb1eNV9JO+pycMuG0MCeOgD5HyQTv/ad3yvRpMP35J1k8NuHAR1rJGsRSg\necDs+2dg2HbePaYMKgzEzxFIa9OGCydv28+8o5mVZ2r7gQel5/3irJJ0rlu24Y/q\nyAjTz0xLkzuQfLk5la+USKGSZs60NX8qo70IP9WLozgE2pEW3aMQwRDz5qhSNw9F\ng7tNvBKfYItEVuNmYzgRfC+IxWXokTN2IdjvosM8c/8FE8Xn29gAsfcoVzIjrTP3\nqW3WQZjG2XwALVZRSydBC1HJ26Q96vpjlu/m8E2u9mnfhrg5/51OUOqv9Ak2yIAD\nUyk=\n-----END ENCRYPTED PRIVATE KEY-----\n",
                  "3025581380b6d8345a7aa309548967c6",
                  "ihmb7hh8");
                var boxJWT = new BoxJWTAuth(boxConfig);
                var userToken = boxJWT.UserToken("6446395751");
                var client = boxJWT.UserClient(userToken, "6446395751");
                var result = client.FilesManager.DeleteAsync(id: id).Result;
                return Json(new { success = true, data = true });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, data = ex.Message });
            }
        }

        public ActionResult Download(string id)
        {
            try
            {
                var boxConfig = new BoxConfig("zb26m6aekk09asuveffr0ee7248lig0j",
                 "LCaux9uTJDo3Tdv3cHD3UcwnTrUFWsgj",
                 "121489751",
                 "-----BEGIN ENCRYPTED PRIVATE KEY-----\nMIIFDjBABgkqhkiG9w0BBQ0wMzAbBgkqhkiG9w0BBQwwDgQI6h7ABizx+M8CAggA\nMBQGCCqGSIb3DQMHBAjEVjKmHM34mQSCBMhSxspcFX06ecS5aAaKH2Exnx/V1o6W\njk5G/fNA0Z+PUceQXnpz+dHCl538nuDb1hz1liFvmhRgsyO7Cg/rARH7PXtv0Ao3\nCA5EfIdQsJKF2EskfZT3oTPQ62Sr2GIkXqvQYbDovEzLDsDvtHZKjE0FsfUGiDwz\nseVRRSY/+aUUYZkgIi42/tc50lIwdT07RZjY3hHqP3UGqbiXMQj9+sPSIlNAu3Qu\n6rT7gHGgW8ZGlUOXSFcdmenBF8BOt0oi4vcKqEDwh4E2uLNkf3lLEWaKNb7VnlyH\nHucXxii75OHG5v8rTj3Wm2LMQi1vTpjjNV6SH1ySeOHdcxwf7VZ2XWcAktjGeGWj\nMsDpFCIqFpbFFjpQ80XCsMZ/4Ghlw80EYfe+U3PALoaSfQ8eeD9T1h0D8AHmXnsI\ntHSDA6IeL2jd68s4d9FFdNC84+jdJjIE9KPaz2bH+8gV8H/xp7ePwStTdKy1eBqp\nv/xyyRh9smnwcCzGsucyYdbtTq6dtneY4tCgYMtcM8anKa7xKLYh9t42MOsB/Zjr\n0ibylcnMSbRRGTlsQZr3YM6dPxBxrPdsd3shoicaF9rc7gNycslHMDmjc7gEAdDQ\nRNevJsTcaI/jCXLb1hkNd00bwy+sX+ABrwXdnqcAyk0xe+HNpSoxgxGJ3Hk7wJ50\nWLo6YMfkDrLKyoIc/sD1de/5v2Yd9iPCosEIr0yNa+FGNmkvp7jXpIb/G741VUgR\nDc6S5I+mqeHN0W2474TJirk0QsuAE42czhe8qTgetKPJpESjcW0kMYhW3/hgksvR\niDpasqW6jHn4++xylcQlyVuCUSBzi00BkA/HvnDnrIy2hN8T8mbrxvHZKMf4SvFg\nRL7IJ0OKtiliceRnGDVBe8zMFMn9cfotnS9C9VGgFEIzzdbNmTvjgMDV6Lx7pMZw\nj6DV1TgtO1PG1t1szoMysgSMMotLzQcTK0pA67nNfpAOuM6aDFCTNsWz0YMkNx7j\nqJvMZAgycC4nH4cBkOsRo99N2jDZPtgfn2/yHVLiy8yDuNctWM2u+48qYuBzUjg0\nWA7+Y29bLwkO0biPqUrHiYM+/ckryROKfMi5f+SSr6lJWXCRWgHyPw9mKmplmgNX\n9Rwqk+qQgccjCx3dCxllhX6ycXmtfUWd86s0OQ/TigwTxkQan3POJyaO/8cFC386\n6EGdshBdDhmvrOFQAFmhwiovIL767khEh4Pa9qvxACN9kZ5EfAdqApeZBluRsUy9\nXcaJ16mccqkDzRXOyXazLWHQdQeKi2P9XmUy5QlJNxRUy6ughxtpqC//iAGxbdlz\ncjmpxedb1eNV9JO+pycMuG0MCeOgD5HyQTv/ad3yvRpMP35J1k8NuHAR1rJGsRSg\necDs+2dg2HbePaYMKgzEzxFIa9OGCydv28+8o5mVZ2r7gQel5/3irJJ0rlu24Y/q\nyAjTz0xLkzuQfLk5la+USKGSZs60NX8qo70IP9WLozgE2pEW3aMQwRDz5qhSNw9F\ng7tNvBKfYItEVuNmYzgRfC+IxWXokTN2IdjvosM8c/8FE8Xn29gAsfcoVzIjrTP3\nqW3WQZjG2XwALVZRSydBC1HJ26Q96vpjlu/m8E2u9mnfhrg5/51OUOqv9Ak2yIAD\nUyk=\n-----END ENCRYPTED PRIVATE KEY-----\n",
                 "3025581380b6d8345a7aa309548967c6",
                 "ihmb7hh8");
                var boxJWT = new BoxJWTAuth(boxConfig);
                var userToken = boxJWT.UserToken("6446395751");
                var client = boxJWT.UserClient(userToken, "6446395751");
                BoxFile file = client.FilesManager.GetInformationAsync(id: id).Result;
                Stream fileContents = client.FilesManager.DownloadStreamAsync(id: id).Result;
                return File(fileContents, "application/unknown", file.Name);
            }
            catch (Exception ex)
            {
                return RedirectToAction("DownloadError");
            }
        }

        public ActionResult DownloadError()
        {
            return View();
        }

        public ActionResult Documents()
        {
            List<BoxItem> model = null;
            try
            {
                var boxConfig = new BoxConfig("zb26m6aekk09asuveffr0ee7248lig0j",
                  "LCaux9uTJDo3Tdv3cHD3UcwnTrUFWsgj",
                  "121489751",
                  "-----BEGIN ENCRYPTED PRIVATE KEY-----\nMIIFDjBABgkqhkiG9w0BBQ0wMzAbBgkqhkiG9w0BBQwwDgQI6h7ABizx+M8CAggA\nMBQGCCqGSIb3DQMHBAjEVjKmHM34mQSCBMhSxspcFX06ecS5aAaKH2Exnx/V1o6W\njk5G/fNA0Z+PUceQXnpz+dHCl538nuDb1hz1liFvmhRgsyO7Cg/rARH7PXtv0Ao3\nCA5EfIdQsJKF2EskfZT3oTPQ62Sr2GIkXqvQYbDovEzLDsDvtHZKjE0FsfUGiDwz\nseVRRSY/+aUUYZkgIi42/tc50lIwdT07RZjY3hHqP3UGqbiXMQj9+sPSIlNAu3Qu\n6rT7gHGgW8ZGlUOXSFcdmenBF8BOt0oi4vcKqEDwh4E2uLNkf3lLEWaKNb7VnlyH\nHucXxii75OHG5v8rTj3Wm2LMQi1vTpjjNV6SH1ySeOHdcxwf7VZ2XWcAktjGeGWj\nMsDpFCIqFpbFFjpQ80XCsMZ/4Ghlw80EYfe+U3PALoaSfQ8eeD9T1h0D8AHmXnsI\ntHSDA6IeL2jd68s4d9FFdNC84+jdJjIE9KPaz2bH+8gV8H/xp7ePwStTdKy1eBqp\nv/xyyRh9smnwcCzGsucyYdbtTq6dtneY4tCgYMtcM8anKa7xKLYh9t42MOsB/Zjr\n0ibylcnMSbRRGTlsQZr3YM6dPxBxrPdsd3shoicaF9rc7gNycslHMDmjc7gEAdDQ\nRNevJsTcaI/jCXLb1hkNd00bwy+sX+ABrwXdnqcAyk0xe+HNpSoxgxGJ3Hk7wJ50\nWLo6YMfkDrLKyoIc/sD1de/5v2Yd9iPCosEIr0yNa+FGNmkvp7jXpIb/G741VUgR\nDc6S5I+mqeHN0W2474TJirk0QsuAE42czhe8qTgetKPJpESjcW0kMYhW3/hgksvR\niDpasqW6jHn4++xylcQlyVuCUSBzi00BkA/HvnDnrIy2hN8T8mbrxvHZKMf4SvFg\nRL7IJ0OKtiliceRnGDVBe8zMFMn9cfotnS9C9VGgFEIzzdbNmTvjgMDV6Lx7pMZw\nj6DV1TgtO1PG1t1szoMysgSMMotLzQcTK0pA67nNfpAOuM6aDFCTNsWz0YMkNx7j\nqJvMZAgycC4nH4cBkOsRo99N2jDZPtgfn2/yHVLiy8yDuNctWM2u+48qYuBzUjg0\nWA7+Y29bLwkO0biPqUrHiYM+/ckryROKfMi5f+SSr6lJWXCRWgHyPw9mKmplmgNX\n9Rwqk+qQgccjCx3dCxllhX6ycXmtfUWd86s0OQ/TigwTxkQan3POJyaO/8cFC386\n6EGdshBdDhmvrOFQAFmhwiovIL767khEh4Pa9qvxACN9kZ5EfAdqApeZBluRsUy9\nXcaJ16mccqkDzRXOyXazLWHQdQeKi2P9XmUy5QlJNxRUy6ughxtpqC//iAGxbdlz\ncjmpxedb1eNV9JO+pycMuG0MCeOgD5HyQTv/ad3yvRpMP35J1k8NuHAR1rJGsRSg\necDs+2dg2HbePaYMKgzEzxFIa9OGCydv28+8o5mVZ2r7gQel5/3irJJ0rlu24Y/q\nyAjTz0xLkzuQfLk5la+USKGSZs60NX8qo70IP9WLozgE2pEW3aMQwRDz5qhSNw9F\ng7tNvBKfYItEVuNmYzgRfC+IxWXokTN2IdjvosM8c/8FE8Xn29gAsfcoVzIjrTP3\nqW3WQZjG2XwALVZRSydBC1HJ26Q96vpjlu/m8E2u9mnfhrg5/51OUOqv9Ak2yIAD\nUyk=\n-----END ENCRYPTED PRIVATE KEY-----\n",
                  "3025581380b6d8345a7aa309548967c6",
                  "ihmb7hh8");
                var boxJWT = new BoxJWTAuth(boxConfig);
               // var adminToken = boxJWT.AdminToken(); //valid for 60 minutes so should be cached and re-used                

                var userToken = boxJWT.UserToken("6446395751");
                var client = boxJWT.UserClient(userToken, "6446395751");

                var items = client.FoldersManager.GetFolderItemsAsync("64118254178", 9999, 0).Result;
                model = items.Entries;
            }
            catch (Exception ex)
            {

            }

            return View(model);
        }

        public ActionResult About()
        {


            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}