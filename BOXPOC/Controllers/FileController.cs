﻿using BusinessLogicLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BO.Models;
using DataAccessLayer.PatientsData;
using DataAccessLayer.PatientsData.Schema;
using System.Configuration;

namespace BOXAPI.Controllers
{
    public class FileController : Controller
    {
        private readonly PatientFileBLL _bll;
        public FileController(PatientFileBLL bll)
        {
            _bll = bll;
        }

        [HttpPost]
        public ActionResult LargeFileUpload(List<HttpPostedFileBase> files, string folderId)
        {
            try
            {
                files.ForEach(file =>
            {
                var model = new BO.Models.FileModel();
                model.FolderId = (folderId.Contains("undefined")) ? string.Empty : folderId;
                model.FileName = file.FileName;
                model.FileStream = file.InputStream;

                var result = _bll.FileUpload(model);



            });
                return Json(new { success = true, data = string.Empty });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, data = ex.Message });
            }

        }
        [HttpPost]
        public ActionResult LargeFileUploadForConversation(List<HttpPostedFileBase> files, string folderId,int RecordId,int PatientId)
        {
            try
            {
                string Result = string.Empty;
                files.ForEach(file =>
                {
                    var model = new BO.Models.FileModel();
                    model.FolderId = (folderId.Contains("undefined")) ? string.Empty : folderId;
                    model.FileName = file.FileName;
                    model.FileStream = file.InputStream;
                    model.RecordId = RecordId;
                    model.PatientId = PatientId;
                    Result = _bll.FileUploadForConversation(model);
                });
                return Json(new { success = true, message = Result });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, data = ex.Message });
            }

        }
        [HttpGet]
        public ActionResult Files(int recordId, string folderId, int? patientId)
        {
            folderId = (folderId.Contains("undefined")) ? string.Empty : folderId;
            var model = new BO.Models.FileListModel();
            try
            {
                model = _bll.Files(recordId, folderId, (patientId == null) ? 0 : (int)patientId);
                return View(model);
            }
            catch (Exception ex)
            {
                return View(model);
            }

        }

        [HttpGet]
        public ActionResult FilesReferral(int recordId, string folderId, int? patientId)
        {
            folderId = (folderId.Contains("undefined")) ? string.Empty : folderId;
            var model = new BO.Models.FileListModel();
            try
            {
                model = _bll.Files(recordId, folderId, (patientId == null) ? 0 : (int)patientId);
                return View(model);
            }
            catch (Exception ex)
            {
                return View(model);
            }

        }

        [HttpGet]
        public ActionResult Download(string id)
        {
            var model = _bll.Download(id);
            return File(model.FileStream, "application/unknown", model.FileName);
        }

        [HttpPost]
        public ActionResult Delete(string id)
        {

            try
            {
                _bll.Delete(id);
                return Json(new { success = true, data = true });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, data = true });
            }

        }

        [HttpGet]
        public ActionResult GetFiles(int recordId, string folderId, int? patientId)
        {
            folderId = folderId.Contains("undefined") ? string.Empty : folderId;
            var model = new FileListModel();
            try
            {
                model = _bll.Files(recordId, folderId, (patientId == null) ? 0 : (int)patientId);
                return View(model);
            }
            catch (Exception ex)
            {
                return View(model);
            }

        }
    }
}