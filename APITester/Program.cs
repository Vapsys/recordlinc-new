﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace APITester
{
    class Program
    {
        static void Main(string[] args)
        {

            var client = new WebClient();

            //carrier payload
            //client.Headers[HttpRequestHeader.ContentType] = "application/json";
            //var carrierPayloadPath = Path.Combine(Directory.GetCurrentDirectory(), "\\CarrierPayload.txt");
            //var carrierPayload = File.ReadAllText(carrierPayloadPath);
            //client.UploadString("http://localhost:56697/api/PatientReward/AddInsuranceCarrier", carrierPayload);

            //record link upload
            client.Headers[HttpRequestHeader.ContentType] = "application/json";
            var recordLinkPath = Path.Combine(Directory.GetCurrentDirectory(), "\\InsuranceLinkPayload.txt");
            var recordLinkPayLoad = File.ReadAllText(recordLinkPath);
            client.UploadString("http://qa.recordlinc.com/RLAPI/api/PatientReward/AddInsuredRecord", recordLinkPayLoad);


        }
    }
}
