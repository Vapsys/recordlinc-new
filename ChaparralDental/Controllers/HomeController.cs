﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChaparralDental.Controllers
{

  public class HomeController : Controller
  {
    [Authorize]
    public ActionResult Index()
    {
     return RedirectToAction("Dashboard");
    
    }
    [Authorize]
    public ActionResult Dashboard()
    {
      return View();
    }

    public ActionResult GoalsAndBudget()
    {
      return View();
    }

    public ActionResult ManageGoals()
    {
      return View();
    }

    public ActionResult Goals()
    {
      return View();
    }

    public ActionResult PracticeProfile()
    {
      return View();
    }

    public ActionResult SmartNumbers()
    {
      return View();
    }

    public ActionResult CriticalNumbers()
    {
      return View();
    }



  }
}