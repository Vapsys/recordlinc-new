﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ChaparralDental.Startup))]
namespace ChaparralDental
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
