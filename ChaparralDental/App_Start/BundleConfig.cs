﻿using System.Web;
using System.Web.Optimization;

namespace ChaparralDental
{
  public class BundleConfig
  {
    // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
    public static void RegisterBundles(BundleCollection bundles)
    {
      bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                  "~/Scripts/jquery-{version}.js"));

      bundles.Add(new ScriptBundle("~/bundles/angular")
        .Include(
            "~/Scripts/angular/angular.min.js")
        .Include("~/Scripts/angular/angular-route.js"));


     // bundles.Add(new ScriptBundle("~/bundles/angular").IncludeDirectory("~/Scripts/angular", "*.js", true));


      bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                  "~/Scripts/jquery.validate*"));

      // Use the development version of Modernizr to develop with and learn from. Then, when you're
      // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
      bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                  "~/Scripts/modernizr-*"));

      bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Scripts/bootstrap.js"));

      bundles.Add(new ScriptBundle("~/bundles/chartjs").Include(
      "~/Scripts/Chart.bundle.min.js", "~/Scripts/animsition.js"));


      bundles.Add(new ScriptBundle("~/bundles/angular")
        .Include(
            "~/Scripts/angular/angular.min.js")
        .Include("~/Scripts/angular/angular-route.js"));


      bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/animsition.css",
                "~/Content/site.css"));
    }
  }
}
