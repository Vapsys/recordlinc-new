﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ChatServer.Models;
using ChatServer.Infrastructure.Service;
using ChatServer.Infrastructure.Model;
using Microsoft.AspNetCore.Authorization;

namespace ChatServer.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        private readonly ChatService _chatService;
        private readonly MemberService _memberService;
        public HomeController(ChatService chatService, MemberService memberService)
        {
            _chatService = chatService;
            _memberService = memberService;
        }
        public IActionResult Index(string user)
        {
            ViewData["ActiveUser"] = user;
            return View(ViewData["ActiveUser"]);
        }

        [HttpPost]
        public async Task<IActionResult> AddSession(ChatModel model, string user)
        {
            var result  = await _chatService.AddChat(model, user);
            return Json(result);
        }

        public async Task<IActionResult> Sessions(string user)
        {
            ViewData["ActiveUser"] = user;
            var model = await _chatService.ChatSessions(user);
            return View(model);
        }


        //public async Task<IActionResult> Messages(long chatId)
        //{
        //    var model = await _chatService.ChatMessages(chatId);
        //    return View(model);
        //}

        public async Task<IActionResult> Messages(long chatId)
        {
            var model = await _chatService.ChatMessages(chatId);
            return Json(model);
        }

        public async Task<IActionResult> MemberLookup(string q)
        {
            var result = await _memberService.SearchMember(q);
            return Json(result);
        }

        public async Task<IActionResult> UpdateRead(string user, long chatId, long messageId)
        {
            await _chatService.UpdateRead(user, chatId, messageId);
            return Json(true);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
