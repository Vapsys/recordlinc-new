﻿using System;
using System.Collections.Concurrent;
using System.IO;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using ChatServer.Infrastructure.Data;
using ChatServer.Infrastructure.Model;
using ChatServer.Infrastructure.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace ChatServer.Infrastructure
{
    public class WebSocketServer
    {
        private static ConcurrentDictionary<string, WebSocket> _sockets = new ConcurrentDictionary<string, WebSocket>();

        private readonly RequestDelegate _next;
        private readonly ChatContext _db;
        public WebSocketServer(RequestDelegate next, IConfiguration configuration)
        {
            _next = next;
            _db = new ChatContext(configuration.GetConnectionString("ChatConnection"));
        }

        public async Task Invoke(HttpContext context)
        {
            if (!context.WebSockets.IsWebSocketRequest)
            {
                await _next.Invoke(context);
                return;
            }

            CancellationToken ct = context.RequestAborted;
            WebSocket currentSocket = await context.WebSockets.AcceptWebSocketAsync();
            var socketId = Guid.NewGuid().ToString();

            _sockets.TryAdd(socketId, currentSocket);

            while (true)
            {
                if (ct.IsCancellationRequested)
                {
                    break;
                }

                var response = await ReceiveStringAsync(currentSocket, ct);
                if (string.IsNullOrEmpty(response))
                {
                    if (currentSocket.State != WebSocketState.Open)
                    {
                        break;
                    }

                    continue;
                }

                foreach (var socket in _sockets)
                {
                    if (socket.Value.State != WebSocketState.Open)
                    {
                        continue;
                    }   
                    await SendStringAsync(socket.Value, response, ct);      
                }

                //this condition blocks duplicate messages when testing with multi-sessoins, same browser and multi session, different browser, same pc.
                var message = JsonConvert.DeserializeObject<ChatMessageModel>(response);
                if (message.Sender != PreviousMessage.Sender && message.Message != PreviousMessage.Message)
                {
                    await AddChatMessage(message);
                    PreviousMessage.Sender = message.Sender;
                    PreviousMessage.Message = message.Message;
                }
            }

            WebSocket dummy;
            _sockets.TryRemove(socketId, out dummy);

            await currentSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closing", ct);
            currentSocket.Dispose();
        }

        public async Task AddChatMessage(ChatMessageModel model)
        {
            model.CreatedDate = DateTime.Now;
            var record = new ChatMessage();
            record.ChatId = model.ChatId;
            record.CreatedDate = model.CreatedDate;
            record.IsAttachment = model.IsAttachment;
            record.Message = model.Message;
            record.Sender = model.Sender;
            
            _db.ChatMessage.Add(record);
            await _db.SaveChangesAsync();

            //add attachment if it is an attachment
            if (model.IsAttachment)
            {
                var attachment = new ChatAttachmentModel
                {
                    ChatId = record.Id,
                    Data = model.ChatMessageAttachmentData
                };
                await AddChatAttachment(attachment);
            }
        }

        public async Task<long> AddChatAttachment(ChatAttachmentModel model)
        {
            var record = Mapper.Map<ChatAttachment>(model);
            _db.ChatAttachment.Add(record);
            await _db.SaveChangesAsync();
            return record.Id;
        }

        private static Task SendStringAsync(WebSocket socket, string data, CancellationToken ct = default(CancellationToken))
        {
            var buffer = Encoding.UTF8.GetBytes(data);
            var segment = new ArraySegment<byte>(buffer);
            return socket.SendAsync(segment, WebSocketMessageType.Text, true, ct);
        }

        private static async Task<string> ReceiveStringAsync(WebSocket socket, CancellationToken ct = default(CancellationToken))
        {
            var buffer = new ArraySegment<byte>(new byte[8192]);
            using (var ms = new MemoryStream())
            {
                WebSocketReceiveResult result;
                do
                {
                    ct.ThrowIfCancellationRequested();

                    result = await socket.ReceiveAsync(buffer, ct);
                    ms.Write(buffer.Array, buffer.Offset, result.Count);
                }
                while (!result.EndOfMessage);

                ms.Seek(0, SeekOrigin.Begin);
                if (result.MessageType != WebSocketMessageType.Text)
                {
                    return null;
                }

                // Encoding UTF8: https://tools.ietf.org/html/rfc6455#section-5.6
                using (var reader = new StreamReader(ms, Encoding.UTF8))
                {
                    return await reader.ReadToEndAsync();
                }
            }
        }
    }

    public static class PreviousMessage
    {
        public static string Sender { get; set; }
        public static string Message { get; set; }
    }
}
