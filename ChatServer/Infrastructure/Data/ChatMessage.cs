﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChatServer.Infrastructure.Data
{
    public class ChatMessage
    {
        [Key]
        public long Id { get; set; }
        public long ChatId { get; set; }
        public bool IsAttachment { get; set; }
        public string Sender { get; set; }
        public string Message { get; set; }
        [ForeignKey("ChatId")]
        public virtual ChatAttachment ChatAttachment { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
