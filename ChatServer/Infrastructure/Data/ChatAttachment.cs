﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ChatServer.Infrastructure.Data
{
    public class ChatAttachment
    {
        [Key]
        public long Id { get; set; }
        public long ChatId { get; set; }
        public byte[] Data { get; set; }
    }
}
