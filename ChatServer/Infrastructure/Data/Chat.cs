﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChatServer.Infrastructure.Data
{
    public class Chat
    {
        [Key]
        public long Id { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedDate { get; set; }
        //[ForeignKey("ChatId")]
        public virtual List<ChatParticipant> ChatParticipants {get;set;}
       
    }
}
