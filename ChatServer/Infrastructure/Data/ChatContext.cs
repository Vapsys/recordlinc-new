﻿using Microsoft.EntityFrameworkCore;

namespace ChatServer.Infrastructure.Data
{
    public class ChatContext: DbContext
    {
        public ChatContext(DbContextOptions<ChatContext> options)
            : base(options)
        {
            this.ChangeTracker.AutoDetectChangesEnabled = true;
            this.ChangeTracker.LazyLoadingEnabled = false;
        }

        public ChatContext(string connection)
           : base(GetOptions(connection))
        {
            this.ChangeTracker.AutoDetectChangesEnabled = true;
            this.ChangeTracker.LazyLoadingEnabled = false;
        }


        private static DbContextOptions GetOptions(string connectionString)
        {
            return SqlServerDbContextOptionsExtensions.UseSqlServer(new DbContextOptionsBuilder(), connectionString).Options;
        }

        public DbSet<Chat> Chat { get; set; }
        public DbSet<ChatMessage> ChatMessage { get; set; }
        public DbSet<ChatParticipant> ChatParticipant { get; set; }
        public DbSet<ChatAttachment> ChatAttachment { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
        }
    }
}
