﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChatServer.Infrastructure.Data
{
    public class Member
    {
        [Key]
        public int UserId { get; set; }
        public string LoginUserName { get; set; }
        public string Username { get; set; }
       // public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
