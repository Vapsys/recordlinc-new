﻿using Microsoft.EntityFrameworkCore;

namespace ChatServer.Infrastructure.Data
{
    public class MemberContext : DbContext
    {
        public MemberContext(DbContextOptions<MemberContext> options)
               : base(options)
        {
            this.ChangeTracker.AutoDetectChangesEnabled = true;
            this.ChangeTracker.LazyLoadingEnabled = false;
        }
        public DbSet<Member> Member { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
        }
    }
}
