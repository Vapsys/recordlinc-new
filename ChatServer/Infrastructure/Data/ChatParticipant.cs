﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ChatServer.Infrastructure.Data
{
    public class ChatParticipant
    {
        [Key]
        public long Id { get; set; }
        public long ChatId { get; set; }
        public long? LastRead { get; set; }
        public string Participant { get; set; }
        public DateTime CreatedDate { get; set; }
        [ForeignKey("ChatId")]
        public Chat Chat { get; set; }
    }
}
