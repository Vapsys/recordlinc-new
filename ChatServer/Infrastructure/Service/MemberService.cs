﻿using AutoMapper;
using ChatServer.Infrastructure.Data;
using ChatServer.Infrastructure.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatServer.Infrastructure.Service
{
    public class MemberService
    {
        private readonly MemberContext _db;
        private readonly IMapper _mapper;
        public MemberService(MemberContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task<List<MemberModel>> SearchMember(string text)
        {
            var result = new List<MemberModel>();
            var words = text.Split(" ");
            var keyWords = words.ToList();
            if (text.Length > 2)
            {
                var query = _db.Member.AsNoTracking();
                keyWords.ForEach(keyWord =>
                {
                    query = query.Where(q => q.LastName.Contains(keyWord) || q.FirstName.Contains(keyWord));
                });
                try
                {
                    var queryResult = await query.OrderBy(q => q.FirstName).Take(300).Select(q => _mapper.Map<MemberModel>(q)).ToListAsync();
                    result.AddRange(queryResult);
                }catch(Exception ex)
                {
                    return null;
                }
            }
            return result;
        }
    }
}
