﻿using AutoMapper;
using ChatServer.Infrastructure.Data;
using ChatServer.Infrastructure.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatServer.Infrastructure.Service
{
    public class ChatService
    {
        private readonly ChatContext _db;
        private readonly IMapper _mapper;
        public ChatService(ChatContext db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task<long> AddChat(ChatModel model, string currentUser)
        {
            try
            {
                model.CreatedBy = currentUser;
                model.CreatedDate = DateTime.Now;

                var record = _mapper.Map<Chat>(model);
                _db.Chat.Add(record);
                await _db.SaveChangesAsync();

                //add initial participants
                model.Participants.ForEach(participant =>
                {
                    AddChatParticipant(new ChatParticipantModel
                    {
                        ChatId = record.Id,
                        CreatedDate = DateTime.Now,
                        LastRead = 0,
                        Participant = participant
                    }).Wait();
                });

                AddChatParticipant(new ChatParticipantModel
                {
                    ChatId = record.Id,
                    CreatedDate = DateTime.Now,
                    LastRead = 0,
                    Participant = currentUser
                }).Wait();

                return record.Id;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public async Task AddChatMessage(ChatMessageModel model)
        {
            model.CreatedDate = DateTime.Now;
            var record = _mapper.Map<ChatMessage>(model);
            _db.ChatMessage.Add(record);
            await _db.SaveChangesAsync();

            //add attachment if it is an attachment
            if (model.IsAttachment)
            {
                var attachment = new ChatAttachmentModel
                {
                    ChatId = record.Id,
                    Data = model.ChatMessageAttachmentData
                };
                await AddChatAttachment(attachment);
            }
        }

        public async Task<long> AddChatAttachment(ChatAttachmentModel model)
        {
            var record = _mapper.Map<ChatAttachment>(model);
            _db.ChatAttachment.Add(record);
            await _db.SaveChangesAsync();
            return record.Id;
        }

        public async Task AddChatParticipant(ChatParticipantModel model)
        {
            var record = _mapper.Map<ChatParticipant>(model);
            _db.ChatParticipant.Add(record);
            await _db.SaveChangesAsync();
        }

        public async Task<List<ChatModel>> ChatSessions(string user)
        {
            try
            {
                var participatingChats = await _db.ChatParticipant
                                                  .Include(q => q.Chat)
                                                  .Include(q => q.Chat.ChatParticipants)
                                                  .AsNoTracking().Where(q => q.Participant == user).ToListAsync();
                return participatingChats.Select(q => _mapper.Map<ChatModel>(q.Chat)).ToList();
            }
            catch (Exception Ex)
            {
                throw;
            }
        }

        public async Task<List<ChatMessageModel>> ChatMessages(long chatId)
        {
            try
            {
                var results = await _db.ChatMessage.AsNoTracking().Where(q => q.ChatId == chatId).Select(q => _mapper.Map<ChatMessageModel>(q)).ToListAsync();
                return results;
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public async Task UpdateRead(string user, long chatId, long messageId)
        {
            var particpantRecord = await _db.ChatParticipant.Where(q => q.Participant == user && q.ChatId == chatId).FirstOrDefaultAsync();
            particpantRecord.LastRead = messageId;
            await _db.SaveChangesAsync();
        }

    }
}
