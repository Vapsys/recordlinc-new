﻿using AutoMapper;
using ChatServer.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatServer.Infrastructure.Model
{
    public class ChatAttachmentModel
    {
        public long Id { get; set; }
        public long ChatId { get; set; }
        public byte[] Data { get; set; }
    }

    public class ChatAttachmentModelProfile : Profile
    {
        public ChatAttachmentModelProfile()
        {
            CreateMap<ChatAttachmentModel, ChatAttachment>().ReverseMap();
        }
    }
}
