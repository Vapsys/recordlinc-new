﻿using AutoMapper;
using ChatServer.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatServer.Infrastructure.Model
{
    public class ChatModel
    {
        public long Id { get; set; }
        public List<string> Participants { get; set; }
        public string Description { get; set; }
        public string CreatedBy { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedDate { get; set; }
        public virtual List<ChatParticipantModel> ChatParticipants { get; set; }
    }
    public class ChatModelProfile : Profile
    {
        public ChatModelProfile()
        {
            CreateMap<ChatModel, Chat>().ReverseMap();
        }
    }

}
