﻿using AutoMapper;
using ChatServer.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatServer.Infrastructure.Model
{
    public class ChatParticipantModel
    {
        public long Id { get; set; }
        public long ChatId { get; set; }
        public long? LastRead { get; set; }
        public string Participant { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class ChatParticipantModelProfile : Profile
    {
        public ChatParticipantModelProfile()
        {
            CreateMap<ChatParticipantModel, ChatParticipant>().ReverseMap();
        }
    }
}
