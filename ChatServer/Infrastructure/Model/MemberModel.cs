﻿using AutoMapper;
using ChatServer.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ChatServer.Infrastructure.Model
{
    public class MemberModel
    {
        public int UserId { get; set; }
        public string LoginUserName { get; set; }
        public string Username { get; set; }
      //  public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
        public DateTime? CreatedDate { get; set; }
    }

    public class MemberModelProfile : Profile
    {
        public MemberModelProfile()
        {
            CreateMap<MemberModel, Member>().ReverseMap();
        }
    }
}
