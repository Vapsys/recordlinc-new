﻿using AutoMapper;
using ChatServer.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ChatServer.Infrastructure.Model
{
    public class ChatMessageModel
    {
        public long Id { get; set; }
        public long ChatId { get; set; }
        public bool IsAttachment { get; set; }
        public long? AttachmentId { get; set; }
        public string Sender { get; set; }
        public string Message { get; set; }
        public byte[] ChatMessageAttachmentData { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class ChatMessageModelProfile : Profile
    {
        public ChatMessageModelProfile()
        {
            CreateMap<ChatMessageModel, ChatMessage>().ReverseMap();
        }
    }
}
