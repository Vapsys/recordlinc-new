﻿using System.Web.Http.Controllers;
using System.Web.Mvc;
using System.Diagnostics;
using System.Web.Http.Filters;
using System.Net.Http;
using System.Web;
using System.Linq;
using System;
using System.Collections.Generic;
using BO.Models;
using BusinessLogicLayer;

namespace iLuvMyDentist.App_Start
{
    public class CheckPatient : System.Web.Http.Filters.ActionFilterAttribute
    {
        /// <summary>
        /// This Action Filter used for check Patient has register with SOlution one or Super Buck and Based on that we will check Patient on Database.
        /// 1 = Solution one Partner 
        /// 2 = SuperBucks Partner
        /// 0 = Not found or Either we will received same Id on both Super Bucks as well Solution one then we also received 0.
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (actionContext.ActionArguments.FirstOrDefault().Value is PatientSolOne)
            {
                PatientSolOne Obj = (PatientSolOne)actionContext.ActionArguments.FirstOrDefault().Value;
                int SourceTypes = PatientRewardBLL.CheckPatientHasExists(Obj.RewardsPlatformID);
                actionContext.Request.Properties.Add(new KeyValuePair<string, object>("sourceType", SourceTypes));
            }
            else if (actionContext.ActionArguments.FirstOrDefault().Value is PatientAssociation)
            {
                PatientAssociation Obj = (PatientAssociation)actionContext.ActionArguments.FirstOrDefault().Value;
                int SourceTypes = PatientRewardBLL.CheckAccountHasLinked(Obj.AccountId);
                actionContext.Request.Properties.Add(new KeyValuePair<string, object>("sourceType", SourceTypes));
            }
            else if (actionContext.ActionArguments.FirstOrDefault().Value is PatientFilter)
            {
                PatientFilter Obj = (PatientFilter)actionContext.ActionArguments.FirstOrDefault().Value;
                int SourceTypes = PatientRewardBLL.CheckAccountHasLinked(0,Obj.UserId);
                actionContext.Request.Properties.Add(new KeyValuePair<string, object>("sourceType", SourceTypes));
            }
            else
            {
                //Assign Action Argument data to an Dynamic Object.
                dynamic d = actionContext.ActionArguments.FirstOrDefault().Value;
                int P = 0;
                P = d.RewardPartnerId;
                //This condition used for If dynamic data doesn't exists RewardPartnerId then Find on other RewardPlatFormId on Dynamic data.
                if (P > 0)
                {
                    //Call the database for check patient is Exists on which System Solution on or Super Bucks.
                    int SourceTypes = PatientRewardBLL.CheckPatientHasExists(P);
                    actionContext.Request.Properties.Add(new KeyValuePair<string, object>("sourceType", SourceTypes));
                }
                else
                {
                    P = d.RewardsPlatformID;
                    //Call the database for check patient is Exists on which System Solution on or Super Bucks.
                    int SourceTypes = PatientRewardBLL.CheckPatientHasExists(P);
                    actionContext.Request.Properties.Add(new KeyValuePair<string, object>("sourceType", SourceTypes));
                }
            }
            
            //int SourceType = PatientRewardBLL.CheckPatientHasExists(d.RewardPartnerId);
            //if (actionContext.ActionArguments.FirstOrDefault().Value is CreateRewards)
            //{
            //    CreateRewards RewardPartnerId = (CreateRewards)actionContext.ActionArguments.FirstOrDefault().Value;
            //    int SourceTypes = PatientRewardBLL.CheckPatientHasExists(RewardPartnerId.RewardPartnerId);
            //    actionContext.Request.Properties.Add(new KeyValuePair<string, object>("sourceType", SourceType));
            //}
            //else if(actionContext.ActionArguments.FirstOrDefault().Value is Reward_PatientUpdate)
            //{
            //    Reward_PatientUpdate Obj = (Reward_PatientUpdate)actionContext.ActionArguments.FirstOrDefault().Value;
            //    int SourceTypes = PatientRewardBLL.CheckPatientHasExists(Obj.RewardPartnerId);
            //    actionContext.Request.Properties.Add(new KeyValuePair<string, object>("sourceType", SourceType));
            //}
            //else if (actionContext.ActionArguments.FirstOrDefault().Value is Reward_UploadProfileImage)
            //{
            //    Reward_UploadProfileImage Obj = (Reward_UploadProfileImage)actionContext.ActionArguments.FirstOrDefault().Value;
            //    int SourceTypes = PatientRewardBLL.CheckPatientHasExists(Obj.RewardPartnerId);
            //    actionContext.Request.Properties.Add(new KeyValuePair<string, object>("sourceType", SourceType));
            //}
            //else if (actionContext.ActionArguments.FirstOrDefault().Value is Reward_RewardsCall)
            //{
            //    Reward_RewardsCall Obj = (Reward_RewardsCall)actionContext.ActionArguments.FirstOrDefault().Value;
            //    int SourceTypes = PatientRewardBLL.CheckPatientHasExists(Obj.RewardPartnerId);
            //    actionContext.Request.Properties.Add(new KeyValuePair<string, object>("sourceType", SourceType));
            //}
            //else if (actionContext.ActionArguments.FirstOrDefault().Value is RewardBalanceJson)
            //{
            //    RewardBalanceJson Obj = (RewardBalanceJson)actionContext.ActionArguments.FirstOrDefault().Value;
            //    int SourceTypes = PatientRewardBLL.CheckPatientHasExists(Obj.RewardPartnerId);
            //    actionContext.Request.Properties.Add(new KeyValuePair<string, object>("sourceType", SourceType));
            //}
            //else if (actionContext.ActionArguments.FirstOrDefault().Value is PatientReferral)
            //{
            //    PatientReferral Obj = (PatientReferral)actionContext.ActionArguments.FirstOrDefault().Value;
            //    int SourceTypes = PatientRewardBLL.CheckPatientHasExists(Obj.RewardPartnerId);
            //    actionContext.Request.Properties.Add(new KeyValuePair<string, object>("sourceType", SourceType));
            //}
            //else if (actionContext.ActionArguments.FirstOrDefault().Value is PatientAssociation)
            //{
            //    PatientAssociation Obj = (PatientAssociation)actionContext.ActionArguments.FirstOrDefault().Value;
            //    int SourceTypes = PatientRewardBLL.CheckPatientHasExists(Obj.RewardPartnerId);
            //    actionContext.Request.Properties.Add(new KeyValuePair<string, object>("sourceType", SourceType));
            //}
            //else if (actionContext.ActionArguments.FirstOrDefault().Value is PatientSolOne)
            //{
            //    PatientSolOne Obj = (PatientSolOne)actionContext.ActionArguments.FirstOrDefault().Value;
            //    int SourceTypes = PatientRewardBLL.CheckPatientHasExists(Obj.RewardsPlatformID);
            //    actionContext.Request.Properties.Add(new KeyValuePair<string, object>("sourceType", SourceType));
            //}
        }
    }
}