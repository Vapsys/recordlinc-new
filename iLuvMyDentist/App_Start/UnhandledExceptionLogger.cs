﻿using BO.ViewModel;
using System.Net;
using System.Web.Http.ExceptionHandling;
using System.Web.Http;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using System.Web.Script.Serialization;

namespace iLuvMyDentist.App_Start
{
    public class UnhandledExceptionLogger : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            var log = "Message:" + context.Exception.Message + " ,StackTrace:" + context.Exception.StackTrace;
            var FunctionName = context.Request.RequestUri.Segments[1];
            var requestURI = context.Request.RequestUri.Authority;
            string strTokenNumber = APIUtil.GetTokenNumber(context.Request.Headers);


            //int ErrorLogId = new ErrorBLL(
            //    System
            //    .Configuration
            //    .ConfigurationManager
            //    .ConnectionStrings["DevPassword"]
            //    .ConnectionString
            //).LogError(log, FunctionName, requestURI, strTokenNumber);

            APIResponseBase<string> objcon = new APIResponseBase<string>();
            objcon.Message = "Some internal server error occured.Sorry for inconvinience.It is logged for further investication.Referance number for the same is : ";// + ErrorLogId;
            objcon.StatusCode =(int) HttpStatusCode.InternalServerError;

            context.Result = new TextPlainErrorResult
            {
                Request = context.ExceptionContext.Request,
                Content = objcon,
            };
        }

        private class TextPlainErrorResult : IHttpActionResult
        {
            public HttpRequestMessage Request { get; set; }

            public APIResponseBase<string> Content { get; set; }

            public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
            {
                HttpResponseMessage response =
                                 new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(new JavaScriptSerializer().Serialize(Content), System.Text.Encoding.UTF8, "application/json");
                response.RequestMessage = Request;
                return Task.FromResult(response);
            }
        }
    }
}