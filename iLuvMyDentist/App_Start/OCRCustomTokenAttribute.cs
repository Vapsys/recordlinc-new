﻿using iLuvMyDentist.App_Start;
using System;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using DataAccessLayer.Common;
using System.Collections.Generic;
using BO.ViewModel;

namespace iLuvMyDentist.Filters
{
    #region Old filter code no need of this (need to be removed after required testing)
    /*  /// <summary>
      /// Generic Basic Authentication filter that checks for basic authentication
      /// headers and challenges for authentication if no authentication is provided
      /// Sets the Thread Principle with a GenericAuthenticationPrincipal.
      /// 
      /// You can override the OnAuthorize method for custom auth logic that
      /// might be application specific.    
      /// </summary>
      /// <remarks>Always remember that Basic Authentication passes username and passwords
      /// from client to server in plain text, so make sure SSL is used with basic auth
      /// to encode the Authorization header on all requests (not just the login).
      /// </remarks>
      [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
      public class OCRCustomTokenAttribute : AuthorizationFilterAttribute
      {
          bool Active;

          /// <summary>
          /// Default constructor to allow this filter by default.
          /// </summary>
          public OCRCustomTokenAttribute()
          {
              Active = true;
          } 
          /// <summary>
          /// Overriden constructor to allow explicit disabling of this
          /// filter's behavior. Pass false to disable (same as no filter
          /// but declarative)
          /// </summary>
          /// <param name="active"></param>
          public OCRCustomTokenAttribute(bool active)
          {
              Active = active;
          }

          /// <summary>
          /// Override to Web API filter method to handle Basic Auth check
          /// </summary>
          /// <param name="actionContext"></param>
          public override void OnAuthorization(HttpActionContext actionContext)
          {
              if (Active)
              {
                  try
                  {
                      string strTokenNumber = string.Empty;
                      if (((KeyValuePair<string, string>[])(actionContext.Request.GetQueryNameValuePairs())).Length > 0)
                      {
                          if (((KeyValuePair<string, string>[])(actionContext.Request.GetQueryNameValuePairs()))[0].Key == "access_token")
                          {
                              strTokenNumber = ((System.Collections.Generic.KeyValuePair<string, string>[])(actionContext.Request.GetQueryNameValuePairs()))[0].Value;
                          }
                      }                    
                      if (string.IsNullOrEmpty(strTokenNumber))
                      {
                          Challenge(actionContext, AuthernticationFailureReason.TokenNumberDoesNotPresentInHeader);
                          return;
                      }
                      if (!OnAuthorizeUser(strTokenNumber, actionContext))
                      {
                          Challenge(actionContext, AuthernticationFailureReason.InValidTokenNumber);
                          return;
                      }
                      base.OnAuthorization(actionContext);
                  }
                  catch(KeyNotFoundException)
                  {
                      Challenge(actionContext, AuthernticationFailureReason.TokenNumberDoesNotPresentInHeader);
                      return;
                  }

              }
          }

          /// <summary>
          /// Base implementation for user authentication - you probably will
          /// want to override this method for application specific logic.
          /// 
          /// Override this method if you want to customize Authentication
          /// Request specific storage.
          /// </summary>
          /// <param name="TokenNumber"></param>        
          /// <param name="actionContext"></param>
          /// <returns>Type of <see cref="bool"/></returns>
          /// <CreatedBy>Hardipsinh Jadeja</CreatedBy>
          /// <ChangedDate>2018-11-23</ChangedDate>
          /// <Reason>Added TokenSource parameter to make sure when token is retrieved</Reason>
          protected virtual bool OnAuthorizeUser(string TokenNumber, HttpActionContext actionContext)
          {
              DataTable dt = clsCommon.GetDentistDetailsByAccessToken(TokenNumber);
              if (dt != null && dt.Rows.Count > 0)
              {
                  APIUtil.SetCurrentUser(dt);
                  return true;
              }
              else
              {
                  return false;
              }
          }

          /// <summary>
          /// Send the Authentication Challenge request
          /// </summary>
          /// <param name="message"></param>
          /// <param name="actionContext"></param>
          void Challenge(HttpActionContext actionContext, AuthernticationFailureReason FailureReason)
          {
              var host = actionContext.Request.RequestUri.DnsSafeHost;
              if (FailureReason == AuthernticationFailureReason.TokenNumberDoesNotPresentInHeader)
                  actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<string>() { Message = "access_token does not present in request." });
              else
                  actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, new APIResponseBase<string>() { Message = "Invalid token number." });

              //actionContext.Response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", host));
          }

      }
      */
    #endregion
    /// <summary>
    /// Generic Basic Authentication filter that checks for basic authentication
    /// headers and challenges for authentication if no authentication is provided
    /// Sets the Thread Principle with a GenericAuthenticationPrincipal.
    /// 
    /// You can override the OnAuthorize method for custom auth logic that
    /// might be application specific.    
    /// </summary>
    /// <remarks>Always remember that Basic Authentication passes username and passwords
    /// from client to server in plain text, so make sure SSL is used with basic auth
    /// to encode the Authorization header on all requests (not just the login).
    /// </remarks>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class OCRCustomTokenAttribute : AuthorizationFilterAttribute
    {
        bool Active = true;

        /// <summary>
        /// Default constructor
        /// </summary>
        public OCRCustomTokenAttribute()
        { }

        /// <summary>
        /// Overriden constructor to allow explicit disabling of this
        /// filter's behavior. Pass false to disable (same as no filter
        /// but declarative)
        /// </summary>
        /// <param name="active"></param>
        public OCRCustomTokenAttribute(bool active)
        {
            Active = active;
        }

        /// <summary>
        /// Override to Web API filter method to handle Basic Auth check
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (Active)
            {
                string strTokenNumber = APIUtil.GetTokenNumber(actionContext.Request.Headers);
                if (string.IsNullOrEmpty(strTokenNumber))
                {
                    Challenge(actionContext, AuthernticationFailureReason.TokenNumberDoesNotPresentInHeader);
                    return;
                }
                // Following condition is for Mobile App as for Mobile App token number should be at least 10 digits
                if (strTokenNumber.Length < 10)
                {
                    Challenge(actionContext, AuthernticationFailureReason.InValidTokenLength);
                    return;
                }
                if (!OnAuthorizeUser(strTokenNumber))
                {
                    Challenge(actionContext, AuthernticationFailureReason.InValidTokenNumber);
                    return;
                }
                base.OnAuthorization(actionContext);
            }
        }

        /// <summary>
        /// Base implementation for user authentication - you probably will
        /// want to override this method for application specific logic.
        /// 
        /// Override this method if you want to customize Authentication
        /// Request specific storage.
        /// </summary>
        /// <param name="TokenNumber">Token to be checked for.</param>       
        /// <returns></returns>
        /// <ChangedBy>Ankit Solanki</ChangedBy>
        /// <ChangedDate>2018-07-25</ChangedDate>
        /// <ChangedReason>Added TokenSource parameter to make sure when token is retrieved</ChangedReason>
        protected virtual bool OnAuthorizeUser(string TokenNumber)
        {
            DataTable dt = clsCommon.GetDentistDetailsByAccessToken(TokenNumber);
            if (dt != null && dt.Rows.Count > 0)
            {
                APIUtil.SetCurrentUser(dt);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Send the Authentication Challenge request
        /// </summary>
        /// <param name="message"></param>
        /// <param name="actionContext"></param>
        void Challenge(HttpActionContext actionContext, AuthernticationFailureReason FailureReason)
        {
            var host = actionContext.Request.RequestUri.DnsSafeHost;
            if (FailureReason == AuthernticationFailureReason.TokenNumberDoesNotPresentInHeader)
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, new APIResponseBase<string>() { Message = "Token_number does not present in header." });
            else if (FailureReason == AuthernticationFailureReason.InValidTokenLength)
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, new APIResponseBase<string>() { Message = "Token_number should be longer than 10 characters." });
            else if (FailureReason == AuthernticationFailureReason.InValidTokenSource)
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, new APIResponseBase<string>() { Message = "Token_source is invalid." });
            else
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, new APIResponseBase<string>() { Message = "Invalid token number." });

            //actionContext.Response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", host));
        }
    }
}