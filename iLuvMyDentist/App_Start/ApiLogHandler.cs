﻿using iLuvMyDentist.App_Start;
using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace iLuvMyDentist.App_Start
{
    public class ApiLogHandler : DelegatingHandler
    {
        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var apiLogEntry = APIUtil.CreateApiLogEntryWithRequestData(request);

            if (request.Content != null)
            {
                await request.Content.ReadAsStringAsync()
                    .ContinueWith(task =>
                    {
                        apiLogEntry.RequestContentBody = task.Result;
                    }, cancellationToken);
            }

            return await base.SendAsync(request, cancellationToken)
                .ContinueWith(task =>
                {
                   
                    var response = task.Result;

                    // Update the API log entry with response info
                    apiLogEntry.ResponseStatusCode = (int)response.StatusCode;
                    apiLogEntry.ResponseTimestamp = DateTime.UtcNow;

                    if (response.Content != null)
                    {
                        apiLogEntry.ResponseContentType = response.Content.Headers.ContentType.MediaType;
                        apiLogEntry.ResponseContentBody = response.Content.ReadAsStringAsync().Result;
                        apiLogEntry.ResponseHeaders = APIUtil.SerializeHeaders(response.Content.Headers);
                    }

                    APIUtil.FillAndSaveAPILogData(apiLogEntry, request);

                    return response;                   
                    

                }, cancellationToken);
        }
    }
}