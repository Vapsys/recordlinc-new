﻿using iLuvMyDentist.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Http.Routing;
using iLuvMyDentist.Models;
using BO.Models;
using BusinessLogicLayer;
using System.Web.Mvc;

namespace iLuvMyDentist.App_Start
{
    public class APIUtil
    {
        public static T GetRouteVariable<T>(IHttpRouteData routeData, IEnumerable<string> names)
        {
            object result;

            foreach (var name in names)
            {
                if (routeData.Values.TryGetValue(name, out result))
                {
                    return (T)result;
                }
            }
            return default(T);
        }



        public static string GetVersion(IHttpRouteData routeData)
        {
            if (routeData.GetSubRoutes() == null)
                return null;
            var subRouteData = routeData.GetSubRoutes().FirstOrDefault();
            if (subRouteData == null) return null;
            var routevariable = GetRouteVariable<string>(subRouteData, Constants.versionnumber);
            return routevariable;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="headers"></param>
        /// <param name="Key"></param>
        /// <returns></returns>
        /// <ChangedBy>Ankit Solanki</ChangedBy>
        /// <ChangedDate>2018-07-25</ChangedDate>
        /// <ChangedReason>Added trim for strKeyValue </ChangedReason>
        public static string FindKeyFromHeader(HttpHeaders headers, string Key)
        {
            string strKeyValue = string.Empty;

            foreach (var item in headers.ToList())
            {
                if (item.Key.ToLower() == Key.ToLower())
                {
                    foreach (var value in item.Value)
                    {
                        strKeyValue += value + " ";
                    }
                    break;
                }
            }

            return strKeyValue.Trim();
        }

        public static string GetTokenNumber(HttpHeaders headers)
        {
            return FindKeyFromHeader(headers, Constants.Token_Number);
        }

        public static string GetTokenSource(HttpHeaders headers)
        {
            return FindKeyFromHeader(headers, Constants.Token_Source);
        }

        public static CustomAuthenticationIdentity GetCurrentUser()
        {
            return (CustomAuthenticationIdentity)Thread.CurrentPrincipal.Identity;
        }

        public static void SetCurrentUser(DataTable dsUser)
        {
            CustomAuthenticationIdentity CurrentUser = new CustomAuthenticationIdentity();

            CurrentUser.UserId = Convert.ToInt32(dsUser.Rows[0]["UserId"]);
            CurrentUser.AccountId = Convert.ToInt32(dsUser.Rows[0]["AccountId"]);
            CurrentUser.TimeZoneOfDentist = Convert.ToString(dsUser.Rows[0]["TimeZoneValue"]);
            Thread.CurrentPrincipal = new GenericPrincipal(CurrentUser, null);
        }

        public static ApiLogEntry CreateApiLogEntryWithRequestData(HttpRequestMessage request)
        {
            var context = ((HttpContextBase)request.Properties["MS_HttpContext"]);
            var routeData = request.GetRouteData();

            ApiLogEntry objApiLogEntry = new ApiLogEntry();

            objApiLogEntry.Application = "[insert-calling-app-here]";
            objApiLogEntry.User = context.User.Identity.Name;
            objApiLogEntry.Machine = Environment.MachineName;
            objApiLogEntry.RequestIpAddress = context.Request.UserHostAddress;
            objApiLogEntry.RequestContentType = context.Request.ContentType;
            objApiLogEntry.RequestContentBody = request.Content.ReadAsStringAsync().Result;
            objApiLogEntry.RequestUri = request.RequestUri.ToString();
            objApiLogEntry.RequestMethod = request.Method.Method;
            objApiLogEntry.RequestRouteTemplate = routeData.Route.RouteTemplate;
            objApiLogEntry.RequestRouteData = SerializeRouteData(routeData);
            objApiLogEntry.RequestHeaders = SerializeHeaders(request.Headers);
            objApiLogEntry.TokenNumber = APIUtil.FindKeyFromHeader(request.Headers, Constants.Token_Number);
            objApiLogEntry.RequestTimestamp = DateTime.UtcNow;
            objApiLogEntry.APIVersion = string.IsNullOrEmpty(APIUtil.GetVersion(routeData)) ? 0 : Convert.ToInt32(APIUtil.GetVersion(routeData).ToLower().Replace("v", string.Empty));
            objApiLogEntry.InOut = 1;

            return objApiLogEntry;
        }


        public static string SerializeRouteData(IHttpRouteData routeData)
        {
            IDictionary<string, object> objIDictionary = null;

            if (!routeData.Values.ContainsKey("MS_SubRoutes"))
                objIDictionary = routeData.Values.ToDictionary(x => x.Key, y => y.Value);
            else
                objIDictionary = ((IHttpRouteData[])routeData.Values["MS_SubRoutes"]).SelectMany(x => x.Values).ToDictionary(x => x.Key, y => y.Value);


            if (objIDictionary != null)
                return JsonConvert.SerializeObject(objIDictionary, Formatting.Indented);
            else
                return string.Empty;
        }

        public static string SerializeHeaders(HttpHeaders headers)
        {
            var dict = new Dictionary<string, string>();

            foreach (var item in headers.ToList())
            {
                if (item.Value != null)
                {
                    var header = String.Empty;
                    foreach (var value in item.Value)
                    {
                        header += value + " ";
                    }

                    // Trim the trailing space and add item to the dictionary
                    header = header.TrimEnd(" ".ToCharArray());
                    dict.Add(item.Key, header);
                }
            }

            return JsonConvert.SerializeObject(dict, Formatting.Indented);
        }

        public static void FillAndSaveAPILogData(ApiLogEntry objApiLogEntry, HttpRequestMessage request)
        {

            if (objApiLogEntry.ResponseStatusCode == (int)HttpStatusCode.Created || objApiLogEntry.ResponseStatusCode == (int)HttpStatusCode.OK)
                objApiLogEntry.Status = "200";
            else
                objApiLogEntry.Status = "400";
          

             APILogBLL.InsertAPILogDetials(objApiLogEntry);
        }

        public static void parseFullName(string strFullName, out string strFirstName, out string strLastName)
        {
            strLastName = string.Empty;
            strFirstName = strFullName;
            if (!string.IsNullOrWhiteSpace(strFullName) && strFullName.IndexOf(" ") > 0)
            {
                strFirstName = strFullName.Substring(0, (strFullName.IndexOf(" "))).Trim();
                strLastName = strFullName.Substring(strFullName.IndexOf(" ")).Trim();
            }
        }

        public static List<T> ConvertTo<T>(DataTable datatable) where T : new()
        {
            List<T> Temp = new List<T>();
            try
            {
                List<string> columnsNames = new List<string>();
                foreach (DataColumn DataColumn in datatable.Columns)
                    columnsNames.Add(DataColumn.ColumnName);
                Temp = datatable.AsEnumerable().ToList().ConvertAll<T>(row => getObject<T>(row, columnsNames));
                return Temp;
            }
            catch
            {
                return Temp;
            }

        }
        public static T getObject<T>(DataRow row, List<string> columnsName) where T : new()
        {
            T obj = new T();
            try
            {
                string columnname = "";
                string value = "";
                PropertyInfo[] Properties;
                Properties = typeof(T).GetProperties();
                foreach (PropertyInfo objProperty in Properties)
                {
                    columnname = columnsName.Find(name => name.ToLower() == objProperty.Name.ToLower());
                    if (!string.IsNullOrEmpty(columnname))
                    {
                        value = row[columnname].ToString();
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (Nullable.GetUnderlyingType(objProperty.PropertyType) != null)
                            {
                                value = row[columnname].ToString().Replace("$", "").Replace(",", "");
                                objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(Nullable.GetUnderlyingType(objProperty.PropertyType).ToString())), null);
                            }
                            else
                            {
                                value = row[columnname].ToString();
                                objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(objProperty.PropertyType.ToString())), null);
                            }
                        }
                    }
                }
                return obj;
            }
            catch
            {
                return obj;
            }
        }

        #region KeyClass
        /// <summary>
        /// Model key message
        /// </summary>
        public class KeyMessage
        {
            /// <summary>
            /// Model key
            /// </summary>
            public string Key { get; set; }
            /// <summary>
            /// Message
            /// </summary>
            public string Message { get; set; }
        }
        #endregion

        #region KeyErrorMessage
        /// <summary>
        /// Get model state errors
        /// </summary>
        /// <param name="Ex"></param>
        /// <returns></returns>
        public static List<KeyMessage> GetModelstateErrors(System.Web.Http.ModelBinding.ModelStateDictionary Ex)
        {
            List<KeyMessage> ErrorList = new List<KeyMessage>();
            foreach (var Key in Ex.Keys)
            {
                string newKey = Key;
                if (Ex[Key] != null)
                {
                    foreach (var error in Ex[Key].Errors)
                    {
                        try
                        {
                            if(newKey.Contains("."))
                            {
                                newKey = newKey.Split('.').Last();
                            }
                            else
                            {
                                newKey = "VALIDATION_ERROR";
                            }
                        }
                        catch (Exception ex)
                        {
                        }

                        ErrorList.Add(new KeyMessage()
                        {
                            Key = newKey,
                            Message = error.ErrorMessage + ((error.Exception != null) ? error.Exception.Message : string.Empty)
                        });
                    }
                }
            }
            return ErrorList;
        }
        #endregion
    }
}