﻿using System.Web.Http.Controllers;
using System.Net.Http;
using System.Linq;
using iLuvMyDentist.Filters;
using System.Data;
using DataAccessLayer.Common;
using System.Net;
using BO.ViewModel;

namespace iLuvMyDentist.App_Start
{
    public class OCRCustomToken1Attribute : System.Web.Http.Filters.ActionFilterAttribute
    {
        /// <summary>
        /// This Action Filter used for check Patient has register with SOlution one or Super Buck and Based on that we will check Patient on Database.
        /// 1 = Solution one Partner 
        /// 2 = SuperBucks Partner
        /// 0 = Not found or Either we will received same Id on both Super Bucks as well Solution one then we also received 0.
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            //Assign Action Argument data to an Dynamic Object.
            dynamic d = actionContext.ActionArguments.FirstOrDefault().Value;
            string access_token = string.Empty;
            if(d is string)
            {
                access_token = d;
            }
            else
            {
                access_token = d.access_token;
            }

            if (string.IsNullOrEmpty(access_token))
            {
                Challenge(actionContext, AuthernticationFailureReason.TokenNumberDoesNotPresentInHeader);
                return;
            }
            if (!OnAuthorizeUser(access_token, actionContext))
            {
                Challenge(actionContext, AuthernticationFailureReason.InValidTokenNumber);
                return;
            }
        }

        /// Base implementation for user authentication - you probably will
        /// want to override this method for application specific logic.
        /// 
        /// Override this method if you want to customize Authentication
        /// Request specific storage.
        /// </summary>
        /// <param name="TokenNumber"></param>        
        /// <param name="actionContext"></param>
        /// <returns>Type of <see cref="bool"/></returns>
        /// <CreatedBy>Hardipsinh Jadeja</CreatedBy>
        /// <ChangedDate>2018-11-23</ChangedDate>
        /// <Reason>Added TokenSource parameter to make sure when token is retrieved</Reason>
        protected virtual bool OnAuthorizeUser(string TokenNumber, HttpActionContext actionContext)
        {
            DataTable dt = clsCommon.GetDentistDetailsByAccessToken(TokenNumber);
            if (dt != null && dt.Rows.Count > 0)
            {
                APIUtil.SetCurrentUser(dt);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Send the Authentication Challenge request
        /// </summary>
        /// <param name="message"></param>
        /// <param name="actionContext"></param>
        void Challenge(HttpActionContext actionContext, AuthernticationFailureReason FailureReason)
        {
            var host = actionContext.Request.RequestUri.DnsSafeHost;
            if (FailureReason == AuthernticationFailureReason.TokenNumberDoesNotPresentInHeader)
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<string>() { Message = "access_token does not present in request." });
            else
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, new APIResponseBase<string>() { Message = "Invalid token number." });

            //actionContext.Response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", host));
        }
    }
}
