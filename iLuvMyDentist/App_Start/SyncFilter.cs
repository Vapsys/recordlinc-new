﻿using BO.Models;
using BO.ViewModel;
using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Configuration;

namespace iLuvMyDentist.App_Start
{
    /// <summary>
    /// This Action Filter used for Enable/Disable API endpoints which are used for Sync.
    /// </summary>
    public class SyncFilter : ActionFilterAttribute
    {
        /// <summary>
        /// This method is called from API endpoints while we are receiving the requests on it.
        /// </summary>
        /// <param name="actionContext"></param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            string IsSyncEnable = ConfigurationManager.AppSettings["IsSyncEnable"];
            if (IsSyncEnable == "false")
            {
                var output = new APIResponseBase<RewardResponse>() { StatusCode = (int)HttpStatusCode.Forbidden, IsSuccess = false, Message = "Currently this API endpoint is disabled!", Result = null };
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Forbidden, output, actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
            }
            base.OnActionExecuting(actionContext);
        }
    }
}