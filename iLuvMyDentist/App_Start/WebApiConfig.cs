﻿using iLuvMyDentist.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.ExceptionHandling;
using iLuvMyDentist.App_Start;

namespace iLuvMyDentist
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            // Web API configuration and services
            //config.Services.Replace(typeof(IExceptionHandler), new UnhandledExceptionLogger());
            // Web API routes
            config.MapHttpAttributeRoutes();
            
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

           // var jsonformatter = new JsonMediaTypeFormatter
           // {
           //     SerializerSettings =
           //     {
           //         NullValueHandling = NullValueHandling.Ignore
           //     }
           // };
           //
           // config.Formatters.RemoveAt(0);
           // config.Formatters.Insert(0, jsonformatter);

           // config.Services.Replace(typeof(IExceptionLogger), new GlobalExceptionLogger());
           // config.Services.Replace(typeof(IExceptionHandler), new GlobalExceptionHandler());

            //config.Filters.Add(new CustomAPIKeyValidationFilter());
            //config.Filters.Add(new CustomTokenValidationFilter());
            //config.Filters.Add(new ValidationActionFilter());

        }
    }
}
