﻿using System.Web;
using System.Web.Mvc;

namespace iLuvMyDentist
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
