﻿using iLuvMyDentist.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace iLuvMyDentist
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            GlobalConfiguration.Configuration.MessageHandlers.Add(new ApiLogHandler());
        }
        protected void Application_PostAuthorizeRequest()
        {
            System.Web.HttpContext.Current.SetSessionStateBehavior(System.Web.SessionState.SessionStateBehavior.Required);
        }

        void Application_Error(object sender, EventArgs e)
        {
            Exception exc = Server.GetLastError().GetBaseException();
            var strRequestUrl = Request.Url.ToString();
            if (strRequestUrl.IndexOf("/images/") < 0
                    && strRequestUrl.IndexOf("/DentistImages/") < 0
                    && strRequestUrl.IndexOf("/ImageBank/") < 0
                    && strRequestUrl.IndexOf("favicon.ico") < 0)
            {
                bool status = new DataAccessLayer.Common.clsCommon().InsertErrorLog(Request.Url.ToString(), "RLAPI: " + exc.Message, exc.StackTrace);
            }
            //Server.ClearError();
        }
    }
}
