﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace iLuvMyDentist.Models
{
    public static class Constants
    {
        //public static DateTime MINIMUMDATEIME = new DateTime(1900, 1, 1);

        //public static char MailDisplayName = '|';
        //public static string GEOIPApiKey = Convert.ToString(ConfigurationManager.AppSettings["GEOIPApiKey"]);

        public static string Token_Number = "access_token";
        public static string Token_Source = "token_source";
        public static string API_Key = "api_key";
        public static string API_Id = "app_id";
        public static string TradeX = "tradex";
        public static string Guest = "guest";
        public static string Admin = "admin";
        public static readonly string MainMenuLogoPath;
        public static readonly string SubMenuLogoPath;

        public static List<string> versionnumber = new List<string>()
        {
            "apiVersion1" ,
            "apiVersion2",
            "apiVersion3"
        };

        #region Applicationvariables

        //public static string CultureInfo = "CultureInfo";
        //public static string TimeZone = "TimeZone";

        #endregion

        //public static string EmailValidationRegex = @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$";
        //public static string ZipcodeValidationRegex = "^[0-9]{5}(?:-[0-9]{4})?$";
        //public static int BestRating = 5;
        //public static int WorstRating = 1;
        //public static string[] strInvalidURLCharacters = new string[] { "<", ">", "*", "%", "&", ":", "\\", "?", "'", "/", "#", "|", "(", ")", ".", "!", ",", "~", "$" };

        //static Constants()
        //{
        //    MainMenuLogoPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["MainMenuLogoPath"]);
        //    if (MainMenuLogoPath.EndsWith("/"))
        //        MainMenuLogoPath = MainMenuLogoPath.Substring(0, MainMenuLogoPath.Length - 1);

        //    SubMenuLogoPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SubMenuLogoPath"]);
        //    if (SubMenuLogoPath.EndsWith("/"))
        //        SubMenuLogoPath = SubMenuLogoPath.Substring(0, SubMenuLogoPath.Length - 1);
        //}
    }
}