﻿namespace iLuvMyDentist.Models
{
    public class LoginResponse 
    {
        public string AccessToken { get; set; }
    }
}