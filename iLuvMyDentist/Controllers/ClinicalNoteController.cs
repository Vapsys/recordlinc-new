﻿using BO.Models;
using BO.ViewModel;
using BusinessLogicLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using iLuvMyDentist.App_Start;

namespace iLuvMyDentist.Controllers
{
    [RoutePrefix("api/ClinicalNote")]
    public class ClinicalNoteController : ApiController
    {

        /// <summary>
        /// This Method add  from outside systems.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("Add")]
        [ResponseType(typeof(APIResponseBase<bool>))]
        public HttpResponseMessage Add(List<ClinicalNote> model)
        {
            return Request.CreateResponse(HttpStatusCode.OK, ClinicalNotesBLL.Upload(model));
        }

        /// <summary>
        /// This Method Used for Delete List of Procedure on Recordlinc side.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("Delete")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<ClinicalNote>>))]
        public HttpResponseMessage Delete(List<ClinicalNote> model)
        {
            APIResponseBase<List<ClinicalNote>> result;
            APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError;
            var clinicalNotesBLL = new ClinicalNotesBLL();
            try
            {
                if (ModelState.IsValid)
                {
                    var List = clinicalNotesBLL.Delete(model);
                    if (List == null || List.Count == 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<ClinicalNote>>()
                        {
                            StatusCode = (int)HttpStatusCode.BadRequest,
                            IsSuccess = false,
                            Result = null,
                            Message = "Invalid DentrixConnectorId",
                        });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<ClinicalNote>>()
                        {
                            StatusCode = (int)HttpStatusCode.OK,
                            IsSuccess = true,
                            Result = List,
                            Message = "Success",
                        });
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        [Route("List")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<ClinicalNote>>))]
        public HttpResponseMessage List(ClinicalNoteFilter model)
        {
            APIResponseBase<List<ClinicalNote>> result;
            APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError;
            var clinicalNotesBLL = new ClinicalNotesBLL();
            try
            {
                if (ModelState.IsValid)
                {
                    var List = clinicalNotesBLL.List(model);
                    if (List == null || List.Count == 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<ClinicalNote>>()
                        {
                            StatusCode = (int)HttpStatusCode.BadRequest,
                            IsSuccess = false,
                            Result = null,
                            Message = "Invalid DentrixConnectorId",
                        });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<ClinicalNote>>()
                        {
                            StatusCode = (int)HttpStatusCode.OK,
                            IsSuccess = true,
                            Result = List,
                            Message = "Success",
                        });
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }
    }
}
