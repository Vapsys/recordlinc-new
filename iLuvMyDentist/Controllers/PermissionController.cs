﻿using BusinessLogicLayer;
using iLuvMyDentist.Filters;
using System;
using static iLuvMyDentist.App_Start.APIUtil;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Collections.Generic;
using BO.Models;
using BO.ViewModel;

namespace iLuvMyDentist.Controllers
{
    /// <summary>
    /// This Controller used for check the User has permission to do something on OCR side.
    /// </summary>
    [RoutePrefix("api/Permission")]
    [OCRCustomToken]
    public class PermissionController : ApiController
    {
        /// <summary>
        /// Check Permissions into OCR side.
        /// </summary>
        /// <returns></returns>
        [Route("FeaturePermission")]
        [HttpPost]
        [ResponseType(typeof(PermissionResult))]
        public HttpResponseMessage FeaturePermission(BO.Enums.Common.Features id,List<MemberPlanDetail> MemberPlan)
        {
            try
            {
                PermissionResult Obj = new PermissionResultBLL().CheckPermissionAndMemberShip(GetCurrentUser().UserId, (int)id, MemberPlan);
                if (Obj.HasPermission || !Obj.DoesMembershipAllow)
                {
                    Obj.Message = PermissionResultBLL.GetPermissionMessage((int)id);
                }
                return Request.CreateResponse(HttpStatusCode.OK, Obj);
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
    }
}
