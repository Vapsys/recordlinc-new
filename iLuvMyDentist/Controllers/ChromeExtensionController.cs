﻿using BO.Models;
using BusinessLogicLayer;
using iLuvMyDentist.Filters;
using System;
using static iLuvMyDentist.App_Start.APIUtil;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BO.ViewModel;
using System.Collections.Generic;

namespace iLuvMyDentist.Controllers
{
    /// <summary>
    /// This Controller Use for Chrome Extenstion Related APIs
    /// </summary>
    [RoutePrefix("api/ChromeExtension")]
    [OCRCustomToken]
    public class ChromeExtensionController : ApiController
    {
        /// <summary>
        /// Get Dentist Details by Access Token
        /// </summary>
        /// <returns></returns>
        [Route("DentistDetails")]
        [ResponseType(typeof(DentistDetail))]
        [HttpGet]
        public HttpResponseMessage DenstistDetails()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ChromeExtentionBLL.DentistDetailsByUserId(GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Un-Read Referrals 
        /// </summary>
        /// <returns></returns>
        [Route("GetUnReadReferrals")]
        [ResponseType(typeof(APIResponseBase<List<OneClickReferralDetail>>))]
        [HttpPost]
        public HttpResponseMessage GetUnReadReferrals(FilterMessageConversation Filter)
        {
            try
            {

                APIResponseBase<List<OneClickReferralDetail>> result = new APIResponseBase<List<OneClickReferralDetail>>();
                PatientReport Bll = new PatientReport();
                List<OneClickReferralDetail> lst = new List<OneClickReferralDetail>();
                lst = MessagesBLL.GetMessageConversation(Filter);
                if (lst.Count > 0)
                {
                    result = new APIResponseBase<List<OneClickReferralDetail>>()
                    {
                        StatusCode = 200,
                        IsSuccess = true,
                        Result = lst,
                        Message = "Success",
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                return Request.CreateResponse(HttpStatusCode.OK, result);

            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
    }
}
