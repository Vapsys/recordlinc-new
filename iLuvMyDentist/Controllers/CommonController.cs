﻿using BO.Models;
using BO.ViewModel;
using BusinessLogicLayer;
using iLuvMyDentist.Filters;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using static iLuvMyDentist.App_Start.APIUtil;

namespace iLuvMyDentist.Controllers
{
    /// <summary>
    /// Commonly used data are fetching with this API section.
    /// </summary>
    [RoutePrefix("api/Common")]
    [OCRCustomToken]
    public class CommonController : ApiController
    {
        /// <summary>
        /// Get List of State details for OCR.
        /// </summary>
        /// <param name="CountryCode">Country code for state list</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetStates")]
        [ResponseType(typeof(List<StateList>))]
        public HttpResponseMessage GetStates(string CountryCode)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.GetStateList(CountryCode));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get All Specialty list for dentist.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSpecialty")]
        [ResponseType(typeof(List<System.Web.Mvc.SelectListItem>))]
        public HttpResponseMessage GetSpecialty(string Specialty = null)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK,DentistBLL.GetSpeacilities(Specialty));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get All TimeZone list for dentist of US.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTimeZone")]
        [ResponseType(typeof(List<TimeZones>))]
        public HttpResponseMessage GetTimeZone()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.GetTimeZoneList());
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get All Country list for dentist of US.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetCountry")]
        [ResponseType(typeof(List<System.Web.Mvc.SelectListItem>))]
        public HttpResponseMessage GetCountry()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK,ProfileBLL.CountryList());
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Gets the patient insurance details for popup.
        /// </summary>
        /// <param name="intPatientId">The int patient identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPatientInsuranceDetails")]
        [ResponseType(typeof(PatientInsuranceDetails))]
        public HttpResponseMessage GetPatientInsuranceDetails(int intPatientId)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK,PatientBLL.GetPatientInsuranceDetails(intPatientId));
            }
            catch (Exception  Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Gets the patient insurance details for popup.
        /// </summary>
        /// <param name="intPatientId">The int patient identifier.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPatientInsuranceDataDetails")]
        public HttpResponseMessage GetPatientInsuranceDataDetails(int intPatientId)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.GetPatientInsuranceDataDetails(intPatientId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
        /// <summary>
        /// This API used for getting colleagues list with Team Member list
        /// </summary>
        /// <param name="SearchText"></param>
        /// <param name="ProviderType"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetColleagueList")]
        [ResponseType(typeof(Colleaguelist))]
        public HttpResponseMessage GetColleagueList(string SearchText, BO.Enums.Common.PMSProviderFilterType ProviderType)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, new SearchProfileOfDoctor().GetColleagueListById(GetCurrentUser().UserId, ProviderType, SearchText));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

       
    }
}
