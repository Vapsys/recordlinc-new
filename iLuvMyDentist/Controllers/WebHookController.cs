﻿using BO.Models;
using BO.ViewModel;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace iLuvMyDentist.Controllers
{
    /// <summary>
    /// This Controller used for creating any thrid party webhooks.
    /// </summary>
    [RoutePrefix("api/WebHook")]
    public class WebHookController : ApiController
    {
        /// <summary>
        /// This Method used for Zipwhip Received Message Weebhooks.
        /// </summary>
        /// <param name="ReceivedMessage"></param>
        /// <returns></returns>
        [Route("ZipWhipReceived")]
        [ResponseType(typeof(APIResponseBase<string>))]
        public HttpResponseMessage ZipWhipReceived(ZipwhipReceiver ReceivedMessage)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK,true);
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
    }
}
