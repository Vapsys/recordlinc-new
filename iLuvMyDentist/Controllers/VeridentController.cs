﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BO.Models;
using BusinessLogicLayer;
using BO.ViewModel;
using System.Data;
using DataAccessLayer.Common;
using System.Web;
using System.Web.Http.Description;
using System.Text.RegularExpressions;
using System.IO;
using iLuvMyDentist.App_Start;
using iLuvMyDentist.Filters;
using static iLuvMyDentist.App_Start.APIUtil;
using DataAccessLayer.ColleaguesData;

namespace iLuvMyDentist.Controllers
{   /// <summary>
    /// 
    /// </summary>
    [RoutePrefix("api/Verident")]
    
    public class VeridentController : BaseController
    {
        #region Global Veriable
        VeridentBLL objVeridentBLL = new VeridentBLL();
        clsColleaguesData ObjColleaguesData = new clsColleaguesData();
        static clsCommon ObjCommon = new clsCommon();
        #endregion

        /// <summary>
        /// Get Patient with appointment list for verident
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetPatients")]
        [ResponseType(typeof(APIResponseBase<List<PatientList>>))]
        [OCRCustomToken]
        public HttpResponseMessage GetPatients(PatientFilterForVeri filter)
        {
            
            try
            {               
                    PatientList objPatientList = new PatientList();
                    filter.UserId = GetCurrentUser().UserId;
                    objPatientList = objVeridentBLL.GetPatients(filter);
                    if (objPatientList != null && objPatientList.lstPatient != null && objPatientList.lstPatient.Count > 0)
                    {
                        return provideResponse(objPatientList);
                    }
                    else
                    {
                        return provideResponse(string.Empty, (int)HttpStatusCode.NotFound, true, "Patients not found.");
                    }                
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentController--GetPatients", Ex.Message, Ex.StackTrace);
                return provideResponse(string.Empty,(int)HttpStatusCode.InternalServerError, false, Ex.Message);
            }
        }

        /// <summary>
        ///  Sign Up 
        /// </summary>
        /// <param name="objUserSignUpForVeri"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SignUp")]
        [ResponseType(typeof(APIResponseBase<int>))]        
        public HttpResponseMessage SignUp(UserSignUpForVeri objUserSignUpForVeri)
        {            
            try
            {
                SignUpBLL ObjSignUpBLL = new SignUpBLL();                
                if (ModelState.IsValid)
                {
                    int InsertedId = ObjSignUpBLL.SignupForVeri(objUserSignUpForVeri);                                        
                    return provideResponse(InsertedId);
                }
                else
                {                    
                    return provideResponse(GetModelstateErrors(ModelState),(int)HttpStatusCode.BadRequest, false, "Method called with invalid parameters.");
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentController--SignUp", Ex.Message, Ex.StackTrace);
                return provideResponse(string.Empty, (int)HttpStatusCode.InternalServerError, false, Ex.Message);
            }
        }

        //[Route("ConfirmUserSignUp")]
        //[ResponseType(typeof(APIResponseBase<int>))]
        //public HttpResponseMessage ConfirmUserSignUp(string UserName, string Password)
        //{
        //    APIResponseBase<int> result = new APIResponseBase<int>();
        //    try
        //    {
             
        //    }
        //    catch (Exception Ex)
        //    {
             
        //    }

        //}



        /// <summary>
        /// Get Patient List for Verident
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPatientDetails")]
        [ResponseType(typeof(APIResponseBase<PatientDetailsForVeri>))]
        [OCRCustomToken]
        public HttpResponseMessage GetPatientDetails(int Id)
        {
            APIResponseBase<PatientDetailsForVeri> result = new APIResponseBase<PatientDetailsForVeri>();
            try
            {
                PatientDetailsForVeri objPatientDetailsForVeri = new PatientDetailsForVeri();
                objPatientDetailsForVeri = objVeridentBLL.GetPatientDetails(Id);
                if (objPatientDetailsForVeri != null )
                {                    
                    return provideResponse(objPatientDetailsForVeri);
                }
                else
                {                 
                    return provideResponse(string.Empty, (int)HttpStatusCode.NotFound,true, "Patient Details not found.");
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentController--GetPatientDetails", Ex.Message, Ex.StackTrace);
                return provideResponse(string.Empty, (int)HttpStatusCode.InternalServerError, false, Ex.Message);
            }
        }
        
        /// <summary>
        /// Add Edit Patient 
        /// </summary>
        /// <param name="objPatientDetailsForVeri"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddEditPatient")]
        [ResponseType(typeof(APIResponseBase<int>))]
        [OCRCustomToken]
        public HttpResponseMessage AddEditPatient(PatientDetailsForVeri objPatientDetailsForVeri)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int newPatientId = objVeridentBLL.AddEditPatientVeri(objPatientDetailsForVeri, GetCurrentUser().UserId);
                    return provideResponse(newPatientId, objPatientDetailsForVeri.PatientId > 0 ? (int)HttpStatusCode.OK : (int)HttpStatusCode.Created);                    
                }
                else
                {
                    return provideResponse(GetModelstateErrors(ModelState), (int)HttpStatusCode.BadRequest, false, "Method called with invalid parameters.");
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentController--AddEditPatient", Ex.Message, Ex.StackTrace);
                return provideResponse(string.Empty, (int)HttpStatusCode.InternalServerError, false, Ex.Message);
            }
        }       


        /// <summary>
        /// Check Email id Exists or not in our system
        /// </summary>
        /// <param name="Email">Email ID</param>
        /// <returns></returns>  
        [HttpGet]
        [Route("CheckEmailExists")]
        [ResponseType(typeof(APIResponseBase<int>))]
        public HttpResponseMessage CheckEmailExists(string Email)
        {
            APIResponseBase<int> result = new APIResponseBase<int>();
            try
            {
                int Status = objVeridentBLL.CheckEmailExists(Email);
                string Message = "";
                if (Status >= 0)
                {                   
                    Message = Status > 0 ? "Email is in System and verified." : "Email is in System and but not verified.";                    
                    return provideResponse(Status, (int)HttpStatusCode.OK, true, Message);
                }
                else
                {                    
                    return provideResponse(Status, (int)HttpStatusCode.OK, true, Message);
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentController--CheckEmailExists", Ex.Message, Ex.StackTrace);
                return provideResponse(-2, (int)HttpStatusCode.InternalServerError,false,Ex.Message);
            }
        }


        /// <summary>
        /// Add Edit lable
        /// </summary>
        /// <param name="objLable"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddEditLabel")]
        [ResponseType(typeof(APIResponseBase<string>))]
        [OCRCustomToken]
        public HttpResponseMessage AddEditLabel(Labels objLable)
        {
            APIResponseBase<int> result = new APIResponseBase<int>();
            int LabelId = 0;
            try
            {
                if (ModelState.IsValid)
                {
                    LabelId = objVeridentBLL.AddEditLable(objLable, GetCurrentUser().UserId, GetCurrentUser().AccountId);
                    if (LabelId > 0)
                    {                        
                        return provideResponse(LabelId, (objLable.Id > 0) ? (int)HttpStatusCode.OK : (int)HttpStatusCode.Created);
                    }
                    else
                    {                        
                        return provideResponse(string.Empty, (int)HttpStatusCode.BadRequest,true, LabelId == -1 ? "This label is already exists." : "This label is not associated with your account.");
                    }
                }
                else
                {                    
                    return provideResponse(GetModelstateErrors(ModelState), (int)HttpStatusCode.BadRequest, false, "Method called with invalid parameters.");
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentController--AddEditLabel", Ex.Message, Ex.StackTrace);
                return provideResponse(string.Empty, (int)HttpStatusCode.InternalServerError, false, Ex.Message);
            }
        }

        /// <summary>
        /// Delete Label 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteLabel")]
        [ResponseType(typeof(APIResponseBase<string>))]
        [OCRCustomToken]
        public HttpResponseMessage DeleteLabel(int Id)
        {
            APIResponseBase<string> result = new APIResponseBase<string>();
            try
            {                           
                int status = objVeridentBLL.DeleteLabel(Id, GetCurrentUser().UserId, GetCurrentUser().AccountId);
                if (status > 0)
                {                    
                    return provideResponse("Label deleted successfully");
                }else
                {
                    return provideResponse(string.Empty, (int)HttpStatusCode.BadRequest,false, status == -1 ? "It is default label you cannot delete it." : "This label is not associated with your account.");
                }                                                  
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentController--DeleteLabel", Ex.Message, Ex.StackTrace);
                return provideResponse(false, (int)HttpStatusCode.InternalServerError, false, Ex.Message);
            }
        }


        /// <summary>
        /// Get label list 
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Getlabels")]
        [ResponseType(typeof(APIResponseBase<List<Labels>>))]
        [OCRCustomToken]
        public HttpResponseMessage Getlabels(int Id=0)
        {            
            List<Labels> lstLabels = new List<Labels>();
            try
            {
                lstLabels  = objVeridentBLL.GetLabels(Id, GetCurrentUser().AccountId);

                if (lstLabels != null && lstLabels.Count > 0)
                {                    
                    return provideResponse(lstLabels);
                }
                else
                {                    
                    return provideResponse(string.Empty, (int)HttpStatusCode.NotFound, false, "Label not found!");
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentController--Getlabels", Ex.Message, Ex.StackTrace);
                return provideResponse(false, (int)HttpStatusCode.InternalServerError, false, Ex.Message);
            }
        }

        /// <summary>
        /// Add Patient label.
        /// </summary>
        /// <param name="patientLabel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddPatientLabel")]
        [ResponseType(typeof(APIResponseBase<bool>))]
        [OCRCustomToken]
        public HttpResponseMessage AddPatientLabel(PatientLabelAssociation patientLabel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return provideResponse(VeridentBLL.AddPatientLabel(patientLabel, GetCurrentUser().UserId, GetCurrentUser().AccountId));
                }
                else
                {                  
                    return provideResponse(GetModelstateErrors(ModelState), (int)HttpStatusCode.BadRequest, false, "Method called with invalid parameters.");
                }
            }
            catch (Exception Ex)
            {                
                new clsCommon().InsertErrorLog("VeridentController--AddPatientLabel", Ex.Message, Ex.StackTrace);
                return provideResponse(false, (int)HttpStatusCode.InternalServerError, false, Ex.Message);
            }
        }

        /// <summary>
        /// Remove Patient Label
        /// </summary>
        /// <param name="patientLabel"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("RemovePatientLabel")]
        [ResponseType(typeof(APIResponseBase<bool>))]
        [OCRCustomToken]
        public HttpResponseMessage RemovePatientLabel(PatientLabelAssociation patientLabel)
        {
            try
            {
                if (ModelState.IsValid)
                {                    
                    return provideResponse(VeridentBLL.RemovePatientLabel(patientLabel, GetCurrentUser().UserId, GetCurrentUser().AccountId));
                }
                else
                {                    
                    return provideResponse(GetModelstateErrors(ModelState), (int)HttpStatusCode.BadRequest, false, "Method called with invalid parameters.");
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentController--RemovePatientLabel", Ex.Message, Ex.StackTrace);
                return provideResponse(false, (int)HttpStatusCode.InternalServerError, false, Ex.Message);
            }
        }

        /// <summary>
        /// Verify / Reverify Patient insurance details.
        /// </summary>
        /// <param name="Id">PatientId</param>
        /// <param name="verify">Patient verify object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("Verify")]
        [ResponseType(typeof(APIResponseBase<List<One_ResponseInsu>>))]
        [OCRCustomToken]
        public HttpResponseMessage Verify(List<VerifyInsu> VerifyInsu)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (VerifyInsu != null && VerifyInsu.Count <= 5)
                    {
                        return provideResponse(VeridentBLL.VerifyReverifyPatientInsruance(VerifyInsu, GetCurrentUser().UserId, GetCurrentUser().AccountId));

                    }
                    else
                    {
                        return provideResponse(string.Empty, (int)HttpStatusCode.BadRequest, false, "Must have 5 patient in single call verification.");
                    }
                }else
                {
                    return provideResponse(GetModelstateErrors(ModelState), (int)HttpStatusCode.BadRequest, false, "Method called with invalid parameters.");
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentController--Verify", Ex.Message, Ex.StackTrace);
                return provideResponse(string.Empty, (int)HttpStatusCode.InternalServerError, false, Ex.Message);
            }
        }

        /// <summary>
        /// Verident 
        /// </summary>
        /// <param name="objVeridentLogin"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Login")]
        [ResponseType(typeof(APIResponseBase<VeridentUserDetails>))]
        public HttpResponseMessage Login(VeridentLogin objVeridentLogin)
        {
            try
            {
                VeridentUserDetails objVeridentUserDetails = new VeridentUserDetails();
                VeridentUser objVeridentUser = new VeridentUser();
                if (ModelState.IsValid)
                {
                    objVeridentUser = objVeridentBLL.VeridentUserLogin(objVeridentLogin);
                    if (objVeridentUser.messageDetais.Id == 1)
                    {                        
                        return provideResponse(objVeridentUser.veridentUserDetails);
                    }                    
                    else
                    {                        
                        return provideResponse(string.Empty, (int)HttpStatusCode.Unauthorized, false, objVeridentUser.messageDetais.Message);
                    }
                }
                else
                {                    
                    return provideResponse(GetModelstateErrors(ModelState), (int)HttpStatusCode.BadRequest, false, "Method called with invalid parameters.");
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentController--Login", Ex.Message, Ex.StackTrace);
                return provideResponse(string.Empty, (int)HttpStatusCode.InternalServerError, false, Ex.Message);
            }
        }

        /// <summary>
        /// Forgot Password 
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ForgotPassowrd")]
        [ResponseType(typeof(APIResponseBase<bool>))]
        public HttpResponseMessage ForgotPassowrd(string Email)
        {
            try
            {
                if (ObjCommon.IsEmail(Email))
                {
                    bool Status = objVeridentBLL.ForgotPassord(Email);
                    if (Status == true)
                    {
                        return provideResponse(true, (int)HttpStatusCode.OK, true, "Reset password link sent succesfully");
                    }
                    else
                    {
                        return provideResponse(false, (int)HttpStatusCode.BadRequest, false, "This email is not in our system.");
                    }
                }else
                {
                    return provideResponse(false, (int)HttpStatusCode.BadRequest, true, "Please provide valid email.");
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentController--ForgotPassowrd", Ex.Message, Ex.StackTrace);
                return provideResponse(string.Empty, (int)HttpStatusCode.InternalServerError, false, Ex.Message);
            }
        }
        /// <summary>
        /// Reset Passowrd
        /// </summary>
        /// <param name="objResetPassword"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ResetPassword")]
        [ResponseType(typeof(APIResponseBase<bool>))]
        public HttpResponseMessage ResetPassord(UserAccountPro objResetPassword)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int Status = objVeridentBLL.ResetPassord(objResetPassword);
                    if (Status == 1)
                    {
                        return provideResponse(true, (int)HttpStatusCode.OK, true, "Reset password succesfully");
                    }
                    else if (Status ==2)
                    {
                        return provideResponse(false, (int)HttpStatusCode.BadRequest, false, "This email is not in our system.");
                    }
                    else 
                    {
                        return provideResponse(false, (int)HttpStatusCode.BadRequest, false, "Someting want to wrong.");
                    }
                }
                else
                {
                    return provideResponse(GetModelstateErrors(ModelState), (int)HttpStatusCode.BadRequest, true, "Please provide valid email.");
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentController--ResetPassword", Ex.Message, Ex.StackTrace);
                return provideResponse(string.Empty, (int)HttpStatusCode.InternalServerError, false, Ex.Message);
            }
        }


        /// <summary>
        /// User Account Activation 
        /// </summary>
        /// <param name="objUserAccountPro"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AccountActivation")]
        [ResponseType(typeof(APIResponseBase<bool>))]
        public HttpResponseMessage AccountActivation(UserAccountPro objUserAccountPro)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int Status = objVeridentBLL.AccountActivation(objUserAccountPro);
                    if (Status == 1)
                    {
                        return provideResponse(true, (int)HttpStatusCode.OK, true, "Account verify succesfully");
                    }
                    else if (Status == 2)
                    {
                        return provideResponse(false, (int)HttpStatusCode.BadRequest, false, "This email is not in our system.");
                    }
                    else
                    {
                        return provideResponse(false, (int)HttpStatusCode.BadRequest, false, "This account is already activated.");
                    }
                }
                else
                {
                    return provideResponse(GetModelstateErrors(ModelState), (int)HttpStatusCode.BadRequest, true, "Please provide valid email.");
                }
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("VeridentController--ResetPassword", Ex.Message, Ex.StackTrace);
                return provideResponse(string.Empty, (int)HttpStatusCode.InternalServerError, false, Ex.Message);
            }
        }
    }
}