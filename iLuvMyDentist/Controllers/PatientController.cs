﻿using BO.Models;
using iLuvMyDentist.Filters;
using System;
using System.Collections.Generic;
using static iLuvMyDentist.App_Start.APIUtil;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BusinessLogicLayer;
using BO.ViewModel;
using DataAccessLayer.Appointment;
using System.Web;
using System.Configuration;
using System.IO;
using DataAccessLayer.Common;
using DataAccessLayer.PatientsData;
using System.Web.Helpers;
using System.Drawing;
using System.Data;

namespace iLuvMyDentist.Controllers
{
    /// <summary>
    /// Need to pass access_token into the Header in order to call APIs of Patient.
    /// </summary>
    [RoutePrefix("api/Patients")]
    [OCRCustomToken]
    public class PatientController : ApiController
    {
        /// <summary>
        /// Get List of Patient for User.
        /// </summary>
        /// <param name="patientFilter"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Get")]
        [ResponseType(typeof(List<NewPatientDetailsOfDoctor>))]
        public HttpResponseMessage Get(PatientFilter patientFilter)
        {
            try
            {
                patientFilter.UserId = GetCurrentUser().UserId;
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.NewGetPatientDetailsOfDoctor(patientFilter));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get List of Location for User.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetLocations")]
        [ResponseType(typeof(List<AddressDetails>))]
        public HttpResponseMessage GetLocations()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ColleaguesBLL.GetDentistLocationsById(Convert.ToString(GetCurrentUser().UserId)));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Delete Multiple/Single Patient for User.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("Delete")]
        [ResponseType(typeof(PatientDelete))]
        public HttpResponseMessage Delete(DeleteMultiplePatient delete)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.DeletePatient(delete, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Request to Patient To Fill the form.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("SendForm")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage SendForm(SendForm Obj)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.SendForm(Obj.PatientId, GetCurrentUser().UserId, Obj.PatientEmail, Obj.DoctorName, Obj.PatientName));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Patient Details for Patient History.
        /// </summary>
        /// <param name="PatientId">Patient Id</param>
        /// <param name="SystemTimeZoneName">Time Zone of Dentist System.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("PatientDetails")]
        [ResponseType(typeof(PatientHistoryViewModel))]
        public HttpResponseMessage PatientDetails(int PatientId, string SystemTimeZoneName)
        {
            try
            {
                CustomAuthenticationIdentity CurrentUser = GetCurrentUser();
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.GetPatientDetails(PatientId, CurrentUser.UserId, CurrentUser.TimeZoneOfDentist));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Colleagues and Team Member list for Compose Communication section.
        /// </summary>
        /// <param name="PatientId">Patient Id</param>
        /// <param name="ColleagueId">Comma separated Colleague Ids</param>
        /// <returns></returns>
        [HttpGet]
        [Route("ComposeCommunicationList")]
        [ResponseType(typeof(List<System.Web.Mvc.SelectListItem>))]
        public HttpResponseMessage ComposeCommunicationList(int PatientId, string ColleagueId)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.GetCommunicationColleagueList(PatientId, GetCurrentUser().UserId, ColleagueId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Selected Colleagues and Team Member list for Compose Communication section.
        /// </summary>
        /// <param name="PatientId">Patient Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSelectedUserList")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage GetSelectedUserList(int PatientId)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.GetSelectedListforComposeComm(PatientId, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="PatientId"></param>
        /// <param name="ColleagueId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSelectedColleagueList")]
        [ResponseType(typeof(List<System.Web.Mvc.SelectListItem>))]
        public HttpResponseMessage GetSelectedColleagueList(int PatientId, string ColleagueId)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.GetSelectedListforcollegues(PatientId, GetCurrentUser().UserId, ColleagueId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
        ///// <summary>
        ///// Add Insurance Coverage details of Patient 
        ///// </summary>
        ///// <param name="insuranceCoverage">Insurance Coverage Object</param>
        ///// <param name="PatientId"></param>
        ///// <returns></returns>
        //[Route("AddInsuranceDetail")]
        //[ResponseType(typeof(bool))]
        //
        //public HttpResponseMessage AddPatientInsuranceDetails(InsuranceCoverage insuranceCoverage,int PatientId)
        //{
        //    try
        //    {
        //        CustomAuthenticationIdentity CurrentUser = GetCurrentUser();
        //        return Request.CreateResponse(HttpStatusCode.OK,new DentistProfileBLL()._AddInsuranceCoverage(insuranceCoverage,PatientId,));
        //    }
        //    catch (Exception Ex)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
        //        throw;
        //    }
        //}
        /// <summary>
        /// Add Medical history of Patient
        /// </summary>
        /// <param name="medicalHistory">Medical History Object</param>
        /// <param name="PatientId">PatientId</param>
        /// <returns></returns>
        [Route("AddMedicalHistory")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage AddMedicalDetails(Compose1ClickMedicalHistory medicalHistory, int PatientId)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, new DentistProfileBLL()._AddMedicalHistory(medicalHistory, PatientId));

            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Add Medical history of Patient
        /// </summary>
        /// <param name="dentalHistory">Dental History Object</param>
        /// <param name="PatientId">PatientId</param>
        /// <returns></returns>
        [Route("AddDentalHistory")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage AddDentalHistory(Compose1ClickDentalHistory dentalHistory, int PatientId)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, new DentistProfileBLL()._AddDentalHistory(dentalHistory, PatientId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Patient Images
        /// </summary>
        /// <param name="id">PatientId</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetImages")]
        [ResponseType(typeof(List<UplodedImagesViewModel>))]
        public HttpResponseMessage GetImages(int id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.GetPatientImages(id, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Patient Documents
        /// </summary>
        /// <param name="id">PatientId</param>
        /// <param name="TimeZone">Time Zone of Doctor</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetDocuments")]
        [ResponseType(typeof(List<UplodedImagesViewModel>))]
        public HttpResponseMessage GetDocuments(int id, string TimeZone)
        {
            try
            {
                CustomAuthenticationIdentity CurrentUser = GetCurrentUser();
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.GetPatientDocuments(id, CurrentUser.UserId, CurrentUser.TimeZoneOfDentist));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Delete Patient Image
        /// </summary>
        /// <param name="ImageId">Image id</param>
        /// <param name="MontageId">Montage Id</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteImage")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage DeleteImage(int ImageId, int MontageId)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.DeletePatientImage(ImageId, MontageId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Delete Patient Document
        /// </summary>
        /// <param name="MontageId">Montage Id</param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteDocument")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage DeleteDocument(int MontageId)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.DeletePatientDocument(MontageId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Edit time Get File Details using images id
        /// </summary>
        /// <param name="id">Image / Montage Id</param>
        /// <param name="Type">1 = Image, 2 = Document</param>
        /// <param name="PatientId">Patient Id</param>
        /// <param name="TimeZone">Time Zone of Dentist.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("FileDetail")]
        [ResponseType(typeof(EditFilesViewModel))]
        public HttpResponseMessage FileDetail(int id, int Type, int PatientId, string TimeZone)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.FileDetails(id, Type, PatientId, GetCurrentUser().TimeZoneOfDentist));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// EditFileDetail
        /// </summary>
        /// <param name="alterFile">Object of file which contain a detail</param>
        /// <returns></returns>
        [HttpPost]
        [Route("EditFileDetail")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage EditFileDetail(AlterFile alterFile)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.EditFileDetails(alterFile));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Referral Category values using referral card Id.
        /// </summary>
        /// <param name="id">Referral Card Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetReferralCategory")]
        [ResponseType(typeof(List<ReferralCategory>))]
        public HttpResponseMessage GetReferralCategory(int id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, new MessagesBLL().GetReferralCategoryValue(id));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Add/Edit Patient Insurance Coverage details.
        /// </summary>
        /// <param name="Obj">Insurance Coverage Object</param>
        /// <param name="PatientId">Patient Id</param>
        /// <returns></returns>
        [Route("AddInsuranceCoverage")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage AddInsuranceCoverage(InsuranceCoverage Obj, int PatientId)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, DentistProfileBLL.AddInsuranceCoverage(Obj, PatientId, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Edit Patient Info with auto save functionality
        /// </summary>
        /// <param name="patient">Patient Information object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("EditPatientHistory")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage EditPatientHistory(PatientEditableVM patient)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.UpdatePatientDetails(patient));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Check assign patient Id is exist or not
        /// </summary>
        /// <param name="PatientId">Patient Id</param>
        /// <param name="AssignPatientid">Assign PatientId of Patient.</param>
        /// <returns></returns>
        [HttpGet]
        [Route("CheckAssignPatientID")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage CheckAssignPatientID(int PatientId, int AssignPatientid)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.CheckAssignPatientId(AssignPatientid, PatientId, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Add Edit Patient using both side add Patient page as well as on Patient form.
        /// </summary>
        /// <param name="patients">Patient info object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddPatientInfo")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage AddPatientInfo(Patients patients)
        {
            try
            {
                patients.OwnerId = GetCurrentUser().UserId;
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.AddEditPatientDetails(patients));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Add Patient Profile images.
        /// </summary>
        /// <param name="profileImage"></param>
        /// <param name="Paths"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddPatientProfile")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage AddPatientProfile(PatientProfileImage profileImage, string Paths = null)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.AddPatientImage(profileImage));
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("API--Patient--AddPatientProfile", Ex.Message, Ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Compose Communication section.
        /// </summary>
        /// <param name="compose"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ComposeCommunication")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage ComposeCommunication(ComposeCommunication compose)
        {
            try
            {
                compose.UserId = GetCurrentUser().UserId;
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.SendMessageFromPatientHistory(compose));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Colleagues list for Referred by 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetColleagues")]
        [ResponseType(typeof(List<System.Web.Mvc.SelectListItem>))]
        public HttpResponseMessage GetColleagues()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.GetColleague(GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Add Patient 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("AddPatient")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage AddPatient(PatientHistoryViewModel add)
        {
            try
            {
                add.OwnerId = GetCurrentUser().UserId;
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.AddPatient(add));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Check Patient
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("CheckPatient")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage CheckPatient(CheckPatients check)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.checkPatient(check, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Message Details 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GetMessageDetails")]
        [ResponseType(typeof(ColleaguesMessagesOfDoctorViewModel))]
        public HttpResponseMessage GetMessageDetails(int id, string TimeZone)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.GetMessageDetials(id, GetCurrentUser().TimeZoneOfDentist));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
        /// <summary>
        /// Get Patient Message Details
        /// </summary>
        /// <param name="id"></param>
        /// <param name="TimeZone"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetPatientMessageDetails")]
        [ResponseType(typeof(OCRPatientMessagesOfDoctor))]
        public HttpResponseMessage GetPatientMessageDetails(int id, string TimeZone)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.PatientMessage(id, GetCurrentUser().TimeZoneOfDentist));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Communication details for Patient History.
        /// </summary>
        /// <param name="id">Patient Id</param>
        /// <param name="TimeZone">TimeZone of Dentist</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetCommunicationDetails")]
        [ResponseType(typeof(ColleaguesMessagesOfDoctorViewModel))]
        public HttpResponseMessage GetCommunicationDetails(int id, string TimeZone)
        {
            try
            {
                CustomAuthenticationIdentity CurrentUser = GetCurrentUser();
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.GetCommunicationHistoryForPatient(CurrentUser.UserId, id, 1, int.MaxValue, 2, 1, CurrentUser.TimeZoneOfDentist));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Referral details for Patient History.
        /// </summary>
        /// <param name="id">Referral Message Id</param>
        /// <param name="TimeZone">TimeZone of Dentist</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetReferral")]
        [ResponseType(typeof(List<ReferralMessageDetails>))]
        public HttpResponseMessage GetReferral(int id, string TimeZone)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.GetReferralDetail(id, GetCurrentUser().TimeZoneOfDentist));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Normal Message details for Patient History.
        /// </summary>
        /// <param name="id">Message Id</param>
        /// <param name="TimeZone">TimeZone of Dentist</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetMessage")]
        [ResponseType(typeof(ColleaguesMessagesOfDoctorViewModel))]
        public HttpResponseMessage GetMessage(int id, string TimeZone)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.GetNormalMessage(id, GetCurrentUser().TimeZoneOfDentist));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Patient Message details for Patient History.
        /// </summary>
        /// <param name="id">Message Id</param>
        /// <param name="TimeZone">TimeZone of Dentist</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPatientMessage")]
        [ResponseType(typeof(PatientMessagesOfDoctorViewModel))]
        public HttpResponseMessage GetPatientMessage(int id, string TimeZone)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.GetPatientMessage(id, GetCurrentUser().TimeZoneOfDentist));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
        /// <summary>
        /// Deletes the family releation of patient.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteRelation")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage DeleteRelation(DeleteRelationRequest request)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.DeleteRelation(request.FromPatientId, request.ToPatientId));
            }
            catch (Exception)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Some internal server error occured !");
            }

        }
        /// <summary>
        /// GetCommunicationHistoryForPatient after sorting
        /// </summary>
        /// <param name="objPatientCommunicationSort"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("GetCommunicationHistoryForPatient")]
        [ResponseType(typeof(ColleaguesMessagesOfDoctorViewModel))]
        public HttpResponseMessage GetCommunicationHistoryForPatient(PatientCommunicationSort objPatientCommunicationSort) {

            try
            {
                CustomAuthenticationIdentity CurrentUser = GetCurrentUser();

                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.GetCommunicationHistoryForPatient(CurrentUser.UserId, objPatientCommunicationSort.PatientId, objPatientCommunicationSort.PageIndex, 20, objPatientCommunicationSort.SortColumn, objPatientCommunicationSort.SortDirection, Convert.ToString(CurrentUser.TimeZoneOfDentist)));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
        public HttpResponseMessage Patientlist()
        {
            try
            {
                CustomAuthenticationIdentity CurrentUser = GetCurrentUser();
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
        /// <summary>
        /// Get Patient Detials for the Verdient Product.
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPatientDetailsVeri")]
        [ResponseType(typeof(Patients))]
        public HttpResponseMessage GetPatientDetailsVeri(int PatientId)
        {
            try
            {
                Patients model = PatientBLL.GetPatientDetailsById(PatientId, Convert.ToString(GetCurrentUser().UserId));
                DataTable result = new clsPatientsData().GetRegistration(PatientId, "GetRegistrationform");
                if (result.Rows.Count > 0 && result != null)
                {
                    model.EmergencyContactName = Convert.ToString(result.Rows[0]["txtemergencyname"]);
                    model.EmergencyPhoneNo = Convert.ToString(result.Rows[0]["txtemergency"]);
                }
                return Request.CreateResponse(HttpStatusCode.OK, model);
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
        /// <summary>
        /// The Patient Insurance details need to be verified using onederful.
        /// </summary>
        /// <param name="PatientId"></param>
        /// <returns></returns>
        /// [Route("GetPatientDetailsVeri")]
        //[ResponseType(typeof(bool))]
        //public HttpResponseMessage VerifyPatientDetails(int PatientId)
        //{
        //    try
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.VerifyPatientInsuranceDetails(PatientId, GetCurrentUser().UserId));
        //    }
        //    catch (Exception Ex)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
        //        throw;
        //    }
        //}

           

        [HttpPost]
        [Route("AddEditPatientForVeri")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage AddEditPatientForVeri(Reward_PatientUpdate objReward_PatientUpdate)
        {
            try
            {
                 int intOwnerId = GetCurrentUser().UserId;
                return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.AddEditPatientDetailsForVeri(objReward_PatientUpdate, intOwnerId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
    }

}

