using BO.Models;
using BO.ViewModel;
using BusinessLogicLayer;
using iLuvMyDentist.Filters;
using iLuvMyDentist.Models;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using iLuvMyDentist.App_Start;
using System.Threading;
using static iLuvMyDentist.App_Start.APIUtil;

namespace iLuvMyDentist.Controllers
{
    /// <summary>
    /// This Controller used for Smile Brand integration.
    /// </summary>
    [RoutePrefix("api/SmileBrand")]
    public class SmileBrandController : ApiController
    {
        /// <summary>
        /// This API method used to get access token using dentist login credentials.
        /// </summary>
        /// <param name="Obj">Accept username and password</param>
        /// <returns></returns>
        [Route("Login")]
        [ResponseType(typeof(APIResponseBase<string>))]
        public HttpResponseMessage Login(Login Obj)
        {
            APIResponseBase<string> result = new APIResponseBase<string>();
            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SmileBrandBLL.GetAccessToken(Obj));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method Used for Get All Dentist list which is Associate with an Organization(Account)
        /// </summary>
        /// <param name="Obj">Accept AccountId and LocationId of user</param>
        /// <returns></returns>
        [Route("GetDentistForOrganization")]
        [HttpPost]
        [ResponseType(typeof(OrganizationResponse<List<DoctorDetails>>))]
        [CustomTokenValidationFilter]
        public HttpResponseMessage GetListOfDentistForOrganization(Account Obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, PatientRewardBLL.GetDentistListOfAccount(Obj));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method will return aging records of Patient.
        /// </summary>
        /// <param name="Obj">Accepts Patient Id of the patient</param>
        /// <returns></returns>
        [Route("GetAgingRecords")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<DentrixAging>))]
        [CustomTokenValidationFilter]
        public HttpResponseMessage GetAgingRecords(DentrixAgingInputModel Obj)
        {
            APIResponseBase<DentrixAging> result = new APIResponseBase<DentrixAging>();
            try
            {
                if (ModelState.IsValid)
                {
                    CustomAuthenticationIdentity Objs = GetCurrentUser();
                    result.StatusCode = 200;
                    result.IsSuccess = true;
                    result.Result = PatientRewardBLL.GetAgingRecords(Obj, Objs.UserId);
                    result.Message = string.Empty;
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method will return List of Patient.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("GetPatientList")]
        [HttpPost]
        [CustomTokenValidationFilter]
        [ResponseType(typeof(Pageing<List<PatientDetailsModel>>))]
        public HttpResponseMessage GetPatientList(PatientFilter model)
        {
            APIResponse<List<PatientDetailsModel>> result = new APIResponse<List<PatientDetailsModel>>();
            List<PatientDetailsModel> lst = new List<PatientDetailsModel>();
            PatientBLL clsPatientBLL = new PatientBLL();

            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.GetPatientList(model, GetCurrentUser().UserId));
                }
                else
                {
                    APIResponseBase<List<KeyMessage>> results = new APIResponseBase<List<KeyMessage>>();
                    results.StatusCode = 500;
                    results.IsSuccess = false;
                    results.Result = GetModelstateErrors(ModelState);
                    results.Message = null;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, results);
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method will return List of Patient Payments. For very first request you can pass all parameters other than RequestId. The system will fetch all records based on filter criteria and will return RequestId with data. When you need second or later pages for the result, please supply only "RequestId, PageIndex and Type", Do not pass any other parameters
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("GetPatientsPayments")]
        [ResponseType(typeof(UpdateResponse<PatientPaymentModel>))]
        [HttpPost]
        [CustomTokenValidationFilter]
        public HttpResponseMessage GetPatientsPayments(PatientPayments model)
        {
            //APIResponseBase<PatientPaymentModel> result = new APIResponseBase<PatientPaymentModel>();
            //PatientPaymentModel result1 = new PatientPaymentModel();
            try
            {
                if (ModelState.IsValid)
                {
                    CustomAuthenticationIdentity Objs = GetCurrentUser();
                    return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.GetPatientPayments(model, Objs.UserId));
                    //result1 = PatientBLL.GetPatientPayments(model, Objs.UserId);

                    //if ((result1.lstPatient_DentrixAdjustments == null) && (result1.lstPatient_DentrixFinanceCharges == null)
                    //     && (result1.lstPatient_DentrixInsurancePayment == null) && (result1.lstPatient_DentrixStandardPayment == null))
                    //{
                    //    result = new APIResponseBase<PatientPaymentModel>()
                    //    {
                    //        StatusCode = 404,
                    //        IsSuccess = false,
                    //        Result = null,
                    //        Message = "No Record found!"
                    //    };
                    //    return Request.CreateResponse(HttpStatusCode.OK, result);
                    //}
                    //else
                    //{
                    //    var notfound = new APIResponseBase<PatientPaymentModel>()
                    //    {
                    //        StatusCode = 200,
                    //        IsSuccess = true,
                    //        Result = result1,
                    //        Message = "Success"
                    //    };
                    //    return Request.CreateResponse(HttpStatusCode.OK, notfound);
                    //}
                }
                else
                {
                    APIResponseBase<List<KeyMessage>> results = new APIResponseBase<List<KeyMessage>>();
                    results.StatusCode = 500;
                    results.IsSuccess = false;
                    results.Result = GetModelstateErrors(ModelState);
                    results.Message = null;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, results);
                }

            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This API method use for getting patient all procedure using requested patientid. For very first request you can pass all parameters other than RequestId. The system will fetch all records based on filter criteria and will return RequestId with data. When you need second or later pages for the result, please supply only "RequestId and PageIndex", Do not pass any other parameters
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("GetPatientProcedures")]
        [ResponseType(typeof(RetriveResponse<List<PatientProcedures>>))]
        [HttpPost]
        [CustomTokenValidationFilter]
        public HttpResponseMessage GetPatientProcedure(PatientProcedure<BO.Enums.Common.PatientProcedureDateFilter> Obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    APIResponseBase<List<Patient_DentrixProcedure>> result = new APIResponseBase<List<Patient_DentrixProcedure>>();
                    CustomAuthenticationIdentity Objs = GetCurrentUser();
                    return Request.CreateResponse(HttpStatusCode.OK, SmileBrandBLL.GetPatientProcedures(Obj, Objs.UserId));
                }
                else
                {
                    APIResponseBase<List<KeyMessage>> results = new APIResponseBase<List<KeyMessage>>();
                    results.StatusCode = 500;
                    results.IsSuccess = false;
                    results.Result = GetModelstateErrors(ModelState);
                    results.Message = null;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, results);
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method used for getting the dentist location list.
        /// </summary>
        /// <returns></returns>
        [Route("GetDentistLocation")]
        [ResponseType(typeof(APIResponseBase<Locationinfo>))]
        [HttpGet]
        [CustomTokenValidationFilter]
        public HttpResponseMessage GetDentistLocation()
        {
            try
            {
                CustomAuthenticationIdentity Objs = GetCurrentUser();
                Locationinfo Loc = new Locationinfo();
                Loc.Locations = SmileBrandBLL.GetLocationList(Objs.UserId);
                APIResponseBase<Locationinfo> Result = new APIResponseBase<Locationinfo>();
                Result.Message = "Success!";
                Result.IsSuccess = true;
                Result.Result = Loc;
                Result.StatusCode = 200;
                return Request.CreateResponse(HttpStatusCode.OK, Result);
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This is a writeback API to accepts responses of patient procedure sent via GetPatientProcedures. It accepts Recordlink Primary key "RL_ID" and "IsSuccess" (either true OR false) for each row. In case if particular row is not inserted in your DB, return false with actual reason, else retun true.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("DPMSProcedureResponse")]
        [ResponseType(typeof(RetriveResponse<bool>))]
        [HttpPost]
        [CustomTokenValidationFilter]
        public HttpResponseMessage ProcedureResponse(DPMS_ProcedureResponse Obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SmileBrandBLL.ProcedureResponse(Obj));
                }
                else
                {
                    APIResponseBase<List<KeyMessage>> results = new APIResponseBase<List<KeyMessage>>();
                    results.StatusCode = 500;
                    results.IsSuccess = false;
                    results.Result = GetModelstateErrors(ModelState);
                    results.Message = null;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, results);
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This is a writeback API to accepts responses of patient payments sent via GetPatientsPayments. It accepts Recordlink Primary key "RL_ID" and "IsSuccess" (either true OR false) for each row. In case if particular row is not inserted in your DB, return false with actual reason, else retun true.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("DPMSPaymentResponse")]
        [ResponseType(typeof(RetriveResponse<bool>))]
        [HttpPost]
        [CustomTokenValidationFilter]
        public HttpResponseMessage PaymentResponse(DMPS_PaymentResponse Obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SmileBrandBLL.PaymentResponse(Obj));
                }
                else
                {
                    APIResponseBase<List<KeyMessage>> results = new APIResponseBase<List<KeyMessage>>();
                    results.StatusCode = 500;
                    results.IsSuccess = false;
                    results.Result = GetModelstateErrors(ModelState);
                    results.Message = null;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, results);
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }


        /// <summary>
        /// This API method use for getting patient all appointments using requested patientid. For very first request you can pass all parameters other than RequestId. The system will fetch all records based on filter criteria and will return RequestId with data. When you need second or later pages for the result, please supply only "RequestId and PageIndex", Do not pass any other parameters
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("GetPatientAppointments")]
        [ResponseType(typeof(RetriveResponse<List<PatientAppointments>>))]
        [HttpPost]
        [CustomTokenValidationFilter]
        public HttpResponseMessage GetPatientAppointments(PatientProcedure<BO.Enums.Common.PatientAppointmentDateFilter> Obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    APIResponseBase<List<Patient_DentrixProcedure>> result = new APIResponseBase<List<Patient_DentrixProcedure>>();
                    CustomAuthenticationIdentity Objs = GetCurrentUser();
                    return Request.CreateResponse(HttpStatusCode.OK, SmileBrandBLL.GetPatientAppointments(Obj, Objs.UserId));
                }
                else
                {
                    APIResponseBase<List<KeyMessage>> results = new APIResponseBase<List<KeyMessage>>();
                    results.StatusCode = 500;
                    results.IsSuccess = false;
                    results.Result = GetModelstateErrors(ModelState);
                    results.Message = null;
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, results);
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<string>()
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    IsSuccess = false,
                    Message = Ex.Message,
                    Result = null
                });
                throw;
            }
        }

    }
}
