﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BO.ViewModel;
using BusinessLogicLayer;
using System.Configuration;
using BO.Models;
using Twilio;
using System.Web;

namespace iLuvMyDentist.Controllers
{
    [RoutePrefix("api/Dentist")]
    public class DoctorProfileController : ApiController
    {

        [Route("GetProfile")]
        public IHttpActionResult GetProfile(string username = null)
        {
            try
            {
                DentistProfileDetail objProfileDetails = new DentistProfileDetail();
                DentistProfileBLL objdentistprofile = new DentistProfileBLL();
                if (!string.IsNullOrWhiteSpace(username))
                {
                    int UserId = 0;
                    UserId = objdentistprofile.GetIdOnPublicProfileURL(username);
                    if (UserId > 0)
                    {
                        objProfileDetails = objdentistprofile.GetDoctorDetials(UserId);
                        return Ok(objProfileDetails);
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception Ex)
            {
                throw new HttpException(400, Ex.Message + Ex.StackTrace);
            }

        }
        [Route("VCF")]
        public IHttpActionResult VCF(string username = null)
        {
            try
            {
                DentistProfileDetail objProfileDetails = new DentistProfileDetail();
                DentistProfileBLL objdentistprofile = new DentistProfileBLL();
                VCardForDoctor ObjVcard = new VCardForDoctor();
                string Filename = string.Empty;
                if (!string.IsNullOrWhiteSpace(username))
                {
                    int UserId = 0;
                    UserId = objdentistprofile.GetIdOnPublicProfileURL(username);
                    if (UserId > 0)
                    {
                        ObjVcard = objdentistprofile.GetVCFfileforRLAPI(UserId);
                        return Ok(ObjVcard);
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception Ex)
            {
                throw new HttpException(400, Ex.Message + Ex.StackTrace);
            }
        }

        [Route("LogError")]
        public IHttpActionResult LogError(Exception ex)
        {
            try
            {
                ErrorLogBLL objError = new ErrorLogBLL();
                bool IsLogged = objError.LogError(ex);
                if (IsLogged) {
                    return Ok();
                }
                else {
                    return NotFound();
                }
            }
            catch (Exception exe)
            {
                throw new HttpException(400, exe.Message + exe.StackTrace);
            }
        }

        [Route("Learn")]
        public IHttpActionResult Learn(MDLPatientSignUp obj)
        {
            try
            {
                var result = new DentistRatingBLL().SendMailToSupportforRequestDemo(obj);
                if (result) {
                    return Ok();
                }
                else {
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                throw new HttpException(400, ex.Message + ex.StackTrace);
            }
        }
    }
}
