﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BO.Models;
using BusinessLogicLayer;
using iLuvMyDentist.Filters;
using static iLuvMyDentist.App_Start.APIUtil;
using System.Web.Helpers;
using BO.ViewModel;

namespace iLuvMyDentist.Controllers
{
    /// <summary>
    /// Get Profile Details.
    /// Need to pass access_token into the Header in order to call APIs of Profile.
    /// </summary>
    [RoutePrefix("api/Settings")]
    [OCRCustomToken]
    public class SettingsController : ApiController
    {
        /// <summary>
        /// Get Account settings details for OCR 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Get")]
        [ResponseType(typeof(AccountSettings))]
        public HttpResponseMessage Get()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, SettingsBLL.GetAccountDetails(GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Post Account settings details from OCR 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("Post")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage Post(SaveAccountSetting accountSettings)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK,SettingsBLL.SaveAccountSettings(accountSettings, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Account settings details for OCR 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetIntegration")]
        [ResponseType(typeof(List<Integration>))]
        public HttpResponseMessage GetIntegration()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, SettingsBLL.GetIntegration(GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Save Integration settings from OCR side.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveIntegrations")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage SaveIntegrations(SaveIntegration save)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, SettingsBLL.SaveIntegrationRecord(save, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get UnSync Location List baised on Integration Id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetUnSyncLocations")]
        [ResponseType(typeof(List<System.Web.Mvc.SelectListItem>))]
        public HttpResponseMessage GetUnSyncLocations(int id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, SettingsBLL.GetUnSyncLocations(id, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get List of Team Member 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("GetTeamMember")]
        [ResponseType(typeof(List<TeamMember>))]
        public HttpResponseMessage GetTeamMember(FilterTeamMember _filter)
        {
            try
            {
                _filter.UserId = GetCurrentUser().UserId;
                return Request.CreateResponse(HttpStatusCode.OK, SettingsBLL.GetTeamMemberList(_filter));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Dentist Location List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetDentistLocations")]
        [ResponseType(typeof(List<System.Web.Mvc.SelectListItem>))]
        public HttpResponseMessage GetDentistLocations()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, SettingsBLL.GetDentistLocationList(GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Team Member Details by Id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetTeamMemberDetails")]
        [ResponseType(typeof(TeamMember))]
        public HttpResponseMessage GetTeamMemberDetails(int id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, SettingsBLL.GetTeamMemberDetails(id, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Add/Edit Team Member Details by Id
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("AddEditTeamMemer")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage AddEditTeamMemer(TeamMember _team)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, SettingsBLL.AddEditTeamMember(_team, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Remove Team Member by Id
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("RemoveTeamMember")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage RemoveTeamMember(RemoveTeam _remove)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, SettingsBLL.RemoveTeamMember(_remove, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Check Dentist Email Already Exists or not
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("CheckDentistEmailExists")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage CheckDentistEmailExists(string Email,int id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, SettingsBLL.CheckDentistEmailExists(Email,id));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Dentist Referral Form settings.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetReferralForm")]
        [ResponseType(typeof(MainReferralForm))]
        public HttpResponseMessage GetReferralForm()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ReferralFormBLL.GetDentistSpeciality(GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Save Dentist Referral Form Settings 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveReferralForm")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage SaveReferralForm(SaveReferralForm _save)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ReferralFormBLL.AddRefferalFormSetting(GetCurrentUser().UserId,_save.RefFrmSetting,_save.SpecialtySetting,_save.ServiceSetting,_save.SubFieldSetting));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Patient Referral Form settings.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetPatientReferralForm")]
        [ResponseType(typeof(List<PatientSectionMapping>))]
        public HttpResponseMessage GetPatientReferralForm()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ReferralFormBLL.GetDentistPatientmappingDetail(GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Save Patient Referral Form Settings 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("SavePatientReferralSettings")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage SavePatientReferralSettings(SavePatientReferralSettings _save)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ReferralFormBLL.AddDentistPatientreferralFormSetting(GetCurrentUser().UserId, _save.SectionSetting, _save.FieldSettings, _save.SubFieldSettings));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Disable Integration settings
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DisableIntegration")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage DisableIntegration(string id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, SettingsBLL.DisableIntegration(id, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Enable Integration settings
        /// </summary>
        /// <param name="id">Integration Id</param>
        /// <returns></returns>
        [HttpPost]
        [Route("EnableIntegration")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage EnableIntegration(int id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, SettingsBLL.EnableIntegration(id, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Integration Details by Integration Id
        /// </summary>
        /// <param name="id">Integration Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetIntegrationDetails")]
        [ResponseType(typeof(List<CompanyIntegrationDetail>))]
        public HttpResponseMessage GetIntegrationDetails(int id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, SettingsBLL.GetIntegrationDetails(id, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Enable Connector baised on Connector Id
        /// </summary>
        /// <param name="id">Connector Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("EnableConnector")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage EnableConnector(int id)
        {
            try
            {
                CustomAuthenticationIdentity CurrentUser = GetCurrentUser();
                return Request.CreateResponse(HttpStatusCode.OK, SettingsBLL.EnableConnector(id,CurrentUser.AccountId,CurrentUser.UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Disable Connector baised on Connector Id
        /// </summary>
        /// <param name="id">Connector Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("DisableConnector")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage DisableConnector(int id)
        {
            try
            {
                CustomAuthenticationIdentity CurrentUser = GetCurrentUser();
                return Request.CreateResponse(HttpStatusCode.OK, SettingsBLL.DisableConnector(id, CurrentUser.AccountId, CurrentUser.UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Save Sequence for Patient Referral Form Settings 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveManageSequence")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage SaveManageSequence(FieldSequence data)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ReferralFormBLL.SaveManageSequence(data));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Save Sequence for Patient Referral Form Settings 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("SavePatientManageSequence")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage SavePatientManageSequence(FieldSequence data)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ReferralFormBLL.SavePatientManageSequence(data));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Return GUID key for Integration 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("AddPMSAccount")]
        [ResponseType(typeof(APIResponseBase<string>))]
        public IHttpActionResult AddPMSAccount()
        {
            APIResponseBase<string> result = new APIResponseBase<string>();
           
            try
            {
                string accountKey = Crypto.SHA1(Convert.ToString(DateTime.Now.Ticks));
                result.StatusCode = 200;
                result.IsSuccess = true;
                result.Result = accountKey;
                result.Message = string.Empty;
                return Ok(result);
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.Message);                
                throw;
            }
                        
        }

    }
}
