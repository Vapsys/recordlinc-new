﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BO.ViewModel;
using BusinessLogicLayer;
namespace iLuvMyDentist.Controllers
{
    [RoutePrefix("api/Login")]
    public class LoginController : ApiController
    {
        [Route("post")]
        public IHttpActionResult post(PatientLogin Obj)
        {
            try
            {
                LoginBLL ObjLoginBLL = new LoginBLL();
                IDictionary<bool, string> objreturn;
                objreturn = ObjLoginBLL.LoginonRecordlinc(Obj);
                if (objreturn.Keys.First()){
                    return Ok();
                } else {
                    return Content(HttpStatusCode.NotFound, objreturn.Values.First());
                }
            }
            catch (Exception Ex)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }

        [Route("Forgot")]
        public IHttpActionResult Forgot(string username)
        {
            try
            {
                LoginBLL ObjLoginBLL = new LoginBLL();
                IDictionary<bool, string> objreturn;
                objreturn = ObjLoginBLL.SendForgetPassword(username);
                if (objreturn.Keys.First())
                {
                    return Ok();
                }
                else {
                    return Content(HttpStatusCode.NotFound, objreturn.Values.First());
                }
            }
            catch (Exception Ex)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }
        /// <summary>
        /// This method check 
        /// </summary>
        /// <param name="access_token"></param>
        /// <returns></returns>
        //[Route("AccessOfOCR")]
        //public HttpResponseMessage AccessOfOCR(string access_token)
        //{
        //    try
        //    {

        //    }
        //    catch (Exception Ex)
        //    {

        //        throw;
        //    }
        //}
    }
}
