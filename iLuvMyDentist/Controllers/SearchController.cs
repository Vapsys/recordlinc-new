﻿using BO.Models;
using BO.ViewModel;
using BusinessLogicLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace iLuvMyDentist.Controllers
{
    [RoutePrefix("api/Search")]
    public class SearchController : ApiController
    {
        [Route("SearchResult")]
        [HttpPost]
        public IHttpActionResult SearchResult(Search model)
        {
            if(model != null)
            {
                SearchProfileOfDoctor ObjOfBLL = new SearchProfileOfDoctor();
                List<DentistDetial> lst = new List<DentistDetial>();
                lst = ObjOfBLL.GetSearchResult(model);
                return Ok(lst);
            }
            else
            {
                return BadRequest("Method called with invalid parameters.");
            }
        }
        [Route("GetSpecialties")]
        [HttpGet]
        public IHttpActionResult GetSpecialties()
        {
            Search model = new Search();
            SearchProfileOfDoctor ObjOfBLL = new SearchProfileOfDoctor();
            model.lst = ObjOfBLL.DoctorSpecialities();
            return Ok(model);
        }
    }
}
