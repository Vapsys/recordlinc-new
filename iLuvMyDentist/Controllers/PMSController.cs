﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using iLuvMyDentist.Models;
using BO.ViewModel;
using BusinessLogicLayer;
using BO.Models;
using System.Web.Http.Description;
using static iLuvMyDentist.App_Start.APIUtil;
using System.Net.Http;
using System;
using System.Data;
using DataAccessLayer.ColleaguesData;
using DataAccessLayer.Common;
using System.Web.Http.Cors;

namespace iLuvMyDentist.Controllers
{
   
    [RoutePrefix("api/PMS")]
    public class PMSController : ApiController
    {

        [Route("Login")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<PMSLoginModel>))]
        public IHttpActionResult Login(UserCredentialsModel model)
        {
            if (model != null)
            {
                PatientLogin login = new PatientLogin();
                login.Email = model.Username;
                login.Password = model.Password;
                APIResponseBase<PMSLoginModel> result = new APIResponseBase<PMSLoginModel>();
                IDictionary<bool, string> response = new Dictionary<bool, string>();
                try
                {
                    response = new LoginBLL().LoginonRecordlinc(login);
                }
                catch (Exception ex)
                {

                }
                List<DentrixConnectors> DentrixConnectors = new List<DentrixConnectors>();
                DentrixConnectors = LoginBLL.GetUserDentrixConnector(login.Email);

                PMSLoginModel objPMSLoginModel = new PMSLoginModel()
                {
                    Token = new LoginBLL().GetAuthenticationTocken(login).Values.FirstOrDefault(),
                    DentrixConnectors = DentrixConnectors,
                };

                if (response.Keys.FirstOrDefault())
                {
                    result.StatusCode = 200;
                    result.IsSuccess = true;
                    result.Result = objPMSLoginModel;
                    result.Message = string.Empty;
                    return Ok(result);
                }
                else
                {

                    //result.StatusCode = 404;
                    //result.IsSuccess = false;
                    //result.Result = string.Empty;
                    //result.Message = response.Values.FirstOrDefault();
                    //return new HttpResponseMessage() { StatusCode = HttpStatusCode.BadRequest, Content =  })
                    return BadRequest(response.Values.FirstOrDefault());
                    
                }
            }
            else
            {
                return BadRequest("Method called with invalid parameters.");
            }

        }

        [Route("ValidateToken")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<bool>))]
        public IHttpActionResult ValidateToken(TokenModel Obj)
        {
            if (!string.IsNullOrWhiteSpace(Obj.Token))
            {
                APIResponseBase<bool> result = new APIResponseBase<bool>();
                IDictionary<bool, string> response = new LoginBLL().ValidateToken(Obj.Token);
                if (response.Keys.FirstOrDefault())
                {
                    result.StatusCode = 200;
                    result.IsSuccess = true;
                    result.Result = true;
                    result.Message = "Valid Token";
                    return Ok(result);
                }
                else
                {
                    return BadRequest(response.Values.FirstOrDefault());
                }
            }
            else
            {
                return BadRequest("Method called with invalid parameters.");
            }
        }

        /// <summary>
        /// Gives encrypted userid based on given access token.
        /// </summary>
        /// <returns></returns>
        [Route("GetEncryptedUserId")]
        [Filters.OCRCustomToken]
        [ResponseType(typeof(string))]
        public string GetEncryptedUserId()
        {
            Filters.CustomAuthenticationIdentity Objs = GetCurrentUser();

            return new DataAccessLayer.Common.TripleDESCryptoHelper().encryptText("" + Objs.UserId);
        }


        [Route("GetReferPatient")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<ResponceReferPatientModel>))]
        public IHttpActionResult GetReferPatient(ReferPatientModel model)
        {
            if (!string.IsNullOrEmpty(model.Token))
            {
                SendReferralBLL bllSendReferral = new SendReferralBLL();
                LoginBLL bllLogin = new LoginBLL();
                ResponceReferPatientModel apiResponse = new ResponceReferPatientModel();

                DoctorLoginForPMS login = bllLogin.GetUserCredentials(model.Token);
                if (login != null)
                {
                    ValidationResponse response = bllSendReferral.ValidateInputsForPMSSendReferral(model.PMSPatientDetails, model.PMSProviderDetails);
                    if (response.Status)
                    {
                        new clsCompany().GetActiveCompany();
                        PMSSendReferralDetails obj = bllSendReferral.SendReferralFromPMS(login.UserId, model.PMSPatientDetails, model.PMSProviderDetails);

                        #region Set Response property
                        apiResponse.Token = model.Token;
                        apiResponse.ToColleague = obj == null ? 0 : obj.ColleagueIds;
                        apiResponse.PatientId = obj == null ? 0 : obj.PatientId;
                        apiResponse.UserId = login.UserId;
                        apiResponse.IsIntegration = true;
                        #endregion
                    }
                }
                return Ok(apiResponse);
            }
            else
            {
                return BadRequest("Method called with invalid parameters.");
            }

        }

    }
}