using BO.Models;
using BusinessLogicLayer;
using iLuvMyDentist.App_Start;
using iLuvMyDentist.Filters;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BO.ViewModel;
using static iLuvMyDentist.App_Start.APIUtil;

namespace iLuvMyDentist.Controllers
{
    [RoutePrefix("api/SuperDentist")]
    public class SuperDentistController : ApiController
    {
        /// <summary>
        /// This API method used to get access token using dentist login credentials.
        /// </summary>
        /// <param name="Obj">Accept username and password</param>
        /// <returns></returns>
        [Route("Login")]
        [ResponseType(typeof(APIResponseBase<string>))]
        public HttpResponseMessage Login(Login Obj)
        {
            APIResponseBase<string> result = new APIResponseBase<string>();
            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, SmileBrandBLL.GetAccessToken(Obj));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This API method used for Edit Reward of SuperDentist
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("EditReward")]
        [ResponseType(typeof(APIResponseBase<bool>))]
        [CustomTokenValidationFilter]
        public HttpResponseMessage EditPatientReward(EditSupPatientReward model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    CustomAuthenticationIdentity Objs = GetCurrentUser();
                    return Request.CreateResponse(HttpStatusCode.OK, PatientRewardBLL.EditPatientRewards(model, Objs.UserId, Objs.AccountId));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This API method used for Add Patient Reward of Super Bucks.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("AddReward")]
        [ResponseType(typeof(RetirveReward<Reward>))]
        [CustomTokenValidationFilter]
        public HttpResponseMessage AddPatientReward(AddPatientReward model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    CustomAuthenticationIdentity Objs = GetCurrentUser();
                    return Request.CreateResponse(HttpStatusCode.OK, PatientRewardBLL.AddPatientReward(model, Objs.UserId, Objs.AccountId));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This API method used for Add Patient Reward of Super Bucks.
        /// </summary>
        /// <param name="RewardId"></param>
        /// <returns></returns>
        [Route("DeleteReward")]
        [ResponseType(typeof(APIResponseBase<bool>))]
        [CustomTokenValidationFilter]
        [HttpPut]
        public HttpResponseMessage DeletePatientReward(int RewardId)
        {
            try
            {
                if(RewardId > 0)
                {
                    CustomAuthenticationIdentity Objs = GetCurrentUser();
                    return Request.CreateResponse(HttpStatusCode.OK, PatientRewardBLL.DeletePatientReward(RewardId, Objs.AccountId));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "RewardId must be bigger then 1!");
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }
    }
}
