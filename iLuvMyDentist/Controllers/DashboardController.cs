﻿using BO.Models;
using BO.ViewModel;
using BusinessLogicLayer;
using DataAccessLayer.Common;
using iLuvMyDentist.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using static iLuvMyDentist.App_Start.APIUtil;

namespace iLuvMyDentist.Controllers
{
    /// <summary>
    /// Need to pass access_token into the Header in order to call APIs of Patient.
    /// </summary>
    [RoutePrefix("api/OneClickReferral")]
    [OCRCustomToken]
    public class DashboardController : ApiController
    {
        ///// <summary>
        ///// This Method Get the Dentist Referral Received History.
        ///// </summary>
        ///// <param name="FilterObj"></param>
        ///// <returns></returns>
        //[Route("OneClickReferralHistory")]
        //[HttpPost]
        //[ResponseType(typeof(APIResponseBase<List<OneClickReferralDetail>>))]
        //public HttpResponseMessage OneClickReferralHistory(FilterMessageConversation FilterObj)
        //{
        //    try
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<OneClickReferralDetail>>()
        //        {
        //            StatusCode = 200,
        //            IsSuccess = true,
        //            Result = MessagesBLL.GetMessageConversation(FilterObj),
        //            Message = "Success",
        //        });
        //    }
        //    catch (Exception Ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<string>()
        //        {
        //            StatusCode = 500,
        //            IsSuccess = true,
        //            Result = Ex.Message,
        //            Message = "Error",
        //        });
        //    }
        //}
        ///// <summary>
        ///// This Method get Disposition status 
        ///// </summary>
        ///// <returns></returns>
        //[Route("GetDispositionListForOCR")]
        //[ResponseType(typeof(APIResponseBase<List<Disposition>>))]
        //[HttpGet]
        //public HttpResponseMessage GetDispositionListForOCR()
        //{
        //    try
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<Disposition>>()
        //        {
        //            StatusCode = 200,
        //            IsSuccess = true,
        //            Result = new PatientReport().GetDispositionlist(),
        //            Message = "Success",
        //        });
        //    }
        //    //Catch the Error Message while requesting a Data.
        //    catch (Exception Ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<string>()
        //        {
        //            StatusCode = 500,
        //            IsSuccess = true,
        //            Result = Ex.Message,
        //            Message = "Error",
        //        });
        //    }
        //}

        ///// <summary>
        ///// This method used for getting Patient name for OCR Dashboard Auto Complete
        ///// </summary>
        ///// <param name="PatientName"></param>
        ///// <returns></returns>
        //[Route("GetPatietName")]
        //[ResponseType(typeof(APIResponseBase<string>))]
        //[HttpGet]
        //public HttpResponseMessage GetPatientNameForAutocomplete(string PatientName)
        //{
        //    try
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<string>()
        //        {
        //            StatusCode = 200,
        //            IsSuccess = true,
        //            Result = ReferralFormBLL.GetPatientNameForAutoComplete(PatientName),
        //            Message = "Success!",
        //        });
        //    }
        //    catch (Exception Ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<string>()
        //        {
        //            StatusCode = 500,
        //            IsSuccess = true,
        //            Result = Ex.Message,
        //            Message = "Error",
        //        });
        //    }
        //}
        ///// <summary>
        ///// This API method is used for OCR Disposition status change of referral.
        ///// </summary>
        ///// <returns></returns>
        //[Route("ChangeDispositionStatus")]
        //[ResponseType(typeof(APIResponseBase<bool>))]
        //[HttpPost]
        //public HttpResponseMessage ChangeDispositionStatus(UpdateDisposition Obj)
        //{
        //    try
        //    {
        //        CustomAuthenticationIdentity CurrentUser = GetCurrentUser();
        //        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<bool>()
        //        {
        //            StatusCode = 200,
        //            IsSuccess = true,
        //            Result = PatientReport.UpdateDispositionStatus(Obj.DispositionId, Obj.MessageId, CurrentUser.UserId, Obj.LoginURL, Obj.Note, Obj.VisibleToPatient, Obj.VisibleToDoctor),
        //            Message = "Success!",
        //        });
        //    }
        //    catch (Exception Ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<string>()
        //        {
        //            StatusCode = 500,
        //            IsSuccess = true,
        //            Result = Ex.Message,
        //            Message = "Error",
        //        });
        //    }
        //}

        ///// <summary>
        ///// This Method get the Dentist details form database.
        ///// </summary>
        ///// <param name="access_token"></param>
        ///// <returns></returns>
        //[HttpGet]
        //[ResponseType(typeof(DentistDetail))]
        //[Route("DentistDetail")]
        //public HttpResponseMessage DentistDetail()
        //{
        //    try
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, ReferralFormBLL.GetDentistDetail(access_token));
        //    }
        //    catch (Exception Ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<string>()
        //        {
        //            StatusCode = 500,
        //            IsSuccess = true,
        //            Result = Ex.Message,
        //            Message = "Error",
        //        });
        //    }
        //}
        ///// <summary>
        ///// This Method is used for Delete the Message from OCR.
        ///// </summary>
        ///// <param name="Obj"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[ResponseType(typeof(int))]
        //[Route("DeleteMessage")]
        //public HttpResponseMessage DeleteMessage(DeleteMessage Obj)
        //{
        //    try
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, ReferralFormBLL.DeleteMessageFromOCR(Obj));
        //    }
        //    catch (Exception Ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<string>()
        //        {
        //            StatusCode = 500,
        //            IsSuccess = true,
        //            Result = Ex.Message,
        //            Message = "Error",
        //        });
        //    }
        //}
        ///// <summary>
        ///// This Method is used for Get Unread Message Count from OCR.
        ///// </summary>
        ///// <param name="access_token"></param>
        ///// <returns></returns>
        //[HttpGet]
        //[ResponseType(typeof(UnreadMessageCount))]
        //[Route("InboxUnreadCount")]
        //public HttpResponseMessage GetInboxUnreadCount(string access_token)
        //{
        //    try
        //    {
        //        UnreadMessageCount Model = new UnreadMessageCount();
        //        DataTable Dt = clsCommon.GetDentistDetailsByAccessToken(access_token);
        //        if (Dt.Rows.Count > 0)
        //        {
        //            Model.Count = new MessagesBLL().GetUnreadMessageCountOfDoctor(Convert.ToInt32(Dt.Rows[0]["UserId"]));
        //        }
        //        return Request.CreateResponse(HttpStatusCode.OK, Model);
        //    }
        //    catch (Exception Ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<string>()
        //        {
        //            StatusCode = 500,
        //            IsSuccess = true,
        //            Result = Ex.Message,
        //            Message = "Error",
        //        });
        //    }
        //}
    }
}
