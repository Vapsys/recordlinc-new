﻿using System.Web.Http;
using System;
using System.Net.Http;
using System.Net;
using System.Web.Http.Description;
using System.Collections.Generic;
using BO.ViewModel;
using BusinessLogicLayer;
using BO.Models;
using iLuvMyDentist.Filters;
using static iLuvMyDentist.App_Start.APIUtil;
using System.Web;
using System.Text.RegularExpressions;
using System.IO;
using System.Configuration;

namespace iLuvMyDentist.Controllers
{
    /// <summary>
    /// Get Colleagues Details.
    /// Need to pass access_token into the Header in order to call APIs of Colleague.
    /// </summary>
    [RoutePrefix("api/Colleagues")]
    public class ColleaguesController : ApiController
    {
        /// <summary>
        /// Get List of Colleagues details for OCR 
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Get")]
        [ResponseType(typeof(List<ColleagueDetails>))]
        [OCRCustomToken]
        public HttpResponseMessage GetColleagueListing(FilterColleagues Obj)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ColleaguesBLL.GetColleaguesForOCR(Obj, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Delete Colleagues for OCR.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteColleagues")]
        [ResponseType(typeof(bool))]
        [OCRCustomToken]
        public HttpResponseMessage DeleteColleagues(DeleteColleagues Obj)
        {
            try
            {
                return (DentistBLL.DeleteColleagues(Obj, GetCurrentUser().UserId)) ? Request.CreateResponse(HttpStatusCode.OK,true) : Request.CreateResponse(HttpStatusCode.NotModified,false);
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Enable/Disable single Has button functionality 
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("EnableHasButton")]
        [ResponseType(typeof(bool))]
        [OCRCustomToken]
        public HttpResponseMessage EnableHasButton(EnableHasButton Obj)
        {
            try
            {
                return (ColleaguesBLL.EnableDisablehasbuttonval(Obj.ReceiverId, GetCurrentUser().UserId)) ? Request.CreateResponse(HttpStatusCode.OK) : Request.CreateResponse(HttpStatusCode.NotModified);
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Enable/Disable Multiple Has button functionality 
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateHasButton")]
        [ResponseType(typeof(bool))]
        [OCRCustomToken]
        public HttpResponseMessage UpdateHasButton(UpdateHasButton Obj)
        {
            try
            {
                return (ColleaguesBLL.UpdateHasButton(Obj.ColleaguesId, GetCurrentUser().UserId, Obj.IsEnable)) ? Request.CreateResponse(HttpStatusCode.OK) : Request.CreateResponse(HttpStatusCode.NotModified);
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Search Colleagues Functionality on OCR side.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SearchColleagues")]
        [ResponseType(typeof(List<SearchColleagueViewModel>))]
        [OCRCustomToken]
        public HttpResponseMessage SearchColleague(FilterSearchColleagues Obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DentistBLL.SearchColleagues(Obj, GetCurrentUser().UserId));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }

            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Add Colleagues on OCR side.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddColleague")]
        [ResponseType(typeof(bool))]
        [OCRCustomToken]
        public HttpResponseMessage AddColleagues(AddColleagues Obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return (DentistBLL.AddColleagues(Obj, GetCurrentUser().UserId)) ? Request.CreateResponse(HttpStatusCode.OK,true) : Request.CreateResponse(HttpStatusCode.BadRequest);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, ModelState);
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Dentists Specialty 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("DentistSpecialty")]
        [ResponseType(typeof(List<SpeacilitiesOfDoctor>))]
        public HttpResponseMessage DentistSpecialty()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, DentistBLL.GetSpeacilities());
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Invite colleagues form OCR side.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Invite")]
        //[ResponseType(typeof(string))]
        [ResponseType(typeof(ResponseMeassge))]
        [OCRCustomToken]
        public HttpResponseMessage Invite(InviteColleague Obj)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ColleaguesBLL.InviteColleagues(Obj, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Invite Colleague Excel sheet.
        /// </summary>
        /// <param name="FileIds"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(List<TempFileAttacheMents>))]
        [OCRCustomToken]
        [Route("GetInviteColleagueFile")]
        public HttpResponseMessage GetInviteColleagueFile(string FileIds)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, new CommonBLL().GetTempAttachments(FileIds, 0, 0));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Upload Excel Sheet for Invite Multiple Colleagues
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("UploadExcelToInvite")]
        [ResponseType(typeof(string))]
        [OCRCustomToken]
        public HttpResponseMessage UploadInviteColleagueFile(string Base64)
        {
            try
            {
                string UploadedFileIds = string.Empty;
                CustomAuthenticationIdentity CurrentUser = GetCurrentUser();
                var files = HttpContext.Current.Request.Files;
                List<string> objId = new List<string>();
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFileBase file = new HttpPostedFileWrapper(files[i]);
                    int RecordId = 0; string Filename = string.Empty; string FileExtension = string.Empty; string NewFileName = string.Empty;
                    if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        Filename = testfiles[testfiles.Length - 1];
                    }
                    else
                    {
                        Filename = file.FileName;
                    }

                    Filename = Regex.Replace(Filename, @"[^0-9a-zA-Z\._]", string.Empty);
                    NewFileName = "$" + DateTime.Now.Ticks + "$" + Filename;
                    FileExtension = Path.GetExtension(Filename).ToLower();

                    if (!new CommonBLL().CheckFileIsValidImage(Filename))
                    {
                        RecordId = new CommonBLL().Insert_Temp_UploadedFiles(CurrentUser.UserId, NewFileName, new CommonBLL().GetUniquenumberForAttachments());
                        objId.Add("F_" + RecordId);
                    }
                    else
                    {
                        RecordId = new CommonBLL().Insert_Temp_UploadedImages(CurrentUser.UserId, NewFileName, new CommonBLL().GetUniquenumberForAttachments());
                        objId.Add("I_" + RecordId);
                    }
                    NewFileName = Path.Combine(ConfigurationManager.AppSettings["CSVUpload"], NewFileName);
                    file.SaveAs(NewFileName);
                }
                UploadedFileIds = string.Join(",", objId);
                return Request.CreateResponse(HttpStatusCode.OK, UploadedFileIds);
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Insert Temp File 
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertTempFile")]
        [OCRCustomToken]
        [ResponseType(typeof(string))]
        public HttpResponseMessage InsertTempFile(string FileName)
        {
            try
            {
                List<string> objId = new List<string>(); int RecordId = 0; string UploadedFileIds = string.Empty;
                CustomAuthenticationIdentity CurrentUser = GetCurrentUser();
                if (!new CommonBLL().CheckFileIsValidImage(FileName))
                {
                    RecordId = new CommonBLL().Insert_Temp_UploadedFiles(CurrentUser.UserId, FileName, new CommonBLL().GetUniquenumberForAttachments());
                    objId.Add("F_" + RecordId);
                }
                else
                {
                    RecordId = new CommonBLL().Insert_Temp_UploadedImages(CurrentUser.UserId, FileName, new CommonBLL().GetUniquenumberForAttachments());
                    objId.Add("I_" + RecordId);
                }
                UploadedFileIds = string.Join(",", objId);
                return Request.CreateResponse(HttpStatusCode.OK, UploadedFileIds);
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Delete Attachment from OCR
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteExcel")]
        [OCRCustomToken]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage DeleteExcelAttachment(int FileId,string FileName)
        {
            try
            {
                bool Status = false;
                string FilePath = Path.Combine(ConfigurationManager.AppSettings["CSVUpload"], FileName);
                if (new CommonBLL().Temp_AttachedFileDeleteById(FileId, 1, 0))
                {
                    Status = true;
                    if (File.Exists(FilePath))
                        File.Delete(FilePath);
                }
                return (Status) ? Request.CreateResponse(HttpStatusCode.OK,Status): Request.CreateResponse(HttpStatusCode.BadRequest, false);
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
    }
}
