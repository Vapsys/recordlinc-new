﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BO.ViewModel;
namespace iLuvMyDentist.Controllers
{
    [RoutePrefix("Request")]
    public class RequestController : ApiController
    {
        [Route("Demo")]
        public IHttpActionResult Demo(MDLPatientSignUp Obj)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    return NotFound();
                }
                else
                {
                    return Content(HttpStatusCode.NotAcceptable, "Model is not valid");
                }
            }
            catch (Exception)
            {

                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }
    }
}
