﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BO.ViewModel;
using BusinessLogicLayer;
using BO.Models;

namespace iLuvMyDentist.Controllers
{
    [RoutePrefix("api/SignUp")]
    public class SignUpController : ApiController
    {
        [Route("post")]
        [HttpPost]
        public IHttpActionResult post(DentistSignup Obj)
        {
            try
            {
                if(ModelState.IsValid)
                {
                    IDictionary<bool, string> objreturn;
                    SignUpBLL SignupBLL = new SignUpBLL();
                    objreturn = SignupBLL.SignuponRecordlinc(Obj);
                    if(objreturn.Keys.First()) {
                        return Ok();
                    } else {
                        return Content(HttpStatusCode.NotFound, objreturn.Values.First());
                    }
                } else {
                    return Content(HttpStatusCode.NotFound, "Model is not Valid");
                }
            }
            catch (Exception)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }
    }
}
