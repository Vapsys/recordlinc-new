﻿using BO.Models;
using BusinessLogicLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Twilio;
using BO.ViewModel;
using System.Web;
using System.Text;

namespace iLuvMyDentist.Controllers
{
    [RoutePrefix("api/Notification")]
    public class NotificationController : ApiController
    {
        private static string AccountSid = ConfigurationSettings.AppSettings["TwillioAccountSid"];
        private static string AuthToken = ConfigurationSettings.AppSettings["TwillioAuthToken"];
        private static string TwillioPhone = ConfigurationSettings.AppSettings["TwillioPhone"];
        private static string CountryCode = ConfigurationSettings.AppSettings["CountryCode"];
        [Route("SendSMS")]
        public IHttpActionResult SendSMS(MessageSendTwillio Model)
        {
            try
            {
                var twilio = new TwilioRestClient(AccountSid, AuthToken);
                DentistProfileBLL objdentistprofile = new DentistProfileBLL();
                if (!string.IsNullOrEmpty(Model.UserName))
                {
                    int UserId = 0;
                    UserId = objdentistprofile.GetIdOnPublicProfileURL(Model.UserName);
                    DentistProfileDetail objProfiledetials = new DentistProfileDetail();
                    objProfiledetials = objdentistprofile.GetDoctorDetials(UserId);                    
                    if (UserId > 0)
                    {
                        Message Message = new Message();
                        if (Model.IsFrom == "1")
                        {
                            string Mobile = string.Empty;
                            if (objProfiledetials.lstDoctorAddressDetails.Where(p => !string.IsNullOrWhiteSpace(p.Mobile)).Any())
                            {
                                Mobile = objProfiledetials.lstDoctorAddressDetails.Where(p => !string.IsNullOrWhiteSpace(p.Mobile)).FirstOrDefault().Mobile;
                            }
                            else
                            {
                                Mobile = objProfiledetials.lstDoctorAddressDetails.Where(p => !string.IsNullOrWhiteSpace(p.Phone)).FirstOrDefault().Phone;
                            }
                            Message = twilio.SendMessage(TwillioPhone, CountryCode + Mobile, Model.Message);
                        }
                        else if(Model.IsFrom == "2")
                        {
                            Message = twilio.SendMessage(TwillioPhone, CountryCode + Model.Phone, Model.Message);
                        }                        
                        if (!string.IsNullOrWhiteSpace(Message.Sid)) {
                            return Ok();
                        } else {
                            return NotFound();
                        }
                    } else {
                        return NotFound();
                    }
                } else {
                    return NotFound();
                }
            }
            catch (Exception Ex)
            {
                throw new HttpException(400, Ex.Message + Ex.StackTrace);
            }
        }
        [Route("SendMail")]
        public IHttpActionResult SendMail(MailSendMendrill Obj)
        {
            try
            {
                bool result = false;
                if(!string.IsNullOrWhiteSpace(Obj.Username))
                {
                    int UserId = 0;
                    DentistProfileBLL objdentistprofile = new DentistProfileBLL();
                    UserId = objdentistprofile.GetIdOnPublicProfileURL(Obj.Username);
                    if(UserId > 0)
                    {
                        VCardForDoctor ObjVcard = new VCardForDoctor();
                        ObjVcard = objdentistprofile.GetVCFfileforRLAPI(UserId);
                        string filename = ObjVcard.FName + "_" + ObjVcard.Userid + ".vcf";
                        HttpContext.Current.Session["filename"] = filename;
                        string VcardFilePath = Convert.ToString(ConfigurationManager.AppSettings["VcardFilePath"]);
                        var webClient = new WebClient();
                        byte[] imageBytes = webClient.DownloadData("https://www.recordlinc.com/DentistImages/" + ObjVcard.ImageName);
                        var _serverPath = HttpContext.Current.Server.MapPath(VcardFilePath) + "\\" + filename;
                        if (System.IO.File.Exists(_serverPath))
                        {
                            string contents = System.IO.File.ReadAllText(_serverPath);
                            contents = contents.Replace(contents, BulidVCard(ObjVcard, Obj.Username, imageBytes));
                            System.IO.File.WriteAllText(_serverPath, contents);
                        }
                        else
                        {
                            var Myfile = System.IO.File.Create(_serverPath);
                            Myfile.Close();
                            string Content = BulidVCard(ObjVcard, Obj.Username, imageBytes);
                            System.IO.File.WriteAllText(_serverPath, Content);
                        }
                        result = new DentistRatingBLL().SendMailfromIluvmyDentist(Obj, _serverPath);
                        if (result)
                            return Ok();
                        else
                            return Content(HttpStatusCode.BadRequest, "Issue on mail send time.");
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception Ex)
            {
                throw new HttpException(400, Ex.Message + Ex.StackTrace);
            }
        }

        public string BulidVCard(VCardForDoctor ObjVcard, string Username, byte[] imageBytes)
        {
            try
            {
                StringBuilder strvCardBuilder = new StringBuilder();
                strvCardBuilder.AppendLine("BEGIN:VCARD");
                strvCardBuilder.AppendLine("VERSION:2.1");
                strvCardBuilder.AppendLine("N:" + ObjVcard.LName + ";" + ObjVcard.FName);
                strvCardBuilder.AppendLine("FN:" + ObjVcard.FName + " " + ObjVcard.LName);
                strvCardBuilder.Append("ADR;HOME;PREF:;;");
                strvCardBuilder.Append(ObjVcard.Address + ";");
                strvCardBuilder.Append(ObjVcard.City + ";;");
                strvCardBuilder.AppendLine(ObjVcard.Country);
                if (!string.IsNullOrWhiteSpace(ObjVcard.Company))
                    strvCardBuilder.AppendLine("ORG:" + ObjVcard.Company);
                if (!string.IsNullOrWhiteSpace(ObjVcard.JobTitle))
                    strvCardBuilder.AppendLine("TITLE:" + ObjVcard.JobTitle);
                if (!string.IsNullOrWhiteSpace(ObjVcard.Phone))
                    strvCardBuilder.AppendLine("TEL;HOME;VOICE:" + ObjVcard.Phone);
                strvCardBuilder.AppendLine("TEL;CELL;VOICE:" + ObjVcard.Mobile);
                strvCardBuilder.AppendLine("EMAIL;type=INTERNET;type=WORK;type=pref:" + ObjVcard.Email);
                string strNote = "This dentist was a referral from a friend on www.LuvDentist.com/" + Username;
                strvCardBuilder.AppendLine("NOTE;ENCODING=QUOTED-PRINTABLE:" + strNote + " Facebook: " + ObjVcard.FacebookUrl + " Google: " +
                    ObjVcard.GoogleplusUrl + " Twitter: " + ObjVcard.TwitterUrl + " YouTube: " + ObjVcard.YoutubeUrl + " Pinterest: " + ObjVcard.PinterestUrl +
                    " Yelp: " + ObjVcard.YelpUrl + " Blog: " + ObjVcard.YelpUrl);                
                strvCardBuilder.AppendLine("URL;WORK:" + ObjVcard.WebSite);
                strvCardBuilder.AppendLine("URL;HOME:" + ObjVcard.WebSite);
                if (imageBytes != null && imageBytes.Length > 0)
                {
                    strvCardBuilder.AppendLine("PHOTO;ENCODING=BASE64;TYPE=JPEG:");
                    strvCardBuilder.AppendLine(Convert.ToBase64String(imageBytes));
                    strvCardBuilder.AppendLine(string.Empty);
                }   
                strvCardBuilder.AppendLine("END:VCARD");
                return strvCardBuilder.ToString();
            }
            catch (Exception ex)
            {                
                return "";
            }
        }
    }
}
