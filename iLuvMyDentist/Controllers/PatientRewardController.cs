﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using iLuvMyDentist.Models;
using BO.Models;
using BusinessLogicLayer;
using BO.ViewModel;
using System.Configuration;
using System.Web.Http.Description;
using iLuvMyDentist.App_Start;
using static iLuvMyDentist.App_Start.APIUtil;

namespace iLuvMyDentist.Controllers
{
    //[RoutePrefix("PatientReward/api")]

    [RoutePrefix("api/PatientReward")]

    public class PatientRewardController : ApiController
    {

        [Route("Login")]
        [ResponseType(typeof(APIResponseBase<string>))]
        [CheckPatient]
        public IHttpActionResult Login(Login Obj)
        {
            APIResponseBase<string> result = new APIResponseBase<string>();
            try
            {
                if (ModelState.IsValid)
                {
                    if (Obj.UserName == "RecordlincPatientReward" && Obj.Password == "Recordlinc#1")
                    {
                        result.StatusCode = 200;
                        result.IsSuccess = true;
                        result.Result = "Success";
                        result.Message = string.Empty;
                        return Ok(result);
                    }
                    else
                    {
                        return BadRequest("Username or Password is invalid");
                    }
                }
                else
                {
                    return BadRequest("Method called with invalid parameters.");
                }
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method will return List Of Doctor.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("ListOfDentist")]
        [ResponseType(typeof(APIResponseBase<List<DentistDetailsForPatientReward>>))]
        public HttpResponseMessage ListOfDentist(SearchDentist model)
        {
            APIResponseBase<List<DentistDetailsForPatientReward>> result = new APIResponseBase<List<DentistDetailsForPatientReward>>();

            try
            {
                if (model.Username == "RecordlincPatientReward" && model.Password == "Recordlinc#1")
                {
                    if (ModelState.IsValid)
                    {
                        Search Obj = new Search();
                        Obj.fname = !String.IsNullOrEmpty(model.Firstname) ? model.Firstname.Trim() : null;
                        Obj.ZipCode = model.zipcode;
                        Obj.LastName = !String.IsNullOrEmpty(model.Lastname) ? model.Lastname.Trim() : null;
                        Obj.PageSize = 50;
                        Obj.PageIndex = 1;
                        Obj.Miles = 20;
                        Obj.FilterBy = !String.IsNullOrEmpty(model.FilterBy) ? model.FilterBy : "A";
                        SearchProfileOfDoctor ObjOfBLL = new SearchProfileOfDoctor();
                        List<DentistDetailsForPatientReward> lst = new List<DentistDetailsForPatientReward>();
                        lst = ObjOfBLL.GetSearchResultForPatientReward(Obj);
                        if (lst.Count > 0)
                        {
                            result = new APIResponseBase<List<DentistDetailsForPatientReward>>()
                            {
                                StatusCode = 200,
                                IsSuccess = true,
                                Result = lst,
                                Message = "Success",
                            };
                            return Request.CreateResponse(HttpStatusCode.OK, result);
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.NotFound, "We are unable to find your Dentist record.");
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Username or Password is invalid");
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }



        /// <summary>
        /// This Method will return List of Patient.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("ListOfPatient")]
        [ResponseType(typeof(APIResponseBase<List<PatientDetialsForPatientreward>>))]
        [CheckPatient]
        public HttpResponseMessage ListOfPatient(PatientFilter model)
        {
            APIResponseBase<List<PatientDetialsForPatientreward>> result = new APIResponseBase<List<PatientDetialsForPatientreward>>();
            APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError;

            try
            {
                ///This Method check the Request data has Source type and based on that we identify that this patient has Solution one or Super Dentist. 
                model.sourcetype = GetRequestedData();
                if (model.sourcetype == APIFilter.FilterFlag.NotFound)
                {
                    resultError = new APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary>()
                    {
                        StatusCode = 404,
                        IsSuccess = false,
                        Result = ModelState,
                        Message = "AccountId doesn't register in any Reward Platform.",
                    };
                    return Request.CreateResponse(HttpStatusCode.BadRequest, resultError);
                }
                else
                {
                    if (model.Username == "RecordlincPatientReward" && model.Password == "Recordlinc#1")
                    {
                        List<PatientDetialsForPatientreward> lst = new List<PatientDetialsForPatientreward>();
                        PatientBLL clsPatientBLL = new PatientBLL();
                        lst = PatientBLL.SUP_GetPatientDetails(model);
                        if (lst.Count > 0)
                        {
                            result = new APIResponseBase<List<PatientDetialsForPatientreward>>()
                            {
                                StatusCode = 200,
                                IsSuccess = true,
                                Result = lst,
                                Message = "Success",
                            };
                            return Request.CreateResponse(HttpStatusCode.OK, result);
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.NotFound, "We are unable to find your patient record.");
                        }

                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "Username or Password is invalid");
                    }
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        [Route("BindPatientFilter")]
        [ResponseType(typeof(APIResponseBase<PatientFilter>))]
        public PatientFilter BindPatientFilter(SearchPatient model)
        {
            try
            {
                PatientFilter Obj = new PatientFilter();
                Obj.UserId = model.UserId;
                if (!string.IsNullOrEmpty(model.Firstname))
                {
                    Obj.FirstName = model.Firstname;
                }
                if (!string.IsNullOrEmpty(model.Lastname))
                {
                    Obj.Lastname = model.Lastname;
                }
                if (!string.IsNullOrEmpty(model.Email))
                {
                    Obj.Email = model.Email;
                }
                if (!string.IsNullOrEmpty(model.Phone))
                {
                    Obj.Phone = model.Phone;
                }
                Obj.ExcludeFamilyMembers = model.ExcludeFamilyMembers;
                Obj.PageIndex = 1;
                Obj.PageSize = 10;
                Obj.SortColumn = 1;
                Obj.SortDirection = 2;
                return Obj;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// This Method will Insert a Doctor into Recordlinc.
        /// </summary>
        /// <returns></returns>
        [Route("InsertProvider")]
        [ResponseType(typeof(APIResponseBase<RewardResponse>))]
        [HttpPost]
        [SyncFilter]
        public HttpResponseMessage InsertProvider(List<InsertMember> Obj)
        {
            try
            {
                APIResponseBase<RewardResponse> result = new APIResponseBase<RewardResponse>();
                APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError;

                if (ModelState.IsValid)
                {
                    var List = PatientRewardBLL.InsertDoctor(Obj);
                    if (List.NotInsertedList != null && List.NotInsertedList.Count == Obj.Count)
                    {
                        result = new APIResponseBase<RewardResponse>()
                        {
                            StatusCode = 400,
                            IsSuccess = false,
                            Result = List,
                            Message = "Failed",
                        };
                    }
                    else
                    {
                        result = new APIResponseBase<RewardResponse>()
                        {
                            StatusCode = 200,
                            IsSuccess = true,
                            Result = List,
                            Message = "Success",
                        };
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                {
                    resultError = new APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary>()
                    {
                        StatusCode = 500,
                        IsSuccess = false,
                        Result = ModelState,
                        Message = "Method called with invalid parameters.",
                    };
                    return Request.CreateResponse(HttpStatusCode.BadRequest, resultError);
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method will Insert a Patient into Recordlinc.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("AddPatient")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<RewardResponse>))]
        [SyncFilter]
        public IHttpActionResult InsertPatient(List<DentrixPatient> Obj)
        {
            try
            {
                APIResponseBase<RewardResponse> result = new APIResponseBase<RewardResponse>();
                if (ModelState.IsValid)
                {
                    var list = PatientRewardBLL.PatientInsertUpdateNew(Obj);
                    if (list.NotInsertedList != null && Obj.Count == list.NotInsertedList.Count)
                    {
                        return Ok(result = new APIResponseBase<RewardResponse>()
                        {
                            StatusCode = 400,
                            IsSuccess = false,
                            Result = list,
                            Message = "Failed",
                        });
                    }
                    else
                    {
                        return Ok(result = new APIResponseBase<RewardResponse>()
                        {
                            StatusCode = 200,
                            IsSuccess = true,
                            Result = list,
                            Message = "Success",
                        });
                    }

                }
                else
                {
                    return BadRequest("Method called with invalid parameters.");
                }
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.Message);
                throw;
            }
        }
        #region InsertPatient
        //public HttpResponseMessage InsertPatient(List<DentrixPatient> Obj)
        //{
        //    APIResponseBase<RewardResponse> result = new APIResponseBase<RewardResponse>();

        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            return Request.CreateResponse(HttpStatusCode.OK, PatientRewardBLL.BulkInsertPatients(Obj));                
        //        }
        //        else
        //        {
        //            return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
        //        }
        //    }
        //    catch (Exception Ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
        //        throw;
        //    }
        //}
        #endregion

        /// <summary>
        /// This Method Insert Patient Adjustment Details on Recordlinc from Dentrix.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("AddAdjustmentDetails")]
        [HttpPost]
        [ResponseType(typeof(RewardResponse))]
        [SyncFilter]
        public HttpResponseMessage InsertPatientAdjustmentDetails(List<Patient_DentrixAdjustments> Obj)
        {
            // APIResponseBase<RewardResponse> result = new APIResponseBase<RewardResponse>();

            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, PatientRewardBLL.BulkInsertAdjustment(Obj));
                    //return Ok(result = new APIResponseBase<RewardResponse>()
                    //{
                    //    StatusCode = 200,
                    //    IsSuccess = true,
                    //    Result = PatientRewardBLL.InsertPatient_DentrixAjustmentDetails(Obj),
                    //    Message = "Success",
                    //});
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method Insert Patient Insurance Payment on Recordlinc from Dentrix.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("AddInsurancePayment")]
        [HttpPost]
        [ResponseType(responseType: typeof(RewardResponse))]
        [SyncFilter]
        public HttpResponseMessage InsertPatientInsurancePayment(List<Patient_DentrixInsurancePayment> Obj)
        {
            APIResponseBase<RewardResponse> result = new APIResponseBase<RewardResponse>();

            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, PatientRewardBLL.BulkInsertOfInsurance(Obj));
                    //return Ok(PatientRewardBLL.BulkInsertOfInsurance(Obj));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method Insert Patient Standard Payment on Recordlinc from Dentrix.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("AddStandardPayment")]
        [HttpPost]
        [ResponseType(typeof(RewardResponse))]
        [SyncFilter]
        public HttpResponseMessage InsertPatientStandardPayment(List<Patient_DentrixStandardPayment> Obj)
        {
            APIResponseBase<RewardResponse> result = new APIResponseBase<RewardResponse>();

            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, PatientRewardBLL.BulkInsertOfPatientStandard(Obj));
                    //return Ok(PatientRewardBLL.BulkInsertOfInsurance(Obj));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method Insert Patient Finance Charges on Recordlinc from Dentrix.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("AddFinanceCharges")]
        [HttpPost]
        [ResponseType(typeof(RewardResponse))]
        [SyncFilter]
        public HttpResponseMessage InsertPatientFinanceCharges(List<Patient_DentrixFinanceCharges> Obj)
        {
            APIResponseBase<RewardResponse> result = new APIResponseBase<RewardResponse>();

            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, PatientRewardBLL.BulkInsertFinance(Obj));
                    //result = new APIResponseBase<RewardResponse>()
                    //{
                    //    StatusCode = 200,
                    //    IsSuccess = true,
                    //    Result = PatientRewardBLL.InsertPatient_DentrixFinanceCharges(Obj),
                    //    Message = "Success",
                    //};
                    //return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method Insert Patient Procedure on Recordlinc from Dentrix.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("AddPatientProcedure")]
        [HttpPost]
        [ResponseType(typeof(RewardResponse))]
        [SyncFilter]
        public HttpResponseMessage InsertPatientProcedure(List<Patient_DentrixProcedure> Obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, PatientRewardBLL.BulkInsertProcedures(Obj));
                    //result = new APIResponseBase<RewardResponse>()
                    //{
                    //    StatusCode = 200,
                    //    IsSuccess = true,
                    //    Result = PatientRewardBLL.InsertPatient_DentrixProcedure(Obj),
                    //    Message = "Success",
                    //};
                    //return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method used for Update Patient From Patient Reward Mobile Application.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdatePatient")]
        [ResponseType(typeof(UpdateResponse<PatientDetails>))]
        [CheckPatient]
        public HttpResponseMessage UpdatePatient(Reward_PatientUpdate Obj)
        {

            APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError1;
            APIResponseBase<DBNull> resultError2;
            try
            {
                ///This Method check the Request data has Source type and based on that we identify that this patient has Solution one or Super Dentist. 
                Obj.sourcetype = GetRequestedData();
                if (Obj.sourcetype == APIFilter.FilterFlag.NotFound)
                {
                    resultError1 = new APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary>()
                    {
                        StatusCode = 404,
                        IsSuccess = false,
                        Result = ModelState,
                        Message = "RewardPartnerID Doesn't exists on the system.",
                    };
                    return Request.CreateResponse(HttpStatusCode.BadRequest, resultError1);
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        if (Obj.Override)
                        {
                            Obj.path = Convert.ToString(ConfigurationSettings.AppSettings["imagebankpath"]);
                            // Ankit here 05-21-2018 For Sup-368
                            // This Code added on based on if Patient is Register on SuperBucks then it has PlanId
                            if (Obj.sourcetype == APIFilter.FilterFlag.IsSuperBucks)
                            {
                                //Add This condition based on Bruce comment on Skype date: 07-25-2018 07:12PM
                                //Hello Hardipsinh, is the plan id required for every call to update patient API? Could we make this so it's only required on the registration API?  Thank you for the clarification on what a plan id is.
                                //So We have change the JIRA task according to him Sup-138.
                                //The PlanId will required only on Register time.
                                if (Obj.PlanId > 0)
                                {
                                    // This condition check that PlanId passed by Mobile team has exists on System for there User
                                    // If not then return error message If yes then change the plan.
                                    if (PatientRewardBLL.CheckPlanIdExistsornot(Obj.RewardPartnerId, Obj.PlanId))
                                    {
                                        return Request.CreateResponse(HttpStatusCode.OK, PatientRewardBLL.UpdatePatientInfo(Obj));
                                    }
                                    else
                                    {
                                        resultError2 = new APIResponseBase<DBNull>()
                                        {
                                            StatusCode = 400,
                                            IsSuccess = false,
                                            Result = null,
                                            Message = "This Plan doesn't exists for this User.",
                                        };
                                        return Request.CreateResponse(HttpStatusCode.BadRequest, resultError2);
                                    }
                                }
                                else
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, PatientRewardBLL.UpdatePatientInfo(Obj));
                                }
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, PatientRewardBLL.UpdatePatientInfo(Obj));
                            }
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.NotAcceptable, "Nothing has to change.");
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                    }
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method used for Upload Patient Profile Image from Patient Reward Mobile Application.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("UploadProfileImage")]
        [ResponseType(typeof(APIResponseBase<string>))]
        [CheckPatient]
        public HttpResponseMessage UploadProfileImage(Reward_UploadProfileImage Obj)
        {
            APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError;
            APIResponseBase<string> resultError1;
            try
            {
                ///This Method check the Request data has Source type and based on that we identify that this patient has Solution one or Super Dentist. 
                Obj.sourcetype = GetRequestedData();
                if (Obj.sourcetype == APIFilter.FilterFlag.NotFound)
                {
                    resultError = new APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary>()
                    {
                        StatusCode = 404,
                        IsSuccess = false,
                        Result = ModelState,
                        Message = "RewardPartnerID Doesn't exists on the system.",
                    };
                    return Request.CreateResponse(HttpStatusCode.BadRequest, resultError);
                }
                else
                {
                    Obj.path = Convert.ToString(ConfigurationSettings.AppSettings["imagebankpath"]);
                    resultError1 = new APIResponseBase<string>()
                    {
                        StatusCode = 200,
                        IsSuccess = true,
                        Result = PatientRewardBLL.UpdatePatientImage(Obj),
                        Message = "Success",
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, resultError1);
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method will create rewards in Recordlinc for solution1.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("CreateReward")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<RewardsJson>))]
        [CheckPatient]
        public HttpResponseMessage CreateRewards(CreateRewards obj)
        {
            APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError;
            APIResponseBase<string> result2;
            APIResponseBase<RewardsJson> result1;

            try
            {
                ///This Method check the Request data has Source type and based on that we identify that this patient has Solution one or Super Dentist. 
                obj.sourcetype = GetRequestedData();
                if (obj.sourcetype == APIFilter.FilterFlag.NotFound)
                {
                    resultError = new APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary>()
                    {
                        StatusCode = 404,
                        IsSuccess = false,
                        Result = ModelState,
                        Message = "RewardPartnerID Doesn't exists on the system.",
                    };
                    return Request.CreateResponse(HttpStatusCode.BadRequest, resultError);
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        if (PatientRewardBLL.CheckRewardtype(obj))
                        {
                            if (!PatientRewardBLL.CheckRegistration(obj))
                            {
                                RewardsJson Obj = PatientRewardBLL.CreateRewardsForPatient(obj, (int)BO.Enums.Common.RewardSource.MOBILE);
                                if (Obj.RewardId == -1)
                                {
                                    result2 = new APIResponseBase<string>()
                                    {
                                        StatusCode = 400,
                                        IsSuccess = false,
                                        Result = "No Data Found.",
                                        Message = Obj.ErrorMessage,
                                    };
                                    return Request.CreateResponse(HttpStatusCode.NotFound, result2);
                                }
                                if (Obj.RewardId == -2)
                                {
                                    return Request.CreateResponse(HttpStatusCode.BadRequest, Obj);
                                }
                                result1 = new APIResponseBase<RewardsJson>()
                                {
                                    StatusCode = 200,
                                    IsSuccess = true,
                                    Result = Obj,
                                    Message = "Success",
                                };
                                return Request.CreateResponse(HttpStatusCode.OK, result1);
                            }
                            else
                            {
                                result2 = new APIResponseBase<string>()
                                {
                                    StatusCode = 401,
                                    IsSuccess = false,
                                    Result = "No Data Found.",
                                    Message = "You are already register with this Reward PlatForm Id.",
                                };
                                return Request.CreateResponse(HttpStatusCode.BadRequest, result2);
                            }
                        }
                        else
                        {
                            result2 = new APIResponseBase<string>()
                            {
                                StatusCode = 401,
                                IsSuccess = false,
                                Result = "No Data Found.",
                                Message = "We are unable to find your Reward type code.",
                            };
                            return Request.CreateResponse(HttpStatusCode.BadRequest, result2);
                        }

                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                    }
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method add Dentist Association for User
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("AddDentist")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<bool>))]
        public HttpResponseMessage AddDentist(Reward_AddDentist Obj)
        {
            APIResponseBase<bool> result;
            APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError;

            try
            {
                if (ModelState.IsValid)
                {
                    bool results = false;
                    results = PatientRewardBLL.AddDentistForReward(Obj);
                    result = new APIResponseBase<bool>()
                    {
                        StatusCode = (results) ? 200 : 500,
                        IsSuccess = true,
                        Result = results,
                        Message = (results) ? "Success" : "Failed",
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This method used for Get Dentist list for particular User.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("GetDentistsForUser")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<Reward_UserList>>))]
        public HttpResponseMessage GetDentistsForUser(Reward_DentistList Obj)
        {
            APIResponseBase<List<Reward_UserList>> result;
            APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError;

            try
            {
                if (ModelState.IsValid)
                {
                    result = new APIResponseBase<List<Reward_UserList>>()
                    {
                        StatusCode = 200,
                        IsSuccess = true,
                        Result = PatientRewardBLL.GetDentistListForUser(Obj),
                        Message = "Success",
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method Will Used for Get Rewards List. 1 = Actual Reward and 2 = Potential Reward. 
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("GetRewards")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<Reward_ListOfPotential>))]
        [CheckPatient]
        public IHttpActionResult GetPotentialRewardsDetails(Reward_RewardsCall Obj)
        {
            APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError;
            APIResponseBase<Reward_ListOfPotential> result;
            try
            {
                ///This Method check the Request data has Source type and based on that we identify that this patient has Solution one or Super Dentist. 
                Obj.sourcetype = GetRequestedData();
                if (Obj.sourcetype == APIFilter.FilterFlag.NotFound)
                {
                    resultError = new APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary>()
                    {
                        StatusCode = 404,
                        IsSuccess = false,
                        Result = ModelState,
                        Message = "RewardPartnerID Doesn't exists on the system.",
                    };
                    return BadRequest(resultError.Message.ToString());
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        Obj.RewardType = (Obj.RewardType == 1) ? 1 : 2;
                        Obj.Pageindex = (Obj.Pageindex == 0) ? 1 : Obj.Pageindex;
                        Obj.PageSize = (Obj.PageSize == 0) ? 100 : Obj.PageSize;
                        var obj = PatientRewardBLL.GetPotentialRewardList(Obj);
                        if (obj.RewardPartnerId == null)
                        {
                            return BadRequest("This RewardPartnerId doesn't exist for this AccountID");
                        }
                        result = new APIResponseBase<Reward_ListOfPotential>()
                        {
                            StatusCode = 200,
                            IsSuccess = true,
                            Result = obj,
                            Message = "Success",
                        };
                        return Ok(result);
                    }
                    else
                    {
                        resultError = new APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary>()
                        {
                            StatusCode = 500,
                            IsSuccess = false,
                            Result = ModelState,
                            Message = "Method called with invalid parameters.",
                        };
                        return BadRequest(ModelState);
                    }
                }
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method Will Used for Get Rewards Balance.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("GetRewardBalance")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<string>))]
        [CheckPatient]
        public HttpResponseMessage GetRewardBalance(RewardBalanceJson Obj)
        {
            APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError;
            APIResponseBase<string> result;
            try
            {
                ///This Method check the Request data has Source type and based on that we identify that this patient has Solution one or Super Dentist. 
                Obj.sourcetype = GetRequestedData();
                if (Obj.sourcetype == APIFilter.FilterFlag.NotFound)
                {
                    resultError = new APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary>()
                    {
                        StatusCode = 404,
                        IsSuccess = false,
                        Result = ModelState,
                        Message = "RewardPartnerID Doesn't exists on the system.",
                    };
                    return Request.CreateResponse(HttpStatusCode.BadRequest, resultError);
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        result = new APIResponseBase<string>()
                        {
                            StatusCode = 200,
                            IsSuccess = true,
                            Result = "{  TotalRewards: " + Convert.ToDecimal(PatientRewardBLL.GetRewardAmountBalance(Obj)) + "    }",
                            Message = "Success",
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                    }
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method Will Used for retrieve doctor details.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("ViewDentist")]
        [HttpGet]
        [ResponseType(typeof(APIResponseBase<List<DoctorDetails>>))]
        public HttpResponseMessage GetDentistDetails(int DoctorId)
        {
            APIResponseBase<dynamic> result;
            APIResponseBase<string> result1;

            try
            {
                if (DoctorId > 0)
                {
                    var obj = PatientRewardBLL.GetDentistDetailsById(DoctorId);
                    if (obj != null)
                    {
                        result = new APIResponseBase<dynamic>()
                        {
                            StatusCode = 200,
                            IsSuccess = true,
                            Result = obj,
                            Message = "Success",
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    else
                    {
                        result = new APIResponseBase<dynamic>()
                        {
                            StatusCode = 402,
                            IsSuccess = false,
                            Result = obj ?? 0,
                            Message = "No Dentist Record Found.",
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.Unauthorized, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method used for Insert Patient Referral Details
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [Route("PatientReferral")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<Reward>))]
        [CheckPatient]
        public HttpResponseMessage PatientReferral(PatientReferral obj)
        {
            APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError;
            APIResponseBase<Reward> result;

            try
            {
                ///This Method check the Request data has Source type and based on that we identify that this patient has Solution one or Super Dentist. 
                obj.sourcetype = GetRequestedData();
                if (obj.sourcetype == APIFilter.FilterFlag.NotFound)
                {
                    resultError = new APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary>()
                    {
                        StatusCode = 404,
                        IsSuccess = false,
                        Result = ModelState,
                        Message = "RewardPartnerID Doesn't exists on the system.",
                    };
                    return Request.CreateResponse(HttpStatusCode.BadRequest, resultError);
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        obj.Source = (int)BO.Enums.Common.RewardSource.MOBILE;
                        Reward x = PatientRewardBLL.PatientReferral(obj);
                        result = new APIResponseBase<Reward>()
                        {
                            StatusCode = (x.RewardId > 0) ? 200 : 500,
                            IsSuccess = true,
                            Result = x,
                            Message = (x.RewardId > 0) ? "Success" : "This dentist doesn't provide Referral Reward.",
                        };
                        return Request.CreateResponse((x.RewardId > 0) ? HttpStatusCode.OK : HttpStatusCode.ServiceUnavailable, result);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                    }
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method used for Association of RewardPartnerId and Recordlinc PatientId.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("PatientAssociation")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<string>))]
        [CheckPatient]
        public HttpResponseMessage ReocrdlincPMSAssociation(PatientAssociation Obj)
        {
            APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError;
            APIResponseBase<string> result1;
            APIResponseBase<bool> result2;

            try
            {
                ///This Method check the Request data has Source type and based on that we identify that this patient has Solution one or Super Dentist. 
                Obj.sourcetype = GetRequestedData();
                if (Obj.sourcetype == APIFilter.FilterFlag.NotFound)
                {
                    resultError = new APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary>()
                    {
                        StatusCode = 404,
                        IsSuccess = false,
                        Result = ModelState,
                        Message = "AccountId doesn't register in any Reward Platform.",
                    };
                    return Request.CreateResponse(HttpStatusCode.BadRequest, resultError);
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        int PatientId = PatientRewardBLL.CheckRewardPartnerAssociatedId(Obj);
                        if (PatientId > 0)
                        {
                            result1 = new APIResponseBase<string>()
                            {
                                StatusCode = 208,
                                IsSuccess = false,
                                Result = $"This PatientId: {PatientId} has already associated with another Reward PartnerId",
                                Message = "Failed",
                            };
                            return Request.CreateResponse(HttpStatusCode.MultipleChoices, result1);
                        }
                        bool x = PatientRewardBLL.RecordlincPMSAssociation(Obj);
                        result2 = new APIResponseBase<bool>()
                        {
                            StatusCode = (x) ? 200 : 503,
                            IsSuccess = (x) ? true : false,
                            Result = x,
                            Message = (x) ? "Success" : "Failed",
                        };
                        return Request.CreateResponse((x) ? HttpStatusCode.OK : HttpStatusCode.ServiceUnavailable, result2);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                    }
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method Used for Delete List of Procedure on Recordlinc side.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("DeleteProcedure")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<DeletedProc>>))]
        [SyncFilter]
        public HttpResponseMessage DeletePatientProcedure(List<RemoveProcedure> Obj)
        {
            APIResponseBase<List<DeletedProc>> result;
            APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError;

            try
            {
                if (ModelState.IsValid)
                {
                    var List = PatientRewardBLL.DeleteDentrixPatientProcedure(Obj);
                    if (List == null || List.Count == 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<DeletedProc>>()
                        {
                            StatusCode = (int)HttpStatusCode.BadRequest,
                            IsSuccess = false,
                            Result = null,
                            Message = "Invalid DentrixConnectorId",
                        });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<DeletedProc>>()
                        {
                            StatusCode = (int)HttpStatusCode.OK,
                            IsSuccess = true,
                            Result = List,
                            Message = "Success",
                        });
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method Used for Add Operatory on Recordlinc side.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("AddOperatory")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<OperatoryResponse>))]
        [SyncFilter]
        public IHttpActionResult AddOperatory(List<Operatory> Obj)
        {
            try
            {
                APIResponseBase<OperatoryResponse> result = new APIResponseBase<OperatoryResponse>();
                if (ModelState.IsValid)
                {
                    var List = AppointmentBLL.InsertOperatory(Obj);
                    if (List.NotInsertedList != null && List.NotInsertedList.Count == Obj.Count)
                    {
                        return Ok(result = new APIResponseBase<OperatoryResponse>()
                        {
                            StatusCode = (int)HttpStatusCode.BadRequest,
                            IsSuccess = false,
                            Result = List,
                            Message = "Failed",
                        });
                    }
                    else
                    {
                        return Ok(result = new APIResponseBase<OperatoryResponse>()
                        {
                            StatusCode = 200,
                            IsSuccess = true,
                            Result = List,
                            Message = "Success",
                        });
                    }
                }
                else
                {
                    return BadRequest("Method called with invalid parameters.");
                }
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.Message);
                throw;
            }
        }





        /// <summary>
        /// This Method Delete list of Operatory on Recordlinc side.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("DeleteOperatory")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<DeletedOperatory>>))]
        [SyncFilter]
        public HttpResponseMessage DeleteOperatory(List<RemoveOperatory> Obj)
        {
            APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError;

            try
            {
                if (ModelState.IsValid)
                {
                    var List = AppointmentBLL.DeleteOperatory(Obj);
                    if (List == null || List.Count == 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<DeletedOperatory>>()
                        {
                            StatusCode = (int)HttpStatusCode.BadRequest,
                            IsSuccess = false,
                            Result = null,
                            Message = "Invalid DentrixConnecoterKey",
                        });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<DeletedOperatory>>()
                        {
                            StatusCode = (int)HttpStatusCode.OK,
                            IsSuccess = true,
                            Result = List,
                            Message = "Success",
                        });
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method use to retrieve Dentrix Sync Details. 
        /// </summary>
        /// <param name="DentrixConnectorID"></param>
        /// <returns></returns>
        [Route("DentrixSyncDetail")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<System.Data.DataTable>))]
        public HttpResponseMessage DentrixSyncDetail(string DentrixConnectorID)
        {
            APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError;
            try
            {
                if (ModelState.IsValid)
                {
                    var obj = AppointmentBLL.GetDentrixSyncDetail(DentrixConnectorID);
                    if (obj != null && obj.Rows.Count > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<System.Data.DataTable>()
                        {
                            StatusCode = (int)HttpStatusCode.OK,
                            IsSuccess = true,
                            Result = obj,
                            Message = "Success",
                        });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<System.Data.DataTable>()
                        {
                            StatusCode = (int)HttpStatusCode.BadRequest,
                            IsSuccess = false,
                            Result = null,
                            Message = "Invalid DentrixConnectorId",
                        });
                    }

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method Used for getting Patient Details using Solution one PatientId.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("GetPatient")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<PateintSolOneDetailsModel>))]
        [CheckPatient]
        public HttpResponseMessage GetPatientBySolutionOne(PatientSolOne Obj)
        {
            APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError;
            APIResponseBase<PateintSolOneDetailsModel> result;

            try
            {
                Obj.sourcetype = GetRequestedData();
                if (Obj.sourcetype == APIFilter.FilterFlag.NotFound)
                {
                    resultError = new APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary>()
                    {
                        StatusCode = 404,
                        IsSuccess = false,
                        Result = ModelState,
                        Message = "RewardPartnerID Doesn't exists on the system.",
                    };
                    return Request.CreateResponse(HttpStatusCode.BadRequest, resultError);
                }
                else
                {
                    if (ModelState.IsValid)
                    {
                        var obj = PatientBLL.GetPatientBySolutionOne(Obj);
                        if (obj != null)
                        {
                            result = new APIResponseBase<PateintSolOneDetailsModel>()
                            {
                                StatusCode = 200,
                                IsSuccess = true,
                                Result = obj,
                                Message = "Success",
                            };
                            return Request.CreateResponse(HttpStatusCode.OK, result);
                        }
                        else
                        {
                            resultError = new APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary>()
                            {
                                StatusCode = 400,
                                IsSuccess = false,
                                Result = ModelState,
                                Message = "Invalid RewardPartner ID.",
                            };
                            return Request.CreateResponse(HttpStatusCode.BadRequest, resultError);
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                    }
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method Insert Patient Appointment on Recordlinc from Dentrix.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("AddAppointments")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<RewardResponse>))]
        [SyncFilter]
        #region OLDInsertAppointments
        //public HttpResponseMessage InsertAppointments(List<Appointments> Obj)
        //{
        //    APIResponseBase<RewardResponse> result;
        //    APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError;
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            var List = PatientRewardBLL.InsertAppointmentsPatient(Obj);
        //            if (List.NotInsertedList != null && List.NotInsertedList.Count == Obj.Count)
        //            {
        //                result = new APIResponseBase<RewardResponse>()
        //                {
        //                    StatusCode = 400,
        //                    IsSuccess = false,
        //                    Result = List,
        //                    Message = "Failed",
        //                };
        //            }
        //            else
        //            {
        //                result = new APIResponseBase<RewardResponse>()
        //                {
        //                    StatusCode = 200,
        //                    IsSuccess = true,
        //                    Result = List,
        //                    Message = "Success",
        //                };
        //            }

        //            return Request.CreateResponse(HttpStatusCode.OK, result);
        //        }
        //        else
        //        {
        //            return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
        //        }
        //    }
        //    catch (Exception Ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
        //        throw;
        //    }
        //}
        #endregion
        public HttpResponseMessage InsertAppointments(List<Appointments> Obj)
        {
            //  APIResponseBase<RewardResponse> result = new APIResponseBase<RewardResponse>();
            try
            {
                APIResponseBase<RewardResponse> result;
                if (ModelState.IsValid)
                {
                    var List = PatientRewardBLL.BulkInsertAppointments_DentrixDetails(Obj);
                    if (List.NotInsertedList != null && List.NotInsertedList.Count == Obj.Count)
                    {
                        result = new APIResponseBase<RewardResponse>()
                        {
                            StatusCode = (int)HttpStatusCode.BadRequest,
                            IsSuccess = false,
                            Result = List,
                            Message = "Failed",
                        };
                    }
                    else
                    {
                        result = new APIResponseBase<RewardResponse>()
                        {
                            StatusCode = (int)HttpStatusCode.OK,
                            IsSuccess = true,
                            Result = List,
                            Message = "Success",
                        };
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, result);

                    //result = new APIResponseBase<RewardResponse>()
                    //{
                    //    StatusCode = 200,
                    //    IsSuccess = true,
                    //    Result = PatientRewardBLL.InsertPatient_DentrixFinanceCharges(Obj),
                    //    Message = "Success",
                    //};
                    //return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method Used for Delete Appointments From Dentrix.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("DeleteAppointments")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<DeletedRecord>>))]
        [SyncFilter]
        public HttpResponseMessage DeleteAppointments(List<RemoveAppointment> Obj)
        {
            APIResponseBase<List<DeletedRecord>> result;
            APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError;

            try
            {
                if (ModelState.IsValid)
                {
                    var List = PatientRewardBLL.DeletePatientAppointment(Obj);
                    if (List == null || List.Count == 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<DeletedRecord>>()
                        {
                            StatusCode = (int)HttpStatusCode.BadRequest,
                            IsSuccess = false,
                            Result = null,
                            Message = "Invalid DentrixConnectorId",
                        });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<DeletedRecord>>()
                        {
                            StatusCode = (int)HttpStatusCode.OK,
                            IsSuccess = true,
                            Result = List,
                            Message = "Success",
                        });
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Methods Used to delete Patient From Dentrix Side.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("DeletePatients")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<DeletePatient>>))]
        [SyncFilter]
        public HttpResponseMessage DeletePatients(List<RemovePatient> Obj)
        {
            APIResponseBase<List<DeletePatient>> result;
            APIResponseBase<System.Web.Http.ModelBinding.ModelStateDictionary> resultError;

            try
            {
                if (ModelState.IsValid)
                {
                    var List = PatientRewardBLL.DeletePatientFromDentrix(Obj);

                    if (List == null || List.Count == 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<DeletePatient>>()
                        {
                            StatusCode = (int)HttpStatusCode.BadRequest,
                            IsSuccess = false,
                            Result = null,
                            Message = "Invalid dentrixConnectorId",
                        });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<DeletePatient>>()
                        {
                            StatusCode = (int)HttpStatusCode.OK,
                            IsSuccess = true,
                            Result = List,
                            Message = "Success",
                        });
                    }
                    // return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method used for Getting Patient form the Recordlinc side and send it to Dentrix.
        /// </summary>
        /// <param name="DentrixConnectorId"></param>
        /// <returns></returns>
        [Route("RetrievePatient")]
        [HttpPost]
        [ResponseType(typeof(RetriveResponse<List<DentrixPatient>>))]
        [SyncFilter]
        public HttpResponseMessage GetPatientList(RetrievePatient Obj)
        {
            APIResponseBase<DBNull> result;

            try
            {
                if (!string.IsNullOrWhiteSpace(Obj.PMSConnectorId))
                {
                    return Request.CreateResponse(HttpStatusCode.OK, PatientRewardBLL.GetPatientList(Obj));
                }
                else
                {
                    result = new APIResponseBase<DBNull>()
                    {
                        StatusCode = 400,
                        IsSuccess = false,
                        Result = null,
                        Message = "PMSConnectorId is required.",
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method Get Response from Dentrix side and do updation on database.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("PMSPatientResponse")]
        [HttpPost]
        [ResponseType(typeof(RetriveResponse<bool>))]
        [SyncFilter]
        public HttpResponseMessage ResponseStatus(DPMS_Response Obj)
        {
            APIResponseBase<object> result;

            try
            {
                if (!string.IsNullOrWhiteSpace(Obj.PMSConnectorId))
                {
                    var response = PatientRewardBLL.CheckPatientResponseData(Obj);
                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }
                else
                {
                    result = new APIResponseBase<object>()
                    {
                        StatusCode = 400,
                        IsSuccess = false,
                        Result = null,
                        Message = "DentrixConnectorId is required.",
                    };
                    return Request.CreateResponse(HttpStatusCode.BadRequest, result);
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method Used for send Appointment related data to PMS.
        /// </summary>
        /// <returns></returns>
        [Route("RetrieveAppointment")]
        [ResponseType(typeof(APIResponseBase<object>))]
        [HttpPost]
        [SyncFilter]
        public HttpResponseMessage RetrieveAppointment(RetrieveAppointment Obj)
        {
            APIResponseBase<object> result;

            try
            {
                if (!string.IsNullOrWhiteSpace(Obj.PMSConnectorId))
                {
                    return Request.CreateResponse(HttpStatusCode.OK, /*PatientRewardBLL.GetAppointmentData(Obj)*/true);
                }
                else
                {
                    result = new APIResponseBase<object>()
                    {
                        StatusCode = 400,
                        IsSuccess = false,
                        Result = null,
                        Message = "PMSConnectorId is required.",
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method check the Request data and based on that we will return ActionFIlter Flag of Solution one and SuperBucks. 
        /// </summary>
        /// <returns></returns>
        [Route("GetRequestedData")]
        public APIFilter.FilterFlag GetRequestedData()
        {
            try
            {
                object IsSuperBucks = null;
                int myObject = 0;
                if (Request.Properties.TryGetValue("sourceType", out IsSuperBucks))
                {
                    myObject = (int)IsSuperBucks;
                }
                if (myObject > 0)
                {
                    if (myObject == 1)
                    {
                        return APIFilter.FilterFlag.IsRewardPlatform;
                    }
                    else if (myObject == 2)
                    {
                        return APIFilter.FilterFlag.IsSuperBucks;
                    }
                    else
                    {
                        return APIFilter.FilterFlag.NotFound;
                    }
                }
                else
                {
                    return APIFilter.FilterFlag.NotFound;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// This Method Used for Get All Dentist list which is Associate with an Organization(Account)
        /// </summary>
        /// <returns></returns>
        [Route("GetDentistForOrganization")]
        [HttpPost]
        [ResponseType(typeof(OrganizationResponse<List<DoctorDetails>>))]
        public HttpResponseMessage GetListOfDentistForOrganization(Account Obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, PatientRewardBLL.GetDentistListOfAccount(Obj));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }


        /// <summary>
        /// This method used for Get Plan list for particular User.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("GetPlansForUser")]
        [HttpPost]
        [ResponseType(typeof(OrganizationResponse<List<Plan>>))]
        public HttpResponseMessage GetPlanListForUser(Account Obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, PatientRewardBLL.GetPlanListForUser(Obj.AccountId));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This method use for inserting the guarantor account aging details on RL database.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("AddPatientAccountAgingDetails")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<RewardResponse>))]
        [SyncFilter]
        public HttpResponseMessage InsertPatientDentrixAccountAgingDetails(List<Patient_DentrixAccountAgingModel> Obj)
        {
            try
            {
                APIResponseBase<RewardResponse> result;
                if (ModelState.IsValid)
                {
                    // return Request.CreateResponse(HttpStatusCode.OK, PatientRewardBLL.BulkInsertPatientDentrixAccountAging(Obj));
                    //result = new APIResponseBase<RewardResponse>()
                    //{
                    //    StatusCode = 200,
                    //    IsSuccess = true,
                    //    Result = PatientRewardBLL.InsertPatient_DentrixProcedure(Obj),
                    //    Message = "Success",
                    //};
                    //return Request.CreateResponse(HttpStatusCode.OK, result);

                    var List = PatientRewardBLL.BulkInsertPatientDentrixAccountAging(Obj);
                    if (List.NotInsertedList != null && List.NotInsertedList.Count == Obj.Count)
                    {
                        result = new APIResponseBase<RewardResponse>()
                        {
                            StatusCode = (int)HttpStatusCode.BadRequest,
                            IsSuccess = false,
                            Result = List,
                            Message = "Failed",
                        };
                    }
                    else
                    {
                        result = new APIResponseBase<RewardResponse>()
                        {
                            StatusCode = (int)HttpStatusCode.OK,
                            IsSuccess = true,
                            Result = List,
                            Message = "Success",
                        };
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, result);

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }


        /// <summary>
        /// This is testing method for insert bulk record on procedure.
        /// </summary>
        /// <param name="patient_DentrixProcedures"></param>
        /// <returns></returns>
        [ResponseType(typeof(APIResponseBase<BulkResponse>))]
        [Route("AddProcedure")]
        [HttpPost]
        [SyncFilter]
        public HttpResponseMessage BulkInsertProcedure(List<Patient_DentrixProcedure> patient_DentrixProcedures)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, PatientRewardBLL.BulkInsertProcedures(patient_DentrixProcedures));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }
        /// <summary>
        /// This is used to re-dim rewards used specially for SuperDentist.
        /// </summary>
        /// <param name="model">Type of <see cref="RedimRewardsRequestModel"/></param>
        /// <returns></returns>
        [ResponseType(typeof(RedimRewardsResponseModel<bool>))]
        [Route("RedimRewards")]
        [HttpPost]
        public HttpResponseMessage RedimRewards(RedimRewardsRequestModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, PatientRewardBLL.RedimRewards(model));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new RedimRewardsResponseModel<List<APIUtil.KeyMessage>>()
                    {
                        IsSuccess = false,
                        Message = "Request validation error",
                        Result = APIUtil.GetModelstateErrors(ModelState),
                        StatusCode = (int)HttpStatusCode.BadRequest,
                        TransactionId = model.TransactionId
                    });
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new RedimRewardsResponseModel<bool>()
                {
                    IsSuccess = false,
                    Message = Ex.Message,
                    Result = false,
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    TransactionId = model.TransactionId
                });
                throw;
            }
        }

        /// <summary>
        /// This Method will save insurance carrier data from outside systems.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("AddInsuranceCarrier")]
        [ResponseType(typeof(APIResponseBase<bool>))]
        [SyncFilter]
        public HttpResponseMessage AddInsuranceCarrier(List<InsuranceCarrier> model)
        {

            //return Request.CreateResponse(HttpStatusCode.OK, PatientInsuranceBLL.UploadCarrier(model));
            return Request.CreateResponse(HttpStatusCode.OK, PatientInsuranceBLL.NewUploadCarrier(model));
        }

        /// <summary>
        /// This Method will save insured records from outside systems.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("AddInsuredRecord")]
        [ResponseType(typeof(APIResponseBase<bool>))]
        [SyncFilter]
        public HttpResponseMessage AddInsuredRecord(List<InsuredRecord> model)
        {
            //return Request.CreateResponse(HttpStatusCode.OK, PatientInsuranceBLL.UploadInsuranceRecord(model));
            return Request.CreateResponse(HttpStatusCode.OK, PatientInsuranceBLL.NewUploadInsuranceRecord(model));
        }

        /// <summary>
        /// This Method will return List of Patient Insurance.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("ListOfPatientInsurance")]
        [ResponseType(typeof(APIResponseBase<List<DataAccessLayer.PatientsData.Schema.InsuredRecordSchema>>))]
        [CheckPatient]
        public HttpResponseMessage ListOfPatientInsurance(PatientInsuranceFilter model)
        {
            APIResponseBase<List<DataAccessLayer.PatientsData.Schema.InsuredRecordSchema>> result = new APIResponseBase<List<DataAccessLayer.PatientsData.Schema.InsuredRecordSchema>>();

            if (model.Username == "RecordlincPatientReward" && model.Password == "Recordlinc#1")
            {
                List<DataAccessLayer.PatientsData.Schema.InsuredRecordSchema> lst = new List<DataAccessLayer.PatientsData.Schema.InsuredRecordSchema>();
                PatientInsuranceBLL clsPatientInsuranceBLL = new PatientInsuranceBLL();
                lst = clsPatientInsuranceBLL.GetPatientInsuranceDetail(model);
                if (lst.Count > 0)
                {
                    result = new APIResponseBase<List<DataAccessLayer.PatientsData.Schema.InsuredRecordSchema>>()
                    {
                        StatusCode = 200,
                        IsSuccess = true,
                        Result = lst,
                        Message = "Success",
                    };
                    return Request.CreateResponse(HttpStatusCode.OK, result);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "We are unable to find your patient insurance record.");
                }

            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Username or Password is invalid");
            }


        }

        /// <summary>
        /// Appointment writeback for PMS
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns>booked appointment list</returns>
        [Route("AppointmentWriteback")]
        [ResponseType(typeof(APIResponseBase<List<AppointmentsWriteback>>))]
        [HttpPost]
        [SyncFilter]
        public HttpResponseMessage AppointmentWriteback(WritebackAppointment Obj)
        {
            APIResponseBase<List<AppointmentsWriteback>> result = new APIResponseBase<List<AppointmentsWriteback>>();
            try
            {
                if (ModelState.IsValid)
                {
                    if (Obj.Startdate != DateTime.MinValue &&
                        Obj.Enddate != DateTime.MinValue &&
                        Obj.Startdate != null &&
                        Obj.Enddate != null)
                    {
                        if (Convert.ToDateTime(Obj.Enddate).Hour == 0 &&
                        Convert.ToDateTime(Obj.Enddate).Minute == 0)
                            Obj.Enddate = Convert.ToDateTime(Obj.Enddate).AddHours(23).AddMinutes(59);
                    }

                    var List = AppointmentBLL.AppointmentWriteback(Obj);
                    if (List != null && List.Count > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<AppointmentsWriteback>>()
                        {
                            StatusCode = 200,
                            IsSuccess = true,
                            Result = List,
                            Message = "Success",
                        });

                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<AppointmentsWriteback>>()
                        {
                            StatusCode = 200,
                            IsSuccess = true,
                            Result = null,
                            Message = "Appointment not found.",
                        });
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<List<KeyMessage>>()
                    {
                        StatusCode = (int)HttpStatusCode.BadRequest,
                        IsSuccess = false,
                        Result = APIUtil.GetModelstateErrors(ModelState),
                        Message = "Method called with invalid parameters.",
                    });
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<List<DoctorTeamMemberList>>()
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    IsSuccess = false,
                    Result = null,
                    Message = ex.Message,
                });
            }
        }

        /// <summary>
        /// Appointment writeback response
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("PMSAppointmentWritebackResponse")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<DMPSAppointmentResponsedData>>))]
        [SyncFilter]
        public HttpResponseMessage PMSAppointmentWritebackResponse(AppointmentResponsed Obj)
        {
            List<DMPSAppointmentResponsedData> result = new List<DMPSAppointmentResponsedData>();
            try
            {
                if (Obj.Result != null && Obj.Result.Count > 0)
                {
                    result = AppointmentBLL.PMSAppointmentWritebackResponse(Obj);
                    return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<DMPSAppointmentResponsedData>>()
                    {
                        StatusCode = 200,
                        IsSuccess = true,
                        Result = result,
                        Message = "Success",
                    });

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<Appointments>>()
                    {
                        StatusCode = 200,
                        IsSuccess = true,
                        Result = null,
                        Message = "Record not found.",
                    });
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<List<DoctorTeamMemberList>>()
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    IsSuccess = false,
                    Result = null,
                    Message = ex.Message,
                });
            }

        }

        /// <summary>
        /// Add Provider Schedule
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns></returns>
        [Route("AddProviderSchedule")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<RewardResponse>>))]
        [SyncFilter]
        public HttpResponseMessage AddProviderSchedule(List<AddSchedule> schedule)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<RewardResponse>()
                    {
                        StatusCode = (int)HttpStatusCode.OK,
                        IsSuccess = true,
                        Result = PatientRewardBLL.BulkInsertProviderSchedule(schedule),
                        Message = "Success"
                    });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<List<KeyMessage>>()
                    {
                        StatusCode = (int)HttpStatusCode.BadRequest,
                        IsSuccess = false,
                        Result = GetModelstateErrors(ModelState),
                        Message = "Failed"
                    });
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<RewardResponse>()
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    IsSuccess = false,
                    Result = null,
                    Message = Ex.Message,
                });
            }
        }
        /// <summary>
        /// Add Operatory Schedule
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns></returns>
        [Route("AddOperatorySchedule")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<RewardResponse>>))]
        [SyncFilter]
        public HttpResponseMessage AddOperatorySchedule(List<AddSchedule> schedule)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<RewardResponse>()
                    {
                        StatusCode = (int)HttpStatusCode.OK,
                        IsSuccess = true,
                        Result = PatientRewardBLL.BulkInsertOperatorySchedule(schedule),
                        Message = "Success"
                    });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<List<KeyMessage>>()
                    {
                        StatusCode = (int)HttpStatusCode.BadRequest,
                        IsSuccess = false,
                        Result = GetModelstateErrors(ModelState),
                        Message = "Failed"
                    });
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<RewardResponse>()
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    IsSuccess = false,
                    Result = null,
                    Message = Ex.Message,
                });
            }
        }
        /// <summary>
        /// Add Practice Schedule
        /// </summary>
        /// <param name="schedule"></param>
        /// <returns></returns>
        [Route("AddPracticeSchedule")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<RewardResponse>>))]
        [SyncFilter]
        public HttpResponseMessage AddPracticeSchedule(List<AddSchedule> schedule)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<RewardResponse>()
                    {
                        StatusCode = (int)HttpStatusCode.OK,
                        IsSuccess = true,
                        Result = PatientRewardBLL.BulkInsertPracticeSchedule(schedule),
                        Message = "Success"
                    });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<List<KeyMessage>>()
                    {
                        StatusCode = (int)HttpStatusCode.BadRequest,
                        IsSuccess = false,
                        Result = GetModelstateErrors(ModelState),
                        Message = "Failed"
                    });
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<RewardResponse>()
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    IsSuccess = false,
                    Result = null,
                    Message = Ex.Message,
                });
            }
        }
        /// <summary>
        /// This Method Used for Delete List of Provider Schedule on Recordlinc side.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("DeleteProviderSchedule")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<DeletedRecord>>))]
        [SyncFilter]
        public HttpResponseMessage DeleteProviderSchedule(List<RemoveProcedure> Obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var List = PatientRewardBLL.DeleteProviderSchedule(Obj);
                    if (List == null || List.Count == 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<DeletedRecord>>()
                        {
                            StatusCode = (int)HttpStatusCode.BadRequest,
                            IsSuccess = false,
                            Result = null,
                            Message = "Invalid DentrixConnectorId",
                        });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<DeletedRecord>>()
                        {
                            StatusCode = (int)HttpStatusCode.OK,
                            IsSuccess = true,
                            Result = List,
                            Message = "Success",
                        });
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<List<KeyMessage>>()
                    {
                        StatusCode = (int)HttpStatusCode.BadRequest,
                        IsSuccess = false,
                        Result = GetModelstateErrors(ModelState),
                        Message = "Failed"
                    });
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<RewardResponse>()
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    IsSuccess = false,
                    Result = null,
                    Message = Ex.Message,
                });
            }
        }
        /// <summary>
        /// This Method Used for Delete List of Practice Schedule on Recordlinc side.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("DeletePracticeSchedule")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<DeletedRecord>>))]
        [SyncFilter]
        public HttpResponseMessage DeletePracticeSchedule(List<RemoveProcedure> Obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var List = PatientRewardBLL.DeletePracticeSchedule(Obj);
                    if (List == null || List.Count == 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<DeletedRecord>>()
                        {
                            StatusCode = (int)HttpStatusCode.BadRequest,
                            IsSuccess = false,
                            Result = null,
                            Message = "Invalid DentrixConnectorId",
                        });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<DeletedRecord>>()
                        {
                            StatusCode = (int)HttpStatusCode.OK,
                            IsSuccess = true,
                            Result = List,
                            Message = "Success",
                        });
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<List<KeyMessage>>()
                    {
                        StatusCode = (int)HttpStatusCode.BadRequest,
                        IsSuccess = false,
                        Result = GetModelstateErrors(ModelState),
                        Message = "Failed"
                    });
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<RewardResponse>()
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    IsSuccess = false,
                    Result = null,
                    Message = Ex.Message,
                });
            }
        }
        /// <summary>
        /// This Method Used for Delete List of Operatory Schedule on Recordlinc side.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [Route("DeleteOperatorySchedule")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<DeletedRecord>>))]
        [SyncFilter]
        public HttpResponseMessage DeleteOperatorySchedule(List<RemoveProcedure> Obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var List = PatientRewardBLL.DeleteOperatorySchedule(Obj);
                    if (List == null || List.Count == 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<DeletedRecord>>()
                        {
                            StatusCode = (int)HttpStatusCode.BadRequest,
                            IsSuccess = false,
                            Result = null,
                            Message = "Invalid DentrixConnectorId",
                        });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<DeletedRecord>>()
                        {
                            StatusCode = (int)HttpStatusCode.OK,
                            IsSuccess = true,
                            Result = List,
                            Message = "Success",
                        });
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<List<KeyMessage>>()
                    {
                        StatusCode = (int)HttpStatusCode.BadRequest,
                        IsSuccess = false,
                        Result = GetModelstateErrors(ModelState),
                        Message = "Failed"
                    });
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<RewardResponse>()
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    IsSuccess = false,
                    Result = null,
                    Message = Ex.Message,
                });
            }
        }
    }
}