﻿using BO.ViewModel;
using BusinessLogicLayer;
using iLuvMyDentist.Filters;
using System;
using System.Web.Http.Description;
using static BO.Enums.Common;
using static iLuvMyDentist.App_Start.APIUtil;
using System.Web.Http;
using System.Collections.Generic;
using System.Drawing;
using System.Net.Http;
using System.Net;

namespace iLuvMyDentist.Controllers
{
    /// <summary>
    /// To get details about inbox 
    /// </summary>
    [RoutePrefix("api/OCRInbox")]
    [OCRCustomToken]
    public class OCRInboxController : ApiController
    {
        private static string NewTempFileUpload = System.Configuration.ConfigurationManager.AppSettings["TempFileUpload"];
        private static string ImageBankFileUpload = System.Configuration.ConfigurationManager.AppSettings["ImageBankFileUpload"];
        private static string ThumbFileUpload = System.Configuration.ConfigurationManager.AppSettings["ThumbFileUpload"];
        private static string DraftFileUpload = System.Configuration.ConfigurationManager.AppSettings["DraftFileUpload"];
        
        // GET: OCRInbox

        /// <summary>
        /// Give requrired details to compose message
        /// </summary>
        
        /// <param name="request">type of <see cref="ComposeMessageRequest"/> </param>
        /// <returns>Type of <see cref="ComposeMessage"/></returns>
        [ResponseType(typeof(ComposeMessage))]
        [Route("Compose")]
        [HttpPost]
        public IHttpActionResult Compose(ComposeMessageRequest request)
        {
            CustomAuthenticationIdentity CurrentUser = GetCurrentUser();
            ComposeMessage objcomposedetails = null;
            switch (request.MessageTypeId)
            {
                case ComposeMessageType.Replay:
                    //ViewBag.Title = "Reply";
                    MessagesBLL Message = new MessagesBLL();
                    objcomposedetails = Message.GetCommopseMessgeDetails(request.MessageId, (int)ComposeMessageType.Replay, CurrentUser.UserId, request.Issent);
                    break;
            }
            return Ok(objcomposedetails);
        }

        /// <summary>
        /// Gives list of colleagues to bind inside select2 while sending message
        /// </summary>
        
        /// <param name="SearchText">text to be search for.</param>
        /// <returns></returns>
        [ResponseType(typeof(List<ColleagueData>))]
        [Route("GetColleagueList")]
        [HttpGet]
        public IHttpActionResult GetColleagueList(string SearchText)
        {
            CustomAuthenticationIdentity CurrentUser = GetCurrentUser();
            MessagesBLL Message = new MessagesBLL();
            List<ColleagueData> objcolleagueList = new List<ColleagueData>();
            objcolleagueList = Message.GetColleagueList(CurrentUser.UserId, SearchText);
            return Ok(objcolleagueList);
        }

        /// <summary>
        /// Gives list of patients to bind inside select2 while sending message
        /// </summary>
        /// <param name="SearchText">text to be search for.</param>
        /// <returns></returns>
        [ResponseType(typeof(List<PatientsData>))]
        [Route("GetPatientList")]
        [HttpGet]
        public IHttpActionResult GetPatientList(string SearchText)
        {
            MessagesBLL Message = new MessagesBLL();
            CustomAuthenticationIdentity CurrentUser = GetCurrentUser();
            List<PatientsData> objpatientList = new List<PatientsData>();
            objpatientList = Message.GetPatientList(CurrentUser.UserId, SearchText);
            return Ok(objpatientList);
        }

        [ResponseType(typeof(bool))]
        [Route("SaveAsDraft")]
        [HttpPost]
        public IHttpActionResult SaveAsDraft(ComposeMessage objcomposedetail)
        {
            CustomAuthenticationIdentity CurrentUser = GetCurrentUser();
            MessagesBLL objmessagebll = new MessagesBLL();
            CommonBLL ObjCommonbll = new CommonBLL();
            int RecordId = 0;
            objcomposedetail.ComposeType = (int)ComposeType.ColleagueCompose;
            RecordId = objmessagebll.InsertUpdateDraftMessage(objcomposedetail, CurrentUser.UserId);
            List<TempFileAttacheMents> objlistofattachements = new List<TempFileAttacheMents>();
            objlistofattachements = ObjCommonbll.GetTempAttachments(objcomposedetail.FileIds);
            foreach (var item in objlistofattachements)
            {
                if (System.IO.File.Exists(NewTempFileUpload + item.FilePath))
                {
                    System.IO.File.Move(NewTempFileUpload + item.FilePath, DraftFileUpload + item.FilePath);
                    objmessagebll.InsertDraftAttachments(RecordId, item.FilePath, CurrentUser.UserId, Convert.ToInt32(item.FileExtension), (int)ComposeType.ColleagueCompose);
                }

            }
            ObjCommonbll.DeleteAllAttachements(objcomposedetail.FileIds);
            return Ok(RecordId > 0 ? true : false);
        }

        [ResponseType(typeof(bool))]
        [Route("SendMessageToColleagues")]
        [HttpPost]
        public IHttpActionResult SendMessageToColleagues(ComposeMessage objcomposedetail)
        {
            CustomAuthenticationIdentity CurrentUser = GetCurrentUser();

            return Ok(new MessagesBLL().SendMessageToColleagues(new SendMessageToColleaguesRequest()
            {
                UserId = CurrentUser.UserId,
                DraftFileUpload = DraftFileUpload,
                ImageBankFileUpload = ImageBankFileUpload,
                NewTempFileUpload = NewTempFileUpload,
                objcomposedetail = objcomposedetail,
                ThumbFileUpload = ThumbFileUpload
            }));
        }

        [ResponseType(typeof(ComposeMessage))]
        [Route("Forward")]
        [HttpPost]
        public IHttpActionResult Forward(ComposeMessage objcomposedetails)
        {
            MessagesBLL Message = new MessagesBLL();
            CustomAuthenticationIdentity CurrentUser = GetCurrentUser();
            objcomposedetails = Message.GetCommopseMessgeDetails(objcomposedetails.MessageId, (int)ComposeMessageType.Forward, CurrentUser.UserId);
            return Ok(objcomposedetails);
        }

        [ResponseType(typeof(bool))]
        [Route("ForwardSaveAsDraft")]
        [HttpPost]
        public IHttpActionResult ForwardSaveAsDraft(ComposeMessage objcomposedetail)
        {
            CustomAuthenticationIdentity CurrentUser = GetCurrentUser();
            return Ok(new MessagesBLL().ForwardSaveAsDraft(new SendMessageToColleaguesRequest()
            {
                UserId = CurrentUser.UserId,
                DraftFileUpload = DraftFileUpload,
                ImageBankFileUpload = ImageBankFileUpload,
                NewTempFileUpload = NewTempFileUpload,
                objcomposedetail = objcomposedetail,
                ThumbFileUpload = ThumbFileUpload
            }));
        }

        [ResponseType(typeof(bool))]
        [Route("ForwardMessageToColleagues")]
        [HttpPost]
        public IHttpActionResult ForwardMessageToColleagues(ComposeMessage objcomposedetail)
        {
            CustomAuthenticationIdentity CurrentUser = GetCurrentUser();


            return Ok(new MessagesBLL().ForwardMessageToColleagues(new SendMessageToColleaguesRequest()
            {
                UserId = CurrentUser.UserId,
                DraftFileUpload = DraftFileUpload,
                ImageBankFileUpload = ImageBankFileUpload,
                NewTempFileUpload = NewTempFileUpload,
                objcomposedetail = objcomposedetail,
                ThumbFileUpload  = ThumbFileUpload
            }));
        }

        /// <summary>
        /// This Method used for Download referral details as PDF.
        /// </summary>
        /// <param name="pDFFilter">Filter object</param>
        /// <returns></returns>
        [ResponseType(typeof(DownloadPDF))]
        [Route("DownloadPDF")]
        [HttpPost]
        public HttpResponseMessage DownloadPDF(DownloadPDFFilter pDFFilter)
        {
            try
            {
                pDFFilter.UserId = GetCurrentUser().UserId;
                return Request.CreateResponse(HttpStatusCode.OK,MessagesBLL.GetReferralMessageDetailsForDownloadPDF(pDFFilter));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
    }
}