﻿using BO.ViewModel;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace iLuvMyDentist.Controllers
{
    public class BaseController : ApiController
    {
        protected HttpResponseMessage provideResponse<T>(T result, int statusCode = 200, bool isSuccess = true, string Message = "Success")
        {
            return Request.CreateResponse((HttpStatusCode) statusCode, new APIResponseBase<T>()
            {
                Result = result,
                StatusCode = statusCode,
                IsSuccess = isSuccess,
                Message = Message
            });
        }
    }
}
