﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iLuvMyDentist.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";
            return View();
        }
        public ActionResult TestPage()
        {
            List<BO.ViewModel.DentistDetailsForPatientReward> lst = new List<BO.ViewModel.DentistDetailsForPatientReward>();
            return View(lst);
        }
    }
}