﻿using BO.ViewModel;
using iLuvMyDentist.Filters;
using System;
using static iLuvMyDentist.App_Start.APIUtil;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BusinessLogicLayer;
using BO.Models;
using DataAccessLayer.Common;
using System.Collections.Generic;

namespace iLuvMyDentist.Controllers
{
    /// <summary>
    /// Get Profile Details.
    /// Need to pass access_token into the Header in order to call APIs of Profile.
    /// </summary>
    [RoutePrefix("api/Profile")]
    [OCRCustomToken]
    public class ProfileController : ApiController
    {
        /// <summary>
        /// Get profile details for OCR 
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Get")]
        [ResponseType(typeof(DentistProfileViewModel))]
        public HttpResponseMessage Get(int UserId = 0)
        {
            try
            {
                if (UserId == 0)
                {
                    CustomAuthenticationIdentity CurrentUser = GetCurrentUser();
                    UserId = CurrentUser.UserId;
                }   
                return Request.CreateResponse(HttpStatusCode.OK,ProfileBLL.GetDentistDetails(UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Add/Edit Address details of Doctor
        /// </summary>
        /// <param name="address">Address detail Object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddAddress")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage AddAddress(AddressDetails address)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.InsertUpdateAddressDetails(address, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// First Check the Location is Used in Integrations 
        /// # IF Yes then Return 0.
        /// # If no then Delete the Location and Retun 1.
        /// # If Some Error occured while delete then Return 2.
        /// # If Some Team Member are associate with this location then retun 3.
        /// Remove Address details of Doctor
        /// </summary>
        /// <param name="Id">Address Info Id</param>
        /// <returns></returns>
        [HttpPost]
        [Route("RemoveAddress")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage RemoveAddress(int Id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.RemoveAddress(Id));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Add Education and Training details of Doctor
        /// </summary>
        /// <param name="education">Education and Training Info object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddEducation")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage AddEducation(EducationandTraining education)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.InsertUpdateEducationTrainingDetail(education, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Remove Eduction details of Doctor
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("RemoveEduction")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage RemoveEduction(int id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.RemoveEducationTrainingDetails(id));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Add/Edit Description details of Doctor
        /// </summary>
        /// <param name="Obj">Object of Description</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddDescription")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage AddDescription(Description Obj)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.InertUpdateDescription(GetCurrentUser().UserId,Obj.Text));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Add/Edit Social Media details of Doctor
        /// </summary>
        /// <param name="socialMedia">social Media of Doctor</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddSocialMedia")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage AddSocialMedia(SocialMediaForDoctorViewModel socialMedia)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.InsertUpdateSocialMedia(GetCurrentUser().UserId, socialMedia));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Add/Edit Website details of Doctor
        /// </summary>
        /// <param name="website">website of Doctor</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddWebsite")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage AddWebsite(WebsiteDetails website)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.InsertUpdateWebSiteURL(GetCurrentUser().UserId, website,null));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Address details by Address info id
        /// </summary>
        /// <param name="id">Address Info Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAddress")]
        [ResponseType(typeof(AddressDetails))]
        public HttpResponseMessage GetAddress(int id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.GetDoctorAddressDetailsByAddressInfoID(id));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Add Profile Image of Doctor
        /// </summary>
        /// <param name="dentistProfile">Dentist profile image object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddProfileImage")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage AddProfileImage(DentistProfileImage dentistProfile)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.AddProfileImage(dentistProfile, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("API--Profile--AddProfileImage", Ex.Message, Ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
        /// <summary>
        /// Check PMS Id already exist or not
        /// </summary>
        /// <param name="ExternalPMSId">External PMS Id</param>
        /// <param name="id">Location Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("CheckPMSId")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage CheckPMSId(string ExternalPMSId,int id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, new ProfileBLL().GetEPMSIdbyAccountId(GetCurrentUser().UserId, ExternalPMSId, id));
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("API--Profile--CheckPMSId", Ex.Message, Ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
        /// <summary>
        /// Edit Personal Info of Dentist
        /// </summary>
        /// <param name="Obj">Object of Dentist info</param>
        /// <returns>bool</returns>
        [HttpPost]
        [Route("EditPersonalInfo")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage EditPersonalInfo(DentistProfileViewModel Obj)
        {
            try
            {
                Obj.UserId = GetCurrentUser().UserId;
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.UpdatePersonalInfo(Obj));
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("API--Profile--EditPersonalInfo", Ex.Message, Ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
        /// <summary>
        /// Get Social Media details of Dentist
        /// </summary>
        /// <returns>bool</returns>
        [HttpGet]
        [Route("GetSocialMedia")]
        [ResponseType(typeof(List<SocialMediaForDoctorViewModel>))]
        public HttpResponseMessage GetSocialMedia()
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.GetSocialMediaDetailByUserId(GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("API--Profile--GetSocialMedia", Ex.Message, Ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Add/Edit Education And Training
        /// </summary>
        /// <returns>bool</returns>
        [HttpPost]
        [Route("AddEducationAndTraining")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage AddEducationAndTraining(EducationandTraining Obj)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.InsertUpdateEducationTrainingDetail(Obj, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("API--Profile--AddEducationAndTraining", Ex.Message, Ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Education Details by Education Id
        /// </summary>
        [HttpGet]
        [Route("GetEducationDetails")]
        [ResponseType(typeof(EducationandTraining))]
        public HttpResponseMessage GetEducationDetails(int id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.GetEducationandTrainingDetailsById(id));
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("API--Profile--GetEducationDetails", Ex.Message, Ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Search University Names for Education and Training
        /// </summary>
        [HttpGet]
        [Route("SearchUniversityNames")]
        [ResponseType(typeof(List<University>))]
        public HttpResponseMessage SearchUniversityNames(string UniversityNames)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.SearchUniversityNames(UniversityNames));
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("API--Profile--SearchUniversityNames", Ex.Message, Ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get Website details by Website Id
        /// </summary>
        /// <param name="Id">Website Id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetWebSiteDetail")]
        [ResponseType(typeof(WebsiteDetails))]
        public HttpResponseMessage GetWebSiteDetail(int Id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.GetWebSiteDetailById(Id, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("API--Profile--GetWebsite", Ex.Message, Ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
        /// <summary>
        /// Remove Website details by Website Id
        /// </summary>
        /// <param name="id">Website Id</param>
        /// <returns></returns>
        [HttpPost]
        [Route("RemoveWebsite")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage RemoveWebsite(int id)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.RemoveWebsite(id, GetCurrentUser().UserId));
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("API--Profile--RemoveWebsite", Ex.Message, Ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Update Website details 
        /// </summary>
        /// <param name="Obj">Website Object</param>
        /// <returns></returns>
        [HttpPost]
        [Route("UpdateWebsite")]
        [ResponseType(typeof(string))]
        public HttpResponseMessage UpdateWebsite(WebSiteUrl Obj)
        {
            try
            {
                Obj.UserId = GetCurrentUser().UserId;
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.UpdatePrimarySecondaryWebsite(Obj));
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("API--Profile--UpdateWebsite", Ex.Message, Ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
        /// <summary>
        /// Remove doctor profile image 
        /// </summary>       
        /// <returns></returns>
        [HttpPost]
        [Route("RemoveDoctorProfileImage")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage RemoveDoctorProfileImage()
        {
            try
            {               
               int UserId = GetCurrentUser().UserId;
               return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.RemoveProfileImage(UserId));
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("API--Profile--RemoveDoctorProfileImage", Ex.Message, Ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Get insurance provider detail list 
        /// </summary>
        /// <returns></returns>

        [HttpGet]
        [Route("GetInsuranceProviderDetail")]
        [ResponseType(typeof(Insurance))]
        public HttpResponseMessage GetInsuranceProviderDetail(int LocationId = 0)
        {
            try
            {
                 int userId = GetCurrentUser().UserId;
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.GetInsuranceProviderDetail(userId, LocationId));
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("API--Profile--GetInsuranceProviderDetail", Ex.Message, Ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertUpdateInsuranceProviderListByLocation")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage InsertUpdateInsuranceProviderListByLocation(Insurance insurance)
        {
            try
            {
                insurance.UserId = GetCurrentUser().UserId;
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.InsertUpdateInsuranceProviderListByLocation(insurance));
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("API--Profile--InsertUpdateInsuranceProviderListByLocation", Ex.Message, Ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllProviderListByUserId")]
        [ResponseType(typeof(List<InsuranceLocation>))]
        public HttpResponseMessage GetAllProviderListByUserId()
        {
            try
            {
                int userId = GetCurrentUser().UserId;
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.GetAllProviderListByUserId(userId));
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("API--Profile--GetAllProviderListByUserId", Ex.Message, Ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }


        [HttpGet]
        [Route("RemoveInsuranceProvider")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage RemoveInsuranceProvider(int InsuranceId)
        {
            try
            {
                int UserId = GetCurrentUser().UserId;
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.RemoveInsuranceProvider(UserId, InsuranceId));
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("API--Profile--RemoveInsuranceProvider", Ex.Message, Ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }


        /// <summary>
        /// Get Member Public Profile Settings base on User Id
        /// </summary>
        /// <returns>Member Public Profile Settings</returns>
        [HttpGet]
        [Route("GetMemberPublicProfileSettings")]
        [ResponseType(typeof(ProfileSection))]
        public HttpResponseMessage GetMemberPublicProfileSettings()
        {
            try
            {
                CustomAuthenticationIdentity CurrentUser = GetCurrentUser();
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.GetMemberPublicProfileSettings(CurrentUser.UserId));
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("API--Profile--GetMemberPublicProfileSettings", Ex.Message, Ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }


        /// <summary>
        /// update public profile url base on userid
        /// </summary>
        /// <returns>bool value</returns>
        [HttpPost]
        [Route("UpdatePublicProfileURL")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage UpdatePublicProfileURL(ProfileSection profileSection)
        {
            try
            {
                
                return Request.CreateResponse(HttpStatusCode.OK, ProfileBLL.UpdatePublicProfileURL(profileSection));
            }
            catch (Exception Ex)
            {
                new clsCommon().InsertErrorLog("API--Profile--UpdatePublicProfileURL", Ex.Message, Ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

    }

}
