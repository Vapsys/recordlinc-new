﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using BO.Models;
using BusinessLogicLayer;
using BO.ViewModel;
using Newtonsoft.Json;
using static iLuvMyDentist.App_Start.APIUtil;
using iLuvMyDentist.Filters;
using DataAccessLayer.Common;
using System.Web;

namespace iLuvMyDentist.Controllers
{
    /// <summary>
    /// This class is use for access all zipwhip api
    /// </summary>
    [RoutePrefix("api/zipwhip")]
    
    public class ZipwhipController : ApiController
    {
        public static clsCommon ObjCommon = new clsCommon();

        #region Messageing Services

        #region Zipwhip Login
        /// <summary>
        /// User/Login takes a username and password and returns a session key in the parameter 'response'. 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("ZipwhipLogin")]
        [HttpPost]
        [OCRCustomToken]
        [ResponseType(typeof(APIResponseBase<UserLogin>))]
        public HttpResponseMessage ZipwhipLogin(ZipwhipModel model)
        {
            APIResponseBase<UserLogin> result = new APIResponseBase<UserLogin>();
            UserLogin response = new UserLogin();
            try
            {
                result = new Zipwhip().CallZipwhipApi<UserLogin>(model);
                return Request.CreateResponse((HttpStatusCode)result.StatusCode, result);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("API Controller: Zipwhip/ZipwhipLogin", ex.Message, ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<string>()
                {
                    StatusCode = 500,
                    IsSuccess = false,
                    Result = null,
                    Message = ex.Message
                });
            }

        }
        #endregion

        #region Send SMS
        /// <summary>
        /// Sends a (SMS) text message to a single contact.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("SendSMS")]
        [HttpPost]
        [OCRCustomToken]
        [ResponseType(typeof(APIResponseBase<MessageSend>))]
        public HttpResponseMessage SendSMS(ZipwhipModel model)
        {
            APIResponseBase<MessageSend> result = new APIResponseBase<MessageSend>();
            MessageSend response = new MessageSend();
            try
            {
                result = new Zipwhip().CallZipwhipApi<MessageSend>(model);
                return Request.CreateResponse((HttpStatusCode)result.StatusCode, result);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("API Controller: Zipwhip/SendSMS", ex.Message, ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<string>()
                {
                    StatusCode = 500,
                    IsSuccess = false,
                    Result = null,
                    Message = ex.Message
                });
            }

        }
        #endregion

        #endregion

        #region Provisioning Services

        #region API For Check phonenumber is eligible for create provisioning account
        /// <summary>
        /// The first step in the process of provisioning a phone number is to verify that the number you want to provision is eligible to be provisioned. The eligible call is a required step in the provisioning process.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("Eligible")]
        [HttpPost]
        [OCRCustomToken]
        [ResponseType(typeof(APIResponseBase<ProvisionEligible>))]
        public HttpResponseMessage Eligible(ZipwhipModel model)
        {
            APIResponseBase<ProvisionEligible> result = new APIResponseBase<ProvisionEligible>();
            ProvisionEligible response = new ProvisionEligible();
            try
            {
                result = new Zipwhip().CallZipwhipApi<ProvisionEligible>(model);
                return Request.CreateResponse((HttpStatusCode)result.StatusCode, result);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("API Controller: Zipwhip/Eligible", ex.Message, ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<string>()
                {
                    StatusCode = 500,
                    IsSuccess = false,
                    Result = null,
                    Message = ex.Message
                });
            }

        }
        #endregion

        #region API For Add Provisioning
        /// <summary>
        /// The second step of the provisioning process is sending the provision request to the primary voice carrier for the eligible telephone number. This step cannot be completed until you have verified that the telephone number being submitted for provisioning is eligible.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("AddProvision")]
        [HttpPost]
        [OCRCustomToken]
        [ResponseType(typeof(APIResponseBase<ProvisionEligible>))]
        public HttpResponseMessage AddProvision(ZipwhipModel model)
        {
            APIResponseBase<ProvisionEligible> result = new APIResponseBase<ProvisionEligible>();
            ProvisionEligible response = new ProvisionEligible();
            try
            {
                result = new Zipwhip().CallZipwhipApi<ProvisionEligible>(model);
                return Request.CreateResponse((HttpStatusCode)result.StatusCode, result);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("API Controller: Zipwhip/AddProvision", ex.Message, ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<string>()
                {
                    StatusCode = 500,
                    IsSuccess = false,
                    Result = null,
                    Message = ex.Message

                });
            }

        }
        #endregion

        #region API For Check Status of Provisioning Account
        /// <summary>
        /// You can use the Provision/Status call to track the status of your provision request
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("CheckStatus")]
        [HttpPost]
        [OCRCustomToken]
        [ResponseType(typeof(APIResponseBase<ProvisionEligible>))]
        public HttpResponseMessage CheckStatus(ZipwhipModel model)
        {
            APIResponseBase<ProvisionEligible> result = new APIResponseBase<ProvisionEligible>();
            ProvisionEligible response = new ProvisionEligible();
            try
            {
                result = new Zipwhip().CallZipwhipApi<ProvisionEligible>(model);
                return Request.CreateResponse((HttpStatusCode)result.StatusCode, result);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("API Controller: Zipwhip/CheckStatus", ex.Message, ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<string>()
                {

                    StatusCode = 500,
                    IsSuccess = false,
                    Result = null,
                    Message = ex.Message

                });
            }

        }
        #endregion

        #endregion

        #region Webhook Services

        #region API For Check Status of Provisioning Account
        /// <summary>
        /// Adds the specified webhook event to the line. Each webhook event must be added individually.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("AddWebhook")]
        [HttpPost]
        [OCRCustomToken]
        [ResponseType(typeof(APIResponseBase<Webhook>))]
        public HttpResponseMessage AddWebhook(ZipwhipModel model)
        {
            APIResponseBase<Webhook> result = new APIResponseBase<Webhook>();
            ProvisionEligible response = new ProvisionEligible();
            try
            {
                result = new Zipwhip().CallZipwhipApi<Webhook>(model);
                return Request.CreateResponse((HttpStatusCode)result.StatusCode, result);
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("API Controller: Zipwhip/AddWebhook", ex.Message, ex.StackTrace);
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<string>()
                {

                    StatusCode = 500,
                    IsSuccess = false,
                    Result = null,
                    Message = ex.Message

                });
            }

        }
        #endregion

        #endregion

        #region Other

        #region API For Insert data in SMS Integration
        /// <summary>
        /// Insert zipwhip provider config details in SMSIntegration table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("InsertProviderConfig")]
        [OCRCustomToken]
        [ResponseType(typeof(APIResponseBase<int>))]
        public HttpResponseMessage InsertProviderConfig(ZipwhipAccount model)
        {
            APIResponseBase<int> result = new APIResponseBase<int>();
            try
            {
                ZipwhipBLL obj = new ZipwhipBLL();
                result = new APIResponseBase<int>()
                {
                    StatusCode = 200,
                    IsSuccess = true,
                    Result = obj.InsertProviderConfig(model),
                    Message = "Success",
                };
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("API Controller: Zipwhip/InsertProviderConfig", ex.Message, ex.StackTrace);
                result = new APIResponseBase<int>()
                {
                    StatusCode = 400,
                    IsSuccess = false,
                    Result = 0,
                    Message = ex.Message,
                };
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        #endregion

        #region API For Get List Of dentist zipwhip account base on location
        /// <summary>
        /// Insert zipwhip provider config details in SMSIntegration table
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("ListOfProviderConfig")]
        [OCRCustomToken]
        [ResponseType(typeof(APIResponseBase<List<ZipwhipAccount>>))]
        public HttpResponseMessage ListOfProviderConfig(int UserId)
        {
            APIResponseBase<List<ZipwhipAccount>> result = new APIResponseBase<List<ZipwhipAccount>>();
            try
            {
                ZipwhipBLL obj = new ZipwhipBLL();
                result = new APIResponseBase<List<ZipwhipAccount>>()
                {
                    StatusCode = 200,
                    IsSuccess = true,
                    Result = obj.ListOfProviderConfig(UserId),
                    Message = "Success",
                };
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("API Controller: Zipwhip/ListOfProviderConfig", ex.Message, ex.StackTrace);
                result = new APIResponseBase<List<ZipwhipAccount>>()
                {
                    StatusCode = 400,
                    IsSuccess = false,
                    Result = null,
                    Message = ex.Message,
                };
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        #endregion

        #region API For Insert data in SMS Integration
        
        /// <summary>
        /// For Send SMS using zipwhip services
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SendMessage")]
        [OCRCustomToken]
        [ResponseType(typeof(APIResponseBase<int>))]
        public HttpResponseMessage SendMessage(SMSDetails model)
        {
            APIResponseBase<int> result = new APIResponseBase<int>();
            try
            {
                ZipwhipBLL obj = new ZipwhipBLL();
                result = new APIResponseBase<int>()
                {
                    StatusCode = 200,
                    IsSuccess = true,
                    Result = obj.SendMessage(model),
                    Message = "Success",
                };
            }
            catch (Exception ex)
            {
                
                result = new APIResponseBase<int>()
                {
                    StatusCode = 400,
                    IsSuccess = false,
                    Result = 0,
                    Message = ex.Message,
                };
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        #endregion

        #region API For Get List Of SMS History
         
        [HttpGet]
        [Route("ListOfSMSHistory")]
        [OCRCustomToken]
        [ResponseType(typeof(APIResponseBase<List<SMSDetails>>))]
        public HttpResponseMessage ListOfSMSHistory(int UserId)
        {
            APIResponseBase<List<SMSDetails>> result = new APIResponseBase<List<SMSDetails>>();
            try
            {
                ZipwhipBLL obj = new ZipwhipBLL();
                result = new APIResponseBase<List<SMSDetails>>()
                {
                    StatusCode = 200,
                    IsSuccess = true,
                    Result = obj.ListOfSMSHistory(UserId),
                    Message = "Success",
                };
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("API Controller: Zipwhip/ListOfSMSHistory", ex.Message, ex.StackTrace);
                result = new APIResponseBase<List<SMSDetails>>()
                {
                    StatusCode = 400,
                    IsSuccess = false,
                    Result = null,
                    Message = ex.Message,
                };
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        #endregion

        #region API For Find Dentist Id

        //[HttpGet]
        //[Route("FindDentistId")]
        //[ResponseType(typeof(APIResponseBase<int>))]
        //public HttpResponseMessage FindDentistId(string PhoneNumber, string Type)
        //{
        //    APIResponseBase<int> result = new APIResponseBase<int>();
        //    try
        //    {
        //        ZipwhipBLL obj = new ZipwhipBLL();
        //        result = new APIResponseBase<int>()
        //        {
        //            StatusCode = 200,
        //            IsSuccess = true,
        //            Result = obj.FindDentistId(PhoneNumber, Type),
        //            Message = "Success",
        //        };
        //    }
        //    catch (Exception ex)
        //    {
        //        ObjCommon.InsertErrorLog("API Controller: Zipwhip/FindDentistId", ex.Message, ex.StackTrace);
        //        result = new APIResponseBase<int>()
        //        {
        //            StatusCode = 400,
        //            IsSuccess = false,
        //            Result = 0,
        //            Message = ex.Message,
        //        };
        //    }

        //    return Request.CreateResponse(HttpStatusCode.OK, result);
        //}
        #endregion

        [HttpPost]
        [Route("WebhookForReceiveSMS")]
        [ResponseType(typeof(int))]
        public HttpResponseMessage WebhookForReceiveSMS(ZipwhipWebhookResponse smsResponse)
        {
            ZipwhipBLL obj = new ZipwhipBLL();
            if (smsResponse!= null)
            {
                #region Prepare required parameter for this API 
                int UserId = obj.FindDentistId(smsResponse.finalDestination, "0");
                int PatientId = obj.FindDentistId(smsResponse.finalSource, "1");

                SMSDetails model = new SMSDetails();
                model.UserId = UserId;
                model.SenderId = PatientId;
                model.SenderType = "PATIENT";
                model.ReceiverId = UserId;
                model.ReceiverType = "DENTIST";
                model.RecepientNumber = smsResponse.finalDestination;
                model.Status = 1; /* {0:Inserting, 1:Sent, 2:Recevied, 3:Failed, 4:Deliverd, 5:Queue} */
                model.MessageBody = smsResponse.body;
                model.SMSIntegrationMasterId = 2;
                model.ResponseBody = JsonConvert.SerializeObject(smsResponse);

                int result1 = obj.SendMessage(model);
                #endregion
            }
            //return Request.CreateResponse(HttpStatusCode.OK);
            //result = new APIResponseBase<int>()
            //{
            //    StatusCode = 200,
            //    IsSuccess = true,
            //    Result = 0,
            //    Message = "success",
            //};
            return Request.CreateResponse(HttpStatusCode.OK, 200);
        }

         

        #region API For Get Deactive Account and process of Token

        [HttpGet]
        [Route("AssignZipwhipToken")]
        [OCRCustomToken]
        [ResponseType(typeof(APIResponseBase<List<ZipwhipAccount>>))]
        public HttpResponseMessage AssignZipwhipToken()
        {
            APIResponseBase<List<ZipwhipAccount>> result = new APIResponseBase<List<ZipwhipAccount>>();
            try
            {

                ZipwhipBLL obj = new ZipwhipBLL();
                result = new APIResponseBase<List<ZipwhipAccount>>()
                {
                    StatusCode = 200,
                    IsSuccess = true,
                    Result = obj.AssignZipwhipToken(),
                    Message = "Success",
                };
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("API Controller: Zipwhip/AssignZipwhipToken", ex.Message, ex.StackTrace);
                result = new APIResponseBase<List<ZipwhipAccount>>()
                {
                    StatusCode = 400,
                    IsSuccess = false,
                    Result = null,
                    Message = ex.Message,
                };
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        #endregion

        #region API For check zipwhip account is activated or not

        [HttpGet]
        [Route("CheckZipwhipAccountActivated")]
        [OCRCustomToken]
        [ResponseType(typeof(APIResponseBase<List<ZipwhipAccount>>))]
        public HttpResponseMessage CheckZipwhipAccountActivated(int UserId,int LocationId)
        {
            APIResponseBase<List<ZipwhipAccount>> result = new APIResponseBase<List<ZipwhipAccount>>();
            try
            {

                ZipwhipBLL obj = new ZipwhipBLL();
                result = new APIResponseBase<List<ZipwhipAccount>>()
                {
                    StatusCode = 200,
                    IsSuccess = true,
                    Result = obj.CheckZipwhipAccountActivated( UserId,  LocationId),
                    Message = "Success",
                };
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("API Controller: Zipwhip/CheckZipwhipAccountActivated", ex.Message, ex.StackTrace);
                result = new APIResponseBase<List<ZipwhipAccount>>()
                {
                    StatusCode = 400,
                    IsSuccess = false,
                    Result = null,
                    Message = ex.Message,
                };
            }

            return Request.CreateResponse(HttpStatusCode.OK, result);
        }
        #endregion


        #region API For List Of Dentist Location
        [HttpGet]
        
        [Route("GetDentistLocations")]
        [ResponseType(typeof(List<LocationList>))]
        [OCRCustomToken]
        public HttpResponseMessage GetDentistLocations(int UserId)
        {
            ZipwhipBLL obj = new ZipwhipBLL();
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, obj.GetDentistLocationList(UserId));
            }
            catch (Exception ex)
            {
                ObjCommon.InsertErrorLog("API Controller: Zipwhip/GetDentistLocations", ex.Message, ex.StackTrace);
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
                throw;
            }
        }
        #endregion
        #endregion

    }
}

