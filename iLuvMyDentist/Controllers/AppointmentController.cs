﻿using BO.Models;
using BusinessLogicLayer;
using iLuvMyDentist.Filters;
using System;
using static iLuvMyDentist.App_Start.APIUtil;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using DataAccessLayer.Common;
using System.Web.Http.Description;
using System.Web.Http.Cors;
using BO.ViewModel;
using DataAccessLayer.AppointmentData;
using DataAccessLayer.ColleaguesData;
using iLuvMyDentist.App_Start;

namespace iLuvMyDentist.Controllers
{
    /// <summary>
    /// Need to pass access_token into the Header in order to call APIs of Appointment.
    /// </summary>

    [RoutePrefix("api/Appointments")]
    [OCRCustomToken]
    public class AppointmentController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        //[HttpPost]
        //[Route("GetAppointments")]
        //[ResponseType(typeof(List<PatientDetailsforVer>))]
        //public HttpResponseMessage GetAppointments(PatientFilterForVeri filter)
        //{
        //    try
        //    {
        //        filter.UserId = GetCurrentUser().UserId;
        //        return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.GetPatientList(filter));
        //    }
        //    catch (Exception Ex)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
        //        throw;
        //    }
        //}

        //[HttpGet]
        //[Route("GetPatientandInsuranceDetails")]
        //[ResponseType(typeof(PatientInsuranceData))]
        //public HttpResponseMessage GetPatientandInsuranceDetails(int PatientId)
        //{
        //    try
        //    {
        //        return Request.CreateResponse(HttpStatusCode.OK, PatientBLL.GetPatientandInsuranceDetails(PatientId));
        //    }
        //    catch (Exception Ex)
        //    {
        //        return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
        //        throw;
        //    }
        //}

        /// <summary>
        /// List of Operatory (OperatoryList)
        /// </summary>
        /// <param name="LocationId"></param>
        /// <returns>List of Opration Room</returns>
        [Route("Operatory")]
        [HttpGet]
        [ResponseType(typeof(APIResponseBase<List<DentistOperatory>>))]
        public HttpResponseMessage OperatoryList(int LocationId)
        {
            try
            {
                var List = AppointmentBLL.OperatoryList(LocationId);
                if (List != null && List.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<DentistOperatory>>()
                    {
                        StatusCode = 200,
                        IsSuccess = true,
                        Result = List,
                        Message = "Success",
                    });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, new APIResponseBase<List<DentistOperatory>>()
                    {
                        StatusCode = 404,
                        IsSuccess = true,
                        Result = null,
                        Message = "Operatory not found.",
                    });
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<string>()
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    IsSuccess = false,
                    Result = null,
                    Message = ex.Message,
                });
            }
        }

        /// <summary>
        /// List of doctor services
        /// </summary>
        /// <param name="LocationId">Dentist locationId</param>
        /// <returns></returns>
        [Route("Services")]
        [HttpGet]
        [ResponseType(typeof(APIResponseBase<List<DoctorServices>>))]
        public HttpResponseMessage DoctorServices(int LocationId)
        {
            try
            {
                var List = AppointmentBLL.DoctorServices(LocationId);
                if (List != null && List.Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<DoctorServices>>()
                    {
                        StatusCode = 200,
                        IsSuccess = true,
                        Result = List,
                        Message = "Success",
                    });

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, new APIResponseBase<List<DoctorServices>>()
                    {
                        StatusCode = 404,
                        IsSuccess = true,
                        Result = null,
                        Message = "Services not found.",
                    });
                }

            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<string>()
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    IsSuccess = false,
                    Result = null,
                    Message = ex.Message,
                });
            }
        }

        /// <summary>
        /// Get List of available appointment listing
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>List of available appointment</returns>
        [Route("GetAll")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<AppointmentListing>>))]
        public HttpResponseMessage AppointmentListing(DentrixAppointment obj)
        {
            try
            {
                if (obj != null && ModelState.IsValid)
                {
                    //if (obj.EndDate.Hour == 0 && obj.EndDate.Minute == 0)
                    //{
                    //    obj.EndDate = obj.EndDate.AddHours(23).AddMinutes(59);
                    //}

                    var List = AppointmentBLL.AppointmentListing(GetCurrentUser().UserId, obj);
                    if (List != null && List.Count > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<AppointmentListing>>()
                        {
                            StatusCode = 200,
                            IsSuccess = true,
                            Result = List,
                            Message = "Success",
                        });

                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<AppointmentListing>>()
                        {
                            StatusCode = 200,
                            IsSuccess = true,
                            Result = null,
                            Message = "Appointment not found.",
                        });
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<List<KeyMessage>>()
                    {
                        StatusCode = (int)HttpStatusCode.BadRequest,
                        IsSuccess = false,
                        Result = GetModelstateErrors(ModelState),
                        Message = "Method called with invalid parameters.",
                    });
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<List<AppointmentListing>>()
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    IsSuccess = false,
                    Result = null,
                    Message = ex.Message,
                });
            }
        }

        /// <summary>
        /// Book appointment 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>Inserted appointmentid</returns>
        [Route("Insert")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<int>))]
        public HttpResponseMessage AppointmentInsert(BookAppointment obj)
        {
            APIResponseBase<int> result = new APIResponseBase<int>();
            try
            {
                if (obj != null && ModelState.IsValid)
                {

                    int InsertedId = AppointmentBLL.AppointmentInsert(GetCurrentUser().UserId, obj);
                    return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<int>()
                    {
                        StatusCode = 200,
                        IsSuccess = true,
                        Result = InsertedId,
                        Message = "Success",
                    });

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<List<KeyMessage>>()
                    {
                        StatusCode = (int)HttpStatusCode.BadRequest,
                        IsSuccess = false,
                        Result = GetModelstateErrors(ModelState),
                        Message = "Method called with invalid parameters.",
                    });
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<int>()
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    IsSuccess = false,
                    Result = 0,
                    Message = ex.Message,
                });
            }
        }

        /// <summary>
        /// This method used for getting dentist time slot.
        /// <param name="filterAppt"></param>
        /// <returns></returns>
        [Route("GetSchedules")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<ApptDayView>>))]
        public HttpResponseMessage DentistTimeSlot(FilterSchedule filterAppt)
        {
            try
            {
                if (filterAppt!= null && ModelState.IsValid)
                {
                    List<Schedules> lst = AppointmentBLL.GetSchedules(filterAppt);
                    if(lst.Count > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<Schedules>>()
                        {
                            IsSuccess = true,
                            Message = "Success",
                            Result = lst,
                            StatusCode = (int)HttpStatusCode.OK
                        });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.NotFound, new APIResponseBase<List<Schedules>>()
                        {
                            IsSuccess = true,
                            Message = "Schedules not found!",
                            Result = null,
                            StatusCode = (int)HttpStatusCode.NotFound
                        });
                    }
                    
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<List<KeyMessage>>()
                    {
                        StatusCode = (int)HttpStatusCode.BadRequest,
                        IsSuccess = false,
                        Result = GetModelstateErrors(ModelState),
                        Message = "Method called with invalid parameters.",
                    });
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<string>()
                {
                    IsSuccess = false,
                    Message = "Failed",
                    Result = null,
                    StatusCode = (int)HttpStatusCode.BadRequest
                });
                throw;
            }
        }


        /// <summary>
        /// get all patient base on doctor
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>List of available appointment</returns>
        [Route("GetPatients")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<GetPatientList>>))]
        public HttpResponseMessage PatientListing(PostPatient obj)
        {
            try
            {
                if (obj != null && ModelState.IsValid)
                {
                    var List = AppointmentBLL.PatientListing(GetCurrentUser().UserId, obj);
                    if (List != null && List.Count > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<GetPatientList>>()
                        {
                            StatusCode = 200,
                            IsSuccess = true,
                            Result = List,
                            Message = "Success",
                        });

                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<GetPatientList>>()
                        {
                            StatusCode = 200,
                            IsSuccess = true,
                            Result = null,
                            Message = "Patient not found.",
                        });
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<List<KeyMessage>>()
                    {
                        StatusCode = (int)HttpStatusCode.BadRequest,
                        IsSuccess = false,
                        Result = GetModelstateErrors(ModelState),
                        Message = "Method called with invalid parameters.",
                    });
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<List<GetPatientList>>()
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    IsSuccess = false,
                    Result = null,
                    Message = ex.Message,
                });
            }
        }


        /// <summary>
        /// Get team member list
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        [Route("GetTeamMembers")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<GetPatientList>>))]
        public HttpResponseMessage TeamMembersListing(PostTeamMember obj)
        {
            try
            {
                if (obj!= null && ModelState.IsValid)
                {
                    var List = AppointmentBLL.TeamMembersListing(GetCurrentUser().UserId, obj);
                    if (List != null && List.Count > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<DoctorTeamMemberList>>()
                        {
                            StatusCode = 200,
                            IsSuccess = true,
                            Result = List,
                            Message = "Success",
                        });

                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new APIResponseBase<List<DoctorTeamMemberList>>()
                        {
                            StatusCode = 200,
                            IsSuccess = true,
                            Result = null,
                            Message = "Patient not found.",
                        });
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, new APIResponseBase<List<KeyMessage>>()
                    {
                        StatusCode = (int)HttpStatusCode.BadRequest,
                        IsSuccess = false,
                        Result = GetModelstateErrors(ModelState),
                        Message = "Method called with invalid parameters.",
                    });
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, new APIResponseBase<List<DoctorTeamMemberList>>()
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    IsSuccess = false,
                    Result = null,
                    Message = ex.Message,
                });
            }
        }
    }
}