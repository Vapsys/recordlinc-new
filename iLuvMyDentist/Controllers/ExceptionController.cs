using BO.Models;
using BusinessLogicLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace iLuvMyDentist.Controllers
{
    /// <summary>
    /// This API Controller used for log exceptions.
    /// </summary>
    [RoutePrefix("api/Exception")]
    public class ExceptionController : ApiController
    {
        /// <summary>
        /// Post Exception
        /// </summary>
        /// <param name="Exs"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("POST")]
        [ResponseType(typeof(bool))]
        public IHttpActionResult POST(ExceptionRequestModel Exs)
        {
            try
            {
                return Ok(ExceptionBLL.Insert(Exs));
            }
            catch (Exception Ex)
            {
                return BadRequest(Ex.Message);
                throw;
            }
        }
    }
}
