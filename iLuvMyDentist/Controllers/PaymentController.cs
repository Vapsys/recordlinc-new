﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BO.Models;
using BusinessLogicLayer;

namespace iLuvMyDentist.Controllers
{
    [RoutePrefix("api/Payment")]
    public class PaymentController : ApiController
    {
        [Route("Recurring")]
        public HttpResponseMessage RecurringPayment(PaymentDetail Obj)
        {
            try
            {
                if(Obj != null)
                {
                   string result = PaymentBLL.CallSubscription(Obj);
                    return Request.CreateResponse(Ok(result));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NoContent);
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(BadRequest(Ex.Message));
                throw;
            }
        }
    }
}
