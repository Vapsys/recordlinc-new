﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BO.ViewModel;
using BusinessLogicLayer;
using BO.Models;

namespace iLuvMyDentist.Controllers
{
    [RoutePrefix("api/Review")]
    public class ReviewController : ApiController
    {
        [Route("Get")]
        public IHttpActionResult Get(string username)
        {
            try
            {
                if(!string.IsNullOrWhiteSpace(username))
                {
                    int userid = new DentistRatingBLL().GetIdOnPublicProfileURL(username);
                    DentistRatingModel model = new DentistRatingModel();
                    if (userid > 0)
                    {
                        model.ratings = new Ratings();
                        model.ratings.ReceiverId = userid;
                        model.ratingList = new DentistRatingBLL().GetDentistRatings(userid, 1);
                        model.totalRatings = new DentistRatingBLL().GetDentistRatingsCount(userid);
                        return Ok(model);
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception Ex)
            {
                throw;
            }
        }
        [Route("GetProfile")]
        public IHttpActionResult GetProfile(string Username)
        {
            try
            {
                DentistProfileDetail objProfileDetails = new DentistProfileDetail();
                DentistProfileBLL objdentistprofile = new DentistProfileBLL();
                if (!string.IsNullOrWhiteSpace(Username))
                {
                    int userid = new DentistRatingBLL().GetIdOnPublicProfileURL(Username);
                    if(userid > 0)
                    {
                        objProfileDetails = objdentistprofile.GetDoctorDetials(userid);
                        return Ok(objProfileDetails);
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception Ex)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("Post")]
        public IHttpActionResult Post(string Username, DentistRatingModel model)
        {
            try
            {
                int userid = new DentistRatingBLL().GetIdOnPublicProfileURL(Username);
                if (userid > 0)
                {
                    if (ModelState.IsValid)
                    {
                        bool result = new DentistRatingBLL().InsertRatings(model);
                        if (result)
                            return Ok();
                        else
                            return NotFound();
                    }
                    else
                    {
                        return NotFound();
                    }
                }
                else
                {
                    return NotFound();
                }
            }
            catch (Exception Ex)
            {
                throw new HttpResponseException(HttpStatusCode.BadRequest);
            }
        }
    }
}
