﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BO.Models;
using BusinessLogicLayer;
using BO.ViewModel;
using System.Data;
using DataAccessLayer.Common;
using System.Web;
using System.Web.Http.Description;
using System.Text.RegularExpressions;
using System.IO;
using iLuvMyDentist.App_Start;
using iLuvMyDentist.Filters;
using static iLuvMyDentist.App_Start.APIUtil;
using System.Linq;
using System.Web.Http.Cors;

namespace iLuvMyDentist.Controllers
{
    /// <summary>
    /// This API CONTROLLER IS USED FOR ONLY ONE-CLICK REFERRAL
    /// </summary>
    
    [RoutePrefix("api/OneClickReferral")]
    public class OneClickReferralController : ApiController
    {
        TripleDESCryptoHelper ObjTripleDESCryptoHelper = new TripleDESCryptoHelper();
        public static clsCommon ObjCommon = new clsCommon();
        //APIResponseBase result = new APIResponseBase();
        /// <summary>
        /// This method get dentist list using latitude and longitude from database.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("DentistList")]
        [ResponseType(typeof(List<DentistDetailsForPatientReward>))]
        [HttpPost]
        public List<DentistDetailsForPatientReward> DentistList(Searchlatitudelongitude model)
        {
            try
            {
                List<DentistDetailsForPatientReward> lst = new List<DentistDetailsForPatientReward>();
                string apiKey = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["GoogleLocationAPIKey"]);
                if (ModelState.IsValid)
                {
                    if(!string.IsNullOrEmpty(apiKey)) {
                        string ZipCode = null;
                        SearchProfileOfDoctor ObjOfBLL = new SearchProfileOfDoctor();
                        // DataTable dt = new DataTable();
                        // dt = ObjOfBLL.GetZipCodeByLatitudeLongitude(model);
                        ZipCode = ObjOfBLL.getZipCodeFromGoogle(model, apiKey);
                        
                        if (!string.IsNullOrEmpty(ZipCode))
                        {
                            //Getting Dentists list based on Zip-code from database using 20 mile.
                            lst = ObjOfBLL.GetSearchDentistByLocation(ZipCode, 20,model.ProviderfilterType);
                            if (lst.Count == 0)
                            {
                                for (int i = 20; i <= 100; i = i + 20)
                                {
                                    //If we didn't found in-cress the miles range by 20
                                    lst = ObjOfBLL.GetSearchDentistByLocation(ZipCode, i,model.ProviderfilterType);
                                    if (lst.Count > 0)
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        ObjCommon.InsertErrorLog("OneClickReferralController--DentistList ", "GoogleLocationAPIKey not found.", null);
                    }
                }
                return lst;
            }
            catch (Exception Ex)
            {
                throw;
            }
        }

        /// <summary>
        /// This method return colleagues list of dentist which has given #Button.
        /// </summary>
        /// <param name="objOCRColleagueList"></param>
        /// <returns></returns>
        [Route("ColleagueList")]
        [ResponseType(typeof(Colleaguelist))]
        [HttpPost]
        public Colleaguelist ColleagueList(OCRColleagueList objOCRColleagueList)
        {
            try
            {
                objOCRColleagueList.EncrypteUserId = objOCRColleagueList.EncrypteUserId.Replace(" ", "+");
                string DycrptedUserId = ObjTripleDESCryptoHelper.decryptText(objOCRColleagueList.EncrypteUserId);
                Colleaguelist lst = new Colleaguelist();
                SearchProfileOfDoctor ObjOfBLL = new SearchProfileOfDoctor();
                lst = ObjOfBLL.GetColleagueListById(Convert.ToInt32(DycrptedUserId), objOCRColleagueList.ProviderType);
                return lst;
            }
            catch (Exception Ex)
            {
                ObjCommon.InsertErrorLog("OneClickReferralController--ColleagueList", Ex.Message, Ex.StackTrace);
                throw;
            }
        }

        /// <summary>
        /// This method used for search by name on step-1 finding WHO AM I
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Route("GetSpecificSearch")]
        [ResponseType(typeof(List<DentistDetailsForPatientReward>))]
        [HttpPost]
        public List<DentistDetailsForPatientReward> GetSpecificSearch(GetSpecificSearchModel model)
        {
            try
            {
                SearchProfileOfDoctor ObjOfBLL = new SearchProfileOfDoctor();
                List<DentistDetailsForPatientReward> spList = new List<DentistDetailsForPatientReward>();
                if (!string.IsNullOrWhiteSpace(model.Keywords))
                {
                    ObjOfBLL = new SearchProfileOfDoctor();
                    spList = new List<DentistDetailsForPatientReward>();
                    spList = ObjOfBLL.GetSpecificSearch(model);
                }
                return spList;
            }
            catch(Exception Ex)
            {
                ObjCommon.InsertErrorLog("OneClickReferralController--GetSpecificSearch", Ex.Message, Ex.StackTrace);
                throw Ex;
            }
        }

        /// <summary>
        /// This API method used for Getting referral details based on User referral form setting page.
        /// </summary>
        /// <param name="VCHDARP"></param>
        /// <returns></returns>
        [Route("GetReferral")]
        [ResponseType(typeof(Compose1Click))]
        [HttpGet]
        public HttpResponseMessage GetReferralData(string VCHDARP)
        {
            APIResponseBase<object> result = new APIResponseBase<object>();

            try
            {
                if (!string.IsNullOrWhiteSpace(VCHDARP))
                {
                    Compose1Click _compose1Click = new Compose1Click();
                    string DecryptedUserId = VCHDARP;
                    try
                    {
                        if (Convert.ToInt32(DecryptedUserId) > 0)
                        {
                            _compose1Click._specialtyService = ReferralFormBLL.GetVisibleSpeciality(Convert.ToInt32(DecryptedUserId));
                            _compose1Click.lstSpecialityMaster = ReferralFormBLL.GetPatientReferralForm(Convert.ToInt32(DecryptedUserId));
                            _compose1Click._innerService = ReferralFormBLL.GetPatientInnerService(Convert.ToInt32(DecryptedUserId));
                            _compose1Click.StateList = PatientBLL.GetStateList("US");                            
                            return Request.CreateResponse(HttpStatusCode.OK, _compose1Click);
                        }
                        else
                        {
                            APIResponseBase<object> Result = new APIResponseBase<object>();
                            result.IsSuccess = false;
                            result.Message = "Input parameter string was not in Correct format!";
                            result.Result = null;
                            result.StatusCode = 500;
                            return Request.CreateResponse(HttpStatusCode.BadRequest, result);
                        }
                    }
                    catch (Exception Ex)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                        throw;
                    }
                }
                else
                {
                    APIResponseBase<object> Result = new APIResponseBase<object>();
                    result.IsSuccess = false;
                    result.Message = "Can not accept null Input parameter!";
                    result.Result = null;
                    result.StatusCode = 500;
                    return Request.CreateResponse(HttpStatusCode.BadRequest, result);
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This API method used for save a referral of One-click referral Last step.
        /// </summary>
        /// <param name="Model"></param>
        /// <returns></returns>
        [Route("ComposeReferral")]
        [ResponseType(typeof(int))]
        [HttpPost]
        public HttpResponseMessage ComposeReferral(Compose1Click Model)
        {
            try
            {
                Model.SenderId = new TripleDESCryptoHelper().decryptText(Model.SenderId);
                return Request.CreateResponse(HttpStatusCode.OK, ReferralFormBLL.ComposeReferral1Click(Model));
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This method used to get colleagues referral form setting values.
        /// </summary>
        /// <param name="ColleagueId"></param>
        /// <returns></returns>
        [Route("ColleagueDetail")]
        [ResponseType(typeof(List<DentistDetailsForPatientReward>))]
        [HttpPost]
        public HttpResponseMessage ColleagueDetail(string ColleagueId)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, new SearchProfileOfDoctor().GetColleagueDetailById(Convert.ToInt32(ColleagueId)));
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }
        [Route("LocationList")]
        [HttpGet]
        public HttpResponseMessage GetColleagueLocationList(string ColleagueId)
        {
            try
            {
                ColleagueId = ColleagueId.Replace(" ", "+");
                string DycrptedUserId = ColleagueId;
                List<BO.Models.LocationDetail> list = new List<BO.Models.LocationDetail>();
                List<BO.Models.LocationDetail> lst = DentistProfileBLL.GetColleaguesLocationList(Convert.ToInt32(DycrptedUserId));
                list = lst.Where(x => x.ContactType == 1).ToList();
                list.AddRange(lst.Where(x => x.ContactType != 1).ToList());
                return Request.CreateResponse(HttpStatusCode.OK, list);
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        [Route("UploadFiles")]
        [HttpPost]
        public HttpResponseMessage SendUploadFiles(string SessionId = "", string path = null)
        {
            CommonBLL ObjCommonbll = new CommonBLL();
            string UploadedFileIds = string.Empty;

            TripleDESCryptoHelper ObjTripleDESCrypto = new TripleDESCryptoHelper();
            SessionId = SessionId.Replace(" ", "+");
            string UserId = ObjTripleDESCrypto.decryptText(SessionId);

            string TempFolderPath = System.Configuration.ConfigurationManager.AppSettings["TempFileUpload"];

            try
            {
                var httpPostedFile = HttpContext.Current.Request.Files;
                List<string> objId = new List<string>();
                if (httpPostedFile != null)
                {
                    for (int i = 0; i < httpPostedFile.Count; i++)
                    {
                        HttpPostedFileBase file = new HttpPostedFileWrapper(httpPostedFile[i]);
                        int RecordId = 0; string Filename = string.Empty; string FileExtension = string.Empty; string NewFileName = string.Empty;
                        if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE" || HttpContext.Current.Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            Filename = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            Filename = file.FileName;
                        }

                        Filename = Regex.Replace(Filename, @"[^0-9a-zA-Z\._]", string.Empty);
                        //string imagebank= System.Configuration.ConfigurationManager.AppSettings["ImageBank"];
                        NewFileName = "$" + DateTime.Now.Ticks + "$" + Filename;
                        FileExtension = Path.GetExtension(Filename).ToLower();

                        if (!new CommonBLL().CheckFileIsValidImage(Filename))
                        {
                            RecordId = ObjCommonbll.Insert_Temp_UploadedFiles(Convert.ToInt32(UserId), NewFileName, ObjCommonbll.GetUniquenumberForAttachments());
                            objId.Add("F_" + RecordId);
                        }
                        else
                        {
                            RecordId = ObjCommonbll.Insert_Temp_UploadedImages(Convert.ToInt32(UserId), NewFileName, ObjCommonbll.GetUniquenumberForAttachments());
                            objId.Add("I_" + RecordId);
                        }
                        string strMapPath = TempFolderPath;
                        DataAccessLayer.AuthenticationPR objAuth = new DataAccessLayer.AuthenticationPR();
                        objAuth.WriteLog("Get Path from the API from OneClickReferralController:- " + strMapPath, true);
                        NewFileName = Path.Combine(strMapPath, NewFileName);
                        file.SaveAs(NewFileName);
                    }
                    UploadedFileIds = string.Join(",", objId);
                    return Request.CreateResponse(HttpStatusCode.OK, UploadedFileIds);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, "File not found");
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        [Route("Fetchfiles")]
        [HttpGet]
        public HttpResponseMessage GetTempUploadForSendReferral(string FileIds, int MessageId = 0)
        {
            try
            {
                CommonBLL ObjCommonbll = new CommonBLL();
                List<TempFileAttacheMents> objlistofattachements = new List<TempFileAttacheMents>();
                objlistofattachements = ObjCommonbll.GetTempAttachments(FileIds, MessageId);
                if (objlistofattachements != null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, objlistofattachements);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
        }

        [Route("DeleteAttachedFile")]
        [HttpPost]
        public HttpResponseMessage RemoveTempUploadFiles(int FileId, int FileFrom, string FileName, int ComposeType = 0)
        {
            try
            {
                CommonBLL ObjCommonbll = new CommonBLL();
                bool Status = false;
                string TempFolderPath = System.Configuration.ConfigurationManager.AppSettings["Tempfolderfullpath"];
                if (!string.IsNullOrWhiteSpace(FileName) && FileId > 0 && FileFrom > 0)
                {
                    string FilePath = string.Empty;
                    switch (FileFrom)
                    {
                        case (int)BO.Enums.Common.FileFromFolder.TempFile_TempFolder:
                        case (int)BO.Enums.Common.FileFromFolder.TempImage_TempFolder:
                            FilePath = TempFolderPath + FileName;
                            //if (FilePath.Contains("RL_API"))
                            //{
                            //    FilePath = System.Configuration.ConfigurationManager.AppSettings["qa_Tempfolderpath"] + FileName;
                            //}
                            //else if (FilePath.Contains("RLAPI"))
                            //{
                            //    FilePath = System.Configuration.ConfigurationManager.AppSettings["prod_Tempfolderpath"] + FileName;
                            //}
                            //else if (FilePath.Contains("iLuvMyDentist"))
                            //{
                            //    FilePath = FilePath + FileName;
                            //    FilePath = FilePath.Replace("iLuvMyDentist", "NewRecordlinc");
                            //}
                            break;
                    }
                    if (ObjCommonbll.Temp_AttachedFileDeleteById(FileId, FileFrom, ComposeType))
                    {
                        Status = true;
                        if (System.IO.File.Exists(FilePath))
                            System.IO.File.Delete(FilePath);
                    }
                }
                return Request.CreateResponse(HttpStatusCode.OK, Status);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get Schedule Appointment data for Dentist.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>        
        [Route("GetScheduleData")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<AppointmentReportModel>>))]        
        public HttpResponseMessage GetScheduleData(GetScheduleDataModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    model.UserId = model.UserId.Replace(" ", "+");
                    model.UserId = ObjTripleDESCryptoHelper.decryptText(model.UserId);
                    APIResponseBase<List<AppointmentReportModel>> response = new APIResponseBase<List<AppointmentReportModel>>()
                    {
                        IsSuccess = true,
                        Message = string.Empty,
                        StatusCode = 200,
                        Result = AppointmentBLL.GetScheduleDataList(model)
                    };

                    return Request.CreateResponse(HttpStatusCode.OK, response);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, APIUtil.GetModelstateErrors(ModelState));
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method will return List of Patient.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>        
        [Route("ListOfPatient")]
        [ResponseType(typeof(APIResponseBase<List<PatientDetialsForPatientreward>>))]
        public HttpResponseMessage ListOfPatient(PatientFilter model)
        {
            APIResponseBase<List<PatientDetialsForPatientreward>> result = new APIResponseBase<List<PatientDetialsForPatientreward>>();
            try
            {
                if (model.Username == "RecordlincPatientReward" && model.Password == "Recordlinc#1")
                {
                    List<PatientDetialsForPatientreward> lst = new List<PatientDetialsForPatientreward>();
                    PatientBLL clsPatientBLL = new PatientBLL();
                    lst = PatientBLL.Appointment_GetPatientDetails(model);
                    if (lst.Count > 0)
                    {
                        result = new APIResponseBase<List<PatientDetialsForPatientreward>>()
                        {
                            StatusCode = 200,
                            IsSuccess = true,
                            Result = lst,
                            Message = "Success",
                        };
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.NotFound, "We are unable to find your patient record.");
                    }

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "Username or Password is invalid");
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, Ex.Message);
                throw;
            }
        }


        /// <summary>
        /// API to check whether dentist is already a integration client or not
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [Route("IsDentistIntegrationPartner")]
        [ResponseType(typeof(APIResponseBase<bool>))]
        [HttpGet]
        public HttpResponseMessage IsDentistIntegrationPartner(string UserId)
        {
            try
            {
                int n;
                if (!string.IsNullOrWhiteSpace(UserId))
                {
                    UserId = UserId.Replace(" ", "+");
                    UserId = ObjTripleDESCryptoHelper.decryptText(UserId);
                    if (int.TryParse(UserId, out n))
                    {
                        bool result = DentistBLL.IsDentistIntegrationPartner(Convert.ToInt32(UserId));
                        return Request.CreateResponse(HttpStatusCode.OK, result);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, "UserId invalid to check for integration");
                    }

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, "UserId is empty to check for integration");
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method used for getting team member list of who am i.
        /// </summary>
        /// <param name="ColleagueId"></param>
        /// <returns></returns>
        [Route("GetTeamMember")]
        [HttpPost]
        public HttpResponseMessage GetTeamMemberList(OCRColleagueList objOCRColleagueList)
        {
            try
            {
                objOCRColleagueList.EncrypteUserId = objOCRColleagueList.EncrypteUserId.Replace(" ", "+");
                string DycrptedUserId = ObjTripleDESCryptoHelper.decryptText(objOCRColleagueList.EncrypteUserId);
                return Request.CreateResponse(HttpStatusCode.OK, ReferralFormBLL.GetTeamMembers(Convert.ToInt32(DycrptedUserId), objOCRColleagueList.ProviderType));
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method used for getting patient detail on patient select in oneclick referral
        /// </summary>
        /// <param name="PatientId"></param>
        /// <param name="DoctorId"></param>
        /// <returns></returns>
        [Route("GetPatientDetail")]
        [HttpGet]
        public HttpResponseMessage GetPatientDetail(string PatientId, string DoctorId)
        {
            try
            {
                Patients model = PatientBLL.GetPatientDetailsById(Convert.ToInt32(ObjTripleDESCryptoHelper.decryptText(PatientId.Replace(" ", "+"))), ObjTripleDESCryptoHelper.decryptText(DoctorId.Replace(" ", "+")));
                DataTable result = new DataAccessLayer.PatientsData.clsPatientsData().GetRegistration(Convert.ToInt32(ObjTripleDESCryptoHelper.decryptText(PatientId.Replace(" ", "+"))), "GetRegistrationform");
                if (result.Rows.Count > 0 && result != null)
                {
                    model.EmergencyContactName = Convert.ToString(result.Rows[0]["txtemergencyname"]);
                    model.EmergencyPhoneNo = Convert.ToString(result.Rows[0]["txtemergency"]);
                }
                return Request.CreateResponse(HttpStatusCode.OK, model);
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Get the dentist detail for login page in one click referral
        /// </summary>
        /// <param name="UserId"></param>
        /// <returns></returns>
        [Route("GetDentistDetail")]
        [HttpGet]
        public HttpResponseMessage GetDentistDetail(string UserId)
        {
            try
            {
                UserId = UserId.Replace(" ", "+");
                string DycrptedUserId = ObjTripleDESCryptoHelper.decryptText(UserId);
                DataTable result = DentistBLL.GetDentistDetailByUserId(Convert.ToInt32(DycrptedUserId));
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            catch (Exception Ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, Ex.Message);
                throw;
            }
        }

        /// <summary>
        /// This Method Get the Dentist Referral Received History.
        /// </summary>
        /// <param name="FilterObj"></param>
        /// <returns></returns>
        [Route("OneClickReferralHistory")]
        [ResponseType(typeof(APIResponseBase<List<OneClickReferralDetail>>))]
        public HttpResponseMessage OneClickReferralHistory(FilterMessageConversation FilterObj)
        {
            APIResponseBase<List<OneClickReferralDetail>> result = new APIResponseBase<List<OneClickReferralDetail>>();
            PatientReport Bll = new PatientReport();
            List<OneClickReferralDetail> lst = new List<OneClickReferralDetail>();
            lst = MessagesBLL.GetMessageConversation(FilterObj);
            if (lst.Count > 0)
            {
                result = new APIResponseBase<List<OneClickReferralDetail>>()
                {
                    StatusCode = 200,
                    IsSuccess = true,
                    Result = lst,
                    Message = "Success",
                };
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// This Method Get the Dentist Referral Received History.
        /// </summary>
        /// <param name="FilterObj"></param>
        /// <returns></returns>
        [Route("GetCommunicationList")]
        [HttpPost]
        [ResponseType(typeof(APIResponseBase<List<OneClickReferralDetail>>))]
        public HttpResponseMessage GetCommunicationList(FilterMessageConversation FilterObj)
        {
            APIResponseBase<List<OneClickReferralDetail>> result = new APIResponseBase<List<OneClickReferralDetail>>();
            PatientReport Bll = new PatientReport();
            List<OneClickReferralDetail> lst = new List<OneClickReferralDetail>();
            lst = MessagesBLL.GetCommunicationList(FilterObj);
            if (lst.Count > 0)
            {
                result = new APIResponseBase<List<OneClickReferralDetail>>()
                {
                    StatusCode = 200,
                    IsSuccess = true,
                    Result = lst,
                    Message = "Success",
                };
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// This Method get Disposition status 
        /// </summary>
        /// <returns></returns>
        [Route("GetDispositionListForOCR")]
        [ResponseType(typeof(APIResponseBase<List<Disposition>>))]
        [HttpGet]
        public HttpResponseMessage GetDispositionListForOCR()
        {
            APIResponseBase<List<Disposition>> result = new APIResponseBase<List<Disposition>>();
            PatientReport Bll = new PatientReport();
            List<Disposition> Dispositionlistlist = new List<Disposition>();
            Dispositionlistlist = Bll.GetDispositionlist();
            if (Dispositionlistlist.Count > 0)
            {
                result = new APIResponseBase<List<Disposition>>()
                {
                    StatusCode = 200,
                    IsSuccess = true,
                    Result = Dispositionlistlist,
                    Message = "Success",
                };
                return Request.CreateResponse(HttpStatusCode.OK, result);
            }
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        /// <summary>
        /// This method used for getting Patient name for OCR Dashboard Auto Complete
        /// </summary>
        /// <param name="PatientName"></param>
        /// <returns></returns>
        [Route("GetPatietName")]
        [ResponseType(typeof(string))]
        [HttpGet]
        public HttpResponseMessage GetPatientNameForAutocomplete(string PatientName)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ReferralFormBLL.GetPatientNameForAutoComplete(PatientName));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// This API method is used for OCR Disposition status change of referral.
        /// </summary>
        /// <returns></returns>
        [Route("ChangeDispositionStatus")]
        [ResponseType(typeof(bool))]
        [HttpPost]
        [OCRCustomToken]
        public HttpResponseMessage ChangeDispositionStatus(UpdateDisposition Obj)
        {
            CustomAuthenticationIdentity CurrentUser = GetCurrentUser();
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientReport.UpdateDispositionStatus(Obj.DispositionId, Obj.MessageId, CurrentUser.UserId, Obj.LoginURL, Obj.Note, Obj.VisibleToPatient, Obj.VisibleToDoctor,Obj.ScheduledDate,CurrentUser.TimeZoneOfDentist));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// This Method get the Dentist details form database.
        /// </summary>
        /// <param name="access_token"></param>
        /// <returns></returns>
        [ResponseType(typeof(DentistDetail))]
        [HttpGet]
        [Route("DentistDetail")]
        public HttpResponseMessage DentistDetail(string access_token)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(access_token))
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ReferralFormBLL.GetDentistDetail(access_token));
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid access_token!");
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
        /// <summary>
        /// This Method is used for Delete the Message from OCR.
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        [HttpPost]
        [ResponseType(typeof(int))]
        [Route("DeleteMessage")]
        public HttpResponseMessage DeleteMessage(DeleteMessage Obj)
        {
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, ReferralFormBLL.DeleteMessageFromOCR(Obj));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
        /// <summary>
        /// This Method is used for Get Unread Message Count from OCR.
        /// </summary>
        /// <param name="access_token"></param>
        /// <returns></returns>
        [HttpGet]
        [ResponseType(typeof(UnreadMessageCount))]
        [Route("InboxUnreadCount")]
        public HttpResponseMessage GetInboxUnreadCount(string access_token)
        {
            try
            {
                UnreadMessageCount Model = new UnreadMessageCount();
                DataTable Dt = clsCommon.GetDentistDetailsByAccessToken(access_token);
                if(Dt.Rows.Count > 0)
                {
                    Model.Count = new MessagesBLL().GetUnreadMessageCountOfDoctor(Convert.ToInt32(Dt.Rows[0]["UserId"]));
                }
                return Request.CreateResponse(HttpStatusCode.OK, Model);
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// This Method get Referral Message Details by MessageId and MessageType (Inbox/Sent) with Access Token.
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ReferralMessageDetails")]
        [ResponseType(typeof(APIResponseBase<ReferralMessage>))]
        public HttpResponseMessage GetReferralMessageDetails(ReferralMessageFilter filter)
        {
            try
            {
                APIResponseBase<ReferralMessage> aPIResponseBase = new APIResponseBase<ReferralMessage>();
                aPIResponseBase.IsSuccess = true;
                aPIResponseBase.Message = "Success!";
                aPIResponseBase.Result = MessagesBLL.referralMessageDetails(filter);
                aPIResponseBase.StatusCode = 200;
                return Request.CreateResponse(HttpStatusCode.OK, aPIResponseBase);
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
        /// <summary>
        /// This Method get Referral Message Details by MessageId and MessageType (Inbox/Sent) with Access Token.
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("NormalMessageDetails")]
        [ResponseType(typeof(APIResponseBase<NormalMessageDetails>))]
        public HttpResponseMessage GetNoramlMessageDetails(ReferralMessageFilter filter)
        {
            try
            {
                APIResponseBase<NormalMessageDetails> aPIResponseBase = new APIResponseBase<NormalMessageDetails>();
                aPIResponseBase.IsSuccess = true;
                aPIResponseBase.Message = "Success!";
                aPIResponseBase.Result = MessagesBLL.NormalMessageDetails(filter);
                aPIResponseBase.StatusCode = 200;
                return Request.CreateResponse(HttpStatusCode.OK, aPIResponseBase);
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Submit ContactUS From OCR side.
        /// </summary>
        /// <param name="contactUs"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ContactUS")]
        [ResponseType(typeof(bool))]
        public HttpResponseMessage ContactUS(ContactUs contactUs)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, ContactUSBLL.SubmitContactUSRequest(contactUs));
                }
                else
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest,ModelState);
                }
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }

        /// <summary>
        /// Change Co-ordinator in referral 
        /// </summary>
        /// <param name="objCoordinator"></param>
        /// <returns></returns>

        [Route("ChangeCoordinator")]
        [ResponseType(typeof(bool))]
        [HttpPost]        
        public HttpResponseMessage ChangeCoordinator(Coordinator objCoordinator)
        {
            
            try
            {
                return Request.CreateResponse(HttpStatusCode.OK, PatientReport.ChangeCoordinator(objCoordinator));
            }
            catch (Exception Ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, Ex);
                throw;
            }
        }
    }
}
