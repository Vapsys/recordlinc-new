﻿using System;
using System.Web;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using RecordlincSync.DAL;
using CookComputing.XmlRpc;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
namespace RecordlincSync
{
    public partial class Form1 : Form
    {
        private string DeveloperAppKey;
        private string DeveloperAppSecret;

        public Form1()
        {
            InitializeComponent();

            timeSchedule1.Value = Properties.Settings.Default.schedule1;
            timeSchedule2.Value = Properties.Settings.Default.schedule2;
            timeSchedule3.Value = Properties.Settings.Default.schedule3;
            timeSchedule4.Value = Properties.Settings.Default.schedule4;

            DeveloperAppKey = Properties.Settings.Default.DeveloperAppKey;
            DeveloperAppSecret = Properties.Settings.Default.DeveloperAppSecret;

        }

        private void timer1_Tick(object sender, EventArgs e)
        {

            var sysdatetime = DateTime.Now;
            lefttime1.Text = sysdatetime.ToString();

            var scheduletime1 = Properties.Settings.Default.schedule1;
            var scheduletime2 = Properties.Settings.Default.schedule2;
            var scheduletime3 = Properties.Settings.Default.schedule3;
            var scheduletime4 = Properties.Settings.Default.schedule4;

            TimeSpan scheduleDiff1 = scheduletime1.TimeOfDay.Add(-sysdatetime.TimeOfDay);
            TimeSpan scheduleDiff2 = scheduletime2.TimeOfDay.Add(-sysdatetime.TimeOfDay);
            TimeSpan scheduleDiff3 = scheduletime3.TimeOfDay.Add(-sysdatetime.TimeOfDay);
            TimeSpan scheduleDiff4 = scheduletime4.TimeOfDay.Add(-sysdatetime.TimeOfDay);

            int ScheduleNo = 0;
            if (scheduleDiff1.Seconds == 0 && scheduleDiff1.Minutes == 0 && scheduleDiff1.Hours == 0)
            {
                if (timeSchedule1.Checked)
                    ScheduleNo = 1;
            }
            if (scheduleDiff2.Seconds == 0 && scheduleDiff2.Minutes == 0 && scheduleDiff2.Hours == 0)
            {
                if (timeSchedule2.Checked)
                    ScheduleNo = 2;
            }
            if (scheduleDiff3.Seconds == 0 && scheduleDiff3.Minutes == 0 && scheduleDiff3.Hours == 0)
            {
                if (timeSchedule3.Checked)
                    ScheduleNo = 3;
            }
            if (scheduleDiff4.Seconds == 0 && scheduleDiff4.Minutes == 0 && scheduleDiff4.Hours == 0)
            {
                if (timeSchedule4.Checked)
                    ScheduleNo = 4;
            }


            if (ScheduleNo > 0)
            {
                object[] arrObjects = new object[] { ScheduleNo };
                if (!SchedulebackgroundWorker1.IsBusy)
                {
                    ScheduleNo = 0;
                    timer1.Enabled = false;
                    SchedulebackgroundWorker1.RunWorkerAsync(arrObjects);
                }

            }

        }

        #region timeSchedule
        private void timeSchedule1_ValueChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.schedule1 = timeSchedule1.Value;
            Properties.Settings.Default.Save();
        }

        private void timeSchedule2_ValueChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.schedule2 = timeSchedule2.Value;
            Properties.Settings.Default.Save();
        }

        private void timeSchedule3_ValueChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.schedule3 = timeSchedule3.Value;
            Properties.Settings.Default.Save();
        }

        private void timeSchedule4_ValueChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.schedule4 = timeSchedule4.Value;
            Properties.Settings.Default.Save();
        }

        #endregion

        #region Schedule 1
        private void SchedulebackgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                BackgroundWorker sendingWorker = (BackgroundWorker)sender;
                object[] arrObjects = (object[])e.Argument;

                int ScheduleNo = (int)arrObjects[0];


                clsSyncInfusionSoft obj = new clsSyncInfusionSoft();
                DataSet ds = obj.GetDataSyncInfusionSoft(ScheduleNo);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        int totalSync, totaluser;
                        int sync = 1, user = 1;
                        totalSync = ds.Tables[0].Rows.Count;

                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            if (!sendingWorker.CancellationPending)
                            {
                                DataTable dt = obj.GetPatientDetailsOfDoctor(Convert.ToInt32(item["MemberId"] ?? "0"));
                                if (dt.Rows.Count > 0 && !string.IsNullOrEmpty(item["RefreshToken"].ToString()))
                                {
                                    string RefreshToken = Callback(Convert.ToInt32(item["MemberId"]), item["RefreshToken"].ToString());

                                    if (!string.IsNullOrEmpty(RefreshToken))
                                    {



                                        totaluser = dt.Rows.Count;

                                        foreach (DataRow dr in dt.Rows)
                                        {
                                            System.Threading.Thread.Sleep(50);

                                            AddWithDupCheck(RefreshToken,
                                                              dr["FirstName"].ToString(),
                                                              dr["LastName"].ToString(),
                                                              dr["ExactAddress"].ToString(),
                                                              dr["City"].ToString(),
                                                              dr["State"].ToString(),
                                                              dr["ZipCode"].ToString(),
                                                              dr["Phone"].ToString(),
                                                              dr["Email"].ToString());

                                            System.Threading.Thread.Sleep(100);

                                            user++;

                                            Object progressmsg = "Sync" + totalSync + "/" + sync + " | Record " + totaluser + "/" + user;
                                            sendingWorker.ReportProgress(0, progressmsg);
                                        }

                                        obj.UpdateLastSyncInfusionSoftApp(Convert.ToInt32(item["MemberId"] ?? "0"));
                                    }
                                }
                            }
                            else
                            {
                                e.Cancel = true;
                                break;
                            }
                            sync++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void SchedulebackgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            lblStatus.Text = string.Format("Progress: {0}", e.UserState.ToString());
        }

        private void SchedulebackgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!e.Cancelled && e.Error == null)
            {
                lblStatus.Text = "Done";
            }
            else if (e.Cancelled)
            {
                lblStatus.Text = "User Canceled";
            }
            else
            {
                lblStatus.Text = "An error has occurred";
            }
            timer1.Enabled = true;
        }

        #endregion

        #region API Method

        static public string EncodeTo64(string toEncode)
        {
            byte[] toEncodeAsBytes
                  = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);
            string returnValue
                  = System.Convert.ToBase64String(toEncodeAsBytes);
            return returnValue;
        }
        public string Callback(int memberid, string code)
        {
            try
            {
                if (!string.IsNullOrEmpty(code))
                {
                    string tokenUrl = "https://api.infusionsoft.com/token";

                    //string tokenDataFormat = "refresh_token={0}&client_id={1}&client_secret={2}&redirect_uri={3}&grant_type=refresh_token";
                    string tokenDataFormat = "grant_type=refresh_token&refresh_token=" + code;

                    HttpWebRequest request = HttpWebRequest.Create(tokenUrl) as HttpWebRequest;
                    request.Method = "POST";
                    request.KeepAlive = true;
                    request.ContentType = "application/x-www-form-urlencoded";
                    request.Headers.Add("Authorization: Basic " + EncodeTo64(DeveloperAppKey + ":" + DeveloperAppSecret));
                    //string dataString = string.Format(tokenDataFormat, code, DeveloperAppKey, DeveloperAppSecret, HttpUtility.UrlEncode("http://localhost/"));

                    string dataString = tokenDataFormat;

                    var dataBytes = Encoding.UTF8.GetBytes(dataString);
                    using (Stream reqStream = request.GetRequestStream())
                    {
                        reqStream.Write(dataBytes, 0, dataBytes.Length);
                    }

                    string resultJSON = string.Empty;
                    using (WebResponse response = request.GetResponse())
                    {
                        var sr = new StreamReader(response.GetResponseStream());
                        resultJSON = sr.ReadToEnd();
                        sr.Close();
                    }

                    var jsonSerializer = new JavaScriptSerializer();

                    var tokenData = jsonSerializer.Deserialize<TokenData>(resultJSON);

                    clsSyncInfusionSoft obj = new clsSyncInfusionSoft();
                    bool result = obj.UpdateDataSyncTokenInfusionSoft(memberid, tokenData.Access_Token, tokenData.Refresh_Token, code);

                    if (result)
                    {
                        return tokenData.Access_Token;
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }

            return string.Empty;
        }

        public void AddWithDupCheck(string accessToken, string FirstName, string LastName, string StreetAddress1, string City, string State, string ZipFour1, string Phone1, string Email)
        {
            var contactService = XmlRpcProxyGen.Create<IInfusionsoftAPI>();

            contactService.Url = "https://api.infusionsoft.com/crm/xmlrpc/v1?access_token=" + accessToken;

            if (!string.IsNullOrEmpty(FirstName) && !string.IsNullOrEmpty(LastName) && !string.IsNullOrEmpty(Email))
            {

                XmlRpcStruct conDat = new XmlRpcStruct();

                conDat.Add("FirstName", FirstName);
                conDat.Add("LastName", LastName);
                conDat.Add("StreetAddress1", StreetAddress1);
                conDat.Add("City", City);
                conDat.Add("State", State);
                conDat.Add("ZipFour1", ZipFour1);
                conDat.Add("Phone1", Phone1);
                conDat.Add("Email", Email);


                try
                {
                    var result = contactService.addWithDupCheck(accessToken, conDat, "Email");
                }
                catch (Exception)
                {
                }
            }

        }

        public interface IInfusionsoftAPI : IXmlRpcProxy
        {
            [XmlRpcMethod("ContactService.addWithDupCheck")]
            int addWithDupCheck(string key, XmlRpcStruct map, string dupCheckType);
        }

        #endregion


    }
}
