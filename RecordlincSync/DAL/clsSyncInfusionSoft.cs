﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecordlincSync
{
    class clsSyncInfusionSoft
    {
        clsHelper ClsHelper = new clsHelper();
        clsHelper ObjHelper = new clsHelper();
        public DataTable GetPatientDetailsOfDoctor(int UserId)
        {
            int PageIndex = 1;
            int PageSize = int.MaxValue;
            string SearchText = string.Empty;
            int? SortColumn = null;
            int? SortDirection = null;

            try
            {
                DataTable dt = new DataTable();
                SqlParameter[] strParameter = new SqlParameter[6];
                strParameter[0] = new SqlParameter("@DoctorId", SqlDbType.Int);
                strParameter[0].Value = UserId;
                strParameter[1] = new SqlParameter("@PageIndex", SqlDbType.Int);
                strParameter[1].Value = PageIndex;
                strParameter[2] = new SqlParameter("@PageSize", SqlDbType.Int);
                strParameter[2].Value = PageSize;
                strParameter[3] = new SqlParameter("@SearchText", SqlDbType.NVarChar);
                strParameter[3].Value = SearchText;
                strParameter[4] = new SqlParameter("@SortColumn", SqlDbType.Int);
                strParameter[4].Value = SortColumn;
                strParameter[5] = new SqlParameter("@SortDirection", SqlDbType.Int);
                strParameter[5].Value = SortDirection;

                dt = ObjHelper.DataTable("USP_GetPatientDetailsOfDoctor", strParameter);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DataSet GetDataSyncInfusionSoft(int Schedule)
        {
            try
            {
                DataSet ds = new DataSet();
                SqlParameter[] strParameter = new SqlParameter[1];
                strParameter[0] = new SqlParameter("@Schedule", SqlDbType.Int);
                strParameter[0].Value = Schedule;

                ds = ClsHelper.GetDatasetData("USP_Get_Sync_InfusionSoft_App", strParameter);
                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public bool UpdateLastSyncInfusionSoftApp(int MemberId)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] updateSyncInfusionSoft = new SqlParameter[1];
                updateSyncInfusionSoft[0] = new SqlParameter("@MemberId", SqlDbType.Int);
                updateSyncInfusionSoft[0].Value = MemberId;

                UpdateStatus = ClsHelper.ExecuteNonQuery("USP_Update_Last_Sync_InfusionSoft_App", updateSyncInfusionSoft);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public bool UpdateDataSyncTokenInfusionSoft(int MemberId, string AccessToken, string RefreshToken, string APIKey)
        {
            try
            {
                bool UpdateStatus = false;
                SqlParameter[] updateSyncInfusionSoft = new SqlParameter[4];
                updateSyncInfusionSoft[0] = new SqlParameter("@MemberId", SqlDbType.Int);
                updateSyncInfusionSoft[0].Value = MemberId;

                updateSyncInfusionSoft[1] = new SqlParameter("@AccessToken", SqlDbType.NVarChar);
                updateSyncInfusionSoft[1].Value = AccessToken;

                updateSyncInfusionSoft[2] = new SqlParameter("@RefreshToken", SqlDbType.NVarChar);
                updateSyncInfusionSoft[2].Value = RefreshToken;

                updateSyncInfusionSoft[3] = new SqlParameter("@APIKey", SqlDbType.NVarChar);
                updateSyncInfusionSoft[3].Value = APIKey;

                UpdateStatus = ClsHelper.ExecuteNonQuery("USP_Update_Sync_token_InfusionSoft", updateSyncInfusionSoft);

                return UpdateStatus;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
