﻿namespace RecordlincSync
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lefttime1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.timeSchedule4 = new System.Windows.Forms.DateTimePicker();
            this.timeSchedule3 = new System.Windows.Forms.DateTimePicker();
            this.timeSchedule2 = new System.Windows.Forms.DateTimePicker();
            this.timeSchedule1 = new System.Windows.Forms.DateTimePicker();
            this.myWorker = new System.ComponentModel.BackgroundWorker();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SchedulebackgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.statusStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus,
            this.lefttime1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 186);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(276, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // lefttime1
            // 
            this.lefttime1.Name = "lefttime1";
            this.lefttime1.Size = new System.Drawing.Size(0, 17);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.timeSchedule4);
            this.groupBox1.Controls.Add(this.timeSchedule3);
            this.groupBox1.Controls.Add(this.timeSchedule2);
            this.groupBox1.Controls.Add(this.timeSchedule1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.Blue;
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(251, 163);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Set SyncTiming";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(6, 124);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 18);
            this.label4.TabIndex = 13;
            this.label4.Text = "Schedule 4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(6, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 18);
            this.label3.TabIndex = 12;
            this.label3.Text = "Schedule 3";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(6, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(81, 18);
            this.label2.TabIndex = 11;
            this.label2.Text = "Schedule 2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(6, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 18);
            this.label1.TabIndex = 7;
            this.label1.Text = "Schedule 1";
            // 
            // timeSchedule4
            // 
            this.timeSchedule4.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timeSchedule4.Location = new System.Drawing.Point(93, 124);
            this.timeSchedule4.Name = "timeSchedule4";
            this.timeSchedule4.ShowCheckBox = true;
            this.timeSchedule4.ShowUpDown = true;
            this.timeSchedule4.Size = new System.Drawing.Size(144, 24);
            this.timeSchedule4.TabIndex = 10;
            this.timeSchedule4.ValueChanged += new System.EventHandler(this.timeSchedule4_ValueChanged);
            // 
            // timeSchedule3
            // 
            this.timeSchedule3.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timeSchedule3.Location = new System.Drawing.Point(93, 95);
            this.timeSchedule3.Name = "timeSchedule3";
            this.timeSchedule3.ShowCheckBox = true;
            this.timeSchedule3.ShowUpDown = true;
            this.timeSchedule3.Size = new System.Drawing.Size(144, 24);
            this.timeSchedule3.TabIndex = 9;
            this.timeSchedule3.ValueChanged += new System.EventHandler(this.timeSchedule3_ValueChanged);
            // 
            // timeSchedule2
            // 
            this.timeSchedule2.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timeSchedule2.Location = new System.Drawing.Point(93, 65);
            this.timeSchedule2.Name = "timeSchedule2";
            this.timeSchedule2.ShowCheckBox = true;
            this.timeSchedule2.ShowUpDown = true;
            this.timeSchedule2.Size = new System.Drawing.Size(144, 24);
            this.timeSchedule2.TabIndex = 8;
            this.timeSchedule2.ValueChanged += new System.EventHandler(this.timeSchedule2_ValueChanged);
            // 
            // timeSchedule1
            // 
            this.timeSchedule1.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.timeSchedule1.Location = new System.Drawing.Point(93, 35);
            this.timeSchedule1.Name = "timeSchedule1";
            this.timeSchedule1.ShowCheckBox = true;
            this.timeSchedule1.ShowUpDown = true;
            this.timeSchedule1.Size = new System.Drawing.Size(144, 24);
            this.timeSchedule1.TabIndex = 7;
            this.timeSchedule1.ValueChanged += new System.EventHandler(this.timeSchedule1_ValueChanged);
            // 
            // myWorker
            // 
            this.myWorker.WorkerReportsProgress = true;
            this.myWorker.WorkerSupportsCancellation = true;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // SchedulebackgroundWorker1
            // 
            this.SchedulebackgroundWorker1.WorkerReportsProgress = true;
            this.SchedulebackgroundWorker1.WorkerSupportsCancellation = true;
            this.SchedulebackgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.SchedulebackgroundWorker1_DoWork);
            this.SchedulebackgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.SchedulebackgroundWorker1_ProgressChanged);
            this.SchedulebackgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.SchedulebackgroundWorker1_RunWorkerCompleted);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(276, 208);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "Form1";
            this.Text = "Data Sync ";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker timeSchedule1;
        private System.Windows.Forms.DateTimePicker timeSchedule2;
        private System.ComponentModel.BackgroundWorker myWorker;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker timeSchedule4;
        private System.Windows.Forms.DateTimePicker timeSchedule3;
        private System.Windows.Forms.ToolStripStatusLabel lefttime1;
        private System.Windows.Forms.Timer timer1;
        private System.ComponentModel.BackgroundWorker SchedulebackgroundWorker1;
    }
}