﻿using System.Configuration;
using System.Data.SqlClient;

namespace RecordlincSync
{
    public class Helper_ConnectionClass
    {
        private SqlConnection objsqlConnection;

        public Helper_ConnectionClass()
        {
            objsqlConnection = null;
        }

        public SqlConnection Open()
        {
            objsqlConnection = new SqlConnection();
            objsqlConnection.ConnectionString = Properties.Settings.Default.RecordlincConnectionString;
            objsqlConnection.Open();
            return objsqlConnection;
        }
        public void Close(SqlConnection con)
        {
            con.Close();
        }
    }
}