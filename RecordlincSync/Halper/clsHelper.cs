﻿using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;

namespace RecordlincSync
{

    public class clsHelper
    {
        Helper_ConnectionClass obj_connection = new Helper_ConnectionClass();

        private SqlConnection objCon = new SqlConnection();
        private SqlCommand objCommand;
        private SqlDataAdapter objAdapter;
        private SqlDataReader objReader;
        private DataTable dt = new DataTable();
        private DataSet ds = new DataSet();


        public clsHelper()
        {
            objCommand = null;
            objAdapter = null;
            objReader = null;
            dt = null;
        }


        //returning data in datatable without passing any parameter....
        public DataTable DataTable(string spProcedureName)
        {

            try
            {
                dt = null;
                DataSet ds = new DataSet();
                objCon = obj_connection.Open();
                objCommand = new SqlCommand();
                objCommand.Connection = objCon;
                objCommand.CommandText = spProcedureName;

                objCommand.CommandType = CommandType.StoredProcedure;
                DebugCommand(objCommand);
                objAdapter = new SqlDataAdapter(objCommand);
                objAdapter.Fill(ds, "a");
                dt = ds.Tables["a"];
                ds.Dispose();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                obj_connection.Close(objCon);
            }

            return dt;
        }

        //returning data in datatable after passing parameter....
        public DataTable DataTable(string spProcedureName, SqlParameter[] pParameter)
        {
            try
            {
                dt = null;
                DataSet ds = new DataSet();
                objCon = obj_connection.Open();
                objCommand = new SqlCommand();
                objCommand.Connection = objCon;
                objCommand.CommandText = spProcedureName;

                objCommand.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter p in pParameter)
                {
                    objCommand.Parameters.Add(p);
                }

                DebugCommand(objCommand);
                objAdapter = new SqlDataAdapter(objCommand);
                objAdapter.Fill(ds, "a");
                dt = ds.Tables["a"];
                ds.Dispose();


            }
            catch (Exception)
            {
                throw;
            }

            finally
            {
                obj_connection.Close(objCon);

            }

            return dt;
        }

        // this method is only use to export doctor record from admin 
        public DataTable DataTableForExportData(string spProcedureName, SqlParameter[] pParameter)
        {
            try
            {
                dt = new DataTable();
               
                objCon = obj_connection.Open();
                objCommand = new SqlCommand();
                objCommand.Connection = objCon;
                objCommand.CommandText = spProcedureName;

                objCommand.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter p in pParameter)
                {
                    objCommand.Parameters.Add(p);
                }

                //DebugCommand(objCommand);
                //objAdapter = new SqlDataAdapter(objCommand);
                //objAdapter.Fill(dt);



                SqlDataReader rdr = objCommand.ExecuteReader();
                    if (rdr.HasRows)
                        dt.Load(rdr);
               

            }
            catch (Exception)
            {
                throw;
            }

            finally
            {
                obj_connection.Close(objCon);

            }

            return dt;
        }

        //reading data through datareader with parameter that returning nothing...
        private SqlDataReader DataReaderData(string spProcedureName)
        {
            try
            {
                objCon = obj_connection.Open();
                objCommand = new SqlCommand();
                objCommand.Connection = objCon;
                objCommand.CommandText = spProcedureName;

                objCommand.CommandType = CommandType.StoredProcedure;
                DebugCommand(objCommand);
                objReader = objCommand.ExecuteReader();

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                obj_connection.Close(objCon);
            }
            return objReader;
        }

        private SqlDataReader DataReader(string spProcedureName, SqlParameter[] pParameter)
        {
            try
            {
                objCon = obj_connection.Open();
                objCommand = new SqlCommand();
                objCommand.Connection = objCon;
                objCommand.CommandText = spProcedureName;

                objCommand.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter p in pParameter)
                {
                    objCommand.Parameters.Add(p);
                }
                DebugCommand(objCommand);
                objReader = objCommand.ExecuteReader();

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                obj_connection.Close(objCon);
            }
            return objReader;
        }



        public DataSet GetDatasetData(string spProcedureName, SqlParameter[] pParameter)
        {
            try
            {
                dt = null;
                ds = new DataSet();
                objCon = obj_connection.Open();
                objCommand = new SqlCommand();
                objCommand.CommandText = spProcedureName;
                objCommand.Connection = objCon; 
                
                objCommand.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter p in pParameter)
                {
                    objCommand.Parameters.Add(p);
                }

                if (objCon.State == ConnectionState.Closed)
                    objCon.Open();

                objAdapter = new SqlDataAdapter(objCommand);
                objAdapter.Fill(ds);

            }
            catch (Exception)
            {
                throw;
            }

            finally
            {
                obj_connection.Close(objCon);

            }

            return ds;
        }

        //return scalar value from database....

        public string ExecuteScalar(string spProcedureName, SqlParameter[] pParameter)
        {
            string strValue = "";
            try
            {
                objCon = obj_connection.Open();

                objCommand = new SqlCommand();
                objCommand.Connection = objCon;
                objCommand.CommandText = spProcedureName;

                objCommand.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter p in pParameter)
                {
                    objCommand.Parameters.Add(p);
                }

                DebugCommand(objCommand);
                if (objCommand.ExecuteScalar() != null)
                {
                    strValue = objCommand.ExecuteScalar().ToString();
                }
                else
                {
                    strValue = "";
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                obj_connection.Close(objCon);
            }
            return strValue;

        }

        //update the database ....
        public bool ExecuteNonQuery(string spStoredProcedure, SqlParameter[] pParameter)
        {
            bool boolSuccess = false;

            try
            {
                objCon = obj_connection.Open();
                objCommand = new SqlCommand();
                objCommand.Connection = objCon;
                objCommand.CommandText = spStoredProcedure;

                objCommand.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter p in pParameter)
                {
                    objCommand.Parameters.Add(p);
                }
                DebugCommand(objCommand);
                int intReturnValue = objCommand.ExecuteNonQuery();

                if (intReturnValue > 0)
                {
                    boolSuccess = true;

                }
                else
                {
                    boolSuccess = false;
                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                obj_connection.Close(objCon);
            }

            return boolSuccess;

        }

        public ArrayList DataReader(string ProcedureName)
        {
            ArrayList arrData = new ArrayList();
            try
            {
                SqlDataReader dr = DataReaderData(ProcedureName);

                while (dr.Read())
                {
                    for (int i = 0; i < dr.FieldCount; i++)
                    {
                        arrData.Add(dr[i].ToString());
                    }
                }
                dr.Close();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                obj_connection.Close(objCon);
            }
            return arrData;
        }

        ~clsHelper()
        {
            objCon = null;
            objCommand = null;
            objAdapter = null;
            objReader = null;
            dt = null;
        }
        public string ExecuteNonQueryString(string spStoredProcedure, SqlParameter[] pParameter)
        {
            string strRetrun = string.Empty;

            try
            {
                objCon = obj_connection.Open();
                objCommand = new SqlCommand();
                objCommand.Connection = objCon;
                objCommand.CommandText = spStoredProcedure;
                objCommand.CommandType = CommandType.StoredProcedure;

                foreach (SqlParameter p in pParameter)
                {
                    objCommand.Parameters.Add(p);
                }
                DebugCommand(objCommand);
                int intReturnValue = objCommand.ExecuteNonQuery();
                strRetrun = pParameter[1].Value.ToString();

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                obj_connection.Close(objCon);
            }

            return strRetrun;

        }

        [Conditional("DEBUG")]
        private void DebugCommand(SqlCommand cmd)
        {
            // these 2 get hammered by ajax polling
            if (cmd.CommandText == "stp_GetMessage" || cmd.CommandText == "Get_Online_Colleagues")
                return;
            Debug.WriteLine(cmd.CommandText);
        }
    }
}