﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RecordlincSync
{
    class comman
    {

        public static string TimeCalculat(DateTime dt1, DateTime dt2)
        {
            TimeSpan difference = dt2.TimeOfDay.Add(-dt1.TimeOfDay);

            int hours = difference.Hours;
            int minutes = difference.Minutes;
            int seconds = difference.Seconds;

            return hours + "h " + minutes + "m " + seconds + "s - " + dt1;
        }

    }
}
