using System;
using System.ComponentModel;
using System.Reflection;

namespace BO.Enums
{
    public class Common
    {
        public enum RatingStatus
        {
            PENDING = 0,
            APPROVED,
            DELETED
        }
        public enum PublicReviewPageSize
        {
            PageSize = 5
        }
        public enum MessageType
        {
            Reply = 0,
            Draft = 1,
            Forward = 2
        }
        public enum DisplayMessageType
        {
            Draft = 1,
            Inbox = 2,
            SentBox = 3,
            ReceiveReferral = 4,
            SentReferral = 5
        }
        public enum FileFromFolder
        {
            TempFile_TempFolder = 1,
            TempImage_TempFolder = 2,
            Draft_DraftAttachments = 3,
            Original_ForwardAttachments = 4,
            Banner_Images = 5,
            Gallery_Images = 6,
            ImageBank = 7,
            DentistImage = 8,
            DOCTOR_ATTACHMENTS = 9,
            PATIENT_ATTACHMENTS = 10,
            PATIENT_FILES = 11,
            PATIENT_TEMPORARY_FILES = 12,
            DOCTOR_TEMPORARY_FILES = 13,
            DirectPath = -9
        }
        public enum ComposeMessageType
        {
            Compose = 0,
            Draft = 2,
            Replay = 1,
            Forward = 3,

        }
        public enum FileTypeExtension
        {
            Image = 0,
            File = 1,
        }
        public enum ComposeType
        {
            ColleagueCompose = 1,
            PatientCompose = 2,
        }
        public enum FileFromUserType
        {
            ColleagueFile = 1,
            PatientFile = 2,
        }

        public enum DateFormat
        {
            GENERAL = 1,
            OnlyDate = 2
        }

        public enum StatusType
        {
            Active = 1,
            Inactive = 2
        }
        public enum Features
        {
            Colleagues = 8,
            View_Attachments =9,
            Update_Profile =10,
            Data_Archiving =11,
            Secure_Messaging=12,
            Attachments =13,
            Health_Forms =14,
            Customer_Support=15,
            Activities_Dashboard=16,
            Patient_Forms=17,
            New_Patient_Leads=18,
            OCR = 21
        }

        public enum Landing
        {
            ReferralAutopilotforDentist = 1,
            DentistsGuideToReferrals = 2,
            ReferralMachineForDentists = 3,
            DosAndDontsForDentists = 4,
        }
        public enum MailTemplate
        {
            RequestForIluvMyDentist = 46
        }

        public enum EmailType
        {
            Gmail = 1,
            GmailOAuth2 = 2,
            Outlook = 3,
            OutlookOAuth2 = 4,
            Other = 5,
        }
        public enum InsuranceFees
        {
            FullFeeforServices = 1,
            MembershipPlanFees = 2
        }

        public enum EnumCommunication
        {
            InsuranceCostEstimatorCall = 1,
            InsuranceCostEstimatorSMS = 2

        }
        public enum EnumCallStatus
        {
            Success = 1,
            failed = 2
        }
        public enum RedirectFrom
        {
            PatientHistory = 1
        }
        public enum DentistRewardsType
        {
            Actual = 1,
            Potential = 2
        }

        public enum RewardSource
        {
            MOBILE = 1,
            DENTRIX = 2,
            OTHER = 3
        }
        public enum WebhookEventType
        {
            CustomerEvent = 1,
            FraudEvent = 2,
            PaymentEvent = 3,
            PaymentProfileEvent = 4,
            SubscriptionEvent = 5,
            UnknownEvent = 6,
            InvalidEvent = 7  // this is used if there is a problem comparing the hashes
        }
        public enum transactionIndustryType
        {
            [Description("DB")]
            DirectMarketing,
            [Description("EC")]
            Ecommerce,
            [Description("RE")]
            Retail,
            [Description("RS")]
            Restaurant,
            [Description("LD")]
            Lodging,
            [Description("PT")]
            CarRental,
            [Description("CCD")]
            CorporateCreditDebit,
            [Description("PPD")]
            PrearrangedPaymentDeposit,
            [Description("POP")]
            PointOfPurchase,
            [Description("TEL")]
            TelephoneInitatiedTransactions,
            [Description("WEB")]
            InternetInitiatedEntry
        }

        public enum transactionCategoryType
        {
            [Description("B")]
            BillPayment,
            [Description("R")]
            Recurring,
            [Description("I")]
            Installment,
            [Description("H")]
            Healthcare
        }

        public enum transactionModeType
        {
            [Description("P")]
            CardPresent,
            [Description("N")]
            CardNotPresent
        }

        public enum accounttype
        {
            [Description("R")]
            BrandedCreditCard,
            [Description("E")]
            BrandedDebitCheckingCard,
            [Description("V")]
            BrandedDebitSavingsCard,
            [Description("S")]
            BankSavingsAccountACH,
            [Description("C")]
            BankCheckingAccountACH
        }

        public enum PaymentType
        {
            ZiftPay = 1
        }

        public enum ReferralType
        {
            SENDER = 1,
            PATIENT = 2,
            REFERRED = 3
        }

        public enum FileUploadType
        {
            TempUpload = 1,
            MoveToImageBank = 2,
            DeleteFromTempUpload = 3,
            InsertDraftAttachments = 4,
            ImageBankUpload = 5
        }
        public enum PatientPaymentType
        {
            DentrixFinanceCharges = 1,
            DentrixAdjustments = 2,
            DentrixInsurancePayment = 3,
            DentrixStandardPayment = 4,
            All = 0
        }
        /// <summary>
        /// Indicates the provider type of PMS
        /// </summary>
        public enum PMSProviderType
        {
            Provider = 0,
            Staff = 1
        }

        public enum PMSProviderFilterType
        {
            Provider = 0,
            Staff = 1,
            All = 2
        }

        public enum PatientSortColumnType
        {
            FirstName = 1,
            LastName = 2,
            WorkPhone = 9,
            Email = 10


        }
        public enum GalleryConstants
        {
            IMAGE_URL = 0,
            VIDEO_URL
        }

        public enum RedimRewardStatus
        {
            Success = 0,
            NotEnoughPoints = 1,
            NotFound = 2,
            Failed= 3,
            AmountDoesNotMatch= 4
        }

        public enum PatientProcedureDateFilter
        {
            ProcedureDate = 0,
            CreatedDate = 1
        }

        public enum PatientAppointmentDateFilter
        {
            AppointmentDate = 0,
            automodifiedtimestamp = 1

        }

        public enum FamilyStatus
        {
            IsSuccess = 1,
            IsExist = 2,
            IsFailed = 3

        }
        /// <summary>
        /// Identify for sending SMS on which platform.
        /// </summary>
        public enum SendSMSFrom
        {
            /// <summary>
            /// Send Message From Twilio
            /// </summary>
            Twilio = 0,
            /// <summary>
            /// Send Message From Zipwhip
            /// </summary>
            Zipwhip = 1
        }
        /// <summary>
        /// Identify for the Schedule Type
        /// </summary>
        public enum ScheduleType
        {
            /// <summary>
            /// Provider Schedule Normal
            /// </summary>
            ProviderScheduleNormal = 0,
            /// <summary>
            /// Practice Schedule Normal
            /// </summary>
            PracticeScheduleNormal = 1,
            /// <summary>
            /// Operatory Schedule Normal
            /// </summary>
            OperatoryScheduleNormal = 2,
            /// <summary>
            /// Provider Schedule Exceptional
            /// </summary>
            ProviderScheduleExceptional = 3,
            /// <summary>
            /// Practice Schedule Exceptional
            /// </summary>
            PracticeScheduleExceptional = 4,
            /// <summary>
            /// Operatory Schedule Exceptional
            /// </summary>
            OperatoryScheduleExceptional = 5
        }
    }
    public static class EnumEx
    {
        public static T GetValueFromDescription<T>(string description)
        {
            var type = typeof(T);
            if (!type.IsEnum) throw new InvalidOperationException();
            foreach (var field in type.GetFields())
            {
                var attribute = Attribute.GetCustomAttribute(field,
                    typeof(DescriptionAttribute)) as DescriptionAttribute;
                if (attribute != null)
                {
                    if (attribute.Description == description)
                        return (T)field.GetValue(null);
                }
                else
                {
                    if (field.Name == description)
                        return (T)field.GetValue(null);
                }
            }
            throw new ArgumentException("Not found.", "description");
            // or return default(T);
        }
    }
}
