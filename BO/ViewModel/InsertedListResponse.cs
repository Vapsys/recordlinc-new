﻿using BO.Models;
using System;
using BO.ViewModel;
namespace BO.ViewModel
{
    public class InsertedListResponse<T> : UpdateResponse<T>
    {
        public T InsertedList { get; set; }
        public T NotInsertedList { get; set; }

        public int TotalInsertedRecords { get; set; }
        public int TotalNotInsertedRecords { get; set; }        

        public int ReceivedRecord { get; set; }
        [Obsolete("Don't use this", true)]
        public override T Result { get; set; }
    }
}
