﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class PatientExtraDetails
    {
    }
    public class DentalHistorys
    {
        //public string txtQue1 { get; set; }
        //public string txtQue2 { get; set; }
        //public string txtQue3 { get; set; }
        //public string txtQue4 { get; set; }
        public string DentistName { get; set; }
        public string DentistAddress { get; set; }
        public string DentistPhone { get; set; }
        public string DentistEmail { get; set; }
        public string Last_Teeth_Cleening_Date { get; set; }
        public string Regural_Visit_Dentist { get; set; }
        public bool IsRegural_visit { get; set; }
        //public bool rdQue8 { get; set; }
        //public bool rdQue9 { get; set; }
        //public string txtQue9a { get; set; }
        //public bool rdQue10 { get; set; }
        //public bool Fixedbridge { get; set; }

        //public bool Removeablebridge { get; set; }

        //public bool Denture { get; set; }

        //public bool Implant { get; set; }

        public string Prob_Complain { get; set; }
        public List<string> Teeth_been_Replaced { get; set; }
        public bool clench_your_teeth { get; set; }
        public bool jaw_click_or_pop { get; set; }
        public bool soreness_in_the_muscles { get; set; }
        public bool frequent_headaches { get; set; }
        public bool food_get_caught { get; set; }
        public List<string> teeth_sensitive { get; set; }
        //public bool chkQue20_1 { get; set; }
        //public bool chkQue20_2 { get; set; }
        //public bool chkQue20_3 { get; set; }
        //public bool chkQue20_4 { get; set; }
        public bool Gums_Bleed { get; set; }
        public string str_Gums_Bleed { get; set; }
        public string Brush_your_teeth { get; set; }


        public string Dental_floss { get; set; }
        public bool Loose_tipped_shifted { get; set; }

        public string Your_Teeth_in_general { get; set; }

        public bool isoften_offensive { get; set; }
        //public string txtQue28a { get; set; }
        //public string txtQue28b { get; set; }
        public string str_offensive { get; set; }
        public string orthodontic_work_you_done { get; set; }
        public string Dental_work_you_done { get; set; }
        //public bool rdQue30 { get; set; }

        public string Additional_info { get; set; }

        public string Dateoffirstvisit { get; set; }
        public string Reasonforfirstvisit { get; set; }
        public string Dateoflastvisit { get; set; }
        public string Reasonforlastvisit { get; set; }
        public string Dateofnextvisit { get; set; }
        public string Reasonfornextvisit { get; set; }


    }
    /// <summary>
    /// Ankit Here: 04-03-2018
    /// As Per Discussion with Bruce I have added lsit of string property on it because he need each values of Patient Medical History.
    /// If any boolean values is pass with True then 'Yes' check box is check and if boolean values is pass with false then 'NO' check box is check.
    /// If null pass on boolean values then it assumption that this patient doesn't attempt this.
    /// </summary>
    public class MedicalHistory
    {
        //section-1
        public MedicalHistory()
        {
            following = new Following();
            allergic = new Allergic();
        }
        public bool? under_a_physicians { get; set; }
        //public bool MrNQue1 { get; set; }
        public string str_physicians { get; set; }
        public bool? hospitalized_or_majoroperation { get; set; }
        //public bool? MrnQue2 { get; set; }
        public string str_hospitalized_or_majoroperation { get; set; }
        public bool? head_or_neck_injury { get; set; }
        //public bool? MrnQue3 { get; set; }
        public string str_head_or_neck_injury { get; set; }
        public bool? medications_pills_drugs { get; set; }
        //public bool? MrdQue4 { get; set; }
        public string str_medications_pills_drugs { get; set; }
        public bool? PhenFen_or_Redux { get; set; }
        //public bool? MrnQue5 { get; set; }
        public string str_PhenFen_or_Redux { get; set; }
        public bool? Isbiphosphonates { get; set; }
        //public bool? MrnQue6 { get; set; }
        public string str_biphosphonates { get; set; }
        public bool? Specialdiet { get; set; }
        //public bool? MrnQuediet7 { get; set; }
        public string str_Specialdiet { get; set; }
        public bool? tobacco { get; set; }
        //public bool? Mrnotobacco8 { get; set; }
        public string str_tobacco { get; set; }
        public bool? substances { get; set; }
        //public bool? Mrnosubstances { get; set; }
        public string str_substances { get; set; }
        public bool? pregnant { get; set; }
        //public bool? Mrnopregnant { get; set; }
        public string str_pregnant { get; set; }
        public bool? contraceptives { get; set; }
        //public bool? Mrnocontraceptives { get; set; }
        public string str_contraceptives { get; set; }
        public bool? Nursing { get; set; }
        //public bool? MrnoNursing { get; set; }
        public string str_Nursing { get; set; }
        public bool? tonsils { get; set; }
        //public bool? Mrnotonsils { get; set; }
        public string str_tonsils { get; set; }
        public Allergic allergic { get; set; }
        public Following following { get; set; }
        public string illness { get; set; }
        public string Additional_info { get; set; }

    }
    public class Allergic
    {
        public bool? Aspirin { get; set; }
        public bool? Penicillin { get; set; }
        public bool? Codeine { get; set; }
        public bool? LocalAnesthetics { get; set; }
        public bool? Acrylic { get; set; }
        public bool? Metal { get; set; }
        public bool? Latex { get; set; }
        public bool? SulfaDrugs { get; set; }
        public string Other { get; set; }
    }
   
    public class InsuranceCoverageModel
    {
        public string ResponsiblepartyFname { get; set; }
        public string ResponsiblepartyLname { get; set; }

        public string Emailaddress { get; set; }
        public string Relationship { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
       // public string DOB { get; set; }
        public string PhoneNumber { get; set; }
    }
    public class PDIC
    {
        public string InsuranceCompany { get; set; }
        public string InsuranceCompanyPhone { get; set; }
        public string DateOfBirth { get; set; }
        public string NameOfInsured { get; set; }
        public string MemberId { get; set; }
        public string GroupNumber { get; set; }
    }
    public class SDIC
    {
        public string InsuranceCompany { get; set; }
        public string InsuranceCompanyPhone { get; set; }
        public string DateOfBirth { get; set; }
        public string NameOfInsured { get; set; }
        public string MemberId { get; set; }
        public string GroupNumber { get; set; }
    }

    public class Following
    {
        public bool AIDS_HIV_Positive { get; set; }
        public bool Alzheimer_Disease { get; set; }
        public bool Anaphylaxis { get; set; }

        public bool Anemia { get; set; }
        public bool Angina { get; set; }
        public bool Arthritis_Gout{ get; set; }

        public bool Artificial_Heart_Valve { get; set; }
        public bool Artificial_Joint { get; set; }
        public bool Asthma { get; set; }

        public bool Blood_Disease { get; set; } 
        public bool Blood_Transfusion { get; set; }
        public bool Bone_Disorders { get; set; }


        public bool Breathing_Problem { get; set; }
        public bool Bruise_Easily { get; set; }
        public bool Cancer { get; set; }

        public bool Chemical_Dependancy  { get; set; }
        public bool Chemotherapy { get; set; }
        public bool Chest_Pains { get; set; }

        public bool Cold_Sores_Fever_Blisters { get; set; }
        public bool Congenital_Heart_Disorder { get; set; }
        public bool Convulsions { get; set; }

        public bool Cortisone_Medicine { get; set; }
        public bool Cortisone_Tretments { get; set; }
        public bool Diabetes { get; set; }

        public bool Drug_Addiction { get; set; }
        public bool Easily_Winded { get; set; }
        public bool Emphysema { get; set; }

        public bool Endorcrine_Problems { get; set; }
        public bool Epilepsy_or_Seizures { get; set; }
        public bool Excessive_Bleeding { get; set; }

        public bool Excessive_Thirst { get; set; }
        public bool Excessive_Urination{ get; set; }
        public bool Fainting_Spells_Dizziness { get; set; }

        public bool Frequent_Cough { get; set; }
        public bool Frequent_Diarrhea { get; set; }
        public bool Frequent_Headaches { get; set; }

        public bool Genital_Herpes { get; set; }
        public bool Glaucoma { get; set; }
        public bool Hay_Fever { get; set; }

        public bool Heart_Attack_Failure { get; set; }
        public bool Heart_Murmur { get; set; }
        public bool Heart_Pacemaker { get; set; }

        public bool Heart_Trouble_Disease { get; set; }
        public bool Hemophilia { get; set; }
        public bool Hepatitis_A { get; set; }

        public bool Hepatitis_B_or_C { get; set; }
        public bool Herpes { get; set; }
        public bool High_Blood_Pressure { get; set; }

        public bool High_Cholesterol { get; set; }
        public bool Hives_or_Rash { get; set; }
        public bool Hypoglycemia { get; set; }

        public bool Irregular_Heartbeat { get; set; }
        public bool Kidney_Problems { get; set; }
        public bool Leukemia { get; set; }

        public bool Liver_Disease { get; set; }
        public bool Low_Blood_Pressure { get; set; }
        public bool Lung_Disease { get; set; }

        public bool Mitral_Valve_Prolapse { get; set; }
        public bool Nervous_Disorder { get; set; }
        public bool Osteoporosis { get; set; }

        public bool Pain_in_Jaw_Joints { get; set; }
        public bool Parathyroid_Disease { get; set; }
        public bool Psychiatric_Care { get; set; }

        public bool Radiation_Treatments { get; set; }
        public bool Recent_Weight_Loss { get; set; }
        public bool Renal_Dialysis{ get; set; }

        public bool Rheumatic_Fever { get; set; }
        public bool Rheumatism { get; set; }
        public bool Scarlet_Fever { get; set; }

        public bool Shingles { get; set; }
        public bool Sickle_Cell_Disease { get; set; }
        public bool Sinus_Trouble { get; set; }

        public bool Spina_Bifida { get; set; }
        public bool Stomach_Intestinal_Disease { get; set; }
        public bool Stroke { get; set; }

        public bool Swelling_of_Limbs { get; set; }
        public bool Thyroid_Disease { get; set; }
        public bool Tuberculosis { get; set; }
        public bool Tonsillitis { get; set; }

        
        public bool Tumors_or_Growths { get; set; }
        public bool Ulcers { get; set; }

        public bool Venereal_Disease { get; set; }
        public bool Yellow_Jaundice { get; set; }
       
    }



}
