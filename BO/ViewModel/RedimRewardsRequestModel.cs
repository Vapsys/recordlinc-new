﻿using System;
using System.ComponentModel.DataAnnotations;
using BO.CustomAttributes;
namespace BO.ViewModel
{
    /// <summary>
    /// 
    /// </summary>
    public class RedimRewardsRequestModel
    {
        /// <summary>
        /// Patient Id of Reward Platform
        /// </summary>
        [Required]
        public int RewardPartnerId { get; set; }
        /// <summary>
        /// Number of points (actual reward) to redim
        /// </summary>
        [Required]
        public double PointsToRedim { get; set; }
        /// <summary>
        /// Amount to be redim
        /// </summary>
        [Required]
        public double AmountToRedim { get; set; }
        /// <summary>
        /// Date of Transaction when rewards are redimed. It must be grater then 2000-01-01.
        /// </summary>
        [Required(ErrorMessage = "Transaction date should not be empty.")]
        [MinValidateDateRange(FirstDate = "2000-01-01")]
        public DateTime TransactionDate { get; set; }
        /// <summary>
        /// Transaction Id of Mobile Platform (or External System) 
        /// </summary>
        [Required]
        public string TransactionId { get; set; }
    }

   
}
