﻿using static BO.Enums.Common;

namespace BO.ViewModel
{
    public class GetSpecificSearchModel
    {
        public GetSpecificSearchModel()
        {
            ProviderfilterType = PMSProviderFilterType.All;
        }
        public string Keywords { get; set; }
        public PMSProviderFilterType ProviderfilterType { get; set; }
    }
}
