﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class PatientInsuranceDetails
    {
        public int PatientId { get; set; }
        public string Office { get; set; }
        public DateTime Date { get; set; }
        public PatientSubscriber objPatientSubscriber { get; set; }
        public InsuranceCompany objInsuranceCompany { get; set; }
        public InsuranceInformation objInsuranceInformation { get; set; }
        public DeductiblesMaximums objDeductiblesMaximums { get; set; }
        public Coverage objCoverage { get; set; }
        public Frequency objFrequency { get; set; }
    }

    public class PatientSubscriber
    {
        public string PatientSubscriberId { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientMiddleName { get; set; }
        public string PatientLastName { get; set; }
        public string PatientSuffix { get; set; }
        public DateTime PatientDateOfBirth { get; set; }
        public string SubscriberFirstName { get; set; }
        public string SubscriberLastName { get; set; }
        public string SubscriberSuffix { get; set; }
        public DateTime SubscriberDateOfBirth { get; set; }
    }
    public class InsuranceCompany
    {
        public int InsuranceCompanyId { get; set; }
        public string InsuranceCompanyName { get; set; }
        public string ClaimsMailingAddress { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string PhoneNumber { get; set; }
    }
    public class InsuranceInformation
    {
        public int InsuranceInformationId { get; set; }
        public string PayerID { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string GroupNumber { get; set; }
        public DateTime TerminationDate { get; set; }
        public string PlanName { get; set; }
        public string FeeSchedule { get; set; }
    }
    public class DeductiblesMaximums
    {
        public int DeductiblesMaximumsId { get; set; }
        public string BenefitPeriod { get; set; }
        public string BenefitYearStarts { get; set; }
        public string YearlyMaximum { get; set; }
        public string RemainingBenefits { get; set; }
        public string IndividualDeductible { get; set; }
        public string IndividualDeductibleMet { get; set; }
        public string FamilyDeductible { get; set; }
        public string FamilyDeductibleMet { get; set; }
        public string DeductibleAppliestoPreventive { get; set; }
        public string Basic { get; set; }
        public string Major { get; set; }
    }
    public class Coverage
    {
        public int CoverageId { get; set; }
        public string Diagnostic { get; set; }
        public string Endo { get; set; }
        public string Preventive { get; set; }
        public string Perio { get; set; }
        public string Basic { get; set; }
        public string OralSurgery { get; set; }
        public string Major { get; set; }
        public string Implant { get; set; }
        public string Ortho { get; set; }
        public string OrthoMaximum { get; set; }
        public string OrthoBillingRequirements { get; set; }
    }
    public class Frequency
    {
        public int FrequencyId { get; set; }
        public string Exam { get; set; }
        public string FullMouthXRaysPano { get; set; }
        public string SharesBenefits { get; set; }
        public string Prophy { get; set; }
        public string Fluoride { get; set; }
        public int AgeLimitlessthan { get; set; }
    }
}
