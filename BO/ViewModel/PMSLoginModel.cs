﻿using BO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class PMSLoginModel
    {
        public string Token { get; set; }

        public List<DentrixConnectors> DentrixConnectors { get; set; }
    }
}
