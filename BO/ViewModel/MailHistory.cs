﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class MailHistory
    {
        public int UserId { get; set; }
        public int PatientId { get; set; }
        public int Count { get; set; }
        public int TemplateId { get; set; }
    }
}
