﻿using BO.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BO.ViewModel
{
    public class DentistProfileDetail
    {
        public int UserId { get; set; }
        public string EncryptUserId { get; set; }

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string ImageName { get; set; }
        public string EmailId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string WebsiteURL { get; set; }
        public string OfficeName { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public int LocationId { get; set; }
        public double RatingValue { get; set; }
        public List<WebsiteDetails> lstsecondarywebsitelist = new List<WebsiteDetails>();
        public List<SocialMediaDetails> lstGetSocialMediaDetailByUserId = new List<SocialMediaDetails>();
        public List<EducationandTraining> lstEducationandTraining = new List<EducationandTraining>();
        public List<ProfessionalMemberships> lstProfessionalMemberships = new List<ProfessionalMemberships>();
        public List<TeamMemberDetailsForDoctor> lstTeamMemberDetailsForDoctor = new List<TeamMemberDetailsForDoctor>();
        public List<SpeacilitiesOfDoctor> lstSpeacilitiesOfDoctor = new List<SpeacilitiesOfDoctor>();
        public List<AddressDetails> lstDoctorAddressDetails = new List<AddressDetails>();
        public List<AddressDetails> lstDoctorSortAddressDetails = new List<AddressDetails>();
        public List<Banner> lstBannerDetails = new List<Banner>();
        public List<InsuranceDetails> lstInsuranceDetails = new List<InsuranceDetails>();
        public List<GallaryDetail> lstGallaryDetails = new List<GallaryDetail>();
        public List<Ratings> lstratingList = new List<Ratings>();
        public List<SpecialOffers> lstOffers = new List<SpecialOffers>();
        public List<LocationForAppointment> LocationList { get; set; }
        public int RatingCount { get; set; }
        public List<AppointmentService> AppointmentServiceList { get; set; }
        public int AppointmentServiceId { get; set; }
        public int AppointmentDefaultResourceId { get; set; }
        public string PublicProfileUrl { get; set; }
        public string TimezoneSystemName { get; set; }
        public string Mobile { get; set; }
        public string Country { get; set; }
        public string Salutation { get; set; }
        public string Address { get; set; }
        public List<MemberPlanDetail> PlanDetails { get; set; }
        public LDJSON ObjLDJSON { get; set; }
    }
    public class SpeacilitiesOfDoctor
    {
        public int SpecialtyId { set; get; }
        public string SpecialtyDescription { set; get; }
    }
    public class InsuranceDetails
    {
        public string Name { set; get; }
        public string Logo { set; get; }
        public string Description { set; get; }
        public string Link { set; get; }
    }
    public class EducationandTraining
    {
        public int Id { get; set; }
        public string Institute { get; set; }
        public string Specialisation { get; set; }
        public string YearAttended { get; set; }
    }
    public class ProfessionalMemberships
    {
        public int Id { get; set; }
        public string Membership { get; set; }
    }
    public class AddressDetails
    {
        public int AddressInfoID { get; set; }
        public string ExactAddress { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string EmailAddress { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public int ContactType { get; set; }
        public string Location { get; set; }
        public int TimeZoneId { get; set; }
        public string Mobile { get; set; }
        public string Website { get; set; }
        public string ExternalPMSId { get; set; }
        public string SchedulingLink { get; set; }
        public List<StateList> StateList { get; set; }
        public List<SelectListItem> CountryList { get; set; }
        public List<TimeZones> TimeZoneList { get; set; }
    }



    public class TeamMemberDetailsForDoctor
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Image { get; set; }
        public string SpecialtyDescription { get; set; }
        public string PublicProfileUrl { get; set; }
        public string AccountName { get; set; }
        public double RatingValue { get; set; }
    }
    public class LicenseDetailsForDoctor
    {

        public int UserId { get; set; }
        public string LicenseNumber { get; set; }
        public string LicenseState { get; set; }
        public DateTime? LicenseExpiration { get; set; }
        public string LicenseExpirationDisplay { get; set; }


    }
    public class WebsiteDetails
    {
        public int SecondaryWebsiteId { get; set; }
        public int UserId { get; set; }
        public string SecondaryWebsiteurl { get; set; }

    }
    public class PatientsSignupEmail
    {
        [Required(ErrorMessage = "Please enter email address.")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string Email { get; set; }
    }
    public class UserSignupEmail
    {
        [Required(ErrorMessage = "Please enter your email address.")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string Email { get; set; }
    }
    public class MDLPatientSignUp
    {
        public int PublicProfileUserId { get; set; }
        public string PublicProfileUserName { get; set; }

        [Required(ErrorMessage = "First name is required.")]
        [Display(Name = "First Name")]
        [StringLength(30, ErrorMessage = "First name must not exceed 30 characters.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required.")]
        [StringLength(30, ErrorMessage = "Last name must not exceed 30 characters.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email address is required.")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Invalid email address.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Phone number is required.")]
        [Display(Name = "Phone")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = " ")]
        public string Phone { get; set; }
        public string Password { get; set; }

        public string PromoCode { get; set; }
        public string OfferID { get; set; }

        public int PatientId { get; set; }
        public string SucessOrErrorMessage { get; set; }
        public PatientContactInfo ObjContactInfo = new PatientContactInfo();
        public string CompanyWebSite { get; set; }
    }

    public class PatientContactInfo
    {

        public int PatientId { get; set; }
        [Display(Name = "Address")]
        public string Address { get; set; }
        [Display(Name = "City")]
        public string City { get; set; }
        [Display(Name = "State")]
        public string State { get; set; }
      
        [Display(Name = "Emergency Contact Name")]
        public string EMGContactName { get; set; }
        [Display(Name = "ZipCode")]
        [RegularExpression(@"\d{5}$", ErrorMessage = "Invalid zip code.")]
        public string ZipCode { get; set; }
        [Display(Name = "Gender")]
        [Required(ErrorMessage = "Please select gender.")]
        public int Gender { get; set; }

        [Display(Name = "Date of Birth")]
        public string BOD { get; set; }
        //[Display(Name = "Emergency Contact Phone Number")]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Phone number is invalid.")]
        public string EMGContactNo { get; set; }
        public FamilyInformation objFamilyInformation { get; set; }
        public string PrimaryPhoneNo { get; set; }
        public string SecondaryPhoneNo { get; set; }
        public string Email { get; set; }
    }
    
    public class PatientLogin
    {
        [Required(ErrorMessage = "Please enter email address.")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter password.")]
        [DataType(DataType.Password)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
    public class GallaryDetail
    {
        public string MediaType { get; set; }
        public string MediaURL { get; set; }
        public int GallaryId { get; set; }
        public int UserId { get; set; }

    }
    public class FamilyInformation
    {
        [Display(Name = "Spouse First Name")]
        public string SpouseFirstName { get; set; }
        [Display(Name = "Spouse Last Name")]
        public string SpouseLastName { get; set; }
        public string Relationship { get; set; }

        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Phone number is invalid.")]
        public string Phone { get; set; }
        [EmailAddress(ErrorMessage = "Email address is invalid.")]
        public string Email { get; set; }
        [Display(Name = "Child First Name")]
        public string ChildFirstName { get; set; }
        [Display(Name = "Child Last Name")]
        public string ChildLastName { get; set; }
    }
    public class LDJSON
    {
        public AgregateRating aggregateRating { get; set; }
        public List<Review> review { get; set; }
        public string PriceRange { get; set; }
        public string Name { get; set; }
        public address address { get; set; }
        [JsonProperty(PropertyName = "@context")]
        public string context { get; set; }
        [JsonProperty(PropertyName = "@type")]
        public string type { get; set; }
        public string telephone { get; set; }
        public string image { get; set; }
    }
    public class AgregateRating
    {
        public int ReviewCount { get; set; }
        public string @type { get; set; }
        public double RatingValue { get; set; }
    }
    public class address
    {
        public string addressLocality { get; set; }
        public string addressRegion { get; set; }
        public string streetAddress { get; set; }
        public string postalCode { get; set; }
        public string addressCountry { get; set; }
    }
    public class Review
    {
        public reviewRating reviewRating { get; set; }
        public string description { get; set; }
        public string author { get; set; }
    }
    public class reviewRating
    {
        public double ratingValue { get; set; }
    }
    public class MDLPatientSignUpDetail
    {
        public int PublicProfileUserId { get; set; }
        public string PublicProfileUserName { get; set; }

        [Required(ErrorMessage = "First name is required.")]
        [Display(Name = "First Name")]
        [StringLength(30, ErrorMessage = "First name must not exceed 30 characters")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last name is required.")]
        [StringLength(30, ErrorMessage = "Last name must not exceed 30 characters")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email address is required.")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        public string Email { get; set; }

        //RM-450 Add Masking to patient health history field.
        //[Display(Name = "Phone")]
        //[DataType(DataType.PhoneNumber)]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Phone number is invalid.")]
        public string Phone { get; set; }

        public string PromoCode { get; set; }

        public string OfferID { get; set; }

        public int PatientId { get; set; }
        public string Password { get; set; }
        
        public string SucessOrErrorMessage { get; set; }
        public PatientContactInfo ObjContactInfo { get; set; }
        public BO.Models.InsuranceCoverage objInsuranceCoverage { get; set; }      
        public MediacalHisotry objHistory { get; set; }
        public DentalHistory objdentalhistory { get; set; }
       
    }
}

