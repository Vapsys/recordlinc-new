﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class ResponseMessages
    {
        public string YourSelfInvite { get; set; }
        public string FailInvite { get; set; }
        public string XlsError { get; set; }
        public List<ResponseEmails> objListResponseEmail { get; set; }
        public string EmailRequired { get; set; }
    }

    public class ResponseEmails
    {
        public string EmailId { get; set; }
        public int Id { get; set; }
    }
}
