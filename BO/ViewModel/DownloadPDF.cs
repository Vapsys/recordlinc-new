﻿using BO.Models;
using System;
using System.Collections.Generic;

namespace BO.ViewModel
{
    /// <summary>
    /// This model used for wrap all the model which used for Download as PDF functionality of Conversation view.
    /// </summary>
    public class DownloadPDF
    {
        /// <summary>
        /// This property has records of Referral related.
        /// </summary>
        public ReferralMessage ReferralDetails { get; set; }
        /// <summary>
        /// This property has records of OfficeCommunication sections of Conversation.
        /// </summary>
        public List<OneClickReferralDetail> OfficeCommunication { get; set; }
        /// <summary>
        /// This property has list of SMS wia zipwhip for the Text Messenger section into Conversation view.
        /// </summary>
        public List<SMSDetails> TextMessenger { get; set; }
        /// <summary>
        /// This property get all the Patient related details.
        /// </summary>
        public PatientHistoryViewModel PatientHistory { get; set; }
        /// <summary>
        /// This property has list of records of Future Appointment for the Patient.
        /// </summary>
        public List<Appointmentslist> FutureAppointments { get; set; }
        /// <summary>
        /// This property has list of records of Past Appointment for the Patient.
        /// </summary>
        public List<Appointmentslist> PastAppointments { get; set; }
    }
    /// <summary>
    /// This model used for appointment related data.
    /// </summary>
    public class Appointmentslist
    {
        /// <summary>
        /// Appointment Id
        /// </summary>
        public int AppointmentId { get; set; }
        /// <summary>
        /// Doctor Name
        /// </summary>
        public string Doctorname { get; set; }
        /// <summary>
        /// UserId
        /// </summary>
        public int DoctorId { get; set; }
        /// <summary>
        /// Appointment Date
        /// </summary>
        public DateTime AppointmentDate { get; set; }
        /// <summary>
        /// Appointment Time
        /// </summary>
        public string AppointmentTime { get; set; }
        /// <summary>
        /// Service Type of Appointment
        /// </summary>
        public string ServiceType { get; set; }
        /// <summary>
        /// Status of Appointment.
        /// </summary>
        public string Status { get; set; }
    }
    /// <summary>
    /// This model used for Request data supply into the API method.
    /// </summary>
    public class DownloadPDFFilter
    {
        /// <summary>
        /// Referral Message Id.
        /// </summary>
        public int MessageId { get; set; }
        /// <summary>
        /// Message Type is defined as SENT or INBOX
        /// </summary>
        public string MessageType { get; set; }
        /// <summary>
        /// Dentist Id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// PatientId
        /// </summary>
        public int PatientId { get; set; }
    }
}
