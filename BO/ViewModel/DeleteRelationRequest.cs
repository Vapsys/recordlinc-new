﻿namespace BO.ViewModel
{
    public class DeleteRelationRequest
    {
        /// <summary>
        /// Patient whose relation to be deleted with <see cref="ToPatientId"/>.
        /// </summary>
        public int FromPatientId { get; set; }
        /// <summary>
        /// Patient whose relation to be deleted with <see cref="FromPatientId"/>
        /// </summary>
        public int ToPatientId { get; set; }
    }
}
