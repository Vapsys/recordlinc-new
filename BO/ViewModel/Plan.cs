﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class Plan
    {
        public int PlanId { get; set; }
        public string PlanName { get; set; }
        public decimal Amount { get; set; }
        public decimal RegisterReward { get; set; }
        public bool IsPrimary { get; set; }
        public decimal Points { get; set; }
        public int ExamCodes { get; set; }
    }
}
