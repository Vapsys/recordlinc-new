﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class DentistReward
    {
        public int SettingId { get; set; }
        public string RewardCode { get; set; }
        public string RewardText { get; set; }
        public string StartRange { get; set; }
        public string EndRange { get; set; }
        public string OptionId { get; set; }
        public int RewardMasterId { get; set; }
    }
}
