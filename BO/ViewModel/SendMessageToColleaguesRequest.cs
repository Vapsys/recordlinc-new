﻿namespace BO.ViewModel
{
    public class SendMessageToColleaguesRequest
    {
        public int UserId { get; set; }
        public ComposeMessage objcomposedetail { get; set; }
        public string NewTempFileUpload { get; set; }
        public string ImageBankFileUpload { get; set; }
        public string ThumbFileUpload { get; set; }
        public string DraftFileUpload { get; set; }
    }

}
