﻿using System;
using System.ComponentModel.DataAnnotations;
namespace BO.ViewModel
{
    /// <summary>
    /// Get Schedule data of Dentist
    /// </summary>
    public class GetScheduleDataModel
    {
        /// <summary>
        /// UserId of Dentist
        /// </summary>
        [Required]
        public string UserId { get; set; }
        /// <summary>
        /// Location Id
        /// </summary>
        public int LocationId { get; set; }
        /// <summary>
        /// From Date
        /// </summary>
        public DateTime? FromDate { get; set; }
        /// <summary>
        /// To Date
        /// </summary>
        public DateTime? ToDate { get; set; }
        /// <summary>
        /// SortColumn
        /// </summary>
        public string SortColumn { get; set; }
        /// <summary>
        /// Sort Direction
        /// </summary>
        public string SortDirection { get; set; }
        /// <summary>
        /// Page Index
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// Page Size
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// Search Text
        /// </summary>
        public string SearchText { get; set; }
        /// <summary>
        /// Patient Name
        /// </summary>
        public string PatientName { get; set; }
        /// <summary>
        /// Page Age
        /// </summary>
        public int PatientAge { get; set; }
        /// <summary>
        /// Doctor Name
        /// </summary>
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DOB { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
    /// <summary>
    /// Patient list baised on Arravial Appointments for Verdient
    /// </summary>
    public class AppointmentReport
    {
        /// <summary>
        /// UserId / Dentist id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Location Id
        /// </summary>
        public int LocationId { get; set; }
        /// <summary>
        /// From Date
        /// </summary>
        public DateTime? FromDate { get; set; }
        /// <summary>
        /// To Date
        /// </summary>
        public DateTime? ToDate { get; set; }
        /// <summary>
        /// SortColumn
        /// </summary>
        public string SortColumn { get; set; }
        /// <summary>
        /// Sort Direction
        /// </summary>
        public string SortDirection { get; set; }
        /// <summary>
        /// Page Index
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// Page Size
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// Search Text
        /// </summary>
        public string SearchText { get; set; }
    }
}
