﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    /// <summary>
    /// Verident View model
    /// </summary>
    public class Verident
    {
        /// <summary>
        /// No of Records
        /// </summary>
        public int TotalRecord { get; set; }
        /// <summary>
        /// List of Patient
        /// </summary>
        public List<PatientDetailsforVer> patients { get; set; }
    }


    public class PatientList
    {
        public int TotalRecord { get; set; }
        public List<PatientDetailsforVer> lstPatient { get; set; }
    }
    /// <summary>
    /// Appointment list for verident 
    /// </summary>
    public class PatientDetailsforVer
    {
        public int DentalId { get; set; }
        public int PatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string DateOfBirth { get; set; }
        public string AppointmentDate { get; set; }
        public string Phone { get; set; }
        public string MemberId { get; set; }
        public bool HasInsurance { get; set; }
        public string InsuranceCompanyName { get; set; }
        public string LastVerified { get; set; }
        public bool IsStar { get; set; }
        public string AttachedLabel { get; set; }
        public string PayerId { get; set; }
        public string GroupNumber { get; set; }
    }
    public class PatientDetailsForVeri
    {
        public int PatientId { get; set; }
        [Required(ErrorMessage = "Please enter first name")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Please enter last name")]
        public string LastName { get; set; }
        public string Email { get; set; }
        [Required(ErrorMessage = "Please enter Date Of Birth")]
        public string DateOfBirth { get; set; }
        public string Phone { get; set; }
        public string MemberId { get; set; }
        [Required(ErrorMessage = "Please enterInsurance Company Name")]
        public string InsuranceCompanyName { get; set; }
        public string NameofInsured { get; set; }
        public string InsuranceCompanyPhone { get; set; }
        public string GroupNumber { get; set; }
    }
    /// <summary>
    /// Patient list filter for verident  
    /// </summary>
    public class PatientFilterForVeri
    {
        public int UserId { get; set; }
       // [Range(1, 50, ErrorMessage = "Sort Column Must be non negative or between 1 to 50")]
        public int SortColumn { get; set; }
       // [Range(1, 2, ErrorMessage = "Sort Direction Must be non negative or between 1 or 2")]
        public int SortDirection { get; set; }
      //  [Range(1, Int64.MaxValue, ErrorMessage = "Page Index Must be non negative.")]
        public int PageIndex { get; set; }
       // [Range(1, Int64.MaxValue, ErrorMessage = "Page Size Must be non negative.")]
        public int PageSize { get; set; }
        public string SearchText { get; set; }
        //public int LocationId { get; set; }
      //  [Range(0, 2, ErrorMessage = "Is InsVerify Must be non negative or between 0 or 2.")]
        public int IsInsVerify { get; set; }
        public bool PatientWithApp { get; set; }
        public List<string> LabelId { get; set; }
    }

    public class Labels
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Please enter label name.")]
        public string Name { get; set; }
    }


    public class VeridentLogin
    {
        [Required(ErrorMessage = "Please enter Username.")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Please enter Password.")]
        public string Password { get; set; }
    }

    public class VeridentUserDetails
    {
        public string Token { get; set; }
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImageUrl { get; set; }
        public string AccountName { get; set; }
        public int AccountId { get; set; }
        public string NPINumber { get; set; }
    }

    public class VeridentUser {
        public MessageDetais messageDetais { get; set; }
        public VeridentUserDetails veridentUserDetails { get; set; }
    }

    public class MessageDetais
    {
        public int Id { get; set; }
        public string Message { get; set; }
    }

    public class DentalRegistarionDetails
    {
        public string txtResponsiblepartyFname { get; set; }
        public string txtResponsiblepartyLname { get; set; }
        public string txtResponsibleRelationship { get; set; }
        public string txtResponsibleAddress { get; set; }
        public string txtResponsibleCity { get; set; }
        public string txtResponsibleState { get; set; }
        public string txtResponsibleZipCode { get; set; }
        public string txtResponsibleDOB { get; set; }
        public string txtResponsibleContact { get; set; }
        public string txtPatientPresentPosition { get; set; }
        public string txtPatientHowLongHeld { get; set; }
        public string txtParentPresentPosition { get; set; }
        public string txtParentHowLongHeld { get; set; }
        public string txtDriversLicense { get; set; }
        public string chkMethodOfPayment { get; set; }
        public string txtPurposeCall { get; set; }
        public string txtOtherFamily { get; set; }
        public string txtWhommay { get; set; }
        public string txtSomeonetonotify { get; set; }
        public string txtemergencyname { get; set; }
        public string txtemergency { get; set; }
        public string txtEmployeeName1 { get; set; }
        public string txtInsurancePhone1 { get; set; }
        public string txtEmployeeDob1 { get; set; }
        public string txtEmployerName1 { get; set; }
        public string txtYearsEmployed1 { get; set; }
        public string txtNameofInsurance1 { get; set; }
        public string txtInsuranceAddress1 { get; set; }
        public string txtInsuranceTelephone1 { get; set; }
        public string txtProgramorpolicy1 { get; set; }
        public string txtSocialSecurity1 { get; set; }
        public string txtUnionLocal1 { get; set; }
        public string txtEmployeeName2 { get; set; }
        public string txtInsurancePhone2 { get; set; }
        public string txtEmployeeDob2 { get; set; }
        public string txtEmployerName2 { get; set; }
        public string txtYearsEmployed2 { get; set; }
        public string txtNameofInsurance2 { get; set; }
        public string txtInsuranceAddress2 { get; set; }
        public string txtInsuranceTelephone2 { get; set; }
        public string txtEmployeeName3 { get; set; }
        public string txtInsurancePhone3 { get; set; }
        public string MedicalNameOfInsured { get; set; }
        public string MedicalDOB { get; set; }
        public string MedicalMemberID { get; set; }
        public string MedicalGroupNumber { get; set; }
        public string txtProgramorpolicy2 { get; set; }
        public string txtSocialSecurity2 { get; set; }
        public string txtUnionLocal2 { get; set; }
        public string txtDigiSign { get; set; }
        public string txtemailaddress { get; set; }
    }


    public class UserAccountPro{
        [Required(ErrorMessage = "Please provide email")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Please provide new password")]
        public string Password { get; set; }
    }
}
