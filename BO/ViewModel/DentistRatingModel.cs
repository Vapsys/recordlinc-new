﻿using BO.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace BO.ViewModel
{
    public class DentistRatingModel
    {
        public DentistRatingModel()
        {
            SortFieldList = new List<SelectListItem>();
        }
        public RatingObjects ratingObject { get; set; }
        public Ratings ratings { get; set; }
        public List<Ratings> ratingList { get; set; }
        public BO.Enums.Common.RatingStatus StatusList { get; set; }
        public IEnumerable<SelectListItem> SortFieldList { get; set; }
        public int totalRatings { get; set; }
       
    }
}
