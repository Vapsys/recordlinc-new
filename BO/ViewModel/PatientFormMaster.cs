﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class PatientFormMaster
    {
        public BasicInfoMaster BasicInfo { get; set; }
        public InsuranceDetailMaster InsuranceDetails { get; set; }
        public DentalHistoryMaster DentalHistory { get; set; }
        public MedicalHistoryMaster MedicalHistory { get; set; }
    }
}
