﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class PublicProfileSection
    {
        public int SectionId { get; set; }
        public int UserId { get; set; }
        public bool PatientForms { get; set; }
        public bool SpecialOffers { get; set; }
        public bool ReferPatient { get; set; }
        public bool Reviews { get; set; }
        public bool AppointmentBooking { get; set; }
        public bool PatientLogin { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
    }
}
