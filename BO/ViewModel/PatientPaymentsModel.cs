﻿using BO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class PatientPaymentsModel
    {
        public IEnumerable<Patient_DentrixFinanceCharges> lstPatient_DentrixFinanceCharges { get; set; }
        public IEnumerable<Patient_DentrixAdjustments> lstPatient_DentrixAdjustments { get; set; }
        public IEnumerable<Patient_DentrixInsurancePayment> lstPatient_DentrixInsurancePayment { get; set; }
        public IEnumerable<Patient_DentrixStandardPayment> lstPatient_DentrixStandardPayment { get; set; }
    }
}
