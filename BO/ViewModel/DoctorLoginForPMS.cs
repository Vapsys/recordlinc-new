﻿
namespace BO.ViewModel
{
    public class DoctorLoginForPMS
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }

    public class ResponceReferPatientModel
    {
        public bool IsIntegration { get; set; }
        public int? ToColleague { get; set; }
        public int? UserId { get; set; }
        public string Token { get; set; }
        public int? PatientId { get; set; }
        

    }
}
