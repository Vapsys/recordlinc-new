﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class PatientInsuranceFilter
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public List<int?> SubscriberId { get; set; }
        public List<int?> CarrierId { get; set; }
        public List<string> ConnectorId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? PageSize { get; set; }
        public int? Page { get; set; }
       

    }

    //public class PatientInsuranceDetails
    //{

    //}
}
