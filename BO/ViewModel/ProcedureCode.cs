﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
  public class ProcedureCode
    {
        public int ProcedureCodeId { get; set; }
        public string Procedurecode { get; set; }
        public string ProcedureName { get; set; }
        public decimal SuperBucksReward { get; set; }
        public int AccountId { get; set; }
        public bool IsExam { get; set; }
        public int PlanId { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }

        public string PlanName { get; set; }
        public bool IsChecked { get; set; }
    }
}
