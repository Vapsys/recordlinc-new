﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class SpecialityMaster
    {
        public SpecialityMaster()
        {
            lstServices = new List<ServiceMasterModel>();
        }
        public int Speciality_Id { get; set; }
        public string Speciality_Name { get; set; }
        public string Speciality_MapFieldName { get; set; }
        public bool Speciality_IsEnable { get; set; }
        public int Speciality_SortOrder { get; set; }
        public List<ServiceMasterModel> lstServices { get; set; }
    }
}
