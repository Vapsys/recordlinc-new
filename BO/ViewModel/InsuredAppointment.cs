﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class InsuredAppointment
    {
        public int PatientId { get; set; }
        public string PatientName { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string AppointmentDate { get; set; }
        public string PatientDOB { get; set; }
        public string PrimaryInsuredName {get;set;}
        public string SecondaryInsuredName { get; set; }
        public string MedicalInsuredName { get; set; }
        public string PrimaryInsuredDOB { get; set; }
        public string SecondaryInsuredDOB { get; set; }
        public string MedicalInsuredDOB { get; set; }
        public string PrimaryInsuranceCompanyName { get; set; }
        public string SecondaryInsuranceCompanyName { get; set; }
        public string MedicalInsuranceCompanyName { get; set; }
        public string PrimaryMemberID { get; set; }
        public string SecondaryMemberID { get; set; }
        public string MedicalMemberID { get; set; }
        public string PrimaryCompanyPhone { get; set; }
        public string SecondaryCompanyPhone { get; set; }
        public string MedicalCompanyPhone { get; set; }


    }
}
