﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
     
    #region User Login
    /// <summary>
    /// Use for zipwhip login api response
    /// </summary>
    public class UserLogin
    {
        /// <summary>
        /// if without error execute api then true other wise false
        /// </summary>
        public bool success { get; set; }
        /// <summary>
        /// provide token for send sms
        /// </summary>
        public string response { get; set; }
        /// <summary>
        /// provide sessions key
        /// </summary>
        public object sessions { get; set; }
    }
    #endregion

    #region User Logout
    /// <summary>
    /// user logout response class
    /// </summary>
    public class UserLogout
    {
        /// <summary>
        /// if without error execute api then true other wise false
        /// </summary>
        public bool success { get; set; }
        /// <summary>
        /// error message
        /// </summary>
        public object response { get; set; }
    }
    #endregion

    #region Message Send 
    /// <summary>
    /// sent message response object 
    /// </summary>
    public class MessageSendToken
    {
        /// <summary>
        /// for message id
        /// </summary>
        public string message { get; set; }
        /// <summary>
        /// for finger print 
        /// </summary>
        public string fingerprint { get; set; }
        /// <summary>
        /// for device information code
        /// </summary>
        public int device { get; set; }
        /// <summary>
        /// for mobile number
        /// </summary>
        public long contact { get; set; }
    }

    /// <summary>
    /// message send response class
    /// </summary>
    public class MessageSendResponse
    {
        /// <summary>
        /// for device information code
        /// </summary>
        public string fingerprint { get; set; }
        /// <summary>
        /// root id
        /// </summary>
        public string root { get; set; }
        /// <summary>
        /// for tokens object
        /// </summary>
        public List<MessageSendToken> tokens { get; set; }
    }

    /// <summary>
    /// message send response class
    /// </summary>
    public class MessageSend
    {
        /// <summary>
        /// if without error execute api then true other wise false
        /// </summary>
        public bool success { get; set; }
        /// <summary>
        /// message send response object
        /// </summary>
        public MessageSendResponse response { get; set; }
    }
    #endregion

    #region Provision Eligible/Add/Status
    /// <summary>
    /// provision response class property
    /// </summary>
    public class ProvisionEligible
    {
        /// <summary>
        /// zipwhip transaction id
        /// </summary>
        public string transaction_id { get; set; }
        /// <summary>
        /// api status code
        /// </summary>
        public string status_code { get; set; }
        /// <summary>
        /// api response description
        /// </summary>
        public string status_desc { get; set; }
        /// <summary>
        ///  return true - false
        /// </summary>
        public string error { get; set; }
        /// <summary>
        ///  return true - false
        /// </summary>
        public string eligible { get; set; }
        /// <summary>
        /// return account status activated or not
        /// </summary>
        public string account_status { get; set; }
        /// <summary>
        /// last updated date
        /// </summary>
        public string last_updated { get; set; }
        /// <summary>
        /// effective date
        /// </summary>
        public string effective_date { get; set; } // For Provision Add
        /// <summary>
        /// password for access zipwhip account
        /// </summary>
        public string password { get; set; } // For Provision Add
    }
    #endregion

    #region Webhook Add/List/Delete

    /// <summary>
    /// for webhook response class
    /// </summary>
    public class WebhookResponse
    {
        /// <summary>
        /// the id of an installed web hook
        /// </summary>
        public int webhookId { get; set; }
        /// <summary>
        /// type like message, mms
        /// </summary>
        public string type { get; set; }
        /// <summary>
        ///  webhook type like send,receive
        /// </summary>
        public string @event { get; set; }
        /// <summary>
        /// webhook url
        /// </summary>
        public string url { get; set; }
        /// <summary>
        /// api method like get,post,put
        /// </summary>
        public string method { get; set; }
    }

    /// <summary>
    /// webhook response class
    /// </summary>
    public class Webhook
    {
        /// <summary>
        /// webhook response object
        /// </summary>
        public List<WebhookResponse> response { get; set; }
        /// <summary>
        /// return true-false value
        /// </summary>
        public bool success { get; set; }
        /// <summary>
        /// for error description
        /// </summary>
        public bool errorDesc { get; set; }
        
    }
    #endregion

    #region Commented Class
    #region Provision Add
    //public class ProvisionAdd
    //{
    //    public string transaction_id { get; set; }
    //    public string status_code { get; set; }
    //    public string status_desc { get; set; }
    //    public string error { get; set; }
    //    public string effective_date { get; set; }
    //    public string password { get; set; }
    //}
    #endregion

    #region Provision Status
    //public class ProvisionStatus
    //{
    //    public string transaction_id { get; set; }
    //    public string status_code { get; set; }
    //    public string status_desc { get; set; }
    //    public string error { get; set; }
    //}
    #endregion
    #endregion


}
