﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
  public class ProcedureExamCodes
    {
        public int ExamId { get; set; }
        public string ProcedureName { get; set; }
        public int ExamCode { get; set; }
    }
}
