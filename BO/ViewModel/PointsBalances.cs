﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class PointsBalances
    {
        public int PM_Id { get; set; }
        public int? PointLevel { get; set; }
        public decimal? Multiplier { get; set; }
        public int PlanId { get; set; }
        public string PlanName { get; set; }
        public List<SuperPlan> Plans { get; set; }
    }
}
