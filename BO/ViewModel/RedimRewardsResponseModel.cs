﻿namespace BO.ViewModel
{
    /// <summary>
    /// Response class of RedimRewards
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class RedimRewardsResponseModel<T> : APIResponseBase<T>
    {
        /// <summary>
        /// Transaction Id of Mobile Platform (or External System) 
        /// </summary>
        public string TransactionId { get; set; }
    }
}
