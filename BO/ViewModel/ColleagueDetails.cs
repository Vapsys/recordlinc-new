﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class ColleagueDetails
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string ImageName { get; set; }
        public int ColleagueId { get; set; }
        public string ExactAddress { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string EmailAddress { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Officename { get; set; }
        public List<SpeacilitiesOfDoctor> lstSpeacilitiesOfDoctor = new List<SpeacilitiesOfDoctor>();
        public int TotalRecord { get; set; }
        public string SecondaryEmail { get; set; }
        public int ColleageReferralCount { get; set; }
        public string Location { get; set; }
        public int LocationId { get; set; }
        public int referralsent { get; set; }
        public int refferralreceived { get; set; }
        public bool HasButton { get; set; }
        public string Type { get; set; }
    }
}
