﻿namespace BO.ViewModel
{
    public class ValidationResponse
    {
        public ValidationResponse()
        {
            Status = false;
            Message = string.Empty;
        }
        public bool Status { get; set; }
        public string Message { get; set; }
    }
}