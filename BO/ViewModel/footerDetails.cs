﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
   public class FooterDetails
    {
        public List<LocationHistory> lstlocationList = new List<LocationHistory>();
        public List<SpecialityHistory> lstSpecialityList = new List<SpecialityHistory>();
        public List<FooterCity> lstFooterCity = new List<FooterCity>();
    }
    public class LocationHistory {
        public string City { get; set; }
        public int Counting { get; set; }

    }
    public class FooterCity
    {
        public string City { get; set; }
        public int SortOrder { get; set; }
    }
    public class SpecialityHistory
    {
        public string Specialitity { get; set; }
        public int Counting { get; set; }
        public int SpecialityId { get; set; }
    }
}
