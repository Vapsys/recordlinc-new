﻿using BO.Models;
using System.Collections.Generic;

namespace BO.ViewModel
{
    public class InsuranceEstimatorModel
    {
        public int InsuranceCompanyId { get; set; }
        public int DentalProcedureId { get; set; }
        public List<InsurnaceCompaniesModel> CompaniesList { get; set; }
        public List<DentalProceduresModel> DentalProceduresList { get; set; }
        //Olivia changes on 10-04-2018
        public string SchedulingLink { get; set; }
    }
}
