﻿using System.Collections.Generic;

namespace BO.ViewModel
{
    public class FullContact
    {
        public FullContact()
        {
            details = new Details();
            dataAddOns = new List<DataAddOn>();
        }
        public int status { get; set; }
        public string message { get; set; }
        public string fullName { get; set; }
        public object ageRange { get; set; }
        public string gender { get; set; }
        public string location { get; set; }
        public string title { get; set; }
        public string organization { get; set; }
        public string twitter { get; set; }
        public string linkedin { get; set; }
        public string facebook { get; set; }
        public string bio { get; set; }
        public string avatar { get; set; }
        public string website { get; set; }
        public Details details { get; set; }
        public List<DataAddOn> dataAddOns { get; set; }
        public string updated { get; set; }
    }

    public class Name
    {
        public string given { get; set; }
        public string family { get; set; }
        public string full { get; set; }
    }

    public class Emaillbl
    {
        public string label { get; set; }
        public string value { get; set; }
    }

    public class Twitter
    {
        public string username { get; set; }
        public string userid { get; set; }
        public string url { get; set; }
        public string service { get; set; }
        public int followers { get; set; }
        public int following { get; set; }
    }

    public class Wordpress
    {
        public string username { get; set; }
        public string url { get; set; }
        public string service { get; set; }
    }

    public class Gravatar
    {
        public string username { get; set; }
        public string userid { get; set; }
        public string url { get; set; }
        public string bio { get; set; }
        public string service { get; set; }
    }

    public class Facebook
    {
        public string username { get; set; }
        public string url { get; set; }
        public string service { get; set; }
    }

    public class Google
    {
        public string userid { get; set; }
        public string url { get; set; }
        public string service { get; set; }
        public int followers { get; set; }
    }

    public class Linkedin
    {
        public string username { get; set; }
        public string userid { get; set; }
        public string url { get; set; }
        public string bio { get; set; }
        public string service { get; set; }
        public int followers { get; set; }
        public int following { get; set; }
    }

    public class Profiles
    {
        public Profiles()
        {
            twitter = new Twitter();
            wordpress = new Wordpress();
            gravatar = new Gravatar();
            facebook = new Facebook();
            google = new Google();
            linkedin = new Linkedin();
        }
        public Twitter twitter { get; set; }
        public Wordpress wordpress { get; set; }
        public Gravatar gravatar { get; set; }
        public Facebook facebook { get; set; }
        public Google google { get; set; }
        public Linkedin linkedin { get; set; }
    }

    public class Locations
    {
        public string city { get; set; }
        public string region { get; set; }
        public string country { get; set; }
        public string countryCode { get; set; }
        public string formatted { get; set; }
    }

    public class Start
    {
        public int year { get; set; }
        public int month { get; set; }
    }

    public class Employment
    {
        public Employment()
        {
            start = new Start();
        }
        public string name { get; set; }
        public bool current { get; set; }
        public string title { get; set; }
        public Start start { get; set; }
    }

    public class Photo
    {
        public string label { get; set; }
        public string value { get; set; }
    }

    public class End
    {
        public int year { get; set; }
    }

    public class Education
    {
        public Education()
        {
            end = new End();
        }
        public string name { get; set; }
        public string degree { get; set; }
        public End end { get; set; }
    }

    public class Url
    {
        public string label { get; set; }
        public string value { get; set; }
    }

    public class Details
    {
        public Details()
        {
            name = new Name();
            emails = new List<Emaillbl>();
            phones = new List<object>();
            profiles = new Profiles();
            locations = new List<Location>();
            employment = new List<Employment>();
            photos = new List<Photo>();
            education = new List<Education>();
            urls = new List<Url>();
            interests = new List<object>();
            topics = new List<object>();
        }

        public Name name { get; set; }
        public object age { get; set; }
        public string gender { get; set; }
        public List<Emaillbl> emails { get; set; }
        public List<object> phones { get; set; }
        public Profiles profiles { get; set; }
        public List<Location> locations { get; set; }
        public List<Employment> employment { get; set; }
        public List<Photo> photos { get; set; }
        public List<Education> education { get; set; }
        public List<Url> urls { get; set; }
        public List<object> interests { get; set; }
        public List<object> topics { get; set; }
    }

    public class DataAddOn
    {
        public string id { get; set; }
        public string name { get; set; }
        public bool enabled { get; set; }
        public bool applied { get; set; }
        public string description { get; set; }
        public string docLink { get; set; }
    }

    public class Emails
    {
        public string Email { get; set; }
    }
}
 
