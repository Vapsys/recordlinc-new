﻿using System;

namespace BO.ViewModel
{
    public class PatientAppointments
    {
        /// <summary>
        /// Primary Key / Recordlinc AppointmentId.
        /// </summary>
        public int RL_Id { get; set; }

        /// <summary>
        /// AppointmentId Of PMS
        /// </summary>
        public int? PMSAppointmentId { get; set; }
        /// <summary>
        /// Dentrix PatientId
        /// </summary>
        public int? PMSPatientId { get; set; }
        /// <summary>
        /// Recordlinc PatientId
        /// </summary>
        public int RLPatientId { get; set; }
        /// <summary>
        /// Recordlinc Primary key of Provider / Recordlinc ProviderId.
        /// </summary>
        public int RLProviderId { get; set; }
        /// <summary>
        /// ProviderId of Dentrix
        /// </summary>
        public string PMSProviderId { get; set; }
        /// <summary>
        /// OperatoryId of PMS (of PMS)
        /// </summary>
        public string PMSOperatoryId { get; set; }
        /// <summary>
        /// Comma saperated Procedure codes of PMS associated with current appointment.
        /// </summary>
        public string PMSAdaCode { get; set; }
        /// <summary>
        /// Comma saperated procedureId of PMS associated with current appointment.
        /// </summary>
        public string PMSTxProcedure { get; set; }
        /// <summary>
        /// Appointment Date / time when appointment is scheduled.
        /// </summary>
        public DateTime AppointmentDateTime { get; set; }
        /// <summary>
        /// Length of appointment in minutes to complete appointment
        /// </summary>
        public int AppointmentLength { get; set; }
        /// <summary>
        /// Reason why appointment is sheduled. (of PMS)
        /// </summary>
        public string PMSAppointmentReason { get; set; }
        /// <summary>
        /// AutoModified TimeStamp of PMS
        /// </summary>
        public DateTime? PMSAutoModifiedTimeStamp { get; set; }
        /// <summary>
        /// Amount changed to patent after completing appointment (of PMS)
        /// </summary>
        public double? PMSAmount { get; set; }
        /// <summary>
        /// Overriden amount of appointment (of PMS)
        /// </summary>
        public bool? PMSAmountOverriden { get; set; }
        /// <summary>
        /// States wether appointment is deleted or not. true means procedure is deleted and false otherwise.
        /// </summary>
        public bool IsDeleted { get; set; }
        /// <summary>
        /// If appointment booked by staff member, his/her Id shown here (came from PMS)
        /// </summary>
        public string PMSStaffId { get; set; }
        /// <summary>
        /// Date when appointment missed by the patient. (of PMS)
        /// </summary>
        public DateTime? PMSBrokenDate { get; set; }
    }
}
