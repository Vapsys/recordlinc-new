﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    /// <summary>
    /// Search Colleagues functionality for OCR
    /// </summary>
    public class SearchColleagueViewModel
    {
        /// <summary>
        /// UserId
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// First Name of Dentist
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Last Name of Dentist
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Image
        /// </summary>
        public string ImageName { get; set; }
        /// <summary>
        /// Exact Address
        /// </summary>
        public string ExactAddress { get; set; }
        /// <summary>
        /// Address 2
        /// </summary>
        public string Address2 { get; set; }
        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// State
        /// </summary>
        public string State { get; set; }
        /// <summary>
        /// Miles
        /// </summary>
        public decimal Miles { get; set; }
        /// <summary>
        /// Specialties
        /// </summary>
        public string Specialities { get; set; }
        /// <summary>
        /// Office name
        /// </summary>
        public string OfficeName { get; set; }
        /// <summary>
        /// Total Record
        /// </summary>
        public int TotalRecord { get; set; }
        /// <summary>
        /// Zip-code
        /// </summary>
        public string ZipCode { get; set; }
        /// <summary>
        /// Phone number
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// Dentist email
        /// </summary>
        public string Email { get; set; }
    }
}
