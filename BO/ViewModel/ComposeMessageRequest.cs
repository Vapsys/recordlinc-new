﻿using static BO.Enums.Common;

namespace BO.ViewModel
{
    public class ComposeMessageRequest
    {
        public ComposeMessageType MessageTypeId { get; set; }
        public int MessageId { get; set; }
        public bool Issent { get; set; }
    }
}
