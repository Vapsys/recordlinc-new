﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class EstimateReport
    {
        public string Name { get; set; }
        public string EstimateViaCall { get; set; }
        public string EstimateViaText { get; set; }
        public string EstimateViaEmail { get; set; }
        public string ProcedureName { get; set; }
        public string InsuranceName { get; set; }
        public decimal ServiceCost { get; set; }
        public decimal TotalCost { get; set; }
        public string Date { get; set; }
        public int HistoryId { get; set; }
    }
}
