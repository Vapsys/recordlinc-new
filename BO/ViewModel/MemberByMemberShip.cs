﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Models;

namespace BO.ViewModel
{
    public class MemberByMemberShip
    {

        public List<MembershipOfMember> lstMemberofMembership = new List<MembershipOfMember>();
        public List<Membership> LstMembership = new List<Membership>();
        public string MembershipName { get; set; }
        public int TotalRecord { get; set; }
    }
}
