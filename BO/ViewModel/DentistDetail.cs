﻿using System.Collections.Generic;

namespace BO.ViewModel
{
    /// <summary>
    /// This Class used for Get Dentist details on OCR for Fill Dentist Images and Name on Layout page.
    /// </summary>
    public class DentistDetail
    {
        /// <summary>
        /// Dentist Id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// First name of Dentist
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Last name of Dentist
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Dentist Image name
        /// </summary>
        public string ImageName { get; set; }
        /// <summary>
        /// Dentist Image name with Full path
        /// </summary>
        public string ImageWithPath { get; set; }
        /// <summary>
        /// Dentist User name
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// This is used identify the Dentist is Receiver.
        /// </summary>
        public bool IsReceiveReferral { get; set; }
        /// <summary>
        /// This is used for identify the Dentist is sender.
        /// </summary>
        public bool IsSendReferral { get; set; }
        /// <summary>
        /// This is used for TimeZone of The doctor
        /// </summary>
        public string TimeZoneSystemName { get; set; }
        /// <summary>
        /// List of Membership Details of Dentist.
        /// </summary>
        public List<MemberPlanDetail> MembershipDetails { get; set; }
    }
}
