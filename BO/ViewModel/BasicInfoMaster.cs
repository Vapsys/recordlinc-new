﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class BasicInfoMaster
    {
        public int PatientId { get; set; }

        public string AssignedPatientId { set; get; }

        [Required(ErrorMessage = "Required")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Required")]
        public string LastName { get; set; }

        public string MiddelName { set; get; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})$", ErrorMessage = "Invalid")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Required")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Confirmation Password Do Not Match.")]
        public string ConfirmPassword { get; set; }

        public string GuardianFirstName { get; set; }
        public string GuardianLastName { get; set; }
        public string GuardianPhone { get; set; }
        public string GuardianEmail { get; set; }

        public string Country { set; get; }
        public string DateOfBirth { set; get; }
        public int Gender { set; get; }
        public string ProfileImage { set; get; }

        public string Phone { set; get; }
        public string txtWorkPhone { set; get; }
        public string txtFax { set; get; }
        public string Address { set; get; }

        public string City { set; get; }
        public string State { set; get; }
        public string Zip { set; get; }
        public string Comments { set; get; }
        public string StartDate { set; get; }
        public string Address2 { set; get; }
        public string GuarFirstName { set; get; }
        public string GuarLastName { set; get; }
        public string GuarPhone { set; get; }
        public string GuarEmail { set; get; }
        public string FacebookUrl { set; get; }
        public string TwitterUrl { set; get; }
        public string LinkedinUrl { set; get; }
        public string Status { set; get; }
        public int OwnerId { set; get; }
        public int Limit { get; set; }
        public int FromRowNumber { get; set; }
        public string ContainerId { get; set; }
        public string AjaxActionUrl { get; set; }
        public string StayloggedMins { get; set; }
        public string SecondaryEmail { get; set; }
    }
    public class InsuranceDetailMaster
    {
        public string txtInsurancePhone2 { get; set; }

        public string txtPatientParentEmployedBy { get; set; }
        public string txtSpouseParentName { get; set; }
        public string txtSpouseParentEmployedBy { get; set; }
        public string txtWhoisResponsible { get; set; }
        
        public string txtResponsiblepartyFname { get; set; }
        public string txtResponsiblepartyLname { get; set; }

        public string txtemailaddress { get; set; }
        public string txtResponsibleRelationship { get; set; }
        public string txtResponsibleAddress { get; set; }
        public string txtResponsibleCity { get; set; }
        public string txtResponsibleState { get; set; }
        public string txtResponsibleZipCode { get; set; }
        public string txtResponsibleDOB { get; set; }
        public string txtResponsibleContact { get; set; }

        public string chkMethodOfPayment { get; set; }
        public string txtWhommay { get; set; }
        public string txtSomeonetonotify { get; set; }
        public string txtemergencyname { get; set; }
        public string txtemergency { get; set; }
        public bool Insurance { get; set; }
        public bool Cash { get; set; }
        public bool CrediteCard { get; set; }

        public string txtEmployeeName1 { get; set; }

        public string Insurance_Phone1 { get; set; }
        public string txtEmployeeDob1 { get; set; }
        public string txtEmployerName1 { get; set; }
        public string txtYearsEmployed1 { get; set; }
        public string txtNameofInsurance1 { get; set; }
        public string txtInsuranceAddress1 { get; set; }
        public string txtInsuranceTelephone1 { get; set; }

        public string txtEmployeeName2 { get; set; }
        public string Insurance_Phone2 { get; set; }
        public string txtEmployeeDob2 { get; set; }
        public string txtEmployerName2 { get; set; }
        public string txtYearsEmployed2 { get; set; }
        public string txtNameofInsurance2 { get; set; }
        public string txtInsuranceAddress2 { get; set; }
        public string txtInsuranceTelephone2 { get; set; }
        public string txtDigiSignreg { get; set; }
        public string txtInsurancePhone1 { get; set; }
    }
    public class DentalHistoryMaster
    {
        public string txtQue1 { get; set; }
        public string txtQue2 { get; set; }
        public string txtQue3 { get; set; }
        public string txtQue4 { get; set; }
        public string txtQue5 { get; set; }
        public string txtQue5a { get; set; }
        public string txtQue5b { get; set; }
        public string txtQue5c { get; set; }
        public string txtQue6 { get; set; }
        public string txtQue7 { get; set; }
        public bool rdQue7a { get; set; }
        public bool rdQue8 { get; set; }
        public bool rdQue9 { get; set; }
        public string txtQue9a { get; set; }
        public bool rdQue10 { get; set; }
        public bool rdoQue11aFixedbridge { get; set; }

        public bool rdoQue11bRemoveablebridge { get; set; }

        public bool rdoQue11cDenture { get; set; }

        public bool rdQue11dImplant { get; set; }

        public string txtQue12 { get; set; }



        public bool rdQue15 { get; set; }
        public bool rdQue16 { get; set; }
        public bool rdQue17 { get; set; }
        public bool rdQue18 { get; set; }
        public bool rdQue19 { get; set; }
        public string chkQue20 { get; set; }
        public bool chkQue20_1 { get; set; }
        public bool chkQue20_2 { get; set; }
        public bool chkQue20_3 { get; set; }
        public bool chkQue20_4 { get; set; }
        public bool rdQue21 { get; set; }
        public string txtQue21a { get; set; }
        public bool txtQue22 { get; set; }


        public string txtQue23 { get; set; }
        public bool rdQue24 { get; set; }

        public string txtQue26 { get; set; }

        public bool rdQue28 { get; set; }
        public string txtQue28a { get; set; }
        public string txtQue28b { get; set; }
        public string txtQue28c { get; set; }
        public string txtQue29 { get; set; }
        public string txtQue29a { get; set; }
        public bool rdQue30 { get; set; }

        public string txtComments { get; set; }

        public string txtDateoffirstvisit { get; set; }
        public string Reasonforfirstvisit { get; set; }
        public string Dateoflastvisit { get; set; }
        public string Reasonforlastvisit { get; set; }
        public string Dateofnextvisit { get; set; }
        public string Reasonfornextvisit { get; set; }
        public string txtQue24 { get; set; }
        public bool MchkPressure { get; set; }
        public bool MchkSweets { get; set; }
        public bool MchkCold { get; set; }
        public bool MchkHot { get; set; }
        public bool MchkImplant { get; set; }
        public bool MchkDenture { get; set; }
        public bool MchkRemoveable { get; set; }
        public bool MchkFixed { get; set; }
        public bool MrdQue7 { get; set; }
    }
    public class MedicalHistoryMaster
    {
        public bool MrdQue1 { get; set; }
        public bool MrnQue1 { get; set; }
        public bool Mtxtphysicians { get; set; }
        public bool MrdQue2 { get; set; }
        public bool MrnQue2 { get; set; }
        public bool Mtxthospitalized { get; set; }
        public bool MrdQue3 { get; set; }
        public bool MrnQue3 { get; set; }
        public bool Mtxtserious { get; set; }
        public bool MrnQue4 { get; set; }
        public bool MrdQue4 { get; set; }
        public bool Mtxtmedications { get; set; }
        public bool MrdQue5 { get; set; }
        public bool MrnQue5 { get; set; }
        public bool MtxtRedux { get; set; }
        public bool MrdQue6 { get; set; }
        public bool MrnQue6 { get; set; }
        public bool MtxtFosamax { get; set; }
        public bool MrdQuediet7 { get; set; }
        public bool MrnQuediet7 { get; set; }
        public bool Mtxt7 { get; set; }
        public bool Mrdotobacco8 { get; set; }
        public bool Mrnotobacco8 { get; set; }
        public bool Mtxt8 { get; set; }
        public bool Mrdosubstances { get; set; }
        public bool Mrnosubstances { get; set; }
        public bool Mtxt9 { get; set; }
        public bool Mrdopregnant { get; set; }
        public bool Mrnopregnant { get; set; }
        public bool Mtxt10 { get; set; }
        public bool Mrdocontraceptives { get; set; }
        public bool Mrnocontraceptives { get; set; }

        public bool Mtxt11 { get; set; }
        public bool MrdoNursing { get; set; }
        public bool MrnoNursing { get; set; }
        public bool Mtxt12 { get; set; }
        public bool Mrdotonsils { get; set; }
        public bool Mrnotonsils { get; set; }
        public string Mtxt13 { get; set; }
        public bool Mtxtallergic { get; set; }
        public bool MchkQue_1 { get; set; }
        public bool MchkQueN_1 { get; set; }
        public bool MchkQue_2 { get; set; }
        public bool MchkQueN_2 { get; set; }
        public bool MchkQue_3 { get; set; }
        public bool MchkQueN_3 { get; set; }
        public bool MchkQue_4 { get; set; }
        public bool MchkQueN_4 { get; set; }
        public bool MchkQue_5 { get; set; }
        public bool MchkQueN_5 { get; set; }
        public bool MchkQue_6 { get; set; }
        public bool MchkQueN_6 { get; set; }
        public bool MchkQue_7 { get; set; }
        public bool MchkQueN_7 { get; set; }
        public bool MchkQue_8 { get; set; }
        public bool MchkQueN_8 { get; set; }
        public bool MchkQue_9 { get; set; }
        public bool MchkQueN_9 { get; set; }
        public bool MtxtchkQue_9 { get; set; }

        //section-2
        public bool MrdQueAIDS_HIV_Positive { get; set; }
        public bool MrdQueAlzheimer { get; set; }
        public bool MrdQueAnaphylaxis { get; set; }
        public bool MrdQueBoneDisorders { get; set; }
        public bool MrdQueChemicalDependancy { get; set; }
        public bool MrdQueCortisoneTreatments { get; set; }
        public bool MrdQueEndocrineProblems { get; set; }
        public bool MrdQueExcessiveUrination { get; set; }
        public bool MrdQueNervousDisorder { get; set; }
        public bool MrdQuePneumonia { get; set; }
        public bool MrdQueProlongedBleending { get; set; }
        public bool MrdQueAnemia { get; set; }
        public bool MrdQueAngina { get; set; }
        public bool MrdQueArthritis_Gout { get; set; }
        public bool MrdQueArtificialHeartValve { get; set; }
        public bool MrdQueArtificialJoint { get; set; }
        public bool MrdQueAsthma { get; set; }
        public bool MrdQueBloodDisease { get; set; }
        public bool MrdQueBloodTransfusion { get; set; }
        public bool MrdQueBreathing { get; set; }
        public bool MrdQueBruise { get; set; }
        public bool MrdQueCancer { get; set; }
        public bool MrdQueChemotherapy { get; set; }
        public bool MrdQueChest { get; set; }
        public bool MrdQueCold_Sores_Fever { get; set; }
        public bool MrdQueCongenital { get; set; }
        public bool MrdQueConvulsions { get; set; }
        public bool MrdQueCortisone { get; set; }
        public bool MrdQueDiabetes { get; set; }
        public bool MrdQueDrug { get; set; }
        public bool MrdQueEasily { get; set; }
        public bool MrdQueEmphysema { get; set; }
        public bool MrdQueEpilepsy { get; set; }
        public bool MrdQueExcessiveBleeding { get; set; }
        public bool MrdQueExcessiveThirst { get; set; }
        public bool MrdQueFainting { get; set; }
        public bool MrdQueFrequentCough { get; set; }
        public bool MrdQueFrequentDiarrhea { get; set; }
        public bool MrdQueFrequentHeadaches { get; set; }
        public bool MrdQueGenital { get; set; }
        public bool MrdQueGlaucoma { get; set; }
        public bool MrdQueHay { get; set; }
        public bool MrdQueHeartAttack_Failure { get; set; }
        public bool MrdQueHeartMurmur { get; set; }
        public bool MrdQueHeartPacemaker { get; set; }
        public bool MrdQueHeartTrouble_Disease { get; set; }
        public bool MrdQueHemophilia { get; set; }
        public bool MrdQueHepatitisA { get; set; }
        public bool MrdQueHepatitisBorC { get; set; }
        public bool MrdQueHerpes { get; set; }
        public bool MrdQueHighBloodPressure { get; set; }
        public bool MrdQueHighCholesterol { get; set; }
        public bool MrdQueHives { get; set; }
        public bool MrdQueHypoglycemia { get; set; }
        public bool MrdQueIrregular { get; set; }
        public bool MrdQueKidney { get; set; }
        public bool MrdQueLeukemia { get; set; }
        public bool MrdQueLiver { get; set; }
        public bool MrdQueLow { get; set; }
        public bool MrdQueLung { get; set; }
        public bool MrdQueMitral { get; set; }
        public bool MrdQueOsteoporosis { get; set; }
        public bool MrdQuePain { get; set; }
        public bool MrdQueParathyroid { get; set; }
        public bool MrdQuePsychiatric { get; set; }
        public bool MrdQueRadiation { get; set; }
        public bool MrdQueRecent { get; set; }
        public bool MrdQueRenal { get; set; }
        public bool MrdQueRheumatic { get; set; }
        public bool MrdQueRheumatism { get; set; }
        public bool MrdQueScarlet { get; set; }
        public bool MrdQueShingles { get; set; }
        public bool MrdQueSickle { get; set; }
        public bool MrdQueSinus { get; set; }
        public bool MrdQueSpina { get; set; }
        public bool MrdQueStomach { get; set; }
        public bool MrdQueStroke { get; set; }
        public bool MrdQueSwelling { get; set; }
        public bool MrdQueThyroid { get; set; }
        public bool MrdQueTonsillitis { get; set; }
        public bool MrdQueTuberculosis { get; set; }
        public bool MrdQueTumors { get; set; }
        public bool MrdQueUlcers { get; set; }
        public bool MrdQueVenereal { get; set; }
        public bool MrdQueYellow { get; set; }

        public string Mtxtillness { get; set; }
        public string MtxtComments { get; set; }
        public bool MrdQueUnexplained { get; set; }
        public bool MrdQueVision { get; set; }
        public bool MrdQueValve { get; set; }
        public bool MrdQueSwollen { get; set; }
        public bool MrdQueSjorgren { get; set; }
        public bool MrdQueShortness { get; set; }
        public bool MrdQueEndocarditis { get; set; }
        public bool MrdQuePainJaw_Joints { get; set; }
        public bool MrdQuePacemaker { get; set; }
        public bool MrdQueNeck_BackProblem { get; set; }
        public bool MrdQueReplacement { get; set; }
        public bool MrdQueHow_often { get; set; }
        public bool MrdQueHow_much { get; set; }
        public bool MrdQueAIDS { get; set; }
        public bool MrdQueBlood_pressure { get; set; }
        public bool MrdQueHiatal { get; set; }
        public bool MrdQueHeart_murmur { get; set; }
        public bool MrdQueHeart_defect { get; set; }
        public bool MrdQueAttack { get; set; }
        public bool MrdQueHeadaches { get; set; }
        public bool MrdQueSubstances { get; set; }
        public bool MrdQueAlcoholic { get; set; }
        public bool MrdQueDizziness { get; set; }
        public bool MrdQueDigestive { get; set; }
        public bool MrdQueDiarrhhea { get; set; }
        public bool MrdQuefillers { get; set; }
        public bool MrdQueCirulation { get; set; }
        public bool MrdQueCigarette { get; set; }
        public bool MrdQueFibrillation { get; set; }
        public bool MrdQueThinners { get; set; }
        public bool MrdQueAbnormal { get; set; }
    }
}
