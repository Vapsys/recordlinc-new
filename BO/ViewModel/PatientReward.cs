﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class PatientReward
    {
        public int PatientId { get; set; }
        public string PatientName { get; set; }
        public string DOB { get; set; }
        public int OwnerId { get; set; }
        public string Age { get; set; }
        public string TotalRecords { get; set; }
        public int CountOfReferral { get; set; }
        public int CountOfPlan { get; set; }
    }
}
