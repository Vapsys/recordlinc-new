﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using static BO.Enums.Common;

namespace BO.ViewModel
{
    /// <summary>
    /// Appointment
    /// </summary>
    public class Appt
    {
        /// <summary>
        /// Constructor for appointment
        /// </summary>
        public Appt()
        {
            ViewApplst = new List<ViewAppt>();
            BookApp = new List<BookAppt>();
            DoctorList = new List<SelectListItem>();
            DoctorServiceslst = new List<SelectListItem>();
        }
        /// <summary>
        /// Appointment resource id
        /// </summary>
        public int ApptResourceId { get; set; }
        /// <summary>
        /// Doctor name
        /// </summary>
        public string DoctorId { get; set; }
        /// <summary>
        /// Appointment service id
        /// </summary>
        public string AppointmentServiceId { get; set; }
        /// <summary>
        /// Doctor list
        /// </summary>
        public List<SelectListItem> DoctorList { get; set; }
        /// <summary>
        /// Appointment view list
        /// </summary>
        public List<ViewAppt> ViewApplst { get; set; }
        /// <summary>
        /// Book appointment list
        /// </summary>
        public List<BookAppt> BookApp { get; set; }
        /// <summary>
        /// Doctor service list 
        /// </summary>
        public List<SelectListItem> DoctorServiceslst { get; set; }
    }

    /// <summary>
    /// View Appointment
    /// </summary>
    public class ViewAppt
    {
        /// <summary>
        /// Appointment id
        /// </summary>
        public int AppointmentId { get; set; }
        /// <summary>
        /// Appointment date
        /// </summary>
        public string AppointmentDate { get; set; }
        /// <summary>
        /// Appointment time
        /// </summary>
        public string AppointmentTime { get; set; }
        /// <summary>
        /// Duration of appointment in minutes
        /// </summary>
        public float AppointmentLength { get; set; }
        /// <summary>
        /// Doctor name
        /// </summary>
        public string DoctorName { get; set; }
        /// <summary>
        /// Appointment status
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Rating
        /// </summary>
        public string Rating { get; set; }
        /// <summary>
        /// Service type
        /// </summary>
        public string ServiceType { get; set; }
        /// <summary>
        /// Appointment status
        /// </summary>
        public int AppointmentStatusId { get; set; }
        /// <summary>
        /// Appointment datetime
        /// </summary>
        public string AppointmentDateTime { get; set; }
        /// <summary>
        /// System date time
        /// </summary>
        public string SystemDateTime { get; set; }
        /// <summary>
        /// Appointment end time
        /// </summary>
        public string AppoinmentEndTime { get; set; }
    }

    /// <summary>
    /// Book Appointment
    /// </summary>
    public class BookAppt
    {
        /// <summary>
        /// Doctor id
        /// </summary>
        public int DoctorId { get; set; }
        /// <summary>
        /// Doctor name
        /// </summary>
        public string DoctorName { get; set; }
        /// <summary>
        /// Doctor services list
        /// </summary>
        public List<DoctorServ> Serviceslst { get; set; }
    }

    /// <summary>
    /// Cancel appointment
    /// </summary>
    public class CancelAppt
    {
        /// <summary>
        /// Appointment id
        /// </summary>
        public int AppointmentId { get; set; }
        /// <summary>
        /// Appointment cancellation note
        /// </summary>
        public string CancelNotes { get; set; }
    }

    /// <summary>
    /// Doctor services
    /// </summary>
    public class DoctorServ
    {
        /// <summary>
        /// Appointment services id
        /// </summary>
        public int AppointmentServiceId { get; set; }
        /// <summary>
        /// Services type
        /// </summary>
        public string ServiceType { get; set; }
    }
    /// <summary>
    /// Create Appointment
    /// </summary>
    public class CreateAppt
    {
        /// <summary>
        /// Constructor for create appointment
        /// </summary>
        public CreateAppt()
        {
            AppointmentDayViewList = new List<ApptDayView>();
            AppointmentServiceList = new List<ApptService>();
        }

        /// <summary>
        /// Appointment time from
        /// </summary>
        public string FromTime { get; set; }
        /// <summary>
        /// Appointment time to
        /// </summary>
        public string ToTime { get; set; }
        /// <summary>
        /// Appointment note
        /// </summary>
        public string Note { get; set; }
        /// <summary>
        /// Doctor id
        /// </summary>
        public int DoctorId { get; set; }
        /// <summary>
        /// Doctor image path
        /// </summary>
        public string DoctorImage { get; set; }
        /// <summary>
        /// DoctorCercleName
        /// </summary>
        public string DoctorCercleName { get; set; }
        /// <summary>
        /// Doctor name
        /// </summary>
        public string DoctorName { get; set; }
        /// <summary>
        /// Service id
        /// </summary>
        public string ServiceId { get; set; }
        /// <summary>
        /// Service type
        /// </summary>
        public string ServiceType { get; set; }
        /// <summary>
        /// Previous date
        /// </summary>
        public string PrevDate { get; set; }
        /// <summary>
        /// Next date
        /// </summary>
        public string NextDate { get; set; }
        /// <summary>
        /// Date is today date
        /// </summary>
        public bool IsToday { get; set; }
        /// <summary>
        /// Service length
        /// </summary>
        public int Servicelenght { get; set; }
        /// <summary>
        /// Service count
        /// </summary>
        public int ServiceCount { get; set; }
        /// <summary>
        /// List of doctor services
        /// </summary>
        public List<SelectListItem> DoctorServices { get; set; }
        /// <summary>
        /// List of patient
        /// </summary>
        public List<SelectListItem> PatientList { get; set; }
        /// <summary>
        /// List of appointment day view
        /// </summary>
        public List<ApptDayView> AppointmentDayViewList { get; set; }
        /// <summary>
        /// List of appointment services
        /// </summary>
        public List<ApptService> AppointmentServiceList { get; set; }
        /// <summary>
        /// Appointment resource id
        /// </summary>
        public int ApptResourceId { get; set; }
        /// <summary>
        /// Appointment time duration in minutes
        /// </summary>
        public int AppointmentLength { get; set; }
        /// <summary>
        /// Current date
        /// </summary>
        public DateTime CurrentDate { get; set; }
        /// <summary>
        /// Appointment date
        /// </summary>
        public int AppointmentId { get; set; }
    }

    /// <summary>
    /// Calendar opratory
    /// </summary>
    public class CalendarRes
    {
        /// <summary>
        /// Id
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// Title
        /// </summary>
        public string title { get; set; }
    }
    /// <summary>
    /// Appointment day view
    /// </summary>
    public class ApptDayView
    {
        /// <summary>
        /// Constructore for appointment day view
        /// </summary>
        public ApptDayView()
        {
            TimeSlots = new List<ApptTimeSlotView>();
        }
        /// <summary>
        /// Appointment date
        /// </summary>
        public string Date { get; set; }
        /// <summary>
        /// Doctor appointment day on/off
        /// </summary>
        public string IsOffDay { get; set; }
        /// <summary>
        /// List for doctor time slot
        /// </summary>
        public List<ApptTimeSlotView> TimeSlots { get; set; }
    }

    /// <summary>
    /// Appointment time slot
    /// </summary>
    public class ApptTimeSlotView
    {
        /// <summary>
        /// Time slot
        /// </summary>
        public string TimeSlot { get; set; }
        //public string IsBookedTimeslot { get; set; }
        //public string Date { get; set; }
        //public TimeSpan ActullTime { get; set; }
    }

    /// <summary>
    /// Appointment service
    /// </summary>
    public class ApptService
    {
        /// <summary>
        /// Appointment service id
        /// </summary>
        public int AppointmentServiceId { get; set; }
        /// <summary>
        /// Doctor id
        /// </summary>
        public int DoctorId { get; set; }
        /// <summary>
        /// Service type
        /// </summary>
        public string ServiceType { get; set; }
        /// <summary>
        /// Time duration in minutes
        /// </summary>
        public string TimeDuration { get; set; }
        /// <summary>
        /// Price
        /// </summary>
        public string Price { get; set; }
        /// <summary>
        /// Created date
        /// </summary>
        public DateTime? CreatedOn { get; set; }
        /// <summary>
        /// Modified date
        /// </summary>
        public DateTime? ModifiedOn { get; set; }
    }

    /// <summary>
    /// Create appointment
    /// </summary>
    public class CreateApptDataManagement
    {
        /// <summary>
        /// constructor for create appointment 
        /// </summary>
        public CreateApptDataManagement()
        {
            WeekSchedulesData = new List<WkScheduleData>();
            SpecialDayChedulesData = new List<SpDayCheduleData>();
            AppointmentsData = new List<ApptData>();
        }
        /// <summary>
        /// List of doctor week schedule
        /// </summary>
        public List<WkScheduleData> WeekSchedulesData { get; set; }
        /// <summary>
        /// List of doctor special day schedule
        /// </summary>
        public List<SpDayCheduleData> SpecialDayChedulesData { get; set; }
        /// <summary>
        /// Appointment data
        /// </summary>
        public List<ApptData> AppointmentsData { get; set; }
    }

    /// <summary>
    /// Appointment week schedule 
    /// </summary>
    public class WkScheduleData
    {
        /// <summary>
        /// Appointment week schedule id
        /// </summary>
        public int AppointmentWeekScheduleId { get; set; }
        /// <summary>
        /// Appointment day
        /// </summary>
        public int Day { get; set; }
        /// <summary>
        /// Doctor appointment day on/off
        /// </summary>
        public string IsOffDay { get; set; }
        /// <summary>
        /// Appointment week time allocation id
        /// </summary>
        public int AppointmentWorkingTimeAllocationId { get; set; }
        /// <summary>
        /// Appointment morning time from
        /// </summary>
        public TimeSpan? MorningFromTime { get; set; }
        /// <summary>
        /// Appointment morning time to
        /// </summary>
        public TimeSpan? MorningToTime { get; set; }
        /// <summary>
        /// Appointment evening time from
        /// </summary>
        public TimeSpan? EveningFromTime { get; set; }
        /// <summary>
        /// Appointment evening time to
        /// </summary>
        public TimeSpan? EveningToTime { get; set; }

    }

    /// <summary>
    /// Special day appointment schedule
    /// </summary>
    public class SpDayCheduleData
    {
        /// <summary>
        /// Appointment schedule id
        /// </summary>
        public int AppointmentSpecailDayScheduleId { get; set; }
        /// <summary>
        /// Appointmet date
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// Doctor appointment day on/off
        /// </summary>
        public string IsOffDay { get; set; }
        /// <summary>
        /// Appointment time allocation id
        /// </summary>
        public int AppointmentWorkingTimeAllocationId { get; set; }
        /// <summary>
        /// Appointment morning time from
        /// </summary>
        public TimeSpan? MorningFromTime { get; set; }
        /// <summary>
        /// Appointment morning time to
        /// </summary>
        public TimeSpan? MorningToTime { get; set; }

    }

    /// <summary>
    /// appointment parameter
    /// </summary>
    public class ApptData
    {
        /// <summary>
        /// Appointment id
        /// </summary>
        public int AppointmentId { get; set; }
        /// <summary>
        /// Doctor id
        /// </summary>
        public int DoctorId { get; set; }
        /// <summary>
        /// Patient id
        /// </summary>
        public int PatientId { get; set; }
        /// <summary>
        /// Appointment date
        /// </summary>
        public DateTime AppointmentDate { get; set; }
        /// <summary>
        /// Appointment time
        /// </summary>
        public TimeSpan AppointmentTime { get; set; }
        /// <summary>
        /// Appointment time duration in minutes
        /// </summary>
        public int AppointmentLength { get; set; }
        /// <summary>
        /// Doctor service type
        /// </summary>
        public string ServiceTypeId { get; set; }
        /// <summary>
        /// Appointment status
        /// </summary>
        public string Status { get; set; }
        /// <summary>
        /// Appointment note
        /// </summary>
        public string AppNote { get; set; }
        /// <summary>
        /// Appointment resource id (opratory)
        /// </summary>
        public int ApptResourceId { get; set; }
    }

    /// <summary>
    /// Email notification for create appointment
    /// </summary>
    public class MailNotiFicationForCreateAppt
    {
        /// <summary>
        /// Mail
        /// </summary>
        public string Mail { get; set; }
        /// <summary>
        /// Doector name
        /// </summary>
        public string DoctorName { get; set; }
        /// <summary>
        /// Doctor email
        /// </summary>
        public string DoctorEmail { get; set; }
        /// <summary>
        /// Doctor phone number
        /// </summary>
        public string DoctorPhoneNo { get; set; }
        /// <summary>
        /// Patient name
        /// </summary>
        public string PatientName { get; set; }
        /// <summary>
        /// Patient email
        /// </summary>
        public string PatientEmail { get; set; }
        /// <summary>
        /// Patient phone number
        /// </summary>
        public string PatientPhoneNo { get; set; }
        /// <summary>
        /// Doctor address
        /// </summary>
        public string DoctorAddress { get; set; }
        /// <summary>
        /// Patient address
        /// </summary>
        public string PatientAddress { get; set; }
        /// <summary>
        /// Appointment date
        /// </summary>
        public string AppointmentDate { get; set; }
        /// <summary>
        /// Appointment time
        /// </summary>
        public string AppointmentTime { get; set; }
    }
    /// <summary>
    /// This model used for Filter appointment time Slot of dentist.
    /// </summary>
    public class FilterSchedule
    {
        /// <summary>
        /// Dentrix Connector Id
        /// </summary>
        public string DentrixConnectorId { get; set; }
        /// <summary>
        /// Schedule types
        /// Provider Schedule Normal = 0
        /// Practice Schedule Normal = 1
        /// Operatory Schedule Normal = 2
        /// Provider Schedule Exceptional = 0
        /// Practice Schedule Exceptional = 1
        /// Operatory Schedule Exceptional = 2
        /// Bydefault 0
        /// </summary>
        public ScheduleType scheduleType { get; set; }
    }
}