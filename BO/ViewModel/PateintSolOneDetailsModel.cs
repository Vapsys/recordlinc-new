﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
  public class PateintSolOneDetailsModel
    {
        public PateintSolOneDetailsModel()
        {
            Plan = new PlanDetails();
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ProfileImage { get; set; }
        public string ExactAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Phone2 { get; set; }
        public string Mobile { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public bool IsSendPushNotification { get; set; }
        public string FirstExamDate { get; set; }
        public string PatientStatus { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyContactPhoneNumber { get; set; }
        public string PrimaryPhoneNumber { get; set; }
        public string SecondaryPhoneNumber { get; set; }
        public string Email2 { get; set; }
        public DentalHistorys DentalHistory { get; set; }
        public MedicalHistory MedicalHistory { get; set; }
        public InsuranceCoverageModel InsuranceCoverage { get; set; }
        public PDIC PrimaryDentalInsurance { get; set; }
        public SDIC SecondaryDentalInsurance { get; set; }
        public PlanDetails Plan { get; set; }
    }

    public class PlanDetails
    {
        public int PlanId { get; set; }
        public string PlanName { get; set; }
        public decimal Amount { get; set; }
        public decimal RegisterReward { get; set; }
        public decimal Point { get; set; }
    }
   
}
