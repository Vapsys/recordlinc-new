﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class InsuranceProcedureMapping
    {
        public int UserId { get; set; }
        public string ProcedureName { get; set; }
        public decimal InsuredAmount { get; set; }
        public DataTable ColumnName { get; set; }
    }
}
