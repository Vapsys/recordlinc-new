﻿using BO.Models;
using System.Collections.Generic;
using static BO.Enums.Common;

namespace BO.ViewModel
{
    public class PatientPaymentModel
    {
        /// <summary>
        /// Gives total records found in Finance charges
        /// </summary>
        public int FinanceCharges_TotalRecord { get; set; }
        /// <summary>
        /// Gives total records found in adjustments
        /// </summary>
        public int Adjustments_TotalRecord { get; set; }
        /// <summary>
        /// Gives total records found in insurance payments
        /// </summary>
        public int InsurancePayment_TotalRecord { get; set; }
        /// <summary>
        /// Gives total records found in standard payments
        /// </summary>
        public int StandardPayment_TotalRecord { get; set; }

        /// <summary>
        /// Gives RequestId of Finance charges
        /// </summary>
        public string FinanceCharges_RequestId { get; set; }
        /// <summary>
        /// Gives RequestId of adjustments
        /// </summary>
        public string Adjustments_RequestId { get; set; }
        /// <summary>
        /// Gives RequestId of insurance payments
        /// </summary>
        public string InsurancePayment_RequestId { get; set; }
        /// <summary>
        /// Gives RequestId of standard payments
        /// </summary>
        public string StandardPayment_RequestId { get; set; }
        /// <summary>
        /// Provides list of Finance charges
        /// </summary>
        public List<FinanceCharges> lstPatient_DentrixFinanceCharges = new List<FinanceCharges>();
        /// <summary>
        /// Provides list of Adjustments 
        /// </summary>
        public List<AdjutmentDetails> lstPatient_DentrixAdjustments = new List<AdjutmentDetails>();
        /// <summary>
        /// Provides list of insurnace payments
        /// </summary>
        public List<InsurancePayment> lstPatient_DentrixInsurancePayment = new List<InsurancePayment>();
        /// <summary>
        /// provides list of standard payment
        /// </summary>
        public List<StandardPayment> lstPatient_DentrixStandardPayment = new List<StandardPayment>();       
    }
}
