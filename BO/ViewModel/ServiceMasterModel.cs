﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class ServiceMasterModel
    {
        public int Service_Id { get; set; }
        public string Service_Name { get; set; }
        public string Service_MapFieldName { get; set; }
        public bool Service_IsEnable { get; set; }
        public string Service_Value { get; set; }
        public int Service_Speciality_Id { get; set; }
        public string Service_NameFor { get; set; }
        public int Service_SortOrder { get; set; }
        
    }
}
