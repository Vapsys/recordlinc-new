﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    /// <summary>
    /// Dentrix appointment
    /// </summary>
    public class PostTeamMember
    {
         
        /// <summary>
        /// Page index which has parse between 1 to 32767
        /// </summary>
        [Required(ErrorMessage = "Page index is required. ")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1")]
        public int PageIndex { get; set; }
        /// <summary>
        /// Page size hich has parse between 1 to 1500
        /// </summary>
        [Required(ErrorMessage = "Page size is required. ")]
        [Range(1, BO.Constatnt.Common.MaxPageSize, ErrorMessage = "Please enter a value between 1 to 100")]
        public int PageSize { get; set; }
        /// <summary>
        /// Search using firstname, lastname and doctor speciality
        /// </summary>
        public string SearchText { get; set; }
        /// <summary>
        /// Team member list including login user
        /// </summary>
        public bool IncludeUser { get; set; }
    }

    /// <summary>
    /// Doctor Details
    /// </summary>
    public class DoctorTeamMemberList
    {
        /// <summary>
        /// Doctor id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Doctor First Name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Doctor Last Name
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Emaul
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Team member image
        /// </summary>
        public string Image { get; set; }
        /// <summary>
        /// Gender
        /// </summary>
        public string Gender { get; set; }
        ///// <summary>
        ///// Team member speciality with comma separated 
        ///// </summary>
        //public string Speciality { get; set; }
        /// <summary>
        /// Team member speciality in array
        /// </summary>
        public List<DoctorLocation> Specialitys { get; set; }
        /// <summary>
        /// Team member location
        /// </summary>
        public DoctorLocation Location { get; set; }
    }

}
