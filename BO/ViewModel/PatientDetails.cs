﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BO.ViewModel
{
    public class PatientDetails
    {
        public List<string> PatientId { get; set; }
    }

    public class GetPatientList
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Address { get; set; }
        public DoctorDetials Doctor { get; set; }
        public PatientReferBy ReferBy { get; set; }
        public DoctorLocation Location { get; set; }

    }
    public class PatientReferBy
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    /// <summary>
    /// Doctor location
    /// </summary>
    public class DoctorLocation
    {
        /// <summary>Gets or sets the identifier.</summary>
        /// <value>The identifier.</value>
        public int Id { get; set; }

        /// <summary>Gets or sets the name.</summary>
        /// <value>The name.</value>
        public string Name { get; set; }
    }

    /// <summary>
    /// Dentrix appointment
    /// </summary>
    public class PostPatient
    {

        /// <summary>
        /// Page index which has parse between 1 to 32767
        /// </summary>
        [Required(ErrorMessage = "Page index is required. ")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1")]
        public int PageIndex { get; set; }
        /// <summary>
        /// Page size hich has parse between 1 to 1500
        /// </summary>
        [Required(ErrorMessage = "Page size is required. ")]
        [Range(1, BO.Constatnt.Common.MaxPageSize, ErrorMessage = "Please enter a value between 1 to 100")]
        public int PageSize { get; set; }
        /// <summary>
        /// Search using patientname, doctorname, referbyname and patient email
        /// </summary>
        public string SearchText { get; set; }
    }
}
