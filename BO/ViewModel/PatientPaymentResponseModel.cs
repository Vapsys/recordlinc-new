﻿using BO.Models;

namespace BO.ViewModel
{
    public class PatientPaymentResponseModel<T> : APIResponse<T>
    {
        public int TotalRecord { get; set; }
        public int NumberOfPages { get; set; }
        public int PageIndex { get; set; }
    }
}
