﻿using BO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    /// <summary>
    /// List of Schedule coming from the Dentrix.
    /// </summary>
    public class Schedules
    {
        /// <summary>
        /// is Default
        /// </summary>
        public bool isDefault { get; set; }
        /// <summary>
        /// Date is required for the Exceptional schedule not required on Normal schedule
        /// </summary>
        public DateTime? date { get; set; }
        /// <summary>
        /// Day of week start with 0 - 7. Start with Sunday = 0
        /// </summary>
        public int dayOfWeek { get; set; }
        /// <summary>
        /// Time slots ex. ["08:00", "12:00", "13:00", "17:00", null, null ],
        /// </summary>
        public List<string> times { get; set; }
        /// <summary>
        /// Available "available": false,
        /// </summary>
        public bool available { get; set; }
        /// <summary>
        /// Auto modified time stemp
        /// </summary>
        public DateTime? automodifiedtimestamp { get; set; }
        /// <summary>
        /// This is used internally for Martin tracking and can be ignored from our side.
        /// </summary>
        public int practiceID { get; set; }
        /// <summary>
        /// Perimaryid of this record
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// This is a duplicate of id 
        /// </summary>
        public int identifier { get; set; }
        /// <summary>
        /// ProviderId "resourceId": "DDS1",
        /// </summary>
        public string resourceId { get; set; }
        /// <summary>
        /// Dentrix ConnectorId
        /// </summary>
        public string connectorID { get; set; }
    }
}
