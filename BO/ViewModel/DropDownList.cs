﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    /// <summary>
    /// This is common View Model used for Bind Dropdown list.
    /// </summary>
    public class DropDownList
    {
        /// <summary>
        /// Unique_id of data 
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Text value of data
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Account name of Id
        /// </summary>
        public string AccountName { get; set; }
        /// <summary>
        /// Encrypted Id
        /// </summary>
        public string Encryptid { get; set; }
    }
}
