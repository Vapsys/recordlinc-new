﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class ReferPatientModel
    {
        public string Token { get; set; }
        public PMSPatientDetails PMSPatientDetails { get; set; }
        public PMSProviderDetails PMSProviderDetails { get; set; }
    }
}
