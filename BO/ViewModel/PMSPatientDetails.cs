﻿
namespace BO.ViewModel
{
    public class PMSPatientDetails
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Gender { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string PatientId { get; set; }
        public string Mobile { get; set; }
    }
}
