﻿using BO.Models;

namespace BO.ViewModel
{
    public class SendInsuranceRequestBO
    {
        public PatientSignup objPatient { get; set; }
        public decimal TotalCost { get; set; }
        public string ProceduresName { get; set; }
        public int HistoryId { get; set; }
        public string SelectedChoice { get; set; }
        public string InsuranceCompanyName { get; set; }
        public string EstimateViaEmail { get; set; }
        public string EstimateViaCall { get; set; }
        public string EstimateViaText { get; set; }
        public decimal ServiceCost { get; set; }

    }
}
