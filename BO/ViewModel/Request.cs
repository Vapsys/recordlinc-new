﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class Request
    {
        public int FinanceRequestId { get; set; }
        public string FinanceRequestKey { get; set; }
        public int InsuranceRequestId { get; set; }
        public string InsuranceRequestKey { get; set; }
        public int StandardRequestId { get; set; }
        public string StandardRequestKey { get; set; }
        public int AdjustmentRequestId { get; set; }
        public string AdjustmentRequestKey { get; set; }
    }
}
