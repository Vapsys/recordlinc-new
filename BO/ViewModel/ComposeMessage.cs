﻿using System.Collections.Generic;
using BO.Models;



namespace BO.ViewModel
{
    public class ComposeMessage
    {

        public int MessageId { get; set; }
        public int MessageTypeId { get; set; }
        public string ColleagueId { get; set; }
        public List<ColleagueData> ColleagueList { get; set; }
        public List<PatientsData> PatientList { get; set; }
        public string PatientId { get; set; }
        public string MessageBody { get; set; }
        public string FileIds { get; set; }
        public int ComposeType { get; set; }
        public string ForwardFileId { get; set; }
        public int IsColleaguesMessage { get; set; }
        public string BoxFolderId { get; set; }
        public string BoxFileId { get; set; }
        public string BoxFileName { get; set; }
        public bool Issent { get; set; }
    }
    public class PatientsData
    {

        public int id { get; set; }
        public string text { get; set; }
    }
    public class ColleagueData
    {

        public int id { get; set; }
        public string text { get; set; }
        
    }
    public class PatientDetail
    {

        public string PatientName { get; set; }
        public string Email { get; set; }
    }
    public class DoctorDetail
    {

        public string DoctorName { get; set; }
       
    }
    public class Colleaguelist
    {
        public List<ColleagueData> colleagues { get; set; }
        public int RecordCount  { get; set; }
    }
}
