﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
   public class ManageSuperBuck
   {
        public List<ProcedureCode> lstProcedureCode { get; set; }
        public List<PointsBalances> lstPointBalances { get; set; }
        public List<ProcedureExamCodes> lstProcedureExamCodes { get; set; }
        public List<RewardsLongevity> lstRewardsLongevity { get; set; }
        
        public ManageSuperBuck()
        {
            lstProcedureCode = new List<ProcedureCode>();
            lstPointBalances = new List<PointsBalances>();
            lstProcedureExamCodes = new List<ProcedureExamCodes>();
            lstRewardsLongevity = new List<RewardsLongevity>();
        }

    }

   

}
