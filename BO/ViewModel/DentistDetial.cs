﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class DentistDetial
    {
        public int AccountId { get; set; }
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Image { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string ImageName { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string ExactAddress { get; set; }
        public string Address2 { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string PublicProfileName { get; set; }
        public string AccountName { get; set; }
        public string Phone { get; set; }
        public string Zipcode { get; set; }
        public int Miles { get; set; }
        public string Gender { get; set; }
        public int RowNumber { get; set; }
        public int TotalRecord { get; set; }
        public string Email { get; set; }
        public string Specialty { get; set; }
        public string MapAddress { get; set; }
        public string FullAddress { get; set; }
        public string Location { get; set; }
        public int Count { get; set; }
    }
    #region RewardPartner Call API related Classes
    public class PatientRewardMDL
    {
        public int external_company_id { get; set; }
        public string name { get; set; }
        public string support_email { get; set; }
        public string support_phone { get; set; }
        public string contact_first_name { get; set; }
        public string contact_last_name { get; set; }
        public string contact_phone { get; set; }
        public string logo { get; set; }
        public string status { get; set; }
        public string address_1 { get; set; }
        public string address_2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip_code { get; set; }
        
    }
    public class company
    {
        public int company_id { get; set; }
        public int external_company_id { get; set; }
    }
    public class responsemdl
    {
        public company company { get; set; }
        public int tran_id { get; set; }
        public string message { get; set; }
    }

    public class location
    {
        public int location_id { get; set; }
        public int company_id { get; set; }
        public int external_location_id { get; set; }
    }

    public class responseLoc
    {
        public location location { get; set; }
        public int tran_id { get; set; }
    }


    public class errors
    {
        public string message { get; set; }
        public int code { get; set; }
        public string field { get; set; }
    }

    public class APIResponse
    {
        public int tran_id { get; set; }
        public int? company_id { get; set; }
        public string message { get; set; }
        public string field { get; set; }
        public int? loctionId { get; set; }
    }

    public class responseerror
    {
        public List<errors> errors { get; set; }
        public int tran_id { get; set; }
    }
    public class Location
    {
        public int company_id { get; set; }
        public int external_location_id { get; set; }
        public string name { get; set; }
        public string contact_first_name { get; set; }
        public string contact_last_name { get; set; }
        public string contact_phone { get; set; }
        public string address_1 { get; set; }
        public string address_2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip_code { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
    }

    public class LocationDetail
    {
        public string LocationName { get; set; }
        public string LocationId { get; set; }
    }

    #endregion

}
