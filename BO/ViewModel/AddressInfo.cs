﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class AddressInfo
    {

        /// <summary>
        /// Primary Key
        /// </summary>
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Zipcode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string ExternalPMSId { get; set; }
    }
    public class Locationinfo
    {
        public List<AddressInfo> Locations { get; set; }
    }
}
