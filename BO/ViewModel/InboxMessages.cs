﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class InboxMessages
    {
        public int MessageId { get; set; }
        public string Field { get; set; }
        public string Body { get; set; }
        public DateTime CreationDate { get; set; }
        public int TotalRecord { get; set; }
        public int MessageTypeId { get; set; }
        public bool IsRead { get; set; }
        public string AttachmentId { get; set; }
        public string PatientName { get; set; }
        public int MessageDisplayType { get; set; }
        public string ReceiverName { get; set; }
        public string MessageBody { get; set; }
        public string SenderName { get; set; }
        public int ReceiveId { get; set; }
        public int PatientId { get; set; }
        public int SenderId { get; set; }
        public int IsColleagueMesage { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public DateTime CurrentDate { get; set; }
    }
    public class TypesofMessage
    {
       public List<string> ColleagueMessageId { get; set; }
        public List<string> PatientMessageId { get; set; }
    }
    public class UnreadMessageCount
    {
        public int Count { get; set; }
    }   
}
