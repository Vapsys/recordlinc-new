﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
  public class PublicProfileDetail
    {
        public string PublicProfileUrl { get; set; }
        public int UserId { get; set; }
    }

    public class WebSiteUrl
    {
        public string WebsiteUrl { get; set; }
        public int UserId { get; set; }
        public int? WebsiteId { get; set; }
    }
}
