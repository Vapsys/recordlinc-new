﻿using System;
using System.Collections.Generic;
using BO.Models;

namespace BO.ViewModel
{
    public class NormalMessage
    {
        public int MessageId { get; set; }
        public List<NormalMessageDetails> MessageList { get; set; }
    }
}
