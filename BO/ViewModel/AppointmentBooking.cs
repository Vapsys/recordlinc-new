﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Models;

namespace BO.ViewModel
{
    public class AppointmentBooking
    {
        public int UserId { get; set; }
        public int AppointmentDefaultResourceId { get; set; }
        public DateTime AppointmentDate { get; set; }
        public TimeSpan AppointmentTime { get; set; }
        public int ServiceTypeId { get; set; }
        public PatientSignup ObjPatientSignup { get; set; }
        public PatientLogin ObjPatientLogin { get; set; }
        public DentistProfileDetail ObjDentistProfile { get; set; }
        public AddressDetails AddressInfo {get;set;}
        public string TimeZoneName { get; set; }
        public string ApptTime { get; set; }
        public string ApptDate { get; set; }
        public int InsuranceId { get; set; }
    }
}
