﻿
namespace BO.ViewModel
{
    public class InsuranceListForAppointment
    {
        public int InsuranceId { get; set; }
        public string InsuranceName { get; set; }
    }
}
