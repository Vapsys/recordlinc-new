﻿namespace BO.ViewModel
{
    public class AppointmentReportModel
    {
        public int AppointmentId { get; set; }
        public int DoctorId { get; set; }
        public int PatientId { get; set; }
        public string PatientFirstName { get; set; }
        public string PatientLastName { get; set; }
        public string DoctorName { get; set; }
        public string AppointmentDate { get; set; }
        public string AppointmentTime { get; set; }
        public string AppointmentType { get; set; }
        public string PatientDOB { get; set; }
        public string NextAppointmentDate { get; set; }
        public string NextAppointmentType { get; set; }
        public string LastAppointmentDate { get; set; }
        public string LastHygieneDate { get; set; }
        public string LastHygieneTime { get; set; }
        public string HygieneType { get; set; }
        public string PatientAge { get; set; }
        public string AppointmentReason { get; set; }
        public string Alert { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
    }
}
