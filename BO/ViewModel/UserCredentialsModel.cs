﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class UserCredentialsModel
    {
        public string Username { get; set; }
        [Display(Name = "Password")]
        [Required(ErrorMessage = "Please enter password.")]
        public string Password { get; set; }
    }
}
