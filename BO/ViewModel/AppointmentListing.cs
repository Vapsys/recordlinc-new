﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    /// <summary>
    /// Appointment Listing
    /// </summary>
    public class AppointmentListing
    {
        /// <summary>
        /// Appointment id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Appointment date
        /// </summary>
        public DateTime? Date { get; set; }
        /// <summary>
        /// Appointment time
        /// </summary>
        public string Time { get; set; }
        /// <summary>
        /// Appointment length
        /// </summary>
        public string DurationInMinutes { get; set; }
        /// <summary>
        /// Note
        /// </summary>
        public string Note { get; set; }
        /// <summary>
        /// Cancellation note
        /// </summary>
        public string CancellationNote { get; set; }
        /// <summary>
        /// Appointment status
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Doctor Details
        /// </summary>
        public DoctorDetials Doctor { get; set; }
        /// <summary>
        /// Doctor Services
        /// </summary>
        public Services Services { get; set; }
        /// <summary>
        /// Doctor Resource
        /// </summary>
        public Resource Operatory { get; set; }
        /// <summary>
        /// Patient details
        /// </summary>
        public AppointmentPatientDetail Patient { get; set; }
    }

    /// <summary>
    /// Doctor Services
    /// </summary>
    public class Services
    {
        /// <summary>
        /// Service id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Service Type
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Service Time
        /// </summary>
        public double Price { get; set; }
        /// <summary>
        /// Service Time
        /// </summary>
        public int DurationInMinutes { get; set; }
    }

    /// <summary>
    /// Doctor Resource
    /// </summary>
    public class Resource
    {
        /// <summary>
        /// Resource id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Resource Name
        /// </summary>
        public string Name { get; set; }

    }

    /// <summary>
    /// Doctor Details
    /// </summary>
    public class DoctorDetials
    {
        /// <summary>
        /// Doctor id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Doctor First Name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Doctor Last Name
        /// </summary>
        public string LastName { get; set; }
    }

    /// <summary>
    /// Appointment patient detail
    /// </summary>
    public class AppointmentPatientDetail
    {
        /// <summary>
        /// Patient id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// First name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Last name
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Gender male/ female
        /// </summary>
        public string Gender { get; set; }
        /// <summary>
        /// Middel name
        /// </summary>
        public DateTime? DateOfBirth { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Work phone number
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// Has patient insurance
        /// </summary>
        public string Insurance { get; set; }
        /// <summary>
        /// Insurance company name
        /// </summary>
        public string InsuranceCompanyName { get; set; }
    }

    /// <summary>
    /// Dentrix appointment
    /// </summary>
    public class DentrixAppointment
    {


        ///// <summary>
        ///// Appointment from date
        ///// </summary>
        //[Required(ErrorMessage = "Start date is required. ")]
        //public DateTime StartDate { get; set; }
        ///// <summary>
        ///// Appointment from date
        ///// </summary>
        //[Required(ErrorMessage = "End date is required. ")]
        //public DateTime EndDate { get; set; }
        ///// <summary>
        ///// Page index which has parse between 1 to 32767
        ///// </summary>
        [Required(ErrorMessage = "Page index is required. ")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1")]
        public int PageIndex { get; set; }
        /// <summary>
        /// Page size hich has parse between 1 to 1500
        /// </summary>
        [Required(ErrorMessage = "Page size is required. ")]
        [Range(1, BO.Constatnt.Common.MaxPageSize, ErrorMessage = "Please enter a value between 1 to 100")]
        public int PageSize { get; set; }
        /// <summary>
        /// DentistId parameter is use for searching perpose.
        /// </summary>
        public int? DentistId { get; set; }
        /// <summary>
        /// LocationId parameter is use for searching perpose.
        /// </summary>
        public int? LocationId { get; set; }
        /// <summary>
        /// PatientName parameter is use for searching perpose.
        /// </summary>
        public string PatientName { get; set; }
        /// <summary>
        /// Appointment status.
        /// </summary>
        public int? StatusId { get; set; }
    }

    /// <summary>
    /// For book appointment
    /// </summary>
    public class BookAppointment
    {
        /// <summary>
        /// Appointment id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Appointment doctor id
        /// </summary>
        [Required(ErrorMessage = "Doctor id is required. ")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a valid doctor id.")]
        public int DoctorId { get; set; }

        /// <summary>
        /// Provider id
        /// </summary>
        public string ProviderId { get; set; }

        /// <summary>
        /// Appointment date
        /// </summary>
        [Required(ErrorMessage = "Appointment date is required. ")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }

        /// <summary>
        /// Appointment time
        /// </summary>
        [Required(ErrorMessage = "Appointment time is required. ")]
        [DataType(DataType.Time)]
        public TimeSpan Time { get; set; }

        /// <summary>
        /// Appointment duration in minutes
        /// </summary>
        [Required(ErrorMessage = "Appointment time duration in minutes is required. ")]
        [Range(1,1000, ErrorMessage = "Please enter a value between 1 and 1000")]
        public int DurationInMinutes { get; set; }

        //2147483647
        /// <summary>
        /// Appointment service id
        /// </summary>
        [Required(ErrorMessage = "Service id is required. ")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a valid service id")]
        public int ServiceId { get; set; }

        /// <summary>
        /// Appointment resource id
        /// </summary>
        [Required(ErrorMessage = "Resource id is required. ")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a valid resource id")]
        public int ResourceId { get; set; }

        /// <summary>
        /// Appointment operatory id
        /// </summary>
        [Required(ErrorMessage = "Operatory id is required. ")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a valid operatory id")]
        public int OperatoryId { get; set; }

        /// <summary>
        /// Appointment note
        /// </summary>
        public string Note { get; set; }

        /// <summary>
        /// Dentrix connector id
        /// </summary>
        [Required(ErrorMessage = "DentrixConnectorID is required.")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a valid dentrix connector id")]
        public int DentrixConnectorID { get; set; }

        /// <summary>
        /// patient insurance id
        /// </summary>
        public int InsuranceId { get; set; }

        /// <summary>
        /// patient id
        /// </summary>
        [Required(ErrorMessage = "Patient id is required. ")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a valid patient id")]
        public int PatientId { get; set; }

    }



}
