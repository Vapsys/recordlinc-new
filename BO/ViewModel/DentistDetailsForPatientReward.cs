﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BO.Enums.Common;

namespace BO.ViewModel
{
    public class DentistDetailsForPatientReward
    {
        public int UserId { get; set; }
        public int AccountId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Image { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string ImageName { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string ExactAddress { get; set; }
        public string Address2 { get; set; }
        public string PublicProfileName { get; set; }
        public string WorkPhone { get; set; }
        public string HomePhone { get; set; }
        public string Mobile { get; set; }
        public string Zipcode { get; set; }
        //public string Gender { get; set; }
        public string Email { get; set; }
        public string Specialty { get; set; }
        public string MapAddress { get; set; }
        public string FullAddress { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string EncrypteUserId { get; set; }
        public string RewardCompanyId { get; set; }
        public string RewardLocationId { get; set; }
        public string Description { get; set; }
        public string Salutation { get; set; }
        public PMSProviderType ProviderType { get; set; }
        public PMSProviderFilterType ProviderFilterType { get; set; }
        public string AccountName { get; set; }
    }
    public class OCRColleagueList {
       public string EncrypteUserId { get; set; }
       public PMSProviderFilterType ProviderType { get; set; }
    }
}
