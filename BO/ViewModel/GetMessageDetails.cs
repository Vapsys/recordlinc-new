﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class GetMessageDetails
    {
        public int MessageId { get; set; }
        public int MessageDisplayType { get; set; }
        public int MessageTypeId { get; set; }
        public int IsColleagueMesage { get; set; }
        public bool Isread { get; set; }
    }
}
