﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace BO.ViewModel
{
    public class SendReferralModel
    {
        [Display(Name = "First Name*")]
        [Required(ErrorMessage = "Patient first name is required.")]
        [RegularExpression(@"^([a-zA-Z0-9 \.\&\'\-]+)$", ErrorMessage = "The patient first name should be non-numeric.")]        
        public string PatientFirstName { get; set; }

        [Display(Name = "Last Name*")]
        [RegularExpression(@"^([a-zA-Z0-9 \.\&\'\-]+)$", ErrorMessage = "The patient last name should be non-numeric.")]
        [Required(ErrorMessage = "Patient last name is required.")]        
        public string PatientLastName { get; set; }

        [Display(Name = "Phone*")]
        [Required(ErrorMessage = "Patient phone number is required.")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^(?:\([0-9]\d{2}\)\ ?|[0-9]\d{2}(?:\-?|\ ?))[0-9]\d{2}[- ]?\d{4}$", ErrorMessage = "Not a valid phone number.")]
        public string PatientPhone { get; set; }

        [Display(Name ="Email Address*")]
        [Required(ErrorMessage = "Patient email address is required.")]
        [EmailAddress(ErrorMessage = "Invalid email address entered.")]
        public string PatientEmail { get; set; }

        [Display(Name = "First Name*")]
        [Required(ErrorMessage = "Doctor first name is required.")]
        [RegularExpression(@"^(?![\W_]+$)(?!\d+$)[a-zA-Z0-9 .&',_-]+$", ErrorMessage = "The doctor first name should be non-numeric.")]
        public string DoctorFirstName { get; set; }

        [Display(Name = "Last Name*")]
        [Required(ErrorMessage = "Doctor last name is required.")]
        [RegularExpression(@"^(?![\W_]+$)(?!\d+$)[a-zA-Z0-9 .&',_-]+$", ErrorMessage = "The doctor last name should be non-numeric.")]
        public string DoctorLastName { get; set; }

        [Display(Name = "Phone*")]
        [Required(ErrorMessage = "Doctor phone number is required.")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^(?:\([0-9]\d{2}\)\ ?|[0-9]\d{2}(?:\-?|\ ?))[0-9]\d{2}[- ]?\d{4}$", ErrorMessage = "Not a valid phone number.")]
        public string DoctorPhone { get; set; }

        [Display(Name = "Email Address*")]
        [Required(ErrorMessage = "Doctor email address is required.")]
        [EmailAddress(ErrorMessage = "Invalid email address entered.")]
        public string DoctorEmail { get; set; }

        [Display(Name = "Comments")]
        public string Comments { get; set; }

        //public List<string> Files { get; set; }
        //public List<string> FileNames { get; set; }
        public string FileIds { get; set; }

        public string username { get; set; }

        public int UserId { get; set; }

        public string AccountName { get; set; }

    }
}
