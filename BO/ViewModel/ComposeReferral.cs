﻿using BO.Models;

namespace BO.ViewModel
{
    public class ComposeReferral
    {
        public NewPatientDetailsOfDoctor PatientDetails { get; set; }
        public string DoctorFullName { get; set; } // this field use in send referral
        public string Notes { get; set; }
        public string FirstExamDate { get; set; }
        public string Location { get; set; }
        public int LocationID { get; set; }
        public string ReferredBy { get; set; }
        public string ReferredByName { get; set; }
        public string IsAuthorize { get; set; }
        public string ColleagueIds { get; set; }// this field use when request for send referral comes from colleague page.
        public string ColleagueFullName { get; set; }
    }
    public class PatientImages
    {
        public int ImageId { get; set; }
        public int ImageType { get; set; }
        public int MontageId { get; set; }
        public string CreationDate { get; set; }
        public string LastModifiedDate { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }

    }
    public class PateintDocuments
    {
        public int DocumentId { get; set; }
        public int MontageId { get; set; }
        public string DocumentName { get; set; }
        public string Description { get; set; }
        public string CreationDate { get; set; }
        public string LastModifiedDate { get; set; }
    }
}
