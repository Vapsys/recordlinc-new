﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BO.ViewModel
{
  public class RewardsLongevity
    {
        public int LM_Id  { get; set; }
        //[Required(ErrorMessage = "Please Enter MonthsofLongevity")]
        public int? MonthsofLongevity { get; set; }
        public decimal? Multiplier { get; set; }
        public int AccountId { get; set; }
        public int PlanId { get; set; }
        public string PlanName { get; set; }
        public int CreatedBy { get; set; }
        public bool IsDeleted { get; set; }

    }

}
