﻿using BO.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BO.Enums.Common;

namespace BO.ViewModel
{
    public class DoctorDetails
    {
        public DoctorDetails()
        {
            DentixDetails = new DentrixDetail();
        }

        /// <summary>
        /// Primary key
        /// </summary>
        public int DoctorId { get; set; }
        public string DoctorFullName { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public string Education { get; set; }
        public string Specialties { get; set; }
        public int AccountId { get; set; }
        public string AccountName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int OrganizationId { get; set; }
        public int OrganizationLocationId { get; set; }
        public int RecordlincLocationId { get; set; }
        public string Dentrixprovid { get; set; }
        public List<PracticeWebsites> PracticeWebsites = new List<PracticeWebsites>();
        public List<SocialMediaWebsites> SocialMediaWebsites = new List<SocialMediaWebsites>();
        public List<SpecialitesDetails> SpecialitesDetails = new List<SpecialitesDetails>();
        public List<AddressesModel> Addresses = new List<AddressesModel>();
        public List<EducationDetails> EducationDetails = new List<EducationDetails>();
        public List<ProfessionalMembershipsDetails> ProfessionalMemberships = new List<ProfessionalMembershipsDetails>();
        public List<TeamMembers> TeamMembers = new List<TeamMembers>();
        public DentrixDetail DentixDetails { get; set; }
        public PMSProviderType ProviderType { get; set; }
    }

    public class SocialMediaWebsites
    {
        /// <summary>
        /// Primary key
        /// </summary>
        public string SocialMediaWebsiteId { get; set; }
        public string SocialMediaWebsiteUrl { get; set; }
    }

    public class PracticeWebsites
    {
        /// <summary>
        /// Primary key
        /// </summary>
        public int PracticeWebsiteId { get; set; }
        public string PracticeWebsiteUrl { get; set; }
    }

    public class SpecialitesDetails
    {
        /// <summary>
        /// Primary key
        /// </summary>
        public int SpecialtyId { get; set; }
        public string SpecialtyDescription { get; set; }
    }

    public class AddressesModel
    {
        /// <summary>
        /// Primary key
        /// </summary>
        public int AddressId { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Zipcode { get; set; }
        public string Phone { get; set; }
        public string AddressType { get; set; }
        public string Email { get; set; }
        public int LocationId { get; set; }
    }

    public class EducationDetails
    {
        /// <summary>
        /// Primary key
        /// </summary>
        public int EducationId { get; set; }
        public string Institute { get; set; }
        public string Course { get; set; }
        public string Degree { get; set; }
        public string EducationCity { get; set; }
        public string EducationState { get; set; }
        public string StartYear { get; set; }
        public string EndYear { get; set; }
    }

    public class ProfessionalMembershipsDetails
    {
        /// <summary>
        /// Primary key
        /// </summary>
        public int MembershipId { get; set; }
        public string Membership { get; set; }
    }

    public class TeamMembers
    {
        /// <summary>
        /// Primary key
        /// </summary>
        public int DoctorId { get; set; }
        public string Name { get; set; }
    }


    public class DentrixDetail
    {
        public int ProviderType { get; set; }
        public int ProviderClass { get; set; }
        public string ProviderTypeString { get; set; }
        public int IdNumber { get; set; }
        public int ssn { get; set; }
        public int feeSched { get; set; }
        public int phoneNext { get; set; }
        public int Isnonperson { get; set; }
        public int Provnum { get; set; }
    }

    public class TeamMembersOCR
    {
        public string Name { get; set; }
        public string DoctorId { get; set; }
        public int id { get; set; }
    }

}
