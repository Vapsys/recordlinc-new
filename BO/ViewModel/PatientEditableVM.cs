﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class PatientEditableVM
    {
        public string AssignedPatientId { get; set; }
        public string GenderId { get; set; }
        public string DobId { get; set; }
        public string PhoneId { get; set; }
        public string EmailId { get; set; }
        public string PatientId { get; set; }
        public string AddressId { get; set; }

        public string SecondryEmail { get; set; }
        public string AddressInfoEmail { get; set; }

        public string Mobile { get; set; }
        public string Workphone { get; set; }
        public string Fax { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string State { get; set; }

    }
}
