﻿namespace BO.ViewModel
{
    public class LocationForAppointment
    {
        public int locationId { get; set; }
        public string locationName { get; set; }
    }
}
