﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Models;

namespace BO.ViewModel
{
    public class SpecialityreferralList
    {
        public SpecialityService SpecialityServiceList { get; set; }
        public PatientDentistReferral ReferalObj { get; set; }
    }
}
