﻿using BO.Models;
using System.Collections.Generic;

namespace BO.ViewModel
{
    public class PlanAndPricing
    {
        public Members Signup { get; set; }
        public BillingInfo ObjBillingInfo { get; set; }
        public int MembershipId { get; set; }
        public List<PlanAndPrice> lstPlanPrice { get; set; }
        public MainPlanPricing objPlanPricing { get; set; }
    }
}
