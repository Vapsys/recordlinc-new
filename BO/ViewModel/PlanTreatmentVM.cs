﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class PlanTreatmentVM
    {
        public List<PlanStage> jsonObj { get; set; }       
        public string isAccepted { get; set; }
        public string acceptdate { get; set; }
        public string fullname { get; set; }
        public string dbo { get; set; }
        public string PatientId { get; set; }
        public string Status { get; set; }
        public string id { get; set; }
        public string hdncaseId { get; set; }
        public string hdnPatientId { get; set; } 
    }

    public class PlanStage
    {
        public string id { get; set; }
        public string desc { get; set; }
        public string date { get; set; }
        public string provider { get; set; }
        public string stepOrder { get; set; }        
        public string caseid { get; set; }
        public string casestepid { get; set; }
    }

    public class PlanTreatmentReport
    {
        public string PatientName { get; set; }
        public string PatientId { get; set; }
        public string CaseId { get; set; }        
        public string DoctorName { get; set; }
        public string CaseName { get; set; }
        public string DoctorId { get; set; }
        public string IsTreatmentAccepted { get; set; }
        public string TreatmentAcceptedDate { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }   
        public string AccountMember { get; set; }
        public List<Models.CaseSteps> lstPlanSteps { get; set; }
        public string Status { get; set; }
    }
}
