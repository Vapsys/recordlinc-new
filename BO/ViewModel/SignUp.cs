﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Models;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Mvc;
namespace BO.ViewModel
{
    public class SignUp
    {

    }
    /// <summary>
    ///  User sign up for verident
    /// </summary>
    public class UserSignUpForVeri
    {
        [Required(ErrorMessage = "Please enter your email address.")]        
        [EmailAddress(ErrorMessage = "Please enter valid email address.")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Phone number is required.")]        
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Please enter valid phone number.")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Please enter speciality.")]
        public string Speciality { get; set; }

        [Required(ErrorMessage = "Please enter practice name.")]
        public string PracticeName { get; set; }

        [Required(ErrorMessage = "Please enter first name.")]
        [Display(Name = "First Name")]
        [StringLength(30, ErrorMessage = "First name must not exceed 30 characters.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter last name.")]
        [Display(Name = "Last Name")]
        [StringLength(30, ErrorMessage = "Last name must not exceed 30 characters.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter npi number.")]
        public string NpiNumber { get; set; }

        public string Password { get; set; }
    }
}
