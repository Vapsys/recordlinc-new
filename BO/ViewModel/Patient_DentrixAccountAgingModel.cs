﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class Patient_DentrixAccountAgingModel
    {        
        public DateTime? AutoModifiedTimestamp { get; set; }
        public int GuarId { get; set; }
        public DateTime? AgingDate { get; set; }
        public decimal? Aging0to30 { get; set; }
        public decimal? Aging31to60 { get; set; }
        public decimal? Aging61to90 { get; set; }
        public decimal? Aging91plus { get; set; }
        public DateTime? LastPaymentDate { get; set; }
        public DateTime? LastInsurancePayDate { get; set; }
        public DateTime? LastBillingDate { get; set; }
        public string dentrixConnectorId { get; set; }
    }
}
