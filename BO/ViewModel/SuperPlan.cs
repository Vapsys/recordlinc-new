﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace BO.ViewModel
{
    public class SuperPlan
    {
        public int PlanId { get; set; }

        [Required(ErrorMessage = "Enter Plan Name")]
        public string PlanName { get; set; }

        public int UserId { get; set; }

        [Required(ErrorMessage = "Enter Amount")]
        public decimal? Amount { get; set; }

        public int CreatedBy { get; set; }

        public Boolean IsDeleted { get; set; }
        public Boolean IsPrimary { get; set; }
        public decimal? RewardPoint { get; set; }
        public decimal? Points { get; set; }
        public int? ExamCodes { get; set; }
    }
}
