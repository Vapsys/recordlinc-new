﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
   public class MemberPlanDetail
    {
        public int FeatureId { get; set; }
        public int  MemberShipId { get; set; }
        public bool? CanSee { get; set; }
        public string MaxCount { get; set; }
    }
}
