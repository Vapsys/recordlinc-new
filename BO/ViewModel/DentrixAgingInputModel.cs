﻿using System.ComponentModel.DataAnnotations;

namespace BO.ViewModel
{
    public class DentrixAgingInputModel
    { 

        /// <summary>
        /// Id of the patient associated with dentist (of Recordlink)
        /// </summary>
        [Required(ErrorMessage = "PatientId is required.")]
        public int PatientId { get; set; }
    }
}
