﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class MetaTag
    {
        public string SiteName { get; set; }
        public string type { get; set; }
        public string Description { get; set; }
        public string URL { get; set; }
        public string image { get; set; }
        public string Title { get; set; }
    }
}
