﻿using BO.Models;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;

namespace BO.ViewModel
{
    /// <summary>
    /// Dentist Profile for OCR AND Recordlinc Profile page.
    /// </summary>
    public class DentistProfileViewModel
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }
        public string OfficeName { get; set; }
        public string Title { get; set; }
        public string ImageName { get; set; }
        public string Email { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string DentrixProviderId { get; set; }
        public string PublicProfile { get; set; }
        public string WebsiteURL { get; set; }
        public int TeamMemberUserId { get; set; }
        public string Location { get; set; }
        public int ProfileComplete { get; set; }
        public string Profilepercentage { get; set; }
        public string RemainList { get; set; }
        public string Institute { get; set; }
        public string MemberShip { get; set; }
        public string EncryptUserId { get; set; }
        public int LocationId { get; set; }
        public string Salutation { get; set; }
        public string specialtyIds { get; set; }
        public List<Banner> lstBanner { get; set; }
        public List<Gallery> lstGallary { get; set; }
        public List<Insurance> lstInsurance { get; set; }
        public string GallaryPath { get; set; }
        public List<AddressDetails> lstDoctorAddressDetails = new List<AddressDetails>();
        public List<AddressDetails> lstDoctorSortedAddressDetails = new List<AddressDetails>();
        public List<EducationandTraining> lstEducationandTraining = new List<EducationandTraining>();
        public List<ProfessionalMemberships> lstProfessionalMemberships = new List<ProfessionalMemberships>();
        public List<SpeacilitiesOfDoctor> lstSpeacilitiesOfDoctor = new List<SpeacilitiesOfDoctor>();
        public List<Procedure> lstProcedure = new List<Procedure>();
        public List<LicenseDetailsForDoctor> licensedetails = new List<LicenseDetailsForDoctor>();
        public LicenseDetailsForDoctor objLicense { get; set; }
        public SectionPublicProfileViewModel ObjProfileSection { get; set; }
        public List<AddressDetails> lstDoctorAddressDetailsByAddressInfoID = new List<AddressDetails>();
        public List<SocialMediaForDoctorViewModel> lstGetSocialMediaDetailByUserId = new List<SocialMediaForDoctorViewModel>();
        public List<EducationandTraining> lstEducationandTrainingForDoctorById = new List<EducationandTraining>();
        public List<ProfessionalMemberships> lstProfessionalMembershipForDoctorById = new List<ProfessionalMemberships>();
        public List<WebsiteDetails> lstsecondarywebsitelist = new List<WebsiteDetails>();
        public List<TimeZonesViewModel> lstTimeZone { get; set; }
        public List<TeamMemberDetailsForDoctorViewModel> lstTeamMemberDetailsForDoctor { get; set; }
    }
    public class TeamMemberDetailsForDoctorViewModel
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string SpecialtyDescription { get; set; }
        public string PublicProfileUrl { get; set; }
        public string AccountName { get; set; }

    }
    public class Gallery
    {
        public int GallaryId { get; set; }
        public string VideoURL { get; set; }
        public HttpPostedFileBase File { get; set; }
        public string FilePath { get; set; }
        public string FileName { get; set; }
        public int UserId { get; set; }
    }
    public class SectionPublicProfileViewModel
    {
        public int Userid { get; set; }
        public bool PatientForms { get; set; }
        public bool SpecialOffers { get; set; }
        public bool ReferPatient { get; set; }
        public bool Reviews { get; set; }
        public bool AppointmentBooking { get; set; }
        public bool PatientLogin { get; set; }
    }
    public class SocialMediaForDoctorViewModel
    {
        public string FacebookUrl { get; set; }
        public string LinkedinUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string GoogleplusUrl { get; set; }
        public string YoutubeUrl { get; set; }
        public string PinterestUrl { get; set; }
        public string BlogUrl { get; set; }
        public string InstagramUrl { get; set; }
        public string YelpUrl { get; set; }
    }
    public class TimeZonesViewModel
    {
        public int TimeZoneId { get; set; }
        public string TimeZoneText { get; set; }
    }
    /// <summary>
    /// Get Team member details.
    /// </summary>
    public class TeamMemberDetails
    {
        /// <summary>
        /// Get Location List while get team member details
        /// </summary>
        public List<LocationDetails> LocationList { get; set; }
        /// <summary>
        /// First Name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// LastName
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// UserId
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// LocationId
        /// </summary>
        public int LocationId { get; set; }
        /// <summary>
        /// PMS ProviderId
        /// </summary>
        public string ProviderId { get; set; }
        /// <summary>
        /// Get Selected SpecialtyId
        /// </summary>
        public int SpecialtyId { get; set; }
        /// <summary>
        /// Get List of Specialty while getting team member details
        /// </summary>
        public List<SelectListItem> SpecialtyList { get; set; }
    }
    public class LocationDetails
    {
        public string LocationName { get; set; }
        public int AddressInfoId { get; set; }
    }
}
