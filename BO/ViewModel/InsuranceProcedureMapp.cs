﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.ViewModel
{
    public class InsuranceProcedureMapp
    {
        public int ProcedureId { get; set; }
        public int InsuranceId { get; set; }
        public decimal InsuredAmount { get; set; }
    }
}
