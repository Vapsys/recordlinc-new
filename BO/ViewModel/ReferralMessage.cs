﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Models;

namespace BO.ViewModel
{
    public class ReferralMessage
    {
        public List<ReferralMessageDetails> ReferralMessageList { get; set; }
        public List<ReferralCategory> ReferralCategory { get; set; }
    }
}
