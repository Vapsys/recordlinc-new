using System.Collections.Generic;

namespace BO.ViewModel
{
    public class CalculateInsuranceModel
    {
        /* Input Parameters */
        public List<int> DentalProcedureId { get; set; }
        public bool isInsurance { get; set; }
        public int InsuranceCompanyId { get; set; }
        public string EstimateViaCall { get; set; }
        public string EstimateViaText { get; set; }
        public string EstimateViaEmail { get; set; }
        public int FeesType { get; set; }
        /* Output parameters */
        public string ProceduresName { get; set; }
        public decimal TotalCost { get; set; }
        public decimal ServiceCost { get; set; }
        public decimal ThreePayments { get; set; }
        public decimal PaymentEMI { get; set; }
        public int HistoryId { get; set; }
        public string InsuranceCompanyName { get; set; }
        public string PatientName { get; set; }
    }
}