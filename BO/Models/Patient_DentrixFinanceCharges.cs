﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BO.Models
{
    public class Patient_DentrixFinanceCharges
    {
        [Required(ErrorMessage = "ChargeId is required. ")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1")]
        public int chargeId { get; set; }
        public int patid { get; set; }
        public int guarid { get; set; }
        public DateTime? procdate { get; set; }
        public string provid { get; set; }
        public int chargetype { get; set; }
        public string chargetypestring { get; set; }
        public decimal amount { get; set; }
        public bool history { get; set; }
        public DateTime? automodifiedtimestamp { get; set; }
        [Required(ErrorMessage = "DentrixConnectorId is required. ")]
        public string dentrixConnectorID { get; set; }
        /// <summary>
        /// Indicates Entry / created date from PMS.
        /// </summary>
        public DateTime? createdate { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class FinanceCharges
    {

        /// <summary>
        /// RL Primary key
        /// </summary>
        public int RL_ID { get; set; }
        /// <summary>
        /// Dentrix Primary key
        /// </summary>
        public int chargeId { get; set; }
        /// <summary>
        /// Patient Id
        /// </summary>
        public int patid { get; set; }
        /// <summary>
        /// Guaranteer Id of Recordlinc
        /// </summary>
        public int guarid { get; set; }
        /// <summary>
        /// Procedure date
        /// </summary>
        public DateTime? procdate { get; set; }
        /// <summary>
        /// Dentrix Provider Id
        /// </summary>
        public string Dentrixprovid { get; set; }
        /// <summary>
        /// Recordlinc Provider Id
        /// </summary>
        public string RLprovid { get; set; }
        /// <summary>
        /// Charge Type
        /// </summary>
        public int chargetype { get; set; }
        /// <summary>
        /// Describes type of charge. 
        /// </summary>
        public string chargetypestring { get; set; }
        /// <summary>
        /// Amount
        /// </summary>
        public decimal amount { get; set; }
        /// <summary>
        /// The history get from Dentrix.
        /// </summary>
        public bool history { get; set; }
        /// <summary>
        /// Auto Modified Timestamp of Dentrix 
        /// </summary>
        public DateTime? automodifiedtimestamp { get; set; }
        /// <summary>
        /// Dentrix PatientId
        /// </summary>
        public int DentrixPatientId { get; set; }
        /// <summary>
        /// Dentrix Guarantor Id
        /// </summary>
        public int DentrixGuarId { get; set; }

        /// <summary>
        /// Entry date / Creation date of Dentrix
        /// </summary>
        public DateTime? createdate { get; set; }
    }
}
