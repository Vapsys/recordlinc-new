﻿namespace BO.Models
{
    /// <summary>
    /// TimeZone list for doctor.
    /// </summary>
    public class TimeZones
    {
        /// <summary>
        /// Time Zone Id
        /// </summary>
        public int TimeZoneId { get; set; }
        /// <summary>
        /// Time Zone Text
        /// </summary>
        public string TimeZoneText { get; set; }
    }
}
