﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class RewardsMaster
    {
        public int RewardsMasterId { get; set; }
        public string RewardsMasterCode { get; set; }        
        public double RewardsMasterPoints { get; set; }
        public int RewardId { get; set; }
    }
}
