﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class CommonParameter
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int SortColumn { get; set; }
        public int SortDirection { get; set; }
        public string SearchText { get; set; }
    }
}
