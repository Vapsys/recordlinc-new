﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class Patient_DentmaDetails
    {
        public int SubscriptionID { get; set; }
        public int PMSID { get; set; }
        public int PMSGuarantorID { get; set; }
        public string PMSGuarantorFirstName { get; set; }
        public DateTime PMSFirstVisitDate { get; set; }
        public DateTime PMSLastVisitDate { get; set; }
        public int PMSProviderID { get; set; }
        public string PMSProviderName { get; set; }
        public double PMSInsuranceRenewalMonth { get; set; }
        public bool PMSIsActive { get; set; }
        public bool PMSIsArchived { get; set; }
        public bool PMSHasEmail { get; set; }
        public bool PMSHasInsurance { get; set; }
        public bool PMSHasMobile { get; set; }
        public string PMSOffice { get; set; }
        public string PMSRecentCompletedProcCodes { get; set; }
        public string PMSReferralFirstName { get; set; }
        public string PMSReferralLastName { get; set; }
        public string PMSReferralAddress1 { get; set; }
        public string PMSReferralAddress2 { get; set; }
        public string PMSReferralCityStateZip { get; set; }
        public string PMSReferralEmail { get; set; }
        public string PMSRecallID { get; set; }
        public string PMSRecallType { get; set; }
        public DateTime PMSRecallDueDate { get; set; }
        public bool PMSHasRecallPlan { get; set; }
        public bool PMSRecallAppointmentScheduled { get; set; }
        public string PMSAppointmentID_Slot1 { get; set; }
        public DateTime PMSAppointmentDate_Slot1 { get; set; }
        public string PMSAppointmentTime_Slot1 { get; set; }
        public string PMSAppointmentProcCodes_Slot1 { get; set; }
        public string PMSAppointmentStatus_Slot1 { get; set; }
        public string PMSAppointmentReason_Slot1 { get; set; }
        public string PMSAppointmentNote_Slot1 { get; set; }
        public string PMSAppointmentProvider_Slot1 { get; set; }
        public string PMSAppointmentID_Slot2 { get; set; }
        public int MyProperty { get; set; }
    }
}
