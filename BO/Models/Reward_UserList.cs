﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BO.Enums.Common;

namespace BO.Models
{
    public class Reward_UserList
    {
        public string DentistFirstName { get; set; }
        public string DentistLastName { get; set; }
        public string AccountName { get; set; }
        public string Location { get; set; }
        public int AccountId { get; set; }
        public PMSProviderType ProviderType { get; set; }
    }
}
