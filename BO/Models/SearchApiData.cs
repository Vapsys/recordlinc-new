﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
 public class SearchApiData
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Zipcode { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int? PageIndex { get; set; }
        public int? PageSize { get; set; }
        public int? Miles { get; set; }
        public int DentistId { get; set; }
        public int MethodName { get; set; }
        public int UserId { get; set; }
        public int AccountId { get; set; }
        public int? RLPatientId { get; set; }
    }
}
