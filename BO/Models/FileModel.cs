﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class FileModel
    {
        public int RecordId { get; set; }
        public string FolderId { get; set; }
        public int PatientId { get; set; }
        public DateTime CreatedDate { get; set; }
        public string FileId { get; set; }
        public string FileName { get; set; }
        public Stream FileStream { get; set; }
        public string Image { get; set; }
        public string FileExtension { get; set; }
    }

    public class FileListModel
    {
        public string FolderId { get; set; }
        public List<FileModel> Files { get; set; }
    }
}
