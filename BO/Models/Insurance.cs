﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class Insurance
    {
        public Insurance()
        {
            List<Insurance> lstInsurance = new List<Insurance>();
        }
        public int InsuranceId { get; set; }
        public int UserId { get; set; }
        public bool Ischeck { get; set; }
        public int Member_InsuranceId { get; set; }
        [Display(Name = "Insurance Name")]
        public string Name { get; set; }
        [Display(Name = "Logo")]
        public string LogoPath { get; set; }
        [Display(Name = "Description")]
        public string Description { get; set; }
        [Display(Name = "Link")]
        public string Link { get; set; }
        public List<Insurance> lstInsurance { get; set; }
        public int CurrentPageNo { get; set; }
        public int LocationId { get; set; }
    }

    public class InsuranceLocation
    {
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public List<InsuranceProvider> objInsuranceProvider { get; set; }
    }
    public class InsuranceProvider
    {
        public int InsuranceId { get; set; }
        public string InsuranceName { get; set; }
    }

}
