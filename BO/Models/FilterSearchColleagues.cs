﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// Filter Search Colleagues for OCR SearchColleague Page.
    /// </summary>
    public class FilterSearchColleagues
    {
        /// <summary>
        /// PageIndex by Default = 1
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// PageSize by default = 50
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// Search text 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Search with Email 
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Search with Phone
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// Search with City
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// Search with State
        /// </summary>
        public string State { get; set; }
        /// <summary>
        /// Search with Zip-code
        /// </summary>
        public string Zipcode { get; set; }
        /// <summary>
        /// Search with Institute
        /// </summary>
        public string Institute { get; set; }
        /// <summary>
        /// Search with SpecialityList
        /// </summary>
        public string SpecialityList { get; set; }
        /// <summary>
        /// Search with Keywords
        /// </summary>
        public string Keywords { get; set; }
        /// <summary>
        /// Dentist access token
        /// </summary>
        public string access_token { get; set; }
    }
}
