﻿using System;
using System.Configuration;

namespace BO.Models
{
    /// <summary>
    /// Onderful API credentails
    /// </summary>
    public class OnederfulCredentials
    {
        /// <summary>
        /// Key for access Onederful API
        /// </summary>
        public string client_id { get { return Convert.ToString(ConfigurationManager.AppSettings.Get("Onederful_Key")); } set { } }
        /// <summary>
        /// Securate Onderful API key.
        /// </summary>
        public string client_secret { get { return Convert.ToString(ConfigurationManager.AppSettings.Get("Onederful_Sec_Key")); } set { } }
    }
}
