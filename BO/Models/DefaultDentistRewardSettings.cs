﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class DefaultDentistRewardSettings
    {
        public int DefaultDentistRewardSettingsId { get; set; }
        public int DefaultDentistRewardOptionId { get; set; }
        public int DefaultDentistRewardMasterId { get; set; }
    }
}
