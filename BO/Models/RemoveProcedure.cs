﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class RemoveProcedure
    {
        [Required(ErrorMessage = "DentrixId is required.")]
        public int DentrixId { get; set; }
        [Required(ErrorMessage = "DentrixConnectorId is required.")]
        public string DentrixConnectorId { get; set; }
    }
}
