﻿using System.ComponentModel.DataAnnotations;

namespace BO.Models
{
    /// <summary>
    /// This class is used for Add Patient Reward.
    /// </summary>
    public class AddPatientReward
    {
        /// <summary>
        /// SuperDentist PatientId
        /// </summary>
        [Required(ErrorMessage = "SupPatientId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1.")]
        public int SupPatientId { get; set; }
        /// <summary>
        /// Reward Type 1 = Actual and 2 = Potential
        /// </summary>
        [Required(ErrorMessage = "RewardType is required")]
        [Range(1, 2, ErrorMessage = "RewardType must be 1 or 2. 1 = Actual and 2 = Potential!")]
        public int RewardType { get; set; }
        /// <summary>
        /// Reward Amount
        /// </summary>
        [Required(ErrorMessage = "Amount is required")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1.")]
        public decimal Amount { get; set; }
        /// <summary>
        /// Procedure code of Reward
        /// </summary>
        [Required(ErrorMessage = "ProcedureCodeId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1.")]
        public int ProcedureCodeId { get; set; }
    }
}
