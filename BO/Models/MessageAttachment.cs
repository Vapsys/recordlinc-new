﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class MessageAttachment
    {
        public int MessageId { get; set; }
        public int AttachmentId { get; set; }
        public string AttachmentName { get; set; }
        public string AttachementFullName { get; set; }
        public bool MessageStatus { get; set; }
        public int PatientId { get; set; }
        public int ReferralCardId { get; set; }
        public bool IsDocument { get; set; }
    }
}
