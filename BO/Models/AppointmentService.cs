﻿using System;

namespace BO.Models
{
    public class AppointmentService
    {
        public int AppointmentServiceId { get; set; }

        public int DoctorId { get; set; }

        public string ServiceType { get; set; }

        public string TimeDuration { get; set; }

        public string Price { get; set; }

        public DateTime? CreatedOn { get; set; }

        public DateTime? ModifiedOn { get; set; }
    }
}
