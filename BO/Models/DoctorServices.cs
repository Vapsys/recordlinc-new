﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// Doctor services
    /// </summary>
    public class DoctorServices
    {
        /// <summary>
        /// Appointment ServiceId
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// ServiceType
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Time Duration in Minutes
        /// </summary>
        public int DurationInMinutes { get; set; }
        /// <summary>
        /// Price
        /// </summary>
        public int? Price { get; set; }
    }

    //public class DoctorTimeSloat
    //{
    //    public int AppointmentWeekScheduleId { get; set; }
    //    public int DoctorId { get; set; }
    //    public int Day { get; set; }
    //    public char IsOffDay { get; set; }
    //    public DateTime? CreatedOn { get; set; }
    //    public DateTime? ModifiedOn { get; set; }
    //    public int AppointmentWorkingTimeAllocationId { get; set; }
    //    public int AppointmentSpecialDayScheduleId { get; set; }
    //    public string MorningFromTime { get; set; }
    //    public string MorningToTime { get; set; }
    //    public string EveningFromTime { get; set; }
    //    public string EveningToTime { get; set; }

    //}

}
