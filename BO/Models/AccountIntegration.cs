﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{

    public class AccountSetting
    {
        public bool SyncInfusionSoft { get; set; }
        public List<AccountIntegration> lst { get; set; }
        public AccountSetting()
        {
            lst = new List<AccountIntegration>();
        }
        public int RewardPartnerId { get; set; }
        public bool IsRewardPartner { get; set; }
        public bool IsSuperDentist { get; set; }
        public decimal Points { get; set; }
        public decimal Examcodes { get; set; }
        public bool IsZiftPayAccount { get; set; }
        public int ZiftPayAccountId { get; set; }
        public bool IsReceiveReferral { get; set; }
        public bool IsSendReferral { get; set; }
    }

    public class AccountIntegration
    {
        public string GUIDKey { get; set; }
        public string LastSyncDate { get; set; }
        public string View { get; set; }
        public string LocationId { get; set; }
        public string AccountId { get; set; }
        public int DentrixConnectorId { get; set; }
    }
}
