﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class InsuranceCoverage
    {
        public int ItHasInsuranceData { get; set; }
        public string ResponsibleFirstName { get; set; }
        public string ResponsibleLastName { get; set; }
        public string ResponsibleRelationship { get; set; }
        public string ResponsibleAddress { get; set; }
        public string ResponsibleCity { get; set; }
        public string ResponsibleState { get; set; }
        public string ResponsibleZipcode { get; set; }
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Phone number is invalid.")]
        public string ResponsiblePhoneNumber { get; set; }
        public string ResponsibleEmail { get; set; }
        public string PrimaryInsuranceCompany { get; set; }
        public string PrimaryInsurancePhone { get; set; }
        public string PrimaryNameOfInsured { get; set; }
        public string PrimaryDateOfBirth { get; set; }
        public string PrimaryMemberID { get; set; }
        public string PrimaryGroupNumber { get; set; }
        public string SecondaryInsuranceCompany { get; set; }
        public string SecondaryInsurancePhone { get; set; }
        public string SecondaryNameOfInsured { get; set; }
        public string SecondaryDateOfBirth { get; set; }
        public string SecondaryMemberID { get; set; }
        public string SecondaryGroupNumber { get; set; }
        public string MedicalInsuranceCompany { get; set; }
        public string MedicalInsurancePhone { get; set; }
        public string MedicalNameOfInsured { get; set; }
        public string MedicalDateOfBirth { get; set; }
        public string MedicalMemberID { get; set; }
        public string MedicalGroupNumber { get; set; }

        public string EMGContactName { get; set; }
        public string EMGContactNo { get; set; }

    }
}
