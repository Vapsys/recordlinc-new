﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class PatientDentalHistory
    {
        public string txtQue1 { get; set; }
        public string txtQue2 { get; set; }
        public string txtQue3 { get; set; }
        public string txtQue4 { get; set; }
        public string txtQue5 { get; set; }
        public string txtQue5a { get; set; }
        public string txtQue5b { get; set; }
        public string txtQue5c { get; set; }
        public string txtQue6 { get; set; }
        public string txtQue7 { get; set; }
        public string rdQue7a { get; set; }
        public bool rdQue8 { get; set; }
        public bool rdQue9 { get; set; }
        public string txtQue9a { get; set; }
        public bool rdQue10 { get; set; }

        public string rdoQue11aFixedbridge { get; set; }
        public string rdoQue11bRemoveablebridge { get; set; }
        public string rdoQue11cDenture { get; set; }
        public string rdQue11dImplant { get; set; }
        public string txtQue12 { get; set; }

        public string rdQue15 { get; set; }
        public string rdQue16 { get; set; }
        public string rdQue17 { get; set; }
        public string rdQue18 { get; set; }
        public string rdQue19 { get; set; }
        public string chkQue20 { get; set; }
        public string chkQue20_1 { get; set; }
        public string chkQue20_2 { get; set; }
        public string chkQue20_3 { get; set; }
        public string chkQue20_4 { get; set; }
        public string rdQue21 { get; set; }
        public string txtQue21a { get; set; }
        public string txtQue22 { get; set; }
        public string txtQue23 { get; set; }
        public string rdQue24 { get; set; }
        public string txtQue26 { get; set; }
        public string rdQue28 { get; set; }
        public string txtQue28a { get; set; }
        public string txtQue28b { get; set; }
        public string txtQue28c { get; set; }
        public string txtQue29 { get; set; }
        public string txtQue29a { get; set; }
        public bool rdQue30 { get; set; }
        public string rdQue27 { get; set; }
        public string txtComments { get; set; }

        public string txtDateoffirstvisit { get; set; }
        public string Reasonforfirstvisit { get; set; }
        public string Dateoflastvisit { get; set; }
        public string Reasonforlastvisit { get; set; }
        public string Dateofnextvisit { get; set; }
        public string Reasonfornextvisit { get; set; }
    }
}
