﻿using System.ComponentModel.DataAnnotations;

namespace BO.Models
{
    public class PatientSolOne
    {
        [Required(ErrorMessage = "The RewardsPlatformID is required.")]
        public int RewardsPlatformID { get; set; }
        public APIFilter.FilterFlag sourcetype { get; set; }
    }
}
