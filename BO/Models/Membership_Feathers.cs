﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Models;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace BO.Models
{
    public class Membership_Feathures
    {
        public int MembershipFeaturesId { get; set; }
        [Required(ErrorMessage = "Please select at least one value")]
        public int MembershipId { get; set; }
        public string MembershipName { get; set; }
        [Required(ErrorMessage = "Please select at least one value")]
        public string FeatureId { get; set; }
        public string FeatureName { get; set; }
        public bool cansee { get; set; }
        [Required(ErrorMessage = "Please select at least one value")]
        public string MaxCount { get; set; }
        public string CreatedOn { get; set; }
        public string ModifiedOn { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
    }
}
