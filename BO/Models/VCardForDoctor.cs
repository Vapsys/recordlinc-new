﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class VCardForDoctor
    {
        public int Userid { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Company { get; set; }
        public string JobTitle { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string FAX { get; set; }
        public string WebSite { get; set; }
        public string ImageName { get; set; }
        public string FacebookUrl { get; set; }
        public string GoogleplusUrl { get; set; }
        public string LinkedinUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string PinterestUrl { get; set; }
        public string YoutubeUrl { get; set; }
        public string BlogUrl { get; set; }
        public string YelpUrl { get; set; }
    }
}
