﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class Membership_History
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string OldValues { get; set; }
        public string NewValues { get; set; }
        public string MembershipName {get;set;}
        public int CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedUserName { get; set; }
    }
}
