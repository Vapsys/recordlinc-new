﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class MailContain
    {
        public int SendarId { get; set; }
        public string SendarName { get; set; }
        public string SendarEmail { get; set; }
        public string SendarPhone { get; set; }
        public int ReceiverId { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverEmail { get; set; }
        public string ReceiverPhone { get; set; }
        public int PatientId { get; set; }
        public string PatientName { get; set; }
        public string PatientEmail { get; set; }
        public string PatientPhone { get; set; }
        public string URL { get; set; }
        public string Body { get; set; }
        public string Status { get; set; }
        public string scheduleDate { get; set; }
        public string Address { get; set; }
        public string socialLink { get; set; }
        public string FacebookUrl { get; set; }
        public string LinkedinUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string GoogleplusUrl { get; set; }
        public string YoutubeUrl { get; set; }
        public string PinterestUrl { get; set; }
        public string BlogUrl { get; set; }
        public string YelpUrl { get; set; }
        public string InstagramUrl { get; set; }        
    }
}
