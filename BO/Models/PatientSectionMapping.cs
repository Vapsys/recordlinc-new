﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// Patient Referral Section Mapping 
    /// </summary>
    public class PatientSectionMapping
    {
        /// <summary>
        /// Section Mapping Id
        /// </summary>
        public int SectionMappingId { get; set; }
        /// <summary>
        /// Section Id
        /// </summary>
        public int SectionId { get; set; }
        /// <summary>
        /// Section Value
        /// </summary>
        public string SectionValue { get; set; }
        /// <summary>
        /// UserId
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Order by 
        /// </summary>
        public int OrderBy { get; set; }
        /// <summary>
        /// Check this Section is Enable or Disable
        /// </summary>
        public bool IsEnable { get; set; }
        /// <summary>
        /// Section Name
        /// </summary>
        public string SectionName { get; set; }
        /// <summary>
        /// List of field mapping with Patient data.
        /// </summary>
        public List<PatientFieldMapping> lstFieldMapping { get; set; }
    }
    /// <summary>
    /// Save Patient Referral form Setting
    /// </summary>
    public class SavePatientReferralSettings
    {
        /// <summary>
        /// Settings of Section related.
        /// </summary>
        public List<PatientSectionMapping> SectionSetting { get; set; }
        /// <summary>
        /// Settings of Fields
        /// </summary>
        public List<PatientFieldMapping> FieldSettings { get; set; }
        /// <summary>
        /// Settings of Sub Fields
        /// </summary>
        public List<PatientSubFieldMapping> SubFieldSettings { get; set; }
    }
}
