﻿using System;
using System.ComponentModel.DataAnnotations;
using BO.Enums;
namespace BO.Models
{
    public class PatientProcedure<T>
    {
        public PatientProcedure()
        {

            DateFilterBy = default(T);
        }
        //Ankit Here: 08-07-2018
        //SBI-35 related change request by Bruce
        /// <summary>
        /// ID of patient associated with Dentist (Represents ID of Recordlinc)
        /// </summary>
        [ValidateInputsName]
        public int PatientId { get; set; }

        /// <summary>
        ///To be used with FromDate searches within specified date.Accepts FromDate in UTC format. Do not use it while calling with RequestID.
        /// </summary>
        public DateTime? FromDate { get; set; }
        /// <summary>
        /// To be used with ToDate searches within specified date .Accepts ToDate in UTC format.  Do not use it while calling with RequestID.
        /// </summary>
        public DateTime? ToDate { get; set; }
        /// <summary>
        /// Sort order of results 1 for Ascending 2 for Descending (Currently it will sort on automodifiedtimestamp)
        /// </summary>
        public int OrderBy { get; set; }

        /// <summary>
        ///ID of user location associated with denitst. Do not use it while calling with RequestID.
        /// </summary>
        public int LocationId { get; set; }

        /// <summary>
        ///Id of specific request when it is called fisrt time.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Page number whose records are being obtained by API Call.
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// Count of records obtained in results.
        /// </summary>
        public int FetchRecordCount { get; set; }

        /// <summary>
        /// Provide option to filter FromDate and ToDate based on selected filter. Default value is 0.
        /// </summary>
        public T DateFilterBy { get; set; }
    }
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class ValidateInputsName : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            DateTime? From_ToDate = (DateTime?)validationContext.ObjectType.GetProperty("FromDate").GetValue(validationContext.ObjectInstance, null);
            DateTime? To_Date = (DateTime?)validationContext.ObjectType.GetProperty("ToDate").GetValue(validationContext.ObjectInstance, null);
            int PatientId = (int)validationContext.ObjectType.GetProperty("PatientId").GetValue(validationContext.ObjectInstance, null);

            //check at least one has a value
            //if (From_ToDate == null && To_Date == null && PatientId == 0)
            //    return new ValidationResult("At least one is required FromDate & ToDate or PatientId!!");
            if((From_ToDate == null && To_Date == null) || PatientId > 0)
                return ValidationResult.Success;
            else if(PatientId == 0 && From_ToDate != null && To_Date != null)
                return ValidationResult.Success;
            else
                return new ValidationResult("At least one is required FromDate & ToDate or PatientId!!");
        }
    }
}
