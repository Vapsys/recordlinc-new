﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace BO.Models
{
    public class ApiLogEntry
    {
        public long ApiLogEntryId { get; set; }             // The (database) ID for the API log entry.
        public string Application { get; set; }             // The application that made the request.
        public string User { get; set; }                    // The user that made the request.
        public string Machine { get; set; }                 // The machine that made the request.
        public string RequestIpAddress { get; set; }        // The IP address that made the request.
        public string RequestContentType { get; set; }      // The request content type.
        public string RequestContentBody { get; set; }      // The request content body.
        public string RequestUri { get; set; }              // The request URI.
        public string RequestMethod { get; set; }           // The request method (GET, POST, etc).
        public string RequestRouteTemplate { get; set; }    // The request route template.
        public string RequestRouteData { get; set; }        // The request route data.
        public string RequestHeaders { get; set; }          // The request headers.
        public DateTime? RequestTimestamp { get; set; }     // The request timestamp.
        public string ResponseContentType { get; set; }     // The response content type.
        public string ResponseContentBody { get; set; }     // The response content body.
        public int? ResponseStatusCode { get; set; }        // The response status code.
        public string ResponseHeaders { get; set; }         // The response headers.
        public DateTime? ResponseTimestamp { get; set; }    // The response timestamp.
        public int ManualTransactionId { get; set; }        // TransactionId if requested has generated any transaction
        public int APIVersion { get; set; }                 // API version
        public string TokenNumber { get; set; }             // The request token number included in header
        public int RequestID { get; set; }
        public string Status { get; set; }
        public int InOut { get; set; }
        public string AppVendor { get; set; }
        public string APICommandID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public DateTime? ClientTime { get; set; }
        public string DeviceName { get; set; }
        public string DeviceAddress { get; set; }
        public string Notes { get; set; }

        public ApiLogEntry()
        {
            var type = this.GetType();
            var properties = from p in this.GetType().GetProperties()
                             //where p.PropertyType == typeof(string) &&
                             //      p.CanRead &&
                             //      p.CanWrite
                             select p;

            foreach (var property in properties)
            {
                var propType = property.PropertyType;
                
                if (propType == typeof(string))
                    property.SetValue(this, string.Empty);
                else if (propType == typeof(Int32))
                    property.SetValue(this, 0);
                else if (propType == typeof(DateTime) || propType == typeof(DateTime?))
                    property.SetValue(this, Convert.ToDateTime("1900-01-01"));

            }
        }

    }
}