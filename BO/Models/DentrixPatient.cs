﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// Add/Edit Patient From Dentrix Database
    /// </summary>
    public class DentrixPatient
    {
        /// <summary>
        /// Recordlinc patientId
        /// </summary>
        public int RL_Id { get; set; }
        /// <summary>
        /// Dentrix PatientId
        /// </summary>
        [Required(ErrorMessage = "patid is required.")]
        public int patid { get; set; }
        /// <summary>
        /// Patient FirstName
        /// </summary>
        [Required(ErrorMessage = "FirstName is required.")]
        public string FirstName { get; set; }
        /// <summary>
        /// Patient LastName
        /// </summary>
        [Required(ErrorMessage = "LastName is required.")]
        public string LastName { get; set; }
        /// <summary>
        /// Patient Middle Name
        /// </summary>
        public string MiddleName { get; set; }
        /// <summary>
        /// Patient GUID
        /// </summary>
        public string patguid { get; set; }
        /// <summary>
        /// Patient Guaranteer Id
        /// </summary>
        public int guarid { get; set; }
        /// <summary>
        /// Is Patient is Guaranteer
        /// </summary>
        public bool isguar { get; set; }
        public int gender { get; set; }
        public bool isinssubscriber { get; set; }
        public int status { get; set; }
        public string statusstring { get; set; }
        public string primprovid { get; set; }
        public string secprovid { get; set; }
        public DateTime? FirstVisitDate { get; set; }
        public DateTime? LastVisitDate { get; set; }
        public DateTime? birthdate { get; set; }
        public DateTime? automodifiedtimestamp { get; set; }
        public string Password { get; set; }
        public int Userid { get; set; }
        public string dentrixConnectorID { get; set; }
        public List<Addresses> Addresses { get; set; }
        public List<PhoneNumbers> PhoneNumbers { get; set; }
        public List<string> Emails { get; set; }
        public int RowNumber { get; set; }
        public string BillingType { get; set; }
    }
    public class RemovePatient
    {
        [Required(ErrorMessage = "DentrixId is required.")]
        public int DentrixId { get; set; }
        [Required(ErrorMessage = "DentrixConnectorId is required.")]
        public string dentrixConnectorId { get; set; }
    }
}
