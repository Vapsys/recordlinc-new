﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class PlanAndPrice
    {
        public int MembershipId { get; set; }
        public int FeatureId { get; set; }
        public string FeatureName { get; set; }
        public bool CanSee { get; set; }
        public string MaxCount { get; set; }
    }
}
