﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class customerProfile
    {
        public string customerProfileId { get; set; }
        public string customerPaymentProfileId { get; set; }
        public string customerShippingAddressId { get; set; }
        public string customerType { get; set; }
    }
}
