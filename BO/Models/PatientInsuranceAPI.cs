﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class InsuranceCarrier
    {
        public int? Identifier { get; set; }
        public int? CarrierID { get; set; }
        public int? InsuranceType { get; set; }
        public string Name { get; set; }
        public string CarrierName { get; set; }
        
        public string GroupName { get; set; }
        public string GroupNumber { get; set; }
        public int? CoverageTableId { get; set; }
        public int? RenewalMonth { get; set; }
        public int? FeeScheduleId { get; set; }
        public string PayorId { get; set; }
        public MaxBenefits MaxBenfits { get; set; }
        public decimal Person { get; set; }
        public decimal Family { get; set; }
        public DeductibleInfo DeductibleInfo { get; set; }
        public List<Address> Addresses { get; set; }
        public List<Phone> PhoneNumbers { get; set; }
        public List<string> Emails { get; set; }
        public int? DentrixId { get; set; }
        public int? PracticeID { get; set; }
        public string ConnectorID { get; set; }
        public string dentrixConnectorID { get; set; }
        public int? PMSConnectorId { get; set; }
        public DateTime? automodifiedtimestamp { get; set; }
        public DateTime DateCreated { get; set; }
    }

    public class MaxBenefits
    {
        public decimal? Person { get; set; }
        public decimal? Family { get; set; }
        public decimal? Lifetime { get; set; }
    }

    public class DeductibleInfo
    {
        public Deductible Person { get; set; }
        public Deductible Family { get; set; }
        public Deductible Lifetime { get; set; }
    }

    public class Deductible
    {
        public decimal? Standard { get; set; }
        public decimal? Preventive { get; set; }
        public decimal? Other { get; set; }
    }

    public class Address
    {
        public int? DentrixId { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
    }

    public class Phone
    {
        public int PhoneType { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class InsuredRecord
    {
        public DateTime? automodifiedtimestamp { get; set; }
        public int? Identifier { get; set; }
        public int? InsuredId { get; set; }
        public int? CarrierId { get; set; }
        public int? InsuredPartyId { get; set; }
        public string SubscriberID { get; set; }
        public string GroupName { get; set; }
        public string GroupNumber { get; set; }
        public decimal? BenefitsUsed { get; set; }
        public int? DentrixId { get; set; }
        public int? PracticeID { get; set; }
        public string ConnectorID { get; set; }
        public string dentrixConnectorID { get; set; }
        public int? PMSConnectorId { get; set; }
        public DeductibleInfo DeductibleUsed { get; set; }
        public InsuredMember SubscriberDetail { get; set; }
        public List<InsuredMember> InsuredMembers { get; set; }
    }

    public class InsuredMember
    {
        public DateTime? automodifiedtimestamp { get; set; }
        public int? PatientId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public int? Gender { get; set; }
        public DateTime? DOB { get; set; }
        public DateTime? BirthDate { get; set; }
        public string SSN { get; set; }
        public decimal? BenefitsUsed { get; set; }
        public int? InsuranceType { get; set; }
        public int? InsurancePosition { get; set; }
        public int? RelationToInsured { get; set; }
        public int? PracticeID { get; set; }
        public int? PMSConnectorId { get; set; }
        public DeductibleInfo DeductibleInfo { get; set; }
    }
}
