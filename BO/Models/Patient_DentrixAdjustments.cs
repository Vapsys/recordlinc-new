﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BO.Models
{
    public class Patient_DentrixAdjustments
    {
        [Required(ErrorMessage = "Adjustid is required.")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1")]
        public int adjustid { get; set; }
        public int patid { get; set; }
        public int guarid { get; set; }
        public DateTime? procdate { get; set; }
        public DateTime? createdate { get; set; }
        public string provid { get; set; }
        public int claimid { get; set; }
        public int adjusttype { get; set; }
        public string adjusttypestring { get; set; }
        public int adjustdefid { get; set; }
        public string adjustdescript { get; set; }
        public decimal amount { get; set; }
        public bool history { get; set; }
        public bool appliedtopayagreement { get; set; }
        public DateTime? automodifiedtimestamp { get; set; }
        [Required(ErrorMessage = "DentrixConnectorId is required. ")]
        public string dentrixConnectorID { get; set; }
    }
    public class AdjutmentDetails
    {
        /// <summary>
        /// Recordlinc Primary key
        /// </summary>
        public int RL_ID { get; set; }
        /// <summary>
        /// Dentrix Primary Key
        /// </summary>
        public int adjustid { get; set; }
        /// <summary>
        /// PatientId of Recordlinc
        /// </summary>
        public int patid { get; set; }

        /// <summary>
        /// PatientId of Dentrix
        /// </summary>
        public int DentrixPatientId { get; set; }
        /// <summary>
        /// Guaranteer Id of Recordlinc
        /// </summary>
        public int guarid { get; set; }
        /// <summary>
        ///  Guaranteer Id of Dentrix
        /// </summary>
        public int DentrixGuarId { get; set; }
        /// <summary>
        /// Procedure date
        /// </summary>
        public DateTime? procdate { get; set; }
        /// <summary>
        /// Dentrix Provider Id
        /// </summary>
        public string Dentrixprovid { get; set; }
        /// <summary>
        /// Recordlinc Provider Id
        /// </summary>
        public string RLprovid { get; set; }
        public int claimid { get; set; }
        public int adjusttype { get; set; }
        public string adjusttypestring { get; set; }
        public int adjustdefid { get; set; }
        public string adjustdescript { get; set; }
        /// <summary>
        /// Amount
        /// </summary>
        public double amount { get; set; }
        public bool history { get; set; }
        public bool appliedtopayagreement { get; set; }
        public DateTime? automodifiedtimestamp { get; set; }

        /// <summary>
        /// Entry date / Creation date of Dentrix
        /// </summary>
        public DateTime? createdate { get; set; }
    }
}
