﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// Enable Single Has Button status for OCR Colleague Page.
    /// </summary>
    public class EnableHasButton
    {
        /// <summary>
        /// Receiver DoctorId
        /// </summary>
        public int ReceiverId { get; set; }
        /// <summary>
        /// Sender Doctor access_token.
        /// </summary>
        public string access_token { get; set; }
    }

    /// <summary>
    /// Enable/Disable Multiple Has button functionality.
    /// </summary>
    public class UpdateHasButton
    {
        /// <summary>
        /// Multiple Colleagues Id
        /// </summary>
        public int[] ColleaguesId { get; set; }
        /// <summary>
        /// IsEnable/Disable functionality.
        /// </summary>
        public bool IsEnable { get; set; }
        /// <summary>
        /// User access_token
        /// </summary>
        public string access_token { get; set; }
    }
}
