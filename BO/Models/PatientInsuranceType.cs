﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class PatientInsuranceType
    {
        public int InsuranceTypeId { get; set; }
        public string Description { get; set; }
    }
}
