﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class ReferralDetailsOneClick
    {
        public int MessageId { get; set; }
        public int SenderId { get; set; }
        public int PatientId { get; set; }
        public int ReceiverId { get; set; }
        public string SenderEmail { get; set; }
        public string PatientEmail { get; set; }
        public string ReceiverEmail { get; set; }
        public string SenderPhone { get; set; }
        public string SenderMobile { get; set; }
        public string PatientPhone { get; set; }
        public string ReceiverPhone { get; set; }
        public string ReceiverMobile { get; set; }
        public string SenderName { get; set; }
        public string PatientName { get; set; }
        public string ReceiverName { get; set; }
        public string Specility { get; set; }
        public string SenderSpecility { get; set; }
    }
}
