﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class RetrievePatient
    {
        public string PMSConnectorId { get; set; }
        public int FetchRecordCount { get; set; }
        public string RequestId { get; set; }
        public int PageIndex { get; set; }
        public bool SendAll { get; set; }
    }
    public class RetrieveAppointment
    {
        public string PMSConnectorId { get; set; }
        public int FetchRecordCount { get; set; }
        public string RequestId { get; set; }
    }
}
