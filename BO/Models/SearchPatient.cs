﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class SearchPatient
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public int UserId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Postalcode { get; set; }
        public bool ExcludeFamilyMembers { get; set; }
    }
}
