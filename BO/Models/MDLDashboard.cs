﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.ViewModel;

namespace BO.Models
{
   public class MDLDashboard
    {
     public List<InboxMessages> InboxList { get; set; }
     public List<ColleagueDetails> lstColleaguesDetails { get; set; }
     public List<NewPatientDetailsOfDoctor> lstPatients { get; set; }
     public int TotalInboxRecord { get; set; }
     public int TotalColleague { get; set; }
     public int TotalPatients { get; set; }


    }
}
