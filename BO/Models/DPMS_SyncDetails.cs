﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class DPMS_SyncDetails
    {
        public int RequestId { get; set; }
        public DateTime RequestTime { get; set; }
        public string DentrixConnectorId { get; set; }
        public string RequestType { get; set; }
        public string GUID { get; set; }
        public bool IsFromSBI { get; set; }
    }
}
