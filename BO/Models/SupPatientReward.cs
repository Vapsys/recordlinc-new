﻿using System.ComponentModel.DataAnnotations;

namespace BO.Models
{
    /// <summary>
    /// This class is used for Add/Edit Patient Reward of SuperDentist.
    /// </summary>
    public class EditSupPatientReward
    {
        /// <summary>
        /// This is Primary Required field for the Edit Reward of SuperDentist.
        /// </summary>
        [Required(ErrorMessage ="RewardId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1.")]
        public int RewardId { get; set; }
        /// <summary>
        /// Procedure code is Associate with Reward
        /// </summary>
        [Required(ErrorMessage = "ProcedureCodeId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1.")]
        public int ProcedureCodeId { get; set; }
        /// <summary>
        /// Reward Amount
        /// </summary>
        [Required(ErrorMessage = "Amount is required")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1.")]
        public decimal Amount { get; set; }
        /// <summary>
        /// Reward Type 1 = Actual 2 = Potential
        /// </summary>
        [Required(ErrorMessage = "RewardType is required")]
        [Range(1, 2, ErrorMessage = "RewardType must be 1 or 2. 1 = Actual and 2 = Potential!")]
        public int RewardType { get; set; }
    }
}
