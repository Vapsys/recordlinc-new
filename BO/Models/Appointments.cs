﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// Please do not change anything in this model
    /// </summary>
    public class Appointments
    {
        public DateTime AppointmentDate { get; set; }
        public DateTime? BrokenDate { get; set; }
        public string ProviderId { get; set; }
        public int AppointmentLength { get; set; }
        public string AppointmentReason { get; set; }
        public List<int> TxProcs { get; set; }
        public int PatientId { get; set; }
        public bool AmountOverriden { get; set; }
        public decimal Amount { get; set; }
        public string OperatoryId { get; set; }
        public string StaffId { get; set; }
        [Required(ErrorMessage = "dentrixConnectorID is required.")]
        public string dentrixConnectorID { get; set; }
        public DateTime? automodifiedtimestamp { get; set; }
        public int AppointmentId { get; set; }
        public List<string> AdaCodes { get; set; }
        public int DentrixId { get; set; }
        public DateTime AppointmentStart { get; set; }
        public NewPatientInfo NewPatientInfo { get; set; }
        public int? StatusId { get; set; }
    }

    public class AppointmentsWriteback
    {
        public int AppointmentId { get; set; }
        public int RLAppointmentId { get; set; }
        public int RLPendingAppointmentId { get; set; }
        public DateTime AppointmentDate { get; set; }
        public DateTime? BrokenDate { get; set; }
        public string ProviderId { get; set; }
        public int AppointmentLength { get; set; }
        public string AppointmentReason { get; set; }
        public List<int> TxProcs { get; set; }
        public int PatientId { get; set; }
        public bool AmountOverriden { get; set; }
        public decimal Amount { get; set; }
        public string OperatoryId { get; set; }
        public string StaffId { get; set; }
        [Required(ErrorMessage = "dentrixConnectorID is required.")]
        public string dentrixConnectorID { get; set; }
        public DateTime? automodifiedtimestamp { get; set; }

        public List<string> AdaCodes { get; set; }
        public int DentrixId { get; set; }
        public int? StatusId { get; set; }
        public DateTime AppointmentStart { get; set; }
        public NewPatientInfo NewPatientInfo { get; set; }
    }

    public class RemoveAppointment
    {
        public int DentrixId { get; set; }
        [Required(ErrorMessage = "DentrixConnectorId is required.")]
        public string DentrixConnectorId { get; set; }
    }

    public class NewPatientInfo
    {
        public string PatientName { get; set; }
        public List<Addresses> Addresses { get; set; }
        public List<PhoneNumbers> phonenumbers { get; set; }
        public List<string> EmailAdresses { get; set; }
    }

    /// <summary>
    /// Appointment Write Back
    /// </summary>
    public class WritebackAppointment
    {
        /// <summary>
        /// PMS connector id
        /// </summary>
        [Required(ErrorMessage = "PMSConnectorId is required.")]
        public string PMSConnectorId { get; set; }

        /// <summary>
        /// Fetch record count
        /// </summary>
        [Required(ErrorMessage = "FetchRecordCount is required.")]
        [Range(1, BO.Constatnt.Common.MaxPageSize, ErrorMessage = "Please enter a value between 1 to 100")]
        public int FetchRecordCount { get; set; }

        /// <summary>
        /// Page index size
        /// </summary>
        [Required(ErrorMessage = "PageIndex is required.")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1")]
        public int PageIndex { get; set; }

        /// <summary>
        /// Appointment start date
        /// </summary>
        public DateTime? Startdate { get; set; } // AppointmentDate

        /// <summary>
        /// Appointment end date
        /// </summary>
        public DateTime? Enddate { get; set; } // AppointmentDate
    }
}
