﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class Compose1ClickMedicalHistory
    {
        public bool MrdQue1 { get; set; }
        public string Mtxtphysicians { get; set; }
        public bool MrdQue2 { get; set; }
        public string Mtxthospitalized { get; set; }
        public bool MrdQue3 { get; set; }
        public string Mtxtserious { get; set; }
        public bool MrdQue4 { get; set; }
        public string Mtxtmedications { get; set; }
        public bool MrdQue5 { get; set; }
        public string MtxtRedux { get; set; }
        public bool MrdQue6 { get; set; }
        public string MtxtFosamax { get; set; }
        public bool MrdQuediet7 { get; set; }
        public string Mtxt7 { get; set; }
        public bool Mrdotobacco8 { get; set; }
        public string Mtxt8 { get; set; }
        public bool Mrdosubstances { get; set; }
        public string Mtxt9 { get; set; }
        public bool Mrdopregnant { get; set; }
        public string Mtxt10 { get; set; }
        public bool Mrdocontraceptives { get; set; }
        public string Mtxt11 { get; set; }
        public bool MrdoNursing { get; set; }
        public string Mtxt12 { get; set; }
        public bool Mrdotonsils { get; set; }
        public string Mtxt13 { get; set; }
        public bool Mchk14 { get; set; }
        public bool MchkQue_1 { get; set; }
        public bool MchkQue_2 { get; set; }
        public bool MchkQue_3 { get; set; }
        public bool MchkQue_4 { get; set; }
        public bool MchkQue_5 { get; set; }
        public bool MchkQue_6 { get; set; }
        public bool MchkQue_7 { get; set; }
        public bool MchkQue_8 { get; set; }
        public bool Mchk15 { get; set; }
        public bool MrdQueAbnormal { get; set; }
        public bool MrdQueAIDS_HIV_Positive { get; set; }
        public bool MrdQueAlzheimer { get; set; }
        public bool MrdQueAnemia { get; set; }
        public bool MrdQueAngina { get; set; }
        public bool MrdQueThinners { get; set; }
        public bool MrdQueArthritis { get; set; }
        public bool MrdQueArtificialHeartValve { get; set; }
        public bool MrdQueArtificialJoint { get; set; }
        public bool MrdQueAsthma { get; set; }
        public bool MrdQueFibrillation { get; set; }
        public bool MrdQueBloodDisease { get; set; }
        public bool MrdQueBloodTransfusion { get; set; }
        public bool MrdQueBreathing { get; set; }

        public bool MrdQueBruise { get; set; }
        public bool MrdQueCancer { get; set; }
        public bool MrdQueChemotherapy { get; set; }
        public bool MrdQueChest { get; set; }
        public bool MrdQueCigarette { get; set; }
        public bool MrdQueCirulation { get; set; }
        public bool MrdQueCortisone { get; set; }
        public bool MrdQueFrequentCough { get; set; }
        public bool MrdQueConvulsions { get; set; }
        public bool MrdQuefillers { get; set; }
        public bool MrdQueDiabetes { get; set; }
        public bool MrdQueDiarrhhea { get; set; }
        public bool MrdQueDigestive { get; set; }
        public bool MrdQueDizziness { get; set; }
        public bool MrdQueAlcoholic { get; set; }
        public bool MrdQueSubstances { get; set; }

        //  public bool MrdQueDrug { get; set; }
        //  public bool MrdQueEasily { get; set; }

        public bool MrdQueEmphysema { get; set; }
        public bool MrdQueEpilepsy { get; set; }
        public bool MrdQueExcessiveurination { get; set; }
        public bool MrdQueExcessiveThirst { get; set; }
        public bool MrdQueGlaucoma { get; set; }
        public bool MrdQueHeadaches { get; set; }
        public bool MrdQueAttack { get; set; }
        public bool MrdQueHeartPacemaker { get; set; }
        public bool MrdQueHemophilia { get; set; }
        public bool MrdQueHeart_defect { get; set; }
        public bool MrdQueHeart_murmur { get; set; }
        public bool MrdQueHepatitisA { get; set; }
        public bool MrdQueHerpes { get; set; }
        public bool MrdQueHiatal { get; set; }
        public bool MrdQueBlood_pressure { get; set; }
        public bool MrdQueAIDS { get; set; }
        public bool MrdQueHow_much { get; set; }
        public bool MrdQueHow_often { get; set; }
        public bool MrdQueHypoglycemia { get; set; }
        public bool MrdQueReplacement { get; set; }
        public bool MrdQueKidney { get; set; }
        public bool MrdQueLeukemia { get; set; }
        public bool MrdQueLiver { get; set; }
        public bool MrdQueLung { get; set; }

        public bool MrdQueMitral { get; set; }
        public bool MrdQueNeck_BackProblem { get; set; }
        public bool MrdQueOsteoporosis { get; set; }
        public bool MrdQuePacemaker { get; set; }
        public bool MrdQuePainJaw_Joints { get; set; }
        public bool MrdQueEndocarditis { get; set; }
        public bool MrdQuePsychiatric { get; set; }
        public bool MrdQueRadiation { get; set; }
        public bool MrdQueRheumatic { get; set; }
        public bool MrdQueScarlet { get; set; }
        public bool MrdQueShingles { get; set; }
        public bool MrdQueShortness { get; set; }
        public bool MrdQueSinus { get; set; }
        public bool MrdQueStroke { get; set; }
        public bool MrdQueSjorgren { get; set; }
        public bool MrdQueStomach { get; set; }
        public bool MrdQueSwelling { get; set; }
        public bool MrdQueSwollen { get; set; }
        public bool MrdQueThyroid { get; set; }
        public bool MrdQueTuberculosis { get; set; }
        public bool MrdQueTumors { get; set; }
        public bool MrdQueValve { get; set; }
        public bool MrdQueVision { get; set; }
        public bool MrdQueUnexplained { get; set; }
        public bool Mrdillness { get; set; }
        public string Mtxtillness { get; set; }
        public bool MrdComments { get; set; }
        public string MtxtComments { get; set; }
    }
}
