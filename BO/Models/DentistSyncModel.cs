﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class DentistSyncModel
    {
 
        public int Id { get; set; }
        public int DentrixConnectorId { get; set; }
        public int AccountId { get; set; }
        public string AccountKey { get; set; }
        public string AccountName { get; set; }
        public int LocationId { get; set; }
        public int UserId { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Location { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public DateTime? SyncDate { get; set; }
        public DateTime ProcessDate { get; set; }
    }
}
