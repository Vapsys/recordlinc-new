﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class Reward_DentistList
    {
        [Required(ErrorMessage = "RewardPartnerId is required.")]
        public dynamic RewardPartnerId { get; set; }
    }
}
