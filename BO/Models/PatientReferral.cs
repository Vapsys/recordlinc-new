﻿using Foolproof;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class PatientReferral
    {
        [Required(ErrorMessage = "RewardPartnerId is required.")]
        public int RewardPartnerId { get; set; }
        [Required(ErrorMessage = "FirstName is required.")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "LastName is required.")]
        public string LastName { get; set; }
        //Added Custom validation ref over here. PRCSP-376.
        [ValidatePersonName]
        public string Email { get; set; }
        public string phone { get; set; }
        [Required(ErrorMessage = "DentistId is required.")]
        public int DentistId { get; set; }
        [Required(ErrorMessage = "AccountId is required.")]
        public int AccountId { get; set; }
        public int Source { get; set; }
        public APIFilter.FilterFlag sourcetype { get; set; }
    }
    /// <summary>
    /// Ankit Here 06-05-2018 PRCSP-376 Added this custom validation method for check if both field are null then required one of them Phone ore Email.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
    public class ValidatePersonName : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string FirstName = (string)validationContext.ObjectType.GetProperty("phone").GetValue(validationContext.ObjectInstance, null);

            string LastName = (string)validationContext.ObjectType.GetProperty("Email").GetValue(validationContext.ObjectInstance, null);

            //check at least one has a value
            if (string.IsNullOrEmpty(FirstName) && string.IsNullOrEmpty(LastName))
                return new ValidationResult("At least one is required Phone or Email!!");

            return ValidationResult.Success;
        }
    }
}
