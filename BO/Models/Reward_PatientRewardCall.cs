﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class Reward_PatientRewardCall
    {
        [Required(ErrorMessage = "PatientId is required.")]
        public int SupPatientId { get; set; }
        public int Pageindex { get; set; }
        public int PageSize { get; set; }
        public DateTime? Startdate { get; set; }
        public DateTime? Enddate { get; set; }
        public int RewardType { get; set; }
        [Required(ErrorMessage = "AccountId is required.")]
        public int AccountId { get; set; }
    }
}
