﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// Delete Multiple Patient
    /// </summary>
    public class DeleteMultiplePatient
    {
        /// <summary>
        /// Multiple Patient Ids with Comma Separated.
        /// </summary>
        public string[] MultiplePatientId { get; set; }
    }
}
