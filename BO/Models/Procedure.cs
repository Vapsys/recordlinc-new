﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
   public class Procedure
    {
        public Procedure()
        {
            List<Procedure> lstProcedure = new List<Procedure>();
        }
        public int ProcedureId { get; set; }
        public string ProcedureName { get; set; }
        public decimal CostPercentage{ get; set; }
        public decimal StandardFees { get; set; }
        public bool Ischeck { get; set; }
    }
}
