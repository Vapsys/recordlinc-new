﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class RewardsOptions
    {
        public int RewardOptionId { get; set; }
        public string RewardText { get; set; }
        public string TreatmentCode { get; set; }
    }
}
