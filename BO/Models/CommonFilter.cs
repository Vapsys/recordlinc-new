﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class CommonFilter
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int SortDirection { get; set; }
        public int SortColumn { get; set; }
    }
}
