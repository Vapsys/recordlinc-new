﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// Invite Colleague used for OCR side Invite Colleagues.
    /// </summary>
    public class InviteColleague
    {
        /// <summary>
        /// Comma Separated Email address.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// File Path of Invite Colleague.
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// Message for Colleague
        /// </summary>
        public string Message { get; set; }
    }
    /// <summary>
    /// ResponseMeassge Invite Colleagues
    /// </summary>
    public class ResponseMeassge
    {
        public string YourSelfInvite { get; set; }
        public string FailInvite { get; set; }
        public string XlsError { get; set; }
        public List<ResponseEmail> objListResponseEmail { get; set; }
        public string EmailRequired { get; set; }
    }
    public class ResponseEmail
    {
        public string EmailId { get; set; }
        public int Id { get; set; }
    }
}
