﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class Features
    {
        public int Feature_Id { get; set; }

        [Display(Name = "Title")]
        [Required(ErrorMessage = "Title is required.")]
        public string Title { get; set; }

        [Display(Name = "Sort order")]
        [Required(ErrorMessage = "Sort order is required")]
        [Range(1, int.MaxValue, ErrorMessage = "Sort order must be greater than {1}.")]
        public int SortOrder { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        [Display(Name = "Status")]
        [Required(ErrorMessage = "Status is required")]
        public int Status { get; set; }
        public bool CanSee { get; set; }
        public string MaxCount { get; set; }
        public List<object> lstTypeOfStatus { get; set; }
    }
}
