﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BO.Enums.Common;
namespace BO.Models
{
    public class Searchlatitudelongitude
    {
        public Searchlatitudelongitude()
        {
            ProviderfilterType = PMSProviderFilterType.All;
        }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string SerachText { get; set; }

        public PMSProviderFilterType ProviderfilterType { get; set; }
    }
}
