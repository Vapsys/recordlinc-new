﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class APIFilter
    {
        public enum FilterFlag
        {
            IsRewardPlatform = 1,
            IsSuperBucks = 2,
            NotFound = 0
        }       
    }
}
