﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
  public class RemoveOperatory
    {
        [Required(ErrorMessage = "DentrixId is required.")]
        public int DentrixId { get; set; }
        [Required(ErrorMessage = "DentrixConnecoterKey is required.")]
        public string DentrixConnecoterKey { get; set; }
    }
}
