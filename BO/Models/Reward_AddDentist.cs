﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class Reward_AddDentist
    {
        [Required(ErrorMessage = "OldSolutionOneId is required.")]
        public int OldSolutionOneId { get; set; }
        [Required(ErrorMessage = "OldAccountId is required.")]
        public int OldAccountId { get; set; }
        [Required(ErrorMessage = "NewSolutionOneId is required.")]
        public int NewSolutionOneId { get; set; }
        [Required(ErrorMessage = "NewAccountId is required.")]
        public int NewAccountId { get; set; }
        [Required(ErrorMessage = "OldRLPatientId is required.")]
        public int OldRLPatientId { get; set; }
        [Required(ErrorMessage = "NewRLPatientId is required.")]
        public int NewRLPatientId { get; set; }
        public APIFilter.FilterFlag sourcetype { get; set; }
    }
}
