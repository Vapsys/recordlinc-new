﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class EmailProviderDetails
    {
        public int providerDetailsId { get; set; }

        [Required(ErrorMessage = "Email Provider is Required.")]
        public int providerId { get; set; }
        public List<EmailProvider> lstProvider { get; set; }

        [Required(ErrorMessage = "Please enter email address.")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string emailAddress { get; set; }

        [Required(ErrorMessage = "Please enter password.")]
        [DataType(DataType.Password)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        public string password { get; set; }
        public string accessToken { get; set; }
        public string refreshToken { get; set; }
        public int memberId { get; set; }

        [Required(ErrorMessage = "Please enter Smtp Server Name.")]
        public string smtpServerName { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "Please enter Smtp Port Number.")]       
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Port must be numeric")]
        public int? smtpPortNumber { get; set; }

        [Required(ErrorMessage = "Please check the box.")]
        public bool requireSSL { get; set; }        
        public bool isDefault { get; set; }
        public bool syncEnabled { get; set; }

        [Required(ErrorMessage = "Please check the box.")]        
        public bool sendEnabled { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt { get; set; }
        public bool Is_Active { get; set; }
    }
}
