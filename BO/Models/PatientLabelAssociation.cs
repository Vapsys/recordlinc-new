﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BO.Models
{
    /// <summary>
    /// This model used for Add/Edit label association with Patient.
    /// </summary>
    public class PatientLabelAssociation
    {
        /// <summary>
        /// PatientId
        /// </summary>
        [EnsureOneElement(ErrorMessage = "At least one PatientId is required")]
        public List<int> PatientId { get; set; }
        /// <summary>
        /// LabelId of Perticular dentist.
        /// </summary>
        [Required(ErrorMessage = "LabelId is required. ")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1")]
        public int LabelId { get; set; }
    }
    /// <summary>
    /// This is custome validation attribute check list of property has at least 1 value.
    /// </summary>
    public class EnsureOneElementAttribute : ValidationAttribute
    {
        /// <summary>
        /// Check object is valid or not
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public override bool IsValid(object value)
        {
            var list = value as IList;
            if (list != null)
            {
                return list.Count > 0;
            }
            return false;
        }
    }
}
