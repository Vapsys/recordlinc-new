﻿using BO.ViewModel;
using System;
using System.ComponentModel.DataAnnotations;

namespace BO.Models
{
    public class Reward_PatientUpdate
    {
        public string Email { get; set; }

        public string WorkPhone { get; set; }
        public string HomePhone { get; set; }
        public string MobilePhone { get; set; }
        public string Password { get; set; }

        [Required(ErrorMessage = "Override is required.")]
        public bool Override { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
        /// <summary>
        /// 0 = Male and 1 = Female
        /// </summary>
        public string Gender { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        public string SecondaryPhoneNumber { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmailAddress { get; set; }
        public int PlanId { get; set; }
        public string EmergencyContactPhoneNumber { get; set; }
        [Required(ErrorMessage = "RewardPartnerId is required.")]
        public int RewardPartnerId { get; set; }
        public string ImageString { get; set; }
        public string ImageNameWithExtension { get; set; }
        public string path { get; set; }
        public bool IsSendPushNotification { get; set; }
        public DentalHistorys DentalHistory { get; set; }
        public MedicalHistory MedicalHistory { get; set; }
        public ViewModel.InsuranceCoverageModel InsuranceCoverage { get; set; }
        public PDIC PrimaryDentalInsurance { get; set; }
        public SDIC SecondaryDentalInsurance { get; set; }
        public APIFilter.FilterFlag sourcetype { get; set; }
    }
}
