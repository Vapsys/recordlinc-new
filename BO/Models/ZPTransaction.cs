﻿using BO.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class ZPTransactionRequest
    {
        public ZPTransactionRequest()
        {
            lstCard = new List<CheckList>();
        }
        //Authentication Details
        public string requestType { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public string accountId { get; set; }
        public bool isProfileSave { get; set; }
        public List<CheckList> lstCard { get; set; }
        public int SelectedCard { get; set; }

        //Card or ACH Details
        public string accountType { get; set; }
       // [Required(ErrorMessage = "Please enter card number")]
        public string accountNumber { get; set; }
        public string accountAccessoryMonth { get; set; }
        public string accountAccessoryYear { get; set; }
       // [Required(ErrorMessage = "Please enter expriation date")]
        //[RegularExpression(@"^(1[0-2]|0[1-9]|\d)\/?([2-9]\d[1-9]\d|[1-9]\d{4})$", ErrorMessage ="Enter valid expiration date")]
        public string expdate { get; set; }

        //[Required(ErrorMessage = "Month is required.")]
        //public string ExpirationMonth { get; set; }
        //[Required(ErrorMessage = "Year is required.")]
        //public string ExpirationYear { get; set; }

        [Required(ErrorMessage = "Please enter CVV no")]
        public string Cvv { get; set; }
        public string token { get; set; }
        public string holderType { get; set; }
        [Required(ErrorMessage = "Please enter holder name")]
        public string holderName { get; set; }
        public string holderBirthdate { get; set; }

        //Transaction Details
        [Required(ErrorMessage = "Please enter amount")]
        public string amount { get; set; }
        public string transactionIndustryType { get; set; }
        public Common.transactionCategoryType transactionCategoryType { get; set; }
        public Common.transactionModeType transactionModeType { get; set; }
        public string memo { get; set; }
        public bool isPartialAuthorization { get; set; }

        //Card Billing Details
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipCode { get; set; }
        public string countryCode { get; set; }
        public string email { get; set; }
        public string customerAccountCode { get; set; }
        public string transactionCode { get; set; }
        public string PatientId { get; set; }
        public int DoctorId { get; set; }
    }

    public class ZPTransactionResponse
    {
        public string responseType { get; set; }
        public string accountId { get; set; }
        public string accountType { get; set; }
        public string extendedAccountType { get; set; }
        public string accountNumberMasked { get; set; }
        public string accountAccessory { get; set; }
        public string holderName { get; set; }
        public string balance { get; set; }
        public string currencyCode { get; set; }
        public decimal originalAmount { get; set; }
        public string feeAmount { get; set; }
        public string transactionDate { get; set; }
        public string transactionId { get; set; }
        public string requestId { get; set; }
        public string approvalCode { get; set; }
        public string cycleCode { get; set; }
        public string warningCode { get; set; }
        public string responseCode { get; set; }
        public string responseMessage { get; set; }
        public string splits { get; set; }
        public string items { get; set; }
        public string requestParam { get; set; }
    }

    public class splits
    {
        public string accountId { get; set; }
        public string transactionId { get; set; }
    }

    public class SplitItem
    {
        public string code { get; set; }
        public string itemId { get; set; }
        public List<splits> splits { get; set; }
    }

    public class ErrorResponse
    {
        public string responseType { get; set; }
        public string failureCode { get; set; }
        public string failureMessage { get; set; }
        public string failedRequestType { get; set; }

    }

    public class PatientPaymentRequest
    {
        public int Id { get; set; }
        public int Patientid { get; set; }
        public DateTime PaymentDate { get; set; }
        public decimal Amount { get; set; }
        public bool Status { get; set; }
        public string CardNumber { get; set; }
        public string Cvv { get; set; }
        public int GatewayId { get; set; }
        public int? PPTId { get; set; }
    }

    public class PatientSplitPayment
    {
        public string Id { get; set; }
        public int PPMId { get; set; }
        public int Split_AccountId { get; set; }
        public string Split_TransactionId { get; set; }
        public decimal Split_Amount { get; set; }
    }

    public class ZPTokenResponse
    {
        public string responseType { get; set; }
        public string holderName { get; set; }
        public string extendedAccountType { get; set; }
        public string providerResponseMessage { get; set; }
        public string providerTransactionId { get; set; }
        public string accountType { get; set; }
        public string transactionCode { get; set; }
        public string transactionId { get; set; }
        public string responseCode { get; set; }
        public string token { get; set; }
        public string accountId { get; set; }
        public string providerReferenceNumber { get; set; }
        public string providerResponseCode { get; set; }
        public string accountAccessory { get; set; }
        public string responseMessage { get; set; }
    }

    public class PatientPaymentToken
    {
        public int? Id { get; set; }
        public int PatientId { get; set; }
        public string Token { get; set; }
        public int CreatedBy { get; set; }
        public bool IsActive { get; set; }
        public int PaymentGatewayId { get; set; }
        public string SourceIP { get; set; }
        public string AccountNumberMasked { get; set; }
        public string TokenResponse { get; set; }
        public string AccountAccessory { get; set; }
    }

    public class CheckList
    {
        public int Id { get; set; }
        public string AccountNumberMasked { get; set; }
        public bool IsCheck { get; set; }
        public string accountAccessory { get; set; }
        public string Token { get; set; }
    }

}
