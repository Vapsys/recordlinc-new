﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    

    public class EmailVerification
    {
        public int UserId { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Emailtype { get; set; }
        public int Status { get; set; }
        public string ProcessDate { get; set; }
        public string Phone { get; set; }
    }

    public class EmailFilter
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string Searchtext { get; set; }
        public int SortColumn { get; set; }
        public int SortDirection { get; set; }
    }
}
