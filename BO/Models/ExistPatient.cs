﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// While Insert Patient time check Patient is already exist or not.
    /// </summary>
    public class ExistPatient
    {
        /// <summary>
        /// Patient New Email
        /// </summary>
        public string PatientEmail { get; set; }
        /// <summary>
        /// Patient Old Email
        /// </summary>
        public string OldEmail { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AssignPatientId { get; set; }
        public string OldAssignPatientId { get; set; }
        public int PatientId { get; set; }
    }
}
