﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BO.Models
{
    public class Patient_DentrixStandardPayment
    {
        [Required(ErrorMessage = "Paymentid is required.  ")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1")]
        public int paymentId { get; set; }
        public int patid { get; set; }
        public int guarid { get; set; }
        public DateTime? procdate { get; set; }
        public DateTime? procdatedatetime { get; set; }
        public DateTime? createdate { get; set; }
        public DateTime? createdatedatetime { get; set; }
        public string provid { get; set; }
        public int paymentdefid { get; set; }
        public string paymentdesc { get; set; }
        public decimal amount { get; set; }
        public bool history { get; set; }
        public bool appliedtopayagreement { get; set; }
        public DateTime? automodifiedtimestamp { get; set; }
        [Required (ErrorMessage = "DentrixConnectorId is required. ")]
        public string dentrixConnectorID { get; set; }
    }
    public class StandardPayment
    {
        /// <summary>
        /// Recordlinc Primary key
        /// </summary>
        public int RL_ID { get; set; }
        /// <summary>
        /// Dentrix Primary Key
        /// </summary>
        public int paymentId { get; set; }
        /// <summary>
        /// PatientId of Recordlinc
        /// </summary>
        public int patid { get; set; }
        /// <summary>
        /// Guaranteer Id of Recordlinc
        /// </summary>
        public int guarid { get; set; }
        /// <summary>
        /// Procedure Date
        /// </summary>
        public DateTime? procdate { get; set; }
        /// <summary>
        /// Entry / Create date of Dentrix
        /// </summary>
        public DateTime? createdate { get; set; }
        /// <summary>
        /// Dentrix Provider Id
        /// </summary>
        public string Dentrixprovid { get; set; }
        /// <summary>
        /// Recordlinc Provider Id
        /// </summary>
        public string RLprovid { get; set; }
        public int paymentdefid { get; set; }
        public string paymentdesc { get; set; }
        /// <summary>
        /// Amount
        /// </summary>
        public decimal amount { get; set; }
        public bool history { get; set; }
        public bool appliedtopayagreement { get; set; }
        /// <summary>
        /// Time stamp of Dentrix
        /// </summary>
        public DateTime? automodifiedtimestamp { get; set; }
        /// <summary>
        /// Dentrix PatientId
        /// </summary>
        public int DentrixPatientId { get; set; }
        /// <summary>
        /// Dentrix Guarantor Id
        /// </summary>
        public int DentrixGuarId { get; set; }
        public DateTime? procdatedatetime { get; set; }
        public DateTime? createdatedatetime { get; set; }
    }
}
