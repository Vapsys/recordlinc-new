﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class ClinicalNote
    {
        [Required(ErrorMessage = "Identifier is required.")]
        public string Identifier { get; set; }
        public int? NoteId { get; set; }
        public int? NoteType { get; set; }
        public string ProviderId { get; set; }
        public int? PatientId { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? NoteDate { get; set; }
        public DateTime? LockedDate { get; set; }
        public string Text { get; set; }
        public DateTime? automodifiedtimestamp { get; set; }
        [Required(ErrorMessage = "DentrixId is required.")]
        public int? DentrixId { get; set; }
        public string ConnectorID { get; set; }
        [Required(ErrorMessage = "dentrixConnectorID is required.")]
        public string dentrixConnectorID { get; set; }
        public bool IsDeleted { get; set; }
    }

    
}
