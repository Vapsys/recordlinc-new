﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// Used for Insert/Update Description text.
    /// </summary>
    public class Description
    {
        /// <summary>
        /// Description text.
        /// </summary>
        public string Text { get; set; }
    }
}
