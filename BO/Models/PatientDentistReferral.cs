﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class PatientDentistReferral
    {
        public bool IstxtFirstName { get; set; }
        public bool IstxtLastName { get; set; }
        public bool IstxtEmail { get; set; }
        public bool IstxtDob { get; set; }
        public bool IstxtGender { get; set; }
        public bool IstxtResidenceStreet { get; set; }
        public bool IstxtCity { get; set; }
        public bool IstxtState { get; set; }
        public bool IstxtZip { get; set; }
        public bool IstxtResidenceTelephone { get; set; }
        public bool IstxtWorkPhone { get; set; }
        public bool Istxtemergencyname { get; set; }
        public bool Istxtemergency { get; set; }
        public bool IstxtResponsiblepartyFname { get; set; }
        public bool IstxtResponsiblepartyLname { get; set; }
        public bool IstxtResponsibleRelationship { get; set; }
        public bool IstxtResponsibleAddress { get; set; }
        public bool IstxtResponsibleCity { get; set; }
        public bool IstxtResponsibleState { get; set; }
        public bool IstxtResponsibleZipCode { get; set; }
        public bool IstxtResponsibleContact { get; set; }
        public bool Istxtemailaddress { get; set; }
        public bool IstxtEmployeeName1 { get; set; }
        public bool IstxtInsurancePhone1 { get; set; }
        public bool IstxtEmployerName1 { get; set; }
        public bool IstxtEmployeeDob1 { get; set; }
        public bool IstxtNameofInsurance1 { get; set; }
        public bool IstxtInsuranceTelephone1 { get; set; }
        public bool IstxtEmployeeName2 { get; set; }
        public bool IstxtInsurancePhone2 { get; set; }
        public bool IstxtEmployerName2 { get; set; }
        public bool IstxtYearsEmployed2 { get; set; }
        public bool IstxtNameofInsurance2 { get; set; }
        public bool IstxtInsuranceTelephone2 { get; set; }
        public bool IsMtxtphysicians { get; set; }
        public bool IsMtxthospitalized { get; set; }
        public bool IsMtxtserious { get; set; }
        public bool IsMtxtmedications { get; set; }
        public bool IsMtxtRedux { get; set; }
        public bool IsMtxtFosamax { get; set; }
        public bool IsMtxt7 { get; set; }
        public bool IsMtxt8 { get; set; }
        public bool IsMtxt9 { get; set; }

        public bool IsMtxt10 { get; set; }
    }
}
