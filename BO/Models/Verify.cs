﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class VerifyInsu
    {   /// <summary>
        ///  Patient Id
        /// </summary>
        [Required(ErrorMessage ="Please provide patient id.")]
        [Range(1, Int64.MaxValue, ErrorMessage = "Patient id  must be greater than 0")]
        public int PatientId { get; set; }
        /// <summary>
        /// Verification object
        /// </summary>
        
        public Verify Verify { get; set; }
    }

    /// <summary>
    /// Verify Patient Insurance details from Onederful side.
    /// </summary>
    public class Verify
    {
        /// <summary>
        /// Subscriber(Patient) details
        /// </summary>
        public One_Subscribe subscriber { get; set; }
        /// <summary>
        /// Provider(Dentist) details
        /// </summary>
        public One_Provider provider { get; set; }
        /// <summary>
        /// Payer(Insurance company) details
        /// </summary>
        public One_Payer payer { get; set; }
    }
    /// <summary>
    /// Subscriber(Patient) details
    /// </summary>
    public class One_Subscribe
    {
        /// <summary>
        /// Patient firstname
        /// </summary>
        [Required(ErrorMessage = "Please provide first name.")]
        public string firstName { get; set; }
        /// <summary>
        /// Patient Lastname
        /// </summary>
        [Required(ErrorMessage = "Please provide last name.")]
        public string lastName { get; set; }
        /// <summary>
        /// Patient DOB
        /// </summary>
        [Required(ErrorMessage = "Please provide date of birth.")]
        public string dob { get; set; }
        /// <summary>
        /// Patient MemberId
        /// </summary>
        [Required(ErrorMessage = "Please provide member id.")]
        public string memberId { get; set; }
        /// <summary>
        /// Patient Group number
        /// </summary>
        [Required(ErrorMessage = "Please provide group number.")]
        public string groupNumber { get; set; }
    }
    /// <summary>
    /// Dentist(Provider) details
    /// </summary>
    public class One_Provider
    {
        /// <summary>
        /// Dentist NPI id
        /// </summary>
        [Required(ErrorMessage = "Please provide npi number.")]
        public string npi { get; set; }
    }
    /// <summary>
    /// Insurance company providerId(CompanyId)
    /// </summary>
    public class One_Payer
    {
        /// <summary>
        /// Insurance Company id
        /// </summary>
        [Required(ErrorMessage = "Please provide payer id.")]
        public string id { get; set; }
    }
}
