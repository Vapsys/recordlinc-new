﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class fraudPayload
    {
        public string responseCode { get; set; }
        public string authCode { get; set; }
        public string avsResponse { get; set; }
        public string authAmount { get; set; }
        public string id { get; set; }
        public string entityName { get; set; }

        public List<fraudList> fraudList { get; set; }

    }
}
