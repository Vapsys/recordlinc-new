﻿using Foolproof;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BO.Models
{
    public class Membership
    {
        //public Membership()
        //{
        //    List<Membership> lstInsurance = new List<Membership>();
        //}
        public int Membership_Id { get; set; }

        [Display(Name = "Title")]
        [Required(ErrorMessage = "Title is required.")]
        public string Title { get; set; }
        [Display(Name = "Description")]
        [Required(ErrorMessage = "Description is required.")]
        [AllowHtml]
        public string Description { get; set; }

        [Display(Name = "Sort order")]
        [Required(ErrorMessage = "Sort order is required")]
        [Range(1, int.MaxValue, ErrorMessage = "Sort order must be greater than 1.")]

        public int SortOrder { get; set; }

        [Display(Name = "Status")]
        [Required(ErrorMessage = "Status is required")]
        public int Status { get; set; }

        public int CurrentPageNo { get; set; }
     
        public DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int CreatedBy { get; set; }

     
        [Display(Name = "Monthly Price")]
        [Required(ErrorMessage = "Monthly Price is required")]
        public decimal MonthlyPrice { get; set; }
        [Display(Name = "Annual Price")]
        public decimal AnnualPrice { get; set; }
        [Display(Name = "Quarterly Price")]
        public decimal QuaterlyPrice { get; set; }
        [Display(Name = "Half Yearly Price")]
        public decimal HalfYearlyPrice { get; set; }

        public List<object> lstTypeOfStatus { get; set; }
    }
   
}
