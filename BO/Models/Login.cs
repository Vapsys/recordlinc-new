﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class Login
    {
        /// <summary>
        /// User name of the Dentist
        /// </summary>
        [Required(ErrorMessage = "Please enter Username/Email address.")]
        [Display(Name = "User Name")]    
        public string UserName { get; set; }
        /// <summary>
        /// Password of the Dentist
        /// </summary>
        [Required(ErrorMessage = "Please enter password.")]
        [DataType(DataType.Password)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}
