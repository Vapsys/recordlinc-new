﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BO.Models
{
    /// <summary>
    /// Team Member Details
    /// </summary>
    public class TeamMember
    {
        /// <summary>
        /// Team Member Id/ UserId
        /// </summary>
        public int TeamMemberId { get; set; }
        /// <summary>
        /// First Name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Last Name
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// Email Address
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// Specialty
        /// </summary>
        public string Specialty { get; set; }
        /// <summary>
        /// Location of that Team Member
        /// </summary>
        public string Location { get; set; }
        /// <summary>
        /// Provider Id
        /// </summary>
        public string ProviderId { get; set; }
        /// <summary>
        /// Common Specialty list for Dentist.
        /// </summary>
        public List<SelectListItem> Specialtylist { get; set; }
        /// <summary>
        /// List of Parrent Dentist Location
        /// </summary>
        public List<SelectListItem> Locationlist { get; set; }
        /// <summary>
        /// Is Staff Member?  
        /// </summary>
        public bool provtype { get; set; }
        /// <summary>
        /// Full Name
        /// </summary>
        public string FullName { get; set; }

    }
    /// <summary>
    /// Used while fetching the Team Member List from API side.
    /// </summary>
    public class FilterTeamMember
    {
        /// <summary>
        /// User id of the dentist
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Page number whose records are obtained via API call
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// Number of records per page to be obtained via API call
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// Search text need to search
        /// </summary>
        public string SearchText { get; set; }

        /// <summary>
        /// Accepts intger value on which data is being sorted
        /// 1.  FirstName
        /// 2.  LastName
        /// 7.  AssignedPatientId
        /// 9.  Phone
        /// 10. Email
        /// 11. FirstName LastName
        /// 12. Patient ID
        /// 14. Dentist FirstName
        /// </summary>
        public TeamMemberSort SortColumn { get; set; }

        /// <summary>
        /// Direction of sorting 1 for ascending or 2 for descending
        /// </summary>
        public int SortDirection { get; set; }

        /// <summary>
        /// Filter Team Member Location wise
        /// </summary>
        public int LocationBy { get; set; }
        /// <summary>
        /// Provider Type filter 
        /// </summary>
        public int ProviderTypefilter { get; set; }
        /// <summary>
        /// In Team member list in Display a drop down of all team members including logged in user
        /// </summary>
        public bool IncludeUser { get; set; }
    }
    /// <summary>
    /// Sorting the Team member using enum
    /// </summary>
    public enum TeamMemberSort
    {
        /// <summary>
        /// Sort FristName
        /// </summary>
        FirstName = 1,
        /// <summary>
        /// Sort LastName
        /// </summary>
        LastName = 2,
        /// <summary>
        /// Sort Email
        /// </summary>
        Email = 3,
        /// <summary>
        /// Sort Specialty
        /// </summary>
        Specialty = 4,
        /// <summary>
        /// Sort Location
        /// </summary>
        Location = 5,
        /// <summary>
        /// Sort Provider Id
        /// </summary>
        ProviderID = 6,
    }
    /// <summary>
    /// Remove Team Member Ids
    /// </summary>
    public class RemoveTeam
    {
        /// <summary>
        /// List of Team Member which want to Removed
        /// </summary>
        public string[] TeamMemberId { get; set; }
    }
}
