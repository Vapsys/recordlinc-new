﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class GenericResponseObject<T>
    {
        private T _Result;
        private bool _success = true;
        private int _total;
        private string message;
        private int _StatusCode;

        [DataMember()]
        public bool IsSuccess
        {
            get { return _success; }
            set { _success = value; }
        }

        [DataMember()]
        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        [DataMember()]
        public T Result
        {
            get { return _Result; }
            set { _Result = value; }
        }

        [DataMember()]
        public int StatusCode
        {
            get { return _StatusCode; }
            set { _StatusCode = value; }
        }

        public void RecordResponseObject(ref T data)
        {
            _Result = data;
        }
    }
}
