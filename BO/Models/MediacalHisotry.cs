﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class MediacalHisotry
    {
        public string MrdQue1 { get; set; }
        public string MrNQue1 { get; set; }
        public string Mtxtphysicians { get; set; }
        public string MrdQue2 { get; set; }
        public string MrnQue2 { get; set; }
        public string Mtxthospitalized { get; set; }
        public string MrdQue3 { get; set; }
        public string MrnQue3 { get; set; }
        public string Mtxtserious { get; set; }
        public string MrdQue4 { get; set; }
        public string MrnQue4 { get; set; }
        public string Mtxtmedications { get; set; }
        public string MrdQue5 { get; set; }
        public string MrnQue5 { get; set; }
        public string MtxtRedux { get; set; }
        public string MrdQue6 { get; set; }
        public string MrnQue6 { get; set; }
        public string MtxtFosamax { get; set; }
        public string MrdQuediet7 { get; set; }
        public string MrnQuediet7 { get; set; }
        public string Mtxt7 { get; set; }
        public string Mrdotobacco8 { get; set; }
        public string Mrnotobacco8 { get; set; }
        public string Mtxt8 { get; set; }
        public string Mrdosubstances { get; set; }
        public string Mrnosubstances { get; set; }
        public string Mtxt9 { get; set; }
        public string Mrdopregnant { get; set; }
        public string Mrnopregnant { get; set; }
        public string Mtxt10 { get; set; }
        public string Mrdocontraceptives { get; set; }
        public string Mrnocontraceptives { get; set; }
        public string MrdoNursing { get; set; }
        public string MrnoNursing { get; set; }
        public string Mrdotonsils { get; set; }
        public string Mrnotonsils { get; set; }
        public string Mtxt13 { get; set; }
        public string MchkQue_1 { get; set; }
        public string MchkQueN_1 { get; set; }
        public string MchkQue_2 { get; set; }
        public string MchkQueN_2 { get; set; }
        public string MchkQue_3 { get; set; }
        public string MchkQueN_3 { get; set; }
        public string MchkQue_4 { get; set; }
        public string MchkQueN_4 { get; set; }
        public string MchkQue_5 { get; set; }
        public string MchkQueN_5 { get; set; }
        public string MchkQue_6 { get; set; }
        public string MchkQueN_6 { get; set; }
        public string MchkQue_7 { get; set; }
        public string MchkQueN_7 { get; set; }
        public string MchkQue_8 { get; set; }
        public string MchkQueN_8 { get; set; }
        public string MchkQue_9 { get; set; }
        public string MchkQueN_9 { get; set; }
        public string MtxtchkQue_9 { get; set; }

        public string MrdQueAIDS_HIV_Positive { get; set; }
        public string MrdQueAlzheimer { get; set; }
        //public string MrdQueAnaphylaxis { get; set; }
        public string MrdQueAnemia { get; set; }
        public string MrdQueAngina { get; set; }
       // public string MrdQueArthritis_Gout { get; set; }
        public string MrdQueArtificialHeartValve { get; set; }
        public string MrdQueArtificialJoint { get; set; }
        public string MrdQueAsthma { get; set; }
        public string MrdQueBloodDisease { get; set; }
        public string MrdQueBloodTransfusion { get; set; }
        public string MrdQueBreathing { get; set; }

        public string MrdQueBruise { get; set; }
        public string MrdQueCancer { get; set; }
        public string MrdQueChemotherapy { get; set; }
        public string MrdQueChest { get; set; }
      //  public string MrdQueCold_Sores_Fever { get; set; }
       // public string MrdQueCongenital { get; set; }
        public string MrdQueConvulsions { get; set; }

        public string MrdQueCortisone { get; set; }
      //  public string MrdQueDiabetes { get; set; }
      //  public string MrdQueDrug { get; set; }
      //  public string MrdQueEasily { get; set; }
        public string MrdQueEmphysema { get; set; }
        public string MrdQueEpilepsy { get; set; }
      //  public string MrdQueExcessiveBleeding { get; set; }
        public string MrdQueExcessiveThirst { get; set; }

      //  public string MrdQueFainting { get; set; }
      //  public string MrdQueFrequentCough { get; set; }
      //  public string MrdQueFrequentDiarrhea { get; set; }
      //  public string MrdQueFrequentHeadaches { get; set; }
      //  public string MrdQueGenital { get; set; }
        public string MrdQueGlaucoma { get; set; }

      //  public string MrdQueHay { get; set; }
      //  public string MrdQueHeartAttack_Failure { get; set; }
      //  public string MrdQueHeartMurmur { get; set; }
        public string MrdQueHeartPacemaker { get; set; }
      //  public string MrdQueHeartTrouble_Disease { get; set; }
        public string MrdQueHemophilia { get; set; }

      //  public string MrdQueHepatitisA { get; set; }
      //  public string MrdQueHepatitisBorC { get; set; }
        public string MrdQueHerpes { get; set; }
      //  public string MrdQueHighBloodPressure { get; set; }
      //  public string MrdQueHighCholesterol { get; set; }
      //  public string MrdQueHives { get; set; }

        public string MrdQueHypoglycemia { get; set; }
      //  public string MrdQueIrregular { get; set; }
        public string MrdQueKidney { get; set; }
        public string MrdQueLeukemia { get; set; }
        public string MrdQueLiver { get; set; }
      //  public string MrdQueLow { get; set; }
        public string MrdQueLung { get; set; }

        public string MrdQueMitral { get; set; }
        public string MrdQueOsteoporosis { get; set; }
        public string MrdQuePain { get; set; }
      //  public string MrdQueParathyroid { get; set; }
        public string MrdQuePsychiatric { get; set; }
        public string MrdQueRadiation { get; set; }
      //  public string MrdQueRecent { get; set; }

      //  public string MrdQueRenal { get; set; }
        public string MrdQueRheumatic { get; set; }
      //  public string MrdQueRheumatism { get; set; }
        public string MrdQueScarlet { get; set; }
        public string MrdQueShingles { get; set; }
      //  public string MrdQueSickle { get; set; }
        public string MrdQueSinus { get; set; }
      //  public string MrdQueSpina { get; set; }

        public string MrdQueStomach { get; set; }
        public string MrdQueStroke { get; set; }
        public string MrdQueSwelling { get; set; }
        public string MrdQueThyroid { get; set; }
      //  public string MrdQueTonsillitis { get; set; }
        public string MrdQueTuberculosis { get; set; }
        public string MrdQueTumors { get; set; }
        public string Mchk14 { get; set; }
        public string Mchk15 { get; set; }
        public string Mrdillness { get; set; }
        public string MrdComments { get; set; }
        //  public string MrdQueUlcers { get; set; }
        //  public string MrdQueVenereal { get; set; }
        //  public string MrdQueYellow { get; set; }
        public string Mtxtillness { get; set; }
        public string MtxtComments { get; set; }

        //new fields add on 06/27/2017
        public string MrdQueAbnormal { get; set; }
        public string MrdQueThinners { get; set; }
        public string MrdQueFibrillation { get; set; }
        public string MrdQueCigarette { get; set; }
        public string MrdQueCirulation { get; set; }
        public string MrdQuePersistent { get; set; }
        public string MrdQuefillers { get; set; }
        public string MrdQueDiabletes { get; set; }
        public string MrdQueDiarrhhea { get; set; }
        public string MrdQueDigestive { get; set; }
        public string MrdQueDizziness { get; set; }
        public string MrdQueAlcoholic { get; set; }
        public string MrdQueSubstances { get; set; }
        public string MrdQueHeadaches { get; set; }
        public string MrdQueAttack { get; set; }
        public string MrdQueHeart_defect { get; set; }
        public string MrdQueHeart_murmur { get; set; }
        public string MrdQueHiatal { get; set; }
        public string MrdQueBlood_pressure { get; set; }
        public string MrdQueAIDS { get; set; }
        public string MrdQueHow_much { get; set; }
        public string MrdQueHow_often { get; set; }
        public string MrdQueReplacement { get; set; }
        public string MrdQueNeck_BackProblem { get; set; }
        public string MrdQuePacemaker { get; set; }
        public string MrdQuePainJaw_Joints { get; set; }
        public string MrdQueEndocarditis { get; set; }
        public string MrdQueSjorgren { get; set; }
        public string MrdQueSwollen { get; set; }
        public string MrdQueValve { get; set; }
        public string MrdQueVision { get; set; }
     
    }
}
