﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class CreateRewards
    {
        /// <summary>
        /// It is a Solution1 patient Id.
        /// </summary>
        public int RewardPartnerId { get; set; }
        public string RewardTypeCode { get; set; }
        public string RewardDate { get; set; }
        public APIFilter.FilterFlag sourcetype { get; set; }        
    }
}
