﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class CheckPatients
    {
        public string PatientEmail { get; set; }
        public string AssignedPatientId { get; set; }
        public string OldPatientEmail { get; set; }
        public string oldAssignedPatientId { get; set; }
        public int PatientId { get; set; }
    }
}
