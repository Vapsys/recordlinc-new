﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class Sorting
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public string SearchText { get; set; }
        public int SortColumn { get; set; }
        public int SortDirection { get; set; }
        public int FilterBy { get; set; }
        public string NewSearchText { get; set; }
        public int Id { get; set; }
        public bool IsDashboard { get; set; }
    }
}
