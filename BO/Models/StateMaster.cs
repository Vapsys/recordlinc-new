﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
  public class StateMaster
    {
        public string StateName { get; set; }
        public string CountryCode { get; set; }
        public IEnumerable<StateMaster> stateMasters { get; set; }
    }
}
