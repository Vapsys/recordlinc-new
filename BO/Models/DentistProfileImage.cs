﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// Add Dentist Profile Image
    /// </summary>
    public class DentistProfileImage
    {
        /// <summary>
        /// Image Bottom side crop
        /// </summary>
        public double Bottom { get; set; }
        /// <summary>
        /// Image Right side crop
        /// </summary>
        public double Right { get; set; }
        /// <summary>
        /// Image Top side crop
        /// </summary>
        public double Top { get; set; }
        /// <summary>
        /// Image Left side crop
        /// </summary>
        public double Left { get; set; }
        /// <summary>
        /// File Name
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// Base 64 string of Image
        /// </summary>
        public string Base64URL { get; set; }
    }
}
