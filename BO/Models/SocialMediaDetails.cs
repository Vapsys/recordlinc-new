﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class SocialMediaDetails
    {
        public int? Id { get; set; }
        public int? Usertype { get; set; }
        public int? Userid { get; set; }

        public string FacebookUrl { get; set; }
        public string LinkedinUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string GoogleplusUrl { get; set; }
        public string YoutubeUrl { get; set; }
        public string PinterestUrl { get; set; }
        public string BlogUrl { get; set; }
        public string YelpUrl { get; set; }
        public string OtherUrl { get; set; }
        public DateTime? LastModifiedDate { get; set; }
    }
}
