﻿namespace BO.Models
{
    public class RatingObjects
    {
        public int ObjectId { get; set; }
        public int OpportunityId { get; set; }
        public string OpportunityType { get; set; }

    }
}
