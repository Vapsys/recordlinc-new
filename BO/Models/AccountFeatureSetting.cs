﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BO.Models
{
    public class AccountFeatureSetting
    {
        public AccountFeatureSetting()
        {
            lstDentist = new List<SelectListItem>();
        }
        public int id { get; set; }
        public int AccountId { get; set; } 
        public int FeatureMasterId { get; set; }
        public bool AdminStatus { get; set; }
        public bool DentistStatus { get; set; }
        public List<SelectListItem> lstDentist { get; set; }
        public int CreatedBy { get; set; }
    }

    public class chkFeatureSetting
    {
        public bool AdminStatus { get; set; }
        public bool DentistStatus { get; set; }
        public int Id { get; set; }
        public string Feature { get; set; }
        public int AccountId { get; set; }
        public int FeatureId { get; set; }
    }
    
}

