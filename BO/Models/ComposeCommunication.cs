﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// Compose Communication section used for Send Message from Communication section in Patient History page.
    /// </summary>
    public class ComposeCommunication
    {
        /// <summary>
        /// Patient Id
        /// </summary>
        public int PatientId { get; set; }
        /// <summary>
        /// Comma Separated Receiver Dentist Id
        /// </summary>
        public string ReciverId { get; set; }
        /// <summary>
        /// Message body
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// UserId / OwnerId which is sending the Message.
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Temp Images/Documents Id which are added on 
        /// </summary>
        public string[] Files { get; set; }
    }
}
