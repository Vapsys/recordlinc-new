﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class SearchDentist
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string zipcode { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string FilterBy { get; set; }
    }
}
