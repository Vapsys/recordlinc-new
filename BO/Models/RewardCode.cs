﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
  public class ProcedureCodes
    {
        public int AccountId { get; set; }
        public int ProcedureCodeId { get; set; }
        [Required(ErrorMessage= "Procedure Code is required.")]
        public string ProcedureCode { get; set; }
        [Required(ErrorMessage = "Procedure Name is required.")]
        public string ProcedureName { get; set; }
        [Required(ErrorMessage = "SuperBucks Rewards is required.")]
        public decimal SuperBucksReward { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
    }
}
