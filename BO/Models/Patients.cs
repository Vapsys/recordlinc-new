﻿using Foolproof;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class Patients
    {
        public int PatientId { get; set; }
        public string AssignedPatientId { get; set; }

        [Required(ErrorMessage = "First name is required.")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Last name is required.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Email address is required.")]
        [Display(Name = "Email")]
        //[EmailAddress(ErrorMessage = "Invalid email address.")]
        [RegularExpression(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$",ErrorMessage = "Invalid email address.")]
        public string Email { get; set; }

        //[Display(Name = "Gender")]
        //[Required(ErrorMessage = "Please select Gender")]
        //public bool? Gender { get; set; }

        [Display(Name = "Gender")]
        [Required(ErrorMessage = "Gender is required.")]
        public string Gender { get; set; }
        //[StringLength(10, ErrorMessage = "Phone number must be at least 10 characters long.", MinimumLength = 10)]
        public string Phone { get; set; }
        public string ReferredBy { get; set; }
        public string LocationName { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }

        public string DateOfBirth { get; set; }
        [LessThan("DateOfBirth", DependentPropertyDisplayName = "DateOfBirth", ErrorMessage = "First exam date must be after date of birth.")]
        public string FirstExamDate { get; set; }

        public string Notes { get; set; }
        //New 
        public string ExactAddress { get; set; }
        public string Age { get; set; }
        public string Address2 { get; set; }
        public string Country { get; set; }
        public int OwnerId { get; set; }
        public int PatientStatus { get; set; }
        public string Password { get; set; }
        public string ColleagueId { get; set; }
        public string LocationId { get; set; }
        public string StateId { get; set; }
        public string SecondaryPhone { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyPhoneNo { get; set; }
        public bool ItHasInsuranceData { get; set; }
        public InsuranceCoverage _insuranceCoverage { get; set; }
        public Compose1ClickMedicalHistory _medicalHistory { get; set; }
        public Compose1ClickDentalHistory _dentalhistory { get; set; }
    }
}
