﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class PatientAssociation
    {
        [Required(ErrorMessage = "PatientId is required.")]
        public int PatientId { get; set; }
        [Required(ErrorMessage = "RewardPartnerId is required.")]
        public int RewardPartnerId { get; set; }
        [Required(ErrorMessage = "AccountId is required.")]
        public int AccountId { get; set; }
        public APIFilter.FilterFlag sourcetype { get; set; }
    }
}
