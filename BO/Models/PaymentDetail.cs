﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class PaymentDetail
    {
        public string CardType { get; set; }
        public string Cvv { get; set; }
        public string CardNumber { get; set; }
        public string expdate { get; set; }
        public string Amount { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public bool isDoctorInSystem { get; set; }
        public string Userid { get; set; }
        public string Description { get; set; }
        public string Planes { get; set; }
        public DateTime PackageStartDate { get; set; }
        public DateTime PackageEndDate { get; set; }
        public List<UserMembership> ListOfMembership = new List<UserMembership>();
        public string payInterval { get; set; }
        public string TransactionId { get; set; }
        public bool IsLuvMyDentist { get; set; }
    }
    public class UserMembership
    {
        public int Membership_Id { get; set; }
        public string Title { get; set; }
        public decimal MonthlyPrice { get; set; }
        public decimal AnnualPrice { get; set; }
        public decimal QuaterlyPrice { get; set; }
        public decimal HalfYearlyPrice { get; set; }
    }
}
