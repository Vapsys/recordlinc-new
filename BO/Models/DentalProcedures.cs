﻿using System;

namespace BO.Models
{
    public class DentalProceduresModel
    {
        public int ProcedureId { get; set; }
        public string ProcedureName { get; set; }
        public decimal StandardRate { get; set; }
        public decimal? InsuranceCostRatio { get; set; }
        public int UserId { get; set; }
        public DateTime CreationDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }

    }
}
