﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class PatientRewardNotification
    {
        public int PatientRewardNotificationId { get; set; }
        public int PatientRewardId { get; set; }
        public bool SentToRewardPlatform { get; set; }
        public DateTime RewardDate { get; set; }
        public bool SentToMobile { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
