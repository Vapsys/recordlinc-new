﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BO.Models;

namespace BO.Models
{
    public class NormalMessageDetails
    {
        public int MessageId { get; set; }
        public int SenderId { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public string CreationDate { get; set; }
        public string ScheduledDate { get; set; }
        public bool IsPatientMessage { get; set; }
        public bool Status { get; set; }
        public string Body { get; set; }
        public string SenderEmail { get; set; }
        public bool IsSentMessage { get; set; }
        public bool Isread { get; set; }
        public string ReceiverEmail { get; set; }
        public string AttachedPatientsId { get; set; }
        public List<MessageAttachment> lstMessageAttachment = new List<MessageAttachment>();
        public List<AttachedPatientDetails> lstAttachedPatient = new List<AttachedPatientDetails>();
        public int MsgStatus { get; set; }
    }
}
