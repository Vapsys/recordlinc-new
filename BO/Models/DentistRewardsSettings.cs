﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class DentistRewardsSettings
    {
        public int DentistRewardsSettingsId { get; set; }
        public int RewardsMasterId { get; set; }
        public int RewardsOptionId { get; set; }
        public int AccountId { get; set; }        
    }
   
}
