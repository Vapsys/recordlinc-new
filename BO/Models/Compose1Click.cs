﻿using BO.ViewModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BO.Models
{
    public class Compose1Click
    {
        public Compose1Click()
        {
            lstSpecialityMaster = new List<SpecialityMaster>();
        }
        public string SenderId { get; set; }
        public string ReceiverId { get; set; }
        public int LocationId { get; set; }
        public PatinentInfo _patientInfo { get; set; }
        public PatientContactInfo _contactInfo { get; set; }
        public InsuranceCoverage _insuranceCoverage { get; set; }
        public Compose1ClickMedicalHistory _medicalHistory { get; set; }
        public Compose1ClickDentalHistory _dentalhistory { get; set; }
        public SpecialityService _specialtyService { get; set; }
        public InnerService _innerService { get; set; }
        public ServicesValues _ServiceVal { get; set; }
        public SendReferralModel _sendReferralModel { get; set; }
        public string XMLstring { get; set; }
        public string fileIds { get; set; }
        public string BoxFolderId { get; set; }
        public string Comment { get; set; }
        public List<SpecialityMaster> lstSpecialityMaster { get; set; }
        public bool IsReferral { get; set; }
        public List<StateList> StateList { get; set; }
    }
    public class InnerService
    {
        public InnerService()
        {
            lstDentalInsurance = new List<AddPatient>();
            lstPatient = new List<AddPatient>();
        }

        public List<AddPatient> lstDentalInsurance { get; set; }
        public List<AddPatient> lstPatient { get; set; }
        public bool txtFirstName { get; set; }
        public bool txtLastName { get; set; }
        public bool txtEmail { get; set; }
        public bool txtDob { get; set; }
        public bool txtGender { get; set; }
        public bool txtResidenceStreet { get; set; }
        public bool txtCity { get; set; }
        public bool txtState { get; set; }
        public bool txtZip { get; set; }
        public bool txtResidenceTelephone { get; set; }
        public bool txtWorkPhone { get; set; }
        public bool txtemergencyname { get; set; }
        public bool txtemergency { get; set; }
        public bool txtResponsiblepartyFname { get; set; }
        public bool txtResponsiblepartyLname { get; set; }
        public bool txtResponsibleRelationship { get; set; }
        public bool txtResponsibleAddress { get; set; }
        public bool txtResponsibleCity { get; set; }
        public bool txtResponsibleState { get; set; }
        public bool txtResponsibleZipCode { get; set; }
        public bool txtResponsibleContact { get; set; }
        public bool txtemailaddress { get; set; }
        public bool MrdIsInsured { get; set; }
        public bool txtEmployeeName1 { get; set; }
        public bool txtInsurancePhone1 { get; set; }
        public bool txtEmployerName1 { get; set; }
        public bool txtEmployeeDob1 { get; set; }
        public bool txtNameofInsurance1 { get; set; }
        public bool txtInsuranceTelephone1 { get; set; }
        public bool txtEmployeeName2 { get; set; }
        public bool txtInsurancePhone2 { get; set; }
        public bool txtEmployerName2 { get; set; }
        public bool txtYearsEmployed2 { get; set; }
        public bool txtNameofInsurance2 { get; set; }
        public bool txtInsuranceTelephone2 { get; set; }

        public string Mtxtphysicians { get; set; }
        public bool MrdQue1 { get; set; }
        public string Mtxthospitalized { get; set; }
        public bool MrdQue2 { get; set; }
        public string Mtxtserious { get; set; }
        public bool MrdQue3 { get; set; }
        public string Mtxtmedications { get; set; }
        public bool MrdQue4 { get; set; }
        public string MtxtRedux { get; set; }
        public bool MrdQue5 { get; set; }
        public string MtxtFosamax { get; set; }
        public bool MrdQue6 { get; set; }

        public string Mtxt7 { get; set; }
        public bool MrdQuediet7 { get; set; }
        public string Mtxt8 { get; set; }
        public bool Mrdotobacco { get; set; }
        public string Mtxt9 { get; set; }
        public bool Mrdosubstances { get; set; }
        public string Mtxt10 { get; set; }
        public bool Mrdopregnant { get; set; }
        public bool Mrdocontraceptives { get; set; }
        public bool Mtxt11 { get; set; }
        public bool MrdoNursing { get; set; }
        public bool Mtxt12 { get; set; }
        public bool Mrdotonsils { get; set; }
        public bool Mchk14 { get; set; }
        public bool Mchk15 { get; set; }
        public bool Mtxt13 { get; set; }
        public bool txtQue5 { get; set; }
        public bool txtQue6 { get; set; }
        public bool txtQue12 { get; set; }
        public bool rdQue15 { get; set; }
        public bool rdQue16 { get; set; }
        public bool rdQue17 { get; set; }
        public bool rdQue18 { get; set; }
        public bool rdQue19 { get; set; }
        public bool rdQue20 { get; set; }
        public bool rdQue21 { get; set; }
        public  bool rdQue23 { get; set; } 
        public bool rdQue25 { get; set; }
        public bool txtQue22 { get; set; }
        public string MtxtWhen { get; set; }
        public bool txtQue23 { get; set; }
        public bool rdQue24 { get; set; }
        public bool rdQue28 { get; set; }
        public bool txtQue29 { get; set; }
        public bool txtQue29a { get; set; }
        public  bool txtQue30 { get; set; }
        public bool txtQue31 { get; set; }
        public bool txtQue26 { get; set; }
        public bool txtComments { get; set; }
        public bool txtDateoffirstvisit { get; set; }
        public bool Reasonforfirstvisit { get; set; }
        public bool Dateoflastvisit { get; set; }
        public bool Reasonforlastvisit { get; set; }
        public bool Dateofnextvisit { get; set; }
        public bool Reasonfornextvisit { get; set; }

        //Following Medical History
        //START CODE
        public bool MrdQueAIDS_HIV_Positive { get; set; }
        public bool MrdQueAlzheimer { get; set; }
        public bool MrdQueAnaphylaxis { get; set; }
        public bool MrdQueAnemia { get; set; }
        public bool MrdQueAngina { get; set; }
        public bool MrdQueArthritis { get; set; }
        public bool MrdQueArtificialHeartValve { get; set; }
        public bool MrdQueArtificialJoint { get; set; }
        public bool MrdQueAsthma { get; set; }
        public bool MrdQueBloodDisease { get; set; }
        public bool MrdQueBloodTransfusion { get; set; }
        public bool MrdQueBoneDisorders { get; set; }
        public bool MrdQueBreathing { get; set; }
        public bool MrdQueBruise { get; set; }
        public bool MrdQueCancer { get; set; }
        public bool MrdQueChemicalDependancy { get; set; }
        public bool MrdQueChemotherapy { get; set; }
        public bool MrdQueChest { get; set; }
        public bool MrdQueCold_Sores_Fever { get; set; }
        public bool MrdQueCongenital { get; set; }
        public bool MrdQueConvulsions { get; set; }
        public bool MrdQueCortisone { get; set; }
        public bool MrdQueCortisoneTreatments { get; set; }
        public bool MrdQueDiabetes { get; set; }
        public bool MrdQueDrug { get; set; }
        public bool MrdQueEasily { get; set; }
        public bool MrdQueEmphysema { get; set; }
        public bool MrdQueEndocrineProblems { get; set; }
        public bool MrdQueEpilepsy { get; set; }
        public bool MrdQueExcessiveBleeding { get; set; }
        public bool MrdQueExcessiveThirst { get; set; }
        public bool MrdQueExcessiveurination { get; set; }
        public bool MrdQueFainting { get; set; }
        public bool MrdQueFrequentCough { get; set; }
        public bool MrdQueFrequentDiarrhea { get; set; }
        public bool MrdQueFrequentHeadaches { get; set; }
        public bool MrdQueGenital { get; set; }
        public bool MrdQueGlaucoma { get; set; }
        public bool MrdQueHay { get; set; }
        public bool MrdQueHeartAttack_Failure { get; set; }
        public bool MrdQueHeartMurmur { get; set; }
        public bool MrdQueHeartPacemaker { get; set; }
        public bool MrdQueHeartTrouble_Disease { get; set; }
        public bool MrdQueHemophilia { get; set; }
        public bool MrdQueHepatitisA { get; set; }
        public bool MrdQueHepatitisBorC { get; set; }
        public bool MrdQueHerpes { get; set; }
        public bool MrdQueHighBloodPressure { get; set; }
        public bool MrdQueHighCholesterol { get; set; }
        public bool MrdQueHives { get; set; }
        public bool MrdQueHypoglycemia { get; set; }
        public bool MrdQueIrregular { get; set; }
        public bool MrdQueKidney { get; set; }
        public bool MrdQueLeukemia { get; set; }
        public bool MrdQueLiver { get; set; }
        public bool MrdQueLow { get; set; }
        public bool MrdQueLung { get; set; }
        public bool MrdQueMitral { get; set; }
        public bool MrdQueNervousDisorder { get; set; }
        public bool MrdQueOsteoporosis { get; set; }
        public bool MrdQuePain { get; set; }
        public bool MrdQueParathyroid { get; set; }
        public bool MrdQuePsychiatric { get; set; }
        public bool MrdQueRadiation { get; set; }
        public bool MrdQueRecent { get; set; }
        public bool MrdQueRenal { get; set; }
        public bool MrdQueRheumatic { get; set; }
        public bool MrdQueRheumatism { get; set; }
        public bool MrdQueScarlet { get; set; }
        public bool MrdQueShingles { get; set; }
        public bool MrdQueSickle { get; set; }
        public bool MrdQueSinus { get; set; }
        public bool MrdQueSpina { get; set; }
        public bool MrdQueStomach { get; set; }
        public bool MrdQueStroke { get; set; }
        public bool MrdQueSwelling { get; set; }
        public bool MrdQueThyroid { get; set; }
        public bool MrdQueTuberculosis { get; set; }
        public bool MrdQueTonsillitis { get; set; }
        public bool MrdQueTumors { get; set; }
        public bool MrdQueUlcers { get; set; }
        public bool MrdQueVenereal { get; set; }
        public bool MrdQueYellow { get; set; }
        public bool MrdQueAbnormal { get; set; }
        public bool MrdQueThinners { get; set; }
        public bool MrdQueFibrillation { get; set; }
        public bool MrdQueCigarette { get; set; }
        public bool MrdQueCirulation { get; set; }
        public bool MrdQuePersistent { get; set; }
        public bool MrdQuefillers { get; set; }
        public bool MrdQueDiabletes { get; set; }
        public bool MrdQueDiarrhhea { get; set; }
        public bool MrdQueDigestive { get; set; }
        public bool MrdQueDizziness { get; set; }
        public bool MrdQueAlcoholic { get; set; }
        public bool MrdQueSubstances { get; set; }
        public bool MrdQueHeadaches { get; set; }
        public bool MrdQueAttack { get; set; }
        public bool MrdQueHeart_defect { get; set; }
        public bool MrdQueHeart_murmur { get; set; }
        public bool MrdQueHiatal { get; set; }
        public bool MrdQueBlood_pressure { get; set; }
        public bool MrdQueAIDS { get; set; }
        public bool MrdQueHow_much { get; set; }
        public bool MrdQueHow_often { get; set; }
        public bool MrdQueReplacement { get; set; }
        public bool MrdQueNeck_BackProblem { get; set; }
        public bool MrdQuePacemaker { get; set; }
        public bool MrdQuePainJaw_Joints { get; set; }
        public bool MrdQueEndocarditis { get; set; }
        public bool MrdQueSjorgren { get; set; }
        public bool MrdQueSwollen { get; set; }
        public bool MrdQueValve { get; set; }
        public bool MrdQueVision { get; set; }
        public bool QueFollowing14 { get; set; }
        public bool QueFollowing15 { get; set; }
        public bool MchkQue_1 { get; set; }
        public bool MchkQue_2 { get; set; }
        public bool MchkQue_3 { get; set; }
        public bool MchkQue_4 { get; set; }
        public bool MchkQue_5 { get; set; }
        public bool MchkQue_6 { get; set; }
        public bool MchkQue_7 { get; set; }
        public bool MchkQue_8 { get; set; }
        public string Mtxtillness { get; set; }
        public bool Mrdillness { get; set; }
        public string MtxtComments { get; set; }
        public bool MrdComments { get; set; }
        //END CODE
        public bool MrdQue7 { get; set; }
        public bool DhQue5 { get; set; }
        //Boolean
        public bool MrdExtract { get; set; }
        public bool MrdLesion { get; set; }        public bool MrdIncision { get; set; }        public bool MrdExposure { get; set; }
        public bool MrdBiopsy { get; set; }
        public bool MrdExpose { get; set; }        public bool MrdBoneGrafting { get; set; }
        public bool MrdWisdomTeeth { get; set; }
        public bool MrdJawSurgery { get; set; }
        public bool MrdTMJ { get; set; }        public bool MrdFacialReconstruction { get; set; }
        public bool MrdFacialAesthetics { get; set; }
        public bool MrdPathology { get; set; }        public bool MrdSnoring { get; set; }        public bool MrdOther { get; set; }        public bool MrdScaling { get; set; }        public bool MrdPeriodontal { get; set; }        public bool MrdOcclusal { get; set; }        public bool MrdLANAP { get; set; }
        public bool MrdOsseous { get; set; }        public bool MrdCrownSurgical { get; set; }        public bool MrdBoneGuided { get; set; }        public bool MrdSoftTissue { get; set; }        public bool MrdImplantPlacement { get; set; }        public bool MrdALLOn4 { get; set; }        public bool MrdImplantSite { get; set; }        public bool MrdSinus { get; set; }        public bool MrdBoneRegeneration { get; set; }        public bool MrdRidge { get; set; }        public bool MrdLimited { get; set; }        public bool MrdCrownDental { get; set; }        public bool MrdImplantConsultation { get; set; }        public bool MrdGingival { get; set; }        public bool MrdMucogingival { get; set; }        public bool MrdFrenum { get; set; }        public bool MrdPeriodonticsOther { get; set; }        public bool MrdPeriodontalHistory { get; set; }        public bool MrdConsultation { get; set; }        public bool MrdTMD { get; set; }        public bool MrdRetainerNeeded { get; set; }        public bool MrdOrthodonticsOther { get; set; }        public bool MrdCaries { get; set; }        public bool MrdExtractions { get; set; }        public bool MrdFracturedTeeth { get; set; }        public bool MrdGrowth { get; set; }        public bool MrdMissingTeeth { get; set; }        public bool MrdOralHabits { get; set; }        public bool MrdOrthodonticEvaluation { get; set; }        public bool MrdPeriodonticCondition { get; set; }        public bool MrdPulpTherapy { get; set; }        public bool MrdPediatricsOther { get; set; }        public bool MrdBehavoral { get; set; }        public bool MrdDiagnosticConsultation { get; set; }        public bool MrdTreatasNecessary { get; set; }        public bool MrdEvaluateforMicrosurgery { get; set; }        public bool MrdProvidePostSpace { get; set; }        public bool MrdPost_core { get; set; }        public bool MrdNitrous { get; set; }        public bool MrdEndodonticsOther { get; set; }        public bool MrdDentalLabteeth { get; set; }        public bool MrdRadiologyteeth { get; set; }
        public bool MrdImplantOther { get; set; }
        public bool MrdGeneralDentistryOther { get; set; }        public bool MrdOtherteeth { get; set; }
        public bool MrdEmployeeName1 { get; set; }        public bool MrdInsurancePhone1 { get; set; }        public bool MrdEmployerName1 { get; set; }        public bool MrdEmployeeDob1 { get; set; }        public bool MrdNameofInsurance1 { get; set; }        public bool MrdInsuranceTelephone1 { get; set; }        public bool MrdEmployeeName2 { get; set; }        public bool MrdInsurancePhone2 { get; set; }        public bool MrdEmployerName2 { get; set; }        public bool MrdYearsEmployed2 { get; set; }        public bool MrdNameofInsurance2 { get; set; }        public bool MrdInsuranceTelephone2 { get; set; }

        //Added Fields
        public bool MrdExtractionMolar { get; set; }
        public bool MrdFrenectomy { get; set; }
        public bool MrdImplantEvaluation { get; set; }
        public bool MrdCosmetic { get; set; }
        public bool MrdInfection { get; set; }
        public bool MrdSoftGrafting { get; set; }
        public bool MrdOrthognathicSurgery { get; set; }
        public bool MrdTrauma { get; set; }
        public bool MrdPDFrenectomy { get; set; }
        public bool MrdGingivalContouring { get; set; }
        public bool MrdGuidedRegeneration { get; set; }
        public bool MrdPDImplantEvaluation { get; set; }
        public bool MrdPeriodontalEvaluation { get; set; }
        public bool MrdRootGraft { get; set; }
        public bool MrdEndodontictreatment { get; set; }
        public bool MrdRetreatment { get; set; }
        public bool MrdClassII { get; set; }
        public bool MrdClassIII { get; set; }
        public bool MrdDeepbite { get; set; }
        public bool MrdCrossbite { get; set; }
        public bool MrdCrowding { get; set; }
        public bool MrdExcessiveoverjet { get; set; }
        public bool MrdImpactedteeth { get; set; }
        public bool MrdMissingteeth { get; set; }
        public bool MrdOpenbite { get; set; }
        public bool MrdODTMJ { get; set; }
        public bool MrdBridge { get; set; }
        public bool MrdComplete { get; set; }
        public bool MrdCrown { get; set; }
        public bool MrdFullmouth { get; set; }
        public bool MrdImmediate { get; set; }
        public bool MrdPTImplantEvaluation { get; set; }
        public bool MrdInlay { get; set; }
        public bool MrdOnlay { get; set; }
        public bool MrdTMJOcclusal { get; set; }
        public bool MrdVeneer { get; set; }
        public bool MrdPediatricdentistry { get; set; }
        public bool MrdChippedfractured { get; set; }
        public bool MrdPDCrown { get; set; }
        public bool MrdParafunctional { get; set; }
        public bool MrdOrthodontic { get; set; }
        public bool MrdPDMissingteeth { get; set; }
        public bool MrdSpecialneeds { get; set; }
        public bool MrdRootcanal { get; set; }
        public bool MrdRecentrestoration { get; set; }
        public bool rdoQue11aFixedbridge { get; set; }
        public bool rdoQue11bRemoveablebridge { get; set; }
        public bool rdoQue11cDenture { get; set; }
        public bool rdQue11dImplant { get; set; }
        public bool chkQue20_1 { get; set; }
        public bool chkQue20_2 { get; set; }
        public bool chkQue20_3 { get; set; }
        public bool chkQue20_4 { get; set; }
        public bool MrdQueShortness { get; set; }
        public bool MrdQueUnexplained { get; set; }
        public bool MrdHybrid { get; set; }
    }
    public class PatinentInfo
    {
        [Required(ErrorMessage = "First name is required.")]
        [Display(Name = "First Name")]
        [StringLength(30, ErrorMessage = "First name must not exceed 30 characters.")]
        public string FirstName { get; set; }

      //  [Required(ErrorMessage = "Last name is required.")]
        [StringLength(30, ErrorMessage = "Last name must not exceed 30 characters.")]
        public string LastName { get; set; }

        //[Required(ErrorMessage = "Email address is required.")]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Invalid email address.")]
        //SBI:38
        public string Email { get; set; }

        [Required(ErrorMessage = "Phone number is required.")]
        [Display(Name = "Mobile Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = " ")]
        public string Phone { get; set; }
        public int PatientId { get; set; }
        public string Password { get; set; }
        public int UserId { get; set; }
    }
    public class ServicesValues
    {
        public string txtExtract { get; set; }
        public string txtLesion { get; set; }
        public string txtOtherteeth { get; set; }
        public string txtRadiologyteeth { get; set; }
        public string txtCosmetic { get; set; }
        public string txtInfection { get; set; }
        public string txtSoftGrafting { get; set; }
        public string txtDentalLabteeth { get; set; }
        public string txtBehavoral { get; set; }
        public string txtPediatricsOther { get; set; }
        public string txtPulpTherapy { get; set; }
        public string txtPeriodonticCondition { get; set; }
        public string txtOrthodonticEvaluation { get; set; }
        public string txtOralHabits { get; set; }
        public string txtMissingTeeth { get; set; }
        public string txtGrowth { get; set; }
        public string txtFracturedTeeth { get; set; }
        public string txtExtractions { get; set; }
        public string txtCaries { get; set; }
        public string txtEndodonticsOther { get; set; }
        public string txtNitrous { get; set; }
        public string txtPost_core { get; set; }
        public string txtProvidePostSpace { get; set; }
        public string txtEvaluateforMicrosurgery { get; set; }
        public string txtTreatasNecessary { get; set; }
        public string txtDiagnosticConsultation { get; set; }
        public string txtOrthodonticsOther { get; set; }
        public string txtRetainerNeeded { get; set; }
        public string txtTMD { get; set; }
        public string txtConsultation { get; set; }
        public string txtPeriodontalHistory { get; set; }
        public string txtPeriodonticsOther { get; set; }
        public string txtFrenum { get; set; }
        public string txtMucogingival { get; set; }
        public string txtGingival { get; set; }
        public string txtImplantConsultation { get; set; }
        public string txtCrownDental { get; set; }
        public string txtLimited { get; set; }
        public string txtRidge { get; set; }
        public string txtBoneRegeneration { get; set; }
        public string txtSinus { get; set; }
        public string txtImplantSite { get; set; }
        public string txtALLOn4 { get; set; }
        public string txtImplantPlacement { get; set; }
        public string txtSoftTissue { get; set; }
        public string txtBoneGuided { get; set; }
        public string txtCrownSurgical { get; set; }
        public string txtOsseous { get; set; }
        public string txtLANAP { get; set; }
        public string txtOcclusal { get; set; }
        public string txtPeriodontal { get; set; }
        public string txtScaling { get; set; }
        public string txtSnoring { get; set; }
        public string txtPathology { get; set; }
        public string txtFacialAesthetics { get; set; }
        public string txtFacialReconstruction { get; set; }
        public string txtTMJ { get; set; }
        public string txtJawSurgery { get; set; }
        public string txtWisdomTeeth { get; set; }
        public string txtOther { get; set; }
        public string txtBoneGrafting { get; set; }
        public string txtExpose { get; set; }
        public string txtBiopsy { get; set; }
        public string txtExposure { get; set; }
        public string txtIncision { get; set; }
        public string txtExtractionMolar { get; set; }
        public string txtFrenectomy { get; set; }
        public string txtImplantEvaluation { get; set; }
        public string txtOrthognathicSurgery { get; set; }
        public string txtTrauma { get; set; }
        public string txtPRFrenectomy { get; set; }
        public string txtGingivalContouring { get; set; }
        public string txtGuidedRegeneration { get; set; }
        public string txtPRImplantEvaluation { get; set; }
        public string txtPeriodontalEvaluation { get; set; }
        public string txtRootGraft { get; set; }
        public string txtEndodonticevaluation { get; set; }
        public string txtRetreatment { get; set; }
        public string txtRootinitiated { get; set; }
        public string txtRecentrestoration { get; set; }
        public string txtClassII { get; set; }
        public string txtClassIII { get; set; }
        public string txtDeepbite { get; set; }
        public string txtCrossbite { get; set; }
        public string txtCrowding { get; set; }
        public string txtExcessiveoverjet { get; set; }
        public string txtImpactedteeth { get; set; }
        public string txtMissingteeth { get; set; }
        public string txtOpenbite { get; set; }
        public string txtODTMJ { get; set; }
        public string txtBridge { get; set; }
        public string txtComplete { get; set; }
        public string txtCrown { get; set; }
        public string txtFullmouth { get; set; }
        public string txtImmediate { get; set; }
        public string txtPDImplantEvaluation { get; set; }
        public string txtInlay { get; set; }
        public string txtOnlay { get; set; }
        public string txtTMJOcclusal { get; set; }
        public string txtVeneer { get; set; }
        public string txtPediatricdentistry { get; set; }
        public string txtChipped { get; set; }
        public string txtPDCrown { get; set; }
        public string txtParafunctional { get; set; }
        public string txtOrthodonticevaluation { get; set; }
        public string txtPDMissingteeth { get; set; }
        public string txtSpecialneeds { get; set; }
        public string MtxtHowoften { get; set; }
         
        public bool txtQue24 { get; set; }
        public bool DhQue11 { get; set; }
    }

  
}
