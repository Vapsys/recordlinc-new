﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// This model used for Filter colleagues list on OCR.
    /// </summary>
    public class FilterColleagues
    {
        /// <summary>
        /// Page Index By default = 1
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// Page size by default = 50
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// Search text
        /// </summary>
        public string SearchText { get; set; }
        /// <summary>
        /// Sort Column by default = 12 (LastName)
        /// </summary>
        public int SortColumn { get; set; }
        /// <summary>
        /// Sort direction by default = 2
        /// </summary>
        public int SortDirection { get; set; }
        /// <summary>
        /// Filter by used for Location drop-down.
        /// </summary>
        public int FilterBy { get; set; }
        /// <summary>
        /// New Search text used for A-Z filter
        /// 
        /// </summary>
        public string NewSearchText { get; set; }
        /// <summary>
        /// Id contain as UserId
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Check Request is come form Dashboard
        /// </summary>
        public bool IsDashboard { get; set; }
        /// <summary>
        /// access_token of user which is login on OCR.
        /// </summary>
        public string access_token { get; set; }
    }
}
