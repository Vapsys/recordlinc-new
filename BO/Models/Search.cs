﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BO.Models
{
    public class Search
    {
        public string fname { get; set; }
        public string LastName { get; set; }
        public string keywords { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Phone { get; set; }
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int Miles { get; set; }
        public string ZipCode { get; set; }
        public string Speciality { get; set; }
        public int Count { get; set; }
        public string[] SpecialityList { get; set; }
        public List<SelectListItem> lst { get; set; }
        public bool isFrom { get; set; }
        public string FilterBy { get; set; }

    }
}
