﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// SMS History Details
    /// </summary>
    public class SMSDetails
    {
        /// <summary>
        /// Primary Key
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// User Id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// SMS Integration Id
        /// </summary>
        public int SMSIntegrationId { get; set; }
        /// <summary>
        /// SenderId either PatientId or UserId
        /// </summary>
        public int SenderId { get; set; }
        /// <summary>
        /// Sender Type either Patient / Doctor
        /// </summary>
        public string SenderType { get; set; }
        /// <summary>
        /// SMS Integration Master Id
        /// </summary>
        public int SMSIntegrationMasterId { get; set; }
        /// <summary>
        /// Receiver Id
        /// </summary>
        public int ReceiverId { get; set; }
        /// <summary>
        /// Receiver Type
        /// </summary>
        public string ReceiverType { get; set; }
        /// <summary>
        /// Recepient Number
        /// </summary>
        public string RecepientNumber { get; set; }
        /// <summary>
        /// Message Body
        /// </summary>
        public string MessageBody { get; set; }
        /// <summary>
        /// Date Created
        /// </summary>
        public DateTime DateCreated { get; set; }
        /// <summary>
        /// Status
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// Retry Count
        /// </summary>
        public int RetryCount { get; set; }
        /// <summary>
        /// Date Sent
        /// </summary>
        public DateTime DateSent { get; set; }
        /// <summary>
        /// Response Body
        /// </summary>
        public string ResponseBody { get; set; }
        /// <summary>
        /// Message Type
        /// </summary>
        public int MessageType { get; set; }
    }
}
