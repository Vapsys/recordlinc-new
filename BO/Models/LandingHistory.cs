﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class LandingHistory
    {
        public LandingHistory()
        {
            List<LandingHistory> lstLandingHistory = new List<LandingHistory>();
        }

        public int LanHistoryId { get; set; }
        public string Email { get; set; }
        public string LandingPageName { get; set; }
        public DateTime CreatedDate { get; set; }
        public List<LandingHistory> lstLandingHistory { get; set; }
    }
}
