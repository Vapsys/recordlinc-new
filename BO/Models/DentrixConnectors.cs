﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class DentrixConnectors
    {
        public int DentrixConnectorId { get; set; }
        public string DentrixConnectorKey { get; set; }
        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public int AccountId { get; set; }
    }
}
