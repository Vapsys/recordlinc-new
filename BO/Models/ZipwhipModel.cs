﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BO.Models
{
    /// <summary>
    /// call zipwhip api class
    /// </summary>
    public class ZipwhipModel
    {
        /// <summary>
        /// initialize class constructor 
        /// </summary>
        public ZipwhipModel()
        {
            api_key = ConfigurationSettings.AppSettings["ZIPWHIP_APIKEY"];
            parameter = new ParameterModel();
        }
        /// <summary>
        /// zipwhip url
        /// </summary>
        public string zipwhip_url { get; set; }
        /// <summary>
        /// method like get,post,put
        /// </summary>
        public string api_method { get; set; }
        /// <summary>
        /// zipwhip api key
        /// </summary>
        public string api_key { get; set; }
        /// <summary>
        /// call api required parameter
        /// </summary>
        public ParameterModel parameter { get; set; }
    }

    /// <summary>
    /// required parameter class
    /// </summary>
    public class ParameterModel
    {
        //Check eligible or not
        /// <summary>
        /// zipwhip phone number
        /// </summary>
        public string phone_number { get; set; }
        /// <summary>
        /// provision api key
        /// </summary>
        public string api_key { get; set; }

        // User Login
        /// <summary>
        /// login user name for zipwhip account
        /// </summary>
        public string username { get; set; }
        /// <summary>
        /// password for access zipwhip account
        /// </summary>
        public string password { get; set; }

        //Message Send
        /// <summary>
        /// provide sessions key
        /// </summary>
        public string session { get; set; }
        /// <summary>
        /// contacts number
        /// </summary>
        public string contacts { get; set; }
        /// <summary>
        /// message body
        /// </summary>
        public string body { get; set; }

        //Add provisioning Account
        //public string api_key { get; set; }
        //public string phone_number { get; set; }

        /// <summary>
        /// zipwhip account name
        /// </summary>
        public string account_name { get; set; }
        /// <summary>
        /// zipwhip email id
        /// </summary>
        public string email { get; set; }
        /// <summary>
        /// feature package key
        /// </summary>
        public string feature_package { get; set; }
        /// <summary>
        /// effective date
        /// </summary>
        public string effective_date { get; set; }
        /// <summary>
        /// vendor customer id
        /// </summary>
        public string vendor_customer_id { get; set; }

        //Add Webhook
        /*public string session { get; set; }*/
        /// <summary>
        ///  webhook type like send,receive
        /// </summary>
        public string @event { get; set; }
        /// <summary>
        /// webhook url
        /// </summary>
        public string url { get; set; }
        /// <summary>
        /// api method like get,post,put
        /// </summary>
        public string method { get; set; }
        

    }

    /// <summary>
    /// provider config json class
    /// </summary>
    public class ProviderConfig
    {
        /// <summary>
        /// zipwhip account name
        /// </summary>
        public string accountname { get; set; }
        /// <summary>
        /// zipwhip email id
        /// </summary>
        public string email { get; set; }
        /// <summary>
        /// provide sessions key
        /// </summary>
        public string session { get; set; } // this is token for access api
        /// <summary>
        /// login user name for zipwhip account
        /// </summary>
        public string username { get; set; } // phonenumber
        /// <summary>
        /// password for access zipwhip account
        /// </summary>
        public string password { get; set; }
        /// <summary>
        /// provision api key
        /// </summary>
        public string api_key { get; set; }
        /// <summary>
        /// provision feature_package
        /// </summary>
        public string feature_package { get; set; }
        /// <summary>
        /// Webhook Id
        /// </summary>
        public int webhookId { get; set; }
     
        
    }

    /// <summary>
    /// Provisioning Account response property
    /// </summary>
    public class ZipwhipAccount
    {
        /// <summary>
        /// initialize class constructor 
        /// </summary>
        public ZipwhipAccount()
        {
            LocationList = new List<LocationList>();
            ProviderConfig = new ProviderConfig();
        }
        /// <summary>
        /// sms integration primary key
        /// </summary>
        public int RefId { get; set; }
        /// <summary>
        /// dentist id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// zipwhip provider config json details
        /// </summary>
        public string ProviderConfigJson { get; set; }
        /// <summary>
        /// location name
        /// </summary>
        public string Location { get; set; }
        /// <summary>
        /// location id
        /// </summary>
        public int LocationId { get; set; }
        /// <summary>
        /// Provider Config json class
        /// </summary>
        public ProviderConfig ProviderConfig { get; set; }
        /// <summary>
        /// account is eligible or not true-false 
        /// </summary>
        public bool IsEligible { get; set; }

        /// <summary>
        /// account is active or not
        /// </summary>
        public bool IsActive { get; set; }

        //public string AccountName { get; set; }
        //public string Email { get; set; }
        //public string Session { get; set; }
        //public string ZipwhipNumber { get; set; }
        //public string Api_Key { get; set; }

        /// <summary>
        /// location listing base on dentist id
        /// </summary>
        public List<LocationList> LocationList { get; set; }
    }


    public class ZipwhipWebhookResponse
    {
        public string body { get; set; }
        public int bodySize { get; set; }
        public bool visible { get; set; }
        public bool hasAttachment { get; set; }
        public object dateRead { get; set; }
        public object bcc { get; set; }
        public string finalDestination { get; set; }
        public string messageType { get; set; }
        public bool deleted { get; set; }
        public int statusCode { get; set; }
        public long id { get; set; }
        public object scheduledDate { get; set; }
        public string fingerprint { get; set; }
        public int messageTransport { get; set; }
        public long contactId { get; set; }
        public string address { get; set; }
        public bool read { get; set; }
        public DateTime dateCreated { get; set; }
        public object dateDeleted { get; set; }
        public object dateDelivered { get; set; }
        public object cc { get; set; }
        public string finalSource { get; set; }
        public int deviceId { get; set; }
    }


    public class LocationList
    {
        public int AddressInfoId { get; set; }
        public int UserId { get; set; }
        public int UserType { get; set; }
        public string ExactAddress { get; set; }
        public string Location { get; set; }
        public int ContactType { get; set; }
    }

    ///// <summary>
    ///// class for SMSHistory table
    ///// </summary>
    //public class SMSHistory
    //{
    //    /// <summary>
    //    /// primary id
    //    /// </summary>
    //    public int Id { get; set; }
    //    public int SMSIntegrationId { get; set; }
    //    public int UserId { get; set; }
    //    public string SenderType { get; set; }
    //    public int SMSIntegrationMasterId { get; set; }
    //    public int ReceiverId { get; set; }
    //    public string ReceiverType { get; set; }
    //    public string RecepientNumber { get; set; }
    //    public int MessageType { get; set; }
    //    public string MessageBody { get; set; }
    //    public string ResponseBody { get; set; }
    //    public string DateCreated { get; set; }
    //    public int Status { get; set; }
    //    public int RetryCount { get; set; }
    //    public DateTime DateSent { get; set; }
    //}
}
