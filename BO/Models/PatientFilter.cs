﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BO.Models
{
    public class PatientFilter
    {

        /// <summary>
        /// User id of the dentist
        /// </summary>
        [Required]
        public int UserId { get; set; }

        /// <summary>
        /// Page number whose records are obtained via API call
        /// </summary>
        public int PageIndex { get; set; }

        //SBI-34 related changes.
        /// <summary>
        /// Number of records per page to be obtained via API call
        /// </summary>
        [Range(1, 1500, ErrorMessage = "Please enter a value bigger than 1 and less than 1500")]
        public int PageSize { get; set; }

        /// <summary>
        /// Search text need to search
        /// </summary>
        public string SearchText { get; set; }


        /// <summary>
        /// Searches on Firstname of the user
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Searches on  Lastname of the user
        /// </summary>
        public string Lastname { get; set; }

        /// <summary>
        ///  Searches on Email Id of the user
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Not used in SmileBrands/GetPatientList
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Accepts intger value on which data is being sorted
        /// 1.  FirstName
        /// 2.  LastName
        /// 7.  AssignedPatientId
        /// 9.  Phone
        /// 10. Email
        /// 11. FirstName LastName
        /// 12. Patient ID
        /// 14. Dentist FirstName
        /// </summary>
        public int SortColumn { get; set; }

        /// <summary>
        /// Direction of sorting 1 for ascending or 2 for descending
        /// </summary>
        public int SortDirection { get; set; }

        /// <summary>
        ///To be used with FromDate paramenter. Searches within specfied range of patient creation date. Accepts From Date in UTC format 
        /// </summary>
        public DateTime? FromDate { get; set; }

        /// <summary>
        /// To be used with ToDate paramenter.Searches within specfied range of patient creation date. Accepts To Date in UTC format 
        /// </summary>
        public DateTime? ToDate { get; set; }

        /// <summary>
        ///  Not used in SmileBrands/GetPatientList
        /// </summary>
        public string NewSearchtext { get; set; }

        /// <summary>
        ///  Not used in SmileBrands/GetPatientList
        /// </summary>
        public string FilterBy { get; set; }

        /// <summary>
        /// Not used in SmileBrands/GetPatientList
        /// </summary>
        public string LocationBy { get; set; }

        /// <summary>
        ///Not used in SmileBrands/GetPatientList
        /// </summary>
        public bool ExcludeFamilyMembers { get; set; }

        /// <summary>
        ///Not used in SmileBrands/GetPatientList
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        ///Not used in SmileBrands/GetPatientList
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Location id of the user you can found it on GetDentistForOrganization API method. 
        /// We are return LocationId on that API method.
        /// </summary>
        public int LocationId { get; set; }
        /// <summary>
        ///Not used in SmileBrands/GetPatientList
        /// </summary>
        public APIFilter.FilterFlag sourcetype { get; set; }

        /// <summary>
        ///Not used in SmileBrands/GetPatientList
        /// </summary>
        public bool IsDashboard { get; set; }

        /// <summary>
        ///Not used in SmileBrands/GetPatientList
        /// </summary>
        public DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// RequestId use only with pagination. Not required in the first call
        /// </summary>
        public string RequestId { get; set; }
    }
    /// <summary>
    /// 1.  FirstName
    /// 2.  LastName
    /// 7.  AssignedPatientId
    /// 9.  Phone
    /// 10. Email
    /// 11. FirstName LastName
    /// 12. Patient ID
    /// 14. Dentist FirstName
    /// </summary>
    public enum PatientSortColumn
    {
        FirstName = 1,
        LastName = 2,
        AssignedPatientId = 7,
        Phone = 9,
        Email =10,
        FullName =11,
        PatientID=12,
        DentistFirstName=14
    }
}
