﻿namespace BO.Models
{
    public class UpdateDisposition
    {
        public int MessageId { get; set; }
        public int UserId { get; set; }
        public int DispositionId { get; set; }
        public string Note { get; set; }
        public bool VisibleToPatient { get; set; }
        public bool VisibleToDoctor { get; set; }
        public string LoginURL { get; set; }
        public string ScheduledDate { get; set; }
    }
}
