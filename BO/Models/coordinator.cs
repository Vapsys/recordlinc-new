﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class Coordinator
    {
        public int MessageId { get; set; }
        public string MemberId { get; set; }
    }
}
