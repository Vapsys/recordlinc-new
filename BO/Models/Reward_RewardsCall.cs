﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BO.Models
{
    public class Reward_RewardsCall
    {
        [Required(ErrorMessage = "RewardPartnerId is required.")]
        public int RewardPartnerId { get; set; }
        //Added Account Id required as comment as per Bruce change request PRCSP-459
        //[Required(ErrorMessage = "AccountId is required.")]
        public int AccountId { get; set; }
        public int Pageindex { get; set; }
        public int PageSize { get; set; }
        public DateTime? Startdate { get; set; }
        public DateTime? Enddate { get; set; }
        //[StringRange(AllowableValues = new[] { "M", "F" }, ErrorMessage = "Gender must be either 'M' or 'F'.")]
        [Range(minimum:1,maximum:2, ErrorMessage = "RewardType must be either 1 or 2.")]
        public int RewardType { get; set; }
        public APIFilter.FilterFlag sourcetype { get; set; }
    }
}
