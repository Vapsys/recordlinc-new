﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class MessageSendTwillio
    {
        public string Message { get; set; }
        public string UserName { get; set; }
        public string Phone { get; set; }
        public string IsFrom { get; set; }
    }
}
