﻿using System;
using BO.Enums;
namespace BO.Models
{
    public class PatientPayments
    {
        public PatientPayments()
        {
            Type = Common.PatientPaymentType.All;
            DateFilterBy = Common.PatientProcedureDateFilter.ProcedureDate;
        }

        /// <summary>
        /// Define the type of Dentrix payment 
        /// <summary>
        /// <para>RequestType 0 = All (Works only when called API without RequestId)</para>
        /// <para>RequestType 1 = Finance</para>
        /// <para>RequestType 2 = Adjustment</para>
        /// <para>RequestType 3 = Insurance</para>
        /// <para>RequestType 4 = Standard</para>
        /// </summary>
        /// </summary>
        public Common.PatientPaymentType Type { get; set; }
        /// <summary>
        /// Id of the patient associated with dentist. Do not use it while calling with RequestID.
        /// </summary>
        [ValidateInputsName]
        public int PatientId { get; set; }

        /// <summary>
        ///To be used with ToDate paramenter. Searches within specfied date range of column specified in DateFilterBy attribute. Accepts From Date in UTC format. Do not use it while calling with RequestID.
        /// </summary>
        public DateTime? FromDate { get; set; }
        /// <summary>
        ///To be used with FromDate paramenter. Searches within specfied date range of column specified in DateFilterBy attribute.Accepts From Date in UTC format. Do not use it while calling with RequestID.
        /// </summary>
        public DateTime? ToDate { get; set; }
        /// <summary>
        /// Order need to sort for column Insert/Update date of PMS. 1 - Ascending and 2-Descending.
        /// </summary>
        public string Order { get; set; }

        /// <summary>
        /// Id of Location associated with Dentist from which need to fetch payment records.Do not use it while calling with RequestID.
        /// </summary>
        public int LocationId { get; set; }

        /// <summary>
        /// Id of specific request when it is called first time.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Page number whose record being obtained via API Call.
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// Number of records per page obtained via API Call.
        /// </summary>
        public int FetchRecordCount { get; set; }

        /// <summary>
        /// Provide option to filter FromDate and ToDate based on selected filter. Default value is 0 (ProcedureDate)
        /// </summary>
        public Common.PatientProcedureDateFilter DateFilterBy { get; set; }
    }
}
