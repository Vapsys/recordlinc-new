﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BO.Models
{
    public class Ratings
    {
        public int RatingId { get; set; }
        public int ObjectId { get; set; }

        [Required(ErrorMessage = "Rating is required.")]
        [Range(0.5, 5, ErrorMessage = "Rating must between 0.5 to 5")]
        public double RatingValue { get; set; }
        public int? SenderId { get; set; }

        [Display(Name = "Your Name")]
        [Required(ErrorMessage = "Name is required")]
        public string SenderName { get; set; }

        [Display(Name = "Email address")]
        [Required(ErrorMessage = "The email address is required")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        public string SenderEmail { get; set; }

        [Display(Name = "Your Message")]
        [Required(ErrorMessage = "Message is required.")]
        [StringLength(2000, ErrorMessage = "Message must not exceed 2000 characters")]
        public string RatingMessage { get; set; }

        public int ReceiverId { get; set; }
        public DateTime CreatedDate { get; set; }
        public int? CreatedById { get; set; }
        public string CreatedSource { get; set; }
        public int Status { get; set; }
        public bool IsCheck { get; set; }

        public Ratings()
        {
            RatingValue = 5;
        }

    }
}
