﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// Zipwhip WebHook Received Message time use this class.
    /// </summary>
    public class ZipwhipReceiver
    {
        /// <summary>
        /// Message Body
        /// </summary>
        public string body { get; set; }
        /// <summary>
        /// Body Size
        /// </summary>
        public int bodySize { get; set; }
        /// <summary>
        /// Visibility
        /// </summary>
        public bool visible { get; set; }
        /// <summary>
        /// It has attachment
        /// </summary>
        public bool hasAttachment { get; set; }
        /// <summary>
        /// Date read
        /// </summary>
        public DateTime? dateRead { get; set; }
        /// <summary>
        /// BCC
        /// </summary>
        public object bcc { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string finalDestination { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string messageType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool deleted { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int statusCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public object scheduledDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string fingerprint { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int messageTransport { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long contactId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string address { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool read { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime dateCreated { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public object dateDeleted { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public object dateDelivered { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public object cc { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string finalSource { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int deviceId { get; set; }
    }
}
