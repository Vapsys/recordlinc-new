﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class OneClickReferralDetails
    {
        public string ReferralDate { get; set; }
        public string DoctorName { get; set; }
        public string PatientName { get; set; }
        public int? DispositionId { get; set; }
        public int ReceiverId { get; set; }
        public int UserId { get; set; }
        public int PatientId { get; set; }
        public int MessageId { get; set; }
        public string Email { get; set; }
        public string DOB { get; set; }
        public string DentrixId { get; set; }
        public string PhoneNumber { get; set; }
        public List<Disposition> DispositionList { get; set; }
        public string ProfileImage { get; set; }
        public int DoctorId { get; set; }
        public string Gender { get; set; }
        public string ReferringDoctorPhone { get; set; }
        public string ReferringDoctorEmail { get; set; }
        public string MessageBody { get; set; }
        public string MessageStatus { get; set; }
        public string MessageFrom { get; set; }
        public int TotalCount { get; set; }
        public int ReferralCardId { get; set; }
        //--XQ1-326 - Added following type to identify system generated message
        public int Status { get; set; } // Status : 2 means system generated message
        public string Subject { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreationDateString { get; set; }
        public string Note { get; set; }
        public string BoxFolderId { get; set; }
        public string BoxFileId { get; set; }
    }

    public class ReferralFilter
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int SortDirection { get; set; }
        public int SortColumn { get; set; }
        public string access_token { get; set; }
        public string DoctorName { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public int Disposition { get; set; }
        public string EncryptedUserId { get; set; }
        public int MessageStatus { get; set; }
        public string Message { get; set; }
        public string PatientName { get; set; }
        public int UserId { get; set; }
        public int ReferralFrom { get; set; }
    }

    public class ZiftPayFilter
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int SortDirection { get; set; }
        public int SortColumn { get; set; }
        public int AccountId { get; set; }
        public int UserId { get; set; }
    }

    public class OneClickReferralDetail
    {
        public string ReferralDate { get; set; }
        public string ReceiverDoctorName { get; set; }
        public string SenderDoctorName { get; set; }
        public string PatientName { get; set; }
        public int? DispositionId { get; set; }
        public int ReceiverId { get; set; }
        public int UserId { get; set; }
        public int PatientId { get; set; }
        public bool PatientStatus { get; set; }
        public int MessageId { get; set; }
        public string Email { get; set; }
        public string DOB { get; set; }
        public string DentrixId { get; set; }
        public string PhoneNumber { get; set; }
        public List<Disposition> DispositionList { get; set; }
        public List<ReferralCategory> lstCategory { get; set; }
        public string ProfileImage { get; set; }
        public int DoctorId { get; set; }
        public string Gender { get; set; }
        public string ReferringDoctorPhone { get; set; }
        public string ReferringDoctorEmail { get; set; }
        public string MessageBody { get; set; }
        public string MessageStatus { get; set; }
        public string MessageFrom { get; set; }
        public int MessageTypeId { get; set; }
        public int TotalCount { get; set; }
        public int ReferralCardId { get; set; }
        //--XQ1-326 - Added following type to identify system generated message
        public int Status { get; set; } // Status : 2 means system generated message
        public string Subject { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreationDateString { get; set; }
        public string Note { get; set; }
        public string BoxFolderId { get; set; }
        public string BoxFileId { get; set; }
        public string BoxFileName { get; set; }
        public int MemberId{ get; set; }
        public string TeamMemberName { get; set; }
        public int SenderId { get; set; }
    }
}
