﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BO.Models
{
    public class Banner
    {
        public int BannerId { get; set; }
        public int UserId { get; set; }
        [Display(Name = "Title")]
        [Required(ErrorMessage = "Please enter title")]
        public string Title { get; set; }
        [Display(Name = "Banner Image")]
        [Required(ErrorMessage = "Please choose image")]
        public string Path { get; set; }
        public string ImagePath { get; set; }
        public HttpPostedFileBase BannerImage { get; set; }
        [Display(Name = "Color")]
        [Required(ErrorMessage = "Please choose color")]
        public string ColorCode { get; set; }
        public int Position { get; set; }
    }
}
