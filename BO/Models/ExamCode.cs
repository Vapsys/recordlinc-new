﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
  public class ProcedureExamCode
  {
        public int ExamId { get; set; }
        public int AccountId { get; set; }
        public string ProcedureName { get; set; }
        public int ExamCode { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
    }
}
