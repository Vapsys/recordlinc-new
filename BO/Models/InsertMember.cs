﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BO.Models
{
    public class InsertMember
    {
        [Required(ErrorMessage = "ProviderId is required.")]
        public object ProviderId { get; set; }

        public int ProviderType { get; set; }
        public int ProviderClass { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string MiddleName { get; set; }
        public string Title { get; set; }
        public List<Addresses> Addresses { get; set; }
        public List<PhoneNumbers> phonenumbers { get; set; }
        public List<string> EmailAdresses { get; set; }
        public bool IsNonPerson { get; set; }
        public bool IsInactive { get; set; }
        [Required(ErrorMessage = "DentrixId is required.")]
        public int DentrixId { get; set; }
        public int Identifier { get; set; }

        public string Specialty { get; set; }
        public DateTime? automodifiedtimestamp { get; set; }
        public string dentrixConnectorID { get; set; }
    }

    public class Addresses
    {
        public string DentrixId { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
    }

    public class PhoneNumbers
    {
        public int PhoneType { get; set; }
        public string PhoneNumber { get; set; }
    }    
}
