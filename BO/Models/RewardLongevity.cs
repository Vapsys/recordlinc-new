﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
   public class RewardLongevity
    {
        public int LMId { get; set; }
        public int AccountId { get; set; }
        public int MonthsofLongevity { get; set; }
        public decimal Multiplier { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public int PlanId { get; set; }
    }
}
