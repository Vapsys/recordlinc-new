﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class Reward_ListOfPotential
    {
        public int? RewardPartnerId { get; set; }
        public double? TotalPotentialRewards { get; set; }
        public double? TotalActualRewards { get; set; }
        public List<RewardList> RewardsList { get; set; }

    }
    public class RewardList
    {
        public int Id { get; set; }
        public int RewardPatnerRewardId { get; set; }
        public string RewardCode { get; set; }
        public string RewardText { get; set; }
        public double Point { get; set; }
        public DateTime Date { get; set; }
        public int RewardType { get; set; }
    }

    public class PatientRewardList
    {
        public int Id { get; set; }
        //public int RewardPatnerRewardId { get; set; }
        public string RewardCode { get; set; }
        public string RewardText { get; set; }
        public double Point { get; set; }
        public DateTime Date { get; set; }
        public int RewardType { get; set; }
    }
    public class PatientReward_ListOfPotential
    {
        public int? SupPatientId { get; set; }
        public double? TotalPotentialRewards { get; set; }
        public double? TotalActualRewards { get; set; }
        public List<PatientRewardList> PatientRewardList { get; set; }
    }
}
