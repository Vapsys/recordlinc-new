﻿using System;

namespace BO.Models
{
    public class CallHistoryModel
    {
        public int CallHistoryId { get; set; }

        public DateTime SentAt { get; set; }

        public string Content { get; set; }

        public string PhoneNumber { get; set; }

        public BO.Enums.Common.EnumCallStatus STATUS { get; set; }

        public string APIMessage { get; set; }

        public int? APIStatus { get; set; }

        public int? APICode { get; set; }

        public string APIMoreInfo { get; set; }

        public string APISId { get; set; }

        public BO.Enums.Common.EnumCommunication CallType { get; set; }

        public int UserId { get; set; }

    }
}
