﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class PatientRewards
    {
        public int PatientRewardsId { get; set; }

        /// <summary>
        /// It is RL PatientId, this is require as we may have Rewards system in RL and in that case RewardPartnerId will be 0.
        /// </summary>
        public int PatientRewardsPatientId { get; set; }

        /// <summary>
        /// It is a Solution1 patient Id.
        /// </summary>
        public int PatientRewardsPartnerId { get; set; }

        /// <summary>
        /// It is DentistRewardsSettings.Id.
        /// </summary>
        public int PatientRewardsRewardId { get; set; }
        public double PatientRewardsPoints { get; set; }
        public string PatientRewardsSource { get; set; }
        public Enums.Common.DentistRewardsType PatientRewardsRewardtype { get; set; }                
        public DateTime PatientRewardsCreatedDate { get; set; }
        public DateTime PatientRewardsModifiedDate { get; set; }
        public int RewardPlatformRewardId { get; set; }
    }

    public class RewardsJson
    {
        public int RewardId { get; set; }
        public double RewardAmount { get; set; }
        public string RewardType { get; set; }
        public string RewardStatus { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class RewardBalanceJson
    {
        [Required(ErrorMessage = "RewardPartnerId is required.")]
        public int RewardPartnerId { get; set; }
        [Required(ErrorMessage = "RewardType is required.")]
        public string RewardType { get; set; }
        public APIFilter.FilterFlag sourcetype { get; set; }

    }
}
