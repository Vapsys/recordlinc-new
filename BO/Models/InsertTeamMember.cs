﻿namespace BO.Models
{
    /// <summary>
    /// Insert / Update TeamMember 
    /// </summary>
    public class InsertTeamMember
    {
        /// <summary>
        /// UserId of Team Member
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Email address
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// First Name
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// Last Name
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// OfficeName
        /// </summary>
        public string OfficeName { get; set; }
        /// <summary>
        ///  Exact Address
        /// </summary>
        public string ExactAddress { get; set; }
        /// <summary>
        /// Address2
        /// </summary>
        public string Address2 { get; set; }
        /// <summary>
        /// City
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// State
        /// </summary>
        public string State { get; set; }
        /// <summary>
        /// Country
        /// </summary>
        public string Country { get; set; }
        /// <summary>
        /// Zip-code
        /// </summary>
        public string ZipCode { get; set; }
        /// <summary>
        /// Specialty
        /// </summary>
        public string Specialty { get; set; }
        /// <summary>
        /// Password
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// LocationId
        /// </summary>
        public int LocationId { get; set; }
        /// <summary>
        /// ProviderId
        /// </summary>
        public string ProviderId { get; set; }
    }
}
