﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
  public class LocationDetail
    {
        public string LocationName { get; set; }
        public string LocationId { get; set; }
        public int ContactType { get; set; }
    }
}
