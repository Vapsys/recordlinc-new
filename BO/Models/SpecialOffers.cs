﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class SpecialOffers
    {
        public SpecialOffers()
        {
            lstOffers = new List<SpecialOffers>();
        }
        public int OfferId { get; set; }
        [Required(ErrorMessage = "Title is required")]
        public string Title { get; set; }
        [Required(ErrorMessage ="Description is required")]
        [StringLength(2000, ErrorMessage = "Description must not exceed 2000 characters")]
        public string Description { get; set; }
        [Display(Name ="Offer Code")]
        [Required(ErrorMessage ="Offer code is required")]
        public string Code { get; set; }
        [Display(Name ="Order")]
        [Required(ErrorMessage ="Display order is required")]
        [RegularExpression("([1-9][0-9]*)", ErrorMessage = "Display order must be a number")]
        public int? SortOrder { get; set; }
        public bool Status { get; set; }
        public string OfferStatus { get; set; }
        public int UserId { get; set; }
        public IList<SpecialOffers> lstOffers { get; set; }
    }
}
