﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BO.Models
{
    class RecurringPaymentHistory
    {
        public int payId { get; set; }
        public int memberId { get; set; }
        public string amount { get; set; }
        public string transactionId { get; set; }
        public string membershipId { get; set; }
        public string customerId { get; set; }
        public DateTime createdDate { get; set; }
        public string subscriptionId { get; set; }
        public string subscriptionType { get; set; }
        public bool IsActive { get; set; }
    }
}
