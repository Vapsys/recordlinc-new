﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class PatientInsurance
    {
        public int PatientInsuranceId { get; set; }
        public int PatientId { get; set; }
        public int InsuranceId { get; set; }
        public string GroupNumber { get; set; }
        public int InsuranceTypeId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? LastModified { get; set; }
        public PatientInsuranceType PatientInsuranceType { get; set; }
        public InsuranceCarrier InsuranceCarrier { get; set; }
        
    }

    public class PatientInsuranceAPI
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DOB { get; set; }
        public string InsuranceName { get; set; }
        public string GroupNumber { get; set; }
    }
}
