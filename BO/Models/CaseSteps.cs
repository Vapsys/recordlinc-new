﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BO.Models
{
    public class CaseSteps
    {
        public int CaseStepId { get; set; }

        public int? CaseId { get; set; }

        public int? StepOrder { get; set; }

        public string Description { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public int? UserId { get; set; }

        public bool? Status { get; set; }

        public DateTime? CreatedAt { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? ModifiedAt { get; set; }

        public string ModifiedBy { get; set; } 
        public string DoctorName { get; set; }
        
    }
}
