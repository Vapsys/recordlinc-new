﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class EventResponse
    {
        public string notificationId { get; set; }
        public string eventType { get; set; }
        public string eventDate { get; set; }
        public string webhookId { get; set; }
        public string responseBody { get; set; }
    }
}
