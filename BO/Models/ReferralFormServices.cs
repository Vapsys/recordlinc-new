﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class ReferralFormServices
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? IsEnable { get; set; }
        public int OrderBy { get; set; }
        public int FieldId { get; set; }
        public string NameFor { get; set; }
        public int SpecialityId { get; set; }
        public int ParentFieldId { get; set; }
        public List<ReferralFormSubField> lstReferralFormSubField { get; set; }
    }

    

    public class ReferralFormSubField
    {
        public int ParentFieldId { get; set; }
        public int SubFieldId { get; set; }
        public int SpecialityId { get; set; }
        public int UserId { get; set; }
        public string Name { get; set; }
        public bool? IsEnable { get; set; }
        public int OrderBy { get; set; }
        public int Id { get; set; }
    }
}
