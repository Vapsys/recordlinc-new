﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class ResponseAuth
    {
        public int notificationId { get; set; }
        public string eventType { get; set; }
        public DateTime eventDate { get; set; }
        public string webhookId { get; set; }
        public Payload payload { get; set; }
    }
    public class Payload
    {
        public int id { get; set; }
        public string entityName { get; set; }
        public string name { get; set; }
        public decimal amount { get; set; }
        public string status { get; set; }
        public Profile profile { get; set; }
    }
    public class Profile
    {
        public int customerProfileId { get; set; }
        public int customerPaymentProfileId { get; set; }
        public int customerShippingAddressId { get; set; }
    }
}
