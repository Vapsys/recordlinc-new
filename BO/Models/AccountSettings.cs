﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace BO.Models
{
    /// <summary>
    /// Account setting page model
    /// </summary>
    public class AccountSettings
    {
        /// <summary>
        /// Primary Location/Address Id which dentist has set.
        /// </summary>
        public int LocationId { get; set; }
        /// <summary>
        /// Stay logged in into OCR for how many hours
        /// </summary>
        public int StayLogin { get; set; }
        /// <summary>
        /// Username which helps to user while login
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// Primary Email address of Dentist
        /// </summary>
        public string PrimaryEmail { get; set; }
        /// <summary>
        /// Secondary Email address of Dentist
        /// </summary>
        public string SecondaryEmail { get; set; }
        /// <summary>
        /// New Password
        /// </summary>
        public string NewPassword { get; set; }
        /// <summary>
        /// Confrim password
        /// </summary>
        public string ConfrimPassword { get; set; }
        /// <summary>
        /// Email notification setting of Dentist want email notification or not.
        /// </summary>
        public bool Email { get; set; }
        /// <summary>
        /// Text notification setting of Dentist want Text notification or not.
        /// </summary>
        public bool Text { get; set; }
        /// <summary>
        /// Fax notification setting of Dentist want Fax notification or not.
        /// </summary>
        public bool Fax { get; set; }
        /// <summary>
        /// Location List of Dentist
        /// </summary>
        public List<SelectListItem> LocationList { get; set; }
        /// <summary>
        /// Stay logged in List for Dropdown
        /// </summary>
        public List<SelectListItem> StayLoginList { get; set; }
    }


    /// <summary>
    /// Save account settings details 
    /// </summary>
    public class SaveAccountSetting
    {
        /// <summary>
        /// Primary Location/Address Id which dentist has set.
        /// </summary>
        public int LocationId { get; set; }
        /// <summary>
        /// Stay logged in into OCR for how many hours
        /// </summary>
        public int StayLogin { get; set; }
        /// <summary>
        /// Username which helps to user while login
        /// </summary>
        public string Username { get; set; }
        /// <summary>
        /// Primary Email address of Dentist
        /// </summary>
        public string PrimaryEmail { get; set; }
        /// <summary>
        /// Secondary Email address of Dentist
        /// </summary>
        public string SecondaryEmail { get; set; }
        /// <summary>
        /// New Password
        /// </summary>
        public string NewPassword { get; set; }
        /// <summary>
        /// Confrim password
        /// </summary>
        public string ConfrimPassword { get; set; }
        /// <summary>
        /// Email notification setting of Dentist want email notification or not.
        /// </summary>
        public bool Email { get; set; }
        /// <summary>
        /// Text notification setting of Dentist want Text notification or not.
        /// </summary>
        public bool Text { get; set; }
        /// <summary>
        /// Fax notification setting of Dentist want Fax notification or not.
        /// </summary>
        public bool Fax { get; set; }
        /// <summary>
        /// Old Primary Email address
        /// </summary>
        public string OldPrimaryEmail { get; set; }
        /// <summary>
        /// Old Secondary Email address
        /// </summary>
        public string OldSecondaryEmail { get; set; }
        /// <summary>
        /// Old Location Id
        /// </summary>
        public int OldLocationId { get; set; }
        /// <summary>
        /// Old Username of Dentist
        /// </summary>
        public string OldUsername { get; set; }
    }
}
