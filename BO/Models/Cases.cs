﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class Cases
    {
        public int CaseId { get; set; }

        public int? PatientId { get; set; }

        public Patient objPatientdetail { get; set; }

        public int Status { get; set; }

        public bool? TreatmentAccepted { get; set; }

        public DateTime? TreatmentAcceptedDate { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string CreatedBy { get; set; }

        public string CaseName { get; set; }
        public List<CaseSteps> _caseStepList { get; set; }

    }
}
