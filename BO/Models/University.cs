﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// University name and id
    /// </summary>
    public class University
    {
        /// <summary>
        /// University Id
        /// </summary>
        public string id { get; set; }
        /// <summary>
        /// University Name
        /// </summary>
        public string text { get; set; }
    }
}
