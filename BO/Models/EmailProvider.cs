﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class EmailProvider
    {
        public int providerId { get; set; }
        public string providerName { get; set; }
        public DateTime createdAt { get; set; }
        public DateTime updatedAt  { get; set; }
        public bool Is_Deleted { get; set; }        
    }
}
