﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
  public class Operatory
    {
        public string OperatoryId { get; set; }
        [Required(ErrorMessage = "PMSId is required.")]
        public int DentrixId { get; set; }
        public string Title { get; set; }
        [Required(ErrorMessage = "DentrixConnectorId is required.")]
        public string DentrixConnectorId { get; set; }
        public int Identifier { get; set; }
        public DateTime? automodifiedtimestamp { get; set; }
    }

    public class DentistOperatory
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
