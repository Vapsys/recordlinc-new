﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class PatientDelete
    {
        public int RemovedCount { get; set; }
        public int UnRemovedCount { get; set; }
    }
}
