﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// Payer information
    /// </summary>
    public class Payer
    {
        /// <summary>
        /// payor identification
        /// </summary>
        public string payor_identification { get; set; }
        /// <summary>
        /// entity
        /// </summary>
        public string entity { get; set; }
        /// <summary>
        /// entity qualifier
        /// </summary>
        public string entity_qualifier { get; set; }
        /// <summary>
        /// Name of payor
        /// </summary>
        public string name { get; set; }
    }

    /// <summary>
    /// Provider information
    /// </summary>
    public class Provider
    {
        /// <summary>
        /// Provider/Dentist NPI ID
        /// </summary>
        public string npi { get; set; }
        /// <summary>
        /// entity
        /// </summary>
        public string entity { get; set; }
        /// <summary>
        /// entity qualifier
        /// </summary>
        public string entity_qualifier { get; set; }
        /// <summary>
        /// Provider code
        /// </summary>
        public string provider_code { get; set; }
        /// <summary>
        /// First name
        /// </summary>
        public string first_name { get; set; }
        /// <summary>
        /// Last name
        /// </summary>
        public string last_name { get; set; }
        /// <summary>
        /// name
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// reference
        /// </summary>
        public IList<Reference> reference { get; set; }
    }

    /// <summary>
    /// Reference Information
    /// </summary>
    public class Reference
    {
        public string tax_id { get; set; }
        public string group_number { get; set; }
        public string group_number_description { get; set; }
        public string description { get; set; }
        public string plan_network_identification_number { get; set; }
    }
    /// <summary>
    /// Date time Period
    /// </summary>
    public class DateTimePeriod
    {
        public string date_string { get; set; }
        public string date_qualifier { get; set; }
    }

    /// <summary>
    /// Subscriber Information
    /// </summary>
    public class Subscriber
    {
        /// <summary>
        /// entity
        /// </summary>
        public string entity { get; set; }
        /// <summary>
        /// entity qualifier
        /// </summary>
        public string entity_qualifier { get; set; }
        public string InformationReceiverAdditionalAddressLine { get; set; }
        public string dob { get; set; }
        public string gender { get; set; }
        public bool is_subscriber { get; set; }
        public string relationship { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public Reference reference { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip_code { get; set; }
        public IList<DateTimePeriod> date_time_periods { get; set; }
        public string member_id { get; set; }
    }

    public class ActiveCoverage
    {
        public string coverage_level { get; set; }
        public string insurance_type { get; set; }
        public string plan_coverage_description { get; set; }
        public IList<string> messages { get; set; }
    }

    public class CoInsurance
    {
        public string service_type { get; set; }
        public string in_network_status { get; set; }
        public string percentage_string { get; set; }
        public string percentage_for_provider { get; set; }
        public string percentage { get; set; }
        public string service_or_procedure_code { get; set; }
        public IList<Reference> reference { get; set; }
        public string plan_coverage_description { get; set; }
        public string procedure_code { get; set; }
    }

    public class Deductibles
    {
        public string in_network_status { get; set; }
        public string amount { get; set; }
        public string procedure_code { get; set; }
        public string service_or_procedure_code { get; set; }
        public IList<Reference> reference { get; set; }
        public string plan_coverage_description { get; set; }
        public string service_type { get; set; }
        public string time_qualifier { get; set; }
        public string coverage_level { get; set; }
    }

    public class HealthServicesDescription
    {
        public string description { get; set; }
    }

    public class AgeLimit
    {
        public string qualifier { get; set; }
        public string quantity { get; set; }
    }

    public class Limitation
    {
        public string service_type { get; set; }
        public string service_or_procedure_code { get; set; }
        public IList<HealthServicesDescription> health_services_description { get; set; }
        public string quantity_description_string { get; set; }
        public AgeLimit age_limit { get; set; }
        public IList<DateTimePeriod> date_time_periods { get; set; }
        public string procedure_code { get; set; }
    }

    public class Maximum
    {
        public string time_qualifier { get; set; }
        public string coverage_level { get; set; }
        public string service_type { get; set; }
        public string in_network_status { get; set; }
        public string amount { get; set; }
        public string service_or_procedure_code { get; set; }
        public IList<Reference> reference { get; set; }
        public string plan_coverage_description { get; set; }
    }

    public class One_ResponseInsu
    {
        public int PatientId { get; set; }
        public One_Response One_Response { get; set; }
    }

    public class One_Response
    {
        public IList<object> metadata { get; set; }
        public Payer payer { get; set; }
        public Provider provider { get; set; }
        public Subscriber subscriber { get; set; }
        public IList<ActiveCoverage> activeCoverage { get; set; }
        public IList<CoInsurance> coInsurance { get; set; }
        public IList<Deductibles> deductible { get; set; }
        public IList<Limitation> limitations { get; set; }
        public IList<Maximum> maximums { get; set; }
        public string onederfulId { get; set; }
        public dynamic StatusCode { get; set; }
    }
    /// <summary>
    /// Onederful Access_token
    /// </summary>
    public class AccessToken
    {
        /// <summary>
        /// access_token
        /// </summary>
        public string access_token { get; set; }
        /// <summary>
        /// expires in minutes
        /// </summary>
        public int expires_in { get; set; }
        /// <summary>
        /// Token Type
        /// </summary>
        public string token_type { get; set; }
    }
    public class InputModel
    {
        public GetSubscriber subscriber { get; set; }
        public GetProvider provider { get; set; }
        public GetPayer payer { get; set; }
    }

    public class GetSubscriber
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string dob { get; set; }
        public string memberId { get; set; }
        public string groupNumber { get; set; }
    }

    public class GetProvider
    {
        public string npi { get; set; }
    }

    public class GetPayer
    {
        public string id { get; set; }
    }
}
