﻿using System.Collections.Generic;

namespace BO.Models
{
    public class RewardResponse
    {
        public int ReceivedRecords { get; set; }
        public int TotalInsertedRecord { get; set; }
        public int TotalNotInsertedRecord { get; set; }
        public List<InsertedRecord> InsertedList { get; set; }
        public List<NotInsertedRecord> NotInsertedList { get; set; }
    }
    public class InsertedRecord
    {
        public dynamic DentrixId { get; set; }
        public int RLId { get; set; }
        public dynamic ProviderId { get; set; }
    }
    public class NotInsertedRecord
    {
        public dynamic DentrixId { get; set; }
        public string ErrorMessage { get; set; }
    }
    public class DeletedRecord
    {
        public int DentrixId { get; set; }
        public bool IsDeleted { get; set; }
    }
    public class DeletedProc
    {
        public int DentrixId { get; set; }
        public bool IsDeleted { get; set; }
    }
    public class DeletePatient
    {
        public int DentrixId { get; set; }
        public bool IsDeleted { get; set; }
    }
    public class OperatoryResponse
    {
        public int ReceivedRecord { get; set; }
        public int TotalInsertedRecords { get; set; }
        public int TotalNotInsertedRecords { get; set; }
        public List<InsertedOperRecords> InsertedList { get; set; }
        public List<NotInsertedOperRecords> NotInsertedList { get; set; }
    }

    public class InsertedOperRecords
    {
        public dynamic DentrixId { get; set; }
        public int RLIdNumber { get; set; }

    }

    public class NotInsertedOperRecords
    {
        public dynamic DentrixId { get; set; }
        public string OperatoryId { get; set; }
        public string ErrorMessage { get; set; }
    }

    public class DeletedOperatory
    {
        public int DentrixId { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class RetrieveListResponse<T>
    {
        public int StatusCode { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public List<T> Result { get; set; }
    }
    public class RetriveResponse<T>
    {
        public int StatusCode { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string RequestId { get; set; }
        public int TotalRecord { get; set; }
        public T Result { get; set; }
    }
    public class RetirveReward<T>
    {
        public int StatusCode { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }
    }
    public class UpdateResponse<T>
    {
        public int StatusCode { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }
    }
    public class SuperResponse<T>
    {
        public int StatusCode { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public int SupPatientId { get; set; }
        public double? TotalPotentialRewards { get; set; }
        public double? TotalActualRewards { get; set; }
        public T Result { get; set; }
    }
    public class OrganizationResponse<T>
    {
        public int StatusCode { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public int TotalRecord { get; set; }
        public T Result { get; set; }
    }
    public class APIResponse<T> 
    {
        public int StatusCode { get; set; }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }
    }
    public class Pageing<T> : APIResponse<T>
    {
        public int TotalRecord { get; set; }
        public int NumberOfPages { get; set; }
        public int PageIndex { get; set; }
    }
    /// <summary>
    /// This class used for Insert Procedure time return number of records are inserted and not inserted count.
    /// </summary>
    public class BulkResponse
    {
        /// <summary>
        /// No of count we received.
        /// </summary>
        public int ReceivedRecords { get; set; }
        /// <summary>
        /// No of count we are able to Inserted.
        /// </summary>
        public int TotalInsertedRecord { get; set; }
        /// <summary>
        /// No of count we are not able to Insert.
        /// </summary>
        public int TotalNotInsertedRecord { get; set; }
        /// <summary>
        /// Not inserted record ids list.
        /// </summary>
        public List<NotInsertedRecord> NotInsertedRecords { get; set; }
    }
}
