﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{

    public class CaseDefaultStep
    {
        public int CaseDefaultId { get; set; }

        public int? StepOrderNo { get; set; }

        public string Description { get; set; }

        public DateTime? CreatedDate { get; set; }

        public Patient objPatientdetail { get; set; }

        public List<PatientsDatalist> PatientListing { get; set; }

        public List<ColleagueDatalst> ColleagueList { get; set; }

        public string PatientId { get; set; }
    }

    public class PatientsDatalist
    {

        public string id { get; set; }
        public string text { get; set; }
    }

    public class PatientDetailList
    {

        public string PatientName { get; set; }
        public string Email { get; set; }
    }

    public class ColleagueDatalst
    {

        public string id { get; set; }
        public string text { get; set; }
    }
}
