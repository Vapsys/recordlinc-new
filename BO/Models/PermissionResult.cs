﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class PermissionResult
    {
        public bool HasPermission { get; set; }
        public bool DoesMembershipAllow { get; set; }
        public string Message { get; set; }
        public int UsedCount { get; set; }
        public int MaxCount { get; set; }
        public int FeatureId { get; set; }

    }
}
