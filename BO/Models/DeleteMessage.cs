﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// This Class is used for Delete message from OCR.
    /// </summary>
    public class DeleteMessage
    {
        /// <summary>
        /// The Message Id which you want to Delete.
        /// </summary>
        public int MessageId { get; set; }
        /// <summary>
        /// The MessageDisplay Type is for Sent or Received Message 2 = Received 3 = Sent
        /// </summary>
        public int MessageDisplayType { get; set; }
        /// <summary>
        /// The access_token which is associate with Message
        /// </summary>
        public string access_token { get; set; }
        /// <summary>
        /// This is identify the message is sent or received
        /// </summary>
        public string MessageFrom { get; set; }
    }
}
