﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class MembershipOfMember
    {
        public int UserId { get; set; }
        public string FirstName { get; set; }
        public string MembershipName { get; set; }
        public string Price { get; set; }        
        public string Interval { get; set; }
        public string PaymentMode { get; set; }
        public string TotalCount { get; set; }
        public string MembershipId { get; set; }
    }
}
