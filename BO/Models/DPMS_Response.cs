﻿using System.Collections.Generic;

namespace BO.Models
{
    public class DPMS_Response
    {

        /// <summary>
        /// Id of specific request when it is called first time.
        /// </summary>
        public string RequestId { get; set; }
        public string PMSConnectorId { get; set; }
        public List<ResponsedData> Result { get; set; }
    }
    /// <summary>
    /// Ankit Solanki : 03-23-2018
    /// PRCSP-29 PMSRetrieveResponse API 
    /// Please Do not change on this Class Property if you are change in it you will change on Data-Type of SQL "ProcessQueue".
    /// Also You need to change on the stored Procedure of "USP_PatientProcessQueueUpdate".
    /// </summary>
    public class ResponsedData
    {

        /// <summary>
        /// Recordlinc ID of Patient 
        /// </summary>
        public int RL_Id { get; set; }
        /// <summary>
        /// Whether API call is successful or not
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Custom information message to be retrun while API call
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// PMSId of the User
        /// </summary>
        public int PMSId { get; set; }
    }
    public class DPMS_ProcedureResponse
    {
        /// <summary>
        /// Id of specific request when it is called first time.
        /// </summary>
        public string RequestId { get; set; }
        /// <summary>
        /// Type of response returned by API
        /// </summary>
        public List<ResponsedData> Result { get; set; }
    }
    public class DMPS_PaymentResponse
    {  
        /// <summary>
       /// Id of specific request when it is called first time.
       /// </summary>
        public string RequestId { get; set; }
        /// <summary>
        /// RequestType 1 = Finance
        /// RequestType 2 = Adjustment
        /// RequestType 3 = Insurance
        /// RequestType 4 = Standard
        /// </summary>
        public Enums.Common.PatientPaymentType RequestType { get; set; }
        /// <summary>
        /// Type of response returned by API
        /// </summary>
        public List<ResponsedData> Result { get; set; }
    }

    /// <summary>
    /// Appointment writeback responsed
    /// </summary>
    public class AppointmentResponsed
    {

        /// <summary>
        /// Id of specific request when it is called first time.
        /// </summary>
        public string RequestId { get; set; }
        /// <summary>
        /// Dentrix connector id
        /// </summary>
        public string PMSConnectorId { get; set; }
        /// <summary>
        /// Response resulte
        /// </summary>
        public List<DMPSAppointmentResponsedData> Result { get; set; }
    }

    /// <summary>
    /// Appointment response resulte
    /// </summary>
    public class DMPSAppointmentResponsedData
    {

        /// <summary>
        /// Recordlinc ID of Patient 
        /// </summary>
        public int RLAppointmentId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RLPendingAppointmentId { get; set; }
        /// <summary>
        /// Whether API call is successful or not
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Custom information message to be retrun while API call
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// PMSId of the User
        /// </summary>
        public int AppointmentId { get; set; }
    }

}
