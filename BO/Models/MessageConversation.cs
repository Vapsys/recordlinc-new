﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// Used for Filter Conversation View.
    /// </summary>
    public class FilterMessageConversation
    {
        /// <summary>
        /// MessageId is Null-able 
        /// </summary>
        public int? MessageId { get; set; }
        /// <summary>
        /// UserId 
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// Page Size
        /// </summary>
        public int PageSize { get; set; }
        /// <summary>
        /// Page Index
        /// </summary>
        public int PageIndex { get; set; }
        /// <summary>
        /// Sort Column
        /// </summary>
        public int SortColumn { get; set; }
        /// <summary>
        /// Sort Direction
        /// </summary>
        public int SortDirection { get; set; }
        /// <summary>
        /// Doctor Name
        /// </summary>
        public string DoctorName { get; set; }
        /// <summary>
        /// From Date
        /// </summary>
        public DateTime? FromDate { get; set; }
        /// <summary>
        /// To Date
        /// </summary>
        public DateTime? Todate { get; set; }
        /// <summary>
        /// Disposition
        /// </summary>
        public string Disposition { get; set; }
        /// <summary>
        /// Message
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Message Status
        /// </summary>
        public int MessageStatus { get; set; }
        /// <summary>
        /// Patient Name
        /// </summary>
        public string PatientName { get; set; }
        /// <summary>
        /// Message From
        /// </summary>
        public int MessageFrom { get; set; }
        /// <summary>
        /// Access Token
        /// </summary>
        public string access_token { get; set; }
        /// <summary>
        /// Added SearchDisposition status for Select2
        /// </summary>
        public string SearchDisposition { get; set; }

        /// <summary>
        /// Time zone system name (for example for India : India Standard Time)
        /// </summary>
        public string TimeZoneSystemName { get; set; }

        /// <summary>
        ///  Patient Insurance Status 
        /// </summary>
        public int InsuranceStatus { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class MessageConversation
    {
        public int UserId { get; set; }
        public string DoctorName { get; set; }
    }
}
