﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class InsuranceCarrier
    {
        public int InsuranceId { get; set; }
        public string InsuranceName { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
