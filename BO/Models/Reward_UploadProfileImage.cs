﻿using System.ComponentModel.DataAnnotations;

namespace BO.Models
{
    public class Reward_UploadProfileImage
    {
        [Required (ErrorMessage = "The RewardPartnerId is required.")]
        public int RewardPartnerId { get; set; }
        [Required(ErrorMessage = "The ImageString is required.")]
        public string ImageString { get; set; }
        [Required(ErrorMessage = "The ImageNameWithExtension is required.")]
        public string ImageNameWithExtension { get; set; }
        public string path { get; set; }
        public APIFilter.FilterFlag sourcetype { get; set; }
    }
}
