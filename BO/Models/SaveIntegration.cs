﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// Save Integration 
    /// </summary>
    public class SaveIntegration
    {
        /// <summary>
        /// String Contain IntegrationId, ConnectorKey, and LocationId
        /// The Formate of String like = "1_DALW134654A65SD4A65SD4AWDAS4D5_120598"
        /// </summary>
        public string[] Integration { get; set; }

    }
}
