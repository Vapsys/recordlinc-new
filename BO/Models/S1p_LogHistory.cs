﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class S1p_LogHistory
    {
        public int LogId { get; set; }
        public int StatusCode { get; set; }
        public DateTime CreatedDate { get; set; }
        public string Notes { get; set; }
        public string RequestContentType { get; set; }
        public string RequestContentBody { get; set; }
        public string RequestUri { get; set; }
        public string RequestMethod { get; set; }
        public string RequestRouteTemplete { get; set; }
        public string RequestRouteData { get; set; }
        public string RequestHeaders { get; set; }
        public DateTime RequestTime { get; set; }
        public string ResponseContentType { get; set; }
        public string ResponseContentBody { get; set; }
        public string ResponseStatusCode { get; set; }
        public string ResponseHeaders { get; set; }
        public DateTime ResponseTime { get; set; }
        public string Token { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorStackTrace { get; set; }
        public string ErrorInnerException { get; set; }
        public string ErrorInnerStackTrace { get; set; }
        public int IsSuccess { get; set; }
    }
}
