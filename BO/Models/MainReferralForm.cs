﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class MainReferralForm
    {
        /// <summary>
        /// Define constractor for all list data
        /// </summary>
        public MainReferralForm()
        {
            RefFormSetting = new List<ReferralFormSetting>();
            DentistReferral = new List<DentistReferral>();
        }
        /// <summary>
        /// List of Referral Form settings
        /// </summary>
        public List<ReferralFormSetting> RefFormSetting { get; set; }
        /// <summary>
        /// List of Dentist Referral Form settings
        /// </summary>
        public List<DentistReferral> DentistReferral { get; set; }
        /// <summary>
        /// First name is Enable/Disable setting
        /// </summary>
        public bool IsFirstName { get; set; }
        /// <summary>
        /// Last Name is Enable/Disable setting
        /// </summary>
        public bool IsLastName { get; set; }
        /// <summary>
        /// Patient Phone is Enable/Disable setting
        /// </summary>
        public bool IsPatientPhone { get; set; }
        /// <summary>
        /// Patient Email is Enable/Disable setting
        /// </summary>
        public bool IsPatientEmail { get; set; }
        /// <summary>
        /// Date of Birth is Enable/Disable setting
        /// </summary>
        public bool IsDateofBirth { get; set; }
        /// <summary>
        /// Insurance Provider is Enable/Disable setting
        /// </summary>
        public bool IsInsuranceProvider { get; set; }
        /// <summary>
        /// Location is Enable/Disable setting
        /// </summary>
        public bool IsDisplayLocation { get; set; }
        /// <summary>
        /// Get User id for sequence update
        /// </summary>
        public int UserId { get; set; }
    }

    /// <summary>
    /// Save Referral Form Setting for Dentist
    /// </summary>
    public class SaveReferralForm
    {
        /// <summary>
        /// List of Referral Form setting fields
        /// </summary>
        public List<ReferralFormSetting> RefFrmSetting { get; set; }

        /// <summary>
        /// List of Specialty 
        /// </summary>
        public List<ReferralFormSetting> SpecialtySetting { get; set; }

        /// <summary>
        /// List of Referral Service Settings
        /// </summary>
        public List<ReferralFormServices> ServiceSetting { get; set; }

        /// <summary>
        /// List of SubField settings
        /// </summary>
        public List<ReferralFormSubField> SubFieldSetting { get; set; }
    }


    /// <summary>
    /// Get Dentist Referral Setting Data
    /// </summary>
    public class DentistReferral
    {
        /// <summary>
        /// Primary key for Dentist Referral setting
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Field name for Dentist Referral setting
        /// </summary>
        public string ControlName { get; set; }

        /// <summary>
        /// Map Field name for Dentist Referral setting
        /// </summary>
        public string MapFieldName { get; set; }

        /// <summary>
        /// IsEnable field for Enable/Disable setting
        /// </summary>
        public bool IsEnable { get; set; }

        /// <summary>
        /// SortOrder field for Dentist Referral setting
        /// </summary>
        public int SortOrder { get; set; }

        /// <summary>
        /// UserId Field For Manage Orderby
        /// </summary>
        public int UserId { get; set; }
    }

    /// <summary>
    /// Get property for save database
    /// </summary>
    public class FieldSequence
    {
        /// <summary>
        /// primary key
        /// </summary>
        public int KeyId { get; set; }
        /// <summary>
        /// parent primary key
        /// </summary>
        public int ParentKeyId { get; set; }
        /// <summary>
        /// get specialityid
        /// </summary>
        public int SpecialityId { get; set; }
        /// <summary>
        /// get servicesid
        /// </summary>
        public int ServicesId { get; set; }
        /// <summary>
        /// new sequence assign by user drag and drop
        /// </summary>
        public int NewSequence { get; set; }
        /// <summary>
        /// login user id
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// table name for track update sequence which table 
        /// </summary>
        public string TableName { get; set; }

    }
}
