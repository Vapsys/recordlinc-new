﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class ReferralMessageDetails
    {
        public int ReferralCardId { get; set; }
        public int MessageId { get; set; }
        public int SenderId { get; set; }
        public int ReceiverID { get; set; }
        public bool IsSent { get; set; }
        public int PatientId { get; set; }
        public string ProfileImage { get; set; }
        public string FullName { get; set; }
        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string SenderName { get; set; }
        public string SenderImage { get; set; }
        public string ReceiverImage { get; set; }
        public string ReceiverName { get; set; }
        public string FullAddress { get; set; }
        public string Phone { get; set; }
        public string EmailAddress { get; set; }
        public string RegardOption { get; set; }
        public string RequestingOption { get; set; }
        public string OtherComments { get; set; }
        public string RequestComments { get; set; }
        public string Comments { get; set; }
        public string CreationDate { get; set; }
        public string PatientName { get; set; }
        public string Location { get; set; }
        public string AssignedPatientId { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        public bool PatientStatus { get; set; }
        public List<MessageAttachment> lstImages = new List<MessageAttachment>();
        public List<MessageAttachment> lstDocuments = new List<MessageAttachment>();
        public List<ToothList> lstToothList { get; set; }
        public List<ReferralMessageDetails> lstRefferalDetails { get; set; }
        public int MessageTypeId { get; set; }//for this if SentBox = 1 if Inbox = 0
        public List<int> lstSelectedTeeths { get; set; }
        public List<int> lstSelectedPermanentTeeths { get; set; }
        public List<int> lstSelectedPrimaryTeeths { get; set; }
        public string BoxFolderId { get; set; }
        public DateTime InsuranceVerified { get; set; }
        public string PatientLastModifiedDate { get; set; }
        public int ServicesCount { get; set; }
        public FileListModel fileListModel { get; set; }

        public string[] ReferralRegarding = { "Orthodontics", "Periodontist", "Oral Surgery", "Prosthodontics", "Radiology", "General Dentistry", "Endodontics", "Lab Work" };
        public string[] ReferralRequest = { "Extractions", "Pathology", "Caries", "Check Periodontal", "Oral Surgery", "Orthodontic Consultation", "Orthognathic Surgery", "Implants" };
    }
}
