﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// This Class used for Insert/Update Referral status from Account setting page
    /// Ankit Solanki : 10-23-2018
    /// </summary>
    public class OCR_ReferralStatus
    {
        /// <summary>
        /// Login User UserId
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// I am specialist and receive referrals frequently.
        /// </summary>
        public bool IsReceiveReferral { get; set; }
        /// <summary>
        /// I am general practitioner and want to check status on my <br /> sent referral as part of daily business process.
        /// </summary>
        public bool IsSendReferral { get; set; }
    }
}
