﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class PointBalance
    {
        public int PointId { get; set; }
        public int AccountId { get; set; }
        public int PointLevel { get; set; }
        public decimal Multiplier { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
    }
}
