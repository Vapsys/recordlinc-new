﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class AttachedPatientDetails
    {
        public int AttachedPatientId { get; set; }
        public string AttachedPatientFirstName { get; set; }
        public string AttachedPatientLastName { get; set; }
    }
}
