﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
   public  class  ResponseStatus
    {
        public string ResponseCode { get; set; }
        public string ResponseMessage { get; set; }

        public bool Status { get; set; }

    }
}
