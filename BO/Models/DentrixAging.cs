﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class DentrixAging
    {
        /// <summary>
        /// AutoModifiedTimestamp of PMS (Patient Management System).
        /// </summary>
        public DateTime? AutoModifiedTimestamp { get; set; }
        /// <summary>
        /// Guarantor Id of Recordlink.
        /// </summary>
        public int GuarId { get; set; }
        /// <summary>
        /// Guarantor Id of PMS (Patient Management System).
        /// </summary>
        public int DentrixGuarId { get; set; }
        /// <summary>
        /// AgingDate of of PMS (Patient Management System).
        /// </summary>
        public DateTime? AgingDate { get; set; }
        /// <summary>
        /// Aging between 0 to 30 of PMS (Patient Management System).
        /// </summary>
        public double? Aging0to30 { get; set; }
        /// <summary>
        ///  Aging between 31 to 60 of PMS (Patient Management System).
        /// </summary>
        public double? Aging31to60 { get; set; }
        /// <summary>
        ///  Aging between 61 to 90 of PMS (Patient Management System).
        /// </summary>
        public double? Aging61to90 { get; set; }
        /// <summary>
        ///  Aging between 91 or higer of PMS (Patient Management System).
        /// </summary>
        public double? Aging91plus { get; set; }
        /// <summary>
        /// Last Payment Date (in UTC) of PMS (Patient Management System).
        /// </summary>
        public DateTime? LastPaymentDate { get; set; }
        /// <summary>
        /// Last Insurance Pay Date (in UTC)  of PMS (Patient Management System).
        /// </summary>
        public DateTime? LastInsurancePayDate { get; set; }
        /// <summary>
        /// Las tBilling Date (in UTC) of PMS (Patient Management System).
        /// </summary>
        public DateTime? LastBillingDate { get; set; }
        /// <summary>
        /// PMS connector Id of Recordlink.
        /// </summary>
        public string DentrixConnectorId { get; set; }
        /// <summary>
        /// PatientId of Recordlink. PatientId is always same as GuarId because againg reocords are available for guarantor 
        /// </summary>
        public int PatientId { get; set; }
        /// <summary>
        /// PatientId of PMS (Patient Management System). DentrixPatientId is always same as DentrixGuarId because againg reocords are available for guarantor 
        /// </summary>
        public int DentrixPatientId { get; set; }
    }
}
