﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class ZPMerchantRequest
    {
        public string requestType { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public string merchantType { get; set; }
        public string merchantProfile { get; set; }
        public string merchantCreationPolicy { get; set; }
        public string profileId { get; set; }
        public string resellerId { get; set; }
        public string clientHost { get; set; }        
        public int portfolioId { get; set; }        
        public int feeTemplateId { get; set; }        
        public string processingConfigurationScript { get; set; }
        public string applicationCode { get; set; }
        public string isEmbedded { get; set; }
        public string pageFormat { get; set; }
        public string notifyURL { get; set; }
        public string cancelURL { get; set; }
        public string returnURL { get; set; }
        public string returnURLPolicy { get; set; }
        public string notificationPolicy { get; set; }
        public MerchantOwner Owner { get; set; }
        public MerchantBusiness Business { get; set; }
        public MerchantAccount Account { get; set; }
    }

    public class MerchantOwner
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string street1 { get; set; }
        public string street2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipCode { get; set; }
        public string countryCode { get; set; }
        public string birthDate { get; set; }
        public string socialSecurity { get; set; }
        public string phone { get; set; }
        public int ZPAccountId { get; set; }
        public string AccountName { get; set; }
        public int AccountId { get; set; }
        public string DoctorName { get; set; }
        public string PatientName { get; set; }
        public  string PaymentDate { get; set; }
        public decimal Amount { get; set; }
        public string Status { get; set; }
        public decimal split_Amount { get; set; }
        public decimal ActualAmount { get; set; }
        public decimal ReceivedAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public int DoctorId { get; set; }
        public string DentrixPatId { get; set; }
        public int PatientId { get; set; }
        public string PrimaryPhone { get; set; }
    }

    public class MerchantBusiness
    {
        public string businessName { get; set; }
        public string legalName { get; set; }        
        public string ownershipStructureType { get; set; }
        public string street1 { get; set; }
        public string street2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zipCode { get; set; }
        public string countryCode { get; set; }
        public string descriptorPhone { get; set; }
        public string paymentCardDescriptor { get; set; }
        public string directDebitDescriptor { get; set; }
        public string taxId { get; set; }
        public string webSite { get; set; }
        public string email { get; set; }
        public string description { get; set; }
        //public string contactPhone { get; set; }
        public string merchantCategoryCode { get; set; }
        public string currencyCode { get; set; }
        public string driverLicenseState { get; set; }
        public string driverLicenseCountryCode { get; set; }
        public string driverLicense { get; set; }
        public string relationshipBeginDate { get; set; }
        public string registrationCountryCode { get; set; }
        public string registrationState { get; set; }
        public string registrationYear { get; set; }
        //public string driverLicense { get; set; }
        //public string driverLicenseCountryCode { get; set; }
        //public string driverLicenseState { get; set; }
        //public string registrationCountryCode { get; set; }
        //public string registrationState { get; set; }
        //public string registrationYear { get; set; }
        //public string workHours { get; set; }
        //public string timeZoneCode { get; set; }
        //public string relationshipBeginDate { get; set; }
    }

    public class MerchantAccount
    {
        public string annualDirectDebitVolume { get; set; }
        public string annualCardsVolume { get; set; }
        public string avgDirectDebitTransactionAmount { get; set; }
        public string avgCardsTransactionAmount { get; set; }
        public string routingNumber { get; set; }
        public string bankName { get; set; }
        public string holderName { get; set; }
        public string accountNumber { get; set; }
        public string accountType { get; set; }
        public string maxTransactionAmount { get; set; }
    }

    public class MerchantResponse
    {
        public string responseType { get; set; }
        public string applicationCode { get; set; }
        public string applicationId { get; set; }
        public string merchantId { get; set; }
        public string directDebitDescriptor { get; set; }
        public string cardsDescriptor { get; set; }
        public string responseCode { get; set; }
        public string responseMessage { get; set; }
        public string accountId { get; set; }
    }

    public class UserAccount
    {
        public string Fullname { get; set; }
        public int ZPAccountId { get; set; }
        public int UserId { get; set; }
    }

    
}
