﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BO.Models
{
    public class Patient_DentrixInsurancePayment
    {
        //Ankit Here 07-19-2018: Commented fields are removed as per Martin change request.
        //SBI-9 Has already added comment on it.
        [Required(ErrorMessage = "Paymentid is required. ")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1")]
        public int paymentId { get; set; }
        public int patid { get; set; }
        public int guarid { get; set; }
        public DateTime? procdate { get; set; }
        public DateTime? createdate { get; set; }
        public string provid { get; set; }
        public int claimid { get; set; }
        public int inspaytype { get; set; }
        public string inspaydescript { get; set; }
        public decimal amount { get; set; }
        public bool history { get; set; }
        public DateTime? automodifiedtimestamp { get; set; }
        [Required(ErrorMessage = "DentrixConnectorId is required. ")]
        public string dentrixConnectorID { get; set; }
    }
    public class InsurancePayment
    {
        /// <summary>
        /// Recordlinc Primary key
        /// </summary>
        public int RL_ID { get; set; }
        /// <summary>
        /// Dentrix Primary Key
        /// </summary>
        public int paymentId { get; set; }
        /// <summary>
        /// PatientId of Recordlinc
        /// </summary>
        public int patid { get; set; }
        /// <summary>
        /// Guaranteer Id of Dentrix
        /// </summary>
        public int guarid { get; set; }
        /// <summary>
        /// Procedure Date
        /// </summary>
        public DateTime? procdate { get; set; }
        /// <summary>
        /// Entry date / Creation date of Dentrix
        /// </summary>
        public DateTime? createdate { get; set; }
        /// <summary>
        /// Dentrix Provider Id
        /// </summary>
        public string Dentrixprovid { get; set; }
        /// <summary>
        /// Recordlinc Provider Id
        /// </summary>
        public string RLprovid { get; set; }
        public int claimid { get; set; }
        public int inspaytype { get; set; }
        public string inspaydescript { get; set; }
        /// <summary>
        /// Amount
        /// </summary>
        public decimal amount { get; set; }
        public bool history { get; set; }
        public DateTime? automodifiedtimestamp { get; set; }
        /// <summary>
        /// Dentrix PatientId
        /// </summary>
        public int DentrixPatientId { get; set; }
        /// <summary>
        /// Dentrix guarantor Id
        /// </summary>
        public int DentrixGuarId { get; set; }      
    }
}
