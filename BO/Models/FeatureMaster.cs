﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BO.Models
{
    public class FeatureMaster
    {
        public int? Id { get; set; }
        [Required(ErrorMessage = "Please enter feature name")]
        [Remote("CheckFeature", "Admin", AdditionalFields = "initialFeature", ErrorMessage = "Feature already exists!")]
        public string Feature { get; set; }
        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
        public List<object> lstTypeOfStatus { get; set; }
        public int Status { get; set; }
    }
}
