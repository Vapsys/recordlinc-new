﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// 1) This Class Used on Patient Reward API Side for get a Dentist list which is Associate with an Account.
    /// </summary>
    public class Account
    {

        /// <summary>
        ///Recordlinc Account Id of the User 
        /// </summary>
        [Required(ErrorMessage = "AccountId is required.")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1")]
        public int AccountId { get; set; }
        /// <summary>
        /// Id of Location associated with dentist
        /// </summary>
        public int? LocationId { get; set; }
    }
}
