﻿namespace BO.Models
{
    /// <summary>
    /// Used for Exception Track 
    /// </summary>
    public class ExceptionRequestModel
    {
        /// <summary>
        /// URL of API
        /// </summary>
        public string URL { get; set; }
        /// <summary>
        /// Exception Message
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// Stack Trace of Exception.
        /// </summary>
        public string StackTrace { get; set; }
    }
}
