﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class BillingInfo
    {
        [Required(ErrorMessage = "Card Number is required.")]
        [Display(Name = "Card Number")]
        [StringLength(30, ErrorMessage = "Card Number must not exceed 14 digit.")]
        public string CardNumber { get; set; }
        [Required(ErrorMessage = "Month is required.")]
        public string ExpirationMonth { get; set; }
        [Required(ErrorMessage = "Year is required.")]
        public string ExpirationYear { get; set; }
        [Required(ErrorMessage = "CVV is required.")]
        public string CVV { get; set; }
        [Required(ErrorMessage = "Zip Code is required.")]
        public string BillingZipCode { get; set; }
        public double BillingAmount { get; set; }
        public string BillPayInterval { get; set; }
        [Required(ErrorMessage = "You can't leave this.")]
        public bool Iagree { get; set; }
    }
}
