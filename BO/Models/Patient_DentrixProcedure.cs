﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    /// <summary>
    /// As per JIRA SUP-7 we have set required fields on this Object. and DC-10
    /// </summary>
    public class Patient_DentrixProcedure
    {
        public int patid { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1")]
        public int guarid { get; set; }
        [Required(ErrorMessage = "provid is required.")]
        public string provid { get; set; }
        [Required(ErrorMessage = "procid is required.")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1")]
        public int procid { get; set; }
        public int DentrixId { get; set; }
        public DateTime procdate { get; set; }
        public bool history { get; set; }

        public int txcaseid { get; set; }
        
        [Required(ErrorMessage = "chartstatus is required.")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1")]
        public int chartstatus { get; set; }

        public string chartstatusstring { get; set; }

        [Required(ErrorMessage = "preauthid is required.")]
        public int preauthid { get; set; }

        [Required(ErrorMessage = "amountpreauth is required.")]
        public decimal amountpreauth { get; set; }

        [Required(ErrorMessage = "authstatus is required.")]
        public int authstatus { get; set; }

        [Required(ErrorMessage = "authstatusstring is required.")]
        public string authstatusstring { get; set; }

        [Required(ErrorMessage = "amountsecpreauth is required.")]
        public decimal amountsecpreauth { get; set; }

        [Required(ErrorMessage = "secauthstatus is required.")]
        public int secauthstatus { get; set; }

        [Required(ErrorMessage = "secauthstatusstring is required.")]
        public string secauthstatusstring { get; set; }
        public DateTime? startdate { get; set; }
        public DateTime? completiondate { get; set; }
        public string adadescription { get; set; }
        public decimal amount { get; set; }

        public decimal amountpriminspaid { get; set; }

        public decimal amountsecinspaid { get; set; }
        [Required(ErrorMessage = "claimid is required.")]
        public int claimid { get; set; }

        //SUP-7
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than 1")]
        public int proccodeid { get; set; }

        [Required(ErrorMessage = "adacode is required.")]
        public string adacode { get; set; }

        public bool medproctype { get; set; }

        public bool donotbillinsurance { get; set; }
        [Required(ErrorMessage = "toothrangestart is required.")]
        public int toothrangestart { get; set; }
        [Required(ErrorMessage = "toothrangeend is required.")]
        public int toothrangeend { get; set; }

        public string surfacestring { get; set; }
        [Required(ErrorMessage = "surfacebytes is required.")]
        [StringLength(40, MinimumLength = 40, ErrorMessage = "surfacebytes must be 40 characters.")]
        public string surfacebytes { get; set; }
        public DateTime? txplanneddate { get; set; }
        public int refid { get; set; }

        public int reftype { get; set; }
        public DateTime automodifiedtimestamp { get; set; }
        [Required(ErrorMessage = "dentrixConnectorID is required. ")]
        public string dentrixConnectorID { get; set; }

        public DateTime? createdate { get; set; }
    }
    public class PatientProcedures
    {
        /// <summary>
        /// Primary Key / Recordlinc ProcedureId.
        /// </summary>
        public int RL_Id { get; set; }
        /// <summary>
        /// Dentrix PatientId
        /// </summary>
        public int DentrixPatientId { get; set; }
        /// <summary>
        /// Recordlinc PatientId
        /// </summary>
        public int RLPatientId { get; set; }
        /// <summary>
        /// Guaranteer Id of Recordlinc
        /// </summary>
        public int RLguarid { get; set; }
        /// <summary>
        /// Guaranteer Id of Dentrix
        /// </summary>
        public int DentrixGuarId { get; set; }
        /// <summary>
        /// Recordlinc Primary key of Provider / Recordlinc ProviderId.
        /// </summary>
        public string RLprovid { get; set; }
        /// <summary>
        /// DentrixProviderId 
        /// </summary>
        public string Dentrixprovid { get; set; }
        /// <summary>
        /// Procedure Id
        /// </summary>
        public int procid { get; set; }
        /// <summary>
        /// Procedure Date
        /// </summary>
        public DateTime procdate { get; set; }
        public bool history { get; set; }
        public int txcaseid { get; set; }
        /// <summary>
        /// Chart Status
        /// </summary>
        public int chartstatus { get; set; }
        /// <summary>
        /// Chart Status string
        /// </summary>
        public string chartstatusstring { get; set; }
        /// <summary>
        /// Pre Auth Id
        /// </summary>
        public int preauthid { get; set; }

        public decimal amountpreauth { get; set; }
        public int authstatus { get; set; }
        public string authstatusstring { get; set; }
        public decimal amountsecpreauth { get; set; }
        public int secauthstatus { get; set; }

        public string secauthstatusstring { get; set; }
        /// <summary>
        /// Start Date
        /// </summary>
        public DateTime? startdate { get; set; }
        /// <summary>
        /// Completion Date
        /// </summary>
        public DateTime? completiondate { get; set; }
        /// <summary>
        /// Ada Description
        /// </summary>
        public string adadescription { get; set; }
        /// <summary>
        /// Amount of Procedure
        /// </summary>
        public decimal amount { get; set; }

        public decimal amountpriminspaid { get; set; }
        public decimal amountsecinspaid { get; set; }
        public int claimid { get; set; }
        /// <summary>
        /// Procedure Code Id
        /// </summary>
        public int proccodeid { get; set; }
        /// <summary>
        /// ADA code
        /// </summary>
        public string adacode { get; set; }
        public bool medproctype { get; set; }

        public bool donotbillinsurance { get; set; }
        public int toothrangestart { get; set; }
        public int toothrangeend { get; set; }

        public string surfacestring { get; set; }
        public string surfacebytes { get; set; }
        public DateTime? txplanneddate { get; set; }
        public int refid { get; set; }
        public int reftype { get; set; }
        public DateTime automodifiedtimestamp { get; set; }
        public string dentrixConnectorID { get; set; }

        /// <summary>
        /// States wether procedure is deleted or not. true means procedure is deleted and false otherwise.
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// Entry date / Creation date of Dentrix
        /// </summary>
        public DateTime? createdate { get; set; }
    }
}
