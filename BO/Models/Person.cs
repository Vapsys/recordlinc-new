﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
    public class Person
    {

        private string _imageUrl = string.Empty;
        private string _fullName = string.Empty;
        private string _firstName = string.Empty;
        private string _lastName = string.Empty;
        private string _age = string.Empty;
        private string _gender = string.Empty;
        private string _location = string.Empty;
        private string _company = string.Empty;
        private string _title = string.Empty;
        private string _email = string.Empty;
        private string _influenceScore = string.Empty;
        private string _facebookURL = string.Empty;
        private string _twitterURL = string.Empty;
        private string _instagramURL = string.Empty;//RM-355
        private string _linkedInURL = string.Empty;
        private string _statusCode = string.Empty;
        private string _statusDescription = string.Empty;
        private string _description = string.Empty;
        private string _website = string.Empty;


        public Person()
        {
        }

        public string ImageUrl
        {
            get { return _imageUrl; }
            set { _imageUrl = value; }
        }

        public string FullName
        {
            get { return _fullName; }
            set { _fullName = value; }
        }
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        public string Age
        {
            get { return _age; }
            set { _age = value; }
        }
        public string Gender
        {
            get { return _gender; }
            set { _gender = value; }
        }
        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }
        public string Company
        {
            get { return _company; }
            set { _company = value; }
        }
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        public string InfluenceScore
        {
            get { return _influenceScore; }
            set { _influenceScore = value; }
        }

        public string FacebookURL
        {
            get { return _facebookURL; }
            set { _facebookURL = value; }
        }

        public string TwitterURL
        {
            get { return _twitterURL; }
            set { _twitterURL = value; }
        }

        public string InstagramURL
        {
            get { return _instagramURL; }
            set { _instagramURL = value; }
        }


        public string LinkedInURL
        {
            get { return _linkedInURL; }
            set { _linkedInURL = value; }
        }

        public string StatusCode
        {
            get { return _statusCode; }
            set { _statusCode = value; }
        }
        public string StatusDescription
        {
            get { return _statusDescription; }
            set { _statusDescription = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public string Website
        {
            get { return _website; }
            set { _website = value; }
        }

    }

}
