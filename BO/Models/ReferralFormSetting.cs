﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Models
{
   public class ReferralFormSetting
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool? IsEnable { get; set; }
        public int OrderBy { get; set; }
        public int SpecialityId { get; set; }

        public List<ReferralFormServices> RefService { get; set; }

        //public ReferralFormSetting()
        //{
        //   RefService = null;
        //}
    }
}
