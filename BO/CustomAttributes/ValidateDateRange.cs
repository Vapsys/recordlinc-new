﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BO.CustomAttributes
{
    public class MinValidateDateRange : ValidationAttribute
    {
        public string FirstDate { get; set; }
       

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var dtValue = Convert.ToDateTime(value);
            // your validation logic
            if (dtValue >= Convert.ToDateTime(FirstDate))
            {
                return ValidationResult.Success;
            }
            else
            {
                var ErrMsg =  $"Date must grater then {FirstDate}.";
                if (!string.IsNullOrEmpty(ErrorMessage))
                {
                    ErrMsg = ErrorMessage;
                }
                return new ValidationResult(ErrMsg);

            }
        }
    }
}
