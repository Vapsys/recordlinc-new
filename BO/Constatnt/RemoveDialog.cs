﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Constatnt
{
  public static class RemoveDialog
    {
        public const string YES = "Yes, delete it!";
        public const string NO = "No";
        public const string CANCEL = "Cancel";
        public const string TITLE = "Are you sure you want to remove?";
    }
}
