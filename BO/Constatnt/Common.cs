﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Constatnt
{
    public static class Common
    {
        public const string IsFrom = "IluvmyDentist";

        //ZiftPay Merchant Constant
        public const string merchantType = "M";
        public const string merchantProfile = "SSSSM";
        public const string merchantCreationPolicy = "A";
        public const string requestType = "create";
        public const string profileId = "1943779842";
        public const string resellerId = "95";
        public const string clientHost = "4.4.4.4";
        public const int portfolioId = 100;
        public const int feeTemplateId = 100010;
        public const string processingConfigurationScript = "zift11";
        public const string applicationCode = "100001";
        public const string isEmbedded = "1";
        public const string pageFormat = "OBDA";
        public const string returnURLPolicy = "page";
        public const string notificationPolicy = "X";
        public const int AppointmentMaxRecord = 100000;
        public const string pass = "LetMeCheckIt";
        public const int CountOverLoad = 50000;
        //ZiftPay transaction Constant
        public const string requestTypePayment = "sale";
        public const string requestTypeToken = "tokenization";
        public const string holderType = "P";
        public const string transactionIndustryType = "RE";
        public const string accountType = "R";
        public const string customerAccountCode = "0000000001";
        public const string transactionCode = "0000000001";
        public const int AdminId = 36390;
        public const string GetTables = "SELECT name FROM sys.tables ORDER BY name";

        //Api Pagesize constant
        public const int MaxPageSize = 100;// Convert.ToInt32(ConfigurationManager.AppSettings["MaxPageSize"].ToString());

    }
}
