﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Constatnt
{
    public static class PermissionMesssage
    {
        public const string New_Patient_Leads = "You do not have permission to add patient. Please upgrade your membership to add more patients.";
        public const string Colleagues = "You do not have permission to add colleague. Please upgrade your membership to add more colleagues.";
        public const string Attachments = "You do not have permission to add attachment. Please upgrade your membership to add more attachments.";
        public const string OCR = "You do not have permission to login into One-Click Referral. Please upgrade your membership to use One-Click Referral";
    }
}
