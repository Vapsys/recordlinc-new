﻿using DotNetOpenAuth.Messaging;
using DotNetOpenAuth.OAuth2;
using MailBee;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BO
{
    public class OAuthHelperClass
    {
        public static OutgoingWebResponse CallOAuthGmail(string clientid, string clientsecret, string redirect)
        {            
            AuthorizationServerDescription server = new AuthorizationServerDescription
            {
                AuthorizationEndpoint = new Uri("https://accounts.google.com/o/oauth2/auth?access_type=offline&approval_prompt=force"),
                TokenEndpoint = new Uri("https://accounts.google.com/o/oauth2/token"),
                ProtocolVersion = ProtocolVersion.V20,
            };
            OAuth2 myOAuth2 = new OAuth2(clientid, clientsecret);
            List<string> scope = new List<string>();
            scope.Add("https://mail.google.com/");
            WebServerClient consumer = new WebServerClient(server, clientid, clientsecret);
            OutgoingWebResponse response = consumer.PrepareRequestUserAuthorization(scope, new Uri(redirect));
            return response;
        }

        public static string CallOAuthOutlook(string clientid, string clientsecret, string redirect)
        {
            string redirectUrl = string.Empty;
            OAuth2 myOAuth2 = new OAuth2(clientid, clientsecret);
            StringDictionary p1 = new StringDictionary();
            //p1.Add(OAuth2.Scope, "wl.imap");
            p1.Add(OAuth2.Scope, "wl.offline_access");
            p1.Add(OAuth2.RedirectUriKey, redirect);
            p1.Add(OAuth2.ResponseTypeKey, "code");
            redirectUrl = myOAuth2.AuthorizeToken("https://login.live.com/oauth20_authorize.srf", p1);
            return redirectUrl;
        }

        public static List<string> GetAccessTokenbyRefresh(string refreshtoken, string gclientID, string gclientSecret)
        {
            string actionUrl = "https://accounts.google.com/o/oauth2/token";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(actionUrl);
            request.CookieContainer = new CookieContainer();
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            string encodedParameters = string.Format(
                "client_id={0}&client_secret={1}&refresh_token={2}&grant_type={3}",
                HttpUtility.UrlEncode(gclientID),                
                HttpUtility.UrlEncode(gclientSecret),
                HttpUtility.UrlEncode(refreshtoken),
                HttpUtility.UrlEncode("refresh_token")
                );
            byte[] requestData = Encoding.UTF8.GetBytes(encodedParameters);
            request.ContentLength = requestData.Length;
            if (requestData.Length > 0)
                using (Stream stream = request.GetRequestStream())
                    stream.Write(requestData, 0, requestData.Length);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string responseText = null;
            using (TextReader reader = new StreamReader(response.GetResponseStream(), Encoding.ASCII))
                responseText = reader.ReadToEnd();
            List<string> strToken = new List<string>();
            foreach (string sPair in responseText.
                Replace("{", "").
                Replace("}", "").
                Replace("\"", "").
                Split(new string[] { ",\n" }, StringSplitOptions.None))
            {
                string[] pair = sPair.Split(':');
                if ("access_token" == pair[0].Trim())
                {
                    strToken.Add(pair[1].Trim());
                }
                if ("refresh_token" == pair[0].Trim())
                {
                    strToken.Add(pair[1].Trim());
                    break;
                }
            }

            return strToken;
        }

        //public static List<string> GetAccessTokenbyRefreshOutlook(string refreshtoken, string gclientID, string redirectUrl, string clientsecret)
        //{

        //    string actionUrl = "https://login.microsoftonline.com/common/oauth2/v2.0/token";
        //    //string actionUrl = "https://login.live.com/oauth20_authorize.srf";
        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(actionUrl);
        //    request.CookieContainer = new CookieContainer();
        //    request.Method = "POST";
        //    request.ContentType = "application/x-www-form-urlencoded";            
        //    string encodedParameters = string.Format(
        //        "client_id={0}&scope={1}&refresh_token={2}&redirect_uri={3}&grant_type={4}&client_secret={5}",
        //        HttpUtility.UrlEncode(gclientID),
        //        HttpUtility.UrlEncode("offline_access"),
        //        HttpUtility.UrlEncode(refreshtoken),
        //        HttpUtility.UrlEncode(redirectUrl),
        //        HttpUtility.UrlEncode("refresh_token"),
        //        HttpUtility.UrlEncode(clientsecret)
        //        );
        //    byte[] requestData = Encoding.UTF8.GetBytes(encodedParameters);
        //    request.ContentLength = requestData.Length;
        //    if (requestData.Length > 0)
        //        using (Stream stream = request.GetRequestStream())
        //            stream.Write(requestData, 0, requestData.Length);
        //    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
        //    string responseText = null;
        //    using (TextReader reader = new StreamReader(response.GetResponseStream(), Encoding.ASCII))
        //        responseText = reader.ReadToEnd();
        //    List<string> strToken = new List<string>();
        //    foreach (string sPair in responseText.
        //        Replace("{", "").
        //        Replace("}", "").
        //        Replace("\"", "").
        //        Split(new string[] { ",\n" }, StringSplitOptions.None))
        //    {
        //        string[] pair = sPair.Split(':');
        //        if ("access_token" == pair[0].Trim())
        //        {
        //            strToken.Add(pair[1].Trim());
        //        }
        //        if ("refresh_token" == pair[0].Trim())
        //        {
        //            strToken.Add(pair[1].Trim());
        //            break;
        //        }
        //    }

        //    return strToken;
        //}
    }
}
