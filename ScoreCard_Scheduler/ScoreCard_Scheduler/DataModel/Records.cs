﻿using ScoreCard_Scheduler.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccessLayer.Reports;

namespace ScoreCard_Scheduler.DataModel
{
    static class Records
    {
        static ReportsDAL reportsDal = new ReportsDAL();
        static string connectionString = ConfigurationManager.ConnectionStrings["ReportConnectionString"].ConnectionString;
        static int bulk = Convert.ToInt32(ConfigurationManager.AppSettings["IsBulk"]);

        public static List<Member_Detail> GetMemberDetails()
        {
            List<Member_Detail> member_Details = new List<Member_Detail>();            DataTable dt = reportsDal.Get_Member_Details();            foreach (DataRow dr in dt.Rows)
            {
                Member_Detail lists = new Member_Detail();
                lists.Member_ID = Convert.ToInt32(dr["UserId"].ToString());
                lists.Member_Name = dr["FirstName"].ToString();
                lists.Member_LName = dr["LastName"].ToString();
                lists.DentrixProviderID = dr["DentrixProviderId"].ToString();
                lists.Create_Date = dr["CreatedDate"].ToString();
                lists.TypeOf_Provider = dr["CreatedDate"].ToString();
                member_Details.Add(lists);
            }            return member_Details;
        }

        public static List<Provider_ProductionGoals> GetProviderProductionGoals_()        {            string procedureCodes = GetProcudureCodes();            string hygieneCodes = GetHygieneCodes();            string providerIDs = GetProviderIDs();            string restoritiveCodes = GetRestoritiveCodes();            string newPatientCodes = GetNewPatientCode();

            //string account_ids = GetAccountID();
            //string locationIds = GetLocationIDs();

            Provider_ProductionGoals providerGoals;            List<Provider_ProductionGoals> listProviderGoals = new List<Provider_ProductionGoals>();            DataTable dt = new DataTable();            dt = reportsDal.GetProviderProductionGoals(providerIDs, procedureCodes, hygieneCodes, restoritiveCodes, newPatientCodes, bulk);            foreach (DataRow dr in dt.Rows)            {                providerGoals = new Provider_ProductionGoals();
                providerGoals.ACCOUNT_ID = Convert.ToString(dr["ACCOUNT_ID"]);
                providerGoals.PROVIDER_DENTRIXID = Convert.ToString(dr["PROVIDER_DENTRIXID"]);                providerGoals.SCHEDULED_PRODUCTION = dr["SCHEDULED_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SCHEDULED_PRODUCTION"]);                providerGoals.NP_APPOINTMENTS = dr["NP_APPOINTMENTS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["NP_APPOINTMENTS"]);
                providerGoals.ROUTINE_HYEGEINE_VISIT = dr["ROUTINE_HYEGEINE_VISIT"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ROUTINE_HYEGEINE_VISIT"]);
                providerGoals.ACTUAL_PRODUCTION = dr["ACTUAL_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt64(dr["ACTUAL_PRODUCTION"]);
                providerGoals.HYGEINE_EXAMS = dr["HYGEINE_EXAMS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HYGEINE_EXAMS"]);
                providerGoals.SRP = dr["SRP"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SRP"]);
                providerGoals.REFDATE = Convert.ToDateTime(dr["REFDATE"]);                listProviderGoals.Add(providerGoals);            }            return listProviderGoals;        }


        #region -- Changes By Bhupesh On Dated : 30-04-2019        // Changes By Bhupesh
        public static List<Show_Hide_Forms> GetLocationFromRecordLincDB()        {            List<Show_Hide_Forms> list_LocationID = new List<Show_Hide_Forms>();            DataTable dt = reportsDal.GetLocations_List_RecordLinc();            foreach (DataRow dr in dt.Rows)
            {
                Show_Hide_Forms lists = new Show_Hide_Forms();
                lists.LocationID = Convert.ToInt32(dr["LocationId"]);
                list_LocationID.Add(lists);
            }            return list_LocationID;        }

        public static List<Show_Hide_Forms> GetAccountIDtoshow()        {
            List<Show_Hide_Forms> list_test = new List<Show_Hide_Forms>();            DataTable dt = reportsDal.Get_AccountID_List_RecordLinc();            foreach (DataRow dr in dt.Rows)            {                Show_Hide_Forms lists = new Show_Hide_Forms();                lists.AccountID = Convert.ToInt32(dr["AccountID"]);                list_test.Add(lists);

            }            return list_test;        }        public static List<Stats_Reports> Get_stats_List()
        {
            List<Stats_Reports> stats_list = new List<Stats_Reports>();            DataTable dt = reportsDal.Get_Stats_Reports(bulk);
            foreach (DataRow dr in dt.Rows)
            {
                Stats_Reports lists = new Stats_Reports();
                lists.ACCOUNT_ID = Convert.ToInt32(dr["AccountId"]);
                lists.NET_PRODUCTION = Convert.ToInt32(dr["NET_PRODUCTION"]);
                stats_list.Add(lists);
            }
            return stats_list;
        }
        public static List<Hygiene_Report> Get_Hygiene_List()
        {
            List<Hygiene_Report> hygeine_list = new List<Hygiene_Report>();            DataTable dt = reportsDal.Get_Hygiene_Location_Amount(bulk);
            foreach (DataRow dr in dt.Rows)
            {
                Hygiene_Report lists = new Hygiene_Report();
                lists.ACCOUNT_ID = Convert.ToInt32(dr["Account_Id"]);
                lists.NET_PRODUCTION = Convert.ToInt32(dr["NET_PRODUCTION"]);
                hygeine_list.Add(lists);
            }
            return hygeine_list;
        }
        public static List<Individual_Summary> Get_Individual_List()
        {
            List<Individual_Summary> individual_list = new List<Individual_Summary>();            DataTable dt = reportsDal.Get_Individual_Location_Amount(bulk);
            foreach (DataRow dr in dt.Rows)
            {
                Individual_Summary lists = new Individual_Summary();
                lists.ACCOUNT_ID = Convert.ToInt32(dr["AccountId"]);
                lists.NetProduction = Convert.ToInt32(dr["NET_PRODUCTION"]);
                individual_list.Add(lists);
            }
            return individual_list;
        }
        public static string GetAccountID()        {            string accountid = string.Empty;            List<Test> list_test = new List<Test>();            DataTable dt = reportsDal.Get_AccountID_List_RecordLinc();            foreach (DataRow dr in dt.Rows)            {                Test lists = new Test();                lists.AccountID = Convert.ToInt32(dr["AccountID"]);                list_test.Add(lists);                accountid += lists.AccountID + ",";            }            return accountid;        }
        #endregion

        public static List<Smart_Numbers> GetSmartNumbers()
        {
            string restoritiveCodes = GetRestoritiveCodes();
            string hygieneCodes = GetHygieneCodes();
            string providerIDs = GetProviderIDs();
            Smart_Numbers smartNumbers;
            List<Smart_Numbers> listSmartNumbers = new List<Smart_Numbers>();

            DataTable dt = new DataTable();
            dt = reportsDal.GetSmartNumbers(hygieneCodes, restoritiveCodes,providerIDs, bulk);
            foreach (DataRow dr in dt.Rows)
            {
                smartNumbers = new Smart_Numbers();
                smartNumbers.COLLECTIBLE_PRODUCTION = dr["COLLECTIBLE_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["COLLECTIBLE_PRODUCTION"]);
                smartNumbers.COLLECTIONS = dr["COLLECTIONS"] == DBNull.Value ? 0 : Convert.ToInt32(dr["COLLECTIONS"]);
                smartNumbers.REFDATE = Convert.ToDateTime(dr["CURRENT_DATE"]);
                smartNumbers.HYGEINE_PRODUCTION = dr["HYGEINE_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HYGEINE_PRODUCTION"]);
                smartNumbers.RESTORATIVE_PRODUCTION = dr["RESTORATIVE_PRODUCTION"] == DBNull.Value ? 0 : Convert.ToInt32(dr["RESTORATIVE_PRODUCTION"]);
                listSmartNumbers.Add(smartNumbers);
            }
            return listSmartNumbers;
        }

        public static List<CriticalNumbers> GetCriticalNumbers()
        {
            string restoritiveCodes = GetRestoritiveCodes();
            string hygieneCodes = GetHygieneCodes();
            string providerIDs = GetProviderIDs();
            CriticalNumbers criticalNumbers;
            List<CriticalNumbers> listCriticalNumbers = new List<CriticalNumbers>();

            DataTable dt = new DataTable();
            dt = reportsDal.GetCriticalNumbers(hygieneCodes, restoritiveCodes,providerIDs, bulk);
            foreach (DataRow dr in dt.Rows)
            {
                criticalNumbers = new CriticalNumbers();
                criticalNumbers.ACCOUNT_ID = dr["ACCOUNT_ID"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ACCOUNT_ID"]);
                // criticalNumbers.TotalProduction_NewPatient = dr["TotalProduction_NewPatient"] == DBNull.Value ? 0 : Convert.ToInt32(dr["TotalProduction_NewPatient"]);
                criticalNumbers.REFDATE = Convert.ToDateTime(dr["CURRENT_DATE"]);
                //criticalNumbers.HygeineProduction_Operatory = dr["HygeineProduction_Operatory"] == DBNull.Value ? 0 : Convert.ToInt32(dr["HygeineProduction_Operatory"]);
                //criticalNumbers.TotalRestorativeProduction_HygeineVisit = dr["TotalRestorativeProduction_HygeineVisit"] == DBNull.Value ? 0 : Convert.ToInt32(dr["TotalRestorativeProduction_HygeineVisit"]);
                listCriticalNumbers.Add(criticalNumbers);
            }
            return listCriticalNumbers;
        }


        public static string GetProcudureCodes()
        {
            string procedureCodes = string.Empty;

            using (SqlConnection myConnection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT Code from dentalCare d  inner join [ProcedureID] p on p.PROCEDURECODE = d.PROCEDURE_CODE  inner join GROUPING g  on g.GROUPING_ID = p.GROUPING_ID", myConnection))
                {
                    cmd.CommandType = CommandType.Text;
                    myConnection.Open();

                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            procedureCodes += Convert.ToString(dr["Code"]) + ",";
                        }
                    }
                }
            }
            procedureCodes = procedureCodes.TrimEnd(',', ' ');
            return procedureCodes;
        }

        public static string GetProviderIDs()
        {
            string providerIds = string.Empty;

            using (SqlConnection myConnection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT M.DENTRIX_PROVIDERID FROM Member_Details M", myConnection))
                {
                    cmd.CommandType = CommandType.Text;
                    myConnection.Open();


                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            providerIds += Convert.ToString(dr["DENTRIX_PROVIDERID"]) + ",";
                        }
                    }
                }
            }
            providerIds = providerIds.TrimEnd(',', ' ');
            return providerIds;
        }



        public static string GetLocationIDs()
        {
            string locationIds = string.Empty;

            using (SqlConnection myConnection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT M.Location_id FROM Member_Details M", myConnection))
                {
                    cmd.CommandType = CommandType.Text;
                    myConnection.Open();


                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            locationIds += Convert.ToString(dr["Location_id"]) + ",";
                        }
                    }
                }
            }
            locationIds = locationIds.TrimEnd(',', ' ');
            return locationIds;
        }
        
        public static string GetHygieneCodes()
        {
            string hygieneCodes = string.Empty;

            using (SqlConnection myConnection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT Code from dentalCare d  inner join [ProcedureID] p on p.PROCEDURECODE = d.PROCEDURE_CODE  inner join GROUPING g  on g.GROUPING_ID = p.GROUPING_ID where g.GROUPING_ID = 7", myConnection))
                {
                    cmd.CommandType = CommandType.Text;
                    myConnection.Open();


                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            hygieneCodes += Convert.ToString(dr["Code"]) + ",";
                        }
                    }
                }
            }
            hygieneCodes = hygieneCodes.TrimEnd(',', ' ');
            return hygieneCodes;
        }

        public static string GetNewPatientCode()
        {
            string newPatientCode = string.Empty;

            using (SqlConnection myConnection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT Code from dentalCare d  inner join [ProcedureID] p on p.PROCEDURECODE = d.PROCEDURE_CODE  inner join GROUPING g  on g.GROUPING_ID = p.GROUPING_ID where g.GROUPING_ID = 8", myConnection))
                {
                    cmd.CommandType = CommandType.Text;
                    myConnection.Open();


                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            newPatientCode += Convert.ToString(dr["Code"]) + ",";
                        }
                    }
                }
            }
            newPatientCode = newPatientCode.TrimEnd(',', ' ');
            return newPatientCode;
        }

        public static string GetRestoritiveCodes()
        {
            string restoritiveCodes = string.Empty;

            using (SqlConnection myConnection = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("SELECT Code from dentalCare d  inner join [ProcedureID] p on p.PROCEDURECODE = d.PROCEDURE_CODE  inner join GROUPING g  on g.GROUPING_ID = p.GROUPING_ID where g.GROUPING_ID = 3", myConnection))
                {
                    cmd.CommandType = CommandType.Text;
                    myConnection.Open();


                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            restoritiveCodes += Convert.ToString(dr["Code"]) + ",";
                        }
                    }
                }
            }
            restoritiveCodes = restoritiveCodes.TrimEnd(',', ' ');

            return restoritiveCodes;
        }
    }
}
