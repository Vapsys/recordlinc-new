﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreCard_Scheduler.DataModel
{
    public static class ErrorLog
    {
        static string connectionString = ConfigurationManager.AppSettings["ReportConnectionString"];
        public static bool InsertErrorLog(string URL, string ErrorMessage, string StackTrace)
        {
            int result = 0;
            try
            {
                using (SqlConnection myConnection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("InsertErrorLog", myConnection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add(new SqlParameter("URL", URL));
                        cmd.Parameters.Add(new SqlParameter("ErrorMessage", ErrorMessage));
                        cmd.Parameters.Add(new SqlParameter("StackTrace", StackTrace));
                        myConnection.Open();
                        result = cmd.ExecuteNonQuery();
                        if (result > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }                   
                }
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
}
