﻿using DataAccessLayer.Reports;
using ScoreCard_Scheduler.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreCard_Scheduler.DataModel
{
    static class FillData
    {
        static ReportsDAL reportsDal = new ReportsDAL();
        static string connectionString = ConfigurationManager.ConnectionStrings["ReportConnectionString"].ConnectionString;
        static int bulk = Convert.ToInt32(ConfigurationManager.AppSettings["IsBulk"]);

        public static bool FillMember_Details(List<Member_Detail> members)
        {
            string errorMessage = string.Empty;
            try
            {
                foreach (Member_Detail member in members)
                {
                    int result = 0;

                    using (SqlConnection myConnection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("Fill_Member_Details", myConnection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("@MEMBER_ID", member.Member_ID));
                            cmd.Parameters.Add(new SqlParameter("@MEMBER_FNAME", member.Member_Name));
                            cmd.Parameters.Add(new SqlParameter("@MEMBER_LNAME", member.Member_LName));
                            cmd.Parameters.Add(new SqlParameter("@DENTRIX_PROVIDERID",member.DentrixProviderID));
                            cmd.Parameters.Add(new SqlParameter("@CREATEDDATE", member.Create_Date));
                          //  cmd.Parameters.Add(new SqlParameter("@LOCATION_ID", member.LocationID));
                            
                            myConnection.Open();
                            result = cmd.ExecuteNonQuery();
                            if (result <= 0)
                            {
                                errorMessage += "Could not add Member Details: " + member.DentrixProviderID + ". For Date: " + member.Create_Date;
                                errorMessage += Environment.NewLine;
                            }
                        }
                    }
                }
                if (string.IsNullOrEmpty(errorMessage))
                {
                    return true;
                }
                else
                {
                    ErrorLog.InsertErrorLog("FillData - Fill_Member_Details", errorMessage, errorMessage);
                    return false;
                }
            }
            catch (Exception Ex)
            {
                ErrorLog.InsertErrorLog("FillData - Fill_Member_Details", Ex.Message, Ex.StackTrace);
                return false;
            }
        }

        public static bool FillProviderProductionGoals(List<Provider_ProductionGoals> provider)
        {
            string errorMessage = string.Empty;
            try
            {
                foreach (Provider_ProductionGoals prodGoals in provider)
                {
                    int result = 0;

                    using (SqlConnection myConnection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("FillProviderGoals", myConnection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("ACCOUNT_ID", prodGoals.ACCOUNT_ID));
                            cmd.Parameters.Add(new SqlParameter("PROVIDER_DENTRIXID", prodGoals.PROVIDER_DENTRIXID));
                            cmd.Parameters.Add(new SqlParameter("SCHEDULED_PRODUCTION", prodGoals.SCHEDULED_PRODUCTION));
                            cmd.Parameters.Add(new SqlParameter("ACTUAL_PRODUCTION", prodGoals.ACTUAL_PRODUCTION));
                            cmd.Parameters.Add(new SqlParameter("HYGEINE_EXAMS", prodGoals.HYGEINE_EXAMS));
                            cmd.Parameters.Add(new SqlParameter("NP_APPOINTMENTS", prodGoals.NP_APPOINTMENTS));
                            cmd.Parameters.Add(new SqlParameter("ROUTINE_HYEGEINE_VISIT", prodGoals.ROUTINE_HYEGEINE_VISIT));
                            cmd.Parameters.Add(new SqlParameter("SRP", prodGoals.SRP));
                            cmd.Parameters.Add(new SqlParameter("REFDATE", prodGoals.REFDATE));
                            myConnection.Open();
                            result = Convert.ToInt32(cmd.ExecuteScalar());
                            if (result <= 0)
                            {
                                errorMessage += "Could not add Provider Goals in Reporting DB for Provider: " + prodGoals.PROVIDER_DENTRIXID + ". For Date: " + prodGoals.REFDATE;
                                errorMessage += Environment.NewLine;
                            }
                        }
                    }
                }
                if (string.IsNullOrEmpty(errorMessage))
                {
                    return true;
                }
                else
                {
                    ErrorLog.InsertErrorLog("FillData - FillProviderProductionGoals", errorMessage, errorMessage);
                    return false;
                }
            }
            catch (Exception Ex)
            {
                ErrorLog.InsertErrorLog("FillData - FillProviderProductionGoals", Ex.Message, Ex.StackTrace);
                return false;
            }
        }

        #region -- Changes By Bhupesh on Dated : 30-04-2019
        public static bool FillShowHideTable(List<Show_Hide_Forms> locationList)
        {
            string errorMessage = string.Empty;
            try
            {
                foreach (Show_Hide_Forms locationId in locationList)
                {
                    int result = 0;

                    using (SqlConnection myConnection = new SqlConnection(connectionString))
                    {
                        for (int i = 1; i <= 9; i++)
                        {
                            SqlCommand cmd = new SqlCommand("FillShowHideTable");
                            cmd.Connection = myConnection;
                            cmd.CommandType = CommandType.StoredProcedure;

                            cmd.Parameters.AddWithValue("@Pages_ID", i);
                            cmd.Parameters.AddWithValue("@Acount_ID", locationId.AccountID);
                            myConnection.Open();
                            cmd.CommandTimeout = 150;
                            result = cmd.ExecuteNonQuery();
                            myConnection.Close();
                        }

                        if (result <= 0)
                        {
                            errorMessage += Environment.NewLine;
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ErrorLog.InsertErrorLog("FillData - FillProviderProductionGoals", Ex.Message, Ex.StackTrace);
                return false;
            }
            return true;
        }
        public static bool FillIndividualAll(List<Individual_Summary> individual_Reports)
        {
            string errorMessage = string.Empty;
            try
            {
                foreach (Individual_Summary id in individual_Reports)
                {
                    int result = 0;

                    using (SqlConnection myConnection = new SqlConnection(connectionString))
                    {
                        SqlCommand cmd = new SqlCommand("FillIndividualSummary");
                        cmd.Connection = myConnection;
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@ACCOUNT_ID", id.ACCOUNT_ID);
                        cmd.Parameters.AddWithValue("@GrossProduction", id.NetProduction);
                        cmd.Parameters.AddWithValue("@AdjustmentPer", id.AdjustmentPer);  // Relted to Adjustment Percentage
                        cmd.Parameters.AddWithValue("@Days", 1);   /// Related To Days

                        myConnection.Open();
                        cmd.CommandTimeout = 150;
                        result = cmd.ExecuteNonQuery();
                        myConnection.Close();

                        if (result <= 0)
                        {
                            errorMessage += Environment.NewLine;
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ErrorLog.InsertErrorLog("FillData - FillProviderProductionGoals", Ex.Message, Ex.StackTrace);
                return false;
            }
            return true;
        }

        public static bool FillStatsAll(List<Stats_Reports> stats_Reports)
        {
            string errorMessage = string.Empty;
            try
            {
                foreach (Stats_Reports id in stats_Reports)
                {
                    int result = 0;

                    using (SqlConnection myConnection = new SqlConnection(connectionString))
                    {
                        SqlCommand cmd = new SqlCommand("FillStatsReports");
                        cmd.Connection = myConnection;
                        cmd.CommandType = CommandType.StoredProcedure;


                        cmd.Parameters.AddWithValue("@ACCOUNT_ID", id.ACCOUNT_ID);
                        cmd.Parameters.AddWithValue("@GrossProduction", id.NET_PRODUCTION);
                        cmd.Parameters.AddWithValue("@AdjustmentPer", 5);  // Relted to Adjustment Percentage
                        cmd.Parameters.AddWithValue("@Days", 10);   /// Related To Days

                        myConnection.Open();
                        cmd.CommandTimeout = 150;
                        result = cmd.ExecuteNonQuery();
                        myConnection.Close();

                        if (result <= 0)
                        {
                            errorMessage += Environment.NewLine;
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ErrorLog.InsertErrorLog("FillData - FillProviderProductionGoals", Ex.Message, Ex.StackTrace);
                return false;
            }
            return true;
        }

        public static bool FillHygieneAll(List<Hygiene_Report> hygiene_Reports)
        {
            string errorMessage = string.Empty;
            try
            {
                foreach (Hygiene_Report id in hygiene_Reports)
                {
                    int result = 0;

                    using (SqlConnection myConnection = new SqlConnection(connectionString))
                    {
                        SqlCommand cmd = new SqlCommand("FillHygieneReport");
                        cmd.Connection = myConnection;
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.AddWithValue("@ACCOUNT_ID", id.ACCOUNT_ID);
                        cmd.Parameters.AddWithValue("@GrossProduction", id.NET_PRODUCTION);
                        cmd.Parameters.AddWithValue("@AdjustmentPer", 5);  // Relted to Adjustment Percentage
                        cmd.Parameters.AddWithValue("@Days", 10);   // Related To Days

                        myConnection.Open();
                        cmd.CommandTimeout = 150;
                        result = cmd.ExecuteNonQuery();
                        myConnection.Close();

                        if (result <= 0)
                        {
                            errorMessage += Environment.NewLine;
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ErrorLog.InsertErrorLog("FillData - FillProviderProductionGoals", Ex.Message, Ex.StackTrace);
                return false;
            }
            return true;
        }
        #endregion
        public static bool FillSmartNumbers(List<Smart_Numbers> smartNumbers)
        {
            string errorMessage = string.Empty;
            try
            {
                foreach (Smart_Numbers smartNumber in smartNumbers)
                {
                    int result = 0;

                    using (SqlConnection myConnection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("FillSmartNumbers", myConnection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("ACCOUNT_ID", smartNumber.ACCOUNT_ID));
                      //      cmd.Parameters.Add(new SqlParameter("PROVIDER_DENTRIXID", smartNumber.PROVIDER_DENTRIXID));
                            cmd.Parameters.Add(new SqlParameter("COLLECTIONS", smartNumber.COLLECTIONS));
                            cmd.Parameters.Add(new SqlParameter("COLLECTIBLE_PRODUCTION", smartNumber.COLLECTIBLE_PRODUCTION));
                            cmd.Parameters.Add(new SqlParameter("HYGEINE_PRODUCTION", smartNumber.HYGEINE_PRODUCTION));
                            cmd.Parameters.Add(new SqlParameter("RESTORATIVE_PRODUCTION", smartNumber.RESTORATIVE_PRODUCTION));
                            cmd.Parameters.Add(new SqlParameter("REFDATE", smartNumber.REFDATE));
                            myConnection.Open();
                            cmd.CommandTimeout = 150;
                            result = cmd.ExecuteNonQuery();
                            if (result <= 0)
                            {
                                errorMessage += "Could not add Smart Numbers in Reporting DB for Date: " + smartNumber.REFDATE;
                                errorMessage += Environment.NewLine;
                            }
                        }
                    }
                }
                if (string.IsNullOrEmpty(errorMessage))
                {
                    return true;
                }
                else
                {
                    ErrorLog.InsertErrorLog("FillData - FillSmartNumbers", errorMessage, errorMessage);
                    return false;
                }
            }
            catch (Exception Ex)
            {
                ErrorLog.InsertErrorLog("FillData - FillSmartNumbers", Ex.Message, Ex.StackTrace);
                return false;
            }
        }

        public static bool FillCriticalNumbers(List<CriticalNumbers> criticalNumbers)
        {
            string errorMessage = string.Empty;
            try
            {
                foreach (CriticalNumbers criticalNumber in criticalNumbers)
                {
                    int result = 0;

                    using (SqlConnection myConnection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("FillCriticalNumbers", myConnection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add(new SqlParameter("ACCOUNT_ID", criticalNumber.ACCOUNT_ID));
                            cmd.Parameters.Add(new SqlParameter("LOCATION_ID", criticalNumber.LOCATION_ID));

                            //cmd.Parameters.Add(new SqlParameter("COLLECTIONS", criticalNumber.COLLECTIONS));
                            //cmd.Parameters.Add(new SqlParameter("COLLECTIBLE_PRODUCTION", criticalNumber.COLLECTIBLE_PRODUCTION));
                            //cmd.Parameters.Add(new SqlParameter("HYGEINE_PRODUCTION", criticalNumber.HYGEINE_PRODUCTION));
                            //cmd.Parameters.Add(new SqlParameter("RESTORATIVE_PRODUCTION", criticalNumber.RESTORATIVE_PRODUCTION));
                            cmd.Parameters.Add(new SqlParameter("REFDATE", criticalNumber.REFDATE));
                            cmd.Parameters.Add(new SqlParameter("isBulk", bulk));
                            
                            myConnection.Open();
                            cmd.CommandTimeout = 150;
                            result = cmd.ExecuteNonQuery();
                            if (result <= 0)
                            {
                                errorMessage += "Could not add Critical Numbers in Reporting DB for Date: " + criticalNumber.REFDATE;
                                errorMessage += Environment.NewLine;
                            }
                        }
                    }
                }
                if (string.IsNullOrEmpty(errorMessage))
                {
                    return true;
                }
                else
                {
                    ErrorLog.InsertErrorLog("FillData - FillCriticalNumbers", errorMessage, errorMessage);
                    return false;
                }
            }
            catch (Exception Ex)
            {
                ErrorLog.InsertErrorLog("FillData - FillCriticalNumbers", Ex.Message, Ex.StackTrace);
                return false;
            }
        }
        //public static string FillClinicalProductionGoals(Clinical_ProductionGoals clinicalProductionGoals)
        //{
        //}
        //public static string FillAdminProductionGoals(Admin_ProductionGoals adminProductionGoals)
        //{
        //}
    }
}
