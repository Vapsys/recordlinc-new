﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreCard_Scheduler.Data
{
    class Hygene_Report
    {
        public int ID { get; set; }
        public int LOCATION_ID { get; set; }
        public int ACCOUNT_ID { get; set; }
        public int OFFICE_ID { get; set; }
        public int ORG_ID { get; set; }
        public int GENERAL_PRODUCTION { get; set; }
        public int NET_PRODUCTION { get; set; }
        public int ORTHO_PRODUCTION { get; set; }
        public int NET_ORTHO_PRODUCTION { get; set; }
        public int PTS_SEEN { get; set; }
        public int DAYS { get; set; }
        public int GROSS_PRODUCTION_PER_PTS_SEEN { get; set; }
        public int GROSS_PROVIDER_PRODUCTION_PER_DAY { get; set; }
        public int GROSS_PRODUCTION { get; set; }
        public int NET_PRODUCTION_NEW { get; set; }
        public int PTS_SEEN_NEW { get; set; }
        public int GROSS_PRODUCTION_PER_PTS_SEEN_NEW { get; set; }
        public int ARESTIN { get; set; }
        public int SRP_PERIO_MAINT { get; set; }
        public int ARESTIN_PERIO_PTS_SEEN { get; set; }
        public string OFFICE { get; set; }
        public string DOCTORS { get; set; }
        public string CREATEDDATE { get; set; }
        public int CREATEDBY { get; set; }
        public string MODIFIEDATE { get; set; }
        public string MODIFIEDBY { get; set; }
    }
}
