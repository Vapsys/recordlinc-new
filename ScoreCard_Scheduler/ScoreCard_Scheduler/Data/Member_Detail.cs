﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreCard_Scheduler.Data
{
    public class Member_Detail
    {
        public int Member_ID { get; set; }
        public string Member_Name { get; set; }
        public string Member_LName { get; set; }
        public string DentrixProviderID { get; set; }
        public string TypeOf_Provider { get; set; }
        public string Create_Date { get; set; }
        public int LocationID { get; set; }
    }
}
