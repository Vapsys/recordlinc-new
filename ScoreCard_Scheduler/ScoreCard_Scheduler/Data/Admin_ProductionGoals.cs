﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreCard_Scheduler.Data
{
    class Admin_ProductionGoals
    {
        public string STAFF_ID { get; set; }
        public int LOCATION_ID { get; set; }
        public int OFFICE_ID { get; set; }
        public int ORG_ID { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public int NP_SCHEDULED { get; set; }
        public int PATIENTS_RESCHEDULED { get; set; }
        public int PATIENTS_CANCELLATIONS { get; set; }
        public int RECALLS { get; set; }
        public int CONTACTS { get; set; }
        public int SCHEDULED { get; set; }
        public TimeSpan HOURS { get; set; }
        public DateTime CREATEDDATE { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime MODIFIEDATE { get; set; }
        public string MODIFIEDBY { get; set; }
    }
}
