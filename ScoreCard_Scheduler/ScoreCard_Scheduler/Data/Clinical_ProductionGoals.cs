﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreCard_Scheduler.Data
{
    class Clinical_ProductionGoals
    {
        public string CLINICAL_STAFF_ID { get; set; }
        public string CLINICAL_DENTRIXID { get; set; }
        public int LOCATION_ID { get; set; }
        public int OFFICE_ID { get; set; }
        public int ORG_ID { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public int NUMBER_PROCEDURES { get; set; }
        public int PROCEDURES_COST { get; set; }
        public int ROOMS_TURNED { get; set; }
        public TimeSpan HOURS { get; set; }      
        public DateTime CREATEDDATE { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime MODIFIEDATE { get; set; }
        public string MODIFIEDBY { get; set; }
    }
}
