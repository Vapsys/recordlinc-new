﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreCard_Scheduler.Data
{
    public class Test
    {
        public int Location_id { get; set; }
        public int DentrixProviderId { get; set; }
        public int AccountID { get; set; }
        public int UserId { get; set; }
    }
}
