﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreCard_Scheduler.Data
{
    class CriticalNumbers
    {
        public int ID { get; set; }
        public int LOCATION_ID { get; set; }
        public int ACCOUNT_ID { get; set; }
        public int OFFICE_ID { get; set; }
        public int ORG_ID { get; set; }
        public int Collection_Ratio { get; set; }
        public int TotalProduction_NewPatient { get; set; }
        public int TotalProduction_HygeineVisit { get; set; }
        public int CollectibleProduction_HygeineVisit { get; set; }
        public int TotalRestorativeProduction_HygeineVisit { get; set; }
        public int MarketingCosts_NewPatient { get; set; }
        public int Marketing_ROI { get; set; }
        public int Production_Hour { get; set; }
        public int Production_Operatory { get; set; }
        public int RestorativeProduction_Operatory { get; set; }
        public int HygeineProduction_Operatory { get; set; }
        public int Production_NonProdEmployee { get; set; }
        public int Production_NonDoc { get; set; }
        public DateTime REFDATE { get; set; }
        public DateTime CREATEDDATE { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime MODIFIEDATE { get; set; }
        public string MODIFIEDBY { get; set; }
    }
}
