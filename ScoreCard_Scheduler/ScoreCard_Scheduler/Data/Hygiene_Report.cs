﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreCard_Scheduler.Data
{
   public class Hygiene_Report
    {
        public int ID { get; set; }
        public int LOCATION_ID { get; set; }
        public int ACCOUNT_ID { get; set; }
        public float GENERAL_PRODUCTION { get; set; }
        public float NET_PRODUCTION { get; set; }
        public float ORTHO_PRODUCTION { get; set; }
        public float NET_ORTHO_PRODUCTION { get; set; }
        public float PTS_SEEN { get; set; }
        public float DAYS { get; set; }
        public float GROSS_PRODUCTION_PER_PTS_SEEN { get; set; }
        public float GROSS_PROVIDER_PRODUCTION_PER_DAY { get; set; }
        public float GROSS_PRODUCTION { get; set; }
        public float NET_PRODUCTION_NEW { get; set; }
        public float PTS_SEEN_NEW { get; set; }
        public float GROSS_PRODUCTION_PER_PTS_SEEN_NEW { get; set; }
        public float ARESTIN { get; set; }
        public float SRP_PERIO_MAINT { get; set; }
        public float ARESTIN_PERIO_PTS_SEEN { get; set; }

        public int Month { get; set; }
        public string MonthName { get; set; }

        public DateTime REFDATE { get; set; }
        public DateTime CREATEDDATE { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime MODIFIEDATE { get; set; }
        public string MODIFIEDBY { get; set; }
    }
}
