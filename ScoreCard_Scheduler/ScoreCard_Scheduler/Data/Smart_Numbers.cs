﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreCard_Scheduler.Data
{
    class Smart_Numbers
    {
        public int ID { get; set; }
        public int LOCATION_ID { get; set; }
        public int ACCOUNT_ID { get; set; }
        public string PROVIDER_DENTRIXID { get; set; }
        public int OFFICE_ID { get; set; }
        public int ORG_ID { get; set; }
        public int TOTAL_OFFICE_HOURS { get; set; }
        public int HYGEINE_VISITS { get; set; }
        public int NEW_PATIENTS { get; set; }
        public int MARKETING_COSTS { get; set; }
        public int COLLECTIONS { get; set; }
        public int PRODUCTION { get; set; }
        public int COLLECTIBLE_PRODUCTION { get; set; }
        public int HYGEINE_PRODUCTION { get; set; }
        public int RESTORATIVE_PRODUCTION { get; set; }     
        public DateTime REFDATE { get; set; }
        public DateTime CREATEDDATE { get; set; }
        public string CREATEDBY { get; set; }
        public DateTime MODIFIEDATE { get; set; }
        public string MODIFIEDBY { get; set; }
    }
}
