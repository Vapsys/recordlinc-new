﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreCard_Scheduler.Data
{
    class Grouping
    {
        public int GROUPING_ID { get; set; }
        public string GROUPING_NAME { get; set; }
        public string PROCEDURE_CODE { get; set; }
        public string PROCEDURE_NAME { get; set; }       
    }
}
