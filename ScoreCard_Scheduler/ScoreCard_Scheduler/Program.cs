﻿using ScoreCard_Scheduler.Data;
using ScoreCard_Scheduler.DataModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScoreCard_Scheduler
{
    class Program
    {
        static List<Provider_ProductionGoals> provider = new List<Provider_ProductionGoals>();
        static List<Smart_Numbers> smartNumbers = new List<Smart_Numbers>();
        static List<CriticalNumbers> criticalNumbers = new List<CriticalNumbers>();

        // Changes By Bhupesh On Dated : 30-04-2019
        static List<Show_Hide_Forms> show_hide = new List<Show_Hide_Forms>();
        static List<Stats_Reports> stats_reports = new List<Stats_Reports>();
        static List<Individual_Summary> individual_reports = new List<Individual_Summary>();
        static List<Hygiene_Report> hygiene_report = new List<Hygiene_Report>();
        static List<Test> tests = new List<Test>();
        static List<Member_Detail> members = new List<Member_Detail>();

        static bool resultProductionGoals;
        static bool resultSmartNumbers;
        static bool resultCriticalNumbers;
        static bool resultLocationFromLive;
        static bool resultStatsAllReports;
        static bool resultIndividualReports;
        static bool resulthygieneReports;
        static bool memberdetails;
        //static string resultClinical_ProductionGoals = string.Empty;
        //static string resultAdmin_ProductionGoals = string.Empty;

        static void Main(string[] args)
        {
            Execute();
            FillRecord();
        }

        public static void Execute()
        {
            hygiene_report = Records.Get_Hygiene_List();
            // members = Records.GetMemberDetails();
            // memberdetails = FillData.FillMember_Details(members);
            // smartNumbers = Records.GetSmartNumbers();
            //resultSmartNumbers = FillData.FillSmartNumbers(smartNumbers);
            //provider = Records.GetProviderProductionGoals_();
            //resultProductionGoals = FillData.FillProviderProductionGoals(provider);
            //criticalNumbers = Records.GetCriticalNumbers();
            //// Changes By Bhupesh On Dated : 30-04-2019
            //stats_reports = Records.Get_stats_List();
            //resultStatsAllReports = FillData.FillStatsAll(stats_reports);
            //individual_reports = Records.Get_Individual_List();
            //hygiene_report = Records.Get_Hygiene_List();
        }

        public static void FillRecord()
        {
           // resultCriticalNumbers = FillData.FillCriticalNumbers(criticalNumbers);
            // Changes By Bhupesh On Dated: 30 - 04 - 2019
          //  resultIndividualReports = FillData.FillIndividualAll(individual_reports);
            resulthygieneReports = FillData.FillHygieneAll(hygiene_report);
            // resultClinical_ProductionGoals = FillData.FillClinicalProductionGoals(clinicalProductionGoals);
            // resultAdmin_ProductionGoals    = FillData.FillAdminProductionGoals(adminProductionGoals);
        }

        public static void SendEmail()
        {
        }

    }
}
